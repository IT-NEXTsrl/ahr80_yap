* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsveqbst                                                        *
*              Stampa schede trasporto                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-09-28                                                      *
* Last revis.: 2013-07-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_MVSERIAL,w_MVCODVET,w_MVDATINI,w_MVDATFIN,w_MVTIPELA,w_MVFLRIEP,w_MVFLFORZ,w_MVMAGPAR,w_MVGRUMER,w_MVTIPCOM,w_MVRIFCOM,w_MVPIVCOM,w_MVTIPPRO,w_MVRIFPRO,w_MVPIVPRO,w_MVTIPCAR,w_MVRIFCAR,w_MVPIVCAR,w_MVEVEIST,w_MVSTDDIC,w_GMDESCRI,w_MVCITPAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsveqbst",oParentObject,m.w_MVSERIAL,m.w_MVCODVET,m.w_MVDATINI,m.w_MVDATFIN,m.w_MVTIPELA,m.w_MVFLRIEP,m.w_MVFLFORZ,m.w_MVMAGPAR,m.w_MVGRUMER,m.w_MVTIPCOM,m.w_MVRIFCOM,m.w_MVPIVCOM,m.w_MVTIPPRO,m.w_MVRIFPRO,m.w_MVPIVPRO,m.w_MVTIPCAR,m.w_MVRIFCAR,m.w_MVPIVCAR,m.w_MVEVEIST,m.w_MVSTDDIC,m.w_GMDESCRI,m.w_MVCITPAR)
return(i_retval)

define class tgsveqbst as StdBatch
  * --- Local variables
  w_MVSERIAL = space(10)
  w_MVCODVET = space(5)
  w_MVDATINI = ctod("  /  /  ")
  w_MVDATFIN = ctod("  /  /  ")
  w_MVTIPELA = space(1)
  w_MVFLRIEP = space(1)
  w_MVFLFORZ = space(1)
  w_MVMAGPAR = space(5)
  w_MVGRUMER = space(5)
  w_MVTIPCOM = space(1)
  w_MVRIFCOM = space(0)
  w_MVPIVCOM = space(12)
  w_MVTIPPRO = space(1)
  w_MVRIFPRO = space(0)
  w_MVPIVPRO = space(12)
  w_MVTIPCAR = space(1)
  w_MVRIFCAR = space(0)
  w_MVPIVCAR = space(12)
  w_MVEVEIST = space(0)
  w_MVSTDDIC = space(50)
  w_GMDESCRI = space(35)
  w_MVCITPAR = space(30)
  w_AZPIVAZI = space(12)
  w_CURSTMP = space(10)
  w_OLSERIAL = space(10)
  w_ATSERIAL = space(10)
  w_LST_TIPO = space(0)
  w_LOOP = 0
  w_LOOPINS = 0
  w_FIRSTDOC = .f.
  w_MGRIFFOR = space(15)
  w_MGGESCAR = space(1)
  w_MGINDMAG = space(30)
  w_MGCAPMAG = space(8)
  w_MGCITMAG = space(25)
  w_MGPROMAG = space(2)
  w_ANDESCRI = space(40)
  w_ANINDIRI = space(35)
  w_AN___CAP = space(13)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_ANTELEFO = space(18)
  w_ANTELFAX = space(18)
  w_AN_EMAIL = space(254)
  w_FLCOMDIF = .f.
  w_FLCARDIF = .f.
  w_FLPRODIF = .f.
  w_INICOM = space(20)
  w_INICAR = space(20)
  w_INIPRO = space(20)
  w_RIFDOC = space(0)
  w_SERIALWR = space(10)
  w_OLDSED = space(5)
  w_MVTCOMAG = space(3)
  w_OKMAG = .f.
  w_AZ_EMAIL = space(254)
  w_APDESDOC = space(254)
  w_PADRE = .NULL.
  w_PADRE = .NULL.
  w_OBJCTRL = .NULL.
  w_codese = space(4)
  w_datain = ctod("  /  /  ")
  w_datafi = ctod("  /  /  ")
  w_numini = 0
  w_numfin = 0
  w_serie1 = space(10)
  w_serie2 = space(10)
  w_clifor = space(15)
  w_fc = space(1)
  w_TIPOIN = space(5)
  w_TMPARIVA = space(12)
  w_UTE__STA = space(85)
  w_DPNOME = space(40)
  w_DPCOGNOM = space(40)
  w_CODAGE = space(5)
  w_LINGUA = space(3)
  w_CATEGO = space(2)
  w_CODVET = space(5)
  w_CODZON = space(3)
  w_CATCOM = space(3)
  w_CODDES = space(5)
  w_CODPAG = space(5)
  w_NOSBAN = space(5)
  w_PSSERIAL = space(10)
  * --- WorkFile variables
  TMPVEND1_idx=0
  AZIENDA_idx=0
  gsveqbst_idx=0
  DOC_MAST_idx=0
  TMPVEND3_idx=0
  MAGAZZIN_idx=0
  CONTI_idx=0
  DOC_DETT_idx=0
  TMPDOCUM_idx=0
  TMPCRUART_idx=0
  DIPENDEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_MVCODVET = IIF(vartype(this.w_MVCODVET)<>"C", "", this.w_MVCODVET)
    this.w_MVDATINI = IIF(vartype(this.w_MVDATINI)="L", i_inidat, this.w_MVDATINI)
    this.w_MVDATFIN = IIF(vartype(this.w_MVDATFIN)="L", i_findat, this.w_MVDATFIN)
    this.w_MVTIPELA = IIF( vartype(this.w_MVTIPELA)<>"C", "D", this.w_MVTIPELA)
    this.w_MVFLRIEP = IIF(vartype(this.w_MVFLRIEP)<>"C", "N", this.w_MVFLRIEP )
    this.w_MVFLFORZ = IIF(vartype(this.w_MVFLFORZ)<>"C", "N", this.w_MVFLFORZ )
    this.w_MVMAGPAR = IIF( vartype(this.w_MVMAGPAR)<>"C", "", this.w_MVMAGPAR)
    this.w_MVGRUMER = IIF( vartype( this.w_MVGRUMER)<>"C", "", this.w_MVGRUMER)
    this.w_MVTIPCOM = IIF( vartype( this.w_MVTIPCOM )<>"C", "", this.w_MVTIPCOM )
    this.w_MVRIFCOM = IIF( vartype( this.w_MVRIFCOM )<>"C", "", this.w_MVRIFCOM )
    this.w_MVPIVCOM = IIF( vartype( this.w_MVPIVCOM )<>"C", "", this.w_MVPIVCOM )
    this.w_MVTIPPRO = IIF( vartype( this.w_MVTIPPRO )<>"C", "", this.w_MVTIPPRO )
    this.w_MVRIFPRO = IIF( vartype( this.w_MVRIFPRO )<>"C", "", this.w_MVRIFPRO )
    this.w_MVPIVPRO = IIF( vartype( this.w_MVPIVPRO )<>"C", "", this.w_MVPIVPRO )
    this.w_MVTIPCAR = IIF( vartype( this.w_MVTIPCAR )<>"C", "", this.w_MVTIPCAR )
    this.w_MVRIFCAR = IIF( vartype( this.w_MVRIFCAR )<>"C", "", this.w_MVRIFCAR )
    this.w_MVPIVCAR = IIF( vartype( this.w_MVPIVCAR )<>"C", "", this.w_MVPIVCAR )
    this.w_MVEVEIST = IIF( vartype( this.w_MVEVEIST )<>"C", "", this.w_MVEVEIST )
    this.w_MVSTDDIC = IIF( vartype( this.w_MVSTDDIC )<>"C", "", this.w_MVSTDDIC )
    this.w_GMDESCRI = IIF( vartype( this.w_GMDESCRI )<>"C", "", this.w_GMDESCRI )
    this.w_MVCITPAR = IIF( vartype( this.w_MVCITPAR )<>"C", "", this.w_MVCITPAR )
    this.w_PADRE = this.oParentObject
    do case
      case !EMPTY(this.w_MVSERIAL) AND this.w_MVTIPELA<>"M"
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_QNTULFIKIV[1]
        indexes_QNTULFIKIV[1]='MVSERIAL'
        i_nConn=i_TableProp[this.DOC_MAST_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"MVSERIAL "," from "+i_cTable;
              +" where 1=0";
              ,.f.,@indexes_QNTULFIKIV)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Insert into TMPVEND3
        i_nConn=i_TableProp[this.TMPVEND3_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPVEND3_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MVSERIAL"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'TMPVEND3','MVSERIAL');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_MVSERIAL)
          insert into (i_cTable) (MVSERIAL &i_ccchkf. );
             values (;
               this.w_MVSERIAL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case vartype (this.w_PADRE.oParentObject)=="O" AND upper(this.w_PADRE.oParentObject.class)=="TGSVE_SDV"
        this.w_codese = this.w_PADRE.oParentObject.w_codese
        this.w_datain = this.w_PADRE.oParentObject.w_datain
        this.w_datafi = this.w_PADRE.oParentObject.w_datafi
        this.w_numini = this.w_PADRE.oParentObject.w_numini
        this.w_numfin = this.w_PADRE.oParentObject.w_numfin
        this.w_serie1 = this.w_PADRE.oParentObject.w_serie1
        this.w_serie2 = this.w_PADRE.oParentObject.w_serie2
        this.w_clifor = this.w_PADRE.oParentObject.w_clifor
        this.w_fc = this.w_PADRE.oParentObject.w_fc
        this.w_CODAGE = this.w_PADRE.oParentObject.w_CODAGE
        this.w_LINGUA = this.w_PADRE.oParentObject.w_LINGUA
        this.w_CATEGO = this.w_PADRE.oParentObject.w_CATEGO
        this.w_CODVET = this.w_PADRE.oParentObject.w_CODVET
        this.w_CODZON = this.w_PADRE.oParentObject.w_CODZON
        this.w_CATCOM = this.w_PADRE.oParentObject.w_CATCOM
        this.w_CODDES = this.w_PADRE.oParentObject.w_CODDES
        this.w_CODPAG = this.w_PADRE.oParentObject.w_CODPAG
        this.w_NOSBAN = this.w_PADRE.oParentObject.w_NOSBAN
        if vartype(this.w_PADRE.oParentObject.w_TIPOIN)="C" AND EMPTY(this.w_PADRE.oParentObject.w_TIPOIN)
          * --- Create temporary table TMPVEND3
          i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsve_bs6',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVEND3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          this.w_TIPOIN = this.w_PADRE.oParentObject.w_TIPOIN
          * --- Create temporary table TMPVEND3
          i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsve_bs7',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVEND3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
      case vartype (this.w_PADRE.oParentObject)=="O" AND upper(this.w_PADRE.oParentObject.class)=="TGSVA_BPS"
        * --- Recuper il seriale del piano di spedizione dalla maschera GSVA_APS e carico i seriali dei documenti collegati
        this.w_PSSERIAL = this.w_PADRE.oParentObject.oParentObject.w_PSSERIAL
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gsve_bs8',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endcase
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_FAVNUMKQJH[1]
    indexes_FAVNUMKQJH[1]='MVSERIAL'
    i_nConn=i_TableProp[this.DOC_DETT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"MVSERIAL,MVCODMAG "," from "+i_cTable;
          +" where 1=0";
          ,.f.,@indexes_FAVNUMKQJH)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_ATSERIAL = ""
    this.w_OLSERIAL = ""
    this.w_SERIALWR = ""
    this.w_INICOM = ""
    this.w_OLDSED = ""
    this.w_OKMAG = .F.
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      * --- Select from gsve5bst
      do vq_exec with 'gsve5bst',this,'_Curs_gsve5bst','',.f.,.t.
      if used('_Curs_gsve5bst')
        select _Curs_gsve5bst
        locate for 1=1
        do while not(eof())
        this.w_ATSERIAL = _Curs_gsve5bst.MVSERIAL
        if this.w_OLSERIAL<>this.w_ATSERIAL
          if this.w_OKMAG
            * --- Insert into TMPVEND1
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPVEND1_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MVSERIAL"+",MVCODMAG"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_OLSERIAL),'TMPVEND1','MVSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_INICOM),'TMPVEND1','MVCODMAG');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_OLSERIAL,'MVCODMAG',this.w_INICOM)
              insert into (i_cTable) (MVSERIAL,MVCODMAG &i_ccchkf. );
                 values (;
                   this.w_OLSERIAL;
                   ,this.w_INICOM;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Create temporary table TMPVEND1
            i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('query\gsve3bst',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPVEND1_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
          this.w_OKMAG = .F.
          this.w_OLDSED = ""
          this.w_SERIALWR = ""
          this.w_INICOM = ""
        endif
        if this.w_ATSERIAL<>this.w_SERIALWR
          * --- Il seriale attuale non � da scartare, occorre analizzarlo
          if !EMPTY(this.w_OLDSED) AND this.w_ATSERIAL=this.w_OLSERIAL AND NVL( _Curs_gsve5bst.MGCODSED , " ")<>this.w_OLDSED
            * --- Magazzini diversi del solito documenti link su diversi sedi azienda
            this.w_SERIALWR = this.w_ATSERIAL
            * --- Verifico magazzino di testata
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVTCOMAG"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVSERIAL = "+cp_ToStrODBC(w_ATSERIAWR);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVTCOMAG;
                from (i_cTable) where;
                    MVSERIAL = w_ATSERIAWR;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MVTCOMAG = NVL(cp_ToDate(_read_.MVTCOMAG),cp_NullValue(_read_.MVTCOMAG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_INICOM = this.w_MVTCOMAG
            this.w_OKMAG = !EMPTY(this.w_MVTCOMAG)
          endif
          this.w_OLDSED = NVL( _Curs_gsve5bst.MGCODSED , " ")
          this.w_OLSERIAL = this.w_ATSERIAL
        endif
          select _Curs_gsve5bst
          continue
        enddo
        use
      endif
    else
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gsve3bst',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    this.w_SERIALWR = ""
    * --- Lettura dati aggiuntivi azienda
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZPIVAZI,AZ_EMAIL"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZPIVAZI,AZ_EMAIL;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZPIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      this.w_AZ_EMAIL = NVL(cp_ToDate(_read_.AZ_EMAIL),cp_NullValue(_read_.AZ_EMAIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE"
      public g_EMAIL
      g_EMAIL = this.w_AZ_EMAIL
    endif
    if !EMPTY(this.w_MVMAGPAR)
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGGESCAR,MGRIFFOR,MGINDMAG,MGCAPMAG,MGCITMAG,MGPROMAG"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_MVMAGPAR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGGESCAR,MGRIFFOR,MGINDMAG,MGCAPMAG,MGCITMAG,MGPROMAG;
          from (i_cTable) where;
              MGCODMAG = this.w_MVMAGPAR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MGGESCAR = NVL(cp_ToDate(_read_.MGGESCAR),cp_NullValue(_read_.MGGESCAR))
        this.w_MGRIFFOR = NVL(cp_ToDate(_read_.MGRIFFOR),cp_NullValue(_read_.MGRIFFOR))
        this.w_MGINDMAG = NVL(cp_ToDate(_read_.MGINDMAG),cp_NullValue(_read_.MGINDMAG))
        this.w_MGCAPMAG = NVL(cp_ToDate(_read_.MGCAPMAG),cp_NullValue(_read_.MGCAPMAG))
        this.w_MGCITMAG = NVL(cp_ToDate(_read_.MGCITMAG),cp_NullValue(_read_.MGCITMAG))
        this.w_MGPROMAG = NVL(cp_ToDate(_read_.MGPROMAG),cp_NullValue(_read_.MGPROMAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE"
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGMAGCAP"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.w_MVMAGPAR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGMAGCAP;
            from (i_cTable) where;
                MGCODMAG = this.w_MVMAGPAR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MGCAPMAG = NVL(cp_ToDate(_read_.MGMAGCAP),cp_NullValue(_read_.MGMAGCAP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.w_MGGESCAR = "S"
        if !EMPTY(NVL(this.w_MGRIFFOR, " "))
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANTELEFO,ANTELFAX,AN_EMAIL,ANPARIVA"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC("F");
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_MGRIFFOR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANTELEFO,ANTELFAX,AN_EMAIL,ANPARIVA;
              from (i_cTable) where;
                  ANTIPCON = "F";
                  and ANCODICE = this.w_MGRIFFOR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ANDESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
            this.w_ANINDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
            this.w_AN___CAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
            this.w_ANLOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
            this.w_ANPROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
            this.w_ANTELEFO = NVL(cp_ToDate(_read_.ANTELEFO),cp_NullValue(_read_.ANTELEFO))
            this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
            this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
            this.w_TMPARIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_ANDESCRI = g_RAGAZI
          this.w_ANINDIRI = g_INDAZI
          this.w_AN___CAP = g_CAPAZI
          this.w_ANLOCALI = g_LOCAZI
          this.w_ANPROVIN = g_PROAZI
          this.w_ANTELEFO = g_TELEFO
          this.w_ANTELFAX = g_TELFAX
          this.w_AN_EMAIL = g_EMAIL
          this.w_TMPARIVA = this.w_AZPIVAZI
        endif
      endif
    endif
    if this.w_MVTIPCAR<>"A"
      this.w_MVPIVCAR = this.w_TMPARIVA
    endif
    * --- Prepara tabella temporanea output
    this.w_CURSTMP = SYS(2015)
    this.w_ATSERIAL = ""
    this.w_OLSERIAL = ""
    this.w_LST_TIPO = ""
    this.w_LOOP = 1
    this.w_FIRSTDOC = .T.
    this.w_FLCOMDIF = .F.
    this.w_FLCARDIF = .F.
    this.w_FLPRODIF = .F.
    this.w_INICOM = ""
    this.w_INICAR = ""
    this.w_INIPRO = ""
    this.w_RIFDOC = ""
    this.w_UTE__STA = ""
    * --- Read from DIPENDEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIPENDEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DPCOGNOM,DPNOME"+;
        " from "+i_cTable+" DIPENDEN where ";
            +"DPCODUTE = "+cp_ToStrODBC(i_codute);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DPCOGNOM,DPNOME;
        from (i_cTable) where;
            DPCODUTE = i_codute;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DPCOGNOM = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
      this.w_DPNOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_UTE__STA = ALLTRIM(ALLTRIM(NVL(this.w_DPNOME, " "))+" "+ALLTRIM(NVL(this.w_DPCOGNOM, " ")))
    this.w_UTE__STA = IIF(EMPTY(ALLTRIM(NVL(this.w_UTE__STA, " "))), g_DESUTE, this.w_UTE__STA )
    * --- Su SQL Server 2000 non � possibile creare una sola tabella per la dimensione
    *     massima di 8K per record. Avendo due campi memo si � costretti a spezzare
    *     la query in que e poi unirle al termine delle operazioni
    vq_exec("query\gsve_bs2.vqr", this, this.w_CURSTMP )
    * --- Create temporary table TMPDOCUM
    i_nIdx=cp_AddTableDef('TMPDOCUM') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsve_bs3',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPDOCUM_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPCRUART
    i_nIdx=cp_AddTableDef('TMPCRUART') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsve_bs4',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPCRUART_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if USED(this.w_CURSTMP)
      SELECT (this.w_CURSTMP)
      GO TOP
      SCAN
      this.w_ATSERIAL = MVSERIAL
      if EMPTY(this.w_INICOM)
        this.w_INICOM = ICASE(this.w_MVTIPCOM="D", NVL(MVCODCON," "), this.w_MVTIPCOM="P", NVL(MVCODORN," "), " ")
      endif
      if EMPTY(this.w_INICAR)
        this.w_INICAR = ICASE(this.w_MVTIPCAR="D", NVL(MVCODCON," "), this.w_MVTIPCAR="P", NVL(MVCODORN," "), " ")
      endif
      if EMPTY(this.w_INIPRO)
        this.w_INIPRO = ICASE(this.w_MVTIPPRO="D", NVL(MVCODCON, " "), this.w_MVTIPPRO="P", NVL(MVCODORN, " "), " ")
      endif
      if this.w_MVFLRIEP="S"
        this.w_FLCOMDIF = this.w_FLCOMDIF OR this.w_INICOM<>ICASE(this.w_MVTIPCOM="D", NVL(MVCODCON," "), this.w_MVTIPCOM="P", NVL(MVCODORN," "), " ")
        this.w_FLCARDIF = this.w_FLCARDIF OR this.w_INICAR<>ICASE(this.w_MVTIPCAR="D", NVL(MVCODCON," "), this.w_MVTIPCAR="P", NVL(MVCODORN," "), " ")
        this.w_FLPRODIF = this.w_FLPRODIF OR this.w_INIPRO<>ICASE(this.w_MVTIPPRO="D", NVL(MVCODCON, " "), this.w_MVTIPPRO="P", NVL(MVCODORN, " "), " ")
        this.w_APDESDOC = ah_MsgFormat("Nr." + ALLTRIM(STR(MVNUMDOC)) + IIF(EMPTY(NVL(MVALFDOC, " ")), "", "/"+ALLTRIM(MVALFDOC))) + ah_msgformat(" del ")+DTOC(MVDATDOC)
        if !(this.w_APDESDOC $ this.w_RIFDOC)
          this.w_RIFDOC = this.w_RIFDOC + ALLTRIM(this.w_APDESDOC) + " - "
        endif
      endif
      if !EMPTY(this.w_OLSERIAL) AND this.w_OLSERIAL<>this.w_ATSERIAL
        * --- Elimino l'ultima virgola
        this.w_LST_TIPO = ICASE( this.w_MVFLFORZ="S" AND !EMPTY(this.w_GMDESCRI), ALLTRIM(this.w_GMDESCRI), this.w_MVFLRIEP="S", IIF(!EMPTY(this.w_GMDESCRI), ALLTRIM(this.w_GMDESCRI), this.w_MVSTDDIC) , LEFT(this.w_LST_TIPO, LEN(this.w_LST_TIPO)-1) )
        * --- Aggiorno il record nella tabella temporanea
        * --- Write into TMPCRUART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPCRUART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCRUART_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCRUART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GMDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_LST_TIPO),'TMPCRUART','GMDESCRI');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_OLSERIAL);
                 )
        else
          update (i_cTable) set;
              GMDESCRI = this.w_LST_TIPO;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_OLSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_LST_TIPO = ""
        this.w_LOOP = 1
      endif
      if this.w_LOOP = 1 AND (this.w_FIRSTDOC OR this.w_MVFLRIEP<>"S" )
        * --- Inserisco nella tabella i valori del record corente
        AFIELDS(L_ARRFLD, (this.w_CURSTMP) )
        this.w_LOOP = ALEN( L_ARRFLD, 1)
        this.w_LOOPINS = this.w_LOOP - 1
        DIMENSION L_ARRINS(ALEN(L_ARRFLD, 1) - 1, 2)
        DIMENSION L_ARRINS2(3, 2)
        L_ARRINS2(2,1) = "LSTDOCAS"
        L_ARRINS2(2,2) = SPACE(1)
        L_ARRINS2(3,1) = "GMDESCRI"
        L_ARRINS2(3,2) = SPACE(1)
        * --- Scansione array campi e gestione eccezione per descrizione gruppo merceologico
        *     e lista documenti associati
        do while this.w_LOOP>0
          do case
            case L_ARRFLD(this.w_LOOP,1)="MVSERIAL"
              L_ARRINS(this.w_LOOPINS,1) = L_ARRFLD(this.w_LOOP, 1)
              L_ARRINS(this.w_LOOPINS,2) = NVL( &L_ARRFLD(this.w_LOOP, 1), SPACE(1))
              L_ARRINS2(1,1) = "MVSERIAL"
              L_ARRINS2(1,2) = NVL( &L_ARRFLD(this.w_LOOP, 1), SPACE(1))
              this.w_LOOPINS = this.w_LOOPINS - 1
            case L_ARRFLD(this.w_LOOP,1)="LSTDOCAS"
            case L_ARRFLD(this.w_LOOP,1)="GMDESCRI"
            otherwise
              L_ARRINS(this.w_LOOPINS,1) = L_ARRFLD(this.w_LOOP, 1)
              L_ARRINS(this.w_LOOPINS,2) = NVL( &L_ARRFLD(this.w_LOOP, 1), SPACE(1))
              this.w_LOOPINS = this.w_LOOPINS - 1
          endcase
          this.w_LOOP = this.w_LOOP - 1
        enddo
        inserttable("TMPDOCUM", @L_ARRINS)
        inserttable("TMPCRUART", @L_ARRINS2)
        RELEASE L_ARRFLD, L_ARRINS, L_ARRINS2
        this.w_LOOP = 1
        this.w_FIRSTDOC = .F.
        this.w_SERIALWR = this.w_ATSERIAL
      endif
      if !(ALLTRIM(GMDESCRI) $ this.w_LST_TIPO)
        this.w_LST_TIPO = this.w_LST_TIPO + ALLTRIM(GMDESCRI) + ","
      endif
      this.w_LOOP = this.w_LOOP + 1
      this.w_OLSERIAL = this.w_ATSERIAL
      SELECT (this.w_CURSTMP)
      ENDSCAN
      * --- Elimino l'ultima virgola
      this.w_LST_TIPO = ICASE( this.w_MVFLFORZ="S" AND !EMPTY(this.w_GMDESCRI), ALLTRIM(this.w_GMDESCRI), this.w_MVFLRIEP="S", IIF(!EMPTY(this.w_GMDESCRI), ALLTRIM(this.w_GMDESCRI), this.w_MVSTDDIC) , LEFT(this.w_LST_TIPO, LEN(this.w_LST_TIPO)-1) )
      this.w_RIFDOC = IIF(!EMPTY(this.w_RIFDOC), ah_msgformat("Doc.: ")+LEFT(this.w_RIFDOC, LEN(this.w_RIFDOC)-2), "")
      * --- Aggiorno l'ultimo record nella tabella temporanea
      * --- Write into TMPCRUART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPCRUART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPCRUART_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCRUART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"LSTDOCAS ="+cp_NullLink(cp_ToStrODBC(IIF( this.w_MVFLRIEP="S", this.w_RIFDOC, "")),'TMPCRUART','LSTDOCAS');
        +",GMDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_LST_TIPO),'TMPCRUART','GMDESCRI');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIALWR);
               )
      else
        update (i_cTable) set;
            LSTDOCAS = IIF( this.w_MVFLRIEP="S", this.w_RIFDOC, "");
            ,GMDESCRI = this.w_LST_TIPO;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SERIALWR;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if this.w_FLCOMDIF
        if vartype(this.w_PADRE.oParentObject.oParentObject)="O"
          this.w_OBJCTRL = this.w_PADRE.oParentObject.oParentObject.GetCtrl("w_MVTIPCOM")
        endif
        * --- Write into TMPDOCUM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDOCUM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AHRAGCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVSTDDIC+": "+ALLTRIM(this.w_OBJCTRL.DisplayValue)),'TMPDOCUM','AHRAGCOM');
          +",AHINDCOM ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHINDCOM');
          +",AHCAPCOM ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHCAPCOM');
          +",AHLOCCOM ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHLOCCOM');
          +",AHPROCOM ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHPROCOM');
          +",AHTELCOM ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHTELCOM');
          +",AHFAXCOM ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHFAXCOM');
          +",AHPIVCOM ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHPIVCOM');
          +",AHMAICOM ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHMAICOM');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIALWR);
                 )
        else
          update (i_cTable) set;
              AHRAGCOM = this.w_MVSTDDIC+": "+ALLTRIM(this.w_OBJCTRL.DisplayValue);
              ,AHINDCOM = "";
              ,AHCAPCOM = "";
              ,AHLOCCOM = "";
              ,AHPROCOM = "";
              ,AHTELCOM = "";
              ,AHFAXCOM = "";
              ,AHPIVCOM = "";
              ,AHMAICOM = "";
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERIALWR;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.w_FLCARDIF AND this.w_MVTIPCAR<>"Z"
        if vartype(this.w_PADRE.oParentObject.oParentObject)="O"
          this.w_OBJCTRL = this.w_PADRE.oParentObject.oParentObject.GetCtrl("w_MVTIPCAR")
        endif
        * --- Write into TMPDOCUM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDOCUM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AHRAGCAR ="+cp_NullLink(cp_ToStrODBC(this.w_MVSTDDIC+": "+ALLTRIM(this.w_OBJCTRL.DisplayValue)),'TMPDOCUM','AHRAGCAR');
          +",AHINDCAR ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHINDCAR');
          +",AHCAPCAR ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHCAPCAR');
          +",AHLOCCAR ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHLOCCAR');
          +",AHPROCAR ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHPROCAR');
          +",AHTELCAR ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHTELCAR');
          +",AHFAXCAR ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHFAXCAR');
          +",AHPIVCAR ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHPIVCAR');
          +",AHMAICAR ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHMAICAR');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIALWR);
                 )
        else
          update (i_cTable) set;
              AHRAGCAR = this.w_MVSTDDIC+": "+ALLTRIM(this.w_OBJCTRL.DisplayValue);
              ,AHINDCAR = "";
              ,AHCAPCAR = "";
              ,AHLOCCAR = "";
              ,AHPROCAR = "";
              ,AHTELCAR = "";
              ,AHFAXCAR = "";
              ,AHPIVCAR = "";
              ,AHMAICAR = "";
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERIALWR;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.w_FLPRODIF
        if vartype(this.w_PADRE.oParentObject.oParentObject)="O"
          this.w_OBJCTRL = this.w_PADRE.oParentObject.oParentObject.GetCtrl("w_MVTIPPRO")
        endif
        * --- Write into TMPDOCUM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDOCUM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AHRAGPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVSTDDIC+": "+ALLTRIM(this.w_OBJCTRL.DisplayValue)),'TMPDOCUM','AHRAGPRO');
          +",AHINDPRO ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHINDPRO');
          +",AHCAPPRO ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHCAPPRO');
          +",AHLOCPRO ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHLOCPRO');
          +",AHPROPRO ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHPROPRO');
          +",AHTELPRO ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHTELPRO');
          +",AHFAXPRO ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHFAXPRO');
          +",AHPIVPRO ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHPIVPRO');
          +",AHMAIPRO ="+cp_NullLink(cp_ToStrODBC(""),'TMPDOCUM','AHMAIPRO');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIALWR);
                 )
        else
          update (i_cTable) set;
              AHRAGPRO = this.w_MVSTDDIC+": "+ALLTRIM(this.w_OBJCTRL.DisplayValue);
              ,AHINDPRO = "";
              ,AHCAPPRO = "";
              ,AHLOCPRO = "";
              ,AHPROPRO = "";
              ,AHTELPRO = "";
              ,AHFAXPRO = "";
              ,AHPIVPRO = "";
              ,AHMAIPRO = "";
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERIALWR;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    USE IN SELECT (this.w_CURSTMP)
    * --- Create temporary table gsveqbst
    i_nIdx=cp_AddTableDef('gsveqbst') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsve_bs5',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.gsveqbst_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Delete from gsveqbst
    i_nConn=i_TableProp[this.gsveqbst_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.gsveqbst_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MVSERIAL = "+cp_ToStrODBC(SPACE(10));
             )
    else
      delete from (i_cTable) where;
            MVSERIAL = SPACE(10);

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
    * --- Drop temporary table TMPVEND3
    i_nIdx=cp_GetTableDefIdx('TMPVEND3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND3')
    endif
    * --- Drop temporary table TMPDOCUM
    i_nIdx=cp_GetTableDefIdx('TMPDOCUM')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDOCUM')
    endif
    * --- Drop temporary table TMPCRUART
    i_nIdx=cp_GetTableDefIdx('TMPCRUART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPCRUART')
    endif
    if UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE"
      release g_EMAIL
    endif
  endproc


  proc Init(oParentObject,w_MVSERIAL,w_MVCODVET,w_MVDATINI,w_MVDATFIN,w_MVTIPELA,w_MVFLRIEP,w_MVFLFORZ,w_MVMAGPAR,w_MVGRUMER,w_MVTIPCOM,w_MVRIFCOM,w_MVPIVCOM,w_MVTIPPRO,w_MVRIFPRO,w_MVPIVPRO,w_MVTIPCAR,w_MVRIFCAR,w_MVPIVCAR,w_MVEVEIST,w_MVSTDDIC,w_GMDESCRI,w_MVCITPAR)
    this.w_MVSERIAL=w_MVSERIAL
    this.w_MVCODVET=w_MVCODVET
    this.w_MVDATINI=w_MVDATINI
    this.w_MVDATFIN=w_MVDATFIN
    this.w_MVTIPELA=w_MVTIPELA
    this.w_MVFLRIEP=w_MVFLRIEP
    this.w_MVFLFORZ=w_MVFLFORZ
    this.w_MVMAGPAR=w_MVMAGPAR
    this.w_MVGRUMER=w_MVGRUMER
    this.w_MVTIPCOM=w_MVTIPCOM
    this.w_MVRIFCOM=w_MVRIFCOM
    this.w_MVPIVCOM=w_MVPIVCOM
    this.w_MVTIPPRO=w_MVTIPPRO
    this.w_MVRIFPRO=w_MVRIFPRO
    this.w_MVPIVPRO=w_MVPIVPRO
    this.w_MVTIPCAR=w_MVTIPCAR
    this.w_MVRIFCAR=w_MVRIFCAR
    this.w_MVPIVCAR=w_MVPIVCAR
    this.w_MVEVEIST=w_MVEVEIST
    this.w_MVSTDDIC=w_MVSTDDIC
    this.w_GMDESCRI=w_GMDESCRI
    this.w_MVCITPAR=w_MVCITPAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='*TMPVEND1'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='*gsveqbst'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='*TMPVEND3'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='DOC_DETT'
    this.cWorkTables[9]='*TMPDOCUM'
    this.cWorkTables[10]='*TMPCRUART'
    this.cWorkTables[11]='DIPENDEN'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_gsve5bst')
      use in _Curs_gsve5bst
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_MVSERIAL,w_MVCODVET,w_MVDATINI,w_MVDATFIN,w_MVTIPELA,w_MVFLRIEP,w_MVFLFORZ,w_MVMAGPAR,w_MVGRUMER,w_MVTIPCOM,w_MVRIFCOM,w_MVPIVCOM,w_MVTIPPRO,w_MVRIFPRO,w_MVPIVPRO,w_MVTIPCAR,w_MVRIFCAR,w_MVPIVCAR,w_MVEVEIST,w_MVSTDDIC,w_GMDESCRI,w_MVCITPAR"
endproc
