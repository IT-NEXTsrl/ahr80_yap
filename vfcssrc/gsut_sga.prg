* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_sga                                                        *
*              Stampa globale accessi                                          *
*                                                                              *
*      Author: ZUCCHETTI S.P.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-19                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_sga
*Questa gestione la pu� usare solo l'amministratore
if not cp_IsAdministrator(.t.)
  ah_ERRORMSG("Accesso negato",'stop',"Gestione sicurezza")
  return null
endif
* --- Fine Area Manuale
return(createobject("tgsut_sga",oParentObject))

* --- Class definition
define class tgsut_sga as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 467
  Height = 177
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-02"
  HelpContextID=244759703
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  CPGROUPS_IDX = 0
  CPUSERS_IDX = 0
  TSMENUVO_IDX = 0
  cPrg = "gsut_sga"
  cComment = "Stampa globale accessi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODUTE = 0
  w_CODGRU = 0
  w_PROGRAM = space(10)
  w_DESCPROG = space(30)
  w_OQRY = space(50)
  w_OREP = space(50)
  w_CODEDESU = space(40)
  w_CODEDESC = space(40)
  w_CODREL = space(10)
  w_MODULO = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_sgaPag1","gsut_sga",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODUTE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CPGROUPS'
    this.cWorkTables[2]='CPUSERS'
    this.cWorkTables[3]='TSMENUVO'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODUTE=0
      .w_CODGRU=0
      .w_PROGRAM=space(10)
      .w_DESCPROG=space(30)
      .w_OQRY=space(50)
      .w_OREP=space(50)
      .w_CODEDESU=space(40)
      .w_CODEDESC=space(40)
      .w_CODREL=space(10)
      .w_MODULO=space(2)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODUTE))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODGRU))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PROGRAM))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_OQRY = 'QUERY\GSUT_SGA.VQR'
        .w_OREP = 'QUERY\GSUT_SGA.FRX'
          .DoRTCalc(7,8,.f.)
        .w_CODREL = ALLTRIM(STRTRAN(UPPER(g_VERSION),'REL.'))
        .w_MODULO = "0000"
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_sga
    * TRADUCE LE DESCRIZIONI DEI MODULI
    DO GSUT_BMA WITH THIS
    
    * --- derivata dalla classe combo da tabella
    
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_MODULO")
    CTRL_CONCON.Popola()
    * --- valorizzo a tutti la combo...
    this.SetControlsValue()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODUTE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCODUTE_1_1'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.CODE,0)
      this.w_CODEDESU = NVL(_Link_.NAME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_CODEDESU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODGRU);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODGRU)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oCODGRU_1_2'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODGRU)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.CODE,0)
      this.w_CODEDESC = NVL(_Link_.NAME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = 0
      endif
      this.w_CODEDESC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PROGRAM
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TSMENUVO_IDX,3]
    i_lTable = "TSMENUVO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TSMENUVO_IDX,2], .t., this.TSMENUVO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TSMENUVO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROGRAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TSMENUVO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PROGRAM like "+cp_ToStrODBC(trim(this.w_PROGRAM)+"%");

          i_ret=cp_SQL(i_nConn,"select PROGRAM,PROGDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PROGRAM","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PROGRAM',trim(this.w_PROGRAM))
          select PROGRAM,PROGDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PROGRAM into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROGRAM)==trim(_Link_.PROGRAM) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROGRAM) and !this.bDontReportError
            deferred_cp_zoom('TSMENUVO','*','PROGRAM',cp_AbsName(oSource.parent,'oPROGRAM_1_3'),i_cWhere,'',"Elenco programmi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PROGRAM,PROGDES";
                     +" from "+i_cTable+" "+i_lTable+" where PROGRAM="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PROGRAM',oSource.xKey(1))
            select PROGRAM,PROGDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROGRAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PROGRAM,PROGDES";
                   +" from "+i_cTable+" "+i_lTable+" where PROGRAM="+cp_ToStrODBC(this.w_PROGRAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PROGRAM',this.w_PROGRAM)
            select PROGRAM,PROGDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROGRAM = NVL(_Link_.PROGRAM,space(10))
      this.w_DESCPROG = NVL(_Link_.PROGDES,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PROGRAM = space(10)
      endif
      this.w_DESCPROG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TSMENUVO_IDX,2])+'\'+cp_ToStr(_Link_.PROGRAM,1)
      cp_ShowWarn(i_cKey,this.TSMENUVO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROGRAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_1.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_1.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_2.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_2.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGRAM_1_3.value==this.w_PROGRAM)
      this.oPgFrm.Page1.oPag.oPROGRAM_1_3.value=this.w_PROGRAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCPROG_1_9.value==this.w_DESCPROG)
      this.oPgFrm.Page1.oPag.oDESCPROG_1_9.value=this.w_DESCPROG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEDESU_1_13.value==this.w_CODEDESU)
      this.oPgFrm.Page1.oPag.oCODEDESU_1_13.value=this.w_CODEDESU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEDESC_1_14.value==this.w_CODEDESC)
      this.oPgFrm.Page1.oPag.oCODEDESC_1_14.value=this.w_CODEDESC
    endif
    if not(this.oPgFrm.Page1.oPag.oMODULO_1_16.RadioValue()==this.w_MODULO)
      this.oPgFrm.Page1.oPag.oMODULO_1_16.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_sgaPag1 as StdContainer
  Width  = 463
  height = 177
  stdWidth  = 463
  stdheight = 177
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODUTE_1_1 as StdField with uid="DCUMQVCKNT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 114274266,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=83, Top=15, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCODUTE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oCODGRU_1_2 as StdField with uid="GFGCEOZCDI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice del gruppo.",;
    HelpContextID = 117288922,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=83, Top=43, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oCODGRU_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oPROGRAM_1_3 as StdField with uid="GOBDJKCQKR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PROGRAM", cQueryName = "PROGRAM",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Nome del programma.",;
    HelpContextID = 184351754,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=83, Top=101, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TSMENUVO", oKey_1_1="PROGRAM", oKey_1_2="this.w_PROGRAM"

  func oPROGRAM_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROGRAM_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROGRAM_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TSMENUVO','*','PROGRAM',cp_AbsName(this.parent,'oPROGRAM_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco programmi",'',this.parent.oContained
  endproc


  add object oBtn_1_4 as StdButton with uid="ACLZZHBAEB",left=358, top=128, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare la stampa";
    , HelpContextID = 201165290;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="TLAGCYFKXM",left=410, top=128, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 201165290;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCPROG_1_9 as StdField with uid="MRUJCUDHPV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCPROG", cQueryName = "DESCPROG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione programma.",;
    HelpContextID = 169920899,;
   bGlobalFont=.t.,;
    Height=21, Width=276, Left=181, Top=101, InputMask=replicate('X',30)

  add object oCODEDESU_1_13 as StdField with uid="GHVHHYURKG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODEDESU", cQueryName = "CODEDESU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 132099973,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=141, Top=15, InputMask=replicate('X',40)

  add object oCODEDESC_1_14 as StdField with uid="GJBTCBKGWH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODEDESC", cQueryName = "CODEDESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del gruppo.",;
    HelpContextID = 132099991,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=141, Top=43, InputMask=replicate('X',40)


  add object oMODULO_1_16 as StdZTamTableCombo with uid="YGTRMCJOAH",rtseq=10,rtrep=.f.,left=83,top=72,width=178,height=21;
    , ToolTipText = "Seleziona un modulo.";
    , HelpContextID = 223326010;
    , cFormVar="w_MODULO",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='QUERY\ELENMODU.VQR',cKey='MOCODMOD',cValue='MOTRADES',cOrderBy='MOORDINA',xDefault=space(2);
  , bGlobalFont=.t.


  add object oStr_1_6 as StdString with uid="NFUGRPTEDF",Visible=.t., Left=4, Top=18,;
    Alignment=1, Width=77, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RUTNWFMEPL",Visible=.t., Left=4, Top=47,;
    Alignment=1, Width=77, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="MXUJBZBATF",Visible=.t., Left=4, Top=105,;
    Alignment=1, Width=77, Height=18,;
    Caption="Programma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="WZTMYXGKQC",Visible=.t., Left=4, Top=75,;
    Alignment=1, Width=77, Height=18,;
    Caption="Moduli:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_sga','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_sga
* --- Classe per gestire la combo dei moduli attivati
* --- derivata dalla classe combo da tabella

define class StdZTamTableCombo as StdTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF

endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif

enddefine
* --- Fine Area Manuale
