* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_btl                                                        *
*              Calcolo record tabella treeview lotti                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-31                                                      *
* Last revis.: 2008-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pLIVEL,pTIPRIC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_btl",oParentObject,m.pLIVEL,m.pTIPRIC)
return(i_retval)

define class tgsma_btl as StdBatch
  * --- Local variables
  pLIVEL = 0
  pTIPRIC = space(1)
  w_FLSTAT = space(1)
  w_LOTTO = space(20)
  w_SERDOC = space(10)
  w_KEYLOT = space(61)
  w_DATREG = ctod("  /  /  ")
  w_TIPCAS = space(1)
  w_VARIANTE = space(20)
  w_SERDIC = space(20)
  w_OK = .f.
  * --- WorkFile variables
  TMPVEND1_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue ricerca prodotto finito in cascata sulle casistiche previste dato
    *     LOTTO e ARTICOLO del componente\semilavorato
    *     ed inserisce record trovati nella tabella temporanea utilizzata per
    *     costruire treeview
    * --- Livello iterazione
    * --- Tipo di ricerca 
    *     P per prodotto finito   
    *     C per componente
    * --- Casistiche gestite
    if g_APPLICATION="ADHOC REVOLUTION"
      * --- Dichiarazione di produzione  (articolo e lotto sono presenti in documenti interni di scarico collegati ad una dichiarazione di produzione dalla quale recuperiamo il prodotto finito) .
      if g_PROD="S" 
        * --- Inserisco record nodo figlio trovato
        if this.pTIPRIC="P"
          * --- Insert into TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA_QTR",this.TMPVEND1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA_QTR_1",this.TMPVEND1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        this.w_OK = i_Rows <>0
      endif
      * --- Esplosione su documenti ricavo prodotto finitto attraverso il legame di riga campo MVSEREST del documento di esplosione 
      if g_DISB="S"
        * --- Inserisco record nodo figlio trovato
        if this.pTIPRIC="P"
          * --- Insert into TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMAEQTR",this.TMPVEND1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMAEQTR_1",this.TMPVEND1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if Not this.w_OK
          this.w_OK = i_Rows <>0
        endif
      endif
      * --- Documento di rientro da conto lavoro (filtro sul DOC_DETT in cui DOC_MAST ha  MVSERDDT,MVROWDDT pieno) 
      if g_COLA="S" 
        if this.pTIPRIC="P"
          * --- Insert into TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMAOQTR",this.TMPVEND1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMAOQTR_1",this.TMPVEND1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if Not this.w_OK
          this.w_OK = i_Rows <>0
        endif
      endif
    else
      * --- Inserisco record nodo figlio trovato
      if g_COLA="S" 
        if this.pTIPRIC="P"
          * --- Insert into TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA_QTR_E",this.TMPVEND1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA_QTR_E_1",this.TMPVEND1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        this.w_OK = i_Rows <>0
        if this.pTIPRIC="P"
          * --- Insert into TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA1QTR_E",this.TMPVEND1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA1QTR_E_1",this.TMPVEND1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if Not this.w_OK
          this.w_OK = i_Rows <>0
        endif
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_OK
    return
  endproc


  proc Init(oParentObject,pLIVEL,pTIPRIC)
    this.pLIVEL=pLIVEL
    this.pTIPRIC=pTIPRIC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TMPVEND1'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pLIVEL,pTIPRIC"
endproc
