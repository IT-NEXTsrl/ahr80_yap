* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bem                                                        *
*              Estrazione e-mail da Microsoft Outlook                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-14                                                      *
* Last revis.: 2017-05-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pIndex
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bem",oParentObject,m.pIndex)
return(i_retval)

define class tgsut_bem as StdBatch
  * --- Local variables
  pIndex = space(15)
  w_PropToAdd = space(254)
  w_oOutlook = .NULL.
  w_oNameSpace = .NULL.
  w_oFolderSent = .NULL.
  w_oItems = .NULL.
  w_oMessage = .NULL.
  w_IdxMsg = 0
  w_Table = space(30)
  w_TableKey = space(50)
  w_CursRead = space(10)
  w_PrimaryKeyStr = space(254)
  w_IdxKey = 0
  w_SavePath = space(254)
  w_LICLADOC = space(100)
  w_MODALLEG = space(1)
  w_TIPOARCH = space(1)
  w_CLAALLE = space(5)
  w_TIPPATH = space(1)
  w_TIPCLA = space(1)
  w_IndexMail = space(15)
  w_ReadFromKey = .f.
  w_SaveMail = .f.
  w_OutlookVer = 0
  w_CurViewXML = space(0)
  w_IdxAttrib = 0
  w_CurrAttrib = space(254)
  w_FLATTADDED = .f.
  w_DateSent = ctod("  /  /  ")
  w_OraSent = space(8)
  w_MSubject = space(220)
  w_OBJMESS = .NULL.
  w_PosLastColumn = 0
  w_StrComposer = space(0)
  * --- WorkFile variables
  PROMCLAS_idx=0
  PROMINDI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_IndexMail = IIF(VARTYPE(this.pIndex)="C", ALLTRIM(this.pIndex), "")
    if VARTYPE(this.oParentObject.w_MSG)="C"
      this.oParentObject.w_MSG = ""
      this.w_OBJMESS = this
    else
      this.w_OBJMESS = this.oParentObject.w_MskMESS
    endif
    addmsgnl("Inizio elaborazione %1 %2", this.w_OBJMESS, DTOC(DATE()), LEFT(TTOC(DATETIME(),2),8))
    addmsgnl("Inizializzazione oggetto Microsoft Outlook", this.w_OBJMESS)
    * --- Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
    DecoratorState ("D")
    private L_bErr, L_OLDERR
    L_OLDERR = ON("ERROR")
    ON ERROR L_bErr=.T.
    L_bErr = .F.
    this.w_oOutlook = CreateObject("Outlook.Application")
    if L_bERR or VARTYPE(this.w_oOutlook)<>"O"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_OutlookVer = INT(VAL(LEFT(ALLTRIM(this.w_oOUTLOOK.Version),2)))
    addmsgnl("Inizializzazione oggetto area di lavoro MAPI", this.w_OBJMESS)
    this.w_oNameSpace = this.w_oOutlook.GetNamespace("MAPI")
    if L_bErr OR VARTYPE(this.w_oNameSpace)<>"O"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Riferimento a cartella Posta Inviata
    addmsgnl("Inizializzazione riferimento a cartella 'Posta inviata'", this.w_OBJMESS)
    this.w_oFolderSent = this.w_oNameSpace.GetDefaultfolder(5)
    if L_bErr OR VARTYPE(this.w_oFolderSent)<>"O"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- ATTENZIONE!!! Gli attributi di Outlook sono Case sensitive
    addmsgnl("Verifica presenza attributi cartella:", this.w_OBJMESS)
    DIMENSION L_AttList(5)
    L_AttList(1) = "adhocTable"
    L_AttList(2) = "adhocTableKey"
    L_AttList(3) = "adhocProcessed"
    L_AttList(4) = "adhocIndexID"
    L_AttList(5) = "adhocCompany"
    this.w_FLATTADDED = .F.
    if this.w_OutlookVer<=11
      this.w_CurViewXML = this.w_oFolderSent.CurrentView.XML
    endif
    this.w_IdxAttrib = 1
    do while this.w_IdxAttrib <= ALEN(L_AttList,1) AND !L_bErr
      this.w_CurrAttrib = ALLTRIM( L_AttList(this.w_IdxAttrib) )
      addmsg("%1%2...", this.w_OBJMESS, CHR(9), this.w_CurrAttrib )
      if this.w_OutlookVer>11
        if VARTYPE(this.w_oFolderSent.UserDefinedProperties.Find(this.w_CurrAttrib))="O"
          addmsgnl("Ok", this.w_OBJMESS)
        else
          this.w_FLATTADDED = .T.
          addmsgnl("Da creare", this.w_OBJMESS)
          addmsgnl("%1%1Creazione attributo %2", this.w_OBJMESS, CHR(9), this.w_CurrAttrib )
          this.w_oFolderSent.UserDefinedProperties.Add(this.w_CurrAttrib,1)     
          if L_bErr
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      else
        if AT("<heading>"+this.w_CurrAttrib+"</heading>", this.w_CurViewXML)>0
          addmsgnl("Ok", this.w_OBJMESS)
        else
          this.w_FLATTADDED = .T.
          addmsgnl("Da creare", this.w_OBJMESS)
          addmsgnl("%1%1Creazione attributo %2", this.w_OBJMESS, CHR(9), this.w_CurrAttrib )
          this.w_PropToAdd = this.w_CurrAttrib
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if L_bErr
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
      this.w_IdxAttrib = this.w_IdxAttrib + 1
    enddo
    if this.w_OutlookVer<=11 and this.w_FLATTADDED
      this.w_oFolderSent.CurrentView.XML = this.w_CurViewXML
      this.w_oFolderSent.CurrentView.Save()     
    endif
    release L_AttList
    * --- Recupero i soli elementi inviati da adhoc e che non sono stati processati
    addmsgnl("Selezione messaggi da esportare", this.w_OBJMESS)
    if EMPTY(this.w_IndexMail)
      this.w_oItems = this.w_oFolderSent.Items.Restrict("@SQL=" + Chr(34) + "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/adhocTable" + Chr(34) + " is not null AND "+CHR(34)+ "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/adhocProcessed"+CHR(34)+"='N' AND "+CHR(34)+ "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/adhocCompany"+CHR(34)+"='"+ALLTRIM(i_CodAzi)+"'")
    else
      this.w_oItems = this.w_oFolderSent.Items.Restrict("@SQL=" + Chr(34) + "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/adhocTable" + Chr(34) + " is not null AND "+CHR(34)+ "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/adhocProcessed"+CHR(34)+"='N' AND "+CHR(34)+ "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/adhocCompany"+CHR(34)+"='"+ALLTRIM(i_CodAzi)+"' AND "+CHR(34)+"http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/adhocIndexID"+CHR(34)+"='"+this.w_IndexMail+"'")
    endif
    if L_bErr
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_oItems.Count=0
      addmsgnl("Nessun messaggio da esportare", this.w_OBJMESS)
    else
      this.w_IdxMsg = 1
      do while this.w_IdxMsg<=this.w_oItems.Count
        * --- Selezione messaggio
        addmsgnl("Apertura messaggio %1 di %2", this.w_OBJMESS, ALLTRIM(STR(this.w_IdxMsg)), ALLTRIM(STR(this.w_oItems.Count)))
        this.w_oMessage = this.w_oItems.Item(this.w_IdxMsg)
        * --- Lettura propriet� messaggio
        this.w_Table = this.w_oMessage.UserProperties("adhocTable").Value
        this.w_TableKey = this.w_oMessage.UserProperties("adhocTableKey").Value
        this.w_IndexMail = this.w_oMessage.UserProperties("adhocIndexID").Value
        this.w_DateSent = TTOD( this.w_oMessage.SentOn )
        this.w_OraSent = TTOC( this.w_oMessage.SentOn, 2 )
        this.w_MSubject = LEFT(ALLTRIM(this.w_oMessage.Subject), 220)
        addmsgnl("%1Oggetto: %2", this.w_OBJMESS, CHR(9), ALLTRIM(this.w_oMessage.Subject))
        addmsgnl("%1Tabella collegata: %2", this.w_OBJMESS, CHR(9), ALLTRIM(this.w_Table))
        addmsgnl("%1Chiave tabella collegata: %2", this.w_OBJMESS, CHR(9), ALLTRIM(this.w_TableKey))
        addmsgnl("%1Chiave indice: %2", this.w_OBJMESS, CHR(9), ALLTRIM(this.w_IndexMail))
        * --- Lettura dati da database
        * --- Attualmente gestito il solo caso della mail inviata conindice gi� creato
        this.w_ReadFromKey = .F.
        addmsgnl("Lettura informazioni database", this.w_OBJMESS)
        if this.w_ReadFromKey
          this.w_SaveMail = .F.
          this.w_PrimaryKeyStr = ALLTRIM(i_dcx.getidxdef(lower(alltrim(this.w_table)),1))
          * --- Dimensiono l'array delle chiavi per la read table
          DIMENSION L_ArrValues( OCCURS(",", this.w_PrimaryKeyStr )+1 ,2 )
          * --- Preparo gli arry con i nomi ed i valori delle chiavi
          ALINES( L_FIELDS, this.w_PrimaryKeyStr , 5 , "," )
          ALINES( L_KEYVALUES, this.w_TableKey , 5 , "\" )
          this.w_IdxKey = 1
          do while this.w_IdxKey <= ALEN( L_KEYVALUES , 1)
            L_ArrValues( this.w_IdxKey ,1 ) = L_FIELDS(this.w_IdxKey)
            L_ArrValues( this.w_IdxKey ,2 ) = L_KEYVALUES(this.w_IdxKey)
            this.w_IdxKey = this.w_IdxKey + 1
          enddo
          release L_ArrValues, L_Fileds, L_KEYVALUES
          this.w_CursRead = ""
          this.w_CursRead = ReadTable(ALLTRIM(this.w_Table), "*", @L_ArrValues, this.oParentObject, .F.)
          if !EMPTY(this.w_CursRead)
            * --- Inserire lettura campi necessari da cursore
            this.w_SaveMail = .T.
            USE IN SELECT(this.w_CursRead)
          endif
        else
          * --- Read from PROMINDI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IDORIFIL"+;
              " from "+i_cTable+" PROMINDI where ";
                  +"IDSERIAL = "+cp_ToStrODBC(this.w_IndexMail);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IDORIFIL;
              from (i_cTable) where;
                  IDSERIAL = this.w_IndexMail;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SavePath = NVL(cp_ToDate(_read_.IDORIFIL),cp_NullValue(_read_.IDORIFIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_SaveMail = .T.
        endif
        if this.w_SaveMail
          this.w_SavePath = FULLPATH(ALLTRIM(this.w_SavePath))
          * --- Salvataggio messaggio e creazione indice
          addmsgnl("Verifica percorso salvataggio %1", this.w_OBJMESS, ALLTRIM(JUSTPATH(this.w_SavePath)) )
          if !DIRECTORY( ALLTRIM(JUSTPATH(this.w_SavePath)) )
            md ( ALLTRIM(JUSTPATH(this.w_SavePath)) )
          endif
          if L_bErr
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          addmsgnl("Esportazione messaggio di posta", this.w_OBJMESS)
          private L_Garbage
          if this.w_OutlookVer<11
            L_Garbage = this.w_oMessage.SaveAs(this.w_SavePath )
          else
            L_Garbage = this.w_oMessage.SaveAs(this.w_SavePath, 9 )
          endif
          if L_bErr
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Write into PROMINDI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PROMINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IDFLHIDE ="+cp_NullLink(cp_ToStrODBC("N"),'PROMINDI','IDFLHIDE');
            +",IDDATCRE ="+cp_NullLink(cp_ToStrODBC(this.w_DateSent),'PROMINDI','IDDATCRE');
            +",IDORACRE ="+cp_NullLink(cp_ToStrODBC(this.w_OraSent),'PROMINDI','IDORACRE');
                +i_ccchkf ;
            +" where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_IndexMail);
                   )
          else
            update (i_cTable) set;
                IDFLHIDE = "N";
                ,IDDATCRE = this.w_DateSent;
                ,IDORACRE = this.w_OraSent;
                &i_ccchkf. ;
             where;
                IDSERIAL = this.w_IndexMail;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          release L_Garbage
          * --- Memorizzazione flag processato nel messaggio
          addmsgnl("Salvataggio messaggio come gi� processato", this.w_OBJMESS)
          this.w_oMessage.UserProperties("adhocProcessed").Value = "S"
          this.w_oMessage.Save()     
          if this.w_OutlookVer<=11
            * --- Per Outlook 2003 occorre salvare due volte il messaggio e per farlo occorre sempre variarlo
            this.w_oMessage.UserProperties("adhocProcessed").Value = "S"
            this.w_oMessage.Save()     
          endif
        else
          addmsgnl("Nessuna corrispondenza trovata nel database. Impossibile salvare e-mail", this.w_OBJMESS)
        endif
        this.w_IdxMsg = this.w_IdxMsg + 1
      enddo
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Al termine del esportazion riattiva il decorator dei form 
    DecoratorState ("A")
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_oMessage = .NULL.
    this.w_oItems = .NULL.
    this.w_oFolderSent = .NULL.
    this.w_oOutlook = .NULL.
    this.w_oNameSpace = .NULL.
    if L_bErr
      addmsgnl("ATTENZIONE!!! Procedura terminata con errori", this.w_OBJMESS)
    endif
    addmsgnl("Fine elaborazione %1 %2", this.w_OBJMESS, DTOC(DATE()), LEFT(TTOC(DATETIME(),2),8))
    * --- Al termine del esportazion riattiva il decorator dei form 
    DecoratorState ("A")
    i_retcode = 'stop'
    i_retval = L_bErr
    return
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_StrComposer = ""
    this.w_PosLastColumn = RAT("</column>", this.w_CurViewXML )
    this.w_StrComposer = LEFT( this.w_CurViewXML, this.w_PosLastColumn - 1)
    this.w_StrComposer = this.w_StrComposer + "</column>"+CHR(13)+CHR(10)
    this.w_StrComposer = this.w_StrComposer + CHR(9)+"<column>"+CHR(13)+CHR(10)
    this.w_StrComposer = this.w_StrComposer + CHR(9) + CHR(9) + "<heading>"+ALLTRIM(this.w_PropToAdd)+"</heading>"+CHR(13)+CHR(10)
    this.w_StrComposer = this.w_StrComposer + CHR(9) + CHR(9) + "<prop>http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/"+ALLTRIM(this.w_PropToAdd)+"</prop>"+CHR(13)+CHR(10)
    this.w_StrComposer = this.w_StrComposer + CHR(9) + CHR(9) + "<type>string</type>"+CHR(13)+CHR(10)
    this.w_StrComposer = this.w_StrComposer + CHR(9) + CHR(9) + "<width>48</width>"+CHR(13)+CHR(10)
    this.w_StrComposer = this.w_StrComposer + CHR(9) + CHR(9) + "<style>text-align:left;padding-left:3px</style>"+CHR(13)+CHR(10)
    this.w_StrComposer = this.w_StrComposer + CHR(9) + SUBSTR( this.w_CurViewXML, this.w_PosLastColumn)
    this.w_CurViewXML = this.w_StrComposer
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pIndex)
    this.pIndex=pIndex
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='PROMINDI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pIndex"
endproc
