* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_blg                                                        *
*              Elabora stampa giornale                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_28]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2014-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_blg",oParentObject)
return(i_retval)

define class tgsma_blg as StdBatch
  * --- Local variables
  w_PAGINE = 0
  w_INDAZI = space(25)
  w_LOCAZI = space(35)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_Intervallo = 0
  w_PrimoGiorno = space(10)
  w_Campo = space(10)
  w_Messaggio = space(10)
  w_DataBloc = ctod("  /  /  ")
  w_lg_nrig = 0
  w_lg_data = ctod("  /  /  ")
  w_Conferma = space(1)
  w_TmpVar = 0
  * --- WorkFile variables
  ESERCIZI_idx=0
  GIORMAGA_idx=0
  MVM_MAST_idx=0
  DOC_MAST_idx=0
  AZIENDA_idx=0
  MAGAZZIN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa giornale di magazzino (da GSMA_SLG)
    * --- Variabili della maschera
    * --- Variabili per il report
    CODESE = this.oParentObject.w_CODESE
    DATINI = this.oParentObject.w_DATINI
    SELCON = this.oParentObject.w_SELCON
    DATFIN = this.oParentObject.w_DATFIN
    SELFOR = this.oParentObject.w_SELFOR
    ULTDAT = this.oParentObject.w_ULTDAT
    SELPER = this.oParentObject.w_SELPER
    ULTRIG = this.oParentObject.w_ULTRIG
    CODMAG = this.oParentObject.w_CODMAG
    DESMAG = this.oParentObject.w_DESMAG
    SELMOD = this.oParentObject.w_SELMOD
    * --- PER NUMERAZIONE PAGINE IN TESTATA
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI,AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_AZIENDA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI,AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_AZIENDA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
      this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
      this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
      this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
      this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_PAGINE=0
    L_PRPAGM=this.oParentObject.w_PRPAGM
    L_INTLGM=this.oParentObject.w_INTLGM
    L_PREFIS=this.oParentObject.w_PREFIS
    L_INDAZI=this.w_INDAZI
    L_LOCAZI=this.w_LOCAZI
    L_CAPAZI=this.w_CAPAZI
    L_PROAZI=this.w_PROAZI
    L_COFAZI=this.w_COFAZI
    L_PIVAZI=this.w_PIVAZI
    * --- Variabili locali
    this.w_Intervallo = 0
    this.w_PrimoGiorno = val(sys(11,this.oParentObject.w_DATINI))
    this.w_Campo = iif( this.oParentObject.w_SelCon = "B" , "periiniz, mmcodart" , "arcatomo, periiniz")
    this.w_Messaggio = ""
    this.w_DataBloc = cp_CharToDate("  -  -  ")
    this.w_lg_nrig = this.oParentObject.w_UltRig
    this.w_lg_data = this.oParentObject.w_DATFIN
    this.w_Conferma = " "
    this.w_TmpVar = 0
    * --- Controllo selezioni impostate sulla maschera
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Blocco movimenti di magazzino per stampa in definitiva
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Creazione cursore stampa
    ah_Msg("Elaborazione giornale di magazzino...")
    vq_exec("query\GSMA_SLG",this,"__tmp__")
    if reccount("__tmp__")=0
      if this.oParentObject.w_SelMod = "B"
        * --- Se stampa definitiva chiedo se si desidera aggiornare la data
        if ah_YesNo("Per la selezione effettuata non esistono dati da stampare%0Si desidera ugualmente aggiornare la data di ultima stampa?")
          * --- Lancio la maschera di aggiornamento
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        else
          * --- Chiudo tutto
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
      else
        * --- Stampa non definitiva
        ah_ErrorMsg("Per la selezione effettuata non esistono dati da stampare","!")
        * --- Chiudo tutto
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_CLASSE="S"
      * --- Verifico se Esistono Articoli che non hanno avvalorata la Classe ABC (nel caso check attivato)
      vq_exec("query\GSMA6SLG",this,"CursABC")
      if reccount("CursABC")=0
        ah_errormsg("Esistono articoli che non hanno la classe ABC di appartenenza specificata",48)
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      SELECT * FROM __TMP__ LEFT OUTER JOIN CursABC;
      ON PRCODART=MMCODART INTO Cursor TEMP WHERE EMPTY(NVL(PRCODART," "))
      if reccount("TEMP") > 0
        ah_errormsg("Esistono articoli che non hanno la classe ABC di appartenenza specificata",48)
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      else
        SELECT * FROM __TMP__ LEFT OUTER JOIN CursABC;
        ON PRCODART=MMCODART INTO Cursor __TMP__ WHERE PRCLAABC<>"C"
      endif
      if reccount("__tmp__")=0
        AH_ERRORMSG("Per la selezione effettuata non esistono dati da stampare",48)
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Creazione cursore con chiavi movimenti per aggiornamento
    if this.oParentObject.w_SelMod = "B"
      select mmserial from __tmp__ order by mmserial where tipomovi="M" into cursor keys_mvm
      select mmserial from __tmp__ order by mmserial where tipomovi="V" into cursor keys_doc
    endif
    * --- Ordinamento per Categorie Omogenee
    if this.oParentObject.w_SelCon = "C"
      select * from __tmp__ ;
      order by mmcodese, arcatomo, mmdatreg, cmseqcal, mmcaumag ;
      into cursor __tmp__
    endif
    * ---  ... o per data di registrazione
    if this.oParentObject.w_SelCon <> "C" and this.oParentObject.w_SELFOR <>"R"
      select * from __tmp__ ;
      order by mmcodese, mmdatreg, cmseqcal, mmcaumag ;
      into cursor __tmp__
    endif
    * --- Gestione riepiloghi ed ordinamenti
    if this.oParentObject.w_SELFOR = "R"
      * --- Calcola il numero di giorni che compongono il periodo selezionato
      do case
        case this.oParentObject.w_SELPER = "M"
          * --- Mese
          this.w_Intervallo = 30
        case this.oParentObject.w_SELPER = "Q"
          * --- Quindicina
          this.w_Intervallo = 15
        case this.oParentObject.w_SELPER = "D"
          * --- Decade
          this.w_Intervallo = 10
        case this.oParentObject.w_SELPER = "S"
          * --- Settimana
          this.w_Intervallo = 7
      endcase
      * --- Aggiunge i periodi di riferimento
      * --- Non � possibile fare un'unica select con la seguente perch� il fox entra in loop.
      select mmcodese, mmdatreg, mmcaumag, mmcodmag, mmcodice, mmcodart, ;
      ardesart, cadesart, arcatomo, omdescri, cmdescri, ;
      mmqtaum1, qtacar, qtasca, cmseqcal, ;
      iif( this.oParentObject.w_SelPer="M", (year(MMDATREG)*100)+month(MMDATREG) , ;
      iif( this.oParentObject.w_SelPer="S", (year(MMDATREG)*100)+week(MMDATREG) , ;
      int( ( val(sys(11,MMDATREG)) - this.w_PrimoGiorno ) / this.w_Intervallo ) )) as periodo, ;
      ELABPGM(cp_todate(mmdatreg), this.oParentObject.w_SelPer, this.oParentObject.w_DatIni, this.oParentObject.w_DatFin, "I") as PeriIniz, ;
      ELABPGM(cp_todate(mmdatreg), this.oParentObject.w_SelPer, this.oParentObject.w_DatIni, this.oParentObject.w_DatFin, "F") as PeriFine ;
      from __tmp__ ;
      into cursor __tmp__
      * --- Raggruppa i dati secondo il criterio selezionato ed ordina per articolo o categoria omogenea
      Campo = this.w_Campo
      select mmcodese, mmdatreg, mmcaumag, mmcodmag, mmcodice, mmcodart, ;
      ardesart, cadesart, arcatomo, omdescri, cmdescri, ;
      sum(mmqtaum1) as mmqtaum1, ;
      sum(qtacar) as qtacar, ;
      sum(qtasca) as qtasca, ;
      max(cmseqcal) as cmseqcal, ;
      periodo, PeriIniz, PeriFine ;
      from __tmp__ group by mmcodese, &Campo , mmcaumag ;
      order by mmcodese, &Campo , cmseqcal, mmcaumag ;
      into cursor __tmp__
    endif
    * --- Esecuzione report
    cp_chprn("QUERY\gsma_slg.FRX", " ", this)
    this.w_PAGINE = L_PAGINE+this.oParentObject.w_PRPAGM
    * --- Aggiornamento archivi per stampa su bollato
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Chiusura cursori
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo delle selezioni impostate sulla maschera
    * --- Magazzino o gruppo di magazzini
    if empty( this.oParentObject.w_CODMAG )
      ah_ErrorMsg("Attenzione: selezionare il magazzino di riferimento")
      i_retcode = 'stop'
      return
    endif
    * --- Data iniziale (Consecutiva all'ultima stampa od uguale all'inizio dell'esercizio)
    if this.oParentObject.w_SELMOD<>"R"
      if ((this.oParentObject.w_DatIni<>this.oParentObject.w_UltDat+1 .and. (.not. empty(this.oParentObject.w_UltDat))) .or. (this.oParentObject.w_DatIni<>this.oParentObject.w_InizEser .and. empty(this.oParentObject.w_UltDat)))
        if this.oParentObject.w_SELMOD="B"
          ah_ErrorMsg("Data iniziale non consecutiva all'ultima stampa")
          i_retcode = 'stop'
          return
        else
          this.w_Messaggio = "Data iniziale non consecutiva all'ultima stampa%0Potrebbero essere ignorate alcune registrazioni%0%0Confermi ugualmente?"
          if NOT ah_YesNo(this.w_Messaggio)
            i_retcode = 'stop'
            return
          endif
        endif
      endif
    endif
    * --- Intervallo di date
    if (this.oParentObject.w_DatIni<this.oParentObject.w_InizEser .or. this.oParentObject.w_DatFin>this.oParentObject.w_FineEser)
      this.w_Messaggio = "L'intervallo di date non � contenuto nell'esercizio attuale%0Selezionare l'esercizio di competenza corretto per effettuare la stampa con le selezioni impostate"
      ah_ErrorMsg(this.w_Messaggio)
      i_retcode = 'stop'
      return
    endif
    * --- Controllo per progressivo numerazione pagine per anno solare
    if YEAR(this.oParentObject.w_DATINI)<>YEAR(this.oParentObject.w_DATFIN) AND this.oParentObject.w_INTLGM="S"
      this.w_MESSAGGIO = "La data di fine selezione � al di fuori dell'anno solare della data di inizio selezione%0Come fine selezione impostare una data all'interno dello stesso anno solare"
      ah_ErrorMsg(this.w_MESSAGGIO)
      i_retcode = 'stop'
      return
    endif
    * --- Se ristampa la data finale non deve superare la data di stampa definitiva
    if this.oParentObject.w_SELMOD="R" .and. this.oParentObject.w_DATFIN>this.oParentObject.w_ULTDAT
      this.w_Messaggio = "La data finale supera l'ultima data di stampa definitiva"
      ah_ErrorMsg(this.w_Messaggio)
      i_retcode = 'stop'
      return
    endif
    * --- La data finale non deve inferiore alla data finale
    if this.oParentObject.w_DATINI>this.oParentObject.w_DATFIN
      this.w_Messaggio = "La data iniziale � maggiore di quella finale"
      ah_ErrorMsg(this.w_Messaggio)
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Blocco movimenti di magazzino
    if this.oParentObject.w_SELMOD="B"
      * --- Blocco movimenti di magazzino
      * --- Try
      local bErr_02DAE568
      bErr_02DAE568=bTrsErr
      this.Try_02DAE568()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Non � possibile stampare il giornale sui bollati in questo momento")
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_02DAE568
      * --- End
    endif
  endproc
  proc Try_02DAE568()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from GIORMAGA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.GIORMAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GIORMAGA_idx,2],.t.,this.GIORMAGA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "GMDATBLO"+;
        " from "+i_cTable+" GIORMAGA where ";
            +"GMCODAZI = "+cp_ToStrODBC(this.oParentObject.w_AZIENDA);
            +" and GMCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
            +" and GMCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        GMDATBLO;
        from (i_cTable) where;
            GMCODAZI = this.oParentObject.w_AZIENDA;
            and GMCODESE = this.oParentObject.w_CODESE;
            and GMCODMAG = this.oParentObject.w_CODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATABLOC = NVL(cp_ToDate(_read_.GMDATBLO),cp_NullValue(_read_.GMDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(CP_TODATE(this.w_DataBloc))
      * --- Scrittura data blocco movimenti
      * --- Try
      local bErr_02DAD908
      bErr_02DAD908=bTrsErr
      this.Try_02DAD908()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_02DAD908
      * --- End
      * --- Write into GIORMAGA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.GIORMAGA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GIORMAGA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.GIORMAGA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GMDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'GIORMAGA','GMDATBLO');
            +i_ccchkf ;
        +" where ";
            +"GMCODAZI = "+cp_ToStrODBC(this.oParentObject.w_AZIENDA);
            +" and GMCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
            +" and GMCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
               )
      else
        update (i_cTable) set;
            GMDATBLO = this.oParentObject.w_DATFIN;
            &i_ccchkf. ;
         where;
            GMCODAZI = this.oParentObject.w_AZIENDA;
            and GMCODESE = this.oParentObject.w_CODESE;
            and GMCODMAG = this.oParentObject.w_CODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- UN ALTRO UTENTE STA STAMPANDO IL GIORNALE - controllo concorrenza
      ah_ErrorMsg("Stampa giornale in corso da parte di un altro utente")
      * --- Raise
      i_Error="Blocco giornale di magazzino"
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_02DAD908()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into GIORMAGA
    i_nConn=i_TableProp[this.GIORMAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GIORMAGA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GIORMAGA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GMCODAZI"+",GMCODESE"+",GMCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AZIENDA),'GIORMAGA','GMCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'GIORMAGA','GMCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'GIORMAGA','GMCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GMCODAZI',this.oParentObject.w_AZIENDA,'GMCODESE',this.oParentObject.w_CODESE,'GMCODMAG',this.oParentObject.w_CODMAG)
      insert into (i_cTable) (GMCODAZI,GMCODESE,GMCODMAG &i_ccchkf. );
         values (;
           this.oParentObject.w_AZIENDA;
           ,this.oParentObject.w_CODESE;
           ,this.oParentObject.w_CODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento archivi per stampa su bollato
    if this.oParentObject.w_SelMod="B"
      * --- Calcolo progressivo ultima riga stampata
      this.w_lg_nrig = this.w_lg_nrig + reccount("__tmp__")
      * --- Richiesta di conferma per l'aggiornamento
      this.w_Conferma = "N"
      do GSMA_SL2 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Aggiornamento Files
      if this.w_conferma = "S"
        ah_Msg("Aggiornamento progressivi giornale magazzino...")
        * --- Try
        local bErr_02DAAC08
        bErr_02DAAC08=bTrsErr
        this.Try_02DAAC08()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- Eliminazione blocco
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Messaggio errore aggiornamento archivi
          ah_ErrorMsg("Impossibile marcare come definitive le registrazioni stampate")
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_02DAAC08
        * --- End
      endif
      * --- Eliminazione blocco
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_02DAAC08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Scrittura ultima data ed ultimo numero di riga
    * --- Write into GIORMAGA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.GIORMAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GIORMAGA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.GIORMAGA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GMDATGIO ="+cp_NullLink(cp_ToStrODBC(this.w_lg_data),'GIORMAGA','GMDATGIO');
      +",GMRIGGIO ="+cp_NullLink(cp_ToStrODBC(this.w_lg_nrig),'GIORMAGA','GMRIGGIO');
          +i_ccchkf ;
      +" where ";
          +"GMCODAZI = "+cp_ToStrODBC(this.oParentObject.w_AZIENDA);
          +" and GMCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
          +" and GMCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
             )
    else
      update (i_cTable) set;
          GMDATGIO = this.w_lg_data;
          ,GMRIGGIO = this.w_lg_nrig;
          &i_ccchkf. ;
       where;
          GMCODAZI = this.oParentObject.w_AZIENDA;
          and GMCODESE = this.oParentObject.w_CODESE;
          and GMCODMAG = this.oParentObject.w_CODMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorno il progressivo pagine solo se Attivo il check intestazione
    if this.oParentObject.w_INTLGM="S"
      * --- Aggiornamento progressivo pagine nei magazzini
      this.oParentObject.w_PRPAGM = this.w_PAGINE
      * --- Write into MAGAZZIN
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MAGAZZIN_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MGPRPAGM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPAGM),'MAGAZZIN','MGPRPAGM');
            +i_ccchkf ;
        +" where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
               )
      else
        update (i_cTable) set;
            MGPRPAGM = this.oParentObject.w_PRPAGM;
            &i_ccchkf. ;
         where;
            MGCODMAG = this.oParentObject.w_CODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Aggiornamento variabili maschera
    this.oParentObject.w_UltDat = this.w_lg_data
    this.oParentObject.w_UltRig = this.w_lg_nrig
    wait clear
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursori
    if used("__tmp__")
      select __tmp__
      use
    endif
    if used("keys_mvm")
      select keys_mvm
      use
    endif
    if used("keys_doc")
      select keys_doc
      use
    endif
    if used("CursABC")
      select CursABC
      use
    endif
    if used("TEMP")
      select TEMP
      use
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sblocco movimenti di magazzino
    * --- Try
    local bErr_02D94688
    bErr_02D94688=bTrsErr
    this.Try_02D94688()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile rimuovere blocco movimenti magazzino")
    endif
    bTrsErr=bTrsErr or bErr_02D94688
    * --- End
  endproc
  proc Try_02D94688()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_TmpVar = cp_CharToDate("  -  -  ")
    * --- Write into GIORMAGA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.GIORMAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GIORMAGA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.GIORMAGA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GMDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_TmpVar),'GIORMAGA','GMDATBLO');
          +i_ccchkf ;
      +" where ";
          +"GMCODAZI = "+cp_ToStrODBC(this.oParentObject.w_AZIENDA);
          +" and GMCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
          +" and GMCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
             )
    else
      update (i_cTable) set;
          GMDATBLO = this.w_TmpVar;
          &i_ccchkf. ;
       where;
          GMCODAZI = this.oParentObject.w_AZIENDA;
          and GMCODESE = this.oParentObject.w_CODESE;
          and GMCODMAG = this.oParentObject.w_CODMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='GIORMAGA'
    this.cWorkTables[3]='MVM_MAST'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='MAGAZZIN'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
