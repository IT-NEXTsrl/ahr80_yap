* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_amd                                                        *
*              Metodo di calcolo periodicit�                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_38]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-08                                                      *
* Last revis.: 2012-04-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_amd"))

* --- Class definition
define class tgsar_amd as StdForm
  Top    = 6
  Left   = 10

  * --- Standard Properties
  Width  = 525
  Height = 318+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-19"
  HelpContextID=210335593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Constant Properties
  MODCLDAT_IDX = 0
  TAB_CALE_IDX = 0
  cFile = "MODCLDAT"
  cKeySelect = "MDCODICE"
  cKeyWhere  = "MDCODICE=this.w_MDCODICE"
  cKeyWhereODBC = '"MDCODICE="+cp_ToStrODBC(this.w_MDCODICE)';

  cKeyWhereODBCqualified = '"MODCLDAT.MDCODICE="+cp_ToStrODBC(this.w_MDCODICE)';

  cPrg = "gsar_amd"
  cComment = "Metodo di calcolo periodicit�"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MDCODICE = space(3)
  w_MDDESCRI = space(50)
  w_MD__FREQ = space(1)
  w_MDDATUNI = ctod('  /  /  ')
  w_MDORAINI = 0
  o_MDORAINI = 0
  w_MDORAFIN = 0
  o_MDORAFIN = 0
  w_MD__WEEK = 0
  o_MD__WEEK = 0
  w_MD_MONTH = 0
  o_MD_MONTH = 0
  w_ORAINI = space(2)
  w_MININI = space(2)
  w_ORAFIN = space(2)
  w_MDNUMFRQ = 0
  w_MINFIN = space(2)
  w_MDMININT = 0
  w_SHOWWEEK = space(1)
  w_SHOWMONT = space(1)
  w_Lun = 0
  w_Mar = 0
  w_Mer = 0
  w_Gio = 0
  w_Ven = 0
  w_Sab = 0
  w_Dom = 0
  w_MDIMPDAT = space(1)
  w_Gen = 0
  w_MD_GGFIS = 0
  w_Feb = 0
  w_Marz = 0
  w_Apr = 0
  w_Mag = 0
  w_Giu = 0
  w_Lug = 0
  w_Ago = 0
  w_Set = 0
  w_Ott = 0
  w_Nov = 0
  w_Dic = 0
  w_MDESCLUS = space(1)
  o_MDESCLUS = space(1)
  w_MDESCLUS = space(1)
  w_MDCALEND = space(5)
  w_TCDESCRI = space(40)
  w_TCFLDEFA = space(1)
  w_TCCODICE = space(5)
  w_LBL = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MODCLDAT','gsar_amd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_amdPag1","gsar_amd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Calcolo")
      .Pages(1).HelpContextID = 114339366
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMDCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_LBL = this.oPgFrm.Pages(1).oPag.LBL
      DoDefault()
    proc Destroy()
      this.w_LBL = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TAB_CALE'
    this.cWorkTables[2]='MODCLDAT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MODCLDAT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MODCLDAT_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MDCODICE = NVL(MDCODICE,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_59_joined
    link_1_59_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MODCLDAT where MDCODICE=KeySet.MDCODICE
    *
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MODCLDAT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MODCLDAT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MODCLDAT '
      link_1_59_joined=this.AddJoinedLink_1_59(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MDCODICE',this.w_MDCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TCDESCRI = space(40)
        .w_TCFLDEFA = "S"
        .w_TCCODICE = space(5)
        .w_MDCODICE = NVL(MDCODICE,space(3))
        .w_MDDESCRI = NVL(MDDESCRI,space(50))
        .w_MD__FREQ = NVL(MD__FREQ,space(1))
        .w_MDDATUNI = NVL(cp_ToDate(MDDATUNI),ctod("  /  /  "))
        .w_MDORAINI = NVL(MDORAINI,0)
        .w_MDORAFIN = NVL(MDORAFIN,0)
        .w_MD__WEEK = NVL(MD__WEEK,0)
        .w_MD_MONTH = NVL(MD_MONTH,0)
        .w_ORAINI = RIGHT("00"+ALLTRIM(STR(INT(.w_MDORAINI/60))), 2)
        .w_MININI = RIGHT("00"+ALLTRIM(STR(MOD(.w_MDORAINI,60))), 2)
        .w_ORAFIN = iif(RIGHT("00"+ALLTRIM(STR(INT(.w_MDORAFIN/60))), 2)='00','23',RIGHT("00"+ALLTRIM(STR(INT(.w_MDORAFIN/60))), 2))
        .w_MDNUMFRQ = NVL(MDNUMFRQ,0)
        .w_MINFIN = iif(RIGHT("00"+ALLTRIM(STR(MOD(.w_MDORAFIN,60))), 2)='00','59',RIGHT("00"+ALLTRIM(STR(MOD(.w_MDORAFIN,60))), 2))
        .w_MDMININT = NVL(MDMININT,0)
        .w_SHOWWEEK = IIF( .w_MD__FREQ $ "G-S-Q", "S", "N")
        .w_SHOWMONT = IIF( .w_MD__FREQ $ "M-A", "S", "N")
        .w_Lun = BITAND(.w_MD__WEEK, 0x01)
        .w_Mar = BITAND(.w_MD__WEEK, 0x02)
        .w_Mer = BITAND(.w_MD__WEEK, 0x04)
        .w_Gio = BITAND(.w_MD__WEEK, 0x08)
        .w_Ven = BITAND(.w_MD__WEEK, 0x10)
        .w_Sab = BITAND(.w_MD__WEEK, 0x20)
        .w_Dom = BITAND(.w_MD__WEEK, 0x40)
        .w_MDIMPDAT = NVL(MDIMPDAT,space(1))
        .w_Gen = BITAND(.w_MD_MONTH, 0x0001)
        .w_MD_GGFIS = NVL(MD_GGFIS,0)
        .w_Feb = BITAND(.w_MD_MONTH, 0x0002)
        .w_Marz = BITAND(.w_MD_MONTH, 0x0004)
        .w_Apr = BITAND(.w_MD_MONTH, 0x0008)
        .w_Mag = BITAND(.w_MD_MONTH, 0x0010)
        .w_Giu = BITAND(.w_MD_MONTH, 0x0020)
        .w_Lug = BITAND(.w_MD_MONTH, 0x0040)
        .w_Ago = BITAND(.w_MD_MONTH, 0x0080)
        .w_Set = BITAND(.w_MD_MONTH, 0x0100)
        .w_Ott = BITAND(.w_MD_MONTH, 0x0200)
        .w_Nov = BITAND(.w_MD_MONTH, 0x0400)
        .w_Dic = BITAND(.w_MD_MONTH, 0x0800)
        .oPgFrm.Page1.oPag.LBL.Calculate(ICASE(.w_MD__FREQ='G',ah_msgformat('Giorno\i'), .w_MD__FREQ='S', ah_msgformat('Settimana\e'), .w_MD__FREQ='M', ah_msgformat('Mese\i'), .w_MD__FREQ='A', ah_msgformat('Anno\i'), .w_MD__FREQ='Q', ah_msgformat('Quindicina\e'), ''))
        .w_MDESCLUS = NVL(MDESCLUS,space(1))
        .w_MDESCLUS = NVL(MDESCLUS,space(1))
        .w_MDCALEND = NVL(MDCALEND,space(5))
          if link_1_59_joined
            this.w_MDCALEND = NVL(TCCODICE159,NVL(this.w_MDCALEND,space(5)))
            this.w_TCDESCRI = NVL(TCDESCRI159,space(40))
          else
          .link_1_59('Load')
          endif
          .link_1_62('Load')
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        cp_LoadRecExtFlds(this,'MODCLDAT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MDCODICE = space(3)
      .w_MDDESCRI = space(50)
      .w_MD__FREQ = space(1)
      .w_MDDATUNI = ctod("  /  /  ")
      .w_MDORAINI = 0
      .w_MDORAFIN = 0
      .w_MD__WEEK = 0
      .w_MD_MONTH = 0
      .w_ORAINI = space(2)
      .w_MININI = space(2)
      .w_ORAFIN = space(2)
      .w_MDNUMFRQ = 0
      .w_MINFIN = space(2)
      .w_MDMININT = 0
      .w_SHOWWEEK = space(1)
      .w_SHOWMONT = space(1)
      .w_Lun = 0
      .w_Mar = 0
      .w_Mer = 0
      .w_Gio = 0
      .w_Ven = 0
      .w_Sab = 0
      .w_Dom = 0
      .w_MDIMPDAT = space(1)
      .w_Gen = 0
      .w_MD_GGFIS = 0
      .w_Feb = 0
      .w_Marz = 0
      .w_Apr = 0
      .w_Mag = 0
      .w_Giu = 0
      .w_Lug = 0
      .w_Ago = 0
      .w_Set = 0
      .w_Ott = 0
      .w_Nov = 0
      .w_Dic = 0
      .w_MDESCLUS = space(1)
      .w_MDESCLUS = space(1)
      .w_MDCALEND = space(5)
      .w_TCDESCRI = space(40)
      .w_TCFLDEFA = space(1)
      .w_TCCODICE = space(5)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_MD__FREQ = 'U'
          .DoRTCalc(4,4,.f.)
        .w_MDORAINI = VAL(.w_ORAINI) * 60 + VAL(.w_MININI)
        .w_MDORAFIN = VAL(.w_ORAFIN) * 60 + VAL(.w_MINFIN)
        .w_MD__WEEK = BITOR(.w_lun, .w_mar, .w_mer, .w_gio, .w_ven, .w_sab, .w_dom)
        .w_MD_MONTH = BITOR(.w_gen, .w_feb, .w_marz, .w_apr, .w_mag, .w_giu, .w_lug, .w_ago, .w_set, .w_ott, .w_nov, .w_dic)
        .w_ORAINI = RIGHT("00"+ALLTRIM(STR(INT(.w_MDORAINI/60))), 2)
        .w_MININI = RIGHT("00"+ALLTRIM(STR(MOD(.w_MDORAINI,60))), 2)
        .w_ORAFIN = iif(RIGHT("00"+ALLTRIM(STR(INT(.w_MDORAFIN/60))), 2)='00','23',RIGHT("00"+ALLTRIM(STR(INT(.w_MDORAFIN/60))), 2))
        .w_MDNUMFRQ = 1
        .w_MINFIN = iif(RIGHT("00"+ALLTRIM(STR(MOD(.w_MDORAFIN,60))), 2)='00','59',RIGHT("00"+ALLTRIM(STR(MOD(.w_MDORAFIN,60))), 2))
          .DoRTCalc(14,14,.f.)
        .w_SHOWWEEK = IIF( .w_MD__FREQ $ "G-S-Q", "S", "N")
        .w_SHOWMONT = IIF( .w_MD__FREQ $ "M-A", "S", "N")
        .w_Lun = BITAND(.w_MD__WEEK, 0x01)
        .w_Mar = BITAND(.w_MD__WEEK, 0x02)
        .w_Mer = BITAND(.w_MD__WEEK, 0x04)
        .w_Gio = BITAND(.w_MD__WEEK, 0x08)
        .w_Ven = BITAND(.w_MD__WEEK, 0x10)
        .w_Sab = BITAND(.w_MD__WEEK, 0x20)
        .w_Dom = BITAND(.w_MD__WEEK, 0x40)
        .w_MDIMPDAT = 'N'
        .w_Gen = BITAND(.w_MD_MONTH, 0x0001)
          .DoRTCalc(26,26,.f.)
        .w_Feb = BITAND(.w_MD_MONTH, 0x0002)
        .w_Marz = BITAND(.w_MD_MONTH, 0x0004)
        .w_Apr = BITAND(.w_MD_MONTH, 0x0008)
        .w_Mag = BITAND(.w_MD_MONTH, 0x0010)
        .w_Giu = BITAND(.w_MD_MONTH, 0x0020)
        .w_Lug = BITAND(.w_MD_MONTH, 0x0040)
        .w_Ago = BITAND(.w_MD_MONTH, 0x0080)
        .w_Set = BITAND(.w_MD_MONTH, 0x0100)
        .w_Ott = BITAND(.w_MD_MONTH, 0x0200)
        .w_Nov = BITAND(.w_MD_MONTH, 0x0400)
        .w_Dic = BITAND(.w_MD_MONTH, 0x0800)
        .oPgFrm.Page1.oPag.LBL.Calculate(ICASE(.w_MD__FREQ='G',ah_msgformat('Giorno\i'), .w_MD__FREQ='S', ah_msgformat('Settimana\e'), .w_MD__FREQ='M', ah_msgformat('Mese\i'), .w_MD__FREQ='A', ah_msgformat('Anno\i'), .w_MD__FREQ='Q', ah_msgformat('Quindicina\e'), ''))
        .w_MDESCLUS = 'N'
        .w_MDESCLUS = 'N'
        .w_MDCALEND = IIF( .w_MDESCLUS = "C", .w_TCCODICE, Space(5) )
        .DoRTCalc(40,40,.f.)
          if not(empty(.w_MDCALEND))
          .link_1_59('Full')
          endif
          .DoRTCalc(41,41,.f.)
        .w_TCFLDEFA = "S"
        .DoRTCalc(42,42,.f.)
          if not(empty(.w_TCFLDEFA))
          .link_1_62('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MODCLDAT')
    this.DoRTCalc(43,43,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMDCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oMDDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oMD__FREQ_1_4.enabled = i_bVal
      .Page1.oPag.oMDDATUNI_1_5.enabled = i_bVal
      .Page1.oPag.oORAINI_1_14.enabled = i_bVal
      .Page1.oPag.oMININI_1_15.enabled = i_bVal
      .Page1.oPag.oORAFIN_1_18.enabled = i_bVal
      .Page1.oPag.oMDNUMFRQ_1_19.enabled = i_bVal
      .Page1.oPag.oMINFIN_1_20.enabled = i_bVal
      .Page1.oPag.oMDMININT_1_21.enabled = i_bVal
      .Page1.oPag.oLun_1_28.enabled = i_bVal
      .Page1.oPag.oMar_1_29.enabled = i_bVal
      .Page1.oPag.oMer_1_30.enabled = i_bVal
      .Page1.oPag.oGio_1_31.enabled = i_bVal
      .Page1.oPag.oVen_1_32.enabled = i_bVal
      .Page1.oPag.oSab_1_33.enabled = i_bVal
      .Page1.oPag.oDom_1_34.enabled = i_bVal
      .Page1.oPag.oMDIMPDAT_1_35.enabled = i_bVal
      .Page1.oPag.oGen_1_36.enabled = i_bVal
      .Page1.oPag.oMD_GGFIS_1_37.enabled = i_bVal
      .Page1.oPag.oFeb_1_38.enabled = i_bVal
      .Page1.oPag.oMarz_1_39.enabled = i_bVal
      .Page1.oPag.oApr_1_40.enabled = i_bVal
      .Page1.oPag.oMag_1_41.enabled = i_bVal
      .Page1.oPag.oGiu_1_42.enabled = i_bVal
      .Page1.oPag.oLug_1_43.enabled = i_bVal
      .Page1.oPag.oAgo_1_44.enabled = i_bVal
      .Page1.oPag.oSet_1_45.enabled = i_bVal
      .Page1.oPag.oOtt_1_46.enabled = i_bVal
      .Page1.oPag.oNov_1_47.enabled = i_bVal
      .Page1.oPag.oDic_1_48.enabled = i_bVal
      .Page1.oPag.oMDESCLUS_1_55.enabled = i_bVal
      .Page1.oPag.oMDESCLUS_1_56.enabled = i_bVal
      .Page1.oPag.oMDCALEND_1_59.enabled = i_bVal
      .Page1.oPag.oBtn_1_51.enabled = .Page1.oPag.oBtn_1_51.mCond()
      .Page1.oPag.LBL.enabled = i_bVal
      .Page1.oPag.oObj_1_64.enabled = i_bVal
      .Page1.oPag.oObj_1_65.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMDCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMDCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MODCLDAT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODICE,"MDCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDDESCRI,"MDDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MD__FREQ,"MD__FREQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDDATUNI,"MDDATUNI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDORAINI,"MDORAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDORAFIN,"MDORAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MD__WEEK,"MD__WEEK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MD_MONTH,"MD_MONTH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDNUMFRQ,"MDNUMFRQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDMININT,"MDMININT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDIMPDAT,"MDIMPDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MD_GGFIS,"MD_GGFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDESCLUS,"MDESCLUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDESCLUS,"MDESCLUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCALEND,"MDCALEND",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    i_lTable = "MODCLDAT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MODCLDAT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MODCLDAT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MODCLDAT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MODCLDAT')
        i_extval=cp_InsertValODBCExtFlds(this,'MODCLDAT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MDCODICE,MDDESCRI,MD__FREQ,MDDATUNI,MDORAINI"+;
                  ",MDORAFIN,MD__WEEK,MD_MONTH,MDNUMFRQ,MDMININT"+;
                  ",MDIMPDAT,MD_GGFIS,MDESCLUS,MDCALEND "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MDCODICE)+;
                  ","+cp_ToStrODBC(this.w_MDDESCRI)+;
                  ","+cp_ToStrODBC(this.w_MD__FREQ)+;
                  ","+cp_ToStrODBC(this.w_MDDATUNI)+;
                  ","+cp_ToStrODBC(this.w_MDORAINI)+;
                  ","+cp_ToStrODBC(this.w_MDORAFIN)+;
                  ","+cp_ToStrODBC(this.w_MD__WEEK)+;
                  ","+cp_ToStrODBC(this.w_MD_MONTH)+;
                  ","+cp_ToStrODBC(this.w_MDNUMFRQ)+;
                  ","+cp_ToStrODBC(this.w_MDMININT)+;
                  ","+cp_ToStrODBC(this.w_MDIMPDAT)+;
                  ","+cp_ToStrODBC(this.w_MD_GGFIS)+;
                  ","+cp_ToStrODBC(this.w_MDESCLUS)+;
                  ","+cp_ToStrODBCNull(this.w_MDCALEND)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MODCLDAT')
        i_extval=cp_InsertValVFPExtFlds(this,'MODCLDAT')
        cp_CheckDeletedKey(i_cTable,0,'MDCODICE',this.w_MDCODICE)
        INSERT INTO (i_cTable);
              (MDCODICE,MDDESCRI,MD__FREQ,MDDATUNI,MDORAINI,MDORAFIN,MD__WEEK,MD_MONTH,MDNUMFRQ,MDMININT,MDIMPDAT,MD_GGFIS,MDESCLUS,MDCALEND  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MDCODICE;
                  ,this.w_MDDESCRI;
                  ,this.w_MD__FREQ;
                  ,this.w_MDDATUNI;
                  ,this.w_MDORAINI;
                  ,this.w_MDORAFIN;
                  ,this.w_MD__WEEK;
                  ,this.w_MD_MONTH;
                  ,this.w_MDNUMFRQ;
                  ,this.w_MDMININT;
                  ,this.w_MDIMPDAT;
                  ,this.w_MD_GGFIS;
                  ,this.w_MDESCLUS;
                  ,this.w_MDCALEND;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MODCLDAT_IDX,i_nConn)
      *
      * update MODCLDAT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MODCLDAT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MDDESCRI="+cp_ToStrODBC(this.w_MDDESCRI)+;
             ",MD__FREQ="+cp_ToStrODBC(this.w_MD__FREQ)+;
             ",MDDATUNI="+cp_ToStrODBC(this.w_MDDATUNI)+;
             ",MDORAINI="+cp_ToStrODBC(this.w_MDORAINI)+;
             ",MDORAFIN="+cp_ToStrODBC(this.w_MDORAFIN)+;
             ",MD__WEEK="+cp_ToStrODBC(this.w_MD__WEEK)+;
             ",MD_MONTH="+cp_ToStrODBC(this.w_MD_MONTH)+;
             ",MDNUMFRQ="+cp_ToStrODBC(this.w_MDNUMFRQ)+;
             ",MDMININT="+cp_ToStrODBC(this.w_MDMININT)+;
             ",MDIMPDAT="+cp_ToStrODBC(this.w_MDIMPDAT)+;
             ",MD_GGFIS="+cp_ToStrODBC(this.w_MD_GGFIS)+;
             ",MDESCLUS="+cp_ToStrODBC(this.w_MDESCLUS)+;
             ",MDCALEND="+cp_ToStrODBCNull(this.w_MDCALEND)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MODCLDAT')
        i_cWhere = cp_PKFox(i_cTable  ,'MDCODICE',this.w_MDCODICE  )
        UPDATE (i_cTable) SET;
              MDDESCRI=this.w_MDDESCRI;
             ,MD__FREQ=this.w_MD__FREQ;
             ,MDDATUNI=this.w_MDDATUNI;
             ,MDORAINI=this.w_MDORAINI;
             ,MDORAFIN=this.w_MDORAFIN;
             ,MD__WEEK=this.w_MD__WEEK;
             ,MD_MONTH=this.w_MD_MONTH;
             ,MDNUMFRQ=this.w_MDNUMFRQ;
             ,MDMININT=this.w_MDMININT;
             ,MDIMPDAT=this.w_MDIMPDAT;
             ,MD_GGFIS=this.w_MD_GGFIS;
             ,MDESCLUS=this.w_MDESCLUS;
             ,MDCALEND=this.w_MDCALEND;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MODCLDAT_IDX,i_nConn)
      *
      * delete MODCLDAT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MDCODICE',this.w_MDCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
            .w_MDORAINI = VAL(.w_ORAINI) * 60 + VAL(.w_MININI)
            .w_MDORAFIN = VAL(.w_ORAFIN) * 60 + VAL(.w_MINFIN)
            .w_MD__WEEK = BITOR(.w_lun, .w_mar, .w_mer, .w_gio, .w_ven, .w_sab, .w_dom)
            .w_MD_MONTH = BITOR(.w_gen, .w_feb, .w_marz, .w_apr, .w_mag, .w_giu, .w_lug, .w_ago, .w_set, .w_ott, .w_nov, .w_dic)
        if .o_MDORAINI<>.w_MDORAINI
            .w_ORAINI = RIGHT("00"+ALLTRIM(STR(INT(.w_MDORAINI/60))), 2)
        endif
        if .o_MDORAINI<>.w_MDORAINI
            .w_MININI = RIGHT("00"+ALLTRIM(STR(MOD(.w_MDORAINI,60))), 2)
        endif
        if .o_MDORAFIN<>.w_MDORAFIN
            .w_ORAFIN = iif(RIGHT("00"+ALLTRIM(STR(INT(.w_MDORAFIN/60))), 2)='00','23',RIGHT("00"+ALLTRIM(STR(INT(.w_MDORAFIN/60))), 2))
        endif
        .DoRTCalc(12,12,.t.)
        if .o_MDORAFIN<>.w_MDORAFIN
            .w_MINFIN = iif(RIGHT("00"+ALLTRIM(STR(MOD(.w_MDORAFIN,60))), 2)='00','59',RIGHT("00"+ALLTRIM(STR(MOD(.w_MDORAFIN,60))), 2))
        endif
        .DoRTCalc(14,14,.t.)
            .w_SHOWWEEK = IIF( .w_MD__FREQ $ "G-S-Q", "S", "N")
            .w_SHOWMONT = IIF( .w_MD__FREQ $ "M-A", "S", "N")
        if .o_MD__WEEK<>.w_MD__WEEK
            .w_Lun = BITAND(.w_MD__WEEK, 0x01)
        endif
        if .o_MD__WEEK<>.w_MD__WEEK
            .w_Mar = BITAND(.w_MD__WEEK, 0x02)
        endif
        if .o_MD__WEEK<>.w_MD__WEEK
            .w_Mer = BITAND(.w_MD__WEEK, 0x04)
        endif
        if .o_MD__WEEK<>.w_MD__WEEK
            .w_Gio = BITAND(.w_MD__WEEK, 0x08)
        endif
        if .o_MD__WEEK<>.w_MD__WEEK
            .w_Ven = BITAND(.w_MD__WEEK, 0x10)
        endif
        if .o_MD__WEEK<>.w_MD__WEEK
            .w_Sab = BITAND(.w_MD__WEEK, 0x20)
        endif
        if .o_MD__WEEK<>.w_MD__WEEK
            .w_Dom = BITAND(.w_MD__WEEK, 0x40)
        endif
        .DoRTCalc(24,24,.t.)
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Gen = BITAND(.w_MD_MONTH, 0x0001)
        endif
        .DoRTCalc(26,26,.t.)
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Feb = BITAND(.w_MD_MONTH, 0x0002)
        endif
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Marz = BITAND(.w_MD_MONTH, 0x0004)
        endif
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Apr = BITAND(.w_MD_MONTH, 0x0008)
        endif
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Mag = BITAND(.w_MD_MONTH, 0x0010)
        endif
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Giu = BITAND(.w_MD_MONTH, 0x0020)
        endif
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Lug = BITAND(.w_MD_MONTH, 0x0040)
        endif
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Ago = BITAND(.w_MD_MONTH, 0x0080)
        endif
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Set = BITAND(.w_MD_MONTH, 0x0100)
        endif
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Ott = BITAND(.w_MD_MONTH, 0x0200)
        endif
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Nov = BITAND(.w_MD_MONTH, 0x0400)
        endif
        if .o_MD_MONTH<>.w_MD_MONTH
            .w_Dic = BITAND(.w_MD_MONTH, 0x0800)
        endif
        .oPgFrm.Page1.oPag.LBL.Calculate(ICASE(.w_MD__FREQ='G',ah_msgformat('Giorno\i'), .w_MD__FREQ='S', ah_msgformat('Settimana\e'), .w_MD__FREQ='M', ah_msgformat('Mese\i'), .w_MD__FREQ='A', ah_msgformat('Anno\i'), .w_MD__FREQ='Q', ah_msgformat('Quindicina\e'), ''))
        .DoRTCalc(38,39,.t.)
        if .o_MDESCLUS<>.w_MDESCLUS
            .w_MDCALEND = IIF( .w_MDESCLUS = "C", .w_TCCODICE, Space(5) )
          .link_1_59('Full')
        endif
        .DoRTCalc(41,41,.t.)
          .link_1_62('Full')
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(43,43,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.LBL.Calculate(ICASE(.w_MD__FREQ='G',ah_msgformat('Giorno\i'), .w_MD__FREQ='S', ah_msgformat('Settimana\e'), .w_MD__FREQ='M', ah_msgformat('Mese\i'), .w_MD__FREQ='A', ah_msgformat('Anno\i'), .w_MD__FREQ='Q', ah_msgformat('Quindicina\e'), ''))
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
    endwith
  return

  proc Calculate_AHRTAZTNTZ()
    with this
          * --- Inizializza i giorni della settimana e i mesi come attivi
          .w_Lun = 0x01
          .w_Mar = 0x02
          .w_Mer = 0x04
          .w_Gio = 0x08
          .w_Ven = 0x10
          .w_Sab = 0x20
          .w_Dom = 0x40
          .w_MD__WEEK = BITOR(.w_lun, .w_mar, .w_mer, .w_gio, .w_ven, .w_sab, .w_dom)
          .w_Gen = 0x0001
          .w_Feb = 0x0002
          .w_Marz = 0x0004
          .w_Apr = 0x0008
          .w_Mag = 0x0010
          .w_Giu = 0x0020
          .w_Lug = 0x0040
          .w_Ago = 0x0080
          .w_Set = 0x0100
          .w_Ott = 0x0200
          .w_Nov = 0x0400
          .w_Dic = 0x0800
          .w_MD_MONTH = BITOR(.w_gen, .w_feb, .w_marz, .w_apr, .w_mag, .w_giu, .w_lug, .w_ago, .w_set, .w_ott, .w_nov, .w_dic)
    endwith
  endproc
  proc Calculate_LHRBWNCGGK()
    with this
          * --- Nel caso di frequenza giornaliera i minuti sono sbiancati se si specificano i giorni
          .w_MDMININT = IIF( .w_MD__FREQ='G' AND .w_MDNUMFRQ<>0, 0,  .w_MDMININT)
    endwith
  endproc
  proc Calculate_EHUGTMPZYM()
    with this
          * --- Nel caso di frequenza giornaliera i giorni sono sbiancati se si specificano i minuti
          .w_MDNUMFRQ = IIF( .w_MD__FREQ='G' AND .w_MDMININT<>0, 0,  .w_MDNUMFRQ)
    endwith
  endproc
  proc Calculate_JFKWKAKBJK()
    with this
          * --- Le esclusioni devono mostrare i casi "Domenica" e "Sabato e domenica" solo in caso di frequenza mensile o pluriennale
          .w_MDESCLUS = IIF( .w_MD__FREQ $ "U-G-S-Q", "N",  .w_MDESCLUS)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oORAFIN_1_18.enabled = this.oPgFrm.Page1.oPag.oORAFIN_1_18.mCond()
    this.oPgFrm.Page1.oPag.oMDNUMFRQ_1_19.enabled = this.oPgFrm.Page1.oPag.oMDNUMFRQ_1_19.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_20.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_20.mCond()
    this.oPgFrm.Page1.oPag.oMDMININT_1_21.enabled = this.oPgFrm.Page1.oPag.oMDMININT_1_21.mCond()
    this.oPgFrm.Page1.oPag.oMDESCLUS_1_56.enabled = this.oPgFrm.Page1.oPag.oMDESCLUS_1_56.mCond()
    this.oPgFrm.Page1.oPag.oMDCALEND_1_59.enabled = this.oPgFrm.Page1.oPag.oMDCALEND_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMDDATUNI_1_5.visible=!this.oPgFrm.Page1.oPag.oMDDATUNI_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oORAINI_1_14.visible=!this.oPgFrm.Page1.oPag.oORAINI_1_14.mHide()
    this.oPgFrm.Page1.oPag.oMININI_1_15.visible=!this.oPgFrm.Page1.oPag.oMININI_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oORAFIN_1_18.visible=!this.oPgFrm.Page1.oPag.oORAFIN_1_18.mHide()
    this.oPgFrm.Page1.oPag.oMDNUMFRQ_1_19.visible=!this.oPgFrm.Page1.oPag.oMDNUMFRQ_1_19.mHide()
    this.oPgFrm.Page1.oPag.oMINFIN_1_20.visible=!this.oPgFrm.Page1.oPag.oMINFIN_1_20.mHide()
    this.oPgFrm.Page1.oPag.oMDMININT_1_21.visible=!this.oPgFrm.Page1.oPag.oMDMININT_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oLun_1_28.visible=!this.oPgFrm.Page1.oPag.oLun_1_28.mHide()
    this.oPgFrm.Page1.oPag.oMar_1_29.visible=!this.oPgFrm.Page1.oPag.oMar_1_29.mHide()
    this.oPgFrm.Page1.oPag.oMer_1_30.visible=!this.oPgFrm.Page1.oPag.oMer_1_30.mHide()
    this.oPgFrm.Page1.oPag.oGio_1_31.visible=!this.oPgFrm.Page1.oPag.oGio_1_31.mHide()
    this.oPgFrm.Page1.oPag.oVen_1_32.visible=!this.oPgFrm.Page1.oPag.oVen_1_32.mHide()
    this.oPgFrm.Page1.oPag.oSab_1_33.visible=!this.oPgFrm.Page1.oPag.oSab_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDom_1_34.visible=!this.oPgFrm.Page1.oPag.oDom_1_34.mHide()
    this.oPgFrm.Page1.oPag.oMDIMPDAT_1_35.visible=!this.oPgFrm.Page1.oPag.oMDIMPDAT_1_35.mHide()
    this.oPgFrm.Page1.oPag.oGen_1_36.visible=!this.oPgFrm.Page1.oPag.oGen_1_36.mHide()
    this.oPgFrm.Page1.oPag.oMD_GGFIS_1_37.visible=!this.oPgFrm.Page1.oPag.oMD_GGFIS_1_37.mHide()
    this.oPgFrm.Page1.oPag.oFeb_1_38.visible=!this.oPgFrm.Page1.oPag.oFeb_1_38.mHide()
    this.oPgFrm.Page1.oPag.oMarz_1_39.visible=!this.oPgFrm.Page1.oPag.oMarz_1_39.mHide()
    this.oPgFrm.Page1.oPag.oApr_1_40.visible=!this.oPgFrm.Page1.oPag.oApr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oMag_1_41.visible=!this.oPgFrm.Page1.oPag.oMag_1_41.mHide()
    this.oPgFrm.Page1.oPag.oGiu_1_42.visible=!this.oPgFrm.Page1.oPag.oGiu_1_42.mHide()
    this.oPgFrm.Page1.oPag.oLug_1_43.visible=!this.oPgFrm.Page1.oPag.oLug_1_43.mHide()
    this.oPgFrm.Page1.oPag.oAgo_1_44.visible=!this.oPgFrm.Page1.oPag.oAgo_1_44.mHide()
    this.oPgFrm.Page1.oPag.oSet_1_45.visible=!this.oPgFrm.Page1.oPag.oSet_1_45.mHide()
    this.oPgFrm.Page1.oPag.oOtt_1_46.visible=!this.oPgFrm.Page1.oPag.oOtt_1_46.mHide()
    this.oPgFrm.Page1.oPag.oNov_1_47.visible=!this.oPgFrm.Page1.oPag.oNov_1_47.mHide()
    this.oPgFrm.Page1.oPag.oDic_1_48.visible=!this.oPgFrm.Page1.oPag.oDic_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oMDESCLUS_1_55.visible=!this.oPgFrm.Page1.oPag.oMDESCLUS_1_55.mHide()
    this.oPgFrm.Page1.oPag.oMDESCLUS_1_56.visible=!this.oPgFrm.Page1.oPag.oMDESCLUS_1_56.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("New record")
          .Calculate_AHRTAZTNTZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.LBL.Event(cEvent)
        if lower(cEvent)==lower("w_MD__FREQ") or lower(cEvent)==lower("w_MDNUMFRQ")
          .Calculate_LHRBWNCGGK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_MD__FREQ") or lower(cEvent)==lower("w_MDMININT")
          .Calculate_EHUGTMPZYM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_MD__FREQ")
          .Calculate_JFKWKAKBJK()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_amd
    * Se � specificato un intervallo in minuti di ripetibilit� l'ora inizio e l'ora fine devono definire un intervallo di validit�
    IF cEVENT = "w_MDMININT Changed"
       IF this.w_MDMININT > 0 and VAL(this.w_ORAFIN) * 60 + VAL(this.w_MINFIN) <= VAL(this.w_ORAINI) * 60 + VAL(this.w_MININI)
         this.w_ORAINI = "00"
         this.w_MININI = "00"
         this.w_ORAFIN = "23"
         this.w_MINFIN = "59"
       ENDIF
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MDCALEND
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCALEND) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATL',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_MDCALEND)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_MDCALEND))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCALEND)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStrODBC(trim(this.w_MDCALEND)+"%");

            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStr(trim(this.w_MDCALEND)+"%");

            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MDCALEND) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oMDCALEND_1_59'),i_cWhere,'GSAR_ATL',"Calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCALEND)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_MDCALEND);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_MDCALEND)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCALEND = NVL(_Link_.TCCODICE,space(5))
      this.w_TCDESCRI = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MDCALEND = space(5)
      endif
      this.w_TCDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCALEND Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_59(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_CALE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_59.TCCODICE as TCCODICE159"+ ",link_1_59.TCDESCRI as TCDESCRI159"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_59 on MODCLDAT.MDCALEND=link_1_59.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_59"
          i_cKey=i_cKey+'+" and MODCLDAT.MDCALEND=link_1_59.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TCFLDEFA
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TCFLDEFA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TCFLDEFA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCFLDEFA,TCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TCFLDEFA="+cp_ToStrODBC(this.w_TCFLDEFA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCFLDEFA',this.w_TCFLDEFA)
            select TCFLDEFA,TCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TCFLDEFA = NVL(_Link_.TCFLDEFA,space(1))
      this.w_TCCODICE = NVL(_Link_.TCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TCFLDEFA = space(1)
      endif
      this.w_TCCODICE = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCFLDEFA,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TCFLDEFA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMDCODICE_1_1.value==this.w_MDCODICE)
      this.oPgFrm.Page1.oPag.oMDCODICE_1_1.value=this.w_MDCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDESCRI_1_3.value==this.w_MDDESCRI)
      this.oPgFrm.Page1.oPag.oMDDESCRI_1_3.value=this.w_MDDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMD__FREQ_1_4.RadioValue()==this.w_MD__FREQ)
      this.oPgFrm.Page1.oPag.oMD__FREQ_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDATUNI_1_5.value==this.w_MDDATUNI)
      this.oPgFrm.Page1.oPag.oMDDATUNI_1_5.value=this.w_MDDATUNI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_14.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_14.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_15.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_15.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_1_18.value==this.w_ORAFIN)
      this.oPgFrm.Page1.oPag.oORAFIN_1_18.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMDNUMFRQ_1_19.value==this.w_MDNUMFRQ)
      this.oPgFrm.Page1.oPag.oMDNUMFRQ_1_19.value=this.w_MDNUMFRQ
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_20.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_20.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMDMININT_1_21.value==this.w_MDMININT)
      this.oPgFrm.Page1.oPag.oMDMININT_1_21.value=this.w_MDMININT
    endif
    if not(this.oPgFrm.Page1.oPag.oLun_1_28.RadioValue()==this.w_Lun)
      this.oPgFrm.Page1.oPag.oLun_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMar_1_29.RadioValue()==this.w_Mar)
      this.oPgFrm.Page1.oPag.oMar_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMer_1_30.RadioValue()==this.w_Mer)
      this.oPgFrm.Page1.oPag.oMer_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGio_1_31.RadioValue()==this.w_Gio)
      this.oPgFrm.Page1.oPag.oGio_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVen_1_32.RadioValue()==this.w_Ven)
      this.oPgFrm.Page1.oPag.oVen_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSab_1_33.RadioValue()==this.w_Sab)
      this.oPgFrm.Page1.oPag.oSab_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDom_1_34.RadioValue()==this.w_Dom)
      this.oPgFrm.Page1.oPag.oDom_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMDIMPDAT_1_35.RadioValue()==this.w_MDIMPDAT)
      this.oPgFrm.Page1.oPag.oMDIMPDAT_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGen_1_36.RadioValue()==this.w_Gen)
      this.oPgFrm.Page1.oPag.oGen_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMD_GGFIS_1_37.value==this.w_MD_GGFIS)
      this.oPgFrm.Page1.oPag.oMD_GGFIS_1_37.value=this.w_MD_GGFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oFeb_1_38.RadioValue()==this.w_Feb)
      this.oPgFrm.Page1.oPag.oFeb_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMarz_1_39.RadioValue()==this.w_Marz)
      this.oPgFrm.Page1.oPag.oMarz_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oApr_1_40.RadioValue()==this.w_Apr)
      this.oPgFrm.Page1.oPag.oApr_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMag_1_41.RadioValue()==this.w_Mag)
      this.oPgFrm.Page1.oPag.oMag_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGiu_1_42.RadioValue()==this.w_Giu)
      this.oPgFrm.Page1.oPag.oGiu_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLug_1_43.RadioValue()==this.w_Lug)
      this.oPgFrm.Page1.oPag.oLug_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAgo_1_44.RadioValue()==this.w_Ago)
      this.oPgFrm.Page1.oPag.oAgo_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSet_1_45.RadioValue()==this.w_Set)
      this.oPgFrm.Page1.oPag.oSet_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOtt_1_46.RadioValue()==this.w_Ott)
      this.oPgFrm.Page1.oPag.oOtt_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNov_1_47.RadioValue()==this.w_Nov)
      this.oPgFrm.Page1.oPag.oNov_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDic_1_48.RadioValue()==this.w_Dic)
      this.oPgFrm.Page1.oPag.oDic_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMDESCLUS_1_55.RadioValue()==this.w_MDESCLUS)
      this.oPgFrm.Page1.oPag.oMDESCLUS_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMDESCLUS_1_56.RadioValue()==this.w_MDESCLUS)
      this.oPgFrm.Page1.oPag.oMDESCLUS_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCALEND_1_59.value==this.w_MDCALEND)
      this.oPgFrm.Page1.oPag.oMDCALEND_1_59.value=this.w_MDCALEND
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDESCRI_1_60.value==this.w_TCDESCRI)
      this.oPgFrm.Page1.oPag.oTCDESCRI_1_60.value=this.w_TCDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'MODCLDAT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(Not( .w_MDMININT<>0 And !(.w_MDORAFIN>=.w_MDORAINI Or .w_MDORAFIN=0) ))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora inizio minore di ora fine")
          case   (empty(.w_MDCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMDCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_MDCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MDDESCRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMDDESCRI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_MDDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ORAINI<"24")  and not(.w_MD__FREQ<>'G')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MININI<"60")  and not(.w_MD__FREQ<>'G')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ORAFIN<"24")  and not(.w_MD__FREQ<>'G')  and (.w_MDMININT<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAFIN_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MDMININT >= 0 OR .w_MDNUMFRQ >= 0)  and not(.w_MD__FREQ = 'U')  and (NOT( .w_MD__FREQ='G' AND .w_MDMININT<>0 ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMDNUMFRQ_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La frequenza non pu� essere negativa")
          case   not(.w_MINFIN<"60")  and not(.w_MD__FREQ<>'G')  and (.w_MDMININT<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MDMININT >= 0 OR .w_MDNUMFRQ >= 0)  and not(.w_MD__FREQ<>'G')  and (NOT( .w_MD__FREQ='G' AND .w_MDNUMFRQ<>0 ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMDMININT_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'intervallo in minuti non pu� essere negativo")
          case   not(.w_MD_GGFIS>0 and .w_MD_GGFIS<=31)  and not(NOT( .w_MD__FREQ$'M-A' AND .w_MDIMPDAT$'D-G' ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMD_GGFIS_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore tra 1 e 31")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_amd
      IF i_bRes AND (! This.w_MD__FREQ $ 'U-Q-M' OR (This.w_MDIMPDAT='N' and This.w_MD__FREQ='M')) AND this.w_MDNUMFRQ<0 and this.w_MDMININT<0
        i_bnoChk = .F.
        i_bRes = .F.
        IF this.w_MDNUMFRQ<0
         i_cErrorMsg = Ah_MsgFormat("La frequenza non pu� essere negativa")
        ENDIF
        IF this.w_MDMININT<0
         i_cErrorMsg = Ah_MsgFormat("L'intervallo in minuti non pu� essere negativo")
        ENDIF
      ENDIF
      IF i_bRes AND this.w_SHOWWEEK='S' AND this.w_MD__WEEK=0
         i_bnoChk = .F.
         i_bRes = .F.
         i_cErrorMsg = Ah_MsgFormat("Occorre specificare almeno un giorno della settimana")
      ENDIF
      IF i_bRes AND this.w_SHOWMONT='S' AND this.w_MD_MONTH=0
         i_bnoChk = .F.
         i_bRes = .F.
         i_cErrorMsg = Ah_MsgFormat("Occorre specificare almeno un mese")
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MDORAINI = this.w_MDORAINI
    this.o_MDORAFIN = this.w_MDORAFIN
    this.o_MD__WEEK = this.w_MD__WEEK
    this.o_MD_MONTH = this.w_MD_MONTH
    this.o_MDESCLUS = this.w_MDESCLUS
    return

enddefine

* --- Define pages as container
define class tgsar_amdPag1 as StdContainer
  Width  = 521
  height = 318
  stdWidth  = 521
  stdheight = 318
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMDCODICE_1_1 as StdField with uid="BJAVBPVQVK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MDCODICE", cQueryName = "MDCODICE",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 17433099,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=88, Top=20, InputMask=replicate('X',3)

  add object oMDDESCRI_1_3 as StdField with uid="OUOEMAVUBC",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MDDESCRI", cQueryName = "MDDESCRI",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 200282639,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=141, Top=20, InputMask=replicate('X',50)


  add object oMD__FREQ_1_4 as StdCombo with uid="SABQCEXKQG",rtseq=3,rtrep=.f.,left=88,top=50,width=142,height=21;
    , ToolTipText = "Frequenza del calcolo";
    , HelpContextID = 171688471;
    , cFormVar="w_MD__FREQ",RowSource=""+"Unica,"+"Giornaliera,"+"Settimanale,"+"Quindicinale (1 - 15),"+"Mensile,"+"Pluriennale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMD__FREQ_1_4.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'G',;
    iif(this.value =3,'S',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'M',;
    iif(this.value =6,'A',;
    space(1))))))))
  endfunc
  func oMD__FREQ_1_4.GetRadio()
    this.Parent.oContained.w_MD__FREQ = this.RadioValue()
    return .t.
  endfunc

  func oMD__FREQ_1_4.SetRadio()
    this.Parent.oContained.w_MD__FREQ=trim(this.Parent.oContained.w_MD__FREQ)
    this.value = ;
      iif(this.Parent.oContained.w_MD__FREQ=='U',1,;
      iif(this.Parent.oContained.w_MD__FREQ=='G',2,;
      iif(this.Parent.oContained.w_MD__FREQ=='S',3,;
      iif(this.Parent.oContained.w_MD__FREQ=='Q',4,;
      iif(this.Parent.oContained.w_MD__FREQ=='M',5,;
      iif(this.Parent.oContained.w_MD__FREQ=='A',6,;
      0))))))
  endfunc

  add object oMDDATUNI_1_5 as StdField with uid="XMRYNNUCII",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MDDATUNI", cQueryName = "MDDATUNI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data per frequenza unica",;
    HelpContextID = 33811953,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=283, Top=50

  func oMDDATUNI_1_5.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'U')
    endwith
  endfunc

  add object oORAINI_1_14 as StdField with uid="GXQCAWSXBP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Ora inizio",;
    HelpContextID = 27520998,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=446, Top=50, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oORAINI_1_14.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  func oORAINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ORAINI<"24")
    endwith
    return bRes
  endfunc

  add object oMININI_1_15 as StdField with uid="JWKQKAJXVM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Minuti inizio",;
    HelpContextID = 27571910,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=478, Top=50, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMININI_1_15.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  func oMININI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MININI<"60")
    endwith
    return bRes
  endfunc

  add object oORAFIN_1_18 as StdField with uid="CWOMTDKPPU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Ora fine",;
    HelpContextID = 105967590,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=446, Top=79, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oORAFIN_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MDMININT<>0)
    endwith
   endif
  endfunc

  func oORAFIN_1_18.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  func oORAFIN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ORAFIN<"24")
    endwith
    return bRes
  endfunc

  add object oMDNUMFRQ_1_19 as StdField with uid="JMEIICXSUZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MDNUMFRQ", cQueryName = "MDNUMFRQ",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La frequenza non pu� essere negativa",;
    ToolTipText = "Intervallo di frequenza",;
    HelpContextID = 245412375,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=88, Top=79, cSayPict='"999"', cGetPict='"999"'

  func oMDNUMFRQ_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT( .w_MD__FREQ='G' AND .w_MDMININT<>0 ))
    endwith
   endif
  endfunc

  func oMDNUMFRQ_1_19.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ = 'U')
    endwith
  endfunc

  func oMDNUMFRQ_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MDMININT >= 0 OR .w_MDNUMFRQ >= 0)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_20 as StdField with uid="SLTOLLRXGF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Minuti inizio",;
    HelpContextID = 106018502,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=478, Top=79, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMINFIN_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MDMININT<>0)
    endwith
   endif
  endfunc

  func oMINFIN_1_20.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  func oMINFIN_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MINFIN<"60")
    endwith
    return bRes
  endfunc

  add object oMDMININT_1_21 as StdField with uid="XZGMDZHAHA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MDMININT", cQueryName = "MDMININT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'intervallo in minuti non pu� essere negativo",;
    ToolTipText = "Intervallo in minuti di ripetibilit�",;
    HelpContextID = 240868838,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=283, Top=79, cSayPict='"9999"', cGetPict='"9999"'

  func oMDMININT_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT( .w_MD__FREQ='G' AND .w_MDNUMFRQ<>0 ))
    endwith
   endif
  endfunc

  func oMDMININT_1_21.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  func oMDMININT_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MDMININT >= 0 OR .w_MDNUMFRQ >= 0)
    endwith
    return bRes
  endfunc

  add object oLun_1_28 as StdCheck with uid="GOZBQWASTO",rtseq=17,rtrep=.f.,left=89, top=159, caption="Lun",;
    ToolTipText = "Luned�",;
    HelpContextID = 209853770,;
    cFormVar="w_Lun", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLun_1_28.RadioValue()
    return(iif(this.value =1,0x01,;
    0))
  endfunc
  func oLun_1_28.GetRadio()
    this.Parent.oContained.w_Lun = this.RadioValue()
    return .t.
  endfunc

  func oLun_1_28.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Lun==0x01,1,;
      0)
  endfunc

  func oLun_1_28.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oMar_1_29 as StdCheck with uid="UHDFKKSAQX",rtseq=18,rtrep=.f.,left=141, top=159, caption="Mar",;
    ToolTipText = "Marted�",;
    HelpContextID = 209842490,;
    cFormVar="w_Mar", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMar_1_29.RadioValue()
    return(iif(this.value =1,0x02,;
    0))
  endfunc
  func oMar_1_29.GetRadio()
    this.Parent.oContained.w_Mar = this.RadioValue()
    return .t.
  endfunc

  func oMar_1_29.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Mar==0x02,1,;
      0)
  endfunc

  func oMar_1_29.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oMer_1_30 as StdCheck with uid="PTXXQJNHDX",rtseq=19,rtrep=.f.,left=195, top=159, caption="Mer",;
    ToolTipText = "Mercoled�",;
    HelpContextID = 209841466,;
    cFormVar="w_Mer", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMer_1_30.RadioValue()
    return(iif(this.value =1,0x04,;
    0))
  endfunc
  func oMer_1_30.GetRadio()
    this.Parent.oContained.w_Mer = this.RadioValue()
    return .t.
  endfunc

  func oMer_1_30.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Mer==0x04,1,;
      0)
  endfunc

  func oMer_1_30.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oGio_1_31 as StdCheck with uid="CRNEGPHSLO",rtseq=20,rtrep=.f.,left=245, top=159, caption="Gio",;
    ToolTipText = "Gioved�",;
    HelpContextID = 209852826,;
    cFormVar="w_Gio", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGio_1_31.RadioValue()
    return(iif(this.value =1,0x08,;
    0))
  endfunc
  func oGio_1_31.GetRadio()
    this.Parent.oContained.w_Gio = this.RadioValue()
    return .t.
  endfunc

  func oGio_1_31.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Gio==0x08,1,;
      0)
  endfunc

  func oGio_1_31.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oVen_1_32 as StdCheck with uid="HJNXEMHAGM",rtseq=21,rtrep=.f.,left=296, top=159, caption="Ven",;
    ToolTipText = "Venerd�",;
    HelpContextID = 209857706,;
    cFormVar="w_Ven", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVen_1_32.RadioValue()
    return(iif(this.value =1,0x10,;
    0))
  endfunc
  func oVen_1_32.GetRadio()
    this.Parent.oContained.w_Ven = this.RadioValue()
    return .t.
  endfunc

  func oVen_1_32.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Ven==0x10,1,;
      0)
  endfunc

  func oVen_1_32.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oSab_1_33 as StdCheck with uid="XHEYPUULCP",rtseq=22,rtrep=.f.,left=360, top=159, caption="Sab",;
    ToolTipText = "Sabato",;
    HelpContextID = 209907930,;
    cFormVar="w_Sab", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSab_1_33.RadioValue()
    return(iif(this.value =1,0x20,;
    0))
  endfunc
  func oSab_1_33.GetRadio()
    this.Parent.oContained.w_Sab = this.RadioValue()
    return .t.
  endfunc

  func oSab_1_33.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Sab==0x20,1,;
      0)
  endfunc

  func oSab_1_33.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oDom_1_34 as StdCheck with uid="UBZXQXOFLC",rtseq=23,rtrep=.f.,left=418, top=159, caption="Dom",;
    ToolTipText = "Domenica",;
    HelpContextID = 209859530,;
    cFormVar="w_Dom", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDom_1_34.RadioValue()
    return(iif(this.value =1,0x40,;
    0))
  endfunc
  func oDom_1_34.GetRadio()
    this.Parent.oContained.w_Dom = this.RadioValue()
    return .t.
  endfunc

  func oDom_1_34.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Dom==0x40,1,;
      0)
  endfunc

  func oDom_1_34.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc


  add object oMDIMPDAT_1_35 as StdCombo with uid="SCZXXXMFCD",rtseq=24,rtrep=.f.,left=171,top=120,width=155,height=21;
    , ToolTipText = "Frequenza mensile";
    , HelpContextID = 214458906;
    , cFormVar="w_MDIMPDAT",RowSource=""+"Nessun rinvio,"+"Nessun rinvio o Fine mese,"+"Fine mese,"+"Giorno fisso,"+"Fine mese + giorno fisso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMDIMPDAT_1_35.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'Z',;
    iif(this.value =3,'F',;
    iif(this.value =4,'D',;
    iif(this.value =5,'G',;
    space(1)))))))
  endfunc
  func oMDIMPDAT_1_35.GetRadio()
    this.Parent.oContained.w_MDIMPDAT = this.RadioValue()
    return .t.
  endfunc

  func oMDIMPDAT_1_35.SetRadio()
    this.Parent.oContained.w_MDIMPDAT=trim(this.Parent.oContained.w_MDIMPDAT)
    this.value = ;
      iif(this.Parent.oContained.w_MDIMPDAT=='N',1,;
      iif(this.Parent.oContained.w_MDIMPDAT=='Z',2,;
      iif(this.Parent.oContained.w_MDIMPDAT=='F',3,;
      iif(this.Parent.oContained.w_MDIMPDAT=='D',4,;
      iif(this.Parent.oContained.w_MDIMPDAT=='G',5,;
      0)))))
  endfunc

  func oMDIMPDAT_1_35.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'M' AND .w_MD__FREQ<>'A')
    endwith
  endfunc

  add object oGen_1_36 as StdCheck with uid="HPCWCEDXTF",rtseq=25,rtrep=.f.,left=89, top=159, caption="Gen",;
    ToolTipText = "Gennaio",;
    HelpContextID = 209857946,;
    cFormVar="w_Gen", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGen_1_36.RadioValue()
    return(iif(this.value =1,0x0001,;
    0))
  endfunc
  func oGen_1_36.GetRadio()
    this.Parent.oContained.w_Gen = this.RadioValue()
    return .t.
  endfunc

  func oGen_1_36.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Gen==0x0001,1,;
      0)
  endfunc

  func oGen_1_36.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oMD_GGFIS_1_37 as StdField with uid="MTRRZUVHVM",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MD_GGFIS", cQueryName = "MD_GGFIS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore tra 1 e 31",;
    ToolTipText = "Giorno per data fissa",;
    HelpContextID = 30162407,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=478, Top=120, cSayPict='"@Z99"', cGetPict='"@Z99"'

  func oMD_GGFIS_1_37.mHide()
    with this.Parent.oContained
      return (NOT( .w_MD__FREQ$'M-A' AND .w_MDIMPDAT$'D-G' ))
    endwith
  endfunc

  func oMD_GGFIS_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MD_GGFIS>0 and .w_MD_GGFIS<=31)
    endwith
    return bRes
  endfunc

  add object oFeb_1_38 as StdCheck with uid="XXKKJPGUES",rtseq=27,rtrep=.f.,left=141, top=159, caption="Feb",;
    ToolTipText = "Febbraio",;
    HelpContextID = 209907114,;
    cFormVar="w_Feb", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFeb_1_38.RadioValue()
    return(iif(this.value =1,0x0002,;
    0))
  endfunc
  func oFeb_1_38.GetRadio()
    this.Parent.oContained.w_Feb = this.RadioValue()
    return .t.
  endfunc

  func oFeb_1_38.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Feb==0x0002,1,;
      0)
  endfunc

  func oFeb_1_38.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oMarz_1_39 as StdCheck with uid="KJVAJHLNOF",rtseq=28,rtrep=.f.,left=195, top=159, caption="Mar",;
    ToolTipText = "Marzo",;
    HelpContextID = 201847098,;
    cFormVar="w_Marz", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMarz_1_39.RadioValue()
    return(iif(this.value =1,0x0004,;
    0))
  endfunc
  func oMarz_1_39.GetRadio()
    this.Parent.oContained.w_Marz = this.RadioValue()
    return .t.
  endfunc

  func oMarz_1_39.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Marz==0x0004,1,;
      0)
  endfunc

  func oMarz_1_39.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oApr_1_40 as StdCheck with uid="KZCVPRZDXR",rtseq=29,rtrep=.f.,left=245, top=159, caption="Apr",;
    ToolTipText = "Aprile",;
    HelpContextID = 209838842,;
    cFormVar="w_Apr", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oApr_1_40.RadioValue()
    return(iif(this.value =1,0x0008,;
    0))
  endfunc
  func oApr_1_40.GetRadio()
    this.Parent.oContained.w_Apr = this.RadioValue()
    return .t.
  endfunc

  func oApr_1_40.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Apr==0x0008,1,;
      0)
  endfunc

  func oApr_1_40.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oMag_1_41 as StdCheck with uid="CUVGCXYGYY",rtseq=30,rtrep=.f.,left=296, top=159, caption="Mag",;
    ToolTipText = "Maggio",;
    HelpContextID = 209887546,;
    cFormVar="w_Mag", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMag_1_41.RadioValue()
    return(iif(this.value =1,0x0010,;
    0))
  endfunc
  func oMag_1_41.GetRadio()
    this.Parent.oContained.w_Mag = this.RadioValue()
    return .t.
  endfunc

  func oMag_1_41.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Mag==0x0010,1,;
      0)
  endfunc

  func oMag_1_41.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oGiu_1_42 as StdCheck with uid="OLHCQFNWTP",rtseq=31,rtrep=.f.,left=360, top=159, caption="Giu",;
    ToolTipText = "Giugno",;
    HelpContextID = 209828250,;
    cFormVar="w_Giu", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGiu_1_42.RadioValue()
    return(iif(this.value =1,0x0020,;
    0))
  endfunc
  func oGiu_1_42.GetRadio()
    this.Parent.oContained.w_Giu = this.RadioValue()
    return .t.
  endfunc

  func oGiu_1_42.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Giu==0x0020,1,;
      0)
  endfunc

  func oGiu_1_42.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oLug_1_43 as StdCheck with uid="VAYQPSRXHY",rtseq=32,rtrep=.f.,left=89, top=185, caption="Lug",;
    ToolTipText = "Luglio",;
    HelpContextID = 209882442,;
    cFormVar="w_Lug", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLug_1_43.RadioValue()
    return(iif(this.value =1,0x0040,;
    0))
  endfunc
  func oLug_1_43.GetRadio()
    this.Parent.oContained.w_Lug = this.RadioValue()
    return .t.
  endfunc

  func oLug_1_43.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Lug==0x0040,1,;
      0)
  endfunc

  func oLug_1_43.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oAgo_1_44 as StdCheck with uid="AHGRZFGFKS",rtseq=33,rtrep=.f.,left=141, top=185, caption="Ago",;
    ToolTipText = "Agosto",;
    HelpContextID = 209853434,;
    cFormVar="w_Ago", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAgo_1_44.RadioValue()
    return(iif(this.value =1,0x0080,;
    0))
  endfunc
  func oAgo_1_44.GetRadio()
    this.Parent.oContained.w_Ago = this.RadioValue()
    return .t.
  endfunc

  func oAgo_1_44.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Ago==0x0080,1,;
      0)
  endfunc

  func oAgo_1_44.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oSet_1_45 as StdCheck with uid="LOPICJVCYI",rtseq=34,rtrep=.f.,left=195, top=185, caption="Set",;
    ToolTipText = "Settembre",;
    HelpContextID = 209833178,;
    cFormVar="w_Set", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSet_1_45.RadioValue()
    return(iif(this.value =1,0x0100,;
    0))
  endfunc
  func oSet_1_45.GetRadio()
    this.Parent.oContained.w_Set = this.RadioValue()
    return .t.
  endfunc

  func oSet_1_45.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Set==0x0100,1,;
      0)
  endfunc

  func oSet_1_45.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oOtt_1_46 as StdCheck with uid="DUNYJWBVSY",rtseq=35,rtrep=.f.,left=245, top=185, caption="Ott",;
    ToolTipText = "Ottobre",;
    HelpContextID = 209829402,;
    cFormVar="w_Ott", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOtt_1_46.RadioValue()
    return(iif(this.value =1,0x0200,;
    0))
  endfunc
  func oOtt_1_46.GetRadio()
    this.Parent.oContained.w_Ott = this.RadioValue()
    return .t.
  endfunc

  func oOtt_1_46.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Ott==0x0200,1,;
      0)
  endfunc

  func oOtt_1_46.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oNov_1_47 as StdCheck with uid="TEHVJRNEWH",rtseq=36,rtrep=.f.,left=296, top=185, caption="Nov",;
    ToolTipText = "Novembre",;
    HelpContextID = 209822506,;
    cFormVar="w_Nov", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNov_1_47.RadioValue()
    return(iif(this.value =1,0x0400,;
    0))
  endfunc
  func oNov_1_47.GetRadio()
    this.Parent.oContained.w_Nov = this.RadioValue()
    return .t.
  endfunc

  func oNov_1_47.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Nov==0x0400,1,;
      0)
  endfunc

  func oNov_1_47.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oDic_1_48 as StdCheck with uid="BTFGPFWMTA",rtseq=37,rtrep=.f.,left=360, top=185, caption="Dic",;
    ToolTipText = "Dicembre",;
    HelpContextID = 209902026,;
    cFormVar="w_Dic", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDic_1_48.RadioValue()
    return(iif(this.value =1,0x0800,;
    0))
  endfunc
  func oDic_1_48.GetRadio()
    this.Parent.oContained.w_Dic = this.RadioValue()
    return .t.
  endfunc

  func oDic_1_48.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Dic==0x0800,1,;
      0)
  endfunc

  func oDic_1_48.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc


  add object LBL as cp_calclbl with uid="ILXKCISRUM",left=126, top=83, width=109,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",alignment=0,;
    nPag=1;
    , HelpContextID = 236097510


  add object oBtn_1_51 as StdButton with uid="LFSDLTFOOV",left=469, top=271, width=48,height=45,;
    CpPicture="BMP\PREVEDI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per testare il metodo di calcolo";
    , HelpContextID = 202235082;
    , caption='\<Test';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      do GSAR_KTD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_51.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Upper(.cFunction)="QUERY" And Not Empty(.w_MDCODICE))
      endwith
    endif
  endfunc


  add object oMDESCLUS_1_55 as StdCombo with uid="EREGNQEMJR",rtseq=38,rtrep=.f.,left=171,top=217,width=155,height=21;
    , ToolTipText = "Esclusioni";
    , HelpContextID = 66986521;
    , cFormVar="w_MDESCLUS",RowSource=""+"Nessuna,"+"Domenica,"+"Sabato e domenica,"+"Da calendario", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMDESCLUS_1_55.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'S',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oMDESCLUS_1_55.GetRadio()
    this.Parent.oContained.w_MDESCLUS = this.RadioValue()
    return .t.
  endfunc

  func oMDESCLUS_1_55.SetRadio()
    this.Parent.oContained.w_MDESCLUS=trim(this.Parent.oContained.w_MDESCLUS)
    this.value = ;
      iif(this.Parent.oContained.w_MDESCLUS=='N',1,;
      iif(this.Parent.oContained.w_MDESCLUS=='D',2,;
      iif(this.Parent.oContained.w_MDESCLUS=='S',3,;
      iif(this.Parent.oContained.w_MDESCLUS=='C',4,;
      0))))
  endfunc

  func oMDESCLUS_1_55.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ $ "U-G-S-Q")
    endwith
  endfunc


  add object oMDESCLUS_1_56 as StdCombo with uid="LQIFHUQLZM",rtseq=39,rtrep=.f.,left=171,top=217,width=155,height=21;
    , ToolTipText = "Esclusioni";
    , HelpContextID = 66986521;
    , cFormVar="w_MDESCLUS",RowSource=""+"Nessuna,"+"Da calendario", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMDESCLUS_1_56.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oMDESCLUS_1_56.GetRadio()
    this.Parent.oContained.w_MDESCLUS = this.RadioValue()
    return .t.
  endfunc

  func oMDESCLUS_1_56.SetRadio()
    this.Parent.oContained.w_MDESCLUS=trim(this.Parent.oContained.w_MDESCLUS)
    this.value = ;
      iif(this.Parent.oContained.w_MDESCLUS=='N',1,;
      iif(this.Parent.oContained.w_MDESCLUS=='C',2,;
      0))
  endfunc

  func oMDESCLUS_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MD__FREQ <> "U")
    endwith
   endif
  endfunc

  func oMDESCLUS_1_56.mHide()
    with this.Parent.oContained
      return (NOT .w_MD__FREQ $ "U-G-S-Q")
    endwith
  endfunc

  add object oMDCALEND_1_59 as StdField with uid="QURXCRTUEE",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MDCALEND", cQueryName = "MDCALEND",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 42204662,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=171, Top=243, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", cZoomOnZoom="GSAR_ATL", oKey_1_1="TCCODICE", oKey_1_2="this.w_MDCALEND"

  func oMDCALEND_1_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MDESCLUS = "C")
    endwith
   endif
  endfunc

  func oMDCALEND_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_59('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCALEND_1_59.ecpDrop(oSource)
    this.Parent.oContained.link_1_59('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCALEND_1_59.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oMDCALEND_1_59'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATL',"Calendari",'',this.parent.oContained
  endproc
  proc oMDCALEND_1_59.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_MDCALEND
     i_obj.ecpSave()
  endproc

  add object oTCDESCRI_1_60 as StdField with uid="TVPBHTLIUA",rtseq=41,rtrep=.f.,;
    cFormVar = "w_TCDESCRI", cQueryName = "TCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200282495,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=259, Top=243, InputMask=replicate('X',40)


  add object oObj_1_64 as cp_runprogram with uid="BQMTUNRJQI",left=543, top=268, width=268,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_B04( 'C', w_MDCALEND, ' ' )",;
    cEvent = "w_MDCALEND Changed",;
    nPag=1;
    , HelpContextID = 236097510


  add object oObj_1_65 as cp_runprogram with uid="QXPSNGMQBF",left=543, top=291, width=268,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_B04( 'E', w_TCCODICE, w_MDESCLUS )",;
    cEvent = "w_MDESCLUS Changed",;
    nPag=1;
    , HelpContextID = 236097510

  add object oStr_1_2 as StdString with uid="RGYURJOLQN",Visible=.t., Left=28, Top=24,;
    Alignment=1, Width=59, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="MGRUEYYQNQ",Visible=.t., Left=406, Top=124,;
    Alignment=1, Width=71, Height=18,;
    Caption="Il giorno:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (NOT( .w_MD__FREQ$'M-A' AND .w_MDIMPDAT$'D-G' ))
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="GEEFFRVART",Visible=.t., Left=372, Top=54,;
    Alignment=1, Width=73, Height=18,;
    Caption="Ora inizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="YQMVADPNQU",Visible=.t., Left=238, Top=83,;
    Alignment=1, Width=45, Height=18,;
    Caption="Ogni:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="YFHADDFNPB",Visible=.t., Left=472, Top=50,;
    Alignment=1, Width=8, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="SQXYJVUQOS",Visible=.t., Left=378, Top=83,;
    Alignment=1, Width=67, Height=18,;
    Caption="Fino alle:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="LSETHHFVBB",Visible=.t., Left=472, Top=79,;
    Alignment=1, Width=8, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="PRDTRZBCLA",Visible=.t., Left=4, Top=54,;
    Alignment=1, Width=83, Height=18,;
    Caption="Frequenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="GNQADKBNWC",Visible=.t., Left=328, Top=83,;
    Alignment=0, Width=47, Height=18,;
    Caption="Minuti"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'G')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="YFOAJEGRRN",Visible=.t., Left=39, Top=124,;
    Alignment=1, Width=131, Height=18,;
    Caption="Rinvio a:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'M' AND .w_MD__FREQ<>'A')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="ZMMUNQXSIT",Visible=.t., Left=36, Top=83,;
    Alignment=1, Width=51, Height=18,;
    Caption="Ogni:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ = 'U')
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="LBXFFHAHMG",Visible=.t., Left=236, Top=54,;
    Alignment=1, Width=47, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'U')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="YPECBPKCEQ",Visible=.t., Left=39, Top=221,;
    Alignment=1, Width=131, Height=18,;
    Caption="Esclusioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="TCLEPAIOYT",Visible=.t., Left=0, Top=248,;
    Alignment=1, Width=170, Height=18,;
    Caption="Calendario per esclusione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_amd','MODCLDAT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MDCODICE=MODCLDAT.MDCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_amd
*--- Abilita il bottone di test
Proc AbilitaBottone(pParent)
   local l_oCtrl
   l_oCtrl=pParent.GetCtrl(Cp_Translate("Test"))
   l_oCtrl.Enabled=.t.
   l_oCtrl=.null.
EndProc
* --- Fine Area Manuale
