* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcg                                                        *
*              Carica cambi in automatico                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_13]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-20                                                      *
* Last revis.: 2006-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcg",oParentObject)
return(i_retval)

define class tgsar_bcg as StdBatch
  * --- Local variables
  w_KEYCAM = ctod("  /  /  ")
  w_DATA = ctod("  /  /  ")
  w_INIZIO = .f.
  w_CAMBIO = 0
  * --- WorkFile variables
  CAM_BI_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica temporaneo Cambi dei Dati del giorno Precedente (da GSAR_MCG)
    * --- Se Sono in Caricamento
    if EMPTY(this.oParentObject.w_CGDATCAM)
      AH_ERRORMSG ("Inserire una data di cambio",48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.cFunction = "Load"
      * --- Azzera il Transitorio
      this.oParentObject.BlankRec()
      this.w_INIZIO = .T.
      * --- Quindi cicla Sui cambi del giorno e Carica il Transitorio
      VQ_EXEC("CROSS2.VQR",this,"CROSS")
      GO TOP
      SCAN
      * --- Nuova Riga del Temporaneo
      if this.w_INIZIO=.F.
        * --- Append gia' eseguito la prima volta nella BlankRec
        this.oParentObject.InitRow()
      else
        this.w_INIZIO = .F.
      endif
      this.oParentObject.w_CGCODVAL = NVL(CROSS.CGCODVAL, SPACE(3))
      this.oParentObject.w_DESVAL = NVL(CROSS.VADESVAL, SPACE(10))
      this.oParentObject.w_CGCAMBIO = NVL(CROSS.CGCAMBIO, 0)
      * --- Carica il Temporaneo dei Dati e skippa al record successivo
      this.oParentObject.TrsFromWork()
      ENDSCAN
      * --- Questa Parte derivata dal Metodo LoadRec
      SELECT (this.oParentObject.cTrsName)
      GO TOP
      With this.oParentObject
      .WorkFromTrs()
      .SetControlsValue()
      .ChildrenChangeRow()
      .oPgFrm.Page1.oPag.oBody.Refresh()
      .oPgFrm.Page1.oPag.oBody.nAbsRow=1
      .oPgFrm.Page1.oPag.oBody.nRelRow=1
      EndWith
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CAM_BI'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
