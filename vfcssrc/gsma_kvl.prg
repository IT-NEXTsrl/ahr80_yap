* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kvl                                                        *
*              Variazione listini prezzi                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_218]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-05                                                      *
* Last revis.: 2010-01-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kvl",oParentObject))

* --- Class definition
define class tgsma_kvl as StdForm
  Top    = 15
  Left   = 9

  * --- Standard Properties
  Width  = 739
  Height = 361+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-01-11"
  HelpContextID=219514007
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=76

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  FAM_ARTI_IDX = 0
  VOCIIVA_IDX = 0
  CONTI_IDX = 0
  MAGAZZIN_IDX = 0
  LISTINI_IDX = 0
  INVENTAR_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  MARCHI_IDX = 0
  CLA_RICA_IDX = 0
  cPrg = "gsma_kvl"
  cComment = "Variazione listini prezzi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_CRITER = space(2)
  o_CRITER = space(2)
  w_CODMAG = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPOCON = space(1)
  w_LISTINO = space(5)
  o_LISTINO = space(5)
  w_LIDESCRI = space(40)
  w_SCAGLIA = space(1)
  w_LORNET = space(1)
  w_VALUTA = space(3)
  o_VALUTA = space(3)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_CAOVAL = 0
  w_CAMBIO1 = 0
  w_DATLISIN = ctod('  /  /  ')
  w_DATLISFI = ctod('  /  /  ')
  w_CREALIS = space(1)
  w_AGSCO = space(1)
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_LISTRIFE = space(5)
  w_LIDESRIF = space(40)
  w_SCAGLIA1 = space(1)
  w_CAOVAL1 = 0
  w_LORNET1 = space(1)
  w_VALRIF = space(3)
  o_VALRIF = space(3)
  w_CAMBIO = 0
  w_DATARIFE = ctod('  /  /  ')
  w_ESERCI = space(4)
  w_INNUMINV = space(6)
  w_DATINV = ctod('  /  /  ')
  w_VALNAZ = space(3)
  w_CAMNAZ = 0
  w_CAMBIO2 = 0
  w_RICALC = space(1)
  o_RICALC = space(1)
  w_RICPE1 = 0
  w_MOLTIP = 0
  w_MOLTI2 = 0
  w_RICVA1 = 0
  w_VALUTA = space(3)
  w_ARROT1 = 0
  w_VALORIN = 0
  w_ARROT2 = 0
  w_VALOR2IN = 0
  w_ARROT3 = 0
  w_VALOR3IN = 0
  w_ARROT4 = 0
  w_VALOFIN = 0
  w_TIPO = space(1)
  w_CODARTIN = space(20)
  w_CODARTFI = space(20)
  w_DESART = space(40)
  w_DESART1 = space(40)
  w_CODIVA = space(5)
  w_IVDESCR = space(35)
  w_GRUPMERC = space(5)
  w_CATOMOGE = space(5)
  w_FAMIGLIA = space(5)
  w_GDESCRI = space(35)
  w_CTDESCRI = space(35)
  w_FDESCRI = space(35)
  w_FLSCO = space(1)
  w_DATOBSOL = ctod('  /  /  ')
  w_ORIGSCA = space(1)
  w_MARCA = space(5)
  w_CLARICA = space(5)
  w_MDESCRI = space(35)
  w_FORABIT = space(15)
  w_FODESCRI = space(40)
  w_FORULTC = space(15)
  w_FUDESCRI = space(40)
  w_CLADESCRI = space(35)
  w_TIPUMI = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kvlPag1","gsma_kvl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(2).addobject("oPag","tgsma_kvlPag2","gsma_kvl",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCRITER_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='GRUMERC'
    this.cWorkTables[3]='CATEGOMO'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='VOCIIVA'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='LISTINI'
    this.cWorkTables[9]='INVENTAR'
    this.cWorkTables[10]='AZIENDA'
    this.cWorkTables[11]='ESERCIZI'
    this.cWorkTables[12]='VALUTE'
    this.cWorkTables[13]='MARCHI'
    this.cWorkTables[14]='CLA_RICA'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAR_BVL with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_CRITER=space(2)
      .w_CODMAG=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPOCON=space(1)
      .w_LISTINO=space(5)
      .w_LIDESCRI=space(40)
      .w_SCAGLIA=space(1)
      .w_LORNET=space(1)
      .w_VALUTA=space(3)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_CAOVAL=0
      .w_CAMBIO1=0
      .w_DATLISIN=ctod("  /  /  ")
      .w_DATLISFI=ctod("  /  /  ")
      .w_CREALIS=space(1)
      .w_AGSCO=space(1)
      .w_SCONT1=0
      .w_SCONT2=0
      .w_SCONT3=0
      .w_SCONT4=0
      .w_LISTRIFE=space(5)
      .w_LIDESRIF=space(40)
      .w_SCAGLIA1=space(1)
      .w_CAOVAL1=0
      .w_LORNET1=space(1)
      .w_VALRIF=space(3)
      .w_CAMBIO=0
      .w_DATARIFE=ctod("  /  /  ")
      .w_ESERCI=space(4)
      .w_INNUMINV=space(6)
      .w_DATINV=ctod("  /  /  ")
      .w_VALNAZ=space(3)
      .w_CAMNAZ=0
      .w_CAMBIO2=0
      .w_RICALC=space(1)
      .w_RICPE1=0
      .w_MOLTIP=0
      .w_MOLTI2=0
      .w_RICVA1=0
      .w_VALUTA=space(3)
      .w_ARROT1=0
      .w_VALORIN=0
      .w_ARROT2=0
      .w_VALOR2IN=0
      .w_ARROT3=0
      .w_VALOR3IN=0
      .w_ARROT4=0
      .w_VALOFIN=0
      .w_TIPO=space(1)
      .w_CODARTIN=space(20)
      .w_CODARTFI=space(20)
      .w_DESART=space(40)
      .w_DESART1=space(40)
      .w_CODIVA=space(5)
      .w_IVDESCR=space(35)
      .w_GRUPMERC=space(5)
      .w_CATOMOGE=space(5)
      .w_FAMIGLIA=space(5)
      .w_GDESCRI=space(35)
      .w_CTDESCRI=space(35)
      .w_FDESCRI=space(35)
      .w_FLSCO=space(1)
      .w_DATOBSOL=ctod("  /  /  ")
      .w_ORIGSCA=space(1)
      .w_MARCA=space(5)
      .w_CLARICA=space(5)
      .w_MDESCRI=space(35)
      .w_FORABIT=space(15)
      .w_FODESCRI=space(40)
      .w_FORULTC=space(15)
      .w_FUDESCRI=space(40)
      .w_CLADESCRI=space(35)
      .w_TIPUMI=0
        .w_AZIENDA = i_CODAZI
        .w_CRITER = 'LI'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODMAG))
          .link_1_3('Full')
        endif
        .w_OBTEST = i_datsys
          .DoRTCalc(5,5,.f.)
        .w_TIPOCON = 'F'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_LISTINO))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,11,.f.)
        if not(empty(.w_VALUTA))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,12,.f.)
        .w_CALCPICT = DEFPCUNI(.w_DECTOT)
          .DoRTCalc(14,14,.f.)
        .w_CAMBIO1 = GETCAM(.w_VALUTA, .w_DATARIFE, 7)
          .DoRTCalc(16,17,.f.)
        .w_CREALIS = ''
        .w_AGSCO = 'F'
          .DoRTCalc(20,23,.f.)
        .w_LISTRIFE = .w_LISTINO
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_LISTRIFE))
          .link_1_24('Full')
        endif
        .DoRTCalc(25,29,.f.)
        if not(empty(.w_VALRIF))
          .link_1_29('Full')
        endif
        .w_CAMBIO = GETCAM(.w_VALRIF, .w_DATARIFE, 7)
        .w_DATARIFE = i_datsys
        .w_ESERCI = g_CODESE
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_ESERCI))
          .link_1_35('Full')
        endif
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_INNUMINV))
          .link_1_36('Full')
        endif
        .DoRTCalc(34,35,.f.)
        if not(empty(.w_VALNAZ))
          .link_1_38('Full')
        endif
          .DoRTCalc(36,37,.f.)
        .w_RICALC = 'R'
        .w_RICPE1 = IIF(.w_RICALC='R', .w_RICPE1,0)
        .w_MOLTIP = IIF(.w_RICALC='M', .w_MOLTIP,0)
        .w_MOLTI2 = IIF(.w_RICALC='M', .w_MOLTI2,0)
        .w_RICVA1 = IIF(.w_RICALC$'RM', .w_RICVA1,0)
          .DoRTCalc(43,50,.f.)
        .w_VALOFIN = MAX(.w_VALORIN, .w_VALOR2IN, .w_VALOR3IN)
        .w_TIPO = 'F'
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_CODARTIN))
          .link_2_2('Full')
        endif
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_CODARTFI))
          .link_2_3('Full')
        endif
        .DoRTCalc(55,57,.f.)
        if not(empty(.w_CODIVA))
          .link_2_8('Full')
        endif
        .DoRTCalc(58,59,.f.)
        if not(empty(.w_GRUPMERC))
          .link_2_11('Full')
        endif
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_CATOMOGE))
          .link_2_12('Full')
        endif
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_FAMIGLIA))
          .link_2_13('Full')
        endif
          .DoRTCalc(62,66,.f.)
        .w_ORIGSCA = IIF(LEFT(.w_CRITER,1)='L', 'S', ' ')
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_MARCA))
          .link_2_20('Full')
        endif
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_CLARICA))
          .link_2_21('Full')
        endif
        .DoRTCalc(70,71,.f.)
        if not(empty(.w_FORABIT))
          .link_2_24('Full')
        endif
        .DoRTCalc(72,73,.f.)
        if not(empty(.w_FORULTC))
          .link_2_27('Full')
        endif
          .DoRTCalc(74,75,.f.)
        .w_TIPUMI = IIF(LEFT(.w_CRITER,1)='L' , .w_TIPUMI, 1)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
          .link_1_11('Full')
        .DoRTCalc(12,12,.t.)
        if .o_VALUTA<>.w_VALUTA
            .w_CALCPICT = DEFPCUNI(.w_DECTOT)
        endif
        .DoRTCalc(14,14,.t.)
        if .o_VALUTA<>.w_VALUTA
            .w_CAMBIO1 = GETCAM(.w_VALUTA, .w_DATARIFE, 7)
        endif
        .DoRTCalc(16,18,.t.)
        if .o_LISTINO<>.w_LISTINO.or. .o_CRITER<>.w_CRITER
            .w_AGSCO = 'F'
        endif
        .DoRTCalc(20,23,.t.)
        if .o_LISTINO<>.w_LISTINO
            .w_LISTRIFE = .w_LISTINO
          .link_1_24('Full')
        endif
        .DoRTCalc(25,28,.t.)
          .link_1_29('Full')
        if .o_VALRIF<>.w_VALRIF
            .w_CAMBIO = GETCAM(.w_VALRIF, .w_DATARIFE, 7)
        endif
        .DoRTCalc(31,34,.t.)
          .link_1_38('Full')
        .DoRTCalc(36,38,.t.)
        if .o_RICALC<>.w_RICALC
            .w_RICPE1 = IIF(.w_RICALC='R', .w_RICPE1,0)
        endif
        if .o_RICALC<>.w_RICALC
            .w_MOLTIP = IIF(.w_RICALC='M', .w_MOLTIP,0)
        endif
        if .o_RICALC<>.w_RICALC
            .w_MOLTI2 = IIF(.w_RICALC='M', .w_MOLTI2,0)
        endif
        if .o_RICALC<>.w_RICALC
            .w_RICVA1 = IIF(.w_RICALC$'RM', .w_RICVA1,0)
        endif
        .DoRTCalc(43,50,.t.)
            .w_VALOFIN = MAX(.w_VALORIN, .w_VALOR2IN, .w_VALOR3IN)
        .DoRTCalc(52,66,.t.)
        if .o_CRITER<>.w_CRITER
            .w_ORIGSCA = IIF(LEFT(.w_CRITER,1)='L', 'S', ' ')
        endif
        .DoRTCalc(68,75,.t.)
        if .o_CRITER<>.w_CRITER
            .w_TIPUMI = IIF(LEFT(.w_CRITER,1)='L' , .w_TIPUMI, 1)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODMAG_1_3.enabled = this.oPgFrm.Page1.oPag.oCODMAG_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCAMBIO1_1_15.enabled = this.oPgFrm.Page1.oPag.oCAMBIO1_1_15.mCond()
    this.oPgFrm.Page1.oPag.oAGSCO_1_19.enabled = this.oPgFrm.Page1.oPag.oAGSCO_1_19.mCond()
    this.oPgFrm.Page1.oPag.oSCONT1_1_20.enabled = this.oPgFrm.Page1.oPag.oSCONT1_1_20.mCond()
    this.oPgFrm.Page1.oPag.oSCONT2_1_21.enabled = this.oPgFrm.Page1.oPag.oSCONT2_1_21.mCond()
    this.oPgFrm.Page1.oPag.oSCONT3_1_22.enabled = this.oPgFrm.Page1.oPag.oSCONT3_1_22.mCond()
    this.oPgFrm.Page1.oPag.oSCONT4_1_23.enabled = this.oPgFrm.Page1.oPag.oSCONT4_1_23.mCond()
    this.oPgFrm.Page1.oPag.oLISTRIFE_1_24.enabled = this.oPgFrm.Page1.oPag.oLISTRIFE_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCAMBIO_1_30.enabled = this.oPgFrm.Page1.oPag.oCAMBIO_1_30.mCond()
    this.oPgFrm.Page1.oPag.oDATARIFE_1_31.enabled = this.oPgFrm.Page1.oPag.oDATARIFE_1_31.mCond()
    this.oPgFrm.Page1.oPag.oESERCI_1_35.enabled = this.oPgFrm.Page1.oPag.oESERCI_1_35.mCond()
    this.oPgFrm.Page1.oPag.oINNUMINV_1_36.enabled = this.oPgFrm.Page1.oPag.oINNUMINV_1_36.mCond()
    this.oPgFrm.Page1.oPag.oCAMBIO2_1_40.enabled = this.oPgFrm.Page1.oPag.oCAMBIO2_1_40.mCond()
    this.oPgFrm.Page1.oPag.oRICPE1_1_47.enabled = this.oPgFrm.Page1.oPag.oRICPE1_1_47.mCond()
    this.oPgFrm.Page1.oPag.oMOLTIP_1_48.enabled = this.oPgFrm.Page1.oPag.oMOLTIP_1_48.mCond()
    this.oPgFrm.Page1.oPag.oMOLTI2_1_49.enabled = this.oPgFrm.Page1.oPag.oMOLTI2_1_49.mCond()
    this.oPgFrm.Page1.oPag.oRICVA1_1_50.enabled = this.oPgFrm.Page1.oPag.oRICVA1_1_50.mCond()
    this.oPgFrm.Page1.oPag.oARROT2_1_54.enabled = this.oPgFrm.Page1.oPag.oARROT2_1_54.mCond()
    this.oPgFrm.Page1.oPag.oVALOR2IN_1_55.enabled = this.oPgFrm.Page1.oPag.oVALOR2IN_1_55.mCond()
    this.oPgFrm.Page1.oPag.oARROT3_1_56.enabled = this.oPgFrm.Page1.oPag.oARROT3_1_56.mCond()
    this.oPgFrm.Page1.oPag.oVALOR3IN_1_57.enabled = this.oPgFrm.Page1.oPag.oVALOR3IN_1_57.mCond()
    this.oPgFrm.Page1.oPag.oORIGSCA_1_79.enabled = this.oPgFrm.Page1.oPag.oORIGSCA_1_79.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODMAG_1_3.visible=!this.oPgFrm.Page1.oPag.oCODMAG_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCAMBIO1_1_15.visible=!this.oPgFrm.Page1.oPag.oCAMBIO1_1_15.mHide()
    this.oPgFrm.Page1.oPag.oAGSCO_1_19.visible=!this.oPgFrm.Page1.oPag.oAGSCO_1_19.mHide()
    this.oPgFrm.Page1.oPag.oSCONT1_1_20.visible=!this.oPgFrm.Page1.oPag.oSCONT1_1_20.mHide()
    this.oPgFrm.Page1.oPag.oSCONT2_1_21.visible=!this.oPgFrm.Page1.oPag.oSCONT2_1_21.mHide()
    this.oPgFrm.Page1.oPag.oSCONT3_1_22.visible=!this.oPgFrm.Page1.oPag.oSCONT3_1_22.mHide()
    this.oPgFrm.Page1.oPag.oSCONT4_1_23.visible=!this.oPgFrm.Page1.oPag.oSCONT4_1_23.mHide()
    this.oPgFrm.Page1.oPag.oLISTRIFE_1_24.visible=!this.oPgFrm.Page1.oPag.oLISTRIFE_1_24.mHide()
    this.oPgFrm.Page1.oPag.oLIDESRIF_1_25.visible=!this.oPgFrm.Page1.oPag.oLIDESRIF_1_25.mHide()
    this.oPgFrm.Page1.oPag.oVALRIF_1_29.visible=!this.oPgFrm.Page1.oPag.oVALRIF_1_29.mHide()
    this.oPgFrm.Page1.oPag.oCAMBIO_1_30.visible=!this.oPgFrm.Page1.oPag.oCAMBIO_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDATARIFE_1_31.visible=!this.oPgFrm.Page1.oPag.oDATARIFE_1_31.mHide()
    this.oPgFrm.Page1.oPag.oESERCI_1_35.visible=!this.oPgFrm.Page1.oPag.oESERCI_1_35.mHide()
    this.oPgFrm.Page1.oPag.oINNUMINV_1_36.visible=!this.oPgFrm.Page1.oPag.oINNUMINV_1_36.mHide()
    this.oPgFrm.Page1.oPag.oDATINV_1_37.visible=!this.oPgFrm.Page1.oPag.oDATINV_1_37.mHide()
    this.oPgFrm.Page1.oPag.oVALNAZ_1_38.visible=!this.oPgFrm.Page1.oPag.oVALNAZ_1_38.mHide()
    this.oPgFrm.Page1.oPag.oCAMBIO2_1_40.visible=!this.oPgFrm.Page1.oPag.oCAMBIO2_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    this.oPgFrm.Page1.oPag.oTIPUMI_1_82.visible=!this.oPgFrm.Page1.oPag.oTIPUMI_1_82.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_3'),i_cWhere,'GSAR_AMA',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LISTINO
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTINO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTINO)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS,LSFLSCON,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTINO))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS,LSFLSCON,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTINO)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LISTINO) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTINO_1_7'),i_cWhere,'GSAR_ALI',"Anagrafica listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS,LSFLSCON,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS,LSFLSCON,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTINO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS,LSFLSCON,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTINO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTINO)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS,LSFLSCON,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTINO = NVL(_Link_.LSCODLIS,space(5))
      this.w_LIDESCRI = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALUTA = NVL(_Link_.LSVALLIS,space(3))
      this.w_SCAGLIA = NVL(_Link_.LSQUANTI,space(1))
      this.w_LORNET = NVL(_Link_.LSIVALIS,space(1))
      this.w_FLSCO = NVL(_Link_.LSFLSCON,space(1))
      this.w_DATOBSOL = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_LISTINO = space(5)
      endif
      this.w_LIDESCRI = space(40)
      this.w_VALUTA = space(3)
      this.w_SCAGLIA = space(1)
      this.w_LORNET = space(1)
      this.w_FLSCO = space(1)
      this.w_DATOBSOL = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSOL) OR .w_DATOBSOL>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino da variare errato, obsoleto o inesistente")
        endif
        this.w_LISTINO = space(5)
        this.w_LIDESCRI = space(40)
        this.w_VALUTA = space(3)
        this.w_SCAGLIA = space(1)
        this.w_LORNET = space(1)
        this.w_FLSCO = space(1)
        this.w_DATOBSOL = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTINO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECUNI,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECUNI,0)
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECTOT = 0
      this.w_CAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LISTRIFE
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTRIFE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTRIFE)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTRIFE))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTRIFE)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LISTRIFE) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTRIFE_1_24'),i_cWhere,'GSAR_ALI',"Anagrafica listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTRIFE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTRIFE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTRIFE)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSQUANTI,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTRIFE = NVL(_Link_.LSCODLIS,space(5))
      this.w_LIDESRIF = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALRIF = NVL(_Link_.LSVALLIS,space(3))
      this.w_SCAGLIA1 = NVL(_Link_.LSQUANTI,space(1))
      this.w_LORNET1 = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LISTRIFE = space(5)
      endif
      this.w_LIDESRIF = space(40)
      this.w_VALRIF = space(3)
      this.w_SCAGLIA1 = space(1)
      this.w_LORNET1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTRIFE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALRIF
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALRIF)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALRIF = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL1 = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALRIF = space(3)
      endif
      this.w_CAOVAL1 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCI
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESERCI)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_ESERCI))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESERCI)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESERCI) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESERCI_1_35'),i_cWhere,'GSAR_KES',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCI);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_ESERCI)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCI = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCI = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INNUMINV
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INNUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_ain',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_INNUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERCI);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_ESERCI;
                     ,'INNUMINV',trim(this.w_INNUMINV))
          select INCODESE,INNUMINV,INDATINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INNUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INNUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oINNUMINV_1_36'),i_cWhere,'gsma_ain',"Inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ESERCI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_ESERCI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INNUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_INNUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERCI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESERCI;
                       ,'INNUMINV',this.w_INNUMINV)
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INNUMINV = NVL(_Link_.INNUMINV,space(6))
      this.w_DATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_INNUMINV = space(6)
      endif
      this.w_DATINV = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INNUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_CAMBIO2 = NVL(_Link_.VACAOVAL,0)
      this.w_CAMNAZ = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_CAMBIO2 = 0
      this.w_CAMNAZ = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARTIN
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODARTIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODARTIN))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODARTIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODARTIN)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODARTIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODARTIN_2_2'),i_cWhere,'GSMA_BZA',"Elenco articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODARTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODARTIN)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTIN = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_CODARTFI)) OR  (.w_CODARTIN<=.w_CODARTFI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � superiore al codice finale oppure obsoleto")
        endif
        this.w_CODARTIN = space(20)
        this.w_DESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARTFI
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODARTFI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODARTFI))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTFI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODARTFI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODARTFI)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODARTFI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODARTFI_2_3'),i_cWhere,'GSMA_BZA',"Elenco articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODARTFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODARTFI)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTFI = NVL(_Link_.ARCODART,space(20))
      this.w_DESART1 = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTFI = space(20)
      endif
      this.w_DESART1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CODARTFI>=.w_CODARTIN) or (empty(.w_CODARTIN))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � superiore al codice finale oppure obsoleto")
        endif
        this.w_CODARTFI = space(20)
        this.w_DESART1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_CODIVA))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_CODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCODIVA_2_8'),i_cWhere,'GSAR_AIV',"Voci IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_IVDESCR = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_IVDESCR = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODIVA = space(5)
        this.w_IVDESCR = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPMERC
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPMERC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUPMERC)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUPMERC))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPMERC)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPMERC) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUPMERC_2_11'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPMERC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUPMERC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUPMERC)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPMERC = NVL(_Link_.GMCODICE,space(5))
      this.w_GDESCRI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPMERC = space(5)
      endif
      this.w_GDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPMERC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOMOGE
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOMOGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOMOGE)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOMOGE))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOMOGE)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATOMOGE) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOMOGE_2_12'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOMOGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOMOGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOMOGE)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOMOGE = NVL(_Link_.OMCODICE,space(5))
      this.w_CTDESCRI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATOMOGE = space(5)
      endif
      this.w_CTDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOMOGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMIGLIA
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMIGLIA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMIGLIA)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMIGLIA))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMIGLIA)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMIGLIA) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMIGLIA_2_13'),i_cWhere,'GSAR_AFA',"Famiglia articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMIGLIA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMIGLIA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMIGLIA)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMIGLIA = NVL(_Link_.FACODICE,space(5))
      this.w_FDESCRI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMIGLIA = space(5)
      endif
      this.w_FDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMIGLIA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARCA
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARCA)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARCA))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARCA)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_MARCA)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_MARCA)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MARCA) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARCA_2_20'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARCA)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARCA = NVL(_Link_.MACODICE,space(5))
      this.w_MDESCRI = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARCA = space(5)
      endif
      this.w_MDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLARICA
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RICA_IDX,3]
    i_lTable = "CLA_RICA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2], .t., this.CLA_RICA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLARICA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACR',True,'CLA_RICA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRCODICE like "+cp_ToStrODBC(trim(this.w_CLARICA)+"%");

          i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRCODICE',trim(this.w_CLARICA))
          select CRCODICE,CRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLARICA)==trim(_Link_.CRCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CRDESCRI like "+cp_ToStrODBC(trim(this.w_CLARICA)+"%");

            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CRDESCRI like "+cp_ToStr(trim(this.w_CLARICA)+"%");

            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLARICA) and !this.bDontReportError
            deferred_cp_zoom('CLA_RICA','*','CRCODICE',cp_AbsName(oSource.parent,'oCLARICA_2_21'),i_cWhere,'GSAR_ACR',"Classi di ricarico dei listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',oSource.xKey(1))
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLARICA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(this.w_CLARICA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',this.w_CLARICA)
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLARICA = NVL(_Link_.CRCODICE,space(5))
      this.w_CLADESCRI = NVL(_Link_.CRDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CLARICA = space(5)
      endif
      this.w_CLADESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])+'\'+cp_ToStr(_Link_.CRCODICE,1)
      cp_ShowWarn(i_cKey,this.CLA_RICA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLARICA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORABIT
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORABIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORABIT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_FORABIT))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORABIT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORABIT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORABIT)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORABIT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORABIT_2_24'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORABIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORABIT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_FORABIT)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORABIT = NVL(_Link_.ANCODICE,space(15))
      this.w_FODESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORABIT = space(15)
      endif
      this.w_FODESCRI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        endif
        this.w_FORABIT = space(15)
        this.w_FODESCRI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORABIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORULTC
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORULTC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORULTC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_FORULTC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORULTC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORULTC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORULTC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORULTC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORULTC_2_27'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORULTC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORULTC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_FORULTC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORULTC = NVL(_Link_.ANCODICE,space(15))
      this.w_FUDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORULTC = space(15)
      endif
      this.w_FUDESCRI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        endif
        this.w_FORULTC = space(15)
        this.w_FUDESCRI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORULTC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCRITER_1_2.RadioValue()==this.w_CRITER)
      this.oPgFrm.Page1.oPag.oCRITER_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_3.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_3.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oLISTINO_1_7.value==this.w_LISTINO)
      this.oPgFrm.Page1.oPag.oLISTINO_1_7.value=this.w_LISTINO
    endif
    if not(this.oPgFrm.Page1.oPag.oLIDESCRI_1_8.value==this.w_LIDESCRI)
      this.oPgFrm.Page1.oPag.oLIDESCRI_1_8.value=this.w_LIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_11.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_11.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO1_1_15.value==this.w_CAMBIO1)
      this.oPgFrm.Page1.oPag.oCAMBIO1_1_15.value=this.w_CAMBIO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATLISIN_1_16.value==this.w_DATLISIN)
      this.oPgFrm.Page1.oPag.oDATLISIN_1_16.value=this.w_DATLISIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATLISFI_1_17.value==this.w_DATLISFI)
      this.oPgFrm.Page1.oPag.oDATLISFI_1_17.value=this.w_DATLISFI
    endif
    if not(this.oPgFrm.Page1.oPag.oCREALIS_1_18.RadioValue()==this.w_CREALIS)
      this.oPgFrm.Page1.oPag.oCREALIS_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGSCO_1_19.RadioValue()==this.w_AGSCO)
      this.oPgFrm.Page1.oPag.oAGSCO_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCONT1_1_20.value==this.w_SCONT1)
      this.oPgFrm.Page1.oPag.oSCONT1_1_20.value=this.w_SCONT1
    endif
    if not(this.oPgFrm.Page1.oPag.oSCONT2_1_21.value==this.w_SCONT2)
      this.oPgFrm.Page1.oPag.oSCONT2_1_21.value=this.w_SCONT2
    endif
    if not(this.oPgFrm.Page1.oPag.oSCONT3_1_22.value==this.w_SCONT3)
      this.oPgFrm.Page1.oPag.oSCONT3_1_22.value=this.w_SCONT3
    endif
    if not(this.oPgFrm.Page1.oPag.oSCONT4_1_23.value==this.w_SCONT4)
      this.oPgFrm.Page1.oPag.oSCONT4_1_23.value=this.w_SCONT4
    endif
    if not(this.oPgFrm.Page1.oPag.oLISTRIFE_1_24.value==this.w_LISTRIFE)
      this.oPgFrm.Page1.oPag.oLISTRIFE_1_24.value=this.w_LISTRIFE
    endif
    if not(this.oPgFrm.Page1.oPag.oLIDESRIF_1_25.value==this.w_LIDESRIF)
      this.oPgFrm.Page1.oPag.oLIDESRIF_1_25.value=this.w_LIDESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oVALRIF_1_29.value==this.w_VALRIF)
      this.oPgFrm.Page1.oPag.oVALRIF_1_29.value=this.w_VALRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO_1_30.value==this.w_CAMBIO)
      this.oPgFrm.Page1.oPag.oCAMBIO_1_30.value=this.w_CAMBIO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATARIFE_1_31.value==this.w_DATARIFE)
      this.oPgFrm.Page1.oPag.oDATARIFE_1_31.value=this.w_DATARIFE
    endif
    if not(this.oPgFrm.Page1.oPag.oESERCI_1_35.value==this.w_ESERCI)
      this.oPgFrm.Page1.oPag.oESERCI_1_35.value=this.w_ESERCI
    endif
    if not(this.oPgFrm.Page1.oPag.oINNUMINV_1_36.value==this.w_INNUMINV)
      this.oPgFrm.Page1.oPag.oINNUMINV_1_36.value=this.w_INNUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINV_1_37.value==this.w_DATINV)
      this.oPgFrm.Page1.oPag.oDATINV_1_37.value=this.w_DATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oVALNAZ_1_38.value==this.w_VALNAZ)
      this.oPgFrm.Page1.oPag.oVALNAZ_1_38.value=this.w_VALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO2_1_40.value==this.w_CAMBIO2)
      this.oPgFrm.Page1.oPag.oCAMBIO2_1_40.value=this.w_CAMBIO2
    endif
    if not(this.oPgFrm.Page1.oPag.oRICALC_1_45.RadioValue()==this.w_RICALC)
      this.oPgFrm.Page1.oPag.oRICALC_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRICPE1_1_47.value==this.w_RICPE1)
      this.oPgFrm.Page1.oPag.oRICPE1_1_47.value=this.w_RICPE1
    endif
    if not(this.oPgFrm.Page1.oPag.oMOLTIP_1_48.value==this.w_MOLTIP)
      this.oPgFrm.Page1.oPag.oMOLTIP_1_48.value=this.w_MOLTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oMOLTI2_1_49.value==this.w_MOLTI2)
      this.oPgFrm.Page1.oPag.oMOLTI2_1_49.value=this.w_MOLTI2
    endif
    if not(this.oPgFrm.Page1.oPag.oRICVA1_1_50.value==this.w_RICVA1)
      this.oPgFrm.Page1.oPag.oRICVA1_1_50.value=this.w_RICVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_51.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_51.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT1_1_52.value==this.w_ARROT1)
      this.oPgFrm.Page1.oPag.oARROT1_1_52.value=this.w_ARROT1
    endif
    if not(this.oPgFrm.Page1.oPag.oVALORIN_1_53.value==this.w_VALORIN)
      this.oPgFrm.Page1.oPag.oVALORIN_1_53.value=this.w_VALORIN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT2_1_54.value==this.w_ARROT2)
      this.oPgFrm.Page1.oPag.oARROT2_1_54.value=this.w_ARROT2
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOR2IN_1_55.value==this.w_VALOR2IN)
      this.oPgFrm.Page1.oPag.oVALOR2IN_1_55.value=this.w_VALOR2IN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT3_1_56.value==this.w_ARROT3)
      this.oPgFrm.Page1.oPag.oARROT3_1_56.value=this.w_ARROT3
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOR3IN_1_57.value==this.w_VALOR3IN)
      this.oPgFrm.Page1.oPag.oVALOR3IN_1_57.value=this.w_VALOR3IN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT4_1_58.value==this.w_ARROT4)
      this.oPgFrm.Page1.oPag.oARROT4_1_58.value=this.w_ARROT4
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOFIN_1_74.value==this.w_VALOFIN)
      this.oPgFrm.Page1.oPag.oVALOFIN_1_74.value=this.w_VALOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODARTIN_2_2.value==this.w_CODARTIN)
      this.oPgFrm.Page2.oPag.oCODARTIN_2_2.value=this.w_CODARTIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODARTFI_2_3.value==this.w_CODARTFI)
      this.oPgFrm.Page2.oPag.oCODARTFI_2_3.value=this.w_CODARTFI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART_2_6.value==this.w_DESART)
      this.oPgFrm.Page2.oPag.oDESART_2_6.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART1_2_7.value==this.w_DESART1)
      this.oPgFrm.Page2.oPag.oDESART1_2_7.value=this.w_DESART1
    endif
    if not(this.oPgFrm.Page2.oPag.oCODIVA_2_8.value==this.w_CODIVA)
      this.oPgFrm.Page2.oPag.oCODIVA_2_8.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oIVDESCR_2_10.value==this.w_IVDESCR)
      this.oPgFrm.Page2.oPag.oIVDESCR_2_10.value=this.w_IVDESCR
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUPMERC_2_11.value==this.w_GRUPMERC)
      this.oPgFrm.Page2.oPag.oGRUPMERC_2_11.value=this.w_GRUPMERC
    endif
    if not(this.oPgFrm.Page2.oPag.oCATOMOGE_2_12.value==this.w_CATOMOGE)
      this.oPgFrm.Page2.oPag.oCATOMOGE_2_12.value=this.w_CATOMOGE
    endif
    if not(this.oPgFrm.Page2.oPag.oFAMIGLIA_2_13.value==this.w_FAMIGLIA)
      this.oPgFrm.Page2.oPag.oFAMIGLIA_2_13.value=this.w_FAMIGLIA
    endif
    if not(this.oPgFrm.Page2.oPag.oGDESCRI_2_17.value==this.w_GDESCRI)
      this.oPgFrm.Page2.oPag.oGDESCRI_2_17.value=this.w_GDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCTDESCRI_2_18.value==this.w_CTDESCRI)
      this.oPgFrm.Page2.oPag.oCTDESCRI_2_18.value=this.w_CTDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oFDESCRI_2_19.value==this.w_FDESCRI)
      this.oPgFrm.Page2.oPag.oFDESCRI_2_19.value=this.w_FDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oORIGSCA_1_79.RadioValue()==this.w_ORIGSCA)
      this.oPgFrm.Page1.oPag.oORIGSCA_1_79.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMARCA_2_20.value==this.w_MARCA)
      this.oPgFrm.Page2.oPag.oMARCA_2_20.value=this.w_MARCA
    endif
    if not(this.oPgFrm.Page2.oPag.oCLARICA_2_21.value==this.w_CLARICA)
      this.oPgFrm.Page2.oPag.oCLARICA_2_21.value=this.w_CLARICA
    endif
    if not(this.oPgFrm.Page2.oPag.oMDESCRI_2_22.value==this.w_MDESCRI)
      this.oPgFrm.Page2.oPag.oMDESCRI_2_22.value=this.w_MDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oFORABIT_2_24.value==this.w_FORABIT)
      this.oPgFrm.Page2.oPag.oFORABIT_2_24.value=this.w_FORABIT
    endif
    if not(this.oPgFrm.Page2.oPag.oFODESCRI_2_26.value==this.w_FODESCRI)
      this.oPgFrm.Page2.oPag.oFODESCRI_2_26.value=this.w_FODESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oFORULTC_2_27.value==this.w_FORULTC)
      this.oPgFrm.Page2.oPag.oFORULTC_2_27.value=this.w_FORULTC
    endif
    if not(this.oPgFrm.Page2.oPag.oFUDESCRI_2_29.value==this.w_FUDESCRI)
      this.oPgFrm.Page2.oPag.oFUDESCRI_2_29.value=this.w_FUDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCLADESCRI_2_30.value==this.w_CLADESCRI)
      this.oPgFrm.Page2.oPag.oCLADESCRI_2_30.value=this.w_CLADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPUMI_1_82.RadioValue()==this.w_TIPUMI)
      this.oPgFrm.Page1.oPag.oTIPUMI_1_82.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODMAG))  and not(LEFT(.w_CRITER,1)<>'U')  and (LEFT(.w_CRITER,1)='U')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_LISTINO)) or not((EMPTY(.w_DATOBSOL) OR .w_DATOBSOL>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLISTINO_1_7.SetFocus()
            i_bnoObbl = !empty(.w_LISTINO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino da variare errato, obsoleto o inesistente")
          case   (empty(.w_CAMBIO1))  and not(EMPTY(.w_VALUTA) OR .w_CAOVAL<>0)  and (.w_CAOVAL=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMBIO1_1_15.SetFocus()
            i_bnoObbl = !empty(.w_CAMBIO1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
          case   ((empty(.w_DATLISIN)) or not(((empty(.w_DATLISFI)) OR  (.w_DATLISIN<=.w_DATLISFI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATLISIN_1_16.SetFocus()
            i_bnoObbl = !empty(.w_DATLISIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio validit� � maggiore della data finale")
          case   ((empty(.w_DATLISFI)) or not(((.w_DATLISFI>=.w_DATLISIN) or (empty(.w_DATLISIN)))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATLISFI_1_17.SetFocus()
            i_bnoObbl = !empty(.w_DATLISFI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio validit� � maggiore della data finale")
          case   (empty(.w_LISTRIFE))  and not(LEFT(.w_CRITER,1)<>'L')  and (LEFT(.w_CRITER,1)='L')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLISTRIFE_1_24.SetFocus()
            i_bnoObbl = !empty(.w_LISTRIFE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino di riferimento errato o inesistente")
          case   (empty(.w_CAMBIO))  and not(LEFT(.w_CRITER,1)<>'L' OR .w_CAOVAL1<>0 OR EMPTY(.w_VALRIF))  and (.w_CAOVAL1=0 AND LEFT(.w_CRITER,1)='L' AND NOT EMPTY(.w_LISTRIFE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMBIO_1_30.SetFocus()
            i_bnoObbl = !empty(.w_CAMBIO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
          case   (empty(.w_DATARIFE))  and not(LEFT(.w_CRITER,1)<>'L')  and (LEFT(.w_CRITER,1)='L')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATARIFE_1_31.SetFocus()
            i_bnoObbl = !empty(.w_DATARIFE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_INNUMINV))  and not(LEFT(.w_CRITER,1)<>'I')  and (LEFT(.w_CRITER,1)='I')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINNUMINV_1_36.SetFocus()
            i_bnoObbl = !empty(.w_INNUMINV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VALOR2IN>.w_VALORIN OR .w_VALOR2IN=0)  and (.w_VALORIN<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALOR2IN_1_55.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_VALOR3IN>.w_VALORIN AND .w_VALOR3IN>.w_VALOR2IN) OR .w_VALOR3IN=0)  and (.w_VALORIN<>0 AND .w_VALOR2IN<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALOR3IN_1_57.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_CODARTFI)) OR  (.w_CODARTIN<=.w_CODARTFI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODARTIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODARTIN_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � superiore al codice finale oppure obsoleto")
          case   not(((.w_CODARTFI>=.w_CODARTIN) or (empty(.w_CODARTIN))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODARTFI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODARTFI_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � superiore al codice finale oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODIVA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODIVA_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_FORABIT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFORABIT_2_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Fornitore inesistente o obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_FORULTC))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFORULTC_2_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Fornitore inesistente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CRITER = this.w_CRITER
    this.o_LISTINO = this.w_LISTINO
    this.o_VALUTA = this.w_VALUTA
    this.o_VALRIF = this.w_VALRIF
    this.o_RICALC = this.w_RICALC
    return

enddefine

* --- Define pages as container
define class tgsma_kvlPag1 as StdContainer
  Width  = 735
  height = 363
  stdWidth  = 735
  stdheight = 363
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCRITER_1_2 as StdCombo with uid="ZEDDEXLZRK",rtseq=2,rtrep=.f.,left=158,top=16,width=206,height=21;
    , ToolTipText = "Criterio di aggiornamento dei listini";
    , HelpContextID = 205624538;
    , cFormVar="w_CRITER",RowSource=""+"Prezzo di listino,"+"Prezzo di listino al netto degli sconti,"+"Ultimo costo,"+"Costo medio ponderato annuo,"+"Costo medio ponderato periodo,"+"LIFO continuo,"+"LIFO scatti,"+"FIFO,"+"Costo standard,"+"Ultimo costo dei saldi,"+"Ultimo prezzo dei saldi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCRITER_1_2.RadioValue()
    return(iif(this.value =1,'LI',;
    iif(this.value =2,'LN',;
    iif(this.value =3,'IU',;
    iif(this.value =4,'IA',;
    iif(this.value =5,'IP',;
    iif(this.value =6,'IL',;
    iif(this.value =7,'IS',;
    iif(this.value =8,'IF',;
    iif(this.value =9,'CS',;
    iif(this.value =10,'UC',;
    iif(this.value =11,'UP',;
    space(2)))))))))))))
  endfunc
  func oCRITER_1_2.GetRadio()
    this.Parent.oContained.w_CRITER = this.RadioValue()
    return .t.
  endfunc

  func oCRITER_1_2.SetRadio()
    this.Parent.oContained.w_CRITER=trim(this.Parent.oContained.w_CRITER)
    this.value = ;
      iif(this.Parent.oContained.w_CRITER=='LI',1,;
      iif(this.Parent.oContained.w_CRITER=='LN',2,;
      iif(this.Parent.oContained.w_CRITER=='IU',3,;
      iif(this.Parent.oContained.w_CRITER=='IA',4,;
      iif(this.Parent.oContained.w_CRITER=='IP',5,;
      iif(this.Parent.oContained.w_CRITER=='IL',6,;
      iif(this.Parent.oContained.w_CRITER=='IS',7,;
      iif(this.Parent.oContained.w_CRITER=='IF',8,;
      iif(this.Parent.oContained.w_CRITER=='CS',9,;
      iif(this.Parent.oContained.w_CRITER=='UC',10,;
      iif(this.Parent.oContained.w_CRITER=='UP',11,;
      0)))))))))))
  endfunc

  add object oCODMAG_1_3 as StdField with uid="ABPLEXRPRZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di selezione",;
    HelpContextID = 126412762,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=544, Top=16, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)='U')
    endwith
   endif
  endfunc

  func oCODMAG_1_3.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'U')
    endwith
  endfunc

  func oCODMAG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"MAGAZZINI",'',this.parent.oContained
  endproc
  proc oCODMAG_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oLISTINO_1_7 as StdField with uid="YNJAORDGVJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LISTINO", cQueryName = "LISTINO",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino da variare errato, obsoleto o inesistente",;
    ToolTipText = "Codice di listino da variare",;
    HelpContextID = 268370614,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=8, Top=73, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTINO"

  func oLISTINO_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTINO_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTINO_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTINO_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Anagrafica listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oLISTINO_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LISTINO
     i_obj.ecpSave()
  endproc

  add object oLIDESCRI_1_8 as StdField with uid="VHVFQRRGPI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_LIDESCRI", cQueryName = "LIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 175172865,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=72, Top=73, InputMask=replicate('X',40)

  add object oVALUTA_1_11 as StdField with uid="CKGQKFAVHW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta del listino da aggiornare",;
    HelpContextID = 206599338,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=327, Top=73, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCAMBIO1_1_15 as StdField with uid="DONDGXBPVQ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CAMBIO1", cQueryName = "CAMBIO1",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta selezionata",;
    HelpContextID = 252929498,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=370, Top=73

  func oCAMBIO1_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0)
    endwith
   endif
  endfunc

  func oCAMBIO1_1_15.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_VALUTA) OR .w_CAOVAL<>0)
    endwith
  endfunc

  add object oDATLISIN_1_16 as StdField with uid="PJPJOFXRWU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DATLISIN", cQueryName = "DATLISIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio validit� � maggiore della data finale",;
    ToolTipText = "Data di inizio validit� listino",;
    HelpContextID = 83298948,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=549, Top=73

  func oDATLISIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_DATLISFI)) OR  (.w_DATLISIN<=.w_DATLISFI)))
    endwith
    return bRes
  endfunc

  add object oDATLISFI_1_17 as StdField with uid="NTEXDBNRRY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DATLISFI", cQueryName = "DATLISFI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio validit� � maggiore della data finale",;
    ToolTipText = "Data di fine validit� listino",;
    HelpContextID = 185136513,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=652, Top=73

  func oDATLISFI_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_DATLISFI>=.w_DATLISIN) or (empty(.w_DATLISIN))))
    endwith
    return bRes
  endfunc

  add object oCREALIS_1_18 as StdCheck with uid="OBOVIFZAZZ",rtseq=18,rtrep=.f.,left=10, top=100, caption="Solo esistenti",;
    ToolTipText = "Solo prezzi presenti in listino da aggiornare",;
    HelpContextID = 82105562,;
    cFormVar="w_CREALIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCREALIS_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCREALIS_1_18.GetRadio()
    this.Parent.oContained.w_CREALIS = this.RadioValue()
    return .t.
  endfunc

  func oCREALIS_1_18.SetRadio()
    this.Parent.oContained.w_CREALIS=trim(this.Parent.oContained.w_CREALIS)
    this.value = ;
      iif(this.Parent.oContained.w_CREALIS=='S',1,;
      0)
  endfunc


  add object oAGSCO_1_19 as StdCombo with uid="FMACTGZMDB",rtseq=19,rtrep=.f.,left=118,top=99,width=100,height=21;
    , ToolTipText = "Criterio di inserimento sconti";
    , HelpContextID = 38666246;
    , cFormVar="w_AGSCO",RowSource=""+"Forza sconti,"+"Copia sconti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAGSCO_1_19.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oAGSCO_1_19.GetRadio()
    this.Parent.oContained.w_AGSCO = this.RadioValue()
    return .t.
  endfunc

  func oAGSCO_1_19.SetRadio()
    this.Parent.oContained.w_AGSCO=trim(this.Parent.oContained.w_AGSCO)
    this.value = ;
      iif(this.Parent.oContained.w_AGSCO=='F',1,;
      iif(this.Parent.oContained.w_AGSCO=='C',2,;
      0))
  endfunc

  func oAGSCO_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)='L')
    endwith
   endif
  endfunc

  func oAGSCO_1_19.mHide()
    with this.Parent.oContained
      return (.w_FLSCO<>'S')
    endwith
  endfunc

  add object oSCONT1_1_20 as StdField with uid="BZKFVCKZVR",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SCONT1", cQueryName = "SCONT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^ Maggiorazione (se positiva) o sconto (se negativa) appicata allo scaglione",;
    HelpContextID = 61390118,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=497, Top=99, cSayPict='"999.99"', cGetPict='"999.99"'

  func oSCONT1_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_NUMSCO>0)
    endwith
   endif
  endfunc

  func oSCONT1_1_20.mHide()
    with this.Parent.oContained
      return (.w_FLSCO<>'S' OR .w_AGSCO<>'F')
    endwith
  endfunc

  add object oSCONT2_1_21 as StdField with uid="IESZUBQZPD",rtseq=21,rtrep=.f.,;
    cFormVar = "w_SCONT2", cQueryName = "SCONT2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^ Maggiorazione (se positiva) o sconto (se negativa) appicata allo scaglione",;
    HelpContextID = 78167334,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=556, Top=99, cSayPict='"999.99"', cGetPict='"999.99"'

  func oSCONT2_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_NUMSCO>1)
    endwith
   endif
  endfunc

  func oSCONT2_1_21.mHide()
    with this.Parent.oContained
      return ( .w_FLSCO<>'S' OR .w_AGSCO<>'F')
    endwith
  endfunc

  add object oSCONT3_1_22 as StdField with uid="RNZDEXXRRX",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SCONT3", cQueryName = "SCONT3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "3^ Maggiorazione (se positiva) o sconto (se negativa) appicata allo scaglione",;
    HelpContextID = 94944550,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=617, Top=99, cSayPict='"999.99"', cGetPict='"999.99"'

  func oSCONT3_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_NUMSCO>2)
    endwith
   endif
  endfunc

  func oSCONT3_1_22.mHide()
    with this.Parent.oContained
      return (.w_FLSCO<>'S' OR .w_AGSCO<>'F')
    endwith
  endfunc

  add object oSCONT4_1_23 as StdField with uid="SUDPZZOYAC",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SCONT4", cQueryName = "SCONT4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "4^ Maggiorazione (se positiva) o sconto (se negativa) appicata allo scaglione",;
    HelpContextID = 111721766,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=676, Top=99, cSayPict='"999.99"', cGetPict='"999.99"'

  func oSCONT4_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_NUMSCO>3)
    endwith
   endif
  endfunc

  func oSCONT4_1_23.mHide()
    with this.Parent.oContained
      return (.w_FLSCO<>'S' OR .w_AGSCO<>'F')
    endwith
  endfunc

  add object oLISTRIFE_1_24 as StdField with uid="RKRXOVLQTW",rtseq=24,rtrep=.f.,;
    cFormVar = "w_LISTRIFE", cQueryName = "LISTRIFE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino di riferimento errato o inesistente",;
    ToolTipText = "Codice di listino di riferimento",;
    HelpContextID = 74513669,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=8, Top=151, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTRIFE"

  func oLISTRIFE_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)='L')
    endwith
   endif
  endfunc

  func oLISTRIFE_1_24.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'L')
    endwith
  endfunc

  func oLISTRIFE_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTRIFE_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTRIFE_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTRIFE_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Anagrafica listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oLISTRIFE_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LISTRIFE
     i_obj.ecpSave()
  endproc

  add object oLIDESRIF_1_25 as StdField with uid="AFMROETALJ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_LIDESRIF", cQueryName = "LIDESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 76485372,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=72, Top=151, InputMask=replicate('X',40)

  func oLIDESRIF_1_25.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'L')
    endwith
  endfunc

  add object oVALRIF_1_29 as StdField with uid="JOBUYIOPHH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_VALRIF", cQueryName = "VALRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 134444202,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=328, Top=151, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALRIF"

  func oVALRIF_1_29.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'L')
    endwith
  endfunc

  func oVALRIF_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCAMBIO_1_30 as StdField with uid="UILQYDKBHS",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CAMBIO", cQueryName = "CAMBIO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta selezionata",;
    HelpContextID = 252929498,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=370, Top=151

  func oCAMBIO_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL1=0 AND LEFT(.w_CRITER,1)='L' AND NOT EMPTY(.w_LISTRIFE))
    endwith
   endif
  endfunc

  func oCAMBIO_1_30.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'L' OR .w_CAOVAL1<>0 OR EMPTY(.w_VALRIF))
    endwith
  endfunc

  add object oDATARIFE_1_31 as StdField with uid="ZOBECVCLVX",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DATARIFE", cQueryName = "DATARIFE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di selezione del listino di riferimento",;
    HelpContextID = 75756933,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=652, Top=151

  func oDATARIFE_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)='L')
    endwith
   endif
  endfunc

  func oDATARIFE_1_31.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'L')
    endwith
  endfunc

  add object oESERCI_1_35 as StdField with uid="RUJJMDHBYY",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ESERCI", cQueryName = "ESERCI",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio relativo all'inventario da considerare come valorizzazione iniziale",;
    HelpContextID = 90428346,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=62, Top=151, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERCI"

  func oESERCI_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)='I')
    endwith
   endif
  endfunc

  func oESERCI_1_35.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'I')
    endwith
  endfunc

  func oESERCI_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
      if .not. empty(.w_INNUMINV)
        bRes2=.link_1_36('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oESERCI_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESERCI_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESERCI_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Esercizi",'',this.parent.oContained
  endproc
  proc oESERCI_1_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_ESERCI
     i_obj.ecpSave()
  endproc

  add object oINNUMINV_1_36 as StdField with uid="HAHVFRFFYG",rtseq=33,rtrep=.f.,;
    cFormVar = "w_INNUMINV", cQueryName = "INNUMINV",nZero=6,;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'inventario da visualizzare",;
    HelpContextID = 188725212,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=175, Top=151, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_ESERCI", oKey_2_1="INNUMINV", oKey_2_2="this.w_INNUMINV"

  func oINNUMINV_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)='I')
    endwith
   endif
  endfunc

  func oINNUMINV_1_36.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'I')
    endwith
  endfunc

  func oINNUMINV_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oINNUMINV_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINNUMINV_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_ESERCI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_ESERCI)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oINNUMINV_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_ain',"Inventari",'',this.parent.oContained
  endproc
  proc oINNUMINV_1_36.mZoomOnZoom
    local i_obj
    i_obj=gsma_ain()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_ESERCI
     i_obj.w_INNUMINV=this.parent.oContained.w_INNUMINV
     i_obj.ecpSave()
  endproc

  add object oDATINV_1_37 as StdField with uid="SMWYJSHKVX",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DATINV", cQueryName = "DATINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fino alla quale sono stati considerati i movimenti dell'inventario prec.",;
    HelpContextID = 129758666,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=549, Top=151

  func oDATINV_1_37.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'I')
    endwith
  endfunc

  add object oVALNAZ_1_38 as StdField with uid="YNHZTIDZUD",rtseq=35,rtrep=.f.,;
    cFormVar = "w_VALNAZ", cQueryName = "VALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 75986090,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=328, Top=151, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALNAZ"

  func oVALNAZ_1_38.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'I')
    endwith
  endfunc

  func oVALNAZ_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCAMBIO2_1_40 as StdField with uid="JQSWZFKFYT",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CAMBIO2", cQueryName = "CAMBIO2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 252929498,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=370, Top=151

  func oCAMBIO2_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)='I' AND NOT EMPTY(.w_INNUMINV) AND .w_CAMNAZ=0)
    endwith
   endif
  endfunc

  func oCAMBIO2_1_40.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'I' OR .w_CAMNAZ<>0 OR EMPTY(.w_VALNAZ))
    endwith
  endfunc


  add object oRICALC_1_45 as StdCombo with uid="LEFNLMRUOT",rtseq=38,rtrep=.f.,left=134,top=208,width=202,height=21;
    , ToolTipText = "Parametro di ricalcolo selezionato (classe di ricalcolo / calcolato/ricarico prezzi articolo per listino)";
    , HelpContextID = 182779114;
    , cFormVar="w_RICALC",RowSource=""+"Classe di ricarico,"+"Ricarico a percentuale fissa,"+"Ricarico con moltiplicatore,"+"Ricarico prezzi articolo,"+"Vendita imposta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRICALC_1_45.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    iif(this.value =3,'M',;
    iif(this.value =4,'P',;
    iif(this.value =5,'V',;
    space(1)))))))
  endfunc
  func oRICALC_1_45.GetRadio()
    this.Parent.oContained.w_RICALC = this.RadioValue()
    return .t.
  endfunc

  func oRICALC_1_45.SetRadio()
    this.Parent.oContained.w_RICALC=trim(this.Parent.oContained.w_RICALC)
    this.value = ;
      iif(this.Parent.oContained.w_RICALC=='C',1,;
      iif(this.Parent.oContained.w_RICALC=='R',2,;
      iif(this.Parent.oContained.w_RICALC=='M',3,;
      iif(this.Parent.oContained.w_RICALC=='P',4,;
      iif(this.Parent.oContained.w_RICALC=='V',5,;
      0)))))
  endfunc

  add object oRICPE1_1_47 as StdField with uid="JCWLSRROBT",rtseq=39,rtrep=.f.,;
    cFormVar = "w_RICPE1", cQueryName = "RICPE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di ricalcolo del listino",;
    HelpContextID = 45744918,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=134, Top=235, cSayPict='"999.99"', cGetPict='"999.99"'

  func oRICPE1_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RICALC='R')
    endwith
   endif
  endfunc

  add object oMOLTIP_1_48 as StdField with uid="YOPWLFTTXG",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MOLTIP", cQueryName = "MOLTIP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1� moltiplicatore",;
    HelpContextID = 234972986,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=134, Top=262, cSayPict='"999999999999999.99"', cGetPict='"999999999999999.99"'

  func oMOLTIP_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RICALC='M')
    endwith
   endif
  endfunc

  add object oMOLTI2_1_49 as StdField with uid="YIEZTCHXVX",rtseq=41,rtrep=.f.,;
    cFormVar = "w_MOLTI2", cQueryName = "MOLTI2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2� moltiplicatore:",;
    HelpContextID = 67016902,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=134, Top=289, cSayPict='"999999999999999.99"', cGetPict='"999999999999999.99"'

  func oMOLTI2_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RICALC='M' AND .w_MOLTIP<>0)
    endwith
   endif
  endfunc

  add object oRICVA1_1_50 as StdField with uid="RAFGTLVTMZ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_RICVA1", cQueryName = "RICVA1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ricalcolo in valore del listino",;
    HelpContextID = 41943830,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=134, Top=315, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oRICVA1_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RICALC$'RM')
    endwith
   endif
  endfunc

  add object oVALUTA_1_51 as StdField with uid="NFSILTCDYY",rtseq=43,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 206599338,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=267, Top=315, InputMask=replicate('X',3)

  add object oARROT1_1_52 as StdField with uid="RVCKWZFISF",rtseq=44,rtrep=.f.,;
    cFormVar = "w_ARROT1", cQueryName = "ARROT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 61471494,;
   bGlobalFont=.t.,;
    Height=21, Width=117, Left=341, Top=208, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  add object oVALORIN_1_53 as StdField with uid="MHNGXGXYBX",rtseq=45,rtrep=.f.,;
    cFormVar = "w_VALORIN", cQueryName = "VALORIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 193563478,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=471, Top=208, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  add object oARROT2_1_54 as StdField with uid="RHGJBSFYAZ",rtseq=46,rtrep=.f.,;
    cFormVar = "w_ARROT2", cQueryName = "ARROT2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 78248710,;
   bGlobalFont=.t.,;
    Height=21, Width=117, Left=341, Top=235, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oARROT2_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0)
    endwith
   endif
  endfunc

  add object oVALOR2IN_1_55 as StdField with uid="NMSIQUJYRO",rtseq=47,rtrep=.f.,;
    cFormVar = "w_VALOR2IN", cQueryName = "VALOR2IN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 76123044,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=471, Top=235, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oVALOR2IN_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0)
    endwith
   endif
  endfunc

  func oVALOR2IN_1_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VALOR2IN>.w_VALORIN OR .w_VALOR2IN=0)
    endwith
    return bRes
  endfunc

  add object oARROT3_1_56 as StdField with uid="HXTPQSAAPP",rtseq=48,rtrep=.f.,;
    cFormVar = "w_ARROT3", cQueryName = "ARROT3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 95025926,;
   bGlobalFont=.t.,;
    Height=21, Width=117, Left=341, Top=262, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oARROT3_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_VALOR2IN<>0)
    endwith
   endif
  endfunc

  add object oVALOR3IN_1_57 as StdField with uid="GVJHJMJPXN",rtseq=49,rtrep=.f.,;
    cFormVar = "w_VALOR3IN", cQueryName = "VALOR3IN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 92900260,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=471, Top=262, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oVALOR3IN_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_VALOR2IN<>0)
    endwith
   endif
  endfunc

  func oVALOR3IN_1_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_VALOR3IN>.w_VALORIN AND .w_VALOR3IN>.w_VALOR2IN) OR .w_VALOR3IN=0)
    endwith
    return bRes
  endfunc

  add object oARROT4_1_58 as StdField with uid="RPOAXMKGRV",rtseq=50,rtrep=.f.,;
    cFormVar = "w_ARROT4", cQueryName = "ARROT4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento per importi diversi dai precedenti (0 = nessun arrotondamento)",;
    HelpContextID = 111803142,;
   bGlobalFont=.t.,;
    Height=21, Width=117, Left=341, Top=289, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"


  add object oBtn_1_63 as StdButton with uid="YSRFFHTDMW",left=632, top=318, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la variazione del listino prezzi";
    , HelpContextID = 219542758;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_63.Click()
      with this.Parent.oContained
        do GSAR_BVL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_64 as StdButton with uid="OJYROTBZFC",left=682, top=318, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 226831430;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_64.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oVALOFIN_1_74 as StdField with uid="ZZMRLWXAIF",rtseq=51,rtrep=.f.,;
    cFormVar = "w_VALOFIN", cQueryName = "VALOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 180980566,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=603, Top=290, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  add object oORIGSCA_1_79 as StdCheck with uid="COPOUKHHFV",rtseq=67,rtrep=.f.,left=228, top=100, caption="Scaglioni di origine",;
    ToolTipText = "Cancella gli scaglioni di origine e inserisce quelli dello scaglione di riferimento",;
    HelpContextID = 175019034,;
    cFormVar="w_ORIGSCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oORIGSCA_1_79.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oORIGSCA_1_79.GetRadio()
    this.Parent.oContained.w_ORIGSCA = this.RadioValue()
    return .t.
  endfunc

  func oORIGSCA_1_79.SetRadio()
    this.Parent.oContained.w_ORIGSCA=trim(this.Parent.oContained.w_ORIGSCA)
    this.value = ;
      iif(this.Parent.oContained.w_ORIGSCA=='S',1,;
      0)
  endfunc

  func oORIGSCA_1_79.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)='L')
    endwith
   endif
  endfunc


  add object oTIPUMI_1_82 as StdCombo with uid="ZLVISPSMQV",value=3,rtseq=76,rtrep=.f.,left=484,top=16,width=114,height=21;
    , ToolTipText = "Cerca il listino nell'unit� di misura indicata (prima , seconda o tutte)";
    , HelpContextID = 79703242;
    , cFormVar="w_TIPUMI",RowSource=""+"Nella 1a U.m.,"+"Nella 2a U.m.,"+"In tutte le U.m.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPUMI_1_82.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,0,;
    0))))
  endfunc
  func oTIPUMI_1_82.GetRadio()
    this.Parent.oContained.w_TIPUMI = this.RadioValue()
    return .t.
  endfunc

  func oTIPUMI_1_82.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TIPUMI==1,1,;
      iif(this.Parent.oContained.w_TIPUMI==2,2,;
      iif(this.Parent.oContained.w_TIPUMI==0,3,;
      0)))
  endfunc

  func oTIPUMI_1_82.mHide()
    with this.Parent.oContained
      return (!LEFT(.w_CRITER,1)='L')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="GPCTDMFHWZ",Visible=.t., Left=7, Top=47,;
    Alignment=0, Width=163, Height=15,;
    Caption="Listino da aggiornare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="RTNQCLHVFI",Visible=.t., Left=473, Top=73,;
    Alignment=1, Width=74, Height=15,;
    Caption="Valido dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="EWACOKHGOF",Visible=.t., Left=628, Top=73,;
    Alignment=1, Width=23, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="MHYKCKYFTW",Visible=.t., Left=7, Top=126,;
    Alignment=0, Width=205, Height=15,;
    Caption="Listino di riferimento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'L')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="DWKBEMSSEJ",Visible=.t., Left=7, Top=126,;
    Alignment=0, Width=212, Height=15,;
    Caption="Inventario di riferimento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'I')
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="FITZNTKILM",Visible=.t., Left=1, Top=151,;
    Alignment=1, Width=61, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'I')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="OCZLBOGXAO",Visible=.t., Left=96, Top=151,;
    Alignment=1, Width=77, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'I')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="WHPHPWLWAK",Visible=.t., Left=475, Top=151,;
    Alignment=1, Width=72, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'I')
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="GOPKQRRHJT",Visible=.t., Left=8, Top=181,;
    Alignment=0, Width=205, Height=15,;
    Caption="Parametri di calcolo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="NYHQXBQHJL",Visible=.t., Left=4, Top=235,;
    Alignment=1, Width=128, Height=15,;
    Caption="Percentuale ricalcolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="SVPRRTBRIR",Visible=.t., Left=4, Top=315,;
    Alignment=1, Width=128, Height=15,;
    Caption="Ricalcolo in valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="JEFECLEULP",Visible=.t., Left=341, Top=181,;
    Alignment=0, Width=104, Height=15,;
    Caption="Arrotondamenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_65 as StdString with uid="HFSKARKXKS",Visible=.t., Left=510, Top=151,;
    Alignment=1, Width=141, Height=15,;
    Caption="Listino valido al:"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'L')
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="XOJPUEDBIP",Visible=.t., Left=4, Top=208,;
    Alignment=1, Width=128, Height=15,;
    Caption="Tipo di ricalcolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="ZQGSPMGDQM",Visible=.t., Left=7, Top=16,;
    Alignment=1, Width=149, Height=15,;
    Caption="Criterio di aggiornamento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_68 as StdString with uid="NBDVYQBKQG",Visible=.t., Left=275, Top=151,;
    Alignment=1, Width=49, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'I')
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="MHEWZFMOQE",Visible=.t., Left=469, Top=181,;
    Alignment=0, Width=146, Height=15,;
    Caption="Per importi fino a"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_73 as StdString with uid="HFEZTPBADH",Visible=.t., Left=460, Top=291,;
    Alignment=1, Width=143, Height=15,;
    Caption="Per importi superiori a:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="ZUFOTDCVAR",Visible=.t., Left=372, Top=99,;
    Alignment=1, Width=125, Height=18,;
    Caption="Sconti/maggiorazioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (.w_FLSCO<>'S' OR .w_AGSCO<>'F')
    endwith
  endfunc

  add object oStr_1_77 as StdString with uid="ZCMFBXNAHJ",Visible=.t., Left=473, Top=16,;
    Alignment=1, Width=69, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CRITER,1)<>'U')
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="RPVQSFQNKM",Visible=.t., Left=4, Top=262,;
    Alignment=1, Width=128, Height=18,;
    Caption="1� moltiplicatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="WMZBUNDJMC",Visible=.t., Left=4, Top=289,;
    Alignment=1, Width=128, Height=18,;
    Caption="2� moltiplicatore:"  ;
  , bGlobalFont=.t.

  add object oBox_1_69 as StdBox with uid="PUVSDTFJMR",left=2, top=143, width=723,height=2

  add object oBox_1_70 as StdBox with uid="LFYMCQCNFX",left=4, top=198, width=723,height=2

  add object oBox_1_71 as StdBox with uid="BNUIOSCOHT",left=4, top=64, width=723,height=2
enddefine
define class tgsma_kvlPag2 as StdContainer
  Width  = 735
  height = 363
  stdWidth  = 735
  stdheight = 363
  resizeXpos=366
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODARTIN_2_2 as StdField with uid="STYAQECGSX",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CODARTIN", cQueryName = "CODARTIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � superiore al codice finale oppure obsoleto",;
    ToolTipText = "Codice articolo/servizio di inizio selezione",;
    HelpContextID = 108730484,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=111, Top=16, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODARTIN"

  func oCODARTIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTIN_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTIN_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODARTIN_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'',this.parent.oContained
  endproc
  proc oCODARTIN_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODARTIN
     i_obj.ecpSave()
  endproc

  add object oCODARTFI_2_3 as StdField with uid="VJWSSZAIJQ",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CODARTFI", cQueryName = "CODARTFI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � superiore al codice finale oppure obsoleto",;
    ToolTipText = "Codice articolo/servizio di fine selezione",;
    HelpContextID = 159704977,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=111, Top=45, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODARTFI"

  func oCODARTFI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTFI_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTFI_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODARTFI_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'',this.parent.oContained
  endproc
  proc oCODARTFI_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODARTFI
     i_obj.ecpSave()
  endproc

  add object oDESART_2_6 as StdField with uid="XIGGDSMEYY",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 159646154,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=268, Top=16, InputMask=replicate('X',40)

  add object oDESART1_2_7 as StdField with uid="UQMDLCZMPO",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESART1", cQueryName = "DESART1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 159646154,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=268, Top=45, InputMask=replicate('X',40)

  add object oCODIVA_2_8 as StdField with uid="QMIJMBDYFI",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA articolo di selezione",;
    HelpContextID = 205318106,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=111, Top=74, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CODIVA"

  func oCODIVA_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIVA_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIVA_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCODIVA_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'',this.parent.oContained
  endproc
  proc oCODIVA_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_CODIVA
     i_obj.ecpSave()
  endproc

  add object oIVDESCR_2_10 as StdField with uid="BOHSKZBYUS",rtseq=58,rtrep=.f.,;
    cFormVar = "w_IVDESCR", cQueryName = "IVDESCR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 175169658,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=178, Top=74, InputMask=replicate('X',35)

  add object oGRUPMERC_2_11 as StdField with uid="MPJRKBZWIT",rtseq=59,rtrep=.f.,;
    cFormVar = "w_GRUPMERC", cQueryName = "GRUPMERC",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Aggiorna i listini del gruppo merceologico selezionato",;
    HelpContextID = 147117143,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=111, Top=103, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUPMERC"

  func oGRUPMERC_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPMERC_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPMERC_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUPMERC_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRUPMERC_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUPMERC
     i_obj.ecpSave()
  endproc

  add object oCATOMOGE_2_12 as StdField with uid="ENAZMQDZCY",rtseq=60,rtrep=.f.,;
    cFormVar = "w_CATOMOGE", cQueryName = "CATOMOGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Aggiorna i listini della categoria omogenea selezionata",;
    HelpContextID = 20580971,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=111, Top=132, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOMOGE"

  func oCATOMOGE_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOMOGE_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOMOGE_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOMOGE_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCATOMOGE_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOMOGE
     i_obj.ecpSave()
  endproc

  add object oFAMIGLIA_2_13 as StdField with uid="KVJFFQQJPD",rtseq=61,rtrep=.f.,;
    cFormVar = "w_FAMIGLIA", cQueryName = "FAMIGLIA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Aggiorna i listini della famiglia selezionata",;
    HelpContextID = 231971479,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=111, Top=161, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMIGLIA"

  func oFAMIGLIA_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMIGLIA_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMIGLIA_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMIGLIA_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglia articoli",'',this.parent.oContained
  endproc
  proc oFAMIGLIA_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMIGLIA
     i_obj.ecpSave()
  endproc

  add object oGDESCRI_2_17 as StdField with uid="BNUJWCXTTZ",rtseq=62,rtrep=.f.,;
    cFormVar = "w_GDESCRI", cQueryName = "GDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 60628326,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=178, Top=103, InputMask=replicate('X',35)

  add object oCTDESCRI_2_18 as StdField with uid="CMMWUUMNTN",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CTDESCRI", cQueryName = "CTDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 175170193,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=178, Top=132, InputMask=replicate('X',35)

  add object oFDESCRI_2_19 as StdField with uid="DQMRSERNJJ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_FDESCRI", cQueryName = "FDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 60628310,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=178, Top=161, InputMask=replicate('X',35)

  add object oMARCA_2_20 as StdField with uid="AYENGNQTZY",rtseq=68,rtrep=.f.,;
    cFormVar = "w_MARCA", cQueryName = "MARCA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Aggiorna i listini della marca selezionata",;
    HelpContextID = 23980742,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=111, Top=190, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARCA"

  func oMARCA_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARCA_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARCA_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARCA_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oMARCA_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARCA
     i_obj.ecpSave()
  endproc

  add object oCLARICA_2_21 as StdField with uid="QINCTUNIXQ",rtseq=69,rtrep=.f.,;
    cFormVar = "w_CLARICA", cQueryName = "CLARICA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe di ricarico di selezione",;
    HelpContextID = 184818394,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=111, Top=219, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_RICA", cZoomOnZoom="GSAR_ACR", oKey_1_1="CRCODICE", oKey_1_2="this.w_CLARICA"

  func oCLARICA_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLARICA_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLARICA_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RICA','*','CRCODICE',cp_AbsName(this.parent,'oCLARICA_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACR',"Classi di ricarico dei listini",'',this.parent.oContained
  endproc
  proc oCLARICA_2_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CRCODICE=this.parent.oContained.w_CLARICA
     i_obj.ecpSave()
  endproc

  add object oMDESCRI_2_22 as StdField with uid="PLAOCFNXPP",rtseq=70,rtrep=.f.,;
    cFormVar = "w_MDESCRI", cQueryName = "MDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 60628422,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=178, Top=190, InputMask=replicate('X',35)

  add object oFORABIT_2_24 as StdField with uid="DMKRORRVUP",rtseq=71,rtrep=.f.,;
    cFormVar = "w_FORABIT", cQueryName = "FORABIT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Fornitore inesistente o obsoleto",;
    ToolTipText = "Fornitore abituale di selezione",;
    HelpContextID = 92538794,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=111, Top=248, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORABIT"

  func oFORABIT_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORABIT_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORABIT_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORABIT_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oFORABIT_2_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_FORABIT
     i_obj.ecpSave()
  endproc

  add object oFODESCRI_2_26 as StdField with uid="DTCOKRUCSH",rtseq=72,rtrep=.f.,;
    cFormVar = "w_FODESCRI", cQueryName = "FODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 175171425,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=234, Top=248, InputMask=replicate('X',40)

  add object oFORULTC_2_27 as StdField with uid="XXJBTNJEPR",rtseq=73,rtrep=.f.,;
    cFormVar = "w_FORULTC", cQueryName = "FORULTC",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Fornitore inesistente o obsoleto",;
    ToolTipText = "Fornitore dal quale l'articolo � stato acquistato",;
    HelpContextID = 164628394,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=111, Top=277, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORULTC"

  func oFORULTC_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORULTC_2_27.ecpDrop(oSource)
    this.Parent.oContained.link_2_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORULTC_2_27.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORULTC_2_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oFORULTC_2_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_FORULTC
     i_obj.ecpSave()
  endproc

  add object oFUDESCRI_2_29 as StdField with uid="HFLIWQSWEU",rtseq=74,rtrep=.f.,;
    cFormVar = "w_FUDESCRI", cQueryName = "FUDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 175169889,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=234, Top=277, InputMask=replicate('X',40)

  add object oCLADESCRI_2_30 as StdField with uid="NTVPXNRHWC",rtseq=75,rtrep=.f.,;
    cFormVar = "w_CLADESCRI", cQueryName = "CLADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189928952,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=178, Top=219, InputMask=replicate('X',35)

  add object oStr_2_4 as StdString with uid="WWWTRAIHUP",Visible=.t., Left=1, Top=16,;
    Alignment=1, Width=108, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="VXROUPAFHF",Visible=.t., Left=1, Top=45,;
    Alignment=1, Width=108, Height=15,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="IFMIEQVNIM",Visible=.t., Left=1, Top=74,;
    Alignment=1, Width=108, Height=15,;
    Caption="Cod. IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="NIRPFDDQPW",Visible=.t., Left=1, Top=103,;
    Alignment=1, Width=108, Height=15,;
    Caption="Gruppo merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="NYRMMNNGVA",Visible=.t., Left=1, Top=132,;
    Alignment=1, Width=108, Height=15,;
    Caption="Cat.omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="WUPLPUXSMQ",Visible=.t., Left=1, Top=161,;
    Alignment=1, Width=108, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="AQJLNGYDYC",Visible=.t., Left=1, Top=190,;
    Alignment=1, Width=108, Height=18,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="ZOZVIDOJBG",Visible=.t., Left=1, Top=248,;
    Alignment=1, Width=108, Height=18,;
    Caption="Fornitore abituale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="PSKSMHLRJK",Visible=.t., Left=1, Top=277,;
    Alignment=1, Width=108, Height=18,;
    Caption="Fornitore ult.costo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="AAFRIQTGBA",Visible=.t., Left=2, Top=219,;
    Alignment=1, Width=107, Height=18,;
    Caption="Classe di ricarico:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kvl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
