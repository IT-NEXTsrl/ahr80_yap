* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bps                                                        *
*              Check politiche di accesso                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-10                                                      *
* Last revis.: 2016-03-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bps",oParentObject,m.pOper)
return(i_retval)

define class tgsut_bps as StdBatch
  * --- Local variables
  pOper = space(1)
  w_BLCSDP = space(1)
  w_UTEA = 0
  w_BLCSDA = space(1)
  w_BLCNMT = space(1)
  w_MAXTEN = 0
  w_VALMAX = 0
  w_ULTACC = 0
  w_GGANTI = 0
  w_ATTIVO = .f.
  w_ESCAMM = space(1)
  w_GGANTI2 = 0
  w_MAXTEN2 = 0
  w_VALMAX2 = 0
  w_BLOCCATO = space(1)
  w_DATULT = ctod("  /  /  ")
  w_DATACC = ctod("  /  /  ")
  w_DATSCA = ctod("  /  /  ")
  w_CAMBIOPWD = space(1)
  w_DATA = ctod("  /  /  ")
  w_OK = .f.
  w_ISADMIN = .f.
  w_LOGIN = space(20)
  w_MACHINE = space(20)
  w_CODUTE = 0
  w_PS__JBSH = space(1)
  w_NOPOLIT = .f.
  w_CODPERSONA = space(5)
  w_COGNOME = space(50)
  w_NOME = space(50)
  w_DPCHKING = space(1)
  w_DPGGBLOC = 0
  w_DATINIPER = ctod("  /  /  ")
  w_CALENDUSED = space(5)
  w_DESCALEND = space(40)
  w_GIORNO = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_PACHKING = space(1)
  w_DATINIFISSA = ctod("  /  /  ")
  w_PAGIOCHK = 0
  w_NumGGIncompl = 0
  w_RESOCONTO = space(0)
  w_SEC = .f.
  w_TIPOATTIVAZIONE = space(1)
  w_GJCODICE = 0
  w_TEST = 0
  w_GSUT_KPA = .NULL.
  w_GSUT_MPS = .NULL.
  w_oCODUTE = 0
  w_ADMBLK = .f.
  w_PWD_1 = space(20)
  w_MSG = space(200)
  * --- WorkFile variables
  POL_SIC_idx=0
  POL_UTE_idx=0
  CPUSRGRP_idx=0
  CPUSERS_idx=0
  cpusers_idx=0
  GROUPSJ_idx=0
  POL_ACC_idx=0
  DIPENDEN_idx=0
  TAB_CALE_idx=0
  CAL_AZIE_idx=0
  PAR_AGEN_idx=0
  cpusrgrp_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Politiche di Sicurezza (Lanciato da GS___UTE) 
    * --- --
    * --- --
    * --- --
    * --- --
    do case
      case this.pOper = "U" 
        * --- A T T E N Z I O N E 
        *     ==================================
        *     Nel caso si intenda aggiungere scritture sul database che possono
        *     assumere valori vuoti verificare attentamente il caricamento dell'XDC.
        *     (Testando comportamento procedura cancellando XDC_AS_MEM.MEM)
        this.w_DATA = Date()
        * --- Controllo schedulatore - se l'utente fa parte del gruppo schedulatore non abilito 
        *     le procedure di sicurezza
        * --- Leggo la politica di sicurezza
        this.w_ISADMIN = .T.
        this.w_NOPOLIT = .F.
        * --- Read from POL_SIC
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.POL_SIC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.POL_SIC_idx,2],.t.,this.POL_SIC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PSVALMAX,PSMAXTEN,PSULTACC,PSGGANTI,PSBLCNMT,PSBLCSDA,PSBLCSDP,PSATTIVO,PS__JBSH,PS_NONUM,PSVALMAA,PSMAXTEA,PSGGANTA,PS__AMMI"+;
            " from "+i_cTable+" POL_SIC where ";
                +"PSSERIAL = "+cp_ToStrODBC("AHE");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PSVALMAX,PSMAXTEN,PSULTACC,PSGGANTI,PSBLCNMT,PSBLCSDA,PSBLCSDP,PSATTIVO,PS__JBSH,PS_NONUM,PSVALMAA,PSMAXTEA,PSGGANTA,PS__AMMI;
            from (i_cTable) where;
                PSSERIAL = "AHE";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_VALMAX = NVL(cp_ToDate(_read_.PSVALMAX),cp_NullValue(_read_.PSVALMAX))
          this.w_MAXTEN = NVL(cp_ToDate(_read_.PSMAXTEN),cp_NullValue(_read_.PSMAXTEN))
          this.w_ULTACC = NVL(cp_ToDate(_read_.PSULTACC),cp_NullValue(_read_.PSULTACC))
          this.w_GGANTI = NVL(cp_ToDate(_read_.PSGGANTI),cp_NullValue(_read_.PSGGANTI))
          this.w_BLCNMT = NVL(cp_ToDate(_read_.PSBLCNMT),cp_NullValue(_read_.PSBLCNMT))
          this.w_BLCSDA = NVL(cp_ToDate(_read_.PSBLCSDA),cp_NullValue(_read_.PSBLCSDA))
          this.w_BLCSDP = NVL(cp_ToDate(_read_.PSBLCSDP),cp_NullValue(_read_.PSBLCSDP))
          this.w_ATTIVO = NVL(cp_ToDate(_read_.PSATTIVO),cp_NullValue(_read_.PSATTIVO))
          this.w_PS__JBSH = NVL(cp_ToDate(_read_.PS__JBSH),cp_NullValue(_read_.PS__JBSH))
          this.w_TIPOATTIVAZIONE = NVL(cp_ToDate(_read_.PS_NONUM),cp_NullValue(_read_.PS_NONUM))
          this.w_VALMAX2 = NVL(cp_ToDate(_read_.PSVALMAA),cp_NullValue(_read_.PSVALMAA))
          this.w_MAXTEN2 = NVL(cp_ToDate(_read_.PSMAXTEA),cp_NullValue(_read_.PSMAXTEA))
          this.w_GGANTI2 = NVL(cp_ToDate(_read_.PSGGANTA),cp_NullValue(_read_.PSGGANTA))
          this.w_ESCAMM = NVL(cp_ToDate(_read_.PS__AMMI),cp_NullValue(_read_.PS__AMMI))
          use
          if i_Rows=0
            this.w_NOPOLIT = .T.
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Non viene utilizzata la cp_isadministrator per verificare se l'utente � amministratore perch� i_codute non � ancora valorizzato
        * --- Read from CPUSRGRP
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CPUSRGRP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CPUSRGRP_idx,2],.t.,this.CPUSRGRP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "usercode"+;
            " from "+i_cTable+" CPUSRGRP where ";
                +"groupcode = "+cp_ToStrODBC(1);
                +" and usercode = "+cp_ToStrODBC(this.oParentObject.w_UTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            usercode;
            from (i_cTable) where;
                groupcode = 1;
                and usercode = this.oParentObject.w_UTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UTEA = NVL(cp_ToDate(_read_.usercode),cp_NullValue(_read_.usercode))
          use
          if i_Rows=0
            this.w_ISADMIN = .F.
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_ISADMIN and this.w_ESCAMM<>"S"
          this.w_VALMAX = this.w_VALMAX2
          this.w_GGANTI = this.w_GGANTI2
          this.w_MAXTEN = this.w_MAXTEN2
        endif
        if Type("g_NoSingleSignOn") = "L" And g_NoSingleSignOn And this.w_TIPOATTIVAZIONE = "W"
          this.w_TIPOATTIVAZIONE = "N"
        endif
        * --- Disattivo le procedure di sicurezza se utente schedulatore
        if this.w_ATTIVO = "S" And this.w_PS__JBSH = "S"
          * --- Read from GROUPSJ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.GROUPSJ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GROUPSJ_idx,2],.t.,this.GROUPSJ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "GJCODICE"+;
              " from "+i_cTable+" GROUPSJ where ";
                  +"1 = "+cp_ToStrODBC(1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              GJCODICE;
              from (i_cTable) where;
                  1 = 1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_GJCODICE = NVL(cp_ToDate(_read_.GJCODICE),cp_NullValue(_read_.GJCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if !Empty(this.w_GJCODICE)
            * --- Ciclo sui gruppi per verificare se appartiene a quello schedulatore
            * --- Select from cpusrgrp
            i_nConn=i_TableProp[this.cpusrgrp_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cpusrgrp_idx,2],.t.,this.cpusrgrp_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select groupcode  from "+i_cTable+" cpusrgrp ";
                  +" where usercode = "+cp_ToStrODBC(this.oParentObject.w_UTE)+"";
                   ,"_Curs_cpusrgrp")
            else
              select groupcode from (i_cTable);
               where usercode = this.oParentObject.w_UTE;
                into cursor _Curs_cpusrgrp
            endif
            if used('_Curs_cpusrgrp')
              select _Curs_cpusrgrp
              locate for 1=1
              do while not(eof())
              if _Curs_cpusrgrp.groupcode = this.w_GJCODICE
                * --- Disattivo le procedure di sicurezza se utente schedulatore
                this.w_ATTIVO = "N"
                exit
              endif
                select _Curs_cpusrgrp
                continue
              enddo
              use
            endif
          endif
        endif
        this.w_SEC = .F.
        this.w_MAXTEN = 3
        if (Vartype(g_Service)="C" And g_Service="S" And i_bSrvTrusted) Or (!Empty(this.oParentObject.oParentObject.w_SSOID) And this.oParentObject.oParentObject.w_UTESILENT<>0)
          * --- Single Sign-On di iRevolution o accesso da servizio secondario, non controllo PWD
          this.w_OK = cp_ChangeUser(this.oParentObject.w_UTE, this.oParentObject.w_PWD, this.w_MAXTEN, .T.)
        else
          if this.w_NOPOLIT Or this.w_ATTIVO="N"
            this.w_OK = cp_ChangeUser(this.oParentObject.w_UTE,this.oParentObject.w_PWD, this.w_MAXTEN )
          else
            if this.w_TIPOATTIVAZIONE = "W"
              * --- Single Sign-On, non controllo PWD
              this.w_OK = cp_ChangeUser(this.oParentObject.w_UTE, this.oParentObject.w_PWD, this.w_MAXTEN, .T.)
            else
              this.w_SEC = .T.
              this.w_OK = this.oParentObject.w_UTE<>0
            endif
          endif
        endif
        if this.oParentObject.w_UTE <> 0 And this.w_SEC
          * --- Leggo numero di tentativi
          * --- Read from POL_UTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.POL_UTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2],.t.,this.POL_UTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PUFLGBLO,PUDATULT,PUDATACC,PUNUMTEN,PUCHGPWD"+;
              " from "+i_cTable+" POL_UTE where ";
                  +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PUFLGBLO,PUDATULT,PUDATACC,PUNUMTEN,PUCHGPWD;
              from (i_cTable) where;
                  PUCODUTE = this.oParentObject.w_UTE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_BLOCCATO = NVL(cp_ToDate(_read_.PUFLGBLO),cp_NullValue(_read_.PUFLGBLO))
            this.w_DATULT = NVL(cp_ToDate(_read_.PUDATULT),cp_NullValue(_read_.PUDATULT))
            this.w_DATACC = NVL(cp_ToDate(_read_.PUDATACC),cp_NullValue(_read_.PUDATACC))
            i_pwdtrys = NVL(cp_ToDate(_read_.PUNUMTEN),cp_NullValue(_read_.PUNUMTEN))
            this.w_CAMBIOPWD = NVL(cp_ToDate(_read_.PUCHGPWD),cp_NullValue(_read_.PUCHGPWD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows = 0
            * --- Per non caricare l'XDC (Cp_NullLink) imposto il valore dei tentativi a -1..
            * --- Insert into POL_UTE
            i_nConn=i_TableProp[this.POL_UTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.POL_UTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PUCODUTE"+",PUFLGBLO"+",PUDATULT"+",PUNUMTEN"+",PUMOTBLO"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UTE),'POL_UTE','PUCODUTE');
              +","+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUFLGBLO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DATA),'POL_UTE','PUDATULT');
              +","+cp_NullLink(cp_ToStrODBC(-1),'POL_UTE','PUNUMTEN');
              +","+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUMOTBLO');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PUCODUTE',this.oParentObject.w_UTE,'PUFLGBLO',"N",'PUDATULT',this.w_DATA,'PUNUMTEN',-1,'PUMOTBLO',"N")
              insert into (i_cTable) (PUCODUTE,PUFLGBLO,PUDATULT,PUNUMTEN,PUMOTBLO &i_ccchkf. );
                 values (;
                   this.oParentObject.w_UTE;
                   ,"N";
                   ,this.w_DATA;
                   ,-1;
                   ,"N";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Se i tentativi =-1 (vedi ramo principale IF) li imposto a 0...
            if i_pwdtrys=-1
              i_pwdtrys = 0
            endif
          endif
          if empty(this.w_DATULT)
            this.w_DATULT = this.w_DATA
          endif
          if empty(this.w_DATACC)
            this.w_DATACC = this.w_DATA
          endif
          this.w_DATSCA = this.w_DATULT + this.w_VALMAX
          * --- Effettuo tutti i controlli
          if this.w_CAMBIOPWD="S"
            GSUT_KWD(this.oParentObject)
            if NOT this.oParentObject.w_CHANGED
              this.oParentObject.w_OKPWD = "RIT"
              i_retcode = 'stop'
              return
            else
              this.w_CAMBIOPWD = "N"
              * --- Write into POL_UTE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.POL_UTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PUCHGPWD ="+cp_NullLink(cp_ToStrODBC(this.w_CAMBIOPWD),'POL_UTE','PUCHGPWD');
                    +i_ccchkf ;
                +" where ";
                    +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTE);
                       )
              else
                update (i_cTable) set;
                    PUCHGPWD = this.w_CAMBIOPWD;
                    &i_ccchkf. ;
                 where;
                    PUCODUTE = this.oParentObject.w_UTE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.oParentObject.w_CHANGED = .F.
            endif
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Non ho l'utente (gestione sicurezza attiva) pws scorretta, gestione
          *     sicurezza non attiva
          if Not this.w_OK
            * --- Se eseguo tentativi specificando utenti inesistenti...
            this.oParentObject.w_CONTACC = this.oParentObject.w_CONTACC + 1
            if this.oParentObject.w_CONTACC >= this.w_MAXTEN 
              ah_ErrorMsg("Superato numero massimo di tentativi di inserimento password", 16)
              this.oParentObject.w_OKPWD = "ESC"
            else
              if this.w_SEC
                AH_ERRORMSG("Utente o password errati",48)
              else
                AH_ERRORMSG("Password errata",48)
              endif
              this.oParentObject.w_OKPWD = "RIT"
            endif
          else
            * --- Password corretta
            this.oParentObject.w_OKPWD = "OK"
          endif
        endif
      case this.pOper = "N" 
        * --- Link su w_UTE
        *     Case Insensitive..
        this.oParentObject.w_UTE = 0
        * --- Read from cpusers
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.cpusers_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2],.t.,this.cpusers_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "code"+;
            " from "+i_cTable+" cpusers where ";
                +"cp_login = "+cp_ToStrODBC(this.oParentObject.w_NOMEUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            code;
            from (i_cTable) where;
                cp_login = this.oParentObject.w_NOMEUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODUTE = NVL(cp_ToDate(_read_.code),cp_NullValue(_read_.code))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_UTE = this.w_CODUTE
        this.oParentObject.w_PWD = " "
      case this.pOper = "A" 
        if this.oParentObject.w_PSULTACC = 0 Or this.oParentObject.w_PSATTIVO="N"
          * --- Se non gestisco la scadenza dell' account o disattivo le politiche di sicurezza 
          *     sbianco la data di ultimo accesso di tutti gli utenti
          * --- Write into POL_UTE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.POL_UTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PUDATACC ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'POL_UTE','PUDATACC');
                +i_ccchkf ;
            +" where ";
                +"1 = "+cp_ToStrODBC(1);
                   )
          else
            update (i_cTable) set;
                PUDATACC = cp_CharToDate("  -  -    ");
                &i_ccchkf. ;
             where;
                1 = 1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Controllo presenza record su tabella POL_ACC se � vuoto carico una prima
        *     associazione per essere sicuri di poter entrare al prossimo ingresso
        if this.oParentObject.w_PS_NONUM = "W"
          this.w_TEST = 0
          * --- Read from POL_ACC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.POL_ACC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.POL_ACC_idx,2],.t.,this.POL_ACC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PACODUTE"+;
              " from "+i_cTable+" POL_ACC where ";
                  +"PASERIAL = "+cp_ToStrODBC("0000000001");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PACODUTE;
              from (i_cTable) where;
                  PASERIAL = "0000000001";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TEST = NVL(cp_ToDate(_read_.PACODUTE),cp_NullValue(_read_.PACODUTE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_TEST = 0
            * --- Non ci sono record, apro la gestione e carico un record
            ah_ErrorMsg("Autenticazione tramite Sistema Operativo attivata%0E' necessario associare almeno un account")
            this.w_GSUT_KPA = GSUT_KPA()
            this.w_GSUT_MPS = this.w_GSUT_KPA.GSUT_MPS
            this.w_GSUT_MPS.MarkPos()     
            this.w_GSUT_MPS.AddRow()     
            setvaluelinked("DT", this.w_GSUT_MPS, "w_PACODUTE", i_CODUTE)
            this.w_GSUT_MPS.w_PA_LOGIN = GetUserAccount()
            this.w_GSUT_MPS.SaveRow()     
            this.w_GSUT_MPS.RePos()     
            this.w_GSUT_MPS = .NULL.
            this.w_GSUT_KPA = .NULL.
          endif
        endif
      case this.pOper = "B" 
        * --- Calcola il codice della persona associata all'utente
        this.w_CODPERSONA = Readdipend(i_codute, "C")
        * --- Legge il campo Controllo utente all'ingresso
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPCHKING,DPGGBLOC,DPDATINI,DPCOGNOM,DPNOME"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_CODPERSONA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPCHKING,DPGGBLOC,DPDATINI,DPCOGNOM,DPNOME;
            from (i_cTable) where;
                DPCODICE = this.w_CODPERSONA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DPCHKING = NVL(cp_ToDate(_read_.DPCHKING),cp_NullValue(_read_.DPCHKING))
          this.w_DPGGBLOC = NVL(cp_ToDate(_read_.DPGGBLOC),cp_NullValue(_read_.DPGGBLOC))
          this.w_DATINIPER = NVL(cp_ToDate(_read_.DPDATINI),cp_NullValue(_read_.DPDATINI))
          this.w_COGNOME = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
          this.w_NOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Nei casi di Blocco/Avvertimento
        if this.w_DPCHKING $ "BA"
          this.w_CALENDUSED = Getcalris(this.w_CODPERSONA)
          * --- Read from TAB_CALE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TAB_CALE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TAB_CALE_idx,2],.t.,this.TAB_CALE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TCDESCRI"+;
              " from "+i_cTable+" TAB_CALE where ";
                  +"TCCODICE = "+cp_ToStrODBC(this.w_CALENDUSED);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TCDESCRI;
              from (i_cTable) where;
                  TCCODICE = this.w_CALENDUSED;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESCALEND = NVL(cp_ToDate(_read_.TCDESCRI),cp_NullValue(_read_.TCDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Controllo di esistenza calendario applicabile alla persona
          do case
            case EMPTY(this.w_CALENDUSED) AND this.w_DPCHKING="B"
              ah_ErrorMsg("Attenzione! Non esiste alcun calendario applicabile alla persona %1", "", "", alltrim(this.w_COGNOME)+space(1)+alltrim(this.w_NOME))
              * --- Blocca ingresso
              i_codute = 0
              i_retcode = 'stop'
              return
            case EMPTY(this.w_CALENDUSED) AND this.w_DPCHKING="A"
              ah_ErrorMsg("Attenzione! Non esiste alcun calendario applicabile alla persona %1", "", "", alltrim(this.w_COGNOME)+space(1)+alltrim(this.w_NOME))
              i_retcode = 'stop'
              return
          endcase
          * --- Controllo di esistenza data di sistema nel calendario applicabile alla persona
          * --- Read from CAL_AZIE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2],.t.,this.CAL_AZIE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAGIORNO"+;
              " from "+i_cTable+" CAL_AZIE where ";
                  +"CACODCAL = "+cp_ToStrODBC(this.w_CALENDUSED);
                  +" and CAGIORNO = "+cp_ToStrODBC(i_datsys);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAGIORNO;
              from (i_cTable) where;
                  CACODCAL = this.w_CALENDUSED;
                  and CAGIORNO = i_datsys;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_GIORNO = NVL(cp_ToDate(_read_.CAGIORNO),cp_NullValue(_read_.CAGIORNO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          do case
            case EMPTY(this.w_GIORNO) AND this.w_DPCHKING="B"
              ah_ErrorMsg("Attenzione! Data di sistema non esistente nel calendario. Occorre generare i giorni per il calendario %1 tramite apposita funzione", "", "", alltrim(this.w_CALENDUSED)+" - "+alltrim(this.w_DESCALEND))
              * --- Blocca ingresso
              i_codute = 0
              i_retcode = 'stop'
              return
            case EMPTY(this.w_GIORNO) AND this.w_DPCHKING="A"
              ah_ErrorMsg("Attenzione! Data di sistema non esistente nel calendario. Occorre generare i giorni per il calendario %1 tramite apposita funzione", "", "", alltrim(this.w_CALENDUSED)+" - "+alltrim(this.w_DESCALEND))
              i_retcode = 'stop'
              return
          endcase
          * --- Data finale controllo: giorno precedente alla data del client
          this.w_DATFIN = DATE() - 1
          * --- Read from PAR_AGEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PACHKING,PADATINI,PAGIOCHK"+;
              " from "+i_cTable+" PAR_AGEN where ";
                  +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PACHKING,PADATINI,PAGIOCHK;
              from (i_cTable) where;
                  PACODAZI = i_codazi;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PACHKING = NVL(cp_ToDate(_read_.PACHKING),cp_NullValue(_read_.PACHKING))
            this.w_DATINIFISSA = NVL(cp_ToDate(_read_.PADATINI),cp_NullValue(_read_.PADATINI))
            this.w_PAGIOCHK = NVL(cp_ToDate(_read_.PAGIOCHK),cp_NullValue(_read_.PAGIOCHK))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Determina la data iniziale controllo
          do case
            case this.w_PACHKING="P"
              * --- Data persona
              this.w_DATINI = this.w_DATINIPER
            case this.w_PACHKING="F"
              * --- Data fissa
              this.w_DATINI = IIF(this.w_DATINIFISSA<this.w_DATINIPER, this.w_DATINIPER, this.w_DATINIFISSA)
            case this.w_PACHKING="A"
              * --- Giorni indietro rispetto alla data del client
              this.w_DATINI = IIF(DATE() - this.w_PAGIOCHK < this.w_DATINIPER, this.w_DATINIPER, DATE() - this.w_PAGIOCHK)
          endcase
          this.w_NumGGIncompl = 0
          this.w_RESOCONTO = "Persona: " + alltrim(this.w_CODPERSONA) + " - " + alltrim(this.w_COGNOME) + space(1) + alltrim(this.w_NOME) +IIF(this.w_DPCHKING = "B", "  con blocco all'ingresso dopo "+alltrim(str(this.w_DPGGBLOC,3,0))+" giorni incompleti o mancanti a partire dal "+dtoc(this.w_DATINI) ,"") + chr(13)
          this.w_RESOCONTO = this.w_RESOCONTO + chr(13) + "Attenzione! Il resoconto giornaliero non � stato inserito oppure � incompleto per i seguenti giorni:" + chr(13)
          * --- Select from Query\Chkute
          do vq_exec with 'Query\Chkute',this,'_Curs_Query_Chkute','',.f.,.t.
          if used('_Curs_Query_Chkute')
            select _Curs_Query_Chkute
            locate for 1=1
            do while not(eof())
            this.w_RESOCONTO = this.w_RESOCONTO + chr(13) + DTOC(CAGIORNO) + " - " + IIF(!EMPTY(CAGIORNO), Ah_MsgFormat(g_GIORNO[DOW(CAGIORNO)]), SPACE(3)) +SPACE(1)
            this.w_NumGGIncompl = this.w_NumGGIncompl + 1
              select _Curs_Query_Chkute
              continue
            enddo
            use
          endif
          if this.w_NumGGIncompl > 0
            do GSUT_KRI with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Nel caso di Blocco e se il numero di giorni con resoconto incompleto/mancante � maggiore di quello indicato nella persona
            if this.w_DPCHKING = "B" AND this.w_NumGGIncompl > this.w_DPGGBLOC
              * --- Blocca ingresso
              ah_ErrorMsg("Superato numero massimo di resoconti giornalieri incompleti o mancanti! %0Impossibile accedere alla procedura.", "", "")
              i_codute = 0
              Clear Events
              keyboard " "
            endif
          endif
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Memorizzo Login e Macchina 
    this.w_LOGIN = Right( Sys(0), Len( Sys(0) ) - Rat("#",Sys( 0 )) - 1)
    this.w_MACHINE = Left( Sys(0), Rat("#",Sys( 0 ))-1)
    * --- Effettuo i controlli
    if this.w_BLOCCATO = "S"
      this.w_oCODUTE = i_CODUTE
      i_CODUTE = this.oParentObject.w_UTE
      * --- Viene impedito l'accesso all'utente
      if cp_IsAdministrator(.t.)
        this.w_OK = .f.
        this.w_CODUTE = this.oParentObject.w_UTE
        do GSUT_KBL with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- w_OK a .t. mi segnala la conferma della maschera (F10) a .f.
        *     l'Esc
        if this.w_OK
          * --- La maschera ha una variabile Caller (w_PWD_1) per contenere
          *     la password inserita dall'utente. Quest'ultima sar� utilzzata per
          *     i controlli (quindi pwd di GSUT_KBL e non pwd di GS___UTE)
          this.oParentObject.w_PWD = this.w_PWD_1
          * --- utilizata a pag3, nel caso l'utente amministratore inserisce
          *     una pwd valida lo sblocca
          this.w_ADMBLK = .T.
          * --- Se Amministratore bloccato e pwd immessa errata esco immeditamente
          *     (solo un tentativo)
          this.w_MAXTEN = 1
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.oParentObject.w_OKPWD = "ESC"
        endif
        i_retcode = 'stop'
        return
      else
        ah_ErrorMsg("Account bloccato%0Accesso non consentito%0%0Rivolgersi all'amministratore di sistema per lo sblocco dell'account", 16)
        this.oParentObject.w_OKPWD = "RIT"
        i_retcode = 'stop'
        return
      endif
      i_CODUTE = this.w_oCODUTE
    else
      * --- Utente non bloccato 
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conta quanti tentativi errati si fanno, se pi� di w_MAXTEN esce
    this.w_OK = cp_ChangeUser(this.oParentObject.w_UTE,this.oParentObject.w_PWD,this.w_MAXTEN)
    if this.w_OK
      * --- Azzero il numero di tentativi
      * --- Write into POL_UTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.POL_UTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PUNUMTEN ="+cp_NullLink(cp_ToStrODBC(-1),'POL_UTE','PUNUMTEN');
            +i_ccchkf ;
        +" where ";
            +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTE);
               )
      else
        update (i_cTable) set;
            PUNUMTEN = -1;
            &i_ccchkf. ;
         where;
            PUCODUTE = this.oParentObject.w_UTE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Se sblocco amministratore allora lo sblocco sul database
      *     (solo se pwd corretta!)
      *     Sbianco anche i campi login e macchina
      if this.w_ADMBLK
        * --- Write into POL_UTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.POL_UTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PUFLGBLO ="+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUFLGBLO');
          +",PUMOTBLO ="+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUMOTBLO');
          +",PU_LOGIN ="+cp_NullLink(cp_ToStrODBC("."),'POL_UTE','PU_LOGIN');
          +",PU___MAC ="+cp_NullLink(cp_ToStrODBC("."),'POL_UTE','PU___MAC');
              +i_ccchkf ;
          +" where ";
              +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTE);
                 )
        else
          update (i_cTable) set;
              PUFLGBLO = "N";
              ,PUMOTBLO = "N";
              ,PU_LOGIN = ".";
              ,PU___MAC = ".";
              &i_ccchkf. ;
           where;
              PUCODUTE = this.oParentObject.w_UTE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      this.oParentObject.w_OKPWD = "OK"
      * --- Avvisi - Se sblocco amministratore gli avvisi mi consentono di gestire il 
      *     blocco password. Infatti nel caso di blocco amministratore per
      *     scadenza password, l'amministratore accede alla procedura ma 
      *     al prossimo accesso, se non cambia pwd, sar� nuovamente bloccato.
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Password errata
      if i_pwdtrys >= this.w_MAXTEN 
        if this.w_BLCNMT = "S"
          ah_ErrorMsg("Superato numero massimo di tentativi di inserimento password%0%0Account bloccato", 16)
          * --- Write into POL_UTE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.POL_UTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PUFLGBLO ="+cp_NullLink(cp_ToStrODBC("S"),'POL_UTE','PUFLGBLO');
            +",PUMOTBLO ="+cp_NullLink(cp_ToStrODBC("S"),'POL_UTE','PUMOTBLO');
            +",PU_LOGIN ="+cp_NullLink(cp_ToStrODBC(this.w_LOGIN),'POL_UTE','PU_LOGIN');
            +",PU___MAC ="+cp_NullLink(cp_ToStrODBC(this.w_MACHINE),'POL_UTE','PU___MAC');
                +i_ccchkf ;
            +" where ";
                +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTE);
                   )
          else
            update (i_cTable) set;
                PUFLGBLO = "S";
                ,PUMOTBLO = "S";
                ,PU_LOGIN = this.w_LOGIN;
                ,PU___MAC = this.w_MACHINE;
                &i_ccchkf. ;
             where;
                PUCODUTE = this.oParentObject.w_UTE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          ah_ErrorMsg("Superato numero massimo di tentativi di inserimento password", 16)
        endif
        * --- Azzero il numero di tentativi
        * --- Write into POL_UTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.POL_UTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PUNUMTEN ="+cp_NullLink(cp_ToStrODBC(-1),'POL_UTE','PUNUMTEN');
              +i_ccchkf ;
          +" where ";
              +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTE);
                 )
        else
          update (i_cTable) set;
              PUNUMTEN = -1;
              &i_ccchkf. ;
           where;
              PUCODUTE = this.oParentObject.w_UTE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Chiudo la procedura
        this.oParentObject.w_OKPWD = "ESC"
      else
        AH_ERRORMSG("Utente o password errati",48)
        * --- Aggiorno il numero di tentativi
        * --- Write into POL_UTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.POL_UTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PUNUMTEN ="+cp_NullLink(cp_ToStrODBC(i_pwdtrys),'POL_UTE','PUNUMTEN');
              +i_ccchkf ;
          +" where ";
              +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTE);
                 )
        else
          update (i_cTable) set;
              PUNUMTEN = i_pwdtrys;
              &i_ccchkf. ;
           where;
              PUCODUTE = this.oParentObject.w_UTE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.oParentObject.w_OKPWD = "RIT"
      endif
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Scadenza Pwd
    if (this.w_VALMAX > 0 AND this.w_DATSCA < this.w_DATA AND this.w_BLCSDP = "S")
      * --- La Password � scaduta
      ah_ErrorMsg("La password � scaduta%0%0Account bloccato", 16)
      * --- Blocco l'utente
      * --- Write into POL_UTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.POL_UTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PUFLGBLO ="+cp_NullLink(cp_ToStrODBC("S"),'POL_UTE','PUFLGBLO');
        +",PUMOTBLO ="+cp_NullLink(cp_ToStrODBC("P"),'POL_UTE','PUMOTBLO');
        +",PU_LOGIN ="+cp_NullLink(cp_ToStrODBC(this.w_LOGIN),'POL_UTE','PU_LOGIN');
        +",PU___MAC ="+cp_NullLink(cp_ToStrODBC(this.w_MACHINE),'POL_UTE','PU___MAC');
            +i_ccchkf ;
        +" where ";
            +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTE);
               )
      else
        update (i_cTable) set;
            PUFLGBLO = "S";
            ,PUMOTBLO = "P";
            ,PU_LOGIN = this.w_LOGIN;
            ,PU___MAC = this.w_MACHINE;
            &i_ccchkf. ;
         where;
            PUCODUTE = this.oParentObject.w_UTE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.oParentObject.w_OKPWD = "RIT"
      i_retcode = 'stop'
      return
    endif
    * --- Controllo se l'account � scaduto
    if this.w_ULTACC > 0 AND this.w_ULTACC <= ( this.w_DATA - this.w_DATACC ) AND this.w_BLCSDA = "S"
      * --- Account scaduto
      ah_ErrorMsg("Account scaduto%0%0Account bloccato", 16)
      * --- Blocco l'utente
      * --- Write into POL_UTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.POL_UTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PUFLGBLO ="+cp_NullLink(cp_ToStrODBC("S"),'POL_UTE','PUFLGBLO');
        +",PUMOTBLO ="+cp_NullLink(cp_ToStrODBC("A"),'POL_UTE','PUMOTBLO');
        +",PU_LOGIN ="+cp_NullLink(cp_ToStrODBC(this.w_LOGIN),'POL_UTE','PU_LOGIN');
        +",PU___MAC ="+cp_NullLink(cp_ToStrODBC(this.w_MACHINE),'POL_UTE','PU___MAC');
            +i_ccchkf ;
        +" where ";
            +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTE);
               )
      else
        update (i_cTable) set;
            PUFLGBLO = "S";
            ,PUMOTBLO = "A";
            ,PU_LOGIN = this.w_LOGIN;
            ,PU___MAC = this.w_MACHINE;
            &i_ccchkf. ;
         where;
            PUCODUTE = this.oParentObject.w_UTE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.oParentObject.w_OKPWD = "RIT"
      i_retcode = 'stop'
      return
    endif
    * --- Controllo Password
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Avviso Password scaduta
    if this.w_VALMAX > 0 
      if this.w_DATSCA < this.w_DATA
        ah_ErrorMsg("La password � scaduta", 48)
        GSUT_KWD(this.oParentObject)
        if NOT this.oParentObject.w_CHANGED
          this.oParentObject.w_OKPWD = "ESC"
          i_retcode = 'stop'
          return
        else
          this.oParentObject.w_CHANGED = .F.
        endif
      else
        if this.w_GGANTI > 0
          * --- Avviso scadenza password
          if this.w_DATSCA - this.w_DATA =< this.w_GGANTI
            if this.w_VALMAX - (this.w_DATA - this.w_DATULT) = 0
              this.w_MSG = "La password scade oggi%0Aggiornare la password ora?"
            else
              if this.w_VALMAX - (this.w_DATA - this.w_DATULT) = 1
                this.w_MSG = "Scadenza password: %1 giorno%0Aggiornare la password ora?"
              else
                this.w_MSG = "Scadenza password: %1 giorni%0Aggiornare la password ora?"
              endif
            endif
            * --- Se l'utente lo vuole pu� cambiare la password. (terzo parametro posto a .f.
            *     per impedire anche ad un evetnuale utente amministratore la modifica
            *     di descrizione / gruppi) - Vincolo funzionale, se si desidera aprire le
            *     funzionalit� complete della maschera inserire cp_IsAdministrator(.t.) al posto
            *     di .f.
            if ah_Yesno( this.w_MSG,"",Alltrim( Str( this.w_VALMAX - ( this.w_DATA - this.w_DATULT ) ) ))
              * --- Sbianco la i_CurForm per evitare che alla pressione del tasto
              *     ESC si chiuda la maschera di autenticazione piuttosto che la maschera per
              *     impostare la password. Tramite questo "sbiancamento" la ESC � di fatto
              *     disabilitata. Il tutto funziona se la finestra cambio password rimane modale
               L_CurForm=i_CurForm 
 i_curform=Null 
 GSUT_KWD(this.oParentObject) 
 i_CurForm=L_CurForm 
              if NOT this.oParentObject.w_CHANGED
                this.oParentObject.w_OKPWD = "RIT"
                i_retcode = 'stop'
                return
              else
                this.oParentObject.w_CHANGED = .F.
              endif
            endif
          endif
        endif
      endif
    endif
    * --- Avviso Account scaduto, se da sblocco amministratore non lo visualizzo
    *     perch� l'ho gia segnalato e l'utente ha fornito una pwd corretta.
    if this.w_ULTACC > 0 AND this.w_ULTACC <= ( this.w_DATA - this.w_DATACC ) And Not this.w_ADMBLK
      ah_ErrorMsg("Account scaduto", 48)
    endif
    * --- Aggiorno ultimo accesso se gestita scadenza account
    if this.w_ULTACC<>0
      * --- Write into POL_UTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.POL_UTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PUDATACC ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'POL_UTE','PUDATACC');
            +i_ccchkf ;
        +" where ";
            +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTE);
               )
      else
        update (i_cTable) set;
            PUDATACC = this.w_DATA;
            &i_ccchkf. ;
         where;
            PUCODUTE = this.oParentObject.w_UTE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,12)]
    this.cWorkTables[1]='POL_SIC'
    this.cWorkTables[2]='POL_UTE'
    this.cWorkTables[3]='CPUSRGRP'
    this.cWorkTables[4]='CPUSERS'
    this.cWorkTables[5]='cpusers'
    this.cWorkTables[6]='GROUPSJ'
    this.cWorkTables[7]='POL_ACC'
    this.cWorkTables[8]='DIPENDEN'
    this.cWorkTables[9]='TAB_CALE'
    this.cWorkTables[10]='CAL_AZIE'
    this.cWorkTables[11]='PAR_AGEN'
    this.cWorkTables[12]='cpusrgrp'
    return(this.OpenAllTables(12))

  proc CloseCursors()
    if used('_Curs_cpusrgrp')
      use in _Curs_cpusrgrp
    endif
    if used('_Curs_Query_Chkute')
      use in _Curs_Query_Chkute
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
