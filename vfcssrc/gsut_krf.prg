* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_krf                                                        *
*              Nome file originario                                            *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-06-18                                                      *
* Last revis.: 2010-12-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_krf",oParentObject))

* --- Class definition
define class tgsut_krf as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 547
  Height = 155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-12-16"
  HelpContextID=115950441
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_krf"
  cComment = "Nome file originario"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FILENAME = space(60)
  o_FILENAME = space(60)
  w_TIPOPERAZ = space(1)
  w_estensione = space(10)
  w_PATH_KRF = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_krfPag1","gsut_krf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFILENAME_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsut_krf
     this.parent.closable=.f.
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_krf
    This.Autocenter=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FILENAME=space(60)
      .w_TIPOPERAZ=space(1)
      .w_estensione=space(10)
      .w_PATH_KRF=space(254)
      .w_FILENAME=oParentObject.w_FILENAME
      .w_TIPOPERAZ=oParentObject.w_TIPOPERAZ
      .w_estensione=oParentObject.w_estensione
      .w_PATH_KRF=oParentObject.w_PATH_KRF
          .DoRTCalc(1,1,.f.)
        .w_TIPOPERAZ = 'N'
    endwith
    this.DoRTCalc(3,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_FILENAME=.w_FILENAME
      .oParentObject.w_TIPOPERAZ=.w_TIPOPERAZ
      .oParentObject.w_estensione=.w_estensione
      .oParentObject.w_PATH_KRF=.w_PATH_KRF
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_FILENAME<>.w_FILENAME
          .Calculate_XQZWFKXMKW()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_XQZWFKXMKW()
    with this
          * --- Aggiungo l'estensione al file originario se non presente
          .w_FILENAME = ForceExt(.w_FILENAME, .w_estensione)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_1.visible=!this.oPgFrm.Page1.oPag.oStr_1_1.mHide()
    this.oPgFrm.Page1.oPag.oFILENAME_1_2.visible=!this.oPgFrm.Page1.oPag.oFILENAME_1_2.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFILENAME_1_2.value==this.w_FILENAME)
      this.oPgFrm.Page1.oPag.oFILENAME_1_2.value=this.w_FILENAME
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_4.RadioValue()==this.w_TIPOPERAZ)
      this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_4.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPOPERAZ = 'N' and ! empty(.w_FILENAME))  and not(.w_TIPOPERAZ#'N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFILENAME_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il nome del file originario")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_krf
      if  this.w_TIPOPERAZ <>'S' and cp_fileexist(Alltrim(This.w_PATH_KRF)+Alltrim(This.w_FILENAME)) and !Ah_Yesno("Nome file gi� presente. Impossibile proseguire. Annullare l'archiviazione?")
         i_bnoChk=.F.
         i_bRes=.F.
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FILENAME = this.w_FILENAME
    return

enddefine

* --- Define pages as container
define class tgsut_krfPag1 as StdContainer
  Width  = 543
  height = 155
  stdWidth  = 543
  stdheight = 155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFILENAME_1_2 as StdField with uid="URWWMRRVJS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FILENAME", cQueryName = "FILENAME",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il nome del file originario",;
    HelpContextID = 12531045,;
   bGlobalFont=.t.,;
    Height=21, Width=412, Left=123, Top=77, InputMask=replicate('X',60)

  func oFILENAME_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPOPERAZ#'N')
    endwith
  endfunc

  func oFILENAME_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TIPOPERAZ = 'N' and ! empty(.w_FILENAME))
    endwith
    return bRes
  endfunc


  add object oBtn_1_3 as StdButton with uid="PXBGFOADEM",left=489, top=104, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 115921690;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTIPOPERAZ_1_4 as StdRadio with uid="VVOOJZTHOU",rtseq=2,rtrep=.f.,left=191, top=32, width=268,height=39;
    , cFormVar="w_TIPOPERAZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oTIPOPERAZ_1_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Salvare il documento con nome diverso"
      this.Buttons(1).HelpContextID = 57348375
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Sovrascrivere il documento"
      this.Buttons(2).HelpContextID = 57348375
      this.Buttons(2).Top=18
      this.SetAll("Width",266)
      this.SetAll("Height",20)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oTIPOPERAZ_1_4.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPOPERAZ_1_4.GetRadio()
    this.Parent.oContained.w_TIPOPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPERAZ_1_4.SetRadio()
    this.Parent.oContained.w_TIPOPERAZ=trim(this.Parent.oContained.w_TIPOPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPERAZ=='N',1,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='S',2,;
      0))
  endfunc

  add object oStr_1_1 as StdString with uid="MCXVWJSFBY",Visible=.t., Left=5, Top=81,;
    Alignment=1, Width=115, Height=18,;
    Caption="Nome file originario:"  ;
  , bGlobalFont=.t.

  func oStr_1_1.mHide()
    with this.Parent.oContained
      return (.w_TIPOPERAZ#'N')
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="IHEHOTEVWC",Visible=.t., Left=11, Top=8,;
    Alignment=0, Width=341, Height=18,;
    Caption="Attenzione il nome file originale � gi� presente, si desidera:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_krf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
