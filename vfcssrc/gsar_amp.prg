* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_amp                                                        *
*              Tipi di pagamento                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_26]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2015-03-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_amp"))

* --- Class definition
define class tgsar_amp as StdForm
  Top    = 66
  Left   = 36

  * --- Standard Properties
  Width  = 587
  Height = 155+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-04"
  HelpContextID=58099863
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  MOD_PAGA_IDX = 0
  CONTI_IDX = 0
  cFile = "MOD_PAGA"
  cKeySelect = "MPCODICE"
  cKeyWhere  = "MPCODICE=this.w_MPCODICE"
  cKeyWhereODBC = '"MPCODICE="+cp_ToStrODBC(this.w_MPCODICE)';

  cKeyWhereODBCqualified = '"MOD_PAGA.MPCODICE="+cp_ToStrODBC(this.w_MPCODICE)';

  cPrg = "gsar_amp"
  cComment = "Tipi di pagamento"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AMP'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MPCODICE = space(10)
  w_MPDESCRI = space(35)
  w_MPTIPPAG = space(2)
  w_MPFLESCL = space(1)
  w_MPSPLPAY = space(1)
  w_MPPAGCBI = space(20)
  w_MPCONCFL = space(15)
  w_TICON = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_PAGA','gsar_amp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ampPag1","gsar_amp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Codice")
      .Pages(1).HelpContextID = 15320026
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMPCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MOD_PAGA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_PAGA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_PAGA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MPCODICE = NVL(MPCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_PAGA where MPCODICE=KeySet.MPCODICE
    *
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_PAGA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_PAGA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_PAGA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MPCODICE',this.w_MPCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_MPCODICE = NVL(MPCODICE,space(10))
        .w_MPDESCRI = NVL( cp_TransLoadField('MPDESCRI') ,space(35))
        .w_MPTIPPAG = NVL(MPTIPPAG,space(2))
        .w_MPFLESCL = NVL(MPFLESCL,space(1))
        .w_MPSPLPAY = NVL(MPSPLPAY,space(1))
        .w_MPPAGCBI = NVL(MPPAGCBI,space(20))
        .w_MPCONCFL = NVL(MPCONCFL,space(15))
          * evitabile
          *.link_1_9('Load')
        .w_TICON = 'G'
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'MOD_PAGA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MPCODICE = space(10)
      .w_MPDESCRI = space(35)
      .w_MPTIPPAG = space(2)
      .w_MPFLESCL = space(1)
      .w_MPSPLPAY = space(1)
      .w_MPPAGCBI = space(20)
      .w_MPCONCFL = space(15)
      .w_TICON = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_MPTIPPAG = 'RD'
          .DoRTCalc(4,4,.f.)
        .w_MPSPLPAY = 'N'
        .DoRTCalc(6,7,.f.)
          if not(empty(.w_MPCONCFL))
          .link_1_9('Full')
          endif
        .w_TICON = 'G'
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_PAGA')
    this.DoRTCalc(9,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMPCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oMPDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oMPTIPPAG_1_3.enabled_(i_bVal)
      .Page1.oPag.oMPFLESCL_1_4.enabled = i_bVal
      .Page1.oPag.oMPSPLPAY_1_5.enabled = i_bVal
      .Page1.oPag.oMPPAGCBI_1_8.enabled = i_bVal
      .Page1.oPag.oMPCONCFL_1_9.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMPCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMPCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MOD_PAGA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPCODICE,"MPCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPDESCRI,"MPDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPTIPPAG,"MPTIPPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPFLESCL,"MPFLESCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPSPLPAY,"MPSPLPAY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPPAGCBI,"MPPAGCBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPCONCFL,"MPCONCFL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    i_lTable = "MOD_PAGA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_PAGA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_STP with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_PAGA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_PAGA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_PAGA')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_PAGA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MPCODICE,MPDESCRI"+cp_TransInsFldName('MPDESCRI')+",MPTIPPAG,MPFLESCL,MPSPLPAY"+;
                  ",MPPAGCBI,MPCONCFL,UTCC,UTCV,UTDC"+;
                  ",UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MPCODICE)+;
                  ","+cp_ToStrODBC(this.w_MPDESCRI)+cp_TransInsFldValue(this.w_MPDESCRI)+;
                  ","+cp_ToStrODBC(this.w_MPTIPPAG)+;
                  ","+cp_ToStrODBC(this.w_MPFLESCL)+;
                  ","+cp_ToStrODBC(this.w_MPSPLPAY)+;
                  ","+cp_ToStrODBC(this.w_MPPAGCBI)+;
                  ","+cp_ToStrODBCNull(this.w_MPCONCFL)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_PAGA')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_PAGA')
        cp_CheckDeletedKey(i_cTable,0,'MPCODICE',this.w_MPCODICE)
        INSERT INTO (i_cTable);
              (MPCODICE,MPDESCRI,MPTIPPAG,MPFLESCL,MPSPLPAY,MPPAGCBI,MPCONCFL,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MPCODICE;
                  ,this.w_MPDESCRI;
                  ,this.w_MPTIPPAG;
                  ,this.w_MPFLESCL;
                  ,this.w_MPSPLPAY;
                  ,this.w_MPPAGCBI;
                  ,this.w_MPCONCFL;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_PAGA_IDX,i_nConn)
      *
      * update MOD_PAGA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_PAGA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MPDESCRI="+cp_TransUpdFldName('MPDESCRI',this.w_MPDESCRI)+;
             ",MPTIPPAG="+cp_ToStrODBC(this.w_MPTIPPAG)+;
             ",MPFLESCL="+cp_ToStrODBC(this.w_MPFLESCL)+;
             ",MPSPLPAY="+cp_ToStrODBC(this.w_MPSPLPAY)+;
             ",MPPAGCBI="+cp_ToStrODBC(this.w_MPPAGCBI)+;
             ",MPCONCFL="+cp_ToStrODBCNull(this.w_MPCONCFL)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_PAGA')
        i_cWhere = cp_PKFox(i_cTable  ,'MPCODICE',this.w_MPCODICE  )
        UPDATE (i_cTable) SET;
              MPDESCRI=this.w_MPDESCRI;
             ,MPTIPPAG=this.w_MPTIPPAG;
             ,MPFLESCL=this.w_MPFLESCL;
             ,MPSPLPAY=this.w_MPSPLPAY;
             ,MPPAGCBI=this.w_MPPAGCBI;
             ,MPCONCFL=this.w_MPCONCFL;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_PAGA_IDX,i_nConn)
      *
      * delete MOD_PAGA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MPCODICE',this.w_MPCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
            .w_TICON = 'G'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMPCONCFL_1_9.enabled = this.oPgFrm.Page1.oPag.oMPCONCFL_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMPPAGCBI_1_8.visible=!this.oPgFrm.Page1.oPag.oMPPAGCBI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oMPCONCFL_1_9.visible=!this.oPgFrm.Page1.oPag.oMPCONCFL_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MPCONCFL
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCONCFL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MPCONCFL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TICON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TICON;
                     ,'ANCODICE',trim(this.w_MPCONCFL))
          select ANTIPCON,ANCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MPCONCFL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MPCONCFL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMPCONCFL_1_9'),i_cWhere,'GSAR_BZC',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TICON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TICON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCONCFL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MPCONCFL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TICON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TICON;
                       ,'ANCODICE',this.w_MPCONCFL)
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCONCFL = NVL(_Link_.ANCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_MPCONCFL = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCONCFL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMPCODICE_1_1.value==this.w_MPCODICE)
      this.oPgFrm.Page1.oPag.oMPCODICE_1_1.value=this.w_MPCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMPDESCRI_1_2.value==this.w_MPDESCRI)
      this.oPgFrm.Page1.oPag.oMPDESCRI_1_2.value=this.w_MPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMPTIPPAG_1_3.RadioValue()==this.w_MPTIPPAG)
      this.oPgFrm.Page1.oPag.oMPTIPPAG_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMPFLESCL_1_4.RadioValue()==this.w_MPFLESCL)
      this.oPgFrm.Page1.oPag.oMPFLESCL_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMPSPLPAY_1_5.RadioValue()==this.w_MPSPLPAY)
      this.oPgFrm.Page1.oPag.oMPSPLPAY_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMPPAGCBI_1_8.value==this.w_MPPAGCBI)
      this.oPgFrm.Page1.oPag.oMPPAGCBI_1_8.value=this.w_MPPAGCBI
    endif
    if not(this.oPgFrm.Page1.oPag.oMPCONCFL_1_9.value==this.w_MPCONCFL)
      this.oPgFrm.Page1.oPag.oMPCONCFL_1_9.value=this.w_MPCONCFL
    endif
    cp_SetControlsValueExtFlds(this,'MOD_PAGA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MPTIPPAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMPTIPPAG_1_3.SetFocus()
            i_bnoObbl = !empty(.w_MPTIPPAG)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ampPag1 as StdContainer
  Width  = 583
  height = 163
  stdWidth  = 583
  stdheight = 163
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMPCODICE_1_1 as StdField with uid="VZQIJBIXAC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MPCODICE", cQueryName = "MPCODICE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipo di pagamento",;
    HelpContextID = 250999285,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=71, Top=11, InputMask=replicate('X',10)

  add object oMPDESCRI_1_2 as StdField with uid="AGZVAMUECG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MPDESCRI", cQueryName = "MPDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .t.,;
    ToolTipText = "Descrizione tipo di pagamento",;
    HelpContextID = 68149745,;
   bGlobalFont=.t.,;
    Height=21, Width=336, Left=176, Top=11, InputMask=replicate('X',35)

  add object oMPTIPPAG_1_3 as StdRadio with uid="EAPNAUUVWS",rtseq=3,rtrep=.f.,left=71, top=41, width=179,height=124;
    , ToolTipText = "Categoria pagamento di appartenenza";
    , cFormVar="w_MPTIPPAG", ButtonCount=7, bObbl=.t., nPag=1;
  , bGlobalFont=.t.

    proc oMPTIPPAG_1_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Anticipazioni /M.AV."
      this.Buttons(1).HelpContextID = 121299443
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Bonifico"
      this.Buttons(2).HelpContextID = 121299443
      this.Buttons(2).Top=17
      this.Buttons(3).Caption="Cambiale/tratta"
      this.Buttons(3).HelpContextID = 121299443
      this.Buttons(3).Top=34
      this.Buttons(4).Caption="R.I.D."
      this.Buttons(4).HelpContextID = 121299443
      this.Buttons(4).Top=51
      this.Buttons(5).Caption="Ric.bancaria/Ri.Ba."
      this.Buttons(5).HelpContextID = 121299443
      this.Buttons(5).Top=68
      this.Buttons(6).Caption="Rimessa diretta"
      this.Buttons(6).HelpContextID = 121299443
      this.Buttons(6).Top=85
      this.Buttons(7).Caption="Compensazione"
      this.Buttons(7).HelpContextID = 121299443
      this.Buttons(7).Top=102
      this.SetAll("Width",177)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Categoria pagamento di appartenenza")
      StdRadio::init()
    endproc

  func oMPTIPPAG_1_3.RadioValue()
    return(iif(this.value =1,'MA',;
    iif(this.value =2,'BO',;
    iif(this.value =3,'CA',;
    iif(this.value =4,'RI',;
    iif(this.value =5,'RB',;
    iif(this.value =6,'RD',;
    iif(this.value =7,'CC',;
    space(2)))))))))
  endfunc
  func oMPTIPPAG_1_3.GetRadio()
    this.Parent.oContained.w_MPTIPPAG = this.RadioValue()
    return .t.
  endfunc

  func oMPTIPPAG_1_3.SetRadio()
    this.Parent.oContained.w_MPTIPPAG=trim(this.Parent.oContained.w_MPTIPPAG)
    this.value = ;
      iif(this.Parent.oContained.w_MPTIPPAG=='MA',1,;
      iif(this.Parent.oContained.w_MPTIPPAG=='BO',2,;
      iif(this.Parent.oContained.w_MPTIPPAG=='CA',3,;
      iif(this.Parent.oContained.w_MPTIPPAG=='RI',4,;
      iif(this.Parent.oContained.w_MPTIPPAG=='RB',5,;
      iif(this.Parent.oContained.w_MPTIPPAG=='RD',6,;
      iif(this.Parent.oContained.w_MPTIPPAG=='CC',7,;
      0)))))))
  endfunc

  add object oMPFLESCL_1_4 as StdCheck with uid="NQQCMTQZBM",rtseq=4,rtrep=.f.,left=280, top=41, caption="Ignora mesi esclusi",;
    ToolTipText = "Se attivo: ignora i mesi e giorni di esclusione dei pagamenti presenti sull'anagrafica cliente/fornitore",;
    HelpContextID = 82362862,;
    cFormVar="w_MPFLESCL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMPFLESCL_1_4.RadioValue()
    return(iif(this.value =1,'N',;
    ' '))
  endfunc
  func oMPFLESCL_1_4.GetRadio()
    this.Parent.oContained.w_MPFLESCL = this.RadioValue()
    return .t.
  endfunc

  func oMPFLESCL_1_4.SetRadio()
    this.Parent.oContained.w_MPFLESCL=trim(this.Parent.oContained.w_MPFLESCL)
    this.value = ;
      iif(this.Parent.oContained.w_MPFLESCL=='N',1,;
      0)
  endfunc

  add object oMPSPLPAY_1_5 as StdCheck with uid="HYXAUSRCPM",rtseq=5,rtrep=.f.,left=280, top=64, caption="Split payment",;
    ToolTipText = "Identifica il tipo pagamento dedicato agli importi iva nella c.d. scissione dei pagamenti",;
    HelpContextID = 125039073,;
    cFormVar="w_MPSPLPAY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMPSPLPAY_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMPSPLPAY_1_5.GetRadio()
    this.Parent.oContained.w_MPSPLPAY = this.RadioValue()
    return .t.
  endfunc

  func oMPSPLPAY_1_5.SetRadio()
    this.Parent.oContained.w_MPSPLPAY=trim(this.Parent.oContained.w_MPSPLPAY)
    this.value = ;
      iif(this.Parent.oContained.w_MPSPLPAY=='S',1,;
      0)
  endfunc

  add object oMPPAGCBI_1_8 as StdField with uid="YWUUKIAVFQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MPPAGCBI", cQueryName = "MPPAGCBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia di pagamento CBI",;
    HelpContextID = 80945649,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=428, Top=81, InputMask=replicate('X',20)

  func oMPPAGCBI_1_8.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S')
    endwith
  endfunc

  add object oMPCONCFL_1_9 as StdField with uid="CNICJKRKWM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MPCONCFL", cQueryName = "MPCONCFL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 195694098,;
   bGlobalFont=.t.,;
    Height=21, Width=115, Left=428, Top=110, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TICON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MPCONCFL"

  func oMPCONCFL_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MPTIPPAG='RD')
    endwith
   endif
  endfunc

  func oMPCONCFL_1_9.mHide()
    with this.Parent.oContained
      return (.w_MPTIPPAG<>'RD')
    endwith
  endfunc

  func oMPCONCFL_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMPCONCFL_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMPCONCFL_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TICON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TICON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMPCONCFL_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"",'',this.parent.oContained
  endproc
  proc oMPCONCFL_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TICON
     i_obj.w_ANCODICE=this.parent.oContained.w_MPCONCFL
     i_obj.ecpSave()
  endproc

  add object oStr_1_6 as StdString with uid="VLXPYWUZWG",Visible=.t., Left=1, Top=11,;
    Alignment=1, Width=66, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="DGJOUSYBSP",Visible=.t., Left=1, Top=41,;
    Alignment=1, Width=66, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="JOEKLIMTNI",Visible=.t., Left=253, Top=113,;
    Alignment=1, Width=171, Height=18,;
    Caption="Conto cassa cash flow:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_MPTIPPAG<>'RD')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="KZMYKYPUDU",Visible=.t., Left=303, Top=84,;
    Alignment=1, Width=121, Height=18,;
    Caption="Tipologia CBI:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_amp','MOD_PAGA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MPCODICE=MOD_PAGA.MPCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
