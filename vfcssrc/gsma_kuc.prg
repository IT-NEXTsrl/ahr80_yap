* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kuc                                                        *
*              Ultimi prezzi per cliente                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_84]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-24                                                      *
* Last revis.: 2012-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kuc",oParentObject))

* --- Class definition
define class tgsma_kuc as StdForm
  Top    = 180
  Left   = 15

  * --- Standard Properties
  Width  = 770
  Height = 262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-30"
  HelpContextID=65698665
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  DOC_MAST_IDX = 0
  MVM_MAST_IDX = 0
  cPrg = "gsma_kuc"
  cComment = "Ultimi prezzi per cliente"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MVSERIAL = space(10)
  o_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_NUMRIF = 0
  w_ARCODART = space(20)
  w_NUMDOCD = 0
  w_SERDOCD = space(10)
  w_DATREGD = ctod('  /  /  ')
  w_CLADOC = space(2)
  w_PARAME = space(3)
  w_FLVEAC = space(1)
  w_MMSERIAL = space(10)
  o_MMSERIAL = space(10)
  w_NUMDOCM = 0
  w_SERDOCM = space(10)
  w_DATREGM = ctod('  /  /  ')
  w_ZoomVEAC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kucPag1","gsma_kuc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomVEAC = this.oPgFrm.Pages(1).oPag.ZoomVEAC
    DoDefault()
    proc Destroy()
      this.w_ZoomVEAC = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='MVM_MAST'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MVSERIAL=space(10)
      .w_CPROWNUM=0
      .w_NUMRIF=0
      .w_ARCODART=space(20)
      .w_NUMDOCD=0
      .w_SERDOCD=space(10)
      .w_DATREGD=ctod("  /  /  ")
      .w_CLADOC=space(2)
      .w_PARAME=space(3)
      .w_FLVEAC=space(1)
      .w_MMSERIAL=space(10)
      .w_NUMDOCM=0
      .w_SERDOCM=space(10)
      .w_DATREGM=ctod("  /  /  ")
      .w_ARCODART=oParentObject.w_ARCODART
      .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
      .oPgFrm.Page1.oPag.ZoomVEAC.Calculate()
        .w_MVSERIAL = .w_ZoomVEAC.getVar("MVSERIAL")
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_MVSERIAL))
          .link_1_8('Full')
        endif
        .w_CPROWNUM = .w_ZoomVEAC.getVar("CPROWNUM")
        .w_NUMRIF = .w_ZoomVEAC.getVar("MVNUMRIF")
          .DoRTCalc(4,8,.f.)
        .w_PARAME = .w_FLVEAC + .w_CLADOC
          .DoRTCalc(10,10,.f.)
        .w_MMSERIAL = .w_ZoomVEAC.getVar("MVSERIAL")
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MMSERIAL))
          .link_1_20('Full')
        endif
    endwith
    this.DoRTCalc(12,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ARCODART=.w_ARCODART
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.ZoomVEAC.Calculate()
            .w_MVSERIAL = .w_ZoomVEAC.getVar("MVSERIAL")
          .link_1_8('Full')
            .w_CPROWNUM = .w_ZoomVEAC.getVar("CPROWNUM")
            .w_NUMRIF = .w_ZoomVEAC.getVar("MVNUMRIF")
        .DoRTCalc(4,8,.t.)
            .w_PARAME = .w_FLVEAC + .w_CLADOC
        .DoRTCalc(10,10,.t.)
            .w_MMSERIAL = .w_ZoomVEAC.getVar("MVSERIAL")
          .link_1_20('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.ZoomVEAC.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNUMDOCD_1_12.visible=!this.oPgFrm.Page1.oPag.oNUMDOCD_1_12.mHide()
    this.oPgFrm.Page1.oPag.oSERDOCD_1_13.visible=!this.oPgFrm.Page1.oPag.oSERDOCD_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDATREGD_1_14.visible=!this.oPgFrm.Page1.oPag.oDATREGD_1_14.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_17.visible=!this.oPgFrm.Page1.oPag.oBtn_1_17.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_19.visible=!this.oPgFrm.Page1.oPag.oBtn_1_19.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOCM_1_21.visible=!this.oPgFrm.Page1.oPag.oNUMDOCM_1_21.mHide()
    this.oPgFrm.Page1.oPag.oSERDOCM_1_22.visible=!this.oPgFrm.Page1.oPag.oSERDOCM_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDATREGM_1_23.visible=!this.oPgFrm.Page1.oPag.oDATREGM_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_1.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomVEAC.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVSERIAL
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATREG,MVCLADOC,MVFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL);
                   +" and MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_MVSERIAL;
                       ,'MVSERIAL',this.w_MVSERIAL)
            select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATREG,MVCLADOC,MVFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVSERIAL = NVL(_Link_.MVSERIAL,space(10))
      this.w_NUMDOCD = NVL(_Link_.MVNUMDOC,0)
      this.w_SERDOCD = NVL(_Link_.MVALFDOC,space(10))
      this.w_DATREGD = NVL(cp_ToDate(_Link_.MVDATREG),ctod("  /  /  "))
      this.w_CLADOC = NVL(_Link_.MVCLADOC,space(2))
      this.w_FLVEAC = NVL(_Link_.MVFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVSERIAL = space(10)
      endif
      this.w_NUMDOCD = 0
      this.w_SERDOCD = space(10)
      this.w_DATREGD = ctod("  /  /  ")
      this.w_CLADOC = space(2)
      this.w_FLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMSERIAL
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
    i_lTable = "MVM_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2], .t., this.MVM_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MMSERIAL,MMNUMDOC,MMALFDOC,MMDATREG";
                   +" from "+i_cTable+" "+i_lTable+" where MMSERIAL="+cp_ToStrODBC(this.w_MMSERIAL);
                   +" and MMSERIAL="+cp_ToStrODBC(this.w_MMSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MMSERIAL',this.w_MMSERIAL;
                       ,'MMSERIAL',this.w_MMSERIAL)
            select MMSERIAL,MMNUMDOC,MMALFDOC,MMDATREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMSERIAL = NVL(_Link_.MMSERIAL,space(10))
      this.w_NUMDOCM = NVL(_Link_.MMNUMDOC,0)
      this.w_SERDOCM = NVL(_Link_.MMALFDOC,space(10))
      this.w_DATREGM = NVL(cp_ToDate(_Link_.MMDATREG),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MMSERIAL = space(10)
      endif
      this.w_NUMDOCM = 0
      this.w_SERDOCM = space(10)
      this.w_DATREGM = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MMSERIAL,1)+'\'+cp_ToStr(_Link_.MMSERIAL,1)
      cp_ShowWarn(i_cKey,this.MVM_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMDOCD_1_12.value==this.w_NUMDOCD)
      this.oPgFrm.Page1.oPag.oNUMDOCD_1_12.value=this.w_NUMDOCD
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDOCD_1_13.value==this.w_SERDOCD)
      this.oPgFrm.Page1.oPag.oSERDOCD_1_13.value=this.w_SERDOCD
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREGD_1_14.value==this.w_DATREGD)
      this.oPgFrm.Page1.oPag.oDATREGD_1_14.value=this.w_DATREGD
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOCM_1_21.value==this.w_NUMDOCM)
      this.oPgFrm.Page1.oPag.oNUMDOCM_1_21.value=this.w_NUMDOCM
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDOCM_1_22.value==this.w_SERDOCM)
      this.oPgFrm.Page1.oPag.oSERDOCM_1_22.value=this.w_SERDOCM
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREGM_1_23.value==this.w_DATREGM)
      this.oPgFrm.Page1.oPag.oDATREGM_1_23.value=this.w_DATREGM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVSERIAL = this.w_MVSERIAL
    this.o_MMSERIAL = this.w_MMSERIAL
    return

enddefine

* --- Define pages as container
define class tgsma_kucPag1 as StdContainer
  Width  = 766
  height = 262
  stdWidth  = 766
  stdheight = 262
  resizeXpos=624
  resizeYpos=137
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_1 as cp_runprogram with uid="SFJAKFPUIG",left=6, top=263, width=142,height=19,;
    caption='GSMA_BCF(C)',;
   bGlobalFont=.t.,;
    prg="GSMA_BCF('C')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 72253996


  add object ZoomVEAC as cp_zoombox with uid="IINBHLOGZY",left=2, top=8, width=761,height=203,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSMA_KUC.DOC_MAST_VZM",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="DOC_MAST",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 112298982


  add object oBtn_1_3 as StdButton with uid="KNQTJANIEM",left=711, top=214, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 58381242;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMDOCD_1_12 as StdField with uid="JLHABGZCMB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMDOCD", cQueryName = "NUMDOCD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento selezionato",;
    HelpContextID = 72265430,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=199, Top=218

  func oNUMDOCD_1_12.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-10)
    endwith
  endfunc

  add object oSERDOCD_1_13 as StdField with uid="AKTVRTHJSQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SERDOCD", cQueryName = "SERDOCD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento selezionato",;
    HelpContextID = 72281894,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=325, Top=218, InputMask=replicate('X',10)

  func oSERDOCD_1_13.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-10)
    endwith
  endfunc

  add object oDATREGD_1_14 as StdField with uid="QQILFVZETQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATREGD", cQueryName = "DATREGD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione documento selezionato",;
    HelpContextID = 129829430,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=463, Top=218

  func oDATREGD_1_14.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-10)
    endwith
  endfunc


  add object oBtn_1_17 as StdButton with uid="FRSHLUKCSC",left=660, top=214, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento/movimento di magazzino associato alla riga selezionata";
    , HelpContextID = 106877281;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_MVSERIAL, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_17.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NUMRIF=-10)
     endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="EMKIAWJZRA",left=660, top=214, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento/movimento di magazzino associato alla riga selezionata";
    , HelpContextID = 106877281;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_MMSERIAL, .w_NUMRIF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MMSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_19.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NUMRIF=-20)
     endwith
    endif
  endfunc

  add object oNUMDOCM_1_21 as StdField with uid="UJLEEMRLOL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NUMDOCM", cQueryName = "NUMDOCM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento selezionato",;
    HelpContextID = 196170026,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=199, Top=218

  func oNUMDOCM_1_21.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-20)
    endwith
  endfunc

  add object oSERDOCM_1_22 as StdField with uid="SYVSRAKKER",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SERDOCM", cQueryName = "SERDOCM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento selezionato",;
    HelpContextID = 196153562,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=325, Top=218, InputMask=replicate('X',10)

  func oSERDOCM_1_22.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-20)
    endwith
  endfunc

  add object oDATREGM_1_23 as StdField with uid="CDGEQEMRVS",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATREGM", cQueryName = "DATREGM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione documento selezionato",;
    HelpContextID = 138606026,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=463, Top=218

  func oDATREGM_1_23.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-20)
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="KYFYOIVTHS",Visible=.t., Left=7, Top=218,;
    Alignment=1, Width=58, Height=19,;
    Caption="Rif.doc.:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="PGJHBENFDX",Visible=.t., Left=135, Top=219,;
    Alignment=1, Width=61, Height=18,;
    Caption="Num. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="SISDCNJTXV",Visible=.t., Left=437, Top=219,;
    Alignment=1, Width=22, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="XQMHUIITOL",Visible=.t., Left=317, Top=218,;
    Alignment=2, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kuc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
