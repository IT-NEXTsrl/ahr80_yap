* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_bas                                                        *
*              Analisi struttura storico                                       *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_50]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-21                                                      *
* Last revis.: 2002-05-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgssr_bas",oParentObject)
return(i_retval)

define class tgssr_bas as StdBatch
  * --- Local variables
  w_INLINEA = space(40)
  w_STORICO = space(40)
  w_NUMCAMLIN = 0
  w_NUMCAMSTO = 0
  w_CONTA = 0
  w_POS = 0
  w_COMMENT = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Analizza la struttura degli archivi da storicizzare con i corrispondenti
    *     storici e visualizza eventuali differenze di tracciato:
    *     Ad ora verifica:
    *     1) campo non presente nell'archivio storico e presente in Linea
    *     2) campo non presente nell'archivio in linea e presente nello storico
    Create Cursor __Tmp__ (Tabella C(50) , Campo C(50), Note C(50))
    Cur = WrCursor ("__Tmp__")
    * --- Contabilit�
    if Not Empty( this.oParentObject.w_PNT_MAST )
      this.w_INLINEA = this.oParentObject.w_PNT_MAST
      this.w_STORICO = "PNTSMAST"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_PNT_DETT )
      this.w_INLINEA = this.oParentObject.w_PNT_DETT
      this.w_STORICO = "PNTSDETT"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_PNT_IVA )
      this.w_INLINEA = this.oParentObject.w_PNT_IVA
      this.w_STORICO = "PNTSIVA"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_PAR_TITE )
      this.w_INLINEA = this.oParentObject.w_PAR_TITE
      this.w_STORICO = "PARSTITE"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_MOVICOST )
      this.w_INLINEA = this.oParentObject.w_MOVICOST
      this.w_STORICO = "MOVISCOST"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_SCA_VARI )
      this.w_INLINEA = this.oParentObject.w_SCA_VARI
      this.w_STORICO = "SCASVARI"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_PNT_CESP )
      this.w_INLINEA = this.oParentObject.w_PNT_CESP
      this.w_STORICO = "PNTSCESP"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_CCM_DETT )
      this.w_INLINEA = this.oParentObject.w_CCM_DETT
      this.w_STORICO = "CCMSDETT"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Logistica
    if Not Empty( this.oParentObject.w_DOC_MAST )
      this.w_INLINEA = this.oParentObject.w_DOC_MAST
      this.w_STORICO = "DOCSMAST"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_DOC_DETT )
      this.w_INLINEA = this.oParentObject.w_DOC_DETT
      this.w_STORICO = "DOCSDETT"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_DOC_RATE )
      this.w_INLINEA = this.oParentObject.w_DOC_RATE
      this.w_STORICO = "DOCSRATE"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_MVM_MAST )
      this.w_INLINEA = this.oParentObject.w_MVM_MAST
      this.w_STORICO = "MVMSMAST"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_MVM_DETT )
      this.w_INLINEA = this.oParentObject.w_MVM_DETT
      this.w_STORICO = "MVMSDETT"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_MOVILOTT )
      this.w_INLINEA = this.oParentObject.w_MOVILOTT
      this.w_STORICO = "MOVSLOTT"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_MOVIMATR) AND g_MATR="S"
      this.w_INLINEA = this.oParentObject.w_MOVIMATR
      this.w_STORICO = "MOVSMATR"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_LOTTIART ) AND g_PERLOT="S"
      this.w_INLINEA = this.oParentObject.w_LOTTIART
      this.w_STORICO = "LOTSIART"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty( this.oParentObject.w_MATRICOL ) AND g_MATR="S"
      this.w_INLINEA = this.oParentObject.w_MATRICOL
      this.w_STORICO = "STO_MATR"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Produzione
    if g_DIBA="S"
      if Not Empty( this.oParentObject.w_ODL_MAST )
        this.w_INLINEA = this.oParentObject.w_ODL_MAST
        this.w_STORICO = "ODLSMAST"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Not Empty( this.oParentObject.w_ODL_DETT )
        this.w_INLINEA = this.oParentObject.w_ODL_DETT
        this.w_STORICO = "ODLSDETT"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Not Empty( this.oParentObject.w_ODL_CICL )
        this.w_INLINEA = this.oParentObject.w_ODL_CICL
        this.w_STORICO = "ODLSCICL"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Not Empty( this.oParentObject.w_ODL_RISO )
        this.w_INLINEA = this.oParentObject.w_ODL_RISO
        this.w_STORICO = "ODLSRISO"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Not Empty( this.oParentObject.w_SCCI_ASF )
        this.w_INLINEA = this.oParentObject.w_SCCI_ASF
        this.w_STORICO = "SCCISASF"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Not Empty( this.oParentObject.w_AVA_PROD )
        this.w_INLINEA = this.oParentObject.w_AVA_PROD
        this.w_STORICO = "AVASPROD"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Not Empty( this.oParentObject.w_DIC_PROD )
        this.w_INLINEA = this.oParentObject.w_DIC_PROD
        this.w_STORICO = "DICSPROD"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Not Empty( this.oParentObject.w_DIC_REPA )
        this.w_INLINEA = this.oParentObject.w_DIC_REPA
        this.w_STORICO = "DICSREPA"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Not Empty( this.oParentObject.w_DIC_VALO )
        this.w_INLINEA = this.oParentObject.w_DIC_VALO
        this.w_STORICO = "DICSVALO"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Svuoto lo zoom
    Select (this.oParentObject.w_ZOOM.cCursor)
    DELETE FROM ( this.oParentObject.w_ZOOM.cCursor )
    * --- Riempio lo Zoom
    Select __TMP__
    Go Top
    Scan
    Scatter Memvar
    Select (this.oParentObject.w_ZOOM.cCursor)
    Append blank
    Gather memvar
    Endscan
    * --- Lo faccio posizionare sul primo documento
    select ( this.oParentObject.w_ZOOM.cCursor )
    go top
    * --- Refresh della Griglia
    this.oparentobject.w_zoom.grd.refresh
    Use in __tmp__
    ah_ErrorMsg("Analisi strutture terminata",,"")
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Analisi
     
 i_nidx=cp_OpenTable( this.w_INLINEA ) 
 i_nConn=i_TableProp[i_nidx,3] 
 i_cTable=cp_SetAzi(i_TableProp[i_nidx,2])
    if i_nConn<>0
      sqlexec( i_nConn , "Select * From "+i_cTable+" where 1=0 ", "Struct")
    else
      i_retcode = 'stop'
      return
    endif
    this.w_NUMCAMLIN = Afields( InLinea )
    Use in struct
     
 i_nidx=cp_OpenTable( this.w_STORICO ) 
 i_nConn=i_TableProp[i_nidx,3] 
 i_cTable=cp_SetAzi(i_TableProp[i_nidx,2])
    sqlexec( i_nConn , "Select * From "+i_ctable+" where 1=0 ", "Struct")
    this.w_NUMCAMSTO = Afields( Storico )
    Use in struct
    * --- Prendo solo la prima colonna, quell che contiene il nome dei campi
     
 Dimension CampiInLinea [ this.w_NUMCAMLIN ] 
 Dimension CampiStorico [ this.w_NUMCAMSTO ] 
    this.w_CONTA = 1
    do while this.w_CONTA<= this.w_NUMCAMLIN
       
 CampiInLinea [ this.w_CONTA ] = InLinea [ this.w_CONTA , 1 ]
      this.w_CONTA = this.w_CONTA + 1
    enddo
    this.w_CONTA = 1
    do while this.w_CONTA<= this.w_NUMCAMSTO
       
 CampiStorico [ this.w_CONTA ] = Storico [ this.w_CONTA , 1 ]
      this.w_CONTA = this.w_CONTA + 1
    enddo
    * --- Ordino l'array per il nome del campo
     
 Asort ( CampiInLinea , 1) 
 Asort ( CampiStorico , 1)
    this.w_CONTA = 1
    do while this.w_CONTA<=this.w_NUMCAMLIN 
      * --- Ricerco nello storico il campo presente in linea se esiste li cancello tutti e due
      this.w_POS = Ascan ( CampiStorico , CampiInLinea[ this.w_CONTA ] )
      if this.w_POS<>0
        * --- Elimino ambedue le entrate
         
 Adel ( CampiInLinea , this.w_CONTA ) 
 Adel( CampiStorico , this.w_POS )
        this.w_NUMCAMLIN = this.w_NUMCAMLIN - 1
      else
        this.w_CONTA = this.w_CONTA + 1
      endif
    enddo
    * --- Aggiono al cursore di stampa i campi (prima i campi in pi� dell'archivio in linea)
    this.w_CONTA = 1
    do while this.w_CONTA<=this.w_NUMCAMLIN 
       
 Fld = i_Dcx.GetFieldIdx(this.w_INLINEA,CampiInLInea[ this.w_CONTA ]) 
 Comment=IIF(fLD>0,i_dcx.GetFieldDescr(this.w_INLINEA, Fld )," ")
      Insert Into __Tmp__ (Tabella , Campo , Note ) Value (this.w_INLINEA , CampiInLInea[ this.w_CONTA ] + " "+ Alltrim(Comment) , ah_Msgformat("Non presente nell'archivio storico") )
      this.w_CONTA = this.w_CONTA + 1
    enddo
    if this.oParentObject.w_TIPO="T" Or ( this.w_CONTA>1 And this.oParentObject.w_TIPO="S" )
      Insert Into __Tmp__ (Tabella , Campo , Note ) Value (this.w_INLINEA , "-----------------" , Alltrim( Str( this.w_CONTA-1 ) )+" " + ah_Msgformat("Campi da verificare"))
    endif
    * --- Campi dello storico
    this.w_CONTA = 1
    do while this.w_CONTA<=this.w_NUMCAMSTO
      if Type( "CampiStorico[ This.w_CONTA ]")="L"
        * --- Esco dal ciclo ho raggiunto un elemento dell'array non valorizzato
        this.w_NUMCAMSTO = this.w_CONTA -1
      else
         
 Fld = i_Dcx.GetFieldIdx(this.w_STORICO, CampiStorico[ this.w_CONTA ]) 
 Comment=IIF(FLD>0,i_dcx.GetFieldDescr(this.w_STORICO, Fld )," ")
        Insert Into __Tmp__ (Tabella , Campo , Note ) Value (this.w_STORICO , CampiStorico[ this.w_CONTA ] + " "+ Alltrim(Comment) , ah_Msgformat("Non presente nell'archivio in linea") )
        this.w_CONTA = this.w_CONTA + 1
      endif
    enddo
    if this.oParentObject.w_TIPO="T" Or ( this.w_CONTA>1 And this.oParentObject.w_TIPO="S" )
      Insert Into __Tmp__ (Tabella , Campo , Note ) Value (this.w_STORICO , "-----------------" , Alltrim( Str( this.w_CONTA-1 ) ) + " " +ah_Msgformat("Campi da verificare") )
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
