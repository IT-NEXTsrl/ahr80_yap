* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_aot                                                        *
*              Categorie attributi                                             *
*                                                                              *
*      Author: Maurizio Rossetti                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2007-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_aot"))

* --- Class definition
define class tgsar_aot as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 433
  Height = 101+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-31"
  HelpContextID=176781161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  CAT_ATTR_IDX = 0
  TIP_CATT_IDX = 0
  cFile = "CAT_ATTR"
  cKeySelect = "CTCODICE"
  cKeyWhere  = "CTCODICE=this.w_CTCODICE"
  cKeyWhereODBC = '"CTCODICE="+cp_ToStrODBC(this.w_CTCODICE)';

  cKeyWhereODBCqualified = '"CAT_ATTR.CTCODICE="+cp_ToStrODBC(this.w_CTCODICE)';

  cPrg = "gsar_aot"
  cComment = "Categorie attributi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CTCODICE = space(10)
  w_CTDESCRI = space(35)
  w_CTTIPCAT = space(5)
  w_CTTIPDES = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAT_ATTR','gsar_aot')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_aotPag1","gsar_aot",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Categoria attributi")
      .Pages(1).HelpContextID = 85225553
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCTCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_CATT'
    this.cWorkTables[2]='CAT_ATTR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAT_ATTR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAT_ATTR_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CTCODICE = NVL(CTCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAT_ATTR where CTCODICE=KeySet.CTCODICE
    *
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAT_ATTR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAT_ATTR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAT_ATTR '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CTCODICE',this.w_CTCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CTTIPDES = space(35)
        .w_CTCODICE = NVL(CTCODICE,space(10))
        .w_CTDESCRI = NVL(CTDESCRI,space(35))
        .w_CTTIPCAT = NVL(CTTIPCAT,space(5))
          .link_1_5('Load')
        cp_LoadRecExtFlds(this,'CAT_ATTR')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CTCODICE = space(10)
      .w_CTDESCRI = space(35)
      .w_CTTIPCAT = space(5)
      .w_CTTIPDES = space(35)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
          if not(empty(.w_CTTIPCAT))
          .link_1_5('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAT_ATTR')
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCTCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCTDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCTTIPCAT_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCTCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCTCODICE_1_1.enabled = .t.
        .Page1.oPag.oCTDESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAT_ATTR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CTCODICE,"CTCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CTDESCRI,"CTDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CTTIPCAT,"CTTIPCAT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    i_lTable = "CAT_ATTR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAT_ATTR_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsof_sca with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAT_ATTR_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAT_ATTR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAT_ATTR')
        i_extval=cp_InsertValODBCExtFlds(this,'CAT_ATTR')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CTCODICE,CTDESCRI,CTTIPCAT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CTCODICE)+;
                  ","+cp_ToStrODBC(this.w_CTDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_CTTIPCAT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAT_ATTR')
        i_extval=cp_InsertValVFPExtFlds(this,'CAT_ATTR')
        cp_CheckDeletedKey(i_cTable,0,'CTCODICE',this.w_CTCODICE)
        INSERT INTO (i_cTable);
              (CTCODICE,CTDESCRI,CTTIPCAT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CTCODICE;
                  ,this.w_CTDESCRI;
                  ,this.w_CTTIPCAT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAT_ATTR_IDX,i_nConn)
      *
      * update CAT_ATTR
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAT_ATTR')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CTDESCRI="+cp_ToStrODBC(this.w_CTDESCRI)+;
             ",CTTIPCAT="+cp_ToStrODBCNull(this.w_CTTIPCAT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAT_ATTR')
        i_cWhere = cp_PKFox(i_cTable  ,'CTCODICE',this.w_CTCODICE  )
        UPDATE (i_cTable) SET;
              CTDESCRI=this.w_CTDESCRI;
             ,CTTIPCAT=this.w_CTTIPCAT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAT_ATTR_IDX,i_nConn)
      *
      * delete CAT_ATTR
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CTCODICE',this.w_CTCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CTTIPCAT
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CTTIPCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOC',True,'TIP_CATT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CTTIPCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CTTIPCAT))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CTTIPCAT)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CTTIPCAT) and !this.bDontReportError
            deferred_cp_zoom('TIP_CATT','*','TCCODICE',cp_AbsName(oSource.parent,'oCTTIPCAT_1_5'),i_cWhere,'GSAR_AOC',"Tipi categorie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CTTIPCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CTTIPCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CTTIPCAT)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CTTIPCAT = NVL(_Link_.TCCODICE,space(5))
      this.w_CTTIPDES = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CTTIPCAT = space(5)
      endif
      this.w_CTTIPDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CTTIPCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCTCODICE_1_1.value==this.w_CTCODICE)
      this.oPgFrm.Page1.oPag.oCTCODICE_1_1.value=this.w_CTCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCTDESCRI_1_3.value==this.w_CTDESCRI)
      this.oPgFrm.Page1.oPag.oCTDESCRI_1_3.value=this.w_CTDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCTTIPCAT_1_5.value==this.w_CTTIPCAT)
      this.oPgFrm.Page1.oPag.oCTTIPCAT_1_5.value=this.w_CTTIPCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCTTIPDES_1_7.value==this.w_CTTIPDES)
      this.oPgFrm.Page1.oPag.oCTTIPDES_1_7.value=this.w_CTTIPDES
    endif
    cp_SetControlsValueExtFlds(this,'CAT_ATTR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CTCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCTCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CTCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CTTIPCAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCTTIPCAT_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CTTIPCAT)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_aotPag1 as StdContainer
  Width  = 429
  height = 101
  stdWidth  = 429
  stdheight = 101
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCTCODICE_1_1 as StdField with uid="QRKJLYFFVD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CTCODICE", cQueryName = "CTCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria attributi",;
    HelpContextID = 50991467,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=84, Left=95, Top=11, InputMask=replicate('X',10)

  add object oCTDESCRI_1_3 as StdField with uid="FFOYEHXTEE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CTDESCRI", cQueryName = "CTDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 233841007,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=95, Top=38, InputMask=replicate('X',35)

  add object oCTTIPCAT_1_5 as StdField with uid="CYSNQRRAXA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CTTIPCAT", cQueryName = "CTTIPCAT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria",;
    HelpContextID = 231022970,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=95, Top=65, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_CATT", cZoomOnZoom="GSAR_AOC", oKey_1_1="TCCODICE", oKey_1_2="this.w_CTTIPCAT"

  func oCTTIPCAT_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCTTIPCAT_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCTTIPCAT_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_CATT','*','TCCODICE',cp_AbsName(this.parent,'oCTTIPCAT_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOC',"Tipi categorie",'',this.parent.oContained
  endproc
  proc oCTTIPCAT_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CTTIPCAT
     i_obj.ecpSave()
  endproc

  add object oCTTIPDES_1_7 as StdField with uid="MVMXUNGCDH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CTTIPDES", cQueryName = "CTTIPDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 247800185,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=161, Top=65, InputMask=replicate('X',35)

  add object oStr_1_2 as StdString with uid="ECZUMZMPXV",Visible=.t., Left=4, Top=11,;
    Alignment=1, Width=89, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="AJGVYSTGLO",Visible=.t., Left=4, Top=38,;
    Alignment=1, Width=89, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="VAEEKIGFMZ",Visible=.t., Left=4, Top=65,;
    Alignment=1, Width=89, Height=18,;
    Caption="Tipo categoria:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_aot','CAT_ATTR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CTCODICE=CAT_ATTR.CTCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
