* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bvm                                                        *
*              Visualizzazione documenti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2000-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bvm",oParentObject)
return(i_retval)

define class tgsac_bvm as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LANCIA AL NOTIFYEVENT (da GSAC_SZM)
    this.w_ZOOM = this.oParentObject.w_ZoomDoc
    This.OparentObject.NotifyEvent("Esegui")
    this.oParentObject.w_TOTCAR = 0
    this.oParentObject.w_TOTSCA = 0
    * --- Legge Totali Carichi, Scarichi
    NC = this.w_Zoom.cCursor
    SELECT &NC
    GO TOP
    SUM QTACAR, QTASCA TO this.oParentObject.w_TOTCAR, this.oParentObject.w_TOTSCA
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
