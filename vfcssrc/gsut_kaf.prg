* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kaf                                                        *
*              Aggiungi file                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-13                                                      *
* Last revis.: 2008-09-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kaf",oParentObject))

* --- Class definition
define class tgsut_kaf as StdForm
  Top    = 102
  Left   = 114

  * --- Standard Properties
  Width  = 529
  Height = 187
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-01"
  HelpContextID=132727657
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kaf"
  cComment = "Aggiungi file"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DESCRIZ = space(120)
  w_FILE = space(20)
  w_estensione = space(5)
  w_appoggio = space(5)
  w_indice = space(5)
  w_numfile = 0
  w_punto = space(1)
  w_NUOVONOME = space(250)
  w_PRINCIPALE = space(1)
  w_SAVE = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsut_kaf
  proc ecpQuit()
    this.oparentobject.w_save="N"
    doDefault()
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kafPag1","gsut_kaf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDESCRIZ_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DESCRIZ=space(120)
      .w_FILE=space(20)
      .w_estensione=space(5)
      .w_appoggio=space(5)
      .w_indice=space(5)
      .w_numfile=0
      .w_punto=space(1)
      .w_NUOVONOME=space(250)
      .w_PRINCIPALE=space(1)
      .w_SAVE=space(1)
      .w_DESCRIZ=oParentObject.w_DESCRIZ
      .w_FILE=oParentObject.w_FILE
      .w_estensione=oParentObject.w_estensione
      .w_indice=oParentObject.w_indice
      .w_numfile=oParentObject.w_numfile
      .w_NUOVONOME=oParentObject.w_NUOVONOME
      .w_PRINCIPALE=oParentObject.w_PRINCIPALE
      .w_SAVE=oParentObject.w_SAVE
          .DoRTCalc(1,3,.f.)
        .w_appoggio = iif(Empty(.w_DESCRIZ), "."+.w_indice,'')
          .DoRTCalc(5,6,.f.)
        .w_punto = iif(not Empty(.w_DESCRIZ),".","")
        .w_NUOVONOME = iif(.w_PRINCIPALE="N" AND .w_numfile<>0,.w_FILE+.w_punto+alltrim(.w_DESCRIZ)+.w_appoggio+.w_estensione, (iif (.w_PRINCIPALE="N" AND .w_numfile=0,.w_FILE+.w_punto+alltrim(.w_DESCRIZ)+.w_estensione,.w_FILE+.w_estensione)))
          .DoRTCalc(9,9,.f.)
        .w_SAVE = "S"
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DESCRIZ=.w_DESCRIZ
      .oParentObject.w_FILE=.w_FILE
      .oParentObject.w_estensione=.w_estensione
      .oParentObject.w_indice=.w_indice
      .oParentObject.w_numfile=.w_numfile
      .oParentObject.w_NUOVONOME=.w_NUOVONOME
      .oParentObject.w_PRINCIPALE=.w_PRINCIPALE
      .oParentObject.w_SAVE=.w_SAVE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
            .w_appoggio = iif(Empty(.w_DESCRIZ), "."+.w_indice,'')
        .DoRTCalc(5,6,.t.)
            .w_punto = iif(not Empty(.w_DESCRIZ),".","")
            .w_NUOVONOME = iif(.w_PRINCIPALE="N" AND .w_numfile<>0,.w_FILE+.w_punto+alltrim(.w_DESCRIZ)+.w_appoggio+.w_estensione, (iif (.w_PRINCIPALE="N" AND .w_numfile=0,.w_FILE+.w_punto+alltrim(.w_DESCRIZ)+.w_estensione,.w_FILE+.w_estensione)))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDESCRIZ_1_1.enabled = this.oPgFrm.Page1.oPag.oDESCRIZ_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDESCRIZ_1_1.value==this.w_DESCRIZ)
      this.oPgFrm.Page1.oPag.oDESCRIZ_1_1.value=this.w_DESCRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oNUOVONOME_1_8.value==this.w_NUOVONOME)
      this.oPgFrm.Page1.oPag.oNUOVONOME_1_8.value=this.w_NUOVONOME
    endif
    if not(this.oPgFrm.Page1.oPag.oPRINCIPALE_1_11.RadioValue()==this.w_PRINCIPALE)
      this.oPgFrm.Page1.oPag.oPRINCIPALE_1_11.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(!(('*' $ .w_DESCRIZ) or ('/' $ .w_DESCRIZ) or ('\' $ .w_DESCRIZ) or ('?' $ .w_DESCRIZ) or ('<' $ .w_DESCRIZ) or ('>' $ .w_DESCRIZ) or ('"' $ .w_DESCRIZ) or ('&' $ .w_DESCRIZ) or ('|' $ .w_DESCRIZ) or ('^' $ .w_DESCRIZ)))  and (.w_PRINCIPALE = "N")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDESCRIZ_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La descrizione non pu� contenere i seguenti caratteri: \, /, *,?, <, >, &, |, ^, "+'"'+"")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kafPag1 as StdContainer
  Width  = 525
  height = 187
  stdWidth  = 525
  stdheight = 187
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESCRIZ_1_1 as StdField with uid="GDYJUWISNI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DESCRIZ", cQueryName = "DESCRIZ",;
    bObbl = .f. , nPag = 1, value=space(120), bMultilanguage =  .f.,;
    sErrorMsg = "La descrizione non pu� contenere i seguenti caratteri: \, /, *,?, <, >, &, |, ^, "+'"'+"",;
    ToolTipText = "Descrizione del file",;
    HelpContextID = 159435210,;
   bGlobalFont=.t.,;
    Height=21, Width=429, Left=90, Top=60, InputMask=replicate('X',120)

  func oDESCRIZ_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRINCIPALE = "N")
    endwith
   endif
  endfunc

  func oDESCRIZ_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!(('*' $ .w_DESCRIZ) or ('/' $ .w_DESCRIZ) or ('\' $ .w_DESCRIZ) or ('?' $ .w_DESCRIZ) or ('<' $ .w_DESCRIZ) or ('>' $ .w_DESCRIZ) or ('"' $ .w_DESCRIZ) or ('&' $ .w_DESCRIZ) or ('|' $ .w_DESCRIZ) or ('^' $ .w_DESCRIZ)))
    endwith
    return bRes
  endfunc

  add object oNUOVONOME_1_8 as StdField with uid="XWGDWOHIWY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NUOVONOME", cQueryName = "NUOVONOME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Nuovo nome del file",;
    HelpContextID = 190974835,;
   bGlobalFont=.t.,;
    Height=21, Width=509, Left=9, Top=115, InputMask=replicate('X',250)


  add object oBtn_1_9 as StdButton with uid="XQKGCSTQHP",left=420, top=139, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 226653718;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (iif(.w_PRINCIPALE='N',not(empty(.w_DESCRIZ)),.T.))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="YPTRHGBNFI",left=471, top=139, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per sospendere operazione";
    , HelpContextID = 226653718;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPRINCIPALE_1_11 as StdCheck with uid="HKVTVCEIKP",rtseq=9,rtrep=.f.,left=20, top=28, caption="File principale",;
    ToolTipText = "Se attivo il file selezionato in precedenza � messo come principale",;
    HelpContextID = 93974007,;
    cFormVar="w_PRINCIPALE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRINCIPALE_1_11.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPRINCIPALE_1_11.GetRadio()
    this.Parent.oContained.w_PRINCIPALE = this.RadioValue()
    return .t.
  endfunc

  func oPRINCIPALE_1_11.SetRadio()
    this.Parent.oContained.w_PRINCIPALE=trim(this.Parent.oContained.w_PRINCIPALE)
    this.value = ;
      iif(this.Parent.oContained.w_PRINCIPALE=="S",1,;
      0)
  endfunc

  add object oStr_1_12 as StdString with uid="URLRWMTHXT",Visible=.t., Left=8, Top=63,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RLVGLJFVOY",Visible=.t., Left=12, Top=95,;
    Alignment=0, Width=236, Height=18,;
    Caption="Nome con cui verr� registrato con il file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="KHJCGBTRUR",Visible=.t., Left=9, Top=5,;
    Alignment=0, Width=90, Height=18,;
    Caption="Attributi file"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_13 as StdBox with uid="MRRDFCFBRW",left=8, top=88, width=501,height=2

  add object oBox_1_16 as StdBox with uid="CHQFYKITKC",left=8, top=22, width=501,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kaf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
