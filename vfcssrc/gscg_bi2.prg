* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bi2                                                        *
*              Legge partite della distinta                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_18]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-08                                                      *
* Last revis.: 2000-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bi2",oParentObject)
return(i_retval)

define class tgscg_bi2 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue la lettura delle Partite/Scadenze in Distinta (Lanciato da GSTE_ADI) durante l'evento Load
    * --- Carico lo Zoom solo se non sono in Caricamento
    if this.oParentObject.cFunction="Load"
      This.bUpdateParentObject=.f.
    endif
    if this.oParentObject.w_FASE=1
      * --- Se Load a seguito di salvataggio da GSCG_BI3 (TASTO RICERCA) non
      *     occorre ri eseguire lo zoom...
      i_retcode = 'stop'
      return
    endif
    * --- Inizializza le Depends per non Ricalcolare i Campi dopo la Load...
    This.OparentObject.SaveDependsOn()
    * --- Lancia lo Zoom
    This.OparentObject.NotifyEvent("Legge")
     
 UPDATE ( this.oParentObject.w_CalcZoom .cCursor ) SET SEGNO=IIF(TOTIMP<0, "A", "D") WHERE SEGNO ="X" 
 SELECT ( this.oParentObject.w_CalcZoom.cCursor ) 
 GO TOP
    this.oParentObject.w_CalcZoom.Refresh()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
