* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpv                                                        *
*              Esportazione verso gestione privacy                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][22]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-10-20                                                      *
* Last revis.: 2005-10-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpv",oParentObject)
return(i_retval)

define class tgsar_bpv as StdBatch
  * --- Local variables
  w_TYPE = space(10)
  w_FILEPOINTER = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ESPORTAZIONE VERSO GESTIONE PRIVACY
    * --- LANCIATO DA GSAR_KPV
    this.w_TYPE = "SDF"
    VQ_EXEC("QUERY\GSAR_BPV.VQR" , THIS , "GSAR_BPV" )
    * --- SOSTITUISCE ";"', UTILIZZATI COME SEPARATORI NEL FILE DI DESTINAZIONE, CON ","
    * --- METTE LA DATA IN FORMATO AAAAMMGG
    do case
      case this.w_TYPE = "SDF"
        SELECT ; 
 ANTIPCON , ; 
 ANPERFIS , ; 
 LEFT( NVL( ANCOGNOM , SPACE ( 40 ) ) + SPACE ( 40 ) , 40 ) AS ANCOGNOM , ; 
 LEFT( NVL( AN__NOME , SPACE ( 40 ) ) + SPACE ( 40 ) , 40 ) AS AN__NOME , ; 
 LEFT( NVL( ANCODFIS , SPACE ( 16 ) ) + SPACE ( 16 ) , 16 ) AS ANCODFIS , ; 
 LEFT( NVL( ANPARIVA , SPACE ( 12 ) ) + SPACE ( 12 ) , 12 ) AS ANPARIVA , ; 
 LEFT( NVL( ANDESCRI , SPACE ( 40 ) ) + SPACE ( 40 ) , 40 ) AS ANDESCRI , ; 
 LEFT( NVL( ANINDIRI , SPACE ( 40 ) ) + SPACE ( 40 ) , 40 ) AS ANINDIRI , ; 
 LEFT( NVL( ANLOCALI , SPACE ( 40 ) ) + SPACE ( 40 ) , 40 ) AS ANLOCALI , ; 
 LEFT( NVL( AN___CAP , SPACE ( 5 ) ) + SPACE( 5 ) , 5 ) AS AN___CAP , ; 
 LEFT( NVL( ANPROVIN , SPACE ( 2 ) ) + SPACE ( 2 ) , 2 ) AS ANPROVIN , ; 
 LEFT( NVL( ANTELEFO , SPACE ( 20 ) ) + SPACE ( 20 ) , 20 ) AS ANTELEFO , ; 
 LEFT( NVL( ANNUMCEL , SPACE ( 20 ) ) + SPACE ( 20 ) , 20 ) AS ANNUMCEL , ; 
 LEFT( NVL( ANTELFAX , SPACE ( 20 ) ) + SPACE ( 20 ) , 20 ) AS ANTELFAX , ; 
 LEFT( NVL( AN_EMAIL , SPACE ( 50 ) ) + SPACE( 50 ) , 50 ) AS AN_EMAIL , ; 
 LEFT( NVL( ANINDWEB , SPACE ( 50 ) ) + SPACE( 50 ) , 50 ) AS ANINDWEB , ; 
 DTOS( NVL( ANDATNAS , CTOD( "  -  -  " ) ) ) AS ANDATNAS , ; 
 LEFT( NVL( ANLOCNAS , SPACE ( 40 ) ) + SPACE ( 40 ) , 40 ) AS ANLOCNAS , ; 
 LEFT( NVL( ANPRONAS , SPACE ( 2 ) ) + SPACE ( 2 ) , 2 ) AS ANPRONAS ; 
 FROM GSAR_BPV ORDER BY 1 INTO CURSOR GSAR_BPV
        * --- AGGIUNGE INTESTAZIONE 'AH'
        SELECT * FROM GSAR_BPV WHERE 1=0 INTO CURSOR GSAR_BPV_AH
        WRCURSOR( "GSAR_BPV_AH" )
        APPEND BLANK
        REPLACE ANTIPCON WITH "A"
        REPLACE ANPERFIS WITH "H"
        SELECT * FROM GSAR_BPV_AH ; 
 UNION ; 
 SELECT * FROM GSAR_BPV INTO CURSOR GSAR_BPV_2
        SELECT * FROM GSAR_BPV_2 ORDER BY 1 , 3 INTO CURSOR GSAR_BPV
        SELECT GSAR_BPV
      case this.w_TYPE = "DELIMITED"
        SELECT ; 
 ANTIPCON , ; 
 ANPERFIS , ; 
 STRTRAN( NVL( ANCOGNOM , SPACE ( 40 ) ) , ";" , "," ) AS ANCOGNOM , ; 
 STRTRAN( NVL( AN__NOME , SPACE ( 40 ) ) , ";" , "," ) AS AN__NOME , ; 
 STRTRAN( NVL( ANCODFIS , SPACE ( 16 ) ) , ";" , "," ) AS ANCODFIS , ; 
 STRTRAN( NVL( ANPARIVA , SPACE ( 12 ) ) , ";" , "," ) AS ANPARIVA , ; 
 STRTRAN( NVL( ANDESCRI , SPACE ( 40 ) ) , ";" , "," ) AS ANDESCRI , ; 
 STRTRAN( NVL( ANINDIRI , SPACE ( 40 ) ) , ";" , "," ) AS ANINDIRI , ; 
 STRTRAN( NVL( ANLOCALI , SPACE ( 40 ) ) , ";" , "," ) AS ANLOCALI , ; 
 STRTRAN( NVL( AN___CAP , SPACE ( 5 ) ) , ";" , "," ) AS AN___CAP , ; 
 STRTRAN( NVL( ANPROVIN , SPACE ( 2 ) ) , ";" , "," ) AS ANPROVIN , ; 
 STRTRAN( NVL( ANTELEFO , SPACE ( 20 ) ) , ";" , "," ) AS ANTELEFO , ; 
 STRTRAN( NVL( ANNUMCEL , SPACE ( 20 ) ) , ";" , "," ) AS ANNUMCEL , ; 
 STRTRAN( NVL( ANTELFAX , SPACE ( 20 ) ) , ";" , "," ) AS ANTELFAX , ; 
 STRTRAN( NVL( AN_EMAIL , SPACE ( 50 ) ) , ";" , "," ) AS AN_EMAIL , ; 
 STRTRAN( NVL( ANINDWEB , SPACE ( 50 ) ) , ";" , "," ) AS ANINDWEB , ; 
 DTOS( NVL( ANDATNAS , cp_CharToDate( "  -  -  " ) ) ) AS ANDATNAS , ; 
 STRTRAN( NVL( ANLOCNAS , SPACE ( 40 ) ) , ";" , "," ) AS ANLOCNAS , ; 
 STRTRAN( NVL( ANPRONAS , SPACE ( 2 ) ) , ";" , "," ) AS ANPRONAS ; 
 FROM GSAR_BPV ORDER BY 1 INTO CURSOR GSAR_BPV
    endcase
    if FILE( this.oParentObject.w_FILEDEST )
      if !AH_YESNO( "Il file %1 sar� sovrascritto.%0Si desidera continuare?" , ,alltrim( this.oParentObject.w_FILEDEST ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
    endif
    L_CREAZIONEESEGUITA = .T.
    ON ERROR L_CREAZIONEESEGUITA = .F.
    L_ISTRUZIONE = "COPY TO " + CHR( 34 ) + ALLTRIM( this.oParentObject.w_FILEDEST ) + CHR( 34 ) + " TYPE SDF"
    &L_ISTRUZIONE
    ON ERROR
    if L_CREAZIONEESEGUITA
      AH_ERRORMSG( "Creazione file eseguita")
    else
      AH_ERRORMSG( "Creazione file non riuscita")
    endif
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED( "GSAR_BPV" )
      SELECT GSAR_BPV
      USE
    endif
    if USED( "GSAR_BPV_AH" )
      SELECT GSAR_BPV_AH
      USE
    endif
    if USED( "GSAR_BPV_2" )
      SELECT GSAR_BPV_2
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
