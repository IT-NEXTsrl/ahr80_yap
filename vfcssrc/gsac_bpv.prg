* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bpv                                                        *
*              Stampa prospetto degli acquisti                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_247]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-03                                                      *
* Last revis.: 2010-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bpv",oParentObject)
return(i_retval)

define class tgsac_bpv as StdBatch
  * --- Local variables
  w_CAMBIOL = 0
  w_VALCOS = space(3)
  w_CAOCOS = 0
  w_CAOVAL = 0
  w_DECTOT2 = 0
  w_CAMBIO2L = 0
  w_VALU = space(3)
  w_SIMVAL = space(5)
  w_DATAIN = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_FLVEAC = space(1)
  w_TEST = .f.
  w_CONTA_DT = 0
  w_CONTA_NC = 0
  w_APPO = space(10)
  w_oERRORLOG = .NULL.
  * --- WorkFile variables
  TMPVEND1_idx=0
  TMPVEND2_idx=0
  TMPVEND3_idx=0
  VALUTE_idx=0
  GSAC67PA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch elaborazione stampa prospetto degli Acquisti lanciato dalla maschera  GSAC_SPA
    this.w_DATAFIN = this.oParentObject.w_ADATA
    this.w_DATAIN = this.oParentObject.w_DADATA
    this.w_VALU = this.oParentObject.w_VALU2
    this.w_CAMBIO2L = this.oParentObject.w_CAMBIO2
    this.w_DECTOT2 = this.oParentObject.w_DECTOT
    this.w_CAOVAL = g_CAOVAL
    * --- Utilizzata per discriminare prospetto del Venduto da quello degli acquisti nelle query in comune
    this.w_FLVEAC = "A"
    * --- Creo la tabella temporanea TMPVEND1 dalla query GSAC_PDA che prende i dati dalla tabella documenti.
    *     Nella query vengono gi� eseguiti i filtri sui documenti che devono essere presi in considerazione a seconda 
    *     dei check spuntati sulla maschera di stampa.
    *     Inoltre per i Documenti di Trasporto viene presa in considerazione la quantit� evasa e spuntato il flag evaso 
    *     solo per le righe totalmente evase.  Per gli altri documenti questi campi sono vuoti.
    *     Nel campo MVSPEACC vengono sommate:spese di trasporto, di incasso,di imballo e di bollo.
    *     Vengono anche predisposti due campi TOTDOC e TOTVEN che assumeranno i valori di totale documento
    *     e totale di riga con tutte le spese ripartite.
    ah_Msg("Seleziono i documenti",.T.)
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsac_pda',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Da TMPVEND1 per ogni documento sommo i valori di riga
    ah_Msg("Elaboro dati di riga",.T.)
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsac1pda',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Scrivo il totale di riga su TMPVEND1 nel campo TOTDOC
    * --- Write into TMPVEND1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTDOC = _t2.TOTDOC";
          +i_ccchkf;
          +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND1.TOTDOC = _t2.TOTDOC";
          +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
          +"TOTDOC = _t2.TOTDOC";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTDOC = (select TOTDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Riscrivo in TMPVEND2 la tabella TMPVEND1 riportando anche il valore di riga con le spese accessorie ripartite
    * --- Inoltre in  MVQTAEV1 riporto la quantit� evasa, cio�, se il flag riga evasa � 'S' riporto MVQTAUM1 altrimenti MVQTAEV1
    ah_Msg("Ripartizione spese accessorie",.T.)
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsac2pda',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Riporto tutto in TMPVEND1 ripartendo il valore di riga per la quantit� evasa e memorizzandolo nel campo TOTVEN
    ah_Msg("Ripartizione per quantit� evasa",.T.)
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsac3pda',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPVEND1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTVEN = (_t2.TOTVEN*(_t2.MVQTAUM1-_t2.MVQTAEV1))/_t2.MVQTAUM1";
          +i_ccchkf;
          +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND1.TOTVEN = (_t2.TOTVEN*(_t2.MVQTAUM1-_t2.MVQTAEV1))/_t2.MVQTAUM1";
          +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
          +"TOTVEN = (_t2.TOTVEN*(_t2.MVQTAUM1-_t2.MVQTAEV1))/_t2.MVQTAUM1";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTVEN = (select (TOTVEN*(MVQTAUM1-MVQTAEV1))/MVQTAUM1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Controllo le righe di documento 'Omaggio imponibile', 'Omaggio imponibile + IVA' o 'Sconto merce'
    *     per le quali creo un temporaneo con TOTVEN = 0
    * --- Create temporary table TMPVEND3
    i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsve14pv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Azzeramento righe sconto",.T.)
    * --- Sulla base del temporaneo TMPVEND3 vado a riscrivere
    *      il TMPVEND1 definitivo con TOTVEN a 0 nel caso dellle righe sopraindicate
    * --- Write into TMPVEND1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTVEN = _t2.TOTVEN";
          +i_ccchkf;
          +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND1.TOTVEN = _t2.TOTVEN";
          +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
          +"TOTVEN = _t2.TOTVEN";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTVEN = (select TOTVEN from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_Msg("Conversione verso Euro",.T.)
    * --- Converto i valori in EURO e metto in negativo le note di credito
    * --- Create temporary table TMPVEND3
    i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsac12pa',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Ricostruisco la TMPVEND1 con TOTVEN vuoto per problemi DB2
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsac97pa',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Eseguo la conversione dei valori nel caso in cui scelgo come valuta finale la moneta di conto
    * --- Create temporary table GSAC67PA
    i_nIdx=cp_AddTableDef('GSAC67PA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSAC67PA',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.GSAC67PA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPVEND1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.GSAC67PA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTVEN = _t2.TOTVEN ";
          +i_ccchkf;
          +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND1.TOTVEN = _t2.TOTVEN ";
          +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
          +"TOTVEN = _t2.TOTVEN ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTVEN = (select TOTVEN  from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MVFLEVAS = "+cp_ToStrODBC("S");
             )
    else
      delete from (i_cTable) where;
            MVFLEVAS = "S";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TMPVEND1_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Costruisco campo COND = MVCLADOC + CMFLCASC per avere 'DT- ' nel caso di DDT di reso a fornitore
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsac99pa',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Metto in negativo i Documenti di trasporto di reso da Cliente........
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsac90pa',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- ......e riporto tutto in TMPVEND1 per la stampa.
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TMPVEND2_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- In caso di Note di credito con causale Magazzino con variazione di Valore attiva,
    *     azzero le quantit� ed il costo. Prendo in considerazione il solo valore
    * --- Write into TMPVEND1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COSTO ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','COSTO');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','MVQTAEV1');
      +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','MVQTAUM1');
          +i_ccchkf ;
      +" where ";
          +"MVCLADOC = "+cp_ToStrODBC("NC");
          +" and CMVARVAL <> "+cp_ToStrODBC("N");
             )
    else
      update (i_cTable) set;
          COSTO = 0;
          ,MVQTAEV1 = 0;
          ,MVQTAUM1 = 0;
          &i_ccchkf. ;
       where;
          MVCLADOC = "NC";
          and CMVARVAL <> "N";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.oParentObject.w_OQRY="..\INFO\EXE\QUERY\GSIN_BPA.VQR"
      i_retcode = 'stop'
      return
    else
      * --- Nel caso in cui non lancio il batch da Inforeader ed ho attivato il flag Stampa Log
      if this.oParentObject.w_STALOG="S"
        this.w_CONTA_DT = 0
        this.w_CONTA_NC = 0
        this.w_TEST = .T.
        vq_exec("gsve_bpv",this,"ERRORI")
        * --- Eseguo la stampa solo se ci sono DDT di Reso da cliente aperti e Note di Credito slegate dal ciclo documentale.
        *     Questi possono essere causa di problemi
         
 Select ERRORI 
 Go Top 
 COUNT FOR MVCLADOC = "DT" TO this.w_CONTA_DT 
 Go Top 
 COUNT FOR MVCLADOC = "NC" TO this.w_CONTA_NC
        if this.w_CONTA_DT >0 And this.w_CONTA_NC >0
          if Reccount("ERRORI")>0 And ah_YesNo("Stampo log dei documenti incongruenti?")
            * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
            this.w_oERRORLOG=createobject("AH_ErrorLog")
            this.w_oERRORLOG.AddMsgLog("Note di credito che non evadono documenti di carico oppure sono slegate dal ciclo documentale")     
             
 Select ERRORI 
 Go Top 
 Scan
            if MVCLADOC = "DT" And this.w_TEST
              * --- Quando incontro il primo DDT inserisco la descrizione
              this.w_oERRORLOG.AddMsgLogNoTranslate(Chr(13) + Repl("-",60) + CHR(13))     
              this.w_oERRORLOG.AddMsgLog("Documenti di trasporto di reso ancora da evadere")     
              this.w_TEST = .F.
            endif
            * --- Messaggio di Errore 
            this.w_oERRORLOG.AddMsgLog("Doc.: %1 n. %2 del %3%0Intestato a %4", Alltrim(MVTIPDOC), Alltrim(Str(MVNUMDOC,15))+ IIF(Not Empty(MVALFDOC),"/"+Alltrim(MVALFDOC),""), Alltrim(DTOC(CP_TODATE(MVDATDOC))), Alltrim(MVCODCON) + " "+ Alltrim(ANDESCRI) )     
             
 EndScan
            this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati",.F.)     
            if USED("__TMP__")
               
 Select __TMP__ 
 Use
            endif
          endif
        endif
        if USED("ERRORI")
           
 Select ERRORI 
 Use
        endif
      endif
    endif
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_VALU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Select from GSVETBPV
    do vq_exec with 'GSVETBPV',this,'_Curs_GSVETBPV','',.f.,.t.
    if used('_Curs_GSVETBPV')
      select _Curs_GSVETBPV
      locate for 1=1
      do while not(eof())
      L_TOTALE=_Curs_GSVETBPV.TOTALE
        select _Curs_GSVETBPV
        continue
      enddo
      use
    endif
    * --- Variabili per la stampa! Principalmente sono le selezioni di stampa selezionate nella maschera 
    L_STATO=this.oParentObject.w_STATO
    L_CLIENTE=this.oParentObject.w_CLIENTE
    L_CODART=this.oParentObject.w_CODART
    L_CODART1=this.oParentObject.w_CODART1
    L_ZONA=this.oParentObject.w_ZONA
    L_PAGAM=this.oParentObject.w_PAGAM
    L_CATCOMM=this.oParentObject.w_CATCOMM
    L_DECTOT2=this.w_DECTOT2
    L_CODART=this.oParentObject.w_CODART
    L_GRUMER=this.oParentObject.w_GRUMER
    L_SIMVAL=this.w_SIMVAL
    L_VALU=this.w_VALU
    L_CAMBIO=this.w_CAMBIO2L
    L_DADATA=this.w_DATAIN
    L_ADATA=this.w_DATAFIN
    L_CODFAM=this.oParentObject.w_CODFAM
    L_CODMAR=this.oParentObject.w_CODMAR
    L_CODCAT=this.oParentObject.w_CODCAT
    L_DATREG=this.oParentObject.w_DATREG
    L_DATREG1=this.oParentObject.w_DATREG1
    * --- Eseguo la query per la Stampa
    ah_Msg("Stampa in corso...",.T.)
    * --- Lancio la stampa
    VX_EXEC(""+ALLTRIM(this.oParentObject.w_OQRY)+", "+ALLTRIM(this.oParentObject.w_OREP)+"",THIS)
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
    * --- Drop temporary table TMPVEND2
    i_nIdx=cp_GetTableDefIdx('TMPVEND2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND2')
    endif
    * --- Drop temporary table TMPVEND3
    i_nIdx=cp_GetTableDefIdx('TMPVEND3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND3')
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='*TMPVEND1'
    this.cWorkTables[2]='*TMPVEND2'
    this.cWorkTables[3]='*TMPVEND3'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='*GSAC67PA'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSVETBPV')
      use in _Curs_GSVETBPV
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
