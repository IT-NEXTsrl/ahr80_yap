* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bvo                                                        *
*              Gestione menu                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_180]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-29                                                      *
* Last revis.: 2011-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNoMessage
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bvo",oParentObject,m.pNoMessage)
return(i_retval)

define class tgsut_bvo as StdBatch
  * --- Local variables
  pNoMessage = .f.
  w_NHF = 0
  w_ADESC = space(100)
  w_AFUN = space(50)
  w_PARAMETRO = space(50)
  w_B = space(50)
  w_C = 0
  w_CONTSTAR = 0
  w_MESS = space(20)
  w_OK = .f.
  w_SERIAL = 0
  w_PRNUMERO = 0
  w_MODULO = space(4)
  w_RELEASE = space(10)
  w_PROGRAM = space(50)
  w_LISTAPROG = space(250)
  w_LISTAPROG1 = space(250)
  w_LISTAPROG2 = space(250)
  * --- WorkFile variables
  TSMENUVO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invocata dalla maschera 'gsut_kvo'. Carica la voce di men� ed il programma ad essa
    *     associato nella tabella 'TSMENUVO'.
    * --- pNoMessage = .t.  non mostra messaggi e ritorna l'eventuale messaggio di errore
    this.w_ADESC = " "
    this.w_AFUN = " "
    this.w_PARAMETRO = ""
    this.w_B = " "
    this.w_C = 0
    this.w_CONTSTAR = 0
    this.w_SERIAL = 0
    this.w_RELEASE = ALLTRIM(STRTRAN(g_VERSION,"Rel.",""))
    * --- Controllo se si vuole riaggiornare la tabella dei menu
    this.w_OK = .T.
    if Not this.pNoMessage
      this.w_OK = ah_YesNo("Vuoi ricaricare la tabella dei menu?")
    endif
    if this.w_OK
      if AH_YESNO("Si vogliono eliminare i precedenti menu caricati?")
        * --- Delete from TSMENUVO
        i_nConn=i_TableProp[this.TSMENUVO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TSMENUVO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"PRRELEAS = "+cp_ToStrODBC(ALLTRIM(STRTRAN(g_VERSION,"Rel.","")));
                 )
        else
          delete from (i_cTable) where;
                PRRELEAS = ALLTRIM(STRTRAN(g_VERSION,"Rel.",""));

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      ah_Msg("Caricamento voci menu",.t.)
      do case
        case this.oParentObject.w_SELEMENU = "1"
          * --- Scansione del dbf
          SELECT(i_CUR_MENU)
          GO TOP
          do while NOT EOF()
            this.w_ADESC = left(ALLTRIM(STRTRAN(Ah_Msgformat(VOCEMENU),"&","")),100)
            this.w_AFUN = ALLTRIM(NAMEPROC)
            if UPPER(this.w_AFUN)<>"NONAMEPROC" AND NOT EMPTY(this.w_AFUN)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            SELECT (i_CUR_MENU)
            SKIP
          enddo
        case this.oParentObject.w_SELEMENU = "2"
          * --- Apertura file ASCII contenente il menu da caricare in tabella
          this.w_NHF = FOPEN(this.oParentObject.w_NOMEFILE)
          if this.w_NHF = -1
            if Not this.pNoMessage
              ah_ErrorMsg("Errore apertura file %1","!","","DEFAULT.VMN")
              =FCLOSE(this.w_NHF)
              i_retcode = 'stop'
              return
            else
              =FCLOSE(this.w_NHF)
              i_retcode = 'stop'
              i_retval = ah_Msgformat("Errore nell'apertura del file %1",ALLTRIM( this.oParentObject.w_NOMEFILE ) )
              return
            endif
          endif
          * --- Scansione del file ASCII
          do while NOT FEOF(this.w_NHF)
            this.w_B = FGET(this.W_NHF)
            this.w_C = this.w_C +1
            if this.w_C = 1 and this.w_CONTSTAR > 1
              if this.oParentObject.w_FLNOTRAD="S"
                this.w_ADESC = left(ALLTRIM(STRTRAN(STRTRAN(STRTRAN(this.w_B,"�","'"),"&",""),'"',"")),100)
              else
                this.w_ADESC = left(ALLTRIM(STRTRAN(Ah_Msgformat(STRTRAN(STRTRAN(this.w_B,"�","'"),'"',"")),"&","")),100)
              endif
            endif
            if this.w_C = 3 and this.w_CONTSTAR > 1
              this.w_AFUN = STRTRAN(STRTRAN(this.w_B,'"',""),"�","")
            endif
            if LEFT(LTRIM(this.w_B),1) = "*"
              * --- Inserimento del nome Funzione e nome Programma nella tabella.
              this.w_CONTSTAR = this.w_CONTSTAR + 1
              if this.w_ADESC= " "
                this.w_ADESC = "No Name Found"
              endif
              if this.w_CONTSTAR > 2 AND UPPER(this.w_AFUN)<>"NONAMEPROC" AND NOT EMPTY(this.w_AFUN)
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              this.w_C = 0
              this.w_ADESC = " "
              this.w_AFUN = " "
            endif
          enddo
          =FCLOSE(this.w_NHF)
      endcase
      if Not this.pNoMessage
        ah_ErrorMsg("Aggiornamento terminato","i","")
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_03BF5878
    bErr_03BF5878=bTrsErr
    this.Try_03BF5878()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03BF5878
    * --- End
  endproc
  proc Try_03BF5878()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_PARAMETRO = LEFT(IIF("#"$this.w_AFUN,ALLTRIM(SUBSTR(this.w_AFUN,AT("#",this.w_AFUN)+1)),""),50)
    this.w_PROGRAM = STRTRAN(LOWER(IIF(NOT EMPTY(this.w_PARAMETRO),ALLTRIM(UPPER(SUBSTR(this.w_AFUN,1,AT("#",this.w_AFUN)-1)))+","+this.w_PARAMETRO,UPPER(ALLTRIM(this.w_AFUN)))),"'","")
    * --- Determina modulo
    do case
      case UPPER(LEFT(this.w_AFUN,2))="GS"
        * --- VOCI DI MENU CON NOME STANDARD
        do case
          case UPPER(SUBSTR(this.w_AFUN,3,2))="AR"
            this.w_MODULO = "ARCH"
            this.w_LISTAPROG = ""
            this.w_LISTAPROG1 = ""
            this.w_LISTAPROG2 = ""
            this.w_LISTAPROG = "GSAR_ANM;GSAR_ATT;GSAR_BBI;GSAR_BEF;GSAR_BEI;GSAR_BIN;GSAR_BIR;GSAR_BIT;GSAR_BMD;GSAR_BNM;GSAR_KEF:GSAR_KEI;GSAR_KMD;GSAR_MIT;GSAR_SBI;GSAR_SIB;GSAR_SNM;GSAR_STT"
            this.w_LISTAPROG1 = "GSAR_A11;GSAR_BDF;GSAR_BEE;GSAR_BGI;GSAR_K11;GSAR_KGD;GSAR_KVD;GSAR_MDF"
            this.w_LISTAPROG2 = "GSAR_ATP;GSAR_MCL;GSAR_MCT;GSAR_KCI;GSAR_SAC"
            * --- --Modulo Anfibi
            do case
              case UPPER(SUBSTR(this.w_AFUN,1,8))$this.w_LISTAPROG1
                this.w_MODULO = "FAEL"
              case UPPER(SUBSTR(this.w_AFUN,1,8))$this.w_LISTAPROG2
                this.w_MODULO = "COAC"
            endcase
            if g_APPLICATION = "ADHOC REVOLUTION"
              do case
                case UPPER(SUBSTR(this.w_AFUN,1,8))$this.w_LISTAPROG
                  this.w_MODULO = "INTR"
              endcase
            endif
          case UPPER(SUBSTR(this.w_AFUN,3,2))="OR"
            if g_APPLICATION = "ADHOC REVOLUTION"
              this.w_MODULO = "ORDI"
            endif
          case UPPER(SUBSTR(this.w_AFUN,3,2))="AC"
            if g_APPLICATION = "ADHOC REVOLUTION"
              this.w_MODULO = "ACQU"
            endif
          case UPPER(SUBSTR(this.w_AFUN,3,2))="AG"
            this.w_MODULO = "AGEN"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="SO"
            if g_APPLICATION = "ADHOC REVOLUTION"
              this.w_MODULO = "SOLL"
            endif
          case UPPER(SUBSTR(this.w_AFUN,3,2))="EP"
            this.w_MODULO = "EPOS"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="OF"
            this.w_MODULO = "OFFE"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="RB"
            this.w_MODULO = "REBA"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="BV"
            this.w_MODULO = "BUVE"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="JB"
            this.w_MODULO = "JBSH"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="IN"
            this.w_MODULO = "INFO"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="IR"
            this.w_MODULO = "IRDR"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="CP"
            this.w_MODULO = "IZCP"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="SC"
            this.w_MODULO = "SCCI"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="CG"
            this.w_MODULO = "COGE"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="TE"
            this.w_MODULO = "TESO"
            if g_APPLICATION = "ADHOC REVOLUTION"
              this.w_MODULO = "COGE"
            endif
          case UPPER(SUBSTR(this.w_AFUN,3,2))="CA"
            this.w_MODULO = "COAN"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="MA"
            this.w_MODULO = "MAGA"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="VE"
            this.w_MODULO = "VEND"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="IM"
            this.w_MODULO = "IMPO"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="BI"
            this.w_MODULO = "BILC"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="BC"
            this.w_MODULO = "BCON"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="CE"
            this.w_MODULO = "CESP"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="RI"
            this.w_MODULO = "RITE"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="EC"
            this.w_MODULO = "ECRM"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="ST"
            this.w_MODULO = "STAT"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="DB"
            this.w_MODULO = "DIBA"
            if g_APPLICATION="ADHOC REVOLUTION"
              this.w_MODULO = "PROD"
            endif
          case UPPER(SUBSTR(this.w_AFUN,3,2))="CI"
            this.w_MODULO = "CILA"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="CO"
            this.w_MODULO = "COLA"
            if g_APPLICATION="ADHOC REVOLUTION"
              this.w_MODULO = "PROD"
            endif
          case UPPER(SUBSTR(this.w_AFUN,3,2))="MR"
            this.w_MODULO = "MMRP"
            if g_APPLICATION="ADHOC REVOLUTION"
              this.w_MODULO = "PROD"
            endif
          case UPPER(SUBSTR(this.w_AFUN,3,2))="CL"
            this.w_MODULO = "COLA"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="RA"
            this.w_MODULO = "RIAN"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="PC"
            this.w_MODULO = "COMM"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="WD"
            this.w_MODULO = "WFLD"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="DM"
            this.w_MODULO = "DOCM"
            if UPPER(SUBSTR(this.w_AFUN,1,8))="GSDM_KED"
              this.w_MODULO = "AEDS"
            endif
          case UPPER(SUBSTR(this.w_AFUN,3,2))="BA"
            this.w_MODULO = "BUAN"
            if g_APPLICATION = "ADHOC REVOLUTION"
              this.w_MODULO = "BANC"
            endif
          case UPPER(SUBSTR(this.w_AFUN,3,2))="DS"
            this.w_MODULO = "DISB"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="PS"
            this.w_MODULO = "GPOS"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="LM"
            this.w_MODULO = "LEMC"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="MD"
            this.w_MODULO = "MADV"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="IL"
            this.w_MODULO = "MAGA"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="VA"
            this.w_MODULO = "VEFA"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="LR"
            this.w_MODULO = "LORE"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="BB"
            this.w_MODULO = "BIBL"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="PR"
            this.w_MODULO = "PRAT"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="AT"
            this.w_MODULO = "ANTI"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="AI"
            this.w_MODULO = "AIFT"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="FA"
            this.w_MODULO = "AGFA"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="CC"
            this.w_MODULO = "CCOM"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="LA"
            this.w_MODULO = "LOAV"
          case UPPER(SUBSTR(this.w_AFUN,3,2))="SI"
            this.w_MODULO = "SINC"
          otherwise
            this.w_MODULO = "UTIL"
        endcase
      case UPPER(LEFT(this.w_AFUN,2))="CP" OR UPPER(LEFT(this.w_AFUN,9))="CHKEDITOR" OR UPPER(this.w_AFUN)$"GSCP_" OR UPPER(LEFT(this.w_AFUN,8))="OPENGEST" OR UPPER(LEFT(this.w_AFUN,4))="QUIT" OR "_BUILD"$UPPER(this.w_AFUN) OR UPPER(LEFT(this.w_AFUN,8))="DCMSETPR" OR UPPER(LEFT(this.w_AFUN,12))="MENU ARCHIVI"
        * --- VOCI DI MENU SISTEMA CON NOME NON STANDARD
        this.w_MODULO = "UTIL"
      otherwise
        * --- ALTRO ( PERSONALIZZAZIONI )
        this.w_MODULO = "9999"
    endcase
    this.w_SERIAL = this.w_SERIAL + 1
    * --- Read from TSMENUVO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TSMENUVO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TSMENUVO_idx,2],.t.,this.TSMENUVO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PRNUMERO"+;
        " from "+i_cTable+" TSMENUVO where ";
            +"PROGRAM = "+cp_ToStrODBC(UPPER(this.w_PROGRAM));
            +" and PRRELEAS = "+cp_ToStrODBC(ALLTRIM(STRTRAN(g_VERSION,"Rel.","")));
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PRNUMERO;
        from (i_cTable) where;
            PROGRAM = UPPER(this.w_PROGRAM);
            and PRRELEAS = ALLTRIM(STRTRAN(g_VERSION,"Rel.",""));
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PRNUMERO = NVL(cp_ToDate(_read_.PRNUMERO),cp_NullValue(_read_.PRNUMERO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NVL(this.w_PRNUMERO,0)=0
      * --- Insert into TSMENUVO
      i_nConn=i_TableProp[this.TSMENUVO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TSMENUVO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TSMENUVO_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PROGDES"+",PROGRAM"+",PRRELEAS"+",PRNUMERO"+",PRPARAME"+",PRMODULO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_ADESC),'TSMENUVO','PROGDES');
        +","+cp_NullLink(cp_ToStrODBC(LOWER(this.w_PROGRAM)),'TSMENUVO','PROGRAM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RELEASE),'TSMENUVO','PRRELEAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TSMENUVO','PRNUMERO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PARAMETRO),'TSMENUVO','PRPARAME');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MODULO),'TSMENUVO','PRMODULO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PROGDES',this.w_ADESC,'PROGRAM',LOWER(this.w_PROGRAM),'PRRELEAS',this.w_RELEASE,'PRNUMERO',this.w_SERIAL,'PRPARAME',this.w_PARAMETRO,'PRMODULO',this.w_MODULO)
        insert into (i_cTable) (PROGDES,PROGRAM,PRRELEAS,PRNUMERO,PRPARAME,PRMODULO &i_ccchkf. );
           values (;
             this.w_ADESC;
             ,LOWER(this.w_PROGRAM);
             ,this.w_RELEASE;
             ,this.w_SERIAL;
             ,this.w_PARAMETRO;
             ,this.w_MODULO;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return


  proc Init(oParentObject,pNoMessage)
    this.pNoMessage=pNoMessage
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TSMENUVO'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNoMessage"
endproc
