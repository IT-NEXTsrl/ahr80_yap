* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kic                                                        *
*              Import contatti                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_154]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2013-12-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kic",oParentObject))

* --- Class definition
define class tgsar_kic as StdForm
  Top    = 51
  Left   = 90

  * --- Standard Properties
  Width  = 492
  Height = 336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-10"
  HelpContextID=266958697
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  CAT_ATTR_IDX = 0
  NOM_CONT_IDX = 0
  TIP_CATT_IDX = 0
  OFF_NOMI_IDX = 0
  PAR_OFFE_IDX = 0
  GRU_NOMI_IDX = 0
  ORI_NOMI_IDX = 0
  CPUSERS_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  RUO_CONT_IDX = 0
  cPrg = "gsar_kic"
  cComment = "Import contatti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_POCODAZI = space(5)
  w_POOUTIMP = space(40)
  w_POOUTRAG = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_OFNOME = space(40)
  w_FlPublic = space(1)
  w_OFSOTFOLD = space(1)
  w_OFCODCON1 = space(40)
  w_OFCODCON3 = space(40)
  w_OFDESNOM1 = space(40)
  w_OFDESNOM3 = space(40)
  w_OFCODNOM1 = space(20)
  w_OFCODNOM3 = space(20)
  w_FLGNUM = space(1)
  w_NODATNS = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kicPag1","gsar_kic",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOFNOME_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='CAT_ATTR'
    this.cWorkTables[2]='NOM_CONT'
    this.cWorkTables[3]='TIP_CATT'
    this.cWorkTables[4]='OFF_NOMI'
    this.cWorkTables[5]='PAR_OFFE'
    this.cWorkTables[6]='GRU_NOMI'
    this.cWorkTables[7]='ORI_NOMI'
    this.cWorkTables[8]='CPUSERS'
    this.cWorkTables[9]='ZONE'
    this.cWorkTables[10]='AGENTI'
    this.cWorkTables[11]='RUO_CONT'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gsar_beo(this,"I")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_POCODAZI=space(5)
      .w_POOUTIMP=space(40)
      .w_POOUTRAG=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_OFNOME=space(40)
      .w_FlPublic=space(1)
      .w_OFSOTFOLD=space(1)
      .w_OFCODCON1=space(40)
      .w_OFCODCON3=space(40)
      .w_OFDESNOM1=space(40)
      .w_OFDESNOM3=space(40)
      .w_OFCODNOM1=space(20)
      .w_OFCODNOM3=space(20)
      .w_FLGNUM=space(1)
      .w_NODATNS=space(1)
        .w_POCODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_POCODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,4,.f.)
        .w_OFNOME = .w_POOUTIMP
        .w_FlPublic = 'N'
        .w_OFSOTFOLD = 'S'
          .DoRTCalc(8,14,.f.)
        .w_NODATNS = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFlPublic_1_6.enabled = this.oPgFrm.Page1.oPag.oFlPublic_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFlPublic_1_6.visible=!this.oPgFrm.Page1.oPag.oFlPublic_1_6.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=POCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_lTable = "PAR_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2], .t., this.PAR_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODAZI,POOUTIMP,POOUTRAG,POFLCOOF";
                   +" from "+i_cTable+" "+i_lTable+" where POCODAZI="+cp_ToStrODBC(this.w_POCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODAZI',this.w_POCODAZI)
            select POCODAZI,POOUTIMP,POOUTRAG,POFLCOOF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POCODAZI = NVL(_Link_.POCODAZI,space(5))
      this.w_POOUTIMP = NVL(_Link_.POOUTIMP,space(40))
      this.w_POOUTRAG = NVL(_Link_.POOUTRAG,space(1))
      this.w_FLGNUM = NVL(_Link_.POFLCOOF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_POCODAZI = space(5)
      endif
      this.w_POOUTIMP = space(40)
      this.w_POOUTRAG = space(1)
      this.w_FLGNUM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.POCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oOFNOME_1_5.value==this.w_OFNOME)
      this.oPgFrm.Page1.oPag.oOFNOME_1_5.value=this.w_OFNOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFlPublic_1_6.RadioValue()==this.w_FlPublic)
      this.oPgFrm.Page1.oPag.oFlPublic_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOFCODCON1_1_8.value==this.w_OFCODCON1)
      this.oPgFrm.Page1.oPag.oOFCODCON1_1_8.value=this.w_OFCODCON1
    endif
    if not(this.oPgFrm.Page1.oPag.oOFCODCON3_1_9.value==this.w_OFCODCON3)
      this.oPgFrm.Page1.oPag.oOFCODCON3_1_9.value=this.w_OFCODCON3
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDESNOM1_1_10.value==this.w_OFDESNOM1)
      this.oPgFrm.Page1.oPag.oOFDESNOM1_1_10.value=this.w_OFDESNOM1
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDESNOM3_1_11.value==this.w_OFDESNOM3)
      this.oPgFrm.Page1.oPag.oOFDESNOM3_1_11.value=this.w_OFDESNOM3
    endif
    if not(this.oPgFrm.Page1.oPag.oOFCODNOM1_1_12.value==this.w_OFCODNOM1)
      this.oPgFrm.Page1.oPag.oOFCODNOM1_1_12.value=this.w_OFCODNOM1
    endif
    if not(this.oPgFrm.Page1.oPag.oOFCODNOM3_1_13.value==this.w_OFCODNOM3)
      this.oPgFrm.Page1.oPag.oOFCODNOM3_1_13.value=this.w_OFCODNOM3
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_OFCODCON3) OR  (UPPER(.w_OFCODCON1)<= UPPER(.w_OFCODCON3)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFCODCON1_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice contatto iniziale � maggiore di quello finale")
          case   not(empty(.w_OFCODCON1) OR  (UPPER(.w_OFCODCON1)<= UPPER(.w_OFCODCON3)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFCODCON3_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice contatto iniziale � maggiore di quello finale")
          case   not(empty(.w_OFDESNOM3) OR  (UPPER(.w_OFDESNOM1)<= UPPER(.w_OFDESNOM3)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFDESNOM1_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La descrizione nominativo iniziale � maggiore di quella finale")
          case   not(empty(.w_OFDESNOM1) OR  (UPPER(.w_OFDESNOM1)<= UPPER(.w_OFDESNOM3)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFDESNOM3_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La descrizione nominativo iniziale � maggiore di quella finale")
          case   not(empty(.w_OFCODNOM3) OR  (UPPER(.w_OFCODNOM1)<= UPPER(.w_OFCODNOM3)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFCODNOM1_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice nominativo iniziale � maggiore di quello finale")
          case   not(empty(.w_OFCODNOM1) OR  (UPPER(.w_OFCODNOM1)<= UPPER(.w_OFCODNOM3)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFCODNOM3_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice nominativo iniziale � maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_kicPag1 as StdContainer
  Width  = 488
  height = 336
  stdWidth  = 488
  stdheight = 336
  resizeXpos=275
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOFNOME_1_5 as StdField with uid="HXMUFIGHAP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_OFNOME", cQueryName = "OFNOME",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Cartella di destinazione import contatti",;
    HelpContextID = 96816154,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=167, Top=17, InputMask=replicate('X',40)

  add object oFlPublic_1_6 as StdCheck with uid="JQJOWCEEOP",rtseq=6,rtrep=.f.,left=167, top=41, caption="Cartella pubblica",;
    ToolTipText = "Se attivo, la cartella di selezione viene ricercata tra le cartelle pubbliche",;
    HelpContextID = 223282759,;
    cFormVar="w_FlPublic", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFlPublic_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFlPublic_1_6.GetRadio()
    this.Parent.oContained.w_FlPublic = this.RadioValue()
    return .t.
  endfunc

  func oFlPublic_1_6.SetRadio()
    this.Parent.oContained.w_FlPublic=trim(this.Parent.oContained.w_FlPublic)
    this.value = ;
      iif(this.Parent.oContained.w_FlPublic=='S',1,;
      0)
  endfunc

  func oFlPublic_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_OFNOME))
    endwith
   endif
  endfunc

  func oFlPublic_1_6.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oOFCODCON1_1_8 as StdField with uid="XUHMEIYKIP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OFCODCON1", cQueryName = "OFCODCON1",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice contatto iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione dal codice contatto",;
    HelpContextID = 139851964,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=167, Top=101, InputMask=replicate('X',40)

  func oOFCODCON1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_OFCODCON3) OR  (UPPER(.w_OFCODCON1)<= UPPER(.w_OFCODCON3)))
    endwith
    return bRes
  endfunc

  add object oOFCODCON3_1_9 as StdField with uid="YRARUMOQPN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_OFCODCON3", cQueryName = "OFCODCON3",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice contatto iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione al codice contatto",;
    HelpContextID = 139851932,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=167, Top=130, InputMask=replicate('X',40)

  func oOFCODCON3_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_OFCODCON1) OR  (UPPER(.w_OFCODCON1)<= UPPER(.w_OFCODCON3)))
    endwith
    return bRes
  endfunc

  add object oOFDESNOM1_1_10 as StdField with uid="PDKERUMNKC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_OFDESNOM1", cQueryName = "OFDESNOM1",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "La descrizione nominativo iniziale � maggiore di quella finale",;
    ToolTipText = "Selezione dalla descrizione nominativo",;
    HelpContextID = 208660669,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=167, Top=159, InputMask=replicate('X',40)

  func oOFDESNOM1_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_OFDESNOM3) OR  (UPPER(.w_OFDESNOM1)<= UPPER(.w_OFDESNOM3)))
    endwith
    return bRes
  endfunc

  add object oOFDESNOM3_1_11 as StdField with uid="PPSXYYSXSF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_OFDESNOM3", cQueryName = "OFDESNOM3",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "La descrizione nominativo iniziale � maggiore di quella finale",;
    ToolTipText = "Selezione alla descrizione nominativo",;
    HelpContextID = 208660637,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=167, Top=188, InputMask=replicate('X',40)

  func oOFDESNOM3_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_OFDESNOM1) OR  (UPPER(.w_OFDESNOM1)<= UPPER(.w_OFDESNOM3)))
    endwith
    return bRes
  endfunc

  add object oOFCODNOM1_1_12 as StdField with uid="QWWBCGFQGT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_OFCODNOM1", cQueryName = "OFCODNOM1",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice nominativo iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione dal codice nominativo",;
    HelpContextID = 223738045,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=167, Top=217, InputMask=replicate('X',20)

  func oOFCODNOM1_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_OFCODNOM3) OR  (UPPER(.w_OFCODNOM1)<= UPPER(.w_OFCODNOM3)))
    endwith
    return bRes
  endfunc

  add object oOFCODNOM3_1_13 as StdField with uid="GUBPMCLSNL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_OFCODNOM3", cQueryName = "OFCODNOM3",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice nominativo iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione al codice nominativo",;
    HelpContextID = 223738013,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=167, Top=246, InputMask=replicate('X',20)

  func oOFCODNOM3_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_OFCODNOM1) OR  (UPPER(.w_OFCODNOM1)<= UPPER(.w_OFCODNOM3)))
    endwith
    return bRes
  endfunc


  add object oBtn_1_14 as StdButton with uid="TMLQHKZUII",left=382, top=285, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per importare i contatti";
    , HelpContextID = 266938138;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        gsar_beo(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="THVPLOIFWZ",left=433, top=285, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 55761670;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_16 as StdString with uid="JJWDKLOCRK",Visible=.t., Left=24, Top=19,;
    Alignment=1, Width=135, Height=18,;
    Caption="Cartella principale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="NUYXTEHMAB",Visible=.t., Left=7, Top=74,;
    Alignment=0, Width=161, Height=18,;
    Caption="Filtri per import contatti"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=24, Top=103,;
    Alignment=1, Width=135, Height=18,;
    Caption="Da contatto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="BRSOYYEROT",Visible=.t., Left=24, Top=132,;
    Alignment=1, Width=135, Height=18,;
    Caption="A contatto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="UXSAGSXEYV",Visible=.t., Left=24, Top=161,;
    Alignment=1, Width=135, Height=18,;
    Caption="Da descr. nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="RWVKJZVNSM",Visible=.t., Left=24, Top=190,;
    Alignment=1, Width=135, Height=18,;
    Caption="A descr. nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="AYGRLKHXLW",Visible=.t., Left=24, Top=219,;
    Alignment=1, Width=135, Height=18,;
    Caption="Da codice nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="CWBVQQDGXU",Visible=.t., Left=24, Top=248,;
    Alignment=1, Width=135, Height=18,;
    Caption="A codice nominativo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_17 as StdBox with uid="RJVEKXAJIQ",left=7, top=95, width=475,height=184
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kic','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
