* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bf2                                                        *
*              Esegue f2 con dialog con parametro                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-19                                                      *
* Last revis.: 2006-07-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pVALORE,pNOME
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bf2",oParentObject,m.pVALORE,m.pNOME)
return(i_retval)

define class tgsar_bf2 as StdBatch
  * --- Local variables
  pVALORE = space(10)
  pNOME = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valore parametro
    * --- Nome maschera da eseguire
    * --- Esegue F2 per maschere con parametro
    Do ( this.pNOME) With this.pVALORE
  endproc


  proc Init(oParentObject,pVALORE,pNOME)
    this.pVALORE=pVALORE
    this.pNOME=pNOME
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pVALORE,pNOME"
endproc
