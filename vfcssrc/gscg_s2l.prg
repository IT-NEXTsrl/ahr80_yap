* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_s2l                                                        *
*              Aggiorna progressivi libro giornale                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_10]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-04                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_s2l",oParentObject))

* --- Class definition
define class tgscg_s2l as StdForm
  Top    = 120
  Left   = 104

  * --- Standard Properties
  Width  = 365
  Height = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-08"
  HelpContextID=107619689
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_s2l"
  cComment = "Aggiorna progressivi libro giornale"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PROLIB = 0
  w_DATULT = ctod('  /  /  ')
  w_CONFER = space(1)
  w_PAGINE = 0
  w_CAPFOR = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_s2lPag1","gscg_s2l",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCONFER_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PROLIB=0
      .w_DATULT=ctod("  /  /  ")
      .w_CONFER=space(1)
      .w_PAGINE=0
      .w_CAPFOR=space(1)
      .w_PROLIB=oParentObject.w_PROLIB
      .w_DATULT=oParentObject.w_DATULT
      .w_CONFER=oParentObject.w_CONFER
      .w_PAGINE=oParentObject.w_PAGINE
      .w_CAPFOR=oParentObject.w_CAPFOR
          .DoRTCalc(1,2,.f.)
        .w_CONFER = 'N'
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate(AH_Msgformat("L'aggiornamento dei progressivi deve essere eseguito %0dopo la stampa definitiva su modulo bollato %0in quanto ne inibisce la ristampa"))
    endwith
    this.DoRTCalc(4,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PROLIB=.w_PROLIB
      .oParentObject.w_DATULT=.w_DATULT
      .oParentObject.w_CONFER=.w_CONFER
      .oParentObject.w_PAGINE=.w_PAGINE
      .oParentObject.w_CAPFOR=.w_CAPFOR
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(AH_Msgformat("L'aggiornamento dei progressivi deve essere eseguito %0dopo la stampa definitiva su modulo bollato %0in quanto ne inibisce la ristampa"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(AH_Msgformat("L'aggiornamento dei progressivi deve essere eseguito %0dopo la stampa definitiva su modulo bollato %0in quanto ne inibisce la ristampa"))
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .Caption = IIF(.w_CAPFOR='S',AH_MSGFORMAT("Aggiorna progressivi registro cronologico"),.Caption)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPROLIB_1_1.value==this.w_PROLIB)
      this.oPgFrm.Page1.oPag.oPROLIB_1_1.value=this.w_PROLIB
    endif
    if not(this.oPgFrm.Page1.oPag.oDATULT_1_2.value==this.w_DATULT)
      this.oPgFrm.Page1.oPag.oDATULT_1_2.value=this.w_DATULT
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFER_1_6.value==this.w_CONFER)
      this.oPgFrm.Page1.oPag.oCONFER_1_6.value=this.w_CONFER
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGINE_1_9.value==this.w_PAGINE)
      this.oPgFrm.Page1.oPag.oPAGINE_1_9.value=this.w_PAGINE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CONFER)) or not(.w_CONFER $ 'SN'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONFER_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CONFER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare S o N per confermare o meno l'elaborazione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_s2lPag1 as StdContainer
  Width  = 361
  height = 239
  stdWidth  = 361
  stdheight = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPROLIB_1_1 as StdField with uid="ANPEBBOFCS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PROLIB", cQueryName = "PROLIB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 7807478,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=200, Top=123, cSayPict='"999999999"', cGetPict='"999999999"'

  add object oDATULT_1_2 as StdField with uid="EGBZLIABCQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATULT", cQueryName = "DATULT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 45113398,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=200, Top=148

  add object oCONFER_1_6 as StdField with uid="EWXTNIKNXJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CONFER", cQueryName = "CONFER",;
    bObbl = .t. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare S o N per confermare o meno l'elaborazione",;
    ToolTipText = "S per confermare N per annullare",;
    HelpContextID = 3214886,;
   bGlobalFont=.t.,;
    Height=21, Width=16, Left=200, Top=179, cSayPict='"!"', cGetPict='"!"', InputMask=replicate('X',1)

  func oCONFER_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CONFER $ 'SN')
    endwith
    return bRes
  endfunc


  add object oBtn_1_8 as StdButton with uid="RTRGAMJBJO",left=304, top=185, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 107590938;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPAGINE_1_9 as StdField with uid="ZSUVUNFALC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PAGINE", cQueryName = "PAGINE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 63148278,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=200, Top=98, cSayPict='"9999999"', cGetPict='"9999999"'


  add object oObj_1_13 as cp_calclbl with uid="VEILTTHURC",left=8, top=23, width=347,height=73,;
    caption='Aggiornamento dei progressivi',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 265761729

  add object oStr_1_3 as StdString with uid="GHKAJSWBQK",Visible=.t., Left=8, Top=6,;
    Alignment=0, Width=347, Height=15,;
    Caption="Attenzione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="GQXQJGXPUZ",Visible=.t., Left=5, Top=148,;
    Alignment=1, Width=192, Height=15,;
    Caption="Ultima data stampata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="AOWDJIXKPS",Visible=.t., Left=5, Top=123,;
    Alignment=1, Width=192, Height=15,;
    Caption="Ultima riga stampata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="KBBVCNQHRB",Visible=.t., Left=5, Top=179,;
    Alignment=1, Width=192, Height=15,;
    Caption="Confermi aggiornamento (S/N)?:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="IQVAJCMJJE",Visible=.t., Left=5, Top=98,;
    Alignment=1, Width=192, Height=15,;
    Caption="Ultima pagina stampata:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_s2l','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
