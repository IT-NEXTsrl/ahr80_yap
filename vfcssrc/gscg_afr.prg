* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_afr                                                        *
*              Modello F24 sez. erario-2007                                    *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-14                                                      *
* Last revis.: 2009-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_afr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_afr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_afr")
  return

* --- Class definition
define class tgscg_afr as StdPCForm
  Width  = 759
  Height = 296
  Top    = 68
  Left   = 84
  cComment = "Modello F24 sez. erario-2007"
  cPrg = "gscg_afr"
  HelpContextID=59385193
  add object cnt as tcgscg_afr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_afr as PCContext
  w_EFSERIAL = space(10)
  w_VALUTA = space(3)
  w_EFTRIER1 = space(5)
  w_EFRATER1 = space(4)
  w_EFMESER1 = space(2)
  w_EFANNER1 = space(4)
  w_EFIMDER1 = 0
  w_EFIMCER1 = 0
  w_EFTRIER2 = space(5)
  w_EFRATER2 = space(4)
  w_EFMESER2 = space(2)
  w_EFANNER2 = space(4)
  w_EFIMDER2 = 0
  w_EFIMCER2 = 0
  w_EFTRIER3 = space(5)
  w_EFRATER3 = space(4)
  w_EFMESER3 = space(2)
  w_EFANNER3 = space(4)
  w_EFIMDER3 = 0
  w_EFIMCER3 = 0
  w_EFTRIER4 = space(5)
  w_EFRATER4 = space(4)
  w_EFMESER4 = space(2)
  w_EFANNER4 = space(4)
  w_EFIMDER4 = 0
  w_EFIMCER4 = 0
  w_EFTRIER5 = space(5)
  w_EFRATER5 = space(4)
  w_EFMESER5 = space(2)
  w_EFANNER5 = space(4)
  w_EFIMDER5 = 0
  w_EFIMCER5 = 0
  w_EFTRIER6 = space(5)
  w_EFRATER6 = space(4)
  w_EFMESER6 = space(2)
  w_EFANNER6 = space(4)
  w_EFIMDER6 = 0
  w_EFIMCER6 = 0
  w_EFTOTDER = 0
  w_EFTOTCER = 0
  w_EFSALDER = 0
  w_TIPTRI = space(10)
  w_CHKOBBME1 = space(1)
  w_CHKOBBME2 = space(1)
  w_CHKOBBME3 = space(1)
  w_CHKOBBME4 = space(1)
  w_CHKOBBME5 = space(1)
  w_CHKOBBME6 = space(1)
  w_RESCHK = 0
  proc Save(oFrom)
    this.w_EFSERIAL = oFrom.w_EFSERIAL
    this.w_VALUTA = oFrom.w_VALUTA
    this.w_EFTRIER1 = oFrom.w_EFTRIER1
    this.w_EFRATER1 = oFrom.w_EFRATER1
    this.w_EFMESER1 = oFrom.w_EFMESER1
    this.w_EFANNER1 = oFrom.w_EFANNER1
    this.w_EFIMDER1 = oFrom.w_EFIMDER1
    this.w_EFIMCER1 = oFrom.w_EFIMCER1
    this.w_EFTRIER2 = oFrom.w_EFTRIER2
    this.w_EFRATER2 = oFrom.w_EFRATER2
    this.w_EFMESER2 = oFrom.w_EFMESER2
    this.w_EFANNER2 = oFrom.w_EFANNER2
    this.w_EFIMDER2 = oFrom.w_EFIMDER2
    this.w_EFIMCER2 = oFrom.w_EFIMCER2
    this.w_EFTRIER3 = oFrom.w_EFTRIER3
    this.w_EFRATER3 = oFrom.w_EFRATER3
    this.w_EFMESER3 = oFrom.w_EFMESER3
    this.w_EFANNER3 = oFrom.w_EFANNER3
    this.w_EFIMDER3 = oFrom.w_EFIMDER3
    this.w_EFIMCER3 = oFrom.w_EFIMCER3
    this.w_EFTRIER4 = oFrom.w_EFTRIER4
    this.w_EFRATER4 = oFrom.w_EFRATER4
    this.w_EFMESER4 = oFrom.w_EFMESER4
    this.w_EFANNER4 = oFrom.w_EFANNER4
    this.w_EFIMDER4 = oFrom.w_EFIMDER4
    this.w_EFIMCER4 = oFrom.w_EFIMCER4
    this.w_EFTRIER5 = oFrom.w_EFTRIER5
    this.w_EFRATER5 = oFrom.w_EFRATER5
    this.w_EFMESER5 = oFrom.w_EFMESER5
    this.w_EFANNER5 = oFrom.w_EFANNER5
    this.w_EFIMDER5 = oFrom.w_EFIMDER5
    this.w_EFIMCER5 = oFrom.w_EFIMCER5
    this.w_EFTRIER6 = oFrom.w_EFTRIER6
    this.w_EFRATER6 = oFrom.w_EFRATER6
    this.w_EFMESER6 = oFrom.w_EFMESER6
    this.w_EFANNER6 = oFrom.w_EFANNER6
    this.w_EFIMDER6 = oFrom.w_EFIMDER6
    this.w_EFIMCER6 = oFrom.w_EFIMCER6
    this.w_EFTOTDER = oFrom.w_EFTOTDER
    this.w_EFTOTCER = oFrom.w_EFTOTCER
    this.w_EFSALDER = oFrom.w_EFSALDER
    this.w_TIPTRI = oFrom.w_TIPTRI
    this.w_CHKOBBME1 = oFrom.w_CHKOBBME1
    this.w_CHKOBBME2 = oFrom.w_CHKOBBME2
    this.w_CHKOBBME3 = oFrom.w_CHKOBBME3
    this.w_CHKOBBME4 = oFrom.w_CHKOBBME4
    this.w_CHKOBBME5 = oFrom.w_CHKOBBME5
    this.w_CHKOBBME6 = oFrom.w_CHKOBBME6
    this.w_RESCHK = oFrom.w_RESCHK
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_EFSERIAL = this.w_EFSERIAL
    oTo.w_VALUTA = this.w_VALUTA
    oTo.w_EFTRIER1 = this.w_EFTRIER1
    oTo.w_EFRATER1 = this.w_EFRATER1
    oTo.w_EFMESER1 = this.w_EFMESER1
    oTo.w_EFANNER1 = this.w_EFANNER1
    oTo.w_EFIMDER1 = this.w_EFIMDER1
    oTo.w_EFIMCER1 = this.w_EFIMCER1
    oTo.w_EFTRIER2 = this.w_EFTRIER2
    oTo.w_EFRATER2 = this.w_EFRATER2
    oTo.w_EFMESER2 = this.w_EFMESER2
    oTo.w_EFANNER2 = this.w_EFANNER2
    oTo.w_EFIMDER2 = this.w_EFIMDER2
    oTo.w_EFIMCER2 = this.w_EFIMCER2
    oTo.w_EFTRIER3 = this.w_EFTRIER3
    oTo.w_EFRATER3 = this.w_EFRATER3
    oTo.w_EFMESER3 = this.w_EFMESER3
    oTo.w_EFANNER3 = this.w_EFANNER3
    oTo.w_EFIMDER3 = this.w_EFIMDER3
    oTo.w_EFIMCER3 = this.w_EFIMCER3
    oTo.w_EFTRIER4 = this.w_EFTRIER4
    oTo.w_EFRATER4 = this.w_EFRATER4
    oTo.w_EFMESER4 = this.w_EFMESER4
    oTo.w_EFANNER4 = this.w_EFANNER4
    oTo.w_EFIMDER4 = this.w_EFIMDER4
    oTo.w_EFIMCER4 = this.w_EFIMCER4
    oTo.w_EFTRIER5 = this.w_EFTRIER5
    oTo.w_EFRATER5 = this.w_EFRATER5
    oTo.w_EFMESER5 = this.w_EFMESER5
    oTo.w_EFANNER5 = this.w_EFANNER5
    oTo.w_EFIMDER5 = this.w_EFIMDER5
    oTo.w_EFIMCER5 = this.w_EFIMCER5
    oTo.w_EFTRIER6 = this.w_EFTRIER6
    oTo.w_EFRATER6 = this.w_EFRATER6
    oTo.w_EFMESER6 = this.w_EFMESER6
    oTo.w_EFANNER6 = this.w_EFANNER6
    oTo.w_EFIMDER6 = this.w_EFIMDER6
    oTo.w_EFIMCER6 = this.w_EFIMCER6
    oTo.w_EFTOTDER = this.w_EFTOTDER
    oTo.w_EFTOTCER = this.w_EFTOTCER
    oTo.w_EFSALDER = this.w_EFSALDER
    oTo.w_TIPTRI = this.w_TIPTRI
    oTo.w_CHKOBBME1 = this.w_CHKOBBME1
    oTo.w_CHKOBBME2 = this.w_CHKOBBME2
    oTo.w_CHKOBBME3 = this.w_CHKOBBME3
    oTo.w_CHKOBBME4 = this.w_CHKOBBME4
    oTo.w_CHKOBBME5 = this.w_CHKOBBME5
    oTo.w_CHKOBBME6 = this.w_CHKOBBME6
    oTo.w_RESCHK = this.w_RESCHK
    PCContext::Load(oTo)
enddefine

define class tcgscg_afr as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 759
  Height = 296
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-04-08"
  HelpContextID=59385193
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=49

  * --- Constant Properties
  MODEPAG_IDX = 0
  CODI_UFF_IDX = 0
  COD_TRIB_IDX = 0
  cFile = "MODEPAG"
  cKeySelect = "EFSERIAL"
  cKeyWhere  = "EFSERIAL=this.w_EFSERIAL"
  cKeyWhereODBC = '"EFSERIAL="+cp_ToStrODBC(this.w_EFSERIAL)';

  cKeyWhereODBCqualified = '"MODEPAG.EFSERIAL="+cp_ToStrODBC(this.w_EFSERIAL)';

  cPrg = "gscg_afr"
  cComment = "Modello F24 sez. erario-2007"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_EFSERIAL = space(10)
  w_VALUTA = space(3)
  w_EFTRIER1 = space(5)
  o_EFTRIER1 = space(5)
  w_EFRATER1 = space(4)
  w_EFMESER1 = space(2)
  w_EFANNER1 = space(4)
  w_EFIMDER1 = 0
  w_EFIMCER1 = 0
  w_EFTRIER2 = space(5)
  o_EFTRIER2 = space(5)
  w_EFRATER2 = space(4)
  w_EFMESER2 = space(2)
  w_EFANNER2 = space(4)
  w_EFIMDER2 = 0
  w_EFIMCER2 = 0
  w_EFTRIER3 = space(5)
  o_EFTRIER3 = space(5)
  w_EFRATER3 = space(4)
  w_EFMESER3 = space(2)
  w_EFANNER3 = space(4)
  w_EFIMDER3 = 0
  w_EFIMCER3 = 0
  w_EFTRIER4 = space(5)
  o_EFTRIER4 = space(5)
  w_EFRATER4 = space(4)
  w_EFMESER4 = space(2)
  w_EFANNER4 = space(4)
  w_EFIMDER4 = 0
  w_EFIMCER4 = 0
  w_EFTRIER5 = space(5)
  o_EFTRIER5 = space(5)
  w_EFRATER5 = space(4)
  w_EFMESER5 = space(2)
  w_EFANNER5 = space(4)
  w_EFIMDER5 = 0
  w_EFIMCER5 = 0
  w_EFTRIER6 = space(5)
  o_EFTRIER6 = space(5)
  w_EFRATER6 = space(4)
  w_EFMESER6 = space(2)
  w_EFANNER6 = space(4)
  w_EFIMDER6 = 0
  w_EFIMCER6 = 0
  w_EFTOTDER = 0
  w_EFTOTCER = 0
  w_EFSALDER = 0
  w_TIPTRI = space(10)
  w_CHKOBBME1 = space(1)
  o_CHKOBBME1 = space(1)
  w_CHKOBBME2 = space(1)
  o_CHKOBBME2 = space(1)
  w_CHKOBBME3 = space(1)
  o_CHKOBBME3 = space(1)
  w_CHKOBBME4 = space(1)
  o_CHKOBBME4 = space(1)
  w_CHKOBBME5 = space(1)
  o_CHKOBBME5 = space(1)
  w_CHKOBBME6 = space(1)
  o_CHKOBBME6 = space(1)
  w_RESCHK = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_afrPag1","gscg_afr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 937738
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oEFTRIER1_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CODI_UFF'
    this.cWorkTables[2]='COD_TRIB'
    this.cWorkTables[3]='MODEPAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MODEPAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MODEPAG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_afr'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    local link_1_35_joined
    link_1_35_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MODEPAG where EFSERIAL=KeySet.EFSERIAL
    *
    i_nConn = i_TableProp[this.MODEPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODEPAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MODEPAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MODEPAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MODEPAG '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_35_joined=this.AddJoinedLink_1_35(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'EFSERIAL',this.w_EFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPTRI = space(10)
        .w_CHKOBBME1 = space(1)
        .w_CHKOBBME2 = space(1)
        .w_CHKOBBME3 = space(1)
        .w_CHKOBBME4 = space(1)
        .w_CHKOBBME5 = space(1)
        .w_CHKOBBME6 = space(1)
        .w_RESCHK = 0
        .w_EFSERIAL = NVL(EFSERIAL,space(10))
        .w_VALUTA = .oParentObject .w_MFVALUTA
        .w_EFTRIER1 = NVL(EFTRIER1,space(5))
          if link_1_3_joined
            this.w_EFTRIER1 = NVL(TRCODICE103,NVL(this.w_EFTRIER1,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI103,space(10))
            this.w_CHKOBBME1 = NVL(TRFLMERF103,space(1))
          else
          .link_1_3('Load')
          endif
        .w_EFRATER1 = NVL(EFRATER1,space(4))
        .w_EFMESER1 = NVL(EFMESER1,space(2))
        .w_EFANNER1 = NVL(EFANNER1,space(4))
        .w_EFIMDER1 = NVL(EFIMDER1,0)
        .w_EFIMCER1 = NVL(EFIMCER1,0)
        .w_EFTRIER2 = NVL(EFTRIER2,space(5))
          if link_1_9_joined
            this.w_EFTRIER2 = NVL(TRCODICE109,NVL(this.w_EFTRIER2,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI109,space(10))
            this.w_CHKOBBME2 = NVL(TRFLMERF109,space(1))
          else
          .link_1_9('Load')
          endif
        .w_EFRATER2 = NVL(EFRATER2,space(4))
        .w_EFMESER2 = NVL(EFMESER2,space(2))
        .w_EFANNER2 = NVL(EFANNER2,space(4))
        .w_EFIMDER2 = NVL(EFIMDER2,0)
        .w_EFIMCER2 = NVL(EFIMCER2,0)
        .w_EFTRIER3 = NVL(EFTRIER3,space(5))
          if link_1_15_joined
            this.w_EFTRIER3 = NVL(TRCODICE115,NVL(this.w_EFTRIER3,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI115,space(10))
            this.w_CHKOBBME3 = NVL(TRFLMERF115,space(1))
          else
          .link_1_15('Load')
          endif
        .w_EFRATER3 = NVL(EFRATER3,space(4))
        .w_EFMESER3 = NVL(EFMESER3,space(2))
        .w_EFANNER3 = NVL(EFANNER3,space(4))
        .w_EFIMDER3 = NVL(EFIMDER3,0)
        .w_EFIMCER3 = NVL(EFIMCER3,0)
        .w_EFTRIER4 = NVL(EFTRIER4,space(5))
          if link_1_21_joined
            this.w_EFTRIER4 = NVL(TRCODICE121,NVL(this.w_EFTRIER4,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI121,space(10))
            this.w_CHKOBBME4 = NVL(TRFLMERF121,space(1))
          else
          .link_1_21('Load')
          endif
        .w_EFRATER4 = NVL(EFRATER4,space(4))
        .w_EFMESER4 = NVL(EFMESER4,space(2))
        .w_EFANNER4 = NVL(EFANNER4,space(4))
        .w_EFIMDER4 = NVL(EFIMDER4,0)
        .w_EFIMCER4 = NVL(EFIMCER4,0)
        .w_EFTRIER5 = NVL(EFTRIER5,space(5))
          if link_1_29_joined
            this.w_EFTRIER5 = NVL(TRCODICE129,NVL(this.w_EFTRIER5,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI129,space(10))
            this.w_CHKOBBME5 = NVL(TRFLMERF129,space(1))
          else
          .link_1_29('Load')
          endif
        .w_EFRATER5 = NVL(EFRATER5,space(4))
        .w_EFMESER5 = NVL(EFMESER5,space(2))
        .w_EFANNER5 = NVL(EFANNER5,space(4))
        .w_EFIMDER5 = NVL(EFIMDER5,0)
        .w_EFIMCER5 = NVL(EFIMCER5,0)
        .w_EFTRIER6 = NVL(EFTRIER6,space(5))
          if link_1_35_joined
            this.w_EFTRIER6 = NVL(TRCODICE135,NVL(this.w_EFTRIER6,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI135,space(10))
            this.w_CHKOBBME6 = NVL(TRFLMERF135,space(1))
          else
          .link_1_35('Load')
          endif
        .w_EFRATER6 = NVL(EFRATER6,space(4))
        .w_EFMESER6 = NVL(EFMESER6,space(2))
        .w_EFANNER6 = NVL(EFANNER6,space(4))
        .w_EFIMDER6 = NVL(EFIMDER6,0)
        .w_EFIMCER6 = NVL(EFIMCER6,0)
        .w_EFTOTDER = NVL(EFTOTDER,0)
        .w_EFTOTCER = NVL(EFTOTCER,0)
        .w_EFSALDER = NVL(EFSALDER,0)
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(ah_MSGFORMAT('Mese di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(ah_MSGFORMAT('Rateazione/%0Regione/Prov.'))
        cp_LoadRecExtFlds(this,'MODEPAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_afr
    local lTot
    lTot=0
    with this.oParentObject
       lTot = .w_MFSALDPS + .w_MFSALDRE  + .w_MFSALINA + .w_MFSALAEN + .w_APPOIMP2
    endwith
    * aggiorna variabile sul padre
    this.oParentObject.w_APPOIMP = this.w_EFSALDER
    this.oParentObject.w_MFSALFIN = this.w_EFSALDER + lTot
    this.oParentObject.SetControlsValue()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_EFSERIAL = space(10)
      .w_VALUTA = space(3)
      .w_EFTRIER1 = space(5)
      .w_EFRATER1 = space(4)
      .w_EFMESER1 = space(2)
      .w_EFANNER1 = space(4)
      .w_EFIMDER1 = 0
      .w_EFIMCER1 = 0
      .w_EFTRIER2 = space(5)
      .w_EFRATER2 = space(4)
      .w_EFMESER2 = space(2)
      .w_EFANNER2 = space(4)
      .w_EFIMDER2 = 0
      .w_EFIMCER2 = 0
      .w_EFTRIER3 = space(5)
      .w_EFRATER3 = space(4)
      .w_EFMESER3 = space(2)
      .w_EFANNER3 = space(4)
      .w_EFIMDER3 = 0
      .w_EFIMCER3 = 0
      .w_EFTRIER4 = space(5)
      .w_EFRATER4 = space(4)
      .w_EFMESER4 = space(2)
      .w_EFANNER4 = space(4)
      .w_EFIMDER4 = 0
      .w_EFIMCER4 = 0
      .w_EFTRIER5 = space(5)
      .w_EFRATER5 = space(4)
      .w_EFMESER5 = space(2)
      .w_EFANNER5 = space(4)
      .w_EFIMDER5 = 0
      .w_EFIMCER5 = 0
      .w_EFTRIER6 = space(5)
      .w_EFRATER6 = space(4)
      .w_EFMESER6 = space(2)
      .w_EFANNER6 = space(4)
      .w_EFIMDER6 = 0
      .w_EFIMCER6 = 0
      .w_EFTOTDER = 0
      .w_EFTOTCER = 0
      .w_EFSALDER = 0
      .w_TIPTRI = space(10)
      .w_CHKOBBME1 = space(1)
      .w_CHKOBBME2 = space(1)
      .w_CHKOBBME3 = space(1)
      .w_CHKOBBME4 = space(1)
      .w_CHKOBBME5 = space(1)
      .w_CHKOBBME6 = space(1)
      .w_RESCHK = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_VALUTA = .oParentObject .w_MFVALUTA
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_EFTRIER1))
          .link_1_3('Full')
          endif
        .w_EFRATER1 = iif(empty(.w_EFTRIER1) or .w_CHKOBBME1='S',' ',.w_EFRATER1)
        .w_EFMESER1 = iif(empty(.w_EFTRIER1),' ',.w_EFMESER1)
        .w_EFANNER1 = iif(empty(.w_EFTRIER1),' ',iif(left(.w_EFTRIER1,4) = '1668','0000',.w_EFANNER1))
        .w_EFIMDER1 = iif(empty(.w_EFTRIER1),0,.w_EFIMDER1)
        .w_EFIMCER1 = iif(empty(.w_EFTRIER1),0,.w_EFIMCER1)
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_EFTRIER2))
          .link_1_9('Full')
          endif
        .w_EFRATER2 = iif(empty(.w_EFTRIER2) or .w_CHKOBBME2='S',' ',.w_EFRATER2)
        .w_EFMESER2 = iif(empty(.w_EFTRIER2),' ',.w_EFMESER2)
        .w_EFANNER2 = iif(empty(.w_EFTRIER2),' ',iif(left(.w_EFTRIER2,4) = '1668','0000',.w_EFANNER2))
        .w_EFIMDER2 = iif(empty(.w_EFTRIER2),0,.w_EFIMDER2)
        .w_EFIMCER2 = iif(empty(.w_EFTRIER2),0,.w_EFIMCER2)
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_EFTRIER3))
          .link_1_15('Full')
          endif
        .w_EFRATER3 = iif(empty(.w_EFTRIER3) or .w_CHKOBBME3='S',' ',.w_EFRATER3)
        .w_EFMESER3 = iif(empty(.w_EFTRIER3),' ',.w_EFMESER3)
        .w_EFANNER3 = iif(empty(.w_EFTRIER3),' ',iif(left(.w_EFTRIER3,4) = '1668','0000',.w_EFANNER3))
        .w_EFIMDER3 = iif(empty(.w_EFTRIER3),0,.w_EFIMDER3)
        .w_EFIMCER3 = iif(empty(.w_EFTRIER3),0,.w_EFIMCER3)
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_EFTRIER4))
          .link_1_21('Full')
          endif
        .w_EFRATER4 = iif(empty(.w_EFTRIER4) or .w_CHKOBBME4='S',' ',.w_EFRATER4)
        .w_EFMESER4 = iif(empty(.w_EFTRIER4),' ',.w_EFMESER4)
        .w_EFANNER4 = iif(empty(.w_EFTRIER4),' ',iif(left(.w_EFTRIER4,4) = '1668','0000',.w_EFANNER4))
        .w_EFIMDER4 = iif(empty(.w_EFTRIER4),0,.w_EFIMDER4)
        .w_EFIMCER4 = iif(empty(.w_EFTRIER4),0,.w_EFIMCER4)
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_EFTRIER5))
          .link_1_29('Full')
          endif
        .w_EFRATER5 = iif(empty(.w_EFTRIER5) or .w_CHKOBBME5='S',' ',.w_EFRATER5)
        .w_EFMESER5 = iif(empty(.w_EFTRIER5),' ',.w_EFMESER5)
        .w_EFANNER5 = iif(empty(.w_EFTRIER5),' ',iif(left(.w_EFTRIER5,4) = '1668','0000',.w_EFANNER5))
        .w_EFIMDER5 = iif(empty(.w_EFTRIER5),0,.w_EFIMDER5)
        .w_EFIMCER5 = iif(empty(.w_EFTRIER5),0,.w_EFIMCER5)
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_EFTRIER6))
          .link_1_35('Full')
          endif
        .w_EFRATER6 = iif(empty(.w_EFTRIER6) or .w_CHKOBBME6='S',' ',.w_EFRATER6)
        .w_EFMESER6 = iif(empty(.w_EFTRIER6),' ',.w_EFMESER6)
        .w_EFANNER6 = iif(empty(.w_EFTRIER6),' ',iif(left(.w_EFTRIER6,4) = '1668','0000',.w_EFANNER6))
        .w_EFIMDER6 = iif(empty(.w_EFTRIER6),0,.w_EFIMDER6)
        .w_EFIMCER6 = iif(empty(.w_EFTRIER6),0,.w_EFIMCER6)
        .w_EFTOTDER = .w_EFIMDER1+.w_EFIMDER2+.w_EFIMDER3+.w_EFIMDER4+.w_EFIMDER5+.w_EFIMDER6
        .w_EFTOTCER = .w_EFIMCER1+.w_EFIMCER2+.w_EFIMCER3+.w_EFIMCER4+.w_EFIMCER5+.w_EFIMCER6
        .w_EFSALDER = .w_EFTOTDER-.w_EFTOTCER
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(ah_MSGFORMAT('Mese di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(ah_MSGFORMAT('Rateazione/%0Regione/Prov.'))
      endif
    endwith
    cp_BlankRecExtFlds(this,'MODEPAG')
    this.DoRTCalc(42,49,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_afr
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oEFTRIER1_1_3.enabled = i_bVal
      .Page1.oPag.oEFRATER1_1_4.enabled = i_bVal
      .Page1.oPag.oEFMESER1_1_5.enabled = i_bVal
      .Page1.oPag.oEFANNER1_1_6.enabled = i_bVal
      .Page1.oPag.oEFIMDER1_1_7.enabled = i_bVal
      .Page1.oPag.oEFIMCER1_1_8.enabled = i_bVal
      .Page1.oPag.oEFTRIER2_1_9.enabled = i_bVal
      .Page1.oPag.oEFRATER2_1_10.enabled = i_bVal
      .Page1.oPag.oEFMESER2_1_11.enabled = i_bVal
      .Page1.oPag.oEFANNER2_1_12.enabled = i_bVal
      .Page1.oPag.oEFIMDER2_1_13.enabled = i_bVal
      .Page1.oPag.oEFIMCER2_1_14.enabled = i_bVal
      .Page1.oPag.oEFTRIER3_1_15.enabled = i_bVal
      .Page1.oPag.oEFRATER3_1_16.enabled = i_bVal
      .Page1.oPag.oEFMESER3_1_17.enabled = i_bVal
      .Page1.oPag.oEFANNER3_1_18.enabled = i_bVal
      .Page1.oPag.oEFIMDER3_1_19.enabled = i_bVal
      .Page1.oPag.oEFIMCER3_1_20.enabled = i_bVal
      .Page1.oPag.oEFTRIER4_1_21.enabled = i_bVal
      .Page1.oPag.oEFRATER4_1_22.enabled = i_bVal
      .Page1.oPag.oEFMESER4_1_23.enabled = i_bVal
      .Page1.oPag.oEFANNER4_1_24.enabled = i_bVal
      .Page1.oPag.oEFIMDER4_1_25.enabled = i_bVal
      .Page1.oPag.oEFIMCER4_1_26.enabled = i_bVal
      .Page1.oPag.oEFTRIER5_1_29.enabled = i_bVal
      .Page1.oPag.oEFRATER5_1_30.enabled = i_bVal
      .Page1.oPag.oEFMESER5_1_31.enabled = i_bVal
      .Page1.oPag.oEFANNER5_1_32.enabled = i_bVal
      .Page1.oPag.oEFIMDER5_1_33.enabled = i_bVal
      .Page1.oPag.oEFIMCER5_1_34.enabled = i_bVal
      .Page1.oPag.oEFTRIER6_1_35.enabled = i_bVal
      .Page1.oPag.oEFRATER6_1_36.enabled = i_bVal
      .Page1.oPag.oEFMESER6_1_37.enabled = i_bVal
      .Page1.oPag.oEFANNER6_1_38.enabled = i_bVal
      .Page1.oPag.oEFIMDER6_1_39.enabled = i_bVal
      .Page1.oPag.oEFIMCER6_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MODEPAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MODEPAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFSERIAL,"EFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER1,"EFTRIER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER1,"EFRATER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFMESER1,"EFMESER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER1,"EFANNER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER1,"EFIMDER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER1,"EFIMCER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER2,"EFTRIER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER2,"EFRATER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFMESER2,"EFMESER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER2,"EFANNER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER2,"EFIMDER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER2,"EFIMCER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER3,"EFTRIER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER3,"EFRATER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFMESER3,"EFMESER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER3,"EFANNER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER3,"EFIMDER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER3,"EFIMCER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER4,"EFTRIER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER4,"EFRATER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFMESER4,"EFMESER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER4,"EFANNER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER4,"EFIMDER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER4,"EFIMCER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER5,"EFTRIER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER5,"EFRATER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFMESER5,"EFMESER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER5,"EFANNER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER5,"EFIMDER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER5,"EFIMCER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER6,"EFTRIER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER6,"EFRATER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFMESER6,"EFMESER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER6,"EFANNER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER6,"EFIMDER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER6,"EFIMCER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTOTDER,"EFTOTDER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTOTCER,"EFTOTCER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFSALDER,"EFSALDER",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODEPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODEPAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MODEPAG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MODEPAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MODEPAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MODEPAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(EFSERIAL,EFTRIER1,EFRATER1,EFMESER1,EFANNER1"+;
                  ",EFIMDER1,EFIMCER1,EFTRIER2,EFRATER2,EFMESER2"+;
                  ",EFANNER2,EFIMDER2,EFIMCER2,EFTRIER3,EFRATER3"+;
                  ",EFMESER3,EFANNER3,EFIMDER3,EFIMCER3,EFTRIER4"+;
                  ",EFRATER4,EFMESER4,EFANNER4,EFIMDER4,EFIMCER4"+;
                  ",EFTRIER5,EFRATER5,EFMESER5,EFANNER5,EFIMDER5"+;
                  ",EFIMCER5,EFTRIER6,EFRATER6,EFMESER6,EFANNER6"+;
                  ",EFIMDER6,EFIMCER6,EFTOTDER,EFTOTCER,EFSALDER "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_EFSERIAL)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER1)+;
                  ","+cp_ToStrODBC(this.w_EFRATER1)+;
                  ","+cp_ToStrODBC(this.w_EFMESER1)+;
                  ","+cp_ToStrODBC(this.w_EFANNER1)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER1)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER1)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER2)+;
                  ","+cp_ToStrODBC(this.w_EFRATER2)+;
                  ","+cp_ToStrODBC(this.w_EFMESER2)+;
                  ","+cp_ToStrODBC(this.w_EFANNER2)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER2)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER2)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER3)+;
                  ","+cp_ToStrODBC(this.w_EFRATER3)+;
                  ","+cp_ToStrODBC(this.w_EFMESER3)+;
                  ","+cp_ToStrODBC(this.w_EFANNER3)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER3)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER3)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER4)+;
                  ","+cp_ToStrODBC(this.w_EFRATER4)+;
                  ","+cp_ToStrODBC(this.w_EFMESER4)+;
                  ","+cp_ToStrODBC(this.w_EFANNER4)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER4)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER4)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER5)+;
                  ","+cp_ToStrODBC(this.w_EFRATER5)+;
                  ","+cp_ToStrODBC(this.w_EFMESER5)+;
                  ","+cp_ToStrODBC(this.w_EFANNER5)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER5)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER5)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER6)+;
                  ","+cp_ToStrODBC(this.w_EFRATER6)+;
                  ","+cp_ToStrODBC(this.w_EFMESER6)+;
                  ","+cp_ToStrODBC(this.w_EFANNER6)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER6)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER6)+;
                  ","+cp_ToStrODBC(this.w_EFTOTDER)+;
                  ","+cp_ToStrODBC(this.w_EFTOTCER)+;
                  ","+cp_ToStrODBC(this.w_EFSALDER)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MODEPAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MODEPAG')
        cp_CheckDeletedKey(i_cTable,0,'EFSERIAL',this.w_EFSERIAL)
        INSERT INTO (i_cTable);
              (EFSERIAL,EFTRIER1,EFRATER1,EFMESER1,EFANNER1,EFIMDER1,EFIMCER1,EFTRIER2,EFRATER2,EFMESER2,EFANNER2,EFIMDER2,EFIMCER2,EFTRIER3,EFRATER3,EFMESER3,EFANNER3,EFIMDER3,EFIMCER3,EFTRIER4,EFRATER4,EFMESER4,EFANNER4,EFIMDER4,EFIMCER4,EFTRIER5,EFRATER5,EFMESER5,EFANNER5,EFIMDER5,EFIMCER5,EFTRIER6,EFRATER6,EFMESER6,EFANNER6,EFIMDER6,EFIMCER6,EFTOTDER,EFTOTCER,EFSALDER  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_EFSERIAL;
                  ,this.w_EFTRIER1;
                  ,this.w_EFRATER1;
                  ,this.w_EFMESER1;
                  ,this.w_EFANNER1;
                  ,this.w_EFIMDER1;
                  ,this.w_EFIMCER1;
                  ,this.w_EFTRIER2;
                  ,this.w_EFRATER2;
                  ,this.w_EFMESER2;
                  ,this.w_EFANNER2;
                  ,this.w_EFIMDER2;
                  ,this.w_EFIMCER2;
                  ,this.w_EFTRIER3;
                  ,this.w_EFRATER3;
                  ,this.w_EFMESER3;
                  ,this.w_EFANNER3;
                  ,this.w_EFIMDER3;
                  ,this.w_EFIMCER3;
                  ,this.w_EFTRIER4;
                  ,this.w_EFRATER4;
                  ,this.w_EFMESER4;
                  ,this.w_EFANNER4;
                  ,this.w_EFIMDER4;
                  ,this.w_EFIMCER4;
                  ,this.w_EFTRIER5;
                  ,this.w_EFRATER5;
                  ,this.w_EFMESER5;
                  ,this.w_EFANNER5;
                  ,this.w_EFIMDER5;
                  ,this.w_EFIMCER5;
                  ,this.w_EFTRIER6;
                  ,this.w_EFRATER6;
                  ,this.w_EFMESER6;
                  ,this.w_EFANNER6;
                  ,this.w_EFIMDER6;
                  ,this.w_EFIMCER6;
                  ,this.w_EFTOTDER;
                  ,this.w_EFTOTCER;
                  ,this.w_EFSALDER;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MODEPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODEPAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MODEPAG_IDX,i_nConn)
      *
      * update MODEPAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MODEPAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " EFTRIER1="+cp_ToStrODBCNull(this.w_EFTRIER1)+;
             ",EFRATER1="+cp_ToStrODBC(this.w_EFRATER1)+;
             ",EFMESER1="+cp_ToStrODBC(this.w_EFMESER1)+;
             ",EFANNER1="+cp_ToStrODBC(this.w_EFANNER1)+;
             ",EFIMDER1="+cp_ToStrODBC(this.w_EFIMDER1)+;
             ",EFIMCER1="+cp_ToStrODBC(this.w_EFIMCER1)+;
             ",EFTRIER2="+cp_ToStrODBCNull(this.w_EFTRIER2)+;
             ",EFRATER2="+cp_ToStrODBC(this.w_EFRATER2)+;
             ",EFMESER2="+cp_ToStrODBC(this.w_EFMESER2)+;
             ",EFANNER2="+cp_ToStrODBC(this.w_EFANNER2)+;
             ",EFIMDER2="+cp_ToStrODBC(this.w_EFIMDER2)+;
             ",EFIMCER2="+cp_ToStrODBC(this.w_EFIMCER2)+;
             ",EFTRIER3="+cp_ToStrODBCNull(this.w_EFTRIER3)+;
             ",EFRATER3="+cp_ToStrODBC(this.w_EFRATER3)+;
             ",EFMESER3="+cp_ToStrODBC(this.w_EFMESER3)+;
             ",EFANNER3="+cp_ToStrODBC(this.w_EFANNER3)+;
             ",EFIMDER3="+cp_ToStrODBC(this.w_EFIMDER3)+;
             ",EFIMCER3="+cp_ToStrODBC(this.w_EFIMCER3)+;
             ",EFTRIER4="+cp_ToStrODBCNull(this.w_EFTRIER4)+;
             ",EFRATER4="+cp_ToStrODBC(this.w_EFRATER4)+;
             ",EFMESER4="+cp_ToStrODBC(this.w_EFMESER4)+;
             ",EFANNER4="+cp_ToStrODBC(this.w_EFANNER4)+;
             ",EFIMDER4="+cp_ToStrODBC(this.w_EFIMDER4)+;
             ",EFIMCER4="+cp_ToStrODBC(this.w_EFIMCER4)+;
             ",EFTRIER5="+cp_ToStrODBCNull(this.w_EFTRIER5)+;
             ",EFRATER5="+cp_ToStrODBC(this.w_EFRATER5)+;
             ",EFMESER5="+cp_ToStrODBC(this.w_EFMESER5)+;
             ",EFANNER5="+cp_ToStrODBC(this.w_EFANNER5)+;
             ",EFIMDER5="+cp_ToStrODBC(this.w_EFIMDER5)+;
             ",EFIMCER5="+cp_ToStrODBC(this.w_EFIMCER5)+;
             ",EFTRIER6="+cp_ToStrODBCNull(this.w_EFTRIER6)+;
             ",EFRATER6="+cp_ToStrODBC(this.w_EFRATER6)+;
             ",EFMESER6="+cp_ToStrODBC(this.w_EFMESER6)+;
             ",EFANNER6="+cp_ToStrODBC(this.w_EFANNER6)+;
             ",EFIMDER6="+cp_ToStrODBC(this.w_EFIMDER6)+;
             ",EFIMCER6="+cp_ToStrODBC(this.w_EFIMCER6)+;
             ",EFTOTDER="+cp_ToStrODBC(this.w_EFTOTDER)+;
             ",EFTOTCER="+cp_ToStrODBC(this.w_EFTOTCER)+;
             ",EFSALDER="+cp_ToStrODBC(this.w_EFSALDER)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MODEPAG')
        i_cWhere = cp_PKFox(i_cTable  ,'EFSERIAL',this.w_EFSERIAL  )
        UPDATE (i_cTable) SET;
              EFTRIER1=this.w_EFTRIER1;
             ,EFRATER1=this.w_EFRATER1;
             ,EFMESER1=this.w_EFMESER1;
             ,EFANNER1=this.w_EFANNER1;
             ,EFIMDER1=this.w_EFIMDER1;
             ,EFIMCER1=this.w_EFIMCER1;
             ,EFTRIER2=this.w_EFTRIER2;
             ,EFRATER2=this.w_EFRATER2;
             ,EFMESER2=this.w_EFMESER2;
             ,EFANNER2=this.w_EFANNER2;
             ,EFIMDER2=this.w_EFIMDER2;
             ,EFIMCER2=this.w_EFIMCER2;
             ,EFTRIER3=this.w_EFTRIER3;
             ,EFRATER3=this.w_EFRATER3;
             ,EFMESER3=this.w_EFMESER3;
             ,EFANNER3=this.w_EFANNER3;
             ,EFIMDER3=this.w_EFIMDER3;
             ,EFIMCER3=this.w_EFIMCER3;
             ,EFTRIER4=this.w_EFTRIER4;
             ,EFRATER4=this.w_EFRATER4;
             ,EFMESER4=this.w_EFMESER4;
             ,EFANNER4=this.w_EFANNER4;
             ,EFIMDER4=this.w_EFIMDER4;
             ,EFIMCER4=this.w_EFIMCER4;
             ,EFTRIER5=this.w_EFTRIER5;
             ,EFRATER5=this.w_EFRATER5;
             ,EFMESER5=this.w_EFMESER5;
             ,EFANNER5=this.w_EFANNER5;
             ,EFIMDER5=this.w_EFIMDER5;
             ,EFIMCER5=this.w_EFIMCER5;
             ,EFTRIER6=this.w_EFTRIER6;
             ,EFRATER6=this.w_EFRATER6;
             ,EFMESER6=this.w_EFMESER6;
             ,EFANNER6=this.w_EFANNER6;
             ,EFIMDER6=this.w_EFIMDER6;
             ,EFIMCER6=this.w_EFIMCER6;
             ,EFTOTDER=this.w_EFTOTDER;
             ,EFTOTCER=this.w_EFTOTCER;
             ,EFSALDER=this.w_EFSALDER;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MODEPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODEPAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MODEPAG_IDX,i_nConn)
      *
      * delete MODEPAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'EFSERIAL',this.w_EFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MODEPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODEPAG_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_VALUTA = .oParentObject .w_MFVALUTA
        if .o_EFTRIER1<>.w_EFTRIER1
          .link_1_3('Full')
        endif
        if .o_EFTRIER1<>.w_EFTRIER1
            .w_EFRATER1 = iif(empty(.w_EFTRIER1) or .w_CHKOBBME1='S',' ',.w_EFRATER1)
        endif
        if .o_EFTRIER1<>.w_EFTRIER1.or. .o_CHKOBBME1<>.w_CHKOBBME1
            .w_EFMESER1 = iif(empty(.w_EFTRIER1),' ',.w_EFMESER1)
        endif
        if .o_EFTRIER1<>.w_EFTRIER1
            .w_EFANNER1 = iif(empty(.w_EFTRIER1),' ',iif(left(.w_EFTRIER1,4) = '1668','0000',.w_EFANNER1))
        endif
        if .o_EFTRIER1<>.w_EFTRIER1
            .w_EFIMDER1 = iif(empty(.w_EFTRIER1),0,.w_EFIMDER1)
        endif
        if .o_EFTRIER1<>.w_EFTRIER1
            .w_EFIMCER1 = iif(empty(.w_EFTRIER1),0,.w_EFIMCER1)
        endif
        if .o_EFTRIER2<>.w_EFTRIER2
          .link_1_9('Full')
        endif
        if .o_EFTRIER2<>.w_EFTRIER2
            .w_EFRATER2 = iif(empty(.w_EFTRIER2) or .w_CHKOBBME2='S',' ',.w_EFRATER2)
        endif
        if .o_EFTRIER2<>.w_EFTRIER2.or. .o_CHKOBBME2<>.w_CHKOBBME2
            .w_EFMESER2 = iif(empty(.w_EFTRIER2),' ',.w_EFMESER2)
        endif
        if .o_EFTRIER2<>.w_EFTRIER2
            .w_EFANNER2 = iif(empty(.w_EFTRIER2),' ',iif(left(.w_EFTRIER2,4) = '1668','0000',.w_EFANNER2))
        endif
        if .o_EFTRIER2<>.w_EFTRIER2
            .w_EFIMDER2 = iif(empty(.w_EFTRIER2),0,.w_EFIMDER2)
        endif
        if .o_EFTRIER2<>.w_EFTRIER2
            .w_EFIMCER2 = iif(empty(.w_EFTRIER2),0,.w_EFIMCER2)
        endif
        if .o_EFTRIER3<>.w_EFTRIER3
          .link_1_15('Full')
        endif
        if .o_EFTRIER3<>.w_EFTRIER3
            .w_EFRATER3 = iif(empty(.w_EFTRIER3) or .w_CHKOBBME3='S',' ',.w_EFRATER3)
        endif
        if .o_EFTRIER3<>.w_EFTRIER3.or. .o_CHKOBBME3<>.w_CHKOBBME3
            .w_EFMESER3 = iif(empty(.w_EFTRIER3),' ',.w_EFMESER3)
        endif
        if .o_EFTRIER3<>.w_EFTRIER3
            .w_EFANNER3 = iif(empty(.w_EFTRIER3),' ',iif(left(.w_EFTRIER3,4) = '1668','0000',.w_EFANNER3))
        endif
        if .o_EFTRIER3<>.w_EFTRIER3
            .w_EFIMDER3 = iif(empty(.w_EFTRIER3),0,.w_EFIMDER3)
        endif
        if .o_EFTRIER3<>.w_EFTRIER3
            .w_EFIMCER3 = iif(empty(.w_EFTRIER3),0,.w_EFIMCER3)
        endif
        if .o_EFTRIER4<>.w_EFTRIER4
          .link_1_21('Full')
        endif
        if .o_EFTRIER4<>.w_EFTRIER4
            .w_EFRATER4 = iif(empty(.w_EFTRIER4) or .w_CHKOBBME4='S',' ',.w_EFRATER4)
        endif
        if .o_EFTRIER4<>.w_EFTRIER4.or. .o_CHKOBBME4<>.w_CHKOBBME4
            .w_EFMESER4 = iif(empty(.w_EFTRIER4),' ',.w_EFMESER4)
        endif
        if .o_EFTRIER4<>.w_EFTRIER4
            .w_EFANNER4 = iif(empty(.w_EFTRIER4),' ',iif(left(.w_EFTRIER4,4) = '1668','0000',.w_EFANNER4))
        endif
        if .o_EFTRIER4<>.w_EFTRIER4
            .w_EFIMDER4 = iif(empty(.w_EFTRIER4),0,.w_EFIMDER4)
        endif
        if .o_EFTRIER4<>.w_EFTRIER4
            .w_EFIMCER4 = iif(empty(.w_EFTRIER4),0,.w_EFIMCER4)
        endif
        if .o_EFTRIER5<>.w_EFTRIER5
          .link_1_29('Full')
        endif
        if .o_EFTRIER5<>.w_EFTRIER5
            .w_EFRATER5 = iif(empty(.w_EFTRIER5) or .w_CHKOBBME5='S',' ',.w_EFRATER5)
        endif
        if .o_EFTRIER5<>.w_EFTRIER5.or. .o_CHKOBBME5<>.w_CHKOBBME5
            .w_EFMESER5 = iif(empty(.w_EFTRIER5),' ',.w_EFMESER5)
        endif
        if .o_EFTRIER5<>.w_EFTRIER5
            .w_EFANNER5 = iif(empty(.w_EFTRIER5),' ',iif(left(.w_EFTRIER5,4) = '1668','0000',.w_EFANNER5))
        endif
        if .o_EFTRIER5<>.w_EFTRIER5
            .w_EFIMDER5 = iif(empty(.w_EFTRIER5),0,.w_EFIMDER5)
        endif
        if .o_EFTRIER5<>.w_EFTRIER5
            .w_EFIMCER5 = iif(empty(.w_EFTRIER5),0,.w_EFIMCER5)
        endif
        if .o_EFTRIER6<>.w_EFTRIER6
          .link_1_35('Full')
        endif
        if .o_EFTRIER6<>.w_EFTRIER6
            .w_EFRATER6 = iif(empty(.w_EFTRIER6) or .w_CHKOBBME6='S',' ',.w_EFRATER6)
        endif
        if .o_EFTRIER6<>.w_EFTRIER6.or. .o_CHKOBBME6<>.w_CHKOBBME6
            .w_EFMESER6 = iif(empty(.w_EFTRIER6),' ',.w_EFMESER6)
        endif
        if .o_EFTRIER6<>.w_EFTRIER6
            .w_EFANNER6 = iif(empty(.w_EFTRIER6),' ',iif(left(.w_EFTRIER6,4) = '1668','0000',.w_EFANNER6))
        endif
        if .o_EFTRIER6<>.w_EFTRIER6
            .w_EFIMDER6 = iif(empty(.w_EFTRIER6),0,.w_EFIMDER6)
        endif
        if .o_EFTRIER6<>.w_EFTRIER6
            .w_EFIMCER6 = iif(empty(.w_EFTRIER6),0,.w_EFIMCER6)
        endif
            .w_EFTOTDER = .w_EFIMDER1+.w_EFIMDER2+.w_EFIMDER3+.w_EFIMDER4+.w_EFIMDER5+.w_EFIMDER6
            .w_EFTOTCER = .w_EFIMCER1+.w_EFIMCER2+.w_EFIMCER3+.w_EFIMCER4+.w_EFIMCER5+.w_EFIMCER6
            .w_EFSALDER = .w_EFTOTDER-.w_EFTOTCER
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(ah_MSGFORMAT('Mese di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(ah_MSGFORMAT('Rateazione/%0Regione/Prov.'))
        * --- Area Manuale = Calculate
        * --- gscg_afr
        * aggiorna variabile sul padre
        .oParentObject.w_APPOIMP = .w_EFSALDER
        .oParentObject.w_MFSALFIN = .w_EFSALDER + .oParentObject.w_APPOIMP2+ .oParentObject.w_MFSALDPS + .oParentObject.w_MFSALDRE +  .oParentObject.w_MFSALINA + .oParentObject.w_MFSALAEN
        * aggiorna variabile sul padre
        .oParentObject.SetControlsValue()
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,49,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(ah_MSGFORMAT('Mese di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(ah_MSGFORMAT('Rateazione/%0Regione/Prov.'))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oEFRATER1_1_4.enabled = this.oPgFrm.Page1.oPag.oEFRATER1_1_4.mCond()
    this.oPgFrm.Page1.oPag.oEFMESER1_1_5.enabled = this.oPgFrm.Page1.oPag.oEFMESER1_1_5.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER1_1_6.enabled = this.oPgFrm.Page1.oPag.oEFANNER1_1_6.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER1_1_7.enabled = this.oPgFrm.Page1.oPag.oEFIMDER1_1_7.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER1_1_8.enabled = this.oPgFrm.Page1.oPag.oEFIMCER1_1_8.mCond()
    this.oPgFrm.Page1.oPag.oEFRATER2_1_10.enabled = this.oPgFrm.Page1.oPag.oEFRATER2_1_10.mCond()
    this.oPgFrm.Page1.oPag.oEFMESER2_1_11.enabled = this.oPgFrm.Page1.oPag.oEFMESER2_1_11.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER2_1_12.enabled = this.oPgFrm.Page1.oPag.oEFANNER2_1_12.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER2_1_13.enabled = this.oPgFrm.Page1.oPag.oEFIMDER2_1_13.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER2_1_14.enabled = this.oPgFrm.Page1.oPag.oEFIMCER2_1_14.mCond()
    this.oPgFrm.Page1.oPag.oEFRATER3_1_16.enabled = this.oPgFrm.Page1.oPag.oEFRATER3_1_16.mCond()
    this.oPgFrm.Page1.oPag.oEFMESER3_1_17.enabled = this.oPgFrm.Page1.oPag.oEFMESER3_1_17.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER3_1_18.enabled = this.oPgFrm.Page1.oPag.oEFANNER3_1_18.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER3_1_19.enabled = this.oPgFrm.Page1.oPag.oEFIMDER3_1_19.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER3_1_20.enabled = this.oPgFrm.Page1.oPag.oEFIMCER3_1_20.mCond()
    this.oPgFrm.Page1.oPag.oEFRATER4_1_22.enabled = this.oPgFrm.Page1.oPag.oEFRATER4_1_22.mCond()
    this.oPgFrm.Page1.oPag.oEFMESER4_1_23.enabled = this.oPgFrm.Page1.oPag.oEFMESER4_1_23.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER4_1_24.enabled = this.oPgFrm.Page1.oPag.oEFANNER4_1_24.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER4_1_25.enabled = this.oPgFrm.Page1.oPag.oEFIMDER4_1_25.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER4_1_26.enabled = this.oPgFrm.Page1.oPag.oEFIMCER4_1_26.mCond()
    this.oPgFrm.Page1.oPag.oEFRATER5_1_30.enabled = this.oPgFrm.Page1.oPag.oEFRATER5_1_30.mCond()
    this.oPgFrm.Page1.oPag.oEFMESER5_1_31.enabled = this.oPgFrm.Page1.oPag.oEFMESER5_1_31.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER5_1_32.enabled = this.oPgFrm.Page1.oPag.oEFANNER5_1_32.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER5_1_33.enabled = this.oPgFrm.Page1.oPag.oEFIMDER5_1_33.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER5_1_34.enabled = this.oPgFrm.Page1.oPag.oEFIMCER5_1_34.mCond()
    this.oPgFrm.Page1.oPag.oEFRATER6_1_36.enabled = this.oPgFrm.Page1.oPag.oEFRATER6_1_36.mCond()
    this.oPgFrm.Page1.oPag.oEFMESER6_1_37.enabled = this.oPgFrm.Page1.oPag.oEFMESER6_1_37.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER6_1_38.enabled = this.oPgFrm.Page1.oPag.oEFANNER6_1_38.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER6_1_39.enabled = this.oPgFrm.Page1.oPag.oEFIMDER6_1_39.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER6_1_40.enabled = this.oPgFrm.Page1.oPag.oEFIMCER6_1_40.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=EFTRIER1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER1))
          select TRCODICE,TRTIPTRI,TRFLMERF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER1)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER1) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER1_1_3'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER1)
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER1 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
      this.w_CHKOBBME1 = NVL(_Link_.TRFLMERF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER1 = space(5)
      endif
      this.w_TIPTRI = space(10)
      this.w_CHKOBBME1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
        endif
        this.w_EFTRIER1 = space(5)
        this.w_TIPTRI = space(10)
        this.w_CHKOBBME1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.TRCODICE as TRCODICE103"+ ",link_1_3.TRTIPTRI as TRTIPTRI103"+ ",link_1_3.TRFLMERF as TRFLMERF103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on MODEPAG.EFTRIER1=link_1_3.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and MODEPAG.EFTRIER1=link_1_3.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EFTRIER2
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER2))
          select TRCODICE,TRTIPTRI,TRFLMERF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER2)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER2) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER2_1_9'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER2)
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER2 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
      this.w_CHKOBBME2 = NVL(_Link_.TRFLMERF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER2 = space(5)
      endif
      this.w_TIPTRI = space(10)
      this.w_CHKOBBME2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
        endif
        this.w_EFTRIER2 = space(5)
        this.w_TIPTRI = space(10)
        this.w_CHKOBBME2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.TRCODICE as TRCODICE109"+ ",link_1_9.TRTIPTRI as TRTIPTRI109"+ ",link_1_9.TRFLMERF as TRFLMERF109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on MODEPAG.EFTRIER2=link_1_9.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and MODEPAG.EFTRIER2=link_1_9.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EFTRIER3
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER3))
          select TRCODICE,TRTIPTRI,TRFLMERF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER3)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER3) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER3_1_15'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER3)
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER3 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
      this.w_CHKOBBME3 = NVL(_Link_.TRFLMERF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER3 = space(5)
      endif
      this.w_TIPTRI = space(10)
      this.w_CHKOBBME3 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
        endif
        this.w_EFTRIER3 = space(5)
        this.w_TIPTRI = space(10)
        this.w_CHKOBBME3 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.TRCODICE as TRCODICE115"+ ",link_1_15.TRTIPTRI as TRTIPTRI115"+ ",link_1_15.TRFLMERF as TRFLMERF115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on MODEPAG.EFTRIER3=link_1_15.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and MODEPAG.EFTRIER3=link_1_15.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EFTRIER4
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER4))
          select TRCODICE,TRTIPTRI,TRFLMERF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER4)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER4) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER4_1_21'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER4)
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER4 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
      this.w_CHKOBBME4 = NVL(_Link_.TRFLMERF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER4 = space(5)
      endif
      this.w_TIPTRI = space(10)
      this.w_CHKOBBME4 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
        endif
        this.w_EFTRIER4 = space(5)
        this.w_TIPTRI = space(10)
        this.w_CHKOBBME4 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.TRCODICE as TRCODICE121"+ ",link_1_21.TRTIPTRI as TRTIPTRI121"+ ",link_1_21.TRFLMERF as TRFLMERF121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on MODEPAG.EFTRIER4=link_1_21.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and MODEPAG.EFTRIER4=link_1_21.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EFTRIER5
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER5)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER5))
          select TRCODICE,TRTIPTRI,TRFLMERF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER5)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER5) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER5_1_29'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER5)
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER5 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
      this.w_CHKOBBME5 = NVL(_Link_.TRFLMERF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER5 = space(5)
      endif
      this.w_TIPTRI = space(10)
      this.w_CHKOBBME5 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
        endif
        this.w_EFTRIER5 = space(5)
        this.w_TIPTRI = space(10)
        this.w_CHKOBBME5 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.TRCODICE as TRCODICE129"+ ",link_1_29.TRTIPTRI as TRTIPTRI129"+ ",link_1_29.TRFLMERF as TRFLMERF129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on MODEPAG.EFTRIER5=link_1_29.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and MODEPAG.EFTRIER5=link_1_29.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EFTRIER6
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER6)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER6))
          select TRCODICE,TRTIPTRI,TRFLMERF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER6)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER6) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER6_1_35'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER6)
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER6 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
      this.w_CHKOBBME6 = NVL(_Link_.TRFLMERF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER6 = space(5)
      endif
      this.w_TIPTRI = space(10)
      this.w_CHKOBBME6 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
        endif
        this.w_EFTRIER6 = space(5)
        this.w_TIPTRI = space(10)
        this.w_CHKOBBME6 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_35(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_35.TRCODICE as TRCODICE135"+ ",link_1_35.TRTIPTRI as TRTIPTRI135"+ ",link_1_35.TRFLMERF as TRFLMERF135"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_35 on MODEPAG.EFTRIER6=link_1_35.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_35"
          i_cKey=i_cKey+'+" and MODEPAG.EFTRIER6=link_1_35.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oEFTRIER1_1_3.value==this.w_EFTRIER1)
      this.oPgFrm.Page1.oPag.oEFTRIER1_1_3.value=this.w_EFTRIER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER1_1_4.value==this.w_EFRATER1)
      this.oPgFrm.Page1.oPag.oEFRATER1_1_4.value=this.w_EFRATER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFMESER1_1_5.value==this.w_EFMESER1)
      this.oPgFrm.Page1.oPag.oEFMESER1_1_5.value=this.w_EFMESER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER1_1_6.value==this.w_EFANNER1)
      this.oPgFrm.Page1.oPag.oEFANNER1_1_6.value=this.w_EFANNER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER1_1_7.value==this.w_EFIMDER1)
      this.oPgFrm.Page1.oPag.oEFIMDER1_1_7.value=this.w_EFIMDER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER1_1_8.value==this.w_EFIMCER1)
      this.oPgFrm.Page1.oPag.oEFIMCER1_1_8.value=this.w_EFIMCER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTRIER2_1_9.value==this.w_EFTRIER2)
      this.oPgFrm.Page1.oPag.oEFTRIER2_1_9.value=this.w_EFTRIER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER2_1_10.value==this.w_EFRATER2)
      this.oPgFrm.Page1.oPag.oEFRATER2_1_10.value=this.w_EFRATER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFMESER2_1_11.value==this.w_EFMESER2)
      this.oPgFrm.Page1.oPag.oEFMESER2_1_11.value=this.w_EFMESER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER2_1_12.value==this.w_EFANNER2)
      this.oPgFrm.Page1.oPag.oEFANNER2_1_12.value=this.w_EFANNER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER2_1_13.value==this.w_EFIMDER2)
      this.oPgFrm.Page1.oPag.oEFIMDER2_1_13.value=this.w_EFIMDER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER2_1_14.value==this.w_EFIMCER2)
      this.oPgFrm.Page1.oPag.oEFIMCER2_1_14.value=this.w_EFIMCER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTRIER3_1_15.value==this.w_EFTRIER3)
      this.oPgFrm.Page1.oPag.oEFTRIER3_1_15.value=this.w_EFTRIER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER3_1_16.value==this.w_EFRATER3)
      this.oPgFrm.Page1.oPag.oEFRATER3_1_16.value=this.w_EFRATER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFMESER3_1_17.value==this.w_EFMESER3)
      this.oPgFrm.Page1.oPag.oEFMESER3_1_17.value=this.w_EFMESER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER3_1_18.value==this.w_EFANNER3)
      this.oPgFrm.Page1.oPag.oEFANNER3_1_18.value=this.w_EFANNER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER3_1_19.value==this.w_EFIMDER3)
      this.oPgFrm.Page1.oPag.oEFIMDER3_1_19.value=this.w_EFIMDER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER3_1_20.value==this.w_EFIMCER3)
      this.oPgFrm.Page1.oPag.oEFIMCER3_1_20.value=this.w_EFIMCER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTRIER4_1_21.value==this.w_EFTRIER4)
      this.oPgFrm.Page1.oPag.oEFTRIER4_1_21.value=this.w_EFTRIER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER4_1_22.value==this.w_EFRATER4)
      this.oPgFrm.Page1.oPag.oEFRATER4_1_22.value=this.w_EFRATER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFMESER4_1_23.value==this.w_EFMESER4)
      this.oPgFrm.Page1.oPag.oEFMESER4_1_23.value=this.w_EFMESER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER4_1_24.value==this.w_EFANNER4)
      this.oPgFrm.Page1.oPag.oEFANNER4_1_24.value=this.w_EFANNER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER4_1_25.value==this.w_EFIMDER4)
      this.oPgFrm.Page1.oPag.oEFIMDER4_1_25.value=this.w_EFIMDER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER4_1_26.value==this.w_EFIMCER4)
      this.oPgFrm.Page1.oPag.oEFIMCER4_1_26.value=this.w_EFIMCER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTRIER5_1_29.value==this.w_EFTRIER5)
      this.oPgFrm.Page1.oPag.oEFTRIER5_1_29.value=this.w_EFTRIER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER5_1_30.value==this.w_EFRATER5)
      this.oPgFrm.Page1.oPag.oEFRATER5_1_30.value=this.w_EFRATER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFMESER5_1_31.value==this.w_EFMESER5)
      this.oPgFrm.Page1.oPag.oEFMESER5_1_31.value=this.w_EFMESER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER5_1_32.value==this.w_EFANNER5)
      this.oPgFrm.Page1.oPag.oEFANNER5_1_32.value=this.w_EFANNER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER5_1_33.value==this.w_EFIMDER5)
      this.oPgFrm.Page1.oPag.oEFIMDER5_1_33.value=this.w_EFIMDER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER5_1_34.value==this.w_EFIMCER5)
      this.oPgFrm.Page1.oPag.oEFIMCER5_1_34.value=this.w_EFIMCER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTRIER6_1_35.value==this.w_EFTRIER6)
      this.oPgFrm.Page1.oPag.oEFTRIER6_1_35.value=this.w_EFTRIER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER6_1_36.value==this.w_EFRATER6)
      this.oPgFrm.Page1.oPag.oEFRATER6_1_36.value=this.w_EFRATER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFMESER6_1_37.value==this.w_EFMESER6)
      this.oPgFrm.Page1.oPag.oEFMESER6_1_37.value=this.w_EFMESER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER6_1_38.value==this.w_EFANNER6)
      this.oPgFrm.Page1.oPag.oEFANNER6_1_38.value=this.w_EFANNER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER6_1_39.value==this.w_EFIMDER6)
      this.oPgFrm.Page1.oPag.oEFIMDER6_1_39.value=this.w_EFIMDER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER6_1_40.value==this.w_EFIMCER6)
      this.oPgFrm.Page1.oPag.oEFIMCER6_1_40.value=this.w_EFIMCER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTOTDER_1_41.value==this.w_EFTOTDER)
      this.oPgFrm.Page1.oPag.oEFTOTDER_1_41.value=this.w_EFTOTDER
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTOTCER_1_42.value==this.w_EFTOTCER)
      this.oPgFrm.Page1.oPag.oEFTOTCER_1_42.value=this.w_EFTOTCER
    endif
    if not(this.oPgFrm.Page1.oPag.oEFSALDER_1_43.value==this.w_EFSALDER)
      this.oPgFrm.Page1.oPag.oEFSALDER_1_43.value=this.w_EFSALDER
    endif
    cp_SetControlsValueExtFlds(this,'MODEPAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI))  and not(empty(.w_EFTRIER1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFTRIER1_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
          case   not((.w_EFMESER1>='01' and .w_EFMESER1<='12') or (empty(.w_EFMESER1) and .w_CHKOBBME1<>'S'))  and (!empty(.w_EFTRIER1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFMESER1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o dato obbligatorio")
          case   not(not empty(.w_EFANNER1) and (val(.w_EFANNER1)>=1996  and val(.w_EFANNER1)<=2050 or .w_EFANNER1=='0000'))  and (!empty(.w_EFTRIER1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER1_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI))  and not(empty(.w_EFTRIER2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFTRIER2_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
          case   not((.w_EFMESER2>='01' and .w_EFMESER2<='12') or (empty(.w_EFMESER2) and .w_CHKOBBME2<>'S'))  and (!empty(.w_EFTRIER2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFMESER2_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o dato obbligatorio")
          case   not(not empty(.w_EFANNER2) and (val(.w_EFANNER2)>=1996  and val(.w_EFANNER2)<=2050  or .w_EFANNER2=='0000'))  and (!empty(.w_EFTRIER2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER2_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI))  and not(empty(.w_EFTRIER3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFTRIER3_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
          case   not((.w_EFMESER3>='01' and .w_EFMESER3<='12') or (empty(.w_EFMESER3) and .w_CHKOBBME3<>'S'))  and (!empty(.w_EFTRIER3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFMESER3_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o dato obbligatorio")
          case   not(not empty(.w_EFANNER3) and (val(.w_EFANNER3)>=1996  and val(.w_EFANNER3)<=2050  or .w_EFANNER3=='0000'))  and (!empty(.w_EFTRIER3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER3_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI))  and not(empty(.w_EFTRIER4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFTRIER4_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
          case   not((.w_EFMESER4>='01' and .w_EFMESER4<='12') or (empty(.w_EFMESER4) and .w_CHKOBBME4<>'S'))  and (!empty(.w_EFTRIER4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFMESER4_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o dato obbligatorio")
          case   not(not empty(.w_EFANNER4) and (val(.w_EFANNER4)>=1996  and val(.w_EFANNER4)<=2050  or .w_EFANNER4=='0000'))  and (!empty(.w_EFTRIER4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER4_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI))  and not(empty(.w_EFTRIER5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFTRIER5_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
          case   not((.w_EFMESER5>='01' and .w_EFMESER5<='12') or (empty(.w_EFMESER5) and .w_CHKOBBME5<>'S'))  and (!empty(.w_EFTRIER5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFMESER5_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o dato obbligatorio")
          case   not(not empty(.w_EFANNER5) and (val(.w_EFANNER5)>=1996  and val(.w_EFANNER5)<=2050  or .w_EFANNER5=='0000'))  and (!empty(.w_EFTRIER5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER5_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(alltrim(.w_TIPTRI)='Erario' or empty(.w_TIPTRI))  and not(empty(.w_EFTRIER6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFTRIER6_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'erario'")
          case   not((.w_EFMESER6>='01' and .w_EFMESER6<='12') or (empty(.w_EFMESER6) and .w_CHKOBBME6<>'S'))  and (!empty(.w_EFTRIER6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFMESER6_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o dato obbligatorio")
          case   not(not empty(.w_EFANNER6) and (val(.w_EFANNER6)>=1996  and val(.w_EFANNER6)<=2050  or .w_EFANNER6=='0000'))  and (!empty(.w_EFTRIER6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER6_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_afr
      if i_bRes
        .w_RESCHK=0
        .NotifyEvent('ControllaDati')
        if .w_RESCHK<>0
          i_bRes=.f.
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_EFTRIER1 = this.w_EFTRIER1
    this.o_EFTRIER2 = this.w_EFTRIER2
    this.o_EFTRIER3 = this.w_EFTRIER3
    this.o_EFTRIER4 = this.w_EFTRIER4
    this.o_EFTRIER5 = this.w_EFTRIER5
    this.o_EFTRIER6 = this.w_EFTRIER6
    this.o_CHKOBBME1 = this.w_CHKOBBME1
    this.o_CHKOBBME2 = this.w_CHKOBBME2
    this.o_CHKOBBME3 = this.w_CHKOBBME3
    this.o_CHKOBBME4 = this.w_CHKOBBME4
    this.o_CHKOBBME5 = this.w_CHKOBBME5
    this.o_CHKOBBME6 = this.w_CHKOBBME6
    return

enddefine

* --- Define pages as container
define class tgscg_afrPag1 as StdContainer
  Width  = 755
  height = 296
  stdWidth  = 755
  stdheight = 296
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oEFTRIER1_1_3 as StdField with uid="GSXFTYQRTX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_EFTRIER1", cQueryName = "EFTRIER1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'erario'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651337,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=10, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER1"

  func oEFTRIER1_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER1_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER1_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER1_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER1_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER1
     i_obj.ecpSave()
  endproc

  add object oEFRATER1_1_4 as StdField with uid="KQMBCQJQBN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_EFRATER1", cQueryName = "EFRATER1",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/Regione/Provincia",;
    HelpContextID = 151239305,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=94, Top=84, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER1_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER1))
    endwith
   endif
  endfunc

  add object oEFMESER1_1_5 as StdField with uid="LVGGHREIUA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_EFMESER1", cQueryName = "EFMESER1",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o dato obbligatorio",;
    ToolTipText = "Mese di riferimento (mm)",;
    HelpContextID = 152046217,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=181, Top=84, InputMask=replicate('X',2)

  func oEFMESER1_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER1))
    endwith
   endif
  endfunc

  func oEFMESER1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EFMESER1>='01' and .w_EFMESER1<='12') or (empty(.w_EFMESER1) and .w_CHKOBBME1<>'S'))
    endwith
    return bRes
  endfunc

  add object oEFANNER1_1_6 as StdField with uid="SQVPJIBPBZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_EFANNER1", cQueryName = "EFANNER1",nZero=4,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748425,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=248, Top=84, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER1_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER1))
    endwith
   endif
  endfunc

  func oEFANNER1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER1) and (val(.w_EFANNER1)>=1996  and val(.w_EFANNER1)<=2050 or .w_EFANNER1=='0000'))
    endwith
    return bRes
  endfunc

  add object oEFIMDER1_1_7 as StdField with uid="SQAUCACRJC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_EFIMDER1", cQueryName = "EFIMDER1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266953,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=319, Top=84, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER1_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER1))
    endwith
   endif
  endfunc

  add object oEFIMCER1_1_8 as StdField with uid="XRKAAUKORU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_EFIMCER1", cQueryName = "EFIMCER1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315529,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=479, Top=84, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER1_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER1))
    endwith
   endif
  endfunc

  add object oEFTRIER2_1_9 as StdField with uid="GMMDAWOXYY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_EFTRIER2", cQueryName = "EFTRIER2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'erario'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651336,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=10, Top=112, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER2"

  func oEFTRIER2_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER2_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER2_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER2_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER2_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER2
     i_obj.ecpSave()
  endproc

  add object oEFRATER2_1_10 as StdField with uid="BILLKRJQQT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_EFRATER2", cQueryName = "EFRATER2",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/Regione/Provincia",;
    HelpContextID = 151239304,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=94, Top=112, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER2_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER2))
    endwith
   endif
  endfunc

  add object oEFMESER2_1_11 as StdField with uid="GKOTEVSELU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_EFMESER2", cQueryName = "EFMESER2",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o dato obbligatorio",;
    ToolTipText = "Mese di riferimento (mm)",;
    HelpContextID = 152046216,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=181, Top=112, InputMask=replicate('X',2)

  func oEFMESER2_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER2))
    endwith
   endif
  endfunc

  func oEFMESER2_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EFMESER2>='01' and .w_EFMESER2<='12') or (empty(.w_EFMESER2) and .w_CHKOBBME2<>'S'))
    endwith
    return bRes
  endfunc

  add object oEFANNER2_1_12 as StdField with uid="LQHGQKKJZS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_EFANNER2", cQueryName = "EFANNER2",nZero=4,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748424,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=248, Top=112, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER2_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER2))
    endwith
   endif
  endfunc

  func oEFANNER2_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER2) and (val(.w_EFANNER2)>=1996  and val(.w_EFANNER2)<=2050  or .w_EFANNER2=='0000'))
    endwith
    return bRes
  endfunc

  add object oEFIMDER2_1_13 as StdField with uid="NMSDTZDSVA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_EFIMDER2", cQueryName = "EFIMDER2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266952,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=319, Top=112, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER2_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER2))
    endwith
   endif
  endfunc

  add object oEFIMCER2_1_14 as StdField with uid="SQOCUCZTRB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_EFIMCER2", cQueryName = "EFIMCER2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315528,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=479, Top=112, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER2_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER2))
    endwith
   endif
  endfunc

  add object oEFTRIER3_1_15 as StdField with uid="MFZMLAQVCU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_EFTRIER3", cQueryName = "EFTRIER3",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'erario'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651335,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=10, Top=140, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER3"

  func oEFTRIER3_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER3_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER3_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER3_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER3_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER3
     i_obj.ecpSave()
  endproc

  add object oEFRATER3_1_16 as StdField with uid="QWIXKXZATJ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_EFRATER3", cQueryName = "EFRATER3",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/Regione/Provincia",;
    HelpContextID = 151239303,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=94, Top=140, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER3_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER3))
    endwith
   endif
  endfunc

  add object oEFMESER3_1_17 as StdField with uid="PZBYZLXBBX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_EFMESER3", cQueryName = "EFMESER3",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o dato obbligatorio",;
    ToolTipText = "Mese di riferimento (mm)",;
    HelpContextID = 152046215,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=181, Top=140, InputMask=replicate('X',2)

  func oEFMESER3_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER3))
    endwith
   endif
  endfunc

  func oEFMESER3_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EFMESER3>='01' and .w_EFMESER3<='12') or (empty(.w_EFMESER3) and .w_CHKOBBME3<>'S'))
    endwith
    return bRes
  endfunc

  add object oEFANNER3_1_18 as StdField with uid="WERPQWHZHU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_EFANNER3", cQueryName = "EFANNER3",nZero=4,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748423,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=248, Top=140, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER3_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER3))
    endwith
   endif
  endfunc

  func oEFANNER3_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER3) and (val(.w_EFANNER3)>=1996  and val(.w_EFANNER3)<=2050  or .w_EFANNER3=='0000'))
    endwith
    return bRes
  endfunc

  add object oEFIMDER3_1_19 as StdField with uid="EVIACYTGOE",rtseq=19,rtrep=.f.,;
    cFormVar = "w_EFIMDER3", cQueryName = "EFIMDER3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266951,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=319, Top=140, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER3_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER3))
    endwith
   endif
  endfunc

  add object oEFIMCER3_1_20 as StdField with uid="KYWEOQHJWB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_EFIMCER3", cQueryName = "EFIMCER3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315527,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=479, Top=140, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER3_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER3))
    endwith
   endif
  endfunc

  add object oEFTRIER4_1_21 as StdField with uid="FHTKXGXMTV",rtseq=21,rtrep=.f.,;
    cFormVar = "w_EFTRIER4", cQueryName = "EFTRIER4",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'erario'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651334,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=10, Top=168, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER4"

  func oEFTRIER4_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER4_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER4_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER4_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER4_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER4
     i_obj.ecpSave()
  endproc

  add object oEFRATER4_1_22 as StdField with uid="MHDCGUZYUG",rtseq=22,rtrep=.f.,;
    cFormVar = "w_EFRATER4", cQueryName = "EFRATER4",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/Regione/Provincia",;
    HelpContextID = 151239302,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=94, Top=168, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER4_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER4))
    endwith
   endif
  endfunc

  add object oEFMESER4_1_23 as StdField with uid="DCVZMAGEBM",rtseq=23,rtrep=.f.,;
    cFormVar = "w_EFMESER4", cQueryName = "EFMESER4",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o dato obbligatorio",;
    ToolTipText = "Mese di riferimento (mm)",;
    HelpContextID = 152046214,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=181, Top=168, InputMask=replicate('X',2)

  func oEFMESER4_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER4))
    endwith
   endif
  endfunc

  func oEFMESER4_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EFMESER4>='01' and .w_EFMESER4<='12') or (empty(.w_EFMESER4) and .w_CHKOBBME4<>'S'))
    endwith
    return bRes
  endfunc

  add object oEFANNER4_1_24 as StdField with uid="LLSDYYHTVX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_EFANNER4", cQueryName = "EFANNER4",nZero=4,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748422,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=248, Top=168, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER4_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER4))
    endwith
   endif
  endfunc

  func oEFANNER4_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER4) and (val(.w_EFANNER4)>=1996  and val(.w_EFANNER4)<=2050  or .w_EFANNER4=='0000'))
    endwith
    return bRes
  endfunc

  add object oEFIMDER4_1_25 as StdField with uid="UYXLSVSCJI",rtseq=25,rtrep=.f.,;
    cFormVar = "w_EFIMDER4", cQueryName = "EFIMDER4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266950,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=319, Top=168, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER4_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER4))
    endwith
   endif
  endfunc

  add object oEFIMCER4_1_26 as StdField with uid="VGSQFEWENW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_EFIMCER4", cQueryName = "EFIMCER4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315526,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=479, Top=168, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER4_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER4))
    endwith
   endif
  endfunc

  add object oEFTRIER5_1_29 as StdField with uid="YGEOOYBGAA",rtseq=27,rtrep=.f.,;
    cFormVar = "w_EFTRIER5", cQueryName = "EFTRIER5",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'erario'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651333,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=10, Top=196, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER5"

  func oEFTRIER5_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER5_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER5_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER5_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER5_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER5
     i_obj.ecpSave()
  endproc

  add object oEFRATER5_1_30 as StdField with uid="XPFNCWVBBM",rtseq=28,rtrep=.f.,;
    cFormVar = "w_EFRATER5", cQueryName = "EFRATER5",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/Regione/Provincia",;
    HelpContextID = 151239301,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=94, Top=196, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER5_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER5))
    endwith
   endif
  endfunc

  add object oEFMESER5_1_31 as StdField with uid="SXBILEGRST",rtseq=29,rtrep=.f.,;
    cFormVar = "w_EFMESER5", cQueryName = "EFMESER5",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o dato obbligatorio",;
    ToolTipText = "Mese di riferimento (mm)",;
    HelpContextID = 152046213,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=181, Top=196, InputMask=replicate('X',2)

  func oEFMESER5_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER5))
    endwith
   endif
  endfunc

  func oEFMESER5_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EFMESER5>='01' and .w_EFMESER5<='12') or (empty(.w_EFMESER5) and .w_CHKOBBME5<>'S'))
    endwith
    return bRes
  endfunc

  add object oEFANNER5_1_32 as StdField with uid="VYFYZQIRTB",rtseq=30,rtrep=.f.,;
    cFormVar = "w_EFANNER5", cQueryName = "EFANNER5",nZero=4,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748421,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=248, Top=196, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER5_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER5))
    endwith
   endif
  endfunc

  func oEFANNER5_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER5) and (val(.w_EFANNER5)>=1996  and val(.w_EFANNER5)<=2050  or .w_EFANNER5=='0000'))
    endwith
    return bRes
  endfunc

  add object oEFIMDER5_1_33 as StdField with uid="VBSWBTOJXK",rtseq=31,rtrep=.f.,;
    cFormVar = "w_EFIMDER5", cQueryName = "EFIMDER5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266949,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=319, Top=196, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER5_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER5))
    endwith
   endif
  endfunc

  add object oEFIMCER5_1_34 as StdField with uid="ZGLAFDKFUK",rtseq=32,rtrep=.f.,;
    cFormVar = "w_EFIMCER5", cQueryName = "EFIMCER5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315525,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=479, Top=196, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER5_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER5))
    endwith
   endif
  endfunc

  add object oEFTRIER6_1_35 as StdField with uid="ZKHBXXIESY",rtseq=33,rtrep=.f.,;
    cFormVar = "w_EFTRIER6", cQueryName = "EFTRIER6",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'erario'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651332,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=10, Top=224, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER6"

  func oEFTRIER6_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER6_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER6_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER6_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER6_1_35.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER6
     i_obj.ecpSave()
  endproc

  add object oEFRATER6_1_36 as StdField with uid="SVKBMSIANH",rtseq=34,rtrep=.f.,;
    cFormVar = "w_EFRATER6", cQueryName = "EFRATER6",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/Regione/Provincia",;
    HelpContextID = 151239300,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=94, Top=224, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER6_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER6))
    endwith
   endif
  endfunc

  add object oEFMESER6_1_37 as StdField with uid="NGTZEJHVVD",rtseq=35,rtrep=.f.,;
    cFormVar = "w_EFMESER6", cQueryName = "EFMESER6",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o dato obbligatorio",;
    ToolTipText = "Mese di riferimento (mm)",;
    HelpContextID = 152046212,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=181, Top=224, InputMask=replicate('X',2)

  func oEFMESER6_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER6))
    endwith
   endif
  endfunc

  func oEFMESER6_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EFMESER6>='01' and .w_EFMESER6<='12') or (empty(.w_EFMESER6) and .w_CHKOBBME6<>'S'))
    endwith
    return bRes
  endfunc

  add object oEFANNER6_1_38 as StdField with uid="DGWOOFFUSO",rtseq=36,rtrep=.f.,;
    cFormVar = "w_EFANNER6", cQueryName = "EFANNER6",nZero=4,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748420,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=248, Top=224, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER6_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER6))
    endwith
   endif
  endfunc

  func oEFANNER6_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER6) and (val(.w_EFANNER6)>=1996  and val(.w_EFANNER6)<=2050  or .w_EFANNER6=='0000'))
    endwith
    return bRes
  endfunc

  add object oEFIMDER6_1_39 as StdField with uid="OBZSCVYYPC",rtseq=37,rtrep=.f.,;
    cFormVar = "w_EFIMDER6", cQueryName = "EFIMDER6",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266948,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=319, Top=224, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER6_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER6))
    endwith
   endif
  endfunc

  add object oEFIMCER6_1_40 as StdField with uid="VIFTXJXKHF",rtseq=38,rtrep=.f.,;
    cFormVar = "w_EFIMCER6", cQueryName = "EFIMCER6",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315524,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=479, Top=224, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER6_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER6))
    endwith
   endif
  endfunc

  add object oEFTOTDER_1_41 as StdField with uid="INJBGREDMF",rtseq=39,rtrep=.f.,;
    cFormVar = "w_EFTOTDER", cQueryName = "EFTOTDER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 101344664,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=319, Top=258, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oEFTOTCER_1_42 as StdField with uid="YQLREAFJDS",rtseq=40,rtrep=.f.,;
    cFormVar = "w_EFTOTCER", cQueryName = "EFTOTCER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 84567448,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=479, Top=259, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oEFSALDER_1_43 as StdField with uid="GUHTOPIQNX",rtseq=41,rtrep=.f.,;
    cFormVar = "w_EFSALDER", cQueryName = "EFSALDER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 92034456,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=616, Top=260, cSayPict="v_PV(78)", cGetPict="v_GV(78)"


  add object oObj_1_57 as cp_runprogram with uid="XGHZOPJGEG",left=257, top=298, width=205,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BBF('ER')",;
    cEvent = "ControllaDati",;
    nPag=1;
    , HelpContextID = 118612454


  add object oObj_1_58 as cp_calclbl with uid="GKGLWDZFVF",left=241, top=45, width=64,height=35,;
    caption='Anno di riferimento',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 209753973


  add object oObj_1_59 as cp_calclbl with uid="PKUHVTUSLK",left=174, top=45, width=64,height=35,;
    caption='Mese di riferimento',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 210390965


  add object oObj_1_60 as cp_calclbl with uid="ZUOHGMEAFA",left=10, top=45, width=52,height=35,;
    caption='Cod. tributo',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 52513199


  add object oObj_1_61 as cp_calclbl with uid="IVXJVGHHFJ",left=74, top=45, width=96,height=35,;
    caption='Rateazione/Regione/Prov.',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 17512930

  add object oStr_1_27 as StdString with uid="SDTAHTJKXO",Visible=.t., Left=316, Top=61,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="QOYRTPTDQN",Visible=.t., Left=479, Top=61,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="URWTKLQWUH",Visible=.t., Left=219, Top=261,;
    Alignment=1, Width=94, Height=15,;
    Caption="Totale A"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="KKRETUTZTW",Visible=.t., Left=616, Top=239,;
    Alignment=2, Width=132, Height=15,;
    Caption="Saldo (A-B)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="MIBHACUXWQ",Visible=.t., Left=460, Top=261,;
    Alignment=1, Width=15, Height=15,;
    Caption="B"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_48 as StdString with uid="QECVEKZDVA",Visible=.t., Left=5, Top=12,;
    Alignment=0, Width=128, Height=15,;
    Caption="Sezione erario"  ;
  , bGlobalFont=.t.

  add object oBox_1_47 as StdBox with uid="SKPDLHLIGC",left=3, top=29, width=729,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_afr','MODEPAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".EFSERIAL=MODEPAG.EFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
