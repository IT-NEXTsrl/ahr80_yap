* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_mvt                                                        *
*              Versamenti trimestrali                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_82]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-05                                                      *
* Last revis.: 2014-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_mvt"))

* --- Class definition
define class tgsve_mvt as StdTrsForm
  Top    = -1
  Left   = 23

  * --- Standard Properties
  Width  = 762
  Height = 434+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-30"
  HelpContextID=46805609
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=54

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  VTR_MAST_IDX = 0
  VTR_DETT_IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  AGENTI_IDX = 0
  PAR_PROV_IDX = 0
  cFile = "VTR_MAST"
  cFileDetail = "VTR_DETT"
  cKeySelect = "VTSERIAL"
  cKeyWhere  = "VTSERIAL=this.w_VTSERIAL"
  cKeyDetail  = "VTSERIAL=this.w_VTSERIAL"
  cKeyWhereODBC = '"VTSERIAL="+cp_ToStrODBC(this.w_VTSERIAL)';

  cKeyDetailWhereODBC = '"VTSERIAL="+cp_ToStrODBC(this.w_VTSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"VTR_DETT.VTSERIAL="+cp_ToStrODBC(this.w_VTSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'VTR_DETT.CPROWNUM '
  cPrg = "gsve_mvt"
  cComment = "Versamenti trimestrali"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_VTSERIAL = space(10)
  w_CODAZE = space(5)
  w_PERAZI = 0
  w_PERAGE = 0
  w_PERASS = 0
  w_SOKAGE = 0
  w_VTNUMREG = 0
  w_VT__ANNO = space(4)
  w_VTDATREG = ctod('  /  /  ')
  o_VTDATREG = ctod('  /  /  ')
  w_VTFLCONT = space(1)
  w_VTPERPRE = 0
  w_VTPERPR2 = 0
  w_VTPERASS = 0
  o_VTPERASS = 0
  w_VTDATINI = ctod('  /  /  ')
  w_VTDATFIN = ctod('  /  /  ')
  o_VTDATFIN = ctod('  /  /  ')
  w_VTCODVAL = space(3)
  o_VTCODVAL = space(3)
  w_OBTEST = ctod('  /  /  ')
  w_INIDATA = ctod('  /  /  ')
  w_FINDATA = ctod('  /  /  ')
  w_TOTPER = 0
  o_TOTPER = 0
  w_ANNO = 0
  w_VALUTA = space(3)
  o_VALUTA = space(3)
  w_DECTOT = 0
  w_CAOVAL = 0
  w_CALCPICT = 0
  w_VTCODAGE = space(5)
  w_VTFLORMI = 0
  w_VTIMPLIQ = 0
  o_VTIMPLIQ = 0
  w_TIPOAGE = space(1)
  w_VTIMPENA = 0
  o_VTIMPENA = 0
  w_VTCONPRE = 0
  w_VTCONASS = 0
  w_DESAGE = space(35)
  w_VTDATVER = ctod('  /  /  ')
  w_VTNUMVER = 0
  w_VTALFVER = space(2)
  w_TOTPRE1 = 0
  w_TOTASS1 = 0
  w_TOTASG1 = 0
  w_TOTPRE = 0
  w_TOTASS = 0
  w_DTOBSO = ctod('  /  /  ')
  w_VALEURO = space(3)
  w_DECEUR = 0
  w_SIMVAL = space(5)
  w_ANPREV = space(4)
  w_TRIPRE = space(1)
  w_VTPERAGE = 0
  o_VTPERAGE = 0
  w_VTCONASG = 0
  w_TOTASG = 0
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_VTSERIAL = this.W_VTSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_VT__ANNO = this.W_VT__ANNO
  op_VTNUMREG = this.W_VTNUMREG
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VTR_MAST','gsve_mvt')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_mvtPag1","gsve_mvt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Versamento trimestrale")
      .Pages(1).HelpContextID = 226454169
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='AGENTI'
    this.cWorkTables[4]='PAR_PROV'
    this.cWorkTables[5]='VTR_MAST'
    this.cWorkTables[6]='VTR_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VTR_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VTR_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_VTSERIAL = NVL(VTSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from VTR_MAST where VTSERIAL=KeySet.VTSERIAL
    *
    i_nConn = i_TableProp[this.VTR_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VTR_MAST_IDX,2],this.bLoadRecFilter,this.VTR_MAST_IDX,"gsve_mvt")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VTR_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VTR_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"VTR_DETT.","VTR_MAST.")
      i_cTable = i_cTable+' VTR_MAST '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'VTSERIAL',this.w_VTSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CODAZE = i_CODAZI
        .w_PERAZI = 0
        .w_PERAGE = 0
        .w_PERASS = 0
        .w_SOKAGE = 0
        .w_INIDATA = DATTRIM(.w_VTDATREG,'I')
        .w_FINDATA = DATTRIM(.w_VTDATREG,'F')
        .w_DECTOT = 0
        .w_TOTPRE1 = 0
        .w_TOTASS1 = 0
        .w_TOTASG1 = 0
        .w_DTOBSO = ctod("  /  /  ")
        .w_VALEURO = g_codeur
        .w_DECEUR = 0
        .w_SIMVAL = space(5)
        .w_ANPREV = space(4)
        .w_TRIPRE = space(1)
        .w_VTSERIAL = NVL(VTSERIAL,space(10))
        .op_VTSERIAL = .w_VTSERIAL
          .link_1_2('Load')
        .w_VTNUMREG = NVL(VTNUMREG,0)
        .op_VTNUMREG = .w_VTNUMREG
        .w_VT__ANNO = NVL(VT__ANNO,space(4))
        .op_VT__ANNO = .w_VT__ANNO
        .w_VTDATREG = NVL(cp_ToDate(VTDATREG),ctod("  /  /  "))
        .w_VTFLCONT = NVL(VTFLCONT,space(1))
        .w_VTPERPRE = NVL(VTPERPRE,0)
        .w_VTPERPR2 = NVL(VTPERPR2,0)
        .w_VTPERASS = NVL(VTPERASS,0)
        .w_VTDATINI = NVL(cp_ToDate(VTDATINI),ctod("  /  /  "))
        .w_VTDATFIN = NVL(cp_ToDate(VTDATFIN),ctod("  /  /  "))
        .w_VTCODVAL = NVL(VTCODVAL,space(3))
          if link_1_16_joined
            this.w_VTCODVAL = NVL(VACODVAL116,NVL(this.w_VTCODVAL,space(3)))
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO116),ctod("  /  /  "))
            this.w_DECTOT = NVL(VADECTOT116,0)
            this.w_SIMVAL = NVL(VASIMVAL116,space(5))
          else
          .link_1_16('Load')
          endif
        .w_OBTEST = .w_VTDATREG
        .w_TOTPER = (.w_VTPERPRE+.w_VTPERPR2)
        .w_ANNO = YEAR(.w_VTDATFIN)
        .w_VALUTA = iif(.w_VTCODVAL=g_perval,g_perval,g_codlir)
          .link_1_23('Load')
        .w_CAOVAL = getcam(.w_VALUTA, I_DATSYS)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_VTDATVER = NVL(cp_ToDate(VTDATVER),ctod("  /  /  "))
        .w_VTNUMVER = NVL(VTNUMVER,0)
        .w_VTALFVER = NVL(VTALFVER,space(2))
        .w_TOTPRE = cp_ROUND(.w_TOTPRE1, .w_DECTOT)
        .w_TOTASS = cp_ROUND(.w_TOTASS1,.w_DECTOT)
          .link_1_28('Load')
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_VTPERAGE = NVL(VTPERAGE,0)
        .w_TOTASG = cp_ROUND(.w_TOTASG1,.w_DECTOT)
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'VTR_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from VTR_DETT where VTSERIAL=KeySet.VTSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.VTR_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VTR_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('VTR_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "VTR_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" VTR_DETT"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'VTSERIAL',this.w_VTSERIAL  )
        select * from (i_cTable) VTR_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTPRE1 = 0
      this.w_TOTASS1 = 0
      this.w_TOTASG1 = 0
      scan
        with this
          .w_TIPOAGE = space(1)
          .w_DESAGE = space(35)
          .w_CPROWNUM = CPROWNUM
          .w_VTCODAGE = NVL(VTCODAGE,space(5))
          if link_2_1_joined
            this.w_VTCODAGE = NVL(AGCODAGE201,NVL(this.w_VTCODAGE,space(5)))
            this.w_DESAGE = NVL(AGDESAGE201,space(35))
            this.w_TIPOAGE = NVL(AGFLAZIE201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_VTFLORMI = NVL(VTFLORMI,0)
          .w_VTIMPLIQ = NVL(VTIMPLIQ,0)
          .w_VTIMPENA = NVL(VTIMPENA,0)
          .w_VTCONPRE = NVL(VTCONPRE,0)
          .w_VTCONASS = NVL(VTCONASS,0)
          .w_VTCONASG = NVL(VTCONASG,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTPRE1 = .w_TOTPRE1+.w_VTCONPRE
          .w_TOTASS1 = .w_TOTASS1+.w_VTCONASS
          .w_TOTASG1 = .w_TOTASG1+.w_VTCONASG
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_OBTEST = .w_VTDATREG
        .w_TOTPER = (.w_VTPERPRE+.w_VTPERPR2)
        .w_ANNO = YEAR(.w_VTDATFIN)
        .w_VALUTA = iif(.w_VTCODVAL=g_perval,g_perval,g_codlir)
        .w_CAOVAL = getcam(.w_VALUTA, I_DATSYS)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_TOTPRE = cp_ROUND(.w_TOTPRE1, .w_DECTOT)
        .w_TOTASS = cp_ROUND(.w_TOTASS1,.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_TOTASG = cp_ROUND(.w_TOTASG1,.w_DECTOT)
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_VTSERIAL=space(10)
      .w_CODAZE=space(5)
      .w_PERAZI=0
      .w_PERAGE=0
      .w_PERASS=0
      .w_SOKAGE=0
      .w_VTNUMREG=0
      .w_VT__ANNO=space(4)
      .w_VTDATREG=ctod("  /  /  ")
      .w_VTFLCONT=space(1)
      .w_VTPERPRE=0
      .w_VTPERPR2=0
      .w_VTPERASS=0
      .w_VTDATINI=ctod("  /  /  ")
      .w_VTDATFIN=ctod("  /  /  ")
      .w_VTCODVAL=space(3)
      .w_OBTEST=ctod("  /  /  ")
      .w_INIDATA=ctod("  /  /  ")
      .w_FINDATA=ctod("  /  /  ")
      .w_TOTPER=0
      .w_ANNO=0
      .w_VALUTA=space(3)
      .w_DECTOT=0
      .w_CAOVAL=0
      .w_CALCPICT=0
      .w_VTCODAGE=space(5)
      .w_VTFLORMI=0
      .w_VTIMPLIQ=0
      .w_TIPOAGE=space(1)
      .w_VTIMPENA=0
      .w_VTCONPRE=0
      .w_VTCONASS=0
      .w_DESAGE=space(35)
      .w_VTDATVER=ctod("  /  /  ")
      .w_VTNUMVER=0
      .w_VTALFVER=space(2)
      .w_TOTPRE1=0
      .w_TOTASS1=0
      .w_TOTASG1=0
      .w_TOTPRE=0
      .w_TOTASS=0
      .w_DTOBSO=ctod("  /  /  ")
      .w_VALEURO=space(3)
      .w_DECEUR=0
      .w_SIMVAL=space(5)
      .w_ANPREV=space(4)
      .w_TRIPRE=space(1)
      .w_VTPERAGE=0
      .w_VTCONASG=0
      .w_TOTASG=0
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODAZE = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZE))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,7,.f.)
        .w_VT__ANNO = STR(YEAR(i_DATSYS),4,0)
        .w_VTDATREG = i_datsys
        .w_VTFLCONT = 'E'
        .w_VTPERPRE = .w_PERAZI
        .w_VTPERPR2 = .w_PERAGE
        .w_VTPERASS = .w_PERASS
        .w_VTDATINI = .w_INIDATA
        .w_VTDATFIN = .w_FINDATA
        .w_VTCODVAL = iif(g_perval=g_codeur,g_codeur,g_codlir)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_VTCODVAL))
         .link_1_16('Full')
        endif
        .w_OBTEST = .w_VTDATREG
        .w_INIDATA = DATTRIM(.w_VTDATREG,'I')
        .w_FINDATA = DATTRIM(.w_VTDATREG,'F')
        .w_TOTPER = (.w_VTPERPRE+.w_VTPERPR2)
        .w_ANNO = YEAR(.w_VTDATFIN)
        .w_VALUTA = iif(.w_VTCODVAL=g_perval,g_perval,g_codlir)
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_VALUTA))
         .link_1_23('Full')
        endif
        .DoRTCalc(23,23,.f.)
        .w_CAOVAL = getcam(.w_VALUTA, I_DATSYS)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_VTCODAGE))
         .link_2_1('Full')
        endif
        .DoRTCalc(27,30,.f.)
        .w_VTCONPRE = IIF(.w_VTFLORMI<>0, .w_VTCONPRE, cp_ROUND(.w_VTIMPENA*.w_TOTPER/100,.w_DECTOT))
        .w_VTCONASS = IIF(.w_TIPOAGE='S', IIF(.w_VTFLORMI<>0, .w_VTCONASS, cp_ROUND(.w_VTIMPLIQ*.w_VTPERASS/100,.w_DECTOT)), 0)
        .DoRTCalc(33,39,.f.)
        .w_TOTPRE = cp_ROUND(.w_TOTPRE1, .w_DECTOT)
        .w_TOTASS = cp_ROUND(.w_TOTASS1,.w_DECTOT)
        .DoRTCalc(42,42,.f.)
        .w_VALEURO = g_codeur
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_VALEURO))
         .link_1_28('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(44,47,.f.)
        .w_VTPERAGE = .w_SOKAGE
        .w_VTCONASG = IIF(.w_TIPOAGE='S', IIF(.w_VTFLORMI<>0, .w_VTCONASG, cp_ROUND(.w_VTIMPLIQ*.w_VTPERAGE/100,.w_DECTOT)), 0)
        .w_TOTASG = cp_ROUND(.w_TOTASG1,.w_DECTOT)
      endif
    endwith
    cp_BlankRecExtFlds(this,'VTR_MAST')
    this.DoRTCalc(51,54,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oVTNUMREG_1_7.enabled = i_bVal
      .Page1.oPag.oVT__ANNO_1_8.enabled = i_bVal
      .Page1.oPag.oVTDATREG_1_9.enabled = i_bVal
      .Page1.oPag.oVTFLCONT_1_10.enabled = i_bVal
      .Page1.oPag.oVTDATINI_1_14.enabled = i_bVal
      .Page1.oPag.oVTDATFIN_1_15.enabled = i_bVal
      .Page1.oPag.oVTCODVAL_1_16.enabled = i_bVal
      .Page1.oPag.oDESAGE_2_8.enabled = i_bVal
      .Page1.oPag.oVTDATVER_3_1.enabled = i_bVal
      .Page1.oPag.oVTNUMVER_3_2.enabled = i_bVal
      .Page1.oPag.oVTALFVER_3_3.enabled = i_bVal
      .Page1.oPag.oBtn_1_17.enabled = i_bVal
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oVTNUMREG_1_7.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'VTR_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VTR_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VTR_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEVTR","i_codazi,w_VTSERIAL")
      cp_AskTableProg(this,i_nConn,"PRVTR","i_codazi,w_VT__ANNO,w_VTNUMREG")
      .op_codazi = .w_codazi
      .op_VTSERIAL = .w_VTSERIAL
      .op_codazi = .w_codazi
      .op_VT__ANNO = .w_VT__ANNO
      .op_VTNUMREG = .w_VTNUMREG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VTR_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTSERIAL,"VTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTNUMREG,"VTNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VT__ANNO,"VT__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTDATREG,"VTDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTFLCONT,"VTFLCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTPERPRE,"VTPERPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTPERPR2,"VTPERPR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTPERASS,"VTPERASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTDATINI,"VTDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTDATFIN,"VTDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTCODVAL,"VTCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTDATVER,"VTDATVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTNUMVER,"VTNUMVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTALFVER,"VTALFVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTPERAGE,"VTPERAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VTR_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VTR_MAST_IDX,2])
    i_lTable = "VTR_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VTR_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_VTCODAGE C(5);
      ,t_VTIMPLIQ N(18,4);
      ,t_VTIMPENA N(18,4);
      ,t_VTCONPRE N(18,4);
      ,t_VTCONASS N(18,4);
      ,t_DESAGE C(35);
      ,t_VTCONASG N(18,4);
      ,CPROWNUM N(10);
      ,t_VTFLORMI N(1);
      ,t_TIPOAGE C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsve_mvtbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVTCODAGE_2_1.controlsource=this.cTrsName+'.t_VTCODAGE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVTIMPLIQ_2_3.controlsource=this.cTrsName+'.t_VTIMPLIQ'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVTIMPENA_2_5.controlsource=this.cTrsName+'.t_VTIMPENA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONPRE_2_6.controlsource=this.cTrsName+'.t_VTCONPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONASS_2_7.controlsource=this.cTrsName+'.t_VTCONASS'
    this.oPgFRm.Page1.oPag.oDESAGE_2_8.controlsource=this.cTrsName+'.t_DESAGE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONASG_2_10.controlsource=this.cTrsName+'.t_VTCONASG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(77)
    this.AddVLine(207)
    this.AddVLine(343)
    this.AddVLine(473)
    this.AddVLine(603)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCODAGE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VTR_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VTR_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEVTR","i_codazi,w_VTSERIAL")
          cp_NextTableProg(this,i_nConn,"PRVTR","i_codazi,w_VT__ANNO,w_VTNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VTR_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VTR_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'VTR_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(VTSERIAL,VTNUMREG,VT__ANNO,VTDATREG,VTFLCONT"+;
                  ",VTPERPRE,VTPERPR2,VTPERASS,VTDATINI,VTDATFIN"+;
                  ",VTCODVAL,VTDATVER,VTNUMVER,VTALFVER,VTPERAGE"+;
                  ",UTCC,UTDC,UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_VTSERIAL)+;
                    ","+cp_ToStrODBC(this.w_VTNUMREG)+;
                    ","+cp_ToStrODBC(this.w_VT__ANNO)+;
                    ","+cp_ToStrODBC(this.w_VTDATREG)+;
                    ","+cp_ToStrODBC(this.w_VTFLCONT)+;
                    ","+cp_ToStrODBC(this.w_VTPERPRE)+;
                    ","+cp_ToStrODBC(this.w_VTPERPR2)+;
                    ","+cp_ToStrODBC(this.w_VTPERASS)+;
                    ","+cp_ToStrODBC(this.w_VTDATINI)+;
                    ","+cp_ToStrODBC(this.w_VTDATFIN)+;
                    ","+cp_ToStrODBCNull(this.w_VTCODVAL)+;
                    ","+cp_ToStrODBC(this.w_VTDATVER)+;
                    ","+cp_ToStrODBC(this.w_VTNUMVER)+;
                    ","+cp_ToStrODBC(this.w_VTALFVER)+;
                    ","+cp_ToStrODBC(this.w_VTPERAGE)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VTR_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'VTR_MAST')
        cp_CheckDeletedKey(i_cTable,0,'VTSERIAL',this.w_VTSERIAL)
        INSERT INTO (i_cTable);
              (VTSERIAL,VTNUMREG,VT__ANNO,VTDATREG,VTFLCONT,VTPERPRE,VTPERPR2,VTPERASS,VTDATINI,VTDATFIN,VTCODVAL,VTDATVER,VTNUMVER,VTALFVER,VTPERAGE,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_VTSERIAL;
                  ,this.w_VTNUMREG;
                  ,this.w_VT__ANNO;
                  ,this.w_VTDATREG;
                  ,this.w_VTFLCONT;
                  ,this.w_VTPERPRE;
                  ,this.w_VTPERPR2;
                  ,this.w_VTPERASS;
                  ,this.w_VTDATINI;
                  ,this.w_VTDATFIN;
                  ,this.w_VTCODVAL;
                  ,this.w_VTDATVER;
                  ,this.w_VTNUMVER;
                  ,this.w_VTALFVER;
                  ,this.w_VTPERAGE;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VTR_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VTR_DETT_IDX,2])
      *
      * insert into VTR_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(VTSERIAL,VTCODAGE,VTFLORMI,VTIMPLIQ,VTIMPENA"+;
                  ",VTCONPRE,VTCONASS,VTCONASG,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_VTSERIAL)+","+cp_ToStrODBCNull(this.w_VTCODAGE)+","+cp_ToStrODBC(this.w_VTFLORMI)+","+cp_ToStrODBC(this.w_VTIMPLIQ)+","+cp_ToStrODBC(this.w_VTIMPENA)+;
             ","+cp_ToStrODBC(this.w_VTCONPRE)+","+cp_ToStrODBC(this.w_VTCONASS)+","+cp_ToStrODBC(this.w_VTCONASG)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'VTSERIAL',this.w_VTSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_VTSERIAL,this.w_VTCODAGE,this.w_VTFLORMI,this.w_VTIMPLIQ,this.w_VTIMPENA"+;
                ",this.w_VTCONPRE,this.w_VTCONASS,this.w_VTCONASG,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.VTR_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VTR_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update VTR_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'VTR_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " VTNUMREG="+cp_ToStrODBC(this.w_VTNUMREG)+;
             ",VT__ANNO="+cp_ToStrODBC(this.w_VT__ANNO)+;
             ",VTDATREG="+cp_ToStrODBC(this.w_VTDATREG)+;
             ",VTFLCONT="+cp_ToStrODBC(this.w_VTFLCONT)+;
             ",VTPERPRE="+cp_ToStrODBC(this.w_VTPERPRE)+;
             ",VTPERPR2="+cp_ToStrODBC(this.w_VTPERPR2)+;
             ",VTPERASS="+cp_ToStrODBC(this.w_VTPERASS)+;
             ",VTDATINI="+cp_ToStrODBC(this.w_VTDATINI)+;
             ",VTDATFIN="+cp_ToStrODBC(this.w_VTDATFIN)+;
             ",VTCODVAL="+cp_ToStrODBCNull(this.w_VTCODVAL)+;
             ",VTDATVER="+cp_ToStrODBC(this.w_VTDATVER)+;
             ",VTNUMVER="+cp_ToStrODBC(this.w_VTNUMVER)+;
             ",VTALFVER="+cp_ToStrODBC(this.w_VTALFVER)+;
             ",VTPERAGE="+cp_ToStrODBC(this.w_VTPERAGE)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'VTR_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'VTSERIAL',this.w_VTSERIAL  )
          UPDATE (i_cTable) SET;
              VTNUMREG=this.w_VTNUMREG;
             ,VT__ANNO=this.w_VT__ANNO;
             ,VTDATREG=this.w_VTDATREG;
             ,VTFLCONT=this.w_VTFLCONT;
             ,VTPERPRE=this.w_VTPERPRE;
             ,VTPERPR2=this.w_VTPERPR2;
             ,VTPERASS=this.w_VTPERASS;
             ,VTDATINI=this.w_VTDATINI;
             ,VTDATFIN=this.w_VTDATFIN;
             ,VTCODVAL=this.w_VTCODVAL;
             ,VTDATVER=this.w_VTDATVER;
             ,VTNUMVER=this.w_VTNUMVER;
             ,VTALFVER=this.w_VTALFVER;
             ,VTPERAGE=this.w_VTPERAGE;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for ( NOT EMPTY(t_VTCODAGE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.VTR_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.VTR_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from VTR_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update VTR_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " VTCODAGE="+cp_ToStrODBCNull(this.w_VTCODAGE)+;
                     ",VTFLORMI="+cp_ToStrODBC(this.w_VTFLORMI)+;
                     ",VTIMPLIQ="+cp_ToStrODBC(this.w_VTIMPLIQ)+;
                     ",VTIMPENA="+cp_ToStrODBC(this.w_VTIMPENA)+;
                     ",VTCONPRE="+cp_ToStrODBC(this.w_VTCONPRE)+;
                     ",VTCONASS="+cp_ToStrODBC(this.w_VTCONASS)+;
                     ",VTCONASG="+cp_ToStrODBC(this.w_VTCONASG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      VTCODAGE=this.w_VTCODAGE;
                     ,VTFLORMI=this.w_VTFLORMI;
                     ,VTIMPLIQ=this.w_VTIMPLIQ;
                     ,VTIMPENA=this.w_VTIMPENA;
                     ,VTCONPRE=this.w_VTCONPRE;
                     ,VTCONASS=this.w_VTCONASS;
                     ,VTCONASG=this.w_VTCONASG;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for ( NOT EMPTY(t_VTCODAGE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.VTR_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.VTR_DETT_IDX,2])
        *
        * delete VTR_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.VTR_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.VTR_MAST_IDX,2])
        *
        * delete VTR_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for ( NOT EMPTY(t_VTCODAGE)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VTR_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VTR_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,16,.t.)
        if .o_VTDATREG<>.w_VTDATREG
          .w_OBTEST = .w_VTDATREG
        endif
        .DoRTCalc(18,19,.t.)
          .w_TOTPER = (.w_VTPERPRE+.w_VTPERPR2)
        if .o_VTDATFIN<>.w_VTDATFIN
          .w_ANNO = YEAR(.w_VTDATFIN)
        endif
        if .o_VTCODVAL<>.w_VTCODVAL
          .w_VALUTA = iif(.w_VTCODVAL=g_perval,g_perval,g_codlir)
          .link_1_23('Full')
        endif
        .DoRTCalc(23,23,.t.)
          .w_CAOVAL = getcam(.w_VALUTA, I_DATSYS)
        if .o_VALUTA<>.w_VALUTA
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(26,30,.t.)
        if .o_VTIMPENA<>.w_VTIMPENA.or. .o_TOTPER<>.w_TOTPER
          .w_TOTPRE1 = .w_TOTPRE1-.w_vtconpre
          .w_VTCONPRE = IIF(.w_VTFLORMI<>0, .w_VTCONPRE, cp_ROUND(.w_VTIMPENA*.w_TOTPER/100,.w_DECTOT))
          .w_TOTPRE1 = .w_TOTPRE1+.w_vtconpre
        endif
        if .o_VTIMPLIQ<>.w_VTIMPLIQ.or. .o_VTPERASS<>.w_VTPERASS
          .w_TOTASS1 = .w_TOTASS1-.w_vtconass
          .w_VTCONASS = IIF(.w_TIPOAGE='S', IIF(.w_VTFLORMI<>0, .w_VTCONASS, cp_ROUND(.w_VTIMPLIQ*.w_VTPERASS/100,.w_DECTOT)), 0)
          .w_TOTASS1 = .w_TOTASS1+.w_vtconass
        endif
        .DoRTCalc(33,39,.t.)
          .w_TOTPRE = cp_ROUND(.w_TOTPRE1, .w_DECTOT)
          .w_TOTASS = cp_ROUND(.w_TOTASS1,.w_DECTOT)
        .DoRTCalc(42,42,.t.)
        if .o_VTCODVAL<>.w_VTCODVAL
          .link_1_28('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(44,48,.t.)
        if .o_VTIMPLIQ<>.w_VTIMPLIQ.or. .o_VTPERAGE<>.w_VTPERAGE
          .w_TOTASG1 = .w_TOTASG1-.w_vtconasg
          .w_VTCONASG = IIF(.w_TIPOAGE='S', IIF(.w_VTFLORMI<>0, .w_VTCONASG, cp_ROUND(.w_VTIMPLIQ*.w_VTPERAGE/100,.w_DECTOT)), 0)
          .w_TOTASG1 = .w_TOTASG1+.w_vtconasg
        endif
          .w_TOTASG = cp_ROUND(.w_TOTASG1,.w_DECTOT)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEVTR","i_codazi,w_VTSERIAL")
          .op_VTSERIAL = .w_VTSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_VT__ANNO<>.w_VT__ANNO
           cp_AskTableProg(this,i_nConn,"PRVTR","i_codazi,w_VT__ANNO,w_VTNUMREG")
          .op_VTNUMREG = .w_VTNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_VT__ANNO = .w_VT__ANNO
      endwith
      this.DoRTCalc(51,54,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_VTFLORMI with this.w_VTFLORMI
      replace t_TIPOAGE with this.w_TIPOAGE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oVTNUMVER_3_2.enabled = this.oPgFrm.Page1.oPag.oVTNUMVER_3_2.mCond()
    this.oPgFrm.Page1.oPag.oVTALFVER_3_3.enabled = this.oPgFrm.Page1.oPag.oVTALFVER_3_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVTIMPLIQ_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVTIMPLIQ_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVTIMPENA_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVTIMPENA_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROV_IDX,3]
    i_lTable = "PAR_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2], .t., this.PAR_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODAZI,PPPREAZI,PPPREAGE,PPPERASS,PPSOKAGE";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODAZI="+cp_ToStrODBC(this.w_CODAZE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODAZI',this.w_CODAZE)
            select PPCODAZI,PPPREAZI,PPPREAGE,PPPERASS,PPSOKAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZE = NVL(_Link_.PPCODAZI,space(5))
      this.w_PERAZI = NVL(_Link_.PPPREAZI,0)
      this.w_PERAGE = NVL(_Link_.PPPREAGE,0)
      this.w_PERASS = NVL(_Link_.PPPERASS,0)
      this.w_SOKAGE = NVL(_Link_.PPSOKAGE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZE = space(5)
      endif
      this.w_PERAZI = 0
      this.w_PERAGE = 0
      this.w_PERASS = 0
      this.w_SOKAGE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])+'\'+cp_ToStr(_Link_.PPCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VTCODVAL
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VTCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VTCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VTCODVAL))
          select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VTCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VTCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVTCODVAL_1_16'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VTCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VTCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VTCODVAL)
            select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VTCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VTCODVAL = space(3)
      endif
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endif
        this.w_VTCODVAL = space(3)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_DECTOT = 0
        this.w_SIMVAL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VTCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.VACODVAL as VACODVAL116"+ ",link_1_16.VADTOBSO as VADTOBSO116"+ ",link_1_16.VADECTOT as VADECTOT116"+ ",link_1_16.VASIMVAL as VASIMVAL116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on VTR_MAST.VTCODVAL=link_1_16.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and VTR_MAST.VTCODVAL=link_1_16.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VTCODAGE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VTCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_VTCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGFLAZIE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_VTCODAGE))
          select AGCODAGE,AGDESAGE,AGFLAZIE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VTCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_VTCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGFLAZIE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_VTCODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGFLAZIE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VTCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oVTCODAGE_2_1'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGFLAZIE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGFLAZIE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VTCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGFLAZIE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_VTCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_VTCODAGE)
            select AGCODAGE,AGDESAGE,AGFLAZIE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VTCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_TIPOAGE = NVL(_Link_.AGFLAZIE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VTCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_TIPOAGE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VTCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.AGCODAGE as AGCODAGE201"+ ",link_2_1.AGDESAGE as AGDESAGE201"+ ",link_2_1.AGFLAZIE as AGFLAZIE201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on VTR_DETT.VTCODAGE=link_2_1.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and VTR_DETT.VTCODAGE=link_2_1.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALEURO
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALEURO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALEURO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALEURO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALEURO)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALEURO = NVL(_Link_.VACODVAL,space(3))
      this.w_DECEUR = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALEURO = space(3)
      endif
      this.w_DECEUR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALEURO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oVTNUMREG_1_7.value==this.w_VTNUMREG)
      this.oPgFrm.Page1.oPag.oVTNUMREG_1_7.value=this.w_VTNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oVT__ANNO_1_8.value==this.w_VT__ANNO)
      this.oPgFrm.Page1.oPag.oVT__ANNO_1_8.value=this.w_VT__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oVTDATREG_1_9.value==this.w_VTDATREG)
      this.oPgFrm.Page1.oPag.oVTDATREG_1_9.value=this.w_VTDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oVTFLCONT_1_10.RadioValue()==this.w_VTFLCONT)
      this.oPgFrm.Page1.oPag.oVTFLCONT_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVTPERPRE_1_11.value==this.w_VTPERPRE)
      this.oPgFrm.Page1.oPag.oVTPERPRE_1_11.value=this.w_VTPERPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oVTPERPR2_1_12.value==this.w_VTPERPR2)
      this.oPgFrm.Page1.oPag.oVTPERPR2_1_12.value=this.w_VTPERPR2
    endif
    if not(this.oPgFrm.Page1.oPag.oVTPERASS_1_13.value==this.w_VTPERASS)
      this.oPgFrm.Page1.oPag.oVTPERASS_1_13.value=this.w_VTPERASS
    endif
    if not(this.oPgFrm.Page1.oPag.oVTDATINI_1_14.value==this.w_VTDATINI)
      this.oPgFrm.Page1.oPag.oVTDATINI_1_14.value=this.w_VTDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oVTDATFIN_1_15.value==this.w_VTDATFIN)
      this.oPgFrm.Page1.oPag.oVTDATFIN_1_15.value=this.w_VTDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oVTCODVAL_1_16.RadioValue()==this.w_VTCODVAL)
      this.oPgFrm.Page1.oPag.oVTCODVAL_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_2_8.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_2_8.value=this.w_DESAGE
      replace t_DESAGE with this.oPgFrm.Page1.oPag.oDESAGE_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oVTDATVER_3_1.value==this.w_VTDATVER)
      this.oPgFrm.Page1.oPag.oVTDATVER_3_1.value=this.w_VTDATVER
    endif
    if not(this.oPgFrm.Page1.oPag.oVTNUMVER_3_2.value==this.w_VTNUMVER)
      this.oPgFrm.Page1.oPag.oVTNUMVER_3_2.value=this.w_VTNUMVER
    endif
    if not(this.oPgFrm.Page1.oPag.oVTALFVER_3_3.value==this.w_VTALFVER)
      this.oPgFrm.Page1.oPag.oVTALFVER_3_3.value=this.w_VTALFVER
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPRE_3_7.value==this.w_TOTPRE)
      this.oPgFrm.Page1.oPag.oTOTPRE_3_7.value=this.w_TOTPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTASS_3_8.value==this.w_TOTASS)
      this.oPgFrm.Page1.oPag.oTOTASS_3_8.value=this.w_TOTASS
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_42.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_42.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oVTPERAGE_1_46.value==this.w_VTPERAGE)
      this.oPgFrm.Page1.oPag.oVTPERAGE_1_46.value=this.w_VTPERAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTASG_3_12.value==this.w_TOTASG)
      this.oPgFrm.Page1.oPag.oTOTASG_3_12.value=this.w_TOTASG
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCODAGE_2_1.value==this.w_VTCODAGE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCODAGE_2_1.value=this.w_VTCODAGE
      replace t_VTCODAGE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCODAGE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTIMPLIQ_2_3.value==this.w_VTIMPLIQ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTIMPLIQ_2_3.value=this.w_VTIMPLIQ
      replace t_VTIMPLIQ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTIMPLIQ_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTIMPENA_2_5.value==this.w_VTIMPENA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTIMPENA_2_5.value=this.w_VTIMPENA
      replace t_VTIMPENA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTIMPENA_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONPRE_2_6.value==this.w_VTCONPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONPRE_2_6.value=this.w_VTCONPRE
      replace t_VTCONPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONPRE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONASS_2_7.value==this.w_VTCONASS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONASS_2_7.value=this.w_VTCONASS
      replace t_VTCONASS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONASS_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONASG_2_10.value==this.w_VTCONASG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONASG_2_10.value=this.w_VTCONASG
      replace t_VTCONASG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVTCONASG_2_10.value
    endif
    cp_SetControlsValueExtFlds(this,'VTR_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_ANPREV<substr(DTOC(.w_VTDATFIN),7,4) OR (.w_ANPREV=substr(DTOC(.w_VTDATFIN),7,4) AND (VAL(.w_TRIPRE)*3)<VAL(substr(DTOC(.w_VTDATFIN),4,2))))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Impossibile salvare: generata distinta su file in data successiva o uguale ai movimenti."))
          case   (empty(.w_VT__ANNO))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oVT__ANNO_1_8.SetFocus()
            i_bnoObbl = !empty(.w_VT__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VTDATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oVTDATREG_1_9.SetFocus()
            i_bnoObbl = !empty(.w_VTDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VTDATINI) or not(.w_VTDATINI<=.w_VTDATFIN or (empty(.w_VTDATFIN))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oVTDATINI_1_14.SetFocus()
            i_bnoObbl = !empty(.w_VTDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   (empty(.w_VTDATFIN) or not(.w_VTDATINI<=.w_VTDATFIN or (empty(.w_VTDATINI))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oVTDATFIN_1_15.SetFocus()
            i_bnoObbl = !empty(.w_VTDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   (empty(.w_VTCODVAL) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oVTCODVAL_1_16.SetFocus()
            i_bnoObbl = !empty(.w_VTCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if  NOT EMPTY(.w_VTCODAGE)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VTDATREG = this.w_VTDATREG
    this.o_VTPERASS = this.w_VTPERASS
    this.o_VTDATFIN = this.w_VTDATFIN
    this.o_VTCODVAL = this.w_VTCODVAL
    this.o_TOTPER = this.w_TOTPER
    this.o_VALUTA = this.w_VALUTA
    this.o_VTIMPLIQ = this.w_VTIMPLIQ
    this.o_VTIMPENA = this.w_VTIMPENA
    this.o_VTPERAGE = this.w_VTPERAGE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=( NOT EMPTY(t_VTCODAGE))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=.F.
    if !i_bRes
      cp_ErrorMsg("Impossibile Eliminare una Singola Riga","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_VTCODAGE=space(5)
      .w_VTFLORMI=0
      .w_VTIMPLIQ=0
      .w_TIPOAGE=space(1)
      .w_VTIMPENA=0
      .w_VTCONPRE=0
      .w_VTCONASS=0
      .w_DESAGE=space(35)
      .w_VTCONASG=0
      .DoRTCalc(1,26,.f.)
      if not(empty(.w_VTCODAGE))
        .link_2_1('Full')
      endif
      .DoRTCalc(27,30,.f.)
        .w_VTCONPRE = IIF(.w_VTFLORMI<>0, .w_VTCONPRE, cp_ROUND(.w_VTIMPENA*.w_TOTPER/100,.w_DECTOT))
        .w_VTCONASS = IIF(.w_TIPOAGE='S', IIF(.w_VTFLORMI<>0, .w_VTCONASS, cp_ROUND(.w_VTIMPLIQ*.w_VTPERASS/100,.w_DECTOT)), 0)
      .DoRTCalc(33,48,.f.)
        .w_VTCONASG = IIF(.w_TIPOAGE='S', IIF(.w_VTFLORMI<>0, .w_VTCONASG, cp_ROUND(.w_VTIMPLIQ*.w_VTPERAGE/100,.w_DECTOT)), 0)
    endwith
    this.DoRTCalc(50,54,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_VTCODAGE = t_VTCODAGE
    this.w_VTFLORMI = t_VTFLORMI
    this.w_VTIMPLIQ = t_VTIMPLIQ
    this.w_TIPOAGE = t_TIPOAGE
    this.w_VTIMPENA = t_VTIMPENA
    this.w_VTCONPRE = t_VTCONPRE
    this.w_VTCONASS = t_VTCONASS
    this.w_DESAGE = t_DESAGE
    this.w_VTCONASG = t_VTCONASG
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_VTCODAGE with this.w_VTCODAGE
    replace t_VTFLORMI with this.w_VTFLORMI
    replace t_VTIMPLIQ with this.w_VTIMPLIQ
    replace t_TIPOAGE with this.w_TIPOAGE
    replace t_VTIMPENA with this.w_VTIMPENA
    replace t_VTCONPRE with this.w_VTCONPRE
    replace t_VTCONASS with this.w_VTCONASS
    replace t_DESAGE with this.w_DESAGE
    replace t_VTCONASG with this.w_VTCONASG
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTPRE1 = .w_TOTPRE1-.w_vtconpre
        .w_TOTASS1 = .w_TOTASS1-.w_vtconass
        .w_TOTASG1 = .w_TOTASG1-.w_vtconasg
        .SetControlsValue()
      endwith
  EndProc
  func CanEdit()
    local i_res
    i_res=this.w_ANPREV<substr(DTOC(this.w_VTDATFIN),7,4) OR (this.w_ANPREV=substr(DTOC(this.w_VTDATFIN),7,4) AND (VAL(this.w_TRIPRE)*3)<VAL(substr(DTOC(this.w_VTDATFIN),4,2)))
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile modificare: generata distinta su file in data successiva o uguale ai movimenti."))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=this.w_ANPREV<substr(DTOC(this.w_VTDATFIN),7,4) OR (this.w_ANPREV=substr(DTOC(this.w_VTDATFIN),7,4) AND (VAL(this.w_TRIPRE)*3)<VAL(substr(DTOC(this.w_VTDATFIN),4,2)))
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile cancellare: generata distinta su file in data successiva o uguale ai movimenti."))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsve_mvtPag1 as StdContainer
  Width  = 758
  height = 434
  stdWidth  = 758
  stdheight = 434
  resizeYpos=227
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVTNUMREG_1_7 as StdField with uid="CUKNLRRIPQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_VTNUMREG", cQueryName = "VTNUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 73402269,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=162, Top=14, cSayPict='"999999"', cGetPict='"999999"'

  add object oVT__ANNO_1_8 as StdField with uid="QOKIHDQSVT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_VT__ANNO", cQueryName = "VT__ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di pagamento delle provvigioni",;
    HelpContextID = 5564507,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=254, Top=14, InputMask=replicate('X',4)

  add object oVTDATREG_1_9 as StdField with uid="NPNPJITRVT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_VTDATREG", cQueryName = "VTDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 79390621,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=389, Top=14


  add object oVTFLCONT_1_10 as StdCombo with uid="WAAYDVPHER",rtseq=10,rtrep=.f.,left=162,top=43,width=92,height=24;
    , ToolTipText = "Visualizza i versamenti previdenziali, assistenziali oppure entrambi";
    , HelpContextID = 256473174;
    , cFormVar="w_VTFLCONT",RowSource=""+"Previdenziali,"+"Assistenziali,"+"Prev./ass.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVTFLCONT_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VTFLCONT,&i_cF..t_VTFLCONT),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'A',;
    iif(xVal =3,'E',;
    space(1)))))
  endfunc
  func oVTFLCONT_1_10.GetRadio()
    this.Parent.oContained.w_VTFLCONT = this.RadioValue()
    return .t.
  endfunc

  func oVTFLCONT_1_10.ToRadio()
    this.Parent.oContained.w_VTFLCONT=trim(this.Parent.oContained.w_VTFLCONT)
    return(;
      iif(this.Parent.oContained.w_VTFLCONT=='P',1,;
      iif(this.Parent.oContained.w_VTFLCONT=='A',2,;
      iif(this.Parent.oContained.w_VTFLCONT=='E',3,;
      0))))
  endfunc

  func oVTFLCONT_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oVTPERPRE_1_11 as StdField with uid="MXLWURIGYY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_VTPERPRE", cQueryName = "VTPERPRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi previdenziali a carico azienda",;
    HelpContextID = 44050331,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=362, Top=43, cSayPict='"99.999"', cGetPict='"99.999"'

  add object oVTPERPR2_1_12 as StdField with uid="VASTYMQSAG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_VTPERPR2", cQueryName = "VTPERPR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi previdenziali a carico agente",;
    HelpContextID = 44050312,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=434, Top=43, cSayPict='"99.999"', cGetPict='"99.999"'

  add object oVTPERASS_1_13 as StdField with uid="CQEENKVOZS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_VTPERASS", cQueryName = "VTPERASS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi assistenziali a carico dell'azienda",;
    HelpContextID = 60827561,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=162, Top=73, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oVTDATINI_1_14 as StdField with uid="LORGNCRJMU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_VTDATINI", cQueryName = "VTDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data iniziale pagamento provvigioni",;
    HelpContextID = 71604321,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=161, Top=101

  func oVTDATINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VTDATINI<=.w_VTDATFIN or (empty(.w_VTDATFIN)))
    endwith
    return bRes
  endfunc

  add object oVTDATFIN_1_15 as StdField with uid="THEJKYLKHI",rtseq=15,rtrep=.f.,;
    cFormVar = "w_VTDATFIN", cQueryName = "VTDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data finale pagamento provvigioni",;
    HelpContextID = 121935964,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=270, Top=101

  func oVTDATFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VTDATINI<=.w_VTDATFIN or (empty(.w_VTDATINI)))
    endwith
    return bRes
  endfunc


  add object oVTCODVAL_1_16 as StdCombo with uid="TJYCSNEXDQ",rtseq=16,rtrep=.f.,left=404,top=101,width=99,height=22;
    , ToolTipText = "Codice valuta del versamento";
    , HelpContextID = 130635682;
    , cFormVar="w_VTCODVAL",RowSource=""+"Valuta di conto,"+"Valuta nazionale", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Valuta inesistente o incongruente o obsoleta";
    , cLinkFile="VALUTE";
  , bGlobalFont=.t.


  func oVTCODVAL_1_16.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VTCODVAL,&i_cF..t_VTCODVAL),this.value)
    return(iif(xVal =1,g_codeur,;
    iif(xVal =2,g_codlir,;
    space(3))))
  endfunc
  func oVTCODVAL_1_16.GetRadio()
    this.Parent.oContained.w_VTCODVAL = this.RadioValue()
    return .t.
  endfunc

  func oVTCODVAL_1_16.ToRadio()
    this.Parent.oContained.w_VTCODVAL=trim(this.Parent.oContained.w_VTCODVAL)
    return(;
      iif(this.Parent.oContained.w_VTCODVAL==g_codeur,1,;
      iif(this.Parent.oContained.w_VTCODVAL==g_codlir,2,;
      0)))
  endfunc

  func oVTCODVAL_1_16.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oVTCODVAL_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oVTCODVAL_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oBtn_1_17 as StdButton with uid="JWHOGQFDFO",left=704, top=76, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare gli agenti";
    , HelpContextID = 130116630;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        do GSVE_BVT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc


  add object oObj_1_30 as cp_runprogram with uid="SDASVTREBE",left=771, top=451, width=151,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BAL(0)',;
    cEvent = "Insert end",;
    nPag=1;
    , HelpContextID = 131192038


  add object oObj_1_31 as cp_runprogram with uid="ILVVJJUJIU",left=773, top=473, width=137,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BAL(1)',;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 131192038

  add object oSIMVAL_1_42 as StdField with uid="YSMXHBZDIO",rtseq=45,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 228650022,;
   bGlobalFont=.t.,;
    Height=19, Width=55, Left=513, Top=101, InputMask=replicate('X',5)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=17, top=150, width=734,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="VTCODAGE",Label1="Agente",Field2="VTIMPLIQ",Label2="Provv.liquidate",Field3="VTIMPENA",Label3="Impon.previdenziale",Field4="VTCONPRE",Label4="Contr. previdenziali",Field5="VTCONASS",Label5="A carico azienda", Field6="VTCONASG",Label6="A carico agente",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 849786

  add object oVTPERAGE_1_46 as StdField with uid="MBJKLHOGCY",rtseq=48,rtrep=.f.,;
    cFormVar = "w_VTPERAGE", cQueryName = "VTPERAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi assistenziali a carico dell'agente",;
    HelpContextID = 60827547,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=362, Top=73, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oStr_1_32 as StdString with uid="TBAOBBKOWP",Visible=.t., Left=32, Top=14,;
    Alignment=1, Width=127, Height=15,;
    Caption="Registrazione n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="DMMCGMUTIW",Visible=.t., Left=241, Top=14,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="EMKNTXLLLP",Visible=.t., Left=298, Top=14,;
    Alignment=1, Width=87, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="DPNUVNAIZR",Visible=.t., Left=422, Top=43,;
    Alignment=0, Width=13, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ABOYBMBWPD",Visible=.t., Left=254, Top=43,;
    Alignment=1, Width=105, Height=15,;
    Caption="% Previdenziali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="VJSQVRYGZY",Visible=.t., Left=64, Top=74,;
    Alignment=1, Width=95, Height=15,;
    Caption="% Assistenziali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="NBRRCFXMJH",Visible=.t., Left=32, Top=43,;
    Alignment=1, Width=127, Height=15,;
    Caption="Contributi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="AMDWOSOWEO",Visible=.t., Left=19, Top=101,;
    Alignment=1, Width=139, Height=15,;
    Caption="Provvigioni pagate dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="TGMEPPQWFJ",Visible=.t., Left=244, Top=101,;
    Alignment=1, Width=23, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="MZNOUHZHAE",Visible=.t., Left=353, Top=101,;
    Alignment=1, Width=49, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="VMIRVQYHRF",Visible=.t., Left=249, Top=74,;
    Alignment=1, Width=110, Height=15,;
    Caption="A carico dell'agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="ZGJVVNDBSH",Visible=.t., Left=544, Top=131,;
    Alignment=0, Width=125, Height=18,;
    Caption="Contributi assistenziali"  ;
  , bGlobalFont=.t.

  add object oBox_1_49 as StdBox with uid="SVLYRHJXAX",left=473, top=128, width=285,height=24

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=7,top=170,;
    width=733+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=8,top=171,width=732+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='AGENTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESAGE_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='AGENTI'
        oDropInto=this.oBodyCol.oRow.oVTCODAGE_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESAGE_2_8 as StdTrsField with uid="YRVLIFFQIT",rtseq=33,rtrep=.t.,;
    cFormVar="w_DESAGE",value=space(35),;
    HelpContextID = 116148022,;
    cTotal="", bFixedPos=.t., cQueryName = "DESAGE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=62, Top=376, InputMask=replicate('X',35)

  add object oStr_2_9 as StdString with uid="GYPOLVVHOI",Visible=.t., Left=20, Top=376,;
    Alignment=0, Width=38, Height=15,;
    Caption="Agente"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oVTDATVER_3_1 as StdField with uid="XXERCQKOSP",rtseq=34,rtrep=.f.,;
    cFormVar="w_VTDATVER",value=ctod("  /  /  "),;
    ToolTipText = "Data versamento trimestrale",;
    HelpContextID = 146499496,;
    cQueryName = "VTDATVER",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=149, Top=408

  add object oVTNUMVER_3_2 as StdField with uid="WIHDPQZALU",rtseq=35,rtrep=.f.,;
    cFormVar="w_VTNUMVER",value=0,;
    ToolTipText = "Numero versamento trimestrale",;
    HelpContextID = 140511144,;
    cQueryName = "VTNUMVER",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=337, Top=408

  func oVTNUMVER_3_2.mCond()
    with this.Parent.oContained
      return (not empty(.w_VTDATVER))
    endwith
  endfunc

  add object oVTALFVER_3_3 as StdField with uid="JPIDTKWDQH",rtseq=36,rtrep=.f.,;
    cFormVar="w_VTALFVER",value=space(2),;
    ToolTipText = "Alfa versamento",;
    HelpContextID = 132528040,;
    cQueryName = "VTALFVER",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=406, Top=408, InputMask=replicate('X',2)

  func oVTALFVER_3_3.mCond()
    with this.Parent.oContained
      return (not empty(.w_VTDATVER))
    endwith
  endfunc

  add object oTOTPRE_3_7 as StdField with uid="HWDSUSOETP",rtseq=40,rtrep=.f.,;
    cFormVar="w_TOTPRE",value=0,enabled=.f.,;
    HelpContextID = 128672310,;
    cQueryName = "TOTPRE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=336, Top=376, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTOTASS_3_8 as StdField with uid="HMHKLXPLOS",rtseq=41,rtrep=.f.,;
    cFormVar="w_TOTASS",value=0,enabled=.f.,;
    HelpContextID = 95183414,;
    cQueryName = "TOTASS",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=469, Top=376, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTOTASG_3_12 as StdField with uid="EDLLMHNZWZ",rtseq=50,rtrep=.f.,;
    cFormVar="w_TOTASG",value=0,enabled=.f.,;
    HelpContextID = 162292278,;
    cQueryName = "TOTASG",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=602, Top=375, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oStr_3_9 as StdString with uid="FQIGEUPELD",Visible=.t., Left=49, Top=408,;
    Alignment=0, Width=96, Height=15,;
    Caption="Data versamento"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="KRZIFRDQVX",Visible=.t., Left=234, Top=408,;
    Alignment=0, Width=97, Height=15,;
    Caption="Num.versamento"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="IPNLYTSDLX",Visible=.t., Left=396, Top=408,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsve_mvtBodyRow as CPBodyRowCnt
  Width=723
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oVTCODAGE_2_1 as StdTrsField with uid="YEQGVCMEQR",rtseq=26,rtrep=.t.,;
    cFormVar="w_VTCODAGE",value=space(5),;
    ToolTipText = "Codice agente",;
    HelpContextID = 46749595,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Agente inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=59, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_VTCODAGE"

  func oVTCODAGE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oVTCODAGE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oVTCODAGE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oVTCODAGE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oVTCODAGE_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_VTCODAGE
    i_obj.ecpSave()
  endproc

  add object oVTIMPLIQ_2_3 as StdTrsField with uid="VKFSIEBQQA",rtseq=28,rtrep=.t.,;
    cFormVar="w_VTIMPLIQ",value=0,;
    ToolTipText = "Importo provvigioni liquidate",;
    HelpContextID = 24660057,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=120, Left=60, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oVTIMPLIQ_2_3.mCond()
    with this.Parent.oContained
      return (.w_VTFLORMI=0)
    endwith
  endfunc

  add object oVTIMPENA_2_5 as StdTrsField with uid="RMLQKCYOTW",rtseq=30,rtrep=.t.,;
    cFormVar="w_VTIMPENA",value=0,;
    ToolTipText = "Imponibile previdenziale",;
    HelpContextID = 142100585,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=120, Left=196, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oVTIMPENA_2_5.mCond()
    with this.Parent.oContained
      return (.w_VTFLORMI=0 AND .w_TIPOAGE='I')
    endwith
  endfunc

  add object oVTCONPRE_2_6 as StdTrsField with uid="EHAIHDXGIF",rtseq=31,rtrep=.t.,;
    cFormVar="w_VTCONPRE",value=0,enabled=.f.,;
    ToolTipText = "Contributi previdenziali",;
    HelpContextID = 40458139,;
    cTotal = "this.Parent.oContained.w_totpre1", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=120, Left=332, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oVTCONASS_2_7 as StdTrsField with uid="TTRBZGOYKF",rtseq=32,rtrep=.t.,;
    cFormVar="w_VTCONASS",value=0,enabled=.f.,;
    ToolTipText = "Contributi assistenziali",;
    HelpContextID = 57235369,;
    cTotal = "this.Parent.oContained.w_totass1", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=457, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oVTCONASG_2_10 as StdTrsField with uid="LDMOITZEIK",rtseq=49,rtrep=.t.,;
    cFormVar="w_VTCONASG",value=0,enabled=.f.,;
    HelpContextID = 57235357,;
    cTotal = "this.Parent.oContained.w_totasg1", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=586, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oVTCODAGE_2_1.When()
    return(.t.)
  proc oVTCODAGE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oVTCODAGE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_mvt','VTR_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".VTSERIAL=VTR_MAST.VTSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
