* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_aca                                                        *
*              Calendario di ripartizione                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_6]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-11                                                      *
* Last revis.: 2013-03-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsca_aca"))

* --- Class definition
define class tgsca_aca as StdForm
  Top    = 41
  Left   = 36

  * --- Standard Properties
  Width  = 461
  Height = 75+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-03-29"
  HelpContextID=158694039
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  CAL_RIPA_IDX = 0
  cFile = "CAL_RIPA"
  cKeySelect = "CAPERIOD"
  cKeyWhere  = "CAPERIOD=this.w_CAPERIOD"
  cKeyWhereODBC = '"CAPERIOD="+cp_ToStrODBC(this.w_CAPERIOD)';

  cKeyWhereODBCqualified = '"CAL_RIPA.CAPERIOD="+cp_ToStrODBC(this.w_CAPERIOD)';

  cPrg = "gsca_aca"
  cComment = "Calendario di ripartizione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CAPERIOD = space(5)
  w_CADESCRI = space(35)
  w_CADATINI = ctod('  /  /  ')
  w_CADATFIN = ctod('  /  /  ')
  w_NUMGIO = 0
  w_CAOLDINI = ctod('  /  /  ')
  w_CAOLDFIN = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAL_RIPA','gsca_aca')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_acaPag1","gsca_aca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Calendario")
      .Pages(1).HelpContextID = 188606936
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCAPERIOD_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAL_RIPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAL_RIPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAL_RIPA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CAPERIOD = NVL(CAPERIOD,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAL_RIPA where CAPERIOD=KeySet.CAPERIOD
    *
    i_nConn = i_TableProp[this.CAL_RIPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAL_RIPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAL_RIPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAL_RIPA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CAPERIOD',this.w_CAPERIOD  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CAOLDINI = ctod("  /  /  ")
        .w_CAOLDFIN = ctod("  /  /  ")
        .w_CAPERIOD = NVL(CAPERIOD,space(5))
        .w_CADESCRI = NVL(CADESCRI,space(35))
        .w_CADATINI = NVL(cp_ToDate(CADATINI),ctod("  /  /  "))
        .w_CADATFIN = NVL(cp_ToDate(CADATFIN),ctod("  /  /  "))
        .w_NUMGIO = (.w_CADATFIN+1)-.w_CADATINI
        cp_LoadRecExtFlds(this,'CAL_RIPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CAPERIOD = space(5)
      .w_CADESCRI = space(35)
      .w_CADATINI = ctod("  /  /  ")
      .w_CADATFIN = ctod("  /  /  ")
      .w_NUMGIO = 0
      .w_CAOLDINI = ctod("  /  /  ")
      .w_CAOLDFIN = ctod("  /  /  ")
      if .cFunction<>"Filter"
          .DoRTCalc(1,4,.f.)
        .w_NUMGIO = (.w_CADATFIN+1)-.w_CADATINI
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAL_RIPA')
    this.DoRTCalc(6,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCAPERIOD_1_1.enabled = i_bVal
      .Page1.oPag.oCADESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oCADATINI_1_3.enabled = i_bVal
      .Page1.oPag.oCADATFIN_1_4.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCAPERIOD_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCAPERIOD_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAL_RIPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAL_RIPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAPERIOD,"CAPERIOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESCRI,"CADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADATINI,"CADATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADATFIN,"CADATFIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAL_RIPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])
    i_lTable = "CAL_RIPA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAL_RIPA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCA_SCA with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAL_RIPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAL_RIPA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAL_RIPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAL_RIPA')
        i_extval=cp_InsertValODBCExtFlds(this,'CAL_RIPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CAPERIOD,CADESCRI,CADATINI,CADATFIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CAPERIOD)+;
                  ","+cp_ToStrODBC(this.w_CADESCRI)+;
                  ","+cp_ToStrODBC(this.w_CADATINI)+;
                  ","+cp_ToStrODBC(this.w_CADATFIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAL_RIPA')
        i_extval=cp_InsertValVFPExtFlds(this,'CAL_RIPA')
        cp_CheckDeletedKey(i_cTable,0,'CAPERIOD',this.w_CAPERIOD)
        INSERT INTO (i_cTable);
              (CAPERIOD,CADESCRI,CADATINI,CADATFIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CAPERIOD;
                  ,this.w_CADESCRI;
                  ,this.w_CADATINI;
                  ,this.w_CADATFIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAL_RIPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAL_RIPA_IDX,i_nConn)
      *
      * update CAL_RIPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAL_RIPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CADESCRI="+cp_ToStrODBC(this.w_CADESCRI)+;
             ",CADATINI="+cp_ToStrODBC(this.w_CADATINI)+;
             ",CADATFIN="+cp_ToStrODBC(this.w_CADATFIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAL_RIPA')
        i_cWhere = cp_PKFox(i_cTable  ,'CAPERIOD',this.w_CAPERIOD  )
        UPDATE (i_cTable) SET;
              CADESCRI=this.w_CADESCRI;
             ,CADATINI=this.w_CADATINI;
             ,CADATFIN=this.w_CADATFIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAL_RIPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAL_RIPA_IDX,i_nConn)
      *
      * delete CAL_RIPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CAPERIOD',this.w_CAPERIOD  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAL_RIPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
            .w_NUMGIO = (.w_CADATFIN+1)-.w_CADATINI
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_HZYMGYYUKO()
    with this
          * --- inizializza le vecchie dati per i controlli successivi
          .w_CAOLDINI = .w_CADATINI
          .w_CAOLDFIN = .w_CADATFIN
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Load")
          .Calculate_HZYMGYYUKO()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCAPERIOD_1_1.value==this.w_CAPERIOD)
      this.oPgFrm.Page1.oPag.oCAPERIOD_1_1.value=this.w_CAPERIOD
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESCRI_1_2.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oCADESCRI_1_2.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCADATINI_1_3.value==this.w_CADATINI)
      this.oPgFrm.Page1.oPag.oCADATINI_1_3.value=this.w_CADATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCADATFIN_1_4.value==this.w_CADATFIN)
      this.oPgFrm.Page1.oPag.oCADATFIN_1_4.value=this.w_CADATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMGIO_1_6.value==this.w_NUMGIO)
      this.oPgFrm.Page1.oPag.oNUMGIO_1_6.value=this.w_NUMGIO
    endif
    cp_SetControlsValueExtFlds(this,'CAL_RIPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CAPERIOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAPERIOD_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CAPERIOD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CADATFIN>=.w_CADATINI)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCADATFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsca_aca
      if i_bRes
         gsca_cca(this)
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsca_acaPag1 as StdContainer
  Width  = 457
  height = 75
  stdWidth  = 457
  stdheight = 75
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCAPERIOD_1_1 as StdField with uid="RPSNHJXCTL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CAPERIOD", cQueryName = "CAPERIOD",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Sigla del periodo di ripartizione",;
    HelpContextID = 136331158,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=89, Top=13, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5)

  add object oCADESCRI_1_2 as StdField with uid="RRCRHEQYHO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione del periodo",;
    HelpContextID = 235995025,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=160, Top=13, InputMask=replicate('X',35)

  add object oCADATINI_1_3 as StdField with uid="BGYJNNNEGM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CADATINI", cQueryName = "CADATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio periodo",;
    HelpContextID = 134545297,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=89, Top=44

  add object oCADATFIN_1_4 as StdField with uid="NEFLWGKERP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CADATFIN", cQueryName = "CADATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine periodo",;
    HelpContextID = 83558516,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=246, Top=44

  func oCADATFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CADATFIN>=.w_CADATINI)
    endwith
    return bRes
  endfunc

  add object oNUMGIO_1_6 as StdField with uid="MFWIKJFHZV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMGIO", cQueryName = "NUMGIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 44981034,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=397, Top=44

  add object oStr_1_5 as StdString with uid="OAGWZCTHPK",Visible=.t., Left=19, Top=13,;
    Alignment=1, Width=68, Height=15,;
    Caption="Periodo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="PTQXCXHVCZ",Visible=.t., Left=2, Top=44,;
    Alignment=1, Width=85, Height=15,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="OKMIKLVNPI",Visible=.t., Left=174, Top=44,;
    Alignment=1, Width=69, Height=15,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="NEYTHWPLUN",Visible=.t., Left=332, Top=44,;
    Alignment=1, Width=62, Height=15,;
    Caption="Giorni:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_aca','CAL_RIPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CAPERIOD=CAL_RIPA.CAPERIOD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
