* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_azi                                                        *
*              Dati azienda                                                    *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_309]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-07                                                      *
* Last revis.: 2018-03-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_azi")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_azi")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_azi")
  return

* --- Class definition
define class tgsar_azi as StdPCForm
  Width  = 723
  Height = 490+35
  Top    = 16
  Left   = 18
  cComment = "Dati azienda"
  cPrg = "gsar_azi"
  HelpContextID=7768215
  add object cnt as tcgsar_azi
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_azi as PCContext
  w_AZCODAZI = space(5)
  w_AZRAGAZI = space(80)
  w_AZPERAZI = space(1)
  w_COAZI = space(5)
  w_AZINDAZI = space(35)
  w_AZCODOLT = 0
  w_AZCODCAT = space(4)
  w_AZCAPAZI = space(8)
  w_AZLOCAZI = space(30)
  w_AZPROAZI = space(2)
  w_AZCODNAZ = space(3)
  w_DESNAZ = space(35)
  w_CODISO = space(3)
  w_AZCODLIN = space(3)
  w_DESLIN = space(30)
  w_AZCODINE = space(9)
  w_AZCOFAZI = space(20)
  w_AZPIVAZI = space(20)
  w_AZTELEFO = space(18)
  w_AZTELFAX = space(18)
  w_AZ_EMAIL = space(254)
  w_AZINDWEB = space(254)
  w_AZCAPSOC = space(30)
  w_AZNAGAZI = space(5)
  w_AZRECREA = space(20)
  w_AZTRIAZI = space(25)
  w_AZESAAZI = space(25)
  w_AZCCFISC = space(20)
  w_AZCONTRI = space(20)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = space(8)
  w_UTDV = space(8)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_AZVALEUR = space(3)
  w_DESEUR = space(35)
  w_AZVALLIR = space(3)
  w_DESLIR = space(35)
  w_AZUNIUTE = space(1)
  w_AZCFNUME = space(1)
  w_AZTRAEXP = space(1)
  w_AZTRAEXP = space(1)
  w_AZTIPLOC = space(3)
  w_AZDATTRA = space(8)
  w_AZQUADRA = space(1)
  w_AZFLUNIV = space(1)
  w_AZFLARDO = space(1)
  w_AZPERPAR = space(1)
  w_AZPERSTS = space(1)
  w_AZCCRICA = space(1)
  w_AZGESCOM = space(1)
  w_AZRIPCOM = space(1)
  w_AZFLRIPC = space(1)
  w_AZDETCON = space(1)
  w_AZFLVEBD = space(1)
  w_AZFLFIDO = space(1)
  w_AZGIORIS = 0
  w_AZFILPAR = space(1)
  w_AZMAXLIV = 0
  w_AZMASCON = space(15)
  w_AZMASCLI = space(15)
  w_AZMASFOR = space(15)
  w_AZMASMCO = space(15)
  w_AZMASCCC = space(15)
  w_AZMASMCC = space(15)
  w_AZINTMIN = 0
  w_AZSCAPNT = space(4)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_AZDATINI = space(8)
  w_AZDATSER = space(8)
  w_AZNPRGEL = 0
  w_AZPRESOB = space(5)
  w_AZTELSOB = space(18)
  w_AZCODPOR = space(10)
  w_AZDTOBBL = space(1)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_AZMAGAZI = space(5)
  w_DESMAG = space(30)
  w_AZSCAMAG = space(8)
  w_AZMAGUTE = space(1)
  w_AZPERAGE = space(1)
  w_AZPERART = space(1)
  w_AZGESCON = space(1)
  w_AZPERDIS = space(1)
  w_AZPERSDA = space(1)
  w_AZPERORN = space(1)
  w_AZGESLOT = space(1)
  w_AZGESUBI = space(1)
  w_AZGESMAT = space(1)
  w_AZFLCAMB = space(1)
  w_AZRATEVA = space(1)
  w_AZFLSCOM = space(1)
  w_AZDTSCOM = space(8)
  w_AZSCOPIE = space(1)
  w_AZDTSCPI = space(8)
  w_AZFLCESC = space(1)
  w_AZXCONDI = space(1)
  w_AZDATDOC = space(1)
  w_AZCALNET = 0
  w_AZPERPQT = 0
  w_AZPERPQD = 0
  w_AZNUMSCO = 0
  w_AZCODEXT = space(1)
  w_AZPREEAN = space(2)
  w_AZDATMAT = space(8)
  w_AZUNIMAT = space(1)
  w_AZSCAMAT = space(1)
  w_AZCODPRO = space(7)
  w_AZBARPRO = space(5)
  w_AZMASART = space(20)
  w_AZMASLOT = space(20)
  w_AZMASUBI = space(20)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_AZPERMUC = space(10)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_AZSTALIG = space(8)
  w_AZINTLIG = space(1)
  w_AZINTLIG = space(1)
  w_AZPRPALG = 0
  w_AZPREFLG = space(20)
  w_AZINTLIN = space(1)
  w_AZPRPALI = 0
  w_AZESLBIN = space(4)
  w_AZPREFLI = space(20)
  w_AZGMGCON = space(1)
  w_AZGMGFOR = space(1)
  w_AZGMGPER = space(1)
  w_AZSTARBE = space(8)
  w_AZINTRBE = space(1)
  w_AZPRPRBE = 0
  w_AZPRERBE = space(20)
  w_AZDATBLO = space(8)
  w_AZBLOGCL = space(1)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_AZMASMAT = space(20)
  w_AZCONCON = space(8)
  w_AZCONVEN = space(8)
  w_AZCONACQ = space(8)
  w_AZCONMAG = space(8)
  w_AZCONANA = space(8)
  w_SEPERSON = space(5)
  w_SETIPRIF = space(2)
  w_SEINDIRI = space(35)
  w_SE___CAP = space(8)
  w_SELOCALI = space(30)
  w_SEPROVIN = space(2)
  w_SETELEFO = space(18)
  w_SE_EMAIL = space(50)
  w_SE_EMPEC = space(50)
  w_SEPREMAP = space(1)
  w_DPCOGNOM = space(40)
  w_DPNOME = space(40)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_CCCAP = space(5)
  w_AZSCONET = 0
  w_DESCRI = space(40)
  w_AZCALUTD = space(1)
  w_AZCONCAS = space(1)
  w_SEDESPRE = space(1)
  w_AZFLRAGG = space(1)
  w_AZAITIPE = space(1)
  w_AZAITIPV = space(1)
  w_AZINPECE = 0
  w_AZAICFPI = space(16)
  w_AZAIRAGS = space(40)
  w_AZAIPREF = space(5)
  w_AZAITELE = space(18)
  w_AZAIINDI = space(35)
  w_AZAI_CAP = space(8)
  w_AZAICITT = space(30)
  w_AZAIPROV = space(2)
  w_AZINPEAC = 0
  w_AZCONCVE = space(1)
  w_AZCONCAC = space(1)
  proc Save(oFrom)
    this.w_AZCODAZI = oFrom.w_AZCODAZI
    this.w_AZRAGAZI = oFrom.w_AZRAGAZI
    this.w_AZPERAZI = oFrom.w_AZPERAZI
    this.w_COAZI = oFrom.w_COAZI
    this.w_AZINDAZI = oFrom.w_AZINDAZI
    this.w_AZCODOLT = oFrom.w_AZCODOLT
    this.w_AZCODCAT = oFrom.w_AZCODCAT
    this.w_AZCAPAZI = oFrom.w_AZCAPAZI
    this.w_AZLOCAZI = oFrom.w_AZLOCAZI
    this.w_AZPROAZI = oFrom.w_AZPROAZI
    this.w_AZCODNAZ = oFrom.w_AZCODNAZ
    this.w_DESNAZ = oFrom.w_DESNAZ
    this.w_CODISO = oFrom.w_CODISO
    this.w_AZCODLIN = oFrom.w_AZCODLIN
    this.w_DESLIN = oFrom.w_DESLIN
    this.w_AZCODINE = oFrom.w_AZCODINE
    this.w_AZCOFAZI = oFrom.w_AZCOFAZI
    this.w_AZPIVAZI = oFrom.w_AZPIVAZI
    this.w_AZTELEFO = oFrom.w_AZTELEFO
    this.w_AZTELFAX = oFrom.w_AZTELFAX
    this.w_AZ_EMAIL = oFrom.w_AZ_EMAIL
    this.w_AZINDWEB = oFrom.w_AZINDWEB
    this.w_AZCAPSOC = oFrom.w_AZCAPSOC
    this.w_AZNAGAZI = oFrom.w_AZNAGAZI
    this.w_AZRECREA = oFrom.w_AZRECREA
    this.w_AZTRIAZI = oFrom.w_AZTRIAZI
    this.w_AZESAAZI = oFrom.w_AZESAAZI
    this.w_AZCCFISC = oFrom.w_AZCCFISC
    this.w_AZCONTRI = oFrom.w_AZCONTRI
    this.w_UTCC = oFrom.w_UTCC
    this.w_UTCV = oFrom.w_UTCV
    this.w_UTDC = oFrom.w_UTDC
    this.w_UTDV = oFrom.w_UTDV
    this.w_COAZI = oFrom.w_COAZI
    this.w_DEAZI = oFrom.w_DEAZI
    this.w_AZVALEUR = oFrom.w_AZVALEUR
    this.w_DESEUR = oFrom.w_DESEUR
    this.w_AZVALLIR = oFrom.w_AZVALLIR
    this.w_DESLIR = oFrom.w_DESLIR
    this.w_AZUNIUTE = oFrom.w_AZUNIUTE
    this.w_AZCFNUME = oFrom.w_AZCFNUME
    this.w_AZTRAEXP = oFrom.w_AZTRAEXP
    this.w_AZTRAEXP = oFrom.w_AZTRAEXP
    this.w_AZTIPLOC = oFrom.w_AZTIPLOC
    this.w_AZDATTRA = oFrom.w_AZDATTRA
    this.w_AZQUADRA = oFrom.w_AZQUADRA
    this.w_AZFLUNIV = oFrom.w_AZFLUNIV
    this.w_AZFLARDO = oFrom.w_AZFLARDO
    this.w_AZPERPAR = oFrom.w_AZPERPAR
    this.w_AZPERSTS = oFrom.w_AZPERSTS
    this.w_AZCCRICA = oFrom.w_AZCCRICA
    this.w_AZGESCOM = oFrom.w_AZGESCOM
    this.w_AZRIPCOM = oFrom.w_AZRIPCOM
    this.w_AZFLRIPC = oFrom.w_AZFLRIPC
    this.w_AZDETCON = oFrom.w_AZDETCON
    this.w_AZFLVEBD = oFrom.w_AZFLVEBD
    this.w_AZFLFIDO = oFrom.w_AZFLFIDO
    this.w_AZGIORIS = oFrom.w_AZGIORIS
    this.w_AZFILPAR = oFrom.w_AZFILPAR
    this.w_AZMAXLIV = oFrom.w_AZMAXLIV
    this.w_AZMASCON = oFrom.w_AZMASCON
    this.w_AZMASCLI = oFrom.w_AZMASCLI
    this.w_AZMASFOR = oFrom.w_AZMASFOR
    this.w_AZMASMCO = oFrom.w_AZMASMCO
    this.w_AZMASCCC = oFrom.w_AZMASCCC
    this.w_AZMASMCC = oFrom.w_AZMASMCC
    this.w_AZINTMIN = oFrom.w_AZINTMIN
    this.w_AZSCAPNT = oFrom.w_AZSCAPNT
    this.w_COAZI = oFrom.w_COAZI
    this.w_DEAZI = oFrom.w_DEAZI
    this.w_AZDATINI = oFrom.w_AZDATINI
    this.w_AZDATSER = oFrom.w_AZDATSER
    this.w_AZNPRGEL = oFrom.w_AZNPRGEL
    this.w_AZPRESOB = oFrom.w_AZPRESOB
    this.w_AZTELSOB = oFrom.w_AZTELSOB
    this.w_AZCODPOR = oFrom.w_AZCODPOR
    this.w_AZDTOBBL = oFrom.w_AZDTOBBL
    this.w_COAZI = oFrom.w_COAZI
    this.w_DEAZI = oFrom.w_DEAZI
    this.w_AZMAGAZI = oFrom.w_AZMAGAZI
    this.w_DESMAG = oFrom.w_DESMAG
    this.w_AZSCAMAG = oFrom.w_AZSCAMAG
    this.w_AZMAGUTE = oFrom.w_AZMAGUTE
    this.w_AZPERAGE = oFrom.w_AZPERAGE
    this.w_AZPERART = oFrom.w_AZPERART
    this.w_AZGESCON = oFrom.w_AZGESCON
    this.w_AZPERDIS = oFrom.w_AZPERDIS
    this.w_AZPERSDA = oFrom.w_AZPERSDA
    this.w_AZPERORN = oFrom.w_AZPERORN
    this.w_AZGESLOT = oFrom.w_AZGESLOT
    this.w_AZGESUBI = oFrom.w_AZGESUBI
    this.w_AZGESMAT = oFrom.w_AZGESMAT
    this.w_AZFLCAMB = oFrom.w_AZFLCAMB
    this.w_AZRATEVA = oFrom.w_AZRATEVA
    this.w_AZFLSCOM = oFrom.w_AZFLSCOM
    this.w_AZDTSCOM = oFrom.w_AZDTSCOM
    this.w_AZSCOPIE = oFrom.w_AZSCOPIE
    this.w_AZDTSCPI = oFrom.w_AZDTSCPI
    this.w_AZFLCESC = oFrom.w_AZFLCESC
    this.w_AZXCONDI = oFrom.w_AZXCONDI
    this.w_AZDATDOC = oFrom.w_AZDATDOC
    this.w_AZCALNET = oFrom.w_AZCALNET
    this.w_AZPERPQT = oFrom.w_AZPERPQT
    this.w_AZPERPQD = oFrom.w_AZPERPQD
    this.w_AZNUMSCO = oFrom.w_AZNUMSCO
    this.w_AZCODEXT = oFrom.w_AZCODEXT
    this.w_AZPREEAN = oFrom.w_AZPREEAN
    this.w_AZDATMAT = oFrom.w_AZDATMAT
    this.w_AZUNIMAT = oFrom.w_AZUNIMAT
    this.w_AZSCAMAT = oFrom.w_AZSCAMAT
    this.w_AZCODPRO = oFrom.w_AZCODPRO
    this.w_AZBARPRO = oFrom.w_AZBARPRO
    this.w_AZMASART = oFrom.w_AZMASART
    this.w_AZMASLOT = oFrom.w_AZMASLOT
    this.w_AZMASUBI = oFrom.w_AZMASUBI
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_DATOBSO = oFrom.w_DATOBSO
    this.w_AZPERMUC = oFrom.w_AZPERMUC
    this.w_COAZI = oFrom.w_COAZI
    this.w_DEAZI = oFrom.w_DEAZI
    this.w_COAZI = oFrom.w_COAZI
    this.w_DEAZI = oFrom.w_DEAZI
    this.w_AZSTALIG = oFrom.w_AZSTALIG
    this.w_AZINTLIG = oFrom.w_AZINTLIG
    this.w_AZINTLIG = oFrom.w_AZINTLIG
    this.w_AZPRPALG = oFrom.w_AZPRPALG
    this.w_AZPREFLG = oFrom.w_AZPREFLG
    this.w_AZINTLIN = oFrom.w_AZINTLIN
    this.w_AZPRPALI = oFrom.w_AZPRPALI
    this.w_AZESLBIN = oFrom.w_AZESLBIN
    this.w_AZPREFLI = oFrom.w_AZPREFLI
    this.w_AZGMGCON = oFrom.w_AZGMGCON
    this.w_AZGMGFOR = oFrom.w_AZGMGFOR
    this.w_AZGMGPER = oFrom.w_AZGMGPER
    this.w_AZSTARBE = oFrom.w_AZSTARBE
    this.w_AZINTRBE = oFrom.w_AZINTRBE
    this.w_AZPRPRBE = oFrom.w_AZPRPRBE
    this.w_AZPRERBE = oFrom.w_AZPRERBE
    this.w_AZDATBLO = oFrom.w_AZDATBLO
    this.w_AZBLOGCL = oFrom.w_AZBLOGCL
    this.w_COAZI = oFrom.w_COAZI
    this.w_DEAZI = oFrom.w_DEAZI
    this.w_AZMASMAT = oFrom.w_AZMASMAT
    this.w_AZCONCON = oFrom.w_AZCONCON
    this.w_AZCONVEN = oFrom.w_AZCONVEN
    this.w_AZCONACQ = oFrom.w_AZCONACQ
    this.w_AZCONMAG = oFrom.w_AZCONMAG
    this.w_AZCONANA = oFrom.w_AZCONANA
    this.w_SEPERSON = oFrom.w_SEPERSON
    this.w_SETIPRIF = oFrom.w_SETIPRIF
    this.w_SEINDIRI = oFrom.w_SEINDIRI
    this.w_SE___CAP = oFrom.w_SE___CAP
    this.w_SELOCALI = oFrom.w_SELOCALI
    this.w_SEPROVIN = oFrom.w_SEPROVIN
    this.w_SETELEFO = oFrom.w_SETELEFO
    this.w_SE_EMAIL = oFrom.w_SE_EMAIL
    this.w_SE_EMPEC = oFrom.w_SE_EMPEC
    this.w_SEPREMAP = oFrom.w_SEPREMAP
    this.w_DPCOGNOM = oFrom.w_DPCOGNOM
    this.w_DPNOME = oFrom.w_DPNOME
    this.w_COAZI = oFrom.w_COAZI
    this.w_DEAZI = oFrom.w_DEAZI
    this.w_COAZI = oFrom.w_COAZI
    this.w_DEAZI = oFrom.w_DEAZI
    this.w_CCCAP = oFrom.w_CCCAP
    this.w_AZSCONET = oFrom.w_AZSCONET
    this.w_DESCRI = oFrom.w_DESCRI
    this.w_AZCALUTD = oFrom.w_AZCALUTD
    this.w_AZCONCAS = oFrom.w_AZCONCAS
    this.w_SEDESPRE = oFrom.w_SEDESPRE
    this.w_AZFLRAGG = oFrom.w_AZFLRAGG
    this.w_AZAITIPE = oFrom.w_AZAITIPE
    this.w_AZAITIPV = oFrom.w_AZAITIPV
    this.w_AZINPECE = oFrom.w_AZINPECE
    this.w_AZAICFPI = oFrom.w_AZAICFPI
    this.w_AZAIRAGS = oFrom.w_AZAIRAGS
    this.w_AZAIPREF = oFrom.w_AZAIPREF
    this.w_AZAITELE = oFrom.w_AZAITELE
    this.w_AZAIINDI = oFrom.w_AZAIINDI
    this.w_AZAI_CAP = oFrom.w_AZAI_CAP
    this.w_AZAICITT = oFrom.w_AZAICITT
    this.w_AZAIPROV = oFrom.w_AZAIPROV
    this.w_AZINPEAC = oFrom.w_AZINPEAC
    this.w_AZCONCVE = oFrom.w_AZCONCVE
    this.w_AZCONCAC = oFrom.w_AZCONCAC
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_AZCODAZI = this.w_AZCODAZI
    oTo.w_AZRAGAZI = this.w_AZRAGAZI
    oTo.w_AZPERAZI = this.w_AZPERAZI
    oTo.w_COAZI = this.w_COAZI
    oTo.w_AZINDAZI = this.w_AZINDAZI
    oTo.w_AZCODOLT = this.w_AZCODOLT
    oTo.w_AZCODCAT = this.w_AZCODCAT
    oTo.w_AZCAPAZI = this.w_AZCAPAZI
    oTo.w_AZLOCAZI = this.w_AZLOCAZI
    oTo.w_AZPROAZI = this.w_AZPROAZI
    oTo.w_AZCODNAZ = this.w_AZCODNAZ
    oTo.w_DESNAZ = this.w_DESNAZ
    oTo.w_CODISO = this.w_CODISO
    oTo.w_AZCODLIN = this.w_AZCODLIN
    oTo.w_DESLIN = this.w_DESLIN
    oTo.w_AZCODINE = this.w_AZCODINE
    oTo.w_AZCOFAZI = this.w_AZCOFAZI
    oTo.w_AZPIVAZI = this.w_AZPIVAZI
    oTo.w_AZTELEFO = this.w_AZTELEFO
    oTo.w_AZTELFAX = this.w_AZTELFAX
    oTo.w_AZ_EMAIL = this.w_AZ_EMAIL
    oTo.w_AZINDWEB = this.w_AZINDWEB
    oTo.w_AZCAPSOC = this.w_AZCAPSOC
    oTo.w_AZNAGAZI = this.w_AZNAGAZI
    oTo.w_AZRECREA = this.w_AZRECREA
    oTo.w_AZTRIAZI = this.w_AZTRIAZI
    oTo.w_AZESAAZI = this.w_AZESAAZI
    oTo.w_AZCCFISC = this.w_AZCCFISC
    oTo.w_AZCONTRI = this.w_AZCONTRI
    oTo.w_UTCC = this.w_UTCC
    oTo.w_UTCV = this.w_UTCV
    oTo.w_UTDC = this.w_UTDC
    oTo.w_UTDV = this.w_UTDV
    oTo.w_COAZI = this.w_COAZI
    oTo.w_DEAZI = this.w_DEAZI
    oTo.w_AZVALEUR = this.w_AZVALEUR
    oTo.w_DESEUR = this.w_DESEUR
    oTo.w_AZVALLIR = this.w_AZVALLIR
    oTo.w_DESLIR = this.w_DESLIR
    oTo.w_AZUNIUTE = this.w_AZUNIUTE
    oTo.w_AZCFNUME = this.w_AZCFNUME
    oTo.w_AZTRAEXP = this.w_AZTRAEXP
    oTo.w_AZTRAEXP = this.w_AZTRAEXP
    oTo.w_AZTIPLOC = this.w_AZTIPLOC
    oTo.w_AZDATTRA = this.w_AZDATTRA
    oTo.w_AZQUADRA = this.w_AZQUADRA
    oTo.w_AZFLUNIV = this.w_AZFLUNIV
    oTo.w_AZFLARDO = this.w_AZFLARDO
    oTo.w_AZPERPAR = this.w_AZPERPAR
    oTo.w_AZPERSTS = this.w_AZPERSTS
    oTo.w_AZCCRICA = this.w_AZCCRICA
    oTo.w_AZGESCOM = this.w_AZGESCOM
    oTo.w_AZRIPCOM = this.w_AZRIPCOM
    oTo.w_AZFLRIPC = this.w_AZFLRIPC
    oTo.w_AZDETCON = this.w_AZDETCON
    oTo.w_AZFLVEBD = this.w_AZFLVEBD
    oTo.w_AZFLFIDO = this.w_AZFLFIDO
    oTo.w_AZGIORIS = this.w_AZGIORIS
    oTo.w_AZFILPAR = this.w_AZFILPAR
    oTo.w_AZMAXLIV = this.w_AZMAXLIV
    oTo.w_AZMASCON = this.w_AZMASCON
    oTo.w_AZMASCLI = this.w_AZMASCLI
    oTo.w_AZMASFOR = this.w_AZMASFOR
    oTo.w_AZMASMCO = this.w_AZMASMCO
    oTo.w_AZMASCCC = this.w_AZMASCCC
    oTo.w_AZMASMCC = this.w_AZMASMCC
    oTo.w_AZINTMIN = this.w_AZINTMIN
    oTo.w_AZSCAPNT = this.w_AZSCAPNT
    oTo.w_COAZI = this.w_COAZI
    oTo.w_DEAZI = this.w_DEAZI
    oTo.w_AZDATINI = this.w_AZDATINI
    oTo.w_AZDATSER = this.w_AZDATSER
    oTo.w_AZNPRGEL = this.w_AZNPRGEL
    oTo.w_AZPRESOB = this.w_AZPRESOB
    oTo.w_AZTELSOB = this.w_AZTELSOB
    oTo.w_AZCODPOR = this.w_AZCODPOR
    oTo.w_AZDTOBBL = this.w_AZDTOBBL
    oTo.w_COAZI = this.w_COAZI
    oTo.w_DEAZI = this.w_DEAZI
    oTo.w_AZMAGAZI = this.w_AZMAGAZI
    oTo.w_DESMAG = this.w_DESMAG
    oTo.w_AZSCAMAG = this.w_AZSCAMAG
    oTo.w_AZMAGUTE = this.w_AZMAGUTE
    oTo.w_AZPERAGE = this.w_AZPERAGE
    oTo.w_AZPERART = this.w_AZPERART
    oTo.w_AZGESCON = this.w_AZGESCON
    oTo.w_AZPERDIS = this.w_AZPERDIS
    oTo.w_AZPERSDA = this.w_AZPERSDA
    oTo.w_AZPERORN = this.w_AZPERORN
    oTo.w_AZGESLOT = this.w_AZGESLOT
    oTo.w_AZGESUBI = this.w_AZGESUBI
    oTo.w_AZGESMAT = this.w_AZGESMAT
    oTo.w_AZFLCAMB = this.w_AZFLCAMB
    oTo.w_AZRATEVA = this.w_AZRATEVA
    oTo.w_AZFLSCOM = this.w_AZFLSCOM
    oTo.w_AZDTSCOM = this.w_AZDTSCOM
    oTo.w_AZSCOPIE = this.w_AZSCOPIE
    oTo.w_AZDTSCPI = this.w_AZDTSCPI
    oTo.w_AZFLCESC = this.w_AZFLCESC
    oTo.w_AZXCONDI = this.w_AZXCONDI
    oTo.w_AZDATDOC = this.w_AZDATDOC
    oTo.w_AZCALNET = this.w_AZCALNET
    oTo.w_AZPERPQT = this.w_AZPERPQT
    oTo.w_AZPERPQD = this.w_AZPERPQD
    oTo.w_AZNUMSCO = this.w_AZNUMSCO
    oTo.w_AZCODEXT = this.w_AZCODEXT
    oTo.w_AZPREEAN = this.w_AZPREEAN
    oTo.w_AZDATMAT = this.w_AZDATMAT
    oTo.w_AZUNIMAT = this.w_AZUNIMAT
    oTo.w_AZSCAMAT = this.w_AZSCAMAT
    oTo.w_AZCODPRO = this.w_AZCODPRO
    oTo.w_AZBARPRO = this.w_AZBARPRO
    oTo.w_AZMASART = this.w_AZMASART
    oTo.w_AZMASLOT = this.w_AZMASLOT
    oTo.w_AZMASUBI = this.w_AZMASUBI
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_DATOBSO = this.w_DATOBSO
    oTo.w_AZPERMUC = this.w_AZPERMUC
    oTo.w_COAZI = this.w_COAZI
    oTo.w_DEAZI = this.w_DEAZI
    oTo.w_COAZI = this.w_COAZI
    oTo.w_DEAZI = this.w_DEAZI
    oTo.w_AZSTALIG = this.w_AZSTALIG
    oTo.w_AZINTLIG = this.w_AZINTLIG
    oTo.w_AZINTLIG = this.w_AZINTLIG
    oTo.w_AZPRPALG = this.w_AZPRPALG
    oTo.w_AZPREFLG = this.w_AZPREFLG
    oTo.w_AZINTLIN = this.w_AZINTLIN
    oTo.w_AZPRPALI = this.w_AZPRPALI
    oTo.w_AZESLBIN = this.w_AZESLBIN
    oTo.w_AZPREFLI = this.w_AZPREFLI
    oTo.w_AZGMGCON = this.w_AZGMGCON
    oTo.w_AZGMGFOR = this.w_AZGMGFOR
    oTo.w_AZGMGPER = this.w_AZGMGPER
    oTo.w_AZSTARBE = this.w_AZSTARBE
    oTo.w_AZINTRBE = this.w_AZINTRBE
    oTo.w_AZPRPRBE = this.w_AZPRPRBE
    oTo.w_AZPRERBE = this.w_AZPRERBE
    oTo.w_AZDATBLO = this.w_AZDATBLO
    oTo.w_AZBLOGCL = this.w_AZBLOGCL
    oTo.w_COAZI = this.w_COAZI
    oTo.w_DEAZI = this.w_DEAZI
    oTo.w_AZMASMAT = this.w_AZMASMAT
    oTo.w_AZCONCON = this.w_AZCONCON
    oTo.w_AZCONVEN = this.w_AZCONVEN
    oTo.w_AZCONACQ = this.w_AZCONACQ
    oTo.w_AZCONMAG = this.w_AZCONMAG
    oTo.w_AZCONANA = this.w_AZCONANA
    oTo.w_SEPERSON = this.w_SEPERSON
    oTo.w_SETIPRIF = this.w_SETIPRIF
    oTo.w_SEINDIRI = this.w_SEINDIRI
    oTo.w_SE___CAP = this.w_SE___CAP
    oTo.w_SELOCALI = this.w_SELOCALI
    oTo.w_SEPROVIN = this.w_SEPROVIN
    oTo.w_SETELEFO = this.w_SETELEFO
    oTo.w_SE_EMAIL = this.w_SE_EMAIL
    oTo.w_SE_EMPEC = this.w_SE_EMPEC
    oTo.w_SEPREMAP = this.w_SEPREMAP
    oTo.w_DPCOGNOM = this.w_DPCOGNOM
    oTo.w_DPNOME = this.w_DPNOME
    oTo.w_COAZI = this.w_COAZI
    oTo.w_DEAZI = this.w_DEAZI
    oTo.w_COAZI = this.w_COAZI
    oTo.w_DEAZI = this.w_DEAZI
    oTo.w_CCCAP = this.w_CCCAP
    oTo.w_AZSCONET = this.w_AZSCONET
    oTo.w_DESCRI = this.w_DESCRI
    oTo.w_AZCALUTD = this.w_AZCALUTD
    oTo.w_AZCONCAS = this.w_AZCONCAS
    oTo.w_SEDESPRE = this.w_SEDESPRE
    oTo.w_AZFLRAGG = this.w_AZFLRAGG
    oTo.w_AZAITIPE = this.w_AZAITIPE
    oTo.w_AZAITIPV = this.w_AZAITIPV
    oTo.w_AZINPECE = this.w_AZINPECE
    oTo.w_AZAICFPI = this.w_AZAICFPI
    oTo.w_AZAIRAGS = this.w_AZAIRAGS
    oTo.w_AZAIPREF = this.w_AZAIPREF
    oTo.w_AZAITELE = this.w_AZAITELE
    oTo.w_AZAIINDI = this.w_AZAIINDI
    oTo.w_AZAI_CAP = this.w_AZAI_CAP
    oTo.w_AZAICITT = this.w_AZAICITT
    oTo.w_AZAIPROV = this.w_AZAIPROV
    oTo.w_AZINPEAC = this.w_AZINPEAC
    oTo.w_AZCONCVE = this.w_AZCONCVE
    oTo.w_AZCONCAC = this.w_AZCONCAC
    PCContext::Load(oTo)
enddefine

define class tcgsar_azi as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 723
  Height = 490+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-05"
  HelpContextID=7768215
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=185

  * --- Constant Properties
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  SEDIAZIE_IDX = 0
  NAZIONI_IDX = 0
  MAGAZZIN_IDX = 0
  ATTIMAST_IDX = 0
  TITOLARI_IDX = 0
  LINGUE_IDX = 0
  ESERCIZI_IDX = 0
  COD_CATA_IDX = 0
  COD_AREO_IDX = 0
  cFile = "AZIENDA"
  cKeySelect = "AZCODAZI"
  cKeyWhere  = "AZCODAZI=this.w_AZCODAZI"
  cKeyWhereODBC = '"AZCODAZI="+cp_ToStrODBC(this.w_AZCODAZI)';

  cKeyWhereODBCqualified = '"AZIENDA.AZCODAZI="+cp_ToStrODBC(this.w_AZCODAZI)';

  cPrg = "gsar_azi"
  cComment = "Dati azienda"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AZI'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZCODAZI = space(5)
  w_AZRAGAZI = space(80)
  w_AZPERAZI = space(1)
  o_AZPERAZI = space(1)
  w_COAZI = space(5)
  w_AZINDAZI = space(35)
  w_AZCODOLT = 0
  w_AZCODCAT = space(4)
  w_AZCAPAZI = space(8)
  o_AZCAPAZI = space(8)
  w_AZLOCAZI = space(30)
  w_AZPROAZI = space(2)
  w_AZCODNAZ = space(3)
  w_DESNAZ = space(35)
  w_CODISO = space(3)
  w_AZCODLIN = space(3)
  w_DESLIN = space(30)
  w_AZCODINE = space(9)
  w_AZCOFAZI = space(20)
  w_AZPIVAZI = space(20)
  w_AZTELEFO = space(18)
  w_AZTELFAX = space(18)
  w_AZ_EMAIL = space(254)
  w_AZINDWEB = space(254)
  w_AZCAPSOC = space(30)
  w_AZNAGAZI = space(5)
  w_AZRECREA = space(20)
  w_AZTRIAZI = space(25)
  w_AZESAAZI = space(25)
  w_AZCCFISC = space(20)
  w_AZCONTRI = space(20)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_AZVALEUR = space(3)
  w_DESEUR = space(35)
  w_AZVALLIR = space(3)
  w_DESLIR = space(35)
  w_AZUNIUTE = space(1)
  w_AZCFNUME = space(1)
  w_AZTRAEXP = space(1)
  o_AZTRAEXP = space(1)
  w_AZTRAEXP = space(1)
  w_AZTIPLOC = space(3)
  w_AZDATTRA = ctod('  /  /  ')
  w_AZQUADRA = space(1)
  w_AZFLUNIV = space(1)
  w_AZFLARDO = space(1)
  w_AZPERPAR = space(1)
  w_AZPERSTS = space(1)
  w_AZCCRICA = space(1)
  o_AZCCRICA = space(1)
  w_AZGESCOM = space(1)
  o_AZGESCOM = space(1)
  w_AZRIPCOM = space(1)
  w_AZFLRIPC = space(1)
  w_AZDETCON = space(1)
  w_AZFLVEBD = space(1)
  w_AZFLFIDO = space(1)
  w_AZGIORIS = 0
  w_AZFILPAR = space(1)
  w_AZMAXLIV = 0
  w_AZMASCON = space(15)
  w_AZMASCLI = space(15)
  w_AZMASFOR = space(15)
  w_AZMASMCO = space(15)
  w_AZMASCCC = space(15)
  w_AZMASMCC = space(15)
  w_AZINTMIN = 0
  w_AZSCAPNT = space(4)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_AZDATINI = ctod('  /  /  ')
  w_AZDATSER = ctod('  /  /  ')
  w_AZNPRGEL = 0
  w_AZPRESOB = space(5)
  w_AZTELSOB = space(18)
  w_AZCODPOR = space(10)
  w_AZDTOBBL = space(1)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_AZMAGAZI = space(5)
  w_DESMAG = space(30)
  w_AZSCAMAG = ctod('  /  /  ')
  w_AZMAGUTE = space(1)
  w_AZPERAGE = space(1)
  w_AZPERART = space(1)
  w_AZGESCON = space(1)
  w_AZPERDIS = space(1)
  w_AZPERSDA = space(1)
  w_AZPERORN = space(1)
  w_AZGESLOT = space(1)
  w_AZGESUBI = space(1)
  w_AZGESMAT = space(1)
  o_AZGESMAT = space(1)
  w_AZFLCAMB = space(1)
  w_AZRATEVA = space(1)
  w_AZFLSCOM = space(1)
  o_AZFLSCOM = space(1)
  w_AZDTSCOM = ctod('  /  /  ')
  w_AZSCOPIE = space(1)
  o_AZSCOPIE = space(1)
  w_AZDTSCPI = ctod('  /  /  ')
  w_AZFLCESC = space(1)
  w_AZXCONDI = space(1)
  w_AZDATDOC = space(1)
  w_AZCALNET = 0
  w_AZPERPQT = 0
  w_AZPERPQD = 0
  w_AZNUMSCO = 0
  w_AZCODEXT = space(1)
  w_AZPREEAN = space(2)
  w_AZDATMAT = ctod('  /  /  ')
  w_AZUNIMAT = space(1)
  w_AZSCAMAT = space(1)
  w_AZCODPRO = space(7)
  o_AZCODPRO = space(7)
  w_AZBARPRO = space(5)
  w_AZMASART = space(20)
  w_AZMASLOT = space(20)
  w_AZMASUBI = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_AZPERMUC = space(10)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_AZSTALIG = ctod('  /  /  ')
  w_AZINTLIG = space(1)
  o_AZINTLIG = space(1)
  w_AZINTLIG = space(1)
  w_AZPRPALG = 0
  w_AZPREFLG = space(20)
  w_AZINTLIN = space(1)
  o_AZINTLIN = space(1)
  w_AZPRPALI = 0
  w_AZESLBIN = space(4)
  w_AZPREFLI = space(20)
  w_AZGMGCON = space(1)
  w_AZGMGFOR = space(1)
  w_AZGMGPER = space(1)
  w_AZSTARBE = ctod('  /  /  ')
  w_AZINTRBE = space(1)
  o_AZINTRBE = space(1)
  w_AZPRPRBE = 0
  w_AZPRERBE = space(20)
  w_AZDATBLO = ctod('  /  /  ')
  w_AZBLOGCL = space(1)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_AZMASMAT = space(20)
  w_AZCONCON = ctod('  /  /  ')
  w_AZCONVEN = ctod('  /  /  ')
  w_AZCONACQ = ctod('  /  /  ')
  w_AZCONMAG = ctod('  /  /  ')
  w_AZCONANA = ctod('  /  /  ')
  w_SEPERSON = space(5)
  w_SETIPRIF = space(2)
  w_SEINDIRI = space(35)
  w_SE___CAP = space(8)
  w_SELOCALI = space(30)
  w_SEPROVIN = space(2)
  w_SETELEFO = space(18)
  w_SE_EMAIL = space(50)
  w_SE_EMPEC = space(50)
  w_SEPREMAP = space(1)
  w_DPCOGNOM = space(40)
  w_DPNOME = space(40)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_COAZI = space(5)
  w_DEAZI = space(80)
  w_CCCAP = space(5)
  w_AZSCONET = 0
  w_DESCRI = space(40)
  w_AZCALUTD = space(1)
  w_AZCONCAS = space(1)
  w_SEDESPRE = space(1)
  w_AZFLRAGG = space(1)
  w_AZAITIPE = space(1)
  w_AZAITIPV = space(1)
  w_AZINPECE = 0
  w_AZAICFPI = space(16)
  w_AZAIRAGS = space(40)
  w_AZAIPREF = space(5)
  w_AZAITELE = space(18)
  w_AZAIINDI = space(35)
  w_AZAI_CAP = space(8)
  w_AZAICITT = space(30)
  w_AZAIPROV = space(2)
  w_AZINPEAC = 0
  w_AZCONCVE = space(1)
  w_AZCONCAC = space(1)

  * --- Children pointers
  GSAR_MTT = .NULL.
  GSAR_ADR = .NULL.
  gsar_abk = .NULL.
  GSAR_AIT = .NULL.
  w_ZoomSedi = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=9, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_aziPag1","gsar_azi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate(""+IIF(IsAlt(),"Studio","Azienda")+"")
      .Pages(1).HelpContextID = 258863344
      .Pages(2).addobject("oPag","tgsar_aziPag2","gsar_azi",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Contabile")
      .Pages(2).HelpContextID = 151118562
      .Pages(3).addobject("oPag","tgsar_aziPag3","gsar_azi",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dati INTRA")
      .Pages(3).HelpContextID = 199718058
      .Pages(4).addobject("oPag","tgsar_aziPag4","gsar_azi",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate(""+IIF(IsAlt(),"Parcelle","Magazzino")+"")
      .Pages(4).HelpContextID = 77527269
      .Pages(5).addobject("oPag","tgsar_aziPag5","gsar_azi",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Stampe fiscali")
      .Pages(5).HelpContextID = 49169380
      .Pages(6).addobject("oPag","tgsar_aziPag6","gsar_azi",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Sedi")
      .Pages(6).HelpContextID = 15086374
      .Pages(7).addobject("oPag","tgsar_aziPag7","gsar_azi",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Titolari")
      .Pages(7).HelpContextID = 122865761
      .Pages(8).addobject("oPag","tgsar_aziPag8","gsar_azi",8)
      .Pages(8).oPag.Visible=.t.
      .Pages(8).Caption=cp_Translate("Backup")
      .Pages(8).HelpContextID = 130540010
      .Pages(9).addobject("oPag","tgsar_aziPag9","gsar_azi",9)
      .Pages(9).oPag.Visible=.t.
      .Pages(9).Caption=cp_Translate("Storico")
      .Pages(9).HelpContextID = 176157222
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAZRAGAZI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
    proc Init()
      this.w_ZoomSedi = this.oPgFrm.Pages(6).oPag.ZoomSedi
      DoDefault()
    proc Destroy()
      this.w_ZoomSedi = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='SEDIAZIE'
    this.cWorkTables[3]='NAZIONI'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='ATTIMAST'
    this.cWorkTables[6]='TITOLARI'
    this.cWorkTables[7]='LINGUE'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='COD_CATA'
    this.cWorkTables[10]='COD_AREO'
    this.cWorkTables[11]='AZIENDA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(11))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AZIENDA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AZIENDA_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MTT = CREATEOBJECT('stdDynamicChild',this,'GSAR_MTT',this.oPgFrm.Page7.oPag.oLinkPC_7_4)
    this.GSAR_ADR = CREATEOBJECT('stdLazyChild',this,'GSAR_ADR')
    this.gsar_abk = CREATEOBJECT('stdDynamicChild',this,'gsar_abk',this.oPgFrm.Page8.oPag.oLinkPC_8_5)
    this.GSAR_AIT = CREATEOBJECT('stdDynamicChild',this,'GSAR_AIT',this.oPgFrm.Page3.oPag.oLinkPC_3_36)
    return

  procedure NewContext()
    return(createobject('tsgsar_azi'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSAR_MTT)
      this.GSAR_MTT.DestroyChildrenChain()
    endif
    if !ISNULL(this.GSAR_ADR)
      this.GSAR_ADR.DestroyChildrenChain()
    endif
    if !ISNULL(this.gsar_abk)
      this.gsar_abk.DestroyChildrenChain()
    endif
    if !ISNULL(this.GSAR_AIT)
      this.GSAR_AIT.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSAR_MTT.HideChildrenChain()
    this.GSAR_ADR.HideChildrenChain()
    this.gsar_abk.HideChildrenChain()
    this.GSAR_AIT.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSAR_MTT.ShowChildrenChain()
    this.gsar_abk.ShowChildrenChain()
    this.GSAR_AIT.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MTT)
      this.GSAR_MTT.DestroyChildrenChain()
      this.GSAR_MTT=.NULL.
    endif
    this.oPgFrm.Page7.oPag.RemoveObject('oLinkPC_7_4')
    if !ISNULL(this.GSAR_ADR)
      this.GSAR_ADR.DestroyChildrenChain()
      this.GSAR_ADR=.NULL.
    endif
    if !ISNULL(this.gsar_abk)
      this.gsar_abk.DestroyChildrenChain()
      this.gsar_abk=.NULL.
    endif
    this.oPgFrm.Page8.oPag.RemoveObject('oLinkPC_8_5')
    if !ISNULL(this.GSAR_AIT)
      this.GSAR_AIT.DestroyChildrenChain()
      this.GSAR_AIT=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_36')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MTT.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_ADR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.gsar_abk.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_AIT.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MTT.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_ADR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.gsar_abk.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_AIT.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MTT.NewDocument()
    this.GSAR_ADR.NewDocument()
    this.gsar_abk.NewDocument()
    this.GSAR_AIT.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MTT.SetKey(;
            .w_AZCODAZI,"TTCODAZI";
            )
      this.GSAR_ADR.SetKey(;
            .w_AZCODAZI,"RFCODAZI";
            )
      this.gsar_abk.SetKey(;
            .w_AZCODAZI,"AZCODAZI";
            )
      this.GSAR_AIT.SetKey(;
            .w_AZCODAZI,"ITCODAZI";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MTT.ChangeRow(this.cRowID+'      1',1;
             ,.w_AZCODAZI,"TTCODAZI";
             )
      .GSAR_ADR.ChangeRow(this.cRowID+'      1',1;
             ,.w_AZCODAZI,"RFCODAZI";
             )
      .gsar_abk.ChangeRow(this.cRowID+'      1',1;
             ,.w_AZCODAZI,"AZCODAZI";
             )
      .GSAR_AIT.ChangeRow(this.cRowID+'      1',1;
             ,.w_AZCODAZI,"ITCODAZI";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MTT)
        i_f=.GSAR_MTT.BuildFilter()
        if !(i_f==.GSAR_MTT.cQueryFilter)
          i_fnidx=.GSAR_MTT.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MTT.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MTT.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MTT.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MTT.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.gsar_abk)
        i_f=.gsar_abk.BuildFilter()
        if !(i_f==.gsar_abk.cQueryFilter)
          i_fnidx=.gsar_abk.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.gsar_abk.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.gsar_abk.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.gsar_abk.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.gsar_abk.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_AIT)
        i_f=.GSAR_AIT.BuildFilter()
        if !(i_f==.GSAR_AIT.cQueryFilter)
          i_fnidx=.GSAR_AIT.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_AIT.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_AIT.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_AIT.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_AIT.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_3_9_joined
    link_3_9_joined=.f.
    local link_4_4_joined
    link_4_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from AZIENDA where AZCODAZI=KeySet.AZCODAZI
    *
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AZIENDA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AZIENDA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AZIENDA '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_9_joined=this.AddJoinedLink_3_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_4_joined=this.AddJoinedLink_4_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESNAZ = space(35)
        .w_CODISO = space(3)
        .w_DESLIN = space(30)
        .w_DESEUR = space(35)
        .w_DESLIR = space(35)
        .w_DESMAG = space(30)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESCRI = space(40)
        .w_AZCODAZI = NVL(AZCODAZI,space(5))
        .w_AZRAGAZI = NVL(AZRAGAZI,space(80))
        .w_AZPERAZI = NVL(AZPERAZI,space(1))
        .w_COAZI = .w_AZCODAZI
        .w_AZINDAZI = NVL(AZINDAZI,space(35))
        .w_AZCODOLT = NVL(AZCODOLT,0)
        .w_AZCODCAT = NVL(AZCODCAT,space(4))
          * evitabile
          *.link_1_7('Load')
        .w_AZCAPAZI = NVL(AZCAPAZI,space(8))
        .w_AZLOCAZI = NVL(AZLOCAZI,space(30))
        .w_AZPROAZI = NVL(AZPROAZI,space(2))
        .w_AZCODNAZ = NVL(AZCODNAZ,space(3))
          if link_1_11_joined
            this.w_AZCODNAZ = NVL(NACODNAZ111,NVL(this.w_AZCODNAZ,space(3)))
            this.w_DESNAZ = NVL(NADESNAZ111,space(35))
            this.w_CODISO = NVL(NACODISO111,space(3))
          else
          .link_1_11('Load')
          endif
        .w_AZCODLIN = NVL(AZCODLIN,space(3))
          if link_1_14_joined
            this.w_AZCODLIN = NVL(LUCODICE114,NVL(this.w_AZCODLIN,space(3)))
            this.w_DESLIN = NVL(LUDESCRI114,space(30))
          else
          .link_1_14('Load')
          endif
        .w_AZCODINE = NVL(AZCODINE,space(9))
        .w_AZCOFAZI = NVL(AZCOFAZI,space(20))
        .w_AZPIVAZI = NVL(AZPIVAZI,space(20))
        .w_AZTELEFO = NVL(AZTELEFO,space(18))
        .w_AZTELFAX = NVL(AZTELFAX,space(18))
        .w_AZ_EMAIL = NVL(AZ_EMAIL,space(254))
        .w_AZINDWEB = NVL(AZINDWEB,space(254))
        .w_AZCAPSOC = NVL(AZCAPSOC,space(30))
        .w_AZNAGAZI = NVL(AZNAGAZI,space(5))
        .w_AZRECREA = NVL(AZRECREA,space(20))
        .w_AZTRIAZI = NVL(AZTRIAZI,space(25))
        .w_AZESAAZI = NVL(AZESAAZI,space(25))
        .w_AZCCFISC = NVL(AZCCFISC,space(20))
        .w_AZCONTRI = NVL(AZCONTRI,space(20))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .w_AZVALEUR = NVL(AZVALEUR,space(3))
          if link_2_3_joined
            this.w_AZVALEUR = NVL(VACODVAL203,NVL(this.w_AZVALEUR,space(3)))
            this.w_DESEUR = NVL(VADESVAL203,space(35))
          else
          .link_2_3('Load')
          endif
        .w_AZVALLIR = NVL(AZVALLIR,space(3))
          if link_2_5_joined
            this.w_AZVALLIR = NVL(VACODVAL205,NVL(this.w_AZVALLIR,space(3)))
            this.w_DESLIR = NVL(VADESVAL205,space(35))
          else
          .link_2_5('Load')
          endif
        .w_AZUNIUTE = NVL(AZUNIUTE,space(1))
        .w_AZCFNUME = NVL(AZCFNUME,space(1))
        .w_AZTRAEXP = NVL(AZTRAEXP,space(1))
        .w_AZTRAEXP = NVL(AZTRAEXP,space(1))
        .w_AZTIPLOC = NVL(AZTIPLOC,space(3))
        .w_AZDATTRA = NVL(cp_ToDate(AZDATTRA),ctod("  /  /  "))
        .w_AZQUADRA = NVL(AZQUADRA,space(1))
        .w_AZFLUNIV = NVL(AZFLUNIV,space(1))
        .w_AZFLARDO = NVL(AZFLARDO,space(1))
        .w_AZPERPAR = NVL(AZPERPAR,space(1))
        .w_AZPERSTS = NVL(AZPERSTS,space(1))
        .w_AZCCRICA = NVL(AZCCRICA,space(1))
        .w_AZGESCOM = NVL(AZGESCOM,space(1))
        .w_AZRIPCOM = NVL(AZRIPCOM,space(1))
        .w_AZFLRIPC = NVL(AZFLRIPC,space(1))
        .w_AZDETCON = NVL(AZDETCON,space(1))
        .w_AZFLVEBD = NVL(AZFLVEBD,space(1))
        .w_AZFLFIDO = NVL(AZFLFIDO,space(1))
        .w_AZGIORIS = NVL(AZGIORIS,0)
        .w_AZFILPAR = NVL(AZFILPAR,space(1))
        .w_AZMAXLIV = NVL(AZMAXLIV,0)
        .w_AZMASCON = NVL(AZMASCON,space(15))
        .w_AZMASCLI = NVL(AZMASCLI,space(15))
        .w_AZMASFOR = NVL(AZMASFOR,space(15))
        .w_AZMASMCO = NVL(AZMASMCO,space(15))
        .w_AZMASCCC = NVL(AZMASCCC,space(15))
        .w_AZMASMCC = NVL(AZMASMCC,space(15))
        .w_AZINTMIN = NVL(AZINTMIN,0)
        .w_AZSCAPNT = NVL(AZSCAPNT,space(4))
          * evitabile
          *.link_2_36('Load')
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .w_AZDATINI = NVL(cp_ToDate(AZDATINI),ctod("  /  /  "))
        .w_AZDATSER = NVL(cp_ToDate(AZDATSER),ctod("  /  /  "))
        .w_AZNPRGEL = NVL(AZNPRGEL,0)
        .w_AZPRESOB = NVL(AZPRESOB,space(5))
        .w_AZTELSOB = NVL(AZTELSOB,space(18))
        .w_AZCODPOR = NVL(AZCODPOR,space(10))
          if link_3_9_joined
            this.w_AZCODPOR = NVL(PPCODICE309,NVL(this.w_AZCODPOR,space(10)))
            this.w_DESCRI = NVL(PPDESCRI309,space(40))
          else
          .link_3_9('Load')
          endif
        .w_AZDTOBBL = NVL(AZDTOBBL,space(1))
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .w_AZMAGAZI = NVL(AZMAGAZI,space(5))
          if link_4_4_joined
            this.w_AZMAGAZI = NVL(MGCODMAG404,NVL(this.w_AZMAGAZI,space(5)))
            this.w_DESMAG = NVL(MGDESMAG404,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(MGDTOBSO404),ctod("  /  /  "))
          else
          .link_4_4('Load')
          endif
        .w_AZSCAMAG = NVL(cp_ToDate(AZSCAMAG),ctod("  /  /  "))
        .w_AZMAGUTE = NVL(AZMAGUTE,space(1))
        .w_AZPERAGE = NVL(AZPERAGE,space(1))
        .w_AZPERART = NVL(AZPERART,space(1))
        .w_AZGESCON = NVL(AZGESCON,space(1))
        .w_AZPERDIS = NVL(AZPERDIS,space(1))
        .w_AZPERSDA = NVL(AZPERSDA,space(1))
        .w_AZPERORN = NVL(AZPERORN,space(1))
        .w_AZGESLOT = NVL(AZGESLOT,space(1))
        .w_AZGESUBI = NVL(AZGESUBI,space(1))
        .w_AZGESMAT = NVL(AZGESMAT,space(1))
        .w_AZFLCAMB = NVL(AZFLCAMB,space(1))
        .w_AZRATEVA = NVL(AZRATEVA,space(1))
        .w_AZFLSCOM = NVL(AZFLSCOM,space(1))
        .w_AZDTSCOM = NVL(cp_ToDate(AZDTSCOM),ctod("  /  /  "))
        .w_AZSCOPIE = NVL(AZSCOPIE,space(1))
        .w_AZDTSCPI = NVL(cp_ToDate(AZDTSCPI),ctod("  /  /  "))
        .w_AZFLCESC = NVL(AZFLCESC,space(1))
        .w_AZXCONDI = NVL(AZXCONDI,space(1))
        .w_AZDATDOC = NVL(AZDATDOC,space(1))
        .w_AZCALNET = NVL(AZCALNET,0)
        .w_AZPERPQT = NVL(AZPERPQT,0)
        .w_AZPERPQD = NVL(AZPERPQD,0)
        .w_AZNUMSCO = NVL(AZNUMSCO,0)
        .w_AZCODEXT = NVL(AZCODEXT,space(1))
        .w_AZPREEAN = NVL(AZPREEAN,space(2))
        .w_AZDATMAT = NVL(cp_ToDate(AZDATMAT),ctod("  /  /  "))
        .w_AZUNIMAT = NVL(AZUNIMAT,space(1))
        .w_AZSCAMAT = NVL(AZSCAMAT,space(1))
        .w_AZCODPRO = NVL(AZCODPRO,space(7))
        .w_AZBARPRO = NVL(AZBARPRO,space(5))
        .w_AZMASART = NVL(AZMASART,space(20))
        .w_AZMASLOT = NVL(AZMASLOT,space(20))
        .w_AZMASUBI = NVL(AZMASUBI,space(20))
        .w_AZPERMUC = NVL(AZPERMUC,space(10))
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .w_AZSTALIG = NVL(cp_ToDate(AZSTALIG),ctod("  /  /  "))
        .w_AZINTLIG = NVL(AZINTLIG,space(1))
        .w_AZINTLIG = NVL(AZINTLIG,space(1))
        .w_AZPRPALG = NVL(AZPRPALG,0)
        .w_AZPREFLG = NVL(AZPREFLG,space(20))
        .w_AZINTLIN = NVL(AZINTLIN,space(1))
        .w_AZPRPALI = NVL(AZPRPALI,0)
        .w_AZESLBIN = NVL(AZESLBIN,space(4))
          * evitabile
          *.link_5_8('Load')
        .w_AZPREFLI = NVL(AZPREFLI,space(20))
        .w_AZGMGCON = NVL(AZGMGCON,space(1))
        .w_AZGMGFOR = NVL(AZGMGFOR,space(1))
        .w_AZGMGPER = NVL(AZGMGPER,space(1))
        .w_AZSTARBE = NVL(cp_ToDate(AZSTARBE),ctod("  /  /  "))
        .w_AZINTRBE = NVL(AZINTRBE,space(1))
        .w_AZPRPRBE = NVL(AZPRPRBE,0)
        .w_AZPRERBE = NVL(AZPRERBE,space(20))
        .w_AZDATBLO = NVL(cp_ToDate(AZDATBLO),ctod("  /  /  "))
        .w_AZBLOGCL = NVL(AZBLOGCL,space(1))
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .oPgFrm.Page4.oPag.oObj_4_57.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_58.Calculate()
        .w_AZMASMAT = NVL(AZMASMAT,space(20))
        .w_AZCONCON = NVL(cp_ToDate(AZCONCON),ctod("  /  /  "))
        .w_AZCONVEN = NVL(cp_ToDate(AZCONVEN),ctod("  /  /  "))
        .w_AZCONACQ = NVL(cp_ToDate(AZCONACQ),ctod("  /  /  "))
        .w_AZCONMAG = NVL(cp_ToDate(AZCONMAG),ctod("  /  /  "))
        .w_AZCONANA = NVL(cp_ToDate(AZCONANA),ctod("  /  /  "))
        .oPgFrm.Page6.oPag.ZoomSedi.Calculate()
        .w_SEPERSON = NVL(.w_ZoomSedi.GetVar("SEPERSRE"),"")
        .w_SETIPRIF = .w_ZoomSedi.GetVar("SETIPRIF")
        .w_SEINDIRI = .w_ZoomSedi.GetVar("SEINDIRI")
        .w_SE___CAP = .w_ZoomSedi.GetVar("SE___CAP")
        .w_SELOCALI = .w_ZoomSedi.GetVar("SELOCALI")
        .w_SEPROVIN = .w_ZoomSedi.GetVar("SEPROVIN")
        .w_SETELEFO = .w_ZoomSedi.GetVar("SETELEFO")
        .w_SE_EMAIL = .w_ZoomSedi.GetVar("SE_EMAIL")
        .w_SE_EMPEC = .w_ZoomSedi.GetVar("SE_EMPEC")
        .w_SEPREMAP = .w_ZoomSedi.GetVar('SEPREMAP')
        .w_DPCOGNOM = NVL(.w_ZoomSedi.GetVar("DPCOGNOM"),"")
        .w_DPNOME = NVL(.w_ZoomSedi.GetVar("DPNOME"),"")
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .oPgFrm.Page4.oPag.oObj_4_65.Calculate()
        .w_CCCAP = IIF( Len( Alltrim( .w_AZCAPAZI ) )  = 5  ,  Left( .w_AZCAPAZI , 5 ) , '' )
        .w_AZSCONET = NVL(AZSCONET,0)
        .w_AZCALUTD = NVL(AZCALUTD,space(1))
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(Ah_MsgFormat(IIF(IsAlt(),"Studio:","Azienda:")))
        .oPgFrm.Page2.oPag.oObj_2_54.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page3.oPag.oObj_3_18.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page4.oPag.oObj_4_67.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page6.oPag.oObj_6_25.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page7.oPag.oObj_7_6.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page8.oPag.oObj_8_4.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page9.oPag.oObj_9_16.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page4.oPag.oObj_4_68.Calculate(IIF(IsAlt(), "Numero decimali campi quantit� per inserimento prestazioni", "Numero decimali campi quantit� su vendite, ordini, magazzino e statistiche"))
        .w_AZCONCAS = NVL(AZCONCAS,space(1))
        .oPgFrm.Page5.oPag.oObj_5_43.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Registro cronologico","Libro giornale")))
        .oPgFrm.Page5.oPag.oObj_5_44.Calculate(IIF(.w_AZCONCAS='S', "Ultima data di stampa registro cronologico", "Ultima data di stampa libro giornale"))
        .oPgFrm.Page5.oPag.oObj_5_45.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Ultima stampa registro cronologico:","Ultima stampa libro giornale:")))
        .oPgFrm.Page5.oPag.oObj_5_46.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Numero pagina registro cronologico:","Numero pagina libro giornale:")))
        .oPgFrm.Page5.oPag.oObj_5_47.Calculate(IIF(.w_AZCONCAS='S', "Progressivo pagine registro cronologico", "Progressivo pagine libro giornale"))
        .oPgFrm.Page5.oPag.oObj_5_48.Calculate(IIF(.w_AZCONCAS='S', "Prefisso numerazione pagine registro cronologico", "Prefisso numerazione pagine libro giornale"))
        .w_SEDESPRE = .w_ZoomSedi.GetVar('SEDESPRE')
        .w_AZFLRAGG = NVL(AZFLRAGG,space(1))
        .w_AZAITIPE = NVL(AZAITIPE,space(1))
        .w_AZAITIPV = NVL(AZAITIPV,space(1))
        .w_AZINPECE = NVL(AZINPECE,0)
        .w_AZAICFPI = NVL(AZAICFPI,space(16))
        .w_AZAIRAGS = NVL(AZAIRAGS,space(40))
        .w_AZAIPREF = NVL(AZAIPREF,space(5))
        .w_AZAITELE = NVL(AZAITELE,space(18))
        .w_AZAIINDI = NVL(AZAIINDI,space(35))
        .w_AZAI_CAP = NVL(AZAI_CAP,space(8))
        .w_AZAICITT = NVL(AZAICITT,space(30))
        .w_AZAIPROV = NVL(AZAIPROV,space(2))
        .w_AZINPEAC = NVL(AZINPEAC,0)
        .w_AZCONCVE = NVL(AZCONCVE,space(1))
        .w_AZCONCAC = NVL(AZCONCAC,space(1))
        cp_LoadRecExtFlds(this,'AZIENDA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page6.oPag.oBtn_6_24.enabled = this.oPgFrm.Page6.oPag.oBtn_6_24.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_21.enabled = this.oPgFrm.Page3.oPag.oBtn_3_21.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_azi
    *** sbianco il campo blank della localizzazione
    RemoveBlankValue(this.getctrl("w_AZTIPLOC"))
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_AZCODAZI = space(5)
      .w_AZRAGAZI = space(80)
      .w_AZPERAZI = space(1)
      .w_COAZI = space(5)
      .w_AZINDAZI = space(35)
      .w_AZCODOLT = 0
      .w_AZCODCAT = space(4)
      .w_AZCAPAZI = space(8)
      .w_AZLOCAZI = space(30)
      .w_AZPROAZI = space(2)
      .w_AZCODNAZ = space(3)
      .w_DESNAZ = space(35)
      .w_CODISO = space(3)
      .w_AZCODLIN = space(3)
      .w_DESLIN = space(30)
      .w_AZCODINE = space(9)
      .w_AZCOFAZI = space(20)
      .w_AZPIVAZI = space(20)
      .w_AZTELEFO = space(18)
      .w_AZTELFAX = space(18)
      .w_AZ_EMAIL = space(254)
      .w_AZINDWEB = space(254)
      .w_AZCAPSOC = space(30)
      .w_AZNAGAZI = space(5)
      .w_AZRECREA = space(20)
      .w_AZTRIAZI = space(25)
      .w_AZESAAZI = space(25)
      .w_AZCCFISC = space(20)
      .w_AZCONTRI = space(20)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_COAZI = space(5)
      .w_DEAZI = space(80)
      .w_AZVALEUR = space(3)
      .w_DESEUR = space(35)
      .w_AZVALLIR = space(3)
      .w_DESLIR = space(35)
      .w_AZUNIUTE = space(1)
      .w_AZCFNUME = space(1)
      .w_AZTRAEXP = space(1)
      .w_AZTRAEXP = space(1)
      .w_AZTIPLOC = space(3)
      .w_AZDATTRA = ctod("  /  /  ")
      .w_AZQUADRA = space(1)
      .w_AZFLUNIV = space(1)
      .w_AZFLARDO = space(1)
      .w_AZPERPAR = space(1)
      .w_AZPERSTS = space(1)
      .w_AZCCRICA = space(1)
      .w_AZGESCOM = space(1)
      .w_AZRIPCOM = space(1)
      .w_AZFLRIPC = space(1)
      .w_AZDETCON = space(1)
      .w_AZFLVEBD = space(1)
      .w_AZFLFIDO = space(1)
      .w_AZGIORIS = 0
      .w_AZFILPAR = space(1)
      .w_AZMAXLIV = 0
      .w_AZMASCON = space(15)
      .w_AZMASCLI = space(15)
      .w_AZMASFOR = space(15)
      .w_AZMASMCO = space(15)
      .w_AZMASCCC = space(15)
      .w_AZMASMCC = space(15)
      .w_AZINTMIN = 0
      .w_AZSCAPNT = space(4)
      .w_COAZI = space(5)
      .w_DEAZI = space(80)
      .w_AZDATINI = ctod("  /  /  ")
      .w_AZDATSER = ctod("  /  /  ")
      .w_AZNPRGEL = 0
      .w_AZPRESOB = space(5)
      .w_AZTELSOB = space(18)
      .w_AZCODPOR = space(10)
      .w_AZDTOBBL = space(1)
      .w_COAZI = space(5)
      .w_DEAZI = space(80)
      .w_AZMAGAZI = space(5)
      .w_DESMAG = space(30)
      .w_AZSCAMAG = ctod("  /  /  ")
      .w_AZMAGUTE = space(1)
      .w_AZPERAGE = space(1)
      .w_AZPERART = space(1)
      .w_AZGESCON = space(1)
      .w_AZPERDIS = space(1)
      .w_AZPERSDA = space(1)
      .w_AZPERORN = space(1)
      .w_AZGESLOT = space(1)
      .w_AZGESUBI = space(1)
      .w_AZGESMAT = space(1)
      .w_AZFLCAMB = space(1)
      .w_AZRATEVA = space(1)
      .w_AZFLSCOM = space(1)
      .w_AZDTSCOM = ctod("  /  /  ")
      .w_AZSCOPIE = space(1)
      .w_AZDTSCPI = ctod("  /  /  ")
      .w_AZFLCESC = space(1)
      .w_AZXCONDI = space(1)
      .w_AZDATDOC = space(1)
      .w_AZCALNET = 0
      .w_AZPERPQT = 0
      .w_AZPERPQD = 0
      .w_AZNUMSCO = 0
      .w_AZCODEXT = space(1)
      .w_AZPREEAN = space(2)
      .w_AZDATMAT = ctod("  /  /  ")
      .w_AZUNIMAT = space(1)
      .w_AZSCAMAT = space(1)
      .w_AZCODPRO = space(7)
      .w_AZBARPRO = space(5)
      .w_AZMASART = space(20)
      .w_AZMASLOT = space(20)
      .w_AZMASUBI = space(20)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_AZPERMUC = space(10)
      .w_COAZI = space(5)
      .w_DEAZI = space(80)
      .w_COAZI = space(5)
      .w_DEAZI = space(80)
      .w_AZSTALIG = ctod("  /  /  ")
      .w_AZINTLIG = space(1)
      .w_AZINTLIG = space(1)
      .w_AZPRPALG = 0
      .w_AZPREFLG = space(20)
      .w_AZINTLIN = space(1)
      .w_AZPRPALI = 0
      .w_AZESLBIN = space(4)
      .w_AZPREFLI = space(20)
      .w_AZGMGCON = space(1)
      .w_AZGMGFOR = space(1)
      .w_AZGMGPER = space(1)
      .w_AZSTARBE = ctod("  /  /  ")
      .w_AZINTRBE = space(1)
      .w_AZPRPRBE = 0
      .w_AZPRERBE = space(20)
      .w_AZDATBLO = ctod("  /  /  ")
      .w_AZBLOGCL = space(1)
      .w_COAZI = space(5)
      .w_DEAZI = space(80)
      .w_AZMASMAT = space(20)
      .w_AZCONCON = ctod("  /  /  ")
      .w_AZCONVEN = ctod("  /  /  ")
      .w_AZCONACQ = ctod("  /  /  ")
      .w_AZCONMAG = ctod("  /  /  ")
      .w_AZCONANA = ctod("  /  /  ")
      .w_SEPERSON = space(5)
      .w_SETIPRIF = space(2)
      .w_SEINDIRI = space(35)
      .w_SE___CAP = space(8)
      .w_SELOCALI = space(30)
      .w_SEPROVIN = space(2)
      .w_SETELEFO = space(18)
      .w_SE_EMAIL = space(50)
      .w_SE_EMPEC = space(50)
      .w_SEPREMAP = space(1)
      .w_DPCOGNOM = space(40)
      .w_DPNOME = space(40)
      .w_COAZI = space(5)
      .w_DEAZI = space(80)
      .w_COAZI = space(5)
      .w_DEAZI = space(80)
      .w_CCCAP = space(5)
      .w_AZSCONET = 0
      .w_DESCRI = space(40)
      .w_AZCALUTD = space(1)
      .w_AZCONCAS = space(1)
      .w_SEDESPRE = space(1)
      .w_AZFLRAGG = space(1)
      .w_AZAITIPE = space(1)
      .w_AZAITIPV = space(1)
      .w_AZINPECE = 0
      .w_AZAICFPI = space(16)
      .w_AZAIRAGS = space(40)
      .w_AZAIPREF = space(5)
      .w_AZAITELE = space(18)
      .w_AZAIINDI = space(35)
      .w_AZAI_CAP = space(8)
      .w_AZAICITT = space(30)
      .w_AZAIPROV = space(2)
      .w_AZINPEAC = 0
      .w_AZCONCVE = space(1)
      .w_AZCONCAC = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_AZPERAZI = 'N'
        .w_COAZI = .w_AZCODAZI
        .DoRTCalc(5,7,.f.)
          if not(empty(.w_AZCODCAT))
          .link_1_7('Full')
          endif
        .DoRTCalc(8,11,.f.)
          if not(empty(.w_AZCODNAZ))
          .link_1_11('Full')
          endif
        .DoRTCalc(12,14,.f.)
          if not(empty(.w_AZCODLIN))
          .link_1_14('Full')
          endif
          .DoRTCalc(15,22,.f.)
        .w_AZCAPSOC = iif(.w_AZPERAZI='S','',.w_AZCAPSOC)
        .w_AZNAGAZI = iif(.w_AZPERAZI='S','',.w_AZNAGAZI)
          .DoRTCalc(25,33,.f.)
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .DoRTCalc(36,36,.f.)
          if not(empty(.w_AZVALEUR))
          .link_2_3('Full')
          endif
        .DoRTCalc(37,38,.f.)
          if not(empty(.w_AZVALLIR))
          .link_2_5('Full')
          endif
          .DoRTCalc(39,39,.f.)
        .w_AZUNIUTE = 'S'
        .w_AZCFNUME = iif(.w_AZTRAEXP='G','S',' ')
        .w_AZTRAEXP = 'N'
        .w_AZTRAEXP = 'N'
        .w_AZTIPLOC = 'ITA'
        .w_AZDATTRA = iif(inlist(.w_AZTRAEXP,'A','C','G'),i_DATSYS,i_FINDAT)
        .w_AZQUADRA = 'S'
          .DoRTCalc(47,48,.f.)
        .w_AZPERPAR = 'S'
        .w_AZPERSTS = 'S'
        .w_AZCCRICA = ' '
        .w_AZGESCOM = ' '
        .w_AZRIPCOM = ' '
        .w_AZFLRIPC = ' '
        .w_AZDETCON = IIF(.w_AZTRAEXP $ 'SACBG', 'S', ' ')
          .DoRTCalc(56,59,.f.)
        .w_AZMAXLIV = 3
          .DoRTCalc(61,61,.f.)
        .w_AZMASCLI = .w_AZMASCON
        .w_AZMASFOR = .w_AZMASCON
        .DoRTCalc(64,68,.f.)
          if not(empty(.w_AZSCAPNT))
          .link_2_36('Full')
          endif
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .DoRTCalc(71,76,.f.)
          if not(empty(.w_AZCODPOR))
          .link_3_9('Full')
          endif
        .w_AZDTOBBL = 'N'
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .DoRTCalc(80,80,.f.)
          if not(empty(.w_AZMAGAZI))
          .link_4_4('Full')
          endif
          .DoRTCalc(81,82,.f.)
        .w_AZMAGUTE = 'S'
        .w_AZPERAGE = 'S'
        .w_AZPERART = 'S'
          .DoRTCalc(86,86,.f.)
        .w_AZPERDIS = IIF(.w_AZGESMAT='S','S',' ')
        .w_AZPERSDA = 'S'
          .DoRTCalc(89,95,.f.)
        .w_AZDTSCOM = IIF(.w_AZFLSCOM='S', i_datsys, cp_CharToDate('  -  -  '))
        .w_AZSCOPIE = 'N'
        .w_AZDTSCPI = IIF(.w_AZSCOPIE='S', i_datsys, cp_CharToDate('  -  -  '))
          .DoRTCalc(99,100,.f.)
        .w_AZDATDOC = 'N'
        .w_AZCALNET = 1
          .DoRTCalc(103,104,.f.)
        .w_AZNUMSCO = 2
          .DoRTCalc(106,108,.f.)
        .w_AZUNIMAT = 'A'
          .DoRTCalc(110,115,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(117,118,.f.)
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
          .DoRTCalc(123,126,.f.)
        .w_AZPREFLG = IIF(.w_AZINTLIG<>'S',SPACE(20),.w_AZPREFLG)
        .DoRTCalc(128,130,.f.)
          if not(empty(.w_AZESLBIN))
          .link_5_8('Full')
          endif
        .w_AZPREFLI = IIF(.w_AZINTLIN<>'S',SPACE(20),.w_AZPREFLI)
        .w_AZGMGCON = 'B'
        .w_AZGMGFOR = 'R'
        .w_AZGMGPER = 'M'
          .DoRTCalc(135,136,.f.)
        .w_AZPRPRBE = 0
        .w_AZPRERBE = IIF(.w_AZINTRBE<>'S',SPACE(20),.w_AZPRERBE)
          .DoRTCalc(139,140,.f.)
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .oPgFrm.Page4.oPag.oObj_4_57.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_58.Calculate()
        .oPgFrm.Page6.oPag.ZoomSedi.Calculate()
          .DoRTCalc(143,148,.f.)
        .w_SEPERSON = NVL(.w_ZoomSedi.GetVar("SEPERSRE"),"")
        .w_SETIPRIF = .w_ZoomSedi.GetVar("SETIPRIF")
        .w_SEINDIRI = .w_ZoomSedi.GetVar("SEINDIRI")
        .w_SE___CAP = .w_ZoomSedi.GetVar("SE___CAP")
        .w_SELOCALI = .w_ZoomSedi.GetVar("SELOCALI")
        .w_SEPROVIN = .w_ZoomSedi.GetVar("SEPROVIN")
        .w_SETELEFO = .w_ZoomSedi.GetVar("SETELEFO")
        .w_SE_EMAIL = .w_ZoomSedi.GetVar("SE_EMAIL")
        .w_SE_EMPEC = .w_ZoomSedi.GetVar("SE_EMPEC")
        .w_SEPREMAP = .w_ZoomSedi.GetVar('SEPREMAP')
        .w_DPCOGNOM = NVL(.w_ZoomSedi.GetVar("DPCOGNOM"),"")
        .w_DPNOME = NVL(.w_ZoomSedi.GetVar("DPNOME"),"")
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .w_COAZI = .w_AZCODAZI
        .w_DEAZI = .w_AZRAGAZI
        .oPgFrm.Page4.oPag.oObj_4_65.Calculate()
        .w_CCCAP = IIF( Len( Alltrim( .w_AZCAPAZI ) )  = 5  ,  Left( .w_AZCAPAZI , 5 ) , '' )
        .w_AZSCONET = 1
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(Ah_MsgFormat(IIF(IsAlt(),"Studio:","Azienda:")))
        .oPgFrm.Page2.oPag.oObj_2_54.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page3.oPag.oObj_3_18.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page4.oPag.oObj_4_67.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page6.oPag.oObj_6_25.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page7.oPag.oObj_7_6.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page8.oPag.oObj_8_4.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page9.oPag.oObj_9_16.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page4.oPag.oObj_4_68.Calculate(IIF(IsAlt(), "Numero decimali campi quantit� per inserimento prestazioni", "Numero decimali campi quantit� su vendite, ordini, magazzino e statistiche"))
          .DoRTCalc(167,168,.f.)
        .w_AZCONCAS = ' '
        .oPgFrm.Page5.oPag.oObj_5_43.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Registro cronologico","Libro giornale")))
        .oPgFrm.Page5.oPag.oObj_5_44.Calculate(IIF(.w_AZCONCAS='S', "Ultima data di stampa registro cronologico", "Ultima data di stampa libro giornale"))
        .oPgFrm.Page5.oPag.oObj_5_45.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Ultima stampa registro cronologico:","Ultima stampa libro giornale:")))
        .oPgFrm.Page5.oPag.oObj_5_46.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Numero pagina registro cronologico:","Numero pagina libro giornale:")))
        .oPgFrm.Page5.oPag.oObj_5_47.Calculate(IIF(.w_AZCONCAS='S', "Progressivo pagine registro cronologico", "Progressivo pagine libro giornale"))
        .oPgFrm.Page5.oPag.oObj_5_48.Calculate(IIF(.w_AZCONCAS='S', "Prefisso numerazione pagine registro cronologico", "Prefisso numerazione pagine libro giornale"))
        .w_SEDESPRE = .w_ZoomSedi.GetVar('SEDESPRE')
          .DoRTCalc(171,183,.f.)
        .w_AZCONCVE = 'U'
        .w_AZCONCAC = 'U'
      endif
    endwith
    cp_BlankRecExtFlds(this,'AZIENDA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page6.oPag.oBtn_6_24.enabled = this.oPgFrm.Page6.oPag.oBtn_6_24.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_21.enabled = this.oPgFrm.Page3.oPag.oBtn_3_21.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oAZRAGAZI_1_2.enabled = i_bVal
      .Page1.oPag.oAZPERAZI_1_3.enabled = i_bVal
      .Page1.oPag.oAZINDAZI_1_5.enabled = i_bVal
      .Page1.oPag.oAZCODOLT_1_6.enabled = i_bVal
      .Page1.oPag.oAZCODCAT_1_7.enabled = i_bVal
      .Page1.oPag.oAZCAPAZI_1_8.enabled = i_bVal
      .Page1.oPag.oAZLOCAZI_1_9.enabled = i_bVal
      .Page1.oPag.oAZPROAZI_1_10.enabled = i_bVal
      .Page1.oPag.oAZCODNAZ_1_11.enabled = i_bVal
      .Page1.oPag.oAZCODLIN_1_14.enabled = i_bVal
      .Page1.oPag.oAZCODINE_1_16.enabled = i_bVal
      .Page1.oPag.oAZCOFAZI_1_17.enabled = i_bVal
      .Page1.oPag.oAZPIVAZI_1_18.enabled = i_bVal
      .Page1.oPag.oAZTELEFO_1_19.enabled = i_bVal
      .Page1.oPag.oAZTELFAX_1_20.enabled = i_bVal
      .Page1.oPag.oAZ_EMAIL_1_21.enabled = i_bVal
      .Page1.oPag.oAZINDWEB_1_22.enabled = i_bVal
      .Page1.oPag.oAZCAPSOC_1_23.enabled = i_bVal
      .Page1.oPag.oAZNAGAZI_1_24.enabled = i_bVal
      .Page1.oPag.oAZRECREA_1_25.enabled = i_bVal
      .Page1.oPag.oAZTRIAZI_1_26.enabled = i_bVal
      .Page1.oPag.oAZESAAZI_1_27.enabled = i_bVal
      .Page1.oPag.oAZCCFISC_1_28.enabled = i_bVal
      .Page1.oPag.oAZCONTRI_1_29.enabled = i_bVal
      .Page2.oPag.oAZVALEUR_2_3.enabled = i_bVal
      .Page2.oPag.oAZVALLIR_2_5.enabled = i_bVal
      .Page2.oPag.oAZUNIUTE_2_7.enabled = i_bVal
      .Page2.oPag.oAZCFNUME_2_8.enabled = i_bVal
      .Page2.oPag.oAZTRAEXP_2_9.enabled = i_bVal
      .Page2.oPag.oAZTRAEXP_2_10.enabled = i_bVal
      .Page2.oPag.oAZTIPLOC_2_11.enabled = i_bVal
      .Page2.oPag.oAZDATTRA_2_12.enabled = i_bVal
      .Page2.oPag.oAZQUADRA_2_13.enabled = i_bVal
      .Page2.oPag.oAZFLUNIV_2_14.enabled = i_bVal
      .Page2.oPag.oAZFLARDO_2_15.enabled = i_bVal
      .Page2.oPag.oAZPERPAR_2_16.enabled = i_bVal
      .Page2.oPag.oAZCCRICA_2_18.enabled = i_bVal
      .Page2.oPag.oAZGESCOM_2_19.enabled = i_bVal
      .Page2.oPag.oAZRIPCOM_2_20.enabled = i_bVal
      .Page2.oPag.oAZFLRIPC_2_21.enabled = i_bVal
      .Page2.oPag.oAZDETCON_2_22.enabled = i_bVal
      .Page2.oPag.oAZFLVEBD_2_23.enabled = i_bVal
      .Page2.oPag.oAZFLFIDO_2_24.enabled = i_bVal
      .Page2.oPag.oAZGIORIS_2_25.enabled = i_bVal
      .Page2.oPag.oAZFILPAR_2_26.enabled = i_bVal
      .Page2.oPag.oAZMAXLIV_2_27.enabled = i_bVal
      .Page2.oPag.oAZMASCON_2_28.enabled = i_bVal
      .Page2.oPag.oAZMASMCO_2_31.enabled = i_bVal
      .Page2.oPag.oAZMASCCC_2_32.enabled = i_bVal
      .Page2.oPag.oAZMASMCC_2_33.enabled = i_bVal
      .Page2.oPag.oAZINTMIN_2_35.enabled = i_bVal
      .Page2.oPag.oAZSCAPNT_2_36.enabled = i_bVal
      .Page3.oPag.oAZDATINI_3_4.enabled = i_bVal
      .Page3.oPag.oAZDATSER_3_5.enabled = i_bVal
      .Page3.oPag.oAZNPRGEL_3_6.enabled = i_bVal
      .Page3.oPag.oAZPRESOB_3_7.enabled = i_bVal
      .Page3.oPag.oAZTELSOB_3_8.enabled = i_bVal
      .Page3.oPag.oAZCODPOR_3_9.enabled = i_bVal
      .Page3.oPag.oAZDTOBBL_3_10.enabled = i_bVal
      .Page4.oPag.oAZMAGAZI_4_4.enabled = i_bVal
      .Page4.oPag.oAZSCAMAG_4_6.enabled = i_bVal
      .Page4.oPag.oAZMAGUTE_4_7.enabled = i_bVal
      .Page4.oPag.oAZPERAGE_4_8.enabled = i_bVal
      .Page4.oPag.oAZPERART_4_9.enabled = i_bVal
      .Page4.oPag.oAZGESCON_4_10.enabled = i_bVal
      .Page4.oPag.oAZPERDIS_4_11.enabled = i_bVal
      .Page4.oPag.oAZPERSDA_4_12.enabled = i_bVal
      .Page4.oPag.oAZPERORN_4_13.enabled = i_bVal
      .Page4.oPag.oAZGESLOT_4_14.enabled = i_bVal
      .Page4.oPag.oAZGESUBI_4_15.enabled = i_bVal
      .Page4.oPag.oAZGESMAT_4_16.enabled = i_bVal
      .Page4.oPag.oAZFLCAMB_4_17.enabled = i_bVal
      .Page4.oPag.oAZRATEVA_4_18.enabled = i_bVal
      .Page4.oPag.oAZFLSCOM_4_19.enabled = i_bVal
      .Page4.oPag.oAZDTSCOM_4_21.enabled = i_bVal
      .Page4.oPag.oAZSCOPIE_4_22.enabled = i_bVal
      .Page4.oPag.oAZDTSCPI_4_23.enabled = i_bVal
      .Page4.oPag.oAZFLCESC_4_24.enabled = i_bVal
      .Page4.oPag.oAZXCONDI_4_25.enabled = i_bVal
      .Page4.oPag.oAZDATDOC_4_26.enabled = i_bVal
      .Page4.oPag.oAZCALNET_4_27.enabled = i_bVal
      .Page4.oPag.oAZPERPQT_4_28.enabled = i_bVal
      .Page4.oPag.oAZPERPQD_4_29.enabled = i_bVal
      .Page4.oPag.oAZNUMSCO_4_30.enabled = i_bVal
      .Page4.oPag.oAZCODEXT_4_31.enabled = i_bVal
      .Page4.oPag.oAZPREEAN_4_32.enabled = i_bVal
      .Page4.oPag.oAZDATMAT_4_33.enabled = i_bVal
      .Page4.oPag.oAZUNIMAT_4_34.enabled = i_bVal
      .Page4.oPag.oAZSCAMAT_4_35.enabled = i_bVal
      .Page4.oPag.oAZCODPRO_4_36.enabled = i_bVal
      .Page4.oPag.oAZMASART_4_38.enabled = i_bVal
      .Page4.oPag.oAZMASLOT_4_39.enabled = i_bVal
      .Page4.oPag.oAZMASUBI_4_40.enabled = i_bVal
      .Page4.oPag.oAZPERMUC_4_53.enabled = i_bVal
      .Page5.oPag.oAZSTALIG_5_1.enabled = i_bVal
      .Page5.oPag.oAZINTLIG_5_2.enabled = i_bVal
      .Page5.oPag.oAZINTLIG_5_3.enabled = i_bVal
      .Page5.oPag.oAZPRPALG_5_4.enabled = i_bVal
      .Page5.oPag.oAZPREFLG_5_5.enabled = i_bVal
      .Page5.oPag.oAZINTLIN_5_6.enabled = i_bVal
      .Page5.oPag.oAZPRPALI_5_7.enabled = i_bVal
      .Page5.oPag.oAZESLBIN_5_8.enabled = i_bVal
      .Page5.oPag.oAZPREFLI_5_9.enabled = i_bVal
      .Page5.oPag.oAZGMGCON_5_10.enabled = i_bVal
      .Page5.oPag.oAZGMGFOR_5_11.enabled = i_bVal
      .Page5.oPag.oAZGMGPER_5_12.enabled = i_bVal
      .Page5.oPag.oAZSTARBE_5_13.enabled = i_bVal
      .Page5.oPag.oAZINTRBE_5_14.enabled = i_bVal
      .Page5.oPag.oAZPRPRBE_5_15.enabled = i_bVal
      .Page5.oPag.oAZPRERBE_5_16.enabled = i_bVal
      .Page5.oPag.oAZDATBLO_5_22.enabled = i_bVal
      .Page5.oPag.oAZBLOGCL_5_23.enabled = i_bVal
      .Page9.oPag.oAZCONCON_9_8.enabled = i_bVal
      .Page9.oPag.oAZCONVEN_9_9.enabled = i_bVal
      .Page9.oPag.oAZCONACQ_9_10.enabled = i_bVal
      .Page9.oPag.oAZCONMAG_9_11.enabled = i_bVal
      .Page9.oPag.oAZCONANA_9_12.enabled = i_bVal
      .Page4.oPag.oAZSCONET_4_66.enabled = i_bVal
      .Page1.oPag.oAZCALUTD_1_60.enabled = i_bVal
      .Page2.oPag.oAZCONCAS_2_55.enabled = i_bVal
      .Page2.oPag.oAZFLRAGG_2_58.enabled = i_bVal
      .Page2.oPag.oAZCONCVE_2_59.enabled = i_bVal
      .Page2.oPag.oAZCONCAC_2_61.enabled = i_bVal
      .Page6.oPag.oBtn_6_24.enabled = .Page6.oPag.oBtn_6_24.mCond()
      .Page3.oPag.oBtn_3_21.enabled = .Page3.oPag.oBtn_3_21.mCond()
      .Page1.oPag.oObj_1_61.enabled = i_bVal
      .Page2.oPag.oObj_2_54.enabled = i_bVal
      .Page3.oPag.oObj_3_18.enabled = i_bVal
      .Page4.oPag.oObj_4_67.enabled = i_bVal
      .Page5.oPag.oObj_5_42.enabled = i_bVal
      .Page6.oPag.oObj_6_25.enabled = i_bVal
      .Page7.oPag.oObj_7_6.enabled = i_bVal
      .Page8.oPag.oObj_8_4.enabled = i_bVal
      .Page9.oPag.oObj_9_16.enabled = i_bVal
      .Page5.oPag.oObj_5_43.enabled = i_bVal
      .Page5.oPag.oObj_5_45.enabled = i_bVal
      .Page5.oPag.oObj_5_46.enabled = i_bVal
    endwith
    this.GSAR_MTT.SetStatus(i_cOp)
    this.GSAR_ADR.SetStatus(i_cOp)
    this.gsar_abk.SetStatus(i_cOp)
    this.GSAR_AIT.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'AZIENDA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MTT.SetChildrenStatus(i_cOp)
  *  this.GSAR_ADR.SetChildrenStatus(i_cOp)
  *  this.gsar_abk.SetChildrenStatus(i_cOp)
  *  this.GSAR_AIT.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODAZI,"AZCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZRAGAZI,"AZRAGAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERAZI,"AZPERAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINDAZI,"AZINDAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODOLT,"AZCODOLT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODCAT,"AZCODCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCAPAZI,"AZCAPAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZLOCAZI,"AZLOCAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPROAZI,"AZPROAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODNAZ,"AZCODNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODLIN,"AZCODLIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODINE,"AZCODINE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCOFAZI,"AZCOFAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPIVAZI,"AZPIVAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTELEFO,"AZTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTELFAX,"AZTELFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZ_EMAIL,"AZ_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINDWEB,"AZINDWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCAPSOC,"AZCAPSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZNAGAZI,"AZNAGAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZRECREA,"AZRECREA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTRIAZI,"AZTRIAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZESAAZI,"AZESAAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCCFISC,"AZCCFISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONTRI,"AZCONTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZVALEUR,"AZVALEUR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZVALLIR,"AZVALLIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZUNIUTE,"AZUNIUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCFNUME,"AZCFNUME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTRAEXP,"AZTRAEXP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTRAEXP,"AZTRAEXP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTIPLOC,"AZTIPLOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDATTRA,"AZDATTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZQUADRA,"AZQUADRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFLUNIV,"AZFLUNIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFLARDO,"AZFLARDO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERPAR,"AZPERPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERSTS,"AZPERSTS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCCRICA,"AZCCRICA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGESCOM,"AZGESCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZRIPCOM,"AZRIPCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFLRIPC,"AZFLRIPC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDETCON,"AZDETCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFLVEBD,"AZFLVEBD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFLFIDO,"AZFLFIDO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGIORIS,"AZGIORIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFILPAR,"AZFILPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMAXLIV,"AZMAXLIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMASCON,"AZMASCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMASCLI,"AZMASCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMASFOR,"AZMASFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMASMCO,"AZMASMCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMASCCC,"AZMASCCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMASMCC,"AZMASMCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINTMIN,"AZINTMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZSCAPNT,"AZSCAPNT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDATINI,"AZDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDATSER,"AZDATSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZNPRGEL,"AZNPRGEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPRESOB,"AZPRESOB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTELSOB,"AZTELSOB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODPOR,"AZCODPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDTOBBL,"AZDTOBBL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMAGAZI,"AZMAGAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZSCAMAG,"AZSCAMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMAGUTE,"AZMAGUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERAGE,"AZPERAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERART,"AZPERART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGESCON,"AZGESCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERDIS,"AZPERDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERSDA,"AZPERSDA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERORN,"AZPERORN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGESLOT,"AZGESLOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGESUBI,"AZGESUBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGESMAT,"AZGESMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFLCAMB,"AZFLCAMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZRATEVA,"AZRATEVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFLSCOM,"AZFLSCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDTSCOM,"AZDTSCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZSCOPIE,"AZSCOPIE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDTSCPI,"AZDTSCPI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFLCESC,"AZFLCESC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZXCONDI,"AZXCONDI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDATDOC,"AZDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCALNET,"AZCALNET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERPQT,"AZPERPQT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERPQD,"AZPERPQD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZNUMSCO,"AZNUMSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODEXT,"AZCODEXT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPREEAN,"AZPREEAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDATMAT,"AZDATMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZUNIMAT,"AZUNIMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZSCAMAT,"AZSCAMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODPRO,"AZCODPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZBARPRO,"AZBARPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMASART,"AZMASART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMASLOT,"AZMASLOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMASUBI,"AZMASUBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPERMUC,"AZPERMUC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZSTALIG,"AZSTALIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINTLIG,"AZINTLIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINTLIG,"AZINTLIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPRPALG,"AZPRPALG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPREFLG,"AZPREFLG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINTLIN,"AZINTLIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPRPALI,"AZPRPALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZESLBIN,"AZESLBIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPREFLI,"AZPREFLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGMGCON,"AZGMGCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGMGFOR,"AZGMGFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGMGPER,"AZGMGPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZSTARBE,"AZSTARBE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINTRBE,"AZINTRBE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPRPRBE,"AZPRPRBE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPRERBE,"AZPRERBE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDATBLO,"AZDATBLO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZBLOGCL,"AZBLOGCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMASMAT,"AZMASMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONCON,"AZCONCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONVEN,"AZCONVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONACQ,"AZCONACQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONMAG,"AZCONMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONANA,"AZCONANA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZSCONET,"AZSCONET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCALUTD,"AZCALUTD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONCAS,"AZCONCAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFLRAGG,"AZFLRAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZAITIPE,"AZAITIPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZAITIPV,"AZAITIPV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINPECE,"AZINPECE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZAICFPI,"AZAICFPI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZAIRAGS,"AZAIRAGS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZAIPREF,"AZAIPREF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZAITELE,"AZAITELE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZAIINDI,"AZAIINDI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZAI_CAP,"AZAI_CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZAICITT,"AZAICITT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZAIPROV,"AZAIPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINPEAC,"AZINPEAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONCVE,"AZCONCVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONCAC,"AZCONCAC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.AZIENDA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into AZIENDA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AZIENDA')
        i_extval=cp_InsertValODBCExtFlds(this,'AZIENDA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AZCODAZI,AZRAGAZI,AZPERAZI,AZINDAZI,AZCODOLT"+;
                  ",AZCODCAT,AZCAPAZI,AZLOCAZI,AZPROAZI,AZCODNAZ"+;
                  ",AZCODLIN,AZCODINE,AZCOFAZI,AZPIVAZI,AZTELEFO"+;
                  ",AZTELFAX,AZ_EMAIL,AZINDWEB,AZCAPSOC,AZNAGAZI"+;
                  ",AZRECREA,AZTRIAZI,AZESAAZI,AZCCFISC,AZCONTRI"+;
                  ",UTCC,UTCV,UTDC,UTDV,AZVALEUR"+;
                  ",AZVALLIR,AZUNIUTE,AZCFNUME,AZTRAEXP,AZTIPLOC"+;
                  ",AZDATTRA,AZQUADRA,AZFLUNIV,AZFLARDO,AZPERPAR"+;
                  ",AZPERSTS,AZCCRICA,AZGESCOM,AZRIPCOM,AZFLRIPC"+;
                  ",AZDETCON,AZFLVEBD,AZFLFIDO,AZGIORIS,AZFILPAR"+;
                  ",AZMAXLIV,AZMASCON,AZMASCLI,AZMASFOR,AZMASMCO"+;
                  ",AZMASCCC,AZMASMCC,AZINTMIN,AZSCAPNT,AZDATINI"+;
                  ",AZDATSER,AZNPRGEL,AZPRESOB,AZTELSOB,AZCODPOR"+;
                  ",AZDTOBBL,AZMAGAZI,AZSCAMAG,AZMAGUTE,AZPERAGE"+;
                  ",AZPERART,AZGESCON,AZPERDIS,AZPERSDA,AZPERORN"+;
                  ",AZGESLOT,AZGESUBI,AZGESMAT,AZFLCAMB,AZRATEVA"+;
                  ",AZFLSCOM,AZDTSCOM,AZSCOPIE,AZDTSCPI,AZFLCESC"+;
                  ",AZXCONDI,AZDATDOC,AZCALNET,AZPERPQT,AZPERPQD"+;
                  ",AZNUMSCO,AZCODEXT,AZPREEAN,AZDATMAT,AZUNIMAT"+;
                  ",AZSCAMAT,AZCODPRO,AZBARPRO,AZMASART,AZMASLOT"+;
                  ",AZMASUBI,AZPERMUC,AZSTALIG,AZINTLIG,AZPRPALG"+;
                  ",AZPREFLG,AZINTLIN,AZPRPALI,AZESLBIN,AZPREFLI"+;
                  ",AZGMGCON,AZGMGFOR,AZGMGPER,AZSTARBE,AZINTRBE"+;
                  ",AZPRPRBE,AZPRERBE,AZDATBLO,AZBLOGCL,AZMASMAT"+;
                  ",AZCONCON,AZCONVEN,AZCONACQ,AZCONMAG,AZCONANA"+;
                  ",AZSCONET,AZCALUTD,AZCONCAS,AZFLRAGG,AZAITIPE"+;
                  ",AZAITIPV,AZINPECE,AZAICFPI,AZAIRAGS,AZAIPREF"+;
                  ",AZAITELE,AZAIINDI,AZAI_CAP,AZAICITT,AZAIPROV"+;
                  ",AZINPEAC,AZCONCVE,AZCONCAC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AZCODAZI)+;
                  ","+cp_ToStrODBC(this.w_AZRAGAZI)+;
                  ","+cp_ToStrODBC(this.w_AZPERAZI)+;
                  ","+cp_ToStrODBC(this.w_AZINDAZI)+;
                  ","+cp_ToStrODBC(this.w_AZCODOLT)+;
                  ","+cp_ToStrODBCNull(this.w_AZCODCAT)+;
                  ","+cp_ToStrODBC(this.w_AZCAPAZI)+;
                  ","+cp_ToStrODBC(this.w_AZLOCAZI)+;
                  ","+cp_ToStrODBC(this.w_AZPROAZI)+;
                  ","+cp_ToStrODBCNull(this.w_AZCODNAZ)+;
                  ","+cp_ToStrODBCNull(this.w_AZCODLIN)+;
                  ","+cp_ToStrODBC(this.w_AZCODINE)+;
                  ","+cp_ToStrODBC(this.w_AZCOFAZI)+;
                  ","+cp_ToStrODBC(this.w_AZPIVAZI)+;
                  ","+cp_ToStrODBC(this.w_AZTELEFO)+;
                  ","+cp_ToStrODBC(this.w_AZTELFAX)+;
                  ","+cp_ToStrODBC(this.w_AZ_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_AZINDWEB)+;
                  ","+cp_ToStrODBC(this.w_AZCAPSOC)+;
                  ","+cp_ToStrODBC(this.w_AZNAGAZI)+;
                  ","+cp_ToStrODBC(this.w_AZRECREA)+;
                  ","+cp_ToStrODBC(this.w_AZTRIAZI)+;
                  ","+cp_ToStrODBC(this.w_AZESAAZI)+;
                  ","+cp_ToStrODBC(this.w_AZCCFISC)+;
                  ","+cp_ToStrODBC(this.w_AZCONTRI)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_AZVALEUR)+;
                  ","+cp_ToStrODBCNull(this.w_AZVALLIR)+;
                  ","+cp_ToStrODBC(this.w_AZUNIUTE)+;
                  ","+cp_ToStrODBC(this.w_AZCFNUME)+;
                  ","+cp_ToStrODBC(this.w_AZTRAEXP)+;
                  ","+cp_ToStrODBC(this.w_AZTIPLOC)+;
                  ","+cp_ToStrODBC(this.w_AZDATTRA)+;
                  ","+cp_ToStrODBC(this.w_AZQUADRA)+;
                  ","+cp_ToStrODBC(this.w_AZFLUNIV)+;
                  ","+cp_ToStrODBC(this.w_AZFLARDO)+;
                  ","+cp_ToStrODBC(this.w_AZPERPAR)+;
                  ","+cp_ToStrODBC(this.w_AZPERSTS)+;
                  ","+cp_ToStrODBC(this.w_AZCCRICA)+;
                  ","+cp_ToStrODBC(this.w_AZGESCOM)+;
                  ","+cp_ToStrODBC(this.w_AZRIPCOM)+;
                  ","+cp_ToStrODBC(this.w_AZFLRIPC)+;
                  ","+cp_ToStrODBC(this.w_AZDETCON)+;
                  ","+cp_ToStrODBC(this.w_AZFLVEBD)+;
                  ","+cp_ToStrODBC(this.w_AZFLFIDO)+;
                  ","+cp_ToStrODBC(this.w_AZGIORIS)+;
                  ","+cp_ToStrODBC(this.w_AZFILPAR)+;
                  ","+cp_ToStrODBC(this.w_AZMAXLIV)+;
                  ","+cp_ToStrODBC(this.w_AZMASCON)+;
                  ","+cp_ToStrODBC(this.w_AZMASCLI)+;
                  ","+cp_ToStrODBC(this.w_AZMASFOR)+;
                  ","+cp_ToStrODBC(this.w_AZMASMCO)+;
                  ","+cp_ToStrODBC(this.w_AZMASCCC)+;
                  ","+cp_ToStrODBC(this.w_AZMASMCC)+;
                  ","+cp_ToStrODBC(this.w_AZINTMIN)+;
                  ","+cp_ToStrODBCNull(this.w_AZSCAPNT)+;
                  ","+cp_ToStrODBC(this.w_AZDATINI)+;
                  ","+cp_ToStrODBC(this.w_AZDATSER)+;
                  ","+cp_ToStrODBC(this.w_AZNPRGEL)+;
                  ","+cp_ToStrODBC(this.w_AZPRESOB)+;
                  ","+cp_ToStrODBC(this.w_AZTELSOB)+;
                  ","+cp_ToStrODBCNull(this.w_AZCODPOR)+;
                  ","+cp_ToStrODBC(this.w_AZDTOBBL)+;
                  ","+cp_ToStrODBCNull(this.w_AZMAGAZI)+;
                  ","+cp_ToStrODBC(this.w_AZSCAMAG)+;
                  ","+cp_ToStrODBC(this.w_AZMAGUTE)+;
                  ","+cp_ToStrODBC(this.w_AZPERAGE)+;
                  ","+cp_ToStrODBC(this.w_AZPERART)+;
                  ","+cp_ToStrODBC(this.w_AZGESCON)+;
                  ","+cp_ToStrODBC(this.w_AZPERDIS)+;
                  ","+cp_ToStrODBC(this.w_AZPERSDA)+;
                  ","+cp_ToStrODBC(this.w_AZPERORN)+;
                  ","+cp_ToStrODBC(this.w_AZGESLOT)+;
                  ","+cp_ToStrODBC(this.w_AZGESUBI)+;
                  ","+cp_ToStrODBC(this.w_AZGESMAT)+;
                  ","+cp_ToStrODBC(this.w_AZFLCAMB)+;
                  ","+cp_ToStrODBC(this.w_AZRATEVA)+;
                  ","+cp_ToStrODBC(this.w_AZFLSCOM)+;
                  ","+cp_ToStrODBC(this.w_AZDTSCOM)+;
                  ","+cp_ToStrODBC(this.w_AZSCOPIE)+;
                  ","+cp_ToStrODBC(this.w_AZDTSCPI)+;
                  ","+cp_ToStrODBC(this.w_AZFLCESC)+;
                  ","+cp_ToStrODBC(this.w_AZXCONDI)+;
                  ","+cp_ToStrODBC(this.w_AZDATDOC)+;
                  ","+cp_ToStrODBC(this.w_AZCALNET)+;
                  ","+cp_ToStrODBC(this.w_AZPERPQT)+;
                  ","+cp_ToStrODBC(this.w_AZPERPQD)+;
                  ","+cp_ToStrODBC(this.w_AZNUMSCO)+;
                  ","+cp_ToStrODBC(this.w_AZCODEXT)+;
                  ","+cp_ToStrODBC(this.w_AZPREEAN)+;
                  ","+cp_ToStrODBC(this.w_AZDATMAT)+;
                  ","+cp_ToStrODBC(this.w_AZUNIMAT)+;
                  ","+cp_ToStrODBC(this.w_AZSCAMAT)+;
                  ","+cp_ToStrODBC(this.w_AZCODPRO)+;
                  ","+cp_ToStrODBC(this.w_AZBARPRO)+;
                  ","+cp_ToStrODBC(this.w_AZMASART)+;
                  ","+cp_ToStrODBC(this.w_AZMASLOT)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_AZMASUBI)+;
                  ","+cp_ToStrODBC(this.w_AZPERMUC)+;
                  ","+cp_ToStrODBC(this.w_AZSTALIG)+;
                  ","+cp_ToStrODBC(this.w_AZINTLIG)+;
                  ","+cp_ToStrODBC(this.w_AZPRPALG)+;
                  ","+cp_ToStrODBC(this.w_AZPREFLG)+;
                  ","+cp_ToStrODBC(this.w_AZINTLIN)+;
                  ","+cp_ToStrODBC(this.w_AZPRPALI)+;
                  ","+cp_ToStrODBCNull(this.w_AZESLBIN)+;
                  ","+cp_ToStrODBC(this.w_AZPREFLI)+;
                  ","+cp_ToStrODBC(this.w_AZGMGCON)+;
                  ","+cp_ToStrODBC(this.w_AZGMGFOR)+;
                  ","+cp_ToStrODBC(this.w_AZGMGPER)+;
                  ","+cp_ToStrODBC(this.w_AZSTARBE)+;
                  ","+cp_ToStrODBC(this.w_AZINTRBE)+;
                  ","+cp_ToStrODBC(this.w_AZPRPRBE)+;
                  ","+cp_ToStrODBC(this.w_AZPRERBE)+;
                  ","+cp_ToStrODBC(this.w_AZDATBLO)+;
                  ","+cp_ToStrODBC(this.w_AZBLOGCL)+;
                  ","+cp_ToStrODBC(this.w_AZMASMAT)+;
                  ","+cp_ToStrODBC(this.w_AZCONCON)+;
                  ","+cp_ToStrODBC(this.w_AZCONVEN)+;
                  ","+cp_ToStrODBC(this.w_AZCONACQ)+;
                  ","+cp_ToStrODBC(this.w_AZCONMAG)+;
                  ","+cp_ToStrODBC(this.w_AZCONANA)+;
                  ","+cp_ToStrODBC(this.w_AZSCONET)+;
                  ","+cp_ToStrODBC(this.w_AZCALUTD)+;
                  ","+cp_ToStrODBC(this.w_AZCONCAS)+;
                  ","+cp_ToStrODBC(this.w_AZFLRAGG)+;
                  ","+cp_ToStrODBC(this.w_AZAITIPE)+;
                  ","+cp_ToStrODBC(this.w_AZAITIPV)+;
                  ","+cp_ToStrODBC(this.w_AZINPECE)+;
                  ","+cp_ToStrODBC(this.w_AZAICFPI)+;
                  ","+cp_ToStrODBC(this.w_AZAIRAGS)+;
                  ","+cp_ToStrODBC(this.w_AZAIPREF)+;
                  ","+cp_ToStrODBC(this.w_AZAITELE)+;
                  ","+cp_ToStrODBC(this.w_AZAIINDI)+;
                  ","+cp_ToStrODBC(this.w_AZAI_CAP)+;
                  ","+cp_ToStrODBC(this.w_AZAICITT)+;
                  ","+cp_ToStrODBC(this.w_AZAIPROV)+;
                  ","+cp_ToStrODBC(this.w_AZINPEAC)+;
                  ","+cp_ToStrODBC(this.w_AZCONCVE)+;
                  ","+cp_ToStrODBC(this.w_AZCONCAC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AZIENDA')
        i_extval=cp_InsertValVFPExtFlds(this,'AZIENDA')
        cp_CheckDeletedKey(i_cTable,0,'AZCODAZI',this.w_AZCODAZI)
        INSERT INTO (i_cTable);
              (AZCODAZI,AZRAGAZI,AZPERAZI,AZINDAZI,AZCODOLT,AZCODCAT,AZCAPAZI,AZLOCAZI,AZPROAZI,AZCODNAZ,AZCODLIN,AZCODINE,AZCOFAZI,AZPIVAZI,AZTELEFO,AZTELFAX,AZ_EMAIL,AZINDWEB,AZCAPSOC,AZNAGAZI,AZRECREA,AZTRIAZI,AZESAAZI,AZCCFISC,AZCONTRI,UTCC,UTCV,UTDC,UTDV,AZVALEUR,AZVALLIR,AZUNIUTE,AZCFNUME,AZTRAEXP,AZTIPLOC,AZDATTRA,AZQUADRA,AZFLUNIV,AZFLARDO,AZPERPAR,AZPERSTS,AZCCRICA,AZGESCOM,AZRIPCOM,AZFLRIPC,AZDETCON,AZFLVEBD,AZFLFIDO,AZGIORIS,AZFILPAR,AZMAXLIV,AZMASCON,AZMASCLI,AZMASFOR,AZMASMCO,AZMASCCC,AZMASMCC,AZINTMIN,AZSCAPNT,AZDATINI,AZDATSER,AZNPRGEL,AZPRESOB,AZTELSOB,AZCODPOR,AZDTOBBL,AZMAGAZI,AZSCAMAG,AZMAGUTE,AZPERAGE,AZPERART,AZGESCON,AZPERDIS,AZPERSDA,AZPERORN,AZGESLOT,AZGESUBI,AZGESMAT,AZFLCAMB,AZRATEVA,AZFLSCOM,AZDTSCOM,AZSCOPIE,AZDTSCPI,AZFLCESC,AZXCONDI,AZDATDOC,AZCALNET,AZPERPQT,AZPERPQD,AZNUMSCO,AZCODEXT,AZPREEAN,AZDATMAT,AZUNIMAT,AZSCAMAT,AZCODPRO,AZBARPRO,AZMASART,AZMASLOT,AZMASUBI,AZPERMUC,AZSTALIG,AZINTLIG,AZPRPALG,AZPREFLG,AZINTLIN,AZPRPALI,AZESLBIN,AZPREFLI,AZGMGCON,AZGMGFOR,AZGMGPER,AZSTARBE,AZINTRBE,AZPRPRBE,AZPRERBE,AZDATBLO,AZBLOGCL,AZMASMAT,AZCONCON,AZCONVEN,AZCONACQ,AZCONMAG,AZCONANA,AZSCONET,AZCALUTD,AZCONCAS,AZFLRAGG,AZAITIPE,AZAITIPV,AZINPECE,AZAICFPI,AZAIRAGS,AZAIPREF,AZAITELE,AZAIINDI,AZAI_CAP,AZAICITT,AZAIPROV,AZINPEAC,AZCONCVE,AZCONCAC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AZCODAZI;
                  ,this.w_AZRAGAZI;
                  ,this.w_AZPERAZI;
                  ,this.w_AZINDAZI;
                  ,this.w_AZCODOLT;
                  ,this.w_AZCODCAT;
                  ,this.w_AZCAPAZI;
                  ,this.w_AZLOCAZI;
                  ,this.w_AZPROAZI;
                  ,this.w_AZCODNAZ;
                  ,this.w_AZCODLIN;
                  ,this.w_AZCODINE;
                  ,this.w_AZCOFAZI;
                  ,this.w_AZPIVAZI;
                  ,this.w_AZTELEFO;
                  ,this.w_AZTELFAX;
                  ,this.w_AZ_EMAIL;
                  ,this.w_AZINDWEB;
                  ,this.w_AZCAPSOC;
                  ,this.w_AZNAGAZI;
                  ,this.w_AZRECREA;
                  ,this.w_AZTRIAZI;
                  ,this.w_AZESAAZI;
                  ,this.w_AZCCFISC;
                  ,this.w_AZCONTRI;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_AZVALEUR;
                  ,this.w_AZVALLIR;
                  ,this.w_AZUNIUTE;
                  ,this.w_AZCFNUME;
                  ,this.w_AZTRAEXP;
                  ,this.w_AZTIPLOC;
                  ,this.w_AZDATTRA;
                  ,this.w_AZQUADRA;
                  ,this.w_AZFLUNIV;
                  ,this.w_AZFLARDO;
                  ,this.w_AZPERPAR;
                  ,this.w_AZPERSTS;
                  ,this.w_AZCCRICA;
                  ,this.w_AZGESCOM;
                  ,this.w_AZRIPCOM;
                  ,this.w_AZFLRIPC;
                  ,this.w_AZDETCON;
                  ,this.w_AZFLVEBD;
                  ,this.w_AZFLFIDO;
                  ,this.w_AZGIORIS;
                  ,this.w_AZFILPAR;
                  ,this.w_AZMAXLIV;
                  ,this.w_AZMASCON;
                  ,this.w_AZMASCLI;
                  ,this.w_AZMASFOR;
                  ,this.w_AZMASMCO;
                  ,this.w_AZMASCCC;
                  ,this.w_AZMASMCC;
                  ,this.w_AZINTMIN;
                  ,this.w_AZSCAPNT;
                  ,this.w_AZDATINI;
                  ,this.w_AZDATSER;
                  ,this.w_AZNPRGEL;
                  ,this.w_AZPRESOB;
                  ,this.w_AZTELSOB;
                  ,this.w_AZCODPOR;
                  ,this.w_AZDTOBBL;
                  ,this.w_AZMAGAZI;
                  ,this.w_AZSCAMAG;
                  ,this.w_AZMAGUTE;
                  ,this.w_AZPERAGE;
                  ,this.w_AZPERART;
                  ,this.w_AZGESCON;
                  ,this.w_AZPERDIS;
                  ,this.w_AZPERSDA;
                  ,this.w_AZPERORN;
                  ,this.w_AZGESLOT;
                  ,this.w_AZGESUBI;
                  ,this.w_AZGESMAT;
                  ,this.w_AZFLCAMB;
                  ,this.w_AZRATEVA;
                  ,this.w_AZFLSCOM;
                  ,this.w_AZDTSCOM;
                  ,this.w_AZSCOPIE;
                  ,this.w_AZDTSCPI;
                  ,this.w_AZFLCESC;
                  ,this.w_AZXCONDI;
                  ,this.w_AZDATDOC;
                  ,this.w_AZCALNET;
                  ,this.w_AZPERPQT;
                  ,this.w_AZPERPQD;
                  ,this.w_AZNUMSCO;
                  ,this.w_AZCODEXT;
                  ,this.w_AZPREEAN;
                  ,this.w_AZDATMAT;
                  ,this.w_AZUNIMAT;
                  ,this.w_AZSCAMAT;
                  ,this.w_AZCODPRO;
                  ,this.w_AZBARPRO;
                  ,this.w_AZMASART;
                  ,this.w_AZMASLOT;
                  ,this.w_AZMASUBI;
                  ,this.w_AZPERMUC;
                  ,this.w_AZSTALIG;
                  ,this.w_AZINTLIG;
                  ,this.w_AZPRPALG;
                  ,this.w_AZPREFLG;
                  ,this.w_AZINTLIN;
                  ,this.w_AZPRPALI;
                  ,this.w_AZESLBIN;
                  ,this.w_AZPREFLI;
                  ,this.w_AZGMGCON;
                  ,this.w_AZGMGFOR;
                  ,this.w_AZGMGPER;
                  ,this.w_AZSTARBE;
                  ,this.w_AZINTRBE;
                  ,this.w_AZPRPRBE;
                  ,this.w_AZPRERBE;
                  ,this.w_AZDATBLO;
                  ,this.w_AZBLOGCL;
                  ,this.w_AZMASMAT;
                  ,this.w_AZCONCON;
                  ,this.w_AZCONVEN;
                  ,this.w_AZCONACQ;
                  ,this.w_AZCONMAG;
                  ,this.w_AZCONANA;
                  ,this.w_AZSCONET;
                  ,this.w_AZCALUTD;
                  ,this.w_AZCONCAS;
                  ,this.w_AZFLRAGG;
                  ,this.w_AZAITIPE;
                  ,this.w_AZAITIPV;
                  ,this.w_AZINPECE;
                  ,this.w_AZAICFPI;
                  ,this.w_AZAIRAGS;
                  ,this.w_AZAIPREF;
                  ,this.w_AZAITELE;
                  ,this.w_AZAIINDI;
                  ,this.w_AZAI_CAP;
                  ,this.w_AZAICITT;
                  ,this.w_AZAIPROV;
                  ,this.w_AZINPEAC;
                  ,this.w_AZCONCVE;
                  ,this.w_AZCONCAC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.AZIENDA_IDX,i_nConn)
      *
      * update AZIENDA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'AZIENDA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AZRAGAZI="+cp_ToStrODBC(this.w_AZRAGAZI)+;
             ",AZPERAZI="+cp_ToStrODBC(this.w_AZPERAZI)+;
             ",AZINDAZI="+cp_ToStrODBC(this.w_AZINDAZI)+;
             ",AZCODOLT="+cp_ToStrODBC(this.w_AZCODOLT)+;
             ",AZCODCAT="+cp_ToStrODBCNull(this.w_AZCODCAT)+;
             ",AZCAPAZI="+cp_ToStrODBC(this.w_AZCAPAZI)+;
             ",AZLOCAZI="+cp_ToStrODBC(this.w_AZLOCAZI)+;
             ",AZPROAZI="+cp_ToStrODBC(this.w_AZPROAZI)+;
             ",AZCODNAZ="+cp_ToStrODBCNull(this.w_AZCODNAZ)+;
             ",AZCODLIN="+cp_ToStrODBCNull(this.w_AZCODLIN)+;
             ",AZCODINE="+cp_ToStrODBC(this.w_AZCODINE)+;
             ",AZCOFAZI="+cp_ToStrODBC(this.w_AZCOFAZI)+;
             ",AZPIVAZI="+cp_ToStrODBC(this.w_AZPIVAZI)+;
             ",AZTELEFO="+cp_ToStrODBC(this.w_AZTELEFO)+;
             ",AZTELFAX="+cp_ToStrODBC(this.w_AZTELFAX)+;
             ",AZ_EMAIL="+cp_ToStrODBC(this.w_AZ_EMAIL)+;
             ",AZINDWEB="+cp_ToStrODBC(this.w_AZINDWEB)+;
             ",AZCAPSOC="+cp_ToStrODBC(this.w_AZCAPSOC)+;
             ",AZNAGAZI="+cp_ToStrODBC(this.w_AZNAGAZI)+;
             ",AZRECREA="+cp_ToStrODBC(this.w_AZRECREA)+;
             ",AZTRIAZI="+cp_ToStrODBC(this.w_AZTRIAZI)+;
             ",AZESAAZI="+cp_ToStrODBC(this.w_AZESAAZI)+;
             ",AZCCFISC="+cp_ToStrODBC(this.w_AZCCFISC)+;
             ",AZCONTRI="+cp_ToStrODBC(this.w_AZCONTRI)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",AZVALEUR="+cp_ToStrODBCNull(this.w_AZVALEUR)+;
             ",AZVALLIR="+cp_ToStrODBCNull(this.w_AZVALLIR)+;
             ",AZUNIUTE="+cp_ToStrODBC(this.w_AZUNIUTE)+;
             ",AZCFNUME="+cp_ToStrODBC(this.w_AZCFNUME)+;
             ",AZTRAEXP="+cp_ToStrODBC(this.w_AZTRAEXP)+;
             ",AZTIPLOC="+cp_ToStrODBC(this.w_AZTIPLOC)+;
             ",AZDATTRA="+cp_ToStrODBC(this.w_AZDATTRA)+;
             ",AZQUADRA="+cp_ToStrODBC(this.w_AZQUADRA)+;
             ",AZFLUNIV="+cp_ToStrODBC(this.w_AZFLUNIV)+;
             ",AZFLARDO="+cp_ToStrODBC(this.w_AZFLARDO)+;
             ",AZPERPAR="+cp_ToStrODBC(this.w_AZPERPAR)+;
             ",AZPERSTS="+cp_ToStrODBC(this.w_AZPERSTS)+;
             ",AZCCRICA="+cp_ToStrODBC(this.w_AZCCRICA)+;
             ",AZGESCOM="+cp_ToStrODBC(this.w_AZGESCOM)+;
             ",AZRIPCOM="+cp_ToStrODBC(this.w_AZRIPCOM)+;
             ",AZFLRIPC="+cp_ToStrODBC(this.w_AZFLRIPC)+;
             ",AZDETCON="+cp_ToStrODBC(this.w_AZDETCON)+;
             ",AZFLVEBD="+cp_ToStrODBC(this.w_AZFLVEBD)+;
             ",AZFLFIDO="+cp_ToStrODBC(this.w_AZFLFIDO)+;
             ",AZGIORIS="+cp_ToStrODBC(this.w_AZGIORIS)+;
             ",AZFILPAR="+cp_ToStrODBC(this.w_AZFILPAR)+;
             ",AZMAXLIV="+cp_ToStrODBC(this.w_AZMAXLIV)+;
             ",AZMASCON="+cp_ToStrODBC(this.w_AZMASCON)+;
             ",AZMASCLI="+cp_ToStrODBC(this.w_AZMASCLI)+;
             ",AZMASFOR="+cp_ToStrODBC(this.w_AZMASFOR)+;
             ",AZMASMCO="+cp_ToStrODBC(this.w_AZMASMCO)+;
             ",AZMASCCC="+cp_ToStrODBC(this.w_AZMASCCC)+;
             ",AZMASMCC="+cp_ToStrODBC(this.w_AZMASMCC)+;
             ",AZINTMIN="+cp_ToStrODBC(this.w_AZINTMIN)+;
             ",AZSCAPNT="+cp_ToStrODBCNull(this.w_AZSCAPNT)+;
             ",AZDATINI="+cp_ToStrODBC(this.w_AZDATINI)+;
             ",AZDATSER="+cp_ToStrODBC(this.w_AZDATSER)+;
             ",AZNPRGEL="+cp_ToStrODBC(this.w_AZNPRGEL)+;
             ",AZPRESOB="+cp_ToStrODBC(this.w_AZPRESOB)+;
             ",AZTELSOB="+cp_ToStrODBC(this.w_AZTELSOB)+;
             ",AZCODPOR="+cp_ToStrODBCNull(this.w_AZCODPOR)+;
             ",AZDTOBBL="+cp_ToStrODBC(this.w_AZDTOBBL)+;
             ",AZMAGAZI="+cp_ToStrODBCNull(this.w_AZMAGAZI)+;
             ",AZSCAMAG="+cp_ToStrODBC(this.w_AZSCAMAG)+;
             ",AZMAGUTE="+cp_ToStrODBC(this.w_AZMAGUTE)+;
             ",AZPERAGE="+cp_ToStrODBC(this.w_AZPERAGE)+;
             ",AZPERART="+cp_ToStrODBC(this.w_AZPERART)+;
             ",AZGESCON="+cp_ToStrODBC(this.w_AZGESCON)+;
             ",AZPERDIS="+cp_ToStrODBC(this.w_AZPERDIS)+;
             ",AZPERSDA="+cp_ToStrODBC(this.w_AZPERSDA)+;
             ",AZPERORN="+cp_ToStrODBC(this.w_AZPERORN)+;
             ",AZGESLOT="+cp_ToStrODBC(this.w_AZGESLOT)+;
             ",AZGESUBI="+cp_ToStrODBC(this.w_AZGESUBI)+;
             ",AZGESMAT="+cp_ToStrODBC(this.w_AZGESMAT)+;
             ",AZFLCAMB="+cp_ToStrODBC(this.w_AZFLCAMB)+;
             ",AZRATEVA="+cp_ToStrODBC(this.w_AZRATEVA)+;
             ",AZFLSCOM="+cp_ToStrODBC(this.w_AZFLSCOM)+;
             ",AZDTSCOM="+cp_ToStrODBC(this.w_AZDTSCOM)+;
             ",AZSCOPIE="+cp_ToStrODBC(this.w_AZSCOPIE)+;
             ",AZDTSCPI="+cp_ToStrODBC(this.w_AZDTSCPI)+;
             ",AZFLCESC="+cp_ToStrODBC(this.w_AZFLCESC)+;
             ",AZXCONDI="+cp_ToStrODBC(this.w_AZXCONDI)+;
             ",AZDATDOC="+cp_ToStrODBC(this.w_AZDATDOC)+;
             ",AZCALNET="+cp_ToStrODBC(this.w_AZCALNET)+;
             ",AZPERPQT="+cp_ToStrODBC(this.w_AZPERPQT)+;
             ",AZPERPQD="+cp_ToStrODBC(this.w_AZPERPQD)+;
             ",AZNUMSCO="+cp_ToStrODBC(this.w_AZNUMSCO)+;
             ",AZCODEXT="+cp_ToStrODBC(this.w_AZCODEXT)+;
             ",AZPREEAN="+cp_ToStrODBC(this.w_AZPREEAN)+;
             ",AZDATMAT="+cp_ToStrODBC(this.w_AZDATMAT)+;
             ",AZUNIMAT="+cp_ToStrODBC(this.w_AZUNIMAT)+;
             ",AZSCAMAT="+cp_ToStrODBC(this.w_AZSCAMAT)+;
             ",AZCODPRO="+cp_ToStrODBC(this.w_AZCODPRO)+;
             ",AZBARPRO="+cp_ToStrODBC(this.w_AZBARPRO)+;
             ",AZMASART="+cp_ToStrODBC(this.w_AZMASART)+;
             ",AZMASLOT="+cp_ToStrODBC(this.w_AZMASLOT)+;
             ",AZMASUBI="+cp_ToStrODBC(this.w_AZMASUBI)+;
             ""
             i_nnn=i_nnn+;
             ",AZPERMUC="+cp_ToStrODBC(this.w_AZPERMUC)+;
             ",AZSTALIG="+cp_ToStrODBC(this.w_AZSTALIG)+;
             ",AZINTLIG="+cp_ToStrODBC(this.w_AZINTLIG)+;
             ",AZPRPALG="+cp_ToStrODBC(this.w_AZPRPALG)+;
             ",AZPREFLG="+cp_ToStrODBC(this.w_AZPREFLG)+;
             ",AZINTLIN="+cp_ToStrODBC(this.w_AZINTLIN)+;
             ",AZPRPALI="+cp_ToStrODBC(this.w_AZPRPALI)+;
             ",AZESLBIN="+cp_ToStrODBCNull(this.w_AZESLBIN)+;
             ",AZPREFLI="+cp_ToStrODBC(this.w_AZPREFLI)+;
             ",AZGMGCON="+cp_ToStrODBC(this.w_AZGMGCON)+;
             ",AZGMGFOR="+cp_ToStrODBC(this.w_AZGMGFOR)+;
             ",AZGMGPER="+cp_ToStrODBC(this.w_AZGMGPER)+;
             ",AZSTARBE="+cp_ToStrODBC(this.w_AZSTARBE)+;
             ",AZINTRBE="+cp_ToStrODBC(this.w_AZINTRBE)+;
             ",AZPRPRBE="+cp_ToStrODBC(this.w_AZPRPRBE)+;
             ",AZPRERBE="+cp_ToStrODBC(this.w_AZPRERBE)+;
             ",AZDATBLO="+cp_ToStrODBC(this.w_AZDATBLO)+;
             ",AZBLOGCL="+cp_ToStrODBC(this.w_AZBLOGCL)+;
             ",AZMASMAT="+cp_ToStrODBC(this.w_AZMASMAT)+;
             ",AZCONCON="+cp_ToStrODBC(this.w_AZCONCON)+;
             ",AZCONVEN="+cp_ToStrODBC(this.w_AZCONVEN)+;
             ",AZCONACQ="+cp_ToStrODBC(this.w_AZCONACQ)+;
             ",AZCONMAG="+cp_ToStrODBC(this.w_AZCONMAG)+;
             ",AZCONANA="+cp_ToStrODBC(this.w_AZCONANA)+;
             ",AZSCONET="+cp_ToStrODBC(this.w_AZSCONET)+;
             ",AZCALUTD="+cp_ToStrODBC(this.w_AZCALUTD)+;
             ",AZCONCAS="+cp_ToStrODBC(this.w_AZCONCAS)+;
             ",AZFLRAGG="+cp_ToStrODBC(this.w_AZFLRAGG)+;
             ",AZAITIPE="+cp_ToStrODBC(this.w_AZAITIPE)+;
             ",AZAITIPV="+cp_ToStrODBC(this.w_AZAITIPV)+;
             ",AZINPECE="+cp_ToStrODBC(this.w_AZINPECE)+;
             ",AZAICFPI="+cp_ToStrODBC(this.w_AZAICFPI)+;
             ",AZAIRAGS="+cp_ToStrODBC(this.w_AZAIRAGS)+;
             ",AZAIPREF="+cp_ToStrODBC(this.w_AZAIPREF)+;
             ",AZAITELE="+cp_ToStrODBC(this.w_AZAITELE)+;
             ",AZAIINDI="+cp_ToStrODBC(this.w_AZAIINDI)+;
             ",AZAI_CAP="+cp_ToStrODBC(this.w_AZAI_CAP)+;
             ",AZAICITT="+cp_ToStrODBC(this.w_AZAICITT)+;
             ",AZAIPROV="+cp_ToStrODBC(this.w_AZAIPROV)+;
             ",AZINPEAC="+cp_ToStrODBC(this.w_AZINPEAC)+;
             ",AZCONCVE="+cp_ToStrODBC(this.w_AZCONCVE)+;
             ",AZCONCAC="+cp_ToStrODBC(this.w_AZCONCAC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'AZIENDA')
        i_cWhere = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
        UPDATE (i_cTable) SET;
              AZRAGAZI=this.w_AZRAGAZI;
             ,AZPERAZI=this.w_AZPERAZI;
             ,AZINDAZI=this.w_AZINDAZI;
             ,AZCODOLT=this.w_AZCODOLT;
             ,AZCODCAT=this.w_AZCODCAT;
             ,AZCAPAZI=this.w_AZCAPAZI;
             ,AZLOCAZI=this.w_AZLOCAZI;
             ,AZPROAZI=this.w_AZPROAZI;
             ,AZCODNAZ=this.w_AZCODNAZ;
             ,AZCODLIN=this.w_AZCODLIN;
             ,AZCODINE=this.w_AZCODINE;
             ,AZCOFAZI=this.w_AZCOFAZI;
             ,AZPIVAZI=this.w_AZPIVAZI;
             ,AZTELEFO=this.w_AZTELEFO;
             ,AZTELFAX=this.w_AZTELFAX;
             ,AZ_EMAIL=this.w_AZ_EMAIL;
             ,AZINDWEB=this.w_AZINDWEB;
             ,AZCAPSOC=this.w_AZCAPSOC;
             ,AZNAGAZI=this.w_AZNAGAZI;
             ,AZRECREA=this.w_AZRECREA;
             ,AZTRIAZI=this.w_AZTRIAZI;
             ,AZESAAZI=this.w_AZESAAZI;
             ,AZCCFISC=this.w_AZCCFISC;
             ,AZCONTRI=this.w_AZCONTRI;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,AZVALEUR=this.w_AZVALEUR;
             ,AZVALLIR=this.w_AZVALLIR;
             ,AZUNIUTE=this.w_AZUNIUTE;
             ,AZCFNUME=this.w_AZCFNUME;
             ,AZTRAEXP=this.w_AZTRAEXP;
             ,AZTIPLOC=this.w_AZTIPLOC;
             ,AZDATTRA=this.w_AZDATTRA;
             ,AZQUADRA=this.w_AZQUADRA;
             ,AZFLUNIV=this.w_AZFLUNIV;
             ,AZFLARDO=this.w_AZFLARDO;
             ,AZPERPAR=this.w_AZPERPAR;
             ,AZPERSTS=this.w_AZPERSTS;
             ,AZCCRICA=this.w_AZCCRICA;
             ,AZGESCOM=this.w_AZGESCOM;
             ,AZRIPCOM=this.w_AZRIPCOM;
             ,AZFLRIPC=this.w_AZFLRIPC;
             ,AZDETCON=this.w_AZDETCON;
             ,AZFLVEBD=this.w_AZFLVEBD;
             ,AZFLFIDO=this.w_AZFLFIDO;
             ,AZGIORIS=this.w_AZGIORIS;
             ,AZFILPAR=this.w_AZFILPAR;
             ,AZMAXLIV=this.w_AZMAXLIV;
             ,AZMASCON=this.w_AZMASCON;
             ,AZMASCLI=this.w_AZMASCLI;
             ,AZMASFOR=this.w_AZMASFOR;
             ,AZMASMCO=this.w_AZMASMCO;
             ,AZMASCCC=this.w_AZMASCCC;
             ,AZMASMCC=this.w_AZMASMCC;
             ,AZINTMIN=this.w_AZINTMIN;
             ,AZSCAPNT=this.w_AZSCAPNT;
             ,AZDATINI=this.w_AZDATINI;
             ,AZDATSER=this.w_AZDATSER;
             ,AZNPRGEL=this.w_AZNPRGEL;
             ,AZPRESOB=this.w_AZPRESOB;
             ,AZTELSOB=this.w_AZTELSOB;
             ,AZCODPOR=this.w_AZCODPOR;
             ,AZDTOBBL=this.w_AZDTOBBL;
             ,AZMAGAZI=this.w_AZMAGAZI;
             ,AZSCAMAG=this.w_AZSCAMAG;
             ,AZMAGUTE=this.w_AZMAGUTE;
             ,AZPERAGE=this.w_AZPERAGE;
             ,AZPERART=this.w_AZPERART;
             ,AZGESCON=this.w_AZGESCON;
             ,AZPERDIS=this.w_AZPERDIS;
             ,AZPERSDA=this.w_AZPERSDA;
             ,AZPERORN=this.w_AZPERORN;
             ,AZGESLOT=this.w_AZGESLOT;
             ,AZGESUBI=this.w_AZGESUBI;
             ,AZGESMAT=this.w_AZGESMAT;
             ,AZFLCAMB=this.w_AZFLCAMB;
             ,AZRATEVA=this.w_AZRATEVA;
             ,AZFLSCOM=this.w_AZFLSCOM;
             ,AZDTSCOM=this.w_AZDTSCOM;
             ,AZSCOPIE=this.w_AZSCOPIE;
             ,AZDTSCPI=this.w_AZDTSCPI;
             ,AZFLCESC=this.w_AZFLCESC;
             ,AZXCONDI=this.w_AZXCONDI;
             ,AZDATDOC=this.w_AZDATDOC;
             ,AZCALNET=this.w_AZCALNET;
             ,AZPERPQT=this.w_AZPERPQT;
             ,AZPERPQD=this.w_AZPERPQD;
             ,AZNUMSCO=this.w_AZNUMSCO;
             ,AZCODEXT=this.w_AZCODEXT;
             ,AZPREEAN=this.w_AZPREEAN;
             ,AZDATMAT=this.w_AZDATMAT;
             ,AZUNIMAT=this.w_AZUNIMAT;
             ,AZSCAMAT=this.w_AZSCAMAT;
             ,AZCODPRO=this.w_AZCODPRO;
             ,AZBARPRO=this.w_AZBARPRO;
             ,AZMASART=this.w_AZMASART;
             ,AZMASLOT=this.w_AZMASLOT;
             ,AZMASUBI=this.w_AZMASUBI;
             ,AZPERMUC=this.w_AZPERMUC;
             ,AZSTALIG=this.w_AZSTALIG;
             ,AZINTLIG=this.w_AZINTLIG;
             ,AZPRPALG=this.w_AZPRPALG;
             ,AZPREFLG=this.w_AZPREFLG;
             ,AZINTLIN=this.w_AZINTLIN;
             ,AZPRPALI=this.w_AZPRPALI;
             ,AZESLBIN=this.w_AZESLBIN;
             ,AZPREFLI=this.w_AZPREFLI;
             ,AZGMGCON=this.w_AZGMGCON;
             ,AZGMGFOR=this.w_AZGMGFOR;
             ,AZGMGPER=this.w_AZGMGPER;
             ,AZSTARBE=this.w_AZSTARBE;
             ,AZINTRBE=this.w_AZINTRBE;
             ,AZPRPRBE=this.w_AZPRPRBE;
             ,AZPRERBE=this.w_AZPRERBE;
             ,AZDATBLO=this.w_AZDATBLO;
             ,AZBLOGCL=this.w_AZBLOGCL;
             ,AZMASMAT=this.w_AZMASMAT;
             ,AZCONCON=this.w_AZCONCON;
             ,AZCONVEN=this.w_AZCONVEN;
             ,AZCONACQ=this.w_AZCONACQ;
             ,AZCONMAG=this.w_AZCONMAG;
             ,AZCONANA=this.w_AZCONANA;
             ,AZSCONET=this.w_AZSCONET;
             ,AZCALUTD=this.w_AZCALUTD;
             ,AZCONCAS=this.w_AZCONCAS;
             ,AZFLRAGG=this.w_AZFLRAGG;
             ,AZAITIPE=this.w_AZAITIPE;
             ,AZAITIPV=this.w_AZAITIPV;
             ,AZINPECE=this.w_AZINPECE;
             ,AZAICFPI=this.w_AZAICFPI;
             ,AZAIRAGS=this.w_AZAIRAGS;
             ,AZAIPREF=this.w_AZAIPREF;
             ,AZAITELE=this.w_AZAITELE;
             ,AZAIINDI=this.w_AZAIINDI;
             ,AZAI_CAP=this.w_AZAI_CAP;
             ,AZAICITT=this.w_AZAICITT;
             ,AZAIPROV=this.w_AZAIPROV;
             ,AZINPEAC=this.w_AZINPEAC;
             ,AZCONCVE=this.w_AZCONCVE;
             ,AZCONCAC=this.w_AZCONCAC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAR_MTT : Saving
      this.GSAR_MTT.ChangeRow(this.cRowID+'      1',0;
             ,this.w_AZCODAZI,"TTCODAZI";
             )
      this.GSAR_MTT.mReplace()
      * --- GSAR_ADR : Saving
      this.GSAR_ADR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_AZCODAZI,"RFCODAZI";
             )
      this.GSAR_ADR.mReplace()
      * --- gsar_abk : Saving
      this.gsar_abk.ChangeRow(this.cRowID+'      1',0;
             ,this.w_AZCODAZI,"AZCODAZI";
             )
      this.gsar_abk.mReplace()
      * --- GSAR_AIT : Saving
      this.GSAR_AIT.ChangeRow(this.cRowID+'      1',0;
             ,this.w_AZCODAZI,"ITCODAZI";
             )
      this.GSAR_AIT.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSAR_MTT : Deleting
    this.GSAR_MTT.ChangeRow(this.cRowID+'      1',0;
           ,this.w_AZCODAZI,"TTCODAZI";
           )
    this.GSAR_MTT.mDelete()
    * --- GSAR_ADR : Deleting
    this.GSAR_ADR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_AZCODAZI,"RFCODAZI";
           )
    this.GSAR_ADR.mDelete()
    * --- gsar_abk : Deleting
    this.gsar_abk.ChangeRow(this.cRowID+'      1',0;
           ,this.w_AZCODAZI,"AZCODAZI";
           )
    this.gsar_abk.mDelete()
    * --- GSAR_AIT : Deleting
    this.GSAR_AIT.ChangeRow(this.cRowID+'      1',0;
           ,this.w_AZCODAZI,"ITCODAZI";
           )
    this.GSAR_AIT.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.AZIENDA_IDX,i_nConn)
      *
      * delete AZIENDA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
            .w_COAZI = .w_AZCODAZI
        .DoRTCalc(5,22,.t.)
        if .o_AZPERAZI<>.w_AZPERAZI
            .w_AZCAPSOC = iif(.w_AZPERAZI='S','',.w_AZCAPSOC)
        endif
        if .o_AZPERAZI<>.w_AZPERAZI
            .w_AZNAGAZI = iif(.w_AZPERAZI='S','',.w_AZNAGAZI)
        endif
        .DoRTCalc(25,33,.t.)
            .w_COAZI = .w_AZCODAZI
            .w_DEAZI = .w_AZRAGAZI
        .DoRTCalc(36,40,.t.)
        if .o_AZTRAEXP<>.w_AZTRAEXP
            .w_AZCFNUME = iif(.w_AZTRAEXP='G','S',' ')
        endif
        .DoRTCalc(42,44,.t.)
        if .o_AZTRAEXP<>.w_AZTRAEXP
            .w_AZDATTRA = iif(inlist(.w_AZTRAEXP,'A','C','G'),i_DATSYS,i_FINDAT)
        endif
        .DoRTCalc(46,51,.t.)
        if .o_AZCCRICA<>.w_AZCCRICA
            .w_AZGESCOM = ' '
        endif
        if .o_AZCCRICA<>.w_AZCCRICA
            .w_AZRIPCOM = ' '
        endif
        if .o_AZCCRICA<>.w_AZCCRICA.or. .o_AZGESCOM<>.w_AZGESCOM
            .w_AZFLRIPC = ' '
        endif
        if .o_AZTRAEXP<>.w_AZTRAEXP
            .w_AZDETCON = IIF(.w_AZTRAEXP $ 'SACBG', 'S', ' ')
        endif
        .DoRTCalc(56,61,.t.)
            .w_AZMASCLI = .w_AZMASCON
            .w_AZMASFOR = .w_AZMASCON
        .DoRTCalc(64,68,.t.)
            .w_COAZI = .w_AZCODAZI
            .w_DEAZI = .w_AZRAGAZI
        .DoRTCalc(71,77,.t.)
            .w_COAZI = .w_AZCODAZI
            .w_DEAZI = .w_AZRAGAZI
        .DoRTCalc(80,86,.t.)
        if .o_AZGESMAT<>.w_AZGESMAT
            .w_AZPERDIS = IIF(.w_AZGESMAT='S','S',' ')
        endif
        .DoRTCalc(88,95,.t.)
        if .o_AZFLSCOM<>.w_AZFLSCOM
            .w_AZDTSCOM = IIF(.w_AZFLSCOM='S', i_datsys, cp_CharToDate('  -  -  '))
        endif
        .DoRTCalc(97,97,.t.)
        if .o_AZSCOPIE<>.w_AZSCOPIE
            .w_AZDTSCPI = IIF(.w_AZSCOPIE='S', i_datsys, cp_CharToDate('  -  -  '))
        endif
        .DoRTCalc(99,118,.t.)
            .w_COAZI = .w_AZCODAZI
            .w_DEAZI = .w_AZRAGAZI
            .w_COAZI = .w_AZCODAZI
            .w_DEAZI = .w_AZRAGAZI
        .DoRTCalc(123,126,.t.)
        if .o_AZINTLIG<>.w_AZINTLIG
            .w_AZPREFLG = IIF(.w_AZINTLIG<>'S',SPACE(20),.w_AZPREFLG)
        endif
        .DoRTCalc(128,130,.t.)
        if .o_AZINTLIN<>.w_AZINTLIN
            .w_AZPREFLI = IIF(.w_AZINTLIN<>'S',SPACE(20),.w_AZPREFLI)
        endif
        .DoRTCalc(132,136,.t.)
        if .o_AZINTRBE<>.w_AZINTRBE
            .w_AZPRPRBE = 0
        endif
        if .o_AZINTRBE<>.w_AZINTRBE
            .w_AZPRERBE = IIF(.w_AZINTRBE<>'S',SPACE(20),.w_AZPRERBE)
        endif
        .DoRTCalc(139,140,.t.)
            .w_COAZI = .w_AZCODAZI
            .w_DEAZI = .w_AZRAGAZI
        .oPgFrm.Page4.oPag.oObj_4_57.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_58.Calculate()
        if .o_AZCODPRO<>.w_AZCODPRO
          .Calculate_QPBYLYTGJU()
        endif
        .oPgFrm.Page6.oPag.ZoomSedi.Calculate()
        .DoRTCalc(143,148,.t.)
            .w_SEPERSON = NVL(.w_ZoomSedi.GetVar("SEPERSRE"),"")
            .w_SETIPRIF = .w_ZoomSedi.GetVar("SETIPRIF")
            .w_SEINDIRI = .w_ZoomSedi.GetVar("SEINDIRI")
            .w_SE___CAP = .w_ZoomSedi.GetVar("SE___CAP")
            .w_SELOCALI = .w_ZoomSedi.GetVar("SELOCALI")
            .w_SEPROVIN = .w_ZoomSedi.GetVar("SEPROVIN")
            .w_SETELEFO = .w_ZoomSedi.GetVar("SETELEFO")
            .w_SE_EMAIL = .w_ZoomSedi.GetVar("SE_EMAIL")
            .w_SE_EMPEC = .w_ZoomSedi.GetVar("SE_EMPEC")
            .w_SEPREMAP = .w_ZoomSedi.GetVar('SEPREMAP')
            .w_DPCOGNOM = NVL(.w_ZoomSedi.GetVar("DPCOGNOM"),"")
            .w_DPNOME = NVL(.w_ZoomSedi.GetVar("DPNOME"),"")
            .w_COAZI = .w_AZCODAZI
            .w_DEAZI = .w_AZRAGAZI
            .w_COAZI = .w_AZCODAZI
            .w_DEAZI = .w_AZRAGAZI
        .oPgFrm.Page4.oPag.oObj_4_65.Calculate()
        if .o_AZCAPAZI<>.w_AZCAPAZI
            .w_CCCAP = IIF( Len( Alltrim( .w_AZCAPAZI ) )  = 5  ,  Left( .w_AZCAPAZI , 5 ) , '' )
        endif
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(Ah_MsgFormat(IIF(IsAlt(),"Studio:","Azienda:")))
        .oPgFrm.Page2.oPag.oObj_2_54.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page3.oPag.oObj_3_18.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page4.oPag.oObj_4_67.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page6.oPag.oObj_6_25.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page7.oPag.oObj_7_6.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page8.oPag.oObj_8_4.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page9.oPag.oObj_9_16.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page4.oPag.oObj_4_68.Calculate(IIF(IsAlt(), "Numero decimali campi quantit� per inserimento prestazioni", "Numero decimali campi quantit� su vendite, ordini, magazzino e statistiche"))
        .oPgFrm.Page5.oPag.oObj_5_43.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Registro cronologico","Libro giornale")))
        .oPgFrm.Page5.oPag.oObj_5_44.Calculate(IIF(.w_AZCONCAS='S', "Ultima data di stampa registro cronologico", "Ultima data di stampa libro giornale"))
        .oPgFrm.Page5.oPag.oObj_5_45.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Ultima stampa registro cronologico:","Ultima stampa libro giornale:")))
        .oPgFrm.Page5.oPag.oObj_5_46.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Numero pagina registro cronologico:","Numero pagina libro giornale:")))
        .oPgFrm.Page5.oPag.oObj_5_47.Calculate(IIF(.w_AZCONCAS='S', "Progressivo pagine registro cronologico", "Progressivo pagine libro giornale"))
        .oPgFrm.Page5.oPag.oObj_5_48.Calculate(IIF(.w_AZCONCAS='S', "Prefisso numerazione pagine registro cronologico", "Prefisso numerazione pagine libro giornale"))
        if .o_AZSCOPIE<>.w_AZSCOPIE
          .Calculate_KCIWXCMWXS()
        endif
        .DoRTCalc(166,169,.t.)
            .w_SEDESPRE = .w_ZoomSedi.GetVar('SEDESPRE')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(171,185,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page4.oPag.oObj_4_57.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_58.Calculate()
        .oPgFrm.Page6.oPag.ZoomSedi.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(Ah_MsgFormat(IIF(IsAlt(),"Studio:","Azienda:")))
        .oPgFrm.Page2.oPag.oObj_2_54.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page3.oPag.oObj_3_18.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page4.oPag.oObj_4_67.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page6.oPag.oObj_6_25.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page7.oPag.oObj_7_6.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page8.oPag.oObj_8_4.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page9.oPag.oObj_9_16.Calculate(Cp_Translate(IIF(IsAlt(),"Studio","Azienda")))
        .oPgFrm.Page4.oPag.oObj_4_68.Calculate(IIF(IsAlt(), "Numero decimali campi quantit� per inserimento prestazioni", "Numero decimali campi quantit� su vendite, ordini, magazzino e statistiche"))
        .oPgFrm.Page5.oPag.oObj_5_43.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Registro cronologico","Libro giornale")))
        .oPgFrm.Page5.oPag.oObj_5_44.Calculate(IIF(.w_AZCONCAS='S', "Ultima data di stampa registro cronologico", "Ultima data di stampa libro giornale"))
        .oPgFrm.Page5.oPag.oObj_5_45.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Ultima stampa registro cronologico:","Ultima stampa libro giornale:")))
        .oPgFrm.Page5.oPag.oObj_5_46.Calculate(Cp_Translate(IIF(.w_AZCONCAS='S',"Numero pagina registro cronologico:","Numero pagina libro giornale:")))
        .oPgFrm.Page5.oPag.oObj_5_47.Calculate(IIF(.w_AZCONCAS='S', "Progressivo pagine registro cronologico", "Progressivo pagine libro giornale"))
        .oPgFrm.Page5.oPag.oObj_5_48.Calculate(IIF(.w_AZCONCAS='S', "Prefisso numerazione pagine registro cronologico", "Prefisso numerazione pagine libro giornale"))
    endwith
  return

  proc Calculate_QPBYLYTGJU()
    with this
          * --- Simulo zerofill
          .w_AZCODPRO = IIF( ( Left( Alltrim( .w_AZCODPRO ) ,1 ) <>"0" And  Len( Alltrim( .w_AZCODPRO ) )=5 ) Or Left( Alltrim( .w_AZCODPRO ) ,1 ) ="0" , .w_AZCODPRO , Right( Repl('0',7)+Alltrim( .w_AZCODPRO ) ,7 )  )
    endwith
  endproc
  proc Calculate_KCIWXCMWXS()
    with this
          * --- Messaggi al variare di w_AZSCOPIE
          Esegui(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAZCAPSOC_1_23.enabled = this.oPgFrm.Page1.oPag.oAZCAPSOC_1_23.mCond()
    this.oPgFrm.Page1.oPag.oAZNAGAZI_1_24.enabled = this.oPgFrm.Page1.oPag.oAZNAGAZI_1_24.mCond()
    this.oPgFrm.Page2.oPag.oAZRIPCOM_2_20.enabled = this.oPgFrm.Page2.oPag.oAZRIPCOM_2_20.mCond()
    this.oPgFrm.Page2.oPag.oAZFLRIPC_2_21.enabled = this.oPgFrm.Page2.oPag.oAZFLRIPC_2_21.mCond()
    this.oPgFrm.Page2.oPag.oAZDETCON_2_22.enabled = this.oPgFrm.Page2.oPag.oAZDETCON_2_22.mCond()
    this.oPgFrm.Page2.oPag.oAZFLVEBD_2_23.enabled = this.oPgFrm.Page2.oPag.oAZFLVEBD_2_23.mCond()
    this.oPgFrm.Page2.oPag.oAZGIORIS_2_25.enabled = this.oPgFrm.Page2.oPag.oAZGIORIS_2_25.mCond()
    this.oPgFrm.Page3.oPag.oAZDTOBBL_3_10.enabled = this.oPgFrm.Page3.oPag.oAZDTOBBL_3_10.mCond()
    this.oPgFrm.Page4.oPag.oAZPERDIS_4_11.enabled = this.oPgFrm.Page4.oPag.oAZPERDIS_4_11.mCond()
    this.oPgFrm.Page4.oPag.oAZGESLOT_4_14.enabled = this.oPgFrm.Page4.oPag.oAZGESLOT_4_14.mCond()
    this.oPgFrm.Page4.oPag.oAZGESUBI_4_15.enabled = this.oPgFrm.Page4.oPag.oAZGESUBI_4_15.mCond()
    this.oPgFrm.Page4.oPag.oAZGESMAT_4_16.enabled = this.oPgFrm.Page4.oPag.oAZGESMAT_4_16.mCond()
    this.oPgFrm.Page4.oPag.oAZDTSCOM_4_21.enabled = this.oPgFrm.Page4.oPag.oAZDTSCOM_4_21.mCond()
    this.oPgFrm.Page4.oPag.oAZDTSCPI_4_23.enabled = this.oPgFrm.Page4.oPag.oAZDTSCPI_4_23.mCond()
    this.oPgFrm.Page4.oPag.oAZPERPQD_4_29.enabled = this.oPgFrm.Page4.oPag.oAZPERPQD_4_29.mCond()
    this.oPgFrm.Page4.oPag.oAZMASLOT_4_39.enabled = this.oPgFrm.Page4.oPag.oAZMASLOT_4_39.mCond()
    this.oPgFrm.Page4.oPag.oAZMASUBI_4_40.enabled = this.oPgFrm.Page4.oPag.oAZMASUBI_4_40.mCond()
    this.oPgFrm.Page5.oPag.oAZINTLIG_5_2.enabled = this.oPgFrm.Page5.oPag.oAZINTLIG_5_2.mCond()
    this.oPgFrm.Page5.oPag.oAZINTLIG_5_3.enabled = this.oPgFrm.Page5.oPag.oAZINTLIG_5_3.mCond()
    this.oPgFrm.Page5.oPag.oAZPRPALG_5_4.enabled = this.oPgFrm.Page5.oPag.oAZPRPALG_5_4.mCond()
    this.oPgFrm.Page5.oPag.oAZPREFLG_5_5.enabled = this.oPgFrm.Page5.oPag.oAZPREFLG_5_5.mCond()
    this.oPgFrm.Page5.oPag.oAZPRPALI_5_7.enabled = this.oPgFrm.Page5.oPag.oAZPRPALI_5_7.mCond()
    this.oPgFrm.Page5.oPag.oAZPREFLI_5_9.enabled = this.oPgFrm.Page5.oPag.oAZPREFLI_5_9.mCond()
    this.oPgFrm.Page5.oPag.oAZPRPRBE_5_15.enabled = this.oPgFrm.Page5.oPag.oAZPRPRBE_5_15.mCond()
    this.oPgFrm.Page5.oPag.oAZPRERBE_5_16.enabled = this.oPgFrm.Page5.oPag.oAZPRERBE_5_16.mCond()
    this.oPgFrm.Page2.oPag.oAZCONCAS_2_55.enabled = this.oPgFrm.Page2.oPag.oAZCONCAS_2_55.mCond()
    this.oPgFrm.Page2.oPag.oLinkPC_2_56.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_56.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oAZCODOLT_1_6.visible=!this.oPgFrm.Page1.oPag.oAZCODOLT_1_6.mHide()
    this.oPgFrm.Page1.oPag.oAZCODINE_1_16.visible=!this.oPgFrm.Page1.oPag.oAZCODINE_1_16.mHide()
    this.oPgFrm.Page2.oPag.oAZTRAEXP_2_9.visible=!this.oPgFrm.Page2.oPag.oAZTRAEXP_2_9.mHide()
    this.oPgFrm.Page2.oPag.oAZTRAEXP_2_10.visible=!this.oPgFrm.Page2.oPag.oAZTRAEXP_2_10.mHide()
    this.oPgFrm.Page2.oPag.oAZDATTRA_2_12.visible=!this.oPgFrm.Page2.oPag.oAZDATTRA_2_12.mHide()
    this.oPgFrm.Page2.oPag.oAZFLARDO_2_15.visible=!this.oPgFrm.Page2.oPag.oAZFLARDO_2_15.mHide()
    this.oPgFrm.Page2.oPag.oAZFLVEBD_2_23.visible=!this.oPgFrm.Page2.oPag.oAZFLVEBD_2_23.mHide()
    this.oPgFrm.Page2.oPag.oAZMASCON_2_28.visible=!this.oPgFrm.Page2.oPag.oAZMASCON_2_28.mHide()
    this.oPgFrm.Page2.oPag.oAZSCAPNT_2_36.visible=!this.oPgFrm.Page2.oPag.oAZSCAPNT_2_36.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_40.visible=!this.oPgFrm.Page2.oPag.oStr_2_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_45.visible=!this.oPgFrm.Page2.oPag.oStr_2_45.mHide()
    this.oPgFrm.Page3.oPag.oAZCODPOR_3_9.visible=!this.oPgFrm.Page3.oPag.oAZCODPOR_3_9.mHide()
    this.oPgFrm.Page4.oPag.oAZMAGAZI_4_4.visible=!this.oPgFrm.Page4.oPag.oAZMAGAZI_4_4.mHide()
    this.oPgFrm.Page4.oPag.oDESMAG_4_5.visible=!this.oPgFrm.Page4.oPag.oDESMAG_4_5.mHide()
    this.oPgFrm.Page4.oPag.oAZSCAMAG_4_6.visible=!this.oPgFrm.Page4.oPag.oAZSCAMAG_4_6.mHide()
    this.oPgFrm.Page4.oPag.oAZMAGUTE_4_7.visible=!this.oPgFrm.Page4.oPag.oAZMAGUTE_4_7.mHide()
    this.oPgFrm.Page4.oPag.oAZPERAGE_4_8.visible=!this.oPgFrm.Page4.oPag.oAZPERAGE_4_8.mHide()
    this.oPgFrm.Page4.oPag.oAZPERART_4_9.visible=!this.oPgFrm.Page4.oPag.oAZPERART_4_9.mHide()
    this.oPgFrm.Page4.oPag.oAZPERDIS_4_11.visible=!this.oPgFrm.Page4.oPag.oAZPERDIS_4_11.mHide()
    this.oPgFrm.Page4.oPag.oAZPERSDA_4_12.visible=!this.oPgFrm.Page4.oPag.oAZPERSDA_4_12.mHide()
    this.oPgFrm.Page4.oPag.oAZPERORN_4_13.visible=!this.oPgFrm.Page4.oPag.oAZPERORN_4_13.mHide()
    this.oPgFrm.Page4.oPag.oAZGESLOT_4_14.visible=!this.oPgFrm.Page4.oPag.oAZGESLOT_4_14.mHide()
    this.oPgFrm.Page4.oPag.oAZGESUBI_4_15.visible=!this.oPgFrm.Page4.oPag.oAZGESUBI_4_15.mHide()
    this.oPgFrm.Page4.oPag.oAZGESMAT_4_16.visible=!this.oPgFrm.Page4.oPag.oAZGESMAT_4_16.mHide()
    this.oPgFrm.Page4.oPag.oAZFLCAMB_4_17.visible=!this.oPgFrm.Page4.oPag.oAZFLCAMB_4_17.mHide()
    this.oPgFrm.Page4.oPag.oAZRATEVA_4_18.visible=!this.oPgFrm.Page4.oPag.oAZRATEVA_4_18.mHide()
    this.oPgFrm.Page4.oPag.oAZFLSCOM_4_19.visible=!this.oPgFrm.Page4.oPag.oAZFLSCOM_4_19.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_20.visible=!this.oPgFrm.Page4.oPag.oStr_4_20.mHide()
    this.oPgFrm.Page4.oPag.oAZDTSCOM_4_21.visible=!this.oPgFrm.Page4.oPag.oAZDTSCOM_4_21.mHide()
    this.oPgFrm.Page4.oPag.oAZSCOPIE_4_22.visible=!this.oPgFrm.Page4.oPag.oAZSCOPIE_4_22.mHide()
    this.oPgFrm.Page4.oPag.oAZDTSCPI_4_23.visible=!this.oPgFrm.Page4.oPag.oAZDTSCPI_4_23.mHide()
    this.oPgFrm.Page4.oPag.oAZFLCESC_4_24.visible=!this.oPgFrm.Page4.oPag.oAZFLCESC_4_24.mHide()
    this.oPgFrm.Page4.oPag.oAZXCONDI_4_25.visible=!this.oPgFrm.Page4.oPag.oAZXCONDI_4_25.mHide()
    this.oPgFrm.Page4.oPag.oAZDATDOC_4_26.visible=!this.oPgFrm.Page4.oPag.oAZDATDOC_4_26.mHide()
    this.oPgFrm.Page4.oPag.oAZCALNET_4_27.visible=!this.oPgFrm.Page4.oPag.oAZCALNET_4_27.mHide()
    this.oPgFrm.Page4.oPag.oAZPERPQD_4_29.visible=!this.oPgFrm.Page4.oPag.oAZPERPQD_4_29.mHide()
    this.oPgFrm.Page4.oPag.oAZNUMSCO_4_30.visible=!this.oPgFrm.Page4.oPag.oAZNUMSCO_4_30.mHide()
    this.oPgFrm.Page4.oPag.oAZCODEXT_4_31.visible=!this.oPgFrm.Page4.oPag.oAZCODEXT_4_31.mHide()
    this.oPgFrm.Page4.oPag.oAZPREEAN_4_32.visible=!this.oPgFrm.Page4.oPag.oAZPREEAN_4_32.mHide()
    this.oPgFrm.Page4.oPag.oAZDATMAT_4_33.visible=!this.oPgFrm.Page4.oPag.oAZDATMAT_4_33.mHide()
    this.oPgFrm.Page4.oPag.oAZUNIMAT_4_34.visible=!this.oPgFrm.Page4.oPag.oAZUNIMAT_4_34.mHide()
    this.oPgFrm.Page4.oPag.oAZSCAMAT_4_35.visible=!this.oPgFrm.Page4.oPag.oAZSCAMAT_4_35.mHide()
    this.oPgFrm.Page4.oPag.oAZCODPRO_4_36.visible=!this.oPgFrm.Page4.oPag.oAZCODPRO_4_36.mHide()
    this.oPgFrm.Page4.oPag.oAZMASART_4_38.visible=!this.oPgFrm.Page4.oPag.oAZMASART_4_38.mHide()
    this.oPgFrm.Page4.oPag.oAZMASLOT_4_39.visible=!this.oPgFrm.Page4.oPag.oAZMASLOT_4_39.mHide()
    this.oPgFrm.Page4.oPag.oAZMASUBI_4_40.visible=!this.oPgFrm.Page4.oPag.oAZMASUBI_4_40.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_41.visible=!this.oPgFrm.Page4.oPag.oStr_4_41.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_43.visible=!this.oPgFrm.Page4.oPag.oStr_4_43.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_44.visible=!this.oPgFrm.Page4.oPag.oStr_4_44.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_47.visible=!this.oPgFrm.Page4.oPag.oStr_4_47.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_48.visible=!this.oPgFrm.Page4.oPag.oStr_4_48.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_49.visible=!this.oPgFrm.Page4.oPag.oStr_4_49.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_50.visible=!this.oPgFrm.Page4.oPag.oStr_4_50.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_51.visible=!this.oPgFrm.Page4.oPag.oStr_4_51.mHide()
    this.oPgFrm.Page4.oPag.oAZPERMUC_4_53.visible=!this.oPgFrm.Page4.oPag.oAZPERMUC_4_53.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_54.visible=!this.oPgFrm.Page4.oPag.oStr_4_54.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_55.visible=!this.oPgFrm.Page4.oPag.oStr_4_55.mHide()
    this.oPgFrm.Page5.oPag.oAZINTLIG_5_2.visible=!this.oPgFrm.Page5.oPag.oAZINTLIG_5_2.mHide()
    this.oPgFrm.Page5.oPag.oAZINTLIG_5_3.visible=!this.oPgFrm.Page5.oPag.oAZINTLIG_5_3.mHide()
    this.oPgFrm.Page5.oPag.oAZINTLIN_5_6.visible=!this.oPgFrm.Page5.oPag.oAZINTLIN_5_6.mHide()
    this.oPgFrm.Page5.oPag.oAZPRPALI_5_7.visible=!this.oPgFrm.Page5.oPag.oAZPRPALI_5_7.mHide()
    this.oPgFrm.Page5.oPag.oAZESLBIN_5_8.visible=!this.oPgFrm.Page5.oPag.oAZESLBIN_5_8.mHide()
    this.oPgFrm.Page5.oPag.oAZPREFLI_5_9.visible=!this.oPgFrm.Page5.oPag.oAZPREFLI_5_9.mHide()
    this.oPgFrm.Page5.oPag.oAZGMGCON_5_10.visible=!this.oPgFrm.Page5.oPag.oAZGMGCON_5_10.mHide()
    this.oPgFrm.Page5.oPag.oAZGMGFOR_5_11.visible=!this.oPgFrm.Page5.oPag.oAZGMGFOR_5_11.mHide()
    this.oPgFrm.Page5.oPag.oAZGMGPER_5_12.visible=!this.oPgFrm.Page5.oPag.oAZGMGPER_5_12.mHide()
    this.oPgFrm.Page5.oPag.oAZSTARBE_5_13.visible=!this.oPgFrm.Page5.oPag.oAZSTARBE_5_13.mHide()
    this.oPgFrm.Page5.oPag.oAZINTRBE_5_14.visible=!this.oPgFrm.Page5.oPag.oAZINTRBE_5_14.mHide()
    this.oPgFrm.Page5.oPag.oAZPRPRBE_5_15.visible=!this.oPgFrm.Page5.oPag.oAZPRPRBE_5_15.mHide()
    this.oPgFrm.Page5.oPag.oAZPRERBE_5_16.visible=!this.oPgFrm.Page5.oPag.oAZPRERBE_5_16.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_17.visible=!this.oPgFrm.Page5.oPag.oStr_5_17.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_18.visible=!this.oPgFrm.Page5.oPag.oStr_5_18.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_19.visible=!this.oPgFrm.Page5.oPag.oStr_5_19.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_21.visible=!this.oPgFrm.Page5.oPag.oStr_5_21.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_30.visible=!this.oPgFrm.Page5.oPag.oStr_5_30.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_32.visible=!this.oPgFrm.Page5.oPag.oStr_5_32.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_33.visible=!this.oPgFrm.Page5.oPag.oStr_5_33.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_56.visible=!this.oPgFrm.Page4.oPag.oStr_4_56.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_60.visible=!this.oPgFrm.Page4.oPag.oStr_4_60.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_61.visible=!this.oPgFrm.Page4.oPag.oStr_4_61.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_62.visible=!this.oPgFrm.Page4.oPag.oStr_4_62.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_5.visible=!this.oPgFrm.Page9.oPag.oStr_9_5.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_6.visible=!this.oPgFrm.Page9.oPag.oStr_9_6.mHide()
    this.oPgFrm.Page9.oPag.oAZCONACQ_9_10.visible=!this.oPgFrm.Page9.oPag.oAZCONACQ_9_10.mHide()
    this.oPgFrm.Page9.oPag.oAZCONMAG_9_11.visible=!this.oPgFrm.Page9.oPag.oAZCONMAG_9_11.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_63.visible=!this.oPgFrm.Page4.oPag.oStr_4_63.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_36.visible=!this.oPgFrm.Page5.oPag.oStr_5_36.mHide()
    this.oPgFrm.Page4.oPag.oAZSCONET_4_66.visible=!this.oPgFrm.Page4.oPag.oAZSCONET_4_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_16.visible=!this.oPgFrm.Page3.oPag.oStr_3_16.mHide()
    this.oPgFrm.Page3.oPag.oDESCRI_3_17.visible=!this.oPgFrm.Page3.oPag.oDESCRI_3_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_37.visible=!this.oPgFrm.Page5.oPag.oStr_5_37.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_38.visible=!this.oPgFrm.Page5.oPag.oStr_5_38.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_39.visible=!this.oPgFrm.Page5.oPag.oStr_5_39.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_41.visible=!this.oPgFrm.Page5.oPag.oStr_5_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page2.oPag.oAZCONCAS_2_55.visible=!this.oPgFrm.Page2.oPag.oAZCONCAS_2_55.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_69.visible=!this.oPgFrm.Page4.oPag.oStr_4_69.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_57.visible=!this.oPgFrm.Page2.oPag.oStr_2_57.mHide()
    this.oPgFrm.Page6.oPag.oSEDESPRE_6_26.visible=!this.oPgFrm.Page6.oPag.oSEDESPRE_6_26.mHide()
    this.oPgFrm.Page2.oPag.oAZCONCVE_2_59.visible=!this.oPgFrm.Page2.oPag.oAZCONCVE_2_59.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_60.visible=!this.oPgFrm.Page2.oPag.oStr_2_60.mHide()
    this.oPgFrm.Page2.oPag.oAZCONCAC_2_61.visible=!this.oPgFrm.Page2.oPag.oAZCONCAC_2_61.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_62.visible=!this.oPgFrm.Page2.oPag.oStr_2_62.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page4.oPag.oObj_4_57.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_58.Event(cEvent)
      .oPgFrm.Page6.oPag.ZoomSedi.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_54.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_18.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_67.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_42.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_25.Event(cEvent)
      .oPgFrm.Page7.oPag.oObj_7_6.Event(cEvent)
      .oPgFrm.Page8.oPag.oObj_8_4.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_16.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_68.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_43.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_44.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_45.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_46.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_47.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_48.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZCODCAT
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CATA_IDX,3]
    i_lTable = "COD_CATA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2], .t., this.COD_CATA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACC',True,'COD_CATA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_AZCODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_AZCODCAT))
          select CCCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCODCAT)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCODCAT) and !this.bDontReportError
            deferred_cp_zoom('COD_CATA','*','CCCODICE',cp_AbsName(oSource.parent,'oAZCODCAT_1_7'),i_cWhere,'GSAR_ACC',"Codici catastali",'GSAR_ACA.COD_CATA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_AZCODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_AZCODCAT)
            select CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCODCAT = NVL(_Link_.CCCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_AZCODCAT = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_CATA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCODNAZ
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCODNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_AZCODNAZ)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_AZCODNAZ))
          select NACODNAZ,NADESNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCODNAZ)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCODNAZ) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oAZCODNAZ_1_11'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCODNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_AZCODNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_AZCODNAZ)
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCODNAZ = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ = NVL(_Link_.NADESNAZ,space(35))
      this.w_CODISO = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_AZCODNAZ = space(3)
      endif
      this.w_DESNAZ = space(35)
      this.w_CODISO = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCODNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.NACODNAZ as NACODNAZ111"+ ",link_1_11.NADESNAZ as NADESNAZ111"+ ",link_1_11.NACODISO as NACODISO111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on AZIENDA.AZCODNAZ=link_1_11.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and AZIENDA.AZCODNAZ=link_1_11.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZCODLIN
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_AZCODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_AZCODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oAZCODLIN_1_14'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_AZCODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_AZCODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLIN = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_AZCODLIN = space(3)
      endif
      this.w_DESLIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LINGUE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.LUCODICE as LUCODICE114"+ ",link_1_14.LUDESCRI as LUDESCRI114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on AZIENDA.AZCODLIN=link_1_14.LUCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and AZIENDA.AZCODLIN=link_1_14.LUCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZVALEUR
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZVALEUR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_AZVALEUR)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_AZVALEUR))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZVALEUR)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_AZVALEUR)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_AZVALEUR)+"%");

            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AZVALEUR) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oAZVALEUR_2_3'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZVALEUR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_AZVALEUR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_AZVALEUR)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZVALEUR = NVL(_Link_.VACODVAL,space(3))
      this.w_DESEUR = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_AZVALEUR = space(3)
      endif
      this.w_DESEUR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZVALEUR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.VACODVAL as VACODVAL203"+ ",link_2_3.VADESVAL as VADESVAL203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on AZIENDA.AZVALEUR=link_2_3.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and AZIENDA.AZVALEUR=link_2_3.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZVALLIR
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZVALLIR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_AZVALLIR)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_AZVALLIR))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZVALLIR)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_AZVALLIR)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_AZVALLIR)+"%");

            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AZVALLIR) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oAZVALLIR_2_5'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZVALLIR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_AZVALLIR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_AZVALLIR)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZVALLIR = NVL(_Link_.VACODVAL,space(3))
      this.w_DESLIR = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_AZVALLIR = space(3)
      endif
      this.w_DESLIR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZVALLIR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.VACODVAL as VACODVAL205"+ ",link_2_5.VADESVAL as VADESVAL205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on AZIENDA.AZVALLIR=link_2_5.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and AZIENDA.AZVALLIR=link_2_5.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZSCAPNT
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZSCAPNT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_AZSCAPNT)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZCODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZCODAZI;
                     ,'ESCODESE',trim(this.w_AZSCAPNT))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZSCAPNT)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZSCAPNT) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oAZSCAPNT_2_36'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZCODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZCODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZSCAPNT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_AZSCAPNT);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZCODAZI;
                       ,'ESCODESE',this.w_AZSCAPNT)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZSCAPNT = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_AZSCAPNT = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZSCAPNT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCODPOR
  func Link_3_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_AREO_IDX,3]
    i_lTable = "COD_AREO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2], .t., this.COD_AREO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCODPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GLES_APP',True,'COD_AREO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PPCODICE like "+cp_ToStrODBC(trim(this.w_AZCODPOR)+"%");

          i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PPCODICE',trim(this.w_AZCODPOR))
          select PPCODICE,PPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCODPOR)==trim(_Link_.PPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCODPOR) and !this.bDontReportError
            deferred_cp_zoom('COD_AREO','*','PPCODICE',cp_AbsName(oSource.parent,'oAZCODPOR_3_9'),i_cWhere,'GLES_APP',"Porti/aeroporti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',oSource.xKey(1))
            select PPCODICE,PPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCODPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_AZCODPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_AZCODPOR)
            select PPCODICE,PPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCODPOR = NVL(_Link_.PPCODICE,space(10))
      this.w_DESCRI = NVL(_Link_.PPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_AZCODPOR = space(10)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_AREO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCODPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_AREO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_9.PPCODICE as PPCODICE309"+ ",link_3_9.PPDESCRI as PPDESCRI309"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_9 on AZIENDA.AZCODPOR=link_3_9.PPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_9"
          i_cKey=i_cKey+'+" and AZIENDA.AZCODPOR=link_3_9.PPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZMAGAZI
  func Link_4_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZMAGAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_AZMAGAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_AZMAGAZI))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZMAGAZI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_AZMAGAZI)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_AZMAGAZI)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AZMAGAZI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oAZMAGAZI_4_4'),i_cWhere,'GSAR_AMA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZMAGAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_AZMAGAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_AZMAGAZI)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZMAGAZI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZMAGAZI = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_AZMAGAZI = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZMAGAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_4.MGCODMAG as MGCODMAG404"+ ",link_4_4.MGDESMAG as MGDESMAG404"+ ",link_4_4.MGDTOBSO as MGDTOBSO404"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_4 on AZIENDA.AZMAGAZI=link_4_4.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_4"
          i_cKey=i_cKey+'+" and AZIENDA.AZMAGAZI=link_4_4.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZESLBIN
  func Link_5_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZESLBIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_AZESLBIN)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_COAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_COAZI;
                     ,'ESCODESE',trim(this.w_AZESLBIN))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZESLBIN)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZESLBIN) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oAZESLBIN_5_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_COAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZESLBIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_AZESLBIN);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_COAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_COAZI;
                       ,'ESCODESE',this.w_AZESLBIN)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZESLBIN = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_AZESLBIN = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZESLBIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAZRAGAZI_1_2.value==this.w_AZRAGAZI)
      this.oPgFrm.Page1.oPag.oAZRAGAZI_1_2.value=this.w_AZRAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZPERAZI_1_3.RadioValue()==this.w_AZPERAZI)
      this.oPgFrm.Page1.oPag.oAZPERAZI_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOAZI_1_4.value==this.w_COAZI)
      this.oPgFrm.Page1.oPag.oCOAZI_1_4.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZINDAZI_1_5.value==this.w_AZINDAZI)
      this.oPgFrm.Page1.oPag.oAZINDAZI_1_5.value=this.w_AZINDAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCODOLT_1_6.value==this.w_AZCODOLT)
      this.oPgFrm.Page1.oPag.oAZCODOLT_1_6.value=this.w_AZCODOLT
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCODCAT_1_7.value==this.w_AZCODCAT)
      this.oPgFrm.Page1.oPag.oAZCODCAT_1_7.value=this.w_AZCODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCAPAZI_1_8.value==this.w_AZCAPAZI)
      this.oPgFrm.Page1.oPag.oAZCAPAZI_1_8.value=this.w_AZCAPAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZLOCAZI_1_9.value==this.w_AZLOCAZI)
      this.oPgFrm.Page1.oPag.oAZLOCAZI_1_9.value=this.w_AZLOCAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZPROAZI_1_10.value==this.w_AZPROAZI)
      this.oPgFrm.Page1.oPag.oAZPROAZI_1_10.value=this.w_AZPROAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCODNAZ_1_11.value==this.w_AZCODNAZ)
      this.oPgFrm.Page1.oPag.oAZCODNAZ_1_11.value=this.w_AZCODNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNAZ_1_12.value==this.w_DESNAZ)
      this.oPgFrm.Page1.oPag.oDESNAZ_1_12.value=this.w_DESNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCODISO_1_13.value==this.w_CODISO)
      this.oPgFrm.Page1.oPag.oCODISO_1_13.value=this.w_CODISO
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCODLIN_1_14.value==this.w_AZCODLIN)
      this.oPgFrm.Page1.oPag.oAZCODLIN_1_14.value=this.w_AZCODLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIN_1_15.value==this.w_DESLIN)
      this.oPgFrm.Page1.oPag.oDESLIN_1_15.value=this.w_DESLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCODINE_1_16.value==this.w_AZCODINE)
      this.oPgFrm.Page1.oPag.oAZCODINE_1_16.value=this.w_AZCODINE
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCOFAZI_1_17.value==this.w_AZCOFAZI)
      this.oPgFrm.Page1.oPag.oAZCOFAZI_1_17.value=this.w_AZCOFAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZPIVAZI_1_18.value==this.w_AZPIVAZI)
      this.oPgFrm.Page1.oPag.oAZPIVAZI_1_18.value=this.w_AZPIVAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZTELEFO_1_19.value==this.w_AZTELEFO)
      this.oPgFrm.Page1.oPag.oAZTELEFO_1_19.value=this.w_AZTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oAZTELFAX_1_20.value==this.w_AZTELFAX)
      this.oPgFrm.Page1.oPag.oAZTELFAX_1_20.value=this.w_AZTELFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oAZ_EMAIL_1_21.value==this.w_AZ_EMAIL)
      this.oPgFrm.Page1.oPag.oAZ_EMAIL_1_21.value=this.w_AZ_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oAZINDWEB_1_22.value==this.w_AZINDWEB)
      this.oPgFrm.Page1.oPag.oAZINDWEB_1_22.value=this.w_AZINDWEB
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCAPSOC_1_23.value==this.w_AZCAPSOC)
      this.oPgFrm.Page1.oPag.oAZCAPSOC_1_23.value=this.w_AZCAPSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oAZNAGAZI_1_24.value==this.w_AZNAGAZI)
      this.oPgFrm.Page1.oPag.oAZNAGAZI_1_24.value=this.w_AZNAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZRECREA_1_25.value==this.w_AZRECREA)
      this.oPgFrm.Page1.oPag.oAZRECREA_1_25.value=this.w_AZRECREA
    endif
    if not(this.oPgFrm.Page1.oPag.oAZTRIAZI_1_26.value==this.w_AZTRIAZI)
      this.oPgFrm.Page1.oPag.oAZTRIAZI_1_26.value=this.w_AZTRIAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZESAAZI_1_27.value==this.w_AZESAAZI)
      this.oPgFrm.Page1.oPag.oAZESAAZI_1_27.value=this.w_AZESAAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCCFISC_1_28.value==this.w_AZCCFISC)
      this.oPgFrm.Page1.oPag.oAZCCFISC_1_28.value=this.w_AZCCFISC
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCONTRI_1_29.value==this.w_AZCONTRI)
      this.oPgFrm.Page1.oPag.oAZCONTRI_1_29.value=this.w_AZCONTRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCOAZI_2_1.value==this.w_COAZI)
      this.oPgFrm.Page2.oPag.oCOAZI_2_1.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page2.oPag.oDEAZI_2_2.value==this.w_DEAZI)
      this.oPgFrm.Page2.oPag.oDEAZI_2_2.value=this.w_DEAZI
    endif
    if not(this.oPgFrm.Page2.oPag.oAZVALEUR_2_3.value==this.w_AZVALEUR)
      this.oPgFrm.Page2.oPag.oAZVALEUR_2_3.value=this.w_AZVALEUR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESEUR_2_4.value==this.w_DESEUR)
      this.oPgFrm.Page2.oPag.oDESEUR_2_4.value=this.w_DESEUR
    endif
    if not(this.oPgFrm.Page2.oPag.oAZVALLIR_2_5.value==this.w_AZVALLIR)
      this.oPgFrm.Page2.oPag.oAZVALLIR_2_5.value=this.w_AZVALLIR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLIR_2_6.value==this.w_DESLIR)
      this.oPgFrm.Page2.oPag.oDESLIR_2_6.value=this.w_DESLIR
    endif
    if not(this.oPgFrm.Page2.oPag.oAZUNIUTE_2_7.RadioValue()==this.w_AZUNIUTE)
      this.oPgFrm.Page2.oPag.oAZUNIUTE_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZCFNUME_2_8.RadioValue()==this.w_AZCFNUME)
      this.oPgFrm.Page2.oPag.oAZCFNUME_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZTRAEXP_2_9.RadioValue()==this.w_AZTRAEXP)
      this.oPgFrm.Page2.oPag.oAZTRAEXP_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZTRAEXP_2_10.RadioValue()==this.w_AZTRAEXP)
      this.oPgFrm.Page2.oPag.oAZTRAEXP_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZTIPLOC_2_11.RadioValue()==this.w_AZTIPLOC)
      this.oPgFrm.Page2.oPag.oAZTIPLOC_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZDATTRA_2_12.value==this.w_AZDATTRA)
      this.oPgFrm.Page2.oPag.oAZDATTRA_2_12.value=this.w_AZDATTRA
    endif
    if not(this.oPgFrm.Page2.oPag.oAZQUADRA_2_13.RadioValue()==this.w_AZQUADRA)
      this.oPgFrm.Page2.oPag.oAZQUADRA_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZFLUNIV_2_14.RadioValue()==this.w_AZFLUNIV)
      this.oPgFrm.Page2.oPag.oAZFLUNIV_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZFLARDO_2_15.RadioValue()==this.w_AZFLARDO)
      this.oPgFrm.Page2.oPag.oAZFLARDO_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZPERPAR_2_16.RadioValue()==this.w_AZPERPAR)
      this.oPgFrm.Page2.oPag.oAZPERPAR_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZCCRICA_2_18.RadioValue()==this.w_AZCCRICA)
      this.oPgFrm.Page2.oPag.oAZCCRICA_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZGESCOM_2_19.RadioValue()==this.w_AZGESCOM)
      this.oPgFrm.Page2.oPag.oAZGESCOM_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZRIPCOM_2_20.RadioValue()==this.w_AZRIPCOM)
      this.oPgFrm.Page2.oPag.oAZRIPCOM_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZFLRIPC_2_21.RadioValue()==this.w_AZFLRIPC)
      this.oPgFrm.Page2.oPag.oAZFLRIPC_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZDETCON_2_22.RadioValue()==this.w_AZDETCON)
      this.oPgFrm.Page2.oPag.oAZDETCON_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZFLVEBD_2_23.RadioValue()==this.w_AZFLVEBD)
      this.oPgFrm.Page2.oPag.oAZFLVEBD_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZFLFIDO_2_24.RadioValue()==this.w_AZFLFIDO)
      this.oPgFrm.Page2.oPag.oAZFLFIDO_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZGIORIS_2_25.value==this.w_AZGIORIS)
      this.oPgFrm.Page2.oPag.oAZGIORIS_2_25.value=this.w_AZGIORIS
    endif
    if not(this.oPgFrm.Page2.oPag.oAZFILPAR_2_26.RadioValue()==this.w_AZFILPAR)
      this.oPgFrm.Page2.oPag.oAZFILPAR_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZMAXLIV_2_27.RadioValue()==this.w_AZMAXLIV)
      this.oPgFrm.Page2.oPag.oAZMAXLIV_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZMASCON_2_28.value==this.w_AZMASCON)
      this.oPgFrm.Page2.oPag.oAZMASCON_2_28.value=this.w_AZMASCON
    endif
    if not(this.oPgFrm.Page2.oPag.oAZMASMCO_2_31.value==this.w_AZMASMCO)
      this.oPgFrm.Page2.oPag.oAZMASMCO_2_31.value=this.w_AZMASMCO
    endif
    if not(this.oPgFrm.Page2.oPag.oAZMASCCC_2_32.value==this.w_AZMASCCC)
      this.oPgFrm.Page2.oPag.oAZMASCCC_2_32.value=this.w_AZMASCCC
    endif
    if not(this.oPgFrm.Page2.oPag.oAZMASMCC_2_33.value==this.w_AZMASMCC)
      this.oPgFrm.Page2.oPag.oAZMASMCC_2_33.value=this.w_AZMASMCC
    endif
    if not(this.oPgFrm.Page2.oPag.oAZINTMIN_2_35.value==this.w_AZINTMIN)
      this.oPgFrm.Page2.oPag.oAZINTMIN_2_35.value=this.w_AZINTMIN
    endif
    if not(this.oPgFrm.Page2.oPag.oAZSCAPNT_2_36.value==this.w_AZSCAPNT)
      this.oPgFrm.Page2.oPag.oAZSCAPNT_2_36.value=this.w_AZSCAPNT
    endif
    if not(this.oPgFrm.Page3.oPag.oCOAZI_3_1.value==this.w_COAZI)
      this.oPgFrm.Page3.oPag.oCOAZI_3_1.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page3.oPag.oDEAZI_3_2.value==this.w_DEAZI)
      this.oPgFrm.Page3.oPag.oDEAZI_3_2.value=this.w_DEAZI
    endif
    if not(this.oPgFrm.Page3.oPag.oAZDATINI_3_4.value==this.w_AZDATINI)
      this.oPgFrm.Page3.oPag.oAZDATINI_3_4.value=this.w_AZDATINI
    endif
    if not(this.oPgFrm.Page3.oPag.oAZDATSER_3_5.value==this.w_AZDATSER)
      this.oPgFrm.Page3.oPag.oAZDATSER_3_5.value=this.w_AZDATSER
    endif
    if not(this.oPgFrm.Page3.oPag.oAZNPRGEL_3_6.value==this.w_AZNPRGEL)
      this.oPgFrm.Page3.oPag.oAZNPRGEL_3_6.value=this.w_AZNPRGEL
    endif
    if not(this.oPgFrm.Page3.oPag.oAZPRESOB_3_7.value==this.w_AZPRESOB)
      this.oPgFrm.Page3.oPag.oAZPRESOB_3_7.value=this.w_AZPRESOB
    endif
    if not(this.oPgFrm.Page3.oPag.oAZTELSOB_3_8.value==this.w_AZTELSOB)
      this.oPgFrm.Page3.oPag.oAZTELSOB_3_8.value=this.w_AZTELSOB
    endif
    if not(this.oPgFrm.Page3.oPag.oAZCODPOR_3_9.value==this.w_AZCODPOR)
      this.oPgFrm.Page3.oPag.oAZCODPOR_3_9.value=this.w_AZCODPOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZDTOBBL_3_10.RadioValue()==this.w_AZDTOBBL)
      this.oPgFrm.Page3.oPag.oAZDTOBBL_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCOAZI_4_1.value==this.w_COAZI)
      this.oPgFrm.Page4.oPag.oCOAZI_4_1.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page4.oPag.oDEAZI_4_2.value==this.w_DEAZI)
      this.oPgFrm.Page4.oPag.oDEAZI_4_2.value=this.w_DEAZI
    endif
    if not(this.oPgFrm.Page4.oPag.oAZMAGAZI_4_4.value==this.w_AZMAGAZI)
      this.oPgFrm.Page4.oPag.oAZMAGAZI_4_4.value=this.w_AZMAGAZI
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAG_4_5.value==this.w_DESMAG)
      this.oPgFrm.Page4.oPag.oDESMAG_4_5.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page4.oPag.oAZSCAMAG_4_6.value==this.w_AZSCAMAG)
      this.oPgFrm.Page4.oPag.oAZSCAMAG_4_6.value=this.w_AZSCAMAG
    endif
    if not(this.oPgFrm.Page4.oPag.oAZMAGUTE_4_7.RadioValue()==this.w_AZMAGUTE)
      this.oPgFrm.Page4.oPag.oAZMAGUTE_4_7.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZPERAGE_4_8.RadioValue()==this.w_AZPERAGE)
      this.oPgFrm.Page4.oPag.oAZPERAGE_4_8.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZPERART_4_9.RadioValue()==this.w_AZPERART)
      this.oPgFrm.Page4.oPag.oAZPERART_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZGESCON_4_10.RadioValue()==this.w_AZGESCON)
      this.oPgFrm.Page4.oPag.oAZGESCON_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZPERDIS_4_11.RadioValue()==this.w_AZPERDIS)
      this.oPgFrm.Page4.oPag.oAZPERDIS_4_11.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZPERSDA_4_12.RadioValue()==this.w_AZPERSDA)
      this.oPgFrm.Page4.oPag.oAZPERSDA_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZPERORN_4_13.RadioValue()==this.w_AZPERORN)
      this.oPgFrm.Page4.oPag.oAZPERORN_4_13.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZGESLOT_4_14.RadioValue()==this.w_AZGESLOT)
      this.oPgFrm.Page4.oPag.oAZGESLOT_4_14.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZGESUBI_4_15.RadioValue()==this.w_AZGESUBI)
      this.oPgFrm.Page4.oPag.oAZGESUBI_4_15.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZGESMAT_4_16.RadioValue()==this.w_AZGESMAT)
      this.oPgFrm.Page4.oPag.oAZGESMAT_4_16.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZFLCAMB_4_17.RadioValue()==this.w_AZFLCAMB)
      this.oPgFrm.Page4.oPag.oAZFLCAMB_4_17.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZRATEVA_4_18.RadioValue()==this.w_AZRATEVA)
      this.oPgFrm.Page4.oPag.oAZRATEVA_4_18.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZFLSCOM_4_19.RadioValue()==this.w_AZFLSCOM)
      this.oPgFrm.Page4.oPag.oAZFLSCOM_4_19.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZDTSCOM_4_21.value==this.w_AZDTSCOM)
      this.oPgFrm.Page4.oPag.oAZDTSCOM_4_21.value=this.w_AZDTSCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oAZSCOPIE_4_22.RadioValue()==this.w_AZSCOPIE)
      this.oPgFrm.Page4.oPag.oAZSCOPIE_4_22.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZDTSCPI_4_23.value==this.w_AZDTSCPI)
      this.oPgFrm.Page4.oPag.oAZDTSCPI_4_23.value=this.w_AZDTSCPI
    endif
    if not(this.oPgFrm.Page4.oPag.oAZFLCESC_4_24.RadioValue()==this.w_AZFLCESC)
      this.oPgFrm.Page4.oPag.oAZFLCESC_4_24.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZXCONDI_4_25.RadioValue()==this.w_AZXCONDI)
      this.oPgFrm.Page4.oPag.oAZXCONDI_4_25.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZDATDOC_4_26.RadioValue()==this.w_AZDATDOC)
      this.oPgFrm.Page4.oPag.oAZDATDOC_4_26.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZCALNET_4_27.RadioValue()==this.w_AZCALNET)
      this.oPgFrm.Page4.oPag.oAZCALNET_4_27.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZPERPQT_4_28.RadioValue()==this.w_AZPERPQT)
      this.oPgFrm.Page4.oPag.oAZPERPQT_4_28.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZPERPQD_4_29.RadioValue()==this.w_AZPERPQD)
      this.oPgFrm.Page4.oPag.oAZPERPQD_4_29.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZNUMSCO_4_30.RadioValue()==this.w_AZNUMSCO)
      this.oPgFrm.Page4.oPag.oAZNUMSCO_4_30.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZCODEXT_4_31.value==this.w_AZCODEXT)
      this.oPgFrm.Page4.oPag.oAZCODEXT_4_31.value=this.w_AZCODEXT
    endif
    if not(this.oPgFrm.Page4.oPag.oAZPREEAN_4_32.value==this.w_AZPREEAN)
      this.oPgFrm.Page4.oPag.oAZPREEAN_4_32.value=this.w_AZPREEAN
    endif
    if not(this.oPgFrm.Page4.oPag.oAZDATMAT_4_33.value==this.w_AZDATMAT)
      this.oPgFrm.Page4.oPag.oAZDATMAT_4_33.value=this.w_AZDATMAT
    endif
    if not(this.oPgFrm.Page4.oPag.oAZUNIMAT_4_34.RadioValue()==this.w_AZUNIMAT)
      this.oPgFrm.Page4.oPag.oAZUNIMAT_4_34.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZSCAMAT_4_35.RadioValue()==this.w_AZSCAMAT)
      this.oPgFrm.Page4.oPag.oAZSCAMAT_4_35.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZCODPRO_4_36.value==this.w_AZCODPRO)
      this.oPgFrm.Page4.oPag.oAZCODPRO_4_36.value=this.w_AZCODPRO
    endif
    if not(this.oPgFrm.Page4.oPag.oAZMASART_4_38.value==this.w_AZMASART)
      this.oPgFrm.Page4.oPag.oAZMASART_4_38.value=this.w_AZMASART
    endif
    if not(this.oPgFrm.Page4.oPag.oAZMASLOT_4_39.value==this.w_AZMASLOT)
      this.oPgFrm.Page4.oPag.oAZMASLOT_4_39.value=this.w_AZMASLOT
    endif
    if not(this.oPgFrm.Page4.oPag.oAZMASUBI_4_40.value==this.w_AZMASUBI)
      this.oPgFrm.Page4.oPag.oAZMASUBI_4_40.value=this.w_AZMASUBI
    endif
    if not(this.oPgFrm.Page4.oPag.oAZPERMUC_4_53.value==this.w_AZPERMUC)
      this.oPgFrm.Page4.oPag.oAZPERMUC_4_53.value=this.w_AZPERMUC
    endif
    if not(this.oPgFrm.Page7.oPag.oCOAZI_7_2.value==this.w_COAZI)
      this.oPgFrm.Page7.oPag.oCOAZI_7_2.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page7.oPag.oDEAZI_7_3.value==this.w_DEAZI)
      this.oPgFrm.Page7.oPag.oDEAZI_7_3.value=this.w_DEAZI
    endif
    if not(this.oPgFrm.Page6.oPag.oCOAZI_6_1.value==this.w_COAZI)
      this.oPgFrm.Page6.oPag.oCOAZI_6_1.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page6.oPag.oDEAZI_6_2.value==this.w_DEAZI)
      this.oPgFrm.Page6.oPag.oDEAZI_6_2.value=this.w_DEAZI
    endif
    if not(this.oPgFrm.Page5.oPag.oAZSTALIG_5_1.value==this.w_AZSTALIG)
      this.oPgFrm.Page5.oPag.oAZSTALIG_5_1.value=this.w_AZSTALIG
    endif
    if not(this.oPgFrm.Page5.oPag.oAZINTLIG_5_2.RadioValue()==this.w_AZINTLIG)
      this.oPgFrm.Page5.oPag.oAZINTLIG_5_2.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oAZINTLIG_5_3.RadioValue()==this.w_AZINTLIG)
      this.oPgFrm.Page5.oPag.oAZINTLIG_5_3.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oAZPRPALG_5_4.value==this.w_AZPRPALG)
      this.oPgFrm.Page5.oPag.oAZPRPALG_5_4.value=this.w_AZPRPALG
    endif
    if not(this.oPgFrm.Page5.oPag.oAZPREFLG_5_5.value==this.w_AZPREFLG)
      this.oPgFrm.Page5.oPag.oAZPREFLG_5_5.value=this.w_AZPREFLG
    endif
    if not(this.oPgFrm.Page5.oPag.oAZINTLIN_5_6.RadioValue()==this.w_AZINTLIN)
      this.oPgFrm.Page5.oPag.oAZINTLIN_5_6.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oAZPRPALI_5_7.value==this.w_AZPRPALI)
      this.oPgFrm.Page5.oPag.oAZPRPALI_5_7.value=this.w_AZPRPALI
    endif
    if not(this.oPgFrm.Page5.oPag.oAZESLBIN_5_8.value==this.w_AZESLBIN)
      this.oPgFrm.Page5.oPag.oAZESLBIN_5_8.value=this.w_AZESLBIN
    endif
    if not(this.oPgFrm.Page5.oPag.oAZPREFLI_5_9.value==this.w_AZPREFLI)
      this.oPgFrm.Page5.oPag.oAZPREFLI_5_9.value=this.w_AZPREFLI
    endif
    if not(this.oPgFrm.Page5.oPag.oAZGMGCON_5_10.RadioValue()==this.w_AZGMGCON)
      this.oPgFrm.Page5.oPag.oAZGMGCON_5_10.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oAZGMGFOR_5_11.RadioValue()==this.w_AZGMGFOR)
      this.oPgFrm.Page5.oPag.oAZGMGFOR_5_11.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oAZGMGPER_5_12.RadioValue()==this.w_AZGMGPER)
      this.oPgFrm.Page5.oPag.oAZGMGPER_5_12.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oAZSTARBE_5_13.value==this.w_AZSTARBE)
      this.oPgFrm.Page5.oPag.oAZSTARBE_5_13.value=this.w_AZSTARBE
    endif
    if not(this.oPgFrm.Page5.oPag.oAZINTRBE_5_14.RadioValue()==this.w_AZINTRBE)
      this.oPgFrm.Page5.oPag.oAZINTRBE_5_14.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oAZPRPRBE_5_15.value==this.w_AZPRPRBE)
      this.oPgFrm.Page5.oPag.oAZPRPRBE_5_15.value=this.w_AZPRPRBE
    endif
    if not(this.oPgFrm.Page5.oPag.oAZPRERBE_5_16.value==this.w_AZPRERBE)
      this.oPgFrm.Page5.oPag.oAZPRERBE_5_16.value=this.w_AZPRERBE
    endif
    if not(this.oPgFrm.Page5.oPag.oAZDATBLO_5_22.value==this.w_AZDATBLO)
      this.oPgFrm.Page5.oPag.oAZDATBLO_5_22.value=this.w_AZDATBLO
    endif
    if not(this.oPgFrm.Page5.oPag.oAZBLOGCL_5_23.RadioValue()==this.w_AZBLOGCL)
      this.oPgFrm.Page5.oPag.oAZBLOGCL_5_23.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oCOAZI_5_26.value==this.w_COAZI)
      this.oPgFrm.Page5.oPag.oCOAZI_5_26.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page5.oPag.oDEAZI_5_27.value==this.w_DEAZI)
      this.oPgFrm.Page5.oPag.oDEAZI_5_27.value=this.w_DEAZI
    endif
    if not(this.oPgFrm.Page9.oPag.oAZCONCON_9_8.value==this.w_AZCONCON)
      this.oPgFrm.Page9.oPag.oAZCONCON_9_8.value=this.w_AZCONCON
    endif
    if not(this.oPgFrm.Page9.oPag.oAZCONVEN_9_9.value==this.w_AZCONVEN)
      this.oPgFrm.Page9.oPag.oAZCONVEN_9_9.value=this.w_AZCONVEN
    endif
    if not(this.oPgFrm.Page9.oPag.oAZCONACQ_9_10.value==this.w_AZCONACQ)
      this.oPgFrm.Page9.oPag.oAZCONACQ_9_10.value=this.w_AZCONACQ
    endif
    if not(this.oPgFrm.Page9.oPag.oAZCONMAG_9_11.value==this.w_AZCONMAG)
      this.oPgFrm.Page9.oPag.oAZCONMAG_9_11.value=this.w_AZCONMAG
    endif
    if not(this.oPgFrm.Page9.oPag.oAZCONANA_9_12.value==this.w_AZCONANA)
      this.oPgFrm.Page9.oPag.oAZCONANA_9_12.value=this.w_AZCONANA
    endif
    if not(this.oPgFrm.Page6.oPag.oSEPERSON_6_12.value==this.w_SEPERSON)
      this.oPgFrm.Page6.oPag.oSEPERSON_6_12.value=this.w_SEPERSON
    endif
    if not(this.oPgFrm.Page6.oPag.oSETIPRIF_6_13.RadioValue()==this.w_SETIPRIF)
      this.oPgFrm.Page6.oPag.oSETIPRIF_6_13.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oSEINDIRI_6_14.value==this.w_SEINDIRI)
      this.oPgFrm.Page6.oPag.oSEINDIRI_6_14.value=this.w_SEINDIRI
    endif
    if not(this.oPgFrm.Page6.oPag.oSE___CAP_6_15.value==this.w_SE___CAP)
      this.oPgFrm.Page6.oPag.oSE___CAP_6_15.value=this.w_SE___CAP
    endif
    if not(this.oPgFrm.Page6.oPag.oSELOCALI_6_16.value==this.w_SELOCALI)
      this.oPgFrm.Page6.oPag.oSELOCALI_6_16.value=this.w_SELOCALI
    endif
    if not(this.oPgFrm.Page6.oPag.oSEPROVIN_6_17.value==this.w_SEPROVIN)
      this.oPgFrm.Page6.oPag.oSEPROVIN_6_17.value=this.w_SEPROVIN
    endif
    if not(this.oPgFrm.Page6.oPag.oSETELEFO_6_18.value==this.w_SETELEFO)
      this.oPgFrm.Page6.oPag.oSETELEFO_6_18.value=this.w_SETELEFO
    endif
    if not(this.oPgFrm.Page6.oPag.oSE_EMAIL_6_19.value==this.w_SE_EMAIL)
      this.oPgFrm.Page6.oPag.oSE_EMAIL_6_19.value=this.w_SE_EMAIL
    endif
    if not(this.oPgFrm.Page6.oPag.oSE_EMPEC_6_20.value==this.w_SE_EMPEC)
      this.oPgFrm.Page6.oPag.oSE_EMPEC_6_20.value=this.w_SE_EMPEC
    endif
    if not(this.oPgFrm.Page6.oPag.oSEPREMAP_6_21.RadioValue()==this.w_SEPREMAP)
      this.oPgFrm.Page6.oPag.oSEPREMAP_6_21.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oDPCOGNOM_6_22.value==this.w_DPCOGNOM)
      this.oPgFrm.Page6.oPag.oDPCOGNOM_6_22.value=this.w_DPCOGNOM
    endif
    if not(this.oPgFrm.Page6.oPag.oDPNOME_6_23.value==this.w_DPNOME)
      this.oPgFrm.Page6.oPag.oDPNOME_6_23.value=this.w_DPNOME
    endif
    if not(this.oPgFrm.Page8.oPag.oCOAZI_8_2.value==this.w_COAZI)
      this.oPgFrm.Page8.oPag.oCOAZI_8_2.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page8.oPag.oDEAZI_8_3.value==this.w_DEAZI)
      this.oPgFrm.Page8.oPag.oDEAZI_8_3.value=this.w_DEAZI
    endif
    if not(this.oPgFrm.Page9.oPag.oCOAZI_9_14.value==this.w_COAZI)
      this.oPgFrm.Page9.oPag.oCOAZI_9_14.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page9.oPag.oDEAZI_9_15.value==this.w_DEAZI)
      this.oPgFrm.Page9.oPag.oDEAZI_9_15.value=this.w_DEAZI
    endif
    if not(this.oPgFrm.Page4.oPag.oAZSCONET_4_66.RadioValue()==this.w_AZSCONET)
      this.oPgFrm.Page4.oPag.oAZSCONET_4_66.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI_3_17.value==this.w_DESCRI)
      this.oPgFrm.Page3.oPag.oDESCRI_3_17.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCALUTD_1_60.RadioValue()==this.w_AZCALUTD)
      this.oPgFrm.Page1.oPag.oAZCALUTD_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZCONCAS_2_55.RadioValue()==this.w_AZCONCAS)
      this.oPgFrm.Page2.oPag.oAZCONCAS_2_55.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oSEDESPRE_6_26.RadioValue()==this.w_SEDESPRE)
      this.oPgFrm.Page6.oPag.oSEDESPRE_6_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZFLRAGG_2_58.RadioValue()==this.w_AZFLRAGG)
      this.oPgFrm.Page2.oPag.oAZFLRAGG_2_58.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZCONCVE_2_59.RadioValue()==this.w_AZCONCVE)
      this.oPgFrm.Page2.oPag.oAZCONCVE_2_59.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZCONCAC_2_61.RadioValue()==this.w_AZCONCAC)
      this.oPgFrm.Page2.oPag.oAZCONCAC_2_61.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'AZIENDA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_AZRAGAZI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZRAGAZI_1_2.SetFocus()
            i_bnoObbl = !empty(.w_AZRAGAZI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF (.w_CODISO ='IT'  OR EMPTY (.w_CODISO), EMPTY(.w_AZCOFAZI) OR CHKCFP(.w_AZCOFAZI,"CF")  ,.T. ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZCOFAZI_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_AZPIVAZI) OR CHKCFP(.w_AZPIVAZI,"PI","","", "", .w_AZCODNAZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZPIVAZI_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AZVALEUR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAZVALEUR_2_3.SetFocus()
            i_bnoObbl = !empty(.w_AZVALEUR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AZVALLIR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAZVALLIR_2_5.SetFocus()
            i_bnoObbl = !empty(.w_AZVALLIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AZMAXLIV))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAZMAXLIV_2_27.SetFocus()
            i_bnoObbl = !empty(.w_AZMAXLIV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(IsAlt())  and not(empty(.w_AZMAGAZI))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZMAGAZI_4_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   (empty(.w_AZDTSCOM))  and not(.w_AZFLSCOM<>'S' OR IsAlt())  and (.w_AZFLSCOM='S')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZDTSCOM_4_21.SetFocus()
            i_bnoObbl = !empty(.w_AZDTSCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AZDTSCPI))  and not(.w_AZSCOPIE<>'S' OR IsAlt())  and (.w_AZSCOPIE='S')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZDTSCPI_4_23.SetFocus()
            i_bnoObbl = !empty(.w_AZDTSCPI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_AZPERPQT<4)
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZPERPQT_4_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_AZPERPQT>=0 AND .w_AZPERPQT<6)  and not(g_DISB<>'S' OR IsAlt())  and (g_DISB='S')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZPERPQD_4_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Indicare al massimo 5 decimali")
          case   not(.w_AZNUMSCO<=4)  and not(IsAlt())
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZNUMSCO_4_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAR_MTT.CheckForm()
      if i_bres
        i_bres=  .GSAR_MTT.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=7
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_ADR.CheckForm()
      if i_bres
        i_bres=  .GSAR_ADR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .gsar_abk.CheckForm()
      if i_bres
        i_bres=  .gsar_abk.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=8
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_AIT.CheckForm()
      if i_bres
        i_bres=  .GSAR_AIT.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AZPERAZI = this.w_AZPERAZI
    this.o_AZCAPAZI = this.w_AZCAPAZI
    this.o_AZTRAEXP = this.w_AZTRAEXP
    this.o_AZCCRICA = this.w_AZCCRICA
    this.o_AZGESCOM = this.w_AZGESCOM
    this.o_AZGESMAT = this.w_AZGESMAT
    this.o_AZFLSCOM = this.w_AZFLSCOM
    this.o_AZSCOPIE = this.w_AZSCOPIE
    this.o_AZCODPRO = this.w_AZCODPRO
    this.o_AZINTLIG = this.w_AZINTLIG
    this.o_AZINTLIN = this.w_AZINTLIN
    this.o_AZINTRBE = this.w_AZINTRBE
    * --- GSAR_MTT : Depends On
    this.GSAR_MTT.SaveDependsOn()
    * --- GSAR_ADR : Depends On
    this.GSAR_ADR.SaveDependsOn()
    * --- gsar_abk : Depends On
    this.gsar_abk.SaveDependsOn()
    * --- GSAR_AIT : Depends On
    this.GSAR_AIT.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsar_aziPag1 as StdContainer
  Width  = 719
  height = 492
  stdWidth  = 719
  stdheight = 492
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZRAGAZI_1_2 as StdField with uid="YUJPWJOTUS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AZRAGAZI", cQueryName = "AZRAGAZI",;
    bObbl = .t. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale completa dell'azienda",;
    HelpContextID = 164821169,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=226, Top=10, InputMask=replicate('X',80)


  add object oAZPERAZI_1_3 as StdCombo with uid="ZVAEGFLPGA",rtseq=3,rtrep=.f.,left=557,top=10,width=138,height=21;
    , ToolTipText = "Tipologia azienda";
    , HelpContextID = 153032881;
    , cFormVar="w_AZPERAZI",RowSource=""+"Societ� di capitali,"+"Societ� di persone,"+"Persona fisica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAZPERAZI_1_3.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oAZPERAZI_1_3.GetRadio()
    this.Parent.oContained.w_AZPERAZI = this.RadioValue()
    return .t.
  endfunc

  func oAZPERAZI_1_3.SetRadio()
    this.Parent.oContained.w_AZPERAZI=trim(this.Parent.oContained.w_AZPERAZI)
    this.value = ;
      iif(this.Parent.oContained.w_AZPERAZI=='N',1,;
      iif(this.Parent.oContained.w_AZPERAZI=='P',2,;
      iif(this.Parent.oContained.w_AZPERAZI=='S',3,;
      0)))
  endfunc

  add object oCOAZI_1_4 as StdField with uid="AMBSRJXMDZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90500134,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=10, InputMask=replicate('X',5)

  func oCOAZI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_AZESLBIN)
        bRes2=.link_5_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oAZINDAZI_1_5 as StdField with uid="MAHNZIXTJW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_AZINDAZI", cQueryName = "AZINDAZI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo completo",;
    HelpContextID = 167151793,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=113, Top=38, InputMask=replicate('X',35)

  add object oAZCODOLT_1_6 as StdField with uid="NEHESYOVLR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AZCODOLT", cQueryName = "AZCODOLT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "codice azienda bilancio e oltre",;
    HelpContextID = 67770202,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=650, Top=38

  func oAZCODOLT_1_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZCODCAT_1_7 as StdField with uid="MNLESDKOKI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_AZCODCAT", cQueryName = "AZCODCAT",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice catastale (Sdi-Basilea2)",;
    HelpContextID = 134879066,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=650, Top=66, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="COD_CATA", cZoomOnZoom="GSAR_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_AZCODCAT"

  func oAZCODCAT_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCODCAT_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCODCAT_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_CATA','*','CCCODICE',cp_AbsName(this.parent,'oAZCODCAT_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACC',"Codici catastali",'GSAR_ACA.COD_CATA_VZM',this.parent.oContained
  endproc
  proc oAZCODCAT_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_AZCODCAT
     i_obj.ecpSave()
  endproc

  add object oAZCAPAZI_1_8 as StdField with uid="WVMLONNYZM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_AZCAPAZI", cQueryName = "AZCAPAZI",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale della localit�",;
    HelpContextID = 155445425,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=113, Top=66, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8), bHasZoom = .t. 

  proc oAZCAPAZI_1_8.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AZCAPAZI",".w_AZLOCAZI",".w_AZPROAZI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAZLOCAZI_1_9 as StdField with uid="MKLVJXAAHN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_AZLOCAZI", cQueryName = "AZLOCAZI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� in cui � situata l'azienda",;
    HelpContextID = 168122545,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=192, Top=66, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oAZLOCAZI_1_9.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AZCAPAZI",".w_AZLOCAZI",".w_AZPROAZI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAZPROAZI_1_10 as StdField with uid="ZVSCWWNYAN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_AZPROAZI", cQueryName = "AZPROAZI",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia",;
    HelpContextID = 155326641,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=511, Top=66, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. 

  proc oAZPROAZI_1_10.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_AZPROAZI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAZCODNAZ_1_11 as StdField with uid="CTAAPSBNUE",rtseq=11,rtrep=.f.,;
    cFormVar = "w_AZCODNAZ", cQueryName = "AZCODNAZ",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Nazione di appartenenza della azienda",;
    HelpContextID = 50992992,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=113, Top=94, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_AZCODNAZ"

  func oAZCODNAZ_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCODNAZ_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCODNAZ_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oAZCODNAZ_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oAZCODNAZ_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_AZCODNAZ
     i_obj.ecpSave()
  endproc

  add object oDESNAZ_1_12 as StdField with uid="CPRLEOPDEE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESNAZ", cQueryName = "DESNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 249168438,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=165, Top=94, InputMask=replicate('X',35)

  add object oCODISO_1_13 as StdField with uid="FMDAJDMYWZ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODISO", cQueryName = "CODISO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 83106854,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=511, Top=94, InputMask=replicate('X',3)

  add object oAZCODLIN_1_14 as StdField with uid="FALJJLZAMT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_AZCODLIN", cQueryName = "AZCODLIN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice lingua nazionale utilizzata di default per le stampe dei documenti",;
    HelpContextID = 250996908,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=113, Top=122, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_AZCODLIN"

  func oAZCODLIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCODLIN_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCODLIN_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oAZCODLIN_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oAZCODLIN_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_AZCODLIN
     i_obj.ecpSave()
  endproc

  add object oDESLIN_1_15 as StdField with uid="VXGEGBUNBP",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESLIN", cQueryName = "DESLIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 56099382,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=165, Top=122, InputMask=replicate('X',30)

  add object oAZCODINE_1_16 as StdField with uid="XGPFKVFTAH",rtseq=16,rtrep=.f.,;
    cFormVar = "w_AZCODINE", cQueryName = "AZCODINE",;
    bObbl = .f. , nPag = 1, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice I.N.E. ( istituto nazionale di statistica) della citt� di emissione",;
    HelpContextID = 235542347,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=511, Top=120, InputMask=replicate('X',9)

  func oAZCODINE_1_16.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oAZCOFAZI_1_17 as StdField with uid="JGGAYMEXIF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_AZCOFAZI", cQueryName = "AZCOFAZI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'azienda",;
    HelpContextID = 165013681,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=114, Top=150, InputMask=replicate('X',20)

  func oAZCOFAZI_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF (.w_CODISO ='IT'  OR EMPTY (.w_CODISO), EMPTY(.w_AZCOFAZI) OR CHKCFP(.w_AZCOFAZI,"CF")  ,.T. ))
    endwith
    return bRes
  endfunc

  add object oAZPIVAZI_1_18 as StdField with uid="JPPXESPIFS",rtseq=18,rtrep=.f.,;
    cFormVar = "w_AZPIVAZI", cQueryName = "AZPIVAZI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA dell'azienda",;
    HelpContextID = 148576433,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=474, Top=150, InputMask=replicate('X',20)

  func oAZPIVAZI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AZPIVAZI) OR CHKCFP(.w_AZPIVAZI,"PI","","", "", .w_AZCODNAZ))
    endwith
    return bRes
  endfunc

  add object oAZTELEFO_1_19 as StdField with uid="OQQEMRJSHK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_AZTELEFO", cQueryName = "AZTELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero telefonico principale dell'azienda",;
    HelpContextID = 92199083,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=113, Top=191, InputMask=replicate('X',18)

  add object oAZTELFAX_1_20 as StdField with uid="CIDFPYNWCQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_AZTELFAX", cQueryName = "AZTELFAX",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di FAX dell'azienda",;
    HelpContextID = 193013598,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=474, Top=191, InputMask=replicate('X',18)

  add object oAZ_EMAIL_1_21 as StdField with uid="IWHKNPDRAB",rtseq=21,rtrep=.f.,;
    cFormVar = "w_AZ_EMAIL", cQueryName = "AZ_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica",;
    HelpContextID = 158214318,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=113, Top=219, InputMask=replicate('X',254)

  add object oAZINDWEB_1_22 as StdField with uid="UEYMGCIGLV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_AZINDWEB", cQueryName = "AZINDWEB",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indicare il sito Internet",;
    HelpContextID = 66488504,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=113, Top=247, InputMask=replicate('X',254)

  add object oAZCAPSOC_1_23 as StdField with uid="RTTOSZRNIA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_AZCAPSOC", cQueryName = "AZCAPSOC",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Capitale sociale dell'azienda",;
    HelpContextID = 146544457,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=113, Top=275, InputMask=replicate('X',30)

  func oAZCAPSOC_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  add object oAZNAGAZI_1_24 as StdField with uid="AVYQVSXGZX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_AZNAGAZI", cQueryName = "AZNAGAZI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Natura giuridica dell'azienda",;
    HelpContextID = 164837553,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=474, Top=275, InputMask=replicate('X',5)

  func oAZNAGAZI_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  add object oAZRECREA_1_25 as StdField with uid="XHZDCFWCEP",rtseq=25,rtrep=.f.,;
    cFormVar = "w_AZRECREA", cQueryName = "AZRECREA",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Numero di iscrizione alla camera di commercio",;
    HelpContextID = 151976121,;
   bGlobalFont=.t.,;
    Height=21, Width=168, Left=113, Top=303, InputMask=replicate('X',20)

  add object oAZTRIAZI_1_26 as StdField with uid="CNWALMGAKY",rtseq=26,rtrep=.f.,;
    cFormVar = "w_AZTRIAZI", cQueryName = "AZTRIAZI",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Tribunale competente per territorio",;
    HelpContextID = 161601713,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=474, Top=303, InputMask=replicate('X',25)

  add object oAZESAAZI_1_27 as StdField with uid="CCSJCPIJBM",rtseq=27,rtrep=.f.,;
    cFormVar = "w_AZESAAZI", cQueryName = "AZESAAZI",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Esattoria di competenza",;
    HelpContextID = 169986225,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=113, Top=331, InputMask=replicate('X',25)

  add object oAZCCFISC_1_28 as StdField with uid="GYUJTYSHHZ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_AZCCFISC", cQueryName = "AZCCFISC",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice di conto corrente fiscale",;
    HelpContextID = 31582391,;
   bGlobalFont=.t.,;
    Height=21, Width=168, Left=474, Top=331, InputMask=replicate('X',20)

  add object oAZCONTRI_1_29 as StdField with uid="URLMMPJQGL",rtseq=29,rtrep=.f.,;
    cFormVar = "w_AZCONTRI", cQueryName = "AZCONTRI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice del contribuente",;
    HelpContextID = 106293425,;
   bGlobalFont=.t.,;
    Height=21, Width=168, Left=113, Top=359, InputMask=replicate('X',20)


  add object oAZCALUTD_1_60 as StdCombo with uid="ORXIHLIOET",rtseq=168,rtrep=.f.,left=476,top=359,width=168,height=21;
    , ToolTipText = "Metodo di calcolo ultima data di carica/ ultima modifica, o in base alla data inputata in ad hoc o in base alla data presente sul PC (la modifica avr� effetto al rientro nella procedura)";
    , HelpContextID = 92530870;
    , cFormVar="w_AZCALUTD",RowSource=""+"Data di sistema,"+"Data sistema operativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAZCALUTD_1_60.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oAZCALUTD_1_60.GetRadio()
    this.Parent.oContained.w_AZCALUTD = this.RadioValue()
    return .t.
  endfunc

  func oAZCALUTD_1_60.SetRadio()
    this.Parent.oContained.w_AZCALUTD=trim(this.Parent.oContained.w_AZCALUTD)
    this.value = ;
      iif(this.Parent.oContained.w_AZCALUTD=='A',1,;
      iif(this.Parent.oContained.w_AZCALUTD=='P',2,;
      0))
  endfunc


  add object oObj_1_61 as cp_calclbl with uid="VSIXYPIKDY",left=6, top=10, width=56,height=25,;
    caption='StrAzi',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=1;
    , HelpContextID = 245423578

  add object oStr_1_30 as StdString with uid="HTGBJLMHUB",Visible=.t., Left=0, Top=38,;
    Alignment=1, Width=111, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="NZJMWVVBPE",Visible=.t., Left=0, Top=66,;
    Alignment=1, Width=111, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="UVMLXHLESX",Visible=.t., Left=449, Top=66,;
    Alignment=1, Width=60, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="UOTOSHFYQH",Visible=.t., Left=0, Top=152,;
    Alignment=1, Width=111, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="QCFMZUJDYF",Visible=.t., Left=344, Top=152,;
    Alignment=1, Width=128, Height=15,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="WLYEISCGFX",Visible=.t., Left=0, Top=94,;
    Alignment=1, Width=111, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="BMQZJIVYZK",Visible=.t., Left=449, Top=94,;
    Alignment=1, Width=60, Height=15,;
    Caption="Cod.ISO:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="WJHIQGHXUB",Visible=.t., Left=0, Top=275,;
    Alignment=1, Width=111, Height=15,;
    Caption="Capitale sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="YNJVMLWVAA",Visible=.t., Left=0, Top=191,;
    Alignment=1, Width=111, Height=15,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="ACKHCGQGFY",Visible=.t., Left=387, Top=191,;
    Alignment=1, Width=85, Height=15,;
    Caption="Telefax:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="RTQPYVYXKK",Visible=.t., Left=0, Top=219,;
    Alignment=1, Width=111, Height=15,;
    Caption="E@mail address:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="VEOJFCBVNP",Visible=.t., Left=0, Top=247,;
    Alignment=1, Width=111, Height=15,;
    Caption="Internet web:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="FLDPFCHGMO",Visible=.t., Left=0, Top=331,;
    Alignment=1, Width=111, Height=15,;
    Caption="Esattoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="WGBZNLQQCR",Visible=.t., Left=367, Top=331,;
    Alignment=1, Width=105, Height=15,;
    Caption="C/C. fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="RHRODZRFXR",Visible=.t., Left=367, Top=303,;
    Alignment=1, Width=105, Height=15,;
    Caption="Tribunale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="GOPITXSXTL",Visible=.t., Left=0, Top=303,;
    Alignment=1, Width=111, Height=15,;
    Caption="Num. C.C.I.A.A.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="ZCRHFDZWJI",Visible=.t., Left=367, Top=275,;
    Alignment=1, Width=105, Height=15,;
    Caption="Natura giuridica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="VYJBILIOEX",Visible=.t., Left=0, Top=359,;
    Alignment=1, Width=111, Height=15,;
    Caption="Cod.contribuente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="AAOINGTUBK",Visible=.t., Left=0, Top=122,;
    Alignment=1, Width=111, Height=15,;
    Caption="Lingua nazionale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="TSCFBEMDRG",Visible=.t., Left=133, Top=10,;
    Alignment=1, Width=90, Height=15,;
    Caption="Rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="TTMMWQYUVN",Visible=.t., Left=562, Top=66,;
    Alignment=1, Width=85, Height=18,;
    Caption="Cod.catastale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="MAYHTAQOBM",Visible=.t., Left=420, Top=124,;
    Alignment=1, Width=89, Height=15,;
    Caption="Codice I.N.E.:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="ONTQSEIOFH",Visible=.t., Left=415, Top=38,;
    Alignment=1, Width=232, Height=18,;
    Caption="Cod.bilancio e oltre:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="PSRVEOSLQY",Visible=.t., Left=-3, Top=503,;
    Alignment=0, Width=764, Height=19,;
    Caption="Attenzione nel caso di  aggiunta di nuovi campi in AZIENDA  controllare PARAMETRI IVA  se superato limite cursore Link"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="OPQGROTIYP",Visible=.t., Left=391, Top=359,;
    Alignment=1, Width=81, Height=18,;
    Caption="Ultima data:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsar_aziPag2 as StdContainer
  Width  = 719
  height = 492
  stdWidth  = 719
  stdheight = 492
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOAZI_2_1 as StdField with uid="DLNLUKGJDR",rtseq=34,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90500134,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=10, InputMask=replicate('X',5)

  func oCOAZI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_AZESLBIN)
        bRes2=.link_5_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDEAZI_2_2 as StdField with uid="SVYDUYTDIB",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DEAZI", cQueryName = "DEAZI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 90497590,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=226, Top=10, InputMask=replicate('X',80)

  add object oAZVALEUR_2_3 as StdField with uid="NCCHBUHLRR",rtseq=36,rtrep=.f.,;
    cFormVar = "w_AZVALEUR", cQueryName = "AZVALEUR",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta di conto",;
    HelpContextID = 92453032,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=226, Top=40, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_AZVALEUR"

  func oAZVALEUR_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZVALEUR_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZVALEUR_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oAZVALEUR_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oAZVALEUR_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_AZVALEUR
     i_obj.ecpSave()
  endproc

  add object oDESEUR_2_4 as StdField with uid="HOLLMXWLDJ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESEUR", cQueryName = "DESEUR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 135332406,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=282, Top=40, InputMask=replicate('X',35)

  add object oAZVALLIR_2_5 as StdField with uid="WFTZDBMOXY",rtseq=38,rtrep=.f.,;
    cFormVar = "w_AZVALLIR", cQueryName = "AZVALLIR",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta nazionale",;
    HelpContextID = 243447976,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=226, Top=67, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_AZVALLIR"

  func oAZVALLIR_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZVALLIR_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZVALLIR_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oAZVALLIR_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oAZVALLIR_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_AZVALLIR
     i_obj.ecpSave()
  endproc

  add object oDESLIR_2_6 as StdField with uid="TZLYVIAQCY",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESLIR", cQueryName = "DESLIR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 123208246,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=282, Top=67, InputMask=replicate('X',35)


  add object oAZUNIUTE_2_7 as StdCombo with uid="GYRYAPQHTO",rtseq=40,rtrep=.f.,left=226,top=126,width=187,height=21;
    , ToolTipText = "Definisce la sequenza del progressivo n.registrazione di primanota";
    , HelpContextID = 94750901;
    , cFormVar="w_AZUNIUTE",RowSource=""+"Unico (x esercizio),"+"Utente,"+"Giorno,"+"Giorno+utente", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAZUNIUTE_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'U',;
    iif(this.value =3,'G',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oAZUNIUTE_2_7.GetRadio()
    this.Parent.oContained.w_AZUNIUTE = this.RadioValue()
    return .t.
  endfunc

  func oAZUNIUTE_2_7.SetRadio()
    this.Parent.oContained.w_AZUNIUTE=trim(this.Parent.oContained.w_AZUNIUTE)
    this.value = ;
      iif(this.Parent.oContained.w_AZUNIUTE=='S',1,;
      iif(this.Parent.oContained.w_AZUNIUTE=='U',2,;
      iif(this.Parent.oContained.w_AZUNIUTE=='G',3,;
      iif(this.Parent.oContained.w_AZUNIUTE=='E',4,;
      0))))
  endfunc

  add object oAZCFNUME_2_8 as StdCheck with uid="WVJKYQEGSK",rtseq=41,rtrep=.f.,left=434, top=126, caption="Codifica cli/for numerica",;
    ToolTipText = "Se attivo: abillita la gestione progressiva dei codici clienti/fornitori in formato numerico",;
    HelpContextID = 178329419,;
    cFormVar="w_AZCFNUME", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZCFNUME_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZCFNUME_2_8.GetRadio()
    this.Parent.oContained.w_AZCFNUME = this.RadioValue()
    return .t.
  endfunc

  func oAZCFNUME_2_8.SetRadio()
    this.Parent.oContained.w_AZCFNUME=trim(this.Parent.oContained.w_AZCFNUME)
    this.value = ;
      iif(this.Parent.oContained.w_AZCFNUME=='S',1,;
      0)
  endfunc


  add object oAZTRAEXP_2_9 as StdCombo with uid="LDFMCYBVXK",rtseq=42,rtrep=.f.,left=226,top=156,width=187,height=21;
    , ToolTipText = "Abilita il trasferimento verso apri specificando il tipo di export";
    , HelpContextID = 102881450;
    , cFormVar="w_AZTRAEXP",RowSource=""+"Non attivo,"+"Export semplificato,"+"Export avanzato cogen,"+"Export semplificato cblembi,"+"Export avanzato contb,"+"Export AGO Infinity", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAZTRAEXP_2_9.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    iif(this.value =4,'B',;
    iif(this.value =5,'C',;
    iif(this.value =6,'G',;
    space(1))))))))
  endfunc
  func oAZTRAEXP_2_9.GetRadio()
    this.Parent.oContained.w_AZTRAEXP = this.RadioValue()
    return .t.
  endfunc

  func oAZTRAEXP_2_9.SetRadio()
    this.Parent.oContained.w_AZTRAEXP=trim(this.Parent.oContained.w_AZTRAEXP)
    this.value = ;
      iif(this.Parent.oContained.w_AZTRAEXP=='N',1,;
      iif(this.Parent.oContained.w_AZTRAEXP=='S',2,;
      iif(this.Parent.oContained.w_AZTRAEXP=='A',3,;
      iif(this.Parent.oContained.w_AZTRAEXP=='B',4,;
      iif(this.Parent.oContained.w_AZTRAEXP=='C',5,;
      iif(this.Parent.oContained.w_AZTRAEXP=='G',6,;
      0))))))
  endfunc

  func oAZTRAEXP_2_9.mHide()
    with this.Parent.oContained
      return (IsAlt() and g_COGE<>'S')
    endwith
  endfunc


  add object oAZTRAEXP_2_10 as StdCombo with uid="WSPTZHXJUU",rtseq=43,rtrep=.f.,left=226,top=156,width=187,height=21;
    , ToolTipText = "Abilita il trasferimento verso apri specificando il tipo di export";
    , HelpContextID = 102881450;
    , cFormVar="w_AZTRAEXP",RowSource=""+"Non attivo,"+"Export semplificato,"+"Export avanzato cogen,"+"Export semplificato cblembi,"+"Export avanzato lemse", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAZTRAEXP_2_10.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    iif(this.value =4,'B',;
    iif(this.value =5,'C',;
    space(1)))))))
  endfunc
  func oAZTRAEXP_2_10.GetRadio()
    this.Parent.oContained.w_AZTRAEXP = this.RadioValue()
    return .t.
  endfunc

  func oAZTRAEXP_2_10.SetRadio()
    this.Parent.oContained.w_AZTRAEXP=trim(this.Parent.oContained.w_AZTRAEXP)
    this.value = ;
      iif(this.Parent.oContained.w_AZTRAEXP=='N',1,;
      iif(this.Parent.oContained.w_AZTRAEXP=='S',2,;
      iif(this.Parent.oContained.w_AZTRAEXP=='A',3,;
      iif(this.Parent.oContained.w_AZTRAEXP=='B',4,;
      iif(this.Parent.oContained.w_AZTRAEXP=='C',5,;
      0)))))
  endfunc

  func oAZTRAEXP_2_10.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR g_COGE='S')
    endwith
  endfunc


  add object oAZTIPLOC_2_11 as StdTableCombo with uid="NRIWFURTYL",rtseq=44,rtrep=.f.,left=226,top=186,width=187,height=21;
    , ToolTipText = "Identifica la localizzazione contabile e fiscale dell'azienda";
    , HelpContextID = 29697865;
    , cFormVar="w_AZTIPLOC",tablefilter="", bObbl = .f. , nPag = 2;
    , cTable='LOCALIZZAZ',cKey='LZCODICE',cValue='LZLOCDES',cOrderBy='LZCODICE',xDefault=space(3);
  , bGlobalFont=.t.


  add object oAZDATTRA_2_12 as StdField with uid="LHMHJMCGJF",rtseq=45,rtrep=.f.,;
    cFormVar = "w_AZDATTRA", cQueryName = "AZDATTRA",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data attivazione export avanzato",;
    HelpContextID = 100915385,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=568, Top=154

  func oAZDATTRA_2_12.mHide()
    with this.Parent.oContained
      return (! inlist(.w_AZTRAEXP,'A','C','G'))
    endwith
  endfunc

  add object oAZQUADRA_2_13 as StdCheck with uid="QMGUIBNXDX",rtseq=46,rtrep=.f.,left=13, top=219, caption="Quadratura p.nota",;
    ToolTipText = "Accetta solo registrazioni primanota con quadratura",;
    HelpContextID = 148961095,;
    cFormVar="w_AZQUADRA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZQUADRA_2_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZQUADRA_2_13.GetRadio()
    this.Parent.oContained.w_AZQUADRA = this.RadioValue()
    return .t.
  endfunc

  func oAZQUADRA_2_13.SetRadio()
    this.Parent.oContained.w_AZQUADRA=trim(this.Parent.oContained.w_AZQUADRA)
    this.value = ;
      iif(this.Parent.oContained.w_AZQUADRA=='S',1,;
      0)
  endfunc

  add object oAZFLUNIV_2_14 as StdCheck with uid="WZPCWXYOYP",rtseq=47,rtrep=.f.,left=13, top=237, caption="Controllo univocit�",;
    ToolTipText = "Rende bloccante il controllo univocit� in primanota e documenti",;
    HelpContextID = 199800996,;
    cFormVar="w_AZFLUNIV", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZFLUNIV_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZFLUNIV_2_14.GetRadio()
    this.Parent.oContained.w_AZFLUNIV = this.RadioValue()
    return .t.
  endfunc

  func oAZFLUNIV_2_14.SetRadio()
    this.Parent.oContained.w_AZFLUNIV=trim(this.Parent.oContained.w_AZFLUNIV)
    this.value = ;
      iif(this.Parent.oContained.w_AZFLUNIV=='S',1,;
      0)
  endfunc

  add object oAZFLARDO_2_15 as StdCheck with uid="INIQCAXWAK",rtseq=48,rtrep=.f.,left=13, top=255, caption="Archiv. documentale",;
    ToolTipText = "Attiva funzionalit� di archiviazione documentale",;
    HelpContextID = 153663659,;
    cFormVar="w_AZFLARDO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZFLARDO_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZFLARDO_2_15.GetRadio()
    this.Parent.oContained.w_AZFLARDO = this.RadioValue()
    return .t.
  endfunc

  func oAZFLARDO_2_15.SetRadio()
    this.Parent.oContained.w_AZFLARDO=trim(this.Parent.oContained.w_AZFLARDO)
    this.value = ;
      iif(this.Parent.oContained.w_AZFLARDO=='S',1,;
      0)
  endfunc

  func oAZFLARDO_2_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZPERPAR_2_16 as StdCheck with uid="OOCXDZAZNE",rtseq=49,rtrep=.f.,left=13, top=273, caption="Gestione partite",;
    ToolTipText = "Abilita la gestione partite, obbligatorie per gestire l'IVA ad esigibilit� diff.",;
    HelpContextID = 98625368,;
    cFormVar="w_AZPERPAR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZPERPAR_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZPERPAR_2_16.GetRadio()
    this.Parent.oContained.w_AZPERPAR = this.RadioValue()
    return .t.
  endfunc

  func oAZPERPAR_2_16.SetRadio()
    this.Parent.oContained.w_AZPERPAR=trim(this.Parent.oContained.w_AZPERPAR)
    this.value = ;
      iif(this.Parent.oContained.w_AZPERPAR=='S',1,;
      0)
  endfunc

  add object oAZCCRICA_2_18 as StdCheck with uid="XMKUWRMVXF",rtseq=51,rtrep=.f.,left=13, top=291, caption="Contab. analitica",;
    ToolTipText = "Abilita la contabilit� analitica a livello di primanota/documenti",;
    HelpContextID = 18999481,;
    cFormVar="w_AZCCRICA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZCCRICA_2_18.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZCCRICA_2_18.GetRadio()
    this.Parent.oContained.w_AZCCRICA = this.RadioValue()
    return .t.
  endfunc

  func oAZCCRICA_2_18.SetRadio()
    this.Parent.oContained.w_AZCCRICA=trim(this.Parent.oContained.w_AZCCRICA)
    this.value = ;
      iif(this.Parent.oContained.w_AZCCRICA=='S',1,;
      0)
  endfunc

  add object oAZGESCOM_2_19 as StdCheck with uid="TEWZUKYNHH",rtseq=52,rtrep=.f.,left=13, top=309, caption="Analitica commesse",;
    ToolTipText = "Se attivata la contabilit� analitica, abilita la gestione commesse",;
    HelpContextID = 149968723,;
    cFormVar="w_AZGESCOM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZGESCOM_2_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZGESCOM_2_19.GetRadio()
    this.Parent.oContained.w_AZGESCOM = this.RadioValue()
    return .t.
  endfunc

  func oAZGESCOM_2_19.SetRadio()
    this.Parent.oContained.w_AZGESCOM=trim(this.Parent.oContained.w_AZGESCOM)
    this.value = ;
      iif(this.Parent.oContained.w_AZGESCOM=='S',1,;
      0)
  endfunc

  add object oAZRIPCOM_2_20 as StdCheck with uid="JBZTHLEECY",rtseq=53,rtrep=.f.,left=13, top=327, caption="Ripartizione analitica per competenza",;
    ToolTipText = "Se attivo: abilita la ripartizione per c. C/R per data competenza",;
    HelpContextID = 147130195,;
    cFormVar="w_AZRIPCOM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZRIPCOM_2_20.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZRIPCOM_2_20.GetRadio()
    this.Parent.oContained.w_AZRIPCOM = this.RadioValue()
    return .t.
  endfunc

  func oAZRIPCOM_2_20.SetRadio()
    this.Parent.oContained.w_AZRIPCOM=trim(this.Parent.oContained.w_AZRIPCOM)
    this.value = ;
      iif(this.Parent.oContained.w_AZRIPCOM=='S',1,;
      0)
  endfunc

  func oAZRIPCOM_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZCCRICA='S')
    endwith
   endif
  endfunc

  add object oAZFLRIPC_2_21 as StdCheck with uid="CNOFPLQVGD",rtseq=54,rtrep=.f.,left=13, top=345, caption="Ripartizione analitica su commessa",;
    ToolTipText = "Se attivo: abilita la ripartizione per c. C/R su commessa",;
    HelpContextID = 250038089,;
    cFormVar="w_AZFLRIPC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZFLRIPC_2_21.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZFLRIPC_2_21.GetRadio()
    this.Parent.oContained.w_AZFLRIPC = this.RadioValue()
    return .t.
  endfunc

  func oAZFLRIPC_2_21.SetRadio()
    this.Parent.oContained.w_AZFLRIPC=trim(this.Parent.oContained.w_AZFLRIPC)
    this.value = ;
      iif(this.Parent.oContained.w_AZFLRIPC=='S',1,;
      0)
  endfunc

  func oAZFLRIPC_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZCCRICA='S' AND .w_AZGESCOM='S')
    endwith
   endif
  endfunc

  add object oAZDETCON_2_22 as StdCheck with uid="PLYOGAHPKC",rtseq=55,rtrep=.f.,left=226, top=219, caption="Dett. IVA per contropartite",;
    ToolTipText = "Se attivo: castelletto primanota IVA dettagliato per contropartita",;
    HelpContextID = 151005012,;
    cFormVar="w_AZDETCON", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZDETCON_2_22.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZDETCON_2_22.GetRadio()
    this.Parent.oContained.w_AZDETCON = this.RadioValue()
    return .t.
  endfunc

  func oAZDETCON_2_22.SetRadio()
    this.Parent.oContained.w_AZDETCON=trim(this.Parent.oContained.w_AZDETCON)
    this.value = ;
      iif(this.Parent.oContained.w_AZDETCON=='S',1,;
      0)
  endfunc

  func oAZDETCON_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not .w_AZTRAEXP$'A-C')
    endwith
   endif
  endfunc

  add object oAZFLVEBD_2_23 as StdCheck with uid="QFPTAXSHRY",rtseq=56,rtrep=.f.,left=226, top=237, caption="Vendita beni deperibili",;
    ToolTipText = "Default utilizzato nella creazione delle partite legate al cliente (documenti, primanota e scadenze diverse)",;
    HelpContextID = 187123530,;
    cFormVar="w_AZFLVEBD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZFLVEBD_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZFLVEBD_2_23.GetRadio()
    this.Parent.oContained.w_AZFLVEBD = this.RadioValue()
    return .t.
  endfunc

  func oAZFLVEBD_2_23.SetRadio()
    this.Parent.oContained.w_AZFLVEBD=trim(this.Parent.oContained.w_AZFLVEBD)
    this.value = ;
      iif(this.Parent.oContained.w_AZFLVEBD=='S',1,;
      0)
  endfunc

  func oAZFLVEBD_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERPAR='S')
    endwith
   endif
  endfunc

  func oAZFLVEBD_2_23.mHide()
    with this.Parent.oContained
      return (.w_AZPERPAR<>'S' OR IsAlt())
    endwith
  endfunc

  add object oAZFLFIDO_2_24 as StdCheck with uid="UJZCRRIGXT",rtseq=57,rtrep=.f.,left=226, top=256, caption="Controllo fido",;
    ToolTipText = "Abilita il controllo sul fido del cliente",;
    HelpContextID = 30980267,;
    cFormVar="w_AZFLFIDO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZFLFIDO_2_24.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZFLFIDO_2_24.GetRadio()
    this.Parent.oContained.w_AZFLFIDO = this.RadioValue()
    return .t.
  endfunc

  func oAZFLFIDO_2_24.SetRadio()
    this.Parent.oContained.w_AZFLFIDO=trim(this.Parent.oContained.w_AZFLFIDO)
    this.value = ;
      iif(this.Parent.oContained.w_AZFLFIDO=='S',1,;
      0)
  endfunc

  add object oAZGIORIS_2_25 as StdField with uid="SHRMXNBMXU",rtseq=58,rtrep=.f.,;
    cFormVar = "w_AZGIORIS", cQueryName = "AZGIORIS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni di tolleranza nel calcolo rischio cliente",;
    HelpContextID = 139176103,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=397, Top=274, cSayPict='"999"', cGetPict='"999"'

  func oAZGIORIS_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZFLFIDO='S')
    endwith
   endif
  endfunc

  add object oAZFILPAR_2_26 as StdCheck with uid="CRUGKDNNGC",rtseq=59,rtrep=.f.,left=226, top=382, caption="Applica zero filling  nel numero partita",;
    ToolTipText = "Se attivo esegue zero filling sul numero documento nel calcolo del numero partita",;
    HelpContextID = 92555096,;
    cFormVar="w_AZFILPAR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZFILPAR_2_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZFILPAR_2_26.GetRadio()
    this.Parent.oContained.w_AZFILPAR = this.RadioValue()
    return .t.
  endfunc

  func oAZFILPAR_2_26.SetRadio()
    this.Parent.oContained.w_AZFILPAR=trim(this.Parent.oContained.w_AZFILPAR)
    this.value = ;
      iif(this.Parent.oContained.w_AZFILPAR=='S',1,;
      0)
  endfunc


  add object oAZMAXLIV_2_27 as StdCombo with uid="QCAYVBKHVP",rtseq=60,rtrep=.f.,left=568,top=186,width=106,height=21;
    , ToolTipText = "Indicare il livello massimo di dettaglio dei mastri contabili (da 1 a 4)";
    , HelpContextID = 230901924;
    , cFormVar="w_AZMAXLIV",RowSource=""+"Uno,"+"Due,"+"Tre,"+"Quattro", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oAZMAXLIV_2_27.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    0)))))
  endfunc
  func oAZMAXLIV_2_27.GetRadio()
    this.Parent.oContained.w_AZMAXLIV = this.RadioValue()
    return .t.
  endfunc

  func oAZMAXLIV_2_27.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AZMAXLIV==1,1,;
      iif(this.Parent.oContained.w_AZMAXLIV==2,2,;
      iif(this.Parent.oContained.w_AZMAXLIV==3,3,;
      iif(this.Parent.oContained.w_AZMAXLIV==4,4,;
      0))))
  endfunc

  add object oAZMASCON_2_28 as StdField with uid="HTMDQNSOPT",rtseq=61,rtrep=.f.,;
    cFormVar = "w_AZMASCON", cQueryName = "AZMASCON",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura input codici conti/clienti/fornitori",;
    HelpContextID = 149731156,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=568, Top=216, InputMask=replicate('X',15)

  func oAZMASCON_2_28.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZMASMCO_2_31 as StdField with uid="WAMOSGXRXH",rtseq=64,rtrep=.f.,;
    cFormVar = "w_AZMASMCO", cQueryName = "AZMASMCO",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura input codici mastri contabili",;
    HelpContextID = 219367595,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=568, Top=244, InputMask=replicate('X',15)

  add object oAZMASCCC_2_32 as StdField with uid="UPPVFPQBOM",rtseq=65,rtrep=.f.,;
    cFormVar = "w_AZMASCCC", cQueryName = "AZMASCCC",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura input codici centri di costo o ricavo",;
    HelpContextID = 118704311,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=568, Top=272, InputMask=replicate('X',15)

  add object oAZMASMCC_2_33 as StdField with uid="TXGOFJAKEA",rtseq=66,rtrep=.f.,;
    cFormVar = "w_AZMASMCC", cQueryName = "AZMASMCC",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura input codici voci di costo o ricavo",;
    HelpContextID = 219367607,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=568, Top=300, InputMask=replicate('X',15)

  add object oAZINTMIN_2_35 as StdField with uid="VUFKCFKMLU",rtseq=67,rtrep=.f.,;
    cFormVar = "w_AZINTMIN", cQueryName = "AZINTMIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Se valorizzato la stampa relativa esclude righe con interessi inferiori all'importo indicato",;
    HelpContextID = 217483436,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=569, Top=328, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oAZSCAPNT_2_36 as StdField with uid="CTMEGVXCYE",rtseq=68,rtrep=.f.,;
    cFormVar = "w_AZSCAPNT", cQueryName = "AZSCAPNT",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Ultimo esercizio scaricato",;
    HelpContextID = 80680794,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=659, Top=412, cSayPict="'XXXX'", cGetPict="'XXXX'", InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZCODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_AZSCAPNT"

  func oAZSCAPNT_2_36.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oAZSCAPNT_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZSCAPNT_2_36.ecpDrop(oSource)
    this.Parent.oContained.link_2_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZSCAPNT_2_36.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZCODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZCODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oAZSCAPNT_2_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oObj_2_54 as cp_calclbl with uid="ZUOQHEGTZI",left=6, top=10, width=56,height=25,;
    caption='StrAzi',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=2;
    , HelpContextID = 245423578

  add object oAZCONCAS_2_55 as StdCheck with uid="EEXALCWUTV",rtseq=169,rtrep=.f.,left=13, top=363, caption="Contabilit� per cassa",;
    ToolTipText = "Contabilit� per cassa",;
    HelpContextID = 145364825,;
    cFormVar="w_AZCONCAS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZCONCAS_2_55.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZCONCAS_2_55.GetRadio()
    this.Parent.oContained.w_AZCONCAS = this.RadioValue()
    return .t.
  endfunc

  func oAZCONCAS_2_55.SetRadio()
    this.Parent.oContained.w_AZCONCAS=trim(this.Parent.oContained.w_AZCONCAS)
    this.value = ;
      iif(this.Parent.oContained.w_AZCONCAS=='S',1,;
      0)
  endfunc

  func oAZCONCAS_2_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oAZCONCAS_2_55.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc


  add object oLinkPC_2_56 as StdButton with uid="HQFSDFPTTA",left=13, top=382, width=48,height=45,;
    caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla gestione del rappresentante firmatario";
    , HelpContextID = 7768310;
    , caption='\<Rappr.',picture='bmp\manuale.bmp';
  , bGlobalFont=.t.

    proc oLinkPC_2_56.Click()
      this.Parent.oContained.GSAR_ADR.LinkPCClick()
    endproc

  func oLinkPC_2_56.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_AZPERAZI # 'S')
      endwith
    endif
  endfunc

  add object oAZFLRAGG_2_58 as StdCheck with uid="YRBCGIEQOF",rtseq=171,rtrep=.f.,left=226, top=409, caption="Blocca accorpa scadenze con dati aggiuntivi",;
    ToolTipText = "Se attivo inibisce l'accorpamento delle scadenze in presenza di dati aggiuntivi",;
    HelpContextID = 152615091,;
    cFormVar="w_AZFLRAGG", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZFLRAGG_2_58.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZFLRAGG_2_58.GetRadio()
    this.Parent.oContained.w_AZFLRAGG = this.RadioValue()
    return .t.
  endfunc

  func oAZFLRAGG_2_58.SetRadio()
    this.Parent.oContained.w_AZFLRAGG=trim(this.Parent.oContained.w_AZFLRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_AZFLRAGG=='S',1,;
      0)
  endfunc


  add object oAZCONCVE_2_59 as StdCombo with uid="HGJPAPPOUP",rtseq=184,rtrep=.f.,left=569,top=356,width=139,height=21;
    , ToolTipText = "Data di contabilizzazione ciclo vendite: unica per tutti i documenti o uguale alla data documento";
    , HelpContextID = 123070645;
    , cFormVar="w_AZCONCVE",RowSource=""+"Unica,"+"Data documento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAZCONCVE_2_59.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oAZCONCVE_2_59.GetRadio()
    this.Parent.oContained.w_AZCONCVE = this.RadioValue()
    return .t.
  endfunc

  func oAZCONCVE_2_59.SetRadio()
    this.Parent.oContained.w_AZCONCVE=trim(this.Parent.oContained.w_AZCONCVE)
    this.value = ;
      iif(this.Parent.oContained.w_AZCONCVE=='U',1,;
      iif(this.Parent.oContained.w_AZCONCVE=='D',2,;
      0))
  endfunc

  func oAZCONCVE_2_59.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc


  add object oAZCONCAC_2_61 as StdCombo with uid="IYXUAVUXLC",rtseq=185,rtrep=.f.,left=569,top=384,width=139,height=21;
    , ToolTipText = "Data di contabilizzazione ciclo acquisti: unica per tutti i documenti o uguale alla data di registrazione del documento";
    , HelpContextID = 145364809;
    , cFormVar="w_AZCONCAC",RowSource=""+"Unica,"+"Data reg. doc.", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAZCONCAC_2_61.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oAZCONCAC_2_61.GetRadio()
    this.Parent.oContained.w_AZCONCAC = this.RadioValue()
    return .t.
  endfunc

  func oAZCONCAC_2_61.SetRadio()
    this.Parent.oContained.w_AZCONCAC=trim(this.Parent.oContained.w_AZCONCAC)
    this.value = ;
      iif(this.Parent.oContained.w_AZCONCAC=='U',1,;
      iif(this.Parent.oContained.w_AZCONCAC=='D',2,;
      0))
  endfunc

  func oAZCONCAC_2_61.mHide()
    with this.Parent.oContained
      return (g_ACQU<>'S' and isalt())
    endwith
  endfunc

  add object oStr_2_34 as StdString with uid="PVSZGQAYIU",Visible=.t., Left=135, Top=10,;
    Alignment=1, Width=88, Height=15,;
    Caption="Rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="BPNGYIWNWG",Visible=.t., Left=5, Top=96,;
    Alignment=0, Width=226, Height=15,;
    Caption="Contabilit� generale"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="GEGQZWZTSX",Visible=.t., Left=434, Top=96,;
    Alignment=0, Width=273, Height=15,;
    Caption="Strutture codici"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="EJYMUKHPGC",Visible=.t., Left=409, Top=221,;
    Alignment=1, Width=155, Height=15,;
    Caption="Conti/clienti/fornitori:"  ;
  , bGlobalFont=.t.

  func oStr_2_40.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_41 as StdString with uid="SGVQGPPYQI",Visible=.t., Left=430, Top=274,;
    Alignment=1, Width=134, Height=15,;
    Caption="Centri di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="GCJNDNGZTO",Visible=.t., Left=430, Top=248,;
    Alignment=1, Width=134, Height=15,;
    Caption="Mastri:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="AIGUWHNHEX",Visible=.t., Left=430, Top=302,;
    Alignment=1, Width=134, Height=15,;
    Caption="Voci c.di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="LZRBJCIOAC",Visible=.t., Left=502, Top=412,;
    Alignment=1, Width=154, Height=15,;
    Caption="Ultimo scarico primanota:"  ;
  , bGlobalFont=.t.

  func oStr_2_45.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_46 as StdString with uid="BASNYRTXRG",Visible=.t., Left=84, Top=40,;
    Alignment=1, Width=133, Height=18,;
    Caption="Valuta di conto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="UXDZLDPVNB",Visible=.t., Left=84, Top=67,;
    Alignment=1, Width=133, Height=18,;
    Caption="Valuta nazionale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="GYMXMATDNN",Visible=.t., Left=393, Top=189,;
    Alignment=1, Width=171, Height=15,;
    Caption="N.livelli mastri contabili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="XPBTKLBDXV",Visible=.t., Left=212, Top=274,;
    Alignment=1, Width=183, Height=15,;
    Caption="Giorni tolleranza rischio cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="XCPTUBJHWW",Visible=.t., Left=75, Top=126,;
    Alignment=1, Width=142, Height=15,;
    Caption="Progressivo P.N.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="DDUFTVJRWJ",Visible=.t., Left=75, Top=155,;
    Alignment=1, Width=142, Height=18,;
    Caption="Trasf. commercialisti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="IJHDTGQMUF",Visible=.t., Left=377, Top=329,;
    Alignment=1, Width=187, Height=18,;
    Caption="Importo min. interessi di mora:"  ;
  , bGlobalFont=.t.

  add object oStr_2_53 as StdString with uid="IIDHWONNUA",Visible=.t., Left=75, Top=186,;
    Alignment=1, Width=142, Height=18,;
    Caption="Localizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="JGQPALJUAQ",Visible=.t., Left=391, Top=155,;
    Alignment=1, Width=173, Height=18,;
    Caption="Data attivazione trasf.:"  ;
  , bGlobalFont=.t.

  func oStr_2_57.mHide()
    with this.Parent.oContained
      return (! inlist(.w_AZTRAEXP,'A','C','G'))
    endwith
  endfunc

  add object oStr_2_60 as StdString with uid="RUEHIMBQUH",Visible=.t., Left=428, Top=356,;
    Alignment=1, Width=136, Height=18,;
    Caption="Ciclo vendite:"  ;
  , bGlobalFont=.t.

  func oStr_2_60.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_2_62 as StdString with uid="HUPOOFEVFB",Visible=.t., Left=461, Top=384,;
    Alignment=1, Width=103, Height=18,;
    Caption="Ciclo acquisti:"  ;
  , bGlobalFont=.t.

  func oStr_2_62.mHide()
    with this.Parent.oContained
      return (g_ACQU<>'S' and isalt())
    endwith
  endfunc

  add object oBox_2_38 as StdBox with uid="KHOSIEFHVH",left=5, top=113, width=407,height=1

  add object oBox_2_43 as StdBox with uid="VMTIJYUIVN",left=425, top=112, width=285,height=1
enddefine
define class tgsar_aziPag3 as StdContainer
  Width  = 719
  height = 492
  stdWidth  = 719
  stdheight = 492
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOAZI_3_1 as StdField with uid="QFUQCQGHBX",rtseq=69,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90500134,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=10, InputMask=replicate('X',5)

  func oCOAZI_3_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_AZESLBIN)
        bRes2=.link_5_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDEAZI_3_2 as StdField with uid="XCLPGAOXWI",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DEAZI", cQueryName = "DEAZI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 90497590,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=226, Top=10, InputMask=replicate('X',80)

  add object oAZDATINI_3_4 as StdField with uid="XBQVETTYPR",rtseq=71,rtrep=.f.,;
    cFormVar = "w_AZDATINI", cQueryName = "AZDATINI",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data a partire dalla quale la procedura inizia a generare gli elenchi intra di tipo beni e di tipo servizi",;
    HelpContextID = 251406159,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=317, Top=42

  add object oAZDATSER_3_5 as StdField with uid="NLQHBGFOTN",rtseq=72,rtrep=.f.,;
    cFormVar = "w_AZDATSER", cQueryName = "AZDATSER",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data a partire dalla quale la procedura inizia a generare gli elenchi intra di tipo servizi",;
    HelpContextID = 117692584,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=317, Top=69

  add object oAZNPRGEL_3_6 as StdField with uid="JTCOYZRCRH",rtseq=73,rtrep=.f.,;
    cFormVar = "w_AZNPRGEL", cQueryName = "AZNPRGEL",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo elenchi presentati su floppy",;
    HelpContextID = 51656878,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=258, Top=100

  add object oAZPRESOB_3_7 as StdField with uid="GBXZBWJEWU",rtseq=74,rtrep=.f.,;
    cFormVar = "w_AZPRESOB", cQueryName = "AZPRESOB",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso telefonico del soggetto obbligato",;
    HelpContextID = 136177480,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=447, Top=100, InputMask=replicate('X',5)

  add object oAZTELSOB_3_8 as StdField with uid="LEAYHWBOFW",rtseq=75,rtrep=.f.,;
    cFormVar = "w_AZTELSOB", cQueryName = "AZTELSOB",;
    bObbl = .f. , nPag = 3, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero telefonico del soggetto obbligato",;
    HelpContextID = 142681928,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=517, Top=100, InputMask=replicate('X',18)

  add object oAZCODPOR_3_9 as StdField with uid="QMFVPSKFGZ",rtseq=76,rtrep=.f.,;
    cFormVar = "w_AZCODPOR", cQueryName = "AZCODPOR",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice porto/aeroporto da dove viene spedita la merce ai clienti",;
    HelpContextID = 84547416,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=165, Top=333, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COD_AREO", cZoomOnZoom="GLES_APP", oKey_1_1="PPCODICE", oKey_1_2="this.w_AZCODPOR"

  func oAZCODPOR_3_9.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  func oAZCODPOR_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCODPOR_3_9.ecpDrop(oSource)
    this.Parent.oContained.link_3_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCODPOR_3_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_AREO','*','PPCODICE',cp_AbsName(this.parent,'oAZCODPOR_3_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GLES_APP',"Porti/aeroporti",'',this.parent.oContained
  endproc
  proc oAZCODPOR_3_9.mZoomOnZoom
    local i_obj
    i_obj=GLES_APP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PPCODICE=this.parent.oContained.w_AZCODPOR
     i_obj.ecpSave()
  endproc


  add object oAZDTOBBL_3_10 as StdCombo with uid="ETXIWHMZJT",rtseq=77,rtrep=.f.,left=165,top=365,width=141,height=21;
    , ToolTipText = "Test obbligatoriet� dati statistici (solo per periodicit� mensile)";
    , HelpContextID = 129967954;
    , cFormVar="w_AZDTOBBL",RowSource=""+"Nessuna,"+"Solo cessioni UE,"+"Solo acquisti UE,"+"Acquisti e cessioni UE", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oAZDTOBBL_3_10.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'C',;
    iif(this.value =3,'A',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oAZDTOBBL_3_10.GetRadio()
    this.Parent.oContained.w_AZDTOBBL = this.RadioValue()
    return .t.
  endfunc

  func oAZDTOBBL_3_10.SetRadio()
    this.Parent.oContained.w_AZDTOBBL=trim(this.Parent.oContained.w_AZDTOBBL)
    this.value = ;
      iif(this.Parent.oContained.w_AZDTOBBL=='N',1,;
      iif(this.Parent.oContained.w_AZDTOBBL=='C',2,;
      iif(this.Parent.oContained.w_AZDTOBBL=='A',3,;
      iif(this.Parent.oContained.w_AZDTOBBL=='E',4,;
      0))))
  endfunc

  func oAZDTOBBL_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZAITIPE='M' OR .w_AZAITIPV='M')
    endwith
   endif
  endfunc

  add object oDESCRI_3_17 as StdField with uid="NDMSYOBCLT",rtseq=167,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 249496118,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=252, Top=333, InputMask=replicate('X',40)

  func oDESCRI_3_17.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc


  add object oObj_3_18 as cp_calclbl with uid="VRVTUCWFEN",left=6, top=10, width=56,height=25,;
    caption='StrAzi',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=3;
    , HelpContextID = 245423578


  add object oBtn_3_21 as StdButton with uid="WCOHOMPNHV",left=8, top=447, width=48,height=45,;
    CpPicture="bmp\manuale.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per accedere ai dati del soggetto delegato";
    , HelpContextID = 63218251;
    , caption='\<Sogg.Del.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_21.Click()
      do GSAR_KSO with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oLinkPC_3_36 as stdDynamicChildContainer with uid="NLVAQLONZI",left=3, top=139, width=713, height=174, bOnScreen=.t.;


  add object oStr_3_3 as StdString with uid="VTCDCRLZPV",Visible=.t., Left=133, Top=10,;
    Alignment=1, Width=90, Height=15,;
    Caption="Rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="OXPNMXHZUB",Visible=.t., Left=110, Top=100,;
    Alignment=1, Width=144, Height=15,;
    Caption="N. progr. elenco:"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="CCZXGNWLYS",Visible=.t., Left=503, Top=100,;
    Alignment=2, Width=12, Height=15,;
    Caption="-"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="JKLNTOWKCV",Visible=.t., Left=323, Top=100,;
    Alignment=1, Width=120, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_3_14 as StdString with uid="HWLJZMKGMR",Visible=.t., Left=17, Top=365,;
    Alignment=1, Width=144, Height=15,;
    Caption="Obbligatoriet�:"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="AIKFIXEKQV",Visible=.t., Left=325, Top=365,;
    Alignment=0, Width=378, Height=18,;
    Caption="(Condizione di consegna, modalit� di trasporto, valore statistico)"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="OEWKKMPQBW",Visible=.t., Left=11, Top=333,;
    Alignment=1, Width=154, Height=18,;
    Caption="Codice porto/aeroporto:"  ;
  , bGlobalFont=.t.

  func oStr_3_16.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oStr_3_19 as StdString with uid="MDIUCNZHVT",Visible=.t., Left=68, Top=45,;
    Alignment=1, Width=243, Height=18,;
    Caption="Data inizio generazione elenchi INTRA:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="AXXTUAFBVM",Visible=.t., Left=12, Top=72,;
    Alignment=1, Width=299, Height=18,;
    Caption="Data inizio generazione elenchi INTRA per servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="FIQLWMCWGU",Visible=.t., Left=6, Top=422,;
    Alignment=0, Width=188, Height=15,;
    Caption="Dati del soggetto delegato"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_23 as StdBox with uid="CRYYSBWDUC",left=5, top=440, width=151,height=1
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_ait",lower(this.oContained.GSAR_AIT.class))=0
        this.oContained.GSAR_AIT.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsar_aziPag4 as StdContainer
  Width  = 719
  height = 492
  stdWidth  = 719
  stdheight = 492
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOAZI_4_1 as StdField with uid="SHCRJYSECL",rtseq=78,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90500134,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=10, InputMask=replicate('X',5)

  func oCOAZI_4_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_AZESLBIN)
        bRes2=.link_5_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDEAZI_4_2 as StdField with uid="LLHBLGHWMS",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DEAZI", cQueryName = "DEAZI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 90497590,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=226, Top=10, InputMask=replicate('X',80)

  add object oAZMAGAZI_4_4 as StdField with uid="XEVSICGPKQ",rtseq=80,rtrep=.f.,;
    cFormVar = "w_AZMAGAZI", cQueryName = "AZMAGAZI",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice del deposito principale",;
    HelpContextID = 164841649,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=132, Top=43, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_AZMAGAZI"

  func oAZMAGAZI_4_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oAZMAGAZI_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZMAGAZI_4_4.ecpDrop(oSource)
    this.Parent.oContained.link_4_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZMAGAZI_4_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oAZMAGAZI_4_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"",'',this.parent.oContained
  endproc
  proc oAZMAGAZI_4_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_AZMAGAZI
     i_obj.ecpSave()
  endproc

  add object oDESMAG_4_5 as StdField with uid="IIKYGWKMYV",rtseq=81,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 198771254,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=197, Top=43, InputMask=replicate('X',30)

  func oDESMAG_4_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZSCAMAG_4_6 as StdField with uid="OFBRSNYRGJ",rtseq=82,rtrep=.f.,;
    cFormVar = "w_AZSCAMAG", cQueryName = "AZSCAMAG",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'ultimo scarico dei movimenti di magazzino",;
    HelpContextID = 30349133,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=623, Top=43

  func oAZSCAMAG_4_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZMAGUTE_4_7 as StdCheck with uid="BRIVSIEMUC",rtseq=83,rtrep=.f.,left=15, top=70, caption="Prog. mov.mag. unico",;
    ToolTipText = "Abilita un unico progressivo movimenti altrimenti un progressivo per utente",;
    HelpContextID = 97732789,;
    cFormVar="w_AZMAGUTE", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZMAGUTE_4_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZMAGUTE_4_7.GetRadio()
    this.Parent.oContained.w_AZMAGUTE = this.RadioValue()
    return .t.
  endfunc

  func oAZMAGUTE_4_7.SetRadio()
    this.Parent.oContained.w_AZMAGUTE=trim(this.Parent.oContained.w_AZMAGUTE)
    this.value = ;
      iif(this.Parent.oContained.w_AZMAGUTE=='S',1,;
      0)
  endfunc

  func oAZMAGUTE_4_7.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZPERAGE_4_8 as StdCheck with uid="VNBROPUUWC",rtseq=84,rtrep=.f.,left=15, top=90, caption="Provvigioni agente",;
    ToolTipText = "Abilita la gestione delle provvigioni agente",;
    HelpContextID = 153032885,;
    cFormVar="w_AZPERAGE", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZPERAGE_4_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZPERAGE_4_8.GetRadio()
    this.Parent.oContained.w_AZPERAGE = this.RadioValue()
    return .t.
  endfunc

  func oAZPERAGE_4_8.SetRadio()
    this.Parent.oContained.w_AZPERAGE=trim(this.Parent.oContained.w_AZPERAGE)
    this.value = ;
      iif(this.Parent.oContained.w_AZPERAGE=='S',1,;
      0)
  endfunc

  func oAZPERAGE_4_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZPERART_4_9 as StdCheck with uid="DXEBWWYCSK",rtseq=85,rtrep=.f.,left=15, top=110, caption="Car.rapido articoli",;
    ToolTipText = "Abilita il caricamento rapido degli articoli, riproponendo i dati principali",;
    HelpContextID = 115402586,;
    cFormVar="w_AZPERART", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZPERART_4_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZPERART_4_9.GetRadio()
    this.Parent.oContained.w_AZPERART = this.RadioValue()
    return .t.
  endfunc

  func oAZPERART_4_9.SetRadio()
    this.Parent.oContained.w_AZPERART=trim(this.Parent.oContained.w_AZPERART)
    this.value = ;
      iif(this.Parent.oContained.w_AZPERART=='S',1,;
      0)
  endfunc

  func oAZPERART_4_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZGESCON_4_10 as StdCheck with uid="GJNRGAIACP",rtseq=86,rtrep=.f.,left=15, top=130, caption="Gestione contratti",;
    ToolTipText = "Abilita la gestione dei contratti",;
    HelpContextID = 149968724,;
    cFormVar="w_AZGESCON", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZGESCON_4_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZGESCON_4_10.GetRadio()
    this.Parent.oContained.w_AZGESCON = this.RadioValue()
    return .t.
  endfunc

  func oAZGESCON_4_10.SetRadio()
    this.Parent.oContained.w_AZGESCON=trim(this.Parent.oContained.w_AZGESCON)
    this.value = ;
      iif(this.Parent.oContained.w_AZGESCON=='S',1,;
      0)
  endfunc

  add object oAZPERDIS_4_11 as StdCheck with uid="WYVQLBGITR",rtseq=87,rtrep=.f.,left=15, top=150, caption="Controllo disponib.",;
    ToolTipText = "Se attivo: abilita il controllo sulla disponibilit� dell'articolo di magazzino",;
    HelpContextID = 102701223,;
    cFormVar="w_AZPERDIS", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZPERDIS_4_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZPERDIS_4_11.GetRadio()
    this.Parent.oContained.w_AZPERDIS = this.RadioValue()
    return .t.
  endfunc

  func oAZPERDIS_4_11.SetRadio()
    this.Parent.oContained.w_AZPERDIS=trim(this.Parent.oContained.w_AZPERDIS)
    this.value = ;
      iif(this.Parent.oContained.w_AZPERDIS=='S',1,;
      0)
  endfunc

  func oAZPERDIS_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZGESMAT<>'S')
    endwith
   endif
  endfunc

  func oAZPERDIS_4_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZPERSDA_4_12 as StdCheck with uid="QICZALHJGR",rtseq=88,rtrep=.f.,left=15, top=170, caption="Des.agg. articoli",;
    ToolTipText = "La descrizione supplementare dell'articolo viene riportata sui documenti",;
    HelpContextID = 119478457,;
    cFormVar="w_AZPERSDA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZPERSDA_4_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZPERSDA_4_12.GetRadio()
    this.Parent.oContained.w_AZPERSDA = this.RadioValue()
    return .t.
  endfunc

  func oAZPERSDA_4_12.SetRadio()
    this.Parent.oContained.w_AZPERSDA=trim(this.Parent.oContained.w_AZPERSDA)
    this.value = ;
      iif(this.Parent.oContained.w_AZPERSDA=='S',1,;
      0)
  endfunc

  func oAZPERSDA_4_12.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZPERORN_4_13 as StdCheck with uid="MYLTQIFPAB",rtseq=89,rtrep=.f.,left=15, top=190, caption="Consegna x conto di",;
    ToolTipText = "Se attivo: abilita l'import dei documenti consegnati per conto di..",;
    HelpContextID = 186587308,;
    cFormVar="w_AZPERORN", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZPERORN_4_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZPERORN_4_13.GetRadio()
    this.Parent.oContained.w_AZPERORN = this.RadioValue()
    return .t.
  endfunc

  func oAZPERORN_4_13.SetRadio()
    this.Parent.oContained.w_AZPERORN=trim(this.Parent.oContained.w_AZPERORN)
    this.value = ;
      iif(this.Parent.oContained.w_AZPERORN=='S',1,;
      0)
  endfunc

  func oAZPERORN_4_13.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZGESLOT_4_14 as StdCheck with uid="YPQLYVWWQO",rtseq=90,rtrep=.f.,left=15, top=210, caption="Gestione lotti",;
    ToolTipText = "Abilita la gestione dei lotti di magazzino",;
    HelpContextID = 32528218,;
    cFormVar="w_AZGESLOT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZGESLOT_4_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZGESLOT_4_14.GetRadio()
    this.Parent.oContained.w_AZGESLOT = this.RadioValue()
    return .t.
  endfunc

  func oAZGESLOT_4_14.SetRadio()
    this.Parent.oContained.w_AZGESLOT=trim(this.Parent.oContained.w_AZGESLOT)
    this.value = ;
      iif(this.Parent.oContained.w_AZGESLOT=='S',1,;
      0)
  endfunc

  func oAZGESLOT_4_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S')
    endwith
   endif
  endfunc

  func oAZGESLOT_4_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZGESUBI_4_15 as StdCheck with uid="DUHAFUFIJW",rtseq=91,rtrep=.f.,left=15, top=230, caption="Gestione ubicazioni",;
    ToolTipText = "Abilita la gestione delle ubicazioni di magazzino",;
    HelpContextID = 84912305,;
    cFormVar="w_AZGESUBI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZGESUBI_4_15.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZGESUBI_4_15.GetRadio()
    this.Parent.oContained.w_AZGESUBI = this.RadioValue()
    return .t.
  endfunc

  func oAZGESUBI_4_15.SetRadio()
    this.Parent.oContained.w_AZGESUBI=trim(this.Parent.oContained.w_AZGESUBI)
    this.value = ;
      iif(this.Parent.oContained.w_AZGESUBI=='S',1,;
      0)
  endfunc

  func oAZGESUBI_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S')
    endwith
   endif
  endfunc

  func oAZGESUBI_4_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZGESMAT_4_16 as StdCheck with uid="UBSZRRCJGL",rtseq=92,rtrep=.f.,left=15, top=250, caption="Gestione matricole",;
    ToolTipText = "Abilita la gestione delle matricole",;
    HelpContextID = 49305434,;
    cFormVar="w_AZGESMAT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZGESMAT_4_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZGESMAT_4_16.GetRadio()
    this.Parent.oContained.w_AZGESMAT = this.RadioValue()
    return .t.
  endfunc

  func oAZGESMAT_4_16.SetRadio()
    this.Parent.oContained.w_AZGESMAT=trim(this.Parent.oContained.w_AZGESMAT)
    this.value = ;
      iif(this.Parent.oContained.w_AZGESMAT=='S',1,;
      0)
  endfunc

  func oAZGESMAT_4_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S')
    endwith
   endif
  endfunc

  func oAZGESMAT_4_16.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZFLCAMB_4_17 as StdCheck with uid="AQFWRSVCDT",rtseq=93,rtrep=.f.,left=15, top=270, caption="Cambio di origine",;
    ToolTipText = "Se attivo: in generazione fatture differite viene utilizzato il cambio del documento di origine",;
    HelpContextID = 100091720,;
    cFormVar="w_AZFLCAMB", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZFLCAMB_4_17.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZFLCAMB_4_17.GetRadio()
    this.Parent.oContained.w_AZFLCAMB = this.RadioValue()
    return .t.
  endfunc

  func oAZFLCAMB_4_17.SetRadio()
    this.Parent.oContained.w_AZFLCAMB=trim(this.Parent.oContained.w_AZFLCAMB)
    this.value = ;
      iif(this.Parent.oContained.w_AZFLCAMB=='S',1,;
      0)
  endfunc

  func oAZFLCAMB_4_17.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZRATEVA_4_18 as StdCheck with uid="RRYHWXPLBD",rtseq=94,rtrep=.f.,left=15, top=291, caption="Rate ordini per data evasione",;
    ToolTipText = "Abilita la generazione rate in base alla data evasione",;
    HelpContextID = 84080825,;
    cFormVar="w_AZRATEVA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZRATEVA_4_18.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZRATEVA_4_18.GetRadio()
    this.Parent.oContained.w_AZRATEVA = this.RadioValue()
    return .t.
  endfunc

  func oAZRATEVA_4_18.SetRadio()
    this.Parent.oContained.w_AZRATEVA=trim(this.Parent.oContained.w_AZRATEVA)
    this.value = ;
      iif(this.Parent.oContained.w_AZRATEVA=='S',1,;
      0)
  endfunc

  func oAZRATEVA_4_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZFLSCOM_4_19 as StdCheck with uid="INMCPGICMM",rtseq=95,rtrep=.f.,left=15, top=311, caption="Calcola sconti su omaggi",;
    ToolTipText = "Se attivo: gli sconti di piede doc. vengono calcolati anche su righe omaggio imp. e omaggio imp. + IVA",;
    HelpContextID = 150423379,;
    cFormVar="w_AZFLSCOM", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZFLSCOM_4_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZFLSCOM_4_19.GetRadio()
    this.Parent.oContained.w_AZFLSCOM = this.RadioValue()
    return .t.
  endfunc

  func oAZFLSCOM_4_19.SetRadio()
    this.Parent.oContained.w_AZFLSCOM=trim(this.Parent.oContained.w_AZFLSCOM)
    this.value = ;
      iif(this.Parent.oContained.w_AZFLSCOM=='S',1,;
      0)
  endfunc

  func oAZFLSCOM_4_19.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZDTSCOM_4_21 as StdField with uid="KNIENPTAOG",rtseq=96,rtrep=.f.,;
    cFormVar = "w_AZDTSCOM", cQueryName = "AZDTSCOM",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data attivazione flag calcola sconti su omaggi.",;
    HelpContextID = 150939475,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=366, Top=307

  func oAZDTSCOM_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZFLSCOM='S')
    endwith
   endif
  endfunc

  func oAZDTSCOM_4_21.mHide()
    with this.Parent.oContained
      return (.w_AZFLSCOM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oAZSCOPIE_4_22 as StdCheck with uid="NLAFBQUTTG",rtseq=97,rtrep=.f.,left=15, top=332, caption="Calcola sconti su righe positive e negative",;
    ToolTipText = "Se attivo: gli sconti di piede vengono calcolati sul valore determinato come somma delle righe positive e negative diverse da omaggio e sconto merce",;
    HelpContextID = 173074613,;
    cFormVar="w_AZSCOPIE", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZSCOPIE_4_22.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAZSCOPIE_4_22.GetRadio()
    this.Parent.oContained.w_AZSCOPIE = this.RadioValue()
    return .t.
  endfunc

  func oAZSCOPIE_4_22.SetRadio()
    this.Parent.oContained.w_AZSCOPIE=trim(this.Parent.oContained.w_AZSCOPIE)
    this.value = ;
      iif(this.Parent.oContained.w_AZSCOPIE=='S',1,;
      0)
  endfunc

  func oAZSCOPIE_4_22.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZDTSCPI_4_23 as StdField with uid="TXDNGEHOYZ",rtseq=98,rtrep=.f.,;
    cFormVar = "w_AZDTSCPI", cQueryName = "AZDTSCPI",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data a partire dalla quale nel calcolo degli sconti di piede vengono considerate anche le righe negative",;
    HelpContextID = 150939471,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=366, Top=332

  func oAZDTSCPI_4_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZSCOPIE='S')
    endwith
   endif
  endfunc

  func oAZDTSCPI_4_23.mHide()
    with this.Parent.oContained
      return (.w_AZSCOPIE<>'S' OR IsAlt())
    endwith
  endfunc

  add object oAZFLCESC_4_24 as StdCheck with uid="NQMBUTCWAO",rtseq=99,rtrep=.f.,left=15, top=353, caption="Codici cli/for esclusivi",;
    ToolTipText = "Attivo: per ogni cliente/fornitore sar� disponibile un suffisso che andr� a completare il codice di ricerca.",;
    HelpContextID = 101234871,;
    cFormVar="w_AZFLCESC", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZFLCESC_4_24.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZFLCESC_4_24.GetRadio()
    this.Parent.oContained.w_AZFLCESC = this.RadioValue()
    return .t.
  endfunc

  func oAZFLCESC_4_24.SetRadio()
    this.Parent.oContained.w_AZFLCESC=trim(this.Parent.oContained.w_AZFLCESC)
    this.value = ;
      iif(this.Parent.oContained.w_AZFLCESC=='S',1,;
      0)
  endfunc

  func oAZFLCESC_4_24.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZXCONDI_4_25 as StdCheck with uid="UDXJAWXFPY",rtseq=100,rtrep=.f.,left=15, top=374, caption="Gest. per conto di",;
    ToolTipText = "Attivo: sui documenti i dati relativi a pagamenti e prezzi sono calcolati in base al per conto di (se presente)",;
    HelpContextID = 206608561,;
    cFormVar="w_AZXCONDI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZXCONDI_4_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZXCONDI_4_25.GetRadio()
    this.Parent.oContained.w_AZXCONDI = this.RadioValue()
    return .t.
  endfunc

  func oAZXCONDI_4_25.SetRadio()
    this.Parent.oContained.w_AZXCONDI=trim(this.Parent.oContained.w_AZXCONDI)
    this.value = ;
      iif(this.Parent.oContained.w_AZXCONDI=='S',1,;
      0)
  endfunc

  func oAZXCONDI_4_25.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZDATDOC_4_26 as StdCheck with uid="JMGGSZEUIG",rtseq=101,rtrep=.f.,left=15, top=395, caption="Calcola cambio per data documento",;
    ToolTipText = "Se attivo, privilegia data documento nella ricerca del cambio nei documenti di acquisto",;
    HelpContextID = 167520073,;
    cFormVar="w_AZDATDOC", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZDATDOC_4_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZDATDOC_4_26.GetRadio()
    this.Parent.oContained.w_AZDATDOC = this.RadioValue()
    return .t.
  endfunc

  func oAZDATDOC_4_26.SetRadio()
    this.Parent.oContained.w_AZDATDOC=trim(this.Parent.oContained.w_AZDATDOC)
    this.value = ;
      iif(this.Parent.oContained.w_AZDATDOC=='S',1,;
      0)
  endfunc

  func oAZDATDOC_4_26.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oAZCALNET_4_27 as StdCombo with uid="QEJDRUGPMA",rtseq=102,rtrep=.f.,left=366,top=357,width=141,height=21;
    , ToolTipText = "Ordine di calcolo del valore netto di riga nei documenti";
    , HelpContextID = 209971366;
    , cFormVar="w_AZCALNET",RowSource=""+"(Prz - sconti) * qta,"+"(Prz * qta) - sconti", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAZCALNET_4_27.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oAZCALNET_4_27.GetRadio()
    this.Parent.oContained.w_AZCALNET = this.RadioValue()
    return .t.
  endfunc

  func oAZCALNET_4_27.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AZCALNET==1,1,;
      iif(this.Parent.oContained.w_AZCALNET==2,2,;
      0))
  endfunc

  func oAZCALNET_4_27.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oAZPERPQT_4_28 as StdCombo with uid="TJYQKYJHNF",value=1,rtseq=103,rtrep=.f.,left=499,top=72,width=98,height=21;
    , ToolTipText = "Numero decimali campi quantit� su vendite, ordini, magazzino e statistiche";
    , HelpContextID = 98625370;
    , cFormVar="w_AZPERPQT",RowSource=""+"Nessuno,"+"Uno,"+"Due,"+"Tre", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAZPERPQT_4_28.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    0)))))
  endfunc
  func oAZPERPQT_4_28.GetRadio()
    this.Parent.oContained.w_AZPERPQT = this.RadioValue()
    return .t.
  endfunc

  func oAZPERPQT_4_28.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AZPERPQT==0,1,;
      iif(this.Parent.oContained.w_AZPERPQT==1,2,;
      iif(this.Parent.oContained.w_AZPERPQT==2,3,;
      iif(this.Parent.oContained.w_AZPERPQT==3,4,;
      0))))
  endfunc

  func oAZPERPQT_4_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AZPERPQT<4)
    endwith
    return bRes
  endfunc


  add object oAZPERPQD_4_29 as StdCombo with uid="VLFTFGHXUA",value=1,rtseq=104,rtrep=.f.,left=499,top=99,width=98,height=21;
    , ToolTipText = "Numero decimali utilizzabili per le quantit� sulla distinta base";
    , HelpContextID = 98625354;
    , cFormVar="w_AZPERPQD",RowSource=""+"Nessuno,"+"Uno,"+"Due,"+"Tre,"+"Quattro,"+"Cinque", bObbl = .f. , nPag = 4;
    , sErrorMsg = "Indicare al massimo 5 decimali";
  , bGlobalFont=.t.


  func oAZPERPQD_4_29.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,4,;
    iif(this.value =6,5,;
    0)))))))
  endfunc
  func oAZPERPQD_4_29.GetRadio()
    this.Parent.oContained.w_AZPERPQD = this.RadioValue()
    return .t.
  endfunc

  func oAZPERPQD_4_29.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AZPERPQD==0,1,;
      iif(this.Parent.oContained.w_AZPERPQD==1,2,;
      iif(this.Parent.oContained.w_AZPERPQD==2,3,;
      iif(this.Parent.oContained.w_AZPERPQD==3,4,;
      iif(this.Parent.oContained.w_AZPERPQD==4,5,;
      iif(this.Parent.oContained.w_AZPERPQD==5,6,;
      0))))))
  endfunc

  func oAZPERPQD_4_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DISB='S')
    endwith
   endif
  endfunc

  func oAZPERPQD_4_29.mHide()
    with this.Parent.oContained
      return (g_DISB<>'S' OR IsAlt())
    endwith
  endfunc

  func oAZPERPQD_4_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AZPERPQT>=0 AND .w_AZPERPQT<6)
    endwith
    return bRes
  endfunc


  add object oAZNUMSCO_4_30 as StdCombo with uid="QROCKXRVPJ",value=1,rtseq=105,rtrep=.f.,left=499,top=127,width=98,height=21;
    , ToolTipText = "Numero massimo di sconti utilizzati";
    , HelpContextID = 123680939;
    , cFormVar="w_AZNUMSCO",RowSource=""+"Nessuno,"+"Uno,"+"Due,"+"Tre,"+"Quattro", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAZNUMSCO_4_30.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,4,;
    0))))))
  endfunc
  func oAZNUMSCO_4_30.GetRadio()
    this.Parent.oContained.w_AZNUMSCO = this.RadioValue()
    return .t.
  endfunc

  func oAZNUMSCO_4_30.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AZNUMSCO==0,1,;
      iif(this.Parent.oContained.w_AZNUMSCO==1,2,;
      iif(this.Parent.oContained.w_AZNUMSCO==2,3,;
      iif(this.Parent.oContained.w_AZNUMSCO==3,4,;
      iif(this.Parent.oContained.w_AZNUMSCO==4,5,;
      0)))))
  endfunc

  func oAZNUMSCO_4_30.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oAZNUMSCO_4_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AZNUMSCO<=4)
    endwith
    return bRes
  endfunc

  add object oAZCODEXT_4_31 as StdField with uid="WAKVUHGPWO",rtseq=106,rtrep=.f.,;
    cFormVar = "w_AZCODEXT", cQueryName = "AZCODEXT",nZero=1,;
    bObbl = .f. , nPag = 4, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Cifra di estensione (usato nella generazione del codice SSCC delle unit� logistiche)",;
    HelpContextID = 100001958,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=387, Top=154, cSayPict="'9'", cGetPict="'9'", InputMask=replicate('X',1)

  func oAZCODEXT_4_31.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZPREEAN_4_32 as StdField with uid="KVKIBUWATM",rtseq=107,rtrep=.f.,;
    cFormVar = "w_AZPREEAN", cQueryName = "AZPREEAN",nZero=2,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso EAN della nazione (usato in generazione barcodes)",;
    HelpContextID = 169731924,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=499, Top=154, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oAZPREEAN_4_32.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZDATMAT_4_33 as StdField with uid="NAYKTRQWHW",rtseq=108,rtrep=.f.,;
    cFormVar = "w_AZDATMAT", cQueryName = "AZDATMAT",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Da questa data divengono attivi i controlli sulle matricole",;
    HelpContextID = 50079578,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=499, Top=179

  func oAZDATMAT_4_33.mHide()
    with this.Parent.oContained
      return (.w_AZGESMAT<>'S' OR IsAlt())
    endwith
  endfunc


  add object oAZUNIMAT_4_34 as StdCombo with uid="OEUEMQPCMV",rtseq=109,rtrep=.f.,left=387,top=206,width=98,height=21;
    , ToolTipText = "Per matricola=matricola univoca, per articolo=matricola � univoca per articolo, per classe=matricola univoca per classe, per articolo e classe=controlla che all'interno della stessa classe non esistano codici uguali";
    , HelpContextID = 39466842;
    , cFormVar="w_AZUNIMAT",RowSource=""+"Per articolo,"+"Per classe,"+"Per matricola,"+"Per articolo e classe", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAZUNIMAT_4_34.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    iif(this.value =3,'M',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oAZUNIMAT_4_34.GetRadio()
    this.Parent.oContained.w_AZUNIMAT = this.RadioValue()
    return .t.
  endfunc

  func oAZUNIMAT_4_34.SetRadio()
    this.Parent.oContained.w_AZUNIMAT=trim(this.Parent.oContained.w_AZUNIMAT)
    this.value = ;
      iif(this.Parent.oContained.w_AZUNIMAT=='A',1,;
      iif(this.Parent.oContained.w_AZUNIMAT=='C',2,;
      iif(this.Parent.oContained.w_AZUNIMAT=='M',3,;
      iif(this.Parent.oContained.w_AZUNIMAT=='E',4,;
      0))))
  endfunc

  func oAZUNIMAT_4_34.mHide()
    with this.Parent.oContained
      return (.w_AZGESMAT<>'S' OR IsAlt())
    endwith
  endfunc

  add object oAZSCAMAT_4_35 as StdCheck with uid="DDNTOJLIMD",rtseq=110,rtrep=.f.,left=499, top=204, caption="Escludi matricole scaricate",;
    ToolTipText = "Se attivo visualizza solo matricole mai scaricate",;
    HelpContextID = 30349146,;
    cFormVar="w_AZSCAMAT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAZSCAMAT_4_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZSCAMAT_4_35.GetRadio()
    this.Parent.oContained.w_AZSCAMAT = this.RadioValue()
    return .t.
  endfunc

  func oAZSCAMAT_4_35.SetRadio()
    this.Parent.oContained.w_AZSCAMAT=trim(this.Parent.oContained.w_AZSCAMAT)
    this.value = ;
      iif(this.Parent.oContained.w_AZSCAMAT=='S',1,;
      0)
  endfunc

  func oAZSCAMAT_4_35.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZCODPRO_4_36 as StdField with uid="NAYIONEFVN",rtseq=111,rtrep=.f.,;
    cFormVar = "w_AZCODPRO", cQueryName = "AZCODPRO",;
    bObbl = .f. , nPag = 4, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Cod. produttore per barcode assegnato dall'INDICOD al proprietario del marchio",;
    HelpContextID = 183888043,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=640, Top=154, cSayPict="'9999999'", cGetPict="'9999999'", InputMask=replicate('X',7)

  func oAZCODPRO_4_36.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZMASART_4_38 as StdField with uid="JUKXLOQHQK",rtseq=113,rtrep=.f.,;
    cFormVar = "w_AZMASART", cQueryName = "AZMASART",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Struttura input codici articoli / servizi",;
    HelpContextID = 116176730,;
   bGlobalFont=.t.,;
    Height=21, Width=170, Left=499, Top=231, InputMask=replicate('X',20)

  func oAZMASART_4_38.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZMASLOT_4_39 as StdField with uid="FSXEQVVRCR",rtseq=114,rtrep=.f.,;
    cFormVar = "w_AZMASLOT", cQueryName = "AZMASLOT",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Struttura input codici lotti",;
    HelpContextID = 32290650,;
   bGlobalFont=.t.,;
    Height=21, Width=170, Left=499, Top=256, InputMask=replicate('X',20)

  func oAZMASLOT_4_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S' AND .w_AZGESLOT='S')
    endwith
   endif
  endfunc

  func oAZMASLOT_4_39.mHide()
    with this.Parent.oContained
      return (g_MADV<>'S' OR IsAlt())
    endwith
  endfunc

  add object oAZMASUBI_4_40 as StdField with uid="SFJMKYOWQI",rtseq=115,rtrep=.f.,;
    cFormVar = "w_AZMASUBI", cQueryName = "AZMASUBI",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Struttura input codici ubicazioni",;
    HelpContextID = 85149873,;
   bGlobalFont=.t.,;
    Height=21, Width=170, Left=499, Top=282, InputMask=replicate('X',20)

  func oAZMASUBI_4_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S' AND .w_AZGESUBI='S')
    endwith
   endif
  endfunc

  func oAZMASUBI_4_40.mHide()
    with this.Parent.oContained
      return (g_MADV<>'S' OR IsAlt())
    endwith
  endfunc

  add object oAZPERMUC_4_53 as StdField with uid="DSJLPJNPZP",rtseq=118,rtrep=.f.,;
    cFormVar = "w_AZPERMUC", cQueryName = "AZPERMUC",;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codifica per mascherare i prezzi nella stampa etichette",;
    HelpContextID = 220141751,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=586, Top=329, InputMask=replicate('X',10)

  func oAZPERMUC_4_53.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oObj_4_57 as cp_runprogram with uid="ASQSBNIFKC",left=-1, top=447, width=306,height=22,;
    caption='GSAR_BAM( FlagMess )',;
   bGlobalFont=.t.,;
    prg="GSAR_BAM('FlagMess')",;
    cEvent = "w_AZFLSCOM Changed",;
    nPag=4;
    , HelpContextID = 160895544


  add object oObj_4_58 as cp_runprogram with uid="ANQVBWRROO",left=-2, top=471, width=308,height=22,;
    caption='GSAR_BAM( DataMess )',;
   bGlobalFont=.t.,;
    prg="GSAR_BAM('DataMess')",;
    cEvent = "w_AZDTSCOM Changed",;
    nPag=4;
    , HelpContextID = 242364984


  add object oObj_4_65 as cp_runprogram with uid="FLOPBAGHEU",left=-2, top=494, width=310,height=22,;
    caption='GSAR_BAM( RigaMess )',;
   bGlobalFont=.t.,;
    prg="GSAR_BAM('RigaMess')",;
    cEvent = "w_AZCALNET Changed",;
    nPag=4;
    , HelpContextID = 255414840


  add object oAZSCONET_4_66 as StdCombo with uid="LPOMRWKGBR",rtseq=166,rtrep=.f.,left=510,top=357,width=161,height=21;
    , ToolTipText = "Arrotonda importo scontato: esegue arrotondamento su importo al netto degli sconti. Arrotonda sconto: arrotonda il valore dello sconto prima di sottrarlo all'importo";
    , HelpContextID = 206629030;
    , cFormVar="w_AZSCONET",RowSource=""+"Arrotonda importo scontato,"+"Arrotonda sconto", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAZSCONET_4_66.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oAZSCONET_4_66.GetRadio()
    this.Parent.oContained.w_AZSCONET = this.RadioValue()
    return .t.
  endfunc

  func oAZSCONET_4_66.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AZSCONET==1,1,;
      iif(this.Parent.oContained.w_AZSCONET==2,2,;
      0))
  endfunc

  func oAZSCONET_4_66.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oObj_4_67 as cp_calclbl with uid="YHZQHFXHUZ",left=6, top=10, width=56,height=25,;
    caption='StrAzi',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=4;
    , HelpContextID = 245423578


  add object oObj_4_68 as cp_setobjprop with uid="ZUTJKMVIEY",left=500, top=447, width=119,height=22,;
    caption='ToolTipText',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_AZPERPQT",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 254913498

  add object oStr_4_3 as StdString with uid="WRSZKPVZLG",Visible=.t., Left=133, Top=10,;
    Alignment=1, Width=90, Height=15,;
    Caption="Rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_4_20 as StdString with uid="KZIYRCRDYH",Visible=.t., Left=286, Top=310,;
    Alignment=1, Width=76, Height=18,;
    Caption="Attivato il:"  ;
  , bGlobalFont=.t.

  func oStr_4_20.mHide()
    with this.Parent.oContained
      return (.w_AZFLSCOM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oStr_4_41 as StdString with uid="TVXMTDSLAI",Visible=.t., Left=457, Top=43,;
    Alignment=1, Width=162, Height=15,;
    Caption="Ultimo scarico magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_4_41.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_42 as StdString with uid="WNSBAKCOYA",Visible=.t., Left=311, Top=72,;
    Alignment=1, Width=183, Height=15,;
    Caption="Num. decimali quantit�:"  ;
  , bGlobalFont=.t.

  add object oStr_4_43 as StdString with uid="CXNLKDLENP",Visible=.t., Left=1, Top=43,;
    Alignment=1, Width=128, Height=15,;
    Caption="Deposito di default:"  ;
  , bGlobalFont=.t.

  func oStr_4_43.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_44 as StdString with uid="UFZSNBHQFC",Visible=.t., Left=311, Top=231,;
    Alignment=1, Width=183, Height=15,;
    Caption="Struttura codice articolo:"  ;
  , bGlobalFont=.t.

  func oStr_4_44.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_47 as StdString with uid="YGGTZJBPUP",Visible=.t., Left=415, Top=154,;
    Alignment=1, Width=79, Height=15,;
    Caption="Prefisso EAN:"  ;
  , bGlobalFont=.t.

  func oStr_4_47.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_48 as StdString with uid="DJZBYSQLNU",Visible=.t., Left=531, Top=154,;
    Alignment=1, Width=107, Height=15,;
    Caption="Codice produttore:"  ;
  , bGlobalFont=.t.

  func oStr_4_48.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_49 as StdString with uid="GCFIRWJPDD",Visible=.t., Left=311, Top=129,;
    Alignment=1, Width=183, Height=15,;
    Caption="Num. sconti utilizzati:"  ;
  , bGlobalFont=.t.

  func oStr_4_49.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_50 as StdString with uid="OSOLIRRWCG",Visible=.t., Left=594, Top=308,;
    Alignment=0, Width=70, Height=18,;
    Caption="0123456789"  ;
  , bGlobalFont=.t.

  func oStr_4_50.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_51 as StdString with uid="PQGTCNCYJW",Visible=.t., Left=499, Top=308,;
    Alignment=0, Width=98, Height=18,;
    Caption="Maschera prezzi"  ;
  , bGlobalFont=.t.

  func oStr_4_51.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_54 as StdString with uid="FSCGKPEUUN",Visible=.t., Left=311, Top=282,;
    Alignment=1, Width=183, Height=15,;
    Caption="Struttura codice ubicazione:"  ;
  , bGlobalFont=.t.

  func oStr_4_54.mHide()
    with this.Parent.oContained
      return (g_MADV<>'S' OR IsAlt())
    endwith
  endfunc

  add object oStr_4_55 as StdString with uid="ABHXTDWTSK",Visible=.t., Left=311, Top=256,;
    Alignment=1, Width=183, Height=15,;
    Caption="Struttura codice lotto:"  ;
  , bGlobalFont=.t.

  func oStr_4_55.mHide()
    with this.Parent.oContained
      return (g_MADV<>'S' OR IsAlt())
    endwith
  endfunc

  add object oStr_4_56 as StdString with uid="OEZJOCNDZJ",Visible=.t., Left=215, Top=99,;
    Alignment=1, Width=279, Height=18,;
    Caption="Num. decimali quantit� in distinta base:"  ;
  , bGlobalFont=.t.

  func oStr_4_56.mHide()
    with this.Parent.oContained
      return (g_DISB<>'S' OR IsAlt())
    endwith
  endfunc

  add object oStr_4_60 as StdString with uid="QJFAGBIFKZ",Visible=.t., Left=208, Top=206,;
    Alignment=1, Width=175, Height=15,;
    Caption="Controllo univocit� matricole:"  ;
  , bGlobalFont=.t.

  func oStr_4_60.mHide()
    with this.Parent.oContained
      return (.w_AZGESMAT<>'S' OR IsAlt())
    endwith
  endfunc

  add object oStr_4_61 as StdString with uid="HFLZIZBTWD",Visible=.t., Left=294, Top=179,;
    Alignment=1, Width=200, Height=15,;
    Caption="Data inizio controllo matricole:"  ;
  , bGlobalFont=.t.

  func oStr_4_61.mHide()
    with this.Parent.oContained
      return (.w_AZGESMAT<>'S' OR IsAlt())
    endwith
  endfunc

  add object oStr_4_62 as StdString with uid="GLOVZHKAIX",Visible=.t., Left=195, Top=357,;
    Alignment=1, Width=167, Height=18,;
    Caption="Calcolo del netto riga:"  ;
  , bGlobalFont=.t.

  func oStr_4_62.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_63 as StdString with uid="FCXXIEPJMA",Visible=.t., Left=285, Top=154,;
    Alignment=1, Width=98, Height=18,;
    Caption="Cifra di esten.:"  ;
  , bGlobalFont=.t.

  func oStr_4_63.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_69 as StdString with uid="TEOROJGDRH",Visible=.t., Left=286, Top=335,;
    Alignment=1, Width=76, Height=18,;
    Caption="Attivato il:"  ;
  , bGlobalFont=.t.

  func oStr_4_69.mHide()
    with this.Parent.oContained
      return (.w_AZSCOPIE<>'S' OR IsAlt())
    endwith
  endfunc

  add object oBox_4_52 as StdBox with uid="FZAWFUVWKU",left=496, top=324, width=184,height=2
enddefine
define class tgsar_aziPag5 as StdContainer
  Width  = 719
  height = 492
  stdWidth  = 719
  stdheight = 492
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZSTALIG_5_1 as StdField with uid="XWFWMXEMLJ",rtseq=123,rtrep=.f.,;
    cFormVar = "w_AZSTALIG", cQueryName = "AZSTALIG",;
    bObbl = .f. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di stampa libro giornale",;
    HelpContextID = 253749427,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=189, Top=78

  add object oAZINTLIG_5_2 as StdCheck with uid="JSBKVWWTBA",rtseq=124,rtrep=.f.,left=41, top=105, caption="Stampa intestazione libro giornale",;
    ToolTipText = "Attivo: stampa intestazione e numerazione pagine sul libro giornale",;
    HelpContextID = 234260659,;
    cFormVar="w_AZINTLIG", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oAZINTLIG_5_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZINTLIG_5_2.GetRadio()
    this.Parent.oContained.w_AZINTLIG = this.RadioValue()
    return .t.
  endfunc

  func oAZINTLIG_5_2.SetRadio()
    this.Parent.oContained.w_AZINTLIG=trim(this.Parent.oContained.w_AZINTLIG)
    this.value = ;
      iif(this.Parent.oContained.w_AZINTLIG=='S',1,;
      0)
  endfunc

  func oAZINTLIG_5_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_AZCONCAS='S')
    endwith
   endif
  endfunc

  func oAZINTLIG_5_2.mHide()
    with this.Parent.oContained
      return (.w_AZCONCAS='S')
    endwith
  endfunc

  add object oAZINTLIG_5_3 as StdCheck with uid="NLUXLVMRHT",rtseq=125,rtrep=.f.,left=16, top=105, caption="Stampa intestazione registro cronologico",;
    ToolTipText = "Attivo: stampa intestazione e numerazione pagine sul registro cronologico",;
    HelpContextID = 234260659,;
    cFormVar="w_AZINTLIG", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oAZINTLIG_5_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZINTLIG_5_3.GetRadio()
    this.Parent.oContained.w_AZINTLIG = this.RadioValue()
    return .t.
  endfunc

  func oAZINTLIG_5_3.SetRadio()
    this.Parent.oContained.w_AZINTLIG=trim(this.Parent.oContained.w_AZINTLIG)
    this.value = ;
      iif(this.Parent.oContained.w_AZINTLIG=='S',1,;
      0)
  endfunc

  func oAZINTLIG_5_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZCONCAS='S')
    endwith
   endif
  endfunc

  func oAZINTLIG_5_3.mHide()
    with this.Parent.oContained
      return (not .w_AZCONCAS='S')
    endwith
  endfunc

  add object oAZPRPALG_5_4 as StdField with uid="XHYWFZVDNW",rtseq=126,rtrep=.f.,;
    cFormVar = "w_AZPRPALG", cQueryName = "AZPRPALG",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo pagine libro giornale",;
    HelpContextID = 114157389,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=133, cSayPict='"9999999"', cGetPict='"9999999"'

  func oAZPRPALG_5_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZINTLIG='S')
    endwith
   endif
  endfunc

  add object oAZPREFLG_5_5 as StdField with uid="HSHHJVSCAC",rtseq=127,rtrep=.f.,;
    cFormVar = "w_AZPREFLG", cQueryName = "AZPREFLG",;
    bObbl = .f. , nPag = 5, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso numerazione pagine libro giornale",;
    HelpContextID = 186509133,;
   bGlobalFont=.t.,;
    Height=21, Width=164, Left=189, Top=160, InputMask=replicate('X',20)

  func oAZPREFLG_5_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZINTLIG='S')
    endwith
   endif
  endfunc

  add object oAZINTLIN_5_6 as StdCheck with uid="WMCZFDQBXC",rtseq=128,rtrep=.f.,left=16, top=237, caption="Stampa intestazione libro inventari",;
    ToolTipText = "Attivo: stampa intestazione e numerazione pagine sul libro inventari",;
    HelpContextID = 234260652,;
    cFormVar="w_AZINTLIN", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oAZINTLIN_5_6.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZINTLIN_5_6.GetRadio()
    this.Parent.oContained.w_AZINTLIN = this.RadioValue()
    return .t.
  endfunc

  func oAZINTLIN_5_6.SetRadio()
    this.Parent.oContained.w_AZINTLIN=trim(this.Parent.oContained.w_AZINTLIN)
    this.value = ;
      iif(this.Parent.oContained.w_AZINTLIN=='S',1,;
      0)
  endfunc

  func oAZINTLIN_5_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZPRPALI_5_7 as StdField with uid="XSAWFWVRAP",rtseq=129,rtrep=.f.,;
    cFormVar = "w_AZPRPALI", cQueryName = "AZPRPALI",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo pagine libro inventari",;
    HelpContextID = 114157391,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=265

  func oAZPRPALI_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZINTLIN='S')
    endwith
   endif
  endfunc

  func oAZPRPALI_5_7.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZESLBIN_5_8 as StdField with uid="USHKMMAGEA",rtseq=130,rtrep=.f.,;
    cFormVar = "w_AZESLBIN", cQueryName = "AZESLBIN",;
    bObbl = .f. , nPag = 5, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio ultima stampa libro inventari",;
    HelpContextID = 141674668,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=309, Top=265, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_COAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_AZESLBIN"

  func oAZESLBIN_5_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oAZESLBIN_5_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZESLBIN_5_8.ecpDrop(oSource)
    this.Parent.oContained.link_5_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZESLBIN_5_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_COAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_COAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oAZESLBIN_5_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oAZPREFLI_5_9 as StdField with uid="PNMSTLTWCG",rtseq=131,rtrep=.f.,;
    cFormVar = "w_AZPREFLI", cQueryName = "AZPREFLI",;
    bObbl = .f. , nPag = 5, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso numerazione pagine libro inventari",;
    HelpContextID = 186509135,;
   bGlobalFont=.t.,;
    Height=21, Width=165, Left=189, Top=292, InputMask=replicate('X',20)

  func oAZPREFLI_5_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZINTLIN='S')
    endwith
   endif
  endfunc

  func oAZPREFLI_5_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oAZGMGCON_5_10 as StdCombo with uid="PKMTEVHSBG",rtseq=132,rtrep=.f.,left=581,top=78,width=116,height=21;
    , ToolTipText = "Tipo stampa giornale di magazzino";
    , HelpContextID = 137910100;
    , cFormVar="w_AZGMGCON",RowSource=""+"Beni singoli,"+"Cat.omogenee", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oAZGMGCON_5_10.RadioValue()
    return(iif(this.value =1,'B',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oAZGMGCON_5_10.GetRadio()
    this.Parent.oContained.w_AZGMGCON = this.RadioValue()
    return .t.
  endfunc

  func oAZGMGCON_5_10.SetRadio()
    this.Parent.oContained.w_AZGMGCON=trim(this.Parent.oContained.w_AZGMGCON)
    this.value = ;
      iif(this.Parent.oContained.w_AZGMGCON=='B',1,;
      iif(this.Parent.oContained.w_AZGMGCON=='C',2,;
      0))
  endfunc

  func oAZGMGCON_5_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oAZGMGFOR_5_11 as StdCombo with uid="IIPWQCUNSJ",rtseq=133,rtrep=.f.,left=581,top=105,width=116,height=21;
    , ToolTipText = "Forma stampa giornale di magazzino";
    , HelpContextID = 188241752;
    , cFormVar="w_AZGMGFOR",RowSource=""+"Riepilogativa,"+"Dettagliata", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oAZGMGFOR_5_11.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oAZGMGFOR_5_11.GetRadio()
    this.Parent.oContained.w_AZGMGFOR = this.RadioValue()
    return .t.
  endfunc

  func oAZGMGFOR_5_11.SetRadio()
    this.Parent.oContained.w_AZGMGFOR=trim(this.Parent.oContained.w_AZGMGFOR)
    this.value = ;
      iif(this.Parent.oContained.w_AZGMGFOR=='R',1,;
      iif(this.Parent.oContained.w_AZGMGFOR=='D',2,;
      0))
  endfunc

  func oAZGMGFOR_5_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oAZGMGPER_5_12 as StdCombo with uid="WVYLYDJTXX",rtseq=134,rtrep=.f.,left=581,top=132,width=116,height=21;
    , ToolTipText = "Periodicit� stampa giornale di magazzino";
    , HelpContextID = 180857000;
    , cFormVar="w_AZGMGPER",RowSource=""+"Mensile,"+"Quindicinale,"+"Decade,"+"Settimanale", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oAZGMGPER_5_12.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'Q',;
    iif(this.value =3,'D',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oAZGMGPER_5_12.GetRadio()
    this.Parent.oContained.w_AZGMGPER = this.RadioValue()
    return .t.
  endfunc

  func oAZGMGPER_5_12.SetRadio()
    this.Parent.oContained.w_AZGMGPER=trim(this.Parent.oContained.w_AZGMGPER)
    this.value = ;
      iif(this.Parent.oContained.w_AZGMGPER=='M',1,;
      iif(this.Parent.oContained.w_AZGMGPER=='Q',2,;
      iif(this.Parent.oContained.w_AZGMGPER=='D',3,;
      iif(this.Parent.oContained.w_AZGMGPER=='S',4,;
      0))))
  endfunc

  func oAZGMGPER_5_12.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZSTARBE_5_13 as StdField with uid="XVSVNCCDXM",rtseq=135,rtrep=.f.,;
    cFormVar = "w_AZSTARBE", cQueryName = "AZSTARBE",;
    bObbl = .f. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima stampa del registro beni in lavorazione",;
    HelpContextID = 153086133,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=546, Top=217

  func oAZSTARBE_5_13.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZINTRBE_5_14 as StdCheck with uid="TEREMVDGJR",rtseq=136,rtrep=.f.,left=420, top=244, caption="Stampa intestazione R. beni",;
    ToolTipText = "Attivo: stampa intestazione e numerazione pagine sul registro beni in lavorazione",;
    HelpContextID = 133597365,;
    cFormVar="w_AZINTRBE", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oAZINTRBE_5_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZINTRBE_5_14.GetRadio()
    this.Parent.oContained.w_AZINTRBE = this.RadioValue()
    return .t.
  endfunc

  func oAZINTRBE_5_14.SetRadio()
    this.Parent.oContained.w_AZINTRBE=trim(this.Parent.oContained.w_AZINTRBE)
    this.value = ;
      iif(this.Parent.oContained.w_AZINTRBE=='S',1,;
      0)
  endfunc

  func oAZINTRBE_5_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZPRPRBE_5_15 as StdField with uid="TSHEVMZGXL",rtseq=137,rtrep=.f.,;
    cFormVar = "w_AZPRPRBE", cQueryName = "AZPRPRBE",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima pagina stampata del registro beni in lavorazione",;
    HelpContextID = 137500853,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=546, Top=282, cSayPict='"9999999"', cGetPict='"9999999"'

  func oAZPRPRBE_5_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZINTRBE='S')
    endwith
   endif
  endfunc

  func oAZPRPRBE_5_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZPRERBE_5_16 as StdField with uid="EZMWSNYDTS",rtseq=138,rtrep=.f.,;
    cFormVar = "w_AZPRERBE", cQueryName = "AZPRERBE",;
    bObbl = .f. , nPag = 5, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Dicitura numerazione registro beni in lavorazione",;
    HelpContextID = 149035189,;
   bGlobalFont=.t.,;
    Height=21, Width=165, Left=546, Top=313, InputMask=replicate('X',20)

  func oAZPRERBE_5_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZINTRBE='S')
    endwith
   endif
  endfunc

  func oAZPRERBE_5_16.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZDATBLO_5_22 as StdField with uid="BYLRGFTDTR",rtseq=139,rtrep=.f.,;
    cFormVar = "w_AZDATBLO", cQueryName = "AZDATBLO",;
    bObbl = .f. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Utilizzata per bloccare temporaneamente le reg.primanota durante la stampa L.G.",;
    HelpContextID = 133965653,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=546, Top=357

  add object oAZBLOGCL_5_23 as StdCheck with uid="UCSJLKBUOU",rtseq=140,rtrep=.f.,left=16, top=381, caption="Blocco generazione comunicazioni lettere intento",;
    ToolTipText = "Utilizzato per bloccare temporaneamente la generazione delle comunicazioni delle lettere d'intento ricevute",;
    HelpContextID = 55113902,;
    cFormVar="w_AZBLOGCL", bObbl = .f. , nPag = 5;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oAZBLOGCL_5_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZBLOGCL_5_23.GetRadio()
    this.Parent.oContained.w_AZBLOGCL = this.RadioValue()
    return .t.
  endfunc

  func oAZBLOGCL_5_23.SetRadio()
    this.Parent.oContained.w_AZBLOGCL=trim(this.Parent.oContained.w_AZBLOGCL)
    this.value = ;
      iif(this.Parent.oContained.w_AZBLOGCL=='S',1,;
      0)
  endfunc

  add object oCOAZI_5_26 as StdField with uid="XZLFNAAEKD",rtseq=141,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90500134,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=10, InputMask=replicate('X',5)

  func oCOAZI_5_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_AZESLBIN)
        bRes2=.link_5_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDEAZI_5_27 as StdField with uid="MBBLNHAQLL",rtseq=142,rtrep=.f.,;
    cFormVar = "w_DEAZI", cQueryName = "DEAZI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 90497590,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=226, Top=10, InputMask=replicate('X',80)


  add object oObj_5_42 as cp_calclbl with uid="CWALOYHIUJ",left=6, top=10, width=56,height=25,;
    caption='StrAzi',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=5;
    , HelpContextID = 245423578


  add object oObj_5_43 as cp_calclbl with uid="YPKWZMJESH",left=4, top=47, width=269,height=25,;
    caption='StrLG',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=0,;
    nPag=5;
    , HelpContextID = 87695910


  add object oObj_5_44 as cp_setobjprop with uid="WCHWCIXIXG",left=20, top=446, width=304,height=38,;
    caption='ToolTip di w_AZSTALIG',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_AZSTALIG",;
    cEvent = "w_AZCONCAS Changed",;
    nPag=5;
    , HelpContextID = 189883177


  add object oObj_5_45 as cp_calclbl with uid="VAYUDPKUWV",left=18, top=78, width=171,height=25,;
    caption='StrSLG',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=5;
    , HelpContextID = 210838054


  add object oObj_5_46 as cp_calclbl with uid="DTVMETULLT",left=16, top=133, width=172,height=25,;
    caption='StrNPLG',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=5;
    , HelpContextID = 238280154


  add object oObj_5_47 as cp_setobjprop with uid="LTSGKYZSPO",left=20, top=486, width=304,height=38,;
    caption='ToolTip di w_AZPRPALG',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_AZPRPALG",;
    cEvent = "w_AZCONCAS Changed",;
    nPag=5;
    , HelpContextID = 190038534


  add object oObj_5_48 as cp_setobjprop with uid="AJDEOTDVSB",left=20, top=526, width=304,height=38,;
    caption='ToolTip di w_AZPREFLG',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_AZPREFLG",;
    cEvent = "w_AZCONCAS Changed",;
    nPag=5;
    , HelpContextID = 190056198

  add object oStr_5_17 as StdString with uid="YPZMZKEXPZ",Visible=.t., Left=427, Top=47,;
    Alignment=0, Width=269, Height=15,;
    Caption="Vincoli giornale magazzino"  ;
  , bGlobalFont=.t.

  func oStr_5_17.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_18 as StdString with uid="IDAULFANOJ",Visible=.t., Left=425, Top=105,;
    Alignment=1, Width=153, Height=15,;
    Caption="Formato di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_5_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_19 as StdString with uid="CBFNBGDGTJ",Visible=.t., Left=425, Top=132,;
    Alignment=1, Width=153, Height=15,;
    Caption="Periodicit�:"  ;
  , bGlobalFont=.t.

  func oStr_5_19.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_21 as StdString with uid="NSPIYTNGWD",Visible=.t., Left=425, Top=78,;
    Alignment=1, Width=153, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_5_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_24 as StdString with uid="LYKCNPGANN",Visible=.t., Left=330, Top=357,;
    Alignment=1, Width=212, Height=15,;
    Caption="Semaforo stampe bollati:"  ;
  , bGlobalFont=.t.

  add object oStr_5_28 as StdString with uid="ACIUEASRCE",Visible=.t., Left=133, Top=10,;
    Alignment=1, Width=90, Height=15,;
    Caption="Rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_5_29 as StdString with uid="ZDCFTCHZPL",Visible=.t., Left=45, Top=160,;
    Alignment=1, Width=143, Height=18,;
    Caption="Prefisso numerazione:"  ;
  , bGlobalFont=.t.

  add object oStr_5_30 as StdString with uid="AFUTEDCYZC",Visible=.t., Left=4, Top=199,;
    Alignment=0, Width=269, Height=18,;
    Caption="Libro inventari"  ;
  , bGlobalFont=.t.

  func oStr_5_30.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_32 as StdString with uid="EPSXHBJNMS",Visible=.t., Left=0, Top=265,;
    Alignment=1, Width=188, Height=18,;
    Caption="Numero pagina libro inventari:"  ;
  , bGlobalFont=.t.

  func oStr_5_32.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_33 as StdString with uid="MJANKNAJFT",Visible=.t., Left=45, Top=292,;
    Alignment=1, Width=143, Height=18,;
    Caption="Prefisso numerazione:"  ;
  , bGlobalFont=.t.

  func oStr_5_33.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_34 as StdString with uid="SUBTWUBBZN",Visible=.t., Left=4, Top=351,;
    Alignment=0, Width=269, Height=18,;
    Caption="Lettere di intento"  ;
  , bGlobalFont=.t.

  add object oStr_5_36 as StdString with uid="WFFAUXTBEZ",Visible=.t., Left=255, Top=265,;
    Alignment=1, Width=53, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_5_36.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_37 as StdString with uid="ZABFYKRYZG",Visible=.t., Left=382, Top=282,;
    Alignment=1, Width=160, Height=18,;
    Caption="N. pag. R.Beni:"  ;
  , bGlobalFont=.t.

  func oStr_5_37.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_38 as StdString with uid="YXAOCJRRYR",Visible=.t., Left=391, Top=219,;
    Alignment=1, Width=151, Height=18,;
    Caption="Ultima stampa R.beni:"  ;
  , bGlobalFont=.t.

  func oStr_5_38.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_39 as StdString with uid="LNGYEJMIXF",Visible=.t., Left=386, Top=189,;
    Alignment=0, Width=269, Height=18,;
    Caption="Registro beni in lavorazione"  ;
  , bGlobalFont=.t.

  func oStr_5_39.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_5_41 as StdString with uid="OZQBUBYSJX",Visible=.t., Left=399, Top=313,;
    Alignment=1, Width=143, Height=18,;
    Caption="Prefisso numerazione:"  ;
  , bGlobalFont=.t.

  func oStr_5_41.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oBox_5_20 as StdBox with uid="EGILYVKAME",left=426, top=66, width=273,height=1

  add object oBox_5_25 as StdBox with uid="IYNONEGOFZ",left=3, top=66, width=313,height=1

  add object oBox_5_31 as StdBox with uid="GFHKUQTLWP",left=4, top=221, width=313,height=1

  add object oBox_5_35 as StdBox with uid="KHWSJOVLOO",left=4, top=373, width=313,height=1

  add object oBox_5_40 as StdBox with uid="DMOXORTEEO",left=386, top=211, width=313,height=1
enddefine
define class tgsar_aziPag6 as StdContainer
  Width  = 719
  height = 492
  stdWidth  = 719
  stdheight = 492
  resizeXpos=331
  resizeYpos=171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOAZI_6_1 as StdField with uid="VYZDQCTBLC",rtseq=121,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90500134,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=10, InputMask=replicate('X',5)

  func oCOAZI_6_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_AZESLBIN)
        bRes2=.link_5_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDEAZI_6_2 as StdField with uid="IKLMVTFGZM",rtseq=122,rtrep=.f.,;
    cFormVar = "w_DEAZI", cQueryName = "DEAZI",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 90497590,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=226, Top=10, InputMask=replicate('X',80)


  add object ZoomSedi as cp_zoombox with uid="OSONKRKLVH",left=28, top=64, width=495,height=154,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.t.,cZoomFile="GSAR2ASE",cTable="SEDIAZIE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bRetriveAllRows=.f.,cZoomOnZoom="gsar_ase",cMenuFile="",;
    nPag=6;
    , HelpContextID = 82669594

  add object oSEPERSON_6_12 as StdField with uid="YTOVTFVTST",rtseq=149,rtrep=.f.,;
    cFormVar = "w_SEPERSON", cQueryName = "SEPERSON",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 148951924,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=79, Top=257, InputMask=replicate('X',5)


  add object oSETIPRIF_6_13 as StdCombo with uid="NQODZHGLTB",rtseq=150,rtrep=.f.,left=79,top=230,width=167,height=21, enabled=.f.;
    , HelpContextID = 138079380;
    , cFormVar="w_SETIPRIF",RowSource=""+"Generica,"+"Sede legale,"+"Sede amministrativa,"+"Domicilio fiscale,"+"Filiale,"+"Agenzia,"+"Altro,"+"Unit� operativa,"+"Dipartimento", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oSETIPRIF_6_13.RadioValue()
    return(iif(this.value =1,"GE",;
    iif(this.value =2,"SL",;
    iif(this.value =3,"SA",;
    iif(this.value =4,"DF",;
    iif(this.value =5,"FI",;
    iif(this.value =6,"AG",;
    iif(this.value =7,"AL",;
    iif(this.value =8,"UO",;
    iif(this.value =9,"DI",;
    space(2)))))))))))
  endfunc
  func oSETIPRIF_6_13.GetRadio()
    this.Parent.oContained.w_SETIPRIF = this.RadioValue()
    return .t.
  endfunc

  func oSETIPRIF_6_13.SetRadio()
    this.Parent.oContained.w_SETIPRIF=trim(this.Parent.oContained.w_SETIPRIF)
    this.value = ;
      iif(this.Parent.oContained.w_SETIPRIF=="GE",1,;
      iif(this.Parent.oContained.w_SETIPRIF=="SL",2,;
      iif(this.Parent.oContained.w_SETIPRIF=="SA",3,;
      iif(this.Parent.oContained.w_SETIPRIF=="DF",4,;
      iif(this.Parent.oContained.w_SETIPRIF=="FI",5,;
      iif(this.Parent.oContained.w_SETIPRIF=="AG",6,;
      iif(this.Parent.oContained.w_SETIPRIF=="AL",7,;
      iif(this.Parent.oContained.w_SETIPRIF=="UO",8,;
      iif(this.Parent.oContained.w_SETIPRIF=="DI",9,;
      0)))))))))
  endfunc

  add object oSEINDIRI_6_14 as StdField with uid="VZNSLSVXUL",rtseq=151,rtrep=.f.,;
    cFormVar = "w_SEINDIRI", cQueryName = "SEINDIRI",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 235496303,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=79, Top=305, InputMask=replicate('X',35)

  add object oSE___CAP_6_15 as StdField with uid="NPBNQYAYRV",rtseq=152,rtrep=.f.,;
    cFormVar = "w_SE___CAP", cQueryName = "SE___CAP",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 164348790,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=79, Top=329, InputMask=replicate('X',8)

  add object oSELOCALI_6_16 as StdField with uid="QRGDTDYHMY",rtseq=153,rtrep=.f.,;
    cFormVar = "w_SELOCALI", cQueryName = "SELOCALI",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 100307823,;
   bGlobalFont=.t.,;
    Height=21, Width=220, Left=157, Top=329, InputMask=replicate('X',30)

  add object oSEPROVIN_6_17 as StdField with uid="BESNHQBMSR",rtseq=154,rtrep=.f.,;
    cFormVar = "w_SEPROVIN", cQueryName = "SEPROVIN",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 71445644,;
   bGlobalFont=.t.,;
    Height=21, Width=36, Left=379, Top=329, InputMask=replicate('X',2)

  add object oSETELEFO_6_18 as StdField with uid="TPMFGEKWFW",rtseq=155,rtrep=.f.,;
    cFormVar = "w_SETELEFO", cQueryName = "SETELEFO",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 92204171,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=79, Top=353, InputMask=replicate('X',18)

  add object oSE_EMAIL_6_19 as StdField with uid="CWTTAFBJCJ",rtseq=156,rtrep=.f.,;
    cFormVar = "w_SE_EMAIL", cQueryName = "SE_EMAIL",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 158219406,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=79, Top=377, InputMask=replicate('X',50)

  add object oSE_EMPEC_6_20 as StdField with uid="BKARUBURON",rtseq=157,rtrep=.f.,;
    cFormVar = "w_SE_EMPEC", cQueryName = "SE_EMPEC",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 174996631,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=79, Top=401, InputMask=replicate('X',50)

  add object oSEPREMAP_6_21 as StdCheck with uid="EWXZXJSFYG",rtseq=158,rtrep=.f.,left=79, top=426, caption="Sede preferita per le mappe", enabled=.f.,;
    ToolTipText = "Sede preferita per le mappe",;
    HelpContextID = 35509110,;
    cFormVar="w_SEPREMAP", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oSEPREMAP_6_21.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSEPREMAP_6_21.GetRadio()
    this.Parent.oContained.w_SEPREMAP = this.RadioValue()
    return .t.
  endfunc

  func oSEPREMAP_6_21.SetRadio()
    this.Parent.oContained.w_SEPREMAP=trim(this.Parent.oContained.w_SEPREMAP)
    this.value = ;
      iif(this.Parent.oContained.w_SEPREMAP=='S',1,;
      0)
  endfunc

  add object oDPCOGNOM_6_22 as StdField with uid="WQBYMPYFVO",rtseq=159,rtrep=.f.,;
    cFormVar = "w_DPCOGNOM", cQueryName = "DPCOGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 54136195,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=79, Top=281, InputMask=replicate('X',40)

  add object oDPNOME_6_23 as StdField with uid="LJERJZRAMT",rtseq=160,rtrep=.f.,;
    cFormVar = "w_DPNOME", cQueryName = "DPNOME",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 177913142,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=374, Top=281, InputMask=replicate('X',40)


  add object oBtn_6_24 as StdButton with uid="RPHQSXWHBW",left=526, top=78, width=48,height=45,;
    CpPicture="BMP\SEDE.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per accedere alla gestione della sede";
    , HelpContextID = 14824230;
    , tabstop=.f., caption='\<Sede';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_24.Click()
      with this.Parent.oContained
        GSAR_BSE(this.Parent.oContained, "A", NVL(.w_ZoomSedi.GetVar("SECODDES"),"") )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_6_25 as cp_calclbl with uid="WJYXUEMMMT",left=6, top=10, width=56,height=25,;
    caption='StrAzi',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=6;
    , HelpContextID = 245423578

  add object oSEDESPRE_6_26 as StdCheck with uid="WUVGTQCWWY",rtseq=170,rtrep=.f.,left=374, top=426, caption="Sede preferita di consegna merce", enabled=.f.,;
    ToolTipText = "Sede preferita di consegna merce",;
    HelpContextID = 168815765,;
    cFormVar="w_SEDESPRE", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oSEDESPRE_6_26.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSEDESPRE_6_26.GetRadio()
    this.Parent.oContained.w_SEDESPRE = this.RadioValue()
    return .t.
  endfunc

  func oSEDESPRE_6_26.SetRadio()
    this.Parent.oContained.w_SEDESPRE=trim(this.Parent.oContained.w_SEDESPRE)
    this.value = ;
      iif(this.Parent.oContained.w_SEDESPRE=='S',1,;
      0)
  endfunc

  func oSEDESPRE_6_26.mHide()
    with this.Parent.oContained
      return (not isahr())
    endwith
  endfunc

  add object oStr_6_3 as StdString with uid="DXDGRNWIBU",Visible=.t., Left=133, Top=10,;
    Alignment=1, Width=90, Height=15,;
    Caption="Rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_6_4 as StdString with uid="BWSWAYHVVC",Visible=.t., Left=36, Top=48,;
    Alignment=0, Width=484, Height=15,;
    Caption="Sedi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_6 as StdString with uid="QUEPDJEUKO",Visible=.t., Left=1, Top=260,;
    Alignment=1, Width=72, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  add object oStr_6_7 as StdString with uid="WAFNUPBULF",Visible=.t., Left=1, Top=307,;
    Alignment=1, Width=72, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_6_8 as StdString with uid="JQWRFNUFID",Visible=.t., Left=1, Top=330,;
    Alignment=1, Width=72, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_6_9 as StdString with uid="EINFSOPHEI",Visible=.t., Left=1, Top=353,;
    Alignment=1, Width=72, Height=18,;
    Caption="Tel.:"  ;
  , bGlobalFont=.t.

  add object oStr_6_10 as StdString with uid="JOLSAOATNM",Visible=.t., Left=1, Top=377,;
    Alignment=1, Width=72, Height=18,;
    Caption="e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_6_11 as StdString with uid="RBHFZAOTUY",Visible=.t., Left=1, Top=230,;
    Alignment=1, Width=72, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_6_27 as StdString with uid="RIIYPTVKSU",Visible=.t., Left=1, Top=401,;
    Alignment=1, Width=72, Height=18,;
    Caption="e-mail PEC:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsar_aziPag7 as StdContainer
  Width  = 719
  height = 492
  stdWidth  = 719
  stdheight = 492
  resizeXpos=307
  resizeYpos=233
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOAZI_7_2 as StdField with uid="WPDZLGFOGJ",rtseq=119,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90500134,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=10, InputMask=replicate('X',5)

  func oCOAZI_7_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_AZESLBIN)
        bRes2=.link_5_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDEAZI_7_3 as StdField with uid="HDLQEZWBVU",rtseq=120,rtrep=.f.,;
    cFormVar = "w_DEAZI", cQueryName = "DEAZI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 90497590,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=226, Top=10, InputMask=replicate('X',80)


  add object oLinkPC_7_4 as stdDynamicChildContainer with uid="KQDGMVJLWH",left=17, top=60, width=609, height=310, bOnScreen=.t.;



  add object oObj_7_6 as cp_calclbl with uid="GYWCKNXELR",left=6, top=10, width=56,height=25,;
    caption='StrAzi',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=7;
    , HelpContextID = 245423578

  add object oStr_7_1 as StdString with uid="QQNTYBUEXE",Visible=.t., Left=133, Top=10,;
    Alignment=1, Width=90, Height=15,;
    Caption="Rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_7_5 as StdString with uid="IDBRTXOGGU",Visible=.t., Left=22, Top=41,;
    Alignment=0, Width=602, Height=15,;
    Caption="Titolari"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_mtt",lower(this.oContained.GSAR_MTT.class))=0
        this.oContained.GSAR_MTT.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsar_aziPag8 as StdContainer
  Width  = 719
  height = 492
  stdWidth  = 719
  stdheight = 492
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOAZI_8_2 as StdField with uid="ZDWHTORKNN",rtseq=161,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90500134,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=10, InputMask=replicate('X',5)

  func oCOAZI_8_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_AZESLBIN)
        bRes2=.link_5_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDEAZI_8_3 as StdField with uid="WLVYUYWPVO",rtseq=162,rtrep=.f.,;
    cFormVar = "w_DEAZI", cQueryName = "DEAZI",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 90497590,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=226, Top=10, InputMask=replicate('X',80)


  add object oObj_8_4 as cp_calclbl with uid="ZAPKPHYVRW",left=6, top=10, width=56,height=25,;
    caption='StrAzi',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=8;
    , HelpContextID = 245423578


  add object oLinkPC_8_5 as stdDynamicChildContainer with uid="CWANRETGEX",left=16, top=65, width=648, height=289, bOnScreen=.t.;


  add object oStr_8_1 as StdString with uid="YOKMDMVNAT",Visible=.t., Left=133, Top=10,;
    Alignment=1, Width=90, Height=15,;
    Caption="Rag.sociale:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_abk",lower(this.oContained.gsar_abk.class))=0
        this.oContained.gsar_abk.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsar_aziPag9 as StdContainer
  Width  = 719
  height = 492
  stdWidth  = 719
  stdheight = 492
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZCONCON_9_8 as StdField with uid="RICZCJVEKQ",rtseq=144,rtrep=.f.,;
    cFormVar = "w_AZCONCON", cQueryName = "AZCONCON",;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Le registrazioni contabili antecedenti a tale data non possono essere modificate",;
    HelpContextID = 145364820,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=105, Top=81

  add object oAZCONVEN_9_9 as StdField with uid="CRGOEWOEHB",rtseq=145,rtrep=.f.,;
    cFormVar = "w_AZCONVEN", cQueryName = "AZCONVEN",;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "I documenti di vendita antecedenti a tale data non possono essere modificati",;
    HelpContextID = 72738988,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=105, Top=109

  add object oAZCONACQ_9_10 as StdField with uid="YEIQFXSUIZ",rtseq=146,rtrep=.f.,;
    cFormVar = "w_AZCONACQ", cQueryName = "AZCONACQ",;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "I documenti di acquisto antecedenti a tale data non possono essere modificati",;
    HelpContextID = 156625065,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=105, Top=137

  func oAZCONACQ_9_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZCONMAG_9_11 as StdField with uid="OVUTMNSWUJ",rtseq=147,rtrep=.f.,;
    cFormVar = "w_AZCONMAG", cQueryName = "AZCONMAG",;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "I movimenti di magazzino antecedenti a tale data non possono essere modificati",;
    HelpContextID = 44701517,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=105, Top=165

  func oAZCONMAG_9_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAZCONANA_9_12 as StdField with uid="HESVIRSSIZ",rtseq=148,rtrep=.f.,;
    cFormVar = "w_AZCONANA", cQueryName = "AZCONANA",;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "I movimenti di analitica antecedenti a tale data non possono essere modificati",;
    HelpContextID = 111810375,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=105, Top=193

  add object oCOAZI_9_14 as StdField with uid="MBUUIWUSQI",rtseq=163,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90500134,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=10, InputMask=replicate('X',5)

  func oCOAZI_9_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_AZESLBIN)
        bRes2=.link_5_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDEAZI_9_15 as StdField with uid="WTKXSEHHXA",rtseq=164,rtrep=.f.,;
    cFormVar = "w_DEAZI", cQueryName = "DEAZI",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 90497590,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=226, Top=10, InputMask=replicate('X',80)


  add object oObj_9_16 as cp_calclbl with uid="JMKUOEPAZT",left=6, top=10, width=56,height=25,;
    caption='StrAzi',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,alignment=1,;
    nPag=9;
    , HelpContextID = 245423578

  add object oStr_9_2 as StdString with uid="AWDMUXMNJJ",Visible=.t., Left=17, Top=53,;
    Alignment=0, Width=307, Height=18,;
    Caption="Date di blocco/consolidamento registrazioni"  ;
  , bGlobalFont=.t.

  add object oStr_9_3 as StdString with uid="VTFHKBYNAS",Visible=.t., Left=19, Top=83,;
    Alignment=1, Width=83, Height=18,;
    Caption="Primanota:"  ;
  , bGlobalFont=.t.

  add object oStr_9_4 as StdString with uid="SJXQXETLZU",Visible=.t., Left=16, Top=111,;
    Alignment=1, Width=86, Height=18,;
    Caption="Ciclo vendite:"  ;
  , bGlobalFont=.t.

  add object oStr_9_5 as StdString with uid="QKOYTUJUHG",Visible=.t., Left=7, Top=139,;
    Alignment=1, Width=95, Height=18,;
    Caption="Ciclo acquisti:"  ;
  , bGlobalFont=.t.

  func oStr_9_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_9_6 as StdString with uid="DVWCYOQPZD",Visible=.t., Left=33, Top=167,;
    Alignment=1, Width=69, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_9_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_9_7 as StdString with uid="AHALNHLODE",Visible=.t., Left=53, Top=195,;
    Alignment=1, Width=49, Height=18,;
    Caption="Analitica:"  ;
  , bGlobalFont=.t.

  add object oStr_9_13 as StdString with uid="CBIYUXVCLY",Visible=.t., Left=133, Top=10,;
    Alignment=1, Width=90, Height=15,;
    Caption="Rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oBox_9_1 as StdBox with uid="BABJTGHJKJ",left=16, top=72, width=253,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_azi','AZIENDA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AZCODAZI=AZIENDA.AZCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_azi
proc Esegui(pParent)
  Ah_ErrorMsg("Attenzione: la modifica potrebbe avere influenza su tutti i documenti gi� caricati")
endproc

proc RemoveBlankValue(obj)
if obj.nvalues>1 and obj.combovalues[1]=' '
  adel(obj.combovalues,1)
  obj.RemoveItem(1)
  obj.nvalues=obj.nvalues-1
  obj.SetRadio()
endif
endproc
* --- Fine Area Manuale
