* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bgs                                                        *
*              Treev. strutture bilancio                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_188]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-14                                                      *
* Last revis.: 2014-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bgs",oParentObject,m.pTipOp)
return(i_retval)

define class tgscg_bgs as StdBatch
  * --- Local variables
  pTipOp = space(10)
  w_TRCODICE = space(15)
  ZERI = space(10)
  w_DESCRI = space(50)
  w_NWROWORD = 0
  w_NWCODVOC = space(15)
  w_NWDESCRI = space(50)
  w_CPROWORD = 0
  w_CODVOC = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore da passare alla TreeView (da GSCG_KGS)
    * --- Se l'utente apre al treeview, chiude la gestione e chiude la treeview senza questo
    *     if andrebbe in errore
    if Type("this.oParentObject.oParentObject.w_TRCODICE")="U"
      i_retcode = 'stop'
      return
    endif
    this.w_TRCODICE = this.oParentObject.oParentObject.w_TRCODICE
    this.ZERI = "0000000000"
    do case
      case this.pTipOp="Reload"
        if EMPTY(this.oParentObject.w_CURSORNA)
          this.oParentObject.w_CURSORNA = SYS(2015)
        endif
        L_NomeCursore = this.oParentObject.w_CURSORNA
         
 CREATE CURSOR &L_NomeCursore (CPROWORD N(6), ; 
 CODICE C(15), VRCODVOC C(15), FLMACO C(6), DESCRI C(40), CAMPO C(60))
        * --- Reperimento dati su Voci di bilancio
        VQ_EXEC("QUERY\GSCG_BGS.VQR",this,"CursVoci")
        b = WRCURSOR("CursVoci")
        * --- Reperisco i dati dalla maschera padre
        SELECT * FROM (this.oParentObject.oParentObject.cTrsName) INTO CURSOR CursStrut ORDER BY t_CPROWORD NoFilter
        * --- Seleziono solo i valori utili
         
 SELECT DISTINCT CursStrut.t_CPROWORD, CursStrut.t_TRDESDET,CursVoci.* ; 
 FROM (CursStrut Left Outer Join CursVoci on CursVoci.VRROWORD=CursStrut.t_CPROWNUM) ; 
 INTO CURSOR CursSelez ORDER BY CursStrut.t_CPROWORD NoFilter
        SELECT CursSelez
        GO TOP
        SCAN
         
 INSERT INTO &L_NomeCursore (CPROWORD,CODICE,VRCODVOC,FLMACO,DESCRI,CAMPO) ; 
 VALUES (CursSelez.t_CPROWORD, ; 
 IIF(CursSelez.DVFLMACO="M",NVL(CursSelez.DVCODMAS,""), ; 
 IIF(CursSelez.DVFLMACO="G", ; 
 NVL(CursSelez.DVCODCON," ")," ")), ; 
 NVL(CursSelez.VRCODVOC," "), ; 
 IIF(CursSelez.DVFLMACO="M",Ah_MsgFormat("Mastro"),IIF(CursSelez.DVFLMACO="G", ; 
 Ah_MsgFormat("Conto")," ")), CursSelez.t_TRDESDET, ; 
 ALLTRIM(NVL(CursSelez.VRCODVOC," "))+"     "+ALLTRIM(CursSelez.t_TRDESDET)) 
 
        ENDSCAN
        * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
        SELECT ( L_NomeCursore )
        GO TOP
        cp_Level1( this.oParentObject.w_CURSORNA ,"CPROWORD","")
        DELETE FROM &L_NomeCursore WHERE &L_NomeCursore..CODICE=" " AND SUBSTR(&L_NomeCursore..LVLKEY,14,1)="1"
        UPDATE &L_NomeCursore SET &L_NomeCursore..CAMPO=ALLTRIM(&L_NomeCursore..FLMACO)+":   "+ALLTRIM(&L_NomeCursore..CODICE) WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+1,1)="."
        if used("CursStrut")
          select CursStrut
          use
        endif
        if used("CursVoci")
          select CursVoci
          use
        endif
        if used("CursSelez")
          select CursSelez
          use
        endif
        this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
        this.oParentObject.w_TREEVIEW.FillTree()     
      case this.pTipop="Close"
        * --- Elimina i cursori
        if used( this.oParentObject.w_CURSORNA )
          select ( this.oParentObject.w_CURSORNA )
          use
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipOp)
    this.pTipOp=pTipOp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOp"
endproc
