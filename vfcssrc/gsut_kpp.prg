* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kpp                                                        *
*              Parametri generali (PostaLite)                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-28                                                      *
* Last revis.: 2011-01-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kpp",oParentObject))

* --- Class definition
define class tgsut_kpp as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 584
  Height = 440
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-01-11"
  HelpContextID=118930583
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=1

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kpp"
  cComment = "Parametri generali (PostaLite)"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CHIAVE = 0
  o_CHIAVE = 0

  * --- Children pointers
  GSUT_APL = .NULL.
  GSUT_MP2 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSUT_APL additive
    *set procedure to GSUT_MP2 additive
    with this
      .Pages(1).addobject("oPag","tgsut_kppPag1","gsut_kpp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSUT_APL
    *release procedure GSUT_MP2
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSUT_APL = CREATEOBJECT('stdDynamicChild',this,'GSUT_APL',this.oPgFrm.Page1.oPag.oLinkPC_1_2)
    this.GSUT_APL.createrealchild()
    this.GSUT_MP2 = CREATEOBJECT('stdDynamicChild',this,'GSUT_MP2',this.oPgFrm.Page1.oPag.oLinkPC_1_3)
    this.GSUT_MP2.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_APL)
      this.GSUT_APL.DestroyChildrenChain()
      this.GSUT_APL=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_2')
    if !ISNULL(this.GSUT_MP2)
      this.GSUT_MP2.DestroyChildrenChain()
      this.GSUT_MP2=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_APL.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSUT_MP2.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_APL.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSUT_MP2.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_APL.NewDocument()
    this.GSUT_MP2.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSUT_APL.SetKey(;
            .w_CHIAVE,"PLSERIAL";
            )
      this.GSUT_MP2.SetKey(;
            .w_CHIAVE,"POSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSUT_APL.ChangeRow(this.cRowID+'      1',1;
             ,.w_CHIAVE,"PLSERIAL";
             )
      .GSUT_MP2.ChangeRow(this.cRowID+'      1',1;
             ,.w_CHIAVE,"POSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSUT_APL)
        i_f=.GSUT_APL.BuildFilter()
        if !(i_f==.GSUT_APL.cQueryFilter)
          i_fnidx=.GSUT_APL.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_APL.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_APL.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_APL.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_APL.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSUT_MP2)
        i_f=.GSUT_MP2.BuildFilter()
        if !(i_f==.GSUT_MP2.cQueryFilter)
          i_fnidx=.GSUT_MP2.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_MP2.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_MP2.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_MP2.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_MP2.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSUT_APL(.f.)
      this.Save_GSUT_MP2(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSUT_APL.IsAChildUpdated() or this.GSUT_MP2.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSUT_APL(i_ask)
    if this.GSUT_APL.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP))
      cp_BeginTrs()
      this.GSUT_APL.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc
  proc Save_GSUT_MP2(i_ask)
    if this.GSUT_MP2.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP))
      cp_BeginTrs()
      this.GSUT_MP2.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CHIAVE=0
        .w_CHIAVE = 1
      .GSUT_APL.NewDocument()
      .GSUT_APL.ChangeRow('1',1,.w_CHIAVE,"PLSERIAL")
      if not(.GSUT_APL.bLoaded)
        .GSUT_APL.SetKey(.w_CHIAVE,"PLSERIAL")
      endif
      .GSUT_MP2.NewDocument()
      .GSUT_MP2.ChangeRow('1',1,.w_CHIAVE,"POSERIAL")
      if not(.GSUT_MP2.bLoaded)
        .GSUT_MP2.SetKey(.w_CHIAVE,"POSERIAL")
      endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSUT_APL.SetStatus(i_cOp)
    this.GSUT_MP2.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_APL.SetChildrenStatus(i_cOp)
  *  this.GSUT_MP2.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_CHIAVE<>.o_CHIAVE
          .Save_GSUT_APL(.t.)
          .GSUT_APL.NewDocument()
          .GSUT_APL.ChangeRow('1',1,.w_CHIAVE,"PLSERIAL")
          if not(.GSUT_APL.bLoaded)
            .GSUT_APL.SetKey(.w_CHIAVE,"PLSERIAL")
          endif
        endif
        if .w_CHIAVE<>.o_CHIAVE
          .Save_GSUT_MP2(.t.)
          .GSUT_MP2.NewDocument()
          .GSUT_MP2.ChangeRow('1',1,.w_CHIAVE,"POSERIAL")
          if not(.GSUT_MP2.bLoaded)
            .GSUT_MP2.SetKey(.w_CHIAVE,"POSERIAL")
          endif
        endif
      endwith
      this.DoRTCalc(1,1,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSUT_APL.CheckForm()
      if i_bres
        i_bres=  .GSUT_APL.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSUT_MP2.CheckForm()
      if i_bres
        i_bres=  .GSUT_MP2.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CHIAVE = this.w_CHIAVE
    * --- GSUT_APL : Depends On
    this.GSUT_APL.SaveDependsOn()
    * --- GSUT_MP2 : Depends On
    this.GSUT_MP2.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsut_kppPag1 as StdContainer
  Width  = 580
  height = 440
  stdWidth  = 580
  stdheight = 440
  resizeXpos=355
  resizeYpos=308
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_1_2 as stdDynamicChildContainer with uid="BBFNJHTOPS",left=5, top=8, width=502, height=217, bOnScreen=.t.;



  add object oLinkPC_1_3 as stdDynamicChildContainer with uid="HPJTJSDEKR",left=3, top=248, width=571, height=186, bOnScreen=.t.;


  add object oStr_1_4 as StdString with uid="WLZVWKPSOI",Visible=.t., Left=11, Top=229,;
    Alignment=0, Width=337, Height=15,;
    Caption="Elenco workstations abilitate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kpp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
