* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mdm                                                        *
*              Elenco totalizzatori manuali                                    *
*                                                                              *
*      Author: TAM SOFTWARE & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-12-22                                                      *
* Last revis.: 2011-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mdm")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mdm")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mdm")
  return

* --- Class definition
define class tgscg_mdm as StdPCForm
  Width  = 770
  Height = 311
  Top    = 31
  Left   = 22
  cComment = "Elenco totalizzatori manuali"
  cPrg = "gscg_mdm"
  HelpContextID=188078743
  add object cnt as tcgscg_mdm
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mdm as PCContext
  w_MRNUMBIL = 0
  w_MRCODESE = space(4)
  w_SALVA = space(1)
  w_CPROWORD = 0
  w_MRCODTOT = space(10)
  w_MRCODMIS = space(15)
  w_MRCODFON = space(10)
  w_MRIMPORT = 0
  w_DESCRI = space(40)
  w_MDESCRI = space(80)
  w_TIPTOT = space(1)
  proc Save(i_oFrom)
    this.w_MRNUMBIL = i_oFrom.w_MRNUMBIL
    this.w_MRCODESE = i_oFrom.w_MRCODESE
    this.w_SALVA = i_oFrom.w_SALVA
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_MRCODTOT = i_oFrom.w_MRCODTOT
    this.w_MRCODMIS = i_oFrom.w_MRCODMIS
    this.w_MRCODFON = i_oFrom.w_MRCODFON
    this.w_MRIMPORT = i_oFrom.w_MRIMPORT
    this.w_DESCRI = i_oFrom.w_DESCRI
    this.w_MDESCRI = i_oFrom.w_MDESCRI
    this.w_TIPTOT = i_oFrom.w_TIPTOT
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MRNUMBIL = this.w_MRNUMBIL
    i_oTo.w_MRCODESE = this.w_MRCODESE
    i_oTo.w_SALVA = this.w_SALVA
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_MRCODTOT = this.w_MRCODTOT
    i_oTo.w_MRCODMIS = this.w_MRCODMIS
    i_oTo.w_MRCODFON = this.w_MRCODFON
    i_oTo.w_MRIMPORT = this.w_MRIMPORT
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.w_MDESCRI = this.w_MDESCRI
    i_oTo.w_TIPTOT = this.w_TIPTOT
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mdm as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 770
  Height = 311
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-28"
  HelpContextID=188078743
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DET_MANU_IDX = 0
  TOT_MAST_IDX = 0
  TAB_MISU_IDX = 0
  TOT_DETT_IDX = 0
  cFile = "DET_MANU"
  cKeySelect = "MRNUMBIL,MRCODESE"
  cKeyWhere  = "MRNUMBIL=this.w_MRNUMBIL and MRCODESE=this.w_MRCODESE"
  cKeyDetail  = "MRNUMBIL=this.w_MRNUMBIL and MRCODESE=this.w_MRCODESE"
  cKeyWhereODBC = '"MRNUMBIL="+cp_ToStrODBC(this.w_MRNUMBIL)';
      +'+" and MRCODESE="+cp_ToStrODBC(this.w_MRCODESE)';

  cKeyDetailWhereODBC = '"MRNUMBIL="+cp_ToStrODBC(this.w_MRNUMBIL)';
      +'+" and MRCODESE="+cp_ToStrODBC(this.w_MRCODESE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DET_MANU.MRNUMBIL="+cp_ToStrODBC(this.w_MRNUMBIL)';
      +'+" and DET_MANU.MRCODESE="+cp_ToStrODBC(this.w_MRCODESE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DET_MANU.CPROWORD '
  cPrg = "gscg_mdm"
  cComment = "Elenco totalizzatori manuali"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MRNUMBIL = 0
  w_MRCODESE = space(4)
  w_SALVA = .F.
  w_CPROWORD = 0
  w_MRCODTOT = space(10)
  o_MRCODTOT = space(10)
  w_MRCODMIS = space(15)
  w_MRCODFON = space(10)
  w_MRIMPORT = 0
  w_DESCRI = space(40)
  w_MDESCRI = space(80)
  w_TIPTOT = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mdmPag1","gscg_mdm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='TOT_MAST'
    this.cWorkTables[2]='TAB_MISU'
    this.cWorkTables[3]='TOT_DETT'
    this.cWorkTables[4]='DET_MANU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DET_MANU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DET_MANU_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mdm'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DET_MANU where MRNUMBIL=KeySet.MRNUMBIL
    *                            and MRCODESE=KeySet.MRCODESE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DET_MANU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_MANU_IDX,2],this.bLoadRecFilter,this.DET_MANU_IDX,"gscg_mdm")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DET_MANU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DET_MANU.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DET_MANU '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MRNUMBIL',this.w_MRNUMBIL  ,'MRCODESE',this.w_MRCODESE  )
      select * from (i_cTable) DET_MANU where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SALVA = .F.
        .w_TIPTOT = space(1)
        .w_MRNUMBIL = NVL(MRNUMBIL,0)
        .w_MRCODESE = NVL(MRCODESE,space(4))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DET_MANU')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCRI = space(40)
          .w_MDESCRI = space(80)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MRCODTOT = NVL(MRCODTOT,space(10))
          if link_2_2_joined
            this.w_MRCODTOT = NVL(TICODICE202,NVL(this.w_MRCODTOT,space(10)))
            this.w_TIPTOT = NVL(TITIPTOT202,space(1))
            this.w_MRCODFON = NVL(TI_FONTE202,space(10))
            this.w_DESCRI = NVL(TIDESCRI202,space(40))
          else
          .link_2_2('Load')
          endif
          .w_MRCODMIS = NVL(MRCODMIS,space(15))
          .link_2_3('Load')
          .w_MRCODFON = NVL(MRCODFON,space(10))
          .w_MRIMPORT = NVL(MRIMPORT,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MRNUMBIL=0
      .w_MRCODESE=space(4)
      .w_SALVA=.f.
      .w_CPROWORD=10
      .w_MRCODTOT=space(10)
      .w_MRCODMIS=space(15)
      .w_MRCODFON=space(10)
      .w_MRIMPORT=0
      .w_DESCRI=space(40)
      .w_MDESCRI=space(80)
      .w_TIPTOT=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_SALVA = .F.
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_MRCODTOT))
         .link_2_2('Full')
        endif
        .w_MRCODMIS = SPACE(15)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MRCODMIS))
         .link_2_3('Full')
        endif
        .DoRTCalc(7,7,.f.)
        .w_MRIMPORT = 0
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DET_MANU')
    this.DoRTCalc(9,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DET_MANU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DET_MANU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRNUMBIL,"MRNUMBIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODESE,"MRCODESE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MRCODTOT C(10);
      ,t_MRCODMIS C(15);
      ,t_MRIMPORT N(18,4);
      ,t_DESCRI C(40);
      ,t_MDESCRI C(80);
      ,CPROWNUM N(10);
      ,t_MRCODFON C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mdmbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODTOT_2_2.controlsource=this.cTrsName+'.t_MRCODTOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODMIS_2_3.controlsource=this.cTrsName+'.t_MRCODMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRIMPORT_2_5.controlsource=this.cTrsName+'.t_MRIMPORT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_6.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFRm.Page1.oPag.oMDESCRI_2_7.controlsource=this.cTrsName+'.t_MDESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(74)
    this.AddVLine(195)
    this.AddVLine(464)
    this.AddVLine(586)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODTOT_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DET_MANU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_MANU_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DET_MANU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_MANU_IDX,2])
      *
      * insert into DET_MANU
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DET_MANU')
        i_extval=cp_InsertValODBCExtFlds(this,'DET_MANU')
        i_cFldBody=" "+;
                  "(MRNUMBIL,MRCODESE,CPROWORD,MRCODTOT,MRCODMIS"+;
                  ",MRCODFON,MRIMPORT,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MRNUMBIL)+","+cp_ToStrODBC(this.w_MRCODESE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_MRCODTOT)+","+cp_ToStrODBCNull(this.w_MRCODMIS)+;
             ","+cp_ToStrODBC(this.w_MRCODFON)+","+cp_ToStrODBC(this.w_MRIMPORT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DET_MANU')
        i_extval=cp_InsertValVFPExtFlds(this,'DET_MANU')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MRNUMBIL',this.w_MRNUMBIL,'MRCODESE',this.w_MRCODESE)
        INSERT INTO (i_cTable) (;
                   MRNUMBIL;
                  ,MRCODESE;
                  ,CPROWORD;
                  ,MRCODTOT;
                  ,MRCODMIS;
                  ,MRCODFON;
                  ,MRIMPORT;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MRNUMBIL;
                  ,this.w_MRCODESE;
                  ,this.w_CPROWORD;
                  ,this.w_MRCODTOT;
                  ,this.w_MRCODMIS;
                  ,this.w_MRCODFON;
                  ,this.w_MRIMPORT;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DET_MANU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_MANU_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWORD<>0 AND Not Empt(t_MRCODTOT) and Not Empty(t_MRCODMIS)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DET_MANU')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DET_MANU')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 AND Not Empt(t_MRCODTOT) and Not Empty(t_MRCODMIS)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DET_MANU
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DET_MANU')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MRCODTOT="+cp_ToStrODBCNull(this.w_MRCODTOT)+;
                     ",MRCODMIS="+cp_ToStrODBCNull(this.w_MRCODMIS)+;
                     ",MRCODFON="+cp_ToStrODBC(this.w_MRCODFON)+;
                     ",MRIMPORT="+cp_ToStrODBC(this.w_MRIMPORT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DET_MANU')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MRCODTOT=this.w_MRCODTOT;
                     ,MRCODMIS=this.w_MRCODMIS;
                     ,MRCODFON=this.w_MRCODFON;
                     ,MRIMPORT=this.w_MRIMPORT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DET_MANU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_MANU_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND Not Empt(t_MRCODTOT) and Not Empty(t_MRCODMIS)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DET_MANU
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND Not Empt(t_MRCODTOT) and Not Empty(t_MRCODMIS)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DET_MANU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_MANU_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_MRCODTOT<>.w_MRCODTOT
          .w_MRCODMIS = SPACE(15)
          .link_2_3('Full')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_MRCODTOT<>.w_MRCODTOT
          .w_MRIMPORT = 0
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MRCODFON with this.w_MRCODFON
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRIMPORT_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRIMPORT_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MRCODTOT
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TOT_MAST_IDX,3]
    i_lTable = "TOT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TOT_MAST_IDX,2], .t., this.TOT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TOT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODTOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MTI',True,'TOT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_MRCODTOT)+"%");

          i_ret=cp_SQL(i_nConn,"select TICODICE,TITIPTOT,TI_FONTE,TIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TICODICE',trim(this.w_MRCODTOT))
          select TICODICE,TITIPTOT,TI_FONTE,TIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODTOT)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODTOT) and !this.bDontReportError
            deferred_cp_zoom('TOT_MAST','*','TICODICE',cp_AbsName(oSource.parent,'oMRCODTOT_2_2'),i_cWhere,'GSCG_MTI',"ELENCO TOTALIZZATORI",'Totmanu.TOT_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TICODICE,TITIPTOT,TI_FONTE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TICODICE',oSource.xKey(1))
            select TICODICE,TITIPTOT,TI_FONTE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODTOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TICODICE,TITIPTOT,TI_FONTE,TIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_MRCODTOT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TICODICE',this.w_MRCODTOT)
            select TICODICE,TITIPTOT,TI_FONTE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODTOT = NVL(_Link_.TICODICE,space(10))
      this.w_TIPTOT = NVL(_Link_.TITIPTOT,space(1))
      this.w_MRCODFON = NVL(_Link_.TI_FONTE,space(10))
      this.w_DESCRI = NVL(_Link_.TIDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODTOT = space(10)
      endif
      this.w_TIPTOT = space(1)
      this.w_MRCODFON = space(10)
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTOT='M' or empty(.w_MRCODTOT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Totalizzatore errato o non di tipo manuale")
        endif
        this.w_MRCODTOT = space(10)
        this.w_TIPTOT = space(1)
        this.w_MRCODFON = space(10)
        this.w_DESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TOT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TOT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODTOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TOT_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TOT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.TICODICE as TICODICE202"+ ",link_2_2.TITIPTOT as TITIPTOT202"+ ",link_2_2.TI_FONTE as TI_FONTE202"+ ",link_2_2.TIDESCRI as TIDESCRI202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on DET_MANU.MRCODTOT=link_2_2.TICODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and DET_MANU.MRCODTOT=link_2_2.TICODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRCODMIS
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TOT_DETT_IDX,3]
    i_lTable = "TOT_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TOT_DETT_IDX,2], .t., this.TOT_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TOT_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gscg_mti',True,'TOT_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODCOM like "+cp_ToStrODBC(trim(this.w_MRCODMIS)+"%");
                   +" and TICODICE="+cp_ToStrODBC(this.w_MRCODTOT);

          i_ret=cp_SQL(i_nConn,"select TICODICE,TICODCOM,TIDESMIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TICODICE,TICODCOM","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TICODICE',this.w_MRCODTOT;
                     ,'TICODCOM',trim(this.w_MRCODMIS))
          select TICODICE,TICODCOM,TIDESMIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TICODICE,TICODCOM into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODMIS)==trim(_Link_.TICODCOM) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODMIS) and !this.bDontReportError
            deferred_cp_zoom('TOT_DETT','*','TICODICE,TICODCOM',cp_AbsName(oSource.parent,'oMRCODMIS_2_3'),i_cWhere,'gscg_mti',"MISURE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MRCODTOT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TICODICE,TICODCOM,TIDESMIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TICODICE,TICODCOM,TIDESMIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Misura errata o non associata al totalizzatore selezionato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TICODICE,TICODCOM,TIDESMIS";
                     +" from "+i_cTable+" "+i_lTable+" where TICODCOM="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TICODICE="+cp_ToStrODBC(this.w_MRCODTOT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TICODICE',oSource.xKey(1);
                       ,'TICODCOM',oSource.xKey(2))
            select TICODICE,TICODCOM,TIDESMIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TICODICE,TICODCOM,TIDESMIS";
                   +" from "+i_cTable+" "+i_lTable+" where TICODCOM="+cp_ToStrODBC(this.w_MRCODMIS);
                   +" and TICODICE="+cp_ToStrODBC(this.w_MRCODTOT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TICODICE',this.w_MRCODTOT;
                       ,'TICODCOM',this.w_MRCODMIS)
            select TICODICE,TICODCOM,TIDESMIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODMIS = NVL(_Link_.TICODCOM,space(15))
      this.w_MDESCRI = NVL(_Link_.TIDESMIS,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODMIS = space(15)
      endif
      this.w_MDESCRI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TOT_DETT_IDX,2])+'\'+cp_ToStr(_Link_.TICODICE,1)+'\'+cp_ToStr(_Link_.TICODCOM,1)
      cp_ShowWarn(i_cKey,this.TOT_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMDESCRI_2_7.value==this.w_MDESCRI)
      this.oPgFrm.Page1.oPag.oMDESCRI_2_7.value=this.w_MDESCRI
      replace t_MDESCRI with this.oPgFrm.Page1.oPag.oMDESCRI_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODTOT_2_2.value==this.w_MRCODTOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODTOT_2_2.value=this.w_MRCODTOT
      replace t_MRCODTOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODTOT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODMIS_2_3.value==this.w_MRCODMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODMIS_2_3.value=this.w_MRCODMIS
      replace t_MRCODMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODMIS_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRIMPORT_2_5.value==this.w_MRIMPORT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRIMPORT_2_5.value=this.w_MRIMPORT
      replace t_MRIMPORT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRIMPORT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_6.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_6.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'DET_MANU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_TIPTOT='M' or empty(.w_MRCODTOT)) and not(empty(.w_MRCODTOT)) and (.w_CPROWORD<>0 AND Not Empt(.w_MRCODTOT) and Not Empty(.w_MRCODMIS))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODTOT_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Totalizzatore errato o non di tipo manuale")
      endcase
      if .w_CPROWORD<>0 AND Not Empt(.w_MRCODTOT) and Not Empty(.w_MRCODMIS)
        * --- Area Manuale = Check Row
        * --- gscg_mdm
        * Controllo che la matricola inserita non sia gia presente
        * nel dettaglio
        Local L_Posizione,L_Area,l_codmis,L_Trovato,L_codTot
        
        L_Area = Select()
        
        Select (this.cTrsName)
        L_Posizione=Recno(this.cTrsName)
        l_codmis= t_MRCODMIS
        L_CodTot= t_MRCODTOT
        
        if L_Posizione<>0 And Not Empty(l_codmis)
        	
        	Go Top
        	
        	LOCATE FOR t_MRCODTOT = L_CodTot AND t_MRCODMIS = l_codmis And Not Deleted()
        	
        	L_Trovato=Found()
        	
        	if L_Trovato And Recno()<>L_Posizione
        		i_bRes = .f.
        		i_bnoChk = .f.
        		i_cErrorMsg = Ah_MsgFormat("Misura gi� utilizzata nel dettaglio")
        	ELse
        		if L_Trovato
        			Continue
        			if Found() And Recno()<>L_Posizione
        				i_bRes = .f.
        				i_bnoChk = .f.
        				i_cErrorMsg = Ah_MsgFormat("Misura gi� utilizzata nel dettaglio")
        			endif
        		Endif
        	Endif
        
        	* mi riposiziono nella riga di partenza
        	Select (this.cTrsName)
        	Go L_Posizione
        endif
        			
        * mi rimetto nella vecchia area
        Select (L_Area)
        
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MRCODTOT = this.w_MRCODTOT
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND Not Empt(t_MRCODTOT) and Not Empty(t_MRCODMIS))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MRCODTOT=space(10)
      .w_MRCODMIS=space(15)
      .w_MRCODFON=space(10)
      .w_MRIMPORT=0
      .w_DESCRI=space(40)
      .w_MDESCRI=space(80)
      .DoRTCalc(1,5,.f.)
      if not(empty(.w_MRCODTOT))
        .link_2_2('Full')
      endif
        .w_MRCODMIS = SPACE(15)
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_MRCODMIS))
        .link_2_3('Full')
      endif
      .DoRTCalc(7,7,.f.)
        .w_MRIMPORT = 0
    endwith
    this.DoRTCalc(9,11,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MRCODTOT = t_MRCODTOT
    this.w_MRCODMIS = t_MRCODMIS
    this.w_MRCODFON = t_MRCODFON
    this.w_MRIMPORT = t_MRIMPORT
    this.w_DESCRI = t_DESCRI
    this.w_MDESCRI = t_MDESCRI
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MRCODTOT with this.w_MRCODTOT
    replace t_MRCODMIS with this.w_MRCODMIS
    replace t_MRCODFON with this.w_MRCODFON
    replace t_MRIMPORT with this.w_MRIMPORT
    replace t_DESCRI with this.w_DESCRI
    replace t_MDESCRI with this.w_MDESCRI
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mdmPag1 as StdContainer
  Width  = 766
  height = 311
  stdWidth  = 766
  stdheight = 311
  resizeXpos=369
  resizeYpos=147
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=6, width=750,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Rigo",Field2="MRCODTOT",Label2="Totalizzatore",Field3="DESCRI",Label3="Descrizione",Field4="MRCODMIS",Label4="Misura",Field5="MRIMPORT",Label5="Importo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32701318

  add object oStr_1_4 as StdString with uid="NNPRBMBFIA",Visible=.t., Left=10, Top=263,;
    Alignment=1, Width=123, Height=18,;
    Caption="Descrizione misura:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=25,;
    width=746+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=26,width=745+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TOT_MAST|TOT_DETT|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oMDESCRI_2_7.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TOT_MAST'
        oDropInto=this.oBodyCol.oRow.oMRCODTOT_2_2
      case cFile='TOT_DETT'
        oDropInto=this.oBodyCol.oRow.oMRCODMIS_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oMDESCRI_2_7 as StdTrsField with uid="BMVHJQNTPM",rtseq=10,rtrep=.t.,;
    cFormVar="w_MDESCRI",value=space(80),enabled=.f.,;
    ToolTipText = "Descrizione misura",;
    HelpContextID = 29193158,;
    cTotal="", bFixedPos=.t., cQueryName = "MDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=555, Left=138, Top=261, InputMask=replicate('X',80)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mdmBodyRow as CPBodyRowCnt
  Width=736
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="OKPGGIQPHF",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,enabled=.f.,;
    ToolTipText = "Progressivo riga",;
    HelpContextID = 372886,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=63, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], BackStyle = 0

  add object oMRCODTOT_2_2 as StdTrsField with uid="PNLRLLDNAV",rtseq=5,rtrep=.t.,;
    cFormVar="w_MRCODTOT",value=space(10),;
    HelpContextID = 204905958,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Totalizzatore errato o non di tipo manuale",;
   bGlobalFont=.t.,;
    Height=17, Width=114, Left=70, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TOT_MAST", cZoomOnZoom="GSCG_MTI", oKey_1_1="TICODICE", oKey_1_2="this.w_MRCODTOT"

  func oMRCODTOT_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
      if .not. empty(.w_MRCODMIS)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMRCODTOT_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODTOT_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TOT_MAST','*','TICODICE',cp_AbsName(this.parent,'oMRCODTOT_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MTI',"ELENCO TOTALIZZATORI",'Totmanu.TOT_MAST_VZM',this.parent.oContained
  endproc
  proc oMRCODTOT_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MTI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TICODICE=this.parent.oContained.w_MRCODTOT
    i_obj.ecpSave()
  endproc

  add object oMRCODMIS_2_3 as StdTrsField with uid="FEPGKHNAFU",rtseq=6,rtrep=.t.,;
    cFormVar="w_MRCODMIS",value=space(15),;
    HelpContextID = 214524441,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Misura errata o non associata al totalizzatore selezionato",;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=457, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TOT_DETT", cZoomOnZoom="gscg_mti", oKey_1_1="TICODICE", oKey_1_2="this.w_MRCODTOT", oKey_2_1="TICODCOM", oKey_2_2="this.w_MRCODMIS"

  func oMRCODMIS_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODMIS_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODMIS_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TOT_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TICODICE="+cp_ToStrODBC(this.Parent.oContained.w_MRCODTOT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TICODICE="+cp_ToStr(this.Parent.oContained.w_MRCODTOT)
    endif
    do cp_zoom with 'TOT_DETT','*','TICODICE,TICODCOM',cp_AbsName(this.parent,'oMRCODMIS_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'gscg_mti',"MISURE",'',this.parent.oContained
  endproc
  proc oMRCODMIS_2_3.mZoomOnZoom
    local i_obj
    i_obj=gscg_mti()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TICODICE=w_MRCODTOT
     i_obj.w_TICODCOM=this.parent.oContained.w_MRCODMIS
    i_obj.ecpSave()
  endproc

  add object oMRIMPORT_2_5 as StdTrsField with uid="CVMYHRXMWM",rtseq=8,rtrep=.t.,;
    cFormVar="w_MRIMPORT",value=0,;
    HelpContextID = 7880166,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=152, Left=579, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)], BackStyle = 0

  func oMRIMPORT_2_5.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_MRCODMIS))
    endwith
  endfunc

  add object oDESCRI_2_6 as StdTrsField with uid="QOKPVLLJNZ",rtseq=9,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione voce",;
    HelpContextID = 107064266,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=266, Left=188, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover
  * ---
  func oMRCODTOT_2_2.When()
    return(.t.)
  proc oMRCODTOT_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMRCODTOT_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mdm','DET_MANU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MRNUMBIL=DET_MANU.MRNUMBIL";
  +" and "+i_cAliasName2+".MRCODESE=DET_MANU.MRCODESE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
