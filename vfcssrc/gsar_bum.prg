* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bum                                                        *
*              Controlla cancellazione UM                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-07-14                                                      *
* Last revis.: 2009-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bum",oParentObject)
return(i_retval)

define class tgsar_bum as StdBatch
  * --- Local variables
  w_CONTA = 0
  w_CONTA1 = 0
  w_MESS = space(10)
  w_OK = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da GSAR_AUM: inibisce la cancellazione se l'UM � presente in altre tabelle
    this.w_OK = .F.
    * --- Select from GSAR_AUM
    do vq_exec with 'GSAR_AUM',this,'_Curs_GSAR_AUM','',.f.,.t.
    if used('_Curs_GSAR_AUM')
      select _Curs_GSAR_AUM
      locate for 1=1
      do while not(eof())
      this.w_CONTA = NVL(_Curs_GSAR_AUM.CONTA, 0)
      this.w_CONTA1 = this.w_CONTA1 + this.w_CONTA
      this.w_OK = .T.
        select _Curs_GSAR_AUM
        continue
      enddo
      use
    endif
    if !this.w_OK
      if g_AGEN="S"
        * --- Select from GSAR2AUM
        do vq_exec with 'GSAR2AUM',this,'_Curs_GSAR2AUM','',.f.,.t.
        if used('_Curs_GSAR2AUM')
          select _Curs_GSAR2AUM
          locate for 1=1
          do while not(eof())
          this.w_CONTA = NVL(_Curs_GSAR2AUM.CONTA, 0)
          this.w_CONTA1 = this.w_CONTA1 + this.w_CONTA
            select _Curs_GSAR2AUM
            continue
          enddo
          use
        endif
      endif
    endif
    if this.w_CONTA1>0
      ah_ErrorMsg("L'unit� di misura che si intende cancellare%0� presente in altre tabelle%0Impossibile cancellare",,"")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Eliminazione annullata")
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSAR_AUM')
      use in _Curs_GSAR_AUM
    endif
    if used('_Curs_GSAR2AUM')
      use in _Curs_GSAR2AUM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
