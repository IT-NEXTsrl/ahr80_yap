* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_b11                                                        *
*              Elabora coge ratei/risc. x S.a                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_155]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-12                                                      *
* Last revis.: 2007-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_b11",oParentObject)
return(i_retval)

define class tgscg_b11 as StdBatch
  * --- Local variables
  w_FILINI = ctod("  /  /  ")
  w_VALNAZ = space(3)
  w_GIOPER = 0
  w_COD001 = space(15)
  w_FILFIN = ctod("  /  /  ")
  w_CODBUN = space(3)
  w_GIOCOM = 0
  w_COD002 = space(15)
  w_TIPCON = space(10)
  w_INICOM = ctod("  /  /  ")
  w_RATEO = 0
  w_COD003 = space(15)
  w_CODICE = space(15)
  w_FINCOM = ctod("  /  /  ")
  w_RISCON = 0
  w_IMPDAR = 0
  w_TOTIMP = 0
  w_CAOVAL = 0
  w_APPO = space(15)
  w_IMPAVE = 0
  w_RISATT = space(15)
  w_RECODICE = space(15)
  w_DATREG = ctod("  /  /  ")
  w_RATATT = space(15)
  w_RISPAS = space(15)
  w_REDESCRI = space(45)
  w_CONSUP = space(15)
  w_RATPAS = space(15)
  w_TIPSEZ = space(1)
  w_SERIAL = space(10)
  w_RAGGSCRI = space(1)
  w_DESCRI = space(50)
  w_CODCEN = space(15)
  w_VOCCEN = space(15)
  w_CODCOM = space(15)
  w_CODCEN = space(15)
  w_VOCCEN = space(15)
  w_CODCOM = space(15)
  w_DATCIN = ctod("  /  /  ")
  w_DATCFI = ctod("  /  /  ")
  w_PARAMETRO = 0
  w_TOTIMP = 0
  w_PNSERIAL = space(10)
  w_IMPORTO = 0
  w_TIPO = space(1)
  w_CODTESTA = space(15)
  * --- WorkFile variables
  CONTI_idx=0
  MASTRI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Genera Assestamento (da GSCG_BAT)
    * --- Viene impostato nelle regole di Elaborazione relativi alla Primanota COGE
    this.w_FILINI = this.oParentObject.oParentObject.w_ASDATINI
    this.w_FILFIN = this.oParentObject.oParentObject.w_ASDATFIN
    this.w_RISATT = this.oParentObject.oParentObject.w_CORISATT
    this.w_RISPAS = this.oParentObject.oParentObject.w_CORISPAS
    this.w_RATATT = this.oParentObject.oParentObject.w_CORATATT
    this.w_RATPAS = this.oParentObject.oParentObject.w_CORATPAS
    this.w_RAGGSCRI = this.oParentObject.oParentObject.w_RAGGSCRI
    if USED("EXTCG001")
      * --- Crea il Temporaneo di Appoggio
      CREATE CURSOR TmpAgg1 (TIPCON C(1), CODICE C(15), CODBUN C(3), TOTIMP N(18,4),INICOM D(8), ;
      FINCOM D(8), SERIAL C(10), DESCRI C(50),CODCEN C(15),VOCCEN C(15),CODCOM C(15),IMPORTO N(18,4),PNSERIAL C(10),TIPO C(1),CODCON C(15))
      SELECT EXTCG001
      GO TOP
      SCAN FOR NVL(TIPCON," ") $ "G" AND (NVL(IMPDAR,0)-NVL(IMPAVE,0))<>0 AND NOT EMPTY(NVL(CODICE," "))
      this.w_TIPCON = TIPCON
      this.w_CODICE = CODICE
      this.w_TOTIMP = NVL(IMPDAR,0) - NVL(IMPAVE,0)
      this.w_TIPO = IIF(this.w_RAGGSCRI="S",SPACE(1),NVL(TIPO,SPACE(1)))
      this.w_CODTESTA = IIF(this.w_RAGGSCRI="S",SPACE(15),NVL(CODCON,SPACE(15)))
      this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
      this.w_CODBUN = NVL(CODBUN, g_CODBUN)
      this.w_INICOM = CP_TODATE(INICOM)
      this.w_FINCOM = CP_TODATE(FINCOM)
      this.w_DATREG = CP_TODATE(DATREG)
      this.w_RATEO = 0
      this.w_RISCON = 0
      this.w_SERIAL = IIF(this.w_RAGGSCRI="S","",NVL(SERIAL, ""))
      this.w_DESCRI = IIF(this.w_RAGGSCRI="S","",NVL(DESCRI,""))
      this.w_CODCEN = NVL(CODCEN,space(15))
      this.w_VOCCEN = NVL(VOCCEN,Space(15))
      this.w_CODCOM = NVL(CODCOM,Space(15))
      * --- 1^OPERAZIONE: Riporta i Valori alla Valuta di Conto (si Presume Lira o EURO)
      if this.w_VALNAZ<>g_PERVAL
        this.w_CAOVAL = GETCAM(this.w_VALNAZ,I_DATSYS)
        this.w_TOTIMP = VAL2MON(this.w_TOTIMP, this.w_CAOVAL, GETCAM(g_PERVAL,I_DATSYS), i_DATSYS, g_PERVAL, g_PERPVL)
      endif
      * --- Scrive il Temporaneo di Appoggio interno al Batch
      if this.w_TOTIMP<>0 
        INSERT INTO TmpAgg1 (TIPCON, CODICE, CODBUN, TOTIMP, INICOM, FINCOM, SERIAL, DESCRI,CODCEN,VOCCEN,CODCOM,;
        IMPORTO,PNSERIAL,TIPO,CODCON);
        VALUES (this.w_TIPCON, this.w_CODICE, this.w_CODBUN, this.w_TOTIMP, this.w_INICOM, this.w_FINCOM, this.w_SERIAL, this.w_DESCRI,;
        this.w_CODCEN,this.w_VOCCEN,this.w_CODCOM,this.w_TOTIMP,this.w_PNSERIAL,this.w_TIPO,this.w_CODTESTA)
      endif
      SELECT EXTCG001
      ENDSCAN
      * --- Devo verificare se ho selezionato Raggruppa Scritture nella maschera di
      *     selezione.
      *     Creo un cursore intermedio contenente i dati contabili.
      *     Raggruppo per seriale documento,tipo conto,codice,b.unit
      if this.w_RAGGSCRI="S"
        Select Tipcon,Codice,Codbun,; 
 Sum(Totimp) as Totimp,Space(1) as Tipo,Space(15) as Codcon from Tmpagg1 into cursor Appoggio; 
 group by Tipcon,Codice,Codbun order by Tipcon,Codice,Codbun
        * --- Raggruppo i dati analitici per avere un unico cursore
        Select Tipcon,Codice,Codbun,Inicom,Fincom,Max(Serial) as Serial,Max(Descri) as Descri,Codcen,; 
 Voccen,Codcom,Sum(Importo) as Importo; 
 From TmpAgg1 into cursor Analitica group by Tipcon,Codice,Codbun,Codcen,Voccen,Codcom,Inicom,Fincom order by Tipcon,Codice,Codbun,Codcen,Voccen,Codcom,Inicom,Fincom
        * --- Scrivo l'importo corretto nel cursore di elaborazione
        Select Analitica.Tipcon as Tipcon,Analitica.Codice as Codice,Analitica.Codbun as Codbun,Appoggio.Totimp as Totimp,; 
 Analitica.Inicom as Inicom,Analitica.Fincom as Fincom,Analitica.Serial as Serial,Analitica.Descri as Descri,Analitica.Codcen as Codcen,; 
 Analitica.Voccen as Voccen,Analitica.Codcom as Codcom,Analitica.Importo as Importo,Appoggio.Tipo,Appoggio.Codcon; 
 From Analitica Inner Join Appoggio On Appoggio.Tipcon=Analitica.Tipcon and Appoggio.Codice=Analitica.Codice and Appoggio.Codbun=Analitica.Codbun; 
 order by Analitica.Tipcon,Analitica.Codice,Analitica.Codbun into cursor TEMP 
      else
        Select Tipcon,Codice,Codbun,Serial,; 
 Sum(Totimp) as Totimp,Max(Tipo) as Tipo,Max(Codcon) as Codcon from Tmpagg1 into cursor Appoggio; 
 group by Serial,Tipcon,Codice,Codbun order by Serial,Tipcon,Codice,Codbun
        * --- Raggruppo i dati analitici per avere un unico cursore
        Select Tipcon,Codice,Codbun,Inicom,Fincom,Serial,Max(Descri) as Descri,Codcen,; 
 Voccen,Codcom,Sum(Importo) as Importo ; 
 From TmpAgg1 into cursor Analitica group by Serial,Tipcon,Codice,Codbun,Codcen,Voccen,Codcom,Inicom,Fincom; 
 order by Pnserial,Tipcon,Codice,Codbun,Codcen,Voccen,Codcom,Inicom,Fincom
        Select Analitica.Tipcon as Tipcon,Analitica.Codice as Codice,Analitica.Codbun as Codbun,Appoggio.Totimp as Totimp,; 
 Analitica.Inicom as Inicom,Analitica.Fincom as Fincom,Analitica.Serial as Serial,Analitica.Descri as Descri,Analitica.Codcen as Codcen,; 
 Analitica.Voccen as Voccen,Analitica.Codcom as Codcom,Analitica.Importo as Importo,Appoggio.Tipo,Appoggio.Codcon; 
 From Analitica Inner Join Appoggio On Appoggio.Serial=Analitica.Serial and Appoggio.Tipcon=Analitica.Tipcon and Appoggio.Codice=Analitica.Codice; 
 and Appoggio.Codbun=Analitica.Codbun; 
 order by Analitica.Serial,Analitica.Tipcon,Analitica.Codice,Analitica.Codbun into cursor TEMP 
      endif
      if used("EXTCG001")
        select EXTCG001
        use
      endif
      if USED("TmpAgg1")
        * --- Azzera temporaneo
        SELECT TmpAgg1
        USE
      endif
      if used("Appoggio")
        select Appoggio
        use
      endif
      if used("Analitica")
        select Analitica
        use
      endif
      * --- 3^OPERAZIONE: Raggruppa tutto quanto per Tipo, Codice, Business Unit
      if USED("Temp")
        SELECT Temp
        if RECCOUNT()>0
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT Temp
          GO TOP
          SCAN FOR NVL(TIPCON," ") $ "GM" AND NVL(TOTIMP,0)<>0 AND NOT EMPTY(NVL(CODICE," "))
          this.w_CODBUN = NVL(CODBUN, g_CODBUN)
          this.w_TIPCON = TIPCON
          this.w_COD001 = IIF(this.w_TIPCON="M", CODICE, SPACE(15))
          this.w_COD002 = IIF(this.w_TIPCON="G", CODICE, SPACE(15))
          this.w_COD003 = SPACE(15)
          this.w_TIPO = Tipo
          this.w_CODTESTA = Codcon
          this.w_IMPDAR = IIF(TOTIMP>0, TOTIMP, 0)
          this.w_IMPAVE = IIF(TOTIMP<0, ABS(TOTIMP), 0)
          this.w_INICOM = cp_CharToDate("  -  -    ")
          this.w_FINCOM = cp_CharToDate("  -  -    ")
          this.w_SERIAL = SERIAL
          this.w_DESCRI = NVL(DESCRI,"")
          this.w_CODCEN = NVL(CODCEN,SPACE(15))
          this.w_VOCCEN = NVL(VOCCEN,SPACE(15))
          this.w_CODCOM = NVL(CODCOM,SPACE(15))
          this.w_DATCIN = CP_TODATE(INICOM)
          this.w_DATCFI = CP_TODATE(FINCOM)
          this.w_IMPORTO = Nvl(Importo,0)
          this.w_PARAMETRO = IIF(this.w_IMPORTO<>0,(ABS(this.w_IMPORTO)/ABS(TOTIMP))*100,1)
          INSERT INTO TmpAgg (CODICE, CODBUN, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE, INICOM, FINCOM,SERIAL,DESCRI2 ,PNSERIAL,;
          CODCEN,VOCCEN,CODCOM,DATCIN,DATCFI,PARAMETRO,TOTIMP,TIPO,CODCON);
          VALUES (this.w_RECODICE, this.w_CODBUN, this.w_REDESCRI, this.w_TIPCON, this.w_COD001, this.w_COD002, ;
          this.w_COD003, this.w_IMPDAR, this.w_IMPAVE, this.w_INICOM, this.w_FINCOM,this.w_SERIAL,this.w_DESCRI,this.w_SERIAL,;
          this.w_CODCEN,this.w_VOCCEN,this.w_CODCOM,this.w_DATCIN,this.w_DATCFI,this.w_PARAMETRO,this.w_IMPORTO,this.w_TIPO,this.w_CODTESTA)
          SELECT Temp
          ENDSCAN
        endif
        SELECT Temp
        USE
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
