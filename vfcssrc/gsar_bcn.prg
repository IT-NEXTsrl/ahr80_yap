* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcn                                                        *
*              Controlli associazione attributi                                *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-16                                                      *
* Last revis.: 2013-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcn",oParentObject,m.pParam)
return(i_retval)

define class tgsar_bcn as StdBatch
  * --- Local variables
  pParam = space(1)
  w_TIPO = space(1)
  w_GUID = space(14)
  w_TROVATO = .f.
  w_GRUATTR = space(10)
  w_CODATTRI = space(10)
  w_CODFAM = space(10)
  w_VALATTR = space(20)
  w_PADRE = .NULL.
  w_MESS = space(100)
  w_data = ctod("  /  /  ")
  w_OK = .f.
  w_MESS_OK = space(100)
  w_NRIGA = 0
  w_RIGA = 0
  * --- WorkFile variables
  MODDATTR_idx=0
  MODMATTR_idx=0
  GRUDATTR_idx=0
  IMP_DETT_idx=0
  COM_ATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Controlli sull'obbligatoriet� dei valori
    this.w_OK = .T.
    this.w_MESS_OK = "Transazione abbandonata"
    this.w_PADRE = this.OparentObject
    this.w_data = i_datsys
    * --- --Determinazione degli attributi obbligatori
    if this.pParam="I" and UPPER(this.w_PADRE.CLASS)="TGSAG_MIM"
      this.w_GUID = this.w_PADRE.w_GESTGUID
      * --- --Si determinano gli attributi Multipli associati alla gestione
      * --- -- si controlla se gli attributi possono essere multipli
      * --- Verifico nel caso in cui non � stata specificata nessuna riga nel dettaglio impianti
      *     se esiste almeno un attributo obbligatorio nel modello indicato
      * --- Select from GSARCBCN
      do vq_exec with 'GSARCBCN',this,'_Curs_GSARCBCN','',.f.,.t.
      if used('_Curs_GSARCBCN')
        select _Curs_GSARCBCN
        locate for 1=1
        do while not(eof())
        this.w_OK = .F.
        this.w_RIGA = _Curs_GSARCBCN.CPROWORD
        this.w_MESS = ah_Msgformat("Impossibile confermare%0La riga impianto %1 prevede attributi obbligatori",this.w_RIGA)
        exit
          select _Curs_GSARCBCN
          continue
        enddo
        use
      endif
      if this.w_OK
        * --- Select from MULTIPLO
        do vq_exec with 'MULTIPLO',this,'_Curs_MULTIPLO','',.f.,.t.
        if used('_Curs_MULTIPLO')
          select _Curs_MULTIPLO
          locate for 1=1
          do while not(eof())
          this.w_NRIGA = _Curs_MULTIPLO.CPROWORD
          this.w_MESS = ah_Msgformat("Impossibile confermare%0L'attributo definito come singolo � stato associato pi� volte per la riga %1",this.w_NRIGA)
          this.w_OK = .F.
          exit
            select _Curs_MULTIPLO
            continue
          enddo
          use
        endif
      endif
      * --- Verifico corrispondenza numero attributi obbligatori indicati nel dettaglio impianti 
      *     con quelli previsti nel modello indicato.
      if this.w_OK
        * --- Select from OBBLIGATORIO
        do vq_exec with 'OBBLIGATORIO',this,'_Curs_OBBLIGATORIO','',.f.,.t.
        if used('_Curs_OBBLIGATORIO')
          select _Curs_OBBLIGATORIO
          locate for 1=1
          do while not(eof())
          this.w_OK = .F.
          this.w_RIGA = _Curs_OBBLIGATORIO.CPROWORD
          this.w_MESS = ah_Msgformat("Impossibile confermare%0Attributi definiti come obbligatori non associati per la riga %1",this.w_RIGA)
          exit
            select _Curs_OBBLIGATORIO
            continue
          enddo
          use
        endif
      endif
      * --- Controlli sulla presenza del valore attributo
      if this.w_OK
        * --- Select from controllovalori
        do vq_exec with 'controllovalori',this,'_Curs_controllovalori','',.f.,.t.
        if used('_Curs_controllovalori')
          select _Curs_controllovalori
          locate for 1=1
          do while not(eof())
          this.w_OK = .F.
          this.w_MESS = ah_Msgformat("Occorre inserire il gruppo attributi e/o il valore da associare all'attributo")
          exit
            select _Curs_controllovalori
            continue
          enddo
          use
        endif
      endif
      * --- Controllo di univocit� gestguid
      if this.w_OK
        * --- Select from gsag_chk
        do vq_exec with 'gsag_chk',this,'_Curs_gsag_chk','',.f.,.t.
        if used('_Curs_gsag_chk')
          select _Curs_gsag_chk
          locate for 1=1
          do while not(eof())
          this.w_OK = .F.
          this.w_MESS = ah_Msgformat("Chiave gi� utilizzata")
          exit
            select _Curs_gsag_chk
            continue
          enddo
          use
        endif
      endif
      if Not this.w_OK
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
      endif
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='MODDATTR'
    this.cWorkTables[2]='MODMATTR'
    this.cWorkTables[3]='GRUDATTR'
    this.cWorkTables[4]='IMP_DETT'
    this.cWorkTables[5]='COM_ATTI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSARCBCN')
      use in _Curs_GSARCBCN
    endif
    if used('_Curs_MULTIPLO')
      use in _Curs_MULTIPLO
    endif
    if used('_Curs_OBBLIGATORIO')
      use in _Curs_OBBLIGATORIO
    endif
    if used('_Curs_controllovalori')
      use in _Curs_controllovalori
    endif
    if used('_Curs_gsag_chk')
      use in _Curs_gsag_chk
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
