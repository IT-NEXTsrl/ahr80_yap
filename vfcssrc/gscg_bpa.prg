* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpa                                                        *
*              Situazione partite                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_31]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-26                                                      *
* Last revis.: 2000-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpa",oParentObject)
return(i_retval)

define class tgscg_bpa as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  w_ODATA = ctod("  /  /  ")
  w_OSERRIF = space(10)
  w_OROWRIF = 0
  w_ONUMRIF = 0
  w_DAR = 0
  w_AVE = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LANCIA AL NOTIFYEVENT (da GSCG_KPA)
    this.oParentObject.w_DATSCA = IIF(this.oParentObject.w_FLSALD="S", this.oParentObject.w_PTDATSCA, cp_CharToDate("  -  -  "))
    This.oParentObject.NotifyEvent("Calcola")
    * --- Cambio gli importi non in moneta di Conto nel cursore a VIDEO
    this.w_ZOOM = this.oParentObject.w_ZoomScad
    this.oParentObject.w_TOTDAR = 0
    this.oParentObject.w_TOTAVE = 0
    SELECT ( this.w_ZOOM.cCursor )
    SUM TOTDAR, TOTAVE TO this.oParentObject.w_TOTDAR,this.oParentObject.w_TOTAVE
    this.oParentObject.w_SALDO = this.oParentObject.w_TOTDAR-this.oParentObject.w_TOTAVE
    if this.oParentObject.w_FLSALD="D"
      SELECT PTSERRIF AS SERRIF, PTORDRIF AS ORDRIF,PTNUMRIF AS NUMRIF ,DATSCA, SUM(TOTDAR) AS TOTDAR, SUM(TOTAVE) AS TOTAVE, SUM(TOTABB) ;
      FROM ( this.w_ZOOM.cCursor ) INTO CURSOR APPOPAR ORDER BY 1,2,3,4 GROUP BY 1,2,3,4
      SELECT ( this.w_ZOOM.cCursor )
      GO TOP
      this.w_ODATA = i_INIDAT
      this.w_OSERRIF = SPACE(10)
      this.w_OROWRIF = 0
      this.w_ONUMRIF = 0
      SCAN
      if this.w_OSERRIF<> NVL(PTSERRIF,SPACE(10)) OR this.w_OROWRIF <> NVL(PTORDRIF,0) OR this.w_ONUMRIF <> NVL(PTNUMRIF,0)
        this.w_DAR = NVL(TOTDAR,0)
        this.w_AVE = NVL(TOTAVE,0)
        this.w_ODATA = CP_TODATE(DATSCA)
        this.w_OSERRIF = NVL(PTSERRIF,SPACE(10))
        this.w_OROWRIF = NVL(PTORDRIF,0)
        this.w_ONUMRIF = NVL(PTNUMRIF,0)
        SELECT APPOPAR
        GO TOP
        LOCATE FOR CP_TODATE(DATSCA)=this.w_ODATA AND (this.w_OSERRIF= NVL(SERRIF,SPACE(10)) AND this.w_OROWRIF = NVL(ORDRIF,0) AND this.w_ONUMRIF = NVL(NUMRIF,0) )
        if FOUND()
          this.w_DAR = NVL(TOTDAR,0)
          this.w_AVE = NVL(TOTAVE,0)
        endif
        SELECT ( this.w_ZOOM.cCursor )
        REPLACE TOTDAR with this.w_DAR
        REPLACE TOTAVE with this.w_AVE
      else
        DELETE
      endif
      ENDSCAN
      SELECT APPOPAR
      USE
    endif
    SELECT ( this.w_ZOOM.cCursor )
    GO TOP
    This.oParentObject.mCalc(.t.)
    this.w_ZOOM.refresh()
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
