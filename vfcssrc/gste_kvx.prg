* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kvx                                                        *
*              Calcola rate documenti cash flow                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-28                                                      *
* Last revis.: 2014-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kvx",oParentObject))

* --- Class definition
define class tgste_kvx as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 374
  Height = 183
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-06-25"
  HelpContextID=48903273
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  FILDTREG_IDX = 0
  cPrg = "gste_kvx"
  cComment = "Calcola rate documenti cash flow"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(10)
  w_AZFLSEVA = space(1)
  w_CODAZI1 = space(10)
  w_FLDATCFL = ctod('  /  /  ')
  w_INIZIO = ctod('  /  /  ')
  o_INIZIO = ctod('  /  /  ')
  w_FINE = ctod('  /  /  ')
  w_FLSEVA = space(1)
  w_CFDATCRE = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kvxPag1","gste_kvx",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oINIZIO_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='FILDTREG'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSTE_BVF(this,"X")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(10)
      .w_AZFLSEVA=space(1)
      .w_CODAZI1=space(10)
      .w_FLDATCFL=ctod("  /  /  ")
      .w_INIZIO=ctod("  /  /  ")
      .w_FINE=ctod("  /  /  ")
      .w_FLSEVA=space(1)
      .w_CFDATCRE=ctod("  /  /  ")
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODAZI1 = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI1))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_INIZIO = iif(empty(.w_FLDATCFL), i_INIDAT, cp_todate(.w_FLDATCFL)+1)
        .w_FINE = i_FINDAT
        .w_FLSEVA = .w_AZFLSEVA
        .w_CFDATCRE = cp_todate(DataStoricoCF())
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLSEVA";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZFLSEVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(10))
      this.w_AZFLSEVA = NVL(_Link_.AZFLSEVA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_AZFLSEVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FILDTREG_IDX,3]
    i_lTable = "FILDTREG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FILDTREG_IDX,2], .t., this.FILDTREG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FILDTREG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLCODAZI,FLDATCFL";
                   +" from "+i_cTable+" "+i_lTable+" where FLCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLCODAZI',this.w_CODAZI1)
            select FLCODAZI,FLDATCFL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.FLCODAZI,space(10))
      this.w_FLDATCFL = NVL(cp_ToDate(_Link_.FLDATCFL),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(10)
      endif
      this.w_FLDATCFL = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FILDTREG_IDX,2])+'\'+cp_ToStr(_Link_.FLCODAZI,1)
      cp_ShowWarn(i_cKey,this.FILDTREG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oINIZIO_1_5.value==this.w_INIZIO)
      this.oPgFrm.Page1.oPag.oINIZIO_1_5.value=this.w_INIZIO
    endif
    if not(this.oPgFrm.Page1.oPag.oFINE_1_6.value==this.w_FINE)
      this.oPgFrm.Page1.oPag.oFINE_1_6.value=this.w_FINE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSEVA_1_8.RadioValue()==this.w_FLSEVA)
      this.oPgFrm.Page1.oPag.oFLSEVA_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDATCRE_1_10.value==this.w_CFDATCRE)
      this.oPgFrm.Page1.oPag.oCFDATCRE_1_10.value=this.w_CFDATCRE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_INIZIO<=.w_FINE)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINIZIO_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore di data finale")
          case   not(.w_INIZIO<=.w_FINE)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFINE_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore della data finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_INIZIO = this.w_INIZIO
    return

enddefine

* --- Define pages as container
define class tgste_kvxPag1 as StdContainer
  Width  = 370
  height = 183
  stdWidth  = 370
  stdheight = 183
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINIZIO_1_5 as StdField with uid="PMTTMHPITH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_INIZIO", cQueryName = "INIZIO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore di data finale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 17084038,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=103, Top=11

  func oINIZIO_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_INIZIO<=.w_FINE)
    endwith
    return bRes
  endfunc

  add object oFINE_1_6 as StdField with uid="OTCGWMGFYS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FINE", cQueryName = "FINE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore della data finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 44041898,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=282, Top=11

  func oFINE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_INIZIO<=.w_FINE)
    endwith
    return bRes
  endfunc

  add object oFLSEVA_1_8 as StdCheck with uid="UWFTWYUGVD",rtseq=7,rtrep=.f.,left=104, top=52, caption="Evasione per data scadenza",;
    ToolTipText = "Se attivo verranno evase le rate in ordine di scadenza invece che propozionalmente",;
    HelpContextID = 62934102,;
    cFormVar="w_FLSEVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSEVA_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLSEVA_1_8.GetRadio()
    this.Parent.oContained.w_FLSEVA = this.RadioValue()
    return .t.
  endfunc

  func oFLSEVA_1_8.SetRadio()
    this.Parent.oContained.w_FLSEVA=trim(this.Parent.oContained.w_FLSEVA)
    this.value = ;
      iif(this.Parent.oContained.w_FLSEVA=='S',1,;
      0)
  endfunc

  add object oCFDATCRE_1_10 as StdField with uid="XBGXKLRWXX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CFDATCRE", cQueryName = "CFDATCRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di aggiornamento storico",;
    HelpContextID = 94066283,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=282, Top=91


  add object oBtn_1_12 as StdButton with uid="FMNRJPCTLS",left=257, top=130, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare le rate dei documenti";
    , HelpContextID = 42042646;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        gste_bvf(this.Parent.oContained,"RateDoc")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="LZNQFGWXIN",left=309, top=130, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 42042646;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="SMODERLOEY",Visible=.t., Left=42, Top=11,;
    Alignment=1, Width=55, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="WSESJLGWYK",Visible=.t., Left=239, Top=11,;
    Alignment=1, Width=38, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="TRIBKYHAXY",Visible=.t., Left=38, Top=93,;
    Alignment=1, Width=239, Height=18,;
    Caption="Ultima data elaborazione storico:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kvx','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gste_kvx
func DataStoricoCF()
  local w_CFDATCRE
  vq_exec('query\gste_kvx',,'CursDataStorico')
  if used('CursDataStorico')
    select CursDataStorico
    w_CFDATCRE=CFDATCRE
    select CursDataStorico
    use
  endif
  w_CFDATCRE=nvl(w_CFDATCRE,cp_chartodate('  -  -    '))
return(w_CFDATCRE)
* --- Fine Area Manuale
