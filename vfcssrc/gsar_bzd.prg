* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bzd                                                        *
*              Documenti da schede                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-21                                                      *
* Last revis.: 2010-12-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bzd",oParentObject)
return(i_retval)

define class tgsar_bzd as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_DOC = .NULL.
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia I DOCUMENTI DI VENDITA / ACQUISTI dalla visualizzazione delle schede contabili
    * --- ALFA NUMERO E DATA DEL DOCUMENTO
    * --- CERCO IL DOCUMENTO
    this.w_FLVEAC = "V"
    this.w_CLADOC = "  "
    * --- Select from DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MVSERIAL,MVFLVEAC,MVCLADOC  from "+i_cTable+" DOC_MAST ";
          +" where MVALFDOC= "+cp_ToStrODBC(this.oParentObject.w_ALFA)+" and MVNUMDOC= "+cp_ToStrODBC(this.oParentObject.w_NUMERO)+" and MVDATDOC="+cp_ToStrODBC(this.oParentObject.w_DATA)+" and MVRIFCON = "+cp_ToStrODBC(this.oParentObject.w_SERIALP)+"";
           ,"_Curs_DOC_MAST")
    else
      select MVSERIAL,MVFLVEAC,MVCLADOC from (i_cTable);
       where MVALFDOC= this.oParentObject.w_ALFA and MVNUMDOC= this.oParentObject.w_NUMERO and MVDATDOC=this.oParentObject.w_DATA and MVRIFCON = this.oParentObject.w_SERIALP;
        into cursor _Curs_DOC_MAST
    endif
    if used('_Curs_DOC_MAST')
      select _Curs_DOC_MAST
      locate for 1=1
      do while not(eof())
      this.w_SERIAL = _Curs_DOC_MAST.MVSERIAL
      this.w_FLVEAC = _Curs_DOC_MAST.MVFLVEAC
      this.w_CLADOC = _Curs_DOC_MAST.MVCLADOC
        select _Curs_DOC_MAST
        continue
      enddo
      use
    endif
    * --- Il documento potrebbe non esistere
    if EMPTY(NVL(this.w_SERIAL," "))
      ah_ErrorMsg("Documento non esistente",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Questo oggetto sar� definito come documento
    * --- definisco l'oggetto come appartenente alla classe dei documenti
    if this.w_CLADOC="OR"
      this.w_DOC = GSOR_MDV(this.w_FLVEAC)
    else
      if this.w_FLVEAC="V"
        this.w_DOC = GSVE_MDV(this.w_CLADOC)
      else
        this.w_DOC = GSAC_MDV(this.w_CLADOC)
      endif
    endif
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_DOC.bSec1)
      Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
      i_retcode = 'stop'
      return
    endif
    * --- creo il cursore delle solo chiavi
    this.w_DOC.ecpFilter()     
    this.w_DOC.BlankRec()     
    this.w_DOC.w_MVSERIAL = this.w_SERIAL
    * --- carico il record
    this.w_DOC.ecpSave()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
