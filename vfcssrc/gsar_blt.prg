* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_blt                                                        *
*              Lancia causali documenti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_10]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2011-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_blt",oParentObject)
return(i_retval)

define class tgsar_blt as StdBatch
  * --- Local variables
  w_PROG = .NULL.
  w_TIPDOC = space(5)
  w_TDFLVEAC = space(1)
  w_TDCATDOC = space(2)
  * --- WorkFile variables
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Le Causali Documenti dai Documenti di Ciclo passivo (da GSAC_MDV)
    * --- Gestione tasto destro
    if Type("g_oMenu.oKey")<>"U"
      this.w_TIPDOC = Nvl(g_oMenu.oKey(1,3),"")
    else
      if i_curform=.NULL.
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Tipo di Conto
      this.w_TIPDOC = &cCurs..TDTIPDOC
    endif
    if NOT EMPTY( this.w_TIPDOC )
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDFLVEAC,TDCATDOC"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDFLVEAC,TDCATDOC;
          from (i_cTable) where;
              TDTIPDOC = this.w_TIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TDFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.w_TDCATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      i_retcode = 'stop'
      return
    endif
    if ! Isahe()
      this.w_PROG = ICASE( this.w_TDCATDOC="OR", "GSOR_ATD", this.w_TDFLVEAC="A", "GSAC_ATD", "GSVE_ATD" )
      if g_ACQU<>"S" and this.w_PROG="GSAC_ATD" OR g_ORDI<>"S" and this.w_PROG="GSOR_ATD"
        this.w_PROG = "GSVE_ATD"
      endif
    else
      this.w_PROG = "GSVE_ATD"
    endif
    OpenGest(iif(Type("g_oMenu.oKey")<>"U",g_oMenu.cBatchType,"S"),this.w_PROG,"TDTIPDOC",this.w_TIPDOC)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TIP_DOCU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
