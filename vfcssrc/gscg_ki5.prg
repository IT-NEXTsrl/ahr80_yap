* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_ki5                                                        *
*              Dettaglio partite/scadenze                                      *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_74]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-01                                                      *
* Last revis.: 2008-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_ki5",oParentObject))

* --- Class definition
define class tgscg_ki5 as StdForm
  Top    = 10
  Left   = 89

  * --- Standard Properties
  Width  = 477
  Height = 158
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-07"
  HelpContextID=267003241
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  PAR_TITE_IDX = 0
  BAN_CHE_IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  MOD_PAGA_IDX = 0
  cPrg = "gscg_ki5"
  cComment = "Dettaglio partite/scadenze"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATSCA1 = ctod('  /  /  ')
  o_DATSCA1 = ctod('  /  /  ')
  w_DATSCA = ctod('  /  /  ')
  w_NUMPAR = space(31)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_MODPAG = space(10)
  w_TOTIMP = 0
  w_SIMVAL = space(3)
  w_BANAPP = space(10)
  w_BANNOS = space(15)
  w_PTSERIAL = space(10)
  o_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_OK = .F.
  w_TEST1 = .F.
  w_FLSOSP = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_ki5Pag1","gscg_ki5",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATSCA_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='BAN_CHE'
    this.cWorkTables[3]='COC_MAST'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='MOD_PAGA'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATSCA1=ctod("  /  /  ")
      .w_DATSCA=ctod("  /  /  ")
      .w_NUMPAR=space(31)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_MODPAG=space(10)
      .w_TOTIMP=0
      .w_SIMVAL=space(3)
      .w_BANAPP=space(10)
      .w_BANNOS=space(15)
      .w_PTSERIAL=space(10)
      .w_PTROWORD=0
      .w_CPROWNUM=0
      .w_OK=.f.
      .w_TEST1=.f.
      .w_FLSOSP=space(1)
      .w_DATSCA1=oParentObject.w_DATSCA1
      .w_DATSCA=oParentObject.w_DATSCA
      .w_NUMPAR=oParentObject.w_NUMPAR
      .w_TIPCON=oParentObject.w_TIPCON
      .w_CODCON=oParentObject.w_CODCON
      .w_MODPAG=oParentObject.w_MODPAG
      .w_TOTIMP=oParentObject.w_TOTIMP
      .w_SIMVAL=oParentObject.w_SIMVAL
      .w_BANAPP=oParentObject.w_BANAPP
      .w_BANNOS=oParentObject.w_BANNOS
      .w_OK=oParentObject.w_OK
      .w_TEST1=oParentObject.w_TEST1
      .w_FLSOSP=oParentObject.w_FLSOSP
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_DATSCA1))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_NUMPAR))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_CODCON))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MODPAG))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_TOTIMP))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SIMVAL))
          .link_1_13('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_BANAPP))
          .link_1_14('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_BANNOS))
          .link_1_15('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_PTSERIAL))
          .link_1_20('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_PTROWORD))
          .link_1_21('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CPROWNUM))
          .link_1_22('Full')
        endif
        .w_OK = .T.
    endwith
    this.DoRTCalc(15,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATSCA1=.w_DATSCA1
      .oParentObject.w_DATSCA=.w_DATSCA
      .oParentObject.w_NUMPAR=.w_NUMPAR
      .oParentObject.w_TIPCON=.w_TIPCON
      .oParentObject.w_CODCON=.w_CODCON
      .oParentObject.w_MODPAG=.w_MODPAG
      .oParentObject.w_TOTIMP=.w_TOTIMP
      .oParentObject.w_SIMVAL=.w_SIMVAL
      .oParentObject.w_BANAPP=.w_BANAPP
      .oParentObject.w_BANNOS=.w_BANNOS
      .oParentObject.w_OK=.w_OK
      .oParentObject.w_TEST1=.w_TEST1
      .oParentObject.w_FLSOSP=.w_FLSOSP
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        if .o_DATSCA1<>.w_DATSCA1
            .w_DATSCA = .w_DATSCA1
        endif
          .link_1_3('Full')
        .DoRTCalc(4,4,.t.)
          .link_1_5('Full')
        .DoRTCalc(6,6,.t.)
          .link_1_7('Full')
          .link_1_13('Full')
        if .o_PTSERIAL<>.w_PTSERIAL
          .link_1_14('Full')
        endif
        if .o_PTSERIAL<>.w_PTSERIAL
          .link_1_15('Full')
        endif
          .link_1_20('Full')
          .link_1_21('Full')
          .link_1_22('Full')
            .w_OK = .T.
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATSCA_1_2.enabled = this.oPgFrm.Page1.oPag.oDATSCA_1_2.mCond()
    this.oPgFrm.Page1.oPag.oMODPAG_1_6.enabled = this.oPgFrm.Page1.oPag.oMODPAG_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBANAPP_1_14.enabled = this.oPgFrm.Page1.oPag.oBANAPP_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBANNOS_1_15.enabled = this.oPgFrm.Page1.oPag.oBANNOS_1_15.mCond()
    this.oPgFrm.Page1.oPag.oFLSOSP_1_25.enabled = this.oPgFrm.Page1.oPag.oFLSOSP_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DATSCA1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DATSCA1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DATSCA1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTDATSCA";
                   +" from "+i_cTable+" "+i_lTable+" where PTDATSCA="+cp_ToStrODBC(this.w_DATSCA1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTDATSCA',this.w_DATSCA1)
            select PTDATSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DATSCA1 = NVL(cp_todate(_Link_.PTDATSCA),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DATSCA1 = ctod("  /  /  ")
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTDATSCA,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DATSCA1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMPAR
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTNUMPAR";
                   +" from "+i_cTable+" "+i_lTable+" where PTNUMPAR="+cp_ToStrODBC(this.w_NUMPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTNUMPAR',this.w_NUMPAR)
            select PTNUMPAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMPAR = NVL(_Link_.PTNUMPAR,space(31))
    else
      if i_cCtrl<>'Load'
        this.w_NUMPAR = space(31)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTNUMPAR,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where PTCODCON="+cp_ToStrODBC(this.w_CODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTCODCON',this.w_CODCON)
            select PTCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.PTCODCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTCODCON,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MODPAG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_lTable = "MOD_PAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2], .t., this.MOD_PAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMP',True,'MOD_PAGA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_MODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_MODPAG))
          select MPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODPAG)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODPAG) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAGA','*','MPCODICE',cp_AbsName(oSource.parent,'oMODPAG_1_6'),i_cWhere,'GSAR_AMP',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_MODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_MODPAG)
            select MPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODPAG = NVL(_Link_.MPCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MODPAG = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_PAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TOTIMP
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TOTIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TOTIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTTOTIMP";
                   +" from "+i_cTable+" "+i_lTable+" where PTTOTIMP="+cp_ToStrODBC(this.w_TOTIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTTOTIMP',this.w_TOTIMP)
            select PTTOTIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TOTIMP = NVL(_Link_.PTTOTIMP,0)
    else
      if i_cCtrl<>'Load'
        this.w_TOTIMP = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTTOTIMP,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TOTIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SIMVAL
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SIMVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SIMVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where PTCODVAL="+cp_ToStrODBC(this.w_SIMVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTCODVAL',this.w_SIMVAL)
            select PTCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SIMVAL = NVL(_Link_.PTCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_SIMVAL = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTCODVAL,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SIMVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANAPP
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANAPP))
          select BACODBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oBANAPP_1_14'),i_cWhere,'GSAR_ABA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANAPP)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANAPP = NVL(_Link_.BACODBAN,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_BANAPP = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANNOS
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANNOS))
          select BACODBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oBANNOS_1_15'),i_cWhere,'GSTE_ACB',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANNOS)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANNOS = NVL(_Link_.BACODBAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_BANNOS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTSERIAL
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL);
                   +" and PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTSERIAL',this.w_PTSERIAL;
                       ,'PTSERIAL',this.w_PTSERIAL)
            select PTSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTSERIAL = NVL(_Link_.PTSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PTSERIAL = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTSERIAL,1)+'\'+cp_ToStr(_Link_.PTSERIAL,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTROWORD
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTROWORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTROWORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTSERIAL,PTROWORD";
                   +" from "+i_cTable+" "+i_lTable+" where PTROWORD="+cp_ToStrODBC(this.w_PTROWORD);
                   +" and PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTSERIAL',this.w_PTSERIAL;
                       ,'PTROWORD',this.w_PTROWORD)
            select PTSERIAL,PTROWORD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTROWORD = NVL(_Link_.PTROWORD,0)
    else
      if i_cCtrl<>'Load'
        this.w_PTROWORD = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTSERIAL,1)+'\'+cp_ToStr(_Link_.PTROWORD,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTROWORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CPROWNUM
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CPROWNUM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CPROWNUM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTSERIAL,CPROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM);
                   +" and PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTSERIAL',this.w_PTSERIAL;
                       ,'CPROWNUM',this.w_CPROWNUM)
            select PTSERIAL,CPROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CPROWNUM = NVL(_Link_.CPROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_CPROWNUM = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTSERIAL,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CPROWNUM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATSCA_1_2.value==this.w_DATSCA)
      this.oPgFrm.Page1.oPag.oDATSCA_1_2.value=this.w_DATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPAR_1_3.value==this.w_NUMPAR)
      this.oPgFrm.Page1.oPag.oNUMPAR_1_3.value=this.w_NUMPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_4.value==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_4.value=this.w_TIPCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_5.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_5.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oMODPAG_1_6.value==this.w_MODPAG)
      this.oPgFrm.Page1.oPag.oMODPAG_1_6.value=this.w_MODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMP_1_7.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oTOTIMP_1_7.value=this.w_TOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oBANAPP_1_14.value==this.w_BANAPP)
      this.oPgFrm.Page1.oPag.oBANAPP_1_14.value=this.w_BANAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oBANNOS_1_15.value==this.w_BANNOS)
      this.oPgFrm.Page1.oPag.oBANNOS_1_15.value=this.w_BANNOS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSOSP_1_25.RadioValue()==this.w_FLSOSP)
      this.oPgFrm.Page1.oPag.oFLSOSP_1_25.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATSCA))  and (.w_TEST1=.T.)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSCA_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DATSCA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MODPAG))  and (.w_TEST1=.T.)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMODPAG_1_6.SetFocus()
            i_bnoObbl = !empty(.w_MODPAG)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATSCA1 = this.w_DATSCA1
    this.o_PTSERIAL = this.w_PTSERIAL
    return

enddefine

* --- Define pages as container
define class tgscg_ki5Pag1 as StdContainer
  Width  = 473
  height = 158
  stdWidth  = 473
  stdheight = 158
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATSCA_1_2 as StdField with uid="SYTAXLRLVI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATSCA", cQueryName = "DATSCA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 94265398,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=122, Top=13

  func oDATSCA_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TEST1=.T.)
    endwith
   endif
  endfunc

  add object oNUMPAR_1_3 as StdField with uid="LBSWEOUUNH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NUMPAR", cQueryName = "NUMPAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(31), bMultilanguage =  .f.,;
    HelpContextID = 108725462,;
   bGlobalFont=.t.,;
    Height=21, Width=215, Left=247, Top=13, InputMask=replicate('X',31), cLinkFile="PAR_TITE", cZoomOnZoom="GSCG_MPA", oKey_1_1="PTNUMPAR", oKey_1_2="this.w_NUMPAR"

  func oNUMPAR_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oTIPCON_1_4 as StdField with uid="JZWEZIBBJX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TIPCON", cQueryName = "TIPCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 55454006,;
   bGlobalFont=.t.,;
    Height=21, Width=22, Left=229, Top=41, InputMask=replicate('X',1)

  add object oCODCON_1_5 as StdField with uid="YZGAGIEZFI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 55406118,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=122, Top=41, InputMask=replicate('X',15), cLinkFile="PAR_TITE", cZoomOnZoom="GSCG_MPA", oKey_1_1="PTCODCON", oKey_1_2="this.w_CODCON"

  func oCODCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMODPAG_1_6 as StdField with uid="ZJHZLKWHZB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MODPAG", cQueryName = "MODPAG",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 192573126,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=122, Top=69, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAGA", cZoomOnZoom="GSAR_AMP", oKey_1_1="MPCODICE", oKey_1_2="this.w_MODPAG"

  func oMODPAG_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TEST1=.T.)
    endwith
   endif
  endfunc

  func oMODPAG_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODPAG_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODPAG_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAGA','*','MPCODICE',cp_AbsName(this.parent,'oMODPAG_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMP',"",'',this.parent.oContained
  endproc
  proc oMODPAG_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MPCODICE=this.parent.oContained.w_MODPAG
     i_obj.ecpSave()
  endproc

  add object oTOTIMP_1_7 as StdField with uid="NGVQQJZXSW",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TOTIMP", cQueryName = "TOTIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 87322422,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=323, Top=41, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)", cLinkFile="PAR_TITE", cZoomOnZoom="GSCG_MPA", oKey_1_1="PTTOTIMP", oKey_1_2="this.w_TOTIMP"

  func oTOTIMP_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oBANAPP_1_14 as StdField with uid="RYKZZIWJBE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_BANAPP", cQueryName = "BANAPP",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 89915414,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=122, Top=125, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANAPP"

  func oBANAPP_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TEST1=.T.)
    endwith
   endif
  endfunc

  func oBANAPP_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANAPP_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANAPP_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oBANAPP_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"",'',this.parent.oContained
  endproc
  proc oBANAPP_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANAPP
     i_obj.ecpSave()
  endproc

  add object oBANNOS_1_15 as StdField with uid="ZRAUZYGMGC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_BANNOS", cQueryName = "BANNOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 140050454,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=122, Top=97, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANNOS"

  func oBANNOS_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TEST1=.T.)
    endwith
   endif
  endfunc

  func oBANNOS_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANNOS_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANNOS_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oBANNOS_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"",'',this.parent.oContained
  endproc
  proc oBANNOS_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANNOS
     i_obj.ecpSave()
  endproc


  add object oBtn_1_18 as StdButton with uid="CGQUUYLBKU",left=366, top=106, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 266974490;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="BWIZTEBHWH",left=417, top=106, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259685818;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLSOSP_1_25 as StdCheck with uid="VIGLEQNJJI",rtseq=16,rtrep=.f.,left=324, top=69, caption="Sospesa",;
    HelpContextID = 94002006,;
    cFormVar="w_FLSOSP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSOSP_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLSOSP_1_25.GetRadio()
    this.Parent.oContained.w_FLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oFLSOSP_1_25.SetRadio()
    this.Parent.oContained.w_FLSOSP=trim(this.Parent.oContained.w_FLSOSP)
    this.value = ;
      iif(this.Parent.oContained.w_FLSOSP=='S',1,;
      0)
  endfunc

  func oFLSOSP_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TEST1=.T.)
    endwith
   endif
  endfunc

  add object oStr_1_8 as StdString with uid="KHFXRMLUBP",Visible=.t., Left=201, Top=13,;
    Alignment=1, Width=44, Height=18,;
    Caption="Partita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="TGKRDUHAPS",Visible=.t., Left=7, Top=13,;
    Alignment=1, Width=112, Height=18,;
    Caption="Scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="NRNYURFYMD",Visible=.t., Left=7, Top=41,;
    Alignment=1, Width=112, Height=18,;
    Caption="Cli/for/conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="QZPJJTTQUX",Visible=.t., Left=7, Top=69,;
    Alignment=1, Width=112, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ZAPYXQLXAS",Visible=.t., Left=277, Top=45,;
    Alignment=1, Width=45, Height=18,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="CWUSBYLTOT",Visible=.t., Left=7, Top=125,;
    Alignment=1, Width=112, Height=18,;
    Caption="Banca d'app.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="FZTESGFRRP",Visible=.t., Left=7, Top=97,;
    Alignment=1, Width=112, Height=18,;
    Caption="Ns. banca:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_ki5','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
