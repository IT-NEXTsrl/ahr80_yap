* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_mmc                                                        *
*              Movimenti di analitica manual                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_30]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-11                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsca_mmc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsca_mmc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsca_mmc")
  return

* --- Class definition
define class tgsca_mmc as StdPCForm
  Width  = 745
  Height = 257
  Top    = 56
  Left   = 17
  cComment = "Movimenti di analitica manual"
  cPrg = "gsca_mmc"
  HelpContextID=197821801
  add object cnt as tcgsca_mmc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsca_mmc as PCContext
  w_MRSERIAL = space(10)
  w_MRROWORD = 0
  w_GIACAR = space(1)
  w_SEGNO = space(1)
  w_MRCODVOC = space(15)
  w_MRCODICE = space(15)
  w_MRCODCOM = space(15)
  w_MR_SEGNO = space(1)
  w_MRTOTIMP = 0
  w_MRINICOM = space(8)
  w_MRFINCOM = space(8)
  w_DESVOC = space(40)
  w_DESPIA = space(40)
  w_DESCAN = space(30)
  w_NUMLIV = 0
  w_MRPARAME = 0
  w_IMPRIG = 0
  w_MRFLRIPA = space(1)
  w_TOTRIG = 0
  w_TOTVIS = 0
  w_SEGTOT = space(1)
  w_VCDTOBSO = space(8)
  w_CCDTOBSO = space(8)
  w_CNDTOBSO = space(8)
  w_OBTEST = space(8)
  proc Save(i_oFrom)
    this.w_MRSERIAL = i_oFrom.w_MRSERIAL
    this.w_MRROWORD = i_oFrom.w_MRROWORD
    this.w_GIACAR = i_oFrom.w_GIACAR
    this.w_SEGNO = i_oFrom.w_SEGNO
    this.w_MRCODVOC = i_oFrom.w_MRCODVOC
    this.w_MRCODICE = i_oFrom.w_MRCODICE
    this.w_MRCODCOM = i_oFrom.w_MRCODCOM
    this.w_MR_SEGNO = i_oFrom.w_MR_SEGNO
    this.w_MRTOTIMP = i_oFrom.w_MRTOTIMP
    this.w_MRINICOM = i_oFrom.w_MRINICOM
    this.w_MRFINCOM = i_oFrom.w_MRFINCOM
    this.w_DESVOC = i_oFrom.w_DESVOC
    this.w_DESPIA = i_oFrom.w_DESPIA
    this.w_DESCAN = i_oFrom.w_DESCAN
    this.w_NUMLIV = i_oFrom.w_NUMLIV
    this.w_MRPARAME = i_oFrom.w_MRPARAME
    this.w_IMPRIG = i_oFrom.w_IMPRIG
    this.w_MRFLRIPA = i_oFrom.w_MRFLRIPA
    this.w_TOTRIG = i_oFrom.w_TOTRIG
    this.w_TOTVIS = i_oFrom.w_TOTVIS
    this.w_SEGTOT = i_oFrom.w_SEGTOT
    this.w_VCDTOBSO = i_oFrom.w_VCDTOBSO
    this.w_CCDTOBSO = i_oFrom.w_CCDTOBSO
    this.w_CNDTOBSO = i_oFrom.w_CNDTOBSO
    this.w_OBTEST = i_oFrom.w_OBTEST
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MRSERIAL = this.w_MRSERIAL
    i_oTo.w_MRROWORD = this.w_MRROWORD
    i_oTo.w_GIACAR = this.w_GIACAR
    i_oTo.w_SEGNO = this.w_SEGNO
    i_oTo.w_MRCODVOC = this.w_MRCODVOC
    i_oTo.w_MRCODICE = this.w_MRCODICE
    i_oTo.w_MRCODCOM = this.w_MRCODCOM
    i_oTo.w_MR_SEGNO = this.w_MR_SEGNO
    i_oTo.w_MRTOTIMP = this.w_MRTOTIMP
    i_oTo.w_MRINICOM = this.w_MRINICOM
    i_oTo.w_MRFINCOM = this.w_MRFINCOM
    i_oTo.w_DESVOC = this.w_DESVOC
    i_oTo.w_DESPIA = this.w_DESPIA
    i_oTo.w_DESCAN = this.w_DESCAN
    i_oTo.w_NUMLIV = this.w_NUMLIV
    i_oTo.w_MRPARAME = this.w_MRPARAME
    i_oTo.w_IMPRIG = this.w_IMPRIG
    i_oTo.w_MRFLRIPA = this.w_MRFLRIPA
    i_oTo.w_TOTRIG = this.w_TOTRIG
    i_oTo.w_TOTVIS = this.w_TOTVIS
    i_oTo.w_SEGTOT = this.w_SEGTOT
    i_oTo.w_VCDTOBSO = this.w_VCDTOBSO
    i_oTo.w_CCDTOBSO = this.w_CCDTOBSO
    i_oTo.w_CNDTOBSO = this.w_CNDTOBSO
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsca_mmc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 745
  Height = 257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=197821801
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  MOVICOST_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  CAN_TIER_IDX = 0
  cFile = "MOVICOST"
  cKeySelect = "MRSERIAL,MRROWORD"
  cKeyWhere  = "MRSERIAL=this.w_MRSERIAL and MRROWORD=this.w_MRROWORD"
  cKeyDetail  = "MRSERIAL=this.w_MRSERIAL and MRROWORD=this.w_MRROWORD"
  cKeyWhereODBC = '"MRSERIAL="+cp_ToStrODBC(this.w_MRSERIAL)';
      +'+" and MRROWORD="+cp_ToStrODBC(this.w_MRROWORD)';

  cKeyDetailWhereODBC = '"MRSERIAL="+cp_ToStrODBC(this.w_MRSERIAL)';
      +'+" and MRROWORD="+cp_ToStrODBC(this.w_MRROWORD)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"MOVICOST.MRSERIAL="+cp_ToStrODBC(this.w_MRSERIAL)';
      +'+" and MOVICOST.MRROWORD="+cp_ToStrODBC(this.w_MRROWORD)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MOVICOST.CPROWNUM '
  cPrg = "gsca_mmc"
  cComment = "Movimenti di analitica manual"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MRSERIAL = space(10)
  w_MRROWORD = 0
  w_GIACAR = space(1)
  w_SEGNO = space(1)
  w_MRCODVOC = space(15)
  w_MRCODICE = space(15)
  w_MRCODCOM = space(15)
  w_MR_SEGNO = space(1)
  w_MRTOTIMP = 0
  w_MRINICOM = ctod('  /  /  ')
  w_MRFINCOM = ctod('  /  /  ')
  w_DESVOC = space(40)
  w_DESPIA = space(40)
  w_DESCAN = space(30)
  w_NUMLIV = 0
  w_MRPARAME = 0
  w_IMPRIG = 0
  w_MRFLRIPA = space(1)
  w_TOTRIG = 0
  w_TOTVIS = 0
  w_SEGTOT = space(1)
  w_VCDTOBSO = ctod('  /  /  ')
  w_CCDTOBSO = ctod('  /  /  ')
  w_CNDTOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_mmcPag1","gsca_mmc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='VOC_COST'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='MOVICOST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOVICOST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOVICOST_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsca_mmc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from MOVICOST where MRSERIAL=KeySet.MRSERIAL
    *                            and MRROWORD=KeySet.MRROWORD
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.MOVICOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2],this.bLoadRecFilter,this.MOVICOST_IDX,"gsca_mmc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOVICOST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOVICOST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOVICOST '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MRSERIAL',this.w_MRSERIAL  ,'MRROWORD',this.w_MRROWORD  )
      select * from (i_cTable) MOVICOST where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_GIACAR = space(1)
        .w_SEGNO = 'D'
        .w_TOTRIG = 0
        .w_VCDTOBSO = ctod("  /  /  ")
        .w_CCDTOBSO = ctod("  /  /  ")
        .w_CNDTOBSO = ctod("  /  /  ")
        .w_MRSERIAL = NVL(MRSERIAL,space(10))
        .w_MRROWORD = NVL(MRROWORD,0)
        .w_TOTVIS = ABS(.w_TOTRIG)
        .w_SEGTOT = IIF(.w_TOTRIG<0, 'A', 'D')
        .w_OBTEST = this.oParentObject .w_OBTEST
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'MOVICOST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTRIG = 0
      scan
        with this
          .w_DESVOC = space(40)
          .w_DESPIA = space(40)
          .w_DESCAN = space(30)
          .w_NUMLIV = 0
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_MRCODVOC = NVL(MRCODVOC,space(15))
          if link_2_1_joined
            this.w_MRCODVOC = NVL(VCCODICE201,NVL(this.w_MRCODVOC,space(15)))
            this.w_DESVOC = NVL(VCDESCRI201,space(40))
            this.w_VCDTOBSO = NVL(cp_ToDate(VCDTOBSO201),ctod("  /  /  "))
          else
          .link_2_1('Load')
          endif
          .w_MRCODICE = NVL(MRCODICE,space(15))
          if link_2_2_joined
            this.w_MRCODICE = NVL(CC_CONTO202,NVL(this.w_MRCODICE,space(15)))
            this.w_DESPIA = NVL(CCDESPIA202,space(40))
            this.w_NUMLIV = NVL(CCNUMLIV202,0)
            this.w_CCDTOBSO = NVL(cp_ToDate(CCDTOBSO202),ctod("  /  /  "))
          else
          .link_2_2('Load')
          endif
          .w_MRCODCOM = NVL(MRCODCOM,space(15))
          if link_2_3_joined
            this.w_MRCODCOM = NVL(CNCODCAN203,NVL(this.w_MRCODCOM,space(15)))
            this.w_DESCAN = NVL(CNDESCAN203,space(30))
            this.w_CNDTOBSO = NVL(cp_ToDate(CNDTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
          .w_MR_SEGNO = NVL(MR_SEGNO,space(1))
          .w_MRTOTIMP = NVL(MRTOTIMP,0)
          .w_MRINICOM = NVL(cp_ToDate(MRINICOM),ctod("  /  /  "))
          .w_MRFINCOM = NVL(cp_ToDate(MRFINCOM),ctod("  /  /  "))
          .w_MRPARAME = NVL(MRPARAME,0)
        .w_IMPRIG = .w_MRTOTIMP*IIF(.w_MR_SEGNO='D',1,-1)
          .w_MRFLRIPA = NVL(MRFLRIPA,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTRIG = .w_TOTRIG+.w_IMPRIG
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TOTVIS = ABS(.w_TOTRIG)
        .w_SEGTOT = IIF(.w_TOTRIG<0, 'A', 'D')
        .w_OBTEST = this.oParentObject .w_OBTEST
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MRSERIAL=space(10)
      .w_MRROWORD=0
      .w_GIACAR=space(1)
      .w_SEGNO=space(1)
      .w_MRCODVOC=space(15)
      .w_MRCODICE=space(15)
      .w_MRCODCOM=space(15)
      .w_MR_SEGNO=space(1)
      .w_MRTOTIMP=0
      .w_MRINICOM=ctod("  /  /  ")
      .w_MRFINCOM=ctod("  /  /  ")
      .w_DESVOC=space(40)
      .w_DESPIA=space(40)
      .w_DESCAN=space(30)
      .w_NUMLIV=0
      .w_MRPARAME=0
      .w_IMPRIG=0
      .w_MRFLRIPA=space(1)
      .w_TOTRIG=0
      .w_TOTVIS=0
      .w_SEGTOT=space(1)
      .w_VCDTOBSO=ctod("  /  /  ")
      .w_CCDTOBSO=ctod("  /  /  ")
      .w_CNDTOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_SEGNO = 'D'
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_MRCODVOC))
         .link_2_1('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MRCODICE))
         .link_2_2('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_MRCODCOM))
         .link_2_3('Full')
        endif
        .w_MR_SEGNO = .w_SEGNO
        .DoRTCalc(9,16,.f.)
        .w_IMPRIG = .w_MRTOTIMP*IIF(.w_MR_SEGNO='D',1,-1)
        .w_MRFLRIPA = ' '
        .DoRTCalc(19,19,.f.)
        .w_TOTVIS = ABS(.w_TOTRIG)
        .w_SEGTOT = IIF(.w_TOTRIG<0, 'A', 'D')
        .DoRTCalc(22,24,.f.)
        .w_OBTEST = this.oParentObject .w_OBTEST
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOVICOST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MOVICOST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOVICOST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRSERIAL,"MRSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRROWORD,"MRROWORD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MRCODVOC C(15);
      ,t_MRCODICE C(15);
      ,t_MRCODCOM C(15);
      ,t_MR_SEGNO C(1);
      ,t_MRTOTIMP N(18,4);
      ,t_MRINICOM D(8);
      ,t_MRFINCOM D(8);
      ,t_DESVOC C(40);
      ,t_DESPIA C(40);
      ,t_DESCAN C(30);
      ,CPROWNUM N(10);
      ,t_NUMLIV N(2);
      ,t_MRPARAME N(8,4);
      ,t_IMPRIG N(18,4);
      ,t_MRFLRIPA C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsca_mmcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.controlsource=this.cTrsName+'.t_MRCODVOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODICE_2_2.controlsource=this.cTrsName+'.t_MRCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCOM_2_3.controlsource=this.cTrsName+'.t_MRCODCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMR_SEGNO_2_4.controlsource=this.cTrsName+'.t_MR_SEGNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_5.controlsource=this.cTrsName+'.t_MRTOTIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRINICOM_2_6.controlsource=this.cTrsName+'.t_MRINICOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRFINCOM_2_7.controlsource=this.cTrsName+'.t_MRFINCOM'
    this.oPgFRm.Page1.oPag.oDESVOC_2_8.controlsource=this.cTrsName+'.t_DESVOC'
    this.oPgFRm.Page1.oPag.oDESPIA_2_9.controlsource=this.cTrsName+'.t_DESPIA'
    this.oPgFRm.Page1.oPag.oDESCAN_2_10.controlsource=this.cTrsName+'.t_DESCAN'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(133)
    this.AddVLine(265)
    this.AddVLine(397)
    this.AddVLine(424)
    this.AddVLine(556)
    this.AddVLine(637)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOVICOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOVICOST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
      *
      * insert into MOVICOST
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOVICOST')
        i_extval=cp_InsertValODBCExtFlds(this,'MOVICOST')
        i_cFldBody=" "+;
                  "(MRSERIAL,MRROWORD,MRCODVOC,MRCODICE,MRCODCOM"+;
                  ",MR_SEGNO,MRTOTIMP,MRINICOM,MRFINCOM,MRPARAME"+;
                  ",MRFLRIPA,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MRSERIAL)+","+cp_ToStrODBC(this.w_MRROWORD)+","+cp_ToStrODBCNull(this.w_MRCODVOC)+","+cp_ToStrODBCNull(this.w_MRCODICE)+","+cp_ToStrODBCNull(this.w_MRCODCOM)+;
             ","+cp_ToStrODBC(this.w_MR_SEGNO)+","+cp_ToStrODBC(this.w_MRTOTIMP)+","+cp_ToStrODBC(this.w_MRINICOM)+","+cp_ToStrODBC(this.w_MRFINCOM)+","+cp_ToStrODBC(this.w_MRPARAME)+;
             ","+cp_ToStrODBC(this.w_MRFLRIPA)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOVICOST')
        i_extval=cp_InsertValVFPExtFlds(this,'MOVICOST')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MRSERIAL',this.w_MRSERIAL,'MRROWORD',this.w_MRROWORD)
        INSERT INTO (i_cTable) (;
                   MRSERIAL;
                  ,MRROWORD;
                  ,MRCODVOC;
                  ,MRCODICE;
                  ,MRCODCOM;
                  ,MR_SEGNO;
                  ,MRTOTIMP;
                  ,MRINICOM;
                  ,MRFINCOM;
                  ,MRPARAME;
                  ,MRFLRIPA;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MRSERIAL;
                  ,this.w_MRROWORD;
                  ,this.w_MRCODVOC;
                  ,this.w_MRCODICE;
                  ,this.w_MRCODCOM;
                  ,this.w_MR_SEGNO;
                  ,this.w_MRTOTIMP;
                  ,this.w_MRINICOM;
                  ,this.w_MRFINCOM;
                  ,this.w_MRPARAME;
                  ,this.w_MRFLRIPA;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.MOVICOST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'MOVICOST')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'MOVICOST')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MOVICOST
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'MOVICOST')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MRCODVOC="+cp_ToStrODBCNull(this.w_MRCODVOC)+;
                     ",MRCODICE="+cp_ToStrODBCNull(this.w_MRCODICE)+;
                     ",MRCODCOM="+cp_ToStrODBCNull(this.w_MRCODCOM)+;
                     ",MR_SEGNO="+cp_ToStrODBC(this.w_MR_SEGNO)+;
                     ",MRTOTIMP="+cp_ToStrODBC(this.w_MRTOTIMP)+;
                     ",MRINICOM="+cp_ToStrODBC(this.w_MRINICOM)+;
                     ",MRFINCOM="+cp_ToStrODBC(this.w_MRFINCOM)+;
                     ",MRPARAME="+cp_ToStrODBC(this.w_MRPARAME)+;
                     ",MRFLRIPA="+cp_ToStrODBC(this.w_MRFLRIPA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'MOVICOST')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MRCODVOC=this.w_MRCODVOC;
                     ,MRCODICE=this.w_MRCODICE;
                     ,MRCODCOM=this.w_MRCODCOM;
                     ,MR_SEGNO=this.w_MR_SEGNO;
                     ,MRTOTIMP=this.w_MRTOTIMP;
                     ,MRINICOM=this.w_MRINICOM;
                     ,MRFINCOM=this.w_MRFINCOM;
                     ,MRPARAME=this.w_MRPARAME;
                     ,MRFLRIPA=this.w_MRFLRIPA;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOVICOST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete MOVICOST
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOVICOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,16,.t.)
          .w_TOTRIG = .w_TOTRIG-.w_imprig
          .w_IMPRIG = .w_MRTOTIMP*IIF(.w_MR_SEGNO='D',1,-1)
          .w_TOTRIG = .w_TOTRIG+.w_imprig
        .DoRTCalc(18,19,.t.)
          .w_TOTVIS = ABS(.w_TOTRIG)
          .w_SEGTOT = IIF(.w_TOTRIG<0, 'A', 'D')
        .DoRTCalc(22,24,.t.)
          .w_OBTEST = this.oParentObject .w_OBTEST
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_NUMLIV with this.w_NUMLIV
      replace t_MRPARAME with this.w_MRPARAME
      replace t_IMPRIG with this.w_IMPRIG
      replace t_MRFLRIPA with this.w_MRFLRIPA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRCODCOM_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRCODCOM_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRFINCOM_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRFINCOM_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MRCODVOC
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_MRCODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_MRCODVOC))
          select VCCODICE,VCDESCRI,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODVOC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oMRCODVOC_2_1'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'GSCA_MMC.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_MRCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_MRCODVOC)
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODVOC = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_VCDTOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODVOC = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_VCDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_VCDTOBSO) OR .w_VCDTOBSO>this.oParentObject .w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MRCODVOC = space(15)
        this.w_DESVOC = space(40)
        this.w_VCDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.VCCODICE as VCCODICE201"+ ",link_2_1.VCDESCRI as VCDESCRI201"+ ",link_2_1.VCDTOBSO as VCDTOBSO201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on MOVICOST.MRCODVOC=link_2_1.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and MOVICOST.MRCODVOC=link_2_1.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRCODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_MRCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_MRCODICE))
          select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODICE)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_MRCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_MRCODICE)+"%");

            select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MRCODICE) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oMRCODICE_2_2'),i_cWhere,'GSCA_ACC',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_MRCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_MRCODICE)
            select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODICE = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_NUMLIV = NVL(_Link_.CCNUMLIV,0)
      this.w_CCDTOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODICE = space(15)
      endif
      this.w_DESPIA = space(40)
      this.w_NUMLIV = 0
      this.w_CCDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CCDTOBSO>this.oParentObject .w_OBTEST or EMPTY(.w_CCDTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MRCODICE = space(15)
        this.w_DESPIA = space(40)
        this.w_NUMLIV = 0
        this.w_CCDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CC_CONTO as CC_CONTO202"+ ",link_2_2.CCDESPIA as CCDESPIA202"+ ",link_2_2.CCNUMLIV as CCNUMLIV202"+ ",link_2_2.CCDTOBSO as CCDTOBSO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on MOVICOST.MRCODICE=link_2_2.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and MOVICOST.MRCODICE=link_2_2.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRCODCOM
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MRCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MRCODCOM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_MRCODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_MRCODCOM)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MRCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMRCODCOM_2_3'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MRCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MRCODCOM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
      this.w_CNDTOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
      this.w_CNDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CNDTOBSO>this.oParentObject .w_OBTEST OR EMPTY(.w_CNDTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa inesistente oppure obsoleto")
        endif
        this.w_MRCODCOM = space(15)
        this.w_DESCAN = space(30)
        this.w_CNDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CNCODCAN as CNCODCAN203"+ ",link_2_3.CNDESCAN as CNDESCAN203"+ ",link_2_3.CNDTOBSO as CNDTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on MOVICOST.MRCODCOM=link_2_3.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and MOVICOST.MRCODCOM=link_2_3.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESVOC_2_8.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_2_8.value=this.w_DESVOC
      replace t_DESVOC with this.oPgFrm.Page1.oPag.oDESVOC_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA_2_9.value==this.w_DESPIA)
      this.oPgFrm.Page1.oPag.oDESPIA_2_9.value=this.w_DESPIA
      replace t_DESPIA with this.oPgFrm.Page1.oPag.oDESPIA_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_2_10.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_2_10.value=this.w_DESCAN
      replace t_DESCAN with this.oPgFrm.Page1.oPag.oDESCAN_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTVIS_3_2.value==this.w_TOTVIS)
      this.oPgFrm.Page1.oPag.oTOTVIS_3_2.value=this.w_TOTVIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSEGTOT_3_3.value==this.w_SEGTOT)
      this.oPgFrm.Page1.oPag.oSEGTOT_3_3.value=this.w_SEGTOT
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.value==this.w_MRCODVOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.value=this.w_MRCODVOC
      replace t_MRCODVOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODICE_2_2.value==this.w_MRCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODICE_2_2.value=this.w_MRCODICE
      replace t_MRCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCOM_2_3.value==this.w_MRCODCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCOM_2_3.value=this.w_MRCODCOM
      replace t_MRCODCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCOM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMR_SEGNO_2_4.value==this.w_MR_SEGNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMR_SEGNO_2_4.value=this.w_MR_SEGNO
      replace t_MR_SEGNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMR_SEGNO_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_5.value==this.w_MRTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_5.value=this.w_MRTOTIMP
      replace t_MRTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRINICOM_2_6.value==this.w_MRINICOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRINICOM_2_6.value=this.w_MRINICOM
      replace t_MRINICOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRINICOM_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFINCOM_2_7.value==this.w_MRFINCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFINCOM_2_7.value=this.w_MRFINCOM
      replace t_MRFINCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFINCOM_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'MOVICOST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_VCDTOBSO) OR .w_VCDTOBSO>this.oParentObject .w_OBTEST) and not(empty(.w_MRCODVOC)) and (.w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        case   not(.w_CCDTOBSO>this.oParentObject .w_OBTEST or EMPTY(.w_CCDTOBSO)) and not(empty(.w_MRCODICE)) and (.w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODICE_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        case   not(.w_CNDTOBSO>this.oParentObject .w_OBTEST OR EMPTY(.w_CNDTOBSO)) and (g_PERCAN='S') and not(empty(.w_MRCODCOM)) and (.w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCOM_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice commessa inesistente oppure obsoleto")
        case   not(.w_MR_SEGNO $ "DA") and (.w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMR_SEGNO_2_4
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(((.w_MRINICOM<=.w_MRFINCOM) and not empty(.w_MRINICOM)) OR EMPTY(.w_MRFINCOM)) and (.w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRINICOM_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Data inizio competenza incongruente")
        case   not(.w_mrinicom<=.w_mrfincom) and ((NOT empty(.w_MRINICOM)) OR ( NOT empty(.w_MRFINCOM) )) and (.w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFINCOM_2_7
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Data fine competenza incongruente")
      endcase
      if .w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MRCODVOC=space(15)
      .w_MRCODICE=space(15)
      .w_MRCODCOM=space(15)
      .w_MR_SEGNO=space(1)
      .w_MRTOTIMP=0
      .w_MRINICOM=ctod("  /  /  ")
      .w_MRFINCOM=ctod("  /  /  ")
      .w_DESVOC=space(40)
      .w_DESPIA=space(40)
      .w_DESCAN=space(30)
      .w_NUMLIV=0
      .w_MRPARAME=0
      .w_IMPRIG=0
      .w_MRFLRIPA=space(1)
      .DoRTCalc(1,5,.f.)
      if not(empty(.w_MRCODVOC))
        .link_2_1('Full')
      endif
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_MRCODICE))
        .link_2_2('Full')
      endif
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_MRCODCOM))
        .link_2_3('Full')
      endif
        .w_MR_SEGNO = .w_SEGNO
      .DoRTCalc(9,16,.f.)
        .w_IMPRIG = .w_MRTOTIMP*IIF(.w_MR_SEGNO='D',1,-1)
        .w_MRFLRIPA = ' '
    endwith
    this.DoRTCalc(19,25,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MRCODVOC = t_MRCODVOC
    this.w_MRCODICE = t_MRCODICE
    this.w_MRCODCOM = t_MRCODCOM
    this.w_MR_SEGNO = t_MR_SEGNO
    this.w_MRTOTIMP = t_MRTOTIMP
    this.w_MRINICOM = t_MRINICOM
    this.w_MRFINCOM = t_MRFINCOM
    this.w_DESVOC = t_DESVOC
    this.w_DESPIA = t_DESPIA
    this.w_DESCAN = t_DESCAN
    this.w_NUMLIV = t_NUMLIV
    this.w_MRPARAME = t_MRPARAME
    this.w_IMPRIG = t_IMPRIG
    this.w_MRFLRIPA = t_MRFLRIPA
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MRCODVOC with this.w_MRCODVOC
    replace t_MRCODICE with this.w_MRCODICE
    replace t_MRCODCOM with this.w_MRCODCOM
    replace t_MR_SEGNO with this.w_MR_SEGNO
    replace t_MRTOTIMP with this.w_MRTOTIMP
    replace t_MRINICOM with this.w_MRINICOM
    replace t_MRFINCOM with this.w_MRFINCOM
    replace t_DESVOC with this.w_DESVOC
    replace t_DESPIA with this.w_DESPIA
    replace t_DESCAN with this.w_DESCAN
    replace t_NUMLIV with this.w_NUMLIV
    replace t_MRPARAME with this.w_MRPARAME
    replace t_IMPRIG with this.w_IMPRIG
    replace t_MRFLRIPA with this.w_MRFLRIPA
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTRIG = .w_TOTRIG-.w_imprig
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsca_mmcPag1 as StdContainer
  Width  = 741
  height = 257
  stdWidth  = 741
  stdheight = 257
  resizeXpos=243
  resizeYpos=123
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=4, width=731,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="MRCODVOC",Label1="Voce costo/ricavo",Field2="MRCODICE",Label2="Centro di costo/ricavo",Field3="MRCODCOM",Label3="Commessa",Field4="MR_SEGNO",Label4="D/A",Field5="MRTOTIMP",Label5="Importo",Field6="MRINICOM",Label6="Inizio comp.",Field7="MRFINCOM",Label7="Fine comp.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118269050

  add object oStr_1_4 as StdString with uid="IGXWXTFBPO",Visible=.t., Left=351, Top=185,;
    Alignment=1, Width=54, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ULPPVHCXVT",Visible=.t., Left=6, Top=185,;
    Alignment=1, Width=93, Height=15,;
    Caption="Voce di C./R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="MWFNTQFYTD",Visible=.t., Left=6, Top=209,;
    Alignment=1, Width=93, Height=15,;
    Caption="Centro di C./R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="IVNUOGDWKR",Visible=.t., Left=6, Top=233,;
    Alignment=1, Width=93, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_3 as StdBox with uid="ABSIKWHVUZ",left=-86, top=-105, width=752,height=24

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=23,;
    width=727+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=24,width=726+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOC_COST|CENCOST|CAN_TIER|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESVOC_2_8.Refresh()
      this.Parent.oDESPIA_2_9.Refresh()
      this.Parent.oDESCAN_2_10.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOC_COST'
        oDropInto=this.oBodyCol.oRow.oMRCODVOC_2_1
      case cFile='CENCOST'
        oDropInto=this.oBodyCol.oRow.oMRCODICE_2_2
      case cFile='CAN_TIER'
        oDropInto=this.oBodyCol.oRow.oMRCODCOM_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oDESVOC_2_8 as StdTrsField with uid="UGKJOYPTTM",rtseq=12,rtrep=.t.,;
    cFormVar="w_DESVOC",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione voce di costo o ricavo",;
    HelpContextID = 58657738,;
    cTotal="", bFixedPos=.t., cQueryName = "DESVOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=102, Top=185, InputMask=replicate('X',40)

  add object oDESPIA_2_9 as StdTrsField with uid="INRQQCCOIS",rtseq=13,rtrep=.t.,;
    cFormVar="w_DESPIA",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione centro di costo o ricavo",;
    HelpContextID = 98896842,;
    cTotal="", bFixedPos=.t., cQueryName = "DESPIA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=102, Top=209, InputMask=replicate('X',40)

  add object oDESCAN_2_10 as StdTrsField with uid="XPFBRGBBAF",rtseq=14,rtrep=.t.,;
    cFormVar="w_DESCAN",value=space(30),enabled=.f.,;
    ToolTipText = "Descrizione della commessa",;
    HelpContextID = 158469066,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=102, Top=233, InputMask=replicate('X',30)

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTVIS_3_2 as StdField with uid="GQAGCUJBKG",rtseq=20,rtrep=.f.,;
    cFormVar="w_TOTVIS",value=0,enabled=.f.,;
    HelpContextID = 64942282,;
    cQueryName = "TOTVIS",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=131, Left=432, Top=185, cSayPict=[v_PV(20)], cGetPict=[v_GV(20)]

  add object oSEGTOT_3_3 as StdField with uid="GYZGEPEUGT",rtseq=21,rtrep=.f.,;
    cFormVar="w_SEGTOT",value=space(1),enabled=.f.,;
    HelpContextID = 42060506,;
    cQueryName = "SEGTOT",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=408, Top=185, InputMask=replicate('X',1)
enddefine

* --- Defining Body row
define class tgsca_mmcBodyRow as CPBodyRowCnt
  Width=717
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMRCODVOC_2_1 as StdTrsField with uid="HXQXQBBUDR",rtseq=5,rtrep=.t.,;
    cFormVar="w_MRCODVOC",value=space(15),;
    ToolTipText = "Codice voce di costo o ricavo",;
    HelpContextID = 20381175,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=-2, Top=0, cSayPict=[p_MCE], cGetPict=[p_MCE], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_MRCODVOC"

  proc oMRCODVOC_2_1.mBefore
    with this.Parent.oContained
      IIF(EMPTY(.w_MRCODVOC) AND .w_GIACAR=' ', .NotifyEvent('CaricaAuto'), '')
    endwith
  endproc

  func oMRCODVOC_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODVOC_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODVOC_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oMRCODVOC_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'GSCA_MMC.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oMRCODVOC_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_MRCODVOC
    i_obj.ecpSave()
  endproc

  add object oMRCODICE_2_2 as StdTrsField with uid="SBAGBWJOTW",rtseq=6,rtrep=.t.,;
    cFormVar="w_MRCODICE",value=space(15),;
    ToolTipText = "Codice centro di costo e ricavo",;
    HelpContextID = 29950475,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=130, Top=0, cSayPict=[p_CEN], cGetPict=[p_CEN], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_MRCODICE"

  func oMRCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODICE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oMRCODICE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo",'',this.parent.oContained
  endproc
  proc oMRCODICE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_MRCODICE
    i_obj.ecpSave()
  endproc

  add object oMRCODCOM_2_3 as StdTrsField with uid="XXRAOBDHBJ",rtseq=7,rtrep=.t.,;
    cFormVar="w_MRCODCOM",value=space(15),;
    ToolTipText = "Codice commessa",;
    HelpContextID = 70712813,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=262, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MRCODCOM"

  func oMRCODCOM_2_3.mCond()
    with this.Parent.oContained
      return (g_PERCAN='S')
    endwith
  endfunc

  func oMRCODCOM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODCOM_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODCOM_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMRCODCOM_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oMRCODCOM_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_MRCODCOM
    i_obj.ecpSave()
  endproc

  add object oMR_SEGNO_2_4 as StdTrsField with uid="YIGWZTOQGQ",rtseq=8,rtrep=.t.,;
    cFormVar="w_MR_SEGNO",value=space(1),;
    ToolTipText = "Sezione importo: D= dare; A =avere",;
    HelpContextID = 2178539,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=394, Top=0, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  proc oMR_SEGNO_2_4.mAfter
    with this.Parent.oContained
      .w_SEGNO=.w_MR_SEGNO
    endwith
  endproc

  func oMR_SEGNO_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MR_SEGNO $ "DA")
    endwith
    return bRes
  endfunc

  add object oMRTOTIMP_2_5 as StdTrsField with uid="GMMSIVHTUT",rtseq=9,rtrep=.t.,;
    cFormVar="w_MRTOTIMP",value=0,;
    ToolTipText = "Importo centro di costo",;
    HelpContextID = 221638122,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=421, Top=0, cSayPict=[v_PV(20)], cGetPict=[v_GV(20)]

  add object oMRINICOM_2_6 as StdTrsField with uid="WEMLRGGPHR",rtseq=10,rtrep=.t.,;
    cFormVar="w_MRINICOM",value=ctod("  /  /  "),;
    ToolTipText = "Data di inizio competenza; l'intervallo deve essere vuoto oppure finito",;
    HelpContextID = 65510893,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data inizio competenza incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=553, Top=0, TabStop=.t.

  func oMRINICOM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_MRINICOM<=.w_MRFINCOM) and not empty(.w_MRINICOM)) OR EMPTY(.w_MRFINCOM))
    endwith
    return bRes
  endfunc

  add object oMRFINCOM_2_7 as StdTrsField with uid="VDMLADCBDL",rtseq=11,rtrep=.t.,;
    cFormVar="w_MRFINCOM",value=ctod("  /  /  "),;
    ToolTipText = "Data di fine competenza; l'intervallo deve essere vuoto oppure finito",;
    HelpContextID = 60607981,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data fine competenza incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=636, Top=0, TabStop=.t.

  func oMRFINCOM_2_7.mCond()
    with this.Parent.oContained
      return ((NOT empty(.w_MRINICOM)) OR ( NOT empty(.w_MRFINCOM) ))
    endwith
  endfunc

  func oMRFINCOM_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_mrinicom<=.w_mrfincom)
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oMRCODVOC_2_1.When()
    return(.t.)
  proc oMRCODVOC_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMRCODVOC_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_mmc','MOVICOST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MRSERIAL=MOVICOST.MRSERIAL";
  +" and "+i_cAliasName2+".MRROWORD=MOVICOST.MRROWORD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
