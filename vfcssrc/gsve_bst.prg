* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bst                                                        *
*              Stampa schede di trasporto                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-09-29                                                      *
* Last revis.: 2009-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bst",oParentObject,m.pOPER)
return(i_retval)

define class tgsve_bst as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_PADRE = .NULL.
  w_FL_ERROR = .f.
  w_ATTREC = 0
  * --- WorkFile variables
  TMPVEND2_idx=0
  TIP_DOCU_idx=0
  TMPVEND3_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FL_ERROR = .F.
    this.w_PADRE = this.oParentObject
    do case
      case this.pOPER=="SEARCH" OR this.pOPER=="SEARCHPAG"
        if !EMPTY(this.oParentObject.w_MVCODVET)
          if this.oParentObject.w_FLVARMOD OR this.pOPER=="SEARCH"
            this.oParentObject.w_NUMDOCSE = 0
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PADRE.NotifyEvent("Ricerca")     
            this.w_PADRE.oPgFrm.ActivePage = 2
            this.oParentObject.w_FLVARMOD = .F.
          endif
        else
          ah_ErrorMsg("Impossibile avviare la ricerca. Indicare il vettore")
          this.w_PADRE.oPgFrm.ActivePage = 1
        endif
      case this.pOPER=="PRINT"
        SELECT MVSERIAL FROM (this.oParentObject.w_DOCUMENTI.cCursor ) WHERE XCHK=1 INTO CURSOR "_DocSel_"
        if USED("_DocSel_")
          * --- Create temporary table TMPVEND3
          i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          i_nConn=i_TableProp[this.DOC_MAST_idx,3] && recupera la connessione
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          cp_CreateTempTable(i_nConn,i_cTempTable,"MVSERIAL "," from "+i_cTable;
                +" where 1=0";
                )
          this.TMPVEND3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          SELECT "_DocSel_"
          GO TOP
          SCAN
          * --- Insert into TMPVEND3
          i_nConn=i_TableProp[this.TMPVEND3_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPVEND3_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MVSERIAL"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(_DocSel_.MVSERIAL),'TMPVEND3','MVSERIAL');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',_DocSel_.MVSERIAL)
            insert into (i_cTable) (MVSERIAL &i_ccchkf. );
               values (;
                 _DocSel_.MVSERIAL;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          SELECT "_DocSel_"
          ENDSCAN
        endif
        USE IN SELECT("_DocSel_")
        vx_exec(""+ALLTRIM(this.oParentObject.w_OQRY)+","+ALLTRIM(this.oParentObject.w_OREP)+"",this)
      case this.pOPER=="CLOSE"
        * --- Drop temporary table TMPVEND2
        i_nIdx=cp_GetTableDefIdx('TMPVEND2')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND2')
        endif
      case this.pOPER=="LOAD"
        * --- Create temporary table TMPVEND2
        i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"TDTIPDOC "," from "+i_cTable;
              +" where 1=0";
              )
        this.TMPVEND2_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.bUpdateParentObject=.F.
      case this.pOPER=="SELEZ" OR this.pOPER=="DESELEZ" OR this.pOPER=="INVSELEZ" OR this.pOPER=="INIT"
        UPDATE (this.oParentObject.w_CAUSALI.cCursor) SET XCHK=ICASE(this.pOPER=="SELEZ" OR this.pOPER=="INIT", 1, this.pOPER=="DESELEZ", 0, IIF(XCHK=1,0,1))
        SELECT (this.oParentObject.w_CAUSALI.cCursor)
        GO TOP
      case this.pOPER=="CHKDOC"
        SELECT (this.oParentObject.w_DOCUMENTI.cCursor )
        this.w_ATTREC = RECNO()
        COUNT FOR XCHK=1 TO this.oParentObject.w_NUMDOCSE
        GO this.w_ATTREC
      case this.pOPER=="SELDOC" OR this.pOPER=="DESELDOC" OR this.pOPER=="INVSELDOC"
        UPDATE (this.oParentObject.w_DOCUMENTI.cCursor) SET XCHK=ICASE(this.pOPER=="SELDOC", 1, this.pOPER=="DESELDOC", 0, IIF(XCHK=1,0,1))
        SELECT (this.oParentObject.w_DOCUMENTI.cCursor)
        GO TOP
        COUNT FOR XCHK=1 TO this.oParentObject.w_NUMDOCSE
      case this.pOPER=="AGGCAU"
        this.w_PADRE.NotifyEvent("AggCau")     
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    SELECT TDTIPDOC FROM (this.oParentObject.w_CAUSALI.cCursor ) WHERE XCHK=1 INTO CURSOR "_CauSel_"
    if USED("_CauSel_")
      * --- Create temporary table TMPVEND2
      i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"TDTIPDOC "," from "+i_cTable;
            +" where 1=0";
            )
      this.TMPVEND2_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      SELECT "_CauSel_"
      GO TOP
      SCAN
      * --- Insert into TMPVEND2
      i_nConn=i_TableProp[this.TMPVEND2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPVEND2_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"TDTIPDOC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(_CauSel_.TDTIPDOC),'TMPVEND2','TDTIPDOC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'TDTIPDOC',_CauSel_.TDTIPDOC)
        insert into (i_cTable) (TDTIPDOC &i_ccchkf. );
           values (;
             _CauSel_.TDTIPDOC;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      SELECT "_CauSel_"
      ENDSCAN
    endif
    USE IN SELECT("_CauSel_")
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='*TMPVEND2'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='*TMPVEND3'
    this.cWorkTables[4]='DOC_MAST'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
