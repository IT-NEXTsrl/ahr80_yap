* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bi5                                                        *
*              Dettaglio cont.indiretta                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_155]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-01                                                      *
* Last revis.: 2007-01-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bi5",oParentObject,m.pTipOpe)
return(i_retval)

define class tgscg_bi5 as StdBatch
  * --- Local variables
  pTipOpe = space(10)
  w_DATSCA = ctod("  /  /  ")
  w_DATSCA1 = ctod("  /  /  ")
  w_NUMPAR = space(31)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_MODPAG = space(10)
  w_FLSOSP = space(1)
  w_TOTIMP = 0
  w_SIMVAL = space(3)
  w_BANAPP = space(10)
  w_BANNOS = space(15)
  w_SERIAL = space(10)
  w_ROWORD = 0
  w_ROWNUM = 0
  w_NUMERO = space(31)
  w_TEST1 = 0
  w_PTSERRIF = space(10)
  w_OK = .f.
  w_OBANNOS = space(15)
  w_OBANAPP = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PTRIFIND = space(10)
  w_SERIAL = space(10)
  w_ROWORD = 0
  w_ROWNUM = 0
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia la maschera 'Dettaglio Partite' dal bottone Dettaglio nella maschera (GSCG_ACI)
    do case
      case this.pTipOpe="PARTITE"
        * --- Assegno alle altre il valore dei campi dello Zoom
        this.oParentObject.w_PTSERIAL = nvl(this.oParentObject.w_CalcZoom.getVar("PTSERIAL"), space(10))
        this.oParentObject.w_PTROWORD = nvl(this.oParentObject.w_CalcZoom.getVar("PTROWORD"), 0)
        this.oParentObject.w_CPROWNUM = nvl(this.oParentObject.w_CalcZoom.getVar("CPROWNUM"), 0)
        this.w_BANNOS = nvl(this.oParentObject.w_CalcZoom.getVar("BANNOS"), space(15))
        this.w_BANAPP = nvl(this.oParentObject.w_CalcZoom.getVar("BANAPP"), space(10))
        this.w_DATSCA1 = nvl(this.oParentObject.w_CalcZoom.getVar("DATSCA"), cp_CharToDate("  -  -    "))
        this.w_DATSCA = nvl(this.oParentObject.w_CalcZoom.getVar("DATSCA"), cp_CharToDate("  -  -    "))
        this.w_NUMPAR = nvl(this.oParentObject.w_CalcZoom.getVar("NUMPAR"), space(31))
        this.w_TIPCON = nvl(this.oParentObject.w_CalcZoom.getVar("TIPCON"), space(1))
        this.w_CODCON = nvl(this.oParentObject.w_CalcZoom.getVar("CODCON"), space(15))
        this.w_MODPAG = nvl(this.oParentObject.w_CalcZoom.getVar("MODPAG"), space(10))
        this.w_FLSOSP = nvl(this.oParentObject.w_CalcZoom.getVar("PTFLSOSP"), space(1))
        this.w_TOTIMP = nvl(this.oParentObject.w_CalcZoom.getVar("TOTIMP"), 0)
        this.w_SIMVAL = nvl(this.oParentObject.w_CalcZoom.getVar("SIMVAL"), space(3))
        this.w_PTSERRIF = nvl(this.oParentObject.w_CalcZoom.getVar("PTSERRIF"), space(10))
        this.w_PTORDRIF = nvl(this.oParentObject.w_CalcZoom.getVar("PTORDRIF"), 0)
        this.w_PTNUMRIF = nvl(this.oParentObject.w_CalcZoom.getVar("PTNUMRIF"), 0)
        this.w_OBANNOS = nvl(this.oParentObject.w_CalcZoom.getVar("BANNOS"), space(15))
        this.w_OBANAPP = nvl(this.oParentObject.w_CalcZoom.getVar("BANAPP"), space(10))
        this.w_PTRIFIND = nvl(this.oParentObject.w_CalcZoom.getVar("PTRIFIND"), space(10))
        * --- Verifico se la partita � in distinta, se lo � non abilito il cambio della banca
        *     Due Casi pre 2.2 (PTSERRIF vuoto) cerco una partita con le solite "caratteristiche" post
        *     2.2 cerco una partita che "punti" quella contabilizzata e che appartenga
        *     ad una distinta
        if Empty( this.w_PTSERRIF )
          * --- Read from PAR_TITE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PTNUMPAR"+;
              " from "+i_cTable+" PAR_TITE where ";
                  +"PTROWORD = "+cp_ToStrODBC(-2);
                  +" and PTNUMPAR = "+cp_ToStrODBC(this.w_NUMPAR);
                  +" and PTDATSCA = "+cp_ToStrODBC(this.w_DATSCA1);
                  +" and PTTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and PTCODCON = "+cp_ToStrODBC(this.w_CODCON);
                  +" and PTCODVAL = "+cp_ToStrODBC(this.oParentObject.w_CICODVAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PTNUMPAR;
              from (i_cTable) where;
                  PTROWORD = -2;
                  and PTNUMPAR = this.w_NUMPAR;
                  and PTDATSCA = this.w_DATSCA1;
                  and PTTIPCON = this.w_TIPCON;
                  and PTCODCON = this.w_CODCON;
                  and PTCODVAL = this.oParentObject.w_CICODVAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NUMERO = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from PAR_TITE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PTNUMPAR"+;
              " from "+i_cTable+" PAR_TITE where ";
                  +"PTROWORD = "+cp_ToStrODBC(-2);
                  +" and PTSERRIF = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
                  +" and PTORDRIF = "+cp_ToStrODBC(this.oParentObject.w_PTROWORD);
                  +" and PTNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PTNUMPAR;
              from (i_cTable) where;
                  PTROWORD = -2;
                  and PTSERRIF = this.oParentObject.w_PTSERIAL;
                  and PTORDRIF = this.oParentObject.w_PTROWORD;
                  and PTNUMRIF = this.oParentObject.w_CPROWNUM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NUMERO = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_TEST1 = i_Rows=0
        this.w_OK = .F.
        * --- Lancia la maschera Dettaglio Partite
        do GSCG_KI5 with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_OK
          * --- Vado a scrivere nei campi dello Zoom che ho lasciato editabili nella maschera Dettagli Partite
          * --- Controllo la variabile Test per verificare se � stata lanciata la maschera GSCG_KI5
          * --- Write into PAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTBANAPP ="+cp_NullLink(cp_ToStrODBC(this.w_BANAPP),'PAR_TITE','PTBANAPP');
            +",PTBANNOS ="+cp_NullLink(cp_ToStrODBC(this.w_BANNOS),'PAR_TITE','PTBANNOS');
            +",PTDATSCA ="+cp_NullLink(cp_ToStrODBC(this.w_DATSCA),'PAR_TITE','PTDATSCA');
            +",PTMODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MODPAG),'PAR_TITE','PTMODPAG');
            +",PTFLSOSP ="+cp_NullLink(cp_ToStrODBC(this.w_FLSOSP),'PAR_TITE','PTFLSOSP');
                +i_ccchkf ;
            +" where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(this.oParentObject.w_PTROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                PTBANAPP = this.w_BANAPP;
                ,PTBANNOS = this.w_BANNOS;
                ,PTDATSCA = this.w_DATSCA;
                ,PTMODPAG = this.w_MODPAG;
                ,PTFLSOSP = this.w_FLSOSP;
                &i_ccchkf. ;
             where;
                PTSERIAL = this.oParentObject.w_PTSERIAL;
                and PTROWORD = this.oParentObject.w_PTROWORD;
                and CPROWNUM = this.oParentObject.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if this.w_BANNOS<>this.w_OBANNOS OR this.w_BANAPP<>this.w_OBANAPP
            * --- Aggiorno anche banche nelle partita in primanota
            *     azzero eventuale numero c\c impostato perch� non pi� coerente
            * --- Select from PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select PTSERIAL,PTROWORD,CPROWNUM,PTSERRIF,PTORDRIF,PTNUMRIF  from "+i_cTable+" PAR_TITE ";
                  +" where PTSERRIF="+cp_ToStrODBC(this.w_PTSERRIF)+" AND PTORDRIF="+cp_ToStrODBC(this.w_PTORDRIF)+" AND PTNUMRIF="+cp_ToStrODBC(this.w_PTNUMRIF)+" AND PTSERIAL="+cp_ToStrODBC(this.w_PTRIFIND)+"";
                   ,"_Curs_PAR_TITE")
            else
              select PTSERIAL,PTROWORD,CPROWNUM,PTSERRIF,PTORDRIF,PTNUMRIF from (i_cTable);
               where PTSERRIF=this.w_PTSERRIF AND PTORDRIF=this.w_PTORDRIF AND PTNUMRIF=this.w_PTNUMRIF AND PTSERIAL=this.w_PTRIFIND;
                into cursor _Curs_PAR_TITE
            endif
            if used('_Curs_PAR_TITE')
              select _Curs_PAR_TITE
              locate for 1=1
              do while not(eof())
              this.w_SERIAL = _Curs_PAR_TITE.PTSERIAL
              this.w_ROWORD = _Curs_PAR_TITE.PTROWORD
              this.w_ROWNUM = _Curs_PAR_TITE.CPROWNUM
              * --- Write into PAR_TITE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PTBANAPP ="+cp_NullLink(cp_ToStrODBC(this.w_BANAPP),'PAR_TITE','PTBANAPP');
                +",PTBANNOS ="+cp_NullLink(cp_ToStrODBC(this.w_BANNOS),'PAR_TITE','PTBANNOS');
                +",PTNUMCOR ="+cp_NullLink(cp_ToStrODBC(SPACE(25)),'PAR_TITE','PTNUMCOR');
                    +i_ccchkf ;
                +" where ";
                    +"PTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                    +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                       )
              else
                update (i_cTable) set;
                    PTBANAPP = this.w_BANAPP;
                    ,PTBANNOS = this.w_BANNOS;
                    ,PTNUMCOR = SPACE(25);
                    &i_ccchkf. ;
                 where;
                    PTSERIAL = this.w_SERIAL;
                    and PTROWORD = this.w_ROWORD;
                    and CPROWNUM = this.w_ROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
                select _Curs_PAR_TITE
                continue
              enddo
              use
            endif
          endif
           this.oParentObject.NotifyEvent("Legge")
        endif
      case this.pTipOpe="VALUTA"
        * --- Verifica se la Valuta e' Obsoleta
        = CHKDTOBS(this.oParentObject.w_DTOBSO,this.oParentObject.w_OBTEST,"Valuta obsoleta alla data attuale", .T.)
    endcase
  endproc


  proc Init(oParentObject,pTipOpe)
    this.pTipOpe=pTipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe"
endproc
