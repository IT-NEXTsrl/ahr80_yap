* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste1bec                                                        *
*              Crea xml x estratto conto ahe\AHR                               *
*                                                                              *
*      Author: ZUCCHETTI S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_237]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-23                                                      *
* Last revis.: 2009-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste1bec",oParentObject)
return(i_retval)

define class tgste1bec as StdBatch
  * --- Local variables
  w_NOMEFILE = space(40)
  w_PATH = space(200)
  w_LINEFEED = space(2)
  w_ABSFILENAME = space(200)
  w_HANDLEFILE = 0
  w_APPLICATIONID = space(5)
  w_XMLSTR = space(255)
  w_PROGRESS = 0
  w_DESCELAB = space(50)
  w_DESCXML = space(50)
  w_ANSIDATE = space(10)
  w_DESCRPAG = space(35)
  w_CODPAG = space(10)
  w_ALFADOC = space(15)
  w_NUMREC = 0
  w_OLDSETCENTURY = space(2)
  w_CODUTE = 0
  w_SETSEP = space(1)
  w_DATA = ctod("  /  /  ")
  w_FILEXML = space(254)
  w_NHF = 0
  w_TMPC = space(254)
  w_TMPN = 0
  * --- WorkFile variables
  ZOOMPART_idx=0
  ZPARAMGEST_idx=0
  CONTROPA_idx=0
  ZELABORAZ_idx=0
  MOD_PAGA_idx=0
  CONF_INT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per la generazione del file *.xml nel formato richiesto dallo schema del Corporat Portal
    * --- Init
    this.w_LINEFEED = CHR(13)+CHR(10)
    * --- Verifico presenza dati nella tabella ZOOMPART 
    this.w_CODUTE = i_CODUTE
    * --- Read from CONF_INT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONF_INT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2],.t.,this.CONF_INT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CISETMRK"+;
        " from "+i_cTable+" CONF_INT where ";
            +"CICODUTE = "+cp_ToStrODBC(this.w_CODUTE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CISETMRK;
        from (i_cTable) where;
            CICODUTE = this.w_CODUTE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SETSEP = NVL(cp_ToDate(_read_.CISETMRK),cp_NullValue(_read_.CISETMRK))
      use
      if i_Rows=0
        * --- Read from CONF_INT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONF_INT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2],.t.,this.CONF_INT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CISETMRK"+;
            " from "+i_cTable+" CONF_INT where ";
                +"CICODUTE = "+cp_ToStrODBC(-1);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CISETMRK;
            from (i_cTable) where;
                CICODUTE = -1;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SETSEP = NVL(cp_ToDate(_read_.CISETMRK),cp_NullValue(_read_.CISETMRK))
          use
          if i_Rows=0
            this.w_SETSEP = "\"
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Select from ZOOMPART
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2],.t.,this.ZOOMPART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select count(*) as NumRec  from "+i_cTable+" ZOOMPART ";
           ,"_Curs_ZOOMPART")
    else
      select count(*) as NumRec from (i_cTable);
        into cursor _Curs_ZOOMPART
    endif
    if used('_Curs_ZOOMPART')
      select _Curs_ZOOMPART
      locate for 1=1
      do while not(eof())
      this.w_NUMREC = _Curs_ZOOMPART.NumRec
        select _Curs_ZOOMPART
        continue
      enddo
      use
    endif
    if this.w_NUMREC = 0
      CP_ERRORMSG("Attenzione: nessuna partita aperta!"+chr(13)+"Impossibile generare il file.","!")
      * --- Uscita forzata dall'elaborazione
      i_retcode = 'stop'
      return
    endif
    * --- Leggo ID dell'applicazione
    * --- Read from ZPARAMGEST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ZPARAMGEST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZPARAMGEST_idx,2],.t.,this.ZPARAMGEST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAAPPLICATID"+;
        " from "+i_cTable+" ZPARAMGEST where ";
            +"PACODICE = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAAPPLICATID;
        from (i_cTable) where;
            PACODICE = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APPLICATIONID = NVL(cp_ToDate(_read_.PAAPPLICATID),cp_NullValue(_read_.PAAPPLICATID))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo il path assegnato ai file *.xml
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COPATHCP"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COPATHCP;
        from (i_cTable) where;
            COCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PATH = NVL(cp_ToDate(_read_.COPATHCP),cp_NullValue(_read_.COPATHCP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_APPLICATIONID = nvl(this.w_APPLICATIONID,space(5))
    this.w_PATH = nvl(this.w_PATH,space(200))
    * --- Controllo presenza parametri obbligatori
    if empty(this.w_APPLICATIONID) or empty(this.w_PATH)
      if g_IZCP $ "SA"
        if empty(this.w_APPLICATIONID)
          if empty(this.w_PATH)
            ah_ErrorMsg("Non � presente l'id relativo all'applicazione%0Non � presente il percorso di destinazione del file%0%0L'elaborazione sar� interrotta","STOP","")
          else
            ah_ErrorMsg("Non � presente l'id relativo all'applicazione%0%0L'elaborazione sar� interrotta","STOP","")
          endif
        else
          if empty(this.w_PATH)
            ah_ErrorMsg("Non � presente il percorso di destinazione del file%0%0L'elaborazione sar� interrotta","STOP","")
          else
            ah_ErrorMsg("L'elaborazione sar� interrotta","STOP","")
          endif
        endif
        i_retcode = 'stop'
        return
      else
        if EMPTY(this.w_APPLICATIONID)
          this.w_APPLICATIONID = i_CODAZI
        endif
        if EMPTY(this.w_PATH)
          this.w_PATH=Cp_GetDir(sys(5)+sys(2003),ah_Msgformat("Percorso di destinazione") )
        endif
      endif
    endif
    * --- Se non c'� crea la cartella ...
    if Not(this.AH_ExistFolder(this.w_PATH))
      if Not(this.AH_CreateFolder(this.w_PATH))
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_NOMEFILE = alltrim(this.w_PATH)+iif(right(alltrim(this.w_path),1)="\","duedates_in","\duedates_in")
    this.w_NOMEFILE = this.w_NOMEFILE+"_"+dtos(Date())+strtran(Time(),":","")
    this.w_DESCELAB = ah_Msgformat("Estratto conto clienti aggiornato al %1 ore %2" ,dtoc(i_DATSYS), time() )
    this.w_DESCXML = ah_Msgformat("Estratto conto generato il %1 ore %2", dtoc(i_DATSYS), time() )
    * --- Generazione file  per Corporate Portal
    * --- absolute file name
    this.w_ABSFILENAME = this.w_NOMEFILE+".xml"
    * --- Apro il file
    this.w_HANDLEFILE = FCREATE(this.w_ABSFILENAME)
    if this.w_HANDLEFILE < 0
      ah_ErrorMsg("Errore nella creazione del file xml %1","!","", this.w_ABSFILENAME)
      i_retcode = 'stop'
      return
    endif
    do case
      case g_IZCP="S"
        this.w_PROGRESS = ZCPINDIV("EC",this.w_DESCELAB,i_DatSys,"S",this.w_ABSFILENAME," "," ","N"," "," ")
        if this.w_PROGRESS<0
          * --- Chiude file e lo cancella
          FCLOSE(this.w_HANDLEFILE)
          if File(this.w_ABSFILENAME)
            DELETE FILE (this.w_ABSFILENAME)
          endif
          * --- Messaggio ...
          ah_ErrorMsg("Impossibile aggiornare tabella divulgazione dati%0File: %1","!","", this.w_ABSFILENAME )
          * --- Uscita
          i_retcode = 'stop'
          return
        endif
      case g_IZCP="A"
        * --- Stand-alone
        this.w_FILEXML = ""
        this.w_NHF = -100
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_NHF<0
          * --- Chiude file e lo cancella
          FCLOSE(this.w_HANDLEFILE)
          if File(this.w_ABSFILENAME)
            DELETE FILE (this.w_ABSFILENAME)
          endif
          * --- Messaggio ...
          ah_ErrorMsg("Impossibile generare file xml-descrittore per divulgazione dati%0File: %1.xml","!","", this.w_ABSFILENAME)
          * --- Uscita
          i_retcode = 'stop'
          return
        endif
    endcase
    * --- Try
    local bErr_026D8620
    bErr_026D8620=bTrsErr
    this.Try_026D8620()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Elimino il record precedentemente inserito
      do case
        case g_IZCP="S"
          * --- Delete from ZELABORAZ
          i_nConn=i_TableProp[this.ZELABORAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ZELABORAZ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"DVCODELA = "+cp_ToStrODBC(this.w_PROGRESS);
                   )
          else
            delete from (i_cTable) where;
                  DVCODELA = this.w_PROGRESS;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        case g_IZCP="A"
          if FILE(this.w_FILEXML)
            DELETE FILE (this.w_FILEXML)
          endif
      endcase
      FCLOSE(this.w_HANDLEFILE)
      ah_ErrorMsg("Errore in generazione del file %1%0Operazione sospesa","!","", this.w_ABSFILENAME)
    endif
    bTrsErr=bTrsErr or bErr_026D8620
    * --- End
    * --- Ripristino il settaggio iniziale
    if this.w_OLDSETCENTURY = "ON"
      SET CENTURY ON
    else
      SET CENTURY OFF
    endif
  endproc
  proc Try_026D8620()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_OLDSETCENTURY = SET("CENTURY")
    * --- Verifico il formato assegnato all'anno nelle date. Se � di 4 0 2 caratteri
    if this.w_OLDSETCENTURY <> "ON"
      SET CENTURY ON
    endif
    * --- Init file XML
    FWRITE(this.w_HANDLEFILE,"<?xml version="+CHR(34)+"1.0"+CHR(34)+" encoding="+CHR(34)+"iso-8859-1"+CHR(34)+"?>"+this.w_LINEFEED)
    FWRITE(this.w_HANDLEFILE,"<Duedates applicationId="+CHR(34)+this.w_APPLICATIONID+CHR(34)+">"+this.w_LINEFEED)
    set date to ANSI 
 data=strtran(dtoc(date()),this.w_SETSEP,"-")+"T"+time() 
 set date to italian
    FWRITE(this.w_HANDLEFILE,"   <Rows divulgationDate="+CHR(34)+data+chr(34)+" divulgationType="+chr(34)+"EC"+chr(34)+" elaborationCode="+chr(34)+; 
 alltrim(str(this.w_PROGRESS))+chr(34)+" description="+chr(34)+alltrim(this.w_DESCXML)+chr(34)+">"+this.w_LINEFEED)
    * --- Select from ZOOMPART
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2],.t.,this.ZOOMPART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ZOOMPART ";
          +" where ORDINE=0";
           ,"_Curs_ZOOMPART")
    else
      select * from (i_cTable);
       where ORDINE=0;
        into cursor _Curs_ZOOMPART
    endif
    if used('_Curs_ZOOMPART')
      select _Curs_ZOOMPART
      locate for 1=1
      do while not(eof())
      this.w_XMLSTR = ""
      * --- Write XML String
      FWRITE(this.w_HANDLEFILE,"     <Row>"+this.w_LINEFEED)
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<Rownumber>"+alltrim(str(recno()))+"</Rownumber>"+this.w_LINEFEED
      if ! empty(nvl(_Curs_ZOOMPART.CODCON,space(15)))
        this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<CompanyId>"+alltrim(this.convertXML(nvl(_Curs_ZOOMPART.TIPCON," ")+nvl(_Curs_ZOOMPART.CODCON,space(15))))+"</CompanyId>"+this.w_LINEFEED
      endif
      if ! empty(nvl(_Curs_ZOOMPART.CODAGE,space(15)))
        this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<SalesagentId>"+"A"+alltrim(this.convertXML(nvl(_Curs_ZOOMPART.CODAGE,space(15))))+"</SalesagentId>"+this.w_LINEFEED
      endif
      * --- WRITE ON XML
      FWRITE(this.w_HANDLEFILE,this.w_XMLSTR)
      this.w_XMLSTR = ""
      if ! empty(nvl(_Curs_ZOOMPART.NUMDOC,0))
        this.w_ALFADOC = iif(not empty(nvl(_Curs_ZOOMPART.ALFDOC,"")),"/"+nvl(_Curs_ZOOMPART.ALFDOC,""),"")
        this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<DocumentNumber>"+alltrim(this.convertXML(Alltrim(str(nvl(_Curs_ZOOMPART.NUMDOC,0),15))+Alltrim(this.w_ALFADOC)))+"</DocumentNumber>"+this.w_LINEFEED
      endif
      this.w_DATA = nvl(_Curs_ZOOMPART.DATDOC,nvl(_Curs_ZOOMPART.PTDATAPE,cp_CharToDate("  -  -    ")))
      this.w_ANSIDATE = Alltrim(str(Year(this.w_DATA))) + "-"+ Alltrim(Right("00"+Alltrim(str(Month(this.w_DATA))),2)) +"-"+ Alltrim(Right("00"+ALLTRIM(str(Day(this.w_DATA))),2))
      if Not Empty(this.w_DATA)
        this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<DocumentData>"+alltrim(this.convertXML(this.w_ANSIDATE))+"</DocumentData>"+this.w_LINEFEED
      endif
      * --- Verifico se la scadenza � stata abbinata
      if ! empty(nvl(_Curs_ZOOMPART.DATRAG,cp_CharToDate("  -  -    ")))
        this.w_DATA = nvl(_Curs_ZOOMPART.DATRAG,cp_CharToDate("  -  -    "))
      else
        this.w_DATA = nvl(_Curs_ZOOMPART.DATSCA,cp_CharToDate("  -  -    "))
      endif
      this.w_ANSIDATE = Alltrim(str(Year(this.w_DATA))) + "-"+ Alltrim(Right("00"+Alltrim(str(Month(this.w_DATA))),2)) +"-"+ Alltrim(Right("00"+Alltrim(str(Day(this.w_DATA))),2))
      if Not Empty(this.w_DATA)
        this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<MaturityDate>"+alltrim(this.convertXML(this.w_ANSIDATE))+"</MaturityDate>"+this.w_LINEFEED
      endif
      * --- WRITE ON XML
      FWRITE(this.w_HANDLEFILE,this.w_XMLSTR)
      this.w_XMLSTR = ""
      this.w_DATA = nvl(_Curs_ZOOMPART.PTDATREG,nvl(_Curs_ZOOMPART.DATREG,cp_CharToDate("  -  -    ")))
      this.w_ANSIDATE = Alltrim(str(Year(this.w_DATA))) + "-"+ Alltrim(Right("00"+Alltrim(str(Month(this.w_DATA))),2)) +"-"+ Alltrim(Right("00"+Alltrim(str(Day(this.w_DATA))),2))
      if Not Empty(this.w_DATA)
        this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<OperationDate>"+alltrim(this.convertXML(this.w_ANSIDATE))+"</OperationDate>"+this.w_LINEFEED
      endif
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<DueNumber>"+alltrim(this.convertXML(nvl(_Curs_ZOOMPART.NUMPAR,space(31))))+"</DueNumber>"+this.w_LINEFEED
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<DueDescription>"+alltrim(this.convertXML(nvl(_Curs_ZOOMPART.DESCAU,space(50))))+"</DueDescription>"+this.w_LINEFEED
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<FlagUnsolved>"+iif(_Curs_ZOOMPART.INSOLUTO$"IM","S","N")+"</FlagUnsolved>"+this.w_LINEFEED
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<BankDescription>"+alltrim(this.convertXML(nvl(_Curs_ZOOMPART.BANAPP,space(50))))+"</BankDescription>"+this.w_LINEFEED
      * --- Leggo la descrizione associata al codice pagamento
      this.w_CODPAG = nvl(_Curs_ZOOMPART.MODPAG,space(50))
      * --- Read from MOD_PAGA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MPDESCRI"+;
          " from "+i_cTable+" MOD_PAGA where ";
              +"MPCODICE = "+cp_ToStrODBC(this.w_CODPAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MPDESCRI;
          from (i_cTable) where;
              MPCODICE = this.w_CODPAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESCRPAG = NVL(cp_ToDate(_read_.MPDESCRI),cp_NullValue(_read_.MPDESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<PaymentDescription>"+alltrim(this.convertXML(this.w_DESCRPAG))+"</PaymentDescription>"+this.w_LINEFEED
      * --- WRITE ON XML
      FWRITE(this.w_HANDLEFILE,this.w_XMLSTR)
      this.w_XMLSTR = ""
      if g_APPLICATION = "ADHOC REVOLUTION" 
        this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<GroupedPayment>"+alltrim(this.convertXML(nvl(_Curs_ZOOMPART.PAGRAG,space(50))))+"</GroupedPayment>"+this.w_LINEFEED
      else
        this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<GroupedPayment>"+alltrim(this.convertXML(nvl(_Curs_ZOOMPART.MODPAG,space(50))))+"</GroupedPayment>"+this.w_LINEFEED
      endif
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<BusinessUnit>"+alltrim(this.convertXML(nvl(_Curs_ZOOMPART.CODBUN,space(5))))+"</BusinessUnit>"+this.w_LINEFEED
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<Currency>"+alltrim(this.convertXML(nvl(_Curs_ZOOMPART.CODVAL,space(3))))+"</Currency>"+this.w_LINEFEED
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<DebitAmount>"+alltrim(this.convertXML(strtran(str(nvl(_Curs_ZOOMPART.IMPDAR,0),18,2),",",".")))+"</DebitAmount>"+this.w_LINEFEED
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<CreditAmount>"+alltrim(this.convertXML(strtran(str(nvl(_Curs_ZOOMPART.IMPAVE,0),18,2),",",".")))+"</CreditAmount>"+this.w_LINEFEED
      * --- -----------------------------
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<RowOrder>"+alltrim(str(recno()))+"</RowOrder>"+this.w_LINEFEED
      if g_APPLICATION = "ADHOC REVOLUTION" 
        * --- Codice di raggruppamento per discriminare visualizzazione Parte Aperta da non Saldate
        this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<DueReference>"+alltrim(this.convertXML(nvl(_Curs_ZOOMPART.CODRAG,space(60))))+"</DueReference>"+this.w_LINEFEED
      endif
      * --- Riferimento al seriale del documento
      this.w_XMLSTR = this.w_XMLSTR +space(12)+ "<DocumentReference>"+alltrim(iif(not empty(nvl(_Curs_ZOOMPART.PNRIFDOC,"")),"G","")+this.convertXML(left(nvl(_Curs_ZOOMPART.PNRIFDOC,"")+Space(50),50)))+"</DocumentReference>"+this.w_LINEFEED
      * --- WRITE ON XML
      FWRITE(this.w_HANDLEFILE,this.w_XMLSTR)
      FWRITE(this.w_HANDLEFILE,"     </Row>"+this.w_LINEFEED)
      * --- Continuo ciclo
        select _Curs_ZOOMPART
        continue
      enddo
      use
    endif
    * --- Chiudo TAG <Banks>
    FWRITE(this.w_HANDLEFILE,"   </Rows>"+this.w_LINEFEED)
    FWRITE(this.w_HANDLEFILE,"</Duedates>"+this.w_LINEFEED)
    * --- Chiudo il file
    * --- Aggiungere controllo sulla chiusra, per vedere se � andato tutto a buon fine?
    if not (FCLOSE(this.w_HANDLEFILE))
      * --- Elimino il record precedentemente inserito
      do case
        case g_IZCP="S"
          * --- Delete from ZELABORAZ
          i_nConn=i_TableProp[this.ZELABORAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ZELABORAZ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"DVCODELA = "+cp_ToStrODBC(this.w_PROGRESS);
                   )
          else
            delete from (i_cTable) where;
                  DVCODELA = this.w_PROGRESS;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        case g_IZCP="A"
          if FILE(this.w_FILEXML)
            DELETE FILE (this.w_FILEXML)
          endif
      endcase
      ah_ErrorMsg("Errore in chiusura del file %1%0Operazione sospesa","!","", this.w_ABSFILENAME)
    else
      ah_ErrorMsg("File generato con successo","i","")
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea file XML descrittore (nel caso stand-alone)
    this.w_FILEXML = LRTrim(this.w_ABSFILENAME)+".xml"
    this.w_NHF = FCreate(this.w_FILEXML)
    if this.w_NHF>=0
      this.w_TmPC = '<?xml version="1.0" encoding="iso-8859-1"?>'
      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      this.w_TmPC = '<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      this.w_TmPC = "  <Name>" + this.convertXML(LRTrim(this.w_ABSFILENAME)) + "</Name>"
      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      this.w_TmPC = "  <Description>" + this.convertXML(LRTrim(this.w_DESCELAB)) + "</Description>"
      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      this.w_TmPC = "  <RemoveFileAfterUpload>S</RemoveFileAfterUpload>"
      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      this.w_TmPC = "  <Reserved>N</Reserved>"
      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      this.w_TmPC = "  <FileType>DUEDATES</FileType>"
      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      this.w_TmPC = "</Document>"
      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      this.w_TMPN = FClose(this.w_NHF)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='ZOOMPART'
    this.cWorkTables[2]='ZPARAMGEST'
    this.cWorkTables[3]='CONTROPA'
    this.cWorkTables[4]='ZELABORAZ'
    this.cWorkTables[5]='MOD_PAGA'
    this.cWorkTables[6]='CONF_INT'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_ZOOMPART')
      use in _Curs_ZOOMPART
    endif
    if used('_Curs_ZOOMPART')
      use in _Curs_ZOOMPART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gste1bec
  * ===============================================================================
    * convertXML() - Funzione che controlla caratteri non graditi a XML
    *
    FUNCTION convertXML
    PARAMETERS sXml
    sXml = STRTRAN(sXml,"&","&amp;")
    sXml = STRTRAN(sXml,"'","&apos;")
    sXml = STRTRAN(sXml,">","&gt;")
    sXml = STRTRAN(sXml,"<","&lt;")
    sXml = STRTRAN(sXml,CHR(34),"&quot;")
    sXml = STRTRAN(sXml,CHR(13)+CHR(10),"{nbsp;}")
    sXml = STRTRAN(sXml,CHR(1),"")
    sXml = STRTRAN(sXml,CHR(2),"")
    sXml = STRTRAN(sXml,CHR(3),"")
    sXml = STRTRAN(sXml,CHR(4),"")
    sXml = STRTRAN(sXml,CHR(5),"")
    sXml = STRTRAN(sXml,CHR(6),"")
    sXml = STRTRAN(sXml,CHR(7),"")
    sXml = STRTRAN(sXml,CHR(8),"")
    sXml = STRTRAN(sXml,CHR(11),"")
    sXml = STRTRAN(sXml,CHR(12),"")
    sXml = STRTRAN(sXml,CHR(14),"")
    sXml = STRTRAN(sXml,CHR(15),"")
    sXml = STRTRAN(sXml,CHR(16),"")
    sXml = STRTRAN(sXml,CHR(17),"")
    sXml = STRTRAN(sXml,CHR(18),"")
    sXml = STRTRAN(sXml,CHR(19),"")
    sXml = STRTRAN(sXml,CHR(20),"")
    sXml = STRTRAN(sXml,CHR(21),"")
    sXml = STRTRAN(sXml,CHR(22),"")
    sXml = STRTRAN(sXml,CHR(23),"")
    sXml = STRTRAN(sXml,CHR(24),"")
    sXml = STRTRAN(sXml,CHR(25),"")
    sXml = STRTRAN(sXml,CHR(26),"")
    sXml = STRTRAN(sXml,CHR(27),"")
    sXml = STRTRAN(sXml,CHR(28),"")
    sXml = STRTRAN(sXml,CHR(29),"")
    sXml = STRTRAN(sXml,CHR(30),"")
    sXml = STRTRAN(sXml,CHR(31),"")
    sXml = STRTRAN(sXml,CHR(127),"")
    sXml = STRTRAN(sXml,CHR(129),"")
    sXml = STRTRAN(sXml,CHR(141),"")
    sXml = STRTRAN(sXml,CHR(143),"")
    sXml = STRTRAN(sXml,CHR(144),"")
    sXml = STRTRAN(sXml,CHR(157),"")
    RETURN sXml 
  
    * ===============================================================================
    * AH_ExistFolder() - Funzione che verifica l'esistenza di una cartella
    *
    Function AH_ExistFolder(pDirectory,pDirName)
    Private n_File, sTemp,a_Dir,n_Dir,sDirName,lDirectory,lDirName,i
    * check parametri
    pDirectory=alltrim(pDirectory)
    if vartype(pDirName)<>'C'
       pDirectory=iif(right(pDirectory,1)="\", left(pDirectory,len(pDirectory)-1), pDirectory)
       lDirectory=substr(pDirectory,1,rat('\',pDirectory))
       lDirName=substr(pDirectory,rat('\',pDirectory)+1)
    else
       lDirectory=pDirectory
       lDirName=pDirName
    endif
    *
    n_File=0
    n_Dir = adir(a_Dir,lDirectory+iif(right(lDirectory,1)='\','','\')+"*","D")
    for i=1 to n_Dir
       sDirName = upper(alltrim(a_Dir(i,1)))      && ADIR ritorna senza \ finale
       if !((sDirName == ".") .or. (sDirName == "..")) and sDirName==alltrim(upper(LDirName))
         n_File=n_File+1
       endif
    next
    Return (n_File>0)
    
    * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=alltrim(pDirectory)
    pDirectory=iif(right(pDirectory,1)="\", left(pDirectory,len(pDirectory)-1), pDirectory)
    *
    If Not DIRECTORY(pDirectory)
      MD (pDirectory)
    Endif
    *
    if not(Empty(lOldErr))
      on error (lOldErr)
    else
      on error
    endif   
    * 
    if not(lRet)
      = ah_ErrorMsg("Impossibile creare la cartella %1",'stop','', pDirectory )
    endif
    * Risultato
    Return (lRet)
   endfunc 
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
