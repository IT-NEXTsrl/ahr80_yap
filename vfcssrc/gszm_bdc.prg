* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bdc                                                        *
*              Menu contestuale documenti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_13]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-06                                                      *
* Last revis.: 2012-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFUNZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bdc",oParentObject,m.pFUNZ)
return(i_retval)

define class tgszm_bdc as StdBatch
  * --- Local variables
  pFUNZ = space(1)
  w_MVSERIAL = space(10)
  w_NUMRIF = 0
  w_TIPOIN = space(5)
  w_FLEVAS = space(1)
  w_FLVEAC = space(1)
  w_SPUNTA = space(1)
  w_CATDOC = space(2)
  w_CLIFOR = space(15)
  w_DATAIN = ctod("  /  /  ")
  w_DATA1 = ctod("  /  /  ")
  w_DATAFI = ctod("  /  /  ")
  w_DATA2 = ctod("  /  /  ")
  w_NUMINI = 0
  w_NUMFIN = 0
  w_serie1 = space(2)
  w_serie2 = space(2)
  w_CODAGE = space(5)
  w_LINGUA = space(3)
  w_CATEGO = space(2)
  w_CODZON = space(3)
  w_CODVET = space(5)
  w_CATCOM = space(3)
  w_CODPAG = space(5)
  w_NOSBAN = space(5)
  w_CODDES = space(5)
  w_STAMPA = 0
  w_MVRIFFAD = space(10)
  w_MVRIFODL = space(10)
  w_CODESE = space(2)
  w_WIPSCA = space(1)
  w_NETSCA = space(1)
  w_TIPCON = space(1)
  w_CODART = space(20)
  w_SERDOC = space(10)
  w_DT = space(35)
  w_w_HASEVENT = .f.
  w_w_MVFLCONT = space(1)
  w_w_MVRIFACC = space(10)
  w_w_MVFLINTR = space(1)
  w_w_MVFLBLOC = space(1)
  w_w_MVFLVEAC = space(1)
  w_w_MVMOVCOM = space(10)
  w_w_MVSERDDT = space(10)
  w_w_MVCLADOC = space(2)
  w_w_MVSERIAL = space(10)
  w_w_MVFLGIOM = space(1)
  w_w_MVRIFDCO = space(10)
  w_w_MVGENPRO = space(1)
  w_w_MVNUMEST = 0
  w_w_MVNUMDOC = 0
  w_w_MVANNPRO = space(4)
  w_w_MVRIFODL = space(15)
  w_w_MVFLOFFE = space(1)
  w_w_MVPRD = space(2)
  w_w_FLGEFA = space(1)
  w_w_FLEDIT = space(1)
  w_w_MVFLPROV = space(1)
  w_w_MVSERRIF = space(10)
  w_w_MVROWRIF = 0
  w_w_MVNUMRIF = 0
  w_w_MVQTAIMP = 0
  w_w_MVQTAIM1 = 0
  w_w_MVFLARIF = space(1)
  w_w_MOLTIP = 0
  w_w_MOLTI3 = 0
  w_w_MVTIPRIG = space(1)
  w_w_UNMIS1 = space(3)
  w_w_MVUNIMIS = space(3)
  w_w_MVFLELGM = space(1)
  w_w_MVDATREG = ctod("  /  /  ")
  w_w_MVCODMAG = space(5)
  w_w_MVCODMAT = space(5)
  w_w_MVCODESE = space(4)
  w_w_FLBLEV = space(1)
  w_w_MVQTAEVA = 0
  w_w_MVIMPEVA = 0
  w_w_ASSCES = space(1)
  w_RIFPRA = space(1)
  w_RIFOGG = space(1)
  w_RIFVAL = space(1)
  w_RIFDAT = space(1)
  w_RIFSUP = space(1)
  w_RIFRIGHE = space(1)
  w_FLGNOT = space(1)
  w_RIFGU = space(1)
  w_RIFMINMAX = space(1)
  w_RIFSCOFIN = space(1)
  w_RIFDATDIR = space(1)
  w_RIFDATONO = space(1)
  w_RIFDATTEM = space(1)
  w_RIFDATSPA = space(1)
  w_RIFQTA = space(1)
  w_RIFRESTIT = space(1)
  w_RIFPARTI = space(1)
  w_RIFNOIMPO = space(1)
  w_RIFCOEDIF = space(1)
  w_RIFIMP = space(1)
  w_RIFDATGEN = space(1)
  w_RIFRAGFAS = space(1)
  w_PRTIPCAU = space(1)
  w_OBJECT = .NULL.
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVNUMEST = 0
  w_MVALFEST = space(10)
  w_MVDATEST = ctod("  /  /  ")
  * --- WorkFile variables
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  PAR_PARC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine men� contestuale Documenti
    * --- Assegno i valori alle variabili
    this.w_MVSERIAL = g_oMenu.getbyindexkeyvalue(1)
    if Type("g_oMenu.ocontained.ccursor")="C" And Used( g_oMenu.ocontained.ccursor )
      L_CursName=g_oMenu.ocontained.ccursor
      this.w_NUMRIF = iif(Type("&L_CursName..MVNUMRIF")="N",&L_CursName..MVNUMRIF,-20)
    else
      this.w_NUMRIF = -20
    endif
    if NOT EMPTY(this.w_MVSERIAL)
      do case
        case this.pFUNZ = "1" AND (this.w_NUMRIF=-20 OR this.w_NUMRIF=-70)
          * --- Stampa Documenti 
          * --- Ristampa documenti di produzione
          * --- Dichiariamo e inizializziamo le var. che servono per i controlli di GSVE_BNR
          if IsAlt()
            this.w_w_MVSERIAL = this.w_MVSERIAL
            this.w_w_MVSERRIF = ""
            this.w_w_FLEDIT = ""
            this.w_w_FLGEFA = ""
            this.w_w_HASEVENT = .F.
            this.w_w_MVROWRIF = 0
            this.w_w_MVNUMRIF = 0
            this.w_w_MVQTAIMP = 0
            this.w_w_MVQTAIM1 = 0
            this.w_w_MVQTAEVA = 0
            this.w_w_MVIMPEVA = 0
            this.w_w_MVFLARIF = ""
            this.w_w_MOLTIP = 0
            this.w_w_MOLTI3 = 0
            this.w_w_MVTIPRIG = ""
            this.w_w_UNMIS1 = ""
            this.w_w_MVUNIMIS = ""
            this.w_w_MVFLELGM = ""
            this.w_w_MVCODMAG = ""
            this.w_w_MVCODMAT = ""
            this.w_w_FLBLEV = ""
            this.w_w_ASSCES = ""
            this.w_FLGNOT = "S"
            GSVE_BSA(this,"L")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_SPUNTA = "N"
          this.w_CODESE = "    "
          this.w_FLEVAS = " "
          this.w_CLIFOR = SPACE(15)
          this.w_DATAIN = i_INIDAT
          this.w_DATA1 = i_INIDAT
          this.w_DATAFI = i_FINDAT
          this.w_DATA2 = i_FINDAT
          this.w_NUMINI = 1
          this.w_NUMFIN = 999999999999999
          this.w_serie1 = ""
          this.w_serie2 = ""
          this.w_CODAGE = space(5)
          this.w_LINGUA = SPACE(3)
          this.w_CATEGO = space(2)
          this.w_CODZON = space(3)
          this.w_CODVET = space(5)
          this.w_CATCOM = space(3)
          this.w_CODPAG = space(5)
          this.w_NOSBAN = space(5)
          this.w_CODDES = space(5)
          this.w_STAMPA = 0
          this.w_MVRIFFAD = SPACE(10)
          this.w_MVRIFODL = SPACE(10)
          * --- Leggo MVTIPDOC e MVTIPCON
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVTIPDOC,MVTIPCON,MVDATDOC,MVNUMDOC,MVALFDOC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVTIPDOC,MVTIPCON,MVDATDOC,MVNUMDOC,MVALFDOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_MVSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPOIN = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            this.w_TIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
            this.w_DATAIN = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            this.w_DATAFI = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            this.w_DATA1 = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            this.w_DATA2 = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            this.w_NUMINI = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_NUMFIN = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_serie1 = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_serie2 = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_SERDOC = this.w_MVSERIAL
          if EMPTY(this.w_TIPOIN)
            ah_ErrorMsg("Causale documento inesistente",,"")
          else
            * --- Read from PAR_PARC
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_PARC_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_PARC_idx,2],.t.,this.PAR_PARC_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PRRIFPRA,PRRIFOGG,PRRIFVAL,PRRIFDAT,PRRIFSUP,PRRIFRIG,PRRIFGUF,PRRIFMIA,PRRIFSCF,PRDATDIR,PRDATONO,PRDATTEM,PRDATSPA,PRRIFQTA,PRNOIMPO,PRCOEDIF,PRRIFIMP,PRRESTIT,PR_PARTI,PRDATGEN,PRTIPCAU,PRRIFFAS"+;
                " from "+i_cTable+" PAR_PARC where ";
                    +"PRCODAZI = "+cp_ToStrODBC(i_codazi);
                    +" and PRCAUPRE = "+cp_ToStrODBC(this.w_TIPOIN);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PRRIFPRA,PRRIFOGG,PRRIFVAL,PRRIFDAT,PRRIFSUP,PRRIFRIG,PRRIFGUF,PRRIFMIA,PRRIFSCF,PRDATDIR,PRDATONO,PRDATTEM,PRDATSPA,PRRIFQTA,PRNOIMPO,PRCOEDIF,PRRIFIMP,PRRESTIT,PR_PARTI,PRDATGEN,PRTIPCAU,PRRIFFAS;
                from (i_cTable) where;
                    PRCODAZI = i_codazi;
                    and PRCAUPRE = this.w_TIPOIN;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_RIFPRA = NVL(cp_ToDate(_read_.PRRIFPRA),cp_NullValue(_read_.PRRIFPRA))
              this.w_RIFOGG = NVL(cp_ToDate(_read_.PRRIFOGG),cp_NullValue(_read_.PRRIFOGG))
              this.w_RIFVAL = NVL(cp_ToDate(_read_.PRRIFVAL),cp_NullValue(_read_.PRRIFVAL))
              this.w_RIFDAT = NVL(cp_ToDate(_read_.PRRIFDAT),cp_NullValue(_read_.PRRIFDAT))
              this.w_RIFSUP = NVL(cp_ToDate(_read_.PRRIFSUP),cp_NullValue(_read_.PRRIFSUP))
              this.w_RIFRIGHE = NVL(cp_ToDate(_read_.PRRIFRIG),cp_NullValue(_read_.PRRIFRIG))
              this.w_RIFGU = NVL(cp_ToDate(_read_.PRRIFGUF),cp_NullValue(_read_.PRRIFGUF))
              this.w_RIFMINMAX = NVL(cp_ToDate(_read_.PRRIFMIA),cp_NullValue(_read_.PRRIFMIA))
              this.w_RIFSCOFIN = NVL(cp_ToDate(_read_.PRRIFSCF),cp_NullValue(_read_.PRRIFSCF))
              this.w_RIFDATDIR = NVL(cp_ToDate(_read_.PRDATDIR),cp_NullValue(_read_.PRDATDIR))
              this.w_RIFDATONO = NVL(cp_ToDate(_read_.PRDATONO),cp_NullValue(_read_.PRDATONO))
              this.w_RIFDATTEM = NVL(cp_ToDate(_read_.PRDATTEM),cp_NullValue(_read_.PRDATTEM))
              this.w_RIFDATSPA = NVL(cp_ToDate(_read_.PRDATSPA),cp_NullValue(_read_.PRDATSPA))
              this.w_RIFQTA = NVL(cp_ToDate(_read_.PRRIFQTA),cp_NullValue(_read_.PRRIFQTA))
              this.w_RIFNOIMPO = NVL(cp_ToDate(_read_.PRNOIMPO),cp_NullValue(_read_.PRNOIMPO))
              this.w_RIFCOEDIF = NVL(cp_ToDate(_read_.PRCOEDIF),cp_NullValue(_read_.PRCOEDIF))
              this.w_RIFIMP = NVL(cp_ToDate(_read_.PRRIFIMP),cp_NullValue(_read_.PRRIFIMP))
              this.w_RIFRESTIT = NVL(cp_ToDate(_read_.PRRESTIT),cp_NullValue(_read_.PRRESTIT))
              this.w_RIFPARTI = NVL(cp_ToDate(_read_.PR_PARTI),cp_NullValue(_read_.PR_PARTI))
              this.w_RIFDATGEN = NVL(cp_ToDate(_read_.PRDATGEN),cp_NullValue(_read_.PRDATGEN))
              this.w_PRTIPCAU = NVL(cp_ToDate(_read_.PRTIPCAU),cp_NullValue(_read_.PRTIPCAU))
              this.w_RIFRAGFAS = NVL(cp_ToDate(_read_.PRRIFFAS),cp_NullValue(_read_.PRRIFFAS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from TIP_DOCU
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TDFLVEAC,TDCATDOC,TDPRGSTA"+;
                " from "+i_cTable+" TIP_DOCU where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPOIN);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TDFLVEAC,TDCATDOC,TDPRGSTA;
                from (i_cTable) where;
                    TDTIPDOC = this.w_TIPOIN;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
              this.w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
              this.w_STAMPA = NVL(cp_ToDate(_read_.TDPRGSTA),cp_NullValue(_read_.TDPRGSTA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            GSVE_BRD(this,"F")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        case this.pFUNZ = "2" AND (this.w_NUMRIF=-20 OR this.w_NUMRIF=-70)
          * --- Tracciabilit� Ordini
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVNUMDOC,MVALFDOC,MVDATDOC,MVNUMEST,MVALFEST,MVDATEST"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVNUMDOC,MVALFDOC,MVDATDOC,MVNUMEST,MVALFEST,MVDATEST;
              from (i_cTable) where;
                  MVSERIAL = this.w_MVSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            this.w_MVNUMEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
            this.w_MVALFEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
            this.w_MVDATEST = NVL(cp_ToDate(_read_.MVDATEST),cp_NullValue(_read_.MVDATEST))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_OBJECT = GSOR_STD()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_OBJECT.bSec1)
            i_retcode = 'stop'
            return
          endif
          this.w_OBJECT.w_MVSERIAL = this.w_MVSERIAL
          this.w_OBJECT.w_NUMDO1 = this.w_MVNUMDOC
          this.w_OBJECT.w_ALFDO1 = this.w_MVALFDOC
          this.w_OBJECT.w_DATDO1 = this.w_MVDATDOC
          this.w_OBJECT.w_NUMRI1 = this.w_MVNUMEST
          this.w_OBJECT.w_ALFRI1 = this.w_MVALFEST
          this.w_OBJECT.w_DATRI1 = this.w_MVDATEST
          this.w_OBJECT.NotifyEvent("EseguoZoom")     
        case this.pFUNZ = "3"
          GSAR_BZM(this,this.w_MVSERIAL, this.w_NUMRIF)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
    endif
  endproc


  proc Init(oParentObject,pFUNZ)
    this.pFUNZ=pFUNZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='PAR_PARC'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFUNZ"
endproc
