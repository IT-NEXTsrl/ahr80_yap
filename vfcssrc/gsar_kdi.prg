* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kdi                                                        *
*              Elenco dichiarazioni                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-10-31                                                      *
* Last revis.: 2009-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kdi",oParentObject))

* --- Class definition
define class tgsar_kdi as StdForm
  Top    = 7
  Left   = 5

  * --- Standard Properties
  Width  = 588
  Height = 330
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-01-26"
  HelpContextID=82409321
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsar_kdi"
  cComment = "Elenco dichiarazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DDRFDICH = space(10)
  w_ANNDIC = space(4)
  w_DATDIC = ctod('  /  /  ')
  w_NUMDIC = 0
  w_DDTIPCON = space(1)
  w_DDCODICE = space(15)
  w_SERDIC = 0
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kdiPag1","gsar_kdi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DDRFDICH=space(10)
      .w_ANNDIC=space(4)
      .w_DATDIC=ctod("  /  /  ")
      .w_NUMDIC=0
      .w_DDTIPCON=space(1)
      .w_DDCODICE=space(15)
      .w_SERDIC=0
      .w_DDRFDICH=oParentObject.w_DDRFDICH
      .w_ANNDIC=oParentObject.w_ANNDIC
      .w_DATDIC=oParentObject.w_DATDIC
      .w_NUMDIC=oParentObject.w_NUMDIC
      .w_SERDIC=oParentObject.w_SERDIC
      .oPgFrm.Page1.oPag.Zoom.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .w_DDRFDICH = .w_Zoom.getVar('DISERIAL')
        .w_ANNDIC = .w_Zoom.getVar('DI__ANNO')
        .w_DATDIC = CP_TODATE(.w_Zoom.getVar('DIDATDIC'))
        .w_NUMDIC = .w_Zoom.getVar('DINUMDOC')
        .w_DDTIPCON = IIF(TYPE('.oParentObject .w_DDTIPCON')='C', .oParentObject .w_DDTIPCON, .oParentObject.oParentObject .w_DDTIPCON)
        .w_DDCODICE = IIF(TYPE('.oParentObject .w_DDCODICE')='C', .oParentObject .w_DDCODICE, .oParentObject.oParentObject .w_DDCODICE)
        .w_SERDIC = .w_Zoom.getVar('DISERDOC')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DDRFDICH=.w_DDRFDICH
      .oParentObject.w_ANNDIC=.w_ANNDIC
      .oParentObject.w_DATDIC=.w_DATDIC
      .oParentObject.w_NUMDIC=.w_NUMDIC
      .oParentObject.w_SERDIC=.w_SERDIC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
            .w_DDRFDICH = .w_Zoom.getVar('DISERIAL')
            .w_ANNDIC = .w_Zoom.getVar('DI__ANNO')
            .w_DATDIC = CP_TODATE(.w_Zoom.getVar('DIDATDIC'))
            .w_NUMDIC = .w_Zoom.getVar('DINUMDOC')
        .DoRTCalc(5,6,.t.)
            .w_SERDIC = .w_Zoom.getVar('DISERDOC')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_2.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_kdiPag1 as StdContainer
  Width  = 584
  height = 330
  stdWidth  = 584
  stdheight = 330
  resizeXpos=265
  resizeYpos=162
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Zoom as cp_zoombox with uid="JUIMQIRRHJ",left=1, top=2, width=586,height=325,;
    caption='ZOOM',;
   bGlobalFont=.t.,;
    bOptions=.t.,bAdvOptions=.t.,cTable="DIC_INTE",bQueryOnLoad=.t.,cMenuFile="",bRetriveAllRows=.f.,bReadOnly=.t.,cZoomOnZoom="",cZoomFile="GSAR_KDI",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 77017706


  add object oObj_1_2 as cp_runprogram with uid="EQSJMQFHKZ",left=3, top=332, width=204,height=22,;
    caption='GSVE_BOO',;
   bGlobalFont=.t.,;
    prg="GSVE_BOO",;
    cEvent = "w_zoom selected",;
    nPag=1;
    , ToolTipText = "Chiude la maschera al doppio click sullo zoom";
    , HelpContextID = 55656629
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kdi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
