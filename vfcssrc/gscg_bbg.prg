* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bbg                                                        *
*              Stampa bilancio strutturato                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-12-10                                                      *
* Last revis.: 2017-11-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bbg",oParentObject,m.pTipo)
return(i_retval)

define class tgscg_bbg as StdBatch
  * --- Local variables
  pTipo = 0
  w_parto = space(12)
  w_capo = space(5)
  w_provo = space(2)
  w_codo = space(16)
  w_loco = space(30)
  w_indi = space(35)
  w_Capsoc = space(30)
  w_Tmpc = space(40)
  w_ESE1 = space(4)
  w_ESE2 = space(4)
  w_INSERISCI = .f.
  w_ORIGINE = space(1)
  w_DATPREC = ctod("  /  /  ")
  w_DATPINI = ctod("  /  /  ")
  w_GIORNI = 0
  w_DATA2BIS = ctod("  /  /  ")
  w_DATA1BIS = ctod("  /  /  ")
  w_CPROWORD1 = 0
  w_PAR1 = space(10)
  w_PAR2 = space(10)
  w_APERTURA = .f.
  w_OLDDIVISA = space(3)
  w_ESEDIVIDE = 0
  w_IMPORT41 = 0
  w_INIESE = ctod("  /  /  ")
  w_VALESE = space(3)
  w_DATSTA1 = ctod("  /  /  ")
  w_IMPORT31 = 0
  w_GEST_BUN = .f.
  w_MESS = space(10)
  w_VALARRO = 0
  w_RIGA = 0
  w_SecondaVolta = .f.
  PROVVI = space(1)
  w_CAMMASK = 0
  INI_ESE = ctod("  /  /  ")
  w_CAMBIO = 0
  w_DATVAL = ctod("  /  /  ")
  PRE_ESE = space(4)
  w_SEGNO = space(1)
  ATT_DIV = space(3)
  PRE_DIV = space(3)
  w_DIVIDE = 0
  w_TOTALE = 0
  w_ESEORA = space(4)
  w_SEZBIL = space(1)
  w_MOLTI = 0
  w_UTENTE = 0
  w_DATESE = ctod("  /  /  ")
  w_DIVIDEPRE = 0
  w_IMPARRO = 0
  w_MOLTIPRE = 0
  w_DIFFUTILE = 0
  w_CPROWNUM = 0
  w_RIGA11 = 0
  w_SEGNO11 = space(1)
  w_TOTALENA = 0
  w_ULTPAG = 0
  w_RicTotali = .f.
  w_DIFFATT = 0
  w_DIFFPAS = 0
  w_TIPCON_ARR = space(1)
  w_ANCODICE_ARR = space(15)
  w_SLSERIAL = space(10)
  w_SERSAL = space(10)
  * --- WorkFile variables
  VALUTE_idx=0
  RIGTOTAL_idx=0
  ESERCIZI_idx=0
  TMPMOVIMAST_idx=0
  AZIENDA_idx=0
  SEDIAZIE_idx=0
  TIR_MAST_idx=0
  BIL_MAST_idx=0
  BIL_DETT_idx=0
  TMP_PART_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- STAMPA BILANCIO STRUTTURATO (da GSCG_SBG e GSCG_SBI)
    * --- Parametro utilizzato per poter distinguere il Bilancio Strutturato da quello del Libro Inventario
    * --- Se 1 Bilancio Strutturato, 0 Bilancio libro inventario 2 calcolo x confronto
    this.w_INSERISCI = .f.
    if g_APPLICATION="ADHOC REVOLUTION"
      this.oParentObject.w_BILANCIO = "BILANCOGE"
    endif
    * --- Prepara le variabili per Corporate Portal Zucchetti
    if g_IZCP$"SA"
      * --- Propone intestatario di tipo GRUPPO
      g_ZCPTINTEST = "G"
      g_ZCPCINTEST = " "
      g_ZCPALLENAME = "ZB_" + alltrim(this.oParentObject.w_BILANCIO) + "_" + IIF(this.oParentObject.w_totdat="T", alltrim(this.oParentObject.w_Ese), dtoc(this.oParentObject.w_Data1)+"_"+dtoc(this.oParentObject.w_Data2)) + "_"+alltrim(i_CODAZI) + IIF(g_PERBUN="T" and Not(Empty(this.oParentObject.w_CODBUN)), "_"+alltrim(this.oParentObject.w_CODBUN), "")+".PDF"
      * --- Determina descrizione
      if g_APPLICATION="ADHOC REVOLUTION"
        * --- Read from TIR_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIR_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIR_MAST_idx,2],.t.,this.TIR_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRDESCRI"+;
            " from "+i_cTable+" TIR_MAST where ";
                +"TRCODICE = "+cp_ToStrODBC(this.oParentObject.w_BILANCIO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRDESCRI;
            from (i_cTable) where;
                TRCODICE = this.oParentObject.w_BILANCIO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_Tmpc = NVL(cp_ToDate(_read_.TRDESCRI),cp_NullValue(_read_.TRDESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from TIR_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIR_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIR_MAST_idx,2],.t.,this.TIR_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRDESCRI"+;
            " from "+i_cTable+" TIR_MAST where ";
                +"TR__TIPO = "+cp_ToStrODBC(this.oParentObject.w_TIPO);
                +" and TRCODICE = "+cp_ToStrODBC(this.oParentObject.w_BILANCIO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRDESCRI;
            from (i_cTable) where;
                TR__TIPO = this.oParentObject.w_TIPO;
                and TRCODICE = this.oParentObject.w_BILANCIO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_Tmpc = NVL(cp_ToDate(_read_.TRDESCRI),cp_NullValue(_read_.TRDESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      g_ZCPALLETITLE = alltrim(this.w_Tmpc) + " " + IIF(this.oParentObject.w_totdat="T", "Es. "+alltrim(this.oParentObject.w_Ese), "dal "+dtoc(this.oParentObject.w_Data1)+" al "+dtoc(this.oParentObject.w_Data2)) + " - Az.: "+alltrim(i_CODAZI) + IIF(g_PERBUN="T" and Not(Empty(this.oParentObject.w_CODBUN)), " - B.U.: "+alltrim(this.oParentObject.w_CODBUN), "")
    endif
    * --- leggo la valuta e la data inizio esercizio di stampa
    this.w_VALESE = this.oParentObject.w_VALAPP
    this.w_INIESE = this.oParentObject.w_DATINIESE
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_CONFRONTO="S" and empty(this.PRE_ESE)
      ah_errormsg("L'esercizio prescelto non ha esercizi precendenti definiti",48)
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Se true vengono gestite le business unit (o gruppo o singola) se false no
    *     Lo utilizzo per determinare se il saldo iniziale letto � valido (false) o se tratto con Business Unit (true)
    this.w_GEST_BUN = (not EMPTY(this.oParentObject.w_SUPERBU+this.oParentObject.w_CODBUN))
    this.w_DATSTA1 = this.oParentObject.w_DATA2
    * --- Alcune Query (Quelle utilizzate anche da CALCSAL) utilizzano questo parametro per i provvisori
    *     (metto il riferimento intero altrimenti Painter mi prende la variabile locale)
    this.PROVVI = This.oParentObject.w_PROVVI
    ah_msg("Fase 1: costruzione struttura di bilancio")
    * --- Due Query una per recuperare i conti direttamente presenti nelle voci e una
    *     per recuperare i Mastri e conti di cui sono padri - No posso fare una Union da Visual Query
    *     perch� SQL Server non accetta union di campo Memo (Note legate alla voci)
    *     Ho 3 Query una per i conti una per i mastri delle voci e una per totali e descrizioni
    if g_APPLICATION="ADHOC REVOLUTION"
      vq_exec("query\STRU_BIL_R",this,"struttura")
      vq_exec("query\STRU1BIL_R",this,"struttura1")
      vq_exec("query\STRU2BIL_R",this,"struttura2")
    else
      vq_exec("query\stru_bil",this,"struttura")
      vq_exec("query\stru1bil",this,"struttura1")
      vq_exec("query\stru2bil",this,"struttura2")
    endif
    select * from Struttura ; 
 Union ; 
 select * from Struttura1 ; 
 Union ; 
 select * from Struttura2 into cursor Struttura
    if used("Struttura1")
      select Struttura1
      use
    endif
    if used("struttura2")
      select struttura2
      use
    endif
    * --- --Check chiusura infrannuale
    L_ESCINFR=this.oParentObject.w_INFRAAN
    * --- --flag Saldi I dati estratti verranno presi dall'archivio saldi
    L_ESSALDI=this.oParentObject.w_ESSALDI
    * --- Lancio il logo assocciato all'azienda
    L_LOGO=GETBMP("BIL"+I_CODAZI)
    * --- Variabili da passare al Report, Cambio, Divisa, Formato ,Descrizioni
    *     il cambio viene calcolato in modo diverso a sceonda se i_DATSYS e minore di g_DATEUR
    *     se maggiore ho l'euro e due cambi (in caso di valuta intra euroepa)
    *     q cambio che divide, L_molti cambio che moltiplica (1 se prima di g_dateur)
    L_MOLTI=iif(this.oParentObject.w_monete<>"a", 1 ,iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL))
    L_CAMVAL=iif(this.oParentObject.w_MONETE<>"a" , 1 ,GETCAM(this.w_VALESE,i_DATSYS,0))
    L_EUROPEA=IIF(this.oParentObject.w_EXTRAEUR=0,.F.,.T.)
    L_ARROT=this.oParentObject.w_ARROT
    L_ARRVOCI=this.oParentObject.w_ARRVOCI
    * --- Mi dice se la moneta � europea o meno
    this.w_DIVIDEPRE = this.w_DIVIDE
    this.w_MOLTIPRE = this.w_MOLTI
    this.w_DIVIDE = L_CAMVAL
    this.w_MOLTI = L_MOLTI
    * --- FORMATO IMPORTI
    L_decimi=this.oParentObject.w_DECIMI
    s=20*(l_decimi+2)
    * --- Per il libro inventari devo controllare se stampare la numerazione dei registri
    if this.pTipo=0
      * --- Verifico se st� lavorando su Revolution oppure Enterprise
      if g_APPLICATION="ADHOC REVOLUTION"
        * --- PER NUMERAZIONE PAGINE IN TESTATA
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZ1);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
            from (i_cTable) where;
                AZCODAZI = this.oParentObject.w_CODAZ1;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_indi = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
          this.w_loco = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
          this.w_capo = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
          this.w_provo = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
          this.w_parto = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
          this.w_codo = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        L_PAGINE=0
        L_ULTPLI=this.oParentObject.w_PRPALI
        L_PCONLI=this.oParentObject.w_INTLIN
        L_DNUMLI=this.oParentObject.w_PREFLI
        L_INDI=this.w_INDI
        L_LOCO=this.w_LOCO
        L_CAPO=this.w_CAPO
        L_PROVO=this.w_PROVO
        L_CODO=this.w_CODO
        L_PARTO=this.w_PARTO
        L_CAPSOC=SPACE(10)
      else
        * --- Leggo dati da impostare nella testata di stampa
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI,AZCAPSOC"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZ1);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI,AZCAPSOC;
            from (i_cTable) where;
                AZCODAZI = this.oParentObject.w_CODAZ1;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_indi = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
          this.w_loco = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
          this.w_capo = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
          this.w_provo = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
          this.w_parto = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
          this.w_codo = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
          this.w_Capsoc = NVL(cp_ToDate(_read_.AZCAPSOC),cp_NullValue(_read_.AZCAPSOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Verifico se nella tabella Sedi presente nell'anagrafica Dati Azienda � stata 
        *     specificata una Sede Legale.Effettuo questo controllo solo se nella numerazione
        *     registri ho specificato Numerazione Pagine Contestuale alla stampa
        if this.oParentObject.w_PCONLI="S"
          * --- Select from SEDIAZIE
          i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SEINDIRI,SE___CAP,SELOCALI,SEPROVIN  from "+i_cTable+" SEDIAZIE ";
                +" where SECODAZI="+cp_ToStrODBC(this.oParentObject.w_CODAZ1)+" and SETIPRIF='SL'";
                 ,"_Curs_SEDIAZIE")
          else
            select SEINDIRI,SE___CAP,SELOCALI,SEPROVIN from (i_cTable);
             where SECODAZI=this.oParentObject.w_CODAZ1 and SETIPRIF="SL";
              into cursor _Curs_SEDIAZIE
          endif
          if used('_Curs_SEDIAZIE')
            select _Curs_SEDIAZIE
            locate for 1=1
            do while not(eof())
            this.w_capo = _Curs_SEDIAZIE.SE___CAP
            this.w_provo = _Curs_SEDIAZIE.SEPROVIN
            this.w_loco = _Curs_SEDIAZIE.SELOCALI
            this.w_indi = _Curs_SEDIAZIE.SEINDIRI
            Exit
              select _Curs_SEDIAZIE
              continue
            enddo
            use
          endif
        endif
        * --- Stampa nel formato dell'esercizio scelto, traduce gli importi - Aggiunta una voce di differenza cambi
        * --- dati da stampare in testata
        L_PAGINE=0
        L_DNUMLI=this.oParentObject.w_DNUMLI
        L_ULTPLI=this.oParentObject.w_ULTPLI
        L_PCONLI=this.oParentObject.w_PCONLI
        L_indi=this.w_indi
        L_LOCO=this.W_LOCO
        L_CAPO=this.W_CAPO
        L_PROVO=this.W_PROVO
        L_codo=this.w_codo
        L_PARTO=this.W_PARTO
        L_CAPSOC=this.w_CAPSOC
      endif
    endif
    * --- la struttura la calcolo solo una volta
    if this.oParentObject.w_CONFRONTO="S"
      * --- Confronto Esercizio precedente
      this.w_ORIGINE = this.oParentObject.w_ESSALDI
      * --- Calcolo il cursore nell'esercizio attuale
      L_PREESE=this.oParentObject.w_esepre 
 L_ESE=this.oParentObject.w_ESE
      * --- Carica i dati su TMP_PART con il codice dell'esercizio in corso
      this.w_ESE1 = this.oParentObject.w_ESE
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Preparo Cursore per aggiornamenro Saldi
      select TRTIPDET, DVCODMAS, MCDESCRI, ANCODICE, DESCRI, MCSEZBIL, TR__INDE, TRDESDET,; 
 TR__COLO, TR_FONT, TR_FTST, NVL(TOT1,0*999999999999.9999) AS TOT1, DV_SEGNO, DV__DAVE,; 
 NVL(TOTALE,0*999999999999.9999) AS TOTALE, NVL(TMASTRI,0*999999999999.9999) AS TMASTRI,; 
 NVL(TMASTRI,0*999999999999.9999) AS TORAMASTRI, recno() as numerorec,; 
 CPROWORD1,CPROWNUM1,cproword,trseqsta,CPROWNUM,tipcon,codice from USRTMP1 into cursor eser1 NOFILTER ; 
 ORDER BY CPROWORD1,CPROWNUM1,cproword,CPROWNUM,tipcon,codice
      * --- Prima di lanciare la funzione CALCCONT per calcolare l'esercizio precedente
      *     e la valuta ed i cambi, devo verificare se ho selezionato il confronto tra bilanci.
      *     In questo caso devo vedere se risalire all'esercizio precedente se non �
      *     infraannuale oppure all'esercizio precedente a questo
      * --- Data finale esercizio precedente
      this.w_DATPREC = this.w_INIESE-1
      * --- Devo leggere la data iniziale dell'esercizio della variabile DATPREC
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESINIESE"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESFINESE = "+cp_ToStrODBC(this.w_DATPREC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESINIESE;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESFINESE = this.w_DATPREC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATPINI = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Calcolo la differenza tra la data di fine dell'esercizio di elaborazione con la
      *     data di inizio dell'esercizio precedente
      this.w_GIORNI = (this.oParentObject.w_DATFINESE-this.w_DATPINI)+1
      * --- Se la differenza � uguale a 365 oppure a 366 devo assegnare alla
      *     variabile w_INIESE il valore di w_DATPINI, altrimenti tengo il valore preesistente.
      if this.w_GIORNI=365 OR this.w_GIORNI=366
        this.w_INIESE = this.w_DATPINI
      endif
      * --- ................................................
      this.oParentObject.w_ESEPRE = CALCESPR(i_CODAZI,this.w_INIESE,.T.)
      this.pre_ese = this.oParentObject.w_ESEPRE
      L_PREESE=this.oParentObject.w_ESEPRE
      * --- Leggo le informazioni per l'esercizio precedente
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESVALNAZ,ESINIESE"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_ESEPRE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESVALNAZ,ESINIESE;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESCODESE = this.oParentObject.w_ESEPRE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_PRVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
        this.oParentObject.w_PRINIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT,VACAOVAL"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_PRVALNAZ);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT,VACAOVAL;
          from (i_cTable) where;
              VACODVAL = this.oParentObject.w_PRVALNAZ;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_PRDECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        this.oParentObject.w_PRCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_ESE = this.pre_ese
      this.oParentObject.w_VALAPP = this.oParentObject.w_PRVALNAZ
      this.oParentObject.w_CAONAZ = this.oParentObject.w_PRCAOVAL
      this.oParentObject.w_DATINIESE = this.oParentObject.w_PRINIESE
      this.w_VALESE = this.oParentObject.w_VALAPP
      this.w_INIESE = this.oParentObject.w_DATINIESE
      this.oParentObject.w_DECNAZ = this.oParentObject.w_PRDECTOT
      if this.oParentObject.w_totdat="D"
        L_DATA1=this.oParentObject.w_DATA1 
 L_DATA2=this.oParentObject.w_DATA2
        * --- se l'anno non � bisestile la conversione della 29/02 restituisce una data vuota, 
        *     percui viene inserita la prima data utile (la data1 utilizzer� {01-03} mentra la data 2 utilizzer� {28-02})
        this.w_DATA2BIS = STR(DAY(this.oParentObject.w_DATA2))+"-"+STR(MONTH(this.oParentObject.w_DATA2))+"-"+STR(YEAR(this.oParentObject.w_PRINIESE)+YEAR(this.oParentObject.w_DATA2)-YEAR(this.oParentObject.w_DATA1))
        this.w_DATA1BIS = STR(DAY(this.oParentObject.w_DATA1))+"-"+STR(MONTH(this.oParentObject.w_DATA1))+"-"+STR(YEAR(this.oParentObject.w_PRINIESE))
        if DAY(this.oParentObject.w_DATA2)=29 AND MONTH(this.oParentObject.w_DATA2)=2 AND empty (cp_CharToDate(this.w_DATA2BIS) )
          this.oParentObject.w_DATA2 = cp_CharToDate( "28-02"+"-"+STR(YEAR(this.oParentObject.w_PRINIESE)+YEAR(this.oParentObject.w_DATA2)-YEAR(this.oParentObject.w_DATA1)))
        else
          this.oParentObject.w_DATA2 = cp_CharToDate(this.w_DATA2BIS)
        endif
        if DAY(this.oParentObject.w_DATA1)=29 AND MONTH(this.oParentObject.w_DATA1)=2 AND empty (cp_CharToDate(this.w_DATA1BIS) )
          this.oParentObject.w_DATA1 = cp_CharToDate( "01-03"+"-"+STR(YEAR(this.oParentObject.w_PRINIESE)))
        else
          this.oParentObject.w_DATA1 = cp_CharToDate(this.w_DATA1BIS)
        endif
      else
        L_DATA1=this.oParentObject.w_DATA1 
 L_DATA2=this.oParentObject.w_DATA2
      endif
      * --- applico origine specifica scelta per esercizio precedente
      if g_APPLICATION<>"ADHOC REVOLUTION"
        this.w_ORIGINE = this.oParentObject.w_ESSALDI
      else
        this.w_ORIGINE = this.oParentObject.w_ORIPREC
      endif
      * --- Se la stampa non � per movimenti confermati avr� bisogno di leggere
      *     anche i movimenti dell'esercizio precedente all'esercizio precedente
      *     a quello selezionato
      this.oParentObject.w_ESEPRE = CALCESPR(i_CODAZI,this.oParentObject.w_DATINIESE,.T.)
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESVALNAZ,ESINIESE"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_ESEPRE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESVALNAZ,ESINIESE;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESCODESE = this.oParentObject.w_ESEPRE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_PRVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
        this.oParentObject.w_PRINIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT,VACAOVAL"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_PRVALNAZ);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT,VACAOVAL;
          from (i_cTable) where;
              VACODVAL = this.oParentObject.w_PRVALNAZ;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_PRDECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        this.oParentObject.w_PRCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo la Valuta
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- leggo la valuta e la data inizio esercizio di stampa
      this.w_MOLTI = IIF(this.w_VALESE=this.oParentObject.w_DIVISA,1,this.oParentObject.w_CAMVAL)
      this.w_DIVIDE = IIF(this.w_VALESE=this.oParentObject.w_DIVISA,1,GETCAM(this.w_VALESE,i_DATSYS,0))
      this.w_ESE2 = this.oParentObject.w_ESE
      * --- In TMP_PAR si inseriscono i dati relativi al secondo esercizio in modo che si possa aggiornare i BIL_DETT con i saldi di entrambi gli esercizi
      this.w_INSERISCI = .T.
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Ripristino i valori della Maschera
      select USRTMP1.*,recno() as numerorec FROM USRTMP1 INTO CURSOR USRTMP1 NOFILTER
      select eser1.TRTIPDET, eser1.DVCODMAS, eser1.MCDESCRI, eser1.ANCODICE, eser1.DESCRI, IIF (EMPTY (NVL (USRTMP1.MCSEZBIL , " ")), eser1.MCSEZBIL, USRTMP1.MCSEZBIL ) AS MCSEZBIL, eser1.TR__INDE, eser1.TRDESDET,; 
 eser1.TR__COLO, eser1.TR_FONT, eser1.TR_FTST, eser1.TOT1, eser1.DV_SEGNO, eser1.DV__DAVE,eser1.TOTALE, eser1.TMASTRI,; 
 eser1.TORAMASTRI, eser1.numerorec,eser1.CPROWORD1,eser1.CPROWNUM1,eser1.cproword,eser1.trseqsta,eser1.CPROWNUM,eser1.tipcon,eser1.codice,cp_ROUND(NVL(USRTMP1.TOT1,0*999999999999.9999),this.oParentObject.w_DECIMI) AS TOTORA, ; 
 cp_ROUND(NVL(USRTMP1.TOTALE,0*999999999999.9999),this.oParentObject.w_DECIMI) AS TOTALEORA,; 
 cp_ROUND(NVL(USRTMP1.TMASTRI,0*999999999999.9999),this.oParentObject.w_DECIMI) as TPREMASTRI, ; 
 cp_ROUND(NVL(USRTMP1.TMASTRI,0*999999999999.9999),this.oParentObject.w_DECIMI) as tmastriora,"N. D.                     " AS PERCENTOV,; 
 "N. D.                     " AS PERCENTOT; 
 from USRTMP1 left outer join eser1 on USRTMP1.numerorec=eser1.numerorec into cursor TEMP nofilter
      if this.oParentObject.w_TUTTI="S"
        * --- solo quelli con importi <>0
        select * from TEMP where ; 
 (tmastri<>0 or tpremastri<>0 or (trtipdet = "T" and (nvl(totale,0)<>0) or NVL(TOTALEORA,0)<>0)) or (trtipdet = "D") ; 
 into cursor TEMP NOFILTER
      else
        * --- TUTTI
        select * from TEMP into cursor TEMP NOFILTER
      endif
      CreaCurs=WRCURSOR("TEMP")
      SELECT TEMP
      Scan
      if nvl(totora,0)<>0
        * --- Voci
        replace percentov with tran(cp_ROUND(((tot1-totora)/totora)*100,2),"@Z 99999999999.99")
      endif
      replace tMASTRI with tot1-totora
      * --- Tmastri e Tmastriora contengono la differenza tra i due esercizi
      if nvl(totaleora,0)<>0
        * --- Totali
        replace percentot with tran(cp_ROUND(((totale-totaleora)/totaleora)*100,2),"@Z 99999999999.99")
      endif
      replace TMASTRIORA with totale-totaleora
      EndScan
      L_TOTDAT=this.oParentObject.w_TOTDAT 
 L_tutti=this.oParentObject.w_tutti 
 L_CODBUN=this.oParentObject.w_CODBUN 
 L_detcon=this.oParentObject.w_detcon 
 L_SUPERBU=this.oParentObject.w_SUPERBU 
 L_BILANCIO=this.oParentObject.w_BILDES 
 L_MONETE=this.oParentObject.w_MONETE 
 L_DESBUN=this.oParentObject.w_SBDESCRI 
 L_BIL=this.oParentObject.w_BILANCIO 
 L_DIVISA=this.oParentObject.w_DIVISA 
 L_VAPP=this.oParentObject.w_VALAPP 
 L_NOTE=this.oParentObject.w_NOTE 
 L_ESPREC=this.oParentObject.w_esepre 
 L_ESSALDI=this.oParentObject.w_ESSALDI 
 L_ORIPREC=this.oParentObject.w_ORIPREC
      * --- Problema con variabile PROVVI
      L_PROVVI=This.oParentObject.w_PROVVI
      * --- Quando uso interno stampo le selezioni (utilizzato nel Print When)
      L_USO=(this.oParentObject.W_USO<>"S")
      * --- Se arrotondati i decimali metto l_DECIMI a 0
      L_decimi=IIF(this.oParentObject.w_ARROT="N",L_decimi,0) 
 s=IIF(this.oParentObject.w_ARROT="N",s,20*(l_decimi+2))
      SELECT * FROM TEMP INTO CURSOR __TMP__ NOFILTER ; 
 ORDER BY trseqsta
      if RECCOUNT("__TMP__")>0
        if this.pTipo=1
          * --- LANCIO IL REPORT del Bilancio Strutturato
          if this.oParentObject.w_EXCEL="S"
            * --- Su EXCEL - Creo il cursore da quello di stampa
            select trseqsta, CAST ( IIF(TR__INDE>0,"_"+SPACE(TR__INDE-1),"")+trdesdet AS CHAR (254)) as trdesdet,; 
 cp_round(IIF(TRTIPDET="V" ,TOT1,IIF(TRTIPDET="T" ,TOTALE,0*999999999999.9999)),this.oParentObject.w_decimi) AS IMPORTO,; 
 cp_round(IIF(TRTIPDET="V" ,TOTORA,IIF(TRTIPDET="T" ,TOTALEORA,0*999999999999.9999)),this.oParentObject.w_decimi) AS IMPORTO2,; 
 cp_round(iif(TRTIPDET="V",tMASTRI,TMASTRIORA),this.oParentObject.w_decimi) AS DIFFERENZA, ; 
 IIF(TRTIPDET="V",PERCENTOV,PERCENTOT) AS PERCENTO,; 
 CPROWORD1,CPROWNUM1 FROM __TMP__ where trdesdet<>"-----" GROUP BY CPROWNUM,trseqsta; 
 into cursor __tmp__ ORDER BY trseqsta,CPROWORD1,CPROWNUM1
            wrtmp = WRCURSOR("__TMP__") 
 SELECT __TMP__ 
 GO TOP 
 SCAN
            if CPROWORD1=this.w_CPROWORD1
              REPLACE TRDESDET WITH SPACE(40) 
 REPLACE IMPORTO WITH 0 
 REPLACE IMPORTO2 WITH 0 
 REPLACE DIFFERENZA WITH 0
            else
              this.w_CPROWORD1 = CPROWORD1
            endif
            ENDSCAN
            CP_CHPRN("QUERY\BILCONFROEXCEL.XLT","",this.oParentObject)
          else
            CP_CHPRN("QUERY\BilConfro.frx","",this.oParentObject)
          endif
        else
          * --- Lancio il report del Bilancio Libro Inventari
          CP_CHPRN("QUERY\gscg1qbi.FRX","",this.oParentObject)
          if g_APPLICATION="ADHOC REVOLUTION"
            this.w_ULTPAG = IIF( Type("L_PAGINE")<>"N", iif(TYPE("g_PAGENUM")="N" and g_PAGENUM>0, this.oParentObject.w_PRPALI+g_PAGENUM, 0), L_PAGINE)
            if this.oParentObject.w_INTLIN="S"
              if ah_YesNo("Stampa in definitiva libro inventari%0Ultima pagina stampata %1%0Confermi aggiornamento?","",ALLTRIM(STR(this.w_ULTPAG)))
                * --- Try
                local bErr_04882F28
                bErr_04882F28=bTrsErr
                this.Try_04882F28()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                  AH_ERRORMSG("Impossibile aggiornare ultima pagina di stampa. Verificare tabella dati azienda",48)
                endif
                bTrsErr=bTrsErr or bErr_04882F28
                * --- End
              endif
            endif
            if this.oParentObject.w_ESSALDI<>"B" 
              * --- aggiorno manutenzione bilancio UE
              if ah_Yesno("Si desidera aggiornare i saldi Bilanci UE?%0Rispondendo affermativamente verranno sovrascritti eventuali saldi gi� presenti ")
                * --- Inserisce i dati relativi al 1� esercizio
                this.oParentObject.w_ESE = this.w_ESE1
                this.Page_6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- Inserisce i dati relativi al 2� esercizio
                this.oParentObject.w_ESE = this.w_ESE2
                this.w_INSERISCI = .F.
                this.Page_6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          else
            this.w_ULTPAG = IIF( Type("L_PAGINE")<>"N", iif(TYPE("g_PAGENUM")="N" and g_PAGENUM>0, this.oParentObject.w_ULTPLI+g_PAGENUM, 0), L_PAGINE)
            * --- Effattuo la chiamata al batch Gscg_Bb1 per poter effettuare la scrittura sulla
            *     tabella NUMEREGI non presente nell'anlisi di Revolution
            if this.oParentObject.w_PCONLI="S"
              do Gscg_Bb1 with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      else
        ah_ErrorMsg ("Non ci sono dati da stampare",64)
      endif
      this.oParentObject.BlankRec()
    else
      this.w_ORIGINE = this.oParentObject.w_ESSALDI
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.pTipo=2
        * --- Confronto tra bilanci
        select TRDESDET,TR__COLO,TR__INDE,TR_FONT,TR_FTST,; 
 TOT1,TOTALE,CPROWORD1,TRTIPDET FROM USRTMP1 GROUP BY CPROWORD1 ; 
 INTO CURSOR CURBILANCIO NOFILTER
        * --- Chiudo i cursori e il Batch
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      L_TOTDAT=this.oParentObject.w_TOTDAT 
 L_tutti=this.oParentObject.w_tutti 
 L_CODBUN=this.oParentObject.w_CODBUN 
 L_DATA1=this.oParentObject.w_DATA1 
 L_DATA2=this.oParentObject.w_DATA2 
 L_detcon=this.oParentObject.w_detcon 
 L_SUPERBU=this.oParentObject.w_SUPERBU 
 L_BILANCIO=this.oParentObject.w_BILDES 
 L_MONETE=this.oParentObject.w_MONETE 
 L_DESBUN=this.oParentObject.w_SBDESCRI 
 L_ESE=this.oParentObject.w_ESE 
 L_BIL=this.oParentObject.w_BILANCIO 
 L_DIVISA=this.oParentObject.w_DIVISA 
 L_VAPP=this.oParentObject.w_VALAPP 
 L_NOTE=this.oParentObject.w_NOTE 
 L_ESPREC=this.oParentObject.w_esepre 
 L_PREESE=this.oParentObject.w_esepre 
 L_ESSALDI=this.oParentObject.w_ESSALDI 
 L_ORIPREC=this.oParentObject.w_ORIPREC
      * --- Problema con variabile PROVVI
      L_PROVVI=This.oParentObject.w_PROVVI
      * --- Quando uso interno stampo le selezioni (utilizzato nel Print When)
      L_USO=(this.oParentObject.W_USO<>"S")
      * --- Se arrotondati i decimali metto l_DECIMI a 0
      L_decimi=IIF(this.oParentObject.w_ARROT="N",L_decimi,0) 
 s=IIF(this.oParentObject.w_ARROT="N",s,20*(l_decimi+2))
      if this.oParentObject.w_TUTTI="S"
        * --- solo quelli con importi <>0
        select *,recno() as numerorec from usrtmp1 where ; 
 (tmastri<>0 .or. (trtipdet = "T" .and. nvl(totale,0)<>0)) or (trtipdet = "D") into cursor __TMP__ NOFILTER ; 
 ORDER BY trseqsta
      else
        * --- TUTTI
        select *,recno() as numerorec from usrtmp1 into cursor __TMP__ NOFILTER ; 
 ORDER BY trseqsta
      endif
      if RECCOUNT("__TMP__")>0
        if this.pTipo=1
          * --- LANCIO IL REPORT del Bilancio Strutturato
          if this.oParentObject.w_EXCEL="S"
            * --- Su EXCEL - Creo il cursore da quello di stampa
            if L_DETCON="S"
              * --- Dettaglio CONTI
              select __tmp__.*,IIF(EMPTY(NVL(DVCODMAS,"")), ANCODICE, DVCODMAS) AS CONTO; 
 from __tmp__ into cursor __tmp__ ; 
 group by CPROWORD1,CPROWNUM1,cprownum,trseqsta,tipcon,codice; 
 order by trseqsta,CPROWORD1,CPROWNUM1,cprownum,tipcon,codice
              select trseqsta, CAST ( IIF(TR__INDE>0,"_"+SPACE(TR__INDE-1),"")+trdesdet AS CHAR (254)) as trdesdet,; 
 cp_round(IIF(TRTIPDET="V" ,TOT1,IIF(TRTIPDET="T" ,TOTALE,0*999999999999.9999)),this.oParentObject.w_decimi) AS IMPORTO, ; 
 sum(IIF(dv_segno="+",1,-1)*IIF((dv__dave="A" AND mcsezbil$"CA") or (dv__dave="D" AND ; 
 mcsezbil$"RP"),-1,1)*cp_round(TMASTRI,l_decimi)) AS IMPCONTO, CONTO, (MAX ( NVL (DESCRI , " ") + NVL (MCDESCRI, " ") )) AS DESCRI , ; 
 TR__COL1,TR__COL2,TR__COL3,CPROWORD1,CPROWNUM1 ; 
 from __tmp__ where trdesdet<>"-----" into cursor __tmp__ ; 
 group by CPROWORD1,CPROWNUM1,cprownum,trseqsta,conto ; 
 order by trseqsta,CPROWORD1,CPROWNUM1,cprownum,conto
              select trseqsta , trdesdet , IMPORTO , IMPCONTO , alltrim(DESCRI) AS conto , TR__COL1 , TR__COL2 , TR__COL3 , CPROWORD1 , CPROWNUM1 FROM __TMP__ into cursor __TMP__
              wrtmp = WRCURSOR("__TMP__") 
 SELECT __TMP__ 
 GO TOP 
 SCAN
              if CPROWORD1=this.w_CPROWORD1
                REPLACE TRDESDET WITH SPACE(40) 
 REPLACE IMPORTO WITH 0 
 REPLACE TR__COL1 WITH " "
              else
                this.w_CPROWORD1 = CPROWORD1
              endif
              ENDSCAN
            else
              * --- Nessun Dettaglio
              select trseqsta, CAST (IIF(TR__INDE>0,"_"+SPACE(TR__INDE-1),"")+trdesdet AS CHAR (254)) as trdesdet,; 
 cp_round(IIF(TRTIPDET="V" ,TOT1,IIF(TRTIPDET="T" ,TOTALE,0*999999999999.9999)),; 
 this.oParentObject.w_decimi) AS IMPORTO, ; 
 0 AS IMPCONTO,"" AS CONTO,TR__COL1,TR__COL2,TR__COL3,; 
 CPROWORD1,CPROWNUM1 FROM __TMP__ where trdesdet<>"-----" GROUP BY CPROWNUM,trseqsta; 
 into cursor __tmp__ ORDER BY trseqsta,CPROWORD1,CPROWNUM1
            endif
            CP_CHPRN("QUERY\BILANCIOEXCEL.XLT","",this.oParentObject)
          else
            CP_CHPRN("QUERY\gsar_qbi.FRX","",this.oParentObject)
          endif
        else
          * --- Lancio il report del Bilancio Libro Inventari
          CP_CHPRN("QUERY\gscg_qbi.FRX","",this.oParentObject)
          if g_APPLICATION="ADHOC REVOLUTION"
            this.w_ULTPAG = IIF( Type("L_PAGINE")<>"N", iif(TYPE("g_PAGENUM")="N" and g_PAGENUM>0, this.oParentObject.w_PRPALI+g_PAGENUM, 0), L_PAGINE)
            if this.oParentObject.w_INTLIN="S"
              if ah_YesNo("Stampa in definitiva libro inventari%0Ultima pagina stampata %1%0Confermi aggiornamento?","",ALLTRIM(STR(this.w_ULTPAG)))
                * --- Try
                local bErr_04861E48
                bErr_04861E48=bTrsErr
                this.Try_04861E48()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                  AH_ERRORMSG("Impossibile aggiornare ultima pagina di stampa. Verificare tabella dati azienda",48)
                endif
                bTrsErr=bTrsErr or bErr_04861E48
                * --- End
              endif
              if this.oParentObject.w_ESSALDI<>"B" 
                * --- aggiorno manutenzione bilancio UE
                if ah_Yesno("Si desidera aggiornare i saldi Bilanci UE?%0Rispondendo affermativamente verranno sovrascritti eventuali saldi gi� presenti ")
                  this.Page_6()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
          else
            this.w_ULTPAG = IIF( Type("L_PAGINE")<>"N", iif(TYPE("g_PAGENUM")="N" and g_PAGENUM>0, this.oParentObject.w_ULTPLI+g_PAGENUM, 0), L_PAGINE)
            * --- Effattuo la chiamata al batch Gscg_Bb1 per poter effettuare la scrittura sulla
            *     tabella NUMEREGI non presente nell'anlisi di Revolution
            if this.oParentObject.w_PCONLI="S"
              do Gscg_Bb1 with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      else
        ah_ErrorMsg ("Non ci sono dati da stampare",64)
      endif
    endif
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_04882F28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZPRPALI ="+cp_NullLink(cp_ToStrODBC(this.w_ULTPAG),'AZIENDA','AZPRPALI');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZ1);
             )
    else
      update (i_cTable) set;
          AZPRPALI = this.w_ULTPAG;
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZ1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04861E48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZPRPALI ="+cp_NullLink(cp_ToStrODBC(this.w_ULTPAG),'AZIENDA','AZPRPALI');
      +",AZESLBIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ESE),'AZIENDA','AZESLBIN');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZ1);
             )
    else
      update (i_cTable) set;
          AZPRPALI = this.w_ULTPAG;
          ,AZESLBIN = this.oParentObject.w_ESE;
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZ1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.oParentObject.w_PRPALI = this.w_ULTPAG
    this.oParentObject.w_ESESTA = this.oParentObject.w_ESE
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo i due cambi (quello che divide e quello che moltilpica) tra la valuta di stampa
    *      (quella dell'esercizio di stampa) e la valuta dell'esercizio precedente
    *     Se stampa in altra valuta il cambio (/) viene applicato sul report
    *     Comunque tutti gli importi che arrivano al report sono nella valuta dell'esercizio scelto
    do CALCCONT with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_DIVIDE=0
      i_retcode = 'stop'
      return
    endif
    this.w_UTENTE = g_CODUTE
    * --- Cambio tra la valuta di conto e la valuta di stampa scelta nella maschera
    this.w_CAMMASK = iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcoli principali
    this.w_APERTURA = .F.
    if this.oParentObject.w_MONETE="c" AND NOT this.w_SecondaVolta
      * --- Leggo informazioni della moneta di conto esercizio scelto (DECIMALI)
      *     Leggo la valuta dell'esercizio
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESVALNAZ"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_ESE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESVALNAZ;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESCODESE = this.oParentObject.w_ESE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DIVISA = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_DIVISA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.oParentObject.w_DIVISA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DECIMI = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.oParentObject.w_CONFRONTO="S"
      ah_msg("Calcolo saldi di bilancio es. %1",.t.,.f.,.f.,this.oParentObject.w_ESE)
    else
      ah_msg("Fase 2: calcolo saldi di bilancio")
    endif
    * --- LEGGO I MOVIMENTI COMPRESI FRA LE DATE DI STAMPA E DI COMPETENZA DELL'ESERCIZIO
    *     SE SPECIFICATI, APPLICO I FILTRI PER B.U. E MOVIMENTI PROVVISORI
    * --- LEGGO I MOVIMENTI PRECEDENTI ALLA DATA DI INIZIO STAMPA CON COMPETENZA DELL'ESERCIZIO IN CORSO
    *     QUESTA QUERY SI PUO' EVITARE SE LA DATA DI INIZIO STAMPA COINCIDE CON L'INIZIO DELL'ESERCIZIO
    ah_msg("Lettura movimenti compresi fra le date di stampa",.t.,.t.)
    * --- Flag saldi bilancio EU
    do case
      case this.w_ORIGINE="B"
        * --- Create temporary table TMPMOVIMAST
        i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_ZZLNXYRBOF[2]
        indexes_ZZLNXYRBOF[1]='PNTIPCON,PNCODCON'
        indexes_ZZLNXYRBOF[2]='MCCODICE'
        vq_exec('query\b_SALBILUE',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_ZZLNXYRBOF,.f.)
        this.TMPMOVIMAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      case this.w_ORIGINE="S"
        * --- Create temporary table TMPMOVIMAST
        i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_URLWUFGAXI[2]
        indexes_URLWUFGAXI[1]='PNTIPCON,PNCODCON'
        indexes_URLWUFGAXI[2]='MCCODICE'
        vq_exec('query\b_movimenti1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_URLWUFGAXI,.f.)
        this.TMPMOVIMAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      otherwise
        if this.oParentObject.w_MASABI="S"
          * --- Create temporary table TMPMOVIMAST
          i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_GYVSNMRQGQ[2]
          indexes_GYVSNMRQGQ[1]='PNTIPCON,PNCODCON'
          indexes_GYVSNMRQGQ[2]='MCCODICE'
          vq_exec('query\b_movimenti2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_GYVSNMRQGQ,.f.)
          this.TMPMOVIMAST_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Create temporary table TMPMOVIMAST
          i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_XCTRKAETZU[2]
          indexes_XCTRKAETZU[1]='PNTIPCON,PNCODCON'
          indexes_XCTRKAETZU[2]='MCCODICE'
          vq_exec('query\b_movimenti',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_XCTRKAETZU,.f.)
          this.TMPMOVIMAST_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
    endcase
    if this.oParentObject.w_ESPREC="S" AND this.w_ORIGINE<>"B"
      * --- INIZIO LA LETTURA DEI SALDI DELL'ESERCIZIO PRECEDENTE
      *     1- VERIFICO SE PER L'ESERCIZIO IN CORSO E' STATA FATTA L'APERTURA
      if this.oParentObject.w_PROVVI<>"S"
        * --- Entro a verificare se c'� apertura anche nel caso che sto 
        *     stampando tutti i mvimenti ma non esiste esercizio precedente
        *     QUESTO CASO SI VERIFICA SOLO NELLA STAMPA CONFRONTO
        ah_msg("Verifica effettuazione apertura conti",.t.,.t.)
        if this.oParentObject.w_MASABI="S"
          * --- Insert into TMPMOVIMAST
          i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura1",this.TMPMOVIMAST_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into TMPMOVIMAST
          i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura",this.TMPMOVIMAST_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if i_Rows > 0
          this.w_APERTURA = .T.
        endif
      endif
      if NOT this.w_APERTURA 
        * --- 2- SE NON E' STATA FATTA L'APERTURA DEI CONTI, LEGGIAMO I SALDI DAI MOVIMENTI DELL'ESERCIZIO PRECEDENDE
        *     DALLA LETTURA ESCLUDIAMO I MOVIMENTI DI CHIUSURA
        ah_msg("Lettura movimenti dell'esercizio precedente",.t.,.t.)
        if this.oParentObject.w_MASABI="S"
          if this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo2",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo3",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        else
          if this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo1",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        if i_Rows=0 AND this.oParentObject.w_PROVVI<>"S"
          * --- non ci sono movimenti nell'esercizio precedente per cui provo a leggere il movimento di apertura
          ah_msg("Verifica effettuazione apertura conti",.t.,.t.)
          if this.oParentObject.w_MASABI="S"
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura1",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        if this.oParentObject.w_PROVVI<>"S" and EMPTY(this.oParentObject.w_CODBUN) AND EMPTY(this.oParentObject.w_SUPERBU)
          * --- LEGGIAMO ANCHE I MOVIMENTI FUORI LINEA CHE DEVONO POI ESSERE SOMMATI.
          ah_msg("Lettura saldi fuori linea",.t.,.t.)
          if this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_movfuori",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_movfuori1",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      endif
    endif
    if g_APPLICATION="ADHOC REVOLUTION"
      * --- Creo Tabella di Appoggio per eseguire aggiornamento Manutenzione Bilancio EU
      if this.w_INSERISCI
        * --- Insert into TMP_PART
        i_nConn=i_TableProp[this.TMP_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscg_bbg",this.TMP_PART_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        * --- Create temporary table TMP_PART
        i_nIdx=cp_AddTableDef('TMP_PART') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('gscg_bbg',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMP_PART_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    endif
    * --- Per non portarmi tutti i clienti e fornitori modifico il nome del conto
    *     con il valore $$DARE$$ e $$AVERE$$ se hanno saldo rispettivamente
    *     dare o avere per ogni conto, mastro
    * --- Write into TMPMOVIMAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PNTIPCON,PNCODCON,MCCODICE"
      do vq_exec with 'B_BILANCIOB',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMOVIMAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPMOVIMAST.PNTIPCON = _t2.PNTIPCON";
              +" and "+"TMPMOVIMAST.PNCODCON = _t2.PNCODCON";
              +" and "+"TMPMOVIMAST.MCCODICE = _t2.MCCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNCODCON = _t2.ANCODICE";
          +i_ccchkf;
          +" from "+i_cTable+" TMPMOVIMAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPMOVIMAST.PNTIPCON = _t2.PNTIPCON";
              +" and "+"TMPMOVIMAST.PNCODCON = _t2.PNCODCON";
              +" and "+"TMPMOVIMAST.MCCODICE = _t2.MCCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST, "+i_cQueryTable+" _t2 set ";
          +"TMPMOVIMAST.PNCODCON = _t2.ANCODICE";
          +Iif(Empty(i_ccchkf),"",",TMPMOVIMAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPMOVIMAST.PNTIPCON = t2.PNTIPCON";
              +" and "+"TMPMOVIMAST.PNCODCON = t2.PNCODCON";
              +" and "+"TMPMOVIMAST.MCCODICE = t2.MCCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST set (";
          +"PNCODCON";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ANCODICE";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPMOVIMAST.PNTIPCON = _t2.PNTIPCON";
              +" and "+"TMPMOVIMAST.PNCODCON = _t2.PNCODCON";
              +" and "+"TMPMOVIMAST.MCCODICE = _t2.MCCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST set ";
          +"PNCODCON = _t2.ANCODICE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PNTIPCON = "+i_cQueryTable+".PNTIPCON";
              +" and "+i_cTable+".PNCODCON = "+i_cQueryTable+".PNCODCON";
              +" and "+i_cTable+".MCCODICE = "+i_cQueryTable+".MCCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNCODCON = (select ANCODICE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- per estrarre i conti movimentati prendo le tabelle in inner
    if this.oParentObject.w_CONFRONTO="S"
      ah_msg("Calcolo valori e saldi movimenti %1",.t.,.f.,.f.,this.oParentObject.w_ESE )
    else
      ah_msg("Fase 3: calcolo valori e saldi movimenti")
    endif
    vq_exec("query\b_bilancio2",this,"usrtmp")
    * --- Drop temporary table TMPMOVIMAST
    i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIMAST')
    endif
    * --- Aggiungo alla struttura gli importi
    *     Prendo i dati dei soli conti presenti nella struttura 2 Join una per i conti  una per i conti derivanti da mastri
    *     Se stampo da data a data e devo considerare le b. unit INIZIO e INIZIO2 non li considero
    select usrtmp.*,; 
 struttura.* from struttura left outer join usrtmp ; 
 on (usrtmp.tipcon=struttura.antipcon and usrtmp.codice=struttura.ancodice) ; 
 where struttura.antipcon<>"M" ; 
 into cursor usrtmp3 nofilter
    * --- calcolo la valorizzazione per i mastri raggruppando sui conti
    select tipcon, iif(tipcon="C" or tipcon="F",Codice,"               ") as ancodice, anconsup, mcsezbil, ; 
 sum(Import) as Import, sum(Inizio) as Inizio,; 
 sum(Precedente) as Precedente, sum(Import2) as Import2, sum(Inizio2) as Inizio2, sum(Precede2) as Precede2, ; 
 sum(Import3) as Import3, sum(Import4) as Import4, An, An2 from usrtmp where tipcon in ("C","F","M") ; 
 group by tipcon, ancodice, anconsup, mcsezbil, An, An2 into cursor usrtmp5 
    select dist cproword, trseqsta, trtipdet, trtipvoc, trdesdet, tr__col1, tr__col2, tr__col3, dvcodmas, antipcon, ancodice, ; 
 dv_segno, tipconma, iif(tipconma$"CF",conmastro,space(15)) as conmastro, anconsup2, cprownum, ; 
 tr__colo, tr__inde, tr_ftst, tr_font, tr_rifue, anconsup, mctipmas, dv__dave, cprownum1, ; 
 vr__note, cproword1, "                                        " as descri, mcdescri from struttura into cursor struttura1
    select usrtmp5.*, struttura1.* ; 
 from struttura1 left outer join usrtmp5 on (usrtmp5.anconsup=struttura1.dvcodmas and struttura1.antipcon=usrtmp5.tipcon) ; 
 where struttura1.antipcon="M" and NVL(struttura1.tipconma,"M") not in ("C","F") ; 
 into cursor usrtmp4 nofilter ; 
 union ; 
 select usrtmp5.*, struttura1.* ; 
 from struttura1 left outer join usrtmp5 on (usrtmp5.anconsup=struttura1.dvcodmas ; 
 and left(nvl(usrtmp5.ancodice,"")+space(15),15)=struttura1.conmastro) ; 
 where struttura1.antipcon="M" and NVL(struttura1.tipconma,"M") in ("C","F")
    * --- Faccio una Union perch� la condizione di or sulla join rallentava parecchio l'esecuzione
    select usrtmp5 
 use 
 select struttura1 
 use
    if USED("usrtmp3")
      if USED("usrtmp4")
        select * from usrtmp3 ; 
 Union All ; 
 select * from usrtmp4 ; 
 into cursor usrtmp nofilter
        Select usrtmp4 
 use
      else
        select * from usrtmp3 ; 
 into cursor usrtmp nofilter
      endif
      Select usrtmp3 
 use
    else
      if USED("usrtmp4")
        select * from usrtmp4 ; 
 into cursor usrtmp nofilter
        Select usrtmp4 
 use
      endif
    endif
    * --- Rendo scrivibile il cursore USRTMP per calcolare gli importi considenrando anche un eventuale saldo iniziale
    CreaCur=WRCURSOR("USRTMP")
    SELECT USRTMP
    GO TOP
    * --- Scorro il cursore ricavato per costruirne uno nuovo con anche i saldi iniziali
    SCAN
    * --- Calcolo gli importi - conti derivanti da mastri presenti nella struttura
    this.w_SEZBIL = NVL(USRTMP.AN,"X")
    * --- Se da Data a Data
    if this.oParentObject.w_ESPREC="S" And ( this.w_SEZBIL $"APO" ) and (this.oParentObject.w_DATA1=this.oParentObject.w_DATINIESE Or empty(this.oParentObject.w_DATA1)) And !Empty(NVL(USRTMP.ANTIPCON,"")) AND this.w_ORIGINE<>"B"
      * --- Aggiungo agli importi della Prima Nota i Saldi Iniziali
      this.w_IMPORT31 = NVL(USRTMP.IMPORT,0)+nvl(USRTMP.INIZIO,0)+NVL(USRTMP.PRECEDENTE,0)
    else
      * --- Stampo solo i movimenti della Prima Nota
      this.w_IMPORT31 = NVL(USRTMP.IMPORT,0)
    endif
    this.w_SEZBIL = NVL(USRTMP.AN2,"X")
    * --- Importi dei conti presenti direttamente nella struttura
    if this.oParentObject.w_ESPREC="S" And (this.w_SEZBIL $"APO") And (this.oParentObject.w_DATA1 =this.oParentObject.w_DATINIESE Or empty(this.oParentObject.w_DATA1)) And !Empty(NVL(USRTMP.ANTIPCON,""))
      this.w_IMPORT41 = NVL(USRTMP.IMPORT2,0)+NVL(USRTMP.INIZIO2,0)+NVL(USRTMP.PRECEDE2,0)
    else
      this.w_IMPORT41 = NVL(USRTMP.IMPORT2,0)
    endif
    * --- se passivit� o ricavi cambio il segno degli importi
    if NVL(USRTMP.AN,"X") $ "PR"
      this.w_IMPORT31 = -this.w_IMPORT31
    endif
    if NVL(USRTMP.AN2,"X") $ "PR"
      this.w_IMPORT41 = -this.w_IMPORT41
    endif
    * --- Carico gli Importi - nei campi IMPORT4 (conti derivati da mastri nelle voci ) e IMPORT3 (conti direttamente nelle voci)
    if this.oParentObject.w_DIVISA=this.oParentObject.w_VALAPP
      REPLACE IMPORT3 WITH cp_ROUND(this.w_IMPORT31,this.oParentObject.w_DECIMI)
      REPLACE IMPORT4 WITH cp_ROUND(this.w_IMPORT41,this.oParentObject.w_DECIMI)
    else
      REPLACE IMPORT3 WITH mon2val(this.w_IMPORT31,L_MOLTI,0,i_DATSYS,this.oParentObject.w_VALAPP,this.oParentObject.w_DECIMI)
      REPLACE IMPORT4 WITH mon2val(this.w_IMPORT41,L_MOLTI,0,i_DATSYS,this.oParentObject.w_VALAPP,this.oParentObject.w_DECIMI)
    endif
    ENDSCAN
    if this.oParentObject.w_CONFRONTO="S"
      ah_msg("Calcolo voci di totalizzazione es. %1",.t.,.f.,.f.,this.oParentObject.w_ESE)
    else
      ah_msg("Fase 4: calcolo voci di totalizzazione")
    endif
    if this.oParentObject.w_ARROT="S" OR this.oParentObject.w_ARROT="T"
      if this.oParentObject.w_DECIMI=0
        this.w_VALARRO = -3
        this.w_IMPARRO = IIF(this.oParentObject.w_ARROT="S",0,499)
      else
        this.w_VALARRO = 0
        this.w_IMPARRO = IIF(this.oParentObject.w_ARROT="S",0,0.4999)
      endif
    else
      this.w_VALARRO = this.oParentObject.w_DECIMI
    endif
    * --- Calcolo il totale dei Mastri contenuti nelle Voci - Tmastri contiene anche importi conti nelle voci
    if this.oParentObject.w_ARRVOCI="M"
      * --- Arrotondo i Mastri
      SELECT cp_ROUND(SUM(NVL(USRTMP.IMPORT4,0.0000)+NVL(USRTMP.IMPORT3,0.0000))- ; 
 (IIF(SUM(NVL(USRTMP.IMPORT4,0.0000)+NVL(USRTMP.IMPORT3,0.0000))<0,-1,1)*this.w_IMPARRO),this.w_VALARRO) as tMASTRI, ; 
 SUM(NVL(USRTMP.IMPORT4,0.0000)+NVL(USRTMP.IMPORT3,0.0000)) as tMASTRI1,; 
 usrtmp.CPROWORD,usrtmp.cprownum1,USRTMP.DV_SEGNO,usrtmp.dv__dave,max(NVL(usrtmp.an," ")) as an, codice ; 
 FROM USRTMP group by usrtmp.CPROWORD,usrtmp.cprownum1,usrtmp.codice ; 
 having empty(nvl(dv__dave,"")) or (NVL(DV__DAVE,"")="D" and ((max(NVL(usrtmp.an," ")) $ "PR" and tmastri<0) or (max(NVL(usrtmp.an," ")) $ "AC" and tmastri>=0))); 
 or (NVL(DV__DAVE,"")="A" and ((max(NVL(usrtmp.an," ")) $ "AC" and tmastri<0) or (max(NVL(usrtmp.an," ")) $ "PR" and tmastri>=0))) into cursor TOTMASTRI nofilter
      * --- Calcolo il totale delle Voci
      SELECT SUM(IIF(DV_SEGNO="+",1,-1)*cp_ROUND(IIF(empty(nvl(dv__dave,"")),TMASTRI, abs(TMASTRI)), ; 
 this.oParentObject.w_DECIMI)) AS TOT1, ; 
 SUM(IIF(DV_SEGNO="+",1,-1)*cp_ROUND(IIF(empty(nvl(dv__dave,"")),TMASTRI1, abs(TMASTRI1)), ; 
 this.oParentObject.w_DECIMI)) AS TOT2,CPROWORD ; 
 FROM TOTMASTRI GROUP BY CPROWORD INTO CURSOR TOTVOCI nofilter
    else
      * --- Arrotondo le Voci
      SELECT SUM(NVL(USRTMP.IMPORT4,0.0000)+NVL(USRTMP.IMPORT3,0.0000)) as tMASTRI, ; 
 SUM(NVL(USRTMP.IMPORT4,0.0000)+NVL(USRTMP.IMPORT3,0.0000)) as tMASTRI1,; 
 usrtmp.CPROWORD,usrtmp.cprownum1,USRTMP.DV_SEGNO,usrtmp.dv__dave,max(NVL(usrtmp.an," ")) as an, codice ; 
 FROM USRTMP group by usrtmp.CPROWORD,usrtmp.cprownum1,usrtmp.codice ; 
 having empty(nvl(dv__dave,"")) or (NVL(DV__DAVE,"")="D" and ((max(NVL(usrtmp.an," ")) $ "PR" and tmastri<0) or (max(NVL(usrtmp.an," ")) $ "AC" and tmastri>=0))); 
 or (NVL(DV__DAVE,"")="A" and ((max(NVL(usrtmp.an," ")) $ "AC" and tmastri<0) or (max(NVL(usrtmp.an," ")) $ "PR" and tmastri>=0))) into cursor TOTMASTRI nofilter
      * --- Calcolo il totale delle Voci
      SELECT cp_ROUND(SUM(IIF(DV_SEGNO="+",1,-1)*cp_ROUND(IIF(empty(nvl(dv__dave,"")),TMASTRI, abs(TMASTRI)), this.oParentObject.w_DECIMI)) - ; 
 (IIF(SUM(IIF(DV_SEGNO="+",1,-1)*cp_ROUND(IIF(empty(nvl(dv__dave,"")),TMASTRI, abs(TMASTRI)), this.oParentObject.w_DECIMI))<0,-1,1) ; 
 *this.w_IMPARRO),this.w_VALARRO) AS TOT1, ; 
 SUM(IIF(DV_SEGNO="+",1,-1)*cp_ROUND(IIF(empty(nvl(dv__dave,"")),TMASTRI1, abs(TMASTRI1)), ; 
 this.oParentObject.w_DECIMI)) AS TOT2,CPROWORD ; 
 FROM TOTMASTRI GROUP BY CPROWORD INTO CURSOR TOTVOCI nofilter
    endif
    * --- metto tutto assieme - ordino per dettaglio struttura e per dettaglio voce (Cproword)
    select usrtmp.*,cp_ROUND(nvl(totMASTRI.tMASTRI,0),this.oParentObject.w_DECIMI ) AS TMASTRI, ; 
 cp_ROUND(nvl(TOTVOCI.TOT1,0),this.oParentObject.w_DECIMI) AS TOT1, ; 
 cp_ROUND(nvl(TOTVOCI.TOT2,0),this.oParentObject.w_DECIMI) AS TOT2 FROM ; 
 (usrtmp left outer join totMASTRI ; 
 on ( usrtmp.CPROWORD=totMASTRI.CPROWORD and usrtmp.cprownum1=totMASTRI.cprownum1 ; 
 and nvl(usrtmp.codice,space(15))=nvl(totmastri.codice,space(15)))) ; 
 LEFT OUTER JOIN TOTVOCI on ( usrtmp.CPROWORD=totVOCI.CPROWORD ) ; 
 ORDER BY USRTMP.CPROWORD1,USRTMP.CPROWNUM1 into cursor usrtmp1 nofilter
    if this.oParentObject.w_ARROT="S" OR this.oParentObject.w_ARROT="T"
      WrtUsrtmp1=WRCURSOR("usrtmp1")
    endif
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_ARROT="S" OR this.oParentObject.w_ARROT="T"
      * --- Calolo differenza di conversione per utile
      SELECT usrtmp2
      GO TOP
      LOCATE FOR usrtmp2.TR_RIFUE= "U"
      if FOUND()
        this.w_DIFFUTILE = TOTALE-cp_ROUND(TOTALENA-(IIF(TOTALENA<0,-1,1)*this.w_IMPARRO), this.w_VALARRO)
        if this.w_DIFFUTILE<>0
          * --- Se c'� una differenza devo aggiungerla alla voce segnata come Oneri Straodinario
          SELECT ANTIPCON,ANCODICE FROM usrtmp1 WHERE TR_RIFUE ="O" AND TRTIPDET="V" ORDER BY TMASTRI ASC INTO CURSOR ARROTONDA
          SELECT ARROTONDA 
 GO TOP
          this.w_TIPCON_ARR = ARROTONDA.ANTIPCON
          this.w_ANCODICE_ARR = ARROTONDA.ANCODICE
          UPDATE usrtmp1 SET TMASTRI = TMASTRI + this.w_DIFFUTILE ; 
 WHERE TR_RIFUE ="O" AND TRTIPDET="V" AND ANTIPCON=this.w_TIPCON_ARR AND ANCODICE=this.w_ANCODICE_ARR
          UPDATE usrtmp1 SET TOT1 = TOT1 + this.w_DIFFUTILE ; 
 WHERE TR_RIFUE ="O" AND TRTIPDET="V"
          SELECT usrtmp1
          COUNT TO INDICE
          this.w_RicTotali = .T.
        endif
      endif
      * --- Calolo differenza tra totale attivit� e totale passivit�
      SELECT usrtmp2
      GO TOP
      LOCATE FOR usrtmp2.TR_RIFUE= "A"
      if FOUND()
        this.w_DIFFATT = ABS(TOTALE)
        SELECT usrtmp2
        GO TOP
        LOCATE FOR usrtmp2.TR_RIFUE= "P"
        this.w_DIFFPAS = ABS(TOTALE)
        if FOUND()
          UPDATE usrtmp1 SET TOT1 = TOT1 + this.w_DIFFATT - this.w_DIFFPAS, ; 
 TMASTRI = TMASTRI + this.w_DIFFATT - this.w_DIFFPAS ; 
 WHERE TR_RIFUE ="R" AND TRTIPDET="V"
          SELECT usrtmp1
          COUNT TO INDICE
          this.w_RicTotali = .T.
        endif
      endif
      if this.w_RicTotali
        SELECT usrtmp2 
 Use 
 SELECT totali 
 Use
        * --- Devo ricalcolare il totale
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_RicTotali = .F.
    endif
    SELECT usrtmp1 
 USE 
 Select * from usrtmp2 into cursor usrtmp1 nofilter 
 SELECT usrtmp2 
 USE
    this.w_SecondaVolta = .T.
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if used("Eser1")
      select Eser1
      use
    endif
    if used("UsrTmp")
      select UsrTmp
      use
    endif
    if used("UsrTmp1")
      select UsrTmp1
      use
    endif
    if used("TotVoci")
      select TotVoci
      use
    endif
    if used("Totmastri")
      select TotMastri
      use
    endif
    if used("Struttura")
      select Struttura
      use
    endif
    if used("Eserci")
      select Eserci
      use
    endif
    if used("totali")
      select totali
      use
    endif
    if used("TEMP")
      select TEMP
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
    if used("ARROTONDA")
      select ARROTONDA
      use
    endif
    * --- Pulizia variabili corporate
    if g_IZCP$"SA"
      * --- Azziera variabili CPZ
      g_ZCPTINTEST = " "
      g_ZCPCINTEST = " "
      g_ZCPALLENAME = " "
      g_ZCPALLETITLE = " "
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruisco il vettore che ni conterr� gli importi delle voci di tipo TOTALE
    *     un vettore perch� devo necessariamente (caso totale di totali) andare riga per riga
    * --- Creo un cursore per calcolare i totali
    CREATE CURSOR totali (CPROWNUM N(4),CPROWORD N(6),TOTALE N(18,4),TOTALENA N(18,4))
    * --- CPrownum mi serve per la Join mentre CPROWORD  per identifiacarlo per mezzo di RIGTOTAL
    *     Funziona solo se i totali riferiscono totali che li precededono
    *     scorro solo le voci di tipo TOTALIZZATORE
    select USRTMP1
    scan for USRTMP1.TRTIPDET="T"
    this.w_TOTALE = 0.0000
    this.w_TOTALENA = 0.0000
    * --- Totale non arrotondato
    this.w_CPROWNUM = USRTMP1.CPROWORD
    this.w_PAR1 = ALLTRIM(STR(USRTMP1.CPROWORD1))
    this.w_PAR2 = ALLTRIM(USRTMP1.TRDESDET)
    vecchiaArea=select()
    * --- scorro le righe che fanno parte del totale
    * --- Select from RIGTOTAL
    i_nConn=i_TableProp[this.RIGTOTAL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIGTOTAL_idx,2],.t.,this.RIGTOTAL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select RTRIGTOT,RT_SEGNO  from "+i_cTable+" RIGTOTAL ";
          +" where RTCODICE="+cp_ToStrODBC(this.oParentObject.w_BILANCIO)+" and RTNUMRIG="+cp_ToStrODBC(this.w_CPROWNUM)+"";
           ,"_Curs_RIGTOTAL")
    else
      select RTRIGTOT,RT_SEGNO from (i_cTable);
       where RTCODICE=this.oParentObject.w_BILANCIO and RTNUMRIG=this.w_CPROWNUM;
        into cursor _Curs_RIGTOTAL
    endif
    if used('_Curs_RIGTOTAL')
      select _Curs_RIGTOTAL
      locate for 1=1
      do while not(eof())
      this.w_RIGA = _Curs_RIGTOTAL.RTRIGTOT
      this.w_SEGNO = _Curs_RIGTOTAL.RT_SEGNO
      righeArea=select()
      select (vecchiaArea)
      posizione=recno()
      GO TOP
      LOCATE FOR USRTMP1.CPROWORD1= this.w_RIGA
      do case
        case USRTMP1.TRTIPDET="D"
          * --- Il totale punta su una descrizione -
          this.w_TOTALE = this.w_TOTALE + 0.0000
          this.w_TOTALENA = this.w_TOTALENA + 0.0000
        case USRTMP1.TRTIPDET="T"
          * --- il totale punta su un totale
          select totali
          GO TOP
          LOCATE FOR TOTALI.CPROWORD= this.w_RIGA
          if FOUND()
            this.w_TOTALE = this.w_TOTALE+IIF(this.w_SEGNO="+",1,-1)*nvl(totali.TOTALE,0.0000)
            this.w_TOTALENA = this.w_TOTALENA+IIF(this.w_SEGNO="+",1,-1)*nvl(totali.TOTALENA,0.0000)
          else
            * --- Calcola sulle righe totalizzanti
            select (vecchiaArea)
            this.w_RIGA = USRTMP1.CPROWORD
            vq_exec("query\STRUTBIL",this,"RIGTOT11")
            if USED("RIGTOT11")
              select RIGTOT11
              GO TOP
              SCAN
              this.w_RIGA11 = RTRIGTOT
              this.w_SEGNO11 = RT_SEGNO
              select (vecchiaArea)
              GO TOP
              LOCATE FOR USRTMP1.CPROWORD1= this.w_RIGA11
              do case
                case USRTMP1.TRTIPDET="V"
                  this.w_TOTALE = this.w_TOTALE+IIF(this.w_SEGNO11="+",1,-1)*(NVL(USRTMP1.tot1,0.0000))
                  this.w_TOTALENA = this.w_TOTALENA+IIF(this.w_SEGNO11="+",1,-1)*(NVL(USRTMP1.tot2,0.0000))
                case USRTMP1.TRTIPDET="T"
                  this.w_MESS = "Riga %1: %2 %0ha la sequenza di calcolo errata, � possibile totalizzare soltanto righe gi� calcolate"
                  ah_errormsg(this.w_MESS,48,"",this.w_PAR1,this.w_PAR2)
                  EXIT
              endcase
              select RIGTOT11
              ENDSCAN
              select RIGTOT11
              USE
            endif
          endif
          select (vecchiaArea)
        case USRTMP1.TRTIPDET="V"
          * --- Punta su una Voce
          * --- leggo l'importo legato alla voce
          this.w_TOTALE = this.w_TOTALE+IIF(this.w_SEGNO="+",1,-1)*(NVL(USRTMP1.tot1,0.000))
          this.w_TOTALENA = this.w_TOTALENA+IIF(this.w_SEGNO="+",1,-1)*(NVL(USRTMP1.tot2,0.000))
      endcase
      go posizione
      select (righeArea)
        select _Curs_RIGTOTAL
        continue
      enddo
      use
    endif
    * --- Mi rimetto sul cursore
    select (vecchiaArea)
    * --- Aggiungo il totalizzatore
    select totali
    append blank
    replace CPROWNUM with USRTMP1.CPROWNUM
    replace CPROWORD with USRTMP1.CPROWORD1
    replace TOTALE with cp_ROUND(this.w_TOTALE,this.oParentObject.w_DECIMI)
    replace TOTALENA with cp_ROUND(this.w_TOTALENA,this.oParentObject.w_DECIMI)
    select (vecchiaArea)
    ENDSCAN
    * --- Aggiungo i totali
    Select usrtmp1.*, totali.totale, totali.totalena from usrtmp1 left outer join ; 
 totali on totali.cprownum=usrtmp1.cprownum into cursor usrtmp2 nofilter
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento Manutenzione Bilancio UE
    * --- Determino seriale manutenzione da eliminare
    * --- Read from BIL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.BIL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BIL_MAST_idx,2],.t.,this.BIL_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SLSERIAL"+;
        " from "+i_cTable+" BIL_MAST where ";
            +"SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_ESE);
            +" and SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_BILANCIO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SLSERIAL;
        from (i_cTable) where;
            SLCODESE = this.oParentObject.w_ESE;
            and SLCODICE = this.oParentObject.w_BILANCIO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SERSAL = NVL(cp_ToDate(_read_.SLSERIAL),cp_NullValue(_read_.SLSERIAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not Empty(this.w_SERSAL)
      * --- Elimino saldi esercizio
      * --- Delete from BIL_DETT
      i_nConn=i_TableProp[this.BIL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BIL_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"SLSERIAL = "+cp_ToStrODBC(this.w_SERSAL);
               )
      else
        delete from (i_cTable) where;
              SLSERIAL = this.w_SERSAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from BIL_MAST
      i_nConn=i_TableProp[this.BIL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BIL_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"SLSERIAL = "+cp_ToStrODBC(this.w_SERSAL);
               )
      else
        delete from (i_cTable) where;
              SLSERIAL = this.w_SERSAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    this.w_SLSERIAL = Space(10)
    * --- Try
    local bErr_047D8670
    bErr_047D8670=bTrsErr
    this.Try_047D8670()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_047D8670
    * --- End
    * --- Insert into BIL_DETT
    i_nConn=i_TableProp[this.BIL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BIL_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\gscg_sal",this.BIL_DETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nella inserzione dettaglio manutenzione bilancio EU'
      return
    endif
    * --- Quando si stampa il bilancio per confronto TMP_PAR contiene i dati di 2 esercizi e pagina 6 viene chiamata 2 volte (una per ogni esercizio), solo dopo la seconda chiamata TMP_PAR pu� essere cancellata
    if NOT this.w_INSERISCI
      * --- Drop temporary table TMP_PART
      i_nIdx=cp_GetTableDefIdx('TMP_PART')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PART')
      endif
    endif
  endproc
  proc Try_047D8670()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.BIL_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEBIL", "i_codazi,w_SLSERIAL")
    * --- Insert into BIL_MAST
    i_nConn=i_TableProp[this.BIL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BIL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.BIL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLSERIAL"+",SLCODICE"+",SLCODESE"+",SLDESCRI"+",UTCC"+",UTDC"+",SL__NOTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SLSERIAL),'BIL_MAST','SLSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_BILANCIO),'BIL_MAST','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ESE),'BIL_MAST','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Tmpc),'BIL_MAST','SLDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(i_codute),'BIL_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'BIL_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_BILDES),'BIL_MAST','SL__NOTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLSERIAL',this.w_SLSERIAL,'SLCODICE',this.oParentObject.w_BILANCIO,'SLCODESE',this.oParentObject.w_ESE,'SLDESCRI',this.w_Tmpc,'UTCC',i_codute,'UTDC',SetInfoDate(g_CALUTD),'SL__NOTE',this.oParentObject.w_BILDES)
      insert into (i_cTable) (SLSERIAL,SLCODICE,SLCODESE,SLDESCRI,UTCC,UTDC,SL__NOTE &i_ccchkf. );
         values (;
           this.w_SLSERIAL;
           ,this.oParentObject.w_BILANCIO;
           ,this.oParentObject.w_ESE;
           ,this.w_Tmpc;
           ,i_codute;
           ,SetInfoDate(g_CALUTD);
           ,this.oParentObject.w_BILDES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nella inserzione testata manutenzione bilancio EU'
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='RIGTOTAL'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='*TMPMOVIMAST'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='SEDIAZIE'
    this.cWorkTables[7]='TIR_MAST'
    this.cWorkTables[8]='BIL_MAST'
    this.cWorkTables[9]='BIL_DETT'
    this.cWorkTables[10]='*TMP_PART'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_SEDIAZIE')
      use in _Curs_SEDIAZIE
    endif
    if used('_Curs_RIGTOTAL')
      use in _Curs_RIGTOTAL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
