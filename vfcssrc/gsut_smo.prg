* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_smo                                                        *
*              Stampa accessi                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_72]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-11                                                      *
* Last revis.: 2008-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_smo
*Questa gestione la pu� usare solo l'amministratore
if not cp_IsAdministrator(.t.)
  AH_ERRORMSG("Accesso negato",'stop',"Gestione sicurezza")
  return null
endif
* --- Fine Area Manuale
return(createobject("tgsut_smo",oParentObject))

* --- Class definition
define class tgsut_smo as StdForm
  Top    = 27
  Left   = 69

  * --- Standard Properties
  Width  = 485
  Height = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-27"
  HelpContextID=76987543
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  CPGROUPS_IDX = 0
  TSMENUVO_IDX = 0
  CPUSERS_IDX = 0
  OUT_PUTS_IDX = 0
  cPrg = "gsut_smo"
  cComment = "Stampa accessi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPACCES = space(1)
  o_TIPACCES = space(1)
  w_CODGRU = 0
  w_CODEUSE = 0
  w_MODULO2 = space(2)
  w_MODULO = space(4)
  o_MODULO = space(4)
  w_MODULO = space(2)
  w_PROGRAM = space(10)
  w_CODEDESC = space(40)
  w_DESCPROG = space(30)
  w_CODREL = space(10)
  w_NUMROW = 0
  w_CODEDESU = space(40)
  w_PROGNAME = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_smoPag1","gsut_smo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPACCES_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CPGROUPS'
    this.cWorkTables[2]='TSMENUVO'
    this.cWorkTables[3]='CPUSERS'
    this.cWorkTables[4]='OUT_PUTS'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPACCES=space(1)
      .w_CODGRU=0
      .w_CODEUSE=0
      .w_MODULO2=space(2)
      .w_MODULO=space(4)
      .w_MODULO=space(2)
      .w_PROGRAM=space(10)
      .w_CODEDESC=space(40)
      .w_DESCPROG=space(30)
      .w_CODREL=space(10)
      .w_NUMROW=0
      .w_CODEDESU=space(40)
      .w_PROGNAME=space(30)
        .w_TIPACCES = 'G'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODGRU))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODEUSE))
          .link_1_3('Full')
        endif
        .w_MODULO2 = "0000"
        .w_MODULO = LEFT(IIF(.w_MODULO2="0000","    ",.w_MODULO2) + SPACE( 4 ) , 4 )
        .w_MODULO = ''
        .w_PROGRAM = SPACE( 10 )
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_PROGRAM))
          .link_1_7('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
          .DoRTCalc(8,9,.f.)
        .w_CODREL = ALLTRIM(STRTRAN(UPPER(g_VERSION),'REL.'))
        .w_NUMROW = IIF(.w_TIPACCES='G',1,2)
          .DoRTCalc(12,12,.f.)
        .w_PROGNAME = 'GSUT_SMO'
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_PROGNAME))
          .link_1_22('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_smo
    * TRADUCE LE DESCRIZIONI DEI MODULI
    DO GSUT_BMA WITH THIS
    
    * --- derivata dalla classe combo da tabella
    
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_MODULO2")
    CTRL_CONCON.Popola()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
            .w_MODULO = LEFT(IIF(.w_MODULO2="0000","    ",.w_MODULO2) + SPACE( 4 ) , 4 )
        .DoRTCalc(6,6,.t.)
        if .o_MODULO<>.w_MODULO
            .w_PROGRAM = SPACE( 10 )
          .link_1_7('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(8,10,.t.)
        if .o_TIPACCES<>.w_TIPACCES
            .w_NUMROW = IIF(.w_TIPACCES='G',1,2)
        endif
        .DoRTCalc(12,12,.t.)
        if .o_TIPACCES<>.w_TIPACCES
            .w_PROGNAME = 'GSUT_SMO'
          .link_1_22('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODGRU_1_2.visible=!this.oPgFrm.Page1.oPag.oCODGRU_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCODEUSE_1_3.visible=!this.oPgFrm.Page1.oPag.oCODEUSE_1_3.mHide()
    this.oPgFrm.Page1.oPag.oMODULO2_1_4.visible=!this.oPgFrm.Page1.oPag.oMODULO2_1_4.mHide()
    this.oPgFrm.Page1.oPag.oMODULO_1_6.visible=!this.oPgFrm.Page1.oPag.oMODULO_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCODEDESC_1_11.visible=!this.oPgFrm.Page1.oPag.oCODEDESC_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCODEDESU_1_21.visible=!this.oPgFrm.Page1.oPag.oCODEDESU_1_21.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODGRU
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODGRU);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODGRU)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oCODGRU_1_2'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODGRU)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.CODE,0)
      this.w_CODEDESC = NVL(_Link_.NAME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = 0
      endif
      this.w_CODEDESC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODEUSE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODEUSE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODEUSE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODEUSE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODEUSE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCODEUSE_1_3'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODEUSE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODEUSE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODEUSE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODEUSE = NVL(_Link_.CODE,0)
      this.w_CODEDESU = NVL(_Link_.NAME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODEUSE = 0
      endif
      this.w_CODEDESU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODEUSE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PROGRAM
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TSMENUVO_IDX,3]
    i_lTable = "TSMENUVO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TSMENUVO_IDX,2], .t., this.TSMENUVO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TSMENUVO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROGRAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TSMENUVO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PROGRAM like "+cp_ToStrODBC(trim(this.w_PROGRAM)+"%");

          i_ret=cp_SQL(i_nConn,"select PROGRAM,PROGDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PROGRAM","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PROGRAM',trim(this.w_PROGRAM))
          select PROGRAM,PROGDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PROGRAM into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROGRAM)==trim(_Link_.PROGRAM) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROGRAM) and !this.bDontReportError
            deferred_cp_zoom('TSMENUVO','*','PROGRAM',cp_AbsName(oSource.parent,'oPROGRAM_1_7'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PROGRAM,PROGDES";
                     +" from "+i_cTable+" "+i_lTable+" where PROGRAM="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PROGRAM',oSource.xKey(1))
            select PROGRAM,PROGDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROGRAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PROGRAM,PROGDES";
                   +" from "+i_cTable+" "+i_lTable+" where PROGRAM="+cp_ToStrODBC(this.w_PROGRAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PROGRAM',this.w_PROGRAM)
            select PROGRAM,PROGDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROGRAM = NVL(_Link_.PROGRAM,space(10))
      this.w_DESCPROG = NVL(_Link_.PROGDES,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PROGRAM = space(10)
      endif
      this.w_DESCPROG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TSMENUVO_IDX,2])+'\'+cp_ToStr(_Link_.PROGRAM,1)
      cp_ShowWarn(i_cKey,this.TSMENUVO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROGRAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PROGNAME
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_lTable = "OUT_PUTS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2], .t., this.OUT_PUTS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROGNAME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROGNAME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUROWNUM,OUNOMPRG,OUNOMQUE,OUNOMREP,OUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where OUNOMPRG="+cp_ToStrODBC(this.w_PROGNAME);
                   +" and OUROWNUM="+cp_ToStrODBC(this.w_NUMROW);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUROWNUM',this.w_NUMROW;
                       ,'OUNOMPRG',this.w_PROGNAME)
            select OUROWNUM,OUNOMPRG,OUNOMQUE,OUNOMREP,OUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROGNAME = NVL(_Link_.OUNOMPRG,space(30))
      this.w_OQRY = NVL(_Link_.OUNOMQUE,.null.)
      this.w_OREP = NVL(_Link_.OUNOMREP,.null.)
      this.w_ODES = NVL(_Link_.OUDESCRI,.null.)
    else
      if i_cCtrl<>'Load'
        this.w_PROGNAME = space(30)
      endif
      this.w_OQRY = .null.
      this.w_OREP = .null.
      this.w_ODES = .null.
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])+'\'+cp_ToStr(_Link_.OUROWNUM,1)+'\'+cp_ToStr(_Link_.OUNOMPRG,1)
      cp_ShowWarn(i_cKey,this.OUT_PUTS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROGNAME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPACCES_1_1.RadioValue()==this.w_TIPACCES)
      this.oPgFrm.Page1.oPag.oTIPACCES_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_2.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_2.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEUSE_1_3.value==this.w_CODEUSE)
      this.oPgFrm.Page1.oPag.oCODEUSE_1_3.value=this.w_CODEUSE
    endif
    if not(this.oPgFrm.Page1.oPag.oMODULO2_1_4.RadioValue()==this.w_MODULO2)
      this.oPgFrm.Page1.oPag.oMODULO2_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMODULO_1_6.RadioValue()==this.w_MODULO)
      this.oPgFrm.Page1.oPag.oMODULO_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGRAM_1_7.value==this.w_PROGRAM)
      this.oPgFrm.Page1.oPag.oPROGRAM_1_7.value=this.w_PROGRAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEDESC_1_11.value==this.w_CODEDESC)
      this.oPgFrm.Page1.oPag.oCODEDESC_1_11.value=this.w_CODEDESC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCPROG_1_16.value==this.w_DESCPROG)
      this.oPgFrm.Page1.oPag.oDESCPROG_1_16.value=this.w_DESCPROG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEDESU_1_21.value==this.w_CODEDESU)
      this.oPgFrm.Page1.oPag.oCODEDESU_1_21.value=this.w_CODEDESU
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPACCES = this.w_TIPACCES
    this.o_MODULO = this.w_MODULO
    return

enddefine

* --- Define pages as container
define class tgsut_smoPag1 as StdContainer
  Width  = 481
  height = 202
  stdWidth  = 481
  stdheight = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPACCES_1_1 as StdCombo with uid="UWLAVTTLVD",rtseq=1,rtrep=.f.,left=88,top=6,width=78,height=21;
    , ToolTipText = "Seleziona i tipi di accesso gruppo/utente";
    , HelpContextID = 66253943;
    , cFormVar="w_TIPACCES",RowSource=""+"Gruppo,"+"Utente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPACCES_1_1.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'U',;
    space(1))))
  endfunc
  func oTIPACCES_1_1.GetRadio()
    this.Parent.oContained.w_TIPACCES = this.RadioValue()
    return .t.
  endfunc

  func oTIPACCES_1_1.SetRadio()
    this.Parent.oContained.w_TIPACCES=trim(this.Parent.oContained.w_TIPACCES)
    this.value = ;
      iif(this.Parent.oContained.w_TIPACCES=='G',1,;
      iif(this.Parent.oContained.w_TIPACCES=='U',2,;
      0))
  endfunc

  add object oCODGRU_1_2 as StdField with uid="GFGCEOZCDI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice del gruppo.",;
    HelpContextID = 16625626,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=88, Top=33, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPACCES='U')
    endwith
  endfunc

  func oCODGRU_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oCODGRU_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oCODEUSE_1_3 as StdField with uid="DCUMQVCKNT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODEUSE", cQueryName = "CODEUSE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 221270054,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=88, Top=33, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CODEUSE"

  func oCODEUSE_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPACCES='G')
    endwith
  endfunc

  func oCODEUSE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODEUSE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODEUSE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCODEUSE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc


  add object oMODULO2_1_4 as StdZTamTableCombo with uid="YGTRMCJOAH",rtseq=4,rtrep=.f.,left=88,top=61,width=235,height=21;
    , ToolTipText = "Seleziona un modulo.";
    , HelpContextID = 122662714;
    , cFormVar="w_MODULO2",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='QUERY\ELENMODU.VQR',cKey='MOCODMOD',cValue='MOTRADES',cOrderBy='MOORDINA',xDefault=space(2);
  , bGlobalFont=.t.


  func oMODULO2_1_4.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
    endwith
  endfunc


  add object oMODULO_1_6 as StdCombo with uid="YIFNJUVGPE",value=1,rtseq=6,rtrep=.f.,left=88,top=61,width=235,height=21;
    , ToolTipText = "Seleziona un modulo.";
    , HelpContextID = 122662714;
    , cFormVar="w_MODULO",RowSource=""+"Tutti,"+"Acquisti/vendite,"+"Analitica,"+"Analisi di bilancio,"+"Archivi,"+"Budget vendite,"+"Cespiti,"+"Cicli di lavor.,"+"Consolidato,"+"Cont. generale,"+"Corporate Portal,"+"C/Lavoro,"+"Doc.management,"+"eCRM,"+"ePOS,"+"Flussi e aut.,"+"Import,"+"InfoLink,"+"InfoReader,"+"Magazzino,"+"MRP,"+"Offerte,"+"Prod.mod.base,"+"Prod. schedulatore,"+"Prod.su commessa,"+"Raccolta/analisi dati,"+"Remote Banking,"+"Ritenute,"+"Schedulatore job,"+"Sistema,"+"Statistiche,"+"Tesoreria", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMODULO_1_6.RadioValue()
    return(iif(this.value =1,'',;
    iif(this.value =2,'VEND',;
    iif(this.value =3,'COAN',;
    iif(this.value =4,'BILC',;
    iif(this.value =5,'ARCH',;
    iif(this.value =6,'BUVE',;
    iif(this.value =7,'CESP',;
    iif(this.value =8,'CILA',;
    iif(this.value =9,'BCON',;
    iif(this.value =10,'COGE',;
    iif(this.value =11,'IZCP',;
    iif(this.value =12,'COLA',;
    iif(this.value =13,'DOCM',;
    iif(this.value =14,'ECRM',;
    iif(this.value =15,'EPOS',;
    iif(this.value =16,'WFLD',;
    iif(this.value =17,'IMPO',;
    iif(this.value =18,'INFO',;
    iif(this.value =19,'IRDR',;
    iif(this.value =20,'MAGA',;
    iif(this.value =21,'MRP',;
    iif(this.value =22,'OFFE',;
    iif(this.value =23,'PROD',;
    iif(this.value =24,'SCCI',;
    iif(this.value =25,'COMM',;
    iif(this.value =26,'RIAN',;
    iif(this.value =27,'REBA',;
    iif(this.value =28,'RITE',;
    iif(this.value =29,'JBSH',;
    iif(this.value =30,'UTIL',;
    iif(this.value =31,'STAT',;
    iif(this.value =32,'TESO',;
    space(2))))))))))))))))))))))))))))))))))
  endfunc
  func oMODULO_1_6.GetRadio()
    this.Parent.oContained.w_MODULO = this.RadioValue()
    return .t.
  endfunc

  func oMODULO_1_6.SetRadio()
    this.Parent.oContained.w_MODULO=trim(this.Parent.oContained.w_MODULO)
    this.value = ;
      iif(this.Parent.oContained.w_MODULO=='',1,;
      iif(this.Parent.oContained.w_MODULO=='VEND',2,;
      iif(this.Parent.oContained.w_MODULO=='COAN',3,;
      iif(this.Parent.oContained.w_MODULO=='BILC',4,;
      iif(this.Parent.oContained.w_MODULO=='ARCH',5,;
      iif(this.Parent.oContained.w_MODULO=='BUVE',6,;
      iif(this.Parent.oContained.w_MODULO=='CESP',7,;
      iif(this.Parent.oContained.w_MODULO=='CILA',8,;
      iif(this.Parent.oContained.w_MODULO=='BCON',9,;
      iif(this.Parent.oContained.w_MODULO=='COGE',10,;
      iif(this.Parent.oContained.w_MODULO=='IZCP',11,;
      iif(this.Parent.oContained.w_MODULO=='COLA',12,;
      iif(this.Parent.oContained.w_MODULO=='DOCM',13,;
      iif(this.Parent.oContained.w_MODULO=='ECRM',14,;
      iif(this.Parent.oContained.w_MODULO=='EPOS',15,;
      iif(this.Parent.oContained.w_MODULO=='WFLD',16,;
      iif(this.Parent.oContained.w_MODULO=='IMPO',17,;
      iif(this.Parent.oContained.w_MODULO=='INFO',18,;
      iif(this.Parent.oContained.w_MODULO=='IRDR',19,;
      iif(this.Parent.oContained.w_MODULO=='MAGA',20,;
      iif(this.Parent.oContained.w_MODULO=='MRP',21,;
      iif(this.Parent.oContained.w_MODULO=='OFFE',22,;
      iif(this.Parent.oContained.w_MODULO=='PROD',23,;
      iif(this.Parent.oContained.w_MODULO=='SCCI',24,;
      iif(this.Parent.oContained.w_MODULO=='COMM',25,;
      iif(this.Parent.oContained.w_MODULO=='RIAN',26,;
      iif(this.Parent.oContained.w_MODULO=='REBA',27,;
      iif(this.Parent.oContained.w_MODULO=='RITE',28,;
      iif(this.Parent.oContained.w_MODULO=='JBSH',29,;
      iif(this.Parent.oContained.w_MODULO=='UTIL',30,;
      iif(this.Parent.oContained.w_MODULO=='STAT',31,;
      iif(this.Parent.oContained.w_MODULO=='TESO',32,;
      0))))))))))))))))))))))))))))))))
  endfunc

  func oMODULO_1_6.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oPROGRAM_1_7 as StdField with uid="GOBDJKCQKR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PROGRAM", cQueryName = "PROGRAM",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Nome del programma.",;
    HelpContextID = 184746998,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=88, Top=91, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TSMENUVO", oKey_1_1="PROGRAM", oKey_1_2="this.w_PROGRAM"

  func oPROGRAM_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROGRAM_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROGRAM_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TSMENUVO','*','PROGRAM',cp_AbsName(this.parent,'oPROGRAM_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=88, top=129, width=386,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 13450266


  add object oBtn_1_9 as StdButton with uid="ACLZZHBAEB",left=370, top=154, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 100501994;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="TLAGCYFKXM",left=424, top=154, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 100501994;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODEDESC_1_11 as StdField with uid="GJBTCBKGWH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODEDESC", cQueryName = "CODEDESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del gruppo.",;
    HelpContextID = 31436695,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=145, Top=33, InputMask=replicate('X',40)

  func oCODEDESC_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPACCES='U')
    endwith
  endfunc

  add object oDESCPROG_1_16 as StdField with uid="MRUJCUDHPV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCPROG", cQueryName = "DESCPROG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione programma.",;
    HelpContextID = 69257603,;
   bGlobalFont=.t.,;
    Height=21, Width=277, Left=188, Top=91, InputMask=replicate('X',30)

  add object oCODEDESU_1_21 as StdField with uid="GHVHHYURKG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODEDESU", cQueryName = "CODEDESU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 31436677,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=145, Top=33, InputMask=replicate('X',40)

  func oCODEDESU_1_21.mHide()
    with this.Parent.oContained
      return (.w_TIPACCES='G')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="GGCOXPIBMF",Visible=.t., Left=4, Top=37,;
    Alignment=1, Width=81, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPACCES='U')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="WZTMYXGKQC",Visible=.t., Left=4, Top=64,;
    Alignment=1, Width=81, Height=18,;
    Caption="Moduli:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="HYGLZADXXE",Visible=.t., Left=4, Top=133,;
    Alignment=1, Width=81, Height=18,;
    Caption="Tipo stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="MXUJBZBATF",Visible=.t., Left=4, Top=95,;
    Alignment=1, Width=81, Height=18,;
    Caption="Programma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="NSWLPSAYKZ",Visible=.t., Left=4, Top=10,;
    Alignment=1, Width=81, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="AQWBVKLFVW",Visible=.t., Left=4, Top=37,;
    Alignment=1, Width=81, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_TIPACCES='G')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_smo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_smo
* --- Classe per gestire la combo dei moduli attivati
* --- derivata dalla classe combo da tabella

define class StdZTamTableCombo as StdTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF

endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif

enddefine
* --- Fine Area Manuale
