* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bl3                                                        *
*              Inserimento listini                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_90]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-17                                                      *
* Last revis.: 2010-01-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bl3",oParentObject)
return(i_retval)

define class tgsma_bl3 as StdBatch
  * --- Local variables
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_LIDATATT = ctod("  /  /  ")
  w_LIQUANTI = 0
  w_LIDATDIS = ctod("  /  /  ")
  w_LIPREZZO = 0
  w_LICODART = space(20)
  w_CPROWNUM = 0
  w_LIROWNUM = 0
  w_LIUNIMIS = space(3)
  w_OLDART = space(20)
  w_NEWART = space(20)
  w_ROWNUM = 0
  * --- WorkFile variables
  INS_LIST_idx=0
  LIS_TINI_idx=0
  INS_SCAG_idx=0
  LIS_SCAG_idx=0
  LISTINI_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce Definivamente i Listini (da GSMA_ML2)
    ah_Msg("Aggiornamento listino articolo...")
    * --- Legge valori massimi di CPROWNUM nei Lis_tini
    vq_exec("query\GSMA_BL3",this,"RIGHE")
    this.w_CPROWNUM = 0
    this.w_ROWNUM = 0
    this.w_OLDART = "%%%%%%%%"
    * --- Select from INS_LIST
    i_nConn=i_TableProp[this.INS_LIST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INS_LIST_idx,2],.t.,this.INS_LIST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" INS_LIST ";
          +" where LICODLIS="+cp_ToStrODBC(this.oParentObject.w_LICODLIS)+"";
           ,"_Curs_INS_LIST")
    else
      select * from (i_cTable);
       where LICODLIS=this.oParentObject.w_LICODLIS;
        into cursor _Curs_INS_LIST
    endif
    if used('_Curs_INS_LIST')
      select _Curs_INS_LIST
      locate for 1=1
      do while not(eof())
      this.w_OLDART = NVL(_Curs_INS_LIST.LICODART," ")
      this.w_LIDATATT = CP_TODATE(_Curs_INS_LIST.LIDATATT)
      this.w_LIDATDIS = CP_TODATE(_Curs_INS_LIST.LIDATDIS)
      this.w_LICODART = NVL(_Curs_INS_LIST.LICODART," ")
      this.w_LIROWNUM = _Curs_INS_LIST.CPROWNUM
      this.w_LIUNIMIS = NVL(_Curs_INS_LIST.LIUNIMIS, SPACE(3) )
      * --- Elimina Eventuale Listino Precedente
      this.w_CPROWNUM = 0
      * --- Read from LIS_TINI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LIS_TINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CPROWNUM"+;
          " from "+i_cTable+" LIS_TINI where ";
              +"LICODART = "+cp_ToStrODBC(this.w_LICODART);
              +" and LICODLIS = "+cp_ToStrODBC(this.oParentObject.w_LICODLIS);
              +" and LIDATATT = "+cp_ToStrODBC(this.w_LIDATATT);
              +" and LIDATDIS = "+cp_ToStrODBC(this.w_LIDATDIS);
              +" and LIUNIMIS = "+cp_ToStrODBC(this.w_LIUNIMIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CPROWNUM;
          from (i_cTable) where;
              LICODART = this.w_LICODART;
              and LICODLIS = this.oParentObject.w_LICODLIS;
              and LIDATATT = this.w_LIDATATT;
              and LIDATDIS = this.w_LIDATDIS;
              and LIUNIMIS = this.w_LIUNIMIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from LISTINI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LISTINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LSFLSCON"+;
          " from "+i_cTable+" LISTINI where ";
              +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_LICODLIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LSFLSCON;
          from (i_cTable) where;
              LSCODLIS = this.oParentObject.w_LICODLIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_FLSCO = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.w_LIUNIMIS)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_LICODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1;
            from (i_cTable) where;
                ARCODART = this.w_LICODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LIUNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if i_Rows<>0 AND this.w_CPROWNUM<>0
        * --- Delete from LIS_SCAG
        i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"LICODART = "+cp_ToStrODBC(this.w_LICODART);
                +" and LIROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          delete from (i_cTable) where;
                LICODART = this.w_LICODART;
                and LIROWNUM = this.w_CPROWNUM;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from LIS_TINI
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"LICODART = "+cp_ToStrODBC(this.w_LICODART);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          delete from (i_cTable) where;
                LICODART = this.w_LICODART;
                and CPROWNUM = this.w_CPROWNUM;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- legge Max CPROWNUM
      if this.w_OLDART<>this.w_NEWART
        this.w_ROWNUM = 0
        if USED("RIGHE") 
          SELECT RIGHE
          GO TOP
          LOCATE FOR CODART=this.w_LICODART
          if FOUND()
            this.w_ROWNUM = NVL(MAXRIG,0)
          endif
          SELECT _Curs_INS_LIST
        endif
      endif
      this.w_NEWART = this.w_OLDART
      this.w_ROWNUM = this.w_ROWNUM + 1
      ah_Msg("Aggiornamento listino articolo...%1",.T.,.F.,.F.,this.w_LICODART)
      * --- Scrive Nuovo Listino
      * --- Try
      local bErr_03B5C808
      bErr_03B5C808=bTrsErr
      this.Try_03B5C808()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03B5C808
      * --- End
        select _Curs_INS_LIST
        continue
      enddo
      use
    endif
    ah_ErrorMsg("Inserimento completato")
    * --- Elimina INS_SCAG e INS_LIST
    * --- Delete from INS_SCAG
    i_nConn=i_TableProp[this.INS_SCAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INS_SCAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from INS_LIST
    i_nConn=i_TableProp[this.INS_LIST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INS_LIST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if USED("RIGHE")
      SELECT RIGHE
      USE
    endif
  endproc
  proc Try_03B5C808()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into LIS_TINI
    i_nConn=i_TableProp[this.LIS_TINI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LICODART"+",CPROWNUM"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+",LIUNIMIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LICODART),'LIS_TINI','LICODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_TINI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LICODLIS),'LIS_TINI','LICODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LIDATATT),'LIS_TINI','LIDATATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LIDATDIS),'LIS_TINI','LIDATDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LIUNIMIS),'LIS_TINI','LIUNIMIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_LICODART,'CPROWNUM',this.w_ROWNUM,'LICODLIS',this.oParentObject.w_LICODLIS,'LIDATATT',this.w_LIDATATT,'LIDATDIS',this.w_LIDATDIS,'LIUNIMIS',this.w_LIUNIMIS)
      insert into (i_cTable) (LICODART,CPROWNUM,LICODLIS,LIDATATT,LIDATDIS,LIUNIMIS &i_ccchkf. );
         values (;
           this.w_LICODART;
           ,this.w_ROWNUM;
           ,this.oParentObject.w_LICODLIS;
           ,this.w_LIDATATT;
           ,this.w_LIDATDIS;
           ,this.w_LIUNIMIS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Select from INS_SCAG
    i_nConn=i_TableProp[this.INS_SCAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INS_SCAG_idx,2],.t.,this.INS_SCAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" INS_SCAG ";
          +" where LICODLIS="+cp_ToStrODBC(this.oParentObject.w_LICODLIS)+" AND LIROWNUM="+cp_ToStrODBC(this.w_LIROWNUM)+"";
           ,"_Curs_INS_SCAG")
    else
      select * from (i_cTable);
       where LICODLIS=this.oParentObject.w_LICODLIS AND LIROWNUM=this.w_LIROWNUM;
        into cursor _Curs_INS_SCAG
    endif
    if used('_Curs_INS_SCAG')
      select _Curs_INS_SCAG
      locate for 1=1
      do while not(eof())
      this.w_LIQUANTI = NVL(_Curs_INS_SCAG.LIQUANTI, 0)
      this.w_LIPREZZO = NVL(_Curs_INS_SCAG.LIPREZZO, 0)
      this.w_LISCONT1 = NVL(_Curs_INS_SCAG.LISCONT1, 0)
      this.w_LISCONT2 = NVL(_Curs_INS_SCAG.LISCONT2, 0)
      this.w_LISCONT3 = NVL(_Curs_INS_SCAG.LISCONT3, 0)
      this.w_LISCONT4 = NVL(_Curs_INS_SCAG.LISCONT4, 0)
      * --- Insert into LIS_SCAG
      i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_LICODART),'LIS_SCAG','LICODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LIQUANTI),'LIS_SCAG','LIQUANTI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LIPREZZO),'LIS_SCAG','LIPREZZO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_LICODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_LIQUANTI,'LIPREZZO',this.w_LIPREZZO)
        insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO &i_ccchkf. );
           values (;
             this.w_LICODART;
             ,this.w_ROWNUM;
             ,this.w_LIQUANTI;
             ,this.w_LIPREZZO;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      if this.oParentObject.w_FLSCO="S"
        * --- Write into LIS_SCAG
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LISCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
          +",LISCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
          +",LISCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
          +",LISCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
              +i_ccchkf ;
          +" where ";
              +"LICODART = "+cp_ToStrODBC(this.w_LICODART);
              +" and LIROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
              +" and LIQUANTI = "+cp_ToStrODBC(this.w_LIQUANTI);
                 )
        else
          update (i_cTable) set;
              LISCONT1 = this.w_LISCONT1;
              ,LISCONT2 = this.w_LISCONT2;
              ,LISCONT3 = this.w_LISCONT3;
              ,LISCONT4 = this.w_LISCONT4;
              &i_ccchkf. ;
           where;
              LICODART = this.w_LICODART;
              and LIROWNUM = this.w_ROWNUM;
              and LIQUANTI = this.w_LIQUANTI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_INS_SCAG
        continue
      enddo
      use
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='INS_LIST'
    this.cWorkTables[2]='LIS_TINI'
    this.cWorkTables[3]='INS_SCAG'
    this.cWorkTables[4]='LIS_SCAG'
    this.cWorkTables[5]='LISTINI'
    this.cWorkTables[6]='ART_ICOL'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_INS_LIST')
      use in _Curs_INS_LIST
    endif
    if used('_Curs_INS_SCAG')
      use in _Curs_INS_SCAG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
