* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bse                                                        *
*              Lancia gestione sede                                            *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_14]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-29                                                      *
* Last revis.: 2002-11-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pCodSed
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bse",oParentObject,m.pTipo,m.pCodSed)
return(i_retval)

define class tgsar_bse as StdBatch
  * --- Local variables
  w_RESULT = .f.
  pTipo = space(1)
  pCodSed = space(5)
  w_GEST = .NULL.
  w_ND = 0
  w_ALFA = space(10)
  w_DC = ctod("  /  /  ")
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apre la sede (A) o verifica sua eliminabilit� (D)..
    *     Secondo parametro il codice della Sede
    do case
      case this.pTipo="A"
        this.w_GEST = GSAR_ASE()
        if !( this.w_GEST.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          i_retcode = 'stop'
          return
        endif
        if Not Empty( this.pCodSed )
          * --- Se sono posizionato su un record apro la sede in interrogazione. ..
          this.w_GEST.w_SECODAZI = i_CODAZI
          this.w_GEST.w_SECODDES = this.pCodSed
          this.w_GEST.QueryKeySet("SECODAZI='"+ i_CODAZI + "' AND "+"SECODDES='"+ this.pCodSed +"'","")     
          this.w_GEST.LoadRecWarn()     
        else
          * --- ...altrimenti apro le sedi in caricamento
          this.w_GEST.ecpLoad()     
        endif
      case this.pTipo="D"
        * --- Verifico se la sede � cancellabile (non utilizzata nei documenti)
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVNUMDOC,MVALFDOC,MVDATDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVCODSED = "+cp_ToStrODBC(this.pCodSed);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVNUMDOC,MVALFDOC,MVDATDOC;
            from (i_cTable) where;
                MVCODSED = this.pCodSed;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ND = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_ALFA = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          this.w_DC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.oParentObject.w_MESS_ERR = ah_Msgformat("Impossibile eliminare sede utilizzata (n. documento %1 del %2)", ALLTRIM(STR(this.w_ND,15,0))+IIF(NOT EMPTY(this.w_ALFA),"/"+Alltrim(this.w_ALFA), "") , DTOC(this.w_DC) )
          this.w_RESULT = .F.
        else
          this.w_RESULT = .T.
        endif
        i_retcode = 'stop'
        i_retval = this.w_RESULT
        return
    endcase
  endproc


  proc Init(oParentObject,pTipo,pCodSed)
    this.pTipo=pTipo
    this.pCodSed=pCodSed
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pCodSed"
endproc
