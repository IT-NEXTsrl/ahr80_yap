* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mou                                                        *
*              Mappatura campi Outlook                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-07-09                                                      *
* Last revis.: 2012-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mou"))

* --- Class definition
define class tgsar_mou as StdTrsForm
  Top    = 6
  Left   = 5

  * --- Standard Properties
  Width  = 789
  Height = 384+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-12-12"
  HelpContextID=164198249
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  MAP_SINC_IDX = 0
  MAP_OUTL_IDX = 0
  XDC_FIELDS_IDX = 0
  cFile = "MAP_SINC"
  cKeySelect = "MCCODAZI"
  cKeyWhere  = "MCCODAZI=this.w_MCCODAZI"
  cKeyDetail  = "MCCODAZI=this.w_MCCODAZI and MCNOMTAB=this.w_MCNOMTAB and MCGESTIO=this.w_MCGESTIO and MC_VERSO=this.w_MC_VERSO"
  cKeyWhereODBC = '"MCCODAZI="+cp_ToStrODBC(this.w_MCCODAZI)';

  cKeyDetailWhereODBC = '"MCCODAZI="+cp_ToStrODBC(this.w_MCCODAZI)';
      +'+" and MCNOMTAB="+cp_ToStrODBC(this.w_MCNOMTAB)';
      +'+" and MCGESTIO="+cp_ToStrODBC(this.w_MCGESTIO)';
      +'+" and MC_VERSO="+cp_ToStrODBC(this.w_MC_VERSO)';

  cKeyWhereODBCqualified = '"MAP_SINC.MCCODAZI="+cp_ToStrODBC(this.w_MCCODAZI)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MAP_SINC.CPROWORD '
  cPrg = "gsar_mou"
  cComment = "Mappatura campi Outlook"
  i_nRowNum = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ListaCampi = space(0)
  w_FiltroCampi = space(1)
  w_MCCODAZI = space(5)
  w_CPROWORD = 0
  w_MCNOMTAB = space(20)
  o_MCNOMTAB = space(20)
  w_MCGESTIO = space(10)
  w_MCTIPO_P = space(1)
  w_MCMSOUTL = space(50)
  w_MCEXPRES = space(0)
  w_MC_VERSO = space(1)
  w_CTRLTAB = space(15)
  w_CTRLTIPO = space(1)
  w_TipoElemOutl = space(1)
  w_Descri = space(150)
  w_MCFL_KEY = space(1)
  w_DesCampo = space(80)
  w_MCTIPEXP = space(1)
  w_CodiceAziendaDefault = space(5)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MAP_SINC','gsar_mou')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mouPag1","gsar_mou",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Mappatura")
      .Pages(1).HelpContextID = 255986360
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='MAP_OUTL'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='MAP_SINC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MAP_SINC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MAP_SINC_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MCCODAZI = NVL(MCCODAZI,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from MAP_SINC where MCCODAZI=KeySet.MCCODAZI
    *                            and MCNOMTAB=KeySet.MCNOMTAB
    *                            and MCGESTIO=KeySet.MCGESTIO
    *                            and MC_VERSO=KeySet.MC_VERSO
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.MAP_SINC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAP_SINC_IDX,2],this.bLoadRecFilter,this.MAP_SINC_IDX,"gsar_mou")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MAP_SINC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MAP_SINC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MAP_SINC '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MCCODAZI',this.w_MCCODAZI  )
      select * from (i_cTable) MAP_SINC where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ListaCampi = space(0)
        .w_FiltroCampi = 'S'
        .w_TipoElemOutl = 'C'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_MCCODAZI = NVL(MCCODAZI,space(5))
        .w_CodiceAziendaDefault = 'XXXXX'
        cp_LoadRecExtFlds(this,'MAP_SINC')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CTRLTAB = space(15)
          .w_CTRLTIPO = space(1)
          .w_Descri = space(150)
          .w_DesCampo = space(80)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MCNOMTAB = NVL(MCNOMTAB,space(20))
          .w_MCGESTIO = NVL(MCGESTIO,space(10))
          .link_2_3('Load')
          .w_MCTIPO_P = NVL(MCTIPO_P,space(1))
          .w_MCMSOUTL = NVL(MCMSOUTL,space(50))
          .link_2_5('Load')
          .w_MCEXPRES = NVL(MCEXPRES,space(0))
          .w_MC_VERSO = NVL(MC_VERSO,space(1))
          .w_MCFL_KEY = NVL(MCFL_KEY,space(1))
          .w_MCTIPEXP = NVL(MCTIPEXP,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace MCNOMTAB with .w_MCNOMTAB
          replace MCGESTIO with .w_MCGESTIO
          replace MC_VERSO with .w_MC_VERSO
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CodiceAziendaDefault = 'XXXXX'
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ListaCampi=space(0)
      .w_FiltroCampi=space(1)
      .w_MCCODAZI=space(5)
      .w_CPROWORD=10
      .w_MCNOMTAB=space(20)
      .w_MCGESTIO=space(10)
      .w_MCTIPO_P=space(1)
      .w_MCMSOUTL=space(50)
      .w_MCEXPRES=space(0)
      .w_MC_VERSO=space(1)
      .w_CTRLTAB=space(15)
      .w_CTRLTIPO=space(1)
      .w_TipoElemOutl=space(1)
      .w_Descri=space(150)
      .w_MCFL_KEY=space(1)
      .w_DesCampo=space(80)
      .w_MCTIPEXP=space(1)
      .w_CodiceAziendaDefault=space(5)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,1,.f.)
        .w_FiltroCampi = 'S'
        .w_MCCODAZI = .w_CodiceAziendaDefault
        .DoRTCalc(4,6,.f.)
        if not(empty(.w_MCGESTIO))
         .link_2_3('Full')
        endif
        .w_MCTIPO_P = 'C'
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_MCMSOUTL))
         .link_2_5('Full')
        endif
        .DoRTCalc(9,12,.f.)
        .w_TipoElemOutl = 'C'
        .DoRTCalc(14,17,.f.)
        .w_CodiceAziendaDefault = 'XXXXX'
      endif
    endwith
    cp_BlankRecExtFlds(this,'MAP_SINC')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMCEXPRES_2_6.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MAP_SINC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MAP_SINC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODAZI,"MCCODAZI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MAP_SINC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAP_SINC_IDX,2])
    i_lTable = "MAP_SINC"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MAP_SINC_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SO2 with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MCNOMTAB N(3);
      ,t_MCGESTIO C(10);
      ,t_MCMSOUTL C(50);
      ,t_MCEXPRES M(10);
      ,t_MC_VERSO N(3);
      ,t_Descri C(150);
      ,t_MCFL_KEY N(3);
      ,t_DesCampo C(80);
      ,t_MCTIPEXP N(3);
      ,MCNOMTAB C(20);
      ,MCGESTIO C(10);
      ,MC_VERSO C(1);
      ,t_MCTIPO_P C(1);
      ,t_CTRLTAB C(15);
      ,t_CTRLTIPO C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_moubodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCNOMTAB_2_2.controlsource=this.cTrsName+'.t_MCNOMTAB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCGESTIO_2_3.controlsource=this.cTrsName+'.t_MCGESTIO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCMSOUTL_2_5.controlsource=this.cTrsName+'.t_MCMSOUTL'
    this.oPgFRm.Page1.oPag.oMCEXPRES_2_6.controlsource=this.cTrsName+'.t_MCEXPRES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMC_VERSO_2_7.controlsource=this.cTrsName+'.t_MC_VERSO'
    this.oPgFRm.Page1.oPag.oDescri_2_10.controlsource=this.cTrsName+'.t_Descri'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCFL_KEY_2_11.controlsource=this.cTrsName+'.t_MCFL_KEY'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDesCampo_2_12.controlsource=this.cTrsName+'.t_DesCampo'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPEXP_2_13.controlsource=this.cTrsName+'.t_MCTIPEXP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(43)
    this.AddVLine(121)
    this.AddVLine(207)
    this.AddVLine(336)
    this.AddVLine(434)
    this.AddVLine(466)
    this.AddVLine(744)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MAP_SINC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAP_SINC_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAP_SINC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAP_SINC_IDX,2])
      *
      * insert into MAP_SINC
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MAP_SINC')
        i_extval=cp_InsertValODBCExtFlds(this,'MAP_SINC')
        i_cFldBody=" "+;
                  "(MCCODAZI,CPROWORD,MCNOMTAB,MCGESTIO,MCTIPO_P"+;
                  ",MCMSOUTL,MCEXPRES,MC_VERSO,MCFL_KEY,MCTIPEXP,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MCCODAZI)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_MCNOMTAB)+","+cp_ToStrODBCNull(this.w_MCGESTIO)+","+cp_ToStrODBC(this.w_MCTIPO_P)+;
             ","+cp_ToStrODBCNull(this.w_MCMSOUTL)+","+cp_ToStrODBC(this.w_MCEXPRES)+","+cp_ToStrODBC(this.w_MC_VERSO)+","+cp_ToStrODBC(this.w_MCFL_KEY)+","+cp_ToStrODBC(this.w_MCTIPEXP)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MAP_SINC')
        i_extval=cp_InsertValVFPExtFlds(this,'MAP_SINC')
        cp_CheckDeletedKey(i_cTable,0,'MCCODAZI',this.w_MCCODAZI,'MCNOMTAB',this.w_MCNOMTAB,'MCGESTIO',this.w_MCGESTIO,'MC_VERSO',this.w_MC_VERSO)
        INSERT INTO (i_cTable) (;
                   MCCODAZI;
                  ,CPROWORD;
                  ,MCNOMTAB;
                  ,MCGESTIO;
                  ,MCTIPO_P;
                  ,MCMSOUTL;
                  ,MCEXPRES;
                  ,MC_VERSO;
                  ,MCFL_KEY;
                  ,MCTIPEXP;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MCCODAZI;
                  ,this.w_CPROWORD;
                  ,this.w_MCNOMTAB;
                  ,this.w_MCGESTIO;
                  ,this.w_MCTIPO_P;
                  ,this.w_MCMSOUTL;
                  ,this.w_MCEXPRES;
                  ,this.w_MC_VERSO;
                  ,this.w_MCFL_KEY;
                  ,this.w_MCTIPEXP;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MAP_SINC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAP_SINC_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_MCNOMTAB)) AND not(Empty(t_MCGESTIO))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'MAP_SINC')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and MCNOMTAB="+cp_ToStrODBC(&i_TN.->MCNOMTAB)+;
                 " and MCGESTIO="+cp_ToStrODBC(&i_TN.->MCGESTIO)+;
                 " and MC_VERSO="+cp_ToStrODBC(&i_TN.->MC_VERSO)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'MAP_SINC')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and MCNOMTAB=&i_TN.->MCNOMTAB;
                      and MCGESTIO=&i_TN.->MCGESTIO;
                      and MC_VERSO=&i_TN.->MC_VERSO;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_MCNOMTAB)) AND not(Empty(t_MCGESTIO))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and MCNOMTAB="+cp_ToStrODBC(&i_TN.->MCNOMTAB)+;
                            " and MCGESTIO="+cp_ToStrODBC(&i_TN.->MCGESTIO)+;
                            " and MC_VERSO="+cp_ToStrODBC(&i_TN.->MC_VERSO)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and MCNOMTAB=&i_TN.->MCNOMTAB;
                            and MCGESTIO=&i_TN.->MCGESTIO;
                            and MC_VERSO=&i_TN.->MC_VERSO;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace MCNOMTAB with this.w_MCNOMTAB
              replace MCGESTIO with this.w_MCGESTIO
              replace MC_VERSO with this.w_MC_VERSO
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MAP_SINC
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'MAP_SINC')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MCTIPO_P="+cp_ToStrODBC(this.w_MCTIPO_P)+;
                     ",MCMSOUTL="+cp_ToStrODBCNull(this.w_MCMSOUTL)+;
                     ",MCEXPRES="+cp_ToStrODBC(this.w_MCEXPRES)+;
                     ",MCFL_KEY="+cp_ToStrODBC(this.w_MCFL_KEY)+;
                     ",MCTIPEXP="+cp_ToStrODBC(this.w_MCTIPEXP)+;
                     ",MCNOMTAB="+cp_ToStrODBC(this.w_MCNOMTAB)+;
                     ",MCGESTIO="+cp_ToStrODBC(this.w_MCGESTIO)+;
                     ",MC_VERSO="+cp_ToStrODBC(this.w_MC_VERSO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and MCNOMTAB="+cp_ToStrODBC(MCNOMTAB)+;
                             " and MCGESTIO="+cp_ToStrODBC(MCGESTIO)+;
                             " and MC_VERSO="+cp_ToStrODBC(MC_VERSO)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'MAP_SINC')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MCTIPO_P=this.w_MCTIPO_P;
                     ,MCMSOUTL=this.w_MCMSOUTL;
                     ,MCEXPRES=this.w_MCEXPRES;
                     ,MCFL_KEY=this.w_MCFL_KEY;
                     ,MCTIPEXP=this.w_MCTIPEXP;
                     ,MCNOMTAB=this.w_MCNOMTAB;
                     ,MCGESTIO=this.w_MCGESTIO;
                     ,MC_VERSO=this.w_MC_VERSO;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and MCNOMTAB=&i_TN.->MCNOMTAB;
                                      and MCGESTIO=&i_TN.->MCGESTIO;
                                      and MC_VERSO=&i_TN.->MC_VERSO;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MAP_SINC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAP_SINC_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MCNOMTAB)) AND not(Empty(t_MCGESTIO))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete MAP_SINC
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and MCNOMTAB="+cp_ToStrODBC(&i_TN.->MCNOMTAB)+;
                            " and MCGESTIO="+cp_ToStrODBC(&i_TN.->MCGESTIO)+;
                            " and MC_VERSO="+cp_ToStrODBC(&i_TN.->MC_VERSO)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and MCNOMTAB=&i_TN.->MCNOMTAB;
                              and MCGESTIO=&i_TN.->MCGESTIO;
                              and MC_VERSO=&i_TN.->MC_VERSO;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MCNOMTAB)) AND not(Empty(t_MCGESTIO))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MAP_SINC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAP_SINC_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,2,.t.)
          .w_MCCODAZI = .w_CodiceAziendaDefault
        .DoRTCalc(4,5,.t.)
        if .o_MCNOMTAB<>.w_MCNOMTAB
          .link_2_3('Full')
        endif
        .DoRTCalc(7,17,.t.)
          .w_CodiceAziendaDefault = 'XXXXX'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MCTIPO_P with this.w_MCTIPO_P
      replace t_CTRLTAB with this.w_CTRLTAB
      replace t_CTRLTIPO with this.w_CTRLTIPO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_IIOJFDOIQL()
    with this
          * --- Preparazione filtro su XDC Fields
          gsar1beo(this;
              ,'ELENCO';
             )
    endwith
  endproc
  proc Calculate_IRVEOODCAB()
    with this
          * --- Preparazione filtro su XDC Fields
          gsar1beo(this;
              ,'INIT';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oMCEXPRES_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMCEXPRES_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("New record")
          .Calculate_IIOJFDOIQL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_IRVEOODCAB()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MCGESTIO
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCGESTIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_MCGESTIO)+"%");

          i_ret=cp_SQL(i_nConn,"select FLNAME,TBNAME,FLCOMMEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FLNAME',trim(this.w_MCGESTIO))
          select FLNAME,TBNAME,FLCOMMEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCGESTIO)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCGESTIO) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','FLNAME',cp_AbsName(oSource.parent,'oMCGESTIO_2_3'),i_cWhere,'',"Campi gestionale",'GSAR_MOU.XDC_FIELDS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNAME,TBNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNAME',oSource.xKey(1))
            select FLNAME,TBNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCGESTIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNAME,TBNAME,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_MCGESTIO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNAME',this.w_MCGESTIO)
            select FLNAME,TBNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCGESTIO = NVL(_Link_.FLNAME,space(10))
      this.w_CTRLTAB = NVL(_Link_.TBNAME,space(15))
      this.w_DesCampo = NVL(_Link_.FLCOMMEN,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_MCGESTIO = space(10)
      endif
      this.w_CTRLTAB = space(15)
      this.w_DesCampo = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ALLTRIM(.w_CTRLTAB)==ALLTRIM(.w_MCNOMTAB)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un campo diverso")
        endif
        this.w_MCGESTIO = space(10)
        this.w_CTRLTAB = space(15)
        this.w_DesCampo = space(80)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCGESTIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCMSOUTL
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAP_OUTL_IDX,3]
    i_lTable = "MAP_OUTL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAP_OUTL_IDX,2], .t., this.MAP_OUTL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAP_OUTL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCMSOUTL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOU',True,'MAP_OUTL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOPRPRTY like "+cp_ToStrODBC(trim(this.w_MCMSOUTL)+"%");
                   +" and MOTIPO_P="+cp_ToStrODBC(this.w_TipoElemOutl);

          i_ret=cp_SQL(i_nConn,"select MOTIPO_P,MOPRPRTY,MODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOTIPO_P,MOPRPRTY","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOTIPO_P',this.w_TipoElemOutl;
                     ,'MOPRPRTY',trim(this.w_MCMSOUTL))
          select MOTIPO_P,MOPRPRTY,MODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOTIPO_P,MOPRPRTY into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCMSOUTL)==trim(_Link_.MOPRPRTY) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCMSOUTL) and !this.bDontReportError
            deferred_cp_zoom('MAP_OUTL','*','MOTIPO_P,MOPRPRTY',cp_AbsName(oSource.parent,'oMCMSOUTL_2_5'),i_cWhere,'GSAR_AOU',"Campi Outlook",'GSAR_MOU.MAP_OUTL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoElemOutl<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOTIPO_P,MOPRPRTY,MODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select MOTIPO_P,MOPRPRTY,MODESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un campo diverso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOTIPO_P,MOPRPRTY,MODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MOPRPRTY="+cp_ToStrODBC(oSource.xKey(2));
                     +" and MOTIPO_P="+cp_ToStrODBC(this.w_TipoElemOutl);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOTIPO_P',oSource.xKey(1);
                       ,'MOPRPRTY',oSource.xKey(2))
            select MOTIPO_P,MOPRPRTY,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCMSOUTL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOTIPO_P,MOPRPRTY,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOPRPRTY="+cp_ToStrODBC(this.w_MCMSOUTL);
                   +" and MOTIPO_P="+cp_ToStrODBC(this.w_TipoElemOutl);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOTIPO_P',this.w_TipoElemOutl;
                       ,'MOPRPRTY',this.w_MCMSOUTL)
            select MOTIPO_P,MOPRPRTY,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCMSOUTL = NVL(_Link_.MOPRPRTY,space(50))
      this.w_Descri = NVL(_Link_.MODESCRI,space(150))
      this.w_CTRLTIPO = NVL(_Link_.MOTIPO_P,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MCMSOUTL = space(50)
      endif
      this.w_Descri = space(150)
      this.w_CTRLTIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CTRLTIPO=.w_MCTIPO_P
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un campo diverso")
        endif
        this.w_MCMSOUTL = space(50)
        this.w_Descri = space(150)
        this.w_CTRLTIPO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAP_OUTL_IDX,2])+'\'+cp_ToStr(_Link_.MOTIPO_P,1)+'\'+cp_ToStr(_Link_.MOPRPRTY,1)
      cp_ShowWarn(i_cKey,this.MAP_OUTL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCMSOUTL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMCEXPRES_2_6.value==this.w_MCEXPRES)
      this.oPgFrm.Page1.oPag.oMCEXPRES_2_6.value=this.w_MCEXPRES
      replace t_MCEXPRES with this.oPgFrm.Page1.oPag.oMCEXPRES_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDescri_2_10.value==this.w_Descri)
      this.oPgFrm.Page1.oPag.oDescri_2_10.value=this.w_Descri
      replace t_Descri with this.oPgFrm.Page1.oPag.oDescri_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCNOMTAB_2_2.RadioValue()==this.w_MCNOMTAB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCNOMTAB_2_2.SetRadio()
      replace t_MCNOMTAB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCNOMTAB_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCGESTIO_2_3.value==this.w_MCGESTIO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCGESTIO_2_3.value=this.w_MCGESTIO
      replace t_MCGESTIO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCGESTIO_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCMSOUTL_2_5.value==this.w_MCMSOUTL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCMSOUTL_2_5.value=this.w_MCMSOUTL
      replace t_MCMSOUTL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCMSOUTL_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMC_VERSO_2_7.RadioValue()==this.w_MC_VERSO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMC_VERSO_2_7.SetRadio()
      replace t_MC_VERSO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMC_VERSO_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCFL_KEY_2_11.RadioValue()==this.w_MCFL_KEY)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCFL_KEY_2_11.SetRadio()
      replace t_MCFL_KEY with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCFL_KEY_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDesCampo_2_12.value==this.w_DesCampo)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDesCampo_2_12.value=this.w_DesCampo
      replace t_DesCampo with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDesCampo_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPEXP_2_13.RadioValue()==this.w_MCTIPEXP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPEXP_2_13.SetRadio()
      replace t_MCTIPEXP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPEXP_2_13.value
    endif
    cp_SetControlsValueExtFlds(this,'MAP_SINC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_MCNOMTAB)) AND not(Empty(t_MCGESTIO)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(ALLTRIM(.w_CTRLTAB)==ALLTRIM(.w_MCNOMTAB)) and not(empty(.w_MCGESTIO)) and (not(Empty(.w_MCNOMTAB)) AND not(Empty(.w_MCGESTIO)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCGESTIO_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Selezionare un campo diverso")
        case   not(.w_CTRLTIPO=.w_MCTIPO_P) and not(empty(.w_MCMSOUTL)) and (not(Empty(.w_MCNOMTAB)) AND not(Empty(.w_MCGESTIO)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCMSOUTL_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Selezionare un campo diverso")
      endcase
      if not(Empty(.w_MCNOMTAB)) AND not(Empty(.w_MCGESTIO))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MCNOMTAB = this.w_MCNOMTAB
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MCNOMTAB)) AND not(Empty(t_MCGESTIO)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MCNOMTAB=space(20)
      .w_MCGESTIO=space(10)
      .w_MCTIPO_P=space(1)
      .w_MCMSOUTL=space(50)
      .w_MCEXPRES=space(0)
      .w_MC_VERSO=space(1)
      .w_CTRLTAB=space(15)
      .w_CTRLTIPO=space(1)
      .w_Descri=space(150)
      .w_MCFL_KEY=space(1)
      .w_DesCampo=space(80)
      .w_MCTIPEXP=space(1)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_MCGESTIO))
        .link_2_3('Full')
      endif
        .w_MCTIPO_P = 'C'
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_MCMSOUTL))
        .link_2_5('Full')
      endif
    endwith
    this.DoRTCalc(9,18,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MCNOMTAB = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCNOMTAB_2_2.RadioValue(.t.)
    this.w_MCGESTIO = t_MCGESTIO
    this.w_MCTIPO_P = t_MCTIPO_P
    this.w_MCMSOUTL = t_MCMSOUTL
    this.w_MCEXPRES = t_MCEXPRES
    this.w_MC_VERSO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMC_VERSO_2_7.RadioValue(.t.)
    this.w_CTRLTAB = t_CTRLTAB
    this.w_CTRLTIPO = t_CTRLTIPO
    this.w_Descri = t_Descri
    this.w_MCFL_KEY = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCFL_KEY_2_11.RadioValue(.t.)
    this.w_DesCampo = t_DesCampo
    this.w_MCTIPEXP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPEXP_2_13.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MCNOMTAB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCNOMTAB_2_2.ToRadio()
    replace t_MCGESTIO with this.w_MCGESTIO
    replace t_MCTIPO_P with this.w_MCTIPO_P
    replace t_MCMSOUTL with this.w_MCMSOUTL
    replace t_MCEXPRES with this.w_MCEXPRES
    replace t_MC_VERSO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMC_VERSO_2_7.ToRadio()
    replace t_CTRLTAB with this.w_CTRLTAB
    replace t_CTRLTIPO with this.w_CTRLTIPO
    replace t_Descri with this.w_Descri
    replace t_MCFL_KEY with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCFL_KEY_2_11.ToRadio()
    replace t_DesCampo with this.w_DesCampo
    replace t_MCTIPEXP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPEXP_2_13.ToRadio()
    if i_srv='A'
      replace MCNOMTAB with this.w_MCNOMTAB
      replace MCGESTIO with this.w_MCGESTIO
      replace MC_VERSO with this.w_MC_VERSO
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mouPag1 as StdContainer
  Width  = 785
  height = 384
  stdWidth  = 785
  stdheight = 384
  resizeXpos=321
  resizeYpos=111
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=0, top=6, width=783,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1=" Seq.",Field2="MCNOMTAB",Label2="Archivio",Field3="MCGESTIO",Label3="Campi gest.",Field4="DesCampo",Label4=" ",Field5="MC_VERSO",Label5="Verso",Field6="MCTIPEXP",Label6="Exp",Field7="MCMSOUTL",Label7="Campi outlook",Field8="MCFL_KEY",Label8="Chiave",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 151892602

  add object oStr_1_6 as StdString with uid="ZRFYQKJWVY",Visible=.t., Left=476, Top=260,;
    Alignment=0, Width=151, Height=18,;
    Caption="Descrizione campo Outlook"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="TLULVXQADH",Visible=.t., Left=13, Top=302,;
    Alignment=0, Width=137, Height=18,;
    Caption="Espressione da valutare:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-9,top=25,;
    width=773+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-8,top=26,width=772+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='XDC_FIELDS|MAP_OUTL|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oMCEXPRES_2_6.Refresh()
      this.Parent.oDescri_2_10.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='XDC_FIELDS'
        oDropInto=this.oBodyCol.oRow.oMCGESTIO_2_3
      case cFile='MAP_OUTL'
        oDropInto=this.oBodyCol.oRow.oMCMSOUTL_2_5
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oMCEXPRES_2_6 as StdTrsMemo with uid="OCAACRTSAZ",rtseq=9,rtrep=.t.,;
    cFormVar="w_MCEXPRES",value=space(0),;
    ToolTipText = "Espressione da valutare",;
    HelpContextID = 227746073,;
    cTotal="", bFixedPos=.t., cQueryName = "MCEXPRES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=60, Width=776, Left=6, Top=321

  func oMCEXPRES_2_6.mCond()
    with this.Parent.oContained
      return (.w_MCTIPEXP='S')
    endwith
  endfunc

  add object oDescri_2_10 as StdTrsField with uid="WVRKJJFQNZ",rtseq=14,rtrep=.t.,;
    cFormVar="w_Descri",value=space(150),enabled=.f.,;
    HelpContextID = 113320502,;
    cTotal="", bFixedPos=.t., cQueryName = "Descri",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=476, Top=279, InputMask=replicate('X',150)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mouBodyRow as CPBodyRowCnt
  Width=763
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="RWGONRDSHH",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 184221034,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0

  add object oMCNOMTAB_2_2 as StdTrsCombo with uid="LGIVARWRWD",rtrep=.t.,;
    cFormVar="w_MCNOMTAB", RowSource=""+"Nominativi,"+"Persone,"+"Attributi" , ;
    ToolTipText = "Nome tabella",;
    HelpContextID = 257601800,;
    Height=22, Width=76, Left=41, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMCNOMTAB_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MCNOMTAB,&i_cF..t_MCNOMTAB),this.value)
    return(iif(xVal =1,'OFF_NOMI',;
    iif(xVal =2,'NOM_CONT',;
    iif(xVal =3,'NOM_ATTR',;
    space(20)))))
  endfunc
  func oMCNOMTAB_2_2.GetRadio()
    this.Parent.oContained.w_MCNOMTAB = this.RadioValue()
    return .t.
  endfunc

  func oMCNOMTAB_2_2.ToRadio()
    this.Parent.oContained.w_MCNOMTAB=trim(this.Parent.oContained.w_MCNOMTAB)
    return(;
      iif(this.Parent.oContained.w_MCNOMTAB=='OFF_NOMI',1,;
      iif(this.Parent.oContained.w_MCNOMTAB=='NOM_CONT',2,;
      iif(this.Parent.oContained.w_MCNOMTAB=='NOM_ATTR',3,;
      0))))
  endfunc

  func oMCNOMTAB_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMCGESTIO_2_3 as StdTrsField with uid="DBORVQRUJX",rtseq=6,rtrep=.t.,;
    cFormVar="w_MCGESTIO",value=space(10),isprimarykey=.t.,;
    ToolTipText = "Campo gestionale",;
    HelpContextID = 5226219,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un campo diverso",;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=120, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="FLNAME", oKey_1_2="this.w_MCGESTIO"

  func oMCGESTIO_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCGESTIO_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMCGESTIO_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oMCGESTIO_2_3.readonly and this.parent.oMCGESTIO_2_3.isprimarykey)
    do cp_zoom with 'XDC_FIELDS','*','FLNAME',cp_AbsName(this.parent,'oMCGESTIO_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Campi gestionale",'GSAR_MOU.XDC_FIELDS_VZM',this.parent.oContained
   endif
  endproc

  add object oMCMSOUTL_2_5 as StdTrsField with uid="APLJWCUTHN",rtseq=8,rtrep=.t.,;
    cFormVar="w_MCMSOUTL",value=space(50),;
    ToolTipText = "Campo Outlook",;
    HelpContextID = 8298770,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un campo diverso",;
   bGlobalFont=.t.,;
    Height=17, Width=265, Left=473, Top=0, InputMask=replicate('X',50), bHasZoom = .t. , cLinkFile="MAP_OUTL", cZoomOnZoom="GSAR_AOU", oKey_1_1="MOTIPO_P", oKey_1_2="this.w_TipoElemOutl", oKey_2_1="MOPRPRTY", oKey_2_2="this.w_MCMSOUTL"

  func oMCMSOUTL_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCMSOUTL_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMCMSOUTL_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.MAP_OUTL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"MOTIPO_P="+cp_ToStrODBC(this.Parent.oContained.w_TipoElemOutl)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"MOTIPO_P="+cp_ToStr(this.Parent.oContained.w_TipoElemOutl)
    endif
    do cp_zoom with 'MAP_OUTL','*','MOTIPO_P,MOPRPRTY',cp_AbsName(this.parent,'oMCMSOUTL_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOU',"Campi Outlook",'GSAR_MOU.MAP_OUTL_VZM',this.parent.oContained
  endproc
  proc oMCMSOUTL_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.MOTIPO_P=w_TipoElemOutl
     i_obj.w_MOPRPRTY=this.parent.oContained.w_MCMSOUTL
    i_obj.ecpSave()
  endproc

  add object oMC_VERSO_2_7 as StdTrsCombo with uid="APPCHVJEPQ",rtrep=.t.,;
    cFormVar="w_MC_VERSO", RowSource=""+"Import,"+"Export,"+"Import/export" , ;
    ToolTipText = "Import, Export o entrambi",;
    HelpContextID = 216187157,;
    Height=21, Width=94, Left=335, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMC_VERSO_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MC_VERSO,&i_cF..t_MC_VERSO),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'E',;
    iif(xVal =3,'T',;
    space(1)))))
  endfunc
  func oMC_VERSO_2_7.GetRadio()
    this.Parent.oContained.w_MC_VERSO = this.RadioValue()
    return .t.
  endfunc

  func oMC_VERSO_2_7.ToRadio()
    this.Parent.oContained.w_MC_VERSO=trim(this.Parent.oContained.w_MC_VERSO)
    return(;
      iif(this.Parent.oContained.w_MC_VERSO=='I',1,;
      iif(this.Parent.oContained.w_MC_VERSO=='E',2,;
      iif(this.Parent.oContained.w_MC_VERSO=='T',3,;
      0))))
  endfunc

  func oMC_VERSO_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMCFL_KEY_2_11 as StdTrsCheck with uid="MTLLYAYGNV",rtrep=.t.,;
    cFormVar="w_MCFL_KEY",  caption="",;
    ToolTipText = "Chiave di mappatura s�/no",;
    HelpContextID = 125251871,;
    Left=744, Top=0, Width=14,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oMCFL_KEY_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MCFL_KEY,&i_cF..t_MCFL_KEY),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oMCFL_KEY_2_11.GetRadio()
    this.Parent.oContained.w_MCFL_KEY = this.RadioValue()
    return .t.
  endfunc

  func oMCFL_KEY_2_11.ToRadio()
    this.Parent.oContained.w_MCFL_KEY=trim(this.Parent.oContained.w_MCFL_KEY)
    return(;
      iif(this.Parent.oContained.w_MCFL_KEY=='S',1,;
      0))
  endfunc

  func oMCFL_KEY_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDesCampo_2_12 as StdTrsField with uid="DVOEHKNGLI",rtseq=16,rtrep=.t.,;
    cFormVar="w_DesCampo",value=space(80),enabled=.f.,;
    HelpContextID = 160506533,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=206, Top=0, InputMask=replicate('X',80)

  add object oMCTIPEXP_2_13 as StdTrsCheck with uid="XJFKZALUQS",rtrep=.t.,;
    cFormVar="w_MCTIPEXP",  caption=" ",;
    ToolTipText = "Espressione S�/No",;
    HelpContextID = 259714794,;
    Left=433, Top=0, Width=21,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oMCTIPEXP_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MCTIPEXP,&i_cF..t_MCTIPEXP),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oMCTIPEXP_2_13.GetRadio()
    this.Parent.oContained.w_MCTIPEXP = this.RadioValue()
    return .t.
  endfunc

  func oMCTIPEXP_2_13.ToRadio()
    this.Parent.oContained.w_MCTIPEXP=trim(this.Parent.oContained.w_MCTIPEXP)
    return(;
      iif(this.Parent.oContained.w_MCTIPEXP=='S',1,;
      0))
  endfunc

  func oMCTIPEXP_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mou','MAP_SINC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MCCODAZI=MAP_SINC.MCCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
