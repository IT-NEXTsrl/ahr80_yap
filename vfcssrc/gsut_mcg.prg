* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mcg                                                        *
*              Configurazioni gestioni                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-10                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_mcg"))

* --- Class definition
define class tgsut_mcg as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 783
  Height = 316+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=97076073
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CFG_GEST_IDX = 0
  cpusers_IDX = 0
  cpgroups_IDX = 0
  MODMCONF_IDX = 0
  AZIENDA_IDX = 0
  cFile = "CFG_GEST"
  cKeySelect = "CGNOMGES"
  cKeyWhere  = "CGNOMGES=this.w_CGNOMGES"
  cKeyDetail  = "CGNOMGES=this.w_CGNOMGES"
  cKeyWhereODBC = '"CGNOMGES="+cp_ToStrODBC(this.w_CGNOMGES)';

  cKeyDetailWhereODBC = '"CGNOMGES="+cp_ToStrODBC(this.w_CGNOMGES)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"CFG_GEST.CGNOMGES="+cp_ToStrODBC(this.w_CGNOMGES)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CFG_GEST.CPROWNUM '
  cPrg = "gsut_mcg"
  cComment = "Configurazioni gestioni"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CGNOMGES = space(30)
  w_CGDESCFG = space(50)
  w_CGMODSAV = space(1)
  w_CGDEFCFG = space(1)
  w_CGTIPCFG = space(1)
  o_CGTIPCFG = space(1)
  w_CGUTEGRP = 0
  o_CGUTEGRP = 0
  w_GRUPPO = 0
  o_GRUPPO = 0
  w_UTENTE = 0
  o_UTENTE = 0
  w_DESCRIU = space(20)
  w_DESGES = space(50)
  w_TIPCFG = space(1)
  w_DESCRIG = space(20)
  w_CGCODAZI = space(5)
  w_RAGAZI = space(40)

  * --- Children pointers
  GSUT_MPR = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CFG_GEST','gsut_mcg')
    stdPageFrame::Init()
    *set procedure to GSUT_MPR additive
    with this
      .Pages(1).addobject("oPag","tgsut_mcgPag1","gsut_mcg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Configurazioni")
      .Pages(1).HelpContextID = 143270056
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCGNOMGES_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSUT_MPR
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='cpgroups'
    this.cWorkTables[3]='MODMCONF'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='CFG_GEST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CFG_GEST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CFG_GEST_IDX,3]
  return

  function CreateChildren()
    this.GSUT_MPR = CREATEOBJECT('stdLazyChild',this,'GSUT_MPR')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_MPR)
      this.GSUT_MPR.DestroyChildrenChain()
      this.GSUT_MPR=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_MPR.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_MPR.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_MPR.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSUT_MPR.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_CGNOMGES,"PRNOMGES";
             ,.w_CPROWNUM,"PRROWNUM";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CGNOMGES = NVL(CGNOMGES,space(30))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_2_12_joined
    link_2_12_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CFG_GEST where CGNOMGES=KeySet.CGNOMGES
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CFG_GEST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CFG_GEST_IDX,2],this.bLoadRecFilter,this.CFG_GEST_IDX,"gsut_mcg")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CFG_GEST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CFG_GEST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CFG_GEST '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_12_joined=this.AddJoinedLink_2_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CGNOMGES',this.w_CGNOMGES  )
      select * from (i_cTable) CFG_GEST where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESGES = space(50)
        .w_CGNOMGES = NVL(CGNOMGES,space(30))
          if link_1_1_joined
            this.w_CGNOMGES = NVL(MCNOMGES101,NVL(this.w_CGNOMGES,space(30)))
            this.w_DESGES = NVL(MCDESGES101,space(50))
          else
          .link_1_1('Load')
          endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CFG_GEST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_RAGAZI = space(40)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CGDESCFG = NVL(CGDESCFG,space(50))
          .w_CGMODSAV = NVL(CGMODSAV,space(1))
          .w_CGDEFCFG = NVL(CGDEFCFG,space(1))
          .w_CGTIPCFG = NVL(CGTIPCFG,space(1))
          .w_CGUTEGRP = NVL(CGUTEGRP,0)
        .w_GRUPPO = IIF(.w_CGTIPCFG='G',.w_CGUTEGRP, 0)
          .link_2_6('Load')
        .w_UTENTE = IIF(.w_CGTIPCFG='U', .w_CGUTEGRP, 0)
          .link_2_7('Load')
        .w_DESCRIU = IIF(.w_CGTIPCFG='I', 'Installazione', .w_DESCRIU)
        .w_TIPCFG = .w_CGTIPCFG
        .w_DESCRIG = IIF(.w_CGTIPCFG='I', 'Installazione', .w_DESCRIG)
          .w_CGCODAZI = NVL(CGCODAZI,space(5))
          if link_2_12_joined
            this.w_CGCODAZI = NVL(AZCODAZI212,NVL(this.w_CGCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI212,space(40))
          else
          .link_2_12('Load')
          endif
          select (this.cTrsName)
          append blank
          replace CGNOMGES with .w_CGNOMGES
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CGNOMGES=space(30)
      .w_CGDESCFG=space(50)
      .w_CGMODSAV=space(1)
      .w_CGDEFCFG=space(1)
      .w_CGTIPCFG=space(1)
      .w_CGUTEGRP=0
      .w_GRUPPO=0
      .w_UTENTE=0
      .w_DESCRIU=space(20)
      .w_DESGES=space(50)
      .w_TIPCFG=space(1)
      .w_DESCRIG=space(20)
      .w_CGCODAZI=space(5)
      .w_RAGAZI=space(40)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CGNOMGES))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        .w_CGMODSAV = 'N'
        .w_CGDEFCFG = 'N'
        .w_CGTIPCFG = 'I'
        .w_CGUTEGRP = IIF(.w_CGTIPCFG='U',.w_UTENTE, IIF(.w_CGTIPCFG='G', .w_GRUPPO, 0))
        .w_GRUPPO = IIF(.w_CGTIPCFG='G',.w_CGUTEGRP, 0)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_GRUPPO))
         .link_2_6('Full')
        endif
        .w_UTENTE = IIF(.w_CGTIPCFG='U', .w_CGUTEGRP, 0)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_UTENTE))
         .link_2_7('Full')
        endif
        .w_DESCRIU = IIF(.w_CGTIPCFG='I', 'Installazione', .w_DESCRIU)
        .DoRTCalc(10,10,.f.)
        .w_TIPCFG = .w_CGTIPCFG
        .w_DESCRIG = IIF(.w_CGTIPCFG='I', 'Installazione', .w_DESCRIG)
        .w_CGCODAZI = i_CODAZI
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CGCODAZI))
         .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CFG_GEST')
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCGNOMGES_1_1.enabled = i_bVal
      .Page1.oPag.oCGCODAZI_2_12.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCGNOMGES_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCGNOMGES_1_1.enabled = .t.
      endif
    endwith
    this.GSUT_MPR.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CFG_GEST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_MPR.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CFG_GEST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGNOMGES,"CGNOMGES",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CFG_GEST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CFG_GEST_IDX,2])
    i_lTable = "CFG_GEST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CFG_GEST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CGDESCFG C(50);
      ,t_CGMODSAV N(3);
      ,t_CGDEFCFG N(3);
      ,t_CGTIPCFG N(3);
      ,t_GRUPPO N(6);
      ,t_UTENTE N(6);
      ,t_DESCRIU C(20);
      ,t_DESCRIG C(20);
      ,t_CGCODAZI C(5);
      ,t_RAGAZI C(40);
      ,CGNOMGES C(30);
      ,CPROWNUM N(10);
      ,t_CGUTEGRP N(6);
      ,t_TIPCFG C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mcgbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGDESCFG_2_1.controlsource=this.cTrsName+'.t_CGDESCFG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGMODSAV_2_2.controlsource=this.cTrsName+'.t_CGMODSAV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGDEFCFG_2_3.controlsource=this.cTrsName+'.t_CGDEFCFG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCFG_2_4.controlsource=this.cTrsName+'.t_CGTIPCFG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGRUPPO_2_6.controlsource=this.cTrsName+'.t_GRUPPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUTENTE_2_7.controlsource=this.cTrsName+'.t_UTENTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIU_2_9.controlsource=this.cTrsName+'.t_DESCRIU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIG_2_11.controlsource=this.cTrsName+'.t_DESCRIG'
    this.oPgFRm.Page1.oPag.oCGCODAZI_2_12.controlsource=this.cTrsName+'.t_CGCODAZI'
    this.oPgFRm.Page1.oPag.oRAGAZI_2_14.controlsource=this.cTrsName+'.t_RAGAZI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(255)
    this.AddVLine(364)
    this.AddVLine(451)
    this.AddVLine(603)
    this.AddVLine(720)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDESCFG_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CFG_GEST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CFG_GEST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CFG_GEST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CFG_GEST_IDX,2])
      *
      * insert into CFG_GEST
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CFG_GEST')
        i_extval=cp_InsertValODBCExtFlds(this,'CFG_GEST')
        i_cFldBody=" "+;
                  "(CGNOMGES,CGDESCFG,CGMODSAV,CGDEFCFG,CGTIPCFG"+;
                  ",CGUTEGRP,CGCODAZI,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_CGNOMGES)+","+cp_ToStrODBC(this.w_CGDESCFG)+","+cp_ToStrODBC(this.w_CGMODSAV)+","+cp_ToStrODBC(this.w_CGDEFCFG)+","+cp_ToStrODBC(this.w_CGTIPCFG)+;
             ","+cp_ToStrODBC(this.w_CGUTEGRP)+","+cp_ToStrODBCNull(this.w_CGCODAZI)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CFG_GEST')
        i_extval=cp_InsertValVFPExtFlds(this,'CFG_GEST')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CGNOMGES',this.w_CGNOMGES)
        INSERT INTO (i_cTable) (;
                   CGNOMGES;
                  ,CGDESCFG;
                  ,CGMODSAV;
                  ,CGDEFCFG;
                  ,CGTIPCFG;
                  ,CGUTEGRP;
                  ,CGCODAZI;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CGNOMGES;
                  ,this.w_CGDESCFG;
                  ,this.w_CGMODSAV;
                  ,this.w_CGDEFCFG;
                  ,this.w_CGTIPCFG;
                  ,this.w_CGUTEGRP;
                  ,this.w_CGCODAZI;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CFG_GEST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CFG_GEST_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CGNOMGES<>CGNOMGES
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CGDESCFG)) And (t_TIPCFG='I' Or Not Empty(t_CGUTEGRP))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CFG_GEST')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CFG_GEST')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CGNOMGES<>CGNOMGES
            i_bUpdAll = .t.
          endif
        endif
        scan for (not(Empty(t_CGDESCFG)) And (t_TIPCFG='I' Or Not Empty(t_CGUTEGRP))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSUT_MPR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_CGNOMGES,"PRNOMGES";
                     ,this.w_CPROWNUM,"PRROWNUM";
                     )
              this.GSUT_MPR.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CFG_GEST
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CFG_GEST')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CGDESCFG="+cp_ToStrODBC(this.w_CGDESCFG)+;
                     ",CGMODSAV="+cp_ToStrODBC(this.w_CGMODSAV)+;
                     ",CGDEFCFG="+cp_ToStrODBC(this.w_CGDEFCFG)+;
                     ",CGTIPCFG="+cp_ToStrODBC(this.w_CGTIPCFG)+;
                     ",CGUTEGRP="+cp_ToStrODBC(this.w_CGUTEGRP)+;
                     ",CGCODAZI="+cp_ToStrODBCNull(this.w_CGCODAZI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CFG_GEST')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CGDESCFG=this.w_CGDESCFG;
                     ,CGMODSAV=this.w_CGMODSAV;
                     ,CGDEFCFG=this.w_CGDEFCFG;
                     ,CGTIPCFG=this.w_CGTIPCFG;
                     ,CGUTEGRP=this.w_CGUTEGRP;
                     ,CGCODAZI=this.w_CGCODAZI;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_CGDESCFG)) And (t_TIPCFG='I' Or Not Empty(t_CGUTEGRP)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSUT_MPR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CGNOMGES,"PRNOMGES";
               ,this.w_CPROWNUM,"PRROWNUM";
               )
          this.GSUT_MPR.mReplace()
          this.GSUT_MPR.bSaveContext=.f.
        endif
      endscan
     this.GSUT_MPR.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CFG_GEST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CFG_GEST_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CGDESCFG)) And (t_TIPCFG='I' Or Not Empty(t_CGUTEGRP))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSUT_MPR : Deleting
        this.GSUT_MPR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CGNOMGES,"PRNOMGES";
               ,this.w_CPROWNUM,"PRROWNUM";
               )
        this.GSUT_MPR.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CFG_GEST
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CGDESCFG)) And (t_TIPCFG='I' Or Not Empty(t_CGUTEGRP))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CFG_GEST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CFG_GEST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_CGTIPCFG<>.w_CGTIPCFG.or. .o_UTENTE<>.w_UTENTE.or. .o_GRUPPO<>.w_GRUPPO
          .w_CGUTEGRP = IIF(.w_CGTIPCFG='U',.w_UTENTE, IIF(.w_CGTIPCFG='G', .w_GRUPPO, 0))
        endif
        if .o_CGTIPCFG<>.w_CGTIPCFG
          .w_GRUPPO = IIF(.w_CGTIPCFG='G',.w_CGUTEGRP, 0)
          .link_2_6('Full')
        endif
        if .o_CGTIPCFG<>.w_CGTIPCFG.or. .o_CGUTEGRP<>.w_CGUTEGRP
          .w_UTENTE = IIF(.w_CGTIPCFG='U', .w_CGUTEGRP, 0)
          .link_2_7('Full')
        endif
        if .o_CGTIPCFG<>.w_CGTIPCFG
          .w_DESCRIU = IIF(.w_CGTIPCFG='I', 'Installazione', .w_DESCRIU)
        endif
        .DoRTCalc(10,10,.t.)
        if .o_CGTIPCFG<>.w_CGTIPCFG
          .w_TIPCFG = .w_CGTIPCFG
        endif
        if .o_CGTIPCFG<>.w_CGTIPCFG
          .w_DESCRIG = IIF(.w_CGTIPCFG='I', 'Installazione', .w_DESCRIG)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CGUTEGRP with this.w_CGUTEGRP
      replace t_TIPCFG with this.w_TIPCFG
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRUPPO_2_6.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRUPPO_2_6.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUTENTE_2_7.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUTENTE_2_7.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CGNOMGES
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODMCONF_IDX,3]
    i_lTable = "MODMCONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODMCONF_IDX,2], .t., this.MODMCONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODMCONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGNOMGES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MODMCONF')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCNOMGES like "+cp_ToStrODBC(trim(this.w_CGNOMGES)+"%");

          i_ret=cp_SQL(i_nConn,"select MCNOMGES,MCDESGES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCNOMGES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCNOMGES',trim(this.w_CGNOMGES))
          select MCNOMGES,MCDESGES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCNOMGES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGNOMGES)==trim(_Link_.MCNOMGES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGNOMGES) and !this.bDontReportError
            deferred_cp_zoom('MODMCONF','*','MCNOMGES',cp_AbsName(oSource.parent,'oCGNOMGES_1_1'),i_cWhere,'',"Gestioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCNOMGES,MCDESGES";
                     +" from "+i_cTable+" "+i_lTable+" where MCNOMGES="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCNOMGES',oSource.xKey(1))
            select MCNOMGES,MCDESGES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGNOMGES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCNOMGES,MCDESGES";
                   +" from "+i_cTable+" "+i_lTable+" where MCNOMGES="+cp_ToStrODBC(this.w_CGNOMGES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCNOMGES',this.w_CGNOMGES)
            select MCNOMGES,MCDESGES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGNOMGES = NVL(_Link_.MCNOMGES,space(30))
      this.w_DESGES = NVL(_Link_.MCDESGES,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CGNOMGES = space(30)
      endif
      this.w_DESGES = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODMCONF_IDX,2])+'\'+cp_ToStr(_Link_.MCNOMGES,1)
      cp_ShowWarn(i_cKey,this.MODMCONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGNOMGES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODMCONF_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODMCONF_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.MCNOMGES as MCNOMGES101"+ ",link_1_1.MCDESGES as MCDESGES101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CFG_GEST.CGNOMGES=link_1_1.MCNOMGES"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CFG_GEST.CGNOMGES=link_1_1.MCNOMGES(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GRUPPO
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_GRUPPO);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_GRUPPO)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_GRUPPO) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oGRUPPO_2_6'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_GRUPPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_GRUPPO)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPPO = NVL(_Link_.CODE,0)
      this.w_DESCRIG = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPPO = 0
      endif
      this.w_DESCRIG = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTENTE
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_UTENTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_UTENTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTENTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oUTENTE_2_7'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTENTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTENTE = NVL(_Link_.CODE,0)
      this.w_DESCRIU = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTENTE = 0
      endif
      this.w_DESCRIU = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CGCODAZI
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_CGCODAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_CGCODAZI))
          select AZCODAZI,AZRAGAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCODAZI)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGCODAZI) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oCGCODAZI_2_12'),i_cWhere,'',"Aziende",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CGCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CGCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CGCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_12.AZCODAZI as AZCODAZI212"+ ",link_2_12.AZRAGAZI as AZRAGAZI212"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_12 on CFG_GEST.CGCODAZI=link_2_12.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_12"
          i_cKey=i_cKey+'+" and CFG_GEST.CGCODAZI=link_2_12.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCGNOMGES_1_1.value==this.w_CGNOMGES)
      this.oPgFrm.Page1.oPag.oCGNOMGES_1_1.value=this.w_CGNOMGES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGES_1_3.value==this.w_DESGES)
      this.oPgFrm.Page1.oPag.oDESGES_1_3.value=this.w_DESGES
    endif
    if not(this.oPgFrm.Page1.oPag.oCGCODAZI_2_12.value==this.w_CGCODAZI)
      this.oPgFrm.Page1.oPag.oCGCODAZI_2_12.value=this.w_CGCODAZI
      replace t_CGCODAZI with this.oPgFrm.Page1.oPag.oCGCODAZI_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_2_14.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_2_14.value=this.w_RAGAZI
      replace t_RAGAZI with this.oPgFrm.Page1.oPag.oRAGAZI_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDESCFG_2_1.value==this.w_CGDESCFG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDESCFG_2_1.value=this.w_CGDESCFG
      replace t_CGDESCFG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDESCFG_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGMODSAV_2_2.RadioValue()==this.w_CGMODSAV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGMODSAV_2_2.SetRadio()
      replace t_CGMODSAV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGMODSAV_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDEFCFG_2_3.RadioValue()==this.w_CGDEFCFG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDEFCFG_2_3.SetRadio()
      replace t_CGDEFCFG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDEFCFG_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCFG_2_4.RadioValue()==this.w_CGTIPCFG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCFG_2_4.SetRadio()
      replace t_CGTIPCFG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCFG_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRUPPO_2_6.value==this.w_GRUPPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRUPPO_2_6.value=this.w_GRUPPO
      replace t_GRUPPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRUPPO_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUTENTE_2_7.value==this.w_UTENTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUTENTE_2_7.value=this.w_UTENTE
      replace t_UTENTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUTENTE_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIU_2_9.value==this.w_DESCRIU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIU_2_9.value=this.w_DESCRIU
      replace t_DESCRIU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIU_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIG_2_11.value==this.w_DESCRIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIG_2_11.value=this.w_DESCRIG
      replace t_DESCRIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIG_2_11.value
    endif
    cp_SetControlsValueExtFlds(this,'CFG_GEST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_CGDESCFG)) And (t_TIPCFG='I' Or Not Empty(t_CGUTEGRP)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(ControlloDefault(i_curform)) and (not(Empty(.w_CGDESCFG)) And (.w_TIPCFG='I' Or Not Empty(.w_CGUTEGRP)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDEFCFG_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Esiste gi� una configurazione di default")
      endcase
      i_bRes = i_bRes .and. .GSUT_MPR.CheckForm()
      if not(Empty(.w_CGDESCFG)) And (.w_TIPCFG='I' Or Not Empty(.w_CGUTEGRP))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CGTIPCFG = this.w_CGTIPCFG
    this.o_CGUTEGRP = this.w_CGUTEGRP
    this.o_GRUPPO = this.w_GRUPPO
    this.o_UTENTE = this.w_UTENTE
    * --- GSUT_MPR : Depends On
    this.GSUT_MPR.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CGDESCFG)) And (t_TIPCFG='I' Or Not Empty(t_CGUTEGRP)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CGDESCFG=space(50)
      .w_CGMODSAV=space(1)
      .w_CGDEFCFG=space(1)
      .w_CGTIPCFG=space(1)
      .w_CGUTEGRP=0
      .w_GRUPPO=0
      .w_UTENTE=0
      .w_DESCRIU=space(20)
      .w_TIPCFG=space(1)
      .w_DESCRIG=space(20)
      .w_CGCODAZI=space(5)
      .w_RAGAZI=space(40)
      .DoRTCalc(1,2,.f.)
        .w_CGMODSAV = 'N'
        .w_CGDEFCFG = 'N'
        .w_CGTIPCFG = 'I'
        .w_CGUTEGRP = IIF(.w_CGTIPCFG='U',.w_UTENTE, IIF(.w_CGTIPCFG='G', .w_GRUPPO, 0))
        .w_GRUPPO = IIF(.w_CGTIPCFG='G',.w_CGUTEGRP, 0)
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_GRUPPO))
        .link_2_6('Full')
      endif
        .w_UTENTE = IIF(.w_CGTIPCFG='U', .w_CGUTEGRP, 0)
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_UTENTE))
        .link_2_7('Full')
      endif
        .w_DESCRIU = IIF(.w_CGTIPCFG='I', 'Installazione', .w_DESCRIU)
      .DoRTCalc(10,10,.f.)
        .w_TIPCFG = .w_CGTIPCFG
        .w_DESCRIG = IIF(.w_CGTIPCFG='I', 'Installazione', .w_DESCRIG)
        .w_CGCODAZI = i_CODAZI
      .DoRTCalc(13,13,.f.)
      if not(empty(.w_CGCODAZI))
        .link_2_12('Full')
      endif
    endwith
    this.DoRTCalc(14,14,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CGDESCFG = t_CGDESCFG
    this.w_CGMODSAV = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGMODSAV_2_2.RadioValue(.t.)
    this.w_CGDEFCFG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDEFCFG_2_3.RadioValue(.t.)
    this.w_CGTIPCFG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCFG_2_4.RadioValue(.t.)
    this.w_CGUTEGRP = t_CGUTEGRP
    this.w_GRUPPO = t_GRUPPO
    this.w_UTENTE = t_UTENTE
    this.w_DESCRIU = t_DESCRIU
    this.w_TIPCFG = t_TIPCFG
    this.w_DESCRIG = t_DESCRIG
    this.w_CGCODAZI = t_CGCODAZI
    this.w_RAGAZI = t_RAGAZI
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CGDESCFG with this.w_CGDESCFG
    replace t_CGMODSAV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGMODSAV_2_2.ToRadio()
    replace t_CGDEFCFG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDEFCFG_2_3.ToRadio()
    replace t_CGTIPCFG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCFG_2_4.ToRadio()
    replace t_CGUTEGRP with this.w_CGUTEGRP
    replace t_GRUPPO with this.w_GRUPPO
    replace t_UTENTE with this.w_UTENTE
    replace t_DESCRIU with this.w_DESCRIU
    replace t_TIPCFG with this.w_TIPCFG
    replace t_DESCRIG with this.w_DESCRIG
    replace t_CGCODAZI with this.w_CGCODAZI
    replace t_RAGAZI with this.w_RAGAZI
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_mcgPag1 as StdContainer
  Width  = 779
  height = 316
  stdWidth  = 779
  stdheight = 316
  resizeXpos=381
  resizeYpos=163
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCGNOMGES_1_1 as StdField with uid="VZXPELVMNL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CGNOMGES", cQueryName = "CGNOMGES",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 106621049,;
   bGlobalFont=.t.,;
    Height=21, Width=154, Left=133, Top=16, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="MODMCONF", oKey_1_1="MCNOMGES", oKey_1_2="this.w_CGNOMGES"

  func oCGNOMGES_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGNOMGES_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGNOMGES_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCGNOMGES_1_1.readonly and this.parent.oCGNOMGES_1_1.isprimarykey)
    do cp_zoom with 'MODMCONF','*','MCNOMGES',cp_AbsName(this.parent,'oCGNOMGES_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gestioni",'',this.parent.oContained
   endif
  endproc

  add object oDESGES_1_3 as StdField with uid="VNUHVIERRN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESGES", cQueryName = "DESGES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 30619190,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=289, Top=16, InputMask=replicate('X',50)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=54, width=754,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CGDESCFG",Label1="Configurazione",Field2="CGTIPCFG",Label2="Tipo",Field3="GRUPPO",Label3="Utente/gruppo",Field4="UTENTE",Label4="",Field5="DESCRIU",Label5="Descrizione",Field6="DESCRIG",Label6="",Field7="CGMODSAV",Label7="Salvataggio",Field8="CGDEFCFG",Label8="Def.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 219014778

  add object oStr_1_2 as StdString with uid="MKAREZKTVR",Visible=.t., Left=10, Top=17,;
    Alignment=1, Width=122, Height=18,;
    Caption="Nome gestione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=73,;
    width=750+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=74,width=749+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CPGROUPS|CPUSERS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCGCODAZI_2_12.Refresh()
      this.Parent.oRAGAZI_2_14.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CPGROUPS'
        oDropInto=this.oBodyCol.oRow.oGRUPPO_2_6
      case cFile='CPUSERS'
        oDropInto=this.oBodyCol.oRow.oUTENTE_2_7
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_8 as StdButton with uid="GHWYELFSNW",width=18,height=19,;
   left=759, top=75,;
    caption="...", nPag=2;
    , ToolTipText = "Parametri configurazione";
    , HelpContextID = 96875050;
  , bGlobalFont=.t.

    proc oLinkPC_2_8.Click()
      this.Parent.oContained.GSUT_MPR.LinkPCClick()
    endproc

  add object oCGCODAZI_2_12 as StdTrsField with uid="TQLVILQQNV",rtseq=13,rtrep=.t.,;
    cFormVar="w_CGCODAZI",value=space(5),;
    ToolTipText = "Se valorizzato la configurazione � attiva solo per l'azienda valorizzata",;
    HelpContextID = 3524497,;
    cTotal="", bFixedPos=.t., cQueryName = "CGCODAZI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=124, Top=278, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_CGCODAZI"

  func oCGCODAZI_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGCODAZI_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oCGCODAZI_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oCGCODAZI_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Aziende",'',this.parent.oContained
  endproc

  add object oRAGAZI_2_14 as StdTrsField with uid="RZWCVDMCLT",rtseq=14,rtrep=.t.,;
    cFormVar="w_RAGAZI",value=space(40),enabled=.f.,;
    HelpContextID = 152859414,;
    cTotal="", bFixedPos=.t., cQueryName = "RAGAZI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=190, Top=278, InputMask=replicate('X',40)

  add object oStr_2_13 as StdString with uid="YNAJVBZRIY",Visible=.t., Left=16, Top=278,;
    Alignment=1, Width=107, Height=18,;
    Caption="Codice azienda:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_mcgBodyRow as CPBodyRowCnt
  Width=740
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCGDESCFG_2_1 as StdTrsField with uid="MOBUTNUCWF",rtseq=2,rtrep=.t.,;
    cFormVar="w_CGDESCFG",value=space(50),;
    HelpContextID = 223328147,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=243, Left=-2, Top=0, InputMask=replicate('X',50)

  add object oCGMODSAV_2_2 as StdTrsCombo with uid="CREDDTHUEZ",rtrep=.t.,;
    cFormVar="w_CGMODSAV", RowSource=""+"Automatico,"+"Con richiesta,"+"Nessuno" , ;
    ToolTipText = "Modalit� di salvataggio",;
    HelpContextID = 30070908,;
    Height=21, Width=111, Left=599, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCGMODSAV_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CGMODSAV,&i_cF..t_CGMODSAV),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'D',;
    iif(xVal =3,'N',;
    space(1)))))
  endfunc
  func oCGMODSAV_2_2.GetRadio()
    this.Parent.oContained.w_CGMODSAV = this.RadioValue()
    return .t.
  endfunc

  func oCGMODSAV_2_2.ToRadio()
    this.Parent.oContained.w_CGMODSAV=trim(this.Parent.oContained.w_CGMODSAV)
    return(;
      iif(this.Parent.oContained.w_CGMODSAV=='A',1,;
      iif(this.Parent.oContained.w_CGMODSAV=='D',2,;
      iif(this.Parent.oContained.w_CGMODSAV=='N',3,;
      0))))
  endfunc

  func oCGMODSAV_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCGDEFCFG_2_3 as StdTrsCheck with uid="QMCFDUEXOC",rtrep=.t.,;
    cFormVar="w_CGDEFCFG",  caption="",;
    ToolTipText = "Configurazione di default",;
    HelpContextID = 236959635,;
    Left=719, Top=0, Width=16,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , sErrorMsg = "Esiste gi� una configurazione di default";
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oCGDEFCFG_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CGDEFCFG,&i_cF..t_CGDEFCFG),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCGDEFCFG_2_3.GetRadio()
    this.Parent.oContained.w_CGDEFCFG = this.RadioValue()
    return .t.
  endfunc

  func oCGDEFCFG_2_3.ToRadio()
    this.Parent.oContained.w_CGDEFCFG=trim(this.Parent.oContained.w_CGDEFCFG)
    return(;
      iif(this.Parent.oContained.w_CGDEFCFG=='S',1,;
      0))
  endfunc

  func oCGDEFCFG_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCGDEFCFG_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (ControlloDefault(i_curform))
    endwith
    return bRes
  endfunc

  add object oCGTIPCFG_2_4 as StdTrsCombo with uid="BGLWFDMOLL",rtrep=.t.,;
    cFormVar="w_CGTIPCFG", RowSource=""+"Utente,"+"Gruppo,"+"Installazione" , ;
    ToolTipText = "Utente/gruppo/installazione",;
    HelpContextID = 226146195,;
    Height=21, Width=96, Left=258, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCGTIPCFG_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CGTIPCFG,&i_cF..t_CGTIPCFG),this.value)
    return(iif(xVal =1,'U',;
    iif(xVal =2,'G',;
    iif(xVal =3,'I',;
    space(1)))))
  endfunc
  func oCGTIPCFG_2_4.GetRadio()
    this.Parent.oContained.w_CGTIPCFG = this.RadioValue()
    return .t.
  endfunc

  func oCGTIPCFG_2_4.ToRadio()
    this.Parent.oContained.w_CGTIPCFG=trim(this.Parent.oContained.w_CGTIPCFG)
    return(;
      iif(this.Parent.oContained.w_CGTIPCFG=='U',1,;
      iif(this.Parent.oContained.w_CGTIPCFG=='G',2,;
      iif(this.Parent.oContained.w_CGTIPCFG=='I',3,;
      0))))
  endfunc

  func oCGTIPCFG_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oGRUPPO_2_6 as StdTrsField with uid="KNVOKYMHLT",rtseq=7,rtrep=.t.,;
    cFormVar="w_GRUPPO",value=0,;
    HelpContextID = 244081510,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=81, Left=363, Top=0, cSayPict=["999999"], cGetPict=["999999"], bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_GRUPPO"

  func oGRUPPO_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CGTIPCFG, 'U', 'I'))
    endwith
    endif
  endfunc

  func oGRUPPO_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPPO_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oGRUPPO_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oGRUPPO_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oUTENTE_2_7 as StdTrsField with uid="KJCGPFHLYZ",rtseq=8,rtrep=.t.,;
    cFormVar="w_UTENTE",value=0,;
    HelpContextID = 80307782,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=81, Left=363, Top=0, cSayPict=["999999"], cGetPict=["999999"], bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_UTENTE"

  func oUTENTE_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CGTIPCFG, 'G', 'I'))
    endwith
    endif
  endfunc

  func oUTENTE_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTENTE_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oUTENTE_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oUTENTE_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDESCRIU_2_9 as StdTrsField with uid="BSAWMCLZDH",rtseq=9,rtrep=.t.,;
    cFormVar="w_DESCRIU",value=space(20),enabled=.f.,;
    HelpContextID = 144651830,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=148, Left=447, Top=0, InputMask=replicate('X',20)

  add object oDESCRIG_2_11 as StdTrsField with uid="ODRYGLLOLE",rtseq=12,rtrep=.t.,;
    cFormVar="w_DESCRIG",value=space(20),enabled=.f.,;
    HelpContextID = 123783626,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=148, Left=447, Top=1, InputMask=replicate('X',20)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCGDESCFG_2_1.When()
    return(.t.)
  proc oCGDESCFG_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCGDESCFG_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mcg','CFG_GEST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CGNOMGES=CFG_GEST.CGNOMGES";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_mcg
*--- Controllo righe default
Func ControlloDefault(pParent)
   local l_curs
   l_curs=sys(2015)
   pParent.Exec_Select(l_curs, "t_CGDESCFG", "t_CGDESCFG<>'"+pParent.w_CGDESCFG+"' And t_CGTIPCFG="+ICASE(pParent.w_CGTIPCFG='U','1',pParent.w_CGTIPCFG='G','2','3')+;
                               " And t_CGUTEGRP="+AllTrim(Str(pParent.w_CGUTEGRP))+" And t_CGDEFCFG=1 And t_CGCODAZI='"+pParent.w_CGCODAZI+"'")
   *--- Se ho una riga con stessa tipologia, gruppo o utente e gi� selezionata come default do il messaggio
   If Used(l_curs) And Reccount(l_curs)>0 And pParent.w_CGDEFCFG='S'
     Use In Select(l_curs)
     return .f.
   EndIf
   return .t.
EndProc
* --- Fine Area Manuale
