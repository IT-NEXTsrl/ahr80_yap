* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: TX_MODPL                                                        *
*              MODULO POLIVALENTE                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 31/5/01                                                         *
* Last revis.: 17/4/09                                                         *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
private w_StComp,w_MEMO,w_CODCAP,w_LOCALI,w_PROVIN
private w_STRINGA,w_RFDES,w_ANDESC1,w_RFINDI,w_ANINDI1
private w_ANDESCx,w_DATITEST,w_NAZION,w_TXTDES,w_ANCODICE
private w_ANDESC,w_ANCODICE,w_ANINDI,w_DATITEST2,w_NAZION
private w_StComp,w_CNAZ,w_IVAFIS,w_CODPAG,w_RATPAG
private w_TIPPAG,w_NUMDOC,w_DATDOC,w_DESBAN,w_DESBAN1
private CONAI,w_MEMO,w_SERIAL,w_DESVAL,w_StComp
private w_MEMO,w_CODCAP,w_LOCALI,w_PROVIN,w_STRINGA
private w_RFDES,w_ANDESC1,w_RFINDI,w_ANINDI1,w_ANDESCx
private w_DATITEST,w_NAZION,w_TXTDES,w_ANCODICE,w_ANDESC
private w_ANCODICE,w_ANINDI,w_DATITEST2,w_NAZION,w_StComp
private w_CNAZ,w_IVAFIS,w_CODPAG,w_RATPAG,w_TIPPAG
private w_NUMDOC,w_DATDOC,w_DESBAN,w_DESBAN1,w_MEMO
private w_SERIAL,w_DESVAL,w_CODICE,w_MVDESART,pMVVALRIG
private w_PREZZO,SCONTO1,SCONT2,w_VALRIG,w_IVA
private SCONT3,SCONT4,w_DESAR2,IMP,w_TXT1
private w_TXT2,w_IMPIVA,IMPOSTA,w_IMPIVA,TOTALEA
private P_CAMBIO,FLIVDF,w_DOC,w_StComp,w_SETBOL
private w_ESIGIB,w_SETBOL,w_StComp,IMPOSTA,w_IMPIVA
private TOTALEDOC,IMPIV1,CORRIS,w_SUMOK1,L_NOTE
private w_VETDAT2,w_PROVET2,w_VETDAT1,w_PROVE1,NONCORRIS
private L_DESDIC,SETRIG,w_TOTALE,w_SUMO,w_TOTALE1
private w_MVSPEINC,w_MVSPETRA,w_IMPIV1,w_IMPOSTA,VALSIM
private w_SPEBOLL,w_MVAIMPN1,w_IVA1,w_AIMPS21,w_IMP
private w_AIMPS1,w_MVAIMPN2,w_IVA2,w_AIMPS22,w_IMP
private w_AIMPS2,w_TOTDOC,w_MVAIMPN3,w_IVA3,w_AIMPS23
private w_IMP,w_AIMPS3,w_MVAIMPN4,w_IVA4,w_AIMPS24
private w_IMP,w_AIMPS4,w_ACCONT,w_MVAIMPN5,w_IVA5
private w_AIMPS25,w_IMP,w_AIMPS5,w_DOCRAT1,w_DOCRAT2
private w_DOCRAT3,w_DOCRAT4,w_DOCRAT5,w_DOCRAT6,w_CORRIS
private w_DOCRA1,w_DOCRA2,w_DOCRA3,w_DOCRA4,w_DOCRA5
private w_DOCRA6,w_DOC_RA1,w_DOC_RA2,w_DOC_RA3,w_DOC_RA4
private w_DOC_RA5,w_DOC_RA6,w_NONCORR,w_DESSPE,w_CAUTRA
private w_MVQTACOL,w_QTALOR,w_DESPOR,w_DATORA,w_DESVET
private w_VETDAT,w_DATTRA,w_DESVE2,w_VETDA2,TOTALE
private CONAI
w_StComp = space(2)
w_MEMO = space(0)
w_CODCAP = space(10)
w_LOCALI = space(10)
w_PROVIN = space(10)
w_STRINGA = space(30)
w_RFDES = space(35)
w_ANDESC1 = space(35)
w_RFINDI = space(35)
w_ANINDI1 = space(35)
w_ANDESCx = space(35)
w_DATITEST = space(35)
w_NAZION = space(35)
w_TXTDES = space(20)
w_ANCODICE = space(35)
w_ANDESC = space(35)
w_ANCODICE = space(15)
w_ANINDI = space(35)
w_DATITEST2 = space(35)
w_NAZION = space(35)
w_StComp = space(2)
w_CNAZ = space(4)
w_IVAFIS = space(20)
w_CODPAG = space(30)
w_RATPAG = space(10)
w_TIPPAG = space(2)
w_NUMDOC = space(10)
w_DATDOC = ctod("  /  /  ")
w_DESBAN = space(10)
w_DESBAN1 = space(10)
CONAI = 0
w_MEMO = space(0)
w_SERIAL = space(10)
w_DESVAL = space(23)
w_StComp = space(2)
w_MEMO = space(0)
w_CODCAP = space(10)
w_LOCALI = space(10)
w_PROVIN = space(10)
w_STRINGA = space(30)
w_RFDES = space(35)
w_ANDESC1 = space(35)
w_RFINDI = space(35)
w_ANINDI1 = space(35)
w_ANDESCx = space(35)
w_DATITEST = space(35)
w_NAZION = space(35)
w_TXTDES = space(20)
w_ANCODICE = space(35)
w_ANDESC = space(35)
w_ANCODICE = space(15)
w_ANINDI = space(35)
w_DATITEST2 = space(35)
w_NAZION = space(35)
w_StComp = space(2)
w_CNAZ = space(4)
w_IVAFIS = space(20)
w_CODPAG = space(30)
w_RATPAG = space(10)
w_TIPPAG = space(2)
w_NUMDOC = space(10)
w_DATDOC = ctod("  /  /  ")
w_DESBAN = space(10)
w_DESBAN1 = space(10)
w_MEMO = space(0)
w_SERIAL = space(10)
w_DESVAL = space(23)
w_CODICE = space(16)
w_MVDESART = space(40)
pMVVALRIG = 0
w_PREZZO = 0
SCONTO1 = 0
SCONT2 = 0
w_VALRIG = 0
w_IVA = space(4)
SCONT3 = 0
SCONT4 = 0
w_DESAR2 = space(0)
IMP = 0
w_TXT1 = space(0)
w_TXT2 = space(0)
w_IMPIVA = 0
IMPOSTA = 0
w_IMPIVA = 0
TOTALEA = 0
P_CAMBIO = 0
FLIVDF = space(1)
w_DOC = space(15)
w_StComp = space(2)
w_SETBOL = space(1)
w_ESIGIB = space(45)
w_SETBOL = space(1)
w_StComp = space(2)
IMPOSTA = 0
w_IMPIVA = 0
TOTALEDOC = 0
IMPIV1 = 0
CORRIS = 0
w_SUMOK1 = 0
L_NOTE = space(40)
w_VETDAT2 = space(30)
w_PROVET2 = space(30)
w_VETDAT1 = space(30)
w_PROVE1 = space(30)
NONCORRIS = 0
L_DESDIC = space(40)
SETRIG = 0
w_TOTALE = space(20)
w_SUMO = 0
w_TOTALE1 = space(20)
w_MVSPEINC = space(13)
w_MVSPETRA = space(13)
w_IMPIV1 = space(20)
w_IMPOSTA = 0
VALSIM = space(5)
w_SPEBOLL = 0
w_MVAIMPN1 = 0
w_IVA1 = space(6)
w_AIMPS21 = 0
w_IMP = space(25)
w_AIMPS1 = 0
w_MVAIMPN2 = 0
w_IVA2 = space(6)
w_AIMPS22 = 0
w_IMP = space(25)
w_AIMPS2 = 0
w_TOTDOC = 0
w_MVAIMPN3 = 0
w_IVA3 = space(6)
w_AIMPS23 = 0
w_IMP = space(25)
w_AIMPS3 = 0
w_MVAIMPN4 = 0
w_IVA4 = space(6)
w_AIMPS24 = 0
w_IMP = space(25)
w_AIMPS4 = 0
w_ACCONT = 0
w_MVAIMPN5 = 0
w_IVA5 = space(6)
w_AIMPS25 = 0
w_IMP = space(25)
w_AIMPS5 = 0
w_DOCRAT1 = space(10)
w_DOCRAT2 = space(10)
w_DOCRAT3 = space(10)
w_DOCRAT4 = space(10)
w_DOCRAT5 = space(10)
w_DOCRAT6 = space(10)
w_CORRIS = 0
w_DOCRA1 = space(10)
w_DOCRA2 = space(10)
w_DOCRA3 = space(10)
w_DOCRA4 = space(10)
w_DOCRA5 = space(10)
w_DOCRA6 = space(10)
w_DOC_RA1 = space(13)
w_DOC_RA2 = space(13)
w_DOC_RA3 = space(13)
w_DOC_RA4 = space(13)
w_DOC_RA5 = space(13)
w_DOC_RA6 = space(13)
w_NONCORR = 0
w_DESSPE = space(20)
w_CAUTRA = space(30)
w_MVQTACOL = space(5)
w_QTALOR = space(8)
w_DESPOR = space(20)
w_DATORA = space(20)
w_DESVET = space(40)
w_VETDAT = space(40)
w_DATTRA = space(20)
w_DESVE2 = space(40)
w_VETDA2 = space(40)
TOTALE = 0
CONAI = 0

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do TX_M4DPL with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do TX_M4DPL with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


procedure TX_M4DPL
* === Procedure TX_M4DPL
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break, i_cond1, i_cond2

do case
  case i_form_id=1
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_cond1 = (MVSERIAL)
    i_cond2 = (MVSERIAL)
    i_frm_brk = .T.
    do while .not. eof() 
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do TX_M4DPL with 11, 0
        i_frm_rpr = .T.
      endif
      if i_cond1<>(MVSERIAL) .or. i_frm_brk
        i_cond1 = (MVSERIAL)
        i_frm_brk = .T.
        do TX_M5DPL with 1.00, 20
      endif
      if i_cond2<>(MVSERIAL) .or. i_frm_brk
        i_cond2 = (MVSERIAL)
        do TX_M4DPL with 98
        i_form_ph = 1.01
        i_form_phh = 20
        i_form_phg = 380
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
        do TX_M5DPL with 1.02, 0
      if (CTRL_A() or CTRL_D()) and not empty(nvl(mvcodice,' ')) AND CTRL_COAC()
        do TX_M5DPL with 1.03, 1
      endif
      if Ctrl2() AND CTRL_COAC()
        do TX_M5DPL with 1.04, 1
      endif
      if Ctrl1() AND Fscrdessup() AND NOT EMPTY(ALLTRIM(NVL(mvdessup,' '))) AND g_PERSDA='S' AND CTRL_COAC()
        do TX_M5DPL with 1.05, 1
      endif
      if (.not. empty(L_DESDIC)  AND ultrig='S') and (nvl(CCTIPDOC,'')<>'FC') AND TDCATDOC<>'DT'
        do TX_M5DPL with 1.06, 1
      endif
      if ultrig='S' And not empty(nvl(L_NOTE,''))
        do TX_M5DPL with 1.07, 1
      endif
      if ultrig='S' AND CONAI<>0
        do TX_M5DPL with 1.08, 1
      endif
      if ultrig='S' and ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S')
        do TX_M5DPL with 1.09, 1
      endif
      if NVL(ANFLESIG,' ')='S' AND (NVL(TDCATDOC,' ')='FA' OR NVL(TDCATDOC,' ')='NC') AND NVL(CCTIPDOC,' ')<>'FC' AND ULTRIG='S'
        do TX_M5DPL with 1.10, 1
      endif
      if i_usr_brk
        exit
      endif
      * --- passa al record successivo
      i_prevrec = recno()
      if .not. eof()
        skip
      endif
      i_currec = iif(eof(), -1, recno())
      if eof()  .or. i_cond2<>(MVSERIAL) .or. i_cond1<>(MVSERIAL)      
        i_form_ph = 12
        i_form_pf = 13
        i_formh13 = i_saveh13
      endif
      if eof()  .or. i_cond1<>(MVSERIAL)      
        go i_prevrec
        do TX_M5DPL with 1.12, 22
        do cplu_go with i_currec
      endif
    enddo
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do TX_M4DPL with 13, 0
        else
          do TX_M5DPL with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do TX_M4DPL with 12, 0
        else
          do TX_M5DPL with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = 1.11
    i_saveh13 = i_formh13
    i_formh13 = 22
endcase
return

procedure TX_M5DPL
* === Procedure TX_M5DPL
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do TX_M4DPL with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
  * --- 1� form
  case i_form_id=1.0
    do frm1_0
  case i_form_id=1.01
    do frm1_01
  case i_form_id=1.02
    do frm1_02
  case i_form_id=1.03
    do frm1_03
  case i_form_id=1.04
    do frm1_04
  case i_form_id=1.05
    do frm1_05
  case i_form_id=1.06
    do frm1_06
  case i_form_id=1.07
    do frm1_07
  case i_form_id=1.08
    do frm1_08
  case i_form_id=1.09
    do frm1_09
  case i_form_id=1.10
    do frm1_10
  case i_form_id=1.11
    do frm1_11
  case i_form_id=1.12
    do frm1_12
endcase
i_row = i_row+i_form_h
return


* --- 1� form
procedure frm1_0
  w_StComp = ' '
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),0,at_x(0),transform(w_StComp,""),i_fn
  w_MEMO = iif(TDCATDOC='RF',looktab('doc_mast','mv__note','mvserial',mvserial),'')
  if .f.
  i_fn = ""
  w_s = 11
  set memowidth to w_s
  Sm = mline(w_MEMO,1)+space(w_s-len(mline(w_MEMO,1)))
  do F_Say with i_row+0,i_row+at_y(0-0),12,at_x(96),alltrim(Sm),i_fn
   endif
  w_CODCAP = iif(TDCATDOC<>'RF','',substr(w_MEMO,76,5))
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),23,at_x(184),transform(w_CODCAP,""),i_fn
   endif
  w_LOCALI = iif(TDCATDOC<>'RF','',substr(w_MEMO,84,30))
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),28,at_x(224),transform(w_LOCALI,""),i_fn
   endif
  w_PROVIN = iif(TDCATDOC<>'RF','',substr(w_MEMO,114,2))
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),33,at_x(264),transform(w_PROVIN,""),i_fn
   endif
  w_STRINGA = iif(TDCATDOC<>'RF','Spett.le',substr(w_MEMO,1,40))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),41,at_x(328),transform(w_STRINGA,""),i_fn
  w_RFDES = substr(w_MEMO,41,35)
  IF .F.
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),38,at_x(304),transform(w_RFDES,""),i_fn
   ENDIF
  w_ANDESC1 = iif(TDCATDOC<>'RF',(iif((nvl(cctipdoc,'')='FC'),(iif(!empty(NVL(ddnomdes,'')),ddnomdes,andescri)),andescri)),w_RFDES)
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),41,at_x(328),transform(w_ANDESC1,Repl('X',35)),i_fn
  w_RFINDI = alltrim(w_codcap)+' '+alltrim(w_locali)+' '+alltrim(w_provin)
  IF .F.
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),38,at_x(304),transform(w_RFINDI,""),i_fn
   ENDIF
  w_ANINDI1 = iif(TDCATDOC<>'RF',(iif((nvl(cctipdoc,'')='FC'),(iif(!empty(NVL(ddindiri,'')),ddindiri,anindiri)),anindiri)),w_RFINDI)
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),41,at_x(328),transform(w_ANINDI1,Repl('X',35)),i_fn
  w_ANDESCx = IIF((TDCATDOC='DT' or (TDCATDOC='FA' AND TDFLACCO='S')),nomdes,(IIF(TDCATDOC$'FA-NC',ddnomdes,andescri)))
  IF .F.
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),38,at_x(304),transform(w_ANDESCx,Repl('X',35)),i_fn
   ENDIF
  w_DATITEST = iif(TDCATDOC<>'RF',left(Fdatitest(),35),substr(w_MEMO,116,16))
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),41,at_x(328),transform(w_DATITEST,Repl('X',35)),i_fn
  w_NAZION = Fnaz1()
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),41,at_x(328),transform(w_NAZION,""),i_fn
  w_TXTDES = IIF((NVL(TDCATDOC,' ')='NC' OR (NVL(TDCATDOC,' ')='FA' AND TDFLACCO<>'S')),'SEDE DI FATTURAZIONE','DESTINAZIONE DIVERSA')
  if !EMPTY(NVL(w_ANDESCx,'')) and (nvl(CCTIPDOC,'')<>'FC')
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),41,at_x(328),transform(w_TXTDES,""),i_fn
  endif
  w_ANCODICE = iif(TDCATDOC<>'RF','',substr(w_MEMO,1,35))
  if .f.
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(114-0),37,at_x(296),transform(w_ANCODICE,Repl('X',35)),i_fn
   endif
  w_ANDESC = IIF((TDCATDOC='DT' or (TDCATDOC='FA' AND TDFLACCO='S')),nomdes,(IIF(TDCATDOC$'FA-NC',ddnomdes,andescri)))
  if (NVL(CCTIPDOC,'')<>'FC')
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(114-0),41,at_x(328),transform(w_ANDESC,Repl('X',35)),i_fn
  endif
  w_ANCODICE = ancodice
  if .f.
  if !empty(nvl(coddes,'')) and (CCTIPDOC<>'FC')
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),37,at_x(296),transform(w_ANCODICE,Repl('X',15)),i_fn
   endif
  endif
  w_ANINDI = IIF((TDCATDOC='DT' or (TDCATDOC='FA' AND TDFLACCO='S')),indiri,(IIF(TDCATDOC$'FA-NC',ddindiri,anindiri)))
  if (NVL(CCTIPDOC,'')<>'FC')
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),41,at_x(328),transform(w_ANINDI,Repl('X',35)),i_fn
  endif
  w_DATITEST2 = left(Fdatitest2(),35)
  if (NVL(CCTIPDOC,'')<>'FC')
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),41,at_x(328),transform(w_DATITEST2,Repl('X',35)),i_fn
  endif
  w_NAZION = Fnaz2()
  if (NVL(CCTIPDOC,'')<>'FC')
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(171-0),41,at_x(328),transform(w_NAZION,""),i_fn
  endif
  w_StComp = ' '
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(171-0),78,at_x(624),transform(w_StComp,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  if TDCATDOC='DT'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(190-0),89,at_x(712),"X",i_fn
  endif
  if TDCATDOC='RF'
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(228-0),0,at_x(0),"X",i_fn
  endif
  if TDCATDOC='FA' AND NVL(CCTIPDOC,' ')='FC'
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(228-0),21,at_x(168),"X",i_fn
  endif
  if TDCATDOC='FA' AND NVL(CCTIPDOC,' ')<>'FC' AND NVL(TDFLACCO,' ')<>'S'
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(228-0),45,at_x(360),"X",i_fn
  endif
  if TDCATDOC='FA' AND NVL(CCTIPDOC,' ')<>'FC' AND NVL(TDFLACCO,' ')='S'
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(228-0),61,at_x(488),"X",i_fn
  endif
  if TDCATDOC='NC'
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(228-0),116,at_x(928),"X",i_fn
  endif
  i_PAG=IIF(w_SERIAL<>MVSERIAL,1,i_PAG)
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(228-0),130,at_x(1040),transform(i_pag,"9999"),i_fn
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(266-0),1,at_x(8),transform(MVCODCON,""),i_fn
  w_CNAZ = LTRIM(nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),''))
  if !empty(nvl(anpariva,''))
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(266-0),18,at_x(144),transform(w_CNAZ,""),i_fn
  endif
  w_IVAFIS = ltrim(iif(empty(anpariva),ancodfis,(iif(TDCATDOC='RF',substr(w_MEMO,118,16),anpariva))))
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(266-0),22,at_x(176),transform(w_IVAFIS,""),i_fn
  w_CODPAG = Caldespag()
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(266-0),39,at_x(312),transform(w_CODPAG,""),i_fn
  w_RATPAG = NVL(LOOKTAB('DOC_RATE','RSMODPAG','RSSERIAL',MVSERIAL,'RSNUMRAT',1),' ')
  IF .F.
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(266-0),73,at_x(584),transform(w_RATPAG,""),i_fn
   ENDIF
  w_TIPPAG = NVL(LOOKTAB('MOD_PAGA','MPTIPPAG','MPCODICE',w_RATPAG),' ')
  IF .F.
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(266-0),80,at_x(640),transform(w_TIPPAG,""),i_fn
   ENDIF
  w_NUMDOC = Alltrim(str(mvnumdoc,15))+iif(empty(mvalfdoc),'','/'+Alltrim(mvalfdoc))
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(266-0),103,at_x(824),transform(w_NUMDOC,""),i_fn
  w_DATDOC = CP_TODATE(MVDATDOC)
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(266-0),126,at_x(1008),transform(w_DATDOC,""),i_fn
  i_fn = ""
  do F_Say with i_row+16,i_row+at_y(304-0),1,at_x(8),transform(MVCODAGE,""),i_fn
  i_fn = ""
  do F_Say with i_row+16,i_row+at_y(304-0),8,at_x(64),transform(AGDESAGE,""),i_fn
  w_DESBAN = nvl(looktab('ban_che','badesban','bacodban',mvcodban),'')
  if w_TIPPAG<>'MA' AND w_TIPPAG<>'BO'
  i_fn = ""
  do F_Say with i_row+16,i_row+at_y(304-0),68,at_x(544),transform(w_DESBAN,""),i_fn
  endif
  w_DESBAN1 = nvl(looktab('coc_mast','badescri','bacodban',mvcodba2),'')
  if w_TIPPAG='MA' OR w_TIPPAG='BO'
  i_fn = ""
  do F_Say with i_row+16,i_row+at_y(304-0),68,at_x(544),transform(w_DESBAN1,""),i_fn
  endif
  i_fn = ""
  do F_Say with i_row+16,i_row+at_y(304-0),128,at_x(1024),transform(MVTCOMAG,""),i_fn
  i_fn = ""
  do F_Say with i_row+18,i_row+at_y(342-0),1,at_x(8),transform(MVRIFORD,""),i_fn
  CONAI = 0
  IF .F.
  i_fn = ""
  do F_Say with i_row+18,i_row+at_y(342-0),16,at_x(128),transform(CONAI,""),i_fn
   ENDIF
  w_MEMO = ''
  if .f.
  i_fn = ""
  w_s = 11
  set memowidth to w_s
  Sm = mline(w_MEMO,1)+space(w_s-len(mline(w_MEMO,1)))
  do F_Say with i_row+18,i_row+at_y(342-0),48,at_x(384),alltrim(Sm),i_fn
   endif
  w_SERIAL = MVSERIAL
  IF .F.
  i_fn = ""
  do F_Say with i_row+18,i_row+at_y(342-0),97,at_x(776),transform(w_SERIAL,""),i_fn
   ENDIF
  if .f.
  i_fn = ""
  do F_Say with i_row+18,i_row+at_y(342-0),101,at_x(808),transform(VACODVAL,""),i_fn
   endif
  w_DESVAL = left(nvl(looktab('VALUTE','VADESVAL','VACODVAL',VACODVAL),''),23)
  i_fn = ""
  do F_Say with i_row+18,i_row+at_y(342-0),103,at_x(824),transform(w_DESVAL,""),i_fn
   Ambdoc()
  i_ml = 0
  w_s = 11
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_MEMO))
  w_s = 11
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_MEMO))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_M4DPL with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 11
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_MEMO,i_i)+space(w_s-len(mline(w_MEMO,i_i)))
    do F_Say with i_row,i_row,12,at_x(96),alltrim(Sm),i_fn
    w_s = 11
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_MEMO,i_i)+space(w_s-len(mline(w_MEMO,i_i)))
    do F_Say with i_row,i_row,48,at_x(384),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_01
procedure frm1_01
  w_StComp = ' '
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(399-398),0,at_x(0),transform(w_StComp,""),i_fn
  w_MEMO = iif(TDCATDOC='RF',looktab('doc_mast','mv__note','mvserial',mvserial),'')
  if .f.
  i_fn = ""
  w_s = 11
  set memowidth to w_s
  Sm = mline(w_MEMO,1)+space(w_s-len(mline(w_MEMO,1)))
  do F_Say with i_row+0,i_row+at_y(399-398),12,at_x(96),alltrim(Sm),i_fn
   endif
  w_CODCAP = iif(TDCATDOC<>'RF','',substr(w_MEMO,76,5))
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(399-398),23,at_x(184),transform(w_CODCAP,""),i_fn
   endif
  w_LOCALI = iif(TDCATDOC<>'RF','',substr(w_MEMO,84,30))
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(399-398),28,at_x(224),transform(w_LOCALI,""),i_fn
   endif
  w_PROVIN = iif(TDCATDOC<>'RF','',substr(w_MEMO,114,2))
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(399-398),33,at_x(264),transform(w_PROVIN,""),i_fn
   endif
  w_STRINGA = iif(TDCATDOC<>'RF','Spett.le',substr(w_MEMO,1,40))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(399-398),41,at_x(328),transform(w_STRINGA,""),i_fn
  w_RFDES = substr(w_MEMO,41,35)
  IF .F.
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(418-398),38,at_x(304),transform(w_RFDES,""),i_fn
   ENDIF
  w_ANDESC1 = iif(TDCATDOC<>'RF',(iif((nvl(cctipdoc,'')='FC'),(iif(!empty(NVL(ddnomdes,'')),ddnomdes,andescri)),andescri)),w_RFDES)
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(418-398),41,at_x(328),transform(w_ANDESC1,Repl('X',35)),i_fn
  w_RFINDI = alltrim(w_codcap)+' '+alltrim(w_locali)+' '+alltrim(w_provin)
  IF .F.
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(437-398),38,at_x(304),transform(w_RFINDI,""),i_fn
   ENDIF
  w_ANINDI1 = iif(TDCATDOC<>'RF',(iif((nvl(cctipdoc,'')='FC'),(iif(!empty(NVL(ddindiri,'')),ddindiri,anindiri)),anindiri)),w_RFINDI)
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(437-398),41,at_x(328),transform(w_ANINDI1,Repl('X',35)),i_fn
  w_ANDESCx = IIF((TDCATDOC='DT' or (TDCATDOC='FA' AND TDFLACCO='S')),nomdes,(IIF(TDCATDOC$'FA-NC',ddnomdes,andescri)))
  IF .F.
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(456-398),38,at_x(304),transform(w_ANDESCx,Repl('X',35)),i_fn
   ENDIF
  w_DATITEST = iif(TDCATDOC<>'RF',left(Fdatitest(),35),substr(w_MEMO,116,16))
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(456-398),41,at_x(328),transform(w_DATITEST,Repl('X',35)),i_fn
  w_NAZION = Fnaz1()
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(475-398),41,at_x(328),transform(w_NAZION,""),i_fn
  w_TXTDES = IIF((NVL(TDCATDOC,' ')='NC' OR (NVL(TDCATDOC,' ')='FA' AND TDFLACCO<>'S')),'SEDE DI FATTURAZIONE','DESTINAZIONE DIVERSA')
  if !EMPTY(NVL(w_ANDESCx,'')) and (nvl(CCTIPDOC,'')<>'FC')
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(494-398),41,at_x(328),transform(w_TXTDES,""),i_fn
  endif
  w_ANCODICE = iif(TDCATDOC<>'RF','',substr(w_MEMO,1,35))
  if .f.
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(513-398),36,at_x(288),transform(w_ANCODICE,Repl('X',35)),i_fn
   endif
  w_ANDESC = IIF((TDCATDOC='DT' or (TDCATDOC='FA' AND TDFLACCO='S')),nomdes,(IIF(TDCATDOC$'FA-NC',ddnomdes,andescri)))
  if (NVL(CCTIPDOC,'')<>'FC')
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(513-398),41,at_x(328),transform(w_ANDESC,Repl('X',35)),i_fn
  endif
  w_ANCODICE = ancodice
  if .f.
  if !empty(nvl(coddes,'')) and (CCTIPDOC<>'FC')
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(532-398),36,at_x(288),transform(w_ANCODICE,Repl('X',15)),i_fn
   endif
  endif
  w_ANINDI = IIF((TDCATDOC='DT' or (TDCATDOC='FA' AND TDFLACCO='S')),indiri,(IIF(TDCATDOC$'FA-NC',ddindiri,anindiri)))
  if (NVL(CCTIPDOC,'')<>'FC')
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(532-398),41,at_x(328),transform(w_ANINDI,Repl('X',35)),i_fn
  endif
  w_DATITEST2 = left(Fdatitest2(),35)
  if (NVL(CCTIPDOC,'')<>'FC')
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(551-398),41,at_x(328),transform(w_DATITEST2,Repl('X',35)),i_fn
  endif
  w_NAZION = Fnaz2()
  if (NVL(CCTIPDOC,'')<>'FC')
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(570-398),41,at_x(328),transform(w_NAZION,""),i_fn
  endif
  w_StComp = ' '
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(570-398),78,at_x(624),transform(w_StComp,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  if TDCATDOC='DT'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(589-398),89,at_x(712),"X",i_fn
  endif
  if TDCATDOC='RF'
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(627-398),0,at_x(0),"X",i_fn
  endif
  if TDCATDOC='FA' AND NVL(CCTIPDOC,' ')='FC'
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(627-398),20,at_x(160),"X",i_fn
  endif
  if TDCATDOC='FA' AND NVL(CCTIPDOC,' ')<>'FC' AND NVL(TDFLACCO,' ')<>'S'
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(627-398),45,at_x(360),"X",i_fn
  endif
  if TDCATDOC='FA' AND NVL(CCTIPDOC,' ')<>'FC' AND NVL(TDFLACCO,' ')='S'
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(627-398),61,at_x(488),"X",i_fn
  endif
  if TDCATDOC='NC'
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(627-398),115,at_x(920),"X",i_fn
  endif
  i_PAG=IIF(w_SERIAL<>MVSERIAL,1,i_PAG)
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(627-398),130,at_x(1040),transform(i_pag,"9999"),i_fn
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(665-398),1,at_x(8),transform(MVCODCON,""),i_fn
  w_CNAZ = LTRIM(nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),''))
  if !empty(nvl(anpariva,''))
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(665-398),18,at_x(144),transform(w_CNAZ,""),i_fn
  endif
  w_IVAFIS = ltrim(iif(empty(anpariva),ancodfis,(iif(TDCATDOC='RF',substr(w_MEMO,118,16),anpariva))))
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(665-398),22,at_x(176),transform(w_IVAFIS,""),i_fn
  w_CODPAG = Caldespag()
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(665-398),39,at_x(312),transform(w_CODPAG,""),i_fn
  w_RATPAG = NVL(LOOKTAB('DOC_RATE','RSMODPAG','RSSERIAL',MVSERIAL,'RSNUMRAT',1),' ')
  IF .F.
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(665-398),74,at_x(592),transform(w_RATPAG,""),i_fn
   ENDIF
  w_TIPPAG = NVL(LOOKTAB('MOD_PAGA','MPTIPPAG','MPCODICE',w_RATPAG),' ')
  IF .F.
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(665-398),81,at_x(648),transform(w_TIPPAG,""),i_fn
   ENDIF
  w_NUMDOC = Alltrim(str(mvnumdoc,15))+iif(empty(mvalfdoc),'','/'+Alltrim(mvalfdoc))
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(665-398),103,at_x(824),transform(w_NUMDOC,""),i_fn
  w_DATDOC = CP_TODATE(MVDATDOC)
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(665-398),126,at_x(1008),transform(w_DATDOC,""),i_fn
  i_fn = ""
  do F_Say with i_row+16,i_row+at_y(703-398),1,at_x(8),transform(MVCODAGE,""),i_fn
  i_fn = ""
  do F_Say with i_row+16,i_row+at_y(703-398),8,at_x(64),transform(AGDESAGE,""),i_fn
  w_DESBAN = nvl(looktab('ban_che','badesban','bacodban',mvcodban),'')
  if w_TIPPAG<>'MA' AND w_TIPPAG<>'BO'
  i_fn = ""
  do F_Say with i_row+16,i_row+at_y(703-398),68,at_x(544),transform(w_DESBAN,""),i_fn
  endif
  w_DESBAN1 = nvl(looktab('coc_mast','badescri','bacodban',mvcodba2),'')
  if w_TIPPAG='MA' OR w_TIPPAG='BO'
  i_fn = ""
  do F_Say with i_row+16,i_row+at_y(703-398),68,at_x(544),transform(w_DESBAN1,""),i_fn
  endif
  i_fn = ""
  do F_Say with i_row+16,i_row+at_y(703-398),128,at_x(1024),transform(MVTCOMAG,""),i_fn
  i_fn = ""
  do F_Say with i_row+18,i_row+at_y(741-398),1,at_x(8),transform(MVRIFORD,""),i_fn
  w_MEMO = ''
  if .f.
  i_fn = ""
  w_s = 11
  set memowidth to w_s
  Sm = mline(w_MEMO,1)+space(w_s-len(mline(w_MEMO,1)))
  do F_Say with i_row+18,i_row+at_y(741-398),48,at_x(384),alltrim(Sm),i_fn
   endif
  w_SERIAL = MVSERIAL
  IF .F.
  i_fn = ""
  do F_Say with i_row+18,i_row+at_y(741-398),98,at_x(784),transform(w_SERIAL,""),i_fn
   ENDIF
  if .f.
  i_fn = ""
  do F_Say with i_row+18,i_row+at_y(741-398),101,at_x(808),transform(VACODVAL,""),i_fn
   endif
  w_DESVAL = left(nvl(looktab('VALUTE','VADESVAL','VACODVAL',VACODVAL),''),23)
  i_fn = ""
  do F_Say with i_row+18,i_row+at_y(741-398),103,at_x(824),transform(w_DESVAL,""),i_fn
   Ambdoc()
  i_ml = 0
  w_s = 11
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_MEMO))
  w_s = 11
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_MEMO))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_M4DPL with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 11
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_MEMO,i_i)+space(w_s-len(mline(w_MEMO,i_i)))
    do F_Say with i_row,i_row,12,at_x(96),alltrim(Sm),i_fn
    w_s = 11
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_MEMO,i_i)+space(w_s-len(mline(w_MEMO,i_i)))
    do F_Say with i_row,i_row,48,at_x(384),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_02
procedure frm1_02
return

* --- frm1_03
procedure frm1_03
  w_CODICE = iif(nvl(anflcodi,' ')='S',left(mvcodice,16),left(mvcodart,16))
  if nvl(arstacod,' ')<>'S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(817-816),0,at_x(0),transform(w_CODICE,""),i_fn
   CONAI=CONAI+iif(arflcona='S' or arflcon2='S',1,0)
  endif
  w_MVDESART = left(alltrim(MVDESART),40)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(817-816),17,at_x(136),transform(w_MVDESART,""),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(817-816),57,at_x(459),transform(MVUNIMIS,Repl('X',3)),i_fn
  if mvtiprig<>'D' and fcodart()
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(817-816),61,at_x(488),transform(MVQTAMOV,'@Z '+v_PQ(11)),i_fn
  endif
  pMVVALRIG = IIF(MVFLOMAG='X',MVVALRIG,0)
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(817-816),74,at_x(592),transform(pMVVALRIG,""),i_fn
   ENDIF
   TOTALE = TOTALE+pMVVALRIG
  w_PREZZO = iif(mvrifkit<>0,'',TRAN(mvprezzo,'@Z '+V_PV[38+(20*VADECUNI)]))
  if (NVL(TDCATDOC,'')<>'DT') OR anprebol='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(817-816),82,at_x(656),transform(w_PREZZO,""),i_fn
  endif
  SCONTO1 = iif(mvrifkit<>0,0,mvscont1)
  if Ctrl3() .and. sconto1<>0 .AND. ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S')
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(817-816),103,at_x(824),transform(SCONTO1,'999.99'),i_fn
  endif
  SCONT2 = iif(mvrifkit<>0,0,mvscont2)
  if Ctrl4() .and. scont2<>0 .AND. ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S')
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(817-816),109,at_x(872),transform(SCONT2,'999.99'),i_fn
  endif
  w_VALRIG = right(space(16)+alltrim(FValrig()),16)
  if ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S')
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(817-816),116,at_x(928),transform(w_VALRIG,""),i_fn
  endif
  w_IVA = left(alltrim(FIva())+Space(4),4)
  if ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S') and (!empty(mvprezzo))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(817-816),133,at_x(1064),transform(w_IVA,Repl('X',4)),i_fn
  endif
return

* --- frm1_04
procedure frm1_04
  SCONT3 = iif(mvrifkit<>0,0,mvscont3)
  if scont3<>0 .AND. ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S')
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(855-854),103,at_x(824),transform(SCONT3,'999.99'),i_fn
  endif
  SCONT4 = iif(mvrifkit<>0,0,mvscont4)
  if scont4<>0 .AND. ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S')
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(855-854),109,at_x(874),transform(SCONT4,'999.99'),i_fn
  endif
return

* --- frm1_05
procedure frm1_05
  w_DESAR2 = mvdessup
  i_fn = ""
  w_s = 37
  set memowidth to w_s
  Sm = mline(w_DESAR2,1)+space(w_s-len(mline(w_DESAR2,1)))
  do F_Say with i_row+0,i_row+at_y(893-892),17,at_x(136),alltrim(Sm),i_fn
  i_ml = 0
  w_s = 37
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_DESAR2))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_M4DPL with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 37
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_DESAR2,i_i)+space(w_s-len(mline(w_DESAR2,i_i)))
    do F_Say with i_row,i_row,17,at_x(136),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_06
procedure frm1_06
  IMP = TRAN(L_IMPONI,'@Z '+V_PV[20*(VADECTOT+2)])
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(931-930),2,at_x(16),transform(IMP,""),i_fn
   ENDIF
  w_TXT1 = L_DESDIC
  if .not. empty(L_DESDIC)  and ultrig='S'
  i_fn = ""
  w_s = 37
  set memowidth to w_s
  Sm = mline(w_TXT1,1)+space(w_s-len(mline(w_TXT1,1)))
  do F_Say with i_row+0,i_row+at_y(931-930),17,at_x(136),alltrim(Sm),i_fn
  endif
  i_ml = 0
  w_s = 37
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_TXT1))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_M4DPL with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 37
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_TXT1,i_i)+space(w_s-len(mline(w_TXT1,i_i)))
    do F_Say with i_row,i_row,17,at_x(136),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_07
procedure frm1_07
  w_TXT2 = L_NOTE
  if ultrig='S'
  i_fn = ""
  w_s = 37
  set memowidth to w_s
  Sm = mline(w_TXT2,1)+space(w_s-len(mline(w_TXT2,1)))
  do F_Say with i_row+0,i_row+at_y(969-968),17,at_x(136),alltrim(Sm),i_fn
  endif
  i_ml = 0
  w_s = 37
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_TXT2))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_M4DPL with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 37
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_TXT2,i_i)+space(w_s-len(mline(w_TXT2,i_i)))
    do F_Say with i_row,i_row,17,at_x(136),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_08
procedure frm1_08
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1007-1006),17,at_x(136),"Contributo CONAI assolto",i_fn
return

* --- frm1_09
procedure frm1_09
  w_IMPIVA = FImpiva()
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1045-1044),0,at_x(0),transform(w_IMPIVA,""),i_fn
   ENDIF
  IMPOSTA = FImposta()
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1045-1044),3,at_x(24),transform(IMPOSTA,""),i_fn
   endif
  w_IMPIVA = FImpiva()
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1045-1044),6,at_x(48),transform(w_IMPIVA,""),i_fn
   ENDIF
  TOTALEA = w_impiva+imposta+mvimparr
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1045-1044),8,at_x(64),transform(TOTALEA,""),i_fn
   ENDIF
  P_CAMBIO = L_CAMBIO
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1045-1044),11,at_x(88),transform(P_CAMBIO,""),i_fn
   endif
return

* --- frm1_10
procedure frm1_10
  FLIVDF = nvl(looktab('cau_cont','ccflivdf','cccodice',mvcaucon),'')
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1083-1082),4,at_x(32),transform(FLIVDF,""),i_fn
   ENDIF
  w_DOC = IIF(TDCATDOC='FA','Fattura','Nota di Credito')
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1083-1082),6,at_x(48),transform(w_DOC,""),i_fn
   endif
  w_StComp = ' '
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1083-1082),12,at_x(96),transform(w_StComp,""),i_fn
  IF .F.
  if len(trim('&w_t_stbold'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stbold,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1083-1082),15,at_x(120),transform(w_SETBOL,""),i_fn
   ENDIF
  w_ESIGIB = IIF(FLIVDF='S',w_DOC+' ad esigibilita'+"'"+' Differita',w_DOC+' ad esigibilita'+"'"+' Immediata')
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1083-1082),18,at_x(144),transform(w_ESIGIB,""),i_fn
  IF .F.
  if len(trim('&w_t_stbol_'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stbol_,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1083-1082),64,at_x(512),transform(w_SETBOL,""),i_fn
   ENDIF
  w_StComp = ' '
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1083-1082),66,at_x(528),transform(w_StComp,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
return

* --- frm1_11
procedure frm1_11
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(1273-1120),116,at_x(933),"SEGUE =======>",i_fn
return

* --- frm1_12
procedure frm1_12
  IMPOSTA = FImposta()
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),1,at_x(11),transform(IMPOSTA,""),i_fn
   endif
  w_IMPIVA = FImpiva()
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),4,at_x(33),transform(w_IMPIVA,""),i_fn
   ENDIF
  TOTALEDOC = w_impiva+imposta+mvimparr
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),6,at_x(53),transform(TOTALEDOC,""),i_fn
   ENDIF
  IMPIV1 = nvl(mvaimpn1,0)+nvl(mvaimpn2,0)+nvl(mvaimpn3,0)+nvl(mvaimpn4,0)+nvl(mvaimpn5,0)+nvl(mvaimpn6,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),9,at_x(75),transform(IMPIV1,""),i_fn
   endif
  CORRIS = iif(MVACCONT=0,0,MVACCONT)
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),11,at_x(95),transform(CORRIS,""),i_fn
   ENDIF
  w_SUMOK1 = ROUND(((((TOTALE-(-MVSCONTI))-TOTALE)/TOTALE)*100),2)
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),14,at_x(114),transform(w_SUMOK1,""),i_fn
   ENDIF
  L_NOTE = ' '
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),17,at_x(136),transform(L_NOTE,""),i_fn
   ENDIF
  w_VETDAT2 = FVetdat2()
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),19,at_x(157),transform(w_VETDAT2,""),i_fn
   ENDIF
  w_PROVET2 = NVL(looktab('vettori','vtprovet','vtcodvet',mvcodve2),'')
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),22,at_x(178),transform(w_PROVET2,""),i_fn
   ENDIF
  w_VETDAT1 = FVetdat1()
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),24,at_x(198),transform(w_VETDAT1,""),i_fn
   ENDIF
  w_PROVE1 = NVL(looktab('vettori','vtprovet','vtcodvet',mvcodvet),'')
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),27,at_x(217),transform(w_PROVE1,""),i_fn
   ENDIF
  NONCORRIS = iif(mvflsald='S',0,(iif(MVACCONT=0,totaledoc,totaledoc-corris)))
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),30,at_x(241),transform(NONCORRIS,""),i_fn
   ENDIF
  L_DESDIC = ' '
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),33,at_x(265),transform(L_DESDIC,""),i_fn
   ENDIF
  SETRIG = ' '
  I_ROW=46
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1558-1557),35,at_x(287),transform(SETRIG,""),i_fn
  w_TOTALE = TRAN(totale,'@Z '+V_PV[20*(VADECTOT+2)])
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1577-1557),0,at_x(0),transform(w_TOTALE,""),i_fn
   w_TOTALE=0
  endif
  w_SUMO = IIF(MVSCOCL2<>0 OR(MVSCOCL1<>0 AND MVSCOPAG<>0),w_SUMOK1,IIF(MVSCOCL1<>0,MVSCOCL1,IIF(NOT EMPTY(MVSCOPAG),MVSCOPAG,MVSCONTI)))
  if NOT EMPTY(MVSCONTI) and (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1577-1557),23,at_x(186),transform(w_SUMO,'999.99'),i_fn
  endif
  w_TOTALE1 = TRAN(TOTALE-(-MVSCONTI),'@Z '+V_PV[20*(VADECTOT+2)])
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1577-1557),34,at_x(272),transform(w_TOTALE1,""),i_fn
   w_TOTALE1=0
  endif
  w_MVSPEINC = TRAN(MVSPEINC+MVSPEIMB,'@Z '+V_PV[33+(20*VADECTOT)])
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1577-1557),54,at_x(437),transform(w_MVSPEINC,""),i_fn
  endif
  w_MVSPETRA = TRAN(mvspetra,'@Z '+V_PV[33+(20*VADECTOT)])
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1577-1557),73,at_x(587),transform(w_MVSPETRA,""),i_fn
  endif
  w_IMPIV1 = IIF(TDCATDOC='DT','0',TRAN(impiv1,'@Z '+V_PV[20*(VADECTOT+2)]))
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1577-1557),89,at_x(716),transform(w_IMPIV1,""),i_fn
  w_IMPOSTA = IIF(TDCATDOC='DT',STR(0),TRAN(imposta,'@Z '+V_PV[20*(VADECTOT+2)]))
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1577-1557),110,at_x(886),transform(w_IMPOSTA,""),i_fn
  VALSIM = g_VALSIM
  if mvcodval<>g_codeur
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(1615-1557),79,at_x(632),transform(VALSIM,""),i_fn
  endif
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(1615-1557),104,at_x(835),transform(VASIMVAL,""),i_fn
  w_SPEBOLL = TRAN(mvspebol,'@Z '+V_PV[20*(VADECTOT+2)])
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(1615-1557),110,at_x(887),transform(w_SPEBOLL,""),i_fn
  endif
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(1634-1557),1,at_x(8),transform(MVACIVA1,""),i_fn
  endif
  w_MVAIMPN1 = TRAN(mvaimpn1,'@Z '+V_PV[20*(VADECTOT+2)])
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(1634-1557),33,at_x(264),transform(w_MVAIMPN1,""),i_fn
  endif
  w_IVA1 = tran(iva(1,1),'999.99')
  if not empty(iva(1,1)) .and. (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(1634-1557),54,at_x(439),transform(w_IVA1,""),i_fn
  endif
  w_AIMPS21 = TRAN(round(mvaimps1/mvcaoval,2),'@Z '+V_PV[20*(VADECTOT+2)])
  if mvcodval<>g_codeur AND NOT empty(iva(1,1))
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(1634-1557),64,at_x(512),transform(w_AIMPS21,""),i_fn
  endif
  w_IMP = alltrim(iva(1,2))
  if empty(iva(1,1))
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(1634-1557),70,at_x(560),transform(w_IMP,""),i_fn
  endif
  w_AIMPS1 = TRAN(mvaimps1,'@Z '+V_PV[20*(VADECTOT+2)])
  if NOT empty(iva(1,1))
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(1634-1557),89,at_x(712),transform(w_AIMPS1,""),i_fn
  endif
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(1653-1557),1,at_x(8),transform(MVACIVA2,""),i_fn
  endif
  w_MVAIMPN2 = TRAN(mvaimpn2,'@Z '+V_PV[20*(VADECTOT+2)])
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(1653-1557),33,at_x(264),transform(w_MVAIMPN2,""),i_fn
  endif
  w_IVA2 = tran(iva(2,1),'999.99')
  if not empty(iva(2,1)) .and. (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(1653-1557),54,at_x(439),transform(w_IVA2,""),i_fn
  endif
  w_AIMPS22 = TRAN(round(mvaimps2/mvcaoval,2),'@Z '+V_PV[20*(VADECTOT+2)])
  if mvcodval<>g_codeur AND NOT empty(iva(2,1))
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(1653-1557),64,at_x(512),transform(w_AIMPS22,""),i_fn
  endif
  w_IMP = alltrim(iva(2,2))
  if empty(iva(2,1))
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(1653-1557),70,at_x(560),transform(w_IMP,""),i_fn
  endif
  w_AIMPS2 = TRAN(mvaimps2,'@Z '+V_PV[20*(VADECTOT+2)])
  if NOT empty(iva(2,1))
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(1653-1557),89,at_x(712),transform(w_AIMPS2,""),i_fn
  endif
  w_TOTDOC = IIF(TDCATDOC='DT',STR(0),TRAN(totaledoc, V_PV[20*(VADECTOT+2)]))
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(1653-1557),110,at_x(886),transform(w_TOTDOC,""),i_fn
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(1672-1557),1,at_x(8),transform(MVACIVA3,""),i_fn
  endif
  w_MVAIMPN3 = TRAN(mvaimpn3,'@Z '+V_PV[20*(VADECTOT+2)])
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(1672-1557),33,at_x(264),transform(w_MVAIMPN3,""),i_fn
  endif
  w_IVA3 = tran(iva(3,1),'999.99')
  if not empty(iva(3,1)) .and. (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(1672-1557),54,at_x(439),transform(w_IVA3,""),i_fn
  endif
  w_AIMPS23 = TRAN(round(mvaimps3/mvcaoval,2),'@Z '+V_PV[20*(VADECTOT+2)])
  if mvcodval<>g_codeur AND NOT empty(iva(3,1))
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(1672-1557),64,at_x(512),transform(w_AIMPS23,""),i_fn
  endif
  w_IMP = alltrim(iva(3,2))
  if empty(iva(3,1))
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(1672-1557),70,at_x(560),transform(w_IMP,""),i_fn
  endif
  w_AIMPS3 = TRAN(mvaimps3,'@Z '+V_PV[20*(VADECTOT+2)])
  if NOT empty(iva(3,1))
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(1672-1557),89,at_x(712),transform(w_AIMPS3,""),i_fn
  endif
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(1691-1557),1,at_x(8),transform(MVACIVA4,""),i_fn
  endif
  w_MVAIMPN4 = TRAN(mvaimpn4,'@Z '+V_PV[20*(VADECTOT+2)])
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(1691-1557),33,at_x(264),transform(w_MVAIMPN4,""),i_fn
  endif
  w_IVA4 = tran(iva(4,1),'999.99')
  if not empty(iva(4,1)) .and. (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(1691-1557),54,at_x(439),transform(w_IVA4,""),i_fn
  endif
  w_AIMPS24 = TRAN(round(mvaimps4/mvcaoval,2),'@Z '+V_PV[20*(VADECTOT+2)])
  if mvcodval<>g_codeur AND NOT empty(iva(4,1))
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(1691-1557),64,at_x(512),transform(w_AIMPS24,""),i_fn
  endif
  w_IMP = alltrim(iva(4,2))
  if empty(iva(4,1))
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(1691-1557),70,at_x(560),transform(w_IMP,""),i_fn
  endif
  w_AIMPS4 = TRAN(mvaimps4,'@Z '+V_PV[20*(VADECTOT+2)])
  if NOT empty(iva(4,1))
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(1691-1557),89,at_x(712),transform(w_AIMPS4,""),i_fn
  endif
  w_ACCONT = TRAN(MVACCONT,'@Z '+V_PV[20*(VADECTOT+2)])
  if TDCATDOC<>'RF' .and. (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(1691-1557),110,at_x(886),transform(w_ACCONT,""),i_fn
  endif
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(1710-1557),1,at_x(8),transform(MVACIVA5,""),i_fn
  endif
  w_MVAIMPN5 = TRAN(mvaimpn5,'@Z '+V_PV[20*(VADECTOT+2)])
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(1710-1557),33,at_x(264),transform(w_MVAIMPN5,""),i_fn
  endif
  w_IVA5 = tran(iva(5,1),'999.99')
  if not empty(iva(5,1)) .and. (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(1710-1557),54,at_x(439),transform(w_IVA5,""),i_fn
  endif
  w_AIMPS25 = TRAN(round(mvaimps5/mvcaoval,2),'@Z '+V_PV[20*(VADECTOT+2)])
  if mvcodval<>g_codeur AND NOT empty(iva(5,1))
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(1710-1557),64,at_x(512),transform(w_AIMPS25,""),i_fn
  endif
  w_IMP = alltrim(iva(5,2))
  if empty(iva(5,1))
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(1710-1557),70,at_x(560),transform(w_IMP,""),i_fn
  endif
  w_AIMPS5 = TRAN(mvaimps5,'@Z '+V_PV[20*(VADECTOT+2)])
  if NOT empty(iva(5,1))
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(1710-1557),89,at_x(713),transform(w_AIMPS5,""),i_fn
  endif
  w_DOCRAT1 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(1,2)),'')
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(1729-1557),1,at_x(8),transform(w_DOCRAT1,""),i_fn
  endif
  w_DOCRAT2 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(2,2)),'')
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(1729-1557),18,at_x(146),transform(w_DOCRAT2,""),i_fn
  endif
  w_DOCRAT3 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(3,2)),'')
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(1729-1557),36,at_x(294),transform(w_DOCRAT3,""),i_fn
  endif
  w_DOCRAT4 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(4,2)),'')
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(1729-1557),54,at_x(439),transform(w_DOCRAT4,""),i_fn
  endif
  w_DOCRAT5 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(5,2)),'')
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(1729-1557),73,at_x(589),transform(w_DOCRAT5,""),i_fn
  endif
  w_DOCRAT6 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(6,2)),'')
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(1729-1557),92,at_x(736),transform(w_DOCRAT6,""),i_fn
  endif
  w_CORRIS = TRAN(corris,'@Z '+V_PV[20*(VADECTOT+2)])
  if TDCATDOC='RF' .and. (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(1729-1557),110,at_x(886),transform(w_CORRIS,""),i_fn
  endif
  w_DOCRA1 = IIF(TDCATDOC='RF',Fdocra1(),CP_TODATE(doc_rate(1,1)))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(1748-1557),1,at_x(8),transform(w_DOCRA1,""),i_fn
  endif
  w_DOCRA2 = IIF(TDCATDOC='RF',Fdocra2(),CP_TODATE(doc_rate(2,1)))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(1748-1557),18,at_x(146),transform(w_DOCRA2,""),i_fn
  endif
  w_DOCRA3 = IIF(TDCATDOC='RF',Fdocra3(),CP_TODATE(doc_rate(3,1)))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(1748-1557),36,at_x(294),transform(w_DOCRA3,""),i_fn
  endif
  w_DOCRA4 = IIF(TDCATDOC='RF',Fdocra4(),CP_TODATE(doc_rate(4,1)))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(1748-1557),54,at_x(439),transform(w_DOCRA4,""),i_fn
  endif
  w_DOCRA5 = IIF(TDCATDOC='RF',Fdocra5(),CP_TODATE(doc_rate(5,1)))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(1748-1557),73,at_x(589),transform(w_DOCRA5,""),i_fn
  endif
  w_DOCRA6 = IIF(TDCATDOC='RF',Fdocra6(),CP_TODATE(doc_rate(6,1)))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(1748-1557),92,at_x(736),transform(w_DOCRA6,""),i_fn
  endif
  w_DOC_RA1 = IIF(TDCATDOC='RF',Fdoc_ra1(),LTRIM(TRAN(doc_rate(1,3),'@Z '+V_PV[20*(VADECTOT+2)])))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(1767-1557),1,at_x(8),transform(w_DOC_RA1,""),i_fn
  endif
  w_DOC_RA2 = IIF(TDCATDOC='RF',Fdoc_ra2(),LTRIM(TRAN(doc_rate(2,3),'@Z '+V_PV[20*(VADECTOT+2)])))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(1767-1557),18,at_x(146),transform(w_DOC_RA2,""),i_fn
  endif
  w_DOC_RA3 = IIF(TDCATDOC='RF',Fdoc_ra3(),LTRIM(TRAN(doc_rate(3,3),'@Z '+V_PV[20*(VADECTOT+2)])))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(1767-1557),36,at_x(294),transform(w_DOC_RA3,""),i_fn
  endif
  w_DOC_RA4 = IIF(TDCATDOC='RF',Fdoc_ra4(),LTRIM(TRAN(doc_rate(4,3),'@Z '+V_PV[20*(VADECTOT+2)])))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(1767-1557),54,at_x(439),transform(w_DOC_RA4,""),i_fn
  endif
  w_DOC_RA5 = IIF(TDCATDOC='RF',Fdoc_ra5(),LTRIM(TRAN(doc_rate(5,3),'@Z '+V_PV[20*(VADECTOT+2)])))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(1767-1557),73,at_x(589),transform(w_DOC_RA5,""),i_fn
  endif
  w_DOC_RA6 = IIF(TDCATDOC='RF',Fdoc_ra6(),LTRIM(TRAN(doc_rate(6,3),'@Z '+V_PV[20*(VADECTOT+2)])))
  if (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(1767-1557),92,at_x(736),transform(w_DOC_RA6,""),i_fn
  endif
  w_NONCORR = TRAN(nvl(noncorris,''),'@Z '+V_PV[20*(VADECTOT+2)])
  if TDCATDOC='RF' .and. (NVL(TDCATDOC,'')<>'DT')
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(1767-1557),110,at_x(886),transform(w_NONCORR,""),i_fn
  endif
  w_DESSPE = NVL(translat(MVTIPCON,MVCODCON,'TRADSPED','lgdescri','LGCODSPE','lgcodlin','MODASPED', 'SPDESSPE','SPCODSPE',mvcodspe),'')
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(1805-1557),1,at_x(8),transform(w_DESSPE,Repl('X',20)),i_fn
  if NVL(TDFLACCO,' ')='S'
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(1805-1557),61,at_x(489),transform(MVASPEST,Repl('X',30)),i_fn
  endif
  w_CAUTRA = looktab('cam_agaz','cmdescri','cmcodice',mvtcamag)
  if (TDCATDOC='FA'  and TDFLACCO='S') or TDCATDOC='DT'
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(1805-1557),100,at_x(806),transform(w_CAUTRA,Repl('X',30)),i_fn
  endif
  w_MVQTACOL = TRAN(mvqtacol,'@KZ '+'99999')
  if NOT EMPTY(NVL(MVQTACOL,'')) and nvl(tdflpack,'')='S'
  i_fn = ""
  do F_Say with i_row+15,i_row+at_y(1843-1557),1,at_x(8),transform(w_MVQTACOL,""),i_fn
  endif
  w_QTALOR = TRAN(mvqtalor,'@Z '+v_PV(91))
  if NOT EMPTY(NVL(MVQTALOR,'')) and nvl(tdflpack,'')='S'
  i_fn = ""
  do F_Say with i_row+15,i_row+at_y(1843-1557),8,at_x(66),transform(w_QTALOR,""),i_fn
  endif
  w_DESPOR = NVL(translat(MVTIPCON,MVCODCON,'TRADPORT','lgdescri','LGCODPOR','lgcodlin','PORTI', 'PODESPOR','POCODPOR',mvcodpor),'')
  i_fn = ""
  do F_Say with i_row+15,i_row+at_y(1843-1557),24,at_x(195),transform(w_DESPOR,""),i_fn
  w_DATORA = dtoc(mvdattra)+'  '+iif(not empty(nvl(mvoratra,0)) and not empty(nvl(mvmintra,0)),mvoratra+':'+mvmintra,space(5))
  if empty(NVL(mvcodvet,'')) AND TDFLACCO='S' and not empty(nvl(dtoc(mvdattra),' '))
  i_fn = ""
  do F_Say with i_row+15,i_row+at_y(1843-1557),61,at_x(490),transform(w_DATORA,""),i_fn
  endif
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(1881-1557),1,at_x(8),transform(MVNOTAGG,""),i_fn
  w_DESVET = nvl(looktab('vettori','vtdesvet','vtcodvet',mvcodvet),'')
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(1919-1557),3,at_x(24),transform(w_DESVET,Repl('X',40)),i_fn
  w_VETDAT = w_VETDAT1+alltrim(w_PROVE1)
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(1919-1557),44,at_x(352),transform(w_VETDAT,Repl('X',40)),i_fn
  w_DATTRA = dtoc(mvdattra)+'  '+mvoratra+':'+mvmintra
  if NOT EMPTY(NVL(MVCODVET,'')) AND TDFLACCO='S' and not empty(nvl(dtoc(mvdattra),' '))
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(1919-1557),88,at_x(704),transform(w_DATTRA,""),i_fn
  endif
  w_DESVE2 = nvl(looktab('vettori','vtdesvet','vtcodvet',mvcodve2),'')
  i_fn = ""
  do F_Say with i_row+21,i_row+at_y(1957-1557),3,at_x(24),transform(w_DESVE2,Repl('X',40)),i_fn
  w_VETDA2 = w_VETDAT2+alltrim(w_PROVET2)
  i_fn = ""
  do F_Say with i_row+21,i_row+at_y(1957-1557),45,at_x(360),transform(w_VETDA2,Repl('X',40)),i_fn
  TOTALE = 0
  IF .F.
  i_fn = ""
  do F_Say with i_row+21,i_row+at_y(1957-1557),89,at_x(712),transform(TOTALE,""),i_fn
   ENDIF
  CONAI = 0
  IF .F.
  i_fn = ""
  do F_Say with i_row+21,i_row+at_y(1957-1557),92,at_x(736),transform(CONAI,""),i_fn
   ENDIF
return

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

* --- Area Manuale = Functions & Procedures 
* --- TX_MODPL
FUNCTION CTRL_A
Private w_RET
w_RET=.f.

if empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ');
   and mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ') and ;
   mvcodcla<>nvl(tdstacl5,' '))
          w_RET=.T.
endif

Return (w_RET)
endfunc

FUNCTION CTRL_D
Private w_RET
w_RET=.f.

if !empty(mvdesart) and ((empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ') and ;
mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ') ;
and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' '))))
   w_RET=.T.
endif
Return (w_RET)
endfunc

FUNCTION CTRL1
Private w_RET
w_RET=.f.

if ((mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and empty(mvcodcla) or ;
   (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ') and ; 
   mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ') and ;
   mvcodcla<>nvl(tdstacl5,' '))) 
       w_RET=.t.
endif
Return (w_RET)
endfunc

FUNCTION CTRL2
Private w_RET
w_RET=.f.

if (mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont3) or not empty(mvscont4);
   and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
   and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
   and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' ')))
       w_RET=.t.
endif
Return (w_RET)
endfunc


FUNCTION CTRL4
Private w_RET
w_RET=.f.

if (mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont2);
   and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
   and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
   and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' ')))
       w_RET=.t.
endif
Return (w_RET)
endfunc

FUNCTION CTRL3
Private w_RET
w_RET=.f.

if ((mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont1);
    and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
    and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
    and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' '))))
       w_RET=.t.
endif
Return (w_RET)
endfunc

Function FValrig
private w_VALRIG 
w_VALRIG=iif(mvrifkit<>0,'',iif(mvflomag='X',TRAN(mvvalrig,'@Z '+V_PV[18*(VADECTOT+2)]),;
                  iif(mvflomag='S','Sconto Merce',iif(mvflomag='I','Omaggio Impon.',;
                  iif(mvflomag='E','Omaggio Imp.+IVA','')))))
Return(w_VALRIG)
endfunc

Function FIva
private w_IVA
w_IVA=iif(mvrifkit<>0,'',iif(empty(nvl(mvcodive,' ')),mvcodiva,mvcodive))
Return (w_IVA)
endfunc

Function Fscrdessup
Private w_RET
w_RET=.f.
if 'S'=nvl(looktab('ART_ICOL','ARSTASUP','ARCODART',MVCODART),'')
    w_RET=.t.
endif
Return (w_RET)
endfunc

Function Fdatitest
private w_DATITEST

if (tdcatdoc='FA'  and (nvl(CCTIPDOC,'')<>'FC')) 
               w_DATITEST=alltrim(an___cap)+iif(empty(anlocali),'',' '+left(alltrim(anlocali ),20))+;
                         iif(empty(alltrim(anprovin)),'',+' ( ' +anprovin+' ) ')
else
      if !empty(nvl(ddnomdes,''))  and nvl(cctipdoc,'')='FC'
               w_DATITEST= alltrim(dd___cap)+iif(empty(ddlocali),'',' '+left(alltrim(ddlocali ),20))+;
                                       iif(empty(alltrim(ddprovin)),'',+' ( ' +ddprovin+' ) ')
      else
             w_DATITEST=alltrim(an___cap)+iif(empty(anlocali),'',' '+left(alltrim(anlocali ),20))+;
                         iif(empty(alltrim(anprovin)),'',+' ( ' +anprovin+' ) ')  
      endif
endif
Return (w_DATITEST)
endfunc

Function Fdatitest2
private w_DATITEST2

if ((tdcatdoc='FA'  and tdflacco='S') or (tdcatdoc='DT'))
          w_DATITEST2=alltrim(cap)+iif(empty(locali),'',' '+left(alltrim(locali ),20))+;
          iif(empty(alltrim(provin)),'',+;
          ' ( ' +provin+' ) ')
else
          w_DATITEST2=alltrim(dd___cap)+iif(empty(ddlocali),'',' '+left(alltrim(ddlocali ),20))+;
          iif(empty(alltrim(ddprovin)),'',+;
          ' ( ' +ddprovin+' ) ') 
endif
Return (w_DATITEST2)
endfunc

Function FTxt1
Private w_TXT1
w_TXT1='Dichiarazione di intento n� ' + alltrim(str(L_NDIC));
                + ' del ' +dtoc(L_DDIC)+' Art. Es. '+alltrim(L_ARTESE);
                +' del D.P.R. 633/72'+iif(L_TIPOPER='D','  Valida dal '+DTOC(L_DATINI);
                + ' al '+DTOC(L_DATFIN),IIF(L_TIPOPER='I',' Limite es: '+alltrim(IMP),''))
Return (w_TXT1)
Endfunc

Function FImposta
Private imposta
imposta=iif(mvaflom1$'XI',mvaimps1,0)+iif(mvaflom2$'XI',mvaimps2,0);
               +iif(mvaflom3$'XI',mvaimps3,0)+iif(mvaflom4$'XI',mvaimps4,0);
                +iif(mvaflom5$'XI',mvaimps5,0)+iif(mvaflom6$'XI',mvaimps6,0)
Return(imposta)
Endfunc

FUNCTIO FVETDAT1
private w_VETDAT1
w_VETDAT1=alltrim(NVL(looktab('vettori','vtindvet','vtcodvet',mvcodvet),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtvetcap','vtcodvet',mvcodvet),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtlocvet','vtcodvet',mvcodvet),''))+'   '
RETURN (w_VETDAT1)
ENDFUNC

FUNCTION FVETDAT2
private w_VETDAT2
w_VETDAT2=alltrim(NVL(looktab('vettori','vtindvet','vtcodvet',mvcodve2),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtvetcap','vtcodvet',mvcodve2),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtlocvet','vtcodvet',mvcodve3),''))+'   '
RETURN(w_VETDAT2)
ENDFUNC

FUNCTION FIMPIVA
PRIVATE w_IMPIVA
w_IMPIVA=iif(mvaflom1='X',mvaimpn1,0)+iif(mvaflom2='X',mvaimpn2,0);
                 +iif(mvaflom3='X',mvaimpn3,0)+iif(mvaflom4='X',mvaimpn4,0);
                 +iif(mvaflom5='X',mvaimpn5,0)+iif(mvaflom6='X',mvaimpn6,0)
RETURN(w_IMPIVA)
ENDFUNC

FUNCTION FCODART
Private w_RET
w_RET=.f.

if (not empty(mvqtamov) or not empty(mvprezzo) or empty(mvcodcla)) or (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ');
   and mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ');
   and mvcodcla<>nvl(tdstacl5,' '))
       w_RET=.t.
endif
Return (w_RET)
endfunc
* ----------------------------------------------------------------------
* Funzione : Pari_A
* ----------------------------------------------------------------------
*  Esegue traduzione importo documento in EURO o LIRE
* ----------------------------------------------------------------------
Function Pari_a
parameter Valuta
private w_ImpRet,w_TotPar
do case
   case MVCODVAL=g_CODEUR
        w_TotPar =  ROUND(( TOTALEA*p_Cambio),0)
        w_ImpRet = TRAN(w_TotPar, V_PV[(40)])
   case MVCODVAL=g_CODLIR
         w_TotPar =  ROUND((TOTALEA/p_Cambio),2)
         w_ImpRet = TRAN(w_TotPar, V_PV[(80)])
   otherwise
   if mvdatdoc>=g_dateur and vacodval<>g_codeur
        w_TotPar = ROUND((TOTALEA/mvcaoval),2)
        w_ImpRet = TRAN(w_TotPar, V_PV[(80)])
   else
        w_TotPar = ROUND((TOTALEA*p_cambio),0)
        w_ImpRet = TRAN(w_TotPar, V_PV[(80)])
   endif
endcase

return (w_ImpRet)

Function Fdocra1
private docra1
docra1=TRAN(nvl(looktab('doc_rate','rsdatrat','rsserial',mvserial),''),;
              '@Z '+V_PV[20*(VADECTOT+2)])
return(IIF(docra1='  -  -    ',space(1),docra1))
endfunc

Function Fdoc_ra1
private doc_ra1
doc_ra1=TRAN(nvl(looktab('doc_rate','rsimprat','rsserial',mvserial),''),;
              '@Z '+V_PV[20*(VADECTOT+2)])
return(LTRIM(doc_ra1))
endfunc

Function Fdocra2
private docra2
docra2=TRAN(nvl(looktab('doc_rate','rsdatrat','rsserial',mvserial,'rsnumrat',2),''),;
             '@Z '+V_PV[20*(VADECTOT+2)])
return(IIF(docra2='  -  -    ',space(1),docra2))
endfunc

Function Fdoc_ra2
private doc_ra2
doc_ra2=TRAN(nvl(looktab('doc_rate','rsimprat','rsserial',mvserial,'rsnumrat',2),''),;
               '@Z '+V_PV[20*(VADECTOT+2)])
return(LTRIM(doc_ra2))
endfunc

Function Fdocra3
private docra3
docra3=TRAN(nvl(looktab('doc_rate','rsdatrat','rsserial',mvserial,'rsnumrat',3),''),;
             '@Z '+V_PV[20*(VADECTOT+2)])
return(IIF(docra3='  -  -    ',space(1),docra3))
endfunc

Function Fdoc_ra3
private doc_ra3
doc_ra3=TRAN(nvl(looktab('doc_rate','rsimprat','rsserial',mvserial,'rsnumrat',3),''),;
               '@Z '+V_PV[20*(VADECTOT+2)])
return(LTRIM(doc_ra3))
endfunc

Function Fdocra4
private docra4
docra4=TRAN(nvl(looktab('doc_rate','rsdatrat','rsserial',mvserial,'rsnumrat',4),''),;
             '@Z '+V_PV[20*(VADECTOT+2)])
return(IIF(docra4='  -  -    ',space(1),docra4))
endfunc

Function Fdoc_ra4
private doc_ra4
doc_ra4=TRAN(nvl(looktab('doc_rate','rsimprat','rsserial',mvserial,'rsnumrat',4),''),;
               '@Z '+V_PV[20*(VADECTOT+2)])
return(LTRIM(doc_ra4))
endfunc

Function Fdocra5
private docra5
docra5=TRAN(nvl(looktab('doc_rate','rsdatrat','rsserial',mvserial,'rsnumrat',5),''),;
             '@Z '+V_PV[20*(VADECTOT+2)])
return(IIF(docra5='  -  -    ',space(1),docra5))
endfunc

Function Fdoc_ra5
private doc_ra5
doc_ra5=TRAN(nvl(looktab('doc_rate','rsimprat','rsserial',mvserial,'rsnumrat',5),''),;
               '@Z '+V_PV[20*(VADECTOT+2)])
return(LTRIM(doc_ra5))
endfunc

Function Fdocra6
private docra6
docra6=TRAN(nvl(looktab('doc_rate','rsdatrat','rsserial',mvserial,'rsnumrat',6),''),;
             '@Z '+V_PV[20*(VADECTOT+2)])
return(IIF(docra6='  -  -    ',space(1),docra6))
endfunc

Function Fdoc_ra6
private doc_ra6
doc_ra6=TRAN(nvl(looktab('doc_rate','rsimprat','rsserial',mvserial,'rsnumrat',6),''),;
               '@Z '+V_PV[20*(VADECTOT+2)])
return(LTRIM(doc_ra6))
endfunc

Function Fnaz1
private w_NAZION

if (tdcatdoc='FA'  and (nvl(CCTIPDOC,'')<>'FC')) 
               w_NAZION=LOOKTAB('NAZIONI','NADESNAZ','NACODNAZ',ALLTRIM(ANNAZION))
else
      if !empty(nvl(ddnomdes,''))  
               w_NAZION=LOOKTAB('NAZIONI','NADESNAZ','NACODNAZ',ALLTRIM(ddcodnaz))
      else
             w_NAZION=LOOKTAB('NAZIONI','NADESNAZ','NACODNAZ',ALLTRIM(ANNAZION))  
      endif
endif
Return (w_NAZION)
endfunc


Function Fnaz2
private w_NAZION2

if ((tdcatdoc='FA'  and tdflacco='S') or (tdcatdoc='DT'))
          w_NAZION2=LOOKTAB('NAZIONI','NADESNAZ','NACODNAZ',ALLTRIM(CODNAZ))  
else
          w_NAZION2=LOOKTAB('NAZIONI','NADESNAZ','NACODNAZ',ALLTRIM(DDCODNAZ))   
endif
Return (w_NAZION2)
endfunc

Func Caldespag()
    Private w_Ret
    w_Ret = mvcodpag+' '+NVL(translat(mvtipcon,mvcodcon,'tradpaga','lgdescod','lgcodice','lgcodlin','pag_amen','padescri','pacodice',mvcodpag),'')
    Return (w_Ret)
Endfunc

FUNCTION CTRL_COAC
Private w_RET
w_RET=.t.
* INDICA SE STAMPARE LA RIGA DI CONTRIBUTO ACCESSORIO
if G_COAC = "S"
  if VARTYPE(MVCACONT) = "C"
    if NVL( LOOKTAB( "TIP_DOCU" , "TDNOSTCO"  , "TDTIPDOC" , TDTIPDOC ) , "N" ) = "S" and !EMPTY(MVCACONT)
      * se il flag sulla causale � attivo e siamo su una riga di contributo accessorio,
      * la riga non viene stampata
      w_RET=.f.
    else
      w_RET=.t.
    endif
  else
    w_RET=.t.
  endif
else
  w_RET=.t.
endif

Return (w_RET)
endfunc

* --- Fine Area Manuale 
