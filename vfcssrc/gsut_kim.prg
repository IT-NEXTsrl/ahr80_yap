* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kim                                                        *
*              Invio E-mail                                                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_100]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-31                                                      *
* Last revis.: 2016-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kim",oParentObject))

* --- Class definition
define class tgsut_kim as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 709
  Height = 461
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-02-19"
  HelpContextID=1490071
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kim"
  cComment = "Invio E-mail"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SMTPFROM = space(250)
  w_El_Allegati = space(254)
  w_El_To = space(254)
  w_El_Cc = space(254)
  w_El_Ccn = space(254)
  w_IsFax = .F.
  w_oggetto = space(254)
  w_TestoMail = space(0)
  w_UseFaxCover = .F.
  w_SMTP__TO = space(0)
  w_SMTP__CC = space(0)
  w_SMTP_CCN = space(0)
  w_SMTPSUBJ = space(250)
  w_SMTPBODY = space(0)
  w_SMTPPRIO = 0
  w_SMTP_ATT = space(0)
  o_SMTP_ATT = space(0)
  w_SMTPSERV = space(250)
  w_SMTPPORT = 0
  w_COPERTINA = 0
  o_COPERTINA = 0
  w_SMTPCOMM = space(250)
  w_UseOutlook = .F.
  w_AR_TABLE = space(30)
  w_ARTABKEY = space(50)
  w_AR__PATH = space(254)
  w_ARFILNAM = space(254)
  w_AROGGETT = space(220)
  w_AR__NOTE = space(0)
  w_ARTIPALL = space(5)
  w_ARCLAALL = space(5)
  w_ARDESKEY = space(50)
  w_AROPERAZ = space(1)
  w_VisMail = space(1)
  w_PEC = space(1)
  w_Sent = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kimPag1","gsut_kim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSMTP__TO_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsut_kim
    * --- Se Tipo Invio Automatico sposto la maschera fuori dallo schermo
    if VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S'
       this.parent.left=_screen.width+1
    Endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSUT_BIM(this,"INVIO")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SMTPFROM=space(250)
      .w_El_Allegati=space(254)
      .w_El_To=space(254)
      .w_El_Cc=space(254)
      .w_El_Ccn=space(254)
      .w_IsFax=.f.
      .w_oggetto=space(254)
      .w_TestoMail=space(0)
      .w_UseFaxCover=.f.
      .w_SMTP__TO=space(0)
      .w_SMTP__CC=space(0)
      .w_SMTP_CCN=space(0)
      .w_SMTPSUBJ=space(250)
      .w_SMTPBODY=space(0)
      .w_SMTPPRIO=0
      .w_SMTP_ATT=space(0)
      .w_SMTPSERV=space(250)
      .w_SMTPPORT=0
      .w_COPERTINA=0
      .w_SMTPCOMM=space(250)
      .w_UseOutlook=.f.
      .w_AR_TABLE=space(30)
      .w_ARTABKEY=space(50)
      .w_AR__PATH=space(254)
      .w_ARFILNAM=space(254)
      .w_AROGGETT=space(220)
      .w_AR__NOTE=space(0)
      .w_ARTIPALL=space(5)
      .w_ARCLAALL=space(5)
      .w_ARDESKEY=space(50)
      .w_AROPERAZ=space(1)
      .w_VisMail=space(1)
      .w_PEC=space(1)
      .w_Sent=.f.
      .w_Sent=oParentObject.w_Sent
        .w_SMTPFROM = g_MITTEN
        .w_El_Allegati = this.oParentObject.w_El_Allegati
        .w_El_To = this.oParentObject.w_El_To
        .w_El_Cc = this.oParentObject.w_El_Cc
        .w_El_Ccn = this.oParentObject.w_El_Ccn
        .w_IsFax = this.oParentObject.w_IsFax
        .w_oggetto = this.oParentObject.w_oggetto
        .w_TestoMail = this.oParentObject.w_TestoMail
        .w_UseFaxCover = this.oParentObject.w_UseFaxCover
        .w_SMTP__TO = .w_El_To
        .w_SMTP__CC = .w_El_Cc
        .w_SMTP_CCN = IIF(i_bPEC, "", .w_El_Ccn)
        .w_SMTPSUBJ = .w_Oggetto
        .w_SMTPBODY = IIF(!.w_IsFax, .w_TestoMail, '')
        .w_SMTPPRIO = g_EmailPrior
        .w_SMTP_ATT = .w_El_Allegati
        .w_SMTPSERV = IIF(!.w_IsFax, g_SRVMAIL,'fax.zuum.it')
        .w_SMTPPORT = g_SRVPORTA
        .w_COPERTINA = IIF(!.w_IsFax,0,IIF(.w_UseFaxCover,1,0))
        .w_SMTPCOMM = ''
        .w_UseOutlook = this.oParentObject.w_UseOutlook
        .w_AR_TABLE = this.oParentObject.w_AR_TABLE
        .w_ARTABKEY = this.oParentObject.w_ARTABKEY
        .w_AR__PATH = this.oParentObject.w_AR__PATH
        .w_ARFILNAM = this.oParentObject.w_ARFILNAM
        .w_AROGGETT = this.oParentObject.w_AROGGETT
        .w_AR__NOTE = this.oParentObject.w_AR__NOTE
        .w_ARTIPALL = this.oParentObject.w_ARTIPALL
        .w_ARCLAALL = this.oParentObject.w_ARCLAALL
        .w_ARDESKEY = this.oParentObject.w_ARDESKEY
        .w_AROPERAZ = this.oParentObject.w_AROPERAZ
        .w_VisMail = IIF(TYPE('this.oParentObject.w_ShowDialog')='L' and Type('this.oparentobject.oparentobject')='O' and upper(this.oparentobject.oparentobject.class)='TGSAL_BZI', IIF(this.oParentObject.w_ShowDialog, 'S', 'A'), g_UEDIAL)
        .w_PEC = iif(i_bPEC,'S','N')
        .w_Sent = True
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_Sent=.w_Sent
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_SMTP_ATT<>.w_SMTP_ATT
          .Calculate_MNZRWALFXY()
        endif
        .DoRTCalc(1,19,.t.)
        if .o_COPERTINA<>.w_COPERTINA
            .w_SMTPCOMM = ''
        endif
        .DoRTCalc(21,33,.t.)
            .w_Sent = True
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_JOYXXNPELM()
    with this
          * --- Cambia la Caption
          .cComment = ICASE(.w_IsFax, AH_MSGFORMAT("Invio Fax"),.w_UseOutlook, AH_MSGFORMAT("Invio e-mail outlook%1", iif(i_bPEC," PEC","")) , AH_MSGFORMAT("Invio e-mail") + iif(i_bPEC," PEC","") + iif(g_INVIO$"SP", AH_MSGFORMAT("  [Account: %1]",g_MITTEN),""))
          .Caption = .cComment
    endwith
  endproc
  proc Calculate_MNZRWALFXY()
    with this
          * --- Sbianco il campo Smtpbody
          .w_SMTPBODY = IIF(.w_IsFax And Not Empty(.w_SMTP_ATT),'',.w_SMTPBODY)
    endwith
  endproc
  proc Calculate_HFUJACQCKT()
    with this
          * --- Chiusura maschera
          GSUT_BIM(this;
              ,'DONE';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSMTP_CCN_1_12.enabled = this.oPgFrm.Page1.oPag.oSMTP_CCN_1_12.mCond()
    this.oPgFrm.Page1.oPag.oSMTPBODY_1_15.enabled = this.oPgFrm.Page1.oPag.oSMTPBODY_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSMTP__CC_1_11.visible=!this.oPgFrm.Page1.oPag.oSMTP__CC_1_11.mHide()
    this.oPgFrm.Page1.oPag.oSMTP_CCN_1_12.visible=!this.oPgFrm.Page1.oPag.oSMTP_CCN_1_12.mHide()
    this.oPgFrm.Page1.oPag.oSMTPPRIO_1_16.visible=!this.oPgFrm.Page1.oPag.oSMTPPRIO_1_16.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_23.visible=!this.oPgFrm.Page1.oPag.oBtn_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oCOPERTINA_1_30.visible=!this.oPgFrm.Page1.oPag.oCOPERTINA_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oSMTPCOMM_1_33.visible=!this.oPgFrm.Page1.oPag.oSMTPCOMM_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_JOYXXNPELM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_HFUJACQCKT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_kim
    if cEvent='Init'
      if !This.oParentObject.w_ShowDialog and !EMPTY(This.w_SMTP__TO)
        This.bUpdated=.T.
        This.ecpSave()
        This.WindowType=0
        This.EcpQuit()
      endif
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSMTP__TO_1_10.value==this.w_SMTP__TO)
      this.oPgFrm.Page1.oPag.oSMTP__TO_1_10.value=this.w_SMTP__TO
    endif
    if not(this.oPgFrm.Page1.oPag.oSMTP__CC_1_11.value==this.w_SMTP__CC)
      this.oPgFrm.Page1.oPag.oSMTP__CC_1_11.value=this.w_SMTP__CC
    endif
    if not(this.oPgFrm.Page1.oPag.oSMTP_CCN_1_12.value==this.w_SMTP_CCN)
      this.oPgFrm.Page1.oPag.oSMTP_CCN_1_12.value=this.w_SMTP_CCN
    endif
    if not(this.oPgFrm.Page1.oPag.oSMTPSUBJ_1_13.value==this.w_SMTPSUBJ)
      this.oPgFrm.Page1.oPag.oSMTPSUBJ_1_13.value=this.w_SMTPSUBJ
    endif
    if not(this.oPgFrm.Page1.oPag.oSMTPBODY_1_15.value==this.w_SMTPBODY)
      this.oPgFrm.Page1.oPag.oSMTPBODY_1_15.value=this.w_SMTPBODY
    endif
    if not(this.oPgFrm.Page1.oPag.oSMTPPRIO_1_16.RadioValue()==this.w_SMTPPRIO)
      this.oPgFrm.Page1.oPag.oSMTPPRIO_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSMTP_ATT_1_17.value==this.w_SMTP_ATT)
      this.oPgFrm.Page1.oPag.oSMTP_ATT_1_17.value=this.w_SMTP_ATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPERTINA_1_30.RadioValue()==this.w_COPERTINA)
      this.oPgFrm.Page1.oPag.oCOPERTINA_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSMTPCOMM_1_33.value==this.w_SMTPCOMM)
      this.oPgFrm.Page1.oPag.oSMTPCOMM_1_33.value=this.w_SMTPCOMM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsut_kim
      if EMPTY(.w_SMTP__TO)
         i_bRes=.F.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SMTP_ATT = this.w_SMTP_ATT
    this.o_COPERTINA = this.w_COPERTINA
    return

enddefine

* --- Define pages as container
define class tgsut_kimPag1 as StdContainer
  Width  = 705
  height = 461
  stdWidth  = 705
  stdheight = 461
  resizeXpos=347
  resizeYpos=322
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSMTP__TO_1_10 as StdMemo with uid="AZCXSPOHTN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SMTP__TO", cQueryName = "SMTP__TO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Inserire gli indirizzi E-mail separati da punto e virgola",;
    HelpContextID = 178499723,;
   bGlobalFont=.t.,;
    Height=24, Width=585, Left=95, Top=6, bdisablefrasimodello=.T.

  add object oSMTP__CC_1_11 as StdMemo with uid="FLMQVRKHQO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SMTP__CC", cQueryName = "SMTP__CC",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Inserire gli indirizzi E-mail separati da punto e virgola",;
    HelpContextID = 178499735,;
   bGlobalFont=.t.,;
    Height=24, Width=585, Left=95, Top=34, bdisablefrasimodello=.T.

  func oSMTP__CC_1_11.mHide()
    with this.Parent.oContained
      return (.w_IsFax)
    endwith
  endfunc

  add object oSMTP_CCN_1_12 as StdMemo with uid="IGKOJAKQGT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SMTP_CCN", cQueryName = "SMTP_CCN",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Inserire gli indirizzi E-mail separati da punto e virgola",;
    HelpContextID = 111390860,;
   bGlobalFont=.t.,;
    Height=24, Width=585, Left=95, Top=62, bdisablefrasimodello=.T.

  func oSMTP_CCN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not i_bPEC)
    endwith
   endif
  endfunc

  func oSMTP_CCN_1_12.mHide()
    with this.Parent.oContained
      return (.w_IsFax)
    endwith
  endfunc

  add object oSMTPSUBJ_1_13 as StdField with uid="VZGTVBBISU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SMTPSUBJ", cQueryName = "SMTPSUBJ",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto dell'e-mail",;
    HelpContextID = 90419344,;
   bGlobalFont=.t.,;
    Height=24, Width=585, Left=95, Top=90, InputMask=replicate('X',250)

  add object oSMTPBODY_1_15 as StdMemo with uid="IAMCSNGQGE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SMTPBODY", cQueryName = "SMTPBODY",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo dell'E-mail",;
    HelpContextID = 208908417,;
   bGlobalFont=.t.,;
    Height=255, Width=658, Left=22, Top=118

  func oSMTPBODY_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_IsFax or Empty(.w_SMTP_ATT))
    endwith
   endif
  endfunc

  add object oSMTPPRIO_1_16 as StdRadio with uid="KKJCZRNXAV",rtseq=15,rtrep=.f.,left=88, top=378, width=186,height=50;
    , ToolTipText = "Consente di selezionare la priorit� da assegnare all'e-mail";
    , cFormVar="w_SMTPPRIO", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSMTPPRIO_1_16.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Bassa"
      this.Buttons(1).HelpContextID = 124538741
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Normale"
      this.Buttons(2).HelpContextID = 124538741
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Alta"
      this.Buttons(3).HelpContextID = 124538741
      this.Buttons(3).Top=32
      this.SetAll("Width",184)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Consente di selezionare la priorit� da assegnare all'e-mail")
      StdRadio::init()
    endproc

  func oSMTPPRIO_1_16.RadioValue()
    return(iif(this.value =1,5,;
    iif(this.value =2,3,;
    iif(this.value =3,1,;
    0))))
  endfunc
  func oSMTPPRIO_1_16.GetRadio()
    this.Parent.oContained.w_SMTPPRIO = this.RadioValue()
    return .t.
  endfunc

  func oSMTPPRIO_1_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SMTPPRIO==5,1,;
      iif(this.Parent.oContained.w_SMTPPRIO==3,2,;
      iif(this.Parent.oContained.w_SMTPPRIO==1,3,;
      0)))
  endfunc

  func oSMTPPRIO_1_16.mHide()
    with this.Parent.oContained
      return (.w_IsFax)
    endwith
  endfunc

  add object oSMTP_ATT_1_17 as StdMemo with uid="PGNZSSPBID",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SMTP_ATT", cQueryName = "SMTP_ATT",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Inserire in questa casella l'elenco degli allegati separati da punto e virgola",;
    HelpContextID = 144945286,;
   bGlobalFont=.t.,;
    Height=24, Width=436, Left=84, Top=430, bdisablefrasimodello=.T.


  add object oBtn_1_21 as StdButton with uid="XFZTSGQDIZ",left=43, top=7, width=50,height=23,;
    caption="A...", nPag=1;
    , ToolTipText = "Premere per selezionare gli indirizzi e-mail";
    , HelpContextID = 4706054;
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      do GSUT_KZM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="TLJNPGAQLX",left=43, top=35, width=50,height=23,;
    caption="CC...", nPag=1;
    , ToolTipText = "Premere per selezionare gli indirizzi e-mail";
    , HelpContextID = 52945958;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      do GSUT_KZM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_IsFax)
     endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="FYRMYUNWVV",left=43, top=63, width=50,height=23,;
    caption="Ccn...", nPag=1;
    , ToolTipText = "Premere per selezionare gli indirizzi e-mail";
    , HelpContextID = 19661862;
  , bGlobalFont=.t.

    proc oBtn_1_23.Click()
      do GSUT_KZM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not i_bPEC)
      endwith
    endif
  endfunc

  func oBtn_1_23.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_IsFax)
     endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="BYWCYEOHKL",left=525, top=435, width=19,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare uno o pi� files come allegati";
    , HelpContextID = 1691094;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSUT_BIM(this.Parent.oContained,"ALLEG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="WEVZTLAOIU",left=549, top=410, width=48,height=45,;
    CpPicture="bmp\AGGIUNGI.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per gestire gli allegati";
    , HelpContextID = 65372367;
    , caption='\<Gest. All.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      do GSUT_KZA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_27 as StdButton with uid="JBWBMEYONP",left=599, top=410, width=48,height=45,;
    CpPicture="bmp\btsend.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per inviare l'E-mail";
    , HelpContextID = 110595974;
    , caption='\<Invia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SMTP__TO))
      endwith
    endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="NQTZJUOKYS",left=650, top=410, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 8807494;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOPERTINA_1_30 as StdCheck with uid="IGNOQYZHWF",rtseq=19,rtrep=.f.,left=88, top=377, caption="Utilizza frontespizio per FAX",;
    HelpContextID = 159454340,;
    cFormVar="w_COPERTINA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOPERTINA_1_30.RadioValue()
    return(iif(this.value =1,1,;
    0))
  endfunc
  func oCOPERTINA_1_30.GetRadio()
    this.Parent.oContained.w_COPERTINA = this.RadioValue()
    return .t.
  endfunc

  func oCOPERTINA_1_30.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_COPERTINA==1,1,;
      0)
  endfunc

  func oCOPERTINA_1_30.mHide()
    with this.Parent.oContained
      return (!.w_IsFax)
    endwith
  endfunc

  add object oSMTPCOMM_1_33 as StdField with uid="VGMSGARWAV",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SMTPCOMM", cQueryName = "SMTPCOMM",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    HelpContextID = 60575603,;
   bGlobalFont=.t.,;
    Height=24, Width=585, Left=95, Top=62, InputMask=replicate('X',250)

  func oSMTPCOMM_1_33.mHide()
    with this.Parent.oContained
      return (!.w_IsFax OR .w_COPERTINA=0)
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="MEKITZULTG",Visible=.t., Left=29, Top=94,;
    Alignment=1, Width=64, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="CQAETPUPYU",Visible=.t., Left=9, Top=434,;
    Alignment=1, Width=73, Height=18,;
    Caption="Allegati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="HWQJNRHLTD",Visible=.t., Left=28, Top=382,;
    Alignment=1, Width=54, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (.w_IsFax)
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="DKYQCQCXDG",Visible=.t., Left=13, Top=65,;
    Alignment=1, Width=80, Height=18,;
    Caption="Commento:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (!.w_IsFax OR .w_COPERTINA=0)
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="QBBJDJNQFY",Visible=.t., Left=717, Top=276,;
    Alignment=0, Width=195, Height=19,;
    Caption="Informazioni per archiviazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kim','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
