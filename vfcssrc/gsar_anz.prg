* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_anz                                                        *
*              Nazioni                                                         *
*                                                                              *
*      Author: TAM SOFTWARE & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-04-23                                                      *
* Last revis.: 2011-08-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_anz"))

* --- Class definition
define class tgsar_anz as StdForm
  Top    = 45
  Left   = 28

  * --- Standard Properties
  Width  = 482
  Height = 156+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-08-25"
  HelpContextID=74877079
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  NAZIONI_IDX = 0
  VALUTE_IDX = 0
  cFile = "NAZIONI"
  cKeySelect = "NACODNAZ"
  cKeyWhere  = "NACODNAZ=this.w_NACODNAZ"
  cKeyWhereODBC = '"NACODNAZ="+cp_ToStrODBC(this.w_NACODNAZ)';

  cKeyWhereODBCqualified = '"NAZIONI.NACODNAZ="+cp_ToStrODBC(this.w_NACODNAZ)';

  cPrg = "gsar_anz"
  cComment = "Nazioni"
  icon = "anag.ico"
  cAutoZoom = 'GSVE0ANA'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_NACODNAZ = space(3)
  o_NACODNAZ = space(3)
  w_NADESNAZ = space(35)
  w_NACODISO = space(3)
  w_NAPAGISO = space(3)
  w_NACODVAL = space(3)
  w_DESVAL = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_NACODUIC = space(3)
  w_NACODEST = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'NAZIONI','gsar_anz')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_anzPag1","gsar_anz",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Nazione")
      .Pages(1).HelpContextID = 103313706
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNACODNAZ_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='NAZIONI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.NAZIONI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.NAZIONI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_NACODNAZ = NVL(NACODNAZ,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from NAZIONI where NACODNAZ=KeySet.NACODNAZ
    *
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('NAZIONI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "NAZIONI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' NAZIONI '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'NACODNAZ',this.w_NACODNAZ  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESVAL = space(35)
        .w_OBTEST = i_DATSYS
        .w_DATOBSO = ctod("  /  /  ")
        .w_NACODNAZ = NVL(NACODNAZ,space(3))
        .w_NADESNAZ = NVL(NADESNAZ,space(35))
        .w_NACODISO = NVL(NACODISO,space(3))
        .w_NAPAGISO = NVL(NAPAGISO,space(3))
        .w_NACODVAL = NVL(NACODVAL,space(3))
          if link_1_7_joined
            this.w_NACODVAL = NVL(VACODVAL107,NVL(this.w_NACODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL107,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(VADTOBSO107),ctod("  /  /  "))
          else
          .link_1_7('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .w_NACODUIC = NVL(NACODUIC,space(3))
        .w_NACODEST = NVL(NACODEST,space(5))
        cp_LoadRecExtFlds(this,'NAZIONI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NACODNAZ = space(3)
      .w_NADESNAZ = space(35)
      .w_NACODISO = space(3)
      .w_NAPAGISO = space(3)
      .w_NACODVAL = space(3)
      .w_DESVAL = space(35)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_NACODUIC = space(3)
      .w_NACODEST = space(5)
      if .cFunction<>"Filter"
        .DoRTCalc(1,5,.f.)
          if not(empty(.w_NACODVAL))
          .link_1_7('Full')
          endif
          .DoRTCalc(6,6,.f.)
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
          .DoRTCalc(8,8,.f.)
        .w_NACODUIC = .w_NACODNAZ
      endif
    endwith
    cp_BlankRecExtFlds(this,'NAZIONI')
    this.DoRTCalc(10,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oNACODNAZ_1_1.enabled = i_bVal
      .Page1.oPag.oNADESNAZ_1_2.enabled = i_bVal
      .Page1.oPag.oNACODISO_1_3.enabled = i_bVal
      .Page1.oPag.oNAPAGISO_1_6.enabled = i_bVal
      .Page1.oPag.oNACODVAL_1_7.enabled = i_bVal
      .Page1.oPag.oNACODUIC_1_13.enabled = i_bVal
      .Page1.oPag.oNACODEST_1_15.enabled = i_bVal
      .Page1.oPag.oObj_1_12.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oNACODNAZ_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oNACODNAZ_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'NAZIONI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NACODNAZ,"NACODNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NADESNAZ,"NADESNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NACODISO,"NACODISO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NAPAGISO,"NAPAGISO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NACODVAL,"NACODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NACODUIC,"NACODUIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NACODEST,"NACODEST",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    i_lTable = "NAZIONI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.NAZIONI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSVE_QN1.VQR,QUERY\GSVE_SAA.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.NAZIONI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.NAZIONI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into NAZIONI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'NAZIONI')
        i_extval=cp_InsertValODBCExtFlds(this,'NAZIONI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(NACODNAZ,NADESNAZ,NACODISO,NAPAGISO,NACODVAL"+;
                  ",NACODUIC,NACODEST "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_NACODNAZ)+;
                  ","+cp_ToStrODBC(this.w_NADESNAZ)+;
                  ","+cp_ToStrODBC(this.w_NACODISO)+;
                  ","+cp_ToStrODBC(this.w_NAPAGISO)+;
                  ","+cp_ToStrODBCNull(this.w_NACODVAL)+;
                  ","+cp_ToStrODBC(this.w_NACODUIC)+;
                  ","+cp_ToStrODBC(this.w_NACODEST)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'NAZIONI')
        i_extval=cp_InsertValVFPExtFlds(this,'NAZIONI')
        cp_CheckDeletedKey(i_cTable,0,'NACODNAZ',this.w_NACODNAZ)
        INSERT INTO (i_cTable);
              (NACODNAZ,NADESNAZ,NACODISO,NAPAGISO,NACODVAL,NACODUIC,NACODEST  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_NACODNAZ;
                  ,this.w_NADESNAZ;
                  ,this.w_NACODISO;
                  ,this.w_NAPAGISO;
                  ,this.w_NACODVAL;
                  ,this.w_NACODUIC;
                  ,this.w_NACODEST;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.NAZIONI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.NAZIONI_IDX,i_nConn)
      *
      * update NAZIONI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'NAZIONI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " NADESNAZ="+cp_ToStrODBC(this.w_NADESNAZ)+;
             ",NACODISO="+cp_ToStrODBC(this.w_NACODISO)+;
             ",NAPAGISO="+cp_ToStrODBC(this.w_NAPAGISO)+;
             ",NACODVAL="+cp_ToStrODBCNull(this.w_NACODVAL)+;
             ",NACODUIC="+cp_ToStrODBC(this.w_NACODUIC)+;
             ",NACODEST="+cp_ToStrODBC(this.w_NACODEST)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'NAZIONI')
        i_cWhere = cp_PKFox(i_cTable  ,'NACODNAZ',this.w_NACODNAZ  )
        UPDATE (i_cTable) SET;
              NADESNAZ=this.w_NADESNAZ;
             ,NACODISO=this.w_NACODISO;
             ,NAPAGISO=this.w_NAPAGISO;
             ,NACODVAL=this.w_NACODVAL;
             ,NACODUIC=this.w_NACODUIC;
             ,NACODEST=this.w_NACODEST;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.NAZIONI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.NAZIONI_IDX,i_nConn)
      *
      * delete NAZIONI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'NACODNAZ',this.w_NACODNAZ  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .DoRTCalc(1,8,.t.)
        if .o_NACODNAZ<>.w_NACODNAZ
            .w_NACODUIC = .w_NACODNAZ
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNAPAGISO_1_6.visible=!this.oPgFrm.Page1.oPag.oNAPAGISO_1_6.mHide()
    this.oPgFrm.Page1.oPag.oNACODUIC_1_13.visible=!this.oPgFrm.Page1.oPag.oNACODUIC_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_anz
    * --- Se AHE il check obsolescenza lo faccio con batch
    If cEvent='w_NACODVAL Changed' And g_APPLICATION='ad hoc ENTERPRISE'
     this.NotifyEvent('CheckObso')
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NACODVAL
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NACODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_NACODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_NACODVAL))
          select VACODVAL,VADESVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NACODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NACODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oNACODVAL_1_7'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NACODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_NACODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_NACODVAL)
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NACODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NACODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=g_APPLICATION='ad hoc ENTERPRISE' Or CHKDTOBS(.w_DATOBSO,.w_OBTEST,'Valuta obsoleta !')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NACODVAL = space(3)
        this.w_DESVAL = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NACODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.VACODVAL as VACODVAL107"+ ",link_1_7.VADESVAL as VADESVAL107"+ ",link_1_7.VADTOBSO as VADTOBSO107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on NAZIONI.NACODVAL=link_1_7.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and NAZIONI.NACODVAL=link_1_7.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNACODNAZ_1_1.value==this.w_NACODNAZ)
      this.oPgFrm.Page1.oPag.oNACODNAZ_1_1.value=this.w_NACODNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oNADESNAZ_1_2.value==this.w_NADESNAZ)
      this.oPgFrm.Page1.oPag.oNADESNAZ_1_2.value=this.w_NADESNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oNACODISO_1_3.value==this.w_NACODISO)
      this.oPgFrm.Page1.oPag.oNACODISO_1_3.value=this.w_NACODISO
    endif
    if not(this.oPgFrm.Page1.oPag.oNAPAGISO_1_6.value==this.w_NAPAGISO)
      this.oPgFrm.Page1.oPag.oNAPAGISO_1_6.value=this.w_NAPAGISO
    endif
    if not(this.oPgFrm.Page1.oPag.oNACODVAL_1_7.value==this.w_NACODVAL)
      this.oPgFrm.Page1.oPag.oNACODVAL_1_7.value=this.w_NACODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_8.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_8.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oNACODUIC_1_13.value==this.w_NACODUIC)
      this.oPgFrm.Page1.oPag.oNACODUIC_1_13.value=this.w_NACODUIC
    endif
    if not(this.oPgFrm.Page1.oPag.oNACODEST_1_15.value==this.w_NACODEST)
      this.oPgFrm.Page1.oPag.oNACODEST_1_15.value=this.w_NACODEST
    endif
    cp_SetControlsValueExtFlds(this,'NAZIONI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_NACODNAZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNACODNAZ_1_1.SetFocus()
            i_bnoObbl = !empty(.w_NACODNAZ)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(g_APPLICATION='ad hoc ENTERPRISE' Or CHKDTOBS(.w_DATOBSO,.w_OBTEST,'Valuta obsoleta !'))  and not(empty(.w_NACODVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNACODVAL_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NACODNAZ = this.w_NACODNAZ
    return

enddefine

* --- Define pages as container
define class tgsar_anzPag1 as StdContainer
  Width  = 478
  height = 156
  stdWidth  = 478
  stdheight = 156
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNACODNAZ_1_1 as StdField with uid="VLDBIVCDYA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NACODNAZ", cQueryName = "NACODNAZ",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della nazione di appartenenza del cliente/fornitore",;
    HelpContextID = 118095664,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=131, Top=12, InputMask=replicate('X',3)

  add object oNADESNAZ_1_2 as StdField with uid="WAVECSGDGC",rtseq=2,rtrep=.f.,;
    cFormVar = "w_NADESNAZ", cQueryName = "NADESNAZ",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Nome della nazione",;
    HelpContextID = 133173040,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=181, Top=12, InputMask=replicate('X',35)

  add object oNACODISO_1_3 as StdField with uid="RRIVQNHRHW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NACODISO", cQueryName = "NACODISO",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice ISO della nazione comunitaria",;
    HelpContextID = 234225883,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=131, Top=41, InputMask=replicate('X',3)

  add object oNAPAGISO_1_6 as StdField with uid="PATHCUDLOP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NAPAGISO", cQueryName = "NAPAGISO",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice ISO paese di pagamento utilizzato ai fini INTRA",;
    HelpContextID = 231944411,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=435, Top=41, InputMask=replicate('X',3)

  func oNAPAGISO_1_6.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oNACODVAL_1_7 as StdField with uid="EHFYADPKOY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NACODVAL", cQueryName = "NACODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta della nazione",;
    HelpContextID = 252313378,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=131, Top=70, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_NACODVAL"

  func oNACODVAL_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oNACODVAL_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNACODVAL_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oNACODVAL_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oNACODVAL_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_NACODVAL
     i_obj.ecpSave()
  endproc

  add object oDESVAL_1_8 as StdField with uid="GROPTDQSDA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 81920566,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=181, Top=70, InputMask=replicate('X',35)


  add object oObj_1_12 as cp_runprogram with uid="FKYVNVVTWS",left=0, top=193, width=236,height=19,;
    caption='CHKDTOBS',;
   bGlobalFont=.t.,;
    prg="CHKDTOBS(w_DATOBSO,w_OBTEST,'Valuta obsoleta !')",;
    cEvent = "CheckObso",;
    nPag=1;
    , HelpContextID = 117471879

  add object oNACODUIC_1_13 as StdField with uid="KYOULCVUAQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NACODUIC", cQueryName = "NACODUIC",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codifica UIC della nazione (Remote Banking)",;
    HelpContextID = 32899303,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=131, Top=99, cSayPict='"!!!"', cGetPict='"!!!"', InputMask=replicate('X',3)

  func oNACODUIC_1_13.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" And g_REBA<>'S')
    endwith
  endfunc

  add object oNACODEST_1_15 as StdField with uid="CWXLVCRQON",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NACODEST", cQueryName = "NACODEST",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codifica paese estero necessario per le dichiarazioni fiscali",;
    HelpContextID = 32899286,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=131, Top=128, InputMask=replicate('X',5)

  add object oStr_1_4 as StdString with uid="ETSOJHUQLD",Visible=.t., Left=85, Top=12,;
    Alignment=1, Width=42, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="JAQUXNQZWR",Visible=.t., Left=59, Top=41,;
    Alignment=1, Width=68, Height=15,;
    Caption="Codice ISO:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="KYNZILWXHD",Visible=.t., Left=3, Top=70,;
    Alignment=1, Width=124, Height=15,;
    Caption="Valuta nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="GNRBAGSYMR",Visible=.t., Left=42, Top=99,;
    Alignment=1, Width=85, Height=18,;
    Caption="Codifica UIC:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" And g_REBA<>'S')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="CHGUOJPFVM",Visible=.t., Left=29, Top=131,;
    Alignment=1, Width=98, Height=18,;
    Caption="Cod. paese est.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="WJOPTARASJ",Visible=.t., Left=214, Top=41,;
    Alignment=1, Width=219, Height=15,;
    Caption="Codice paese di pagamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_anz','NAZIONI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".NACODNAZ=NAZIONI.NACODNAZ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
