* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bre                                                        *
*              Eliminazione figli riordino                                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-13                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bre",oParentObject)
return(i_retval)

define class tgsma_bre as StdBatch
  * --- Local variables
  w_Messaggio = space(6)
  w_MESS = space(254)
  * --- WorkFile variables
  INVEDETT_idx=0
  LIFOSCAT_idx=0
  LIFOCONT_idx=0
  FIFOCONT_idx=0
  INVENTAR_idx=0
  RIORDETT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione righe Dettaglio Riordino
    * --- Variabili anagrafica
    * --- Variabili locali
    * --- Richiesta della conferma per l'eliminazione dei figli
    if NOT AH_YESNO("Attenzione:%0Verr� eliminato il calcolo del punto e del lotto di riordino per tutti gli articoli nella elaborazione selezionata%0%0Confermi la cancellazione?")
      * --- Abbandona la Transazione
      * --- Try
      local bErr_03944BF8
      bErr_03944BF8=bTrsErr
      this.Try_03944BF8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_msgformat("Eliminazione annullata")
      endif
      bTrsErr=bTrsErr or bErr_03944BF8
      * --- End
      i_retcode = 'stop'
      return
    endif
    * --- Messaggio di elaborazione in corso
    ah_msg("Eliminazione archivio collegato...")
    * --- Eliminazione
    * --- Try
    local bErr_03942EE8
    bErr_03942EE8=bTrsErr
    this.Try_03942EE8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Abbandona la Transazione
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=MSG_TRANSACTION_ERROR
    endif
    bTrsErr=bTrsErr or bErr_03942EE8
    * --- End
  endproc
  proc Try_03944BF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Raise
    i_Error=ah_msgformat("Eliminazione annullata")
    return
    return
  proc Try_03942EE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Dettaglio articoli
    * --- Delete from RIORDETT
    i_nConn=i_TableProp[this.RIORDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIORDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DINUMRIO = "+cp_ToStrODBC(this.oParentObject.w_RINUMRIO);
            +" and DICODESE = "+cp_ToStrODBC(this.oParentObject.w_RICODESE);
             )
    else
      delete from (i_cTable) where;
            DINUMRIO = this.oParentObject.w_RINUMRIO;
            and DICODESE = this.oParentObject.w_RICODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='INVEDETT'
    this.cWorkTables[2]='LIFOSCAT'
    this.cWorkTables[3]='LIFOCONT'
    this.cWorkTables[4]='FIFOCONT'
    this.cWorkTables[5]='INVENTAR'
    this.cWorkTables[6]='RIORDETT'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
