* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_aps                                                        *
*              Politica di sicurezza                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_55]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-09                                                      *
* Last revis.: 2013-11-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_aps")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_aps")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_aps")
  return

* --- Class definition
define class tgsut_aps as StdPCForm
  Width  = 761
  Height = 553
  Top    = 4
  Left   = 5
  cComment = "Politica di sicurezza"
  cPrg = "gsut_aps"
  HelpContextID=159990633
  add object cnt as tcgsut_aps
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_aps as PCContext
  w_PSSERIAL = space(10)
  w_NOTEBAS = space(10)
  w_NOTEMED = space(10)
  w_NOTEFOR = space(10)
  w_NOTEOTT = space(10)
  w_LIMCOMP = 0
  w_LIMCOMA = 0
  w_PSATTIVO = space(1)
  w_PS__JBSH = space(1)
  w_PSLUNMIN = 0
  w_PS_NORIP = space(1)
  w_PSCMPLES = 0
  w_PSNUMPWD = 0
  w_PSLUNMIA = 0
  w_PSNORIPA = space(1)
  w_PSCMPLEA = 0
  w_PSBLCSDP = space(1)
  w_PSVALMAX = 0
  w_PSGGANTI = 0
  w_PS__AMMI = space(1)
  w_PSVALMAA = 0
  w_PSGGANTA = 0
  w_PSMAXTEA = 0
  w_PSMAXTEN = 0
  w_PSBLCNMT = space(1)
  w_PSULTACC = 0
  w_PSBLCSDA = space(1)
  w_PS_NONUM = space(1)
  w_NOTE = space(10)
  w_NOTE2 = space(10)
  proc Save(oFrom)
    this.w_PSSERIAL = oFrom.w_PSSERIAL
    this.w_NOTEBAS = oFrom.w_NOTEBAS
    this.w_NOTEMED = oFrom.w_NOTEMED
    this.w_NOTEFOR = oFrom.w_NOTEFOR
    this.w_NOTEOTT = oFrom.w_NOTEOTT
    this.w_LIMCOMP = oFrom.w_LIMCOMP
    this.w_LIMCOMA = oFrom.w_LIMCOMA
    this.w_PSATTIVO = oFrom.w_PSATTIVO
    this.w_PS__JBSH = oFrom.w_PS__JBSH
    this.w_PSLUNMIN = oFrom.w_PSLUNMIN
    this.w_PS_NORIP = oFrom.w_PS_NORIP
    this.w_PSCMPLES = oFrom.w_PSCMPLES
    this.w_PSNUMPWD = oFrom.w_PSNUMPWD
    this.w_PSLUNMIA = oFrom.w_PSLUNMIA
    this.w_PSNORIPA = oFrom.w_PSNORIPA
    this.w_PSCMPLEA = oFrom.w_PSCMPLEA
    this.w_PSBLCSDP = oFrom.w_PSBLCSDP
    this.w_PSVALMAX = oFrom.w_PSVALMAX
    this.w_PSGGANTI = oFrom.w_PSGGANTI
    this.w_PS__AMMI = oFrom.w_PS__AMMI
    this.w_PSVALMAA = oFrom.w_PSVALMAA
    this.w_PSGGANTA = oFrom.w_PSGGANTA
    this.w_PSMAXTEA = oFrom.w_PSMAXTEA
    this.w_PSMAXTEN = oFrom.w_PSMAXTEN
    this.w_PSBLCNMT = oFrom.w_PSBLCNMT
    this.w_PSULTACC = oFrom.w_PSULTACC
    this.w_PSBLCSDA = oFrom.w_PSBLCSDA
    this.w_PS_NONUM = oFrom.w_PS_NONUM
    this.w_NOTE = oFrom.w_NOTE
    this.w_NOTE2 = oFrom.w_NOTE2
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_PSSERIAL = this.w_PSSERIAL
    oTo.w_NOTEBAS = this.w_NOTEBAS
    oTo.w_NOTEMED = this.w_NOTEMED
    oTo.w_NOTEFOR = this.w_NOTEFOR
    oTo.w_NOTEOTT = this.w_NOTEOTT
    oTo.w_LIMCOMP = this.w_LIMCOMP
    oTo.w_LIMCOMA = this.w_LIMCOMA
    oTo.w_PSATTIVO = this.w_PSATTIVO
    oTo.w_PS__JBSH = this.w_PS__JBSH
    oTo.w_PSLUNMIN = this.w_PSLUNMIN
    oTo.w_PS_NORIP = this.w_PS_NORIP
    oTo.w_PSCMPLES = this.w_PSCMPLES
    oTo.w_PSNUMPWD = this.w_PSNUMPWD
    oTo.w_PSLUNMIA = this.w_PSLUNMIA
    oTo.w_PSNORIPA = this.w_PSNORIPA
    oTo.w_PSCMPLEA = this.w_PSCMPLEA
    oTo.w_PSBLCSDP = this.w_PSBLCSDP
    oTo.w_PSVALMAX = this.w_PSVALMAX
    oTo.w_PSGGANTI = this.w_PSGGANTI
    oTo.w_PS__AMMI = this.w_PS__AMMI
    oTo.w_PSVALMAA = this.w_PSVALMAA
    oTo.w_PSGGANTA = this.w_PSGGANTA
    oTo.w_PSMAXTEA = this.w_PSMAXTEA
    oTo.w_PSMAXTEN = this.w_PSMAXTEN
    oTo.w_PSBLCNMT = this.w_PSBLCNMT
    oTo.w_PSULTACC = this.w_PSULTACC
    oTo.w_PSBLCSDA = this.w_PSBLCSDA
    oTo.w_PS_NONUM = this.w_PS_NONUM
    oTo.w_NOTE = this.w_NOTE
    oTo.w_NOTE2 = this.w_NOTE2
    PCContext::Load(oTo)
enddefine

define class tcgsut_aps as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 761
  Height = 553
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-25"
  HelpContextID=159990633
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  POL_SIC_IDX = 0
  cFile = "POL_SIC"
  cKeySelect = "PSSERIAL"
  cKeyWhere  = "PSSERIAL=this.w_PSSERIAL"
  cKeyWhereODBC = '"PSSERIAL="+cp_ToStrODBC(this.w_PSSERIAL)';

  cKeyWhereODBCqualified = '"POL_SIC.PSSERIAL="+cp_ToStrODBC(this.w_PSSERIAL)';

  cPrg = "gsut_aps"
  cComment = "Politica di sicurezza"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PSSERIAL = space(10)
  w_NOTEBAS = space(0)
  w_NOTEMED = space(0)
  w_NOTEFOR = space(0)
  w_NOTEOTT = space(0)
  w_LIMCOMP = 0
  o_LIMCOMP = 0
  w_LIMCOMA = 0
  o_LIMCOMA = 0
  w_PSATTIVO = space(1)
  o_PSATTIVO = space(1)
  w_PS__JBSH = space(1)
  w_PSLUNMIN = 0
  w_PS_NORIP = space(1)
  w_PSCMPLES = 0
  o_PSCMPLES = 0
  w_PSNUMPWD = 0
  w_PSLUNMIA = 0
  w_PSNORIPA = space(1)
  w_PSCMPLEA = 0
  o_PSCMPLEA = 0
  w_PSBLCSDP = space(1)
  w_PSVALMAX = 0
  o_PSVALMAX = 0
  w_PSGGANTI = 0
  w_PS__AMMI = space(1)
  o_PS__AMMI = space(1)
  w_PSVALMAA = 0
  o_PSVALMAA = 0
  w_PSGGANTA = 0
  w_PSMAXTEA = 0
  w_PSMAXTEN = 0
  w_PSBLCNMT = space(1)
  w_PSULTACC = 0
  o_PSULTACC = 0
  w_PSBLCSDA = space(1)
  w_PS_NONUM = space(1)
  w_NOTE = space(0)
  w_NOTE2 = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_apsPag1","gsut_aps",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 101543178
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPSATTIVO_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='POL_SIC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.POL_SIC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.POL_SIC_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_aps'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from POL_SIC where PSSERIAL=KeySet.PSSERIAL
    *
    i_nConn = i_TableProp[this.POL_SIC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('POL_SIC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "POL_SIC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' POL_SIC '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PSSERIAL',this.w_PSSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_NOTEBAS = "La password pu� contenere valori di una sola delle categorie di caratteri"
        .w_NOTEMED = "La password deve contenere valori appartenti ad almeno due delle categorie di caratteri"
        .w_NOTEFOR = "La password deve contenere valori appartenti ad almeno tre delle categorie di caratteri"
        .w_NOTEOTT = "La password deve contenere valori appartenti a tutte le categorie di caratteri"
        .w_PSSERIAL = NVL(PSSERIAL,space(10))
        .w_LIMCOMP = ICASE(.w_PSCMPLES=2, 4, .w_PSCMPLES=3, 6, .w_PSCMPLES=4, 8, 0)
        .w_LIMCOMA = ICASE(.w_PSCMPLEA=2, 4, .w_PSCMPLEA=3, 6, .w_PSCMPLEA=4, 8, 0)
        .w_PSATTIVO = NVL(PSATTIVO,space(1))
        .w_PS__JBSH = NVL(PS__JBSH,space(1))
        .w_PSLUNMIN = NVL(PSLUNMIN,0)
        .w_PS_NORIP = NVL(PS_NORIP,space(1))
        .w_PSCMPLES = NVL(PSCMPLES,0)
        .w_PSNUMPWD = NVL(PSNUMPWD,0)
        .w_PSLUNMIA = NVL(PSLUNMIA,0)
        .w_PSNORIPA = NVL(PSNORIPA,space(1))
        .w_PSCMPLEA = NVL(PSCMPLEA,0)
        .w_PSBLCSDP = NVL(PSBLCSDP,space(1))
        .w_PSVALMAX = NVL(PSVALMAX,0)
        .w_PSGGANTI = NVL(PSGGANTI,0)
        .w_PS__AMMI = NVL(PS__AMMI,space(1))
        .w_PSVALMAA = NVL(PSVALMAA,0)
        .w_PSGGANTA = NVL(PSGGANTA,0)
        .w_PSMAXTEA = NVL(PSMAXTEA,0)
        .w_PSMAXTEN = NVL(PSMAXTEN,0)
        .w_PSBLCNMT = NVL(PSBLCNMT,space(1))
        .w_PSULTACC = NVL(PSULTACC,0)
        .w_PSBLCSDA = NVL(PSBLCSDA,space(1))
        .w_PS_NONUM = NVL(PS_NONUM,space(1))
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(Ah_MsgFormat("(attiva l'avviso di scadenza con%0un anticipo pari ai giorni indicati)"))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(Ah_MsgFormat("(attiva l'avviso di scadenza con%0un anticipo pari ai giorni indicati)"))
        .w_NOTE = ICASE(.w_PSCMPLES=1,.w_NOTEBAS,.w_PSCMPLES=2,.w_NOTEMED,.w_PSCMPLES=3,.w_NOTEFOR, .w_NOTEOTT)
        .w_NOTE2 = ICASE(.w_PSCMPLEA=1,.w_NOTEBAS,.w_PSCMPLEA=2,.w_NOTEMED,.w_PSCMPLEA=3,.w_NOTEFOR, .w_NOTEOTT)
        cp_LoadRecExtFlds(this,'POL_SIC')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_PSSERIAL = space(10)
      .w_NOTEBAS = space(0)
      .w_NOTEMED = space(0)
      .w_NOTEFOR = space(0)
      .w_NOTEOTT = space(0)
      .w_LIMCOMP = 0
      .w_LIMCOMA = 0
      .w_PSATTIVO = space(1)
      .w_PS__JBSH = space(1)
      .w_PSLUNMIN = 0
      .w_PS_NORIP = space(1)
      .w_PSCMPLES = 0
      .w_PSNUMPWD = 0
      .w_PSLUNMIA = 0
      .w_PSNORIPA = space(1)
      .w_PSCMPLEA = 0
      .w_PSBLCSDP = space(1)
      .w_PSVALMAX = 0
      .w_PSGGANTI = 0
      .w_PS__AMMI = space(1)
      .w_PSVALMAA = 0
      .w_PSGGANTA = 0
      .w_PSMAXTEA = 0
      .w_PSMAXTEN = 0
      .w_PSBLCNMT = space(1)
      .w_PSULTACC = 0
      .w_PSBLCSDA = space(1)
      .w_PS_NONUM = space(1)
      .w_NOTE = space(0)
      .w_NOTE2 = space(0)
      if .cFunction<>"Filter"
        .w_PSSERIAL = 'AHE'
        .w_NOTEBAS = "La password pu� contenere valori di una sola delle categorie di caratteri"
        .w_NOTEMED = "La password deve contenere valori appartenti ad almeno due delle categorie di caratteri"
        .w_NOTEFOR = "La password deve contenere valori appartenti ad almeno tre delle categorie di caratteri"
        .w_NOTEOTT = "La password deve contenere valori appartenti a tutte le categorie di caratteri"
        .w_LIMCOMP = ICASE(.w_PSCMPLES=2, 4, .w_PSCMPLES=3, 6, .w_PSCMPLES=4, 8, 0)
        .w_LIMCOMA = ICASE(.w_PSCMPLEA=2, 4, .w_PSCMPLEA=3, 6, .w_PSCMPLEA=4, 8, 0)
        .w_PSATTIVO = 'N'
        .w_PS__JBSH = ' '
        .w_PSLUNMIN = IIF(NOT EMPTY(.w_LIMCOMP) AND .w_PSLUNMIN<.w_LIMCOMP, .w_LIMCOMP, .w_PSLUNMIN)
          .DoRTCalc(11,13,.f.)
        .w_PSLUNMIA = IIF(NOT EMPTY(.w_LIMCOMA) AND .w_PSLUNMIA<.w_LIMCOMA, .w_LIMCOMA, .w_PSLUNMIA)
          .DoRTCalc(15,16,.f.)
        .w_PSBLCSDP = iif( .w_PSVALMAX=0 ,  ' '  , .w_PSBLCSDP )
          .DoRTCalc(18,18,.f.)
        .w_PSGGANTI = iif( .w_PSVALMAX=0 ,  0  , iif(.w_PSGGANTI=0, 1, .w_PSGGANTI) )
        .w_PS__AMMI = 'N'
          .DoRTCalc(21,21,.f.)
        .w_PSGGANTA = iif( .w_PSVALMAA=0 ,  0  , .w_PSGGANTA )
          .DoRTCalc(23,23,.f.)
        .w_PSMAXTEN = 3
          .DoRTCalc(25,26,.f.)
        .w_PSBLCSDA = iif( .w_PSULTACC=0 ,  'N'  , .w_PSBLCSDA )
        .w_PS_NONUM = 'N'
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(Ah_MsgFormat("(attiva l'avviso di scadenza con%0un anticipo pari ai giorni indicati)"))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(Ah_MsgFormat("(attiva l'avviso di scadenza con%0un anticipo pari ai giorni indicati)"))
        .w_NOTE = ICASE(.w_PSCMPLES=1,.w_NOTEBAS,.w_PSCMPLES=2,.w_NOTEMED,.w_PSCMPLES=3,.w_NOTEFOR, .w_NOTEOTT)
        .w_NOTE2 = ICASE(.w_PSCMPLEA=1,.w_NOTEBAS,.w_PSCMPLEA=2,.w_NOTEMED,.w_PSCMPLEA=3,.w_NOTEFOR, .w_NOTEOTT)
      endif
    endwith
    cp_BlankRecExtFlds(this,'POL_SIC')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPSATTIVO_1_8.enabled = i_bVal
      .Page1.oPag.oPS__JBSH_1_9.enabled = i_bVal
      .Page1.oPag.oPSLUNMIN_1_10.enabled = i_bVal
      .Page1.oPag.oPS_NORIP_1_11.enabled = i_bVal
      .Page1.oPag.oPSCMPLES_1_13.enabled = i_bVal
      .Page1.oPag.oPSNUMPWD_1_15.enabled = i_bVal
      .Page1.oPag.oPSLUNMIA_1_17.enabled = i_bVal
      .Page1.oPag.oPSNORIPA_1_18.enabled = i_bVal
      .Page1.oPag.oPSCMPLEA_1_20.enabled = i_bVal
      .Page1.oPag.oPSBLCSDP_1_22.enabled = i_bVal
      .Page1.oPag.oPSVALMAX_1_23.enabled = i_bVal
      .Page1.oPag.oPSGGANTI_1_25.enabled = i_bVal
      .Page1.oPag.oPS__AMMI_1_26.enabled = i_bVal
      .Page1.oPag.oPSVALMAA_1_27.enabled = i_bVal
      .Page1.oPag.oPSGGANTA_1_28.enabled = i_bVal
      .Page1.oPag.oPSMAXTEA_1_29.enabled = i_bVal
      .Page1.oPag.oPSMAXTEN_1_30.enabled = i_bVal
      .Page1.oPag.oPSBLCNMT_1_31.enabled = i_bVal
      .Page1.oPag.oPSULTACC_1_32.enabled = i_bVal
      .Page1.oPag.oPSBLCSDA_1_33.enabled = i_bVal
      .Page1.oPag.oPS_NONUM_1_34.enabled = i_bVal
      .Page1.oPag.oBtn_1_21.enabled = i_bVal
      .Page1.oPag.oBtn_1_35.enabled = .Page1.oPag.oBtn_1_35.mCond()
      .Page1.oPag.oBtn_1_36.enabled = .Page1.oPag.oBtn_1_36.mCond()
      .Page1.oPag.oObj_1_50.enabled = i_bVal
      .Page1.oPag.oObj_1_51.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'POL_SIC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.POL_SIC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSSERIAL,"PSSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSATTIVO,"PSATTIVO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PS__JBSH,"PS__JBSH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSLUNMIN,"PSLUNMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PS_NORIP,"PS_NORIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCMPLES,"PSCMPLES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSNUMPWD,"PSNUMPWD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSLUNMIA,"PSLUNMIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSNORIPA,"PSNORIPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCMPLEA,"PSCMPLEA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSBLCSDP,"PSBLCSDP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSVALMAX,"PSVALMAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSGGANTI,"PSGGANTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PS__AMMI,"PS__AMMI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSVALMAA,"PSVALMAA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSGGANTA,"PSGGANTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSMAXTEA,"PSMAXTEA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSMAXTEN,"PSMAXTEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSBLCNMT,"PSBLCNMT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSULTACC,"PSULTACC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSBLCSDA,"PSBLCSDA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PS_NONUM,"PS_NONUM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.POL_SIC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.POL_SIC_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into POL_SIC
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'POL_SIC')
        i_extval=cp_InsertValODBCExtFlds(this,'POL_SIC')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PSSERIAL,PSATTIVO,PS__JBSH,PSLUNMIN,PS_NORIP"+;
                  ",PSCMPLES,PSNUMPWD,PSLUNMIA,PSNORIPA,PSCMPLEA"+;
                  ",PSBLCSDP,PSVALMAX,PSGGANTI,PS__AMMI,PSVALMAA"+;
                  ",PSGGANTA,PSMAXTEA,PSMAXTEN,PSBLCNMT,PSULTACC"+;
                  ",PSBLCSDA,PS_NONUM "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PSSERIAL)+;
                  ","+cp_ToStrODBC(this.w_PSATTIVO)+;
                  ","+cp_ToStrODBC(this.w_PS__JBSH)+;
                  ","+cp_ToStrODBC(this.w_PSLUNMIN)+;
                  ","+cp_ToStrODBC(this.w_PS_NORIP)+;
                  ","+cp_ToStrODBC(this.w_PSCMPLES)+;
                  ","+cp_ToStrODBC(this.w_PSNUMPWD)+;
                  ","+cp_ToStrODBC(this.w_PSLUNMIA)+;
                  ","+cp_ToStrODBC(this.w_PSNORIPA)+;
                  ","+cp_ToStrODBC(this.w_PSCMPLEA)+;
                  ","+cp_ToStrODBC(this.w_PSBLCSDP)+;
                  ","+cp_ToStrODBC(this.w_PSVALMAX)+;
                  ","+cp_ToStrODBC(this.w_PSGGANTI)+;
                  ","+cp_ToStrODBC(this.w_PS__AMMI)+;
                  ","+cp_ToStrODBC(this.w_PSVALMAA)+;
                  ","+cp_ToStrODBC(this.w_PSGGANTA)+;
                  ","+cp_ToStrODBC(this.w_PSMAXTEA)+;
                  ","+cp_ToStrODBC(this.w_PSMAXTEN)+;
                  ","+cp_ToStrODBC(this.w_PSBLCNMT)+;
                  ","+cp_ToStrODBC(this.w_PSULTACC)+;
                  ","+cp_ToStrODBC(this.w_PSBLCSDA)+;
                  ","+cp_ToStrODBC(this.w_PS_NONUM)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'POL_SIC')
        i_extval=cp_InsertValVFPExtFlds(this,'POL_SIC')
        cp_CheckDeletedKey(i_cTable,0,'PSSERIAL',this.w_PSSERIAL)
        INSERT INTO (i_cTable);
              (PSSERIAL,PSATTIVO,PS__JBSH,PSLUNMIN,PS_NORIP,PSCMPLES,PSNUMPWD,PSLUNMIA,PSNORIPA,PSCMPLEA,PSBLCSDP,PSVALMAX,PSGGANTI,PS__AMMI,PSVALMAA,PSGGANTA,PSMAXTEA,PSMAXTEN,PSBLCNMT,PSULTACC,PSBLCSDA,PS_NONUM  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PSSERIAL;
                  ,this.w_PSATTIVO;
                  ,this.w_PS__JBSH;
                  ,this.w_PSLUNMIN;
                  ,this.w_PS_NORIP;
                  ,this.w_PSCMPLES;
                  ,this.w_PSNUMPWD;
                  ,this.w_PSLUNMIA;
                  ,this.w_PSNORIPA;
                  ,this.w_PSCMPLEA;
                  ,this.w_PSBLCSDP;
                  ,this.w_PSVALMAX;
                  ,this.w_PSGGANTI;
                  ,this.w_PS__AMMI;
                  ,this.w_PSVALMAA;
                  ,this.w_PSGGANTA;
                  ,this.w_PSMAXTEA;
                  ,this.w_PSMAXTEN;
                  ,this.w_PSBLCNMT;
                  ,this.w_PSULTACC;
                  ,this.w_PSBLCSDA;
                  ,this.w_PS_NONUM;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.POL_SIC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.POL_SIC_IDX,i_nConn)
      *
      * update POL_SIC
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'POL_SIC')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PSATTIVO="+cp_ToStrODBC(this.w_PSATTIVO)+;
             ",PS__JBSH="+cp_ToStrODBC(this.w_PS__JBSH)+;
             ",PSLUNMIN="+cp_ToStrODBC(this.w_PSLUNMIN)+;
             ",PS_NORIP="+cp_ToStrODBC(this.w_PS_NORIP)+;
             ",PSCMPLES="+cp_ToStrODBC(this.w_PSCMPLES)+;
             ",PSNUMPWD="+cp_ToStrODBC(this.w_PSNUMPWD)+;
             ",PSLUNMIA="+cp_ToStrODBC(this.w_PSLUNMIA)+;
             ",PSNORIPA="+cp_ToStrODBC(this.w_PSNORIPA)+;
             ",PSCMPLEA="+cp_ToStrODBC(this.w_PSCMPLEA)+;
             ",PSBLCSDP="+cp_ToStrODBC(this.w_PSBLCSDP)+;
             ",PSVALMAX="+cp_ToStrODBC(this.w_PSVALMAX)+;
             ",PSGGANTI="+cp_ToStrODBC(this.w_PSGGANTI)+;
             ",PS__AMMI="+cp_ToStrODBC(this.w_PS__AMMI)+;
             ",PSVALMAA="+cp_ToStrODBC(this.w_PSVALMAA)+;
             ",PSGGANTA="+cp_ToStrODBC(this.w_PSGGANTA)+;
             ",PSMAXTEA="+cp_ToStrODBC(this.w_PSMAXTEA)+;
             ",PSMAXTEN="+cp_ToStrODBC(this.w_PSMAXTEN)+;
             ",PSBLCNMT="+cp_ToStrODBC(this.w_PSBLCNMT)+;
             ",PSULTACC="+cp_ToStrODBC(this.w_PSULTACC)+;
             ",PSBLCSDA="+cp_ToStrODBC(this.w_PSBLCSDA)+;
             ",PS_NONUM="+cp_ToStrODBC(this.w_PS_NONUM)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'POL_SIC')
        i_cWhere = cp_PKFox(i_cTable  ,'PSSERIAL',this.w_PSSERIAL  )
        UPDATE (i_cTable) SET;
              PSATTIVO=this.w_PSATTIVO;
             ,PS__JBSH=this.w_PS__JBSH;
             ,PSLUNMIN=this.w_PSLUNMIN;
             ,PS_NORIP=this.w_PS_NORIP;
             ,PSCMPLES=this.w_PSCMPLES;
             ,PSNUMPWD=this.w_PSNUMPWD;
             ,PSLUNMIA=this.w_PSLUNMIA;
             ,PSNORIPA=this.w_PSNORIPA;
             ,PSCMPLEA=this.w_PSCMPLEA;
             ,PSBLCSDP=this.w_PSBLCSDP;
             ,PSVALMAX=this.w_PSVALMAX;
             ,PSGGANTI=this.w_PSGGANTI;
             ,PS__AMMI=this.w_PS__AMMI;
             ,PSVALMAA=this.w_PSVALMAA;
             ,PSGGANTA=this.w_PSGGANTA;
             ,PSMAXTEA=this.w_PSMAXTEA;
             ,PSMAXTEN=this.w_PSMAXTEN;
             ,PSBLCNMT=this.w_PSBLCNMT;
             ,PSULTACC=this.w_PSULTACC;
             ,PSBLCSDA=this.w_PSBLCSDA;
             ,PS_NONUM=this.w_PS_NONUM;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.POL_SIC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.POL_SIC_IDX,i_nConn)
      *
      * delete POL_SIC
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PSSERIAL',this.w_PSSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.POL_SIC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_PSCMPLES<>.w_PSCMPLES
            .w_LIMCOMP = ICASE(.w_PSCMPLES=2, 4, .w_PSCMPLES=3, 6, .w_PSCMPLES=4, 8, 0)
        endif
        if .o_PSCMPLEA<>.w_PSCMPLEA
            .w_LIMCOMA = ICASE(.w_PSCMPLEA=2, 4, .w_PSCMPLEA=3, 6, .w_PSCMPLEA=4, 8, 0)
        endif
        .DoRTCalc(8,9,.t.)
        if .o_LIMCOMP<>.w_LIMCOMP
            .w_PSLUNMIN = IIF(NOT EMPTY(.w_LIMCOMP) AND .w_PSLUNMIN<.w_LIMCOMP, .w_LIMCOMP, .w_PSLUNMIN)
        endif
        .DoRTCalc(11,13,.t.)
        if .o_LIMCOMA<>.w_LIMCOMA
            .w_PSLUNMIA = IIF(NOT EMPTY(.w_LIMCOMA) AND .w_PSLUNMIA<.w_LIMCOMA, .w_LIMCOMA, .w_PSLUNMIA)
        endif
        .DoRTCalc(15,16,.t.)
        if .o_PSVALMAX<>.w_PSVALMAX
            .w_PSBLCSDP = iif( .w_PSVALMAX=0 ,  ' '  , .w_PSBLCSDP )
        endif
        .DoRTCalc(18,18,.t.)
        if .o_PSVALMAX<>.w_PSVALMAX
            .w_PSGGANTI = iif( .w_PSVALMAX=0 ,  0  , iif(.w_PSGGANTI=0, 1, .w_PSGGANTI) )
        endif
        .DoRTCalc(20,21,.t.)
        if .o_PSVALMAA<>.w_PSVALMAA
            .w_PSGGANTA = iif( .w_PSVALMAA=0 ,  0  , .w_PSGGANTA )
        endif
        .DoRTCalc(23,26,.t.)
        if .o_PSULTACC<>.w_PSULTACC
            .w_PSBLCSDA = iif( .w_PSULTACC=0 ,  'N'  , .w_PSBLCSDA )
        endif
        if .o_PSATTIVO<>.w_PSATTIVO
            .w_PS_NONUM = 'N'
        endif
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(Ah_MsgFormat("(attiva l'avviso di scadenza con%0un anticipo pari ai giorni indicati)"))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(Ah_MsgFormat("(attiva l'avviso di scadenza con%0un anticipo pari ai giorni indicati)"))
        if .o_PS__AMMI<>.w_PS__AMMI
          .Calculate_XQZQGSPTEY()
        endif
        if .o_PSCMPLES<>.w_PSCMPLES
            .w_NOTE = ICASE(.w_PSCMPLES=1,.w_NOTEBAS,.w_PSCMPLES=2,.w_NOTEMED,.w_PSCMPLES=3,.w_NOTEFOR, .w_NOTEOTT)
        endif
        if .o_PSCMPLEA<>.w_PSCMPLEA
            .w_NOTE2 = ICASE(.w_PSCMPLEA=1,.w_NOTEBAS,.w_PSCMPLEA=2,.w_NOTEMED,.w_PSCMPLEA=3,.w_NOTEFOR, .w_NOTEOTT)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(Ah_MsgFormat("(attiva l'avviso di scadenza con%0un anticipo pari ai giorni indicati)"))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(Ah_MsgFormat("(attiva l'avviso di scadenza con%0un anticipo pari ai giorni indicati)"))
    endwith
  return

  proc Calculate_XQZQGSPTEY()
    with this
          * --- Sbianca campi
          .w_PSVALMAA = IIF(.w_PS__AMMI<>'S',0,0)
          .w_PSGGANTA = IIF(.w_PS__AMMI<>'S',0,0)
          .w_PSMAXTEA = IIF(.w_PS__AMMI='S',0,3)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPS__JBSH_1_9.enabled = this.oPgFrm.Page1.oPag.oPS__JBSH_1_9.mCond()
    this.oPgFrm.Page1.oPag.oPSLUNMIN_1_10.enabled = this.oPgFrm.Page1.oPag.oPSLUNMIN_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPS_NORIP_1_11.enabled = this.oPgFrm.Page1.oPag.oPS_NORIP_1_11.mCond()
    this.oPgFrm.Page1.oPag.oPSCMPLES_1_13.enabled = this.oPgFrm.Page1.oPag.oPSCMPLES_1_13.mCond()
    this.oPgFrm.Page1.oPag.oPSNUMPWD_1_15.enabled = this.oPgFrm.Page1.oPag.oPSNUMPWD_1_15.mCond()
    this.oPgFrm.Page1.oPag.oPSLUNMIA_1_17.enabled = this.oPgFrm.Page1.oPag.oPSLUNMIA_1_17.mCond()
    this.oPgFrm.Page1.oPag.oPSNORIPA_1_18.enabled = this.oPgFrm.Page1.oPag.oPSNORIPA_1_18.mCond()
    this.oPgFrm.Page1.oPag.oPSCMPLEA_1_20.enabled = this.oPgFrm.Page1.oPag.oPSCMPLEA_1_20.mCond()
    this.oPgFrm.Page1.oPag.oPSBLCSDP_1_22.enabled = this.oPgFrm.Page1.oPag.oPSBLCSDP_1_22.mCond()
    this.oPgFrm.Page1.oPag.oPSVALMAX_1_23.enabled = this.oPgFrm.Page1.oPag.oPSVALMAX_1_23.mCond()
    this.oPgFrm.Page1.oPag.oPSGGANTI_1_25.enabled = this.oPgFrm.Page1.oPag.oPSGGANTI_1_25.mCond()
    this.oPgFrm.Page1.oPag.oPS__AMMI_1_26.enabled = this.oPgFrm.Page1.oPag.oPS__AMMI_1_26.mCond()
    this.oPgFrm.Page1.oPag.oPSVALMAA_1_27.enabled = this.oPgFrm.Page1.oPag.oPSVALMAA_1_27.mCond()
    this.oPgFrm.Page1.oPag.oPSGGANTA_1_28.enabled = this.oPgFrm.Page1.oPag.oPSGGANTA_1_28.mCond()
    this.oPgFrm.Page1.oPag.oPSMAXTEA_1_29.enabled = this.oPgFrm.Page1.oPag.oPSMAXTEA_1_29.mCond()
    this.oPgFrm.Page1.oPag.oPSMAXTEN_1_30.enabled = this.oPgFrm.Page1.oPag.oPSMAXTEN_1_30.mCond()
    this.oPgFrm.Page1.oPag.oPSBLCNMT_1_31.enabled = this.oPgFrm.Page1.oPag.oPSBLCNMT_1_31.mCond()
    this.oPgFrm.Page1.oPag.oPSULTACC_1_32.enabled = this.oPgFrm.Page1.oPag.oPSULTACC_1_32.mCond()
    this.oPgFrm.Page1.oPag.oPSBLCSDA_1_33.enabled = this.oPgFrm.Page1.oPag.oPSBLCSDA_1_33.mCond()
    this.oPgFrm.Page1.oPag.oPS_NONUM_1_34.enabled = this.oPgFrm.Page1.oPag.oPS_NONUM_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPSATTIVO_1_8.RadioValue()==this.w_PSATTIVO)
      this.oPgFrm.Page1.oPag.oPSATTIVO_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPS__JBSH_1_9.RadioValue()==this.w_PS__JBSH)
      this.oPgFrm.Page1.oPag.oPS__JBSH_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSLUNMIN_1_10.value==this.w_PSLUNMIN)
      this.oPgFrm.Page1.oPag.oPSLUNMIN_1_10.value=this.w_PSLUNMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPS_NORIP_1_11.RadioValue()==this.w_PS_NORIP)
      this.oPgFrm.Page1.oPag.oPS_NORIP_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCMPLES_1_13.RadioValue()==this.w_PSCMPLES)
      this.oPgFrm.Page1.oPag.oPSCMPLES_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSNUMPWD_1_15.value==this.w_PSNUMPWD)
      this.oPgFrm.Page1.oPag.oPSNUMPWD_1_15.value=this.w_PSNUMPWD
    endif
    if not(this.oPgFrm.Page1.oPag.oPSLUNMIA_1_17.value==this.w_PSLUNMIA)
      this.oPgFrm.Page1.oPag.oPSLUNMIA_1_17.value=this.w_PSLUNMIA
    endif
    if not(this.oPgFrm.Page1.oPag.oPSNORIPA_1_18.RadioValue()==this.w_PSNORIPA)
      this.oPgFrm.Page1.oPag.oPSNORIPA_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCMPLEA_1_20.RadioValue()==this.w_PSCMPLEA)
      this.oPgFrm.Page1.oPag.oPSCMPLEA_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSBLCSDP_1_22.RadioValue()==this.w_PSBLCSDP)
      this.oPgFrm.Page1.oPag.oPSBLCSDP_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSVALMAX_1_23.value==this.w_PSVALMAX)
      this.oPgFrm.Page1.oPag.oPSVALMAX_1_23.value=this.w_PSVALMAX
    endif
    if not(this.oPgFrm.Page1.oPag.oPSGGANTI_1_25.value==this.w_PSGGANTI)
      this.oPgFrm.Page1.oPag.oPSGGANTI_1_25.value=this.w_PSGGANTI
    endif
    if not(this.oPgFrm.Page1.oPag.oPS__AMMI_1_26.RadioValue()==this.w_PS__AMMI)
      this.oPgFrm.Page1.oPag.oPS__AMMI_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSVALMAA_1_27.value==this.w_PSVALMAA)
      this.oPgFrm.Page1.oPag.oPSVALMAA_1_27.value=this.w_PSVALMAA
    endif
    if not(this.oPgFrm.Page1.oPag.oPSGGANTA_1_28.value==this.w_PSGGANTA)
      this.oPgFrm.Page1.oPag.oPSGGANTA_1_28.value=this.w_PSGGANTA
    endif
    if not(this.oPgFrm.Page1.oPag.oPSMAXTEA_1_29.value==this.w_PSMAXTEA)
      this.oPgFrm.Page1.oPag.oPSMAXTEA_1_29.value=this.w_PSMAXTEA
    endif
    if not(this.oPgFrm.Page1.oPag.oPSMAXTEN_1_30.value==this.w_PSMAXTEN)
      this.oPgFrm.Page1.oPag.oPSMAXTEN_1_30.value=this.w_PSMAXTEN
    endif
    if not(this.oPgFrm.Page1.oPag.oPSBLCNMT_1_31.RadioValue()==this.w_PSBLCNMT)
      this.oPgFrm.Page1.oPag.oPSBLCNMT_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSULTACC_1_32.value==this.w_PSULTACC)
      this.oPgFrm.Page1.oPag.oPSULTACC_1_32.value=this.w_PSULTACC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSBLCSDA_1_33.RadioValue()==this.w_PSBLCSDA)
      this.oPgFrm.Page1.oPag.oPSBLCSDA_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPS_NONUM_1_34.RadioValue()==this.w_PS_NONUM)
      this.oPgFrm.Page1.oPag.oPS_NONUM_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_60.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_60.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE2_1_61.value==this.w_NOTE2)
      this.oPgFrm.Page1.oPag.oNOTE2_1_61.value=this.w_NOTE2
    endif
    cp_SetControlsValueExtFlds(this,'POL_SIC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_PSLUNMIN<=16)  and (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSLUNMIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La lunghezza massima di una password per ad hoc � 16 caratteri")
          case   not(.w_PSLUNMIA<=16)  and (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSLUNMIA_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La lunghezza massima di una password per ad hoc � 16 caratteri")
          case   not(.w_PSGGANTI <= .w_PSVALMAX)  and (.w_PSVALMAX>0 AND .w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSGGANTI_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("I giorni di avviso di scadenza devono essere minori o uguali dei giorni di validit� della password")
          case   not(.w_PSGGANTA <= .w_PSVALMAA)  and (.w_PSVALMAA>0 AND .w_PSATTIVO = 'S' And .w_PS_NONUM<>'W' AND .w_PS__AMMI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSGGANTA_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("I giorni di avviso di scadenza devono essere minori o uguali dei giorni di validit� della password")
          case   not(.w_PSMAXTEA > 0)  and (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W' AND .w_PS__AMMI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSMAXTEA_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un numero maggiore o uguale a 1")
          case   not(.w_PSMAXTEN > 0)  and (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSMAXTEN_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un numero maggiore o uguale a 1")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LIMCOMP = this.w_LIMCOMP
    this.o_LIMCOMA = this.w_LIMCOMA
    this.o_PSATTIVO = this.w_PSATTIVO
    this.o_PSCMPLES = this.w_PSCMPLES
    this.o_PSCMPLEA = this.w_PSCMPLEA
    this.o_PSVALMAX = this.w_PSVALMAX
    this.o_PS__AMMI = this.w_PS__AMMI
    this.o_PSVALMAA = this.w_PSVALMAA
    this.o_PSULTACC = this.w_PSULTACC
    return

enddefine

* --- Define pages as container
define class tgsut_apsPag1 as StdContainer
  Width  = 757
  height = 553
  stdWidth  = 757
  stdheight = 553
  resizeXpos=428
  resizeYpos=165
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPSATTIVO_1_8 as StdCombo with uid="WUWFLELIUS",rtseq=8,rtrep=.f.,left=203,top=9,width=106,height=21;
    , ToolTipText = "Politiche di sicurezza attivate o meno";
    , HelpContextID = 84878661;
    , cFormVar="w_PSATTIVO",RowSource=""+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPSATTIVO_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPSATTIVO_1_8.GetRadio()
    this.Parent.oContained.w_PSATTIVO = this.RadioValue()
    return .t.
  endfunc

  func oPSATTIVO_1_8.SetRadio()
    this.Parent.oContained.w_PSATTIVO=trim(this.Parent.oContained.w_PSATTIVO)
    this.value = ;
      iif(this.Parent.oContained.w_PSATTIVO=='S',1,;
      iif(this.Parent.oContained.w_PSATTIVO=='N',2,;
      0))
  endfunc

  add object oPS__JBSH_1_9 as StdCheck with uid="YLXIMIBXDI",rtseq=9,rtrep=.f.,left=310, top=9, caption="Escludi utenti schedulatore",;
    ToolTipText = "Se attivo le politiche di sicurezza non riguardano gli utenti schedulatore per la scadenza password",;
    HelpContextID = 226231614,;
    cFormVar="w_PS__JBSH", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPS__JBSH_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPS__JBSH_1_9.GetRadio()
    this.Parent.oContained.w_PS__JBSH = this.RadioValue()
    return .t.
  endfunc

  func oPS__JBSH_1_9.SetRadio()
    this.Parent.oContained.w_PS__JBSH=trim(this.Parent.oContained.w_PS__JBSH)
    this.value = ;
      iif(this.Parent.oContained.w_PS__JBSH=='S',1,;
      0)
  endfunc

  func oPS__JBSH_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S'  And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  add object oPSLUNMIN_1_10 as StdField with uid="EHXKITIMLL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PSLUNMIN", cQueryName = "PSLUNMIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La lunghezza massima di una password per ad hoc � 16 caratteri",;
    ToolTipText = "Lunghezza minima password",;
    HelpContextID = 122628796,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=140, Top=68, cSayPict='"99"', cGetPict='"99"'

  func oPSLUNMIN_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  func oPSLUNMIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSLUNMIN<=16)
    endwith
    return bRes
  endfunc

  add object oPS_NORIP_1_11 as StdCheck with uid="VTTYCMYHWU",rtseq=11,rtrep=.f.,left=204, top=68, caption="Password non ripetibili",;
    ToolTipText = "Se attivo richiede che la nuova password sia diversa dalla precedente",;
    HelpContextID = 38075066,;
    cFormVar="w_PS_NORIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPS_NORIP_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPS_NORIP_1_11.GetRadio()
    this.Parent.oContained.w_PS_NORIP = this.RadioValue()
    return .t.
  endfunc

  func oPS_NORIP_1_11.SetRadio()
    this.Parent.oContained.w_PS_NORIP=trim(this.Parent.oContained.w_PS_NORIP)
    this.value = ;
      iif(this.Parent.oContained.w_PS_NORIP=='S',1,;
      0)
  endfunc

  func oPS_NORIP_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc


  add object oPSCMPLES_1_13 as StdCombo with uid="PKGQEOYBMG",rtseq=12,rtrep=.f.,left=140,top=94,width=78,height=22;
    , ToolTipText = "Complessit� minima della password";
    , HelpContextID = 130565449;
    , cFormVar="w_PSCMPLES",RowSource=""+"Bassa,"+"Media,"+"Forte,"+"Ottima", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPSCMPLES_1_13.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    0)))))
  endfunc
  func oPSCMPLES_1_13.GetRadio()
    this.Parent.oContained.w_PSCMPLES = this.RadioValue()
    return .t.
  endfunc

  func oPSCMPLES_1_13.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PSCMPLES==1,1,;
      iif(this.Parent.oContained.w_PSCMPLES==2,2,;
      iif(this.Parent.oContained.w_PSCMPLES==3,3,;
      iif(this.Parent.oContained.w_PSCMPLES==4,4,;
      0))))
  endfunc

  func oPSCMPLES_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  add object oPSNUMPWD_1_15 as StdField with uid="EAXCEHGHKH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PSNUMPWD", cQueryName = "PSNUMPWD",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero password da mantenere",;
    HelpContextID = 195097914,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=163, Top=123

  func oPSNUMPWD_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  add object oPSLUNMIA_1_17 as StdField with uid="CUBPZWBZUV",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PSLUNMIA", cQueryName = "PSLUNMIA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La lunghezza massima di una password per ad hoc � 16 caratteri",;
    ToolTipText = "Lunghezza minima password per amministratori",;
    HelpContextID = 122628809,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=517, Top=68, cSayPict='"99"', cGetPict='"99"'

  func oPSLUNMIA_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  func oPSLUNMIA_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSLUNMIA<=16)
    endwith
    return bRes
  endfunc

  add object oPSNORIPA_1_18 as StdCheck with uid="RMYNBJUFEC",rtseq=15,rtrep=.f.,left=578, top=68, caption="Password non ripetibili",;
    ToolTipText = "Se attivo richiede che la nuova password sia diversa dalla precedente",;
    HelpContextID = 185928393,;
    cFormVar="w_PSNORIPA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPSNORIPA_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPSNORIPA_1_18.GetRadio()
    this.Parent.oContained.w_PSNORIPA = this.RadioValue()
    return .t.
  endfunc

  func oPSNORIPA_1_18.SetRadio()
    this.Parent.oContained.w_PSNORIPA=trim(this.Parent.oContained.w_PSNORIPA)
    this.value = ;
      iif(this.Parent.oContained.w_PSNORIPA=='S',1,;
      0)
  endfunc

  func oPSNORIPA_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc


  add object oPSCMPLEA_1_20 as StdCombo with uid="QKTJHSTTSF",rtseq=16,rtrep=.f.,left=517,top=94,width=78,height=22;
    , ToolTipText = "Complessit� minima della password per amministratori";
    , HelpContextID = 130565431;
    , cFormVar="w_PSCMPLEA",RowSource=""+"Bassa,"+"Media,"+"Forte,"+"Ottima", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPSCMPLEA_1_20.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    0)))))
  endfunc
  func oPSCMPLEA_1_20.GetRadio()
    this.Parent.oContained.w_PSCMPLEA = this.RadioValue()
    return .t.
  endfunc

  func oPSCMPLEA_1_20.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PSCMPLEA==1,1,;
      iif(this.Parent.oContained.w_PSCMPLEA==2,2,;
      iif(this.Parent.oContained.w_PSCMPLEA==3,3,;
      iif(this.Parent.oContained.w_PSCMPLEA==4,4,;
      0))))
  endfunc

  func oPSCMPLEA_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="HSVLYRLQFB",left=353, top=167, width=48,height=45,;
    CpPicture="\BMP\AVANZATE.ICO", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Informazioni sulla complessit� della password";
    , HelpContextID = 159990538;
    , Caption='\<Legenda';
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      do gsut_kcp with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPSBLCSDP_1_22 as StdCheck with uid="AUOVCTFIQN",rtseq=17,rtrep=.f.,left=54, top=263, caption="Blocco account alla scadenza",;
    ToolTipText = "Blocco account alla scadenza password",;
    HelpContextID = 234304838,;
    cFormVar="w_PSBLCSDP", bObbl = .f. , nPag = 1;
    , forecolor=RGB(255,0,0);
   , bGlobalFont=.t.


  func oPSBLCSDP_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPSBLCSDP_1_22.GetRadio()
    this.Parent.oContained.w_PSBLCSDP = this.RadioValue()
    return .t.
  endfunc

  func oPSBLCSDP_1_22.SetRadio()
    this.Parent.oContained.w_PSBLCSDP=trim(this.Parent.oContained.w_PSBLCSDP)
    this.value = ;
      iif(this.Parent.oContained.w_PSBLCSDP=='S',1,;
      0)
  endfunc

  func oPSBLCSDP_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSVALMAX>0 AND .w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  add object oPSVALMAX_1_23 as StdField with uid="VZJXXOGEYA",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PSVALMAX", cQueryName = "PSVALMAX",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero giorni validit� password",;
    HelpContextID = 142439758,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=111, Top=289, cSayPict='"999"', cGetPict='"999"'

  func oPSVALMAX_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  add object oPSGGANTI_1_25 as StdField with uid="ZRRBOCCPQL",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PSGGANTI", cQueryName = "PSGGANTI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "I giorni di avviso di scadenza devono essere minori o uguali dei giorni di validit� della password",;
    ToolTipText = "Attiva l'avviso di scadenza della password",;
    HelpContextID = 148014399,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=111, Top=314, cSayPict='"999"', cGetPict='"999"'

  func oPSGGANTI_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSVALMAX>0 AND .w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  func oPSGGANTI_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSGGANTI <= .w_PSVALMAX)
    endwith
    return bRes
  endfunc

  add object oPS__AMMI_1_26 as StdCheck with uid="ISQDHSWQNF",rtseq=20,rtrep=.f.,left=508, top=263, caption="Gestione alternativa per amministratore",;
    ToolTipText = "Se attivo, consente di definire giorni differenti di scadenza e avviso  scadenza password, controllo account per utenti amministartori.",;
    HelpContextID = 135527105,;
    cFormVar="w_PS__AMMI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPS__AMMI_1_26.RadioValue()
    return(iif(this.value =1,'N',;
    'S'))
  endfunc
  func oPS__AMMI_1_26.GetRadio()
    this.Parent.oContained.w_PS__AMMI = this.RadioValue()
    return .t.
  endfunc

  func oPS__AMMI_1_26.SetRadio()
    this.Parent.oContained.w_PS__AMMI=trim(this.Parent.oContained.w_PS__AMMI)
    this.value = ;
      iif(this.Parent.oContained.w_PS__AMMI=='N',1,;
      0)
  endfunc

  func oPS__AMMI_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S'  And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  add object oPSVALMAA_1_27 as StdField with uid="OZJXHLVJKE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PSVALMAA", cQueryName = "PSVALMAA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero giorni validit� password amministratore",;
    HelpContextID = 142439735,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=498, Top=287, cSayPict='"999"', cGetPict='"999"'

  func oPSVALMAA_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W' AND .w_PS__AMMI<>'S')
    endwith
   endif
  endfunc

  add object oPSGGANTA_1_28 as StdField with uid="UQFAUOGJLP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PSGGANTA", cQueryName = "PSGGANTA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "I giorni di avviso di scadenza devono essere minori o uguali dei giorni di validit� della password",;
    ToolTipText = "Attiva l'avviso di scadenza della password",;
    HelpContextID = 148014391,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=498, Top=312, cSayPict='"999"', cGetPict='"999"'

  func oPSGGANTA_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSVALMAA>0 AND .w_PSATTIVO = 'S' And .w_PS_NONUM<>'W' AND .w_PS__AMMI<>'S')
    endwith
   endif
  endfunc

  func oPSGGANTA_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSGGANTA <= .w_PSVALMAA)
    endwith
    return bRes
  endfunc

  add object oPSMAXTEA_1_29 as StdField with uid="NPGFXSHASR",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PSMAXTEA", cQueryName = "PSMAXTEA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un numero maggiore o uguale a 1",;
    ToolTipText = "Numero massimo di tentativi violazione password",;
    HelpContextID = 3990839,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=608, Top=347, cSayPict='"999"', cGetPict='"999"'

  func oPSMAXTEA_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W' AND .w_PS__AMMI<>'S')
    endwith
   endif
  endfunc

  func oPSMAXTEA_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSMAXTEA > 0)
    endwith
    return bRes
  endfunc

  add object oPSMAXTEN_1_30 as StdField with uid="AHGGJXZDNC",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PSMAXTEN", cQueryName = "PSMAXTEN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un numero maggiore o uguale a 1",;
    ToolTipText = "Numero massimo di tentativi violazione password",;
    HelpContextID = 3990852,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=203, Top=409, cSayPict='"999"', cGetPict='"999"'

  func oPSMAXTEN_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  func oPSMAXTEN_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSMAXTEN > 0)
    endwith
    return bRes
  endfunc

  add object oPSBLCNMT_1_31 as StdCheck with uid="JNKRTMXHAM",rtseq=25,rtrep=.f.,left=310, top=408, caption="Blocco violazione account",;
    ToolTipText = "Blocco account a seguito del raggiungimento del massimo numero tentativi password",;
    HelpContextID = 118016694,;
    cFormVar="w_PSBLCNMT", bObbl = .f. , nPag = 1;
    , forecolor=RGB(255,0,0);
   , bGlobalFont=.t.


  func oPSBLCNMT_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPSBLCNMT_1_31.GetRadio()
    this.Parent.oContained.w_PSBLCNMT = this.RadioValue()
    return .t.
  endfunc

  func oPSBLCNMT_1_31.SetRadio()
    this.Parent.oContained.w_PSBLCNMT=trim(this.Parent.oContained.w_PSBLCNMT)
    this.value = ;
      iif(this.Parent.oContained.w_PSBLCNMT=='S',1,;
      0)
  endfunc

  func oPSBLCNMT_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSMAXTEN>0 AND .w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  add object oPSULTACC_1_32 as StdField with uid="JNYRCNZIXD",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PSULTACC", cQueryName = "PSULTACC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indicare il numero di giorni dopo i quali gli account vengono considerati scaduti",;
    HelpContextID = 218654009,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=203, Top=438, cSayPict='"999"', cGetPict='"999"'

  func oPSULTACC_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc

  add object oPSBLCSDA_1_33 as StdCheck with uid="DEGMNXAXHG",rtseq=27,rtrep=.f.,left=310, top=438, caption="Blocco account non usati",;
    ToolTipText = "Se attivo: blocca automaticamente gli account scaduti",;
    HelpContextID = 234304823,;
    cFormVar="w_PSBLCSDA", bObbl = .f. , nPag = 1;
    , forecolor=RGB(255,0,0);
   , bGlobalFont=.t.


  func oPSBLCSDA_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPSBLCSDA_1_33.GetRadio()
    this.Parent.oContained.w_PSBLCSDA = this.RadioValue()
    return .t.
  endfunc

  func oPSBLCSDA_1_33.SetRadio()
    this.Parent.oContained.w_PSBLCSDA=trim(this.Parent.oContained.w_PSBLCSDA)
    this.value = ;
      iif(this.Parent.oContained.w_PSBLCSDA=='S',1,;
      0)
  endfunc

  func oPSBLCSDA_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSULTACC>0 AND .w_PSATTIVO = 'S' And .w_PS_NONUM<>'W')
    endwith
   endif
  endfunc


  add object oPS_NONUM_1_34 as StdCombo with uid="ZSYAAAMDWK",rtseq=28,rtrep=.f.,left=203,top=467,width=129,height=21;
    , ToolTipText = "Tipo di autenticazione: per codice, descrizione o tramite sistema operativo";
    , HelpContextID = 163251523;
    , cFormVar="w_PS_NONUM",RowSource=""+"Codice,"+"Login,"+"Sistema Operativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPS_NONUM_1_34.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'W',;
    space(1)))))
  endfunc
  func oPS_NONUM_1_34.GetRadio()
    this.Parent.oContained.w_PS_NONUM = this.RadioValue()
    return .t.
  endfunc

  func oPS_NONUM_1_34.SetRadio()
    this.Parent.oContained.w_PS_NONUM=trim(this.Parent.oContained.w_PS_NONUM)
    this.value = ;
      iif(this.Parent.oContained.w_PS_NONUM=='N',1,;
      iif(this.Parent.oContained.w_PS_NONUM=='S',2,;
      iif(this.Parent.oContained.w_PS_NONUM=='W',3,;
      0)))
  endfunc

  func oPS_NONUM_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSATTIVO = 'S')
    endwith
   endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="XKAZGVJAOE",left=644, top=497, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 159990538;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_36 as StdButton with uid="OFLKLPHFIY",left=698, top=497, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 159990538;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_50 as cp_runprogram with uid="BDKYIYXSOC",left=823, top=157, width=247,height=26,;
    caption='GSUT_BPS(A)',;
   bGlobalFont=.t.,;
    prg="GSUT_BPS('A')",;
    cEvent = "Update end,Insert start",;
    nPag=1;
    , HelpContextID = 20760519


  add object oObj_1_51 as cp_calclbl with uid="KWQXRJYQKC",left=167, top=313, width=181,height=33,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 18007014


  add object oObj_1_57 as cp_calclbl with uid="RDYTQXTABL",left=554, top=313, width=181,height=33,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 18007014

  add object oNOTE_1_60 as StdMemo with uid="AIRKLHEFVL",rtseq=29,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Livello di complessit� password minima",;
    HelpContextID = 155103018,;
   bGlobalFont=.t.,;
    Height=76, Width=256, Left=19, Top=153

  add object oNOTE2_1_61 as StdMemo with uid="CWRDHJOYHV",rtseq=30,rtrep=.f.,;
    cFormVar = "w_NOTE2", cQueryName = "NOTE2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Livello di complessit� password minima per amministratori",;
    HelpContextID = 102674218,;
   bGlobalFont=.t.,;
    Height=76, Width=256, Left=479, Top=153

  add object oStr_1_12 as StdString with uid="RSISEBFBID",Visible=.t., Left=6, Top=98,;
    Alignment=1, Width=133, Height=18,;
    Caption="Complessit� Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="VXXBBOIMWJ",Visible=.t., Left=3, Top=127,;
    Alignment=1, Width=158, Height=18,;
    Caption="Numero pwd da mantenere:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="BWGJCMIFJI",Visible=.t., Left=379, Top=72,;
    Alignment=1, Width=135, Height=18,;
    Caption="Lunghezza minima:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="QSLTQMSAEM",Visible=.t., Left=381, Top=98,;
    Alignment=1, Width=133, Height=18,;
    Caption="Complessit� Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="SFEQVJOFLM",Visible=.t., Left=6, Top=318,;
    Alignment=1, Width=104, Height=18,;
    Caption="Avviso scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="AVBQJTGOPQ",Visible=.t., Left=4, Top=72,;
    Alignment=1, Width=135, Height=18,;
    Caption="Lunghezza minima:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="AYTRNOLYHR",Visible=.t., Left=52, Top=291,;
    Alignment=1, Width=58, Height=18,;
    Caption="Scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="KQFHTPYUGD",Visible=.t., Left=19, Top=413,;
    Alignment=1, Width=183, Height=18,;
    Caption="Num.massimo di tentativi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="BWDGNXAMOT",Visible=.t., Left=8, Top=44,;
    Alignment=0, Width=216, Height=18,;
    Caption="Controllo cambio password"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="OTSUZTIKDT",Visible=.t., Left=8, Top=240,;
    Alignment=0, Width=216, Height=18,;
    Caption="Controllo scadenza password"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="FZFECQIRRH",Visible=.t., Left=16, Top=376,;
    Alignment=0, Width=216, Height=18,;
    Caption="Controllo/disattivazione account"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_43 as StdString with uid="BLHPBGYCCX",Visible=.t., Left=19, Top=440,;
    Alignment=1, Width=183, Height=18,;
    Caption="Scadenza account non usati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="PUQRXAFJFY",Visible=.t., Left=239, Top=440,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="OWLGFASFOE",Visible=.t., Left=147, Top=291,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="VUHDMLDRFW",Visible=.t., Left=81, Top=9,;
    Alignment=1, Width=121, Height=18,;
    Caption="Politica di sicurezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="WHIJSRGJGE",Visible=.t., Left=107, Top=467,;
    Alignment=1, Width=95, Height=18,;
    Caption="Autenticazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="LZPLWDYUCT",Visible=.t., Left=147, Top=318,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="BTSURSNKMZ",Visible=.t., Left=411, Top=289,;
    Alignment=1, Width=87, Height=18,;
    Caption="Scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="PUVMZVYEYC",Visible=.t., Left=398, Top=316,;
    Alignment=1, Width=100, Height=18,;
    Caption="Avviso scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="FBIZFJXLKP",Visible=.t., Left=383, Top=240,;
    Alignment=0, Width=338, Height=18,;
    Caption="Controllo scadenza password/account per amministratore"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="XEDCTPSSUS",Visible=.t., Left=534, Top=289,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="MPVMMUZKLC",Visible=.t., Left=534, Top=316,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="TJDWNYSRRF",Visible=.t., Left=404, Top=351,;
    Alignment=1, Width=202, Height=18,;
    Caption="Num. massimo di tentativi per amm.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="JMDKMIXBBZ",Visible=.t., Left=383, Top=44,;
    Alignment=0, Width=269, Height=18,;
    Caption="Controllo cambio password per amministratore"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_44 as StdBox with uid="BXQVMALFJJ",left=8, top=399, width=740,height=1

  add object oBox_1_63 as StdBox with uid="BHHFYEZCLK",left=8, top=261, width=740,height=1

  add object oBox_1_64 as StdBox with uid="XFMODUFWLG",left=8, top=64, width=740,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_aps','POL_SIC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PSSERIAL=POL_SIC.PSSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
