* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bdt                                                        *
*              Lancia tracciabilità                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-10                                                      *
* Last revis.: 2009-11-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pLancio
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bdt",oParentObject,m.pLancio)
return(i_retval)

define class tgsve_bdt as StdBatch
  * --- Local variables
  pLancio = space(1)
  w_TIPCAU = space(1)
  w_CAUDOC = space(5)
  w_CODDES = space(35)
  w_CODCLIFOR = space(15)
  w_NUMDO1 = 0
  w_ALFDO1 = space(2)
  w_DATDO1 = ctod("  /  /  ")
  w_NUMRI1 = 0
  w_ALFRI1 = space(2)
  w_DATRI1 = ctod("  /  /  ")
  w_DATREG1 = ctod("  /  /  ")
  w_DATREG2 = ctod("  /  /  ")
  w_MVRIFCLI = space(30)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_NUMRIF = 0
  w_ALFRIF = space(2)
  w_DATRIF = ctod("  /  /  ")
  w_TIPOCF = space(1)
  w_TIPO = space(1)
  w_SERIALE = space(10)
  w_CLADOC = space(2)
  w_MVSERIAL = space(10)
  w_VSRIF = space(2)
  w_FLVEAC = space(1)
  w_OBTEST = ctod("  /  /  ")
  w_DATOBSO = ctod("  /  /  ")
  w_CODCON = space(15)
  w_PLANCIO = space(1)
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Fido
    this.w_PROG = GSOR_STD()
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      Ah_ErrorMsg("Impossibile aprire la gestione del fido!",48,"")
      i_retcode = 'stop'
      return
    endif
    this.w_PROG.w_TIPCAU = this.oParentObject.w_MVFLVEAC
    this.w_PROG.w_CAUDOC = this.oParentObject.w_MVTIPDOC
    this.w_PROG.w_MVSERIAL = this.oParentObject.w_MVSERIAL
    this.w_PROG.w_CODCLIFOR = this.oParentObject.w_MVCODCON
    this.w_PROG.w_NUMDOC = this.oParentObject.w_MVNUMDOC
    this.w_PROG.w_ALFDOC = this.oParentObject.w_MVALFDOC
    this.w_PROG.w_DATDOC = this.oParentObject.w_MVDATDOC
    this.w_PROG.w_TIPOCF = this.oParentObject.w_MVTIPCON
    this.w_PROG.w_CLADOC = this.oParentObject.w_MVCLADOC
    this.w_PROG.w_NUMDO1 = this.oParentObject.w_MVNUMDOC
    this.w_PROG.w_ALFDO1 = this.oParentObject.w_MVALFDOC
    this.w_PROG.w_DATDO1 = this.oParentObject.w_MVDATDOC
    this.w_PROG.w_NUMRIF = this.oParentObject.w_MVNUMDOC
    this.w_PROG.w_ALFRIF = this.oParentObject.w_MVALFDOC
    this.w_PROG.w_DATRIF = this.oParentObject.w_MVDATDOC
    this.w_PROG.w_NUMRI1 = this.oParentObject.w_MVNUMDOC
    this.w_PROG.w_ALFRI1 = this.oParentObject.w_MVALFDOC
    this.w_PROG.w_DATRI1 = this.oParentObject.w_MVDATDOC
    this.w_PROG.mCalc(.T.)     
    this.w_PROG.NotifyEvent("EseguoZoom")     
    this.bUpdateParentObject = .F.
  endproc


  proc Init(oParentObject,pLancio)
    this.pLancio=pLancio
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pLancio"
endproc
