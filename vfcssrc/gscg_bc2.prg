* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bc2                                                        *
*              Ricalcola importi c.di costo 2                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_142]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2017-01-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bc2",oParentObject,m.pParam)
return(i_retval)

define class tgscg_bc2 as StdBatch
  * --- Local variables
  pParam = space(1)
  w_PADRE = .NULL.
  w_GPNT = .NULL.
  w_SEGNO = space(1)
  w_TOTALE = 0
  w_APPO1 = 0
  w_DIFF = 0
  w_TOTIMP = 0
  w_PARAME = 0
  w_SEGNO = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola importi sul Temporaneo (da GSCG_MCA):
    *       pParam='A' w_MR_SEGNO Changed,w_MRPARAME Changed,Row deleted
    *       pParam='C' w_MRTOTIMP Changed
    *       pParam='B' CheckResto
    this.w_PADRE = This.oParentObject
    if UPPER(This.oParentObject.class ) = "TGSCA_MMX"
      this.w_SEGNO = IIF(this.oParentObject.w_IMPDAR-this.oParentObject.w_IMPAVE>0 , "D", "A") 
    else
      this.w_GPNT = This.oParentObject.oParentobject
      * --- Cicla sul Temporaneo...
      this.w_SEGNO = IIF(this.w_GPNT.w_PNIMPDAR<>0,"D","A")
    endif
    this.w_PADRE.MarkPos()     
    this.w_PADRE.Exec_Select("_TmpRip_"," Sum( t_MRTOTIMP*IIF(t_MR_SEGNO='D',1,-1) ) As TotRig,  Sum(t_MRTOTIMP) As Totale,Sum( t_MRPARAME*IIF(t_MR_SEGNO="+cp_tostr( this.w_SEGNO) +",1,-1) ) As TotPar  ", " t_MRCODVOC<>SPACE(15)  AND t_MRCODICE<>SPACE(15) And not deleted()","","","")     
     
 Select("_TmpRip_") 
 Go Top
    * --- Rimuovo il cursore..
    do case
      case this.pParam="A"
        this.oParentObject.w_TOTPAR = Nvl( _TmpRip_.TotPar ,0 )
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParam="C"
        * --- Se variato l'importo
        this.oParentObject.w_TOTRIG = Nvl( _TmpRip_.Totrig ,0 )
        this.w_TOTALE = Nvl( _TmpRip_.Totale ,0 )
        this.oParentObject.w_SEGTOT = IIF(this.oParentObject.w_TOTRIG<0, "A","D")
        this.w_PADRE.firstrow()     
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          this.w_PADRE.Set("w_MRPARAME" , IIF(this.w_TOTALE=0, 0, cp_ROUND(this.w_PADRE.Get("w_MRTOTIMP")/this.w_TOTALE,4)))     
          this.w_PADRE.Set("w_PERCEN" , IIF(this.w_TOTALE=0, 0 , cp_ROUND((this.w_PADRE.Get("w_MRTOTIMP") * 100)/this.w_TOTALE,2)))     
          this.w_PADRE.Set("w_IMPRIG" , this.w_PADRE.Get("w_MRTOTIMP") * IIF( this.w_PADRE.Get("w_MR_SEGNO")="D", 1, -1))     
          this.w_PADRE.NextRow()     
        enddo
      case this.pParam="B"
        * --- Controllo che il totale righe corrisponda all'importo in primanota...
        this.oParentObject.w_TOTRIG = Nvl( _TmpRip_.Totrig ,0 )
        this.w_TOTALE = Nvl( _TmpRip_.Totale ,0 )
        if ABS(this.oParentObject.w_TOTQUA - IIF(this.oParentObject.w_SEGTOT="A",-1,1)*this.oParentObject.w_TOTRIG) = 0.01
          this.w_PADRE.firstrow()     
          this.w_PADRE.SetRow()     
          this.w_SEGNO = this.w_PADRE.Get("w_MR_SEGNO")
          this.w_TOTIMP = this.w_PADRE.Get( "w_MRTOTIMP" )
          this.w_DIFF = (this.oParentObject.w_TOTQUA - IIF(this.oParentObject.w_SEGTOT="A",-1,1)*this.oParentObject.w_TOTRIG)*IIF(this.w_SEGNO="A",-1,1)*IIF(this.oParentObject.w_SEGTOT="A",-1,1)
          this.w_TOTIMP = this.w_TOTIMP+this.w_DIFF
          this.oParentObject.w_TOTRIG = this.oParentObject.w_TOTRIG - this.w_DIFF
          this.w_PADRE.Set("w_MRTOTIMP" , this.w_TOTIMP)     
          this.w_PADRE.Set("w_IMPRIG" , this.w_TOTIMP*IIF(this.w_SEGNO="D",1,-1))     
        endif
      case this.pParam="D"
        if ah_YesNo("Si vogliono ricalcolare gli importi delle righe rimanenti?")
          this.oParentObject.w_TOTPAR = Nvl( _TmpRip_.TotPar ,0 )
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
    Use in "_TmpRip_"
    this.w_PADRE.RePos()     
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se variata la sezione o il parametro o cancello una riga
    this.oParentObject.w_SEGTOT = this.w_SEGNO
    this.oParentObject.w_TOTRIG = 0
    this.w_PADRE.firstrow()     
    do while Not this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      this.w_SEGNO = this.w_PADRE.Get("w_MR_SEGNO")
      this.w_PARAME = this.w_PADRE.Get("w_MRPARAME")
      this.w_TOTIMP = cp_ROUND(this.oParentObject.w_TOTQUA/IIF(this.oParentObject.w_TOTPAR=0,1,this.oParentObject.w_TOTPAR)*this.w_PARAME, IIF(UPPER(This.oParentObject.class ) = "TGSCA_MMX" , this.w_PADRE.w_DECTOP ,this.w_PADRE.oParentObject.w_DECTOP))*SIGN(this.w_PARAME)
      this.w_PADRE.Set("w_MRTOTIMP" , this.w_TOTIMP)     
      this.w_PADRE.Set("w_PERCEN" , ABS(cp_ROUND((this.w_TOTIMP * 100)/IIF(this.w_TOTALE=0, this.oParentObject.w_TOTQUA , this.w_TOTALE),IIF(UPPER(This.oParentObject.class ) = "TGSCA_MMX" , this.w_PADRE.w_DECTOP ,this.w_PADRE.oParentObject.w_DECTOP))))     
      this.w_PADRE.Set("w_IMPRIG" , cp_ROUND(this.w_TOTIMP * IIF(this.w_SEGNO="D", 1, -1), IIF(UPPER(This.oParentObject.class ) = "TGSCA_MMX" , this.w_PADRE.w_DECTOP ,this.w_PADRE.oParentObject.w_DECTOP)))     
      this.oParentObject.w_TOTRIG = this.oParentObject.w_TOTRIG + ( this.w_TOTIMP * IIF( this.w_SEGNO="D",1,-1) )
      this.w_PADRE.NextRow()     
    enddo
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
