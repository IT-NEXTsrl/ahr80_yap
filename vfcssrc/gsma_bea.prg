* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bea                                                        *
*              Elaborazione analisi ABC                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_140]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-28                                                      *
* Last revis.: 2014-05-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bea",oParentObject)
return(i_retval)

define class tgsma_bea as StdBatch
  * --- Local variables
  w_STAMPO = .f.
  w_TOTALE = 0
  w_MAGAZZ = space(5)
  w_ARTICOLO = space(20)
  w_CLASSE = space(1)
  w_PERCENTU = 0
  w_DATA = ctod("  /  /  ")
  w_AGGI = .f.
  w_OP = space(1)
  w_DISCRCLA = 0
  w_OLDCATE = space(5)
  * --- WorkFile variables
  INVENTAR_idx=0
  MAGAZZIN_idx=0
  PAR_RIOR_idx=0
  PAR_RIMA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ELABORAZIONE ANALISI ABC (da GSMA_BEA)
    * --- Elaborazione per categoria omogenea 'C' o per singolo articolo 'A'
    * --- Percentuale classe C
    * --- Variabili per l'aggiornamento dati articolo magazzino
    * --- Variabili per il report
    L_INV=this.oParentObject.w_INV
    L_ESE=this.oParentObject.w_ESE
    L_FLESCSZ=this.oParentObject.w_FLESCSZ
    do case
      case this.oParentObject.w_VALORIZ="A"
        * --- Valorizzazione per costo medio ponderato annuo
        COSTO="DICOSMPA"
        L_VALORIZ=ah_msgformat("Costo medio ponderato annuo")
      case this.oParentObject.w_VALORIZ="B"
        * --- Valorizzazione per costo ultimo
        COSTO="DICOSULT"
        L_VALORIZ=ah_msgformat("Costo ultimo")
      case this.oParentObject.w_VALORIZ="C"
        * --- Valorizzazione per costo standard
        COSTO="nvl(PRCOSSTA,0)"
        L_VALORIZ=ah_msgformat("Costo standard")
    endcase
    vq_exec("QUERY\GSMA_SEA", this, "RESULTSET")
    if RECCOUNT("RESULTSET")<>0
      create cursor RESULT2 (DICODART C (20), COSTART N (18,5),;
      PERCENTUAL N (18,5), CLASSE C(1), ARCATOMO C (5), ARDESART C (40))
      * --- Valorizzazione dei costi
      select DICODART, cp_ROUND(&COSTO*DIQTAESI,5) AS COSTART,;
      cp_ROUND(0*&COSTO*DIQTAESI,5) as PERCENTUAL , "C" as CLASSE, ARCATOMO,ARDESART;
      from RESULTSET into cursor RESULT2
      * --- Valore totale
      select sum(COSTART) from RESULT2 into array TOT
      this.w_TOTALE = TOT
      * --- Calcola la percentuale di ogni articolo
      =wrcursor("RESULT2")
      NEWPERC=cp_ROUND(0,5)
      select RESULT2
      go top
      scan
      NEWPERC=iif(this.w_TOTALE=0,0,cp_ROUND(COSTART/ABS(this.w_TOTALE)*100,2))
      replace PERCENTUAL with NEWPERC
      EndScan
      if this.oParentObject.w_ELAB="A"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Chiude i cursori
    else
      ah_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare")
    endif
    if used ("RESULT2")
      select RESULT2
      use
    endif
    if used ("RESULT")
      select RESULT
      use
    endif
    if used ("__TMP__")
      select __TMP__
      use
    endif
    if used ("RESULT3")
      select RESULT3
      use
    endif
    if used ("AGGIORNA")
      select AGGIORNA
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    select CLASSE, DICODART, COSTART, PERCENTUAL;
    from RESULT2 into cursor __TMP__;
    order by PERCENTUAL desc, DICODART
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    cp_chprn("QUERY\GSMA_SEA.FRX", " ", this)
    * --- Se si utilizza l'editor di report di Visual Fox Pro il cursore __TMP__
    * --- viene chiuso e la procedura va in errore
    if used ("__TMP__") and reccount("__TMP__")>0
      * --- utilizzato per l'aggionamento di PAR_RIOR e PAR_RIMA
      select DICODART as ARTICOLO, ;
      CLASSE, PERCENTUAL as PERCENTU from __TMP__;
      into cursor AGGIORNA
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_STAMPO = .t.
    * --- Seleziona le categorie e i relativi totali
    select ARCATOMO as CATEOMOG, sum(PERCENTUAL) as PERCCATE,;
    sum(COSTART) as COSTCAT;
    from RESULT2 group by CATEOMOG into cursor RESULT3 NOFILTER
    * --- Imposta il cusore __TMP__ per la stampa
    select "C" as CLACAT, DICODART, COSTART, PERCENTUAL, ARCATOMO,ARDESART,;
    PERCCATE, COSTCAT;
    from RESULT2 left join RESULT3 on ARCATOMO=CATEOMOG;
    into cursor __TMP__ order by PERCCATE desc, ARCATOMO desc, DICODART
    * --- Calcola la classe della categoria. Controlla che ogni articolo sia
    * --- provvisto di categoria, altrimenti interrompe l'esecuzione.
    =wrcursor("__TMP__")
    this.w_OLDCATE = ""
    this.w_CLASSE = "A"
    this.w_DISCRCLA = 0
    select __TMP__
    go top
    scan
    if not empty(nvl(ARCATOMO,""))
      * --- Ci sono ancora categorie di classe A o B
      if this.w_DISCRCLA<=this.oParentObject.w_A+this.oParentObject.w_B
        replace CLACAT WITH this.w_CLASSE
        * --- Se la categoria cambia, sommo la nuova percentuale
        if this.w_OLDCATE<>nvl(ARCATOMO,"")
          this.w_DISCRCLA=this.w_DISCRCLA+PERCENTUAL
        endif
        if this.w_DISCRCLA>this.oParentObject.w_A
          this.w_CLASSE = "B"
        endif
      else
        exit
      endif
    else
      ah_ErrorMsg("Esistono articoli sprovvisti di categoria%0L'elaborazione � stata interrotta")
      this.w_STAMPO = .f.
      exit
    endif
    EndScan
    if this.w_STAMPO
      cp_chprn("QUERY\GSMA1SEA.FRX", " ", this)
      * --- Se si utilizza l'editor di report di Visual Fox Pro il cursore __TMP__
      * --- viene chiuso e la procedura va in errore
      if used ("__TMP__") and reccount("__TMP__")>0
        * --- utilizzato per l'aggionamento di PAR_RIOR e PAR_RIMA
        select DICODART as ARTICOLO, ;
        CLACAT as CLASSE, PERCCATE as PERCENTU from __TMP__;
        into cursor AGGIORNA
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna PAR_RIOR e PAR_RIMA
    this.w_AGGI = ah_YesNo("Si desidera aggiornare i dati articolo/magazzini?")
    this.w_OP = "="
    if this.w_AGGI
      * --- Read from INVENTAR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INVENTAR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "INCODMAG"+;
          " from "+i_cTable+" INVENTAR where ";
              +"INNUMINV = "+cp_ToStrODBC(this.oParentObject.w_INV);
              +" and INCODESE = "+cp_ToStrODBC(this.oParentObject.w_ESE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          INCODMAG;
          from (i_cTable) where;
              INNUMINV = this.oParentObject.w_INV;
              and INCODESE = this.oParentObject.w_ESE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MAGAZZ = NVL(cp_ToDate(_read_.INCODMAG),cp_NullValue(_read_.INCODMAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DATA = i_datsys
      this.w_MAGAZZ = alltrim(this.w_MAGAZZ)
      * --- Try
      local bErr_038207E8
      bErr_038207E8=bTrsErr
      this.Try_038207E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Errore durante l'aggiornamento%0Operazione annullata")
      endif
      bTrsErr=bTrsErr or bErr_038207E8
      * --- End
    endif
  endproc
  proc Try_038207E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    select AGGIORNA
    go top
    scan
    this.w_CLASSE = alltrim(CLASSE)
    this.w_ARTICOLO = alltrim(ARTICOLO)
    this.w_PERCENTU = cp_round(PERCENTU,5)
    * --- Try
    local bErr_03D72320
    bErr_03D72320=bTrsErr
    this.Try_03D72320()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03D72320
    * --- End
    * --- Try
    local bErr_03A6E290
    bErr_03A6E290=bTrsErr
    this.Try_03A6E290()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into PAR_RIMA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_OP,'PRDATABC','this.w_DATA',this.w_DATA,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_OP,'PRCLAABC','this.w_CLASSE',this.w_CLASSE,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_OP,'PRPERABC','this.w_PERCENTU',this.w_PERCENTU,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIMA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PRDATABC ="+cp_NullLink(i_cOp1,'PAR_RIMA','PRDATABC');
        +",PRCLAABC ="+cp_NullLink(i_cOp2,'PAR_RIMA','PRCLAABC');
        +",PRPERABC ="+cp_NullLink(i_cOp3,'PAR_RIMA','PRPERABC');
            +i_ccchkf ;
        +" where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_ARTICOLO);
            +" and PRCODMAG = "+cp_ToStrODBC(this.w_MAGAZZ);
               )
      else
        update (i_cTable) set;
            PRDATABC = &i_cOp1.;
            ,PRCLAABC = &i_cOp2.;
            ,PRPERABC = &i_cOp3.;
            &i_ccchkf. ;
         where;
            PRCODART = this.w_ARTICOLO;
            and PRCODMAG = this.w_MAGAZZ;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_03A6E290
    * --- End
    EndScan
    * --- commit
    cp_EndTrs(.t.)
    ah_Msg("Aggiornamento terminato")
    return
  proc Try_03D72320()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_RIOR
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ARTICOLO),'PAR_RIOR','PRCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_ARTICOLO)
      insert into (i_cTable) (PRCODART &i_ccchkf. );
         values (;
           this.w_ARTICOLO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03A6E290()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_RIMA
    i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIMA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODART"+",PRCODMAG"+",PRPERABC"+",PRDATABC"+",PRCLAABC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ARTICOLO),'PAR_RIMA','PRCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGAZZ),'PAR_RIMA','PRCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERCENTU),'PAR_RIMA','PRPERABC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATA),'PAR_RIMA','PRDATABC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CLASSE),'PAR_RIMA','PRCLAABC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_ARTICOLO,'PRCODMAG',this.w_MAGAZZ,'PRPERABC',this.w_PERCENTU,'PRDATABC',this.w_DATA,'PRCLAABC',this.w_CLASSE)
      insert into (i_cTable) (PRCODART,PRCODMAG,PRPERABC,PRDATABC,PRCLAABC &i_ccchkf. );
         values (;
           this.w_ARTICOLO;
           ,this.w_MAGAZZ;
           ,this.w_PERCENTU;
           ,this.w_DATA;
           ,this.w_CLASSE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegna le classi agli articoli
    =wrcursor("__TMP__")
    this.w_OLDCATE = ""
    this.w_CLASSE = "A"
    this.w_DISCRCLA = 0
    select __TMP__
    go top
    scan
    * --- Ci sono ancora articoli di classe A o B
    if this.w_DISCRCLA<=this.oParentObject.w_A+this.oParentObject.w_B
      replace CLASSE WITH this.w_CLASSE
      * --- Sommo la nuova percentuale
      this.w_DISCRCLA=this.w_DISCRCLA+PERCENTUAL
      if this.w_DISCRCLA>this.oParentObject.w_A
        this.w_CLASSE = "B"
      endif
    else
      exit
    endif
    EndScan
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='INVENTAR'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='PAR_RIOR'
    this.cWorkTables[4]='PAR_RIMA'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
