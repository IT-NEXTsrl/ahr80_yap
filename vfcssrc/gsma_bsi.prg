* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bsi                                                        *
*              Elabora saldo iniziale                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2000-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODMAG,w_CODART,w_DATASTAM,w_SELPRO,w_ESISATTU
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bsi",oParentObject,m.w_CODMAG,m.w_CODART,m.w_DATASTAM,m.w_SELPRO,m.w_ESISATTU)
return(i_retval)

define class tgsma_bsi as StdBatch
  * --- Local variables
  w_CODMAG = space(5)
  w_CODART = space(20)
  w_DATASTAM = ctod("  /  /  ")
  w_SELPRO = space(1)
  w_ESISATTU = 0
  w_Saldo = space(10)
  w_SaveArea = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo saldo iniziale per stampa schede magazzino
    * --- Parametri
    * --- Variabili locali
    this.w_Saldo = 0
    this.w_SaveArea = alias()
    * --- Creazione cursore stampa
    vq_exec("query\GSMA4SCH",this,"SaldoIni")
    * --- Calcolo saldo iniziale
    if reccount("SaldoIni")<>0
      Select sum( iif(this.w_CodMag=mmcodmag, EsiCarPri-EsiScaPri, EsiCarPri*0) ) as EsisPrin ;
      from SaldoIni ;
      into cursor SaldoIni
      Select SaldoIni
      go top
      this.w_Saldo = EsisPrin
    endif
    this.w_Saldo = this.w_EsisAttu - this.w_Saldo
    * --- Chiusura cursori
    if used("SaldoIni")
      select SaldoIni
      use
    endif
    * --- Ripristino area di lavoro
    if .not. empty( this.w_SaveArea )
      select ( this.w_SaveArea )
    endif
    * --- Ritorna il saldo iniziale
    i_retcode = 'stop'
    i_retval = this.w_Saldo
    return
  endproc


  proc Init(oParentObject,w_CODMAG,w_CODART,w_DATASTAM,w_SELPRO,w_ESISATTU)
    this.w_CODMAG=w_CODMAG
    this.w_CODART=w_CODART
    this.w_DATASTAM=w_DATASTAM
    this.w_SELPRO=w_SELPRO
    this.w_ESISATTU=w_ESISATTU
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODMAG,w_CODART,w_DATASTAM,w_SELPRO,w_ESISATTU"
endproc
