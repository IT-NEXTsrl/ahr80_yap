* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1kxp                                                        *
*              WE - parametri inoltro a net folder                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_58]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-13                                                      *
* Last revis.: 2007-07-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut1kxp",oParentObject))

* --- Class definition
define class tgsut1kxp as StdForm
  Top    = 7
  Left   = 15

  * --- Standard Properties
  Width  = 556
  Height = 362
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-06"
  HelpContextID=250133655
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  WECONTI_IDX = 0
  cPrg = "gsut1kxp"
  cComment = "WE - parametri inoltro a net folder"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_WETIPCLF = space(1)
  o_WETIPCLF = space(1)
  w_WECODCLF = space(15)
  o_WECODCLF = space(15)
  w_WECODCLF = space(15)
  w_WEACCATT = space(1)
  w_RAGSOC = space(40)
  w_WEPERMESSI = space(1)
  w_WETITDOC = space(60)
  o_WETITDOC = space(60)
  w_WEDESDOC = space(0)
  o_WEDESDOC = space(0)
  w_WEAVVEML = space(1)
  w_WEINDEML = space(50)
  w_WETXTEML = space(0)
  o_WETXTEML = space(0)
  w_WEAVVSMS = space(1)
  w_WENUMSMS = space(20)
  w_WETXTSMS = space(0)
  o_WETXTSMS = space(0)
  w_WEAVVFAX = space(1)
  w_WENUMFAX = space(20)
  w_WEDESCSM = space(1)
  w_WE_LOGIN = space(35)
  w_MASKREJECTED = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut1kxpPag1","gsut1kxp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oWETIPCLF_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='WECONTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_WETIPCLF=space(1)
      .w_WECODCLF=space(15)
      .w_WECODCLF=space(15)
      .w_WEACCATT=space(1)
      .w_RAGSOC=space(40)
      .w_WEPERMESSI=space(1)
      .w_WETITDOC=space(60)
      .w_WEDESDOC=space(0)
      .w_WEAVVEML=space(1)
      .w_WEINDEML=space(50)
      .w_WETXTEML=space(0)
      .w_WEAVVSMS=space(1)
      .w_WENUMSMS=space(20)
      .w_WETXTSMS=space(0)
      .w_WEAVVFAX=space(1)
      .w_WENUMFAX=space(20)
      .w_WEDESCSM=space(1)
      .w_WE_LOGIN=space(35)
      .w_MASKREJECTED=.f.
      .w_WETIPCLF=oParentObject.w_WETIPCLF
      .w_WECODCLF=oParentObject.w_WECODCLF
      .w_WECODCLF=oParentObject.w_WECODCLF
      .w_WEACCATT=oParentObject.w_WEACCATT
      .w_WEPERMESSI=oParentObject.w_WEPERMESSI
      .w_WETITDOC=oParentObject.w_WETITDOC
      .w_WEDESDOC=oParentObject.w_WEDESDOC
      .w_WEAVVEML=oParentObject.w_WEAVVEML
      .w_WEINDEML=oParentObject.w_WEINDEML
      .w_WETXTEML=oParentObject.w_WETXTEML
      .w_WEAVVSMS=oParentObject.w_WEAVVSMS
      .w_WENUMSMS=oParentObject.w_WENUMSMS
      .w_WETXTSMS=oParentObject.w_WETXTSMS
      .w_WEAVVFAX=oParentObject.w_WEAVVFAX
      .w_WENUMFAX=oParentObject.w_WENUMFAX
      .w_WEDESCSM=oParentObject.w_WEDESCSM
      .w_WE_LOGIN=oParentObject.w_WE_LOGIN
      .w_MASKREJECTED=oParentObject.w_MASKREJECTED
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_WECODCLF))
          .link_1_3('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_WECODCLF))
          .link_1_4('Full')
        endif
          .DoRTCalc(4,18,.f.)
        .w_MASKREJECTED = .F.
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate(not empty(.w_WEINDEML))
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate(not empty(.w_WENUMSMS))
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate(not empty(.w_WENUMFAX))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_WETIPCLF=.w_WETIPCLF
      .oParentObject.w_WECODCLF=.w_WECODCLF
      .oParentObject.w_WECODCLF=.w_WECODCLF
      .oParentObject.w_WEACCATT=.w_WEACCATT
      .oParentObject.w_WEPERMESSI=.w_WEPERMESSI
      .oParentObject.w_WETITDOC=.w_WETITDOC
      .oParentObject.w_WEDESDOC=.w_WEDESDOC
      .oParentObject.w_WEAVVEML=.w_WEAVVEML
      .oParentObject.w_WEINDEML=.w_WEINDEML
      .oParentObject.w_WETXTEML=.w_WETXTEML
      .oParentObject.w_WEAVVSMS=.w_WEAVVSMS
      .oParentObject.w_WENUMSMS=.w_WENUMSMS
      .oParentObject.w_WETXTSMS=.w_WETXTSMS
      .oParentObject.w_WEAVVFAX=.w_WEAVVFAX
      .oParentObject.w_WENUMFAX=.w_WENUMFAX
      .oParentObject.w_WEDESCSM=.w_WEDESCSM
      .oParentObject.w_WE_LOGIN=.w_WE_LOGIN
      .oParentObject.w_MASKREJECTED=.w_MASKREJECTED
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_WETIPCLF<>.w_WETIPCLF.or. .o_WECODCLF<>.w_WECODCLF
          .link_1_3('Full')
        endif
          .link_1_4('Full')
        .DoRTCalc(4,6,.t.)
        if .o_WETITDOC<>.w_WETITDOC
            .w_WETITDOC = WEVERTXT(.w_WETITDOC,.T.)
        endif
        if .o_WEDESDOC<>.w_WEDESDOC
            .w_WEDESDOC = WEVERTXT(.w_WEDESDOC,.T.)
        endif
        .DoRTCalc(9,10,.t.)
        if .o_WETXTEML<>.w_WETXTEML
            .w_WETXTEML = WEVERTXT(.w_WETXTEML,.T.)
        endif
        .DoRTCalc(12,13,.t.)
        if .o_WETXTSMS<>.w_WETXTSMS
            .w_WETXTSMS = WEVERTXT(.w_WETXTSMS,.T.)
        endif
        .DoRTCalc(15,18,.t.)
            .w_MASKREJECTED = .F.
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(not empty(.w_WEINDEML))
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(not empty(.w_WENUMSMS))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(not empty(.w_WENUMFAX))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(not empty(.w_WEINDEML))
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(not empty(.w_WENUMSMS))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(not empty(.w_WENUMFAX))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oWEPERMESSI_1_7.enabled = this.oPgFrm.Page1.oPag.oWEPERMESSI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oWETITDOC_1_8.enabled = this.oPgFrm.Page1.oPag.oWETITDOC_1_8.mCond()
    this.oPgFrm.Page1.oPag.oWEDESDOC_1_9.enabled = this.oPgFrm.Page1.oPag.oWEDESDOC_1_9.mCond()
    this.oPgFrm.Page1.oPag.oWEAVVEML_1_11.enabled = this.oPgFrm.Page1.oPag.oWEAVVEML_1_11.mCond()
    this.oPgFrm.Page1.oPag.oWETXTEML_1_13.enabled = this.oPgFrm.Page1.oPag.oWETXTEML_1_13.mCond()
    this.oPgFrm.Page1.oPag.oWEAVVSMS_1_14.enabled = this.oPgFrm.Page1.oPag.oWEAVVSMS_1_14.mCond()
    this.oPgFrm.Page1.oPag.oWETXTSMS_1_16.enabled = this.oPgFrm.Page1.oPag.oWETXTSMS_1_16.mCond()
    this.oPgFrm.Page1.oPag.oWEAVVFAX_1_17.enabled = this.oPgFrm.Page1.oPag.oWEAVVFAX_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=WECODCLF
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.WECONTI_IDX,3]
    i_lTable = "WECONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.WECONTI_IDX,2], .t., this.WECONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.WECONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_WECODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'WECONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" WECODICE like "+cp_ToStrODBC(trim(this.w_WECODCLF)+"%");
                   +" and WETIPCON="+cp_ToStrODBC(this.w_WETIPCLF);

          i_ret=cp_SQL(i_nConn,"select WETIPCON,WECODICE,WEAVVEML,WEINDEML,WETXTEML,WEAVVSMS,WENUMSMS,WETXTSMS,WEAVVFAX,WENUMFAX,WEDESCSM,WEACCATT,WE_LOGIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by WETIPCON,WECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'WETIPCON',this.w_WETIPCLF;
                     ,'WECODICE',trim(this.w_WECODCLF))
          select WETIPCON,WECODICE,WEAVVEML,WEINDEML,WETXTEML,WEAVVSMS,WENUMSMS,WETXTSMS,WEAVVFAX,WENUMFAX,WEDESCSM,WEACCATT,WE_LOGIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by WETIPCON,WECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_WECODCLF)==trim(_Link_.WECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_WECODCLF) and !this.bDontReportError
            deferred_cp_zoom('WECONTI','*','WETIPCON,WECODICE',cp_AbsName(oSource.parent,'oWECODCLF_1_3'),i_cWhere,'',"Account WE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_WETIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select WETIPCON,WECODICE,WEAVVEML,WEINDEML,WETXTEML,WEAVVSMS,WENUMSMS,WETXTSMS,WEAVVFAX,WENUMFAX,WEDESCSM,WEACCATT,WE_LOGIN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select WETIPCON,WECODICE,WEAVVEML,WEINDEML,WETXTEML,WEAVVSMS,WENUMSMS,WETXTSMS,WEAVVFAX,WENUMFAX,WEDESCSM,WEACCATT,WE_LOGIN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Account WE non attivo o non specificato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select WETIPCON,WECODICE,WEAVVEML,WEINDEML,WETXTEML,WEAVVSMS,WENUMSMS,WETXTSMS,WEAVVFAX,WENUMFAX,WEDESCSM,WEACCATT,WE_LOGIN";
                     +" from "+i_cTable+" "+i_lTable+" where WECODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and WETIPCON="+cp_ToStrODBC(this.w_WETIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'WETIPCON',oSource.xKey(1);
                       ,'WECODICE',oSource.xKey(2))
            select WETIPCON,WECODICE,WEAVVEML,WEINDEML,WETXTEML,WEAVVSMS,WENUMSMS,WETXTSMS,WEAVVFAX,WENUMFAX,WEDESCSM,WEACCATT,WE_LOGIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_WECODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select WETIPCON,WECODICE,WEAVVEML,WEINDEML,WETXTEML,WEAVVSMS,WENUMSMS,WETXTSMS,WEAVVFAX,WENUMFAX,WEDESCSM,WEACCATT,WE_LOGIN";
                   +" from "+i_cTable+" "+i_lTable+" where WECODICE="+cp_ToStrODBC(this.w_WECODCLF);
                   +" and WETIPCON="+cp_ToStrODBC(this.w_WETIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'WETIPCON',this.w_WETIPCLF;
                       ,'WECODICE',this.w_WECODCLF)
            select WETIPCON,WECODICE,WEAVVEML,WEINDEML,WETXTEML,WEAVVSMS,WENUMSMS,WETXTSMS,WEAVVFAX,WENUMFAX,WEDESCSM,WEACCATT,WE_LOGIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_WECODCLF = NVL(_Link_.WECODICE,space(15))
      this.w_WEAVVEML = NVL(_Link_.WEAVVEML,space(1))
      this.w_WEINDEML = NVL(_Link_.WEINDEML,space(50))
      this.w_WETXTEML = NVL(_Link_.WETXTEML,space(0))
      this.w_WEAVVSMS = NVL(_Link_.WEAVVSMS,space(1))
      this.w_WENUMSMS = NVL(_Link_.WENUMSMS,space(20))
      this.w_WETXTSMS = NVL(_Link_.WETXTSMS,space(0))
      this.w_WEAVVFAX = NVL(_Link_.WEAVVFAX,space(1))
      this.w_WENUMFAX = NVL(_Link_.WENUMFAX,space(20))
      this.w_WEDESCSM = NVL(_Link_.WEDESCSM,space(1))
      this.w_WEACCATT = NVL(_Link_.WEACCATT,space(1))
      this.w_WE_LOGIN = NVL(_Link_.WE_LOGIN,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_WECODCLF = space(15)
      endif
      this.w_WEAVVEML = space(1)
      this.w_WEINDEML = space(50)
      this.w_WETXTEML = space(0)
      this.w_WEAVVSMS = space(1)
      this.w_WENUMSMS = space(20)
      this.w_WETXTSMS = space(0)
      this.w_WEAVVFAX = space(1)
      this.w_WENUMFAX = space(20)
      this.w_WEDESCSM = space(1)
      this.w_WEACCATT = space(1)
      this.w_WE_LOGIN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_WEACCATT='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Account WE non attivo o non specificato")
        endif
        this.w_WECODCLF = space(15)
        this.w_WEAVVEML = space(1)
        this.w_WEINDEML = space(50)
        this.w_WETXTEML = space(0)
        this.w_WEAVVSMS = space(1)
        this.w_WENUMSMS = space(20)
        this.w_WETXTSMS = space(0)
        this.w_WEAVVFAX = space(1)
        this.w_WENUMFAX = space(20)
        this.w_WEDESCSM = space(1)
        this.w_WEACCATT = space(1)
        this.w_WE_LOGIN = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.WECONTI_IDX,2])+'\'+cp_ToStr(_Link_.WETIPCON,1)+'\'+cp_ToStr(_Link_.WECODICE,1)
      cp_ShowWarn(i_cKey,this.WECONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_WECODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=WECODCLF
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_WECODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_WECODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_WECODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_WETIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_WETIPCLF;
                       ,'ANCODICE',this.w_WECODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_WECODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_RAGSOC = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_WECODCLF = space(15)
      endif
      this.w_RAGSOC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_WEACCATT='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Account WE non attivo o non specificato")
        endif
        this.w_WECODCLF = space(15)
        this.w_RAGSOC = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_WECODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oWETIPCLF_1_1.RadioValue()==this.w_WETIPCLF)
      this.oPgFrm.Page1.oPag.oWETIPCLF_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oWECODCLF_1_3.value==this.w_WECODCLF)
      this.oPgFrm.Page1.oPag.oWECODCLF_1_3.value=this.w_WECODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOC_1_6.value==this.w_RAGSOC)
      this.oPgFrm.Page1.oPag.oRAGSOC_1_6.value=this.w_RAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oWEPERMESSI_1_7.RadioValue()==this.w_WEPERMESSI)
      this.oPgFrm.Page1.oPag.oWEPERMESSI_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oWETITDOC_1_8.value==this.w_WETITDOC)
      this.oPgFrm.Page1.oPag.oWETITDOC_1_8.value=this.w_WETITDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oWEDESDOC_1_9.value==this.w_WEDESDOC)
      this.oPgFrm.Page1.oPag.oWEDESDOC_1_9.value=this.w_WEDESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oWEAVVEML_1_11.RadioValue()==this.w_WEAVVEML)
      this.oPgFrm.Page1.oPag.oWEAVVEML_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oWETXTEML_1_13.value==this.w_WETXTEML)
      this.oPgFrm.Page1.oPag.oWETXTEML_1_13.value=this.w_WETXTEML
    endif
    if not(this.oPgFrm.Page1.oPag.oWEAVVSMS_1_14.RadioValue()==this.w_WEAVVSMS)
      this.oPgFrm.Page1.oPag.oWEAVVSMS_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oWETXTSMS_1_16.value==this.w_WETXTSMS)
      this.oPgFrm.Page1.oPag.oWETXTSMS_1_16.value=this.w_WETXTSMS
    endif
    if not(this.oPgFrm.Page1.oPag.oWEAVVFAX_1_17.RadioValue()==this.w_WEAVVFAX)
      this.oPgFrm.Page1.oPag.oWEAVVFAX_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oWEDESCSM_1_19.RadioValue()==this.w_WEDESCSM)
      this.oPgFrm.Page1.oPag.oWEDESCSM_1_19.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_WETIPCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oWETIPCLF_1_1.SetFocus()
            i_bnoObbl = !empty(.w_WETIPCLF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_WEACCATT='S')  and not(empty(.w_WECODCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oWECODCLF_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Account WE non attivo o non specificato")
          case   not(.w_WEACCATT='S')  and not(empty(.w_WECODCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oWECODCLF_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Account WE non attivo o non specificato")
          case   (empty(.w_WETITDOC))  and (.w_WEDESCSM='D' and not EMPTY(.w_WECODCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oWETITDOC_1_8.SetFocus()
            i_bnoObbl = !empty(.w_WETITDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Indicare un titolo per il documento")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_WETIPCLF = this.w_WETIPCLF
    this.o_WECODCLF = this.w_WECODCLF
    this.o_WETITDOC = this.w_WETITDOC
    this.o_WEDESDOC = this.w_WEDESDOC
    this.o_WETXTEML = this.w_WETXTEML
    this.o_WETXTSMS = this.w_WETXTSMS
    return

enddefine

* --- Define pages as container
define class tgsut1kxpPag1 as StdContainer
  Width  = 552
  height = 362
  stdWidth  = 552
  stdheight = 362
  resizeXpos=277
  resizeYpos=290
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oWETIPCLF_1_1 as StdCombo with uid="DEFPANQQJS",rtseq=1,rtrep=.f.,left=117,top=8,width=104,height=21;
    , ToolTipText = "Tipo: cliente o fornitore";
    , HelpContextID = 147372116;
    , cFormVar="w_WETIPCLF",RowSource=""+"Cliente,"+"Fornitore", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oWETIPCLF_1_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oWETIPCLF_1_1.GetRadio()
    this.Parent.oContained.w_WETIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oWETIPCLF_1_1.SetRadio()
    this.Parent.oContained.w_WETIPCLF=trim(this.Parent.oContained.w_WETIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_WETIPCLF=='C',1,;
      iif(this.Parent.oContained.w_WETIPCLF=='F',2,;
      0))
  endfunc

  func oWETIPCLF_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_WECODCLF)
        bRes2=.link_1_3('Full')
      endif
      if .not. empty(.w_WECODCLF)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oWECODCLF_1_3 as StdField with uid="BCZCDAWHML",rtseq=2,rtrep=.f.,;
    cFormVar = "w_WECODCLF", cQueryName = "WECODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Account WE non attivo o non specificato",;
    ToolTipText = "Codice cliente/fornitore",;
    HelpContextID = 159631444,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=117, Top=36, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="WECONTI", oKey_1_1="WETIPCON", oKey_1_2="this.w_WETIPCLF", oKey_2_1="WECODICE", oKey_2_2="this.w_WECODCLF"

  func oWECODCLF_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oWECODCLF_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oWECODCLF_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.WECONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"WETIPCON="+cp_ToStrODBC(this.Parent.oContained.w_WETIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"WETIPCON="+cp_ToStr(this.Parent.oContained.w_WETIPCLF)
    endif
    do cp_zoom with 'WECONTI','*','WETIPCON,WECODICE',cp_AbsName(this.parent,'oWECODCLF_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Account WE",'',this.parent.oContained
  endproc

  add object oRAGSOC_1_6 as StdField with uid="FCLHIKJTLI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RAGSOC", cQueryName = "RAGSOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 147819754,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=249, Top=36, InputMask=replicate('X',40)


  add object oWEPERMESSI_1_7 as StdCombo with uid="LEKDUNPTMP",rtseq=6,rtrep=.f.,left=117,top=85,width=425,height=21;
    , ToolTipText = "Permessi sul documento da inviare";
    , HelpContextID = 22238697;
    , cFormVar="w_WEPERMESSI",RowSource=""+"Tutti gli utenti possono leggere,"+"Tutti gli utenti possono leggere/scrivere,"+"Gli utenti del gruppo possono leggere,"+"Gli utenti del gruppo possono leggere/scrivere,"+"Privato: solo l'utente ne ha visibilit�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oWEPERMESSI_1_7.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'4',;
    space(1)))))))
  endfunc
  func oWEPERMESSI_1_7.GetRadio()
    this.Parent.oContained.w_WEPERMESSI = this.RadioValue()
    return .t.
  endfunc

  func oWEPERMESSI_1_7.SetRadio()
    this.Parent.oContained.w_WEPERMESSI=trim(this.Parent.oContained.w_WEPERMESSI)
    this.value = ;
      iif(this.Parent.oContained.w_WEPERMESSI=='0',1,;
      iif(this.Parent.oContained.w_WEPERMESSI=='1',2,;
      iif(this.Parent.oContained.w_WEPERMESSI=='2',3,;
      iif(this.Parent.oContained.w_WEPERMESSI=='3',4,;
      iif(this.Parent.oContained.w_WEPERMESSI=='4',5,;
      0)))))
  endfunc

  func oWEPERMESSI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not EMPTY(.w_WECODCLF))
    endwith
   endif
  endfunc

  add object oWETITDOC_1_8 as StdField with uid="BZLWYYCGDK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_WETITDOC", cQueryName = "WETITDOC",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    sErrorMsg = "Indicare un titolo per il documento",;
    ToolTipText = "Titolo documento",;
    HelpContextID = 126400599,;
   bGlobalFont=.t.,;
    Height=21, Width=426, Left=117, Top=113, InputMask=replicate('X',60)

  func oWETITDOC_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_WEDESCSM='D' and not EMPTY(.w_WECODCLF))
    endwith
   endif
  endfunc

  add object oWEDESDOC_1_9 as StdMemo with uid="UTMOJGJCBE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_WEDESDOC", cQueryName = "WEDESDOC",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del documento (max 255 caratteri)",;
    HelpContextID = 127776855,;
   bGlobalFont=.t.,;
    Height=43, Width=426, Left=117, Top=141

  func oWEDESDOC_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_WEDESCSM='D' and not EMPTY(.w_WECODCLF))
    endwith
   endif
  endfunc

  add object oWEAVVEML_1_11 as StdCheck with uid="SNWCKCOFWV",rtseq=9,rtrep=.f.,left=42, top=210, caption="Avviso e-mail",;
    ToolTipText = "Se abilitato, il servizio WE al ricevimento dei documenti invia E-mail all'indirizzo indicato",;
    HelpContextID = 106752078,;
    cFormVar="w_WEAVVEML", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oWEAVVEML_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWEAVVEML_1_11.GetRadio()
    this.Parent.oContained.w_WEAVVEML = this.RadioValue()
    return .t.
  endfunc

  func oWEAVVEML_1_11.SetRadio()
    this.Parent.oContained.w_WEAVVEML=trim(this.Parent.oContained.w_WEAVVEML)
    this.value = ;
      iif(this.Parent.oContained.w_WEAVVEML=='S',1,;
      0)
  endfunc

  func oWEAVVEML_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not EMPTY(.w_WECODCLF) and not empty(.w_WEINDEML))
    endwith
   endif
  endfunc

  add object oWETXTEML_1_13 as StdMemo with uid="GNPXWCEDAQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_WETXTEML", cQueryName = "WETXTEML",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Test per E-mail di avviso di avvenuto invio documenti",;
    HelpContextID = 108640334,;
   bGlobalFont=.t.,;
    Height=43, Width=403, Left=140, Top=214

  func oWETXTEML_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_WEAVVEML='S')
    endwith
   endif
  endfunc

  add object oWEAVVSMS_1_14 as StdCheck with uid="QCOQPDLGKX",rtseq=12,rtrep=.f.,left=42, top=259, caption="Avviso SMS",;
    ToolTipText = "Se abilitato, il servizio WE al ricevimento dei documenti invia SMS al numero indicato",;
    HelpContextID = 140306503,;
    cFormVar="w_WEAVVSMS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oWEAVVSMS_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWEAVVSMS_1_14.GetRadio()
    this.Parent.oContained.w_WEAVVSMS = this.RadioValue()
    return .t.
  endfunc

  func oWEAVVSMS_1_14.SetRadio()
    this.Parent.oContained.w_WEAVVSMS=trim(this.Parent.oContained.w_WEAVVSMS)
    this.value = ;
      iif(this.Parent.oContained.w_WEAVVSMS=='S',1,;
      0)
  endfunc

  func oWEAVVSMS_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not EMPTY(.w_WECODCLF) and not empty(.w_WENUMSMS))
    endwith
   endif
  endfunc

  add object oWETXTSMS_1_16 as StdMemo with uid="DAWZFQPLMU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_WETXTSMS", cQueryName = "WETXTSMS",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo da inviare via SMS (max 150 caratteri)",;
    HelpContextID = 142194759,;
   bGlobalFont=.t.,;
    Height=43, Width=403, Left=140, Top=263

  func oWETXTSMS_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_WEAVVSMS='S')
    endwith
   endif
  endfunc

  add object oWEAVVFAX_1_17 as StdCheck with uid="CXGHSACYKC",rtseq=15,rtrep=.f.,left=41, top=310, caption="Invia documento via FAX",;
    ToolTipText = "Se abilitato, il servizio WE al ricevimento dei documenti invia tali documenti via FAX al numero indicato",;
    HelpContextID = 89974850,;
    cFormVar="w_WEAVVFAX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oWEAVVFAX_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWEAVVFAX_1_17.GetRadio()
    this.Parent.oContained.w_WEAVVFAX = this.RadioValue()
    return .t.
  endfunc

  func oWEAVVFAX_1_17.SetRadio()
    this.Parent.oContained.w_WEAVVFAX=trim(this.Parent.oContained.w_WEAVVFAX)
    this.value = ;
      iif(this.Parent.oContained.w_WEAVVFAX=='S',1,;
      0)
  endfunc

  func oWEAVVFAX_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not EMPTY(.w_WECODCLF) and not empty(.w_WENUMFAX))
    endwith
   endif
  endfunc


  add object oWEDESCSM_1_19 as StdCombo with uid="SIFQMLLNJS",rtseq=17,rtrep=.f.,left=416,top=8,width=128,height=21, enabled=.f.;
    , ToolTipText = "Modalit� di inoltro";
    , HelpContextID = 144554061;
    , cFormVar="w_WEDESCSM",RowSource=""+"Simple-Mail,"+"Descrittore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oWEDESCSM_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oWEDESCSM_1_19.GetRadio()
    this.Parent.oContained.w_WEDESCSM = this.RadioValue()
    return .t.
  endfunc

  func oWEDESCSM_1_19.SetRadio()
    this.Parent.oContained.w_WEDESCSM=trim(this.Parent.oContained.w_WEDESCSM)
    this.value = ;
      iif(this.Parent.oContained.w_WEDESCSM=='S',1,;
      iif(this.Parent.oContained.w_WEDESCSM=='D',2,;
      0))
  endfunc


  add object oBtn_1_22 as StdButton with uid="XQKGCSTQHP",left=440, top=312, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere con confermare dati indicati";
    , HelpContextID = 250162406;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not EMPTY(.w_WECODCLF))
      endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="YPTRHGBNFI",left=494, top=312, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per sospendere operazione";
    , HelpContextID = 257451078;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_32 as cp_trafficlight with uid="XWWXVRYFJZ",left=18, top=208, width=20,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , ToolTipText = "Servizio di avviso non attivabile se non stato specificato l'indirizzo email nei dati WE del destinatario";
    , HelpContextID = 108739610


  add object oObj_1_33 as cp_trafficlight with uid="GMTHTEAPGT",left=18, top=256, width=20,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , ToolTipText = "Servizio di avviso non attivabile se non � stato specificato il numero di cellulare nei dati WE del destinatario";
    , HelpContextID = 108739610


  add object oObj_1_34 as cp_trafficlight with uid="QIVLLFHOWI",left=18, top=307, width=20,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , ToolTipText = "Servizio di avviso non attivabile se non � stato specificato il numero di FAX nei dati WE del destinatario";
    , HelpContextID = 108739610

  add object oStr_1_2 as StdString with uid="FAGZFCYZPZ",Visible=.t., Left=23, Top=8,;
    Alignment=1, Width=91, Height=18,;
    Caption="Tipo cli/for:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="TKHNGRVOBX",Visible=.t., Left=5, Top=36,;
    Alignment=1, Width=109, Height=18,;
    Caption="Cliente/fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YCNVHZYVJT",Visible=.t., Left=300, Top=8,;
    Alignment=1, Width=111, Height=18,;
    Caption="Modalit� di inoltro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="UGNGVNDRTJ",Visible=.t., Left=38, Top=85,;
    Alignment=1, Width=76, Height=18,;
    Caption="Permessi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="PSPIVMBWIJ",Visible=.t., Left=27, Top=141,;
    Alignment=1, Width=87, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="GTBLFYIHYF",Visible=.t., Left=18, Top=191,;
    Alignment=0, Width=135, Height=18,;
    Caption="Configurazione avvisi"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="RXXFJCJZGG",Visible=.t., Left=70, Top=113,;
    Alignment=1, Width=44, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="IVPVWGRRZF",Visible=.t., Left=16, Top=63,;
    Alignment=0, Width=176, Height=18,;
    Caption="Parametri invio documento"  ;
  , bGlobalFont=.t.

  add object oBox_1_26 as StdBox with uid="WZUIAQQYZL",left=17, top=205, width=531,height=2

  add object oBox_1_30 as StdBox with uid="OPFNCXJBVL",left=15, top=77, width=531,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut1kxp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
