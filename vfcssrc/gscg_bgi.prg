* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bgi                                                        *
*              Genera giroconto IVA autotrasportatori                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_143]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-27                                                      *
* Last revis.: 2007-05-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bgi",oParentObject)
return(i_retval)

define class tgscg_bgi as StdBatch
  * --- Local variables
  w_FLPDOC = space(1)
  w_FLPPRO = space(1)
  w_CCDESRIG = space(254)
  w_CCDESSUP = space(254)
  w_DATAUT = ctod("  /  /  ")
  w_TOTALE = 0
  w_IVAAUT = space(15)
  w_FINESE = ctod("  /  /  ")
  w_DATBLO = ctod("  /  /  ")
  w_STALIG = ctod("  /  /  ")
  w_MESS = space(90)
  w_OK = .f.
  w_PNSERIAL = space(10)
  w_PNCODESE = space(4)
  w_PNCODUTE = 0
  w_PNNUMRER = 0
  w_PNDATREG = ctod("  /  /  ")
  w_PNCODCAU = space(5)
  w_PNCOMPET = space(4)
  w_PNTIPREG = space(1)
  w_PNFLIVDF = space(1)
  w_PNNUMREG = 0
  w_PNTIPDOC = space(2)
  w_PNPRD = space(2)
  w_PNALFDOC = space(10)
  w_PNNUMDOC = 0
  w_PNDATDOC = ctod("  /  /  ")
  w_PNALFPRO = space(10)
  w_PNNUMPRO = 0
  w_PNVALNAZ = space(3)
  w_PNCODVAL = space(3)
  w_PNPRG = space(8)
  w_PNCAOVAL = 0
  w_PNDESSUP = space(50)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_PNTOTDOC = 0
  w_PNCOMIVA = ctod("  /  /  ")
  w_PNFLREGI = space(1)
  w_PNFLPROV = space(1)
  w_PNANNDOC = space(4)
  w_PNANNPRO = space(4)
  w_PNPRP = space(2)
  w_PNFLLIBG = space(1)
  w_PNRIFDOC = space(10)
  w_PNRIFDIS = space(10)
  w_PNFLGDIF = space(1)
  w_PNRIFINC = space(10)
  w_PNRIFCES = space(1)
  w_PNRIFACC = space(1)
  w_PNNUMTRA = 0
  w_PNDATPLA = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_PNIMPDAR = 0
  w_PNIMPAVE = 0
  w_PNFLPART = space(1)
  w_PNFLZERO = space(1)
  w_PNDESRIG = space(50)
  w_PNCAURIG = space(5)
  w_PNCODPAG = space(5)
  w_PNFLSALD = space(1)
  w_PNFLSALI = space(1)
  w_PNFLSALF = space(1)
  w_PNFLABAN = space(6)
  w_PNLIBGIO = 0
  w_PNFLABAN = space(6)
  w_PNINICOM = ctod("  /  /  ")
  w_PNFINCOM = ctod("  /  /  ")
  w_PNIMPIND = 0
  w_PNCODBUN = space(3)
  w_PNDESRIG = space(50)
  * --- WorkFile variables
  AZIENDA_idx=0
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  SALDICON_idx=0
  CAU_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Giroconto IVA Autotrasportatori (da GSCG_KIA)
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    DIMENSION ARPARAM[12,2]
    this.w_OK = ah_YesNo("Si desidera effettuare la scrittura contabile di giroconto IVA autotrasportatori?")
    if this.w_OK
      this.w_FINESE = g_FINESE
      this.w_DATBLO = cp_CharToDate("  -  -  ")
      * --- blocco Prima Nota
      * --- Try
      local bErr_038315E8
      bErr_038315E8=bTrsErr
      this.Try_038315E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        do case
          case i_ErrMsg="data"
            ah_ErrorMsg("Data registrazione inferiore o uguale a ultima stampa libro giornale",,"")
          case i_ErrMsg="cons"
            this.w_MESS = CHKCONS("P",this.w_PNDATREG,"B","S")
          case i_ErrMsg="giornale"
            ah_ErrorMsg("Prima nota bloccata - semaforo bollati in dati azienda -. Operazione annullata",,"")
          case i_ErrMsg="inferiore"
            ah_ErrorMsg("Data registrazione inferiore o uguale a data ultimo storno",,"")
          case i_ErrMsg="noconsec"
            ah_ErrorMsg("Data registrazione non consecutiva a data ultimo storno",,"")
          case i_ErrMsg="cauocont"
            ah_ErrorMsg("Causale o conto IVA per storni mancante nei parametri IVA",,"")
          case i_ErrMsg=""
            ah_ErrorMsg("Impossibile bloccare la prima nota - semaforo bollati in dati azienda -. Operazione annullata",,"")
        endcase
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_038315E8
      * --- End
      * --- Legge registrazioni IVA Autotrasportatori
      ah_Msg("Ricerca registrazioni IVA autotrasportatori...",.T.)
      vq_exec("query\GSCG1KIA.VQR",this,"IVA_AUTO")
      if USED("IVA_AUTO")
        if RECCOUNT("IVA_AUTO") > 0
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          ah_ErrorMsg("Per l'intervallo selezionato%0Non esistono conti IVA sospesi da stornare.%0E' stata aggiornata solo la data ultimo storno",,"")
        endif
      endif
      * --- Elimina Cursori di Appoggio
      if USED("IVA_AUTO")
        SELECT IVA_AUTO
        USE
      endif
      * --- Sblocca Primanota
      * --- Try
      local bErr_03BA8508
      bErr_03BA8508=bTrsErr
      this.Try_03BA8508()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Impossibile sbloccare la prima nota - semaforo bollati in dati azienda -. Sbloccarlo manualmente",,"")
      endif
      bTrsErr=bTrsErr or bErr_03BA8508
      * --- End
      * --- Aggiorna Parametri IVA (Data Ultimo Storno IVA Autotrasportatori)
      * --- Try
      local bErr_03E07708
      bErr_03E07708=bTrsErr
      this.Try_03E07708()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Impossibile aggiornare i parametri IVA - data ultimo storno IVA autotrasportatori -.",,"")
      endif
      bTrsErr=bTrsErr or bErr_03E07708
      * --- End
      * --- Chiude la Maschera
      this.oParentObject.EcpQuit()
    endif
  endproc
  proc Try_038315E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Controlli Preliminari
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZSTALIG,AZDATBLO,AZCAUAUT,AZIVAAUT,AZDATAUT"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZSTALIG,AZDATBLO,AZCAUAUT,AZIVAAUT,AZDATAUT;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_PNCODCAU = NVL(cp_ToDate(_read_.AZCAUAUT),cp_NullValue(_read_.AZCAUAUT))
      this.w_IVAAUT = NVL(cp_ToDate(_read_.AZIVAAUT),cp_NullValue(_read_.AZIVAAUT))
      this.w_DATAUT = NVL(cp_ToDate(_read_.AZDATAUT),cp_NullValue(_read_.AZDATAUT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PNCAURIG = this.w_PNCODCAU
    do case
      case this.w_PNDATREG<=this.w_STALIG AND NOT EMPTY(this.w_STALIG)
        * --- Raise
        i_Error="data"
        return
      case Not Empty(CHKCONS("P",this.w_PNDATREG,"B","N"))
        * --- Raise
        i_Error="cons"
        return
      case NOT EMPTY(this.w_DATBLO)
        * --- Raise
        i_Error="giornale"
        return
      case this.w_PNDATREG<=this.w_DATAUT AND NOT EMPTY(this.w_DATAUT)
        * --- Raise
        i_Error="inferiore"
        return
      case this.oParentObject.w_DATFIN<>this.w_DATAUT AND NOT EMPTY(this.w_DATAUT)
        * --- Raise
        i_Error="noconsec"
        return
      case EMPTY(this.w_IVAAUT) OR EMPTY(this.w_PNCODCAU)
        * --- Raise
        i_Error="cauocont"
        return
    endcase
    * --- Blocca Primanota
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_FINESE),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = this.w_FINESE;
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03BA8508()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03E07708()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATAUT ="+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'AZIENDA','AZDATAUT');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATAUT = this.w_PNDATREG;
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizio Fase di Contabilizzazione
    * --- Try
    local bErr_0384A2B8
    bErr_0384A2B8=bTrsErr
    this.Try_0384A2B8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=MSG_TRANSACTION_ERROR
      ah_ErrorMsg("Impossibile creare scrittura contabile di giroconto IVA autotrasportatori",,"")
    endif
    bTrsErr=bTrsErr or bErr_0384A2B8
    * --- End
  endproc
  proc Try_0384A2B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_PNSERIAL = SPACE(10)
    this.w_PNNUMRER = 0
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCFLPDOC,CCFLPPRO,CCDESSUP,CCDESRIG"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCFLPDOC,CCFLPPRO,CCDESSUP,CCDESRIG;
        from (i_cTable) where;
            CCCODICE = this.w_PNCODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLPDOC = NVL(cp_ToDate(_read_.CCFLPDOC),cp_NullValue(_read_.CCFLPDOC))
      this.w_FLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
      this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
      this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PNANNDOC = CALPRO(IIF(EMPTY(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC), this.w_PNCOMPET, this.w_FLPDOC)
    this.w_PNANNPRO = CALPRO(this.w_PNDATREG, this.w_PNCOMPET, this.w_FLPPRO)
    this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
    if (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
      * --- Array elenco parametri per descrizioni di riga e testata
       
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PNNUMDOC)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PNALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PNDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]="" 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]=ALLTRIM(STR(this.w_PNNUMPRO)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=""
      this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
    endif
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
    cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    * --- Scrive la Testata
    ah_Msg("Scrittura dati testata",.T.)
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",PNCODESE"+",PNCODUTE"+",PNNUMRER"+",PNDATREG"+",PNCODCAU"+",PNCOMPET"+",PNTIPREG"+",PNFLIVDF"+",PNNUMREG"+",PNTIPDOC"+",PNPRD"+",PNALFDOC"+",PNNUMDOC"+",PNDATDOC"+",PNALFPRO"+",PNNUMPRO"+",PNVALNAZ"+",PNCODVAL"+",PNPRG"+",PNCAOVAL"+",PNDESSUP"+",PNTIPCLF"+",PNCODCLF"+",PNTOTDOC"+",PNCOMIVA"+",PNFLREGI"+",PNFLPROV"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",PNANNDOC"+",PNANNPRO"+",PNPRP"+",PNFLLIBG"+",PNRIFDOC"+",PNRIFDIS"+",PNFLGDIF"+",PNRIFINC"+",PNRIFCES"+",PNRIFACC"+",PNNUMTRA"+",PNDATPLA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLIVDF),'PNT_MAST','PNFLIVDF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_MAST','PNNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRD),'PNT_MAST','PNPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PNT_MAST','PNALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PNT_MAST','PNNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PNT_MAST','PNDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFPRO),'PNT_MAST','PNALFPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMPRO),'PNT_MAST','PNNUMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PNT_MAST','PNTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PNT_MAST','PNCODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PNT_MAST','PNTOTDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMIVA),'PNT_MAST','PNCOMIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLREGI),'PNT_MAST','PNFLREGI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPROV),'PNT_MAST','PNFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PNT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PNT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PNT_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNDOC),'PNT_MAST','PNANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNPRO),'PNT_MAST','PNANNPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRP),'PNT_MAST','PNPRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLLIBG),'PNT_MAST','PNFLLIBG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNRIFDOC),'PNT_MAST','PNRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNRIFDIS),'PNT_MAST','PNRIFDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLGDIF),'PNT_MAST','PNFLGDIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNRIFINC),'PNT_MAST','PNRIFINC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNRIFCES),'PNT_MAST','PNRIFCES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNRIFACC),'PNT_MAST','PNRIFACC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMTRA),'PNT_MAST','PNNUMTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATPLA),'PNT_MAST','PNDATPLA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNCODESE',this.w_PNCODESE,'PNCODUTE',this.w_PNCODUTE,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_PNDATREG,'PNCODCAU',this.w_PNCODCAU,'PNCOMPET',this.w_PNCOMPET,'PNTIPREG',this.w_PNTIPREG,'PNFLIVDF',this.w_PNFLIVDF,'PNNUMREG',this.w_PNNUMREG,'PNTIPDOC',this.w_PNTIPDOC,'PNPRD',this.w_PNPRD)
      insert into (i_cTable) (PNSERIAL,PNCODESE,PNCODUTE,PNNUMRER,PNDATREG,PNCODCAU,PNCOMPET,PNTIPREG,PNFLIVDF,PNNUMREG,PNTIPDOC,PNPRD,PNALFDOC,PNNUMDOC,PNDATDOC,PNALFPRO,PNNUMPRO,PNVALNAZ,PNCODVAL,PNPRG,PNCAOVAL,PNDESSUP,PNTIPCLF,PNCODCLF,PNTOTDOC,PNCOMIVA,PNFLREGI,PNFLPROV,UTCC,UTDC,UTCV,UTDV,PNANNDOC,PNANNPRO,PNPRP,PNFLLIBG,PNRIFDOC,PNRIFDIS,PNFLGDIF,PNRIFINC,PNRIFCES,PNRIFACC,PNNUMTRA,PNDATPLA &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_PNCODESE;
           ,this.w_PNCODUTE;
           ,this.w_PNNUMRER;
           ,this.w_PNDATREG;
           ,this.w_PNCODCAU;
           ,this.w_PNCOMPET;
           ,this.w_PNTIPREG;
           ,this.w_PNFLIVDF;
           ,this.w_PNNUMREG;
           ,this.w_PNTIPDOC;
           ,this.w_PNPRD;
           ,this.w_PNALFDOC;
           ,this.w_PNNUMDOC;
           ,this.w_PNDATDOC;
           ,this.w_PNALFPRO;
           ,this.w_PNNUMPRO;
           ,this.w_PNVALNAZ;
           ,this.w_PNCODVAL;
           ,this.w_PNPRG;
           ,this.w_PNCAOVAL;
           ,this.w_PNDESSUP;
           ,this.w_PNTIPCLF;
           ,this.w_PNCODCLF;
           ,this.w_PNTOTDOC;
           ,this.w_PNCOMIVA;
           ,this.w_PNFLREGI;
           ,this.w_PNFLPROV;
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,this.w_PNANNDOC;
           ,this.w_PNANNPRO;
           ,this.w_PNPRP;
           ,this.w_PNFLLIBG;
           ,this.w_PNRIFDOC;
           ,this.w_PNRIFDIS;
           ,this.w_PNFLGDIF;
           ,this.w_PNRIFINC;
           ,this.w_PNRIFCES;
           ,this.w_PNRIFACC;
           ,this.w_PNNUMTRA;
           ,this.w_PNDATPLA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    SELECT IVA_AUTO
    GO TOP
    SCAN
    * --- Scrive il Detail di Primanota
    ah_Msg("Scrittura dati dettaglio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    SELECT IVA_AUTO
    ENDSCAN
    * --- Scrive Dettaglio P.N. e Saldi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_PNCODCON = this.w_IVAAUT
    this.w_PNIMPDAR = 0
    this.w_PNIMPAVE = this.w_TOTALE
    * --- Eventuali Righe non Valorizzate
    this.w_PNFLZERO = IIF(this.w_PNIMPDAR=0 AND this.w_PNIMPAVE=0, "S", " ")
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNCODPAG"+",PNFLSALI"+",PNFLSALD"+",PNFLSALF"+",PNFLABAN"+",PNLIBGIO"+",PNINICOM"+",PNFINCOM"+",PNIMPIND"+",PNCODBUN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODPAG),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALI),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALF),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLABAN),'PNT_DETT','PNFLABAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNLIBGIO),'PNT_DETT','PNLIBGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPIND),'PNT_DETT','PNIMPIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODBUN),'PNT_DETT','PNCODBUN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',this.w_PNFLPART,'PNFLZERO',this.w_PNFLZERO,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCAURIG,'PNCODPAG',this.w_PNCODPAG)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNCODPAG,PNFLSALI,PNFLSALD,PNFLSALF,PNFLABAN,PNLIBGIO,PNINICOM,PNFINCOM,PNIMPIND,PNCODBUN &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,this.w_PNFLPART;
           ,this.w_PNFLZERO;
           ,this.w_PNDESRIG;
           ,this.w_PNCAURIG;
           ,this.w_PNCODPAG;
           ,this.w_PNFLSALI;
           ,this.w_PNFLSALD;
           ,this.w_PNFLSALF;
           ,this.w_PNFLABAN;
           ,this.w_PNLIBGIO;
           ,this.w_PNINICOM;
           ,this.w_PNFINCOM;
           ,this.w_PNIMPIND;
           ,this.w_PNCODBUN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorna Saldo
    * --- Try
    local bErr_04016E70
    bErr_04016E70=bTrsErr
    this.Try_04016E70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04016E70
    * --- End
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
      +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
          +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
             )
    else
      update (i_cTable) set;
          SLDARPER = SLDARPER + this.w_PNIMPDAR;
          ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
          &i_ccchkf. ;
       where;
          SLTIPCON = this.w_PNTIPCON;
          and SLCODICE = this.w_PNCODCON;
          and SLCODESE = this.w_PNCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Creata scrittura contabile di giroconto IVA autotrasportatori",,"")
    return
  proc Try_04016E70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Variabili
    * --- Variabili di PNT_MAST
    * --- Variabili di PNT_DETT
    this.w_PNCODESE = this.oParentObject.w_ANNO
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    * --- La data di registrazione del Giroconto IVA Autotrasportatori coincide con la data della fine del trimestre
    do case
      case (this.oParentObject.w_NUMPER=1 AND g_TIPDEN="T") OR (this.oParentObject.w_NUMPER=3 AND g_TIPDEN="M")
        this.w_PNDATREG = cp_CharToDate("31-03-" + this.oParentObject.w_ANNO)
      case (this.oParentObject.w_NUMPER=2 AND g_TIPDEN="T") OR (this.oParentObject.w_NUMPER=6 AND g_TIPDEN="M")
        this.w_PNDATREG = cp_CharToDate("30-06-" + this.oParentObject.w_ANNO)
      case (this.oParentObject.w_NUMPER=3 AND g_TIPDEN="T") OR (this.oParentObject.w_NUMPER=9 AND g_TIPDEN="M")
        this.w_PNDATREG = cp_CharToDate("30-09-" + this.oParentObject.w_ANNO)
      case (this.oParentObject.w_NUMPER=4 AND g_TIPDEN="T") OR (this.oParentObject.w_NUMPER=12 AND g_TIPDEN="M")
        this.w_PNDATREG = cp_CharToDate("31-12-" + this.oParentObject.w_ANNO)
    endcase
    this.w_PNCOMPET = this.oParentObject.w_ANNO
    this.w_PNTIPREG = "N"
    this.w_PNFLIVDF = " "
    this.w_PNNUMREG = 0
    this.w_PNTIPDOC = "NO"
    this.w_PNPRD = "NN"
    this.w_PNALFDOC = Space(10)
    this.w_PNNUMDOC = 0
    this.w_PNDATDOC = cp_CharToDate("  -  -  ")
    this.w_PNALFPRO = Space(10)
    this.w_PNNUMPRO = 0
    this.w_PNVALNAZ = g_PERVAL
    this.w_PNCODVAL = g_PERVAL
    this.w_PNCAOVAL = 1
    this.w_PNDESSUP = " "
    this.w_PNTIPCLF = "N"
    this.w_PNCODCLF = " "
    this.w_PNTOTDOC = 0
    this.w_PNCOMIVA = this.w_PNDATREG
    this.w_PNFLREGI = " "
    this.w_PNFLPROV = "N"
    this.w_PNANNDOC = " "
    this.w_PNANNPRO = " "
    this.w_PNPRP = "NN"
    this.w_PNFLLIBG = " "
    this.w_PNRIFDOC = " "
    this.w_PNRIFDIS = " "
    this.w_PNFLGDIF = " "
    this.w_PNRIFINC = " "
    this.w_PNRIFCES = " "
    this.w_PNRIFACC = " "
    this.w_PNNUMTRA = 0
    this.w_PNDATPLA = this.w_PNDATREG
    this.w_PNTIPCON = "G"
    this.w_PNFLPART = "N"
    this.w_PNFLZERO = " "
    this.w_PNDESRIG = " "
    this.w_PNCODPAG = " "
    this.w_PNFLSALD = "+"
    this.w_PNFLSALI = " "
    this.w_PNFLSALF = " "
    this.w_PNLIBGIO = 0
    this.w_PNFLABAN = " "
    this.w_PNINICOM = cp_CharToDate("  -  -  ")
    this.w_PNFINCOM = cp_CharToDate("  -  -  ")
    this.w_PNIMPIND = 0
    this.w_PNCODBUN = " "
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Dettaglio P.N. e Saldi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_PNCODCON = CCCONIVA
    this.w_PNIMPDAR = IVIMPIVA
    this.w_PNIMPAVE = 0
    this.w_TOTALE = this.w_TOTALE+this.w_PNIMPDAR
    if Empty(this.w_PNDESRIG) and Not EMPTY(this.w_CCDESRIG)
      this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
    endif
    * --- Eventuali Righe non Valorizzate
    this.w_PNFLZERO = IIF(this.w_PNIMPDAR=0 AND this.w_PNIMPAVE=0, "S", " ")
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNCODPAG"+",PNFLSALI"+",PNFLSALD"+",PNFLSALF"+",PNFLABAN"+",PNLIBGIO"+",PNINICOM"+",PNFINCOM"+",PNIMPIND"+",PNCODBUN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODPAG),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALI),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALF),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLABAN),'PNT_DETT','PNFLABAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNLIBGIO),'PNT_DETT','PNLIBGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPIND),'PNT_DETT','PNIMPIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODBUN),'PNT_DETT','PNCODBUN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',this.w_PNFLPART,'PNFLZERO',this.w_PNFLZERO,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCAURIG,'PNCODPAG',this.w_PNCODPAG)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNCODPAG,PNFLSALI,PNFLSALD,PNFLSALF,PNFLABAN,PNLIBGIO,PNINICOM,PNFINCOM,PNIMPIND,PNCODBUN &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,this.w_PNFLPART;
           ,this.w_PNFLZERO;
           ,this.w_PNDESRIG;
           ,this.w_PNCAURIG;
           ,this.w_PNCODPAG;
           ,this.w_PNFLSALI;
           ,this.w_PNFLSALD;
           ,this.w_PNFLSALF;
           ,this.w_PNFLABAN;
           ,this.w_PNLIBGIO;
           ,this.w_PNINICOM;
           ,this.w_PNFINCOM;
           ,this.w_PNIMPIND;
           ,this.w_PNCODBUN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorna Saldo
    * --- Try
    local bErr_03EC1E50
    bErr_03EC1E50=bTrsErr
    this.Try_03EC1E50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03EC1E50
    * --- End
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
      +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
          +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
             )
    else
      update (i_cTable) set;
          SLDARPER = SLDARPER + this.w_PNIMPDAR;
          ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
          &i_ccchkf. ;
       where;
          SLTIPCON = this.w_PNTIPCON;
          and SLCODICE = this.w_PNCODCON;
          and SLCODESE = this.w_PNCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc
  proc Try_03EC1E50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='PNT_DETT'
    this.cWorkTables[4]='SALDICON'
    this.cWorkTables[5]='CAU_CONT'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
