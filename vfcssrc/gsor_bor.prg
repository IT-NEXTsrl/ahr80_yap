* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_bor                                                        *
*              Righe aperte                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_21]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-20                                                      *
* Last revis.: 2014-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPDOC,pTIPCON,pCODCON,pDATDOC1,pDATDOC2,pCODAGE,pCODMAG,pCODICE1,pCODICE2,pDATAEV1,pDATAEV2,pFLVEAC,pCAUORD,pNUMDOC,pNUMDOC1,pALFDOC,pALFDOC1
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor_bor",oParentObject,m.pTIPDOC,m.pTIPCON,m.pCODCON,m.pDATDOC1,m.pDATDOC2,m.pCODAGE,m.pCODMAG,m.pCODICE1,m.pCODICE2,m.pDATAEV1,m.pDATAEV2,m.pFLVEAC,m.pCAUORD,m.pNUMDOC,m.pNUMDOC1,m.pALFDOC,m.pALFDOC1)
return(i_retval)

define class tgsor_bor as StdBatch
  * --- Local variables
  pTIPDOC = space(5)
  pTIPCON = space(1)
  pCODCON = space(15)
  pDATDOC1 = ctod("  /  /  ")
  pDATDOC2 = ctod("  /  /  ")
  pCODAGE = space(5)
  pCODMAG = space(5)
  pCODICE1 = space(20)
  pCODICE2 = space(20)
  pDATAEV1 = ctod("  /  /  ")
  pDATAEV2 = ctod("  /  /  ")
  pFLVEAC = space(1)
  pCAUORD = space(5)
  pNUMDOC = 0
  pNUMDOC1 = 0
  pALFDOC = space(1)
  pALFDOC1 = space(1)
  w_TIPDOC = space(5)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_DATDOC1 = ctod("  /  /  ")
  w_DATDOC2 = ctod("  /  /  ")
  w_CODAGE = space(5)
  w_CODMAG = space(5)
  w_CODICE1 = space(20)
  w_CODICE2 = space(20)
  w_DATAEV1 = ctod("  /  /  ")
  w_DATAEV2 = ctod("  /  /  ")
  w_FLVEAC = space(1)
  w_CAUORD = space(5)
  w_NUMDOC = 0
  w_NUMDOC1 = 0
  w_DAALFA = space(1)
  w_AALFA = space(1)
  * --- WorkFile variables
  TMP_OROWS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch Utilizzato da GSOR_BEV (EvadibilitÓ Ordini)
    * --- Parametri
    * --- Variabili locali
    this.w_TIPDOC = ALLTRIM(this.pTIPDOC)
    this.w_TIPCON = ALLTRIM(this.pTIPCON)
    this.w_CODCON = ALLTRIM(this.pCODCON)
    this.w_DATDOC1 = this.pDATDOC1
    this.w_DATDOC2 = this.pDATDOC2
    this.w_CODAGE = ALLTRIM(this.pCODAGE)
    this.w_CODMAG = ALLTRIM(this.pCODMAG)
    this.w_CODICE1 = ALLTRIM(this.pCODICE1)
    this.w_CODICE2 = ALLTRIM(this.pCODICE2)
    this.w_DATAEV1 = this.pDATAEV1
    this.w_DATAEV2 = this.pDATAEV2
    this.w_FLVEAC = ALLTRIM(this.pFLVEAC)
    this.w_CAUORD = IIF(VARTYPE(this.pCAUORD)<>"C", "", this.pCAUORD)
    this.w_NUMDOC = this.pNUMDOC
    this.w_NUMDOC1 = this.pNUMDOC1
    this.w_DAALFA = ALLTRIM(this.pALFDOC)
    this.w_AALFA = ALLTRIM(this.pALFDOC1)
    * --- Create temporary table TMP_OROWS
    i_nIdx=cp_AddTableDef('TMP_OROWS') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsor_bor',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_OROWS_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
  endproc


  proc Init(oParentObject,pTIPDOC,pTIPCON,pCODCON,pDATDOC1,pDATDOC2,pCODAGE,pCODMAG,pCODICE1,pCODICE2,pDATAEV1,pDATAEV2,pFLVEAC,pCAUORD,pNUMDOC,pNUMDOC1,pALFDOC,pALFDOC1)
    this.pTIPDOC=pTIPDOC
    this.pTIPCON=pTIPCON
    this.pCODCON=pCODCON
    this.pDATDOC1=pDATDOC1
    this.pDATDOC2=pDATDOC2
    this.pCODAGE=pCODAGE
    this.pCODMAG=pCODMAG
    this.pCODICE1=pCODICE1
    this.pCODICE2=pCODICE2
    this.pDATAEV1=pDATAEV1
    this.pDATAEV2=pDATAEV2
    this.pFLVEAC=pFLVEAC
    this.pCAUORD=pCAUORD
    this.pNUMDOC=pNUMDOC
    this.pNUMDOC1=pNUMDOC1
    this.pALFDOC=pALFDOC
    this.pALFDOC1=pALFDOC1
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMP_OROWS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPDOC,pTIPCON,pCODCON,pDATDOC1,pDATDOC2,pCODAGE,pCODMAG,pCODICE1,pCODICE2,pDATAEV1,pDATAEV2,pFLVEAC,pCAUORD,pNUMDOC,pNUMDOC1,pALFDOC,pALFDOC1"
endproc
