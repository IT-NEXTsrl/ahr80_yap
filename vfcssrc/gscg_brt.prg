* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_brt                                                        *
*              Calcola dati ritenute                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_329]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-16                                                      *
* Last revis.: 2018-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAME
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_brt",oParentObject,m.pPARAME)
return(i_retval)

define class tgscg_brt as StdBatch
  * --- Local variables
  pPARAME = space(1)
  w_GSVE_MDV = .NULL.
  w_CODCLF = space(15)
  w_CODIRP = space(5)
  w_CODTR2 = space(5)
  w_CAURIT = space(2)
  w_AbsRow = 0
  w_nRelRow = 0
  w_RECPOS = 0
  w_CAUSALE = space(5)
  w_TOTDOC = 0
  w_TOTIVA = 0
  w_AbsRowMIV = 0
  w_nRelRowMIV = 0
  w_RECPOSMIV = 0
  w_AbsRowMDR = 0
  w_nRelRowMDR = 0
  w_RECPOSMDR = 0
  w_IVAIND = 0
  w_AGEIMPO = 0
  w_AGERITE = 0
  w_AZIENDA = space(5)
  w_APPOINPS = 0
  w_TOTIMP = 0
  w_TOTINP = 0
  w_MESS = space(100)
  w_NONSOG = 0
  w_NONSOG1 = 0
  w_NONSOGGCA = 0
  w_TOTMRIT = 0
  w_TOTMPRE = 0
  w_TIPRITE = space(1)
  w_PERCEN = 0
  w_PADRE = .NULL.
  w_TIPRIT = space(1)
  w_TOTMDR = 0
  w_TIPO = space(1)
  w_CONTROL = .NULL.
  w_PERIND = 0
  * --- WorkFile variables
  CONTI_idx=0
  AGENTI_idx=0
  TRI_BUTI_idx=0
  VOCIIVA_idx=0
  DATIRITE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se la prima lettera del parametro � la 'P' il batch � lanciato dalla maschera
    *     dati ritenuta presente nella primanota altrimenti se vale 'D' � lanciata dai
    *     Documenti
    this.w_AZIENDA = i_codazi
    this.oParentObject.w_DRCODBUN = g_CODBUN
    this.w_PADRE = This.oParentObject.oParentObject
    if LEFT(this.pParame,1)="P"
      this.w_CODCLF = this.w_PADRE.w_Pncodclf
      this.w_CAUSALE = this.w_PADRE.w_Pncodcau
      this.oParentObject.w_TIPCLF = this.w_PADRE.w_Pntipclf
      this.w_TOTMDR = this.w_PADRE.w_TOTMDR
      if this.w_TOTMDR=0 and this.pPARAME="PG"
        * --- eseguo aggiorna
        this.pPARAME = "PI"
      endif
      * --- aggiorno tottale ritenute
      this.w_PADRE.w_TOTMDR = this.w_PADRE.w_TOTFOR
    else
      this.w_CODCLF = this.w_PADRE.w_Mvcodcon
      this.w_CAUSALE = this.w_PADRE.w_Mvcaucon
      this.oParentObject.w_TIPCLF = this.w_PADRE.w_Mvtipcon
    endif
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCODIRP,ANCAURIT,ANTIPCLF,ANCASPRO,ANPEINPS,ANRIINPS,ANCOINPS,ANRITENU,ANCODTR2,ANFLRITE,ANCODSNS"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCLF);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCLF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCODIRP,ANCAURIT,ANTIPCLF,ANCASPRO,ANPEINPS,ANRIINPS,ANCOINPS,ANRITENU,ANCODTR2,ANFLRITE,ANCODSNS;
        from (i_cTable) where;
            ANTIPCON = this.oParentObject.w_TIPCLF;
            and ANCODICE = this.w_CODCLF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODIRP = NVL(cp_ToDate(_read_.ANCODIRP),cp_NullValue(_read_.ANCODIRP))
      this.w_CAURIT = NVL(cp_ToDate(_read_.ANCAURIT),cp_NullValue(_read_.ANCAURIT))
      this.oParentObject.w_DRTIPFOR = NVL(cp_ToDate(_read_.ANTIPCLF),cp_NullValue(_read_.ANTIPCLF))
      this.oParentObject.w_DRPERPRO = NVL(cp_ToDate(_read_.ANCASPRO),cp_NullValue(_read_.ANCASPRO))
      this.oParentObject.w_IMPINPS = NVL(cp_ToDate(_read_.ANPEINPS),cp_NullValue(_read_.ANPEINPS))
      this.oParentObject.w_RITINPS = NVL(cp_ToDate(_read_.ANRIINPS),cp_NullValue(_read_.ANRIINPS))
      this.oParentObject.w_PERINPS = NVL(cp_ToDate(_read_.ANCOINPS),cp_NullValue(_read_.ANCOINPS))
      this.w_TIPRITE = NVL(cp_ToDate(_read_.ANRITENU),cp_NullValue(_read_.ANRITENU))
      this.w_CODTR2 = NVL(cp_ToDate(_read_.ANCODTR2),cp_NullValue(_read_.ANCODTR2))
      this.w_TIPRIT = NVL(cp_ToDate(_read_.ANFLRITE),cp_NullValue(_read_.ANFLRITE))
      this.oParentObject.w_CODSNS = NVL(cp_ToDate(_read_.ANCODSNS),cp_NullValue(_read_.ANCODSNS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.oParentObject.w_TIPORITE = IIF(this.oParentObject.w_TIPCLF="C" , this.w_TIPRIT,this.oParentObject.w_TIPORITE)
    this.w_TIPO = IIF(this.oParentObject.w_TIPCLF="C" , "V","A")
    do case
      case this.pParame$"PI-DI"
        * --- Se il flag Dati Confermati non � attivo, eseguo i ricalcoli, altrimenti no.
        if this.oParentObject.w_DRDATCON<>"S"
          if LEFT(this.pParame,1)="P"
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_DRSOMESC = this.w_IVAIND 
            this.oParentObject.w_DRIMPOSTA = this.oParentObject.w_DRIMPOSTA - this.w_IVAIND
          else
            this.Page_7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_DRSOMESC = this.w_IVAIND + this.w_GSVE_MDV.w_MVSPEBOL + this.w_GSVE_MDV.w_MVIMPARR
            this.oParentObject.w_DRIMPOSTA = this.oParentObject.w_DRIMPOSTA - this.w_IVAIND
            if IsAlt()
              * --- Toglie la cassa previdenza che viene considerata nel fondo professionisti
              this.oParentObject.w_DRSOMESC = this.oParentObject.w_DRSOMESC + this.w_GSVE_MDV.w_TOTRIP4
            endif
          endif
          * --- Inizializzo la variabile contenente il calcolo della Cassa Professionisti
          this.oParentObject.w_DRFODPRO = cp_ROUND(this.oParentObject.w_DRPERPRO/(100 + this.oParentObject.w_DRPERPRO) * (this.oParentObject.w_DRTOTDOC - this.oParentObject.w_DRIMPOSTA - this.oParentObject.w_DRNONSO1 - this.oParentObject.w_DRSOMESC),this.oParentObject.w_DECTOP)
          if IsAlt() AND VARTYPE(this.w_GSVE_MDV)="O"
            * --- La cassa previdenza viene considerata nel fondo professionisti
            this.oParentObject.w_DRFODPRO = this.oParentObject.w_DRFODPRO + this.w_GSVE_MDV.w_MVSPETRA
          endif
          this.oParentObject.w_DRSOGGE = this.oParentObject.w_DRTOTDOC - this.oParentObject.w_DRIMPOSTA - this.oParentObject.w_DRNONSO1 - this.oParentObject.w_DRSOMESC - this.oParentObject.w_DRFODPRO
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if LEFT(this.pParame,1)="P"
            * --- Se nota di credito cambio il segno dell'imposta
            this.oParentObject.w_DRIMPOSTA = this.oParentObject.w_DRIMPOSTA * IIF( this.w_PADRE.w_PNTIPDOC $ "NC-NE" ,-1, 1)
            this.w_IVAIND = this.w_IVAIND* IIF( this.w_PADRE.w_PNTIPDOC $ "NC-NE" ,-1, 1)
            this.oParentObject.w_DRTOTDOC = this.w_PADRE.w_TOTFOR
          else
            this.w_GSVE_MDV = This.oParentObject.oParentObject
            this.oParentObject.w_DRTOTDOC = this.w_GSVE_MDV.w_TOTFATTU
            this.oParentObject.w_DRIMPOSTA = this.w_GSVE_MDV.w_TOTIMPOS
            this.w_IVAIND = 0
          endif
          * --- Inizializzo la variabile contenente il calcolo della Cassa Professionisti
          this.oParentObject.w_DRFODPRO = cp_ROUND(this.oParentObject.w_DRPERPRO/(100 + this.oParentObject.w_DRPERPRO) * (this.oParentObject.w_DRTOTDOC - this.oParentObject.w_DRIMPOSTA - this.oParentObject.w_DRNONSO1 - this.oParentObject.w_DRSOMESC),this.oParentObject.w_DECTOP)
          this.oParentObject.w_DRSOGGE = this.oParentObject.w_DRTOTDOC - this.oParentObject.w_DRIMPOSTA - this.oParentObject.w_DRNONSO1 - this.oParentObject.w_DRSOMESC - this.oParentObject.w_DRFODPRO
          i_retcode = 'stop'
          return
        endif
      case this.pParame$"PA-DA-PB-DB"
        * --- Inizializzo la variabile contenente il calcolo della Cassa Professionisti
        if this.pParame$"PA-DA"
          this.oParentObject.w_DRFODPRO = cp_ROUND(this.oParentObject.w_DRPERPRO/(100+this.oParentObject.w_DRPERPRO) * (this.oParentObject.w_DRTOTDOC - this.oParentObject.w_DRIMPOSTA - this.oParentObject.w_DRNONSO1 - this.oParentObject.w_DRSOMESC),this.oParentObject.w_DECTOP)
        endif
        * --- Calcolo l'importo soggetto a ritenuta
        this.oParentObject.w_DRSOGGE = this.oParentObject.w_DRTOTDOC - this.oParentObject.w_DRIMPOSTA - this.oParentObject.w_DRNONSO1 - this.oParentObject.w_DRSOMESC - this.oParentObject.w_DRFODPRO
      case this.pParame$"PG-DG"
        * --- Cancello il transitorio con i dati ricaricati ed inserisco la nuova riga in base
        *     al valore delle somme non soggette oppure alle somme escluse.
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParame$"PH-PE-DH-DE"
        * --- Devo verificare se la riga � una riga INPS oppure IRPEF
        * --- Assegno posizione per ritorno sul fondo dei controlli
        this.w_AbsRowMDR = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
        this.w_nRelRowMDR = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
        SELECT (this.oParentObject.cTrsName)
        this.w_RECPOSMDR = IIF( Eof() , RECNO(this.oParentObject.cTrsName)-1 , RECNO(this.oParentObject.cTrsName) )
        if t_FLACON="R"
          if this.pParame$"PH-DH"
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Calcolo l'importo della ritenuta
            this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*this.oParentObject.w_DRPERRIT/100,this.oParentObject.w_DECTOP)
          endif
        else
          this.w_PERCEN = IIF(this.oParentObject.w_DRPERRIT<=0, this.oParentObject.w_PERINPS, this.oParentObject.w_DRPERRIT)
          if this.pParame$"PH-DH"
            SUM t_DRNONSOG FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRNONSOG <>0 and t_FLACON$"IPZ" and not deleted() to this.w_NONSOG1
            SUM t_DRIMPONI FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRIMPONI <>0 and t_FLACON$"IPZ" and not deleted() to this.w_TOTINP
            * --- Calcolo Imponibile INPS
            this.oParentObject.w_DRIMPONI = (this.oParentObject.w_DRSOGGE+ this.oParentObject.w_DRIMPONI-this.w_NONSOG1 - this.w_TOTINP)
            this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*(this.w_PERCEN)/100,this.oParentObject.w_DECTOP)
            this.oParentObject.w_DRPERRIT = IIF(this.oParentObject.w_DRPERRIT<=0,this.w_PERCEN,this.oParentObject.w_DRPERRIT)
            this.oParentObject.w_DRRIINPS = IIF(this.oParentObject.w_DRRIINPS<=0,this.oParentObject.w_RITINPS,this.oParentObject.w_DRRIINPS)
            this.oParentObject.w_DRCODSNS = " "
          else
            this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*(this.w_PERCEN)/100,this.oParentObject.w_DECTOP)
          endif
        endif
         SELECT (this.oParentObject.cTrsName) 
 GO this.w_RECPOSMDR 
 this.oParentObject.TrsFromWork()
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if LEFT(this.pParame,1)="D"
          this.pParame="DC"
        else
          this.pParame="PC"
        endif
      case this.pParame$"PD-DD"
        if Empty(this.oParentObject.w_DRCODTRI)
          i_retcode = 'stop'
          return
        endif
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Controllo che l'utente non abbia modificato sulle righe caricate automaticamente
        *     dalla procedura il codice del tributo.
        *     Se ha effettuato questa operazione lo blocco.
        if this.oParentObject.w_MODIFICA="N"
          if this.oParentObject.w_DRCODTRI<>this.oParentObject.o_DRCODTRI
            ah_ErrorMsg("Impossibile modificare il codice tributo%0Cancellare la riga utilizzando il tasto F6","!","")
            this.oParentObject.w_DRCODTRI = this.oParentObject.w_VCODTRI
            * --- Read from TRI_BUTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TRI_BUTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TRI_BUTI_idx,2],.t.,this.TRI_BUTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TRFLACON"+;
                " from "+i_cTable+" TRI_BUTI where ";
                    +"TRCODTRI = "+cp_ToStrODBC(this.oParentObject.w_DRCODTRI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TRFLACON;
                from (i_cTable) where;
                    TRCODTRI = this.oParentObject.w_DRCODTRI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_FLACON = NVL(cp_ToDate(_read_.TRFLACON),cp_NullValue(_read_.TRFLACON))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          i_retcode = 'stop'
          return
        else
          * --- Controllo se il fornitore � soggetto IRPEF oppure anche INPS nel caso fosse
          *     soggetto IRPEF non gli devo permettere di inserire un codice INPS
          * --- Leggo la causale prestazione associata al codice tributo impostato
          * --- Read from TRI_BUTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TRI_BUTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TRI_BUTI_idx,2],.t.,this.TRI_BUTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRCAUPRE2,TRFLACON"+;
              " from "+i_cTable+" TRI_BUTI where ";
                  +"TRCODTRI = "+cp_ToStrODBC(this.oParentObject.w_DRCODTRI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRCAUPRE2,TRFLACON;
              from (i_cTable) where;
                  TRCODTRI = this.oParentObject.w_DRCODTRI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CAURIT = NVL(cp_ToDate(_read_.TRCAUPRE2),cp_NullValue(_read_.TRCAUPRE2))
            this.oParentObject.w_FLACON = NVL(cp_ToDate(_read_.TRFLACON),cp_NullValue(_read_.TRFLACON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_FLACON="R"
            this.oParentObject.w_DRCODCAU = this.w_CAURIT
            this.oParentObject.w_FLACON = "R"
            * --- Calcolo il totale da pagare. Devo ciclare il transitorio per avere l'importo di ogni riga
            *     del detail
            this.w_TOTIMP = 0
            SUM t_DRIMPONI FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRIMPONI <>0 and t_FLACON="R" and not deleted() to this.w_TOTIMP
            SUM t_DRNONSOG FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRNONSOG <>0 and t_FLACON="R" and not deleted() to this.w_NONSOG
            if this.oParentObject.w_DRTIPFOR = "A"
              * --- Se ho riempito le somme non soggette, non devo rieffettuare il calcolo per la percentuale imponibile
              * --- Read from AGENTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.AGENTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "AGPERIMP,AGRITIRP"+;
                  " from "+i_cTable+" AGENTI where ";
                      +"AGCODFOR = "+cp_ToStrODBC(this.w_CODCLF);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  AGPERIMP,AGRITIRP;
                  from (i_cTable) where;
                      AGCODFOR = this.w_CODCLF;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_AGEIMPO = NVL(cp_ToDate(_read_.AGPERIMP),cp_NullValue(_read_.AGPERIMP))
                this.w_AGERITE = NVL(cp_ToDate(_read_.AGRITIRP),cp_NullValue(_read_.AGRITIRP))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.oParentObject.w_DRIMPONI = cp_ROUND(this.w_AGEIMPO/100*(this.oParentObject.w_DRSOGGE-this.w_TOTIMP-this.w_NONSOG),this.oParentObject.w_DECTOP) 
              this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*this.w_AGERITE/100,this.oParentObject.w_DECTOP)
              this.oParentObject.w_DRPERRIT = this.w_AGERITE
              this.oParentObject.w_DRRIINPS = IIF(this.oParentObject.w_DRRIINPS<=0,this.oParentObject.w_RITINPS,this.oParentObject.w_DRRIINPS)
            else
              vq_exec("..\RITE\EXE\QUERY\GSRI_BRT", this,"PERCENTUALI")
              SELECT PERCENTUALI
              GO BOTTOM
              if RECCOUNT("PERCENTUALI") > 0
                this.oParentObject.w_DRIMPONI = cp_ROUND(IMPONIBILE/100*(this.oParentObject.w_DRSOGGE-this.w_TOTIMP-this.w_NONSOG),this.oParentObject.w_DECTOP)
                this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*RITENUTA/100,this.oParentObject.w_DECTOP)
                this.oParentObject.w_DRPERRIT = RITENUTA
                this.oParentObject.w_DRRIINPS = this.oParentObject.w_RITINPS
              else
                ah_ErrorMsg("Codice tributo non definito in tabella",,"")
              endif
              if used ("PERCENTUALI")
                select PERCENTUALI
                use
              endif
            endif
            this.oParentObject.w_DRNONSOG = this.oParentObject.w_DRSOGGE - this.oParentObject.w_DRIMPONI
            this.oParentObject.w_DRCODSNS = IIF(this.oParentObject.w_TIPCLF="C" OR this.oParentObject.w_DRNONSOG=0," ",IIF(EMPTY(this.oParentObject.w_CODSNS),"7",this.oParentObject.w_CODSNS))
          else
            if this.w_TIPRITE="S"
              if this.oParentObject.w_TIPCLF="F"
                ah_ErrorMsg("Impossibile inserire il seguente codice tributo%0Il fornitore � soggetto solo I.R.PE.F.","!","")
              else
                ah_ErrorMsg("Impossibile inserire il seguente codice tributo%0Il cliente � soggetto solo I.R.PE.F.","!","")
              endif
              this.oParentObject.w_DRCODTRI = SPACE(5)
              i_retcode = 'stop'
              return
            endif
            * --- Devo verificare che il codice tributo INPS specificato deve essere identico
            *     a quello specificato nell'anagrafica del fornitore
            if this.oParentObject.w_DRCODTRI<>this.w_CODTR2
              if this.oParentObject.w_TIPCLF="F"
                ah_ErrorMsg("Il codice tributo I.N.P.S. inserito � diverso dal codice tributo%0specificato nell'anagrafica fornitori","!","")
              else
                ah_ErrorMsg("Il codice tributo I.N.P.S. inserito � diverso dal codice tributo%0specificato nell'anagrafica clienti","!","")
              endif
              this.oParentObject.w_DRCODTRI = SPACE(5)
              i_retcode = 'stop'
              return
            endif
            this.oParentObject.w_DRCODCAU = "A"
            this.oParentObject.w_FLACON = "Z"
            this.w_TOTINP = 0
            SUM t_DRNONSOG FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRNONSOG <>0 and t_FLACON="Z" and not deleted() to this.w_NONSOG1
            SUM t_DRIMPONI FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRIMPONI <>0 and t_FLACON="Z" and not deleted() to this.w_TOTINP
            this.oParentObject.w_DRIMPONI = cp_ROUND((this.oParentObject.w_DRSOGGE-this.w_TOTINP-this.w_NONSOG1) * this.oParentObject.w_IMPINPS/100,this.oParentObject.w_DECTOP)
            this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*(this.w_PERCEN)/100,this.oParentObject.w_DECTOP)
            this.oParentObject.w_DRPERRIT = this.oParentObject.w_PERINPS
            this.oParentObject.w_DRRIINPS = this.oParentObject.w_RITINPS
          endif
           SELECT (this.oParentObject.cTrsName) 
 GO this.w_RECPOSMDR 
 this.oParentObject.TrsFromWork()
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Rimetto il detttaglio IVA nella posizione prima della scansione
         
 SELECT (this.oParentObject.cTrsName) 
 GO this.w_RECPOSMDR 
 With this.oParentObject 
 .TrsFromWork() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow= this.w_AbsRowMDR 
 .oPgFrm.Page1.oPag.oBody.nRelRow= this.w_nRelRowMDR 
 EndWith
        if LEFT(this.pParame,1)="D"
          this.pParame="DC"
        else
          this.pParame="PC"
        endif
      case this.pParame$"PF-DF"
        * --- Calcolo l'importo soggetto a ritenuta
        if LEFT(this.pParame,1)="P"
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.pParame="PC"
        else
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.pParame="DC"
        endif
        * --- all'imposta calcolato sul castelleto IVA detraggo la parte indetraibile
        *     (solo Prima Nota)
        if this.oParentObject.w_DRIMPOSTA<>0
          this.oParentObject.w_DRIMPOSTA = this.oParentObject.w_DRIMPOSTA - this.w_IVAIND
        endif
        * --- Al termine calcolo il totale delle Base calcolo Ritenuta
        this.oParentObject.w_DRSOGGE = this.oParentObject.w_DRTOTDOC - this.oParentObject.w_DRIMPOSTA - this.oParentObject.w_DRNONSO1 - this.oParentObject.w_DRSOMESC - this.oParentObject.w_DRFODPRO
    endcase
    if this.pParame$"PC-DC"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Calcolo il totale da pagare. Devo ciclare il transitorio per avere l'importo di ogni riga
      *     del detail
      GO TOP
      SUM t_DRRITENU FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRRITENU <>0 and not deleted() to w_TOTALE
      SUM t_DRIMPONI FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRIMPONI <>0 and t_FLACON="R" and not deleted() to this.w_TOTIMP
      SUM t_DRNONSOG FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRNONSOG <>0 and t_FLACON="R" and not deleted() to this.w_NONSOG
      SUM t_DRIMPONI FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRIMPONI <>0 and t_FLACON$"IPZ" and not deleted() to this.w_TOTINP
      SUM t_DRNONSOG FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRNONSOG <>0 and t_FLACON$"IPZ" and not deleted() to this.w_NONSOG1
      if LEFT(this.pParame,1)="D"
        SUM t_DRRITENU FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRRITENU <>0 and t_FLACON="R" and not deleted() to this.w_TOTMRIT
        SUM t_DRRITENU FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRRITENU <>0 and t_FLACON$"IPZ" and not deleted() to this.w_TOTMPRE
        this.w_GSVE_MDV = This.oParentObject.oParentObject
        this.w_GSVE_MDV.w_MVTOTRIT = this.w_TOTMRIT
        this.w_CONTROL = this.w_GSVE_MDV.GetCtrl( "w_MVTOTRIT" )
        this.w_CONTROL.Value = this.w_GSVE_MDV.w_MVTOTRIT
        this.w_GSVE_MDV.w_MVRITPRE = this.w_TOTMPRE
        if this.oParentObject.w_TIPCLF="F"
          this.w_CONTROL = this.w_GSVE_MDV.GetCtrl( "w_MVRITPRE" )
          this.w_CONTROL.Value = this.w_GSVE_MDV.w_MVRITPRE
        endif
      endif
      this.oParentObject.w_DRESIDUO = this.oParentObject.w_DRSOGGE - this.w_TOTIMP - this.w_NONSOG
      * --- Utilizzo questa variabile per avere il totale delle somme non soggette.
      this.oParentObject.w_DRNSOGGI = this.w_NONSOG
      if this.w_TIPRITE="C"
        this.oParentObject.w_DRESIDU1 = this.oParentObject.w_DRSOGGE - this.w_TOTINP - this.w_NONSOG1
        this.oParentObject.w_DRNSOGGP = this.w_NONSOG1
      else
        this.oParentObject.w_DRESIDU1 = 0
      endif
      this.oParentObject.w_DRTOTRIT = w_TOTALE 
      this.oParentObject.w_DRDAVERS = this.oParentObject.w_DRTOTDOC - w_TOTALE - this.oParentObject.w_TOTENA
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.pParame$"PT-DT"
      this.oParentObject.w_DRDAVERS = this.oParentObject.w_DRTOTDOC -this.oParentObject.w_DRTOTRIT - this.oParentObject.w_TOTENA
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancello il transitorio con i dati caricati.
    SELECT (this.oParentObject.cTrsName)
    Delete All
    this.oParentObject.InitRow()
    this.oParentObject.MarkPos()
    * --- Se il fornitore � soggetto sia al contributo I.R.PE.F che al contributo I.N.P.S. 
    *     devo inserire due righe differenti altrimenti una unica riga
    this.w_PERCEN = IIF(this.oParentObject.w_DRPERRIT<=0, this.oParentObject.w_PERINPS, this.oParentObject.w_DRPERRIT)
    this.oParentObject.w_DRCODTRI = this.w_CODIRP
    this.oParentObject.w_VCODTRI = this.w_CODIRP
    this.oParentObject.w_DRCODCAU = this.w_CAURIT
    this.oParentObject.w_FLACON = "R"
    if this.oParentObject.w_DRTIPFOR = "A"
      * --- Se ho riempito le somme non soggette, non devo rieffettuare il calcolo per la percentuale imponibile
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGPERIMP,AGRITIRP"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODFOR = "+cp_ToStrODBC(this.w_CODCLF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGPERIMP,AGRITIRP;
          from (i_cTable) where;
              AGCODFOR = this.w_CODCLF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AGEIMPO = NVL(cp_ToDate(_read_.AGPERIMP),cp_NullValue(_read_.AGPERIMP))
        this.w_AGERITE = NVL(cp_ToDate(_read_.AGRITIRP),cp_NullValue(_read_.AGRITIRP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_DRIMPONI = cp_ROUND(this.w_AGEIMPO/100*(this.oParentObject.w_DRSOGGE),this.oParentObject.w_DECTOP)
      this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*this.w_AGERITE/100,this.oParentObject.w_DECTOP)
      this.w_NONSOGGCA = this.oParentObject.w_DRSOGGE - this.oParentObject.w_DRIMPONI
      this.oParentObject.w_DRPERRIT = this.w_AGERITE
      this.oParentObject.w_DRRIINPS = this.oParentObject.w_RITINPS
    else
      vq_exec("..\RITE\EXE\QUERY\GSRI_BRT", this,"PERCENTUALI")
      SELECT PERCENTUALI
      GO BOTTOM
      if RECCOUNT("PERCENTUALI") > 0
        this.oParentObject.w_DRIMPONI = cp_ROUND(IMPONIBILE/100*this.oParentObject.w_DRSOGGE,this.oParentObject.w_DECTOP)
        this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*RITENUTA/100,this.oParentObject.w_DECTOP)
        this.w_NONSOGGCA = this.oParentObject.w_DRSOGGE - this.oParentObject.w_DRIMPONI
        this.oParentObject.w_DRPERRIT = RITENUTA
        this.oParentObject.w_DRRIINPS = this.oParentObject.w_RITINPS
      else
        ah_ErrorMsg("Codice tributo non definito in tabella",,"")
      endif
      if used ("PERCENTUALI")
        select PERCENTUALI
        use
      endif
    endif
    this.oParentObject.w_DRNONSOG = this.w_NONSOGGCA
    this.oParentObject.w_DRCODSNS = IIF(this.oParentObject.w_TIPCLF="C" OR this.oParentObject.w_DRNONSOG=0," ",IIF(EMPTY(this.oParentObject.w_CODSNS),"7",this.oParentObject.w_CODSNS))
    this.oParentObject.w_MODIFICA = IIF(EMPTY(this.oParentObject.w_DRCODTRI),"S","N")
    if this.w_TIPRITE="C" And Not empty(this.w_CODTR2)
      * --- Fornitore soggetto sia ad I.R.PE.F. che I.N.P.S.
      *     Devo inserire prima la riga IRPEF e poi successivamente la riga INPS
      * --- Ricalcolo sempre l'importo da versare
      this.oParentObject.TrsFromWork()
      this.oParentObject.InitRow()
      this.oParentObject.w_MODIFICA = "N"
      this.oParentObject.w_DRCODTRI = this.w_CODTR2
      this.oParentObject.w_VCODTRI = this.w_CODTR2
      this.oParentObject.w_DRCODCAU = "A"
      this.oParentObject.w_FLACON = "Z"
      * --- Calcolo Imponibile INPS
      this.oParentObject.w_DRIMPONI = cp_ROUND((this.oParentObject.w_DRSOGGE) * this.oParentObject.w_IMPINPS/100,this.oParentObject.w_DECTOP)
      this.w_NONSOGGCA = this.oParentObject.w_DRSOGGE - this.oParentObject.w_DRIMPONI
      this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*(this.w_PERCEN)/100,this.oParentObject.w_DECTOP)
      this.oParentObject.w_DRPERRIT = this.oParentObject.w_PERINPS
      this.oParentObject.w_DRRIINPS = this.oParentObject.w_RITINPS
      this.oParentObject.w_DRNONSOG = this.w_NONSOGGCA
      this.oParentObject.w_DRCODSNS = " "
    endif
    this.oParentObject.TrsFromWork()
    this.oParentObject.RePos()
    if LEFT(this.pParame,1)="D"
      this.pParame="DC"
    else
      this.pParame="PC"
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_TOTIMP = 0
    this.w_NONSOG = 0
    SUM t_DRNONSOG FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRNONSOG <>0 and t_FLACON="R" and not deleted() to this.w_NONSOG
    SUM t_DRIMPONI FOR NOT EMPTY(NVL(t_DRCODTRI," ")) AND NOT EMPTY(NVL(t_DRCODCAU," ")) ; 
 AND t_DRIMPONI <>0 and t_FLACON="R" and not deleted() to this.w_TOTIMP
    if this.oParentObject.w_DRTIPFOR = "A"
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGPERIMP,AGRITIRP"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODFOR = "+cp_ToStrODBC(this.w_CODCLF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGPERIMP,AGRITIRP;
          from (i_cTable) where;
              AGCODFOR = this.w_CODCLF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AGEIMPO = NVL(cp_ToDate(_read_.AGPERIMP),cp_NullValue(_read_.AGPERIMP))
        this.w_AGERITE = NVL(cp_ToDate(_read_.AGRITIRP),cp_NullValue(_read_.AGRITIRP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_DRIMPONI = (this.oParentObject.w_DRSOGGE+ this.oParentObject.w_DRIMPONI-this.w_NONSOG - this.w_TOTIMP)
      this.oParentObject.w_DRPERRIT = IIF(this.oParentObject.w_DRPERRIT<=0,this.w_AGERITE,this.oParentObject.w_DRPERRIT)
      this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*this.oParentObject.w_DRPERRIT/100,this.oParentObject.w_DECTOP)
      this.oParentObject.w_DRRIINPS = IIF(this.oParentObject.w_DRRIINPS<=0,this.oParentObject.w_RITINPS,this.oParentObject.w_DRRIINPS)
    else
      vq_exec("..\RITE\EXE\QUERY\GSRI_BRT", this,"PERCENTUALI")
      SELECT PERCENTUALI
      GO BOTTOM
      if RECCOUNT("PERCENTUALI") > 0
        this.oParentObject.w_DRIMPONI = (this.oParentObject.w_DRSOGGE+this.oParentObject.w_DRIMPONI-this.w_NONSOG - this.w_TOTIMP)
        this.oParentObject.w_DRPERRIT = IIF(this.oParentObject.w_DRPERRIT<=0,RITENUTA,this.oParentObject.w_DRPERRIT)
        this.oParentObject.w_DRRITENU = cp_ROUND(this.oParentObject.w_DRIMPONI*this.oParentObject.w_DRPERRIT/100,this.oParentObject.w_DECTOP)
        this.oParentObject.w_DRRIINPS = IIF(this.oParentObject.w_DRRIINPS<=0,this.oParentObject.w_RITINPS,this.oParentObject.w_DRRIINPS)
      else
        ah_ErrorMsg("Codice tributo non definito in tabella",,"")
      endif
      if used ("PERCENTUALI")
        select PERCENTUALI
        use
      endif
    endif
    this.oParentObject.w_DRCODSNS = IIF(this.oParentObject.w_TIPCLF="C" OR this.oParentObject.w_DRNONSOG=0," ",IIF(NOT EMPTY(this.oParentObject.w_DRCODSNS),this.oParentObject.w_DRCODSNS,IIF(EMPTY(this.oParentObject.w_CODSNS),"7",this.oParentObject.w_CODSNS)))
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegno posizione per ritorno sul fondo dei controlli
    this.w_AbsRowMDR = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
    this.w_nRelRowMDR = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
    SELECT (this.oParentObject.cTrsName)
    this.w_RECPOSMDR = IIF( Eof() , RECNO(this.oParentObject.cTrsName)-1 , RECNO(this.oParentObject.cTrsName) )
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimetto il dettaglio Dati Ritenute nella posizione prima della scansione
     
 SELECT (this.oParentObject.cTrsName) 
 GO this.w_RECPOSMDR 
 With this.oParentObject 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow= this.w_AbsRowMDR 
 .oPgFrm.Page1.oPag.oBody.nRelRow= this.w_nRelRowMDR 
 EndWith
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegno posizione per ritorno sul fondo dei controlli
    With this.w_PADRE.GSCG_MIV
    this.w_AbsRowMIV = .oPgFrm.Page1.oPag.oBody.nAbsRow
    this.w_nRelRowMIV = .oPgFrm.Page1.oPag.oBody.nRelRow
    SELECT (.cTrsName)
    this.w_RECPOSMIV = IIF( Eof() , RECNO(.cTrsName)-1 , RECNO(.cTrsName) )
    Go Top
    this.w_IVAIND = 0
    this.oParentObject.w_DRIMPOSTA = 0
    SCAN FOR NOT EMPTY(NVL(t_IVCODIVA," ")) AND (t_IVIMPONI<>0 OR t_IVIMPIVA<>0)
    * --- Se il registro � acquisti e l'IVA non � omaggio o sconto
    if (t_CTIPREG="A" AND !t_IVCFLOMA$"SI" and this.oParentObject.w_TIPCLF="F") or (t_CTIPREG="V" AND !t_IVCFLOMA$"SI" and this.oParentObject.w_TIPCLF="C")
      this.w_IVAIND = this.w_IVAIND + cp_ROUND((t_IVIMPIVA*t_IVPERIND)/100, this.oParentObject.w_DECTOP)
    endif
    if (t_CTIPREG="A" And t_IVCFLOMA$"X-I" and this.oParentObject.w_TIPCLF="F") or (t_CTIPREG="V" And t_IVCFLOMA$"X-I" and this.oParentObject.w_TIPCLF="C")
      this.oParentObject.w_DRIMPOSTA = this.oParentObject.w_DRIMPOSTA + Nvl( t_IVIMPIVA , 0 )
    endif
    ENDSCAN
    * --- Rimetto il detttaglio IVA nella posizione prima della scansione
     
 SELECT (.cTrsName) 
 GO this.w_RECPOSMIV 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow= this.w_AbsRowMIV 
 .oPgFrm.Page1.oPag.oBody.nRelRow= this.w_nRelRowMIV
    EndWith
    * --- Se nota di credito cambio il segno dell'imposta
    this.oParentObject.w_DRIMPOSTA = this.oParentObject.w_DRIMPOSTA * IIF( this.w_PADRE.w_PNTIPDOC $ "NC-NE" ,-1, 1)
    this.w_IVAIND = this.w_IVAIND* IIF( this.w_PADRE.w_PNTIPDOC $ "NC-NE" ,-1, 1)
    this.oParentObject.w_DRTOTDOC = this.w_PADRE.w_TOTFOR
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_GSVE_MDV = This.oParentObject.oParentObject
    this.oParentObject.w_DRTOTDOC = this.w_GSVE_MDV.w_TOTFATTU
    this.oParentObject.w_DRIMPOSTA = this.w_GSVE_MDV.w_TOTIMPOS
    this.w_IVAIND = 0
    * --- Determino l'IVA indetraibile 1a Aliquota
    if Not Empty( this.w_GSVE_MDV.w_MVACIVA1 ) And IIF(this.w_GSVE_MDV.w_MVAFLOM1 $ "XI", this.w_GSVE_MDV.w_MVAIMPS1,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_GSVE_MDV.w_MVACIVA1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_GSVE_MDV.w_MVACIVA1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_GSVE_MDV.w_MVAIMPS1 * this.w_PERIND/100 , this.w_GSVE_MDV.w_DECTOT )
    endif
    * --- Determino l'IVA indetraibile 2a Aliquota
    if Not Empty( this.w_GSVE_MDV.w_MVACIVA2 ) And IIF(this.w_GSVE_MDV.w_MVAFLOM2 $ "XI", this.w_GSVE_MDV.w_MVAIMPS2,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_GSVE_MDV.w_MVACIVA2);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_GSVE_MDV.w_MVACIVA2;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_GSVE_MDV.w_MVAIMPS2 * this.w_PERIND/100 , this.w_GSVE_MDV.w_DECTOT )
    endif
    * --- Determino l'IVA indetraibile 3a Aliquota
    if Not Empty( this.w_GSVE_MDV.w_MVACIVA3 ) And IIF(this.w_GSVE_MDV.w_MVAFLOM3 $ "XI", this.w_GSVE_MDV.w_MVAIMPS3,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_GSVE_MDV.w_MVACIVA3);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_GSVE_MDV.w_MVACIVA3;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_GSVE_MDV.w_MVAIMPS3 * this.w_PERIND/100 , this.w_GSVE_MDV.w_DECTOT )
    endif
    * --- Determino l'IVA indetraibile 4a Aliquota
    if Not Empty( this.w_GSVE_MDV.w_MVACIVA4 ) And IIF(this.w_GSVE_MDV.w_MVAFLOM4 $ "XI", this.w_GSVE_MDV.w_MVAIMPS4,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_GSVE_MDV.w_MVACIVA4);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_GSVE_MDV.w_MVACIVA4;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_GSVE_MDV.w_MVAIMPS4 * this.w_PERIND/100 , this.w_GSVE_MDV.w_DECTOT )
    endif
    * --- Determino l'IVA indetraibile 5a Aliquota
    if Not Empty( this.w_GSVE_MDV.w_MVACIVA5 ) And IIF(this.w_GSVE_MDV.w_MVAFLOM5 $ "XI", this.w_GSVE_MDV.w_MVAIMPS5,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_GSVE_MDV.w_MVACIVA5);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_GSVE_MDV.w_MVACIVA5;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_GSVE_MDV.w_MVAIMPS5 * this.w_PERIND/100 , this.w_GSVE_MDV.w_DECTOT )
    endif
    * --- Determino l'IVA indetraibile 6a Aliquota
    if Not Empty( this.w_GSVE_MDV.w_MVACIVA6 ) And IIF(this.w_GSVE_MDV.w_MVAFLOM6 $ "XI", this.w_GSVE_MDV.w_MVAIMPS6,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_GSVE_MDV.w_MVACIVA6);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_GSVE_MDV.w_MVACIVA6;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_GSVE_MDV.w_MVAIMPS6 * this.w_PERIND/100 , this.w_GSVE_MDV.w_DECTOT )
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPARAME)
    this.pPARAME=pPARAME
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='TRI_BUTI'
    this.cWorkTables[4]='VOCIIVA'
    this.cWorkTables[5]='DATIRITE'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAME"
endproc
