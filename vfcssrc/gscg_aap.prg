* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aap                                                        *
*              Valutazione att./pass. in divisa                                *
*                                                                              *
*      Author: ZUCCHETTI                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-11                                                      *
* Last revis.: 2012-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_aap"))

* --- Class definition
define class tgscg_aap as StdForm
  Top    = 3
  Left   = 7

  * --- Standard Properties
  Width  = 780
  Height = 465+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-14"
  HelpContextID=125164183
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=51

  * --- Constant Properties
  VAL_ATPA_IDX = 0
  VALUTE_IDX = 0
  ESERCIZI_IDX = 0
  CAU_CONT_IDX = 0
  BUSIUNIT_IDX = 0
  AZIENDA_IDX = 0
  CONTI_IDX = 0
  CONTROPA_IDX = 0
  cFile = "VAL_ATPA"
  cKeySelect = "APSERIAL"
  cKeyWhere  = "APSERIAL=this.w_APSERIAL"
  cKeyWhereODBC = '"APSERIAL="+cp_ToStrODBC(this.w_APSERIAL)';

  cKeyWhereODBCqualified = '"VAL_ATPA.APSERIAL="+cp_ToStrODBC(this.w_APSERIAL)';

  cPrg = "gscg_aap"
  cComment = "Valutazione att./pass. in divisa"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZIENDA = space(5)
  w_VALUTA = space(3)
  w_ESERCIZI = space(4)
  w_APSERIAL = space(10)
  o_APSERIAL = space(10)
  w_APDATREG = ctod('  /  /  ')
  o_APDATREG = ctod('  /  /  ')
  w_APFLSOSP = space(1)
  w_APCAUCON = space(5)
  w_APBUSUNI = space(3)
  w_APDESCRI = space(50)
  w_APRAGSCR = space(1)
  w_APCLFOGE = space(1)
  w_APCODCON = space(15)
  w_APCODVAL = space(6)
  o_APCODVAL = space(6)
  w_APCAOVAL = 0
  w_CODAZI = space(5)
  w_GESTBUN = space(1)
  w_FINESE = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DTOBSO1 = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_DESVAL = space(35)
  w_CHKIVA = space(1)
  w_PARTITE = space(1)
  w_FLASALDI = space(1)
  w_DESCON = space(35)
  w_FLASALDF = space(1)
  w_RIFE = space(1)
  w_DESBUNIT = space(40)
  w_FLSELE = 0
  w_SELEZI = space(1)
  w_DECIMALI = 0
  w_DESCONTO = space(40)
  w_SERIALE = space(10)
  o_SERIALE = space(10)
  w_AGG_DETTAGLIO = space(20)
  o_AGG_DETTAGLIO = space(20)
  w_TIPCON = space(1)
  w_DIFNEG = space(15)
  w_FLPART = space(1)
  w_DATOBSO1 = ctod('  /  /  ')
  w_DIFPOS = space(15)
  w_DATOBSO2 = ctod('  /  /  ')
  w_FLANAL = space(1)
  w_ERR_MSG = space(256)
  w_DESDIFNEG = space(40)
  w_DESDIFPOS = space(40)
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTDATSCA = ctod('  /  /  ')
  w_PTCODVAL = space(3)
  w_PTMODPAG = space(10)
  w_PTNUMPAR = space(31)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_APSERIAL = this.W_APSERIAL
  w_ZOOMSCAD = .NULL.
  w_Movimenti = .NULL.
  w_Dettaglio = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_aap
  
  proc ecpSave()
  IF (this.cfunction='Edit')
  
  this.cfunction='Query'
  this.oFirstControl.SetFocus()
  this.SetStatus()
  this.mEnableControls()
  oCpToolBar.SetQuery()
  this.mcalc(.T.)
  this.ecpQuery()
  this.refresh()
  ELSE
  DODEFAULT()
  endif
  endproc
  
  
  proc ecpQuit()
  IF (this.cfunction='Edit')
  cp_YesNo(MSG_DISCARD_CHANGES_QP)
  this.cfunction='Query'
  this.oFirstControl.SetFocus()
  this.SetStatus()
  this.mEnableControls()
  oCpToolBar.SetQuery()
  this.mcalc(.T.)
  this.ecpQuery()
  this.refresh()
  ELSE
  DODEFAULT()
  endif
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VAL_ATPA','gscg_aap')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_aapPag1","gscg_aap",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Elaborazione")
      .Pages(1).HelpContextID = 247811760
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAPSERIAL_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZOOMSCAD = this.oPgFrm.Pages(1).oPag.ZOOMSCAD
      this.w_Movimenti = this.oPgFrm.Pages(1).oPag.Movimenti
      this.w_Dettaglio = this.oPgFrm.Pages(1).oPag.Dettaglio
      DoDefault()
    proc Destroy()
      this.w_ZOOMSCAD = .NULL.
      this.w_Movimenti = .NULL.
      this.w_Dettaglio = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='BUSIUNIT'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='CONTROPA'
    this.cWorkTables[8]='VAL_ATPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VAL_ATPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VAL_ATPA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_APSERIAL = NVL(APSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from VAL_ATPA where APSERIAL=KeySet.APSERIAL
    *
    i_nConn = i_TableProp[this.VAL_ATPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAL_ATPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VAL_ATPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VAL_ATPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VAL_ATPA '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'APSERIAL',this.w_APSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZIENDA = i_codazi
        .w_VALUTA = g_perval
        .w_ESERCIZI = g_codese
        .w_CODAZI = i_CODAZI
        .w_GESTBUN = space(1)
        .w_FINESE = ctod("  /  /  ")
        .w_DATOBSO = ctod("  /  /  ")
        .w_DTOBSO1 = ctod("  /  /  ")
        .w_DTOBSO = ctod("  /  /  ")
        .w_DESVAL = space(35)
        .w_CHKIVA = space(1)
        .w_PARTITE = space(1)
        .w_FLASALDI = space(1)
        .w_DESCON = space(35)
        .w_FLASALDF = space(1)
        .w_RIFE = space(1)
        .w_DESBUNIT = space(40)
        .w_FLSELE = 0
        .w_SELEZI = 'D'
        .w_DECIMALI = 0
        .w_DESCONTO = space(40)
        .w_TIPCON = 'G'
        .w_DIFNEG = space(15)
        .w_FLPART = space(1)
        .w_DATOBSO1 = ctod("  /  /  ")
        .w_DIFPOS = space(15)
        .w_DATOBSO2 = ctod("  /  /  ")
        .w_FLANAL = space(1)
        .w_ERR_MSG = space(256)
        .w_DESDIFNEG = space(40)
        .w_DESDIFPOS = space(40)
          .link_1_1('Load')
          .link_1_3('Load')
        .w_APSERIAL = NVL(APSERIAL,space(10))
        .op_APSERIAL = .w_APSERIAL
        .w_APDATREG = NVL(cp_ToDate(APDATREG),ctod("  /  /  "))
        .w_APFLSOSP = NVL(APFLSOSP,space(1))
        .w_APCAUCON = NVL(APCAUCON,space(5))
          if link_1_7_joined
            this.w_APCAUCON = NVL(CCCODICE107,NVL(this.w_APCAUCON,space(5)))
            this.w_DESCON = NVL(CCDESCRI107,space(35))
            this.w_CHKIVA = NVL(CCTIPREG107,space(1))
            this.w_PARTITE = NVL(CCFLPART107,space(1))
            this.w_FLASALDI = NVL(CCFLSALI107,space(1))
            this.w_FLASALDF = NVL(CCFLSALF107,space(1))
            this.w_RIFE = NVL(CCFLRIFE107,space(1))
            this.w_DTOBSO1 = NVL(cp_ToDate(CCDTOBSO107),ctod("  /  /  "))
            this.w_FLANAL = NVL(CCFLANAL107,space(1))
          else
          .link_1_7('Load')
          endif
        .w_APBUSUNI = NVL(APBUSUNI,space(3))
          .link_1_8('Load')
        .w_APDESCRI = NVL(APDESCRI,space(50))
        .w_APRAGSCR = NVL(APRAGSCR,space(1))
        .w_APCLFOGE = NVL(APCLFOGE,space(1))
        .w_APCODCON = NVL(APCODCON,space(15))
          if link_1_12_joined
            this.w_APCODCON = NVL(ANCODICE112,NVL(this.w_APCODCON,space(15)))
            this.w_DESCONTO = NVL(ANDESCRI112,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO112),ctod("  /  /  "))
          else
          .link_1_12('Load')
          endif
        .w_APCODVAL = NVL(APCODVAL,space(6))
          if link_1_13_joined
            this.w_APCODVAL = NVL(VACODVAL113,NVL(this.w_APCODVAL,space(6)))
            this.w_DESVAL = NVL(VADESVAL113,space(35))
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO113),ctod("  /  /  "))
            this.w_DECIMALI = NVL(VADECTOT113,0)
          else
          .link_1_13('Load')
          endif
        .w_APCAOVAL = NVL(APCAOVAL,0)
          .link_1_15('Load')
        .w_OBTEST = .w_APDATREG
        .oPgFrm.Page1.oPag.ZOOMSCAD.Calculate()
        .oPgFrm.Page1.oPag.Movimenti.Calculate()
        .w_SERIALE = .w_Movimenti.GETVAR('PNSERIAL')
        .w_AGG_DETTAGLIO = .w_SERIALE + .w_APSERIAL
        .oPgFrm.Page1.oPag.Dettaglio.Calculate()
          .link_1_53('Load')
          .link_1_56('Load')
        .w_PTTIPCON = .w_ZOOMSCAD.GETVAR('PTTIPCON')
        .w_PTCODCON = .w_ZOOMSCAD.GETVAR('PTCODCON')
        .w_PTDATSCA = .w_ZOOMSCAD.GETVAR('PTDATSCA')
        .w_PTCODVAL = .w_ZOOMSCAD.GETVAR('PTCODVAL')
        .w_PTMODPAG = iif(  g_APPLICATION = "ad hoc ENTERPRISE" ,  .w_ZOOMSCAD.GETVAR('PTMODPAG') ,'' )
        .w_PTNUMPAR = .w_ZOOMSCAD.GETVAR('PTNUMPAR')
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate(IIF(.cFunction <> 'Load','', AH_Msgformat("Differenza cambi %0negativa:")), RGB(255,0,0))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(IIF(.cFunction <> 'Load','', AH_Msgformat("Differenza cambi %0positiva:")), RGB(0,0,255))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'VAL_ATPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_69.enabled = this.oPgFrm.Page1.oPag.oBtn_1_69.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_aap
        * --- Devo aggiornare lo zoom di dettaglio
        THIS.w_SERIALE = this.w_Movimenti.GETVAR('PNSERIAL')
        this.w_AGG_DETTAGLIO = nvl(this.w_SERIALE,space(10)) + nvl(this.w_APSERIAL,space(10))
        this.oPgFrm.Page1.oPag.Dettaglio.Calculate(this.w_AGG_DETTAGLIO)
        gscg_bap(this,'VID')
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA = space(5)
      .w_VALUTA = space(3)
      .w_ESERCIZI = space(4)
      .w_APSERIAL = space(10)
      .w_APDATREG = ctod("  /  /  ")
      .w_APFLSOSP = space(1)
      .w_APCAUCON = space(5)
      .w_APBUSUNI = space(3)
      .w_APDESCRI = space(50)
      .w_APRAGSCR = space(1)
      .w_APCLFOGE = space(1)
      .w_APCODCON = space(15)
      .w_APCODVAL = space(6)
      .w_APCAOVAL = 0
      .w_CODAZI = space(5)
      .w_GESTBUN = space(1)
      .w_FINESE = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DTOBSO1 = ctod("  /  /  ")
      .w_DTOBSO = ctod("  /  /  ")
      .w_DESVAL = space(35)
      .w_CHKIVA = space(1)
      .w_PARTITE = space(1)
      .w_FLASALDI = space(1)
      .w_DESCON = space(35)
      .w_FLASALDF = space(1)
      .w_RIFE = space(1)
      .w_DESBUNIT = space(40)
      .w_FLSELE = 0
      .w_SELEZI = space(1)
      .w_DECIMALI = 0
      .w_DESCONTO = space(40)
      .w_SERIALE = space(10)
      .w_AGG_DETTAGLIO = space(20)
      .w_TIPCON = space(1)
      .w_DIFNEG = space(15)
      .w_FLPART = space(1)
      .w_DATOBSO1 = ctod("  /  /  ")
      .w_DIFPOS = space(15)
      .w_DATOBSO2 = ctod("  /  /  ")
      .w_FLANAL = space(1)
      .w_ERR_MSG = space(256)
      .w_DESDIFNEG = space(40)
      .w_DESDIFPOS = space(40)
      .w_PTTIPCON = space(1)
      .w_PTCODCON = space(15)
      .w_PTDATSCA = ctod("  /  /  ")
      .w_PTCODVAL = space(3)
      .w_PTMODPAG = space(10)
      .w_PTNUMPAR = space(31)
      if .cFunction<>"Filter"
        .w_AZIENDA = i_codazi
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AZIENDA))
          .link_1_1('Full')
          endif
        .w_VALUTA = g_perval
        .w_ESERCIZI = g_codese
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_ESERCIZI))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,4,.f.)
        .w_APDATREG = .w_FINESE
        .w_APFLSOSP = 'N'
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_APCAUCON))
          .link_1_7('Full')
          endif
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_APBUSUNI))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,9,.f.)
        .w_APRAGSCR = 'N'
        .w_APCLFOGE = 'C'
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_APCODCON))
          .link_1_12('Full')
          endif
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_APCODVAL))
          .link_1_13('Full')
          endif
        .w_APCAOVAL = IIF(EMPTY(.w_APCODVAL),0,GETCAM(.w_APCODVAL ,.w_APDATREG, 7))
        .w_CODAZI = i_CODAZI
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_CODAZI))
          .link_1_15('Full')
          endif
          .DoRTCalc(16,17,.f.)
        .w_OBTEST = .w_APDATREG
          .DoRTCalc(19,29,.f.)
        .w_FLSELE = 0
        .oPgFrm.Page1.oPag.ZOOMSCAD.Calculate()
        .w_SELEZI = 'D'
        .oPgFrm.Page1.oPag.Movimenti.Calculate()
          .DoRTCalc(32,33,.f.)
        .w_SERIALE = .w_Movimenti.GETVAR('PNSERIAL')
        .w_AGG_DETTAGLIO = .w_SERIALE + .w_APSERIAL
        .oPgFrm.Page1.oPag.Dettaglio.Calculate()
        .w_TIPCON = 'G'
        .DoRTCalc(37,37,.f.)
          if not(empty(.w_DIFNEG))
          .link_1_53('Full')
          endif
        .DoRTCalc(38,40,.f.)
          if not(empty(.w_DIFPOS))
          .link_1_56('Full')
          endif
          .DoRTCalc(41,45,.f.)
        .w_PTTIPCON = .w_ZOOMSCAD.GETVAR('PTTIPCON')
        .w_PTCODCON = .w_ZOOMSCAD.GETVAR('PTCODCON')
        .w_PTDATSCA = .w_ZOOMSCAD.GETVAR('PTDATSCA')
        .w_PTCODVAL = .w_ZOOMSCAD.GETVAR('PTCODVAL')
        .w_PTMODPAG = iif(  g_APPLICATION = "ad hoc ENTERPRISE" ,  .w_ZOOMSCAD.GETVAR('PTMODPAG') ,'' )
        .w_PTNUMPAR = .w_ZOOMSCAD.GETVAR('PTNUMPAR')
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate(IIF(.cFunction <> 'Load','', AH_Msgformat("Differenza cambi %0negativa:")), RGB(255,0,0))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(IIF(.cFunction <> 'Load','', AH_Msgformat("Differenza cambi %0positiva:")), RGB(0,0,255))
      endif
    endwith
    cp_BlankRecExtFlds(this,'VAL_ATPA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_69.enabled = this.oPgFrm.Page1.oPag.oBtn_1_69.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_aap
    ZAP IN ( this.w_Movimenti.cCursor )
    ZAP IN ( this.w_Dettaglio.cCursor )
    ZAP IN ( this.w_ZOOMSCAD.cCursor )
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VAL_ATPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAL_ATPA_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEVAL","i_codazi,w_APSERIAL")
      .op_codazi = .w_codazi
      .op_APSERIAL = .w_APSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAPSERIAL_1_4.enabled = i_bVal
      .Page1.oPag.oAPDATREG_1_5.enabled = i_bVal
      .Page1.oPag.oAPFLSOSP_1_6.enabled = i_bVal
      .Page1.oPag.oAPCAUCON_1_7.enabled = i_bVal
      .Page1.oPag.oAPBUSUNI_1_8.enabled = i_bVal
      .Page1.oPag.oAPDESCRI_1_9.enabled = i_bVal
      .Page1.oPag.oAPRAGSCR_1_10.enabled = i_bVal
      .Page1.oPag.oAPCLFOGE_1_11.enabled = i_bVal
      .Page1.oPag.oAPCODCON_1_12.enabled = i_bVal
      .Page1.oPag.oAPCODVAL_1_13.enabled = i_bVal
      .Page1.oPag.oAPCAOVAL_1_14.enabled = i_bVal
      .Page1.oPag.oSELEZI_1_41.enabled_(i_bVal)
      .Page1.oPag.oDIFNEG_1_53.enabled = i_bVal
      .Page1.oPag.oDIFPOS_1_56.enabled = i_bVal
      .Page1.oPag.oBtn_1_39.enabled = .Page1.oPag.oBtn_1_39.mCond()
      .Page1.oPag.oBtn_1_42.enabled = .Page1.oPag.oBtn_1_42.mCond()
      .Page1.oPag.oBtn_1_45.enabled = i_bVal
      .Page1.oPag.oBtn_1_47.enabled = .Page1.oPag.oBtn_1_47.mCond()
      .Page1.oPag.oBtn_1_51.enabled = .Page1.oPag.oBtn_1_51.mCond()
      .Page1.oPag.oBtn_1_57.enabled = .Page1.oPag.oBtn_1_57.mCond()
      .Page1.oPag.oBtn_1_69.enabled = .Page1.oPag.oBtn_1_69.mCond()
      .Page1.oPag.ZOOMSCAD.enabled = i_bVal
      .Page1.oPag.oObj_1_79.enabled = i_bVal
      .Page1.oPag.oObj_1_80.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oAPSERIAL_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oAPSERIAL_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'VAL_ATPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VAL_ATPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APSERIAL,"APSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APDATREG,"APDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APFLSOSP,"APFLSOSP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APCAUCON,"APCAUCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APBUSUNI,"APBUSUNI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APDESCRI,"APDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APRAGSCR,"APRAGSCR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APCLFOGE,"APCLFOGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APCODCON,"APCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APCODVAL,"APCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APCAOVAL,"APCAOVAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VAL_ATPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAL_ATPA_IDX,2])
    i_lTable = "VAL_ATPA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VAL_ATPA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VAL_ATPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAL_ATPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.VAL_ATPA_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEVAL","i_codazi,w_APSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VAL_ATPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VAL_ATPA')
        i_extval=cp_InsertValODBCExtFlds(this,'VAL_ATPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(APSERIAL,APDATREG,APFLSOSP,APCAUCON,APBUSUNI"+;
                  ",APDESCRI,APRAGSCR,APCLFOGE,APCODCON,APCODVAL"+;
                  ",APCAOVAL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_APSERIAL)+;
                  ","+cp_ToStrODBC(this.w_APDATREG)+;
                  ","+cp_ToStrODBC(this.w_APFLSOSP)+;
                  ","+cp_ToStrODBCNull(this.w_APCAUCON)+;
                  ","+cp_ToStrODBCNull(this.w_APBUSUNI)+;
                  ","+cp_ToStrODBC(this.w_APDESCRI)+;
                  ","+cp_ToStrODBC(this.w_APRAGSCR)+;
                  ","+cp_ToStrODBC(this.w_APCLFOGE)+;
                  ","+cp_ToStrODBCNull(this.w_APCODCON)+;
                  ","+cp_ToStrODBCNull(this.w_APCODVAL)+;
                  ","+cp_ToStrODBC(this.w_APCAOVAL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VAL_ATPA')
        i_extval=cp_InsertValVFPExtFlds(this,'VAL_ATPA')
        cp_CheckDeletedKey(i_cTable,0,'APSERIAL',this.w_APSERIAL)
        INSERT INTO (i_cTable);
              (APSERIAL,APDATREG,APFLSOSP,APCAUCON,APBUSUNI,APDESCRI,APRAGSCR,APCLFOGE,APCODCON,APCODVAL,APCAOVAL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_APSERIAL;
                  ,this.w_APDATREG;
                  ,this.w_APFLSOSP;
                  ,this.w_APCAUCON;
                  ,this.w_APBUSUNI;
                  ,this.w_APDESCRI;
                  ,this.w_APRAGSCR;
                  ,this.w_APCLFOGE;
                  ,this.w_APCODCON;
                  ,this.w_APCODVAL;
                  ,this.w_APCAOVAL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.VAL_ATPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAL_ATPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.VAL_ATPA_IDX,i_nConn)
      *
      * update VAL_ATPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'VAL_ATPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " APDATREG="+cp_ToStrODBC(this.w_APDATREG)+;
             ",APFLSOSP="+cp_ToStrODBC(this.w_APFLSOSP)+;
             ",APCAUCON="+cp_ToStrODBCNull(this.w_APCAUCON)+;
             ",APBUSUNI="+cp_ToStrODBCNull(this.w_APBUSUNI)+;
             ",APDESCRI="+cp_ToStrODBC(this.w_APDESCRI)+;
             ",APRAGSCR="+cp_ToStrODBC(this.w_APRAGSCR)+;
             ",APCLFOGE="+cp_ToStrODBC(this.w_APCLFOGE)+;
             ",APCODCON="+cp_ToStrODBCNull(this.w_APCODCON)+;
             ",APCODVAL="+cp_ToStrODBCNull(this.w_APCODVAL)+;
             ",APCAOVAL="+cp_ToStrODBC(this.w_APCAOVAL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'VAL_ATPA')
        i_cWhere = cp_PKFox(i_cTable  ,'APSERIAL',this.w_APSERIAL  )
        UPDATE (i_cTable) SET;
              APDATREG=this.w_APDATREG;
             ,APFLSOSP=this.w_APFLSOSP;
             ,APCAUCON=this.w_APCAUCON;
             ,APBUSUNI=this.w_APBUSUNI;
             ,APDESCRI=this.w_APDESCRI;
             ,APRAGSCR=this.w_APRAGSCR;
             ,APCLFOGE=this.w_APCLFOGE;
             ,APCODCON=this.w_APCODCON;
             ,APCODVAL=this.w_APCODVAL;
             ,APCAOVAL=this.w_APCAOVAL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VAL_ATPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAL_ATPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.VAL_ATPA_IDX,i_nConn)
      *
      * delete VAL_ATPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'APSERIAL',this.w_APSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VAL_ATPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAL_ATPA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,13,.t.)
        if .o_APCODVAL<>.w_APCODVAL.or. .o_APDATREG<>.w_APDATREG
            .w_APCAOVAL = IIF(EMPTY(.w_APCODVAL),0,GETCAM(.w_APCODVAL ,.w_APDATREG, 7))
        endif
        if .o_APSERIAL<>.w_APSERIAL
          .link_1_15('Full')
        endif
        .DoRTCalc(16,17,.t.)
        if .o_APDATREG<>.w_APDATREG
            .w_OBTEST = .w_APDATREG
        endif
        .oPgFrm.Page1.oPag.ZOOMSCAD.Calculate()
        .oPgFrm.Page1.oPag.Movimenti.Calculate()
        .DoRTCalc(19,33,.t.)
            .w_SERIALE = .w_Movimenti.GETVAR('PNSERIAL')
        if .o_SERIALE<>.w_SERIALE
            .w_AGG_DETTAGLIO = .w_SERIALE + .w_APSERIAL
        endif
        .oPgFrm.Page1.oPag.Dettaglio.Calculate()
        .DoRTCalc(36,45,.t.)
            .w_PTTIPCON = .w_ZOOMSCAD.GETVAR('PTTIPCON')
            .w_PTCODCON = .w_ZOOMSCAD.GETVAR('PTCODCON')
            .w_PTDATSCA = .w_ZOOMSCAD.GETVAR('PTDATSCA')
            .w_PTCODVAL = .w_ZOOMSCAD.GETVAR('PTCODVAL')
            .w_PTMODPAG = iif(  g_APPLICATION = "ad hoc ENTERPRISE" ,  .w_ZOOMSCAD.GETVAR('PTMODPAG') ,'' )
            .w_PTNUMPAR = .w_ZOOMSCAD.GETVAR('PTNUMPAR')
        if .o_AGG_DETTAGLIO<>.w_AGG_DETTAGLIO
          .Calculate_HUKJSNPAWP()
        endif
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate(IIF(.cFunction <> 'Load','', AH_Msgformat("Differenza cambi %0negativa:")), RGB(255,0,0))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(IIF(.cFunction <> 'Load','', AH_Msgformat("Differenza cambi %0positiva:")), RGB(0,0,255))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEVAL","i_codazi,w_APSERIAL")
          .op_APSERIAL = .w_APSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMSCAD.Calculate()
        .oPgFrm.Page1.oPag.Movimenti.Calculate()
        .oPgFrm.Page1.oPag.Dettaglio.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate(IIF(.cFunction <> 'Load','', AH_Msgformat("Differenza cambi %0negativa:")), RGB(255,0,0))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(IIF(.cFunction <> 'Load','', AH_Msgformat("Differenza cambi %0positiva:")), RGB(0,0,255))
    endwith
  return

  proc Calculate_BYNNNKRCSG()
    with this
          * --- Crea zoompart -gscg_bap (blank)
          gscg_bap(this;
              ,'Blank';
             )
    endwith
  endproc
  proc Calculate_BQSWNEPCAY()
    with this
          * --- Rimuove zoompart -gscg_bap (done)
          gscg_bap(this;
              ,'Done';
             )
    endwith
  endproc
  proc Calculate_UDYNGVBPNO()
    with this
          * --- Lancio gen.prima nota (gscg_bam)
          GSCG_BAM(this;
             )
    endwith
  endproc
  proc Calculate_QQOYOHUJPH()
    with this
          * --- Cancella p.nota (gscg_bap (del)
          GSCG_BAP(this;
              ,'DEL';
             )
    endwith
  endproc
  proc Calculate_UOKDIHWIXM()
    with this
          * --- Msg fine cancella p.nota (gscg_bap (finedel)
          GSCG_BAP(this;
              ,'FINEDEL';
             )
    endwith
  endproc
  proc Calculate_OFGXIOACAX()
    with this
          * --- Seleziona/deseleziona -gscg_bap (selez)
          gscg_bap(this;
              ,'SELEZ';
             )
    endwith
  endproc
  proc Calculate_HUKJSNPAWP()
    with this
          * --- Interroga dettaglio
          gscg_bap(this;
              ,'VID';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAPDATREG_1_5.enabled = this.oPgFrm.Page1.oPag.oAPDATREG_1_5.mCond()
    this.oPgFrm.Page1.oPag.oAPFLSOSP_1_6.enabled = this.oPgFrm.Page1.oPag.oAPFLSOSP_1_6.mCond()
    this.oPgFrm.Page1.oPag.oAPCAUCON_1_7.enabled = this.oPgFrm.Page1.oPag.oAPCAUCON_1_7.mCond()
    this.oPgFrm.Page1.oPag.oAPBUSUNI_1_8.enabled = this.oPgFrm.Page1.oPag.oAPBUSUNI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oAPDESCRI_1_9.enabled = this.oPgFrm.Page1.oPag.oAPDESCRI_1_9.mCond()
    this.oPgFrm.Page1.oPag.oAPRAGSCR_1_10.enabled = this.oPgFrm.Page1.oPag.oAPRAGSCR_1_10.mCond()
    this.oPgFrm.Page1.oPag.oAPCLFOGE_1_11.enabled = this.oPgFrm.Page1.oPag.oAPCLFOGE_1_11.mCond()
    this.oPgFrm.Page1.oPag.oAPCODCON_1_12.enabled = this.oPgFrm.Page1.oPag.oAPCODCON_1_12.mCond()
    this.oPgFrm.Page1.oPag.oAPCODVAL_1_13.enabled = this.oPgFrm.Page1.oPag.oAPCODVAL_1_13.mCond()
    this.oPgFrm.Page1.oPag.oAPCAOVAL_1_14.enabled = this.oPgFrm.Page1.oPag.oAPCAOVAL_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_69.enabled = this.oPgFrm.Page1.oPag.oBtn_1_69.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oAPFLSOSP_1_6.visible=!this.oPgFrm.Page1.oPag.oAPFLSOSP_1_6.mHide()
    this.oPgFrm.Page1.oPag.oAPBUSUNI_1_8.visible=!this.oPgFrm.Page1.oPag.oAPBUSUNI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oDESBUNIT_1_37.visible=!this.oPgFrm.Page1.oPag.oDESBUNIT_1_37.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_39.visible=!this.oPgFrm.Page1.oPag.oBtn_1_39.mHide()
    this.oPgFrm.Page1.oPag.oSELEZI_1_41.visible=!this.oPgFrm.Page1.oPag.oSELEZI_1_41.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_42.visible=!this.oPgFrm.Page1.oPag.oBtn_1_42.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_45.visible=!this.oPgFrm.Page1.oPag.oBtn_1_45.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_47.visible=!this.oPgFrm.Page1.oPag.oBtn_1_47.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_51.visible=!this.oPgFrm.Page1.oPag.oBtn_1_51.mHide()
    this.oPgFrm.Page1.oPag.oDIFNEG_1_53.visible=!this.oPgFrm.Page1.oPag.oDIFNEG_1_53.mHide()
    this.oPgFrm.Page1.oPag.oDIFPOS_1_56.visible=!this.oPgFrm.Page1.oPag.oDIFPOS_1_56.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_57.visible=!this.oPgFrm.Page1.oPag.oBtn_1_57.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_69.visible=!this.oPgFrm.Page1.oPag.oBtn_1_69.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMSCAD.Event(cEvent)
      .oPgFrm.Page1.oPag.Movimenti.Event(cEvent)
      .oPgFrm.Page1.oPag.Dettaglio.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_APBUSUNI Changed") or lower(cEvent)==lower("w_APCAOVAL Changed") or lower(cEvent)==lower("w_APCLFOGE Changed") or lower(cEvent)==lower("w_APCODCON Changed") or lower(cEvent)==lower("w_APCODVAL Changed") or lower(cEvent)==lower("w_APDATREG Changed")
          .Calculate_BYNNNKRCSG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_BQSWNEPCAY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Inserted")
          .Calculate_UDYNGVBPNO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete start")
          .Calculate_QQOYOHUJPH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Deleted")
          .Calculate_UOKDIHWIXM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_SELEZI Changed")
          .Calculate_OFGXIOACAX()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_79.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_80.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_81.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_82.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_GESTBUN = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_GESTBUN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCIZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCIZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCIZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCIZI);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_ESERCIZI)
            select ESCODAZI,ESCODESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCIZI = NVL(_Link_.ESCODESE,space(4))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCIZI = space(4)
      endif
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCIZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=APCAUCON
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_APCAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCDTOBSO,CCFLANAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_APCAUCON))
          select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCDTOBSO,CCFLANAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_APCAUCON)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_APCAUCON)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCDTOBSO,CCFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_APCAUCON)+"%");

            select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCDTOBSO,CCFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_APCAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oAPCAUCON_1_7'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCG1AAP.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCDTOBSO,CCFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCDTOBSO,CCFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCDTOBSO,CCFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_APCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_APCAUCON)
            select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCDTOBSO,CCFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCON = NVL(_Link_.CCDESCRI,space(35))
      this.w_CHKIVA = NVL(_Link_.CCTIPREG,space(1))
      this.w_PARTITE = NVL(_Link_.CCFLPART,space(1))
      this.w_FLASALDI = NVL(_Link_.CCFLSALI,space(1))
      this.w_FLASALDF = NVL(_Link_.CCFLSALF,space(1))
      this.w_RIFE = NVL(_Link_.CCFLRIFE,space(1))
      this.w_DTOBSO1 = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_FLANAL = NVL(_Link_.CCFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_APCAUCON = space(5)
      endif
      this.w_DESCON = space(35)
      this.w_CHKIVA = space(1)
      this.w_PARTITE = space(1)
      this.w_FLASALDI = space(1)
      this.w_FLASALDF = space(1)
      this.w_RIFE = space(1)
      this.w_DTOBSO1 = ctod("  /  /  ")
      this.w_FLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CHKIVA = 'N' AND .w_PARTITE = 'N' AND .w_FLASALDF = 'N' AND .w_FLASALDI = 'N' AND (.w_DTOBSO1 > .w_OBTEST OR EMPTY(.w_DTOBSO1)) AND .w_RIFE = 'N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile incongruente")
        endif
        this.w_APCAUCON = space(5)
        this.w_DESCON = space(35)
        this.w_CHKIVA = space(1)
        this.w_PARTITE = space(1)
        this.w_FLASALDI = space(1)
        this.w_FLASALDF = space(1)
        this.w_RIFE = space(1)
        this.w_DTOBSO1 = ctod("  /  /  ")
        this.w_FLANAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.CCCODICE as CCCODICE107"+ ",link_1_7.CCDESCRI as CCDESCRI107"+ ",link_1_7.CCTIPREG as CCTIPREG107"+ ",link_1_7.CCFLPART as CCFLPART107"+ ",link_1_7.CCFLSALI as CCFLSALI107"+ ",link_1_7.CCFLSALF as CCFLSALF107"+ ",link_1_7.CCFLRIFE as CCFLRIFE107"+ ",link_1_7.CCDTOBSO as CCDTOBSO107"+ ",link_1_7.CCFLANAL as CCFLANAL107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on VAL_ATPA.APCAUCON=link_1_7.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and VAL_ATPA.APCAUCON=link_1_7.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=APBUSUNI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APBUSUNI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KBU',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_APBUSUNI)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_AZIENDA;
                     ,'BUCODICE',trim(this.w_APBUSUNI))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_APBUSUNI)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BUDESCRI like "+cp_ToStrODBC(trim(this.w_APBUSUNI)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BUDESCRI like "+cp_ToStr(trim(this.w_APBUSUNI)+"%");
                   +" and BUCODAZI="+cp_ToStr(this.w_AZIENDA);

            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_APBUSUNI) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oAPBUSUNI_1_8'),i_cWhere,'GSAR_KBU',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APBUSUNI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_APBUSUNI);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_AZIENDA;
                       ,'BUCODICE',this.w_APBUSUNI)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APBUSUNI = NVL(_Link_.BUCODICE,space(3))
      this.w_DESBUNIT = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_APBUSUNI = space(3)
      endif
      this.w_DESBUNIT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GESTBUN $ 'ET'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_APBUSUNI = space(3)
        this.w_DESBUNIT = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APBUSUNI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=APCODCON
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_APCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_APCLFOGE);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_APCLFOGE;
                     ,'ANCODICE',trim(this.w_APCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_APCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_APCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_APCLFOGE);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_APCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_APCLFOGE);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_APCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAPCODCON_1_12'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/generici",'CONTISALD.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_APCLFOGE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_APCLFOGE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_APCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_APCLFOGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_APCLFOGE;
                       ,'ANCODICE',this.w_APCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCONTO = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_APCODCON = space(15)
      endif
      this.w_DESCONTO = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_APCODCON = space(15)
        this.w_DESCONTO = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.ANCODICE as ANCODICE112"+ ",link_1_12.ANDESCRI as ANDESCRI112"+ ",link_1_12.ANDTOBSO as ANDTOBSO112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on VAL_ATPA.APCODCON=link_1_12.ANCODICE"+" and VAL_ATPA.APCLFOGE=link_1_12.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and VAL_ATPA.APCODCON=link_1_12.ANCODICE(+)"'+'+" and VAL_ATPA.APCLFOGE=link_1_12.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=APCODVAL
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_APCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_APCODVAL))
          select VACODVAL,VADESVAL,VADTOBSO,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_APCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_APCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_APCODVAL)+"%");

            select VACODVAL,VADESVAL,VADTOBSO,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_APCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oAPCODVAL_1_13'),i_cWhere,'GSAR_AVL',"Valute",'GSCG_AAP.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADTOBSO,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_APCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_APCODVAL)
            select VACODVAL,VADESVAL,VADTOBSO,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APCODVAL = NVL(_Link_.VACODVAL,space(6))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_DECIMALI = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_APCODVAL = space(6)
      endif
      this.w_DESVAL = space(35)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_DECIMALI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) AND .w_APCODVAL<>.w_VALUTA
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice divisa incongruente")
        endif
        this.w_APCODVAL = space(6)
        this.w_DESVAL = space(35)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_DECIMALI = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.VACODVAL as VACODVAL113"+ ",link_1_13.VADESVAL as VADESVAL113"+ ",link_1_13.VADTOBSO as VADTOBSO113"+ ",link_1_13.VADECTOT as VADECTOT113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on VAL_ATPA.APCODVAL=link_1_13.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and VAL_ATPA.APCODVAL=link_1_13.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COSADIFC,COSADIFN";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COSADIFC,COSADIFN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_DIFPOS = NVL(_Link_.COSADIFC,space(15))
      this.w_DIFNEG = NVL(_Link_.COSADIFN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_DIFPOS = space(15)
      this.w_DIFNEG = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIFNEG
  func Link_1_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIFNEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DIFNEG)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_DIFNEG))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIFNEG)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DIFNEG)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DIFNEG)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DIFNEG) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDIFNEG_1_53'),i_cWhere,'GSAR_BZC',"Conti differenze cambi",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIFNEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DIFNEG);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_DIFNEG)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIFNEG = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDIFNEG = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO1 = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_FLPART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DIFNEG = space(15)
      endif
      this.w_DESDIFNEG = space(40)
      this.w_DATOBSO1 = ctod("  /  /  ")
      this.w_FLPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_FLPART<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_DIFNEG = space(15)
        this.w_DESDIFNEG = space(40)
        this.w_DATOBSO1 = ctod("  /  /  ")
        this.w_FLPART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIFNEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIFPOS
  func Link_1_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIFPOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DIFPOS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_DIFPOS))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIFPOS)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DIFPOS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DIFPOS)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DIFPOS) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDIFPOS_1_56'),i_cWhere,'GSAR_BZC',"Conti differenze cambi",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIFPOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DIFPOS);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_DIFPOS)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIFPOS = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDIFPOS = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO2 = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_FLPART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DIFPOS = space(15)
      endif
      this.w_DESDIFPOS = space(40)
      this.w_DATOBSO2 = ctod("  /  /  ")
      this.w_FLPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO2) OR .w_DATOBSO2>.w_OBTEST) AND .w_FLPART<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_DIFPOS = space(15)
        this.w_DESDIFPOS = space(40)
        this.w_DATOBSO2 = ctod("  /  /  ")
        this.w_FLPART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIFPOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAPSERIAL_1_4.value==this.w_APSERIAL)
      this.oPgFrm.Page1.oPag.oAPSERIAL_1_4.value=this.w_APSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oAPDATREG_1_5.value==this.w_APDATREG)
      this.oPgFrm.Page1.oPag.oAPDATREG_1_5.value=this.w_APDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oAPFLSOSP_1_6.RadioValue()==this.w_APFLSOSP)
      this.oPgFrm.Page1.oPag.oAPFLSOSP_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAPCAUCON_1_7.value==this.w_APCAUCON)
      this.oPgFrm.Page1.oPag.oAPCAUCON_1_7.value=this.w_APCAUCON
    endif
    if not(this.oPgFrm.Page1.oPag.oAPBUSUNI_1_8.value==this.w_APBUSUNI)
      this.oPgFrm.Page1.oPag.oAPBUSUNI_1_8.value=this.w_APBUSUNI
    endif
    if not(this.oPgFrm.Page1.oPag.oAPDESCRI_1_9.value==this.w_APDESCRI)
      this.oPgFrm.Page1.oPag.oAPDESCRI_1_9.value=this.w_APDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oAPRAGSCR_1_10.RadioValue()==this.w_APRAGSCR)
      this.oPgFrm.Page1.oPag.oAPRAGSCR_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAPCLFOGE_1_11.RadioValue()==this.w_APCLFOGE)
      this.oPgFrm.Page1.oPag.oAPCLFOGE_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAPCODCON_1_12.value==this.w_APCODCON)
      this.oPgFrm.Page1.oPag.oAPCODCON_1_12.value=this.w_APCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oAPCODVAL_1_13.value==this.w_APCODVAL)
      this.oPgFrm.Page1.oPag.oAPCODVAL_1_13.value=this.w_APCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oAPCAOVAL_1_14.value==this.w_APCAOVAL)
      this.oPgFrm.Page1.oPag.oAPCAOVAL_1_14.value=this.w_APCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_25.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_25.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_30.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_30.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBUNIT_1_37.value==this.w_DESBUNIT)
      this.oPgFrm.Page1.oPag.oDESBUNIT_1_37.value=this.w_DESBUNIT
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_41.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCONTO_1_44.value==this.w_DESCONTO)
      this.oPgFrm.Page1.oPag.oDESCONTO_1_44.value=this.w_DESCONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFNEG_1_53.value==this.w_DIFNEG)
      this.oPgFrm.Page1.oPag.oDIFNEG_1_53.value=this.w_DIFNEG
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFPOS_1_56.value==this.w_DIFPOS)
      this.oPgFrm.Page1.oPag.oDIFPOS_1_56.value=this.w_DIFPOS
    endif
    cp_SetControlsValueExtFlds(this,'VAL_ATPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(IIf( this.cFunction='Load' , GSCG_BAP( This ,'CHECKFORM' ) ,.T. ))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt(""+This.w_ERR_MSG+"")
          case   (empty(.w_APDATREG))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAPDATREG_1_5.SetFocus()
            i_bnoObbl = !empty(.w_APDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_APCAUCON)) or not(.w_CHKIVA = 'N' AND .w_PARTITE = 'N' AND .w_FLASALDF = 'N' AND .w_FLASALDI = 'N' AND (.w_DTOBSO1 > .w_OBTEST OR EMPTY(.w_DTOBSO1)) AND .w_RIFE = 'N'))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAPCAUCON_1_7.SetFocus()
            i_bnoObbl = !empty(.w_APCAUCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile incongruente")
          case   not(.w_GESTBUN $ 'ET')  and not(g_APPLICATION <> "ad hoc ENTERPRISE")  and (.cFunction='Load' and .w_GESTBUN<>'N')  and not(empty(.w_APBUSUNI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAPBUSUNI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and (.cFunction='Load')  and not(empty(.w_APCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAPCODCON_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   ((empty(.w_APCODVAL)) or not((EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) AND .w_APCODVAL<>.w_VALUTA))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAPCODVAL_1_13.SetFocus()
            i_bnoObbl = !empty(.w_APCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice divisa incongruente")
          case   (empty(.w_APCAOVAL))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAPCAOVAL_1_14.SetFocus()
            i_bnoObbl = !empty(.w_APCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DIFNEG)) or not((EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_FLPART<>'S'))  and not(.cFunction <> 'Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIFNEG_1_53.SetFocus()
            i_bnoObbl = !empty(.w_DIFNEG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   ((empty(.w_DIFPOS)) or not((EMPTY(.w_DATOBSO2) OR .w_DATOBSO2>.w_OBTEST) AND .w_FLPART<>'S'))  and not(.cFunction <> 'Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIFPOS_1_56.SetFocus()
            i_bnoObbl = !empty(.w_DIFPOS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_APSERIAL = this.w_APSERIAL
    this.o_APDATREG = this.w_APDATREG
    this.o_APCODVAL = this.w_APCODVAL
    this.o_SERIALE = this.w_SERIALE
    this.o_AGG_DETTAGLIO = this.w_AGG_DETTAGLIO
    return

enddefine

* --- Define pages as container
define class tgscg_aapPag1 as StdContainer
  Width  = 776
  height = 466
  stdWidth  = 776
  stdheight = 466
  resizeXpos=599
  resizeYpos=282
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAPSERIAL_1_4 as StdField with uid="IPWFNSLLNT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_APSERIAL", cQueryName = "APSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale elaborazione",;
    HelpContextID = 169844910,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=94, Left=109, Top=7, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oAPDATREG_1_5 as StdField with uid="CGJBRYTLAS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_APDATREG", cQueryName = "APDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione reg. prima nota e filtro su data registrazione partite",;
    HelpContextID = 251359053,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=345, Top=7

  func oAPDATREG_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oAPFLSOSP_1_6 as StdCheck with uid="KIJHYOQWBN",rtseq=6,rtrep=.f.,left=470, top=6, caption="Considera le partite sospese",;
    ToolTipText = "Considera le partite sospese",;
    HelpContextID = 67727530,;
    cFormVar="w_APFLSOSP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAPFLSOSP_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAPFLSOSP_1_6.GetRadio()
    this.Parent.oContained.w_APFLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oAPFLSOSP_1_6.SetRadio()
    this.Parent.oContained.w_APFLSOSP=trim(this.Parent.oContained.w_APFLSOSP)
    this.value = ;
      iif(this.Parent.oContained.w_APFLSOSP=='S',1,;
      0)
  endfunc

  func oAPFLSOSP_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oAPFLSOSP_1_6.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oAPCAUCON_1_7 as StdField with uid="TULVHHZHVZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_APCAUCON", cQueryName = "APCAUCON",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile incongruente",;
    ToolTipText = "Causale contabile del movimento che verr� generato",;
    HelpContextID = 267690156,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=34, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_APCAUCON"

  func oAPCAUCON_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oAPCAUCON_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oAPCAUCON_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAPCAUCON_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oAPCAUCON_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCG1AAP.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oAPCAUCON_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_APCAUCON
     i_obj.ecpSave()
  endproc

  add object oAPBUSUNI_1_8 as StdField with uid="BNHSIKSMMV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_APBUSUNI", cQueryName = "APBUSUNI",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Business Unit",;
    HelpContextID = 234926257,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=470, Top=34, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSAR_KBU", oKey_1_1="BUCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="BUCODICE", oKey_2_2="this.w_APBUSUNI"

  func oAPBUSUNI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' and .w_GESTBUN<>'N')
    endwith
   endif
  endfunc

  func oAPBUSUNI_1_8.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  func oAPBUSUNI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oAPBUSUNI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAPBUSUNI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oAPBUSUNI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KBU',"",'',this.parent.oContained
  endproc
  proc oAPBUSUNI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KBU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_AZIENDA
     i_obj.w_BUCODICE=this.parent.oContained.w_APBUSUNI
     i_obj.ecpSave()
  endproc

  add object oAPDESCRI_1_9 as StdField with uid="USMUKRELAL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_APDESCRI", cQueryName = "APDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione movimenti generati",;
    HelpContextID = 1085617,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=109, Top=62, InputMask=replicate('X',50)

  func oAPDESCRI_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oAPRAGSCR_1_10 as StdCheck with uid="NIHOYOPRCT",rtseq=10,rtrep=.f.,left=470, top=62, caption="Raggruppa scritture",;
    ToolTipText = "Raggruppa scritture di prima nota",;
    HelpContextID = 13873320,;
    cFormVar="w_APRAGSCR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAPRAGSCR_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAPRAGSCR_1_10.GetRadio()
    this.Parent.oContained.w_APRAGSCR = this.RadioValue()
    return .t.
  endfunc

  func oAPRAGSCR_1_10.SetRadio()
    this.Parent.oContained.w_APRAGSCR=trim(this.Parent.oContained.w_APRAGSCR)
    this.value = ;
      iif(this.Parent.oContained.w_APRAGSCR=='S',1,;
      0)
  endfunc

  func oAPRAGSCR_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc


  add object oAPCLFOGE_1_11 as StdCombo with uid="DJOXOASBPA",rtseq=11,rtrep=.f.,left=109,top=112,width=90,height=22;
    , ToolTipText = "Tipologia conto selezionata: cliente fornitore generico";
    , HelpContextID = 187064139;
    , cFormVar="w_APCLFOGE",RowSource=""+"Clienti,"+"Fornitori,"+"Conti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAPCLFOGE_1_11.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oAPCLFOGE_1_11.GetRadio()
    this.Parent.oContained.w_APCLFOGE = this.RadioValue()
    return .t.
  endfunc

  func oAPCLFOGE_1_11.SetRadio()
    this.Parent.oContained.w_APCLFOGE=trim(this.Parent.oContained.w_APCLFOGE)
    this.value = ;
      iif(this.Parent.oContained.w_APCLFOGE=='C',1,;
      iif(this.Parent.oContained.w_APCLFOGE=='F',2,;
      iif(this.Parent.oContained.w_APCLFOGE=='G',3,;
      0)))
  endfunc

  func oAPCLFOGE_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oAPCLFOGE_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_APCODCON)
        bRes2=.link_1_12('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oAPCODCON_1_12 as StdField with uid="OVCHXZBAEV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_APCODCON", cQueryName = "APCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Codice conto",;
    HelpContextID = 16162988,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=210, Top=112, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_APCLFOGE", oKey_2_1="ANCODICE", oKey_2_2="this.w_APCODCON"

  func oAPCODCON_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oAPCODCON_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oAPCODCON_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAPCODCON_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_APCLFOGE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_APCLFOGE)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAPCODCON_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/generici",'CONTISALD.CONTI_VZM',this.parent.oContained
  endproc
  proc oAPCODCON_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_APCLFOGE
     i_obj.w_ANCODICE=this.parent.oContained.w_APCODCON
     i_obj.ecpSave()
  endproc

  add object oAPCODVAL_1_13 as StdField with uid="VYXSNDRFIS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_APCODVAL", cQueryName = "APCODVAL",;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    sErrorMsg = "Codice divisa incongruente",;
    ToolTipText = "Codice divisa con cui effettuare l'operazione",;
    HelpContextID = 234266798,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=109, Top=140, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_APCODVAL"

  func oAPCODVAL_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oAPCODVAL_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oAPCODVAL_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAPCODVAL_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oAPCODVAL_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'GSCG_AAP.VALUTE_VZM',this.parent.oContained
  endproc
  proc oAPCODVAL_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_APCODVAL
     i_obj.ecpSave()
  endproc

  add object oAPCAOVAL_1_14 as StdField with uid="HVFHZSYDER",rtseq=14,rtrep=.f.,;
    cFormVar = "w_APCAOVAL", cQueryName = "APCAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio di valutazione",;
    HelpContextID = 223649966,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=541, Top=140, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oAPCAOVAL_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oDESVAL_1_25 as StdField with uid="ZCRJNNWVMA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 136227786,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=182, Top=140, InputMask=replicate('X',35)

  add object oDESCON_1_30 as StdField with uid="UULTHUASQZ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89238474,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=175, Top=34, InputMask=replicate('X',35)

  add object oDESBUNIT_1_37 as StdField with uid="QBLKUNUQPK",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESBUNIT", cQueryName = "DESBUNIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 185422986,;
   bGlobalFont=.t.,;
    Height=21, Width=255, Left=518, Top=34, InputMask=replicate('X',40)

  func oDESBUNIT_1_37.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc


  add object oBtn_1_39 as StdButton with uid="AITCBYAXMM",left=723, top=115, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue la ricerca";
    , HelpContextID = 234784490;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      with this.Parent.oContained
        GSCG_BAP(this.Parent.oContained,"ELA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_APCODVAL))
      endwith
    endif
  endfunc

  func oBtn_1_39.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (thisform.cFunction <> 'Load')
     endwith
    endif
  endfunc


  add object ZOOMSCAD as cp_szoombox with uid="FPGYLKAGYM",left=-2, top=165, width=778,height=253,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSCG_AAP",cTable="PAR_TITE",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.t.,bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 233709082

  add object oSELEZI_1_41 as StdRadio with uid="TOMWADIXXM",rtseq=31,rtrep=.f.,left=4, top=432, width=129,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_41.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 161487578
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 161487578
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_41.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_41.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_41.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_41.mHide()
    with this.Parent.oContained
      return (thisform.cFunction <> 'Load')
    endwith
  endfunc


  add object oBtn_1_42 as StdButton with uid="YKWJZXJWXN",left=676, top=421, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa";
    , HelpContextID = 132328998;
    , tabstop=.f., caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSCG_BAP(this.Parent.oContained,"STA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_42.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (thisform.cFunction <> 'Load')
     endwith
    endif
  endfunc

  add object oDESCONTO_1_44 as StdField with uid="NWXHLUBHEZ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCONTO", cQueryName = "DESCONTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 89238395,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=345, Top=112, InputMask=replicate('X',40)


  add object oBtn_1_45 as StdButton with uid="XXUGFHFUVX",left=727, top=421, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare il movimento di primanota";
    , HelpContextID = 265081209;
    , tabstop=.f., caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Load'  and not empty(.w_APCAUCON) and not empty(.w_DIFNEG) and not empty(.w_DIFPOS))
      endwith
    endif
  endfunc

  func oBtn_1_45.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (thisform.cFunction <> 'Load')
     endwith
    endif
  endfunc


  add object Movimenti as cp_zoombox with uid="FAAVLVDFWB",left=-2, top=165, width=403,height=253,;
    caption='Movimenti',;
   bGlobalFont=.t.,;
    cZoomFile="GSCG_AAP",bOptions=.t.,bAdvOptions=.f.,cTable="PNT_MAST",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",cZoomOnZoom="GSZM_BCC",bRetriveAllRows=.f.,bAdvanced=.t.,;
    cEvent = "Load, After_Del_PN,Blank,RefreshZoom",;
    nPag=1;
    , HelpContextID = 206129718


  add object oBtn_1_47 as StdButton with uid="RQNLRGWUBZ",left=727, top=421, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare il movimento di primanota selezionato";
    , HelpContextID = 59122874;
    , tabstop=.f., caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      with this.Parent.oContained
        GSCG_BAP(this.Parent.oContained,"ELP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit'  and not empty(.w_SERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_47.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (thisform.cFunction <> 'Edit')
     endwith
    endif
  endfunc


  add object Dettaglio as cp_zoombox with uid="ZCSUEYMKTV",left=394, top=165, width=382,height=253,;
    caption='Dettaglio',;
   bGlobalFont=.t.,;
    cZoomFile="GSCG_AAP",bQueryOnDblClick=FALSE,bOptions=.f.,bAdvOptions=.f.,cTable="PNT_DETT",bRetriveAllRows=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",cZoomOnZoom="GSZM_BCC",;
    cEvent = "RefreshDettaglio",;
    nPag=1;
    , HelpContextID = 184448113


  add object oBtn_1_51 as StdButton with uid="FQBWVAYHQN",left=676, top=421, width=48,height=45,;
    CpPicture="bmp\VERIFICA.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione di prima nota";
    , HelpContextID = 265687562;
    , tabstop=.f., caption='\<Reg. Cont.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_SERIALE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_51.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_COGE='S' AND NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_51.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (thisform.cFunction <> 'Query')
     endwith
    endif
  endfunc

  add object oDIFNEG_1_53 as StdField with uid="KAZUDYTJQB",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DIFNEG", cQueryName = "DIFNEG",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto utilizzato per rilevare una differenza cambi",;
    HelpContextID = 216496074,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=244, Top=438, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DIFNEG"

  func oDIFNEG_1_53.mHide()
    with this.Parent.oContained
      return (.cFunction <> 'Load')
    endwith
  endfunc

  func oDIFNEG_1_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_53('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIFNEG_1_53.ecpDrop(oSource)
    this.Parent.oContained.link_1_53('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIFNEG_1_53.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDIFNEG_1_53'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti differenze cambi",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oDIFNEG_1_53.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DIFNEG
     i_obj.ecpSave()
  endproc

  add object oDIFPOS_1_56 as StdField with uid="IDDRPQRVFG",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DIFPOS", cQueryName = "DIFPOS",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto utilizzato per rilevare una differenza cambi",;
    HelpContextID = 4552650,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=488, Top=438, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DIFPOS"

  func oDIFPOS_1_56.mHide()
    with this.Parent.oContained
      return (.cFunction <> 'Load')
    endwith
  endfunc

  func oDIFPOS_1_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_56('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIFPOS_1_56.ecpDrop(oSource)
    this.Parent.oContained.link_1_56('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIFPOS_1_56.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDIFPOS_1_56'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti differenze cambi",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oDIFPOS_1_56.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DIFPOS
     i_obj.ecpSave()
  endproc


  add object oBtn_1_57 as StdButton with uid="PDUIORDUOE",left=727, top=421, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa";
    , HelpContextID = 132328998;
    , tabstop=.f., caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      with this.Parent.oContained
        GSCG_BAP(this.Parent.oContained,"INT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_57.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DIFNEG) AND NOT EMPTY(.w_DIFPOS))
      endwith
    endif
  endfunc

  func oBtn_1_57.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (thisform.cFunction <> 'Query')
     endwith
    endif
  endfunc


  add object oBtn_1_69 as StdButton with uid="JGSHVKEKAT",left=625, top=421, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza registrazione alla quale � legata la scadenza che ha originato la valutazione";
    , HelpContextID = 208921062;
    , tabstop=.f., caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_69.Click()
      with this.Parent.oContained
        GSCG_BAP(this.Parent.oContained, "ORIGINE" )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_69.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_APSERIAL ) or Not Empty( .w_PTCODCON ))
      endwith
    endif
  endfunc

  func oBtn_1_69.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION" OR (Empty( .w_APSERIAL ) and Empty( .w_PTCODCON )) Or .w_APRAGSCR='S')
     endwith
    endif
  endfunc


  add object oObj_1_79 as cp_runprogram with uid="TYTYODRHBO",left=-5, top=570, width=261,height=21,;
    caption='GSCG_BAP(ORIGINE)',;
   bGlobalFont=.t.,;
    prg="GSCG_BAP( 'ORIGINE' )",;
    cEvent = "w_movimenti selected",;
    nPag=1;
    , HelpContextID = 43002101


  add object oObj_1_80 as cp_runprogram with uid="NSWTAVGHOJ",left=260, top=570, width=261,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BZP(w_SERIALE)",;
    cEvent = "w_dettaglio selected",;
    nPag=1;
    , HelpContextID = 233709082


  add object oObj_1_81 as cp_calclbl with uid="FTJWDDIHCA",left=135, top=432, width=109,height=29,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 233709082


  add object oObj_1_82 as cp_calclbl with uid="UAYATHYVSH",left=379, top=432, width=109,height=29,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 233709082

  add object oStr_1_18 as StdString with uid="JKIGFAPXNF",Visible=.t., Left=11, Top=8,;
    Alignment=1, Width=95, Height=18,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="VATGKCXHWY",Visible=.t., Left=3, Top=141,;
    Alignment=1, Width=103, Height=18,;
    Caption="Codice divisa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="KEJAIMIVNX",Visible=.t., Left=226, Top=8,;
    Alignment=1, Width=118, Height=18,;
    Caption="Data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="EKDQNDYBHV",Visible=.t., Left=0, Top=36,;
    Alignment=1, Width=106, Height=18,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="DGLJTBGROY",Visible=.t., Left=449, Top=141,;
    Alignment=1, Width=90, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="VENESXCBBC",Visible=.t., Left=31, Top=112,;
    Alignment=1, Width=75, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="ALDOBWVJQY",Visible=.t., Left=24, Top=64,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="LHQVXRBCVV",Visible=.t., Left=421, Top=36,;
    Alignment=1, Width=48, Height=15,;
    Caption="B.Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="OXKPLFLYEV",Visible=.t., Left=14, Top=87,;
    Alignment=0, Width=112, Height=18,;
    Caption="Filtri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_60 as StdBox with uid="QSXDBFELNG",left=6, top=102, width=665,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aap','VAL_ATPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".APSERIAL=VAL_ATPA.APSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
