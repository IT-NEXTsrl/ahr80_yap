* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bgp                                                        *
*              Gestione codice prezzo/peso variabile                           *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_100]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-05                                                      *
* Last revis.: 2016-02-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCodice
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bgp",oParentObject,m.pCodice)
return(i_retval)

define class tgsar_bgp as StdBatch
  * --- Local variables
  pCodice = space(20)
  w_TEST = space(20)
  w_TIPBAR = space(1)
  w_FLPECO = space(1)
  w_PREZZO = 0
  w_QTA = 0
  w_TmpN = 0
  w_KEYART = space(20)
  w_CODART = space(20)
  w_OBJ_GEST = .NULL.
  w_NUMDEQ = space(1)
  w_StrDec = space(13)
  w_StrInt = space(13)
  w_STRINGA = 0
  w_DATREG = ctod("  /  /  ")
  w_CODVAA = space(3)
  w_VALUCA = 0
  w_PROG = space(3)
  w_CODIVA = space(5)
  w_PERIVA = 0
  w_CALPRZ = 0
  w_CATART = 0
  w_GRUMER = space(5)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_APPO = space(16)
  w_PREZUM = space(1)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_CLUNIMIS = space(3)
  w_NOMEVAR = space(50)
  w_NOMEFIELD = space(10)
  w_OBJCTRL = .NULL.
  w_OBJCTRL = .NULL.
  w_OSOURCE = .NULL.
  * --- WorkFile variables
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  SALDIART_idx=0
  VOCIIVA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione per scorporare il codice ed il prezzo/Qt� dal codice inserito
    *     Utilizzato per gestire i codici EAN13 da Bilance
    *     
    *     Lanciato dalla After Input � invocato all'interno della Valid del control
    *     
    *     Lanciato da Documenti Acquisto, Documenti di Vendita Ordini e Vendita negozio (P.O.S.)
    this.pCodice = Alltrim( this.pCodice )
    this.w_OBJ_GEST = This.oParentObject
    * --- Se inizia per due � lungo 13 (EAN13) ed � composto di sole cifre allora
    *     potrebbe essere un codice bilancia
    if Left (this.pCodice,1)="2" And Len (this.pCodice)=13 And chknum(this.pCodice)
      * --- Se il codice esiste interrompo la codifica
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODICE,CANUMDEQ"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.pCodice);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODICE,CANUMDEQ;
          from (i_cTable) where;
              CACODICE = this.pCodice;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TEST = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
        this.w_NUMDEQ = NVL(cp_ToDate(_read_.CANUMDEQ),cp_NullValue(_read_.CANUMDEQ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Empty( this.w_TEST )
        * --- Estrapolo i caratteri dal secondo al settimo per determinare se � un articolo
        *     codificato senza cifra di controllo
        this.w_TEST = SubStr( this.pCodice , 2, 6 )
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODICE,CATIPBAR,CACODART,CANUMDEQ"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_TEST);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODICE,CATIPBAR,CACODART,CANUMDEQ;
            from (i_cTable) where;
                CACODICE = this.w_TEST;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TEST = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
          this.w_TIPBAR = NVL(cp_ToDate(_read_.CATIPBAR),cp_NullValue(_read_.CATIPBAR))
          this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          this.w_NUMDEQ = NVL(cp_ToDate(_read_.CANUMDEQ),cp_NullValue(_read_.CANUMDEQ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Se articolo non trovato o non senza cifra di controllo
        if Empty( this.w_TEST ) Or this.w_TIPBAR<>"A"
          * --- Estrapolo i caratteri dal secondo al sesto per determinare se � un articolo
          *     codificato con cifra di controllo
          this.w_TEST = SubStr( this.pCodice , 2, 5 )
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CACODICE,CATIPBAR,CACODART,CANUMDEQ"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_TEST);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CACODICE,CATIPBAR,CACODART,CANUMDEQ;
              from (i_cTable) where;
                  CACODICE = this.w_TEST;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TEST = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
            this.w_TIPBAR = NVL(cp_ToDate(_read_.CATIPBAR),cp_NullValue(_read_.CATIPBAR))
            this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
            this.w_NUMDEQ = NVL(cp_ToDate(_read_.CANUMDEQ),cp_NullValue(_read_.CANUMDEQ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Empty( this.w_TEST ) Or this.w_TIPBAR<>"B"
            * --- Esco la stringa inserita non � codificabile come EAN13 da bilancia
            i_retcode = 'stop'
            return
          endif
        endif
        * --- In w_TEST ho il codice di ricerca..
        this.w_KEYART = this.w_TEST
        this.w_TmpN = Val( Substr( this.pcodice ,8,5) ) 
        if this.w_TmpN<>0
          * --- Leggo sull'articolo la sua tipologia (Peso / Prezzo)
          *     Se PESO leggo dall'ottavo carattere al 12 ed eseguo una Val per
          *     ottenere la quantit�.
          *     Se � a PREZZO leggo dall'ottavo al 12-esimo carattere per determinare il
          *     valore globale della merce,determino il prezzo unitario, tramite l'usuale 
          *     elaborazione (con quantita impostata ad uno), determinato il prezzo 
          *     unitario e, se diverso da 0, determino la qt� come prezzo / prezzo unitario.
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARFLPECO"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARFLPECO;
              from (i_cTable) where;
                  ARCODART = this.w_CODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLPECO = NVL(cp_ToDate(_read_.ARFLPECO),cp_NullValue(_read_.ARFLPECO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Se articolo non gestito a prezzo o Peso allora esco..
          if not this.w_FLPECO $ "PZ"
            i_retcode = 'stop'
            return
          endif
          * --- Valorizzo sulla gestione il codice di ricerca, valorizzo la variabile direttamente...
          *     Non funziona chiamare la Valid (il batch � chiamato all'interno della valid)
          do case
            case Lower ( this.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.w_OBJ_GEST.class ) = "tgsac_mdv"
              this.oParentObject.w_MVCODICE = this.w_KEYART
              this.w_NOMEVAR = "w_MVCODICE"
            case Lower ( this.w_OBJ_GEST.class ) = "tgsps_mvd"
              this.oParentObject.w_MDCODICE = this.w_KEYART
              this.w_NOMEVAR = "w_MDCODICE"
            case Lower ( this.w_OBJ_GEST.class ) = "tgsma_mvm"
              this.oParentObject.w_MMCODICE = this.w_KEYART
              this.w_NOMEVAR = "w_MMCODICE"
            case Lower ( this.w_OBJ_GEST.class ) = "tgsar_mpg"
              this.oParentObject.w_GPCODICE = this.w_KEYART
              this.w_NOMEVAR = "w_GPCODICE"
          endcase
          * --- Eseguo il check e conseguentemente tutti i link...
          this.w_OBJCTRL = this.w_OBJ_GEST.GetBodyCtrl( this.w_NOMEVAR )
          this.w_OBJCTRL.Check()     
          * --- Eseguo eventuali assegnamenti a seguito del cambio di MxCODICE
          this.w_OBJ_GEST.mCalc(.T.)     
          do case
            case this.w_FLPECO="P"
              this.w_StrDec = ""
              this.w_StrInt = Substr( this.pcodice ,8,5)
              this.w_STRINGA = LEN(this.w_StrInt)
              do case
                case this.w_NUMDEQ="0"
                  * --- La quantit� � sempre intera..
                  this.w_QTA = Int( this.w_TmpN )
                case this.w_NUMDEQ="1"
                  * --- Quantit� con 1 decimale
                  this.w_StrDec = right( str( this.w_TmpN, 5, 1 ) , 1 )
                  this.w_QTA = ROUND(VAL(SUBSTR(this.w_STRINT,1,4)+","+SUBSTR(this.w_STRINT,5,1)),1)
                  * --- SUBSTR(w_STRINT,1,4)+',',SUBSTR(w_STRINT,4,1)
                case this.w_NUMDEQ="2"
                  * --- Quantit� con 2 decimali
                  this.w_StrDec = right( str( this.w_TmpN, 5, 2 ) , 2 )
                  this.w_QTA = ROUND(VAL(SUBSTR(this.w_STRINT,1,3)+","+SUBSTR(this.w_STRINT,4,2)),2)
                case this.w_NUMDEQ="3"
                  * --- Quantit� con 3 decimali
                  this.w_StrDec = right( str( this.w_TmpN, 5,3 ) , 3 )
                  this.w_QTA = ROUND(VAL(SUBSTR(this.w_STRINT,1,2)+","+SUBSTR(this.w_STRINT,3,3)),3)
              endcase
            case this.w_FLPECO="Z"
              * --- Se Prezzo allora calcolo il prezzo unitario impostanto come quantit� uno.
              this.w_QTA = 1
          endcase
          * --- Valorizzo la quantit�
          do case
            case Lower ( this.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.w_OBJ_GEST.class ) = "tgsac_mdv"
              this.w_NOMEVAR = "w_MVQTAMOV"
            case Lower ( this.w_OBJ_GEST.class ) = "tgsps_mvd"
              this.w_NOMEVAR = "w_MDQTAMOV"
              * --- Azzer� la quanti� per determinare il prezzo
              this.oParentObject.w_MDQTAMOV = 0
            case Lower ( this.w_OBJ_GEST.class ) = "tgsma_mvm"
              this.w_NOMEVAR = "w_MMQTAMOV"
            case Lower ( this.w_OBJ_GEST.class ) = "tgsar_mpg"
              this.w_NOMEVAR = "w_GPQTAMOV"
          endcase
          this.w_NOMEFIELD = "This.w_QTA"
          if Lower ( this.w_OBJ_GEST.class ) = "tgsps_mvd"
            this.oParentObject.w_NOCALQTA = .T.
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Calcolo il prezzo unitario per determinare la quantit� movimentata
          *     se codice bilancia con prezzo..
          if this.w_FLPECO="Z" 
            * --- Pongo a zero o_LIPREZZO per ricalcolare w_MVPREZZO...
            *     La procedura presuppone che tutte le gestioni ad essa collegate abbiamo
            *     questo campo
            if Lower ( this.w_OBJ_GEST.class ) <> "tgsar_mpg"
              this.oParentObject.o_LIPREZZO = 0
              this.w_OBJ_GEST.mCalc(.T.)     
            endif
            * --- Recupero il prezzo calcolato (valorizzo w_PREZZO)
            do case
              case Lower ( this.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.w_OBJ_GEST.class ) = "tgsac_mdv"
                this.w_PREZZO = this.oParentObject.w_MVPREZZO
              case Lower ( this.w_OBJ_GEST.class ) = "tgsps_mvd"
                this.w_PREZZO = this.oParentObject.w_MDPREZZO
              case Lower ( this.w_OBJ_GEST.class ) = "tgsma_mvm"
                this.w_PREZZO = this.oParentObject.w_MMPREZZO
              case Lower ( this.w_OBJ_GEST.class ) = "tgsar_mpg"
                DECLARE ARRCALC (16,1)
                * --- Azzero l'Array che verr� riempito dalla Funzione
                ARRCALC(1)=0
                * --- Nel caso del caricamento rapido, sulla gestione GSAR_MPG non sono disponibili
                *     i dati per calcolare il prezzo da listino.
                * --- Read from KEY_ARTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CAUNIMIS,CAMOLTIP,CAOPERAT"+;
                    " from "+i_cTable+" KEY_ARTI where ";
                        +"CACODICE = "+cp_ToStrODBC(this.w_KEYART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CAUNIMIS,CAMOLTIP,CAOPERAT;
                    from (i_cTable) where;
                        CACODICE = this.w_KEYART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
                  this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
                  this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from SALDIART
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "SLVALUCA,SLCODVAA"+;
                    " from "+i_cTable+" SALDIART where ";
                        +"SLCODICE = "+cp_ToStrODBC(this.w_CODART);
                        +" and SLCODMAG = "+cp_ToStrODBC(this.w_OBJ_GEST.w_GPCODMAG);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    SLVALUCA,SLCODVAA;
                    from (i_cTable) where;
                        SLCODICE = this.w_CODART;
                        and SLCODMAG = this.w_OBJ_GEST.w_GPCODMAG;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VALUCA = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
                  this.w_CODVAA = NVL(cp_ToDate(_read_.SLCODVAA),cp_NullValue(_read_.SLCODVAA))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from ART_ICOL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ARCODIVA,ARCATSCM,ARGRUMER,ARUNMIS1,ARUNMIS2,AROPERAT"+;
                    " from "+i_cTable+" ART_ICOL where ";
                        +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ARCODIVA,ARCATSCM,ARGRUMER,ARUNMIS1,ARUNMIS2,AROPERAT;
                    from (i_cTable) where;
                        ARCODART = this.w_CODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
                  this.w_CATART = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
                  this.w_GRUMER = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
                  this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                  this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
                  this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from VOCIIVA
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.VOCIIVA_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "IVPERIVA"+;
                    " from "+i_cTable+" VOCIIVA where ";
                        +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    IVPERIVA;
                    from (i_cTable) where;
                        IVCODIVA = this.w_CODIVA;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_UNMIS1 = NVL( this.w_UNMIS1 , SPACE( 3 ) )
                this.w_UNMIS2 = NVL( this.w_UNMIS2 , SPACE( 3 ) )
                this.w_UNMIS3 = NVL( this.w_UNMIS3 , SPACE( 3 ) )
                this.w_QTAUM3 = 0
                this.w_QTAUM2 = 0
                DIMENSION pArrUm[9]
                * --- Cerco il listino solo nell'unit� principale
                pArrUm [1] = "N" 
 pArrUm [2] = this.oParentObject.w_GPUNIMIS 
 pArrUm [3] = this.w_QTA 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_QTA 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
                do case
                  case Lower ( this.w_OBJ_GEST.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.w_OBJ_GEST.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.w_OBJ_GEST.w_OBJ_GEST.class ) = "tgsac_mdv"
                    this.w_DATREG = IIF(EMPTY(this.w_OBJ_GEST.w_OBJ_GEST.w_MVDATDOC), MAX(this.w_OBJ_GEST.w_OBJ_GEST.w_MVDATREG,this.w_OBJ_GEST.w_OBJ_GEST.w_MVDATEVA), MAX(this.w_OBJ_GEST.w_OBJ_GEST.w_MVDATDOC,this.w_OBJ_GEST.w_OBJ_GEST.w_MVDATEVA))
                    this.w_PROG = "V" + IIF(Empty(this.w_OBJ_GEST.w_OBJ_GEST.w_FLQRIO)," ",this.w_OBJ_GEST.w_OBJ_GEST.w_FLQRIO) + IIF(Empty(this.w_OBJ_GEST.w_OBJ_GEST.w_FLPREV)," ",this.w_OBJ_GEST.w_OBJ_GEST.w_FLPREV)+ " "
                    this.w_CALPRZ = CalPrzli( REPL("X",16) , this.w_OBJ_GEST.w_OBJ_GEST.w_MVTIPCON , this.w_OBJ_GEST.w_OBJ_GEST.w_MVTCOLIS , this.w_CODART , "XXXXXX" , this.w_QTA , this.w_OBJ_GEST.w_OBJ_GEST.w_MVCODVAL , this.w_OBJ_GEST.w_OBJ_GEST.w_MVCAOVAL , this.w_DatReg , this.w_OBJ_GEST.w_OBJ_GEST.w_CATSCC , "XXXXXX", this.w_CODVAA, this.w_OBJ_GEST.w_OBJ_GEST.w_XCONORN, this.w_OBJ_GEST.w_OBJ_GEST.w_CATCOM, this.w_OBJ_GEST.w_OBJ_GEST.w_MVFLSCOR, this.w_OBJ_GEST.w_OBJ_GEST.w_SCOLIS, this.w_VALUCA, this.w_PROG, @ARRCALC, this.w_OBJ_GEST.w_OBJ_GEST.w_PRZVAC, this.w_OBJ_GEST.w_OBJ_GEST.w_MVFLVEAC,"N", @pArrUm )
                    this.w_CODIVA = CALCODIV(this.w_CODART, this.w_OBJ_GEST.w_OBJ_GEST.w_MVTIPCON, this.w_OBJ_GEST.w_OBJ_GEST.w_XCONORN, this.w_OBJ_GEST.w_OBJ_GEST.w_MVTIPOPE, IIF( this.w_OBJ_GEST.w_OBJ_GEST.w_MVFLVEAC="V", this.w_OBJ_GEST.w_OBJ_GEST.w_MVDATREG , this.w_OBJ_GEST.w_OBJ_GEST.w_MVDATDOC ))
                    this.oParentObject.w_GPPREZZO = cp_Round(CALMMPZ(ARRCALC(5), this.w_QTA, this.w_QTA, IIF(ARRCALC(7)=2, this.w_OBJ_GEST.w_OBJ_GEST.w_IVALIS, "N"), IIF(this.w_OBJ_GEST.w_OBJ_GEST.w_MVFLSCOR="S", 0, this.w_PERIVA), this.w_OBJ_GEST.w_OBJ_GEST.w_DECUNI), this.w_OBJ_GEST.w_OBJ_GEST.w_DECUNI)
                  case Lower ( this.w_OBJ_GEST.w_OBJ_GEST.class ) = "tgsps_mvd"
                    this.w_DATREG = this.w_OBJ_GEST.w_OBJ_GEST.w_MDDATREG
                    this.w_CALPRZ = CalPrzli( this.w_OBJ_GEST.w_OBJ_GEST.w_MDCONTRA , "C" , this.w_OBJ_GEST.w_OBJ_GEST.w_MDCODLIS , this.w_CODART , this.w_GRUMER , this.w_QTA , this.w_OBJ_GEST.w_OBJ_GEST.w_MDCODVAL , this.w_OBJ_GEST.w_OBJ_GEST.w_MDCAOVAL , this.w_DatReg , this.w_OBJ_GEST.w_OBJ_GEST.w_CATSCC , this.w_CATART, this.w_CODVAA, this.w_OBJ_GEST.w_OBJ_GEST.w_CODCLI, this.w_OBJ_GEST.w_OBJ_GEST.w_CATCOM, " ", this.w_OBJ_GEST.w_OBJ_GEST.w_SCOLIS, this.w_VALUCA, "P", @ARRCALC, this.w_OBJ_GEST.w_OBJ_GEST.w_PRZVAC, "V","N", @pArrUm)
                    this.w_APPO = this.w_UNMIS1+this.w_UNMIS2+this.w_UNMIS3+this.oParentObject.w_GPUNIMIS+this.w_OPERAT+this.w_OPERA3+IIF(ARRCALC(7)=2, this.w_OBJ_GEST.w_OBJ_GEST.w_LISIVA, "N")+"P"+ALLTRIM(STR(this.w_OBJ_GEST.w_OBJ_GEST.w_DECUNI))
                    this.oParentObject.w_GPPREZZO = cp_Round(CALMMLIS(ARRCALC(5), this.w_APPO, this.w_MOLTIP, this.w_MOLTI3, 0),this.w_OBJ_GEST.w_OBJ_GEST.w_DECUNI)
                  case Lower ( this.w_OBJ_GEST.w_OBJ_GEST.class ) = "tgsma_mvm"
                    this.w_CALPRZ = CalPrzli( REPL("X",16) , " " , this.w_OBJ_GEST.w_OBJ_GEST.w_MMCODLIS , this.w_CODART , " " , this.w_QTA , this.w_OBJ_GEST.w_OBJ_GEST.w_MMCODVAL , this.w_OBJ_GEST.w_OBJ_GEST.w_MMCAOVAL , this.w_OBJ_GEST.w_OBJ_GEST.w_MMDATREG , this.w_OBJ_GEST.w_OBJ_GEST.w_CATSCC , this.w_CATART, this.w_OBJ_GEST.w_OBJ_GEST.w_MMCODVAL, " ", " ", " ", this.w_OBJ_GEST.w_OBJ_GEST.w_SCOLIS, 0,"M", @ARRCALC, " ", " ","N", @pArrUm )
                    this.oParentObject.w_GPPREZZO = cp_Round(CALMMPZ(ARRCALC(5), this.w_QTA, this.w_QTA, this.w_OBJ_GEST.w_OBJ_GEST.w_IVALIS, this.w_PERIVA, this.w_OBJ_GEST.w_OBJ_GEST.w_DECUNI), this.w_OBJ_GEST.w_OBJ_GEST.w_DECUNI)
                endcase
                this.w_PREZZO = this.oParentObject.w_GPPREZZO
            endcase
            if this.w_PREZZO<>0
              * --- Se unit� di misura non frazionabile arrotondo
              * --- Calcolo la quantit� moltiplicando il prezzo per 100 (la quantit� w_TMPN) �
              *     espressa con due decimali (236 per 2,36)
              if this.oParentObject.w_FLFRAZ="S"
                this.w_QTA = cp_Round( this.w_TmpN / ( this.w_PREZZO*100 ) , 0 )
              else
                this.w_QTA = this.w_TmpN / ( this.w_PREZZO*100 )
              endif
              * --- Rivalorizzo la Quantit� scrivo direttamente sul temporaneo per evitare ricalcoli..
              do case
                case Lower ( this.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.oParentObject.w_MVQTAMOV = this.w_QTA
                case Lower ( this.w_OBJ_GEST.class ) = "tgsps_mvd"
                  this.oParentObject.w_MDQTAMOV = this.w_QTA
                case Lower ( this.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.oParentObject.w_MMQTAMOV = this.w_QTA
                case Lower ( this.w_OBJ_GEST.class ) = "tgsar_mpg"
                  this.oParentObject.w_GPQTAMOV = this.w_QTA
              endcase
            endif
          endif
          * --- Sbianco la o_MxCODICE per eseguire i link sulla mCalc
          do case
            case Lower ( this.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.w_OBJ_GEST.class ) = "tgsac_mdv"
              this.oParentObject.o_MVCODICE = Space(20)
            case Lower ( this.w_OBJ_GEST.class ) = "tgsps_mvd"
              this.oParentObject.o_MDCODICE = Space(20)
              * --- Nel caso di movimenti POS non ricalcolo la qta se arriva da bilancia
              if this.w_FLPECO="Z" 
                this.oParentObject.w_NOCALQTA = this.w_QTA<>0
              else
                this.oParentObject.w_NOCALQTA = .T.
              endif
            case Lower ( this.w_OBJ_GEST.class ) = "tgsma_mvm"
              this.oParentObject.o_MMCODICE = Space(20)
            case Lower ( this.w_OBJ_GEST.class ) = "tgscg_mpg"
              this.oParentObject.o_GPCODICE = Space(20)
          endcase
        endif
      endif
    endif
    if Lower ( this.w_OBJ_GEST.class ) = "tgsps_mvd"
      * --- Da POS, il codice caricato da Emulatore Tastiera potrebbe essere una Fidelity
      *     Controllo prima se � un codice articolo. In questo caso mi comporto normalmente 
      *     e quindi non faccio niente
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.pCodice);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              CACODICE = this.pCodice;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows = 0
        * --- Se non � un codice articolo controllo se � una fidelity
        * --- Select from gsps_qcf
        do vq_exec with 'gsps_qcf',this,'_Curs_gsps_qcf','',.f.,.t.
        if used('_Curs_gsps_qcf')
          select _Curs_gsps_qcf
          locate for 1=1
          do while not(eof())
          this.w_NOMEVAR = "w_MDCODFID"
          this.w_NOMEFIELD = "pCodice"
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Valorizzo il campo per evitare che venga tirato su un articolo che inizia per il codice fidality
          *     ES:
          *     FC = 1234
          *     MDCODART: 12345
          this.w_OBJ_GEST.w_MDCODICE = "@@@@@#######@@@@@#####@@@@@"
            select _Curs_gsps_qcf
            continue
          enddo
          use
        endif
      endif
    endif
    * --- cmq non esegue mai la mCalc
    this.bUpdateParentObject=.f.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza Codice di ricerca, qt� ed eventualmente prezzo sulla gestione....
    this.w_OBJCTRL = this.w_OBJ_GEST.GetBodyCtrl( this.w_NOMEVAR )
     
 Local L_NOMEFIELD 
 L_NOMEFIELD= this.w_NOMEFIELD
    * --- Forzo i ricalcoli (nel caso i valori codificati coincidono con le quantit� proposte in automatico)
    this.w_OBJCTRL.bUpd = .T.
    this.w_OBJCTRL.Value = &L_NOMEFIELD
    this.w_OBJCTRL.Valid()     
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento Fidelity Card su POS tramite Emulazione Tastiera
    * --- Campo di testata (MDCODFID)
    this.w_OBJCTRL = this.w_OBJ_GEST.GetCtrl( this.w_NOMEVAR )
     
 Local L_NOMEFIELD 
 L_NOMEFIELD= this.w_NOMEFIELD
    this.w_OSOURCE.xKey( 1 ) = &L_NOMEFIELD
    * --- Valorizzo campo MDCODFID e suo link e dipendenze
    this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
    * --- Valorizzo questa propriet� di GSPS_MVD a .T. in modo che in questo caso
    *     fallendo la Valid su MDCODICE, non si sposti il focus sul campo successivo
    this.w_OBJ_GEST.w_Lost_Cod = .T.
    * --- Salvo il vecchio valore e lo reimposter� nel GSPS_BSF
    this.w_OBJ_GEST.w_Old_bDRE = this.w_OBJ_GEST.bDontReportError
    * --- Questa propriet� impedisce, Nella Valid(), di emettere il messaggio di errore di Articolo non valido
    this.w_OBJ_GEST.bDontReportError = .T.
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pCodice)
    this.pCodice=pCodice
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='SALDIART'
    this.cWorkTables[4]='VOCIIVA'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_gsps_qcf')
      use in _Curs_gsps_qcf
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCodice"
endproc
