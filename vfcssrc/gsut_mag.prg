* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mag                                                        *
*              Autorizzazioni gadget                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-15                                                      *
* Last revis.: 2015-05-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_mag")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_mag")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_mag")
  return

* --- Class definition
define class tgsut_mag as StdPCForm
  Width  = 292
  Height = 155
  Top    = 10
  Left   = 10
  cComment = "Autorizzazioni gadget"
  cPrg = "gsut_mag"
  HelpContextID=130630505
  add object cnt as tcgsut_mag
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_mag as PCContext
  w_AGCODGAD = space(10)
  w_AGTIPUTE = space(1)
  w_AGUTEGRP = 0
  w_AGUTEGRP = 0
  w_AGTIPNEG = space(1)
  w_DESUTE = space(50)
  w_DESGRP = space(50)
  proc Save(i_oFrom)
    this.w_AGCODGAD = i_oFrom.w_AGCODGAD
    this.w_AGTIPUTE = i_oFrom.w_AGTIPUTE
    this.w_AGUTEGRP = i_oFrom.w_AGUTEGRP
    this.w_AGUTEGRP = i_oFrom.w_AGUTEGRP
    this.w_AGTIPNEG = i_oFrom.w_AGTIPNEG
    this.w_DESUTE = i_oFrom.w_DESUTE
    this.w_DESGRP = i_oFrom.w_DESGRP
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_AGCODGAD = this.w_AGCODGAD
    i_oTo.w_AGTIPUTE = this.w_AGTIPUTE
    i_oTo.w_AGUTEGRP = this.w_AGUTEGRP
    i_oTo.w_AGUTEGRP = this.w_AGUTEGRP
    i_oTo.w_AGTIPNEG = this.w_AGTIPNEG
    i_oTo.w_DESUTE = this.w_DESUTE
    i_oTo.w_DESGRP = this.w_DESGRP
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsut_mag as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 292
  Height = 155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-05-20"
  HelpContextID=130630505
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  GAD_ACCE_IDX = 0
  cpusers_IDX = 0
  cpgroups_IDX = 0
  cFile = "GAD_ACCE"
  cKeySelect = "AGCODGAD"
  cKeyWhere  = "AGCODGAD=this.w_AGCODGAD"
  cKeyDetail  = "AGCODGAD=this.w_AGCODGAD and AGTIPUTE=this.w_AGTIPUTE and AGUTEGRP=this.w_AGUTEGRP and AGUTEGRP=this.w_AGUTEGRP"
  cKeyWhereODBC = '"AGCODGAD="+cp_ToStrODBC(this.w_AGCODGAD)';

  cKeyDetailWhereODBC = '"AGCODGAD="+cp_ToStrODBC(this.w_AGCODGAD)';
      +'+" and AGTIPUTE="+cp_ToStrODBC(this.w_AGTIPUTE)';
      +'+" and AGUTEGRP="+cp_ToStrODBC(this.w_AGUTEGRP)';
      +'+" and AGUTEGRP="+cp_ToStrODBC(this.w_AGUTEGRP)';

  cKeyWhereODBCqualified = '"GAD_ACCE.AGCODGAD="+cp_ToStrODBC(this.w_AGCODGAD)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsut_mag"
  cComment = "Autorizzazioni gadget"
  i_nRowNum = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AGCODGAD = space(10)
  w_AGTIPUTE = space(1)
  o_AGTIPUTE = space(1)
  w_AGUTEGRP = 0
  w_AGUTEGRP = 0
  w_AGTIPNEG = space(1)
  w_DESUTE = space(50)
  w_DESGRP = space(50)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_magPag1","gsut_mag",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='cpgroups'
    this.cWorkTables[3]='GAD_ACCE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GAD_ACCE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GAD_ACCE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_mag'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from GAD_ACCE where AGCODGAD=KeySet.AGCODGAD
    *                            and AGTIPUTE=KeySet.AGTIPUTE
    *                            and AGUTEGRP=KeySet.AGUTEGRP
    *                            and AGUTEGRP=KeySet.AGUTEGRP
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.GAD_ACCE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GAD_ACCE_IDX,2],this.bLoadRecFilter,this.GAD_ACCE_IDX,"gsut_mag")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GAD_ACCE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GAD_ACCE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GAD_ACCE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AGCODGAD',this.w_AGCODGAD  )
      select * from (i_cTable) GAD_ACCE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AGCODGAD = NVL(AGCODGAD,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'GAD_ACCE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESUTE = space(50)
          .w_DESGRP = space(50)
          .w_AGTIPUTE = NVL(AGTIPUTE,space(1))
          .w_AGUTEGRP = NVL(AGUTEGRP,0)
          .link_2_2('Load')
          .w_AGUTEGRP = NVL(AGUTEGRP,0)
          .link_2_3('Load')
          .w_AGTIPNEG = NVL(AGTIPNEG,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace AGTIPUTE with .w_AGTIPUTE
          replace AGUTEGRP with .w_AGUTEGRP
          replace AGUTEGRP with .w_AGUTEGRP
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_AGCODGAD=space(10)
      .w_AGTIPUTE=space(1)
      .w_AGUTEGRP=0
      .w_AGUTEGRP=0
      .w_AGTIPNEG=space(1)
      .w_DESUTE=space(50)
      .w_DESGRP=space(50)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_AGTIPUTE = 'U'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_AGUTEGRP))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_AGUTEGRP))
         .link_2_3('Full')
        endif
        .w_AGTIPNEG = 'V'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'GAD_ACCE')
    this.DoRTCalc(6,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'GAD_ACCE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GAD_ACCE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCODGAD,"AGCODGAD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_AGTIPUTE N(3);
      ,t_AGUTEGRP N(4);
      ,t_AGTIPNEG N(3);
      ,t_DESUTE C(50);
      ,t_DESGRP C(50);
      ,AGTIPUTE C(1);
      ,AGUTEGRP N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_magbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPUTE_2_1.controlsource=this.cTrsName+'.t_AGTIPUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_2.controlsource=this.cTrsName+'.t_AGUTEGRP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_3.controlsource=this.cTrsName+'.t_AGUTEGRP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPNEG_2_4.controlsource=this.cTrsName+'.t_AGTIPNEG'
    this.oPgFRm.Page1.oPag.oDESUTE_2_7.controlsource=this.cTrsName+'.t_DESUTE'
    this.oPgFRm.Page1.oPag.oDESGRP_2_8.controlsource=this.cTrsName+'.t_DESGRP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(82)
    this.AddVLine(134)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPUTE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GAD_ACCE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GAD_ACCE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GAD_ACCE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GAD_ACCE_IDX,2])
      *
      * insert into GAD_ACCE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GAD_ACCE')
        i_extval=cp_InsertValODBCExtFlds(this,'GAD_ACCE')
        i_cFldBody=" "+;
                  "(AGCODGAD,AGTIPUTE,AGUTEGRP,AGTIPNEG,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_AGCODGAD)+","+cp_ToStrODBC(this.w_AGTIPUTE)+","+cp_ToStrODBCNull(this.w_AGUTEGRP)+","+cp_ToStrODBC(this.w_AGTIPNEG)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GAD_ACCE')
        i_extval=cp_InsertValVFPExtFlds(this,'GAD_ACCE')
        cp_CheckDeletedKey(i_cTable,0,'AGCODGAD',this.w_AGCODGAD,'AGTIPUTE',this.w_AGTIPUTE,'AGUTEGRP',this.w_AGUTEGRP,'AGUTEGRP',this.w_AGUTEGRP)
        INSERT INTO (i_cTable) (;
                   AGCODGAD;
                  ,AGTIPUTE;
                  ,AGUTEGRP;
                  ,AGTIPNEG;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_AGCODGAD;
                  ,this.w_AGTIPUTE;
                  ,this.w_AGUTEGRP;
                  ,this.w_AGTIPNEG;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.GAD_ACCE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GAD_ACCE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_AGTIPUTE) OR Empty(t_AGUTEGRP) OR Empty(t_AGTIPNEG))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'GAD_ACCE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and AGTIPUTE="+cp_ToStrODBC(&i_TN.->AGTIPUTE)+;
                 " and AGUTEGRP="+cp_ToStrODBC(&i_TN.->AGUTEGRP)+;
                 " and AGUTEGRP="+cp_ToStrODBC(&i_TN.->AGUTEGRP)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'GAD_ACCE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and AGTIPUTE=&i_TN.->AGTIPUTE;
                      and AGUTEGRP=&i_TN.->AGUTEGRP;
                      and AGUTEGRP=&i_TN.->AGUTEGRP;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_AGTIPUTE) OR Empty(t_AGUTEGRP) OR Empty(t_AGTIPNEG))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and AGTIPUTE="+cp_ToStrODBC(&i_TN.->AGTIPUTE)+;
                            " and AGUTEGRP="+cp_ToStrODBC(&i_TN.->AGUTEGRP)+;
                            " and AGUTEGRP="+cp_ToStrODBC(&i_TN.->AGUTEGRP)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and AGTIPUTE=&i_TN.->AGTIPUTE;
                            and AGUTEGRP=&i_TN.->AGUTEGRP;
                            and AGUTEGRP=&i_TN.->AGUTEGRP;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace AGTIPUTE with this.w_AGTIPUTE
              replace AGUTEGRP with this.w_AGUTEGRP
              replace AGUTEGRP with this.w_AGUTEGRP
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update GAD_ACCE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'GAD_ACCE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " AGTIPNEG="+cp_ToStrODBC(this.w_AGTIPNEG)+;
                     ",AGTIPUTE="+cp_ToStrODBC(this.w_AGTIPUTE)+;
                     ",AGUTEGRP="+cp_ToStrODBC(this.w_AGUTEGRP)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and AGTIPUTE="+cp_ToStrODBC(AGTIPUTE)+;
                             " and AGUTEGRP="+cp_ToStrODBC(AGUTEGRP)+;
                             " and AGUTEGRP="+cp_ToStrODBC(AGUTEGRP)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'GAD_ACCE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      AGTIPNEG=this.w_AGTIPNEG;
                     ,AGTIPUTE=this.w_AGTIPUTE;
                     ,AGUTEGRP=this.w_AGUTEGRP;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and AGTIPUTE=&i_TN.->AGTIPUTE;
                                      and AGUTEGRP=&i_TN.->AGUTEGRP;
                                      and AGUTEGRP=&i_TN.->AGUTEGRP;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GAD_ACCE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GAD_ACCE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_AGTIPUTE) OR Empty(t_AGUTEGRP) OR Empty(t_AGTIPNEG))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete GAD_ACCE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and AGTIPUTE="+cp_ToStrODBC(&i_TN.->AGTIPUTE)+;
                            " and AGUTEGRP="+cp_ToStrODBC(&i_TN.->AGUTEGRP)+;
                            " and AGUTEGRP="+cp_ToStrODBC(&i_TN.->AGUTEGRP)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and AGTIPUTE=&i_TN.->AGTIPUTE;
                              and AGUTEGRP=&i_TN.->AGUTEGRP;
                              and AGUTEGRP=&i_TN.->AGUTEGRP;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_AGTIPUTE) OR Empty(t_AGUTEGRP) OR Empty(t_AGTIPNEG))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GAD_ACCE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GAD_ACCE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_AGTIPUTE<>.w_AGTIPUTE
          .link_2_2('Full')
        endif
        if .o_AGTIPUTE<>.w_AGTIPUTE
          .link_2_3('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        if .o_AGTIPUTE<>.w_AGTIPUTE
          .Calculate_ALPNRUONOS()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_ALPNRUONOS()
    with this
          * --- Sbianca campi al cambio del tipo utilizzatore
          .w_AGUTEGRP = 0
          .w_AGUTEGRP = 0
          .w_DESUTE = ''
          .w_DESGRP = ''
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAGUTEGRP_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAGUTEGRP_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAGUTEGRP_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAGUTEGRP_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_2.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_2.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_3.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_3.mHide()
    this.oPgFrm.Page1.oPag.oDESUTE_2_7.visible=!this.oPgFrm.Page1.oPag.oDESUTE_2_7.mHide()
    this.oPgFrm.Page1.oPag.oDESGRP_2_8.visible=!this.oPgFrm.Page1.oPag.oDESGRP_2_8.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AGUTEGRP
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGUTEGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_AGUTEGRP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_AGUTEGRP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_AGUTEGRP) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','code',cp_AbsName(oSource.parent,'oAGUTEGRP_2_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGUTEGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_AGUTEGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_AGUTEGRP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGUTEGRP = NVL(_Link_.code,0)
      this.w_DESGRP = NVL(_Link_.name,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_AGUTEGRP = 0
      endif
      this.w_DESGRP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGUTEGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGUTEGRP
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGUTEGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_AGUTEGRP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_AGUTEGRP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_AGUTEGRP) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oAGUTEGRP_2_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGUTEGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_AGUTEGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_AGUTEGRP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGUTEGRP = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_AGUTEGRP = 0
      endif
      this.w_DESUTE = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGUTEGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESUTE_2_7.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_2_7.value=this.w_DESUTE
      replace t_DESUTE with this.oPgFrm.Page1.oPag.oDESUTE_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRP_2_8.value==this.w_DESGRP)
      this.oPgFrm.Page1.oPag.oDESGRP_2_8.value=this.w_DESGRP
      replace t_DESGRP with this.oPgFrm.Page1.oPag.oDESGRP_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPUTE_2_1.RadioValue()==this.w_AGTIPUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPUTE_2_1.SetRadio()
      replace t_AGTIPUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPUTE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_2.value==this.w_AGUTEGRP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_2.value=this.w_AGUTEGRP
      replace t_AGUTEGRP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_3.value==this.w_AGUTEGRP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_3.value=this.w_AGUTEGRP
      replace t_AGUTEGRP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGUTEGRP_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPNEG_2_4.RadioValue()==this.w_AGTIPNEG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPNEG_2_4.SetRadio()
      replace t_AGTIPNEG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPNEG_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'GAD_ACCE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_AGTIPUTE) OR Empty(.w_AGUTEGRP) OR Empty(.w_AGTIPNEG))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AGTIPUTE = this.w_AGTIPUTE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_AGTIPUTE) OR Empty(t_AGUTEGRP) OR Empty(t_AGTIPNEG)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_AGTIPUTE=space(1)
      .w_AGUTEGRP=0
      .w_AGUTEGRP=0
      .w_AGTIPNEG=space(1)
      .w_DESUTE=space(50)
      .w_DESGRP=space(50)
      .DoRTCalc(1,1,.f.)
        .w_AGTIPUTE = 'U'
      .DoRTCalc(3,3,.f.)
      if not(empty(.w_AGUTEGRP))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_AGUTEGRP))
        .link_2_3('Full')
      endif
        .w_AGTIPNEG = 'V'
    endwith
    this.DoRTCalc(6,7,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_AGTIPUTE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPUTE_2_1.RadioValue(.t.)
    this.w_AGUTEGRP = t_AGUTEGRP
    this.w_AGUTEGRP = t_AGUTEGRP
    this.w_AGTIPNEG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPNEG_2_4.RadioValue(.t.)
    this.w_DESUTE = t_DESUTE
    this.w_DESGRP = t_DESGRP
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_AGTIPUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPUTE_2_1.ToRadio()
    replace t_AGUTEGRP with this.w_AGUTEGRP
    replace t_AGUTEGRP with this.w_AGUTEGRP
    replace t_AGTIPNEG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAGTIPNEG_2_4.ToRadio()
    replace t_DESUTE with this.w_DESUTE
    replace t_DESGRP with this.w_DESGRP
    if i_srv='A'
      replace AGTIPUTE with this.w_AGTIPUTE
      replace AGUTEGRP with this.w_AGUTEGRP
      replace AGUTEGRP with this.w_AGUTEGRP
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_magPag1 as StdContainer
  Width  = 288
  height = 155
  stdWidth  = 288
  stdheight = 155
  resizeXpos=164
  resizeYpos=97
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=6, width=270,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="AGTIPUTE",Label1="Tipo",Field2="AGUTEGRP",Label2="Codice",Field3="AGUTEGRP",Label3="Codice",Field4="AGTIPNEG",Label4="Accesso",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185460346

  add object oStr_1_3 as StdString with uid="STNXINTZIK",Visible=.t., Left=1, Top=128,;
    Alignment=1, Width=71, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=24,;
    width=264+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=25,width=263+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='cpgroups|cpusers|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESUTE_2_7.Refresh()
      this.Parent.oDESGRP_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='cpgroups'
        oDropInto=this.oBodyCol.oRow.oAGUTEGRP_2_2
      case cFile='cpusers'
        oDropInto=this.oBodyCol.oRow.oAGUTEGRP_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oDESUTE_2_7 as StdTrsField with uid="DLULDUTSHW",rtseq=6,rtrep=.t.,;
    cFormVar="w_DESUTE",value=space(50),enabled=.f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 47265334,;
    cTotal="", bFixedPos=.t., cQueryName = "DESUTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=206, Left=74, Top=126, InputMask=replicate('X',50)

  func oDESUTE_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGTIPUTE<>'U')
    endwith
    endif
  endfunc

  add object oDESGRP_2_8 as StdTrsField with uid="KETKIUXXCS",rtseq=7,rtrep=.t.,;
    cFormVar="w_DESGRP",value=space(50),enabled=.f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 228800054,;
    cTotal="", bFixedPos=.t., cQueryName = "DESGRP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=206, Left=74, Top=126, InputMask=replicate('X',50)

  func oDESGRP_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGTIPUTE<>'G')
    endwith
    endif
  endfunc

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_magBodyRow as CPBodyRowCnt
  Width=254
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oAGTIPUTE_2_1 as StdTrsCombo with uid="ZFSDIEXACW",rtrep=.t.,;
    cFormVar="w_AGTIPUTE", RowSource=""+"Utente,"+"Gruppo" , ;
    ToolTipText = "Selezionare l'accesso � limitato ad un Utente o ad un Gruppo",;
    HelpContextID = 42289227,;
    Height=20, Width=71, Left=-2, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oAGTIPUTE_2_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AGTIPUTE,&i_cF..t_AGTIPUTE),this.value)
    return(iif(xVal =1,'U',;
    iif(xVal =2,'G',;
    'U')))
  endfunc
  func oAGTIPUTE_2_1.GetRadio()
    this.Parent.oContained.w_AGTIPUTE = this.RadioValue()
    return .t.
  endfunc

  func oAGTIPUTE_2_1.ToRadio()
    this.Parent.oContained.w_AGTIPUTE=trim(this.Parent.oContained.w_AGTIPUTE)
    return(;
      iif(this.Parent.oContained.w_AGTIPUTE=='U',1,;
      iif(this.Parent.oContained.w_AGTIPUTE=='G',2,;
      0)))
  endfunc

  func oAGTIPUTE_2_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oAGUTEGRP_2_2 as StdTrsField with uid="PBTSITYTOE",rtseq=3,rtrep=.t.,;
    cFormVar="w_AGUTEGRP",value=0,isprimarykey=.t.,;
    ToolTipText = "Codice Utente o Gruppo",;
    HelpContextID = 65034326,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=73, Top=1, bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="code", oKey_1_2="this.w_AGUTEGRP"

  func oAGUTEGRP_2_2.mCond()
    with this.Parent.oContained
      return (.w_AGTIPUTE='G')
    endwith
  endfunc

  func oAGUTEGRP_2_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGTIPUTE<>'G')
    endwith
    endif
  endfunc

  func oAGUTEGRP_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGUTEGRP_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAGUTEGRP_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oAGUTEGRP_2_2.readonly and this.parent.oAGUTEGRP_2_2.isprimarykey)
    do cp_zoom with 'cpgroups','*','code',cp_AbsName(this.parent,'oAGUTEGRP_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
   endif
  endproc

  add object oAGUTEGRP_2_3 as StdTrsField with uid="VIMBPRXTNC",rtseq=4,rtrep=.t.,;
    cFormVar="w_AGUTEGRP",value=0,isprimarykey=.t.,;
    ToolTipText = "Codice Utente o Gruppo",;
    HelpContextID = 65034326,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=73, Top=1, bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_AGUTEGRP"

  func oAGUTEGRP_2_3.mCond()
    with this.Parent.oContained
      return (.w_AGTIPUTE='U')
    endwith
  endfunc

  func oAGUTEGRP_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGTIPUTE<>'U')
    endwith
    endif
  endfunc

  func oAGUTEGRP_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGUTEGRP_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAGUTEGRP_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oAGUTEGRP_2_3.readonly and this.parent.oAGUTEGRP_2_3.isprimarykey)
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oAGUTEGRP_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
   endif
  endproc

  add object oAGTIPNEG_2_4 as StdTrsCombo with uid="ZNCCYWOORF",rtrep=.t.,;
    cFormVar="w_AGTIPNEG", RowSource=""+"Nega Visualizzazione,"+"Nega Modifica" , ;
    ToolTipText = "Selezionare se negare la visibilit� del gadget o solo la modifica",;
    HelpContextID = 193284173,;
    Height=20, Width=123, Left=126, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oAGTIPNEG_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AGTIPNEG,&i_cF..t_AGTIPNEG),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'M',;
    'V')))
  endfunc
  func oAGTIPNEG_2_4.GetRadio()
    this.Parent.oContained.w_AGTIPNEG = this.RadioValue()
    return .t.
  endfunc

  func oAGTIPNEG_2_4.ToRadio()
    this.Parent.oContained.w_AGTIPNEG=trim(this.Parent.oContained.w_AGTIPNEG)
    return(;
      iif(this.Parent.oContained.w_AGTIPNEG=='V',1,;
      iif(this.Parent.oContained.w_AGTIPNEG=='M',2,;
      0)))
  endfunc

  func oAGTIPNEG_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oAGTIPUTE_2_1.When()
    return(.t.)
  proc oAGTIPUTE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oAGTIPUTE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mag','GAD_ACCE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AGCODGAD=GAD_ACCE.AGCODGAD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
