* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bka                                                        *
*              Controllo allegati                                              *
*                                                                              *
*      Author: Zucchetti -AT                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-11                                                      *
* Last revis.: 2007-10-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bka",oParentObject)
return(i_retval)

define class tgscg_bka as StdBatch
  * --- Local variables
  w_CODI = space(16)
  w_ICF = 0
  w_TOTCF = 0
  w_ASCCA = 0
  w_RISCF = 0
  w_CA = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CONTROLLO ALLEGATI Controlla la correttezza dellla Partita Iva
    * --- FASE 1 - Lancio Query
    vq_exec( ALLTRIM(this.oParentObject.w_OQRY) ,this.oParentObject,"CurElab")
    * --- Cicliamo su tutte le partite iva da controllare
    * --- FASE 2 - Considera solo i record con tipologia Errori 'D' = Da controllare 
    *     La query si occupa gi� di segnalare quelli vuoti tipo 'V', quelli di lunghezza errata tipo 'L'
    Select CurElab 
 Go Top
    do while CurElab.TIPOERR="D"
      this.w_CODI = CurElab.ANPARIVA 
      if NOT(RIGHT(REPL("0",11)+alltrim(STR(VAL(this.w_CODI),11)),11)=LEFT(this.w_CODI,11)) OR REPL("0",11)=LEFT(this.w_CODI,11)
        Select CurElab
        Replace TIPOERR with "E"
      else
        this.w_ICF = 1
        this.w_TOTCF = 0
        this.w_ASCCA = 0
        this.w_RISCF = 0
        do while this.w_icf<11
          this.w_CA = SUBSTR(this.w_CODI, this.w_icf, 1)
          this.w_ASCCA = VAL(this.w_CA)
          * --- Assegna la posizione nella stringa
          if (this.w_icf - INT(this.w_icf/2)*2) <> 0
            * --- assegna posizioni Dispari
            this.w_TOTCF = this.w_TOTCF + this.w_ASCCA
          else
            * --- assegna posizioni Pari
            this.w_ASCCA = this.w_ASCCA * 2
            if this.w_ASCCA > 9
              this.w_ASCCA = this.w_ASCCA - 9
            endif
            this.w_TOTCF = this.w_TOTCF + this.w_ASCCA
          endif
          this.w_icf = this.w_icf + 1
        enddo
        this.w_RISCF = STR(100-this.w_TOTCF, 3)
        if RIGHT(this.w_RISCF,1) <> SUBSTR(this.w_CODI, 11, 1)
          Select CurElab
          Replace TIPOERR with "E"
        endif
      endif
      if Not Eof("CurElab")
        Skip
      endif
    enddo
    * --- Popolo il cursore di stampa __tmp__
    Select * from CurElab Where TIPOERR <> "D" into Cursor __tmp__ Order by ANTIPCON, ANCODICE
    * --- ora carica la query 
 private i_paramqry,i_loader 
 i_paramqry=createobject("cpquery") 
 i_loader=createobject("cpqueryloader") 
 i_loader.LoadQuery(left(ALLTRIM(this.oParentObject.w_OQRY),len(ALLTRIM(this.oParentObject.w_OQRY))-4),i_paramqry,this.oParentObject)
    * --- FASE 3 - Lancio Report
    CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP),"",this.oParentObject)
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if Used("CurElab")
      Select CurElab 
 Use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
