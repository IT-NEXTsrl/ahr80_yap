* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bag                                                        *
*              Aggiorna listini                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_82]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-28                                                      *
* Last revis.: 2010-09-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bag",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_bag as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_CRITER = space(2)
  w_LISTRIFE = space(5)
  w_DATARIFE = ctod("  /  /  ")
  w_CODMAG = space(5)
  w_ESERCI = space(4)
  w_INNUMINV = 0
  w_RICALC = space(1)
  w_ORIGSCA = space(1)
  w_DATLISIN = ctod("  /  /  ")
  w_DATLISFI = ctod("  /  /  ")
  w_LISTINO = space(5)
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_RICPE1 = 0
  w_LORNET = space(1)
  w_LORNET1 = space(1)
  w_CAMBIO = 0
  w_CAMBIO1 = 0
  w_CAMBIO2 = 0
  w_DECTOT = 0
  w_RICVA1 = 0
  w_ARROT1 = 0
  w_VALOR1IN = 0
  w_ARROT2 = 0
  w_VALOR2IN = 0
  w_ARROT3 = 0
  w_VALOR3IN = 0
  w_ARROT4 = 0
  w_CREALIS = space(1)
  w_AGSCO = space(1)
  w_MOLTIP = 0
  w_MOLTI2 = 0
  w_FLAGGIO = space(1)
  w_CLSERIAL = space(10)
  w_VALUTA = space(3)
  w_LIDESCRI = space(40)
  w_UMLIS = space(3)
  w_TIPUMI = 0
  w_PERCRIC = 0
  w_PREZZOBASE = 0
  w_QUANTI = 0
  w_CALC = space(1)
  w_CODART = space(20)
  w_CODIVA = space(5)
  w_PERIVA = 0
  w_MESS = space(10)
  w_PERCENT = 0
  w_CODRIC = space(5)
  w_ROWNUM = 0
  w_AGGNUM = 0
  w_SCANUM = 0
  w_TROV = .f.
  w_PREZZOFI = 0
  w_DELETED = .f.
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_OLDLIS = space(5)
  w_OLDVAL = space(3)
  w_OLDLISRIF = space(5)
  w_UNISEL = space(3)
  w_CLUNIMIS = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_CLUNIMIS = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTUM = 0
  w_MOLTUM3 = 0
  w_PRIMA = .f.
  w_QUANTI = 0
  w_PREZZOB = 0
  w_NSCONT1 = 0
  w_NSCONT2 = 0
  w_NSCONT3 = 0
  w_NSCONT4 = 0
  w_PREZZOBP = 0
  w_COSTADAGG = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  RIC_PREZ_idx=0
  CLA_RICA_idx=0
  LIS_SCAG_idx=0
  LIS_TINI_idx=0
  TMPART_idx=0
  VOCIIVA_idx=0
  LISTINI_idx=0
  VALUTE_idx=0
  PAR_RIOR_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna i listini
    if this.pOper="V"
      * --- da batch gsar_bvl
      this.w_CRITER = this.oparentobject.w_CRITER
      this.w_LISTRIFE = this.oparentobject.w_LISTRIFE
      this.w_DATARIFE = this.oparentobject.w_DATARIFE
      this.w_CODMAG = this.oparentobject.w_CODMAG
      this.w_ESERCI = this.oparentobject.w_ESERCI
      this.w_INNUMINV = this.oparentobject.w_INNUMINV
      this.w_RICALC = this.oparentobject.w_RICALC
      this.w_ORIGSCA = this.oparentobject.w_ORIGSCA
      this.w_DATLISIN = this.oparentobject.w_DATLISIN
      this.w_DATLISFI = this.oparentobject.w_DATLISFI
      this.w_LISTINO = this.oparentobject.w_LISTINO
      this.w_SCONT1 = this.oparentobject.w_LISCONT1
      this.w_SCONT2 = this.oparentobject.w_LISCONT2
      this.w_SCONT3 = this.oparentobject.w_LISCONT3
      this.w_SCONT4 = this.oparentobject.w_LISCONT4
      this.w_RICPE1 = this.oparentobject.w_RICPE1
      this.w_LORNET = this.oparentobject.w_LORNET
      this.w_LORNET1 = this.oparentobject.w_LORNET1
      this.w_CAMBIO = this.oparentobject.w_CAMBIO
      this.w_CAMBIO1 = this.oparentobject.w_CAMBIO1
      this.w_CAMBIO2 = this.oparentobject.w_CAMBIO2
      this.w_DECTOT = this.oparentobject.w_DECTOT
      this.w_RICVA1 = this.oparentobject.w_RICVA1
      this.w_ARROT1 = this.oparentobject.w_ARROT1
      this.w_ARROT2 = this.oparentobject.w_ARROT2
      this.w_ARROT3 = this.oparentobject.w_ARROT3
      this.w_ARROT4 = this.oparentobject.w_ARROT4
      this.w_VALOR1IN = this.oparentobject.w_VALOR1IN
      this.w_VALOR2IN = this.oparentobject.w_VALOR2IN
      this.w_VALOR3IN = this.oparentobject.w_VALOR3IN
      this.w_CREALIS = this.oparentobject.w_CREALIS
      this.w_AGSCO = this.oparentobject.w_AGSCO
      this.w_MOLTIP = this.oparentobject.w_MOLTIP
      this.w_MOLTI2 = this.oparentobject.w_MOLTI2
      this.w_TIPUMI = this.oparentobject.w_TIPUMI
    endif
    this.w_OLDLIS = "#####"
    this.w_OLDVAL = "###"
    this.w_OLDLISRIF = "#####"
    this.oParentObject.w_OK = .F.
    * --- Try
    local bErr_03813D28
    bErr_03813D28=bTrsErr
    this.Try_03813D28()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Errore durante l'inserimento")
    endif
    bTrsErr=bTrsErr or bErr_03813D28
    * --- End
  endproc
  proc Try_03813D28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from TMPART
    i_nConn=i_TableProp[this.TMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPART_idx,2],.t.,this.TMPART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPART ";
           ,"_Curs_TMPART")
    else
      select * from (i_cTable);
        into cursor _Curs_TMPART
    endif
    if used('_Curs_TMPART')
      select _Curs_TMPART
      locate for 1=1
      do while not(eof())
      this.w_CODART = _Curs_TMPART.CODART
      if this.pOper="M"
        * --- da batch gsva_bas
        this.w_PERCRIC = 0
        this.w_CRITER = NVL(_Curs_TMPART.CRITER,SPACE(2))
        this.w_LISTRIFE = NVL(_Curs_TMPART.LISTRIFE,SPACE(5))
        this.w_DATARIFE = NVL(_Curs_TMPART.DATARIFE,CP_CHARTODATE("  -  -    "))
        this.w_CODMAG = NVL(_Curs_TMPART.CODMAG,SPACE(5))
        this.w_ESERCI = NVL(_Curs_TMPART.ESERCI,SPACE(4))
        this.w_INNUMINV = NVL(_Curs_TMPART.INNUMINV,0)
        this.w_RICALC = NVL(_Curs_TMPART.RICALC," ")
        this.w_ORIGSCA = NVL(_Curs_TMPART.ORISCA," ")
        this.w_DATLISIN = NVL(_Curs_TMPART.DATLISIN,CP_CHARTODATE("  -  -    "))
        this.w_DATLISFI = NVL(_Curs_TMPART.DATLISFI,CP_CHARTODATE("  -  -    "))
        this.w_LISTINO = NVL(_Curs_TMPART.LISTINO,SPACE(5))
        this.w_SCONT1 = NVL(_Curs_TMPART.SCONT1,0)
        this.w_SCONT2 = NVL(_Curs_TMPART.SCONT2,0)
        this.w_SCONT3 = NVL(_Curs_TMPART.SCONT3,0)
        this.w_SCONT4 = NVL(_Curs_TMPART.SCONT4,0)
        this.w_RICPE1 = NVL(_Curs_TMPART.RICPE1,0)
        this.w_CAMBIO = NVL(_Curs_TMPART.CAMBIO,0)
        this.w_CAMBIO1 = NVL(_Curs_TMPART.CAMBIO1,0)
        this.w_CAMBIO2 = NVL(_Curs_TMPART.CAMBIO2,0)
        this.w_RICVA1 = NVL(_Curs_TMPART.RICVA1,0)
        this.w_ARROT1 = NVL(_Curs_TMPART.ARROT1,0)
        this.w_ARROT2 = NVL(_Curs_TMPART.ARROT2,0)
        this.w_ARROT3 = NVL(_Curs_TMPART.ARROT3,0)
        this.w_ARROT4 = NVL(_Curs_TMPART.ARROT4,0)
        this.w_VALOR1IN = NVL(_Curs_TMPART.VALOR1IN,0)
        this.w_VALOR2IN = NVL(_Curs_TMPART.VALOR2IN,0)
        this.w_VALOR3IN = NVL(_Curs_TMPART.VALOR3IN,0)
        this.w_CREALIS = NVL(_Curs_TMPART.CREALIS," ")
        this.w_AGSCO = NVL(_Curs_TMPART.AGSCO," ")
        this.w_MOLTIP = NVL(_Curs_TMPART.MOLTIP,0)
        this.w_MOLTI2 = NVL(_Curs_TMPART.MOLTI2,0)
        this.w_CLSERIAL = NVL(_Curs_TMPART.CLSERIAL,SPACE(10))
        this.w_TIPUMI = IIF(NVL(_Curs_TMPART.CLTIPUMI,"P")="S", 2, 1)
        this.w_FLAGGIO = this.oparentobject.w_FLAGGIO
        if this.w_OLDLIS<>this.w_LISTINO
          * --- Read from LISTINI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.LISTINI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LSIVALIS,LSVALLIS,LSDESLIS"+;
              " from "+i_cTable+" LISTINI where ";
                  +"LSCODLIS = "+cp_ToStrODBC(this.w_LISTINO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LSIVALIS,LSVALLIS,LSDESLIS;
              from (i_cTable) where;
                  LSCODLIS = this.w_LISTINO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LORNET = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
            this.w_VALUTA = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
            this.w_LIDESCRI = NVL(cp_ToDate(_read_.LSDESLIS),cp_NullValue(_read_.LSDESLIS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_OLDLIS = this.w_LISTINO
          if this.w_OLDVAL<>this.w_VALUTA
            * --- Read from VALUTE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VALUTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VADECTOT"+;
                " from "+i_cTable+" VALUTE where ";
                    +"VACODVAL = "+cp_ToStrODBC(this.w_VALUTA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VADECTOT;
                from (i_cTable) where;
                    VACODVAL = this.w_VALUTA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_OLDVAL = this.w_VALUTA
          endif
        endif
        if LEFT(this.w_CRITER,1)="L" AND this.w_OLDLISRIF<>this.w_LISTRIFE
          * --- leggo i flag scorpori dal listino di base se il criterio � da listino
          * --- Read from LISTINI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.LISTINI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LSIVALIS"+;
              " from "+i_cTable+" LISTINI where ";
                  +"LSCODLIS = "+cp_ToStrODBC(this.w_LISTRIFE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LSIVALIS;
              from (i_cTable) where;
                  LSCODLIS = this.w_LISTRIFE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LORNET1 = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_OLDLISRIF = this.w_LISTRIFE
        endif
      endif
      this.w_TROV = .F.
      this.w_DELETED = .F.
      * --- metto una quantit� minima per estrarre il primo scaglione valido
      this.w_QUANTI = 0.001
      this.w_PREZZOBASE = 0
      this.w_ROWNUM = 0
      this.w_AGGNUM = 0
      this.w_SCANUM = 0
      DIMENSION pArr[3]
      * --- passo il parametro 'zcj' che in caplrzli vuol dire cerca tutti
      this.w_UNISEL = IIF(this.w_TIPUMI=1, NVL(_Curs_TMPART.UNMIS1,SPACE(3)), IIF(this.w_TIPUMI=2, NVL(_Curs_TMPART.UNMIS2,SPACE(3)), NVL(_Curs_TMPART.LIUNIMIS, _Curs_TMPART.UNMIS1 )))
      this.w_CALC = BASPRZLI( this.w_CODART , this.w_CRITER , this.w_LISTRIFE, this.w_DATARIFE , this.w_QUANTI , this.w_CODMAG, this.w_ESERCI, this.w_INNUMINV , @pArr, this.w_UNISEL )
      this.w_CLUNIMIS = pArr[3]
      if EMPTY(this.w_CLUNIMIS)
        * --- L'origine non � da listino e quindi il prezzo � espresso nella 1UM dell'articolo
        this.w_CLUNIMIS = NVL(_Curs_TMPART.UNMIS1,SPACE(3))
      endif
      * --- Determina il prezzo base 
      this.w_PREZZOBASE = pArr[1]
      * --- Determina  il rownum del listino di riferimento (quando il criterio � prezzo di listino)
      this.w_ROWNUM = pArr[2]
      * --- Se non trovo il prezzo di riferimento non faccio niente
      if this.w_PREZZOBASE>=0
        this.w_PERCENT = 0
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARCODRIC,ARCODIVA,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARCODRIC,ARCODIVA,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODRIC = NVL(cp_ToDate(_read_.ARCODRIC),cp_NullValue(_read_.ARCODRIC))
          this.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
          this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.w_MOLTUM = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_CODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do case
          case this.w_RICALC="P"
            * --- Read from RIC_PREZ
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.RIC_PREZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIC_PREZ_idx,2],.t.,this.RIC_PREZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LIPERRIC"+;
                " from "+i_cTable+" RIC_PREZ where ";
                    +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                    +" and LICODLIS = "+cp_ToStrODBC(this.w_LISTINO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LIPERRIC;
                from (i_cTable) where;
                    LICODART = this.w_CODART;
                    and LICODLIS = this.w_LISTINO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PERCENT = NVL(cp_ToDate(_read_.LIPERRIC),cp_NullValue(_read_.LIPERRIC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          case this.w_RICALC="C"
            * --- Read from CLA_RICA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CLA_RICA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CLA_RICA_idx,2],.t.,this.CLA_RICA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CRPERCEN"+;
                " from "+i_cTable+" CLA_RICA where ";
                    +"CRCODICE = "+cp_ToStrODBC(this.w_CODRIC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CRPERCEN;
                from (i_cTable) where;
                    CRCODICE = this.w_CODRIC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PERCENT = NVL(cp_ToDate(_read_.CRPERCEN),cp_NullValue(_read_.CRPERCEN))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
        endcase
        * --- Controllo che tipo di ricalcolo � stato utilizzato.
        do case
          case this.w_RICALC$"CP"
            this.w_PERCRIC = this.w_PERCENT
          case this.w_RICALC="R"
            this.w_PERCRIC = this.w_RICPE1
        endcase
        * --- Se ho pi� listini identici prende il primo che il server
        *     gli restituisce
        * --- Read from LIS_TINI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CPROWNUM,LIUNIMIS"+;
            " from "+i_cTable+" LIS_TINI where ";
                +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                +" and LICODLIS = "+cp_ToStrODBC(this.w_LISTINO);
                +" and LIDATATT = "+cp_ToStrODBC(this.w_DATLISIN);
                +" and LIDATDIS = "+cp_ToStrODBC(this.w_DATLISFI);
                +" and LIUNIMIS = "+cp_ToStrODBC(this.w_CLUNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CPROWNUM,LIUNIMIS;
            from (i_cTable) where;
                LICODART = this.w_CODART;
                and LICODLIS = this.w_LISTINO;
                and LIDATATT = this.w_DATLISIN;
                and LIDATDIS = this.w_DATLISFI;
                and LIUNIMIS = this.w_CLUNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_AGGNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
          this.w_UMLIS = NVL(cp_ToDate(_read_.LIUNIMIS),cp_NullValue(_read_.LIUNIMIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Determino gli scaglioni da creare / aggiornare sul listino
        *     A seconda se il check (Scaglioni di origine) � attivo utilizzo il prezzo del listino
        *     di riferimento e gli scaglioni. Altrimenti gli scaglioni li recupero dallo stesso listino 
        *     da aggiornare in base ad una data di riferimento
        if !(this.w_CREALIS="S" AND this.w_AGGNUM=0)
          * --- elaboro solo se ho un listino da aggiornare o il check "solo esistenti" non attivo
          if this.w_ORIGSCA="S"
            * --- w_SCANUM identifica il cprownum degli scaglioni da utilizzare (listino di riferimento)
            this.w_SCANUM = this.w_ROWNUM
            if this.w_AGGNUM<>0
              * --- Se gli scaglioni del listini da aggiornare sono gi� esistenti li rimuovo
              * --- Delete from LIS_SCAG
              i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                      +" and LIROWNUM = "+cp_ToStrODBC(this.w_AGGNUM);
                       )
              else
                delete from (i_cTable) where;
                      LICODART = this.w_CODART;
                      and LIROWNUM = this.w_AGGNUM;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              this.w_DELETED = .T.
            endif
          else
            * --- Tengo gli scaglioni del listino da aggiornare
            this.w_SCANUM = this.w_AGGNUM
          endif
          if this.w_AGGNUM=0
            if this.w_CREALIS<>"S"
              * --- Se listino non esistente allora determino il primo CPROWNUM libero e lo assegno a w_AGGNUM
              * --- Select from LIS_TINI
              i_nConn=i_TableProp[this.LIS_TINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MAX(CPROWNUM) As ULTROW  from "+i_cTable+" LIS_TINI ";
                    +" where LICODART="+cp_ToStrODBC(this.w_CODART)+"";
                     ,"_Curs_LIS_TINI")
              else
                select MAX(CPROWNUM) As ULTROW from (i_cTable);
                 where LICODART=this.w_CODART;
                  into cursor _Curs_LIS_TINI
              endif
              if used('_Curs_LIS_TINI')
                select _Curs_LIS_TINI
                locate for 1=1
                do while not(eof())
                this.w_AGGNUM = Nvl( _Curs_LIS_TINI.ULTROW ,0 )
                  select _Curs_LIS_TINI
                  continue
                enddo
                use
              endif
              this.w_AGGNUM = this.w_AGGNUM + 1
              ah_Msg("Inserisce listini articolo: %1",.T.,.F.,.F., ALLTR(this.w_CODART))
              * --- Insert into LIS_TINI
              i_nConn=i_TableProp[this.LIS_TINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"LICODART"+",CPROWNUM"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+",LIUNIMIS"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_TINI','LICODART');
                +","+cp_NullLink(cp_ToStrODBC(this.w_AGGNUM),'LIS_TINI','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_LISTINO),'LIS_TINI','LICODLIS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DATLISIN),'LIS_TINI','LIDATATT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DATLISFI),'LIS_TINI','LIDATDIS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CLUNIMIS),'LIS_TINI','LIUNIMIS');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'CPROWNUM',this.w_AGGNUM,'LICODLIS',this.w_LISTINO,'LIDATATT',this.w_DATLISIN,'LIDATDIS',this.w_DATLISFI,'LIUNIMIS',this.w_CLUNIMIS)
                insert into (i_cTable) (LICODART,CPROWNUM,LICODLIS,LIDATATT,LIDATDIS,LIUNIMIS &i_ccchkf. );
                   values (;
                     this.w_CODART;
                     ,this.w_AGGNUM;
                     ,this.w_LISTINO;
                     ,this.w_DATLISIN;
                     ,this.w_DATLISFI;
                     ,this.w_CLUNIMIS;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error='Errore inserimento listini'
                return
              endif
              this.oParentObject.w_OK = .T.
            endif
          endif
          ah_Msg("Aggiorna listini articolo: %1",.T.,.F.,.F., ALLTR(this.w_CODART))
          this.w_TROV = .F.
          this.w_PRIMA = .T.
          if LEFT(this.w_CRITER,1)$"IUC"
            * --- Per inventario, ultimo costo/prezzo e costo standard il listino � al netto
            this.w_LORNET1 = "N"
          endif
          do case
            case LEFT(this.w_CRITER,1)="I"
              this.w_CAMBIO = this.w_CAMBIO2
            case LEFT(this.w_CRITER,1)$"UC"
              this.w_CAMBIO = g_CAOVAL
          endcase
          * --- Select from LIS_SCAG
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2],.t.,this.LIS_SCAG_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" LIS_SCAG ";
                +" where LICODART="+cp_ToStrODBC(this.w_CODART)+" And LIROWNUM="+cp_ToStrODBC(this.w_SCANUM)+"";
                 ,"_Curs_LIS_SCAG")
          else
            select * from (i_cTable);
             where LICODART=this.w_CODART And LIROWNUM=this.w_SCANUM;
              into cursor _Curs_LIS_SCAG
          endif
          if used('_Curs_LIS_SCAG')
            select _Curs_LIS_SCAG
            locate for 1=1
            do while not(eof())
            this.w_TROV = .T.
            * --- Chiamo NEWPRZCLI per calcolare nuovo prezzo
            if this.w_AGSCO<>"F" 
              this.w_LISCONT1 = NVL(_Curs_LIS_SCAG.LISCONT1,0)
              this.w_LISCONT2 = NVL(_Curs_LIS_SCAG.LISCONT2,0)
              this.w_LISCONT3 = NVL(_Curs_LIS_SCAG.LISCONT3, 0)
              this.w_LISCONT4 = NVL(_Curs_LIS_SCAG.LISCONT4, 0)
            else
              this.w_LISCONT1 = this.w_SCONT1
              this.w_LISCONT2 = this.w_SCONT2
              this.w_LISCONT3 = this.w_SCONT3
              this.w_LISCONT4 = this.w_SCONT4
            endif
            this.w_QUANTI = NVL(_Curs_LIS_SCAG.LIQUANTI, 0)
            if this.w_ORIGSCA="S"
              this.w_PREZZOB = _Curs_LIS_SCAG.LIPREZZO
              if this.w_CRITER="LN"
                * --- Se la base di calcolo � al netto degli sconti li applico
                this.w_NSCONT1 = _Curs_LIS_SCAG.LISCONT1
                this.w_NSCONT2 = _Curs_LIS_SCAG.LISCONT2
                this.w_NSCONT3 = _Curs_LIS_SCAG.LISCONT3
                this.w_NSCONT4 = _Curs_LIS_SCAG.LISCONT4
                this.w_PREZZOB = cp_ROUND(this.w_PREZZOB * (1+this.w_NSCONT1/100)*(1+this.w_NSCONT2/100)*(1+this.w_NSCONT3/100)*(1+this.w_NSCONT4/100),5)
              endif
              if this.w_CLUNIMIS<>this.w_UMLIS AND NOT EMPTY(this.w_UMLIS)
                * --- Se il prezzo di listino da aggiornare (w_UMLIS) � espresso in una UM diversa da quella del prezzo d'origine (w_CLUNIMIS) lo proporziono
                this.w_PREZZOB = UM2UM(this.w_CODART, this.w_PREZZOB, this.w_UMLIS, this.w_CLUNIMIS, this.w_UNMIS1, this.w_UNMIS2, this.w_OPERAT, this.w_MOLTUM)
              endif
              this.w_PREZZOFI = NEWPRZLI(this.w_PREZZOB, this.w_PERCRIC, this.w_LORNET,this.w_LORNET1,this.w_PERIVA, this.w_CAMBIO1,this.w_CAMBIO,this.w_DECTOT, this.w_RICVA1, this.w_ARROT1, this.w_VALOR1IN, this.w_ARROT2, this.w_VALOR2IN, this.w_ARROT3, this.w_VALOR3IN, this.w_ARROT4 ,this.w_MOLTIP, this.w_MOLTI2)
              if this.w_PREZZOFI<>0
                * --- Insert into LIS_SCAG
                i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"LICODART"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+",LIPREZZO"+",LIQUANTI"+",LIROWNUM"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_AGGNUM),'LIS_SCAG','LIROWNUM');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4,'LIPREZZO',this.w_PREZZOFI,'LIQUANTI',this.w_QUANTI,'LIROWNUM',this.w_AGGNUM)
                  insert into (i_cTable) (LICODART,LISCONT1,LISCONT2,LISCONT3,LISCONT4,LIPREZZO,LIQUANTI,LIROWNUM &i_ccchkf. );
                     values (;
                       this.w_CODART;
                       ,this.w_LISCONT1;
                       ,this.w_LISCONT2;
                       ,this.w_LISCONT3;
                       ,this.w_LISCONT4;
                       ,this.w_PREZZOFI;
                       ,this.w_QUANTI;
                       ,this.w_AGGNUM;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error='Errore inserimento scaglioni listini'
                  return
                endif
              endif
              * --- Aggiorno lo scaglione solo se almeno un dato � realmente modificato
              *     Controllo utile in caso di gestione logistica remota per ridurre impatto
              *     aggiornamenti che non modificano attivamente il dato
              if this.w_FLAGGIO="S" And (( this.w_LISCONT1<>NVL(_Curs_LIS_SCAG.LISCONT1,0) ) or ( this.w_LISCONT2<>NVL(_Curs_LIS_SCAG.LISCONT2,0) ) or ( this.w_LISCONT3<>NVL(_Curs_LIS_SCAG.LISCONT3,0) ) or ( this.w_LISCONT4<>NVL(_Curs_LIS_SCAG.LISCONT4,0) ) or ( this.w_PREZZOFI<> _Curs_LIS_SCAG.LIPREZZO) ) 
                * --- aggiorno anche la base di calcolo (listino di riferimento)
                * --- Write into LIS_SCAG
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"LISCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
                  +",LISCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
                  +",LISCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
                  +",LISCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                  +",LIPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
                      +i_ccchkf ;
                  +" where ";
                      +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                      +" and LIROWNUM = "+cp_ToStrODBC(this.w_SCANUM);
                      +" and LIQUANTI = "+cp_ToStrODBC(this.w_QUANTI);
                         )
                else
                  update (i_cTable) set;
                      LISCONT1 = this.w_LISCONT1;
                      ,LISCONT2 = this.w_LISCONT2;
                      ,LISCONT3 = this.w_LISCONT3;
                      ,LISCONT4 = this.w_LISCONT4;
                      ,LIPREZZO = this.w_PREZZOFI;
                      &i_ccchkf. ;
                   where;
                      LICODART = this.w_CODART;
                      and LIROWNUM = this.w_SCANUM;
                      and LIQUANTI = this.w_QUANTI;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            else
              * --- in tutti gli altri casi devo calcolare la proporzione fra prezzo di base e prezzo del primo scaglione
              * --- Mi serve la query perch� devo ordinare per liquanti ascending ma condizionare il valore di liquanti a 9999999 nel caso di 'oltre' 
              *     (liquanti � a 0)
              * --- Trovo la prima riga valida dello scaglione e memorizzo il prezzo
              * --- Select from GSAR_BAG
              do vq_exec with 'GSAR_BAG',this,'_Curs_GSAR_BAG','',.f.,.t.
              if used('_Curs_GSAR_BAG')
                select _Curs_GSAR_BAG
                locate for 1=1
                do while not(eof())
                if this.w_PRIMA
                  this.w_PREZZOBP = NVL(LIPREZZO,0)
                endif
                this.w_PRIMA = .F.
                  select _Curs_GSAR_BAG
                  continue
                enddo
                use
              endif
              * --- Applico la proporzione ai vari scaglioni
              if this.w_CLUNIMIS<>this.w_UMLIS
                * --- Se il prezzo di listino da aggiornare (w_UMLIS) � espresso in una UM diversa da quella del prezzo d'origine (w_CLUNIMIS) lo proporziono
                this.w_PREZZOB = (UM2UM(this.w_CODART, this.w_PREZZOBASE * _Curs_LIS_SCAG.LIPREZZO, this.w_UMLIS, this.w_CLUNIMIS, this.w_UNMIS1, this.w_UNMIS2, this.w_OPERAT, this.w_MOLTUM))/this.w_PREZZOBP
              else
                this.w_PREZZOB = this.w_PREZZOBASE * IIF(this.w_PREZZOBP=0, 1, (_Curs_LIS_SCAG.LIPREZZO/this.w_PREZZOBP) )
              endif
              * --- al prezzo proporzionato applico gli eventuali ricarichi
              this.w_PREZZOFI = NEWPRZLI(this.w_PREZZOB, this.w_PERCRIC, this.w_LORNET,this.w_LORNET1,this.w_PERIVA, this.w_CAMBIO1,this.w_CAMBIO,this.w_DECTOT, this.w_RICVA1, this.w_ARROT1, this.w_VALOR1IN, this.w_ARROT2, this.w_VALOR2IN, this.w_ARROT3, this.w_VALOR3IN, this.w_ARROT4,this.w_MOLTIP, this.w_MOLTI2)
              if this.w_PREZZOFI<>0 And (( this.w_LISCONT1<>NVL(_Curs_LIS_SCAG.LISCONT1,0) ) or ( this.w_LISCONT2<>NVL(_Curs_LIS_SCAG.LISCONT2,0) ) or ( this.w_LISCONT3<>NVL(_Curs_LIS_SCAG.LISCONT3,0) ) or ( this.w_LISCONT4<>NVL(_Curs_LIS_SCAG.LISCONT4,0) ) or ( this.w_PREZZOFI<> _Curs_LIS_SCAG.LIPREZZO) ) 
                * --- Write into LIS_SCAG
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"LISCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
                  +",LISCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
                  +",LISCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
                  +",LISCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                  +",LIPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
                      +i_ccchkf ;
                  +" where ";
                      +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                      +" and LIROWNUM = "+cp_ToStrODBC(this.w_AGGNUM);
                      +" and LIQUANTI = "+cp_ToStrODBC(this.w_QUANTI);
                         )
                else
                  update (i_cTable) set;
                      LISCONT1 = this.w_LISCONT1;
                      ,LISCONT2 = this.w_LISCONT2;
                      ,LISCONT3 = this.w_LISCONT3;
                      ,LISCONT4 = this.w_LISCONT4;
                      ,LIPREZZO = this.w_PREZZOFI;
                      &i_ccchkf. ;
                   where;
                      LICODART = this.w_CODART;
                      and LIROWNUM = this.w_AGGNUM;
                      and LIQUANTI = this.w_QUANTI;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
            * --- Segnalo che ho aggiornato il listino...
            this.oParentObject.w_OK = .T.
              select _Curs_LIS_SCAG
              continue
            enddo
            use
          endif
          if ! this.w_TROV AND (this.w_CREALIS<>"S" OR this.w_DELETED)
            * --- Il Listino esiste ma non sono presenti scaglioni di origine quindi ne inserisce uno
            this.w_PREZZOFI = NEWPRZLI(this.w_PREZZOBASE, this.w_PERCRIC, this.w_LORNET,this.w_LORNET1,this.w_PERIVA,this.w_CAMBIO1,this.w_CAMBIO, this.w_DECTOT, this.w_RICVA1, this.w_ARROT1, this.w_VALOR1IN, this.w_ARROT2, this.w_VALOR2IN, this.w_ARROT3, this.w_VALOR3IN, this.w_ARROT4,this.w_MOLTIP, this.w_MOLTI2)
            * --- Scrivo lo scaglione (quantit�: oltre)
            if this.w_PREZZOFI<>0
              * --- Scrivo lo scaglione (quantit�: oltre)
              this.w_QUANTI = 0
              if this.w_AGSCO<>"F" 
                this.w_LISCONT1 = 0
                this.w_LISCONT2 = 0
                this.w_LISCONT3 = 0
                this.w_LISCONT4 = 0
              else
                this.w_LISCONT1 = this.w_SCONT1
                this.w_LISCONT2 = this.w_SCONT2
                this.w_LISCONT3 = this.w_SCONT3
                this.w_LISCONT4 = this.w_SCONT4
              endif
              * --- Insert into LIS_SCAG
              i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
                +","+cp_NullLink(cp_ToStrODBC(this.w_AGGNUM),'LIS_SCAG','LIROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
                +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
                +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
                +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_AGGNUM,'LIQUANTI',this.w_QUANTI,'LIPREZZO',this.w_PREZZOFI,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
                insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
                   values (;
                     this.w_CODART;
                     ,this.w_AGGNUM;
                     ,this.w_QUANTI;
                     ,this.w_PREZZOFI;
                     ,this.w_LISCONT1;
                     ,this.w_LISCONT2;
                     ,this.w_LISCONT3;
                     ,this.w_LISCONT4;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              * --- Segnalo che ho aggiornato il listino...
              this.oParentObject.w_OK = .T.
            endif
          endif
          if this.w_FLAGGIO="S" AND LEFT(this.w_CRITER,1)="C"
            * --- aggiorno il costo standard
            this.w_COSTADAGG = NEWPRZLI(this.w_PREZZOBASE, this.w_PERCRIC, this.w_LORNET,this.w_LORNET1,this.w_PERIVA, this.w_CAMBIO1,this.w_CAMBIO,this.w_DECTOT, this.w_RICVA1, this.w_ARROT1, this.w_VALOR1IN, this.w_ARROT2, this.w_VALOR2IN, this.w_ARROT3, this.w_VALOR3IN, this.w_ARROT4,this.w_MOLTIP, this.w_MOLTI2)
            * --- Write into PAR_RIOR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_COSTADAGG),'PAR_RIOR','PRCOSSTA');
                  +i_ccchkf ;
              +" where ";
                  +"PRCODART = "+cp_ToStrODBC(this.w_CODART);
                     )
            else
              update (i_cTable) set;
                  PRCOSSTA = this.w_COSTADAGG;
                  &i_ccchkf. ;
               where;
                  PRCODART = this.w_CODART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if this.oParentObject.w_OK AND this.pOPER="M"
            GSVA_BAU(this,this.w_CLSERIAL)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
        select _Curs_TMPART
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.oParentObject.w_OK
      * --- Se non ho fatto niente non d� questo messaggio
      ah_ErrorMsg("Elaborazione terminata")
    endif
    return


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='RIC_PREZ'
    this.cWorkTables[3]='CLA_RICA'
    this.cWorkTables[4]='LIS_SCAG'
    this.cWorkTables[5]='LIS_TINI'
    this.cWorkTables[6]='TMPART'
    this.cWorkTables[7]='VOCIIVA'
    this.cWorkTables[8]='LISTINI'
    this.cWorkTables[9]='VALUTE'
    this.cWorkTables[10]='PAR_RIOR'
    this.cWorkTables[11]='KEY_ARTI'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_TMPART')
      use in _Curs_TMPART
    endif
    if used('_Curs_LIS_TINI')
      use in _Curs_LIS_TINI
    endif
    if used('_Curs_LIS_SCAG')
      use in _Curs_LIS_SCAG
    endif
    if used('_Curs_GSAR_BAG')
      use in _Curs_GSAR_BAG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
