* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kca                                                        *
*              Chiavi articolo                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_85]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-16                                                      *
* Last revis.: 2017-06-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kca",oParentObject))

* --- Class definition
define class tgsma_kca as StdForm
  Top    = 58
  Left   = 8

  * --- Standard Properties
  Width  = 730
  Height = 400
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-06-16"
  HelpContextID=169182359
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  TIP_COLL_IDX = 0
  cPrg = "gsma_kca"
  cComment = "Chiavi articolo"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_ARCODART = space(20)
  w_ARDESART = space(40)
  w_ARTIPBAR = space(1)
  w_ARFLPECO = space(1)
  w_SELEZI = space(1)
  w_DECIMALI = space(1)
  w_TIPBAR = space(1)
  o_TIPBAR = space(1)
  w_NUMDEQ = space(1)
  w_FLFRAZ = space(1)
  o_FLFRAZ = space(1)
  w_AGGPROG = space(1)
  w_BARKEY = space(20)
  o_BARKEY = space(20)
  w_DESCOD = space(40)
  w_TIPCON = space(20)
  o_TIPCON = space(20)
  w_CACODICE = space(20)
  w_CODCON = space(15)
  w_DESCRI = space(40)
  w_CATIPCO3 = space(5)
  w_FLSTAM = space(1)
  w_CAFLIMBA = space(1)
  o_CAFLIMBA = space(1)
  w_DESCOL = space(35)
  w_CODESC = space(5)
  w_ARTIPART = space(2)
  w_CalcZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kcaPag1","gsma_kca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsma_kca
    WITH THIS.PARENT
     If IsAlt()
       .cComment = CP_TRANSLATE('Elenco codici di ricerca voci del tariffario')
     Else
       .cComment = CP_TRANSLATE('Chiavi articolo')
     Endif
    Endwith
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CalcZoom = this.oPgFrm.Pages(1).oPag.CalcZoom
    DoDefault()
    proc Destroy()
      this.w_CalcZoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='TIP_COLL'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_ARCODART=space(20)
      .w_ARDESART=space(40)
      .w_ARTIPBAR=space(1)
      .w_ARFLPECO=space(1)
      .w_SELEZI=space(1)
      .w_DECIMALI=space(1)
      .w_TIPBAR=space(1)
      .w_NUMDEQ=space(1)
      .w_FLFRAZ=space(1)
      .w_AGGPROG=space(1)
      .w_BARKEY=space(20)
      .w_DESCOD=space(40)
      .w_TIPCON=space(20)
      .w_CACODICE=space(20)
      .w_CODCON=space(15)
      .w_DESCRI=space(40)
      .w_CATIPCO3=space(5)
      .w_FLSTAM=space(1)
      .w_CAFLIMBA=space(1)
      .w_DESCOL=space(35)
      .w_CODESC=space(5)
      .w_ARTIPART=space(2)
      .w_ARCODART=oParentObject.w_ARCODART
      .w_ARDESART=oParentObject.w_ARDESART
      .w_ARTIPBAR=oParentObject.w_ARTIPBAR
      .w_ARFLPECO=oParentObject.w_ARFLPECO
      .w_FLFRAZ=oParentObject.w_FLFRAZ
      .w_ARTIPART=oParentObject.w_ARTIPART
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page1.oPag.CalcZoom.Calculate()
          .DoRTCalc(2,5,.f.)
        .w_SELEZI = 'D'
        .w_DECIMALI = .w_CalcZoom.getVar('CANUMDEQ')
        .w_TIPBAR = this.oparentobject .w_TIPBAR
        .w_NUMDEQ = IIF(.w_FLFRAZ<>'S',.w_DECIMALI,'0')
          .DoRTCalc(10,10,.f.)
        .w_AGGPROG = IIF(.w_TIPBAR $ '1-2','S','N')
        .w_BARKEY = SPACE(15)
        .w_DESCOD = .w_ARDESART
        .w_TIPCON = IIF(IsAlt(),IIF(this.oparentobject .w_ARTIPART='DE','D',IIF(this.oparentobject .w_ARTIPART='FO','I','M')),'R')
        .w_CACODICE = .w_CalcZoom.getVar('CACODICE')
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .w_CODCON = SPACE(15)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODCON))
          .link_1_23('Full')
        endif
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_CATIPCO3))
          .link_1_25('Full')
        endif
          .DoRTCalc(19,19,.f.)
        .w_CAFLIMBA = 'N'
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate(IIF(IsAlt(), "Nuovo codice di ricerca da creare", "Codice di ricerca (se EAN 8/13 premere F9 per eventuale calcolo automatico)"))
    endwith
    this.DoRTCalc(21,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ARCODART=.w_ARCODART
      .oParentObject.w_ARDESART=.w_ARDESART
      .oParentObject.w_ARTIPBAR=.w_ARTIPBAR
      .oParentObject.w_ARFLPECO=.w_ARFLPECO
      .oParentObject.w_FLFRAZ=.w_FLFRAZ
      .oParentObject.w_ARTIPART=.w_ARTIPART
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .DoRTCalc(2,6,.t.)
            .w_DECIMALI = .w_CalcZoom.getVar('CANUMDEQ')
        .DoRTCalc(8,8,.t.)
        if .o_TIPBAR<>.w_TIPBAR.or. .o_FLFRAZ<>.w_FLFRAZ
            .w_NUMDEQ = IIF(.w_FLFRAZ<>'S',.w_DECIMALI,'0')
        endif
        .DoRTCalc(10,10,.t.)
        if .o_TIPBAR<>.w_TIPBAR
            .w_AGGPROG = IIF(.w_TIPBAR $ '1-2','S','N')
        endif
        if .o_TIPBAR<>.w_TIPBAR
            .w_BARKEY = SPACE(15)
        endif
        if .o_BARKEY<>.w_BARKEY
            .w_DESCOD = .w_ARDESART
        endif
        .DoRTCalc(14,14,.t.)
            .w_CACODICE = .w_CalcZoom.getVar('CACODICE')
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        if .o_TIPBAR<>.w_TIPBAR
          .Calculate_HFGLNXNTNE()
        endif
        if .o_CAFLIMBA<>.w_CAFLIMBA.or. .o_TIPBAR<>.w_TIPBAR
          .Calculate_FMTKNNYGHP()
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_CODCON = SPACE(15)
          .link_1_23('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(IIF(IsAlt(), "Nuovo codice di ricerca da creare", "Codice di ricerca (se EAN 8/13 premere F9 per eventuale calcolo automatico)"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(IIF(IsAlt(), "Nuovo codice di ricerca da creare", "Codice di ricerca (se EAN 8/13 premere F9 per eventuale calcolo automatico)"))
    endwith
  return

  proc Calculate_HFGLNXNTNE()
    with this
          * --- Check imballo
          .w_CAFLIMBA = IIF( NOT INLIST( .w_TIPBAR, '2', 'C' ), 'N', .w_CAFLIMBA )
    endwith
  endproc
  proc Calculate_FMTKNNYGHP()
    with this
          * --- Sbianca tipo collo
          .w_CATIPCO3 = IIF(.w_CAFLIMBA <> 'S', SPACE(5), .w_CATIPCO3)
          .w_DESCOL = IIF(.w_CAFLIMBA <> 'S', SPACE(35), .w_DESCOL)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNUMDEQ_1_12.enabled = this.oPgFrm.Page1.oPag.oNUMDEQ_1_12.mCond()
    this.oPgFrm.Page1.oPag.oTIPCON_1_17.enabled = this.oPgFrm.Page1.oPag.oTIPCON_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCODCON_1_23.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCATIPCO3_1_25.enabled = this.oPgFrm.Page1.oPag.oCATIPCO3_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPBAR_1_11.visible=!this.oPgFrm.Page1.oPag.oTIPBAR_1_11.mHide()
    this.oPgFrm.Page1.oPag.oNUMDEQ_1_12.visible=!this.oPgFrm.Page1.oPag.oNUMDEQ_1_12.mHide()
    this.oPgFrm.Page1.oPag.oAGGPROG_1_14.visible=!this.oPgFrm.Page1.oPag.oAGGPROG_1_14.mHide()
    this.oPgFrm.Page1.oPag.oDESCOD_1_16.visible=!this.oPgFrm.Page1.oPag.oDESCOD_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCODCON_1_23.visible=!this.oPgFrm.Page1.oPag.oCODCON_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI_1_24.visible=!this.oPgFrm.Page1.oPag.oDESCRI_1_24.mHide()
    this.oPgFrm.Page1.oPag.oCATIPCO3_1_25.visible=!this.oPgFrm.Page1.oPag.oCATIPCO3_1_25.mHide()
    this.oPgFrm.Page1.oPag.oFLSTAM_1_26.visible=!this.oPgFrm.Page1.oPag.oFLSTAM_1_26.mHide()
    this.oPgFrm.Page1.oPag.oCAFLIMBA_1_27.visible=!this.oPgFrm.Page1.oPag.oCAFLIMBA_1_27.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDESCOL_1_35.visible=!this.oPgFrm.Page1.oPag.oDESCOL_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oCODESC_1_37.visible=!this.oPgFrm.Page1.oPag.oCODESC_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CalcZoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCON
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODESC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCODESC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODESC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_23'),i_cWhere,'',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODESC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODESC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODESC";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODESC";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_CODESC = NVL(_Link_.ANCODESC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_CODESC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATIPCO3
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATIPCO3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CATIPCO3)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CATIPCO3))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATIPCO3)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATIPCO3) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oCATIPCO3_1_25'),i_cWhere,'GSAR_MTO',"Tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATIPCO3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CATIPCO3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CATIPCO3)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATIPCO3 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOL = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATIPCO3 = space(5)
      endif
      this.w_DESCOL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATIPCO3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_7.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPBAR_1_11.RadioValue()==this.w_TIPBAR)
      this.oPgFrm.Page1.oPag.oTIPBAR_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDEQ_1_12.RadioValue()==this.w_NUMDEQ)
      this.oPgFrm.Page1.oPag.oNUMDEQ_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGPROG_1_14.RadioValue()==this.w_AGGPROG)
      this.oPgFrm.Page1.oPag.oAGGPROG_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBARKEY_1_15.value==this.w_BARKEY)
      this.oPgFrm.Page1.oPag.oBARKEY_1_15.value=this.w_BARKEY
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOD_1_16.value==this.w_DESCOD)
      this.oPgFrm.Page1.oPag.oDESCOD_1_16.value=this.w_DESCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_17.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_23.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_23.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_24.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_24.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPCO3_1_25.value==this.w_CATIPCO3)
      this.oPgFrm.Page1.oPag.oCATIPCO3_1_25.value=this.w_CATIPCO3
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSTAM_1_26.RadioValue()==this.w_FLSTAM)
      this.oPgFrm.Page1.oPag.oFLSTAM_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLIMBA_1_27.RadioValue()==this.w_CAFLIMBA)
      this.oPgFrm.Page1.oPag.oCAFLIMBA_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOL_1_35.value==this.w_DESCOL)
      this.oPgFrm.Page1.oPag.oDESCOL_1_35.value=this.w_DESCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESC_1_37.value==this.w_CODESC)
      this.oPgFrm.Page1.oPag.oCODESC_1_37.value=this.w_CODESC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(g_perpqt>=val(.w_NUMDEQ))  and not(!(.w_TIPBAR$'A-B') OR .w_ARFLPECO<>'P' OR (.w_ARTIPART$'FM-FO-DE'))  and (.w_FLFRAZ<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMDEQ_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero dei decimali selezionato � maggiore dei decimali impostati nei dati azienda.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPBAR = this.w_TIPBAR
    this.o_FLFRAZ = this.w_FLFRAZ
    this.o_BARKEY = this.w_BARKEY
    this.o_TIPCON = this.w_TIPCON
    this.o_CAFLIMBA = this.w_CAFLIMBA
    return

enddefine

* --- Define pages as container
define class tgsma_kcaPag1 as StdContainer
  Width  = 726
  height = 400
  stdWidth  = 726
  stdheight = 400
  resizeXpos=521
  resizeYpos=116
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object CalcZoom as cp_szoombox with uid="SYGTKMYHWT",left=1, top=1, width=722,height=199,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='KEY_ARTI',cZoomFile='GSMA_KCA',bOptions=.f.,bAdvOptions=.f.,;
    cEvent = "Legge",;
    nPag=1;
    , HelpContextID = 189690906

  add object oSELEZI_1_7 as StdRadio with uid="HJKSOKDTOM",rtseq=6,rtrep=.f.,left=5, top=203, width=132,height=32;
    , ToolTipText = "Seleziona/deseleziona tutti i record alla cancellazione";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_7.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 117469402
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 117469402
      this.Buttons(2).Top=15
      this.SetAll("Width",130)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti i record alla cancellazione")
      StdRadio::init()
    endproc

  func oSELEZI_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_7.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_7.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_8 as StdButton with uid="NRIVKZFIRP",left=614, top=207, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare i codici selezionati";
    , HelpContextID = 15104698;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSMA_BCA(this.Parent.oContained,"AGGIO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="MDKVJHRWVT",left=667, top=207, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla manutenzione delle chiavi di ricerca";
    , HelpContextID = 140429937;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSMA_BGS(this.Parent.oContained,"K", .w_CACODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CACODICE))
      endwith
    endif
  endfunc


  add object oTIPBAR_1_11 as StdCombo with uid="WUVJWDDZIZ",rtseq=8,rtrep=.f.,left=90,top=262,width=120,height=21;
    , HelpContextID = 261303498;
    , cFormVar="w_TIPBAR",RowSource=""+"EAN 8,"+"EAN 13,"+"ALFA 39,"+"UPC A,"+"UPC E,"+"2D5,"+"2D5 interleave,"+"Farmaceutico,"+"198,"+"No,"+"EAN 13 peso variabile,"+"EAN 13 peso var. + digit,"+"EAN/UCC 14", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPBAR_1_11.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    iif(this.value =10,'0',;
    iif(this.value =11,'A',;
    iif(this.value =12,'B',;
    iif(this.value =13,'C',;
    space(1)))))))))))))))
  endfunc
  func oTIPBAR_1_11.GetRadio()
    this.Parent.oContained.w_TIPBAR = this.RadioValue()
    return .t.
  endfunc

  func oTIPBAR_1_11.SetRadio()
    this.Parent.oContained.w_TIPBAR=trim(this.Parent.oContained.w_TIPBAR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPBAR=='1',1,;
      iif(this.Parent.oContained.w_TIPBAR=='2',2,;
      iif(this.Parent.oContained.w_TIPBAR=='3',3,;
      iif(this.Parent.oContained.w_TIPBAR=='4',4,;
      iif(this.Parent.oContained.w_TIPBAR=='5',5,;
      iif(this.Parent.oContained.w_TIPBAR=='6',6,;
      iif(this.Parent.oContained.w_TIPBAR=='7',7,;
      iif(this.Parent.oContained.w_TIPBAR=='8',8,;
      iif(this.Parent.oContained.w_TIPBAR=='9',9,;
      iif(this.Parent.oContained.w_TIPBAR=='0',10,;
      iif(this.Parent.oContained.w_TIPBAR=='A',11,;
      iif(this.Parent.oContained.w_TIPBAR=='B',12,;
      iif(this.Parent.oContained.w_TIPBAR=='C',13,;
      0)))))))))))))
  endfunc

  func oTIPBAR_1_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oNUMDEQ_1_12 as StdCombo with uid="PFNRSRLMKM",rtseq=9,rtrep=.f.,left=317,top=262,width=65,height=22;
    , ToolTipText = "Numero decimali quantit�";
    , HelpContextID = 5329194;
    , cFormVar="w_NUMDEQ",RowSource=""+"Zero,"+"Uno,"+"Due,"+"Tre", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Il numero dei decimali selezionato � maggiore dei decimali impostati nei dati azienda.";
  , bGlobalFont=.t.


  func oNUMDEQ_1_12.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    space(1))))))
  endfunc
  func oNUMDEQ_1_12.GetRadio()
    this.Parent.oContained.w_NUMDEQ = this.RadioValue()
    return .t.
  endfunc

  func oNUMDEQ_1_12.SetRadio()
    this.Parent.oContained.w_NUMDEQ=trim(this.Parent.oContained.w_NUMDEQ)
    this.value = ;
      iif(this.Parent.oContained.w_NUMDEQ=='0',1,;
      iif(this.Parent.oContained.w_NUMDEQ=='1',2,;
      iif(this.Parent.oContained.w_NUMDEQ=='2',3,;
      iif(this.Parent.oContained.w_NUMDEQ=='3',4,;
      0))))
  endfunc

  func oNUMDEQ_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLFRAZ<>'S')
    endwith
   endif
  endfunc

  func oNUMDEQ_1_12.mHide()
    with this.Parent.oContained
      return (!(.w_TIPBAR$'A-B') OR .w_ARFLPECO<>'P' OR (.w_ARTIPART$'FM-FO-DE'))
    endwith
  endfunc

  func oNUMDEQ_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (g_perpqt>=val(.w_NUMDEQ))
    endwith
    return bRes
  endfunc

  add object oAGGPROG_1_14 as StdCheck with uid="LULWBEHXAY",rtseq=11,rtrep=.f.,left=552, top=261, caption="Aggiorna progressivo",;
    ToolTipText = "Se attivo: verr� aggiornato progressivo del barcode EAN 8/13",;
    HelpContextID = 243941382,;
    cFormVar="w_AGGPROG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGPROG_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAGGPROG_1_14.GetRadio()
    this.Parent.oContained.w_AGGPROG = this.RadioValue()
    return .t.
  endfunc

  func oAGGPROG_1_14.SetRadio()
    this.Parent.oContained.w_AGGPROG=trim(this.Parent.oContained.w_AGGPROG)
    this.value = ;
      iif(this.Parent.oContained.w_AGGPROG=='S',1,;
      0)
  endfunc

  func oAGGPROG_1_14.mHide()
    with this.Parent.oContained
      return (NOT INLIST( .w_TIPBAR, '1','2'))
    endwith
  endfunc

  add object oBARKEY_1_15 as StdField with uid="BOYHQOKTPM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_BARKEY", cQueryName = "BARKEY",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 139073002,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=90, Top=289, InputMask=replicate('X',20), bHasZoom = .t. 

  proc oBARKEY_1_15.mZoom
      with this.Parent.oContained
        GSMA_BCC(this.Parent.oContained,"C")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDESCOD_1_16 as StdField with uid="BHCXEFHLMN",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCOD", cQueryName = "DESCOD",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del codice di ricerca",;
    HelpContextID = 212992458,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=244, Top=289, InputMask=replicate('X',40)

  func oDESCOD_1_16.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_BARKEY))
    endwith
  endfunc


  add object oTIPCON_1_17 as StdCombo with uid="TFWOFPLCCW",rtseq=14,rtrep=.f.,left=90,top=317,width=134,height=21;
    , ToolTipText = "Indica la provenienza del codice: interna, cliente o fornitore o altro";
    , HelpContextID = 45231306;
    , cFormVar="w_TIPCON",RowSource=""+"Interna,"+"Cliente,"+"Fornitore,"+"A quantit� e valore,"+"A valore,"+"Descrittivo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_17.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'M',;
    iif(this.value =5,'I',;
    iif(this.value =6,'D',;
    space(20))))))))
  endfunc
  func oTIPCON_1_17.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_17.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='R',1,;
      iif(this.Parent.oContained.w_TIPCON=='C',2,;
      iif(this.Parent.oContained.w_TIPCON=='F',3,;
      iif(this.Parent.oContained.w_TIPCON=='M',4,;
      iif(this.Parent.oContained.w_TIPCON=='I',5,;
      iif(this.Parent.oContained.w_TIPCON=='D',6,;
      0))))))
  endfunc

  func oTIPCON_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
   endif
  endfunc

  func oTIPCON_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_23('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oObj_1_19 as cp_runprogram with uid="BMYSXRCLHG",left=0, top=402, width=219,height=20,;
    caption='GSMA_BCA(CALC)',;
   bGlobalFont=.t.,;
    prg='GSMA_BCA("CALC")',;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 265028135


  add object oObj_1_20 as cp_runprogram with uid="FLGVXXXQEY",left=0, top=425, width=219,height=20,;
    caption='GSMA_BCA(SELE)',;
   bGlobalFont=.t.,;
    prg='GSMA_BCA("SELE")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 267145767

  add object oCODCON_1_23 as StdField with uid="FVIUQPIAOB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Eventuale cliente / fornitore di provenienza",;
    HelpContextID = 45279194,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=226, Top=317, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oCODCON_1_23.mHide()
    with this.Parent.oContained
      return (NOT .w_TIPCON $ 'CF')
    endwith
  endfunc

  func oCODCON_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCRI_1_24 as StdField with uid="FXEJPVJFAX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 125960650,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=364, Top=317, InputMask=replicate('X',40)

  func oDESCRI_1_24.mHide()
    with this.Parent.oContained
      return (NOT .w_TIPCON $ 'CF')
    endwith
  endfunc

  add object oCATIPCO3_1_25 as StdField with uid="XIFJVRLNEU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CATIPCO3", cQueryName = "CATIPCO3",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia collo",;
    HelpContextID = 228324775,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=90, Top=345, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_CATIPCO3"

  func oCATIPCO3_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLIMBA = 'S')
    endwith
   endif
  endfunc

  func oCATIPCO3_1_25.mHide()
    with this.Parent.oContained
      return (.w_CAFLIMBA <> 'S' OR NOT INLIST(.w_TIPBAR,'2','C'))
    endwith
  endfunc

  func oCATIPCO3_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATIPCO3_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATIPCO3_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oCATIPCO3_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tipologie colli",'',this.parent.oContained
  endproc
  proc oCATIPCO3_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CATIPCO3
     i_obj.ecpSave()
  endproc

  add object oFLSTAM_1_26 as StdCheck with uid="WOQLAXGLXX",rtseq=19,rtrep=.f.,left=414, top=261, caption="Stampa etichette",;
    ToolTipText = "Se attivo: il codice compare nella stampa delle etichette",;
    HelpContextID = 75561642,;
    cFormVar="w_FLSTAM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSTAM_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLSTAM_1_26.GetRadio()
    this.Parent.oContained.w_FLSTAM = this.RadioValue()
    return .t.
  endfunc

  func oFLSTAM_1_26.SetRadio()
    this.Parent.oContained.w_FLSTAM=trim(this.Parent.oContained.w_FLSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_FLSTAM=='S',1,;
      0)
  endfunc

  func oFLSTAM_1_26.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCAFLIMBA_1_27 as StdCheck with uid="YKXQHQHUEI",rtseq=20,rtrep=.f.,left=552, top=289, caption="Imballo",;
    ToolTipText = "Se attivo: il codice di ricerca identifica un imballo",;
    HelpContextID = 67753369,;
    cFormVar="w_CAFLIMBA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLIMBA_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCAFLIMBA_1_27.GetRadio()
    this.Parent.oContained.w_CAFLIMBA = this.RadioValue()
    return .t.
  endfunc

  func oCAFLIMBA_1_27.SetRadio()
    this.Parent.oContained.w_CAFLIMBA=trim(this.Parent.oContained.w_CAFLIMBA)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLIMBA=='S',1,;
      0)
  endfunc

  func oCAFLIMBA_1_27.mHide()
    with this.Parent.oContained
      return (NOT INLIST( .w_TIPBAR, '2','C'))
    endwith
  endfunc


  add object oBtn_1_29 as StdButton with uid="CRDAMXTYOM",left=614, top=350, width=48,height=45,;
    CpPicture="bmp\genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per inserire/aggiornare il codice di ricerca";
    , HelpContextID = 210411417;
    , tabstop=.f., Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSMA_BCC(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (vartype(.oParentObject .w_ARDESSUP)<>'C')
     endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="LEHWDKFQJC",left=667, top=350, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 221958842;
    , Caption='\<Uscita';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCOL_1_35 as StdField with uid="KLWDPJWWAV",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCOL", cQueryName = "DESCOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 78774730,;
   bGlobalFont=.t.,;
    Height=21, Width=264, Left=152, Top=345, InputMask=replicate('X',35)

  func oDESCOL_1_35.mHide()
    with this.Parent.oContained
      return (.w_CAFLIMBA <> 'S' OR NOT INLIST(.w_TIPBAR,'2','C'))
    endwith
  endfunc

  add object oCODESC_1_37 as StdField with uid="HXCWQVUDCC",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODESC", cQueryName = "CODESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice legato al cliente/fornitore che completer� come suffisso il codice di ricerca",;
    HelpContextID = 225503194,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=650, Top=317, InputMask=replicate('X',5)

  func oCODESC_1_37.mHide()
    with this.Parent.oContained
      return (Empty(.w_CODESC) Or g_FLCESC<>'S')
    endwith
  endfunc


  add object oObj_1_39 as cp_setobjprop with uid="OXOGLYIYYD",left=228, top=402, width=188,height=23,;
    caption='ToolTip di w_BARKEY',;
   bGlobalFont=.t.,;
    cObj="w_BARKEY",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 20460184

  add object oStr_1_28 as StdString with uid="AGSINRSFCV",Visible=.t., Left=4, Top=316,;
    Alignment=1, Width=80, Height=15,;
    Caption="Tipo codifica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="LOALEZCBSN",Visible=.t., Left=259, Top=236,;
    Alignment=2, Width=198, Height=18,;
    Caption="Inserimento nuovi codici"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="COMMHFNUIN",Visible=.t., Left=12, Top=262,;
    Alignment=1, Width=72, Height=17,;
    Caption="Tipo codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="BVULZSCPIE",Visible=.t., Left=39, Top=289,;
    Alignment=1, Width=45, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="VIBBOOPCVA",Visible=.t., Left=7, Top=346,;
    Alignment=1, Width=77, Height=18,;
    Caption="Tipo collo:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_CAFLIMBA <> 'S' OR NOT INLIST(.w_TIPBAR,'2','C'))
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="KCIYPLTYBL",Visible=.t., Left=649, Top=301,;
    Alignment=0, Width=54, Height=15,;
    Caption="Suffisso"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (Empty(.w_CODESC) Or g_FLCESC<>'S')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="RDUOUVEPBH",Visible=.t., Left=246, Top=262,;
    Alignment=1, Width=68, Height=18,;
    Caption="N.decimali:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (!(.w_TIPBAR$'A-B') OR .w_ARFLPECO<>'P' OR (.w_ARTIPART$'FM-FO-DE'))
    endwith
  endfunc

  add object oBox_1_32 as StdBox with uid="VRHFDTUQRN",left=4, top=256, width=717,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kca','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
