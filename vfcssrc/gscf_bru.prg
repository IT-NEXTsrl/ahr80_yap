* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_bru                                                        *
*              Gestione di GSCG_MRU                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-01-18                                                      *
* Last revis.: 2012-08-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscf_bru",oParentObject,m.pOPER)
return(i_retval)

define class tgscf_bru as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_GSCF_MRU = .NULL.
  w_AZIENDA = space(5)
  w_NUM = 0
  * --- WorkFile variables
  cpgroups_idx=0
  UTE_AZI_idx=0
  cpusers_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione di GSCG_MRU
    this.w_GSCF_MRU = THIS.oParentObject
    this.w_AZIENDA = i_CODAZI
    this.w_GSCF_MRU.MarkPos()     
    do case
      case this.pOPER="INS_UTE"
        * --- Select from UTE_AZI
        i_nConn=i_TableProp[this.UTE_AZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UTE_AZI_idx,2],.t.,this.UTE_AZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select UACODUTE  from "+i_cTable+" UTE_AZI ";
              +" where UACODAZI="+cp_ToStrODBC(this.w_AZIENDA)+"";
               ,"_Curs_UTE_AZI")
        else
          select UACODUTE from (i_cTable);
           where UACODAZI=this.w_AZIENDA;
            into cursor _Curs_UTE_AZI
        endif
        if used('_Curs_UTE_AZI')
          select _Curs_UTE_AZI
          locate for 1=1
          do while not(eof())
          this.w_NUM = _Curs_UTE_AZI.UACODUTE
          if this.w_GSCF_MRU.Search("nvl(t_RUCODUTE,0)=" + CP_TOSTRODBC(this.w_NUM)) <= 0
            this.w_GSCF_MRU.AddRow()     
            this.oParentObject.w_RUCODUTE = this.w_NUM
            * --- Read from cpusers
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.cpusers_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2],.t.,this.cpusers_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "name"+;
                " from "+i_cTable+" cpusers where ";
                    +"code = "+cp_ToStrODBC(this.oParentObject.w_RUCODUTE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                name;
                from (i_cTable) where;
                    code = this.oParentObject.w_RUCODUTE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_RUNAMUTE = NVL(cp_ToDate(_read_.name),cp_NullValue(_read_.name))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_GSCF_MRU.SaveRow()     
          endif
            select _Curs_UTE_AZI
            continue
          enddo
          use
        endif
      case this.pOPER="INS_GRU"
        * --- Select from cpgroups
        i_nConn=i_TableProp[this.cpgroups_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpgroups_idx,2],.t.,this.cpgroups_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select code,name,grptype  from "+i_cTable+" cpgroups ";
               ,"_Curs_cpgroups")
        else
          select code,name,grptype from (i_cTable);
            into cursor _Curs_cpgroups
        endif
        if used('_Curs_cpgroups')
          select _Curs_cpgroups
          locate for 1=1
          do while not(eof())
          if nvl(_Curs_cpgroups.grptype,"G") = "G"
            this.w_NUM = _Curs_cpgroups.code
            if this.w_GSCF_MRU.Search("nvl(t_RUCODGRU,0)=" + CP_TOSTRODBC(this.w_NUM)) <= 0
              this.w_GSCF_MRU.AddRow()     
              this.oParentObject.w_RUCODGRU = this.w_NUM
              this.oParentObject.w_RUNAMGRU = _Curs_cpgroups.name
              this.w_GSCF_MRU.SaveRow()     
            endif
          endif
            select _Curs_cpgroups
            continue
          enddo
          use
        endif
    endcase
    this.w_GSCF_MRU.RePos()     
    this.w_GSCF_MRU.Refresh()     
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='cpgroups'
    this.cWorkTables[2]='UTE_AZI'
    this.cWorkTables[3]='cpusers'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_UTE_AZI')
      use in _Curs_UTE_AZI
    endif
    if used('_Curs_cpgroups')
      use in _Curs_cpgroups
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
