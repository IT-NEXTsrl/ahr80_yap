* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_sib                                                        *
*              Stampa movimenti INTRA                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_14]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-13                                                      *
* Last revis.: 2012-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_sib",oParentObject))

* --- Class definition
define class tgsar_sib as StdForm
  Top    = 33
  Left   = 86

  * --- Standard Properties
  Width  = 495
  Height = 257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-08"
  HelpContextID=258570089
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  ELEIMAST_IDX = 0
  ELEIDETT_IDX = 0
  cPrg = "gsar_sib"
  cComment = "Stampa movimenti INTRA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_tipmov = space(3)
  w_datmini = ctod('  /  /  ')
  w_datmfin = ctod('  /  /  ')
  w_datcini = ctod('  /  /  ')
  w_datcfin = ctod('  /  /  ')
  w_datdini = ctod('  /  /  ')
  w_datdfin = ctod('  /  /  ')
  w_tipcon = space(1)
  o_tipcon = space(1)
  w_codice = space(15)
  w_descri = space(28)
  w_OBTEST = space(10)
  w_DATOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_sibPag1","gsar_sib",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.otipmov_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ELEIMAST'
    this.cWorkTables[3]='ELEIDETT'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec("QUERY\GSAR_SIB.VQR",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsar_sib
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_tipmov=space(3)
      .w_datmini=ctod("  /  /  ")
      .w_datmfin=ctod("  /  /  ")
      .w_datcini=ctod("  /  /  ")
      .w_datcfin=ctod("  /  /  ")
      .w_datdini=ctod("  /  /  ")
      .w_datdfin=ctod("  /  /  ")
      .w_tipcon=space(1)
      .w_codice=space(15)
      .w_descri=space(28)
      .w_OBTEST=space(10)
      .w_DATOBSO=ctod("  /  /  ")
        .w_CODAZI = i_CODAZI
        .w_tipmov = "CE"
          .DoRTCalc(3,9,.f.)
        .w_codice = Space(15)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_codice))
          .link_1_10('Full')
        endif
        .w_descri = ""
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate(IIF(.w_TIPCON='C', AH_MsgFormat("Cliente:"),IIF(.w_TIPCON='F',AH_MsgFormat("Fornitore:"),'')),'','')
        .w_OBTEST = i_INIDAT
    endwith
    this.DoRTCalc(13,13,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_codice = Space(15)
          .link_1_10('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(IIF(.w_TIPCON='C', AH_MsgFormat("Cliente:"),IIF(.w_TIPCON='F',AH_MsgFormat("Fornitore:"),'')),'','')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(IIF(.w_TIPCON='C', AH_MsgFormat("Cliente:"),IIF(.w_TIPCON='F',AH_MsgFormat("Fornitore:"),'')),'','')
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.ocodice_1_10.enabled = this.oPgFrm.Page1.oPag.ocodice_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.ocodice_1_10.visible=!this.oPgFrm.Page1.oPag.ocodice_1_10.mHide()
    this.oPgFrm.Page1.oPag.odescri_1_11.visible=!this.oPgFrm.Page1.oPag.odescri_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=codice
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codice) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_codice)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipcon);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_tipcon;
                     ,'ANCODICE',trim(this.w_codice))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_codice)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_codice)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipcon);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_codice)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_tipcon);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_codice) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'ocodice_1_10'),i_cWhere,'GSAR_BZC',"Clienti\fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_tipcon<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_tipcon);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codice)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_codice);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipcon);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_tipcon;
                       ,'ANCODICE',this.w_codice)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codice = NVL(_Link_.ANCODICE,space(15))
      this.w_descri = NVL(_Link_.ANDESCRI,space(28))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_codice = space(15)
      endif
      this.w_descri = space(28)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_DATOBSO) or .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_codice = space(15)
        this.w_descri = space(28)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codice Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.otipmov_1_2.RadioValue()==this.w_tipmov)
      this.oPgFrm.Page1.oPag.otipmov_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.odatmini_1_3.value==this.w_datmini)
      this.oPgFrm.Page1.oPag.odatmini_1_3.value=this.w_datmini
    endif
    if not(this.oPgFrm.Page1.oPag.odatmfin_1_4.value==this.w_datmfin)
      this.oPgFrm.Page1.oPag.odatmfin_1_4.value=this.w_datmfin
    endif
    if not(this.oPgFrm.Page1.oPag.odatcini_1_5.value==this.w_datcini)
      this.oPgFrm.Page1.oPag.odatcini_1_5.value=this.w_datcini
    endif
    if not(this.oPgFrm.Page1.oPag.odatcfin_1_6.value==this.w_datcfin)
      this.oPgFrm.Page1.oPag.odatcfin_1_6.value=this.w_datcfin
    endif
    if not(this.oPgFrm.Page1.oPag.odatdini_1_7.value==this.w_datdini)
      this.oPgFrm.Page1.oPag.odatdini_1_7.value=this.w_datdini
    endif
    if not(this.oPgFrm.Page1.oPag.odatdfin_1_8.value==this.w_datdfin)
      this.oPgFrm.Page1.oPag.odatdfin_1_8.value=this.w_datdfin
    endif
    if not(this.oPgFrm.Page1.oPag.otipcon_1_9.RadioValue()==this.w_tipcon)
      this.oPgFrm.Page1.oPag.otipcon_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ocodice_1_10.value==this.w_codice)
      this.oPgFrm.Page1.oPag.ocodice_1_10.value=this.w_codice
    endif
    if not(this.oPgFrm.Page1.oPag.odescri_1_11.value==this.w_descri)
      this.oPgFrm.Page1.oPag.odescri_1_11.value=this.w_descri
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_datmfin)) OR  (.w_datmini <= .w_datmfin))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odatmini_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not((.w_datmini <= .w_datmfin) or (empty(.w_datmini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odatmfin_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale iniziale � maggiore della data finale")
          case   not((empty(.w_datcfin)) OR  (.w_datcini <= .w_datcfin))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odatcini_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale iniziale � maggiore della data finale")
          case   not((.w_datcfin <= .w_datcfin) or (empty(.w_datcini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odatcfin_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale iniziale � maggiore della data finale")
          case   not((empty(.w_datdfin)) OR  (.w_datdini <= .w_datdfin))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odatdini_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not((.w_datdini <= .w_datdfin) or (empty(.w_datdini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odatdfin_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale iniziale � maggiore della data finale")
          case   not(empty(.w_DATOBSO) or .w_OBTEST<.w_DATOBSO)  and not(empty(.w_tipcon))  and (not empty(.w_tipcon))  and not(empty(.w_codice))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ocodice_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_tipcon = this.w_tipcon
    return

enddefine

* --- Define pages as container
define class tgsar_sibPag1 as StdContainer
  Width  = 491
  height = 257
  stdWidth  = 491
  stdheight = 257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object otipmov_1_2 as StdCombo with uid="JWTGSWPKXU",rtseq=2,rtrep=.f.,left=148,top=13,width=189,height=21;
    , ToolTipText = "Tipo movimento selezionato";
    , HelpContextID = 234551606;
    , cFormVar="w_tipmov",RowSource=""+"Cessioni di beni,"+"Rettifiche cessioni di beni,"+"Acquisti di beni,"+"Rettifiche acquisti di beni,"+"Cessioni di servizi,"+"Acquisti di servizi,"+"Rettifiche acquisti di  servizi,"+"Rettifiche cessioni di servizi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func otipmov_1_2.RadioValue()
    return(iif(this.value =1,"CE",;
    iif(this.value =2,"RC",;
    iif(this.value =3,"AC",;
    iif(this.value =4,"RA",;
    iif(this.value =5,'CS',;
    iif(this.value =6,'AS',;
    iif(this.value =7,'RX',;
    iif(this.value =8,'RS',;
    space(3))))))))))
  endfunc
  func otipmov_1_2.GetRadio()
    this.Parent.oContained.w_tipmov = this.RadioValue()
    return .t.
  endfunc

  func otipmov_1_2.SetRadio()
    this.Parent.oContained.w_tipmov=trim(this.Parent.oContained.w_tipmov)
    this.value = ;
      iif(this.Parent.oContained.w_tipmov=="CE",1,;
      iif(this.Parent.oContained.w_tipmov=="RC",2,;
      iif(this.Parent.oContained.w_tipmov=="AC",3,;
      iif(this.Parent.oContained.w_tipmov=="RA",4,;
      iif(this.Parent.oContained.w_tipmov=='CS',5,;
      iif(this.Parent.oContained.w_tipmov=='AS',6,;
      iif(this.Parent.oContained.w_tipmov=='RX',7,;
      iif(this.Parent.oContained.w_tipmov=='RS',8,;
      0))))))))
  endfunc

  add object odatmini_1_3 as StdField with uid="ZDSMUWWRVM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_datmini", cQueryName = "datmini",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data movimento di inizio stampa",;
    HelpContextID = 174378954,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=148, Top=44

  func odatmini_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_datmfin)) OR  (.w_datmini <= .w_datmfin))
    endwith
    return bRes
  endfunc

  add object odatmfin_1_4 as StdField with uid="LJPVSVWLXC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_datmfin", cQueryName = "datmfin",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale iniziale � maggiore della data finale",;
    ToolTipText = "Data movimento di fine stampa",;
    HelpContextID = 261410762,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=148, Top=69

  func odatmfin_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_datmini <= .w_datmfin) or (empty(.w_datmini)))
    endwith
    return bRes
  endfunc

  add object odatcini_1_5 as StdField with uid="KANYEYHMNK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_datcini", cQueryName = "datcini",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale iniziale � maggiore della data finale",;
    ToolTipText = "Data documento di inizio stampa",;
    HelpContextID = 175034314,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=410, Top=44

  func odatcini_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_datcfin)) OR  (.w_datcini <= .w_datcfin))
    endwith
    return bRes
  endfunc

  add object odatcfin_1_6 as StdField with uid="HFVNHFPSXO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_datcfin", cQueryName = "datcfin",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale iniziale � maggiore della data finale",;
    ToolTipText = "Data documento di fine stampa",;
    HelpContextID = 262066122,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=410, Top=69

  func odatcfin_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_datcfin <= .w_datcfin) or (empty(.w_datcini)))
    endwith
    return bRes
  endfunc

  add object odatdini_1_7 as StdField with uid="LFXBCQUYNH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_datdini", cQueryName = "datdini",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data documento di inizio stampa",;
    HelpContextID = 174968778,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=148, Top=99

  func odatdini_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_datdfin)) OR  (.w_datdini <= .w_datdfin))
    endwith
    return bRes
  endfunc

  add object odatdfin_1_8 as StdField with uid="NIKPGGDUCH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_datdfin", cQueryName = "datdfin",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale iniziale � maggiore della data finale",;
    ToolTipText = "Data documento di fine stampa",;
    HelpContextID = 262000586,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=148, Top=124

  func odatdfin_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_datdini <= .w_datdfin) or (empty(.w_datdini)))
    endwith
    return bRes
  endfunc


  add object otipcon_1_9 as StdCombo with uid="RYAXJXTOMO",value=3,rtseq=9,rtrep=.f.,left=148,top=152,width=125,height=21;
    , HelpContextID = 99678518;
    , cFormVar="w_tipcon",RowSource=""+"Cliente,"+"Fornitore,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func otipcon_1_9.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,' ',;
    'C'))))
  endfunc
  func otipcon_1_9.GetRadio()
    this.Parent.oContained.w_tipcon = this.RadioValue()
    return .t.
  endfunc

  func otipcon_1_9.SetRadio()
    this.Parent.oContained.w_tipcon=trim(this.Parent.oContained.w_tipcon)
    this.value = ;
      iif(this.Parent.oContained.w_tipcon=='C',1,;
      iif(this.Parent.oContained.w_tipcon=='F',2,;
      iif(this.Parent.oContained.w_tipcon=='',3,;
      0)))
  endfunc

  func otipcon_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_codice)
        bRes2=.link_1_10('Full')
      endif
    endwith
    return bRes
  endfunc

  add object ocodice_1_10 as StdField with uid="LHMBTVUNGK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_codice", cQueryName = "codice",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Cliente o fornitore selezionato",;
    HelpContextID = 63554010,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=148, Top=180, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_tipcon", oKey_2_1="ANCODICE", oKey_2_2="this.w_codice"

  func ocodice_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_tipcon))
    endwith
   endif
  endfunc

  func ocodice_1_10.mHide()
    with this.Parent.oContained
      return (empty(.w_tipcon))
    endwith
  endfunc

  func ocodice_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc ocodice_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocodice_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_tipcon)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_tipcon)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'ocodice_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti\fornitori",'',this.parent.oContained
  endproc
  proc ocodice_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_tipcon
     i_obj.w_ANCODICE=this.parent.oContained.w_codice
     i_obj.ecpSave()
  endproc

  add object odescri_1_11 as StdField with uid="TMQQUAPHCG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_descri", cQueryName = "descri",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(28), bMultilanguage =  .f.,;
    HelpContextID = 18949174,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=281, Top=180, cSayPict='repl("X",28)', InputMask=replicate('X',28)

  func odescri_1_11.mHide()
    with this.Parent.oContained
      return (empty(.w_tipcon))
    endwith
  endfunc


  add object oObj_1_19 as cp_calclbl with uid="GOVDVZFRKC",left=26, top=181, width=120,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 187863014


  add object oBtn_1_22 as StdButton with uid="QVTYADDHUR",left=381, top=206, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 258541338;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      vx_exec("QUERY\GSAR_SIB.VQR",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="YGANWFFQSX",left=435, top=206, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 251252666;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_12 as StdString with uid="SPNRWJIKPI",Visible=.t., Left=24, Top=44,;
    Alignment=1, Width=122, Height=15,;
    Caption="Da data movimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="GXTXZUDYPX",Visible=.t., Left=27, Top=13,;
    Alignment=1, Width=119, Height=15,;
    Caption="Tipo movimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="FZQANZOXJB",Visible=.t., Left=27, Top=69,;
    Alignment=1, Width=119, Height=15,;
    Caption="A data movimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="UEWAZFBGXE",Visible=.t., Left=269, Top=44,;
    Alignment=1, Width=137, Height=15,;
    Caption="Da data competenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="HUGXOJUWIR",Visible=.t., Left=269, Top=69,;
    Alignment=1, Width=137, Height=15,;
    Caption="A data competenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="WMPJSVFXGJ",Visible=.t., Left=7, Top=99,;
    Alignment=1, Width=139, Height=15,;
    Caption="Da data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="IDALZYYFYY",Visible=.t., Left=22, Top=124,;
    Alignment=1, Width=124, Height=15,;
    Caption="A data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="TXSKCEEPTT",Visible=.t., Left=28, Top=153,;
    Alignment=1, Width=118, Height=18,;
    Caption="Tipologia Intestatario:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_sib','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
