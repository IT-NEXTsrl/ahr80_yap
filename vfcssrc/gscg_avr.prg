* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_avr                                                        *
*              Voci di bilancio mastri-conti                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_13]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-25                                                      *
* Last revis.: 2006-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_avr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_avr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_avr")
  return

* --- Class definition
define class tgscg_avr as StdPCForm
  Width  = 674
  Height = 281
  Top    = 95
  Left   = 26
  cComment = "Voci di bilancio mastri-conti"
  cPrg = "gscg_avr"
  HelpContextID=59385193
  add object cnt as tcgscg_avr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_avr as PCContext
  w_VRCODVOC = space(15)
  w_VRDESVOC = space(50)
  w_VR__NOTE = space(10)
  w_VRROWORD = 0
  proc Save(oFrom)
    this.w_VRCODVOC = oFrom.w_VRCODVOC
    this.w_VRDESVOC = oFrom.w_VRDESVOC
    this.w_VR__NOTE = oFrom.w_VR__NOTE
    this.w_VRROWORD = oFrom.w_VRROWORD
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_VRCODVOC = this.w_VRCODVOC
    oTo.w_VRDESVOC = this.w_VRDESVOC
    oTo.w_VR__NOTE = this.w_VR__NOTE
    oTo.w_VRROWORD = this.w_VRROWORD
    PCContext::Load(oTo)
enddefine

define class tcgscg_avr as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 674
  Height = 281
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2006-11-27"
  HelpContextID=59385193
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  VOCIRICL_IDX = 0
  DETTRICL_IDX = 0
  cFile = "VOCIRICL"
  cKeySelect = "VRCODVOC,VRROWORD"
  cKeyWhere  = "VRCODVOC=this.w_VRCODVOC and VRROWORD=this.w_VRROWORD"
  cKeyWhereODBC = '"VRCODVOC="+cp_ToStrODBC(this.w_VRCODVOC)';
      +'+" and VRROWORD="+cp_ToStrODBC(this.w_VRROWORD)';

  cKeyWhereODBCqualified = '"VOCIRICL.VRCODVOC="+cp_ToStrODBC(this.w_VRCODVOC)';
      +'+" and VOCIRICL.VRROWORD="+cp_ToStrODBC(this.w_VRROWORD)';

  cPrg = "gscg_avr"
  cComment = "Voci di bilancio mastri-conti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_VRCODVOC = space(15)
  w_VRDESVOC = space(50)
  w_VR__NOTE = space(0)
  w_VRROWORD = 0

  * --- Children pointers
  GSCG_MDV = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSCG_MDV additive
    with this
      .Pages(1).addobject("oPag","tgscg_avrPag1","gscg_avr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 937738
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVR__NOTE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSCG_MDV
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DETTRICL'
    this.cWorkTables[2]='VOCIRICL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VOCIRICL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VOCIRICL_IDX,3]
  return

  function CreateChildren()
    this.GSCG_MDV = CREATEOBJECT('stdDynamicChild',this,'GSCG_MDV',this.oPgFrm.Page1.oPag.oLinkPC_1_4)
    this.GSCG_MDV.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgscg_avr'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSCG_MDV)
      this.GSCG_MDV.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSCG_MDV.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSCG_MDV.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MDV)
      this.GSCG_MDV.DestroyChildrenChain()
      this.GSCG_MDV=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_4')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MDV.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MDV.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MDV.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_MDV.SetKey(;
            .w_VRCODVOC,"DVCODVOC";
            ,.w_VRROWORD,"DVROWORD";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_MDV.ChangeRow(this.cRowID+'      1',1;
             ,.w_VRCODVOC,"DVCODVOC";
             ,.w_VRROWORD,"DVROWORD";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_MDV)
        i_f=.GSCG_MDV.BuildFilter()
        if !(i_f==.GSCG_MDV.cQueryFilter)
          i_fnidx=.GSCG_MDV.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MDV.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MDV.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MDV.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MDV.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from VOCIRICL where VRCODVOC=KeySet.VRCODVOC
    *                            and VRROWORD=KeySet.VRROWORD
    *
    i_nConn = i_TableProp[this.VOCIRICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIRICL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VOCIRICL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VOCIRICL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VOCIRICL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'VRCODVOC',this.w_VRCODVOC  ,'VRROWORD',this.w_VRROWORD  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_VRCODVOC = NVL(VRCODVOC,space(15))
        .w_VRDESVOC = NVL(VRDESVOC,space(50))
        .w_VR__NOTE = NVL(VR__NOTE,space(0))
        .w_VRROWORD = NVL(VRROWORD,0)
        cp_LoadRecExtFlds(this,'VOCIRICL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_VRCODVOC = space(15)
      .w_VRDESVOC = space(50)
      .w_VR__NOTE = space(0)
      .w_VRROWORD = 0
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'VOCIRICL')
    this.DoRTCalc(1,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oVR__NOTE_1_3.enabled = i_bVal
    endwith
    this.GSCG_MDV.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'VOCIRICL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MDV.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VOCIRICL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VRCODVOC,"VRCODVOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VRDESVOC,"VRDESVOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VR__NOTE,"VR__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VRROWORD,"VRROWORD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gscg_avr
    * --- In certi casi non viene testato linserimento di VOCIRICL
    * --- pur avendo inserito DETTRICL
    * --- La conseguenza e' un errore di Integrita' del Database
    this.bUpdated=.T.
    
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VOCIRICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIRICL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.VOCIRICL_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VOCIRICL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VOCIRICL')
        i_extval=cp_InsertValODBCExtFlds(this,'VOCIRICL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(VRCODVOC,VRDESVOC,VR__NOTE,VRROWORD "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_VRCODVOC)+;
                  ","+cp_ToStrODBC(this.w_VRDESVOC)+;
                  ","+cp_ToStrODBC(this.w_VR__NOTE)+;
                  ","+cp_ToStrODBC(this.w_VRROWORD)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VOCIRICL')
        i_extval=cp_InsertValVFPExtFlds(this,'VOCIRICL')
        cp_CheckDeletedKey(i_cTable,0,'VRCODVOC',this.w_VRCODVOC,'VRROWORD',this.w_VRROWORD)
        INSERT INTO (i_cTable);
              (VRCODVOC,VRDESVOC,VR__NOTE,VRROWORD  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_VRCODVOC;
                  ,this.w_VRDESVOC;
                  ,this.w_VR__NOTE;
                  ,this.w_VRROWORD;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.VOCIRICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIRICL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.VOCIRICL_IDX,i_nConn)
      *
      * update VOCIRICL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'VOCIRICL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " VRDESVOC="+cp_ToStrODBC(this.w_VRDESVOC)+;
             ",VR__NOTE="+cp_ToStrODBC(this.w_VR__NOTE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'VOCIRICL')
        i_cWhere = cp_PKFox(i_cTable  ,'VRCODVOC',this.w_VRCODVOC  ,'VRROWORD',this.w_VRROWORD  )
        UPDATE (i_cTable) SET;
              VRDESVOC=this.w_VRDESVOC;
             ,VR__NOTE=this.w_VR__NOTE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_MDV : Saving
      this.GSCG_MDV.ChangeRow(this.cRowID+'      1',0;
             ,this.w_VRCODVOC,"DVCODVOC";
             ,this.w_VRROWORD,"DVROWORD";
             )
      this.GSCG_MDV.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSCG_MDV : Deleting
    this.GSCG_MDV.ChangeRow(this.cRowID+'      1',0;
           ,this.w_VRCODVOC,"DVCODVOC";
           ,this.w_VRROWORD,"DVROWORD";
           )
    this.GSCG_MDV.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VOCIRICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIRICL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.VOCIRICL_IDX,i_nConn)
      *
      * delete VOCIRICL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'VRCODVOC',this.w_VRCODVOC  ,'VRROWORD',this.w_VRROWORD  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VOCIRICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIRICL_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVR__NOTE_1_3.value==this.w_VR__NOTE)
      this.oPgFrm.Page1.oPag.oVR__NOTE_1_3.value=this.w_VR__NOTE
    endif
    cp_SetControlsValueExtFlds(this,'VOCIRICL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSCG_MDV.CheckForm()
      if i_bres
        i_bres=  .GSCG_MDV.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSCG_MDV : Depends On
    this.GSCG_MDV.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_avrPag1 as StdContainer
  Width  = 670
  height = 281
  stdWidth  = 670
  stdheight = 281
  resizeXpos=380
  resizeYpos=254
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVR__NOTE_1_3 as StdMemo with uid="BLYHAJBLHD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VR__NOTE", cQueryName = "VR__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note aggiuntive",;
    HelpContextID = 12264091,;
   bGlobalFont=.t.,;
    Height=60, Width=662, Left=3, Top=5


  add object oLinkPC_1_4 as stdDynamicChildContainer with uid="RSJIHYDDHX",left=3, top=64, width=663, height=213, bOnScreen=.t.;

enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_avr','VOCIRICL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".VRCODVOC=VOCIRICL.VRCODVOC";
  +" and "+i_cAliasName2+".VRROWORD=VOCIRICL.VRROWORD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
