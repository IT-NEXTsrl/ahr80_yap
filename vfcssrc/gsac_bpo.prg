* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bpo                                                        *
*              Stampa PDA                                                      *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_233]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-20                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bpo",oParentObject)
return(i_retval)

define class tgsac_bpo as StdBatch
  * --- Local variables
  w_PrimoPer = 0
  i = 0
  w_UltimoPer = 0
  w_MPSPER = 0
  w_Periodo = 0
  w_PerDesc = 0
  * --- WorkFile variables
  PDA_TPER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa PDA (da GSAC_SPD)
    if EMPTY(this.oParentObject.w_PERINI)
      this.oParentObject.w_PERINI = "000"
      this.oParentObject.w_PERFIN = right("000"+alltrim(str(this.oParentObject.w_INTERPER,3,0)),3)
    endif
    this.w_PrimoPer = val(this.oParentObject.w_PERINI)
    this.w_UltimoPer = val(this.oParentObject.w_PERFIN)
    * --- Tracciato record del cursore di stampa
    do vq_exec with "QUERY\GSAC2BSM",this,"__tmp__"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    =wrCursor("__tmp__")
    * --- Dati da stampare (pivottati)
    do vq_exec with "QUERY\GSAC_BSM",this,"_smps_"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    select "_smps_"
    scan
    * --- resetta i valori
    this.i = this.w_PrimoPer
    do while this.i <= this.w_UltimoPer
      l_mps = right("000"+alltrim(str(this.i,2,0)),3)
      m.mps_&l_mps = -1
      this.i = this.i + 1
    enddo
    scatter memvar
    * --- Prepara i valori da inserire su __tmp__
    this.i = 0
    do while this.i<=this.oParentObject.w_INTERPER
      l_per = right("000"+alltrim(str(this.i,2,0)),3)
      l_mps = right("000"+alltrim(str( this.i+this.w_PrimoPer, 2,0)),3)
      this.w_MPSPER = m.mps_&l_mps
      if this.w_MPSPER>0
        m.per_&l_per = this.w_MPSPER
      else
        m.per_&l_per = 0
      endif
      this.i = this.i + 1
    enddo
    select __tmp__
    append blank
    gather memvar
    endscan
    * --- Prepara il titolo delle colonne per il report di stampa
    dimension tit(this.oParentObject.w_INTERPER+1)
    * --- Select from PDA_TPER
    i_nConn=i_TableProp[this.PDA_TPER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PDA_TPER_idx,2],.t.,this.PDA_TPER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PDA_TPER ";
          +" where TPPERASS>="+cp_ToStrODBC(this.oParentObject.w_PERINI)+" and TPPERASS<="+cp_ToStrODBC(this.oParentObject.w_PERFIN)+"";
           ,"_Curs_PDA_TPER")
    else
      select * from (i_cTable);
       where TPPERASS>=this.oParentObject.w_PERINI and TPPERASS<=this.oParentObject.w_PERFIN;
        into cursor _Curs_PDA_TPER
    endif
    if used('_Curs_PDA_TPER')
      select _Curs_PDA_TPER
      locate for 1=1
      do while not(eof())
      if not empty(_Curs_PDA_TPER.TPPERASS)
        tit(val(_Curs_PDA_TPER.TPPERASS)+1-this.w_PrimoPer) = iif(_Curs_PDA_TPER.TPPERREL="XXXX", "", _Curs_PDA_TPER.TPPERREL)
      endif
        select _Curs_PDA_TPER
        continue
      enddo
      use
    endif
    codini = this.oParentObject.w_CODINI
    codfin = this.oParentObject.w_CODFIN
    codfor = this.oParentObject.w_CODFOR
    codcom = this.oParentObject.w_CODCOM
    codatt = this.oParentObject.w_CODATT
    if this.oParentObject.w_FLGDES
      * --- Stampa anche la Descrizione
      select __tmp__
      go top
      SELECT a.fmcodice, b.dbdescri,a.fmunimis,a.per_000,a.per_001,a.per_002,a.per_003,a.per_004,a.per_005,a.per_006,;
      a.per_007,a.per_008,a.per_009,a.per_010,a.per_011,a.per_012,a.per_013,a.per_014,a.per_015,a.per_016,a.per_017;
       FROM __tmp__ a, _smps_ b WHERE a.fmcodice = b.fmcodice into cursor app
      if used("__tmp__")
        use in __tmp__
      endif
      select * from app into cursor __tmp__
      CP_CHPRN("QUERY\GSAC_BSM1", " ", this)
    else
      * --- Non stampa la Descrizione
      CP_CHPRN("QUERY\GSAC_BSM", " ", this)
    endif
    * --- Rilascio cursori
    if used("app")
      use in app
    endif
    if used("_smps_")
      use in _smps_
    endif
    if used("__tmp__")
      use in __tmp__
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PDA_TPER'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PDA_TPER')
      use in _Curs_PDA_TPER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
