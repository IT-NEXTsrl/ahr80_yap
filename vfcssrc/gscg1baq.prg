* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg1baq                                                        *
*              Calcolo altri quadri da query                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_7]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-13                                                      *
* Last revis.: 2008-02-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ANNO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg1baq",oParentObject,m.w_ANNO)
return(i_retval)

define class tgscg1baq as StdBatch
  * --- Local variables
  w_ANNO = space(4)
  w_KEYATT = space(5)
  w_VE37 = 0
  w_VE38 = 0
  w_VF16 = 0
  w_VF17 = 0
  * --- WorkFile variables
  GSCG1BAQ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch eseguito nella query GSCG5BST utilizzata nel calcolo totalizzatori IVa
    *     le variabili definite non son altro che i parametri passati attraverso la query
    this.w_KEYATT = Space(5)
    this.w_ANNO = iif(Empty(this.w_ANNO),ALLTRIM(STR(YEAR(i_datsys))),this.w_ANNO)
    * --- Create temporary table GSCG1BAQ
    i_nIdx=cp_AddTableDef('GSCG1BAQ') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSCG1BAQ',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.GSCG1BAQ_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
     
 Dimension aResult(6) 
 aResult[1]=0 
 aResult[2]=0 
 aResult[3]=0 
 aResult[4]=0 
 aResult[5]=0 
 aResult[6]=0
    GSCG_BAQ(this,this.w_KEYATT,this.w_ANNO,@aResult)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_VE37 = aResult[1]
    this.w_VE38 = aResult[2]
    this.w_VF16 = aResult[3]
    this.w_VF17 = aResult[4]
    * --- Insert into GSCG1BAQ
    i_nConn=i_TableProp[this.GSCG1BAQ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GSCG1BAQ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GSCG1BAQ_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"VP37"+",VP38"+",VF16"+",VF17"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_VE37),'GSCG1BAQ','VP37');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VE38),'GSCG1BAQ','VP38');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VF16),'GSCG1BAQ','VF16');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VF17),'GSCG1BAQ','VF17');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'VP37',this.w_VE37,'VP38',this.w_VE38,'VF16',this.w_VF16,'VF17',this.w_VF17)
      insert into (i_cTable) (VP37,VP38,VF16,VF17 &i_ccchkf. );
         values (;
           this.w_VE37;
           ,this.w_VE38;
           ,this.w_VF16;
           ,this.w_VF17;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,w_ANNO)
    this.w_ANNO=w_ANNO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*GSCG1BAQ'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ANNO"
endproc
