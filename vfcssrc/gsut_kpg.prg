* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kpg                                                        *
*              FIltri fatturato                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-03-05                                                      *
* Last revis.: 2015-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kpg",oParentObject))

* --- Class definition
define class tgsut_kpg as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 407
  Height = 186
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-30"
  HelpContextID=149504873
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  CONTI_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  TIP_DOCU_IDX = 0
  cPrg = "gsut_kpg"
  cComment = "FIltri fatturato"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TITLE = space(254)
  w_CODAZI = space(5)
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_FLVEAC = space(1)
  w_CODCON = space(15)
  w_CODESE = space(4)
  w_DADATA = ctod('  /  /  ')
  o_DADATA = ctod('  /  /  ')
  w_ADATA = ctod('  /  /  ')
  o_ADATA = ctod('  /  /  ')
  w_CODAGE = space(5)
  w_DESCON = space(60)
  w_EDIT_FLVEAC = .F.
  w_DESAGE = space(35)
  w_MODIFYMODE = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kpgPag1","gsut_kpg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLVEAC_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ZONE'
    this.cWorkTables[4]='AGENTI'
    this.cWorkTables[5]='TIP_DOCU'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TITLE=space(254)
      .w_CODAZI=space(5)
      .w_TIPCON=space(1)
      .w_FLVEAC=space(1)
      .w_CODCON=space(15)
      .w_CODESE=space(4)
      .w_DADATA=ctod("  /  /  ")
      .w_ADATA=ctod("  /  /  ")
      .w_CODAGE=space(5)
      .w_DESCON=space(60)
      .w_EDIT_FLVEAC=.f.
      .w_DESAGE=space(35)
      .w_MODIFYMODE=.f.
      .w_TITLE=oParentObject.w_TITLE
      .w_TIPCON=oParentObject.w_TIPCON
      .w_FLVEAC=oParentObject.w_FLVEAC
      .w_CODCON=oParentObject.w_CODCON
      .w_CODESE=oParentObject.w_CODESE
      .w_DADATA=oParentObject.w_DADATA
      .w_ADATA=oParentObject.w_ADATA
      .w_CODAGE=oParentObject.w_CODAGE
      .w_EDIT_FLVEAC=oParentObject.w_EDIT_FLVEAC
          .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_codazi
        .w_TIPCON = IIF( Empty(.w_TIPCON), IIF(.w_FLVEAC='A', 'F', 'C'), .w_TIPCON)
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_CODCON))
          .link_1_6('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODESE))
          .link_1_7('Full')
        endif
        .DoRTCalc(7,9,.f.)
        if not(empty(.w_CODAGE))
          .link_1_11('Full')
        endif
          .DoRTCalc(10,12,.f.)
        .w_MODIFYMODE = Vartype(oGadgetManager.oCharmBar)='O' And oGadgetManager.oCharmBar.bModifyMode
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TITLE=.w_TITLE
      .oParentObject.w_TIPCON=.w_TIPCON
      .oParentObject.w_FLVEAC=.w_FLVEAC
      .oParentObject.w_CODCON=.w_CODCON
      .oParentObject.w_CODESE=.w_CODESE
      .oParentObject.w_DADATA=.w_DADATA
      .oParentObject.w_ADATA=.w_ADATA
      .oParentObject.w_CODAGE=.w_CODAGE
      .oParentObject.w_EDIT_FLVEAC=.w_EDIT_FLVEAC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_DADATA<>.w_DADATA.or. .o_ADATA<>.w_ADATA
            .w_ADATA = IIF(.w_ADATA<.w_DADATA,.w_DADATA,.w_ADATA)
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_CODAGE = IIF(.w_TIPCON<>'C', SPACE(5),.w_CODAGE)
          .link_1_11('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .cComment = ah_msgformat("Filtri %1", Lower(.w_TITLE))
          .Caption = .cComment
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLVEAC_1_5.enabled = this.oPgFrm.Page1.oPag.oFLVEAC_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODCON_1_6.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCODESE_1_7.enabled = this.oPgFrm.Page1.oPag.oCODESE_1_7.mCond()
    this.oPgFrm.Page1.oPag.oDADATA_1_8.enabled = this.oPgFrm.Page1.oPag.oDADATA_1_8.mCond()
    this.oPgFrm.Page1.oPag.oADATA_1_10.enabled = this.oPgFrm.Page1.oPag.oADATA_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCODAGE_1_11.enabled = this.oPgFrm.Page1.oPag.oCODAGE_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODAGE_1_11.visible=!this.oPgFrm.Page1.oPag.oCODAGE_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oDESAGE_1_22.visible=!this.oPgFrm.Page1.oPag.oDESAGE_1_22.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCON
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_6'),i_cWhere,'GSAR_BZC',"Intestatari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_7'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE_1_11'),i_cWhere,'',"Agenti",'GSUT_KPE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_4.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLVEAC_1_5.RadioValue()==this.w_FLVEAC)
      this.oPgFrm.Page1.oPag.oFLVEAC_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_6.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_6.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_7.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_7.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA_1_8.value==this.w_DADATA)
      this.oPgFrm.Page1.oPag.oDADATA_1_8.value=this.w_DADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oADATA_1_10.value==this.w_ADATA)
      this.oPgFrm.Page1.oPag.oADATA_1_10.value=this.w_ADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODAGE_1_11.value==this.w_CODAGE)
      this.oPgFrm.Page1.oPag.oCODAGE_1_11.value=this.w_CODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_17.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_17.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_22.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_22.value=this.w_DESAGE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCON = this.w_TIPCON
    this.o_DADATA = this.w_DADATA
    this.o_ADATA = this.w_ADATA
    return

enddefine

* --- Define pages as container
define class tgsut_kpgPag1 as StdContainer
  Width  = 403
  height = 186
  stdWidth  = 403
  stdheight = 186
  resizeXpos=279
  resizeYpos=131
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPCON_1_4 as StdCombo with uid="EWJOIDHGUN",rtseq=3,rtrep=.f.,left=76,top=9,width=76,height=22, enabled=.f.;
    , ToolTipText = "Tipo conto";
    , HelpContextID = 172952374;
    , cFormVar="w_TIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_4.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPCON_1_4.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_4.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      0))
  endfunc

  func oTIPCON_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oFLVEAC_1_5 as StdCombo with uid="XZSVXCIQWC",rtseq=4,rtrep=.f.,left=319,top=9,width=77,height=22;
    , ToolTipText = "Ciclo documentale";
    , HelpContextID = 242314582;
    , cFormVar="w_FLVEAC",RowSource=""+"Vendite,"+"Acquisti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC_1_5.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oFLVEAC_1_5.GetRadio()
    this.Parent.oContained.w_FLVEAC = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC_1_5.SetRadio()
    this.Parent.oContained.w_FLVEAC=trim(this.Parent.oContained.w_FLVEAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC=='V',1,;
      iif(this.Parent.oContained.w_FLVEAC=='A',2,;
      0))
  endfunc

  func oFLVEAC_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE And .w_EDIT_FLVEAC)
    endwith
   endif
  endfunc

  add object oCODCON_1_6 as StdField with uid="CJBTBANKOR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice intestatario",;
    HelpContextID = 172904486,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=76, Top=34, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  func oCODCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Intestatari",'',this.parent.oContained
  endproc
  proc oCODCON_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oCODESE_1_7 as StdField with uid="LRVLKWYKDI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio",;
    HelpContextID = 26234918,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=76, Top=58, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  func oCODESE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDADATA_1_8 as StdField with uid="TIPXHWDWPN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DADATA", cQueryName = "DADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Da data registrazione",;
    HelpContextID = 228344374,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=76, Top=82

  func oDADATA_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  add object oADATA_1_10 as StdField with uid="IZCDOYQPLA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ADATA", cQueryName = "ADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "A data registrazione",;
    HelpContextID = 75557626,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=200, Top=82

  func oADATA_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  add object oCODAGE_1_11 as StdField with uid="SBQYPQBBZE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODAGE", cQueryName = "CODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente",;
    HelpContextID = 13389862,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=76, Top=106, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE"

  func oCODAGE_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  func oCODAGE_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  func oCODAGE_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'GSUT_KPE.AGENTI_VZM',this.parent.oContained
  endproc


  add object oBtn_1_13 as StdButton with uid="OXLJQSTOTY",left=297, top=135, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 149476122;
    , Caption=MSG_OK;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="DKNBURUTRO",left=347, top=135, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 142187450;
    , Caption=MSG_CANCEL_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCON_1_17 as StdField with uid="JMZQAGXMVB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 172963382,;
   bGlobalFont=.t.,;
    Height=21, Width=196, Left=200, Top=34, InputMask=replicate('X',60)

  add object oDESAGE_1_22 as StdField with uid="CDWOAGJSCL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 13448758,;
   bGlobalFont=.t.,;
    Height=21, Width=196, Left=199, Top=107, InputMask=replicate('X',35)

  func oDESAGE_1_22.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="AQLIWDPCSK",Visible=.t., Left=22, Top=86,;
    Alignment=1, Width=50, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="XUEUFLQIHP",Visible=.t., Left=155, Top=86,;
    Alignment=1, Width=39, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="GJDBOFNWGJ",Visible=.t., Left=14, Top=62,;
    Alignment=1, Width=58, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="LJMSLWOMBS",Visible=.t., Left=2, Top=38,;
    Alignment=1, Width=70, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="VCPYNQSPAC",Visible=.t., Left=41, Top=13,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="VOFSJAQZZJ",Visible=.t., Left=283, Top=13,;
    Alignment=1, Width=34, Height=18,;
    Caption="Ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="WWQHYHVACV",Visible=.t., Left=14, Top=110,;
    Alignment=1, Width=58, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kpg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
