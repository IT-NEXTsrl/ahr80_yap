* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bda                                                        *
*              Evanti da GSMA_MDA                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-03-24                                                      *
* Last revis.: 2015-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bda",oParentObject,m.pAzione)
return(i_retval)

define class tgsma_bda as StdBatch
  * --- Local variables
  pAzione = space(10)
  * --- WorkFile variables
  ART_ICOL_idx=0
  ART_DIST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Evanti da GSMA_MDA
    do case
      case this.pAzione="SETDEFDIBA"
        if g_PRFA="S"
          * --- Write into ART_ICOL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.ART_DIST_idx,2])
            i_cTempTable=cp_GetTempTableName(i_nConn)
            i_aIndex[1]='ARCODART'
            cp_CreateTempTable(i_nConn,i_cTempTable,"DICODART AS ARCODART, DICODDIS, DIFLCOMP "," from "+i_cQueryTable+" where DIFLPREF='S'",.f.,@i_aIndex)
            i_cQueryTable=i_cTempTable
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ARCODDIS = _t2.DICODDIS";
                +",ARFLCOMP = _t2.DIFLCOMP";
                +i_ccchkf;
                +" from "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 set ";
                +"ART_ICOL.ARCODDIS = _t2.DICODDIS";
                +",ART_ICOL.ARFLCOMP = _t2.DIFLCOMP";
                +Iif(Empty(i_ccchkf),"",",ART_ICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="PostgreSQL"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set ";
                +"ARCODDIS = _t2.DICODDIS";
                +",ARFLCOMP = _t2.DIFLCOMP";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ARCODART = "+i_cQueryTable+".ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ARCODDIS = (select DICODDIS from "+i_cQueryTable+" where "+i_cWhere+")";
                +",ARFLCOMP = (select DIFLCOMP from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='ART_DIST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
