* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bpr                                                        *
*              Duplica controlli stampante                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_96]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-06                                                      *
* Last revis.: 2001-02-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bpr",oParentObject,m.pParam)
return(i_retval)

define class tgsut_bpr as StdBatch
  * --- Local variables
  pParam = space(1)
  w_CopySetPrinter = space(30)
  w_Test = space(25)
  w_LO_TSDRIVER = space(32)
  w_ANNULLA = .f.
  * --- WorkFile variables
  TAB_STA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili inizializzate sul master Stampanti Solo Testo
    if this.pParam="S"
      this.oParentObject.mCalc()
    else
      if this.pParam="N"
        * --- Serve solo per far scattare la mcalc dall'anagrafica e eseguire il refresh dei campi (bottone attivato in Caricamento)
        i_retcode = 'stop'
        return
      endif
      if empty(this.oParentObject.w_TSDRIVER)
        ah_ErrorMsg("Selezionare prima la definizione da duplicare",,"")
        i_retcode = 'stop'
        return
      endif
      this.w_LO_TSDRIVER = this.oParentObject.w_TSDRIVER
      this.w_ANNULLA = .F.
      * --- Lancio maschera per selezione stampante di destinazione
      do gsut_kpr  with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Verifico che la stampante selezionata non sia gi� caricata sulla tabella stampanti solo testo
      if this.w_ANNULLA
        * --- Read from TAB_STA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TAB_STA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_STA_idx,2],.t.,this.TAB_STA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TSDRIVER"+;
            " from "+i_cTable+" TAB_STA where ";
                +"TSDRIVER = "+cp_ToStrODBC(this.w_CopySetPrinter);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TSDRIVER;
            from (i_cTable) where;
                TSDRIVER = this.w_CopySetPrinter;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_Test = NVL(cp_ToDate(_read_.TSDRIVER),cp_NullValue(_read_.TSDRIVER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !empty(this.w_Test)
          if !ah_YesNo("Attenzione: stampante gi� caricata. Reimposto impostazioni e set caratteri di controllo?")
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Scrivo nuova definizione stampante e set di caratteri su tabella stampanti solo testo
        * --- Try
        local bErr_03CF6278
        bErr_03CF6278=bTrsErr
        this.Try_03CF6278()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03CF6278
        * --- End
        * --- Write into TAB_STA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TAB_STA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_STA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TAB_STA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TS10CPI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TS10CPI),'TAB_STA','TS10CPI');
          +",TS12CPI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TS12CPI),'TAB_STA','TS12CPI');
          +",TS15CPI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TS15CPI),'TAB_STA','TS15CPI');
          +",TSFIBOLD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSFIBOLD),'TAB_STA','TSFIBOLD');
          +",TSFIDOUB ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSFIDOUB),'TAB_STA','TSFIDOUB');
          +",TSFIITAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSFIITAL),'TAB_STA','TSFIITAL');
          +",TSFIUNDE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSFIUNDE),'TAB_STA','TSFIUNDE');
          +",TSFORPAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSFORPAG),'TAB_STA','TSFORPAG');
          +",TSROWOK ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSROWOK),'TAB_STA','TSROWOK');
          +",TSRTCOMP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSRTCOMP),'TAB_STA','TSRTCOMP');
          +",TSSTBOLD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSSTBOLD),'TAB_STA','TSSTBOLD');
          +",TSSTCOMP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSSTCOMP),'TAB_STA','TSSTCOMP');
          +",TSSTDOUB ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSSTDOUB),'TAB_STA','TSSTDOUB');
          +",TSSTITAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSSTITAL),'TAB_STA','TSSTITAL');
          +",TSSTUNDE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSSTUNDE),'TAB_STA','TSSTUNDE');
          +",TSRESET ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSRESET),'TAB_STA','TSRESET');
          +",TSROWPAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSROWPAG),'TAB_STA','TSROWPAG');
          +",TSINIZIA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TSINIZIA),'TAB_STA','TSINIZIA');
              +i_ccchkf ;
          +" where ";
              +"TSDRIVER = "+cp_ToStrODBC(this.w_CopySetPrinter);
                 )
        else
          update (i_cTable) set;
              TS10CPI = this.oParentObject.w_TS10CPI;
              ,TS12CPI = this.oParentObject.w_TS12CPI;
              ,TS15CPI = this.oParentObject.w_TS15CPI;
              ,TSFIBOLD = this.oParentObject.w_TSFIBOLD;
              ,TSFIDOUB = this.oParentObject.w_TSFIDOUB;
              ,TSFIITAL = this.oParentObject.w_TSFIITAL;
              ,TSFIUNDE = this.oParentObject.w_TSFIUNDE;
              ,TSFORPAG = this.oParentObject.w_TSFORPAG;
              ,TSROWOK = this.oParentObject.w_TSROWOK;
              ,TSRTCOMP = this.oParentObject.w_TSRTCOMP;
              ,TSSTBOLD = this.oParentObject.w_TSSTBOLD;
              ,TSSTCOMP = this.oParentObject.w_TSSTCOMP;
              ,TSSTDOUB = this.oParentObject.w_TSSTDOUB;
              ,TSSTITAL = this.oParentObject.w_TSSTITAL;
              ,TSSTUNDE = this.oParentObject.w_TSSTUNDE;
              ,TSRESET = this.oParentObject.w_TSRESET;
              ,TSROWPAG = this.oParentObject.w_TSROWPAG;
              ,TSINIZIA = this.oParentObject.w_TSINIZIA;
              &i_ccchkf. ;
           where;
              TSDRIVER = this.w_CopySetPrinter;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc
  proc Try_03CF6278()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TAB_STA
    i_nConn=i_TableProp[this.TAB_STA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_STA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAB_STA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TSDRIVER"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CopySetPrinter),'TAB_STA','TSDRIVER');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TSDRIVER',this.w_CopySetPrinter)
      insert into (i_cTable) (TSDRIVER &i_ccchkf. );
         values (;
           this.w_CopySetPrinter;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TAB_STA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
