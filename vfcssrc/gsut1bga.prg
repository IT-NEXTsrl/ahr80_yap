* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1bga                                                        *
*              Gestione aggiornamenti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_36]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-05-28                                                      *
* Last revis.: 2009-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut1bga",oParentObject,m.pOper)
return(i_retval)

define class tgsut1bga as StdBatch
  * --- Local variables
  pOper = space(5)
  w_ZOOM = .NULL.
  w_MESS = space(10)
  w_OK = 0
  w_COCODREL = space(15)
  w_COORDINE = space(5)
  w_PROG = .NULL.
  * --- WorkFile variables
  CONVERSI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione conversioni da rieseguire (da GSUT1KGA)
    * --- Inizializza ,,
    this.w_ZOOM = this.oParentObject.w_CalcZoom
    do case
      case this.pOper="CALC"
        * --- Lancia lo Zoom
        This.OparentObject.NotifyEvent("Legge")
      case this.pOper="SELE"
        * --- Seleziona/Deseleziona Tutto
        NC = this.w_ZOOM.cCursor
        UPDATE &NC SET XCHK = IIF(this.oParentObject.w_SELEZI="S", 1, 0)
      case this.pOper="AGGIO"
        * --- Elimina i Codici Selezionati
        NC = this.w_ZOOM.cCursor
        SELECT &NC
        GO TOP
        * --- deve esistere almeno un Record Selezionato
        COUNT FOR XCHK=1 TO w_CONTA
        if w_CONTA=0
          ah_ErrorMsg("Non sono state selezionate conversioni da impostare come da rieseguire","!","")
          i_retcode = 'stop'
          return
        endif
        this.w_OK = 0
        if ah_YesNo("Confermi richiesta?")
          * --- Cicla sui record Selezionati
          SELECT &NC
          GO TOP
          SCAN FOR XCHK=1
          * --- Parametri aggiornamento tabella
          this.w_COCODREL = nvl( &NC..COCODREL , space(15) )
          this.w_COORDINE = nvl( &NC..COORDINE , space(5) )
          * --- Imposta la procedura di conversione come da rieseguire
          * --- Write into CONVERSI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONVERSI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONVERSI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"COESEGUI ="+cp_NullLink(cp_ToStrODBC("N"),'CONVERSI','COESEGUI');
                +i_ccchkf ;
            +" where ";
                +"COCODREL = "+cp_ToStrODBC(this.w_COCODREL);
                +" and COORDINE = "+cp_ToStrODBC(this.w_COORDINE);
                   )
          else
            update (i_cTable) set;
                COESEGUI = "N";
                &i_ccchkf. ;
             where;
                COCODREL = this.w_COCODREL;
                and COORDINE = this.w_COORDINE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_OK = this.w_OK+1
          * --- Controllo risultato
          ENDSCAN
          this.w_MESS = "Elaborazione terminata%0E' ora necessario mettere il sistema in manutenzione ed eseguire le procedure di conversione"
          ah_ErrorMsg(this.w_MESS,"!","",alltrim(str(this.w_OK,4,0)))
          * --- Aggiorna lista
          This.OparentObject.NotifyEvent("Legge")
        endif
      case this.pOper="ORIG"
        this.w_PROG = GSUT_MCO()
        * --- Controllo se ha passato il test di accesso 
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- inizializzo la chiave Manutenzione Tabella Conversioni
        this.w_PROG.w_COCODREL = this.oParentObject.w_CODREL
        * --- creo il curosre delle solo chiavi
        this.w_PROG.QueryKeySet("COCODREL='"+this.oParentObject.w_CODREL+ "'","")     
        * --- mi metto in interrogazione
        this.w_PROG.LoadRecWarn()     
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONVERSI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
