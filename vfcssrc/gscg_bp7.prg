* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bp7                                                        *
*              Visualizza Window con rif. reg. p.nota                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-19                                                      *
* Last revis.: 2013-07-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bp7",oParentObject)
return(i_retval)

define class tgscg_bp7 as StdBatch
  * --- Local variables
  w_MESS = space(150)
  w_PARMSG1 = space(20)
  w_PARMSG2 = space(20)
  w_ASCLADOC = space(15)
  w_ASBARIMM = space(1)
  w_ASBAUTOM = space(1)
  w_ASNOREPS = space(1)
  w_RICDESCRI = space(1)
  w_MODALLEG = space(1)
  w_CATCH = space(250)
  w_Operazione = space(1)
  w_RETVAL = 0
  w_NomeFile = space(10)
  w_NomeRep = space(10)
  w_INCLASSEDOC = space(15)
  w_INARCHIVIO = space(20)
  w_ARCHIVIO = space(20)
  w_CHIAVE = space(20)
  w_DESBRE = space(100)
  w_DESLUN = space(0)
  w_TIPOALLE = space(5)
  w_CLASSEALL = space(5)
  w_QUEMOD = space(60)
  w_CDPUBWEB = space(1)
  w_CDRIFDEF = space(1)
  w_CDRIFTAB = space(20)
  * --- WorkFile variables
  ASSCAUDO_idx=0
  PROMCLAS_idx=0
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Al termine dell'inserimento di un nuovo record, viene visualizzata una finestra contenente i dati relativi
    *     al numero Registrazione, Documento/Alfa, Protocollo/Alfa
    * --- Solo se nel Cp3start.cnf non � dichiarata la variabile pubblica i_Nowaitmess oppure se impostata a .F.
    if isAhe() and (Type("i_Nowaitmess") = "U" Or Not i_Nowaitmess)
      this.w_MESS = "N� reg.: %1 %0N� doc./alfa: %2 %0N� prot./alfa: %3"
      this.w_PARMSG1 = STR(this.oParentObject.w_PNNUMDOC)+" / "+this.oParentObject.w_PNALFDOC
      this.w_PARMSG2 = STR(this.oParentObject.w_PNNUMPRO)+" / "+this.oParentObject.w_PNALFPRO
      ah_msg(this.w_MESS,.f.,.f.,.f.,STR(this.oParentObject.w_PNNUMRER),this.w_PARMSG1,this.w_PARMSG2)
    endif
    * --- Verifico se devo generare il file xml per acquisizione tramite barcode
    * --- Read from ASSCAUDO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ASSCAUDO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASSCAUDO_idx,2],.t.,this.ASSCAUDO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ASCLADOC,ASBARIMM,ASBAUTOM,ASNOREPS"+;
        " from "+i_cTable+" ASSCAUDO where ";
            +"AS__TIPO = "+cp_ToStrODBC("C");
            +" and ASCODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ASCLADOC,ASBARIMM,ASBAUTOM,ASNOREPS;
        from (i_cTable) where;
            AS__TIPO = "C";
            and ASCODCAU = this.oParentObject.w_PNCODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ASCLADOC = NVL(cp_ToDate(_read_.ASCLADOC),cp_NullValue(_read_.ASCLADOC))
      this.w_ASBARIMM = NVL(cp_ToDate(_read_.ASBARIMM),cp_NullValue(_read_.ASBARIMM))
      this.w_ASBAUTOM = NVL(cp_ToDate(_read_.ASBAUTOM),cp_NullValue(_read_.ASBAUTOM))
      this.w_ASNOREPS = NVL(cp_ToDate(_read_.ASNOREPS),cp_NullValue(_read_.ASNOREPS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CDRIFTAB = upper(alltrim(this.w_CDRIFTAB))
    if this.w_ASBARIMM="S" AND NOT EMPTY(this.w_ASCLADOC)
      if SUBSTR( CHKPECLA( this.w_ASCLADOC, i_CODUTE), 1, 1 ) = "S"
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDGETDES,CDMODALL,CDPUBWEB,CDRIFDEF,CDRIFTAB"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDCODCLA = "+cp_ToStrODBC(this.w_ASCLADOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDGETDES,CDMODALL,CDPUBWEB,CDRIFDEF,CDRIFTAB;
            from (i_cTable) where;
                CDCODCLA = this.w_ASCLADOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RICDESCRI = NVL(cp_ToDate(_read_.CDGETDES),cp_NullValue(_read_.CDGETDES))
          this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
          this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
          this.w_CDRIFDEF = NVL(cp_ToDate(_read_.CDRIFDEF),cp_NullValue(_read_.CDRIFDEF))
          this.w_CDRIFTAB = NVL(cp_ToDate(_read_.CDRIFTAB),cp_NullValue(_read_.CDRIFTAB))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do case
          case not this.w_MODALLEG $ "IS"
            ah_ErrorMsg("Generazione barcode: modalit� classe documentale non valida")
          case this.w_CDRIFDEF="N"
            ah_ErrorMsg("Generazione barcode: valore su <archiviazione rapida> non valido")
          case this.w_CDRIFTAB<>"PNT_MAST"
            ah_ErrorMsg("Generazione barcode: tabella di riferimento non valida")
          otherwise
            if this.w_MODALLEG="I"
              * --- creo pdf vuoto per CPIN e DMIP (su IDMSsa non ci deve essere)
              this.w_CATCH = FORCEEXT(ADDBS(tempadhoc())+SUBSTR(SYS(2015),2),"PDF")
              copy file "query\gsut_bcv.pdf" to (this.w_CATCH)
            endif
            this.w_NomeFile = this.w_CATCH
            this.w_NomeRep = "GSUT_BCV.PDF"
            this.w_INCLASSEDOC = this.w_ASCLADOC
            this.w_INARCHIVIO = "X"
            this.w_ARCHIVIO = this.w_CDRIFTAB
            this.w_CHIAVE = this.oParentObject.w_PNSERIAL
            this.w_Operazione = "B"
            i_nConn=i_TableProp[this.PNT_MAST_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
            * --- Questo cursore sarebbe aggiornato dalla LoadRec. ma adesso sono in caricamento (lo usa la GSUT_BBA)
            this.w_RETVAL = cp_SQL(i_nConn,"select * from "+i_cTable+" where PNSERIAL="+cp_ToStrODBC(this.w_CHIAVE), this.oParentObject.cCursor)
            if this.w_RETVAL<>-1
              this.w_RETVAL = GSUT_BPI(this,4)
            else
              ah_ErrorMsg("Errore generazione barcode:%0%1",,, message())
            endif
        endcase
      else
        ah_ErrorMsg("Generazione barcode: non si possiedono le autorizzazioni per la classe documentale indicata")
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ASSCAUDO'
    this.cWorkTables[2]='PROMCLAS'
    this.cWorkTables[3]='PNT_MAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
