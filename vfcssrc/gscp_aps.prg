* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_aps                                                        *
*              Parametri Corporate Portal (stand-alone)                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-25                                                      *
* Last revis.: 2007-12-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_aps"))

* --- Class definition
define class tgscp_aps as StdForm
  Top    = 18
  Left   = 29

  * --- Standard Properties
  Width  = 630
  Height = 170+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-12-14"
  HelpContextID=160011625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  ZPARAMGEST_IDX = 0
  cpazi_IDX = 0
  ZFOLDERS_IDX = 0
  ZACTKEY_IDX = 0
  cFile = "ZPARAMGEST"
  cKeySelect = "PACODICE"
  cKeyWhere  = "PACODICE=this.w_PACODICE"
  cKeyWhereODBC = '"PACODICE="+cp_ToStrODBC(this.w_PACODICE)';

  cKeyWhereODBCqualified = '"ZPARAMGEST.PACODICE="+cp_ToStrODBC(this.w_PACODICE)';

  cPrg = "gscp_aps"
  cComment = "Parametri Corporate Portal (stand-alone)"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PACODICE = space(5)
  o_PACODICE = space(5)
  w_AZRAGAZI = space(40)
  w_PAAPPLICATID = space(5)
  w_TIPOC = space(1)
  w_TIPOS = space(1)
  w_PACODPRF = space(10)
  w_MONUMREL = space(10)
  w_MORELCPZ = space(10)
  w_PACOMFOL = 0
  w_DECOMP = space(255)
  w_PACMNFOL = 0
  w_DECOMU = space(255)
  w_ZACTKEY = space(10)
  * --- Area Manuale = Declare Variables
  * --- gscp_aps
  bFirst = .T.
  proc ecpQuery()
      DoDefault()
      this.oPgFrm.Pages(2).caption=''
      this.oPgFrm.Pages(2).enabled=.f.
      if (this.bFirst)
        this.bFirst = .F.
        this.w_PACODICE=i_codazi
        this.LoadRec()
        if this.bLoaded
          this.ecpQuery()
        else
          this.ecpLoad()
        endif
      endif
  endproc
  
  func ah_HasCPEvents(i_cOp)
      local l_mess
      if Upper(This.cFunction)="QUERY"
      if  upper(i_cop)='ECPLOAD'
         L_mess="Parametri gi� presenti per l'azienda ("+i_codazi+" )"
         Cp_ErrorMsg(L_mess,'!')
         return (.f.)
      endif
      if  upper(i_cop)='ECPDELETE'
         L_mess="Impossibile Cancellare i parametri! ("+i_codazi+" )"
         Cp_ErrorMsg(L_mess,'!')
         return (.f.)
      endif
      endif
     return (.t.)
  endfunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ZPARAMGEST','gscp_aps')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_apsPag1","gscp_aps",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri")
      .Pages(1).HelpContextID = 45021176
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPACODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='cpazi'
    this.cWorkTables[2]='ZFOLDERS'
    this.cWorkTables[3]='ZACTKEY'
    this.cWorkTables[4]='ZPARAMGEST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ZPARAMGEST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ZPARAMGEST_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PACODICE = NVL(PACODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gscp_aps
    this.w_PACODICE=i_codazi
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ZPARAMGEST where PACODICE=KeySet.PACODICE
    *
    i_nConn = i_TableProp[this.ZPARAMGEST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMGEST_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ZPARAMGEST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ZPARAMGEST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ZPARAMGEST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PACODICE',this.w_PACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZRAGAZI = space(40)
        .w_TIPOC = 'C'
        .w_TIPOS = 'S'
        .w_MONUMREL = space(10)
        .w_MORELCPZ = space(10)
        .w_DECOMP = space(255)
        .w_DECOMU = space(255)
        .w_ZACTKEY = '0000000001'
        .w_PACODICE = NVL(PACODICE,space(5))
          .link_1_1('Load')
        .w_PAAPPLICATID = NVL(PAAPPLICATID,space(5))
        .w_PACODPRF = NVL(PACODPRF,space(10))
        .w_PACOMFOL = NVL(PACOMFOL,0)
          .link_1_12('Load')
        .w_PACMNFOL = NVL(PACMNFOL,0)
          .link_1_17('Load')
          .link_1_21('Load')
        cp_LoadRecExtFlds(this,'ZPARAMGEST')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PACODICE = space(5)
      .w_AZRAGAZI = space(40)
      .w_PAAPPLICATID = space(5)
      .w_TIPOC = space(1)
      .w_TIPOS = space(1)
      .w_PACODPRF = space(10)
      .w_MONUMREL = space(10)
      .w_MORELCPZ = space(10)
      .w_PACOMFOL = 0
      .w_DECOMP = space(255)
      .w_PACMNFOL = 0
      .w_DECOMU = space(255)
      .w_ZACTKEY = space(10)
      if .cFunction<>"Filter"
        .w_PACODICE = i_codazi
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_PACODICE))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_TIPOC = 'C'
        .w_TIPOS = 'S'
        .DoRTCalc(6,9,.f.)
          if not(empty(.w_PACOMFOL))
          .link_1_12('Full')
          endif
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_PACMNFOL))
          .link_1_17('Full')
          endif
          .DoRTCalc(12,12,.f.)
        .w_ZACTKEY = '0000000001'
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_ZACTKEY))
          .link_1_21('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'ZPARAMGEST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPACODICE_1_1.enabled = !i_bVal
      .Page1.oPag.oPAAPPLICATID_1_3.enabled = i_bVal
      .Page1.oPag.oPACODPRF_1_8.enabled = i_bVal
      .Page1.oPag.oMONUMREL_1_9.enabled = i_bVal
      .Page1.oPag.oMORELCPZ_1_10.enabled = i_bVal
      .Page1.oPag.oPACOMFOL_1_12.enabled = i_bVal
      .Page1.oPag.oPACMNFOL_1_17.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ZPARAMGEST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ZPARAMGEST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODICE,"PACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAAPPLICATID,"PAAPPLICATID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODPRF,"PACODPRF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACOMFOL,"PACOMFOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACMNFOL,"PACMNFOL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ZPARAMGEST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMGEST_IDX,2])
    i_lTable = "ZPARAMGEST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ZPARAMGEST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gscp_aps
    this.w_PACODICE = alltrim(this.w_PACODICE)
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ZPARAMGEST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMGEST_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ZPARAMGEST_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ZPARAMGEST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ZPARAMGEST')
        i_extval=cp_InsertValODBCExtFlds(this,'ZPARAMGEST')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PACODICE,PAAPPLICATID,PACODPRF,PACOMFOL,PACMNFOL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PACODICE)+;
                  ","+cp_ToStrODBC(this.w_PAAPPLICATID)+;
                  ","+cp_ToStrODBC(this.w_PACODPRF)+;
                  ","+cp_ToStrODBCNull(this.w_PACOMFOL)+;
                  ","+cp_ToStrODBCNull(this.w_PACMNFOL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ZPARAMGEST')
        i_extval=cp_InsertValVFPExtFlds(this,'ZPARAMGEST')
        cp_CheckDeletedKey(i_cTable,0,'PACODICE',this.w_PACODICE)
        INSERT INTO (i_cTable);
              (PACODICE,PAAPPLICATID,PACODPRF,PACOMFOL,PACMNFOL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PACODICE;
                  ,this.w_PAAPPLICATID;
                  ,this.w_PACODPRF;
                  ,this.w_PACOMFOL;
                  ,this.w_PACMNFOL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ZPARAMGEST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMGEST_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ZPARAMGEST_IDX,i_nConn)
      *
      * update ZPARAMGEST
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ZPARAMGEST')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PAAPPLICATID="+cp_ToStrODBC(this.w_PAAPPLICATID)+;
             ",PACODPRF="+cp_ToStrODBC(this.w_PACODPRF)+;
             ",PACOMFOL="+cp_ToStrODBCNull(this.w_PACOMFOL)+;
             ",PACMNFOL="+cp_ToStrODBCNull(this.w_PACMNFOL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ZPARAMGEST')
        i_cWhere = cp_PKFox(i_cTable  ,'PACODICE',this.w_PACODICE  )
        UPDATE (i_cTable) SET;
              PAAPPLICATID=this.w_PAAPPLICATID;
             ,PACODPRF=this.w_PACODPRF;
             ,PACOMFOL=this.w_PACOMFOL;
             ,PACMNFOL=this.w_PACMNFOL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ZPARAMGEST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMGEST_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ZPARAMGEST_IDX,i_nConn)
      *
      * delete ZPARAMGEST
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PACODICE',this.w_PACODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ZPARAMGEST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMGEST_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,12,.t.)
        if .o_PACODICE<>.w_PACODICE
          .link_1_21('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_FPJLTDNZDX()
    with this
          * --- Gscp_bpp
          gscp_bpp(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMONUMREL_1_9.enabled = this.oPgFrm.Page1.oPag.oMONUMREL_1_9.mCond()
    this.oPgFrm.Page1.oPag.oMORELCPZ_1_10.enabled = this.oPgFrm.Page1.oPag.oMORELCPZ_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMONUMREL_1_9.visible=!this.oPgFrm.Page1.oPag.oMONUMREL_1_9.mHide()
    this.oPgFrm.Page1.oPag.oMORELCPZ_1_10.visible=!this.oPgFrm.Page1.oPag.oMORELCPZ_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Insert end") or lower(cEvent)==lower("Update end")
          .Calculate_FPJLTDNZDX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PACODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpazi_IDX,3]
    i_lTable = "cpazi"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpazi_IDX,2], .t., this.cpazi_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpazi_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select codazi,desazi";
                   +" from "+i_cTable+" "+i_lTable+" where codazi="+cp_ToStrODBC(this.w_PACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'codazi',this.w_PACODICE)
            select codazi,desazi;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODICE = NVL(_Link_.codazi,space(5))
      this.w_AZRAGAZI = NVL(_Link_.desazi,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PACODICE = space(5)
      endif
      this.w_AZRAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpazi_IDX,2])+'\'+cp_ToStr(_Link_.codazi,1)
      cp_ShowWarn(i_cKey,this.cpazi_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACOMFOL
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
    i_lTable = "ZFOLDERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2], .t., this.ZFOLDERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACOMFOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZFOLDERS')
        if i_nConn<>0
          i_cWhere = " FOSERIAL="+cp_ToStrODBC(this.w_PACOMFOL);
                   +" and FO__TIPO="+cp_ToStrODBC(this.w_TIPOC);

          i_ret=cp_SQL(i_nConn,"select FO__TIPO,FOSERIAL,FODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FO__TIPO',this.w_TIPOC;
                     ,'FOSERIAL',this.w_PACOMFOL)
          select FO__TIPO,FOSERIAL,FODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PACOMFOL) and !this.bDontReportError
            deferred_cp_zoom('ZFOLDERS','*','FO__TIPO,FOSERIAL',cp_AbsName(oSource.parent,'oPACOMFOL_1_12'),i_cWhere,'',"Elenco folder",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FO__TIPO,FOSERIAL,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FO__TIPO,FOSERIAL,FODESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FO__TIPO,FOSERIAL,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FOSERIAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FO__TIPO="+cp_ToStrODBC(this.w_TIPOC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FO__TIPO',oSource.xKey(1);
                       ,'FOSERIAL',oSource.xKey(2))
            select FO__TIPO,FOSERIAL,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACOMFOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FO__TIPO,FOSERIAL,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FOSERIAL="+cp_ToStrODBC(this.w_PACOMFOL);
                   +" and FO__TIPO="+cp_ToStrODBC(this.w_TIPOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FO__TIPO',this.w_TIPOC;
                       ,'FOSERIAL',this.w_PACOMFOL)
            select FO__TIPO,FOSERIAL,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACOMFOL = NVL(_Link_.FOSERIAL,0)
      this.w_DECOMP = NVL(_Link_.FODESCRI,space(255))
    else
      if i_cCtrl<>'Load'
        this.w_PACOMFOL = 0
      endif
      this.w_DECOMP = space(255)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])+'\'+cp_ToStr(_Link_.FO__TIPO,1)+'\'+cp_ToStr(_Link_.FOSERIAL,1)
      cp_ShowWarn(i_cKey,this.ZFOLDERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACOMFOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACMNFOL
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
    i_lTable = "ZFOLDERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2], .t., this.ZFOLDERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACMNFOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZFOLDERS')
        if i_nConn<>0
          i_cWhere = " FOSERIAL="+cp_ToStrODBC(this.w_PACMNFOL);
                   +" and FO__TIPO="+cp_ToStrODBC(this.w_TIPOS);

          i_ret=cp_SQL(i_nConn,"select FO__TIPO,FOSERIAL,FODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FO__TIPO',this.w_TIPOS;
                     ,'FOSERIAL',this.w_PACMNFOL)
          select FO__TIPO,FOSERIAL,FODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PACMNFOL) and !this.bDontReportError
            deferred_cp_zoom('ZFOLDERS','*','FO__TIPO,FOSERIAL',cp_AbsName(oSource.parent,'oPACMNFOL_1_17'),i_cWhere,'',"Elenco folder",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FO__TIPO,FOSERIAL,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FO__TIPO,FOSERIAL,FODESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FO__TIPO,FOSERIAL,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FOSERIAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FO__TIPO="+cp_ToStrODBC(this.w_TIPOS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FO__TIPO',oSource.xKey(1);
                       ,'FOSERIAL',oSource.xKey(2))
            select FO__TIPO,FOSERIAL,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACMNFOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FO__TIPO,FOSERIAL,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FOSERIAL="+cp_ToStrODBC(this.w_PACMNFOL);
                   +" and FO__TIPO="+cp_ToStrODBC(this.w_TIPOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FO__TIPO',this.w_TIPOS;
                       ,'FOSERIAL',this.w_PACMNFOL)
            select FO__TIPO,FOSERIAL,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACMNFOL = NVL(_Link_.FOSERIAL,0)
      this.w_DECOMU = NVL(_Link_.FODESCRI,space(255))
    else
      if i_cCtrl<>'Load'
        this.w_PACMNFOL = 0
      endif
      this.w_DECOMU = space(255)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])+'\'+cp_ToStr(_Link_.FO__TIPO,1)+'\'+cp_ToStr(_Link_.FOSERIAL,1)
      cp_ShowWarn(i_cKey,this.ZFOLDERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACMNFOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZACTKEY
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZACTKEY_IDX,3]
    i_lTable = "ZACTKEY"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZACTKEY_IDX,2], .t., this.ZACTKEY_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZACTKEY_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZACTKEY) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZACTKEY)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MONUMREL,MORELCPZ";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_ZACTKEY);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_ZACTKEY)
            select MOCODICE,MONUMREL,MORELCPZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZACTKEY = NVL(_Link_.MOCODICE,space(10))
      this.w_MONUMREL = NVL(_Link_.MONUMREL,space(10))
      this.w_MORELCPZ = NVL(_Link_.MORELCPZ,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ZACTKEY = space(10)
      endif
      this.w_MONUMREL = space(10)
      this.w_MORELCPZ = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZACTKEY_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.ZACTKEY_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZACTKEY Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPACODICE_1_1.value==this.w_PACODICE)
      this.oPgFrm.Page1.oPag.oPACODICE_1_1.value=this.w_PACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oAZRAGAZI_1_2.value==this.w_AZRAGAZI)
      this.oPgFrm.Page1.oPag.oAZRAGAZI_1_2.value=this.w_AZRAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oPAAPPLICATID_1_3.value==this.w_PAAPPLICATID)
      this.oPgFrm.Page1.oPag.oPAAPPLICATID_1_3.value=this.w_PAAPPLICATID
    endif
    if not(this.oPgFrm.Page1.oPag.oPACODPRF_1_8.value==this.w_PACODPRF)
      this.oPgFrm.Page1.oPag.oPACODPRF_1_8.value=this.w_PACODPRF
    endif
    if not(this.oPgFrm.Page1.oPag.oMONUMREL_1_9.value==this.w_MONUMREL)
      this.oPgFrm.Page1.oPag.oMONUMREL_1_9.value=this.w_MONUMREL
    endif
    if not(this.oPgFrm.Page1.oPag.oMORELCPZ_1_10.value==this.w_MORELCPZ)
      this.oPgFrm.Page1.oPag.oMORELCPZ_1_10.value=this.w_MORELCPZ
    endif
    if not(this.oPgFrm.Page1.oPag.oPACOMFOL_1_12.value==this.w_PACOMFOL)
      this.oPgFrm.Page1.oPag.oPACOMFOL_1_12.value=this.w_PACOMFOL
    endif
    if not(this.oPgFrm.Page1.oPag.oDECOMP_1_16.value==this.w_DECOMP)
      this.oPgFrm.Page1.oPag.oDECOMP_1_16.value=this.w_DECOMP
    endif
    if not(this.oPgFrm.Page1.oPag.oPACMNFOL_1_17.value==this.w_PACMNFOL)
      this.oPgFrm.Page1.oPag.oPACMNFOL_1_17.value=this.w_PACMNFOL
    endif
    if not(this.oPgFrm.Page1.oPag.oDECOMU_1_19.value==this.w_DECOMU)
      this.oPgFrm.Page1.oPag.oDECOMU_1_19.value=this.w_DECOMU
    endif
    cp_SetControlsValueExtFlds(this,'ZPARAMGEST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MONUMREL))  and not(Not(g_IZCP='A'))  and (g_IZCP='A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMONUMREL_1_9.SetFocus()
            i_bnoObbl = !empty(.w_MONUMREL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MORELCPZ))  and not(Not(g_IZCP='A'))  and (g_IZCP='A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMORELCPZ_1_10.SetFocus()
            i_bnoObbl = !empty(.w_MORELCPZ)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscp_aps
      this.w_PACODICE = alltrim(this.w_PACODICE)
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PACODICE = this.w_PACODICE
    return

enddefine

* --- Define pages as container
define class tgscp_apsPag1 as StdContainer
  Width  = 626
  height = 170
  stdWidth  = 626
  stdheight = 170
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPACODICE_1_1 as StdField with uid="PAJXVVKRAA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PACODICE", cQueryName = "PACODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il codice dell'azienda di lavoro",;
    ToolTipText = "Codice...",;
    HelpContextID = 67756347,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=53, Left=132, Top=12, InputMask=replicate('X',5), cLinkFile="cpazi", oKey_1_1="codazi", oKey_1_2="this.w_PACODICE"

  func oPACODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oAZRAGAZI_1_2 as StdField with uid="LXSKSZPFLM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AZRAGAZI", cQueryName = "AZRAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione applicazione",;
    HelpContextID = 64165553,;
   bGlobalFont=.t.,;
    Height=21, Width=416, Left=187, Top=12, InputMask=replicate('X',40)

  add object oPAAPPLICATID_1_3 as StdField with uid="QCXCDDHQZH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PAAPPLICATID", cQueryName = "PAAPPLICATID",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice applicazione registrata sul portale",;
    HelpContextID = 132929207,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=132, Top=39, InputMask=replicate('X',5)

  add object oPACODPRF_1_8 as StdField with uid="KHWLIBBDCX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PACODPRF", cQueryName = "PACODPRF",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice profilo (per ricerca file di import/export azioni/profilo)",;
    HelpContextID = 185196860,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=132, Top=64, cSayPict='REPL("!",10)', cGetPict='REPL("!",10)', InputMask=replicate('X',10)

  add object oMONUMREL_1_9 as StdField with uid="EGHEZFEAVX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MONUMREL", cQueryName = "MONUMREL",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Release Sistema Software di Federazione (1.1, 1.2,...)",;
    HelpContextID = 228630290,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=520, Top=39, InputMask=replicate('X',10)

  func oMONUMREL_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP='A')
    endwith
   endif
  endfunc

  func oMONUMREL_1_9.mHide()
    with this.Parent.oContained
      return (Not(g_IZCP='A'))
    endwith
  endfunc

  add object oMORELCPZ_1_10 as StdField with uid="NXYADOJCXY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MORELCPZ", cQueryName = "MORELCPZ",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Release Corporate Portal Zucchetti (1.1, 1.2,...)",;
    HelpContextID = 25108704,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=520, Top=64, InputMask=replicate('X',10)

  func oMORELCPZ_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP='A')
    endwith
   endif
  endfunc

  func oMORELCPZ_1_10.mHide()
    with this.Parent.oContained
      return (Not(g_IZCP='A'))
    endwith
  endfunc

  add object oPACOMFOL_1_12 as StdField with uid="VHIAMMKEYQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PACOMFOL", cQueryName = "PACOMFOL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Company-folder default",;
    HelpContextID = 241573566,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=132, Top=119, cSayPict='"@Z 9999999999"', cGetPict='"@Z 9999999999"', bHasZoom = .t. , cLinkFile="ZFOLDERS", oKey_1_1="FO__TIPO", oKey_1_2="this.w_TIPOC", oKey_2_1="FOSERIAL", oKey_2_2="this.w_PACOMFOL"

  func oPACOMFOL_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACOMFOL_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACOMFOL_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ZFOLDERS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FO__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPOC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FO__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPOC)
    endif
    do cp_zoom with 'ZFOLDERS','*','FO__TIPO,FOSERIAL',cp_AbsName(this.parent,'oPACOMFOL_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco folder",'',this.parent.oContained
  endproc

  add object oDECOMP_1_16 as StdField with uid="TIWEWNWJVI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DECOMP", cQueryName = "DECOMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(255), bMultilanguage =  .f.,;
    HelpContextID = 73800650,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=219, Top=119, InputMask=replicate('X',255)

  add object oPACMNFOL_1_17 as StdField with uid="XEMTTWOHWV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PACMNFOL", cQueryName = "PACMNFOL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Common-folder default",;
    HelpContextID = 240656062,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=132, Top=145, cSayPict='"@Z 9999999999"', cGetPict='"@Z 9999999999"', bHasZoom = .t. , cLinkFile="ZFOLDERS", oKey_1_1="FO__TIPO", oKey_1_2="this.w_TIPOS", oKey_2_1="FOSERIAL", oKey_2_2="this.w_PACMNFOL"

  func oPACMNFOL_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACMNFOL_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACMNFOL_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ZFOLDERS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FO__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPOS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FO__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPOS)
    endif
    do cp_zoom with 'ZFOLDERS','*','FO__TIPO,FOSERIAL',cp_AbsName(this.parent,'oPACMNFOL_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco folder",'',this.parent.oContained
  endproc

  add object oDECOMU_1_19 as StdField with uid="PZDDDQLFFE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DECOMU", cQueryName = "DECOMU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(255), bMultilanguage =  .f.,;
    HelpContextID = 10085430,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=219, Top=145, InputMask=replicate('X',255)

  add object oStr_1_4 as StdString with uid="XGBHGJCRXF",Visible=.t., Left=63, Top=12,;
    Alignment=1, Width=64, Height=18,;
    Caption="Azienda:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="RWMWPITHLT",Visible=.t., Left=13, Top=39,;
    Alignment=1, Width=114, Height=18,;
    Caption="Codice applicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="TYEJSXZWSZ",Visible=.t., Left=47, Top=64,;
    Alignment=1, Width=80, Height=18,;
    Caption="Codice profilo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RCDIFSNWHV",Visible=.t., Left=10, Top=94,;
    Alignment=0, Width=191, Height=17,;
    Caption="Folder di default"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="GFEXCFNKXB",Visible=.t., Left=46, Top=119,;
    Alignment=1, Width=81, Height=18,;
    Caption="Company:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KDQJJDESQX",Visible=.t., Left=46, Top=145,;
    Alignment=1, Width=81, Height=18,;
    Caption="Comune:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="SIQZDPVNIT",Visible=.t., Left=189, Top=43,;
    Alignment=0, Width=184, Height=17,;
    Caption="(cod. applicazione registrata)"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="EBBLOVOCUS",Visible=.t., Left=399, Top=39,;
    Alignment=1, Width=116, Height=18,;
    Caption="Release SSFA:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (Not(g_IZCP='A'))
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="TWRCEQDLVG",Visible=.t., Left=357, Top=64,;
    Alignment=1, Width=158, Height=18,;
    Caption="Release Corporate Portal:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (Not(g_IZCP='A'))
    endwith
  endfunc

  add object oBox_1_13 as StdBox with uid="CBKKXGSBIU",left=2, top=112, width=611,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_aps','ZPARAMGEST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PACODICE=ZPARAMGEST.PACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
