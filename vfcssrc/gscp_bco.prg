* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bco                                                        *
*              Controllo automatico ordine                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_154]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-22                                                      *
* Last revis.: 2013-08-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bco",oParentObject)
return(i_retval)

define class tgscp_bco as StdBatch
  * --- Local variables
  w_RECODICE = 0
  w_ORSERIAL = space(10)
  w_NUMREC = 0
  w_NUMREGOLE = 0
  w_ERRORE = .f.
  w_TABEL2 = space(2)
  w_OPERAT = space(2)
  w_CAMPO1 = space(30)
  w_CAMPO2 = space(30)
  w_VALORE = space(0)
  w_INFO = space(80)
  w_ORCODVAL = space(5)
  w_DOQUERY = .f.
  w_TEMPVAL = space(0)
  w_CHECKVAL = .f.
  w_LIVELLO = 0
  w_RGBLIVCON = 0
  w_CODCLI = space(20)
  w_ANTIPCON = space(1)
  w_ANFLFIDO = space(1)
  w_ANVALFID = 0
  w_ANMAXORD = 0
  w_VALFIDIS = 0
  w_VALMAXORD = 0
  w_FIIMPPAP = 0
  w_FIIMPESC = 0
  w_FIIMPESO = 0
  w_FIIMPORD = 0
  w_FIIMPDDT = 0
  w_FIIMPFAT = 0
  w_COSTO = 0
  w_VALFSCO = 0
  * --- WorkFile variables
  ZREGVALI_idx=0
  CONTI_idx=0
  SIT_FIDI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --
    * --- Variabili
    this.w_ERRORE = .F.
    * --- Gestione MSG BOX
    i_MsgTitle=IIF(type("i_MsgTitle")="C", i_MsgTitle, iif(g_TypeButton=1,"adhoc Revolution", "adhoc Enterprise"))
    * --- Controllo che sia stata selezionata almeno una regola
    SELECT (this.oParentObject.w_ZoomRegole.cCursor)
    COUNT FOR xChk=1 TO this.w_NUMREGOLE
    if this.w_NUMREGOLE = 0
      ah_ErrorMsg("Attenzione: selezionare almeno una regola da eseguire","!")
    else
      * --- Nasconde la finestra PARENT
      this.oParentObject.hide()
      * --- Inizio elaborazione regole
      ah_Msg("Elaborazione in corso...")
      * --- Clear note per riportare il risultato del controllo
      UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET NOTECONT= space(1)
      * --- Livello iniziale a VERDE
      UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET ORLIVCON= 8
      UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET RGBLIVCON=65280
      * --- Scansione sugli ORDINI da controllare
      SELECT (this.oParentObject.w_ZoomSel.cCursor)
      GO TOP
      SCAN
      SELECT (this.oParentObject.w_ZoomSel.cCursor)
      this.w_NUMREC = recno()
      * --- Crea cursore "espanso" con ORDINE da controllare
      *     - contiene tutti i possibili campi presenti nelle regole
      this.w_ORSERIAL = ORSERIAL
      vq_exec(".\query\GSCP_BCO.VQR",this,"CHECKORDI")
      Wrcursor("CHECKORDI")
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Chiude cursore CHECKORDI
      if USED("CHECKORDI") 
        USE IN CHECKORDI
      endif
      * --- Gestione errore sulla regola
      if this.w_ERRORE
        EXIT
      endif
      SELECT (this.oParentObject.w_ZoomSel.cCursor)
      GO this.w_NUMREC
      ENDSCAN 
      * --- --
      wait clear
      if !this.w_ERRORE
        ah_ErrorMsg("� terminato il controllo automatico","i","")
      endif
      * --- Chiude finestra Parent
      this.oParentObject.ecpQuit()
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --
    * --- Variabili
    * --- --
    * --- Scansione regole da applicare
    SELECT (this.oParentObject.w_ZoomRegole.cCursor)
    GO TOP
    SCAN FOR xChk=1
    this.w_RECODICE = RECODICE
    this.w_ORCODVAL = CHECKORDI.ORCODVAL
    * --- Select from ZREGVALI
    i_nConn=i_TableProp[this.ZREGVALI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZREGVALI_idx,2],.t.,this.ZREGVALI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select REVALIMP,RETABEL1,RECAMPO1,REOPERAT,RETABEL2,RECAMPO2,REVALORE,RELIVCONF,RE__STOP,REDESSIN,RECODICE  from "+i_cTable+" ZREGVALI ";
          +" where RECODICE = "+cp_ToStrODBC(this.w_RECODICE)+"";
          +" order by REORDINE";
           ,"_Curs_ZREGVALI")
    else
      select REVALIMP,RETABEL1,RECAMPO1,REOPERAT,RETABEL2,RECAMPO2,REVALORE,RELIVCONF,RE__STOP,REDESSIN,RECODICE from (i_cTable);
       where RECODICE = this.w_RECODICE;
       order by REORDINE;
        into cursor _Curs_ZREGVALI
    endif
    if used('_Curs_ZREGVALI')
      select _Curs_ZREGVALI
      locate for 1=1
      do while not(eof())
      this.w_TABEL2 = _Curs_ZREGVALI.RETABEL2
      this.w_OPERAT = _Curs_ZREGVALI.REOPERAT
      this.w_CAMPO1 = ALLTRIM(_Curs_ZREGVALI.RECAMPO1)
      this.w_CAMPO2 = ALLTRIM(_Curs_ZREGVALI.RECAMPO2)
      this.w_VALORE = _Curs_ZREGVALI.REVALORE
      this.w_INFO = "Reg."+ALLTRIM(STR(_Curs_ZREGVALI.RECODICE))+"-"+ALLTRIM(_Curs_ZREGVALI.REDESSIN)+";"+CHR(13)+space(1)
      this.w_DOQUERY = .T.
      * --- Gestione controllo fido e massimo ordine cliente
      if (_Curs_ZREGVALI.RETABEL1 = "CL" and this.w_CAMPO1 = "VALFIDIS") or (_Curs_ZREGVALI.RETABEL2 = "CL" and this.w_CAMPO2 $ "VALFIDIS|ANMAXORD")
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_ANFLFIDO <> "S"
          * --- Non applica la regola sul fido/max ordine per i clienti dove non � stato abilitato il controllo fido.
          this.w_DOQUERY = .F.
        endif
        * --- Assegna il valore alle variabile di campo
        if this.w_CAMPO1 = "VALFIDIS"
          this.w_CAMPO1 = STRTRAN(STR(this.w_VALFIDIS,20,5),",",".") 
        endif
        if this.w_CAMPO2 = "VALFIDIS"
          this.w_CAMPO2 = STRTRAN(STR(this.w_VALFIDIS,20,5),",",".") 
        endif
        if this.w_CAMPO2 = "ANMAXORD"
          this.w_CAMPO2 = STRTRAN(STR(this.w_VALFIDIS,20,5),",",".") 
        endif
      endif
      if this.w_DOQUERY
        * --- Costruisco espressione per query
        do case
          case this.w_OPERAT = "E" or this.w_OPERAT = "NE"
            * --- Gestione controllo campo vuoto / non vuoto
            QUERY = "SELECT ORSERIAL from CHECKORDI where " + iif(this.w_OPERAT="NE","NOT","")+" EMPTY(NVL("+ this.w_CAMPO1 + ",' '))  into cursor __TMP__ nofilter"
          case _Curs_ZREGVALI.RETABEL2 = "EX"
            * --- Gestione campo destro equivalente ad una espressione
            this.w_TEMPVAL = ALLTRIM(STR(VAL(this.w_VALORE)))
            this.w_CHECKVAL = iif(LEN(this.w_TEMPVAL)==LEN(alltrim(this.w_VALORE)),true,false)
            * --- Conversione importo se necessario
            if ALLTRIM(this.w_ORCODVAL) <> ALLTRIM(_Curs_ZREGVALI.REVALIMP) and this.w_CAMPO1 $ "ORSPEINC|ORSPETRA|ORSPEIMB|ORTOTORD|ORSCONTI|ORPREZZO" AND this.w_CHECKVAL
              this.w_VALORE = STR(VALCAM(VAL(_Curs_ZREGVALI.REVALORE),_Curs_ZREGVALI.REVALIMP,this.w_ORCODVAL,i_datsys,0),20)
              this.w_VALORE = STRTRAN(this.w_VALORE,",",".")
            endif
            if alltrim(this.w_OPERAT) = "="
              QUERY = "SELECT ORSERIAL from CHECKORDI where " + this.w_CAMPO1 + " " + "=="+ " " + this.w_VALORE + " into cursor __TMP__ nofilter"
            else
              QUERY = "SELECT ORSERIAL from CHECKORDI where " + this.w_CAMPO1 + " " + this.w_OPERAT+ " " + this.w_VALORE + " into cursor __TMP__ nofilter"
            endif
          case _Curs_ZREGVALI.RETABEL2 = "OD" AND (Substr(this.w_CAMPO2,1,7)="ORSCOIN" or Substr(this.w_CAMPO2,1,7)="ORPREZZ")
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            QUERY = "SELECT ORSERIAL from CHECKORDI where " + this.w_CAMPO1 + " " + this.w_OPERAT+ " " + this.w_CAMPO2 + " into cursor __TMP__ nofilter"
          otherwise
            QUERY = "SELECT ORSERIAL from CHECKORDI where " + this.w_CAMPO1 + " " + this.w_OPERAT+ " " + this.w_CAMPO2 + " into cursor __TMP__ nofilter"
        endcase
        * --- --
        * --- Leggo lo stato attuale di 'Ansi' e 'Exact'
        EXACTSTA = set("Exact")
        ANSISTA = set("Ansi")
        * --- Per garantire la correttezza del confronto su operatori relazionali e stringhe attivo
        *     le regole di confronto su stringhe di lunghezza diversa
        Set Exact On
        Set Ansi On
        if !EMPTY(QUERY)
          w_ErrHnd=ON ("ERROR")
          w_ErrQry=.F.
          ON ERROR w_ErrQry=.T.
          &QUERY
          ON ERROR &w_ErrHnd
          if w_ErrQry
            * --- Chiude cursore __TMP__
            if USED("__TMP__") 
              USE IN __TMP__
            endif
            ah_ErrorMsg("Attenzione: errore eseguendo la regola:%0%0%1%0[%2]","!","",this.w_INFO,message())
            this.w_ERRORE = .T.
            return
          endif
        endif
        * --- Ripristino lo stato iniziale per 'Ansi' e 'Exact'.
        Set Exact &EXACTSTA
        Set Ansi &ANSISTA
      endif
      if USED("__TMP__") and RECCOUNT("__TMP__") > 0
        * --- Aggiorna livello di conformit� dell'ordine su ZoomSel
        this.w_LIVELLO = _Curs_ZREGVALI.RELIVCONF
        this.w_RGBLIVCON = iif(this.w_LIVELLO=8,65280,iif(this.w_LIVELLO=6,65535,iif(this.w_LIVELLO=4,255,iif(this.w_LIVELLO=2,0,16777215)))) 
        UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET RGBLIVCON=this.w_RGBLIVCON WHERE ORSERIAL=__TMP__.ORSERIAL and ORLIVCON > this.w_LIVELLO
        UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET ORLIVCON=this.w_LIVELLO WHERE ORSERIAL=__TMP__.ORSERIAL and ORLIVCON > this.w_LIVELLO
        * --- Aggiorna campo memo con RESOCONTO CONTROLLO
        UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET NOTECONT=NOTECONT+ this.w_INFO WHERE ORSERIAL=__TMP__.ORSERIAL
        if _Curs_ZREGVALI.RE__STOP = "S"
          EXIT
        endif
      endif
        select _Curs_ZREGVALI
        continue
      enddo
      use
    endif
    ENDSCAN 
    * --- Chiude cursore __TMP__
    if USED("__TMP__") 
      USE IN __TMP__
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --
    * --- Variabili
    * --- --
    this.w_ANTIPCON = "C"
    this.w_CODCLI = CHECKORDI.ORCODCON
    this.w_VALFIDIS = 0
    this.w_VALMAXORD = 0
    * --- --
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANFLFIDO,ANVALFID,ANMAXORD"+;
        " from "+i_cTable+" CONTI where ";
            +"ANCODICE = "+cp_ToStrODBC(this.w_CODCLI);
            +" and ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANFLFIDO,ANVALFID,ANMAXORD;
        from (i_cTable) where;
            ANCODICE = this.w_CODCLI;
            and ANTIPCON = this.w_ANTIPCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANFLFIDO = NVL(cp_ToDate(_read_.ANFLFIDO),cp_NullValue(_read_.ANFLFIDO))
      this.w_ANVALFID = NVL(cp_ToDate(_read_.ANVALFID),cp_NullValue(_read_.ANVALFID))
      this.w_ANMAXORD = NVL(cp_ToDate(_read_.ANMAXORD),cp_NullValue(_read_.ANMAXORD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_ANFLFIDO = "S"
      this.w_VALFIDIS = this.w_ANVALFID
      this.w_FIIMPPAP = 0
      this.w_FIIMPESC = 0
      this.w_FIIMPESO = 0
      this.w_FIIMPORD = 0
      this.w_FIIMPDDT = 0
      this.w_FIIMPFAT = 0
      * --- Read from SIT_FIDI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2],.t.,this.SIT_FIDI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT"+;
          " from "+i_cTable+" SIT_FIDI where ";
              +"FICODCLI = "+cp_ToStrODBC(this.w_CODCLI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT;
          from (i_cTable) where;
              FICODCLI = this.w_CODCLI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FIIMPPAP = NVL(cp_ToDate(_read_.FIIMPPAP),cp_NullValue(_read_.FIIMPPAP))
        this.w_FIIMPESC = NVL(cp_ToDate(_read_.FIIMPESC),cp_NullValue(_read_.FIIMPESC))
        this.w_FIIMPESO = NVL(cp_ToDate(_read_.FIIMPESO),cp_NullValue(_read_.FIIMPESO))
        this.w_FIIMPORD = NVL(cp_ToDate(_read_.FIIMPORD),cp_NullValue(_read_.FIIMPORD))
        this.w_FIIMPDDT = NVL(cp_ToDate(_read_.FIIMPDDT),cp_NullValue(_read_.FIIMPDDT))
        this.w_FIIMPFAT = NVL(cp_ToDate(_read_.FIIMPFAT),cp_NullValue(_read_.FIIMPFAT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_VALFIDIS = this.w_VALFIDIS-(this.w_FIIMPDDT+this.w_FIIMPESC+this.w_FIIMPESO+this.w_FIIMPFAT+this.w_FIIMPORD+this.w_FIIMPPAP)
      this.w_VALMAXORD = this.w_ANMAXORD
      if this.w_ORCODVAL <> _Curs_ZREGVALI.REVALIMP
        * --- Effettuo la conversione nel caso in cui la valuta di conto sia differente dalla valuta impostata sulle regole
        this.w_VALFIDIS = VALCAM(this.w_VALFIDIS,_Curs_ZREGVALI.REVALIMP,this.w_ORCODVAL,i_datsys,0)
        this.w_VALMAXORD = VALCAM(this.w_VALMAXORD,_Curs_ZREGVALI.REVALIMP,this.w_ORCODVAL,i_datsys,0)
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Select CHECKORDI 
 Go top 
 Scan
    DECLARE L_ArrRet (16,1)
    this.w_COSTO = gsar_bpz(this,CHECKORDI.ORCODART,CHECKORDI.ORCODCON,CHECKORDI.ORTIPCON,CHECKORDI.ORCODLIS,CHECKORDI.ORQTAMOV,CHECKORDI.ORCODVAL,CHECKORDI.ORDATDOC,CHECKORDI.ORUNIMIS, " ")
    do case
      case this.w_CAMPO1 = "ORSCONT1"
        this.w_VALFSCO = L_ArrRet[1,1]
        Replace ORSCOIN1 with this.w_VALFSCO
      case this.w_CAMPO1 = "ORSCONT2"
        this.w_VALFSCO = L_ArrRet[2,1]
        Replace ORSCOIN2 with this.w_VALFSCO
      case this.w_CAMPO1 = "ORSCONT3"
        this.w_VALFSCO = L_ArrRet[3,1]
        Replace ORSCOIN3 with this.w_VALFSCO
      case this.w_CAMPO1 = "ORSCONT4"
        this.w_VALFSCO = L_ArrRet[4,1]
        Replace ORSCOIN4 with this.w_VALFSCO
      case this.w_CAMPO1 = "ORPREZZO"
        this.w_VALFSCO = L_ArrRet[5,1]
        Replace ORPREZZ1 with this.w_VALFSCO
    endcase
    Endscan
     
 Select CHECKORDI 
 Go top
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ZREGVALI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='SIT_FIDI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_ZREGVALI')
      use in _Curs_ZREGVALI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
