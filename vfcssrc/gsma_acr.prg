* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_acr                                                        *
*              Codici rilevazione                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_30]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-04-13                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_acr"))

* --- Class definition
define class tgsma_acr as StdForm
  Top    = 5
  Left   = 12

  * --- Standard Properties
  Width  = 411
  Height = 242+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=109738857
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  CODIRILE_IDX = 0
  ESERCIZI_IDX = 0
  CAM_AGAZ_IDX = 0
  cFile = "CODIRILE"
  cKeySelect = "RICODICE"
  cKeyWhere  = "RICODICE=this.w_RICODICE"
  cKeyWhereODBC = '"RICODICE="+cp_ToStrODBC(this.w_RICODICE)';

  cKeyWhereODBCqualified = '"CODIRILE.RICODICE="+cp_ToStrODBC(this.w_RICODICE)';

  cPrg = "gsma_acr"
  cComment = "Codici rilevazione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_RICODICE = space(10)
  w_RIDATRIL = ctod('  /  /  ')
  o_RIDATRIL = ctod('  /  /  ')
  w_RICODESE = space(5)
  w_RIDESCRI = space(40)
  w_RI__NOTE = space(0)
  w_RICAUCAR = space(5)
  w_NOINT = space(1)
  w_FLCASC = space(1)
  w_FLAVAL = space(1)
  w_FLCOMM = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESCAR = space(35)
  w_DESSCA = space(35)
  w_NOINTS = space(1)
  w_FLCASCS = space(1)
  w_FLAVALS = space(1)
  w_FLCOMMS = space(1)
  w_AGGVAL = space(1)
  w_AGGVALS = space(1)
  w_RICAUSCA = space(5)
  w_CAUCOL = space(5)
  w_CAUCOLS = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CODIRILE','gsma_acr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_acrPag1","gsma_acr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Rilevazione")
      .Pages(1).HelpContextID = 230131601
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRICODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CODIRILE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CODIRILE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CODIRILE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RICODICE = NVL(RICODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_31_joined
    link_1_31_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CODIRILE where RICODICE=KeySet.RICODICE
    *
    i_nConn = i_TableProp[this.CODIRILE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CODIRILE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CODIRILE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CODIRILE '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_31_joined=this.AddJoinedLink_1_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RICODICE',this.w_RICODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_NOINT = space(1)
        .w_FLCASC = space(1)
        .w_FLAVAL = space(1)
        .w_FLCOMM = space(1)
        .w_OBTEST = ctod("  /  /  ")
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESCAR = space(35)
        .w_DESSCA = space(35)
        .w_NOINTS = space(1)
        .w_FLCASCS = space(1)
        .w_FLAVALS = space(1)
        .w_FLCOMMS = space(1)
        .w_AGGVAL = space(1)
        .w_AGGVALS = space(1)
        .w_CAUCOL = space(5)
        .w_CAUCOLS = space(5)
        .w_RICODICE = NVL(RICODICE,space(10))
        .w_RIDATRIL = NVL(cp_ToDate(RIDATRIL),ctod("  /  /  "))
        .w_RICODESE = NVL(RICODESE,space(5))
          * evitabile
          *.link_1_5('Load')
        .w_RIDESCRI = NVL(RIDESCRI,space(40))
        .w_RI__NOTE = NVL(RI__NOTE,space(0))
        .w_RICAUCAR = NVL(RICAUCAR,space(5))
          if link_1_9_joined
            this.w_RICAUCAR = NVL(CMCODICE109,NVL(this.w_RICAUCAR,space(5)))
            this.w_DESCAR = NVL(CMDESCRI109,space(35))
            this.w_NOINT = NVL(CMFLCLFR109,space(1))
            this.w_FLCASC = NVL(CMFLCASC109,space(1))
            this.w_FLAVAL = NVL(CMFLAVAL109,space(1))
            this.w_FLCOMM = NVL(CMFLCOMM109,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CMDTOBSO109),ctod("  /  /  "))
            this.w_AGGVAL = NVL(CMAGGVAL109,space(1))
            this.w_CAUCOL = NVL(CMCAUCOL109,space(5))
          else
          .link_1_9('Load')
          endif
        .w_RICAUSCA = NVL(RICAUSCA,space(5))
          if link_1_31_joined
            this.w_RICAUSCA = NVL(CMCODICE131,NVL(this.w_RICAUSCA,space(5)))
            this.w_DESSCA = NVL(CMDESCRI131,space(35))
            this.w_NOINTS = NVL(CMFLCLFR131,space(1))
            this.w_FLCASCS = NVL(CMFLCASC131,space(1))
            this.w_FLAVALS = NVL(CMFLAVAL131,space(1))
            this.w_FLCOMMS = NVL(CMFLCOMM131,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CMDTOBSO131),ctod("  /  /  "))
            this.w_AGGVALS = NVL(CMAGGVAL131,space(1))
            this.w_CAUCOLS = NVL(CMCAUCOL131,space(5))
          else
          .link_1_31('Load')
          endif
        cp_LoadRecExtFlds(this,'CODIRILE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_RICODICE = space(10)
      .w_RIDATRIL = ctod("  /  /  ")
      .w_RICODESE = space(5)
      .w_RIDESCRI = space(40)
      .w_RI__NOTE = space(0)
      .w_RICAUCAR = space(5)
      .w_NOINT = space(1)
      .w_FLCASC = space(1)
      .w_FLAVAL = space(1)
      .w_FLCOMM = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DESCAR = space(35)
      .w_DESSCA = space(35)
      .w_NOINTS = space(1)
      .w_FLCASCS = space(1)
      .w_FLAVALS = space(1)
      .w_FLCOMMS = space(1)
      .w_AGGVAL = space(1)
      .w_AGGVALS = space(1)
      .w_RICAUSCA = space(5)
      .w_CAUCOL = space(5)
      .w_CAUCOLS = space(5)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
          .DoRTCalc(2,2,.f.)
        .w_RIDATRIL = i_DATSYS
        .w_RICODESE = CALCESER( .w_RIDATRIL )
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_RICODESE))
          .link_1_5('Full')
          endif
        .DoRTCalc(5,7,.f.)
          if not(empty(.w_RICAUCAR))
          .link_1_9('Full')
          endif
        .DoRTCalc(8,22,.f.)
          if not(empty(.w_RICAUSCA))
          .link_1_31('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CODIRILE')
    this.DoRTCalc(23,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRICODICE_1_2.enabled = i_bVal
      .Page1.oPag.oRIDATRIL_1_4.enabled = i_bVal
      .Page1.oPag.oRIDESCRI_1_6.enabled = i_bVal
      .Page1.oPag.oRI__NOTE_1_8.enabled = i_bVal
      .Page1.oPag.oRICAUCAR_1_9.enabled = i_bVal
      .Page1.oPag.oRICAUSCA_1_31.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRICODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRICODICE_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CODIRILE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CODIRILE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICODICE,"RICODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIDATRIL,"RIDATRIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICODESE,"RICODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIDESCRI,"RIDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RI__NOTE,"RI__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICAUCAR,"RICAUCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICAUSCA,"RICAUSCA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CODIRILE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])
    i_lTable = "CODIRILE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CODIRILE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CODIRILE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CODIRILE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CODIRILE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CODIRILE')
        i_extval=cp_InsertValODBCExtFlds(this,'CODIRILE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RICODICE,RIDATRIL,RICODESE,RIDESCRI,RI__NOTE"+;
                  ",RICAUCAR,RICAUSCA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RICODICE)+;
                  ","+cp_ToStrODBC(this.w_RIDATRIL)+;
                  ","+cp_ToStrODBCNull(this.w_RICODESE)+;
                  ","+cp_ToStrODBC(this.w_RIDESCRI)+;
                  ","+cp_ToStrODBC(this.w_RI__NOTE)+;
                  ","+cp_ToStrODBCNull(this.w_RICAUCAR)+;
                  ","+cp_ToStrODBCNull(this.w_RICAUSCA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CODIRILE')
        i_extval=cp_InsertValVFPExtFlds(this,'CODIRILE')
        cp_CheckDeletedKey(i_cTable,0,'RICODICE',this.w_RICODICE)
        INSERT INTO (i_cTable);
              (RICODICE,RIDATRIL,RICODESE,RIDESCRI,RI__NOTE,RICAUCAR,RICAUSCA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RICODICE;
                  ,this.w_RIDATRIL;
                  ,this.w_RICODESE;
                  ,this.w_RIDESCRI;
                  ,this.w_RI__NOTE;
                  ,this.w_RICAUCAR;
                  ,this.w_RICAUSCA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CODIRILE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CODIRILE_IDX,i_nConn)
      *
      * update CODIRILE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CODIRILE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RIDATRIL="+cp_ToStrODBC(this.w_RIDATRIL)+;
             ",RICODESE="+cp_ToStrODBCNull(this.w_RICODESE)+;
             ",RIDESCRI="+cp_ToStrODBC(this.w_RIDESCRI)+;
             ",RI__NOTE="+cp_ToStrODBC(this.w_RI__NOTE)+;
             ",RICAUCAR="+cp_ToStrODBCNull(this.w_RICAUCAR)+;
             ",RICAUSCA="+cp_ToStrODBCNull(this.w_RICAUSCA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CODIRILE')
        i_cWhere = cp_PKFox(i_cTable  ,'RICODICE',this.w_RICODICE  )
        UPDATE (i_cTable) SET;
              RIDATRIL=this.w_RIDATRIL;
             ,RICODESE=this.w_RICODESE;
             ,RIDESCRI=this.w_RIDESCRI;
             ,RI__NOTE=this.w_RI__NOTE;
             ,RICAUCAR=this.w_RICAUCAR;
             ,RICAUSCA=this.w_RICAUSCA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CODIRILE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CODIRILE_IDX,i_nConn)
      *
      * delete CODIRILE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RICODICE',this.w_RICODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CODIRILE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_RIDATRIL<>.w_RIDATRIL
            .w_RICODESE = CALCESER( .w_RIDATRIL )
          .link_1_5('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RICODESE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_RICODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_RICODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICODESE = NVL(_Link_.ESCODESE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RICODESE = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RICAUCAR
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICAUCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_RICAUCAR)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_RICAUCAR))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICAUCAR)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICAUCAR) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oRICAUCAR_1_9'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSMA_ZGM.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICAUCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_RICAUCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_RICAUCAR)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICAUCAR = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAR = NVL(_Link_.CMDESCRI,space(35))
      this.w_NOINT = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FLCASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_FLAVAL = NVL(_Link_.CMFLAVAL,space(1))
      this.w_FLCOMM = NVL(_Link_.CMFLCOMM,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
      this.w_AGGVAL = NVL(_Link_.CMAGGVAL,space(1))
      this.w_CAUCOL = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RICAUCAR = space(5)
      endif
      this.w_DESCAR = space(35)
      this.w_NOINT = space(1)
      this.w_FLCASC = space(1)
      this.w_FLAVAL = space(1)
      this.w_FLCOMM = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_AGGVAL = space(1)
      this.w_CAUCOL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CAUCOL) and .w_AGGVAL <> 'S' and .w_NOINT='N' and .w_FLCASC='+' and .w_FLAVAL='C' and (EMPTY(.w_FLCOMMS) OR .w_FLCOMM='N')  AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_RICAUCAR = space(5)
        this.w_DESCAR = space(35)
        this.w_NOINT = space(1)
        this.w_FLCASC = space(1)
        this.w_FLAVAL = space(1)
        this.w_FLCOMM = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_AGGVAL = space(1)
        this.w_CAUCOL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICAUCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.CMCODICE as CMCODICE109"+ ",link_1_9.CMDESCRI as CMDESCRI109"+ ",link_1_9.CMFLCLFR as CMFLCLFR109"+ ",link_1_9.CMFLCASC as CMFLCASC109"+ ",link_1_9.CMFLAVAL as CMFLAVAL109"+ ",link_1_9.CMFLCOMM as CMFLCOMM109"+ ",link_1_9.CMDTOBSO as CMDTOBSO109"+ ",link_1_9.CMAGGVAL as CMAGGVAL109"+ ",link_1_9.CMCAUCOL as CMCAUCOL109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on CODIRILE.RICAUCAR=link_1_9.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and CODIRILE.RICAUCAR=link_1_9.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RICAUSCA
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICAUSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_RICAUSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_RICAUSCA))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICAUSCA)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICAUSCA) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oRICAUSCA_1_31'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSMA1ZGM.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICAUSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_RICAUSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_RICAUSCA)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMFLAVAL,CMFLCOMM,CMDTOBSO,CMAGGVAL,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICAUSCA = NVL(_Link_.CMCODICE,space(5))
      this.w_DESSCA = NVL(_Link_.CMDESCRI,space(35))
      this.w_NOINTS = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FLCASCS = NVL(_Link_.CMFLCASC,space(1))
      this.w_FLAVALS = NVL(_Link_.CMFLAVAL,space(1))
      this.w_FLCOMMS = NVL(_Link_.CMFLCOMM,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
      this.w_AGGVALS = NVL(_Link_.CMAGGVAL,space(1))
      this.w_CAUCOLS = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RICAUSCA = space(5)
      endif
      this.w_DESSCA = space(35)
      this.w_NOINTS = space(1)
      this.w_FLCASCS = space(1)
      this.w_FLAVALS = space(1)
      this.w_FLCOMMS = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_AGGVALS = space(1)
      this.w_CAUCOLS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CAUCOLS) and .w_AGGVALS<> 'S' and .w_NOINTS='N' and .w_FLCASCS='-' and .w_FLAVALS='S' and (EMPTY(.w_FLCOMMS) OR .w_FLCOMMS='N') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_RICAUSCA = space(5)
        this.w_DESSCA = space(35)
        this.w_NOINTS = space(1)
        this.w_FLCASCS = space(1)
        this.w_FLAVALS = space(1)
        this.w_FLCOMMS = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_AGGVALS = space(1)
        this.w_CAUCOLS = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICAUSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_31.CMCODICE as CMCODICE131"+ ",link_1_31.CMDESCRI as CMDESCRI131"+ ",link_1_31.CMFLCLFR as CMFLCLFR131"+ ",link_1_31.CMFLCASC as CMFLCASC131"+ ",link_1_31.CMFLAVAL as CMFLAVAL131"+ ",link_1_31.CMFLCOMM as CMFLCOMM131"+ ",link_1_31.CMDTOBSO as CMDTOBSO131"+ ",link_1_31.CMAGGVAL as CMAGGVAL131"+ ",link_1_31.CMCAUCOL as CMCAUCOL131"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_31 on CODIRILE.RICAUSCA=link_1_31.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_31"
          i_cKey=i_cKey+'+" and CODIRILE.RICAUSCA=link_1_31.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRICODICE_1_2.value==this.w_RICODICE)
      this.oPgFrm.Page1.oPag.oRICODICE_1_2.value=this.w_RICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oRIDATRIL_1_4.value==this.w_RIDATRIL)
      this.oPgFrm.Page1.oPag.oRIDATRIL_1_4.value=this.w_RIDATRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oRICODESE_1_5.value==this.w_RICODESE)
      this.oPgFrm.Page1.oPag.oRICODESE_1_5.value=this.w_RICODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oRIDESCRI_1_6.value==this.w_RIDESCRI)
      this.oPgFrm.Page1.oPag.oRIDESCRI_1_6.value=this.w_RIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRI__NOTE_1_8.value==this.w_RI__NOTE)
      this.oPgFrm.Page1.oPag.oRI__NOTE_1_8.value=this.w_RI__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oRICAUCAR_1_9.value==this.w_RICAUCAR)
      this.oPgFrm.Page1.oPag.oRICAUCAR_1_9.value=this.w_RICAUCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAR_1_19.value==this.w_DESCAR)
      this.oPgFrm.Page1.oPag.oDESCAR_1_19.value=this.w_DESCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSCA_1_20.value==this.w_DESSCA)
      this.oPgFrm.Page1.oPag.oDESSCA_1_20.value=this.w_DESSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oRICAUSCA_1_31.value==this.w_RICAUSCA)
      this.oPgFrm.Page1.oPag.oRICAUSCA_1_31.value=this.w_RICAUSCA
    endif
    cp_SetControlsValueExtFlds(this,'CODIRILE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(Not Empty(.w_RICODESE))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare una data interna ad un esercizio caricato")
          case   (empty(.w_RICODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_RICODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RIDATRIL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIDATRIL_1_4.SetFocus()
            i_bnoObbl = !empty(.w_RIDATRIL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RICODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICODESE_1_5.SetFocus()
            i_bnoObbl = !empty(.w_RICODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_RICAUCAR)) or not(EMPTY(.w_CAUCOL) and .w_AGGVAL <> 'S' and .w_NOINT='N' and .w_FLCASC='+' and .w_FLAVAL='C' and (EMPTY(.w_FLCOMMS) OR .w_FLCOMM='N')  AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICAUCAR_1_9.SetFocus()
            i_bnoObbl = !empty(.w_RICAUCAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_RICAUSCA)) or not(EMPTY(.w_CAUCOLS) and .w_AGGVALS<> 'S' and .w_NOINTS='N' and .w_FLCASCS='-' and .w_FLAVALS='S' and (EMPTY(.w_FLCOMMS) OR .w_FLCOMMS='N') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICAUSCA_1_31.SetFocus()
            i_bnoObbl = !empty(.w_RICAUSCA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsma_acr
      * Check ad hoc ENTERPRISE casuale di magazzino che movimenta l'analitica
      * campo CMFLELAN non presente nelle causali di magazzino
      Local L_NOANAL,L_NOANALS
      If i_bRes And g_APPLICATION="ad hoc ENTERPRISE"
        L_NOANAL = LOOKTAB('CAM_AGAZ','CMFLELAN','CMCODICE', This.w_RICAUCAR )
        If Not Empty( L_NOANAL )
         i_bRes = .f.
         i_cErrorMsg = Ah_MsgFormat("Causale di magazzino di carico non valida (movimenta l'analitica)")
        Endif
      
       If i_bRes
        L_NOANALS = LOOKTAB('CAM_AGAZ','CMFLELAN','CMCODICE', This.w_RICAUSCA )
        If Not Empty( L_NOANALS )
         i_bRes = .f.
         i_cErrorMsg = Ah_MsgFormat("Causale di magazzino di scarico non valida (movimenta l'analitica)")
        Endif
       Endif
      
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RIDATRIL = this.w_RIDATRIL
    return

enddefine

* --- Define pages as container
define class tgsma_acrPag1 as StdContainer
  Width  = 407
  height = 242
  stdWidth  = 407
  stdheight = 242
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRICODICE_1_2 as StdField with uid="HWTETGYDOP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RICODICE", cQueryName = "RICODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice rilevazione",;
    HelpContextID = 118031195,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=80, Left=75, Top=12, InputMask=replicate('X',10)

  add object oRIDATRIL_1_4 as StdField with uid="ZDYXMSHKHX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RIDATRIL", cQueryName = "RIDATRIL",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data rilevazione",;
    HelpContextID = 16454498,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=196, Top=12

  add object oRICODESE_1_5 as StdField with uid="CWIMTMBBAT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RICODESE", cQueryName = "RICODESE",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio in cui si esegue la rilevazione",;
    HelpContextID = 50922331,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=346, Top=12, InputMask=replicate('X',5), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_RICODESE"

  func oRICODESE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oRIDESCRI_1_6 as StdField with uid="MSZYXUSUPY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RIDESCRI", cQueryName = "RIDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della rilevazione",;
    HelpContextID = 235990177,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=75, Top=40, InputMask=replicate('X',40)

  add object oRI__NOTE_1_8 as StdMemo with uid="YNVVIKOJDA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_RI__NOTE", cQueryName = "RI__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note relative alla rilevazione",;
    HelpContextID = 230343515,;
   bGlobalFont=.t.,;
    Height=71, Width=319, Left=75, Top=71

  add object oRICAUCAR_1_9 as StdField with uid="EJYIGJRQLD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_RICAUCAR", cQueryName = "RICAUCAR",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di magazzino utilizzata per generare i movimenti di carico",;
    HelpContextID = 234159256,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=75, Top=180, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_RICAUCAR"

  func oRICAUCAR_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICAUCAR_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICAUCAR_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oRICAUCAR_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSMA_ZGM.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oRICAUCAR_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_RICAUCAR
     i_obj.ecpSave()
  endproc

  add object oDESCAR_1_19 as StdField with uid="XVXYNHIKST",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCAR", cQueryName = "DESCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 3277258,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=145, Top=180, InputMask=replicate('X',35)

  add object oDESSCA_1_20 as StdField with uid="LBAVACMJQD",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESSCA", cQueryName = "DESSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 16908746,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=145, Top=207, InputMask=replicate('X',35)

  add object oRICAUSCA_1_31 as StdField with uid="UHFOVVYRZJ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_RICAUSCA", cQueryName = "RICAUSCA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di magazzino utilizzata per generare i movimenti di scarico",;
    HelpContextID = 34276183,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=75, Top=207, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_RICAUSCA"

  func oRICAUSCA_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICAUSCA_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICAUSCA_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oRICAUSCA_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSMA1ZGM.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oRICAUSCA_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_RICAUSCA
     i_obj.ecpSave()
  endproc

  add object oStr_1_3 as StdString with uid="ASJPMGRSJP",Visible=.t., Left=30, Top=16,;
    Alignment=1, Width=42, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="PFQLRTAJFK",Visible=.t., Left=1, Top=44,;
    Alignment=1, Width=71, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="FQQQZSFAOH",Visible=.t., Left=34, Top=71,;
    Alignment=1, Width=38, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="WXKFRNWGTJ",Visible=.t., Left=289, Top=16,;
    Alignment=1, Width=53, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="OOQRCLLJUE",Visible=.t., Left=160, Top=16,;
    Alignment=1, Width=32, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="JAXRROMFKM",Visible=.t., Left=10, Top=152,;
    Alignment=0, Width=125, Height=18,;
    Caption="Causali di magazzino"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="FZXDVABGCV",Visible=.t., Left=16, Top=182,;
    Alignment=1, Width=56, Height=18,;
    Caption="Carico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="EMLJQVZKHU",Visible=.t., Left=11, Top=211,;
    Alignment=1, Width=61, Height=18,;
    Caption="Scarico:"  ;
  , bGlobalFont=.t.

  add object oBox_1_25 as StdBox with uid="LVNILTIOAW",left=25, top=169, width=381,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_acr','CODIRILE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RICODICE=CODIRILE.RICODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
