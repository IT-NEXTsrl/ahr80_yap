* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: PLAF_V                                                          *
*              STAMPA PLAFOND                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 17/9/01                                                         *
* Last revis.: 8/10/02                                                         *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
private w_MESE,w_ESPO,w_UTILI,w_DISPO,w_TOTPAR
private w_Set,w_SETTA,w_STAMPA,PREFIS,w_RAGAZI
private NUMPAG,p_RAGAZI,p_PARTO,w_DATIAZ,p_CODO
private w_ANN,w_DESCME,w_VALPLA,w_Set,w_SETTA
private w_STAMPA,PREFIS,w_RAGAZI,NUMPAG,p_RAGAZI
private p_PARTO,w_DATIAZ,p_CODO,w_ANN,w_DESCME
private w_VALPLA,w_TOTUTI,w_DISINI,w_PLARES,w_DISUTI
private w_UTILIZZI,w_DISPDIS,w_PLADIS,w_DISESPO,w_CESSIONI

w_MESE = space(18)
w_ESPO = 0
w_UTILI = 0
w_DISPO = 0
w_TOTPAR = 0
w_Set = space(10)
w_SETTA = space(1)
w_STAMPA = space(50)
PREFIS = space(20)
w_RAGAZI = space(45)
NUMPAG = 0
p_RAGAZI = space(99)
p_PARTO = space(16)
w_DATIAZ = space(99)
p_CODO = space(16)
w_ANN = space(4)
w_DESCME = space(30)
w_VALPLA = space(3)
w_Set = space(10)
w_SETTA = space(1)
w_STAMPA = space(50)
PREFIS = space(20)
w_RAGAZI = space(45)
NUMPAG = 0
p_RAGAZI = space(99)
p_PARTO = space(16)
w_DATIAZ = space(99)
p_CODO = space(16)
w_ANN = space(4)
w_DESCME = space(30)
w_VALPLA = space(3)
w_TOTUTI = 0
w_DISINI = space(55)
w_PLARES = 0
w_DISUTI = space(55)
w_UTILIZZI = 0
w_DISPDIS = space(55)
w_PLADIS = 0
w_DISESPO = space(55)
w_CESSIONI = 0

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
  i_formh14 = 10
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do PLAF4V with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do PLAF4V with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


procedure PLAF4V
* === Procedure PLAF4V
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break

do case
  case i_form_id=1
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_frm_brk = .T.
    do while .not. eof() 
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do PLAF4V with 11, 0
        i_frm_rpr = .T.
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
        do PLAF5V with 1.00, 0
      if l_plamob='S'
        do PLAF5V with 1.01, 1
      endif
      if i_usr_brk
        exit
      endif
      if .not. eof()
        skip
      endif
    enddo
  case i_form_id=11
    do PLAF5V with 11.00, 10
    if l_plamob='S'
      do PLAF5V with 11.01, 1
    endif
  case i_form_id=12
    do PLAF5V with 12.00, 10
    if l_plamob='S'
      do PLAF5V with 12.01, 1
    endif
  case i_form_id=14
    if i_row+10>w_t_stnrig
      * --- stampa il piede di pagina
      do PLAF4V with 99, 10
    endif
    do PLAF5V with 14.00, 10
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do PLAF4V with 13, 0
        else
          do PLAF5V with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do PLAF4V with 12, 0
        else
          do PLAF5V with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = 0.00
    i_saveh13 = i_formh13
    i_formh13 = 0
endcase
return

procedure PLAF5V
* === Procedure PLAF5V
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do PLAF4V with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
  * --- 1� form
  case i_form_id=1.0
    do frm1_0
  case i_form_id=1.01
    do frm1_01
  * --- 11� form
  case i_form_id=11.0
    do frm11_0
  case i_form_id=11.01
    do frm11_01
  * --- 12� form
  case i_form_id=12.0
    do frm12_0
  case i_form_id=12.01
    do frm12_01
  * --- 14� form
  case i_form_id=14.0
    do frm14_0
endcase
i_row = i_row+i_form_h
return


* --- 1� form
procedure frm1_0
return

* --- frm1_01
procedure frm1_01
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),1,at_x(8),transform(DI__ANNO,""),i_fn
  endif
  w_MESE = left(IIF((diperiod)>0 AND(diperiod)<13, g_MESE(diperiod), SPACE(30)),18)
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),8,at_x(64),transform(w_MESE,""),i_fn
  endif
  w_ESPO = TRAN(ditotesp, V_PV[40+VVL])
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),34,at_x(272),transform(w_ESPO,""),i_fn
  endif
  w_UTILI = TRAN(diplauti, V_PV[40+VVL])
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),58,at_x(464),transform(w_UTILI,""),i_fn
  endif
  w_DISPO = TRAN(dipladis, V_PV[40+VVL])
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),82,at_x(656),transform(w_DISPO,""),i_fn
  endif
  w_TOTPAR = DIPLAUTI
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),117,at_x(936),transform(w_TOTPAR,""),i_fn
   endif
   w_TOTUTI = w_TOTUTI+w_TOTPAR
return

* --- 11� form
procedure frm11_0
  if .f.
  if len(trim('&w_t_st10'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_st10,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),0,at_x(0),transform(w_Set,""),i_fn
   endif
  w_SETTA = " "
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),3,at_x(24),transform(w_SETTA,'X'),i_fn
  w_STAMPA = "Stampa Plafond "+ iif(l_PLAMOB="S","Mobile","Fisso")
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),1,at_x(8),transform(w_STAMPA,""),i_fn
  PREFIS = RIGHT(SPACE(20)+ALLTRIM(L_PREFIS),20)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),87,at_x(696),transform(PREFIS,""),i_fn
  endif
  w_RAGAZI = RIGHT(SPACE(45)+ALLTRIM(g_ragazi),45)
  if L_INTLIG<>'S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),89,at_x(712),transform(w_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),112,at_x(896),"Pag.:",i_fn
  endif
  NUMPAG = L_PRPARI+I_PAG
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),127,at_x(1016),transform(NUMPAG,"9999999"),i_fn
   STPAG=NUMPAG
  endif
  p_RAGAZI = g_RAGAZI
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),1,at_x(8),transform(p_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),105,at_x(840),"Partita IVA:",i_fn
  endif
  p_PARTO = RIGHT(SPACE(16) + ALLTRIM(L_PIVAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),118,at_x(944),transform(p_PARTO,""),i_fn
  endif
  w_DATIAZ = trim(L_INDAZI)+' - '+L_CAPAZI+' - '+TRIM(L_LOCAZI)+IIF(EMPTY(L_PROAZI),'',' ( '+L_PROAZI+' )')
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(114-0),1,at_x(8),transform(w_DATIAZ,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(114-0),102,at_x(816),"Codice fiscale:",i_fn
  endif
  p_CODO = RIGHT(SPACE(16) + ALLTRIM(L_COFAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(114-0),118,at_x(944),transform(p_CODO,""),i_fn
  endif
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),7,at_x(60),"Anno:",i_fn
  w_ANN = l_ANNO
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),16,at_x(128),transform(w_ANN,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),28,at_x(224),"Mese:",i_fn
  w_DESCME = l_DESCRI
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),38,at_x(304),transform(w_DESCME,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),110,at_x(880),"Importi in:",i_fn
  w_VALPLA = l_VALPLA
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),126,at_x(1008),transform(w_VALPLA,""),i_fn
return

* --- frm11_01
procedure frm11_01
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),1,at_x(8),"Anno",i_fn
  endif
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),8,at_x(64),"Mese",i_fn
  endif
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),42,at_x(336),"Esportazioni",i_fn
  endif
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),71,at_x(568),"Utilizzi",i_fn
  endif
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),92,at_x(736),"Disponibile",i_fn
  endif
return

* --- 12� form
procedure frm12_0
  if .f.
  if len(trim('&w_t_st10'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_st10,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),0,at_x(0),transform(w_Set,""),i_fn
   endif
  w_SETTA = " "
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),3,at_x(24),transform(w_SETTA,'X'),i_fn
  w_STAMPA = "Stampa Plafond "+ iif(l_PLAMOB="S","Mobile","Fisso")
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),1,at_x(8),transform(w_STAMPA,""),i_fn
  PREFIS = RIGHT(SPACE(20)+ALLTRIM(L_PREFIS),20)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),87,at_x(696),transform(PREFIS,""),i_fn
  endif
  w_RAGAZI = RIGHT(SPACE(45)+ALLTRIM(g_ragazi),45)
  if L_INTLIG<>'S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),89,at_x(712),transform(w_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),112,at_x(896),"Pag.:",i_fn
  endif
  NUMPAG = L_PRPARI+I_PAG
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),127,at_x(1016),transform(NUMPAG,"9999999"),i_fn
   STPAG=NUMPAG
  endif
  p_RAGAZI = g_RAGAZI
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),1,at_x(8),transform(p_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),105,at_x(840),"Partita IVA:",i_fn
  endif
  p_PARTO = RIGHT(SPACE(16) + ALLTRIM(L_PIVAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),118,at_x(944),transform(p_PARTO,""),i_fn
  endif
  w_DATIAZ = trim(L_INDAZI)+' - '+L_CAPAZI+' - '+TRIM(L_LOCAZI)+IIF(EMPTY(L_PROAZI),'',' ( '+L_PROAZI+' )')
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(114-0),1,at_x(8),transform(w_DATIAZ,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(114-0),102,at_x(816),"Codice fiscale:",i_fn
  endif
  p_CODO = RIGHT(SPACE(16) + ALLTRIM(L_COFAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(114-0),118,at_x(944),transform(p_CODO,""),i_fn
  endif
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),8,at_x(64),"Anno:",i_fn
  w_ANN = l_ANNO
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),16,at_x(128),transform(w_ANN,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),28,at_x(224),"Mese:",i_fn
  w_DESCME = l_DESCRI
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),38,at_x(304),transform(w_DESCME,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),110,at_x(880),"Importi in:",i_fn
  w_VALPLA = l_VALPLA
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),126,at_x(1008),transform(w_VALPLA,""),i_fn
return

* --- frm12_01
procedure frm12_01
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),2,at_x(16),"Anno",i_fn
  endif
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),9,at_x(72),"Mese",i_fn
  endif
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),43,at_x(344),"Esportazioni",i_fn
  endif
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),72,at_x(576),"Utilizzi",i_fn
  endif
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),93,at_x(744),"Disponibile",i_fn
  endif
return

* --- 14� form
procedure frm14_0
  if i_row+i_formh14>w_t_stnrig
    * stampa il piede di pagina
    do PLAF4V with 13, 0
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),5,at_x(40),"���������������������������������������",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),44,at_x(352),"�����������������������������������������",i_fn
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),36,at_x(288),"Totale utilizzi:",i_fn
  endif
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),56,at_x(448),transform(w_TOTUTI,V_PV[40+VVL]),i_fn
  endif
  w_DISINI = RIGHT(SPACE(55)+ALLTRIM('Plafond Disponibile a inizio  ' + L_descri +' ' + l_anno +' :'),55)
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),2,at_x(16),transform(w_DISINI,""),i_fn
  w_PLARES = TRAN(L_PLARES, V_PV[40+VVL])
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),62,at_x(496),transform(w_PLARES,""),i_fn
  w_DISUTI = RIGHT(SPACE(55)+ALLTRIM('Utilizzo ' + L_descri +' ' +l_anno+' :'),55)
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),2,at_x(16),transform(w_DISUTI,""),i_fn
  w_UTILIZZI = TRAN(L_UTILIZZI, V_PV[40+VVL])
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),62,at_x(496),transform(w_UTILIZZI,""),i_fn
  w_DISPDIS = DicPLaDisDif()
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(114-0),2,at_x(16),transform(w_DISPDIS,""),i_fn
  w_PLADIS = TRAN(L_PLARES-L_UTILIZZI, V_PV[40+VVL])
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(114-0),62,at_x(496),transform(w_PLADIS,""),i_fn
  w_DISESPO = RIGHT(SPACE(55)+ALLTRIM('Esportazioni ' + L_descri +' ' +l_anno+' :'),55)
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),2,at_x(16),transform(w_DISESPO,""),i_fn
  endif
  w_CESSIONI = TRAN(L_CESSIONI, V_PV[40+VVL])
  if l_plamob='S'
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),62,at_x(496),transform(w_CESSIONI,""),i_fn
  endif
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),5,at_x(40),"���������������������������������������",i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(152-0),44,at_x(352),"�����������������������������������������",i_fn
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(171-0),7,at_x(56),"Lo spazio sottostante di questa pagina non e' stato utilizzato ed e'",i_fn
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(171-0),75,at_x(600),"da considerarsi annullato",i_fn
return

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

* --- Area Manuale = Functions & Procedures 
* --- PLAF_V
Func DicPLaDisDif()
 * calcola la dicitura delle terza riga nel footer 
 S=iif(l_plamob='S',RIGHT(SPACE(55)+'Differenza :',55),;
  RIGHT(SPACE(55)+ALLTRIM('Plafond Disponibile a fine  ' + L_descri +' ' +l_anno+' :'),55))
 Return(S )
EndFunc
* --- Fine Area Manuale 
