* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_alg                                                        *
*              Lingue                                                          *
*                                                                              *
*      Author: TAM SOFTWARE & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-04-23                                                      *
* Last revis.: 2009-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_alg"))

* --- Class definition
define class tgsar_alg as StdForm
  Top    = 44
  Left   = 36

  * --- Standard Properties
  Width  = 430
  Height = 106+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-09-29"
  HelpContextID=227112809
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  LINGUE_IDX = 0
  cFile = "LINGUE"
  cKeySelect = "LUCODICE"
  cKeyWhere  = "LUCODICE=this.w_LUCODICE"
  cKeyWhereODBC = '"LUCODICE="+cp_ToStrODBC(this.w_LUCODICE)';

  cKeyWhereODBCqualified = '"LINGUE.LUCODICE="+cp_ToStrODBC(this.w_LUCODICE)';

  cPrg = "gsar_alg"
  cComment = "Lingue"
  icon = "anag.ico"
  cAutoZoom = 'GSVE0ALG'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LUCODICE = space(3)
  w_LUDESCRI = space(30)
  w_LUCODISO = space(3)
  o_LUCODISO = space(3)
  w_LUABITRA = space(1)
  w_LUABTRDA = space(1)
  o_LUABTRDA = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'LINGUE','gsar_alg')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_algPag1","gsar_alg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Lingue")
      .Pages(1).HelpContextID = 255121078
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLUCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='LINGUE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.LINGUE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.LINGUE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_LUCODICE = NVL(LUCODICE,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from LINGUE where LUCODICE=KeySet.LUCODICE
    *
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('LINGUE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "LINGUE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' LINGUE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LUCODICE',this.w_LUCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LUCODICE = NVL(LUCODICE,space(3))
        .w_LUDESCRI = NVL(LUDESCRI,space(30))
        .w_LUCODISO = NVL(LUCODISO,space(3))
        .w_LUABITRA = NVL(LUABITRA,space(1))
        .w_LUABTRDA = NVL(LUABTRDA,space(1))
        cp_LoadRecExtFlds(this,'LINGUE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LUCODICE = space(3)
      .w_LUDESCRI = space(30)
      .w_LUCODISO = space(3)
      .w_LUABITRA = space(1)
      .w_LUABTRDA = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,3,.f.)
        .w_LUABITRA = 'N'
        .w_LUABTRDA = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'LINGUE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLUCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oLUDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oLUCODISO_1_6.enabled = i_bVal
      .Page1.oPag.oLUABITRA_1_7.enabled = i_bVal
      .Page1.oPag.oLUABTRDA_1_9.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oLUCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLUCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'LINGUE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LUCODICE,"LUCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LUDESCRI,"LUDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LUCODISO,"LUCODISO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LUABITRA,"LUABITRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LUABTRDA,"LUABTRDA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    i_lTable = "LINGUE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.LINGUE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSVE_QL1.VQR,QUERY\GSVE_SAA.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.LINGUE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.LINGUE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into LINGUE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'LINGUE')
        i_extval=cp_InsertValODBCExtFlds(this,'LINGUE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(LUCODICE,LUDESCRI,LUCODISO,LUABITRA,LUABTRDA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_LUCODICE)+;
                  ","+cp_ToStrODBC(this.w_LUDESCRI)+;
                  ","+cp_ToStrODBC(this.w_LUCODISO)+;
                  ","+cp_ToStrODBC(this.w_LUABITRA)+;
                  ","+cp_ToStrODBC(this.w_LUABTRDA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'LINGUE')
        i_extval=cp_InsertValVFPExtFlds(this,'LINGUE')
        cp_CheckDeletedKey(i_cTable,0,'LUCODICE',this.w_LUCODICE)
        INSERT INTO (i_cTable);
              (LUCODICE,LUDESCRI,LUCODISO,LUABITRA,LUABTRDA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_LUCODICE;
                  ,this.w_LUDESCRI;
                  ,this.w_LUCODISO;
                  ,this.w_LUABITRA;
                  ,this.w_LUABTRDA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.LINGUE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.LINGUE_IDX,i_nConn)
      *
      * update LINGUE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'LINGUE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " LUDESCRI="+cp_ToStrODBC(this.w_LUDESCRI)+;
             ",LUCODISO="+cp_ToStrODBC(this.w_LUCODISO)+;
             ",LUABITRA="+cp_ToStrODBC(this.w_LUABITRA)+;
             ",LUABTRDA="+cp_ToStrODBC(this.w_LUABTRDA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'LINGUE')
        i_cWhere = cp_PKFox(i_cTable  ,'LUCODICE',this.w_LUCODICE  )
        UPDATE (i_cTable) SET;
              LUDESCRI=this.w_LUDESCRI;
             ,LUCODISO=this.w_LUCODISO;
             ,LUABITRA=this.w_LUABITRA;
             ,LUABTRDA=this.w_LUABTRDA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.LINGUE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.LINGUE_IDX,i_nConn)
      *
      * delete LINGUE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'LUCODICE',this.w_LUCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    if i_bUpd
      with this
        if .o_LUCODISO<>.w_LUCODISO
          .Calculate_EFFCZMKUQG()
        endif
        if .o_LUABTRDA<>.w_LUABTRDA
          .Calculate_BNGTYCZBVE()
        endif
        if .o_LUABTRDA<>.w_LUABTRDA
          .Calculate_JYGCCWZEZI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_EFFCZMKUQG()
    with this
          * --- Check traduzioni
          .w_LUABITRA = IIF(Empty(.w_LUCODISO),'N',.w_LUABITRA)
          .w_LUABTRDA = IIF(Empty(.w_LUCODISO),'N',.w_LUABTRDA)
    endwith
  endproc
  proc Calculate_BNGTYCZBVE()
    with this
          * --- Manutenzione tabella cplangs
          GSAR_BLG(this;
             )
    endwith
  endproc
  proc Calculate_JYGCCWZEZI()
    with this
          * --- Messaggio di aggiornamento DB
          MsgManageDB(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLUABITRA_1_7.enabled = this.oPgFrm.Page1.oPag.oLUABITRA_1_7.mCond()
    this.oPgFrm.Page1.oPag.oLUABTRDA_1_9.enabled = this.oPgFrm.Page1.oPag.oLUABTRDA_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Delete end") or lower(cEvent)==lower("Insert end") or lower(cEvent)==lower("Update end")
          .Calculate_BNGTYCZBVE()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLUCODICE_1_1.value==this.w_LUCODICE)
      this.oPgFrm.Page1.oPag.oLUCODICE_1_1.value=this.w_LUCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oLUDESCRI_1_2.value==this.w_LUDESCRI)
      this.oPgFrm.Page1.oPag.oLUDESCRI_1_2.value=this.w_LUDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oLUCODISO_1_6.value==this.w_LUCODISO)
      this.oPgFrm.Page1.oPag.oLUCODISO_1_6.value=this.w_LUCODISO
    endif
    if not(this.oPgFrm.Page1.oPag.oLUABITRA_1_7.RadioValue()==this.w_LUABITRA)
      this.oPgFrm.Page1.oPag.oLUABITRA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLUABTRDA_1_9.RadioValue()==this.w_LUABTRDA)
      this.oPgFrm.Page1.oPag.oLUABTRDA_1_9.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'LINGUE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_LUCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLUCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_LUCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LUCODISO = this.w_LUCODISO
    this.o_LUABTRDA = this.w_LUABTRDA
    return

enddefine

* --- Define pages as container
define class tgsar_algPag1 as StdContainer
  Width  = 426
  height = 106
  stdWidth  = 426
  stdheight = 106
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLUCODICE_1_1 as StdField with uid="SPBCSCWLGU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LUCODICE", cQueryName = "LUCODICE",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della lingua",;
    HelpContextID = 660219,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=94, Top=10, InputMask=replicate('X',3)

  add object oLUDESCRI_1_2 as StdField with uid="ADMKUKBDTA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LUDESCRI", cQueryName = "LUDESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della lingua",;
    HelpContextID = 183509759,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=94, Top=33, InputMask=replicate('X',30)

  add object oLUCODISO_1_6 as StdField with uid="KVYKXUOXDQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LUCODISO", cQueryName = "LUCODISO",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice ISO (ISO-639) lingua",;
    HelpContextID = 660229,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=94, Top=57, InputMask=replicate('X',3)

  add object oLUABITRA_1_7 as StdCheck with uid="TDSSEKWNLA",rtseq=4,rtrep=.f.,left=147, top=57, caption="Abilita traduzione stampe a runtime",;
    ToolTipText = "Abilitazione lingua per traduzioni a runtime",;
    HelpContextID = 189592311,;
    cFormVar="w_LUABITRA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLUABITRA_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oLUABITRA_1_7.GetRadio()
    this.Parent.oContained.w_LUABITRA = this.RadioValue()
    return .t.
  endfunc

  func oLUABITRA_1_7.SetRadio()
    this.Parent.oContained.w_LUABITRA=trim(this.Parent.oContained.w_LUABITRA)
    this.value = ;
      iif(this.Parent.oContained.w_LUABITRA=='S',1,;
      0)
  endfunc

  func oLUABITRA_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not(Empty(.w_LUCODISO)))
    endwith
   endif
  endfunc

  add object oLUABTRDA_1_9 as StdCheck with uid="MIFEGIRJEN",rtseq=5,rtrep=.f.,left=147, top=83, caption="Abilita traduzione dati a runtime",;
    ToolTipText = "Abilitazione lingua per traduzioni dati a runtime",;
    HelpContextID = 167572215,;
    cFormVar="w_LUABTRDA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLUABTRDA_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oLUABTRDA_1_9.GetRadio()
    this.Parent.oContained.w_LUABTRDA = this.RadioValue()
    return .t.
  endfunc

  func oLUABTRDA_1_9.SetRadio()
    this.Parent.oContained.w_LUABTRDA=trim(this.Parent.oContained.w_LUABTRDA)
    this.value = ;
      iif(this.Parent.oContained.w_LUABTRDA=='S',1,;
      0)
  endfunc

  func oLUABTRDA_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not(Empty(.w_LUCODISO)))
    endwith
   endif
  endfunc

  add object oStr_1_3 as StdString with uid="WYUGGAHURI",Visible=.t., Left=5, Top=10,;
    Alignment=1, Width=85, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="PSZXTIGTXP",Visible=.t., Left=5, Top=33,;
    Alignment=1, Width=85, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="BGJEVIRGTH",Visible=.t., Left=5, Top=57,;
    Alignment=1, Width=85, Height=18,;
    Caption="Codice ISO:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_alg','LINGUE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LUCODICE=LINGUE.LUCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_alg
proc MsgManageDB(obj)
if obj.w_LUABTRDA='S'
  ah_errormsg("L'attivazione della traduzione aggiungerà le informazioni necessarie al database:%0uscire, rientrare nell'applicazione ed eseguire la procedura di conversione %1",,,iif(g_APPLICATION="ADHOC REVOLUTION","5.0-M5181","20090700--00000 - 22"))
endif
endproc
* --- Fine Area Manuale
