* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bpt                                                        *
*              Controllo previsioni c./C.R.                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_52]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-09-03                                                      *
* Last revis.: 2007-11-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bpt",oParentObject)
return(i_retval)

define class tgsca_bpt as StdBatch
  * --- Local variables
  w_SIMBOLO = space(5)
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Controllo previsioni per C.C.R. (da GSCA_SPC)
    if empty( this.oParentObject.w_DATA1 ) OR empty( this.oParentObject.w_DATA2 )
      ah_ErrorMsg("Intervallo di date non valido",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Il tipo rappresenta la provenienza del Movimento, pu� valere:
    * --- PN - Prima Nota, AN Mov. Analitica, RA MOVIRIPA
    * --- Movimenti Previsionali (Sono esclusivamente Manuali)
    vq_exec("QUERY\GSCA1SPC.VQR",this,"PREVISIO")
    * --- Movimenti Originari
    * --- Prendo tutti i Movimenti Effettivi (Manuali + Prima Nota + Documenti)
    vq_exec("QUERY\GSCA5SPC.VQR",this,"EFFET")
    if this.oParentObject.w_PROVE<>"O"
      * --- Prendo Tutti i movimenti Effettivi Ripartiti
      vq_exec("QUERY\GSCA_SPC.VQR",this,"RIPAREFF")
      * --- Metto tutto assieme
      SELECT DISTINCT MRCODICE,MRCODVOC,MR_SEGNO,MRTOTIMP,MRINICOM,;
      MRFINCOM,VACAOVAL,VACODVAL,VCDESCRI,CCDESPIA,TIPO,;
      CHIAVE1,CHIAVE2,CHIAVE3,DUPLICA FROM EFFET;
      UNION ALL;
      SELECT MRCODICE,MRCODVOC,MR_SEGNO,MRTOTIMP,MRINICOM,;
      MRFINCOM,VACAOVAL,VACODVAL,VCDESCRI,CCDESPIA,TIPO,;
      CHIAVE1,CHIAVE2,CHIAVE3,DUPLICA FROM RIPAREFF;
      ORDER BY 1,2 INTO CURSOR EFFET
      * --- Prendo Tutti i movimenti Previsionali Ripartiti
      vq_exec("QUERY\GSCA2SPC.VQR",this,"RIPAPREV")
      SELECT MRCODICE,MRCODVOC,MR_SEGNO,MRTOTIMP,MRINICOM,;
      MRFINCOM,VACAOVAL,VACODVAL,VCDESCRI,CCDESPIA,TIPO,;
      CHIAVE1,CHIAVE2,CHIAVE3,DUPLICA FROM PREVISIO;
      UNION ALL;
      SELECT MRCODICE,MRCODVOC,MR_SEGNO,MRTOTIMP,MRINICOM,;
      MRFINCOM,VACAOVAL,VACODVAL,VCDESCRI,CCDESPIA,TIPO,;
      CHIAVE1,CHIAVE2,CHIAVE3,DUPLICA FROM RIPAPREV;
      ORDER BY 1,2 INTO CURSOR PREVISIO
    endif
    * --- Calcolo gli importi in base al periodo se attivo il check
    if this.oParentObject.w_COMPET ="S"
      * --- se una data � vuota lo � anche l'altra- se da magazzino prendo tutto l'importo
      * --- Per calcolare il RATEO moltipiclo l'importo per il numero di giorni della competenza interni al periodo
      * --- il risultato lo divido l'importo per il numero di giorni della competenza
      * --- I cambi e gli arrotondamenti vengono fatti nel report - le valute sono comunque europee (doc. leggo importo in valuta esercizio)
      select mrcodice,mrcodvoc,mr_segno,;
      iif(EMPTY(nvl(cp_todate(MRFINCOM),cp_CharToDate("  -  -  "))) ,MRTOTIMP,mrtotimp*;
      ( ((nvl(cp_todate(mrfincom),cp_CharToDate("  -  -  ")))-(nvl(cp_todate(mrinicom),cp_CharToDate("  -  -  ")))+1)-(;
      iif(this.oParentObject.w_data1>=(nvl(cp_todate(MRINICOM),cp_CharToDate("  -  -  "))),this.oParentObject.w_DATA1-(nvl(cp_todate(MRINICOM),cp_CharToDate("  -  -  "))),0)+;
      IIF(this.oParentObject.w_data2<=(nvl(cp_todate(MRFINCOM),cp_CharToDate("  -  -  "))),;
      (nvl(cp_todate(MRFINCOM),cp_CharToDate("  -  -  ")))-this.oParentObject.w_DATA2,0)) )/;
      ((nvl(cp_todate(mrfincom),cp_CharToDate("  -  -  ")))-(nvl(cp_todate(mrinicom),cp_CharToDate("  -  -  ")))+1)) as mrtotimp,;
      VACODVAL,VACAOVAL,VCDESCRI,CCDESPIA FROM EFFET ORDER BY 1,2 INTO CURSOR EFFET
      select mrcodice,mrcodvoc,mr_segno,;
      iif(EMPTY(nvl(cp_todate(MRFINCOM),cp_CharToDate("  -  -  "))) ,MRTOTIMP,mrtotimp*;
      ( ((nvl(cp_todate(mrfincom),cp_CharToDate("  -  -  ")))-(nvl(cp_todate(mrinicom),cp_CharToDate("  -  -  ")))+1)-(;
      iif(this.oParentObject.w_data1>=(nvl(cp_todate(MRINICOM),cp_CharToDate("  -  -  "))),this.oParentObject.w_DATA1-(nvl(cp_todate(MRINICOM),cp_CharToDate("  -  -  "))),0)+;
      IIF(this.oParentObject.w_data2<=(nvl(cp_todate(MRFINCOM),cp_CharToDate("  -  -  "))),;
      (nvl(cp_todate(MRFINCOM),cp_CharToDate("  -  -  ")))-this.oParentObject.w_DATA2,0)) )/;
      ((nvl(cp_todate(mrfincom),cp_CharToDate("  -  -  ")))-(nvl(cp_todate(mrinicom),cp_CharToDate("  -  -  ")))+1)) as mrtotimp,;
      VACODVAL,VACAOVAL,VCDESCRI,CCDESPIA FROM PREVISIO ORDER BY 1,2 INTO CURSOR PREVISIO
    endif
    * --- Effettuo una select per convertire l'importo
    SELECT MRCODICE,MRCODVOC,MR_SEGNO,EFFET.MRTOTIMP AS MRTOTIMP,;
    VACAOVAL, VACODVAL AS VACODVAL,VCDESCRI,CCDESPIA ;
    FROM EFFET INTO CURSOR EFFET
    SELECT MRCODICE,MRCODVOC,MR_SEGNO,PREVISIO.MRTOTIMP AS MRTOTIMP,;
    VACAOVAL,VACODVAL,VCDESCRI,CCDESPIA ;
    FROM PREVISIO INTO CURSOR PREVISIO
    * --- --
    SELECT MRCODICE,MRCODVOC,MR_SEGNO,SUM(IIF(MR_SEGNO = "D",EFFET.MRTOTIMP, -1*EFFET.MRTOTIMP)) AS MRTOTIMP,;
    VACAOVAL, VACODVAL AS VACODVAL,VCDESCRI,CCDESPIA ;
    FROM EFFET GROUP BY MRCODICE,MRCODVOC INTO CURSOR EFFET
    SELECT MRCODICE,MRCODVOC,MR_SEGNO,SUM(IIF(MR_SEGNO = "D",PREVISIO.MRTOTIMP, -1*PREVISIO.MRTOTIMP)) AS MRTOTIMP,;
    VACAOVAL,VACODVAL,VCDESCRI,CCDESPIA ;
    FROM PREVISIO GROUP BY MRCODICE,MRCODVOC INTO CURSOR PREVISIO
    * --- Effetto una Full Join per collegare tutti i movimenti effettivi con tutti i movimenti previsionali
    SELECT * FROM PREVISIO FULL JOIN EFFET ON PREVISIO.MRCODICE=EFFET.MRCODICE AND;
    PREVISIO.MRCODVOC=EFFET.MRCODVOC;
    INTO CURSOR __TMP__
    SELECT IIF(EMPTY(NVL(MRCODICE_A,"")),MRCODICE_B,MRCODICE_A) AS MRCODICE,;
     IIF(EMPTY(NVL(MRCODVOC_A,"")),MRCODVOC_B,MRCODVOC_A) AS MRCODVOC,;
     IIF(EMPTY(NVL(VACODVAL_A,"")),VACODVAL_B,VACODVAL_A) AS VACODVAL,;
    MR_SEGNO_A AS MR_SEGNOP,MRTOTIMP_A AS MRTOTIMPP,;
     IIF(EMPTY(NVL(VCDESCRI_A,"")),VCDESCRI_B,VCDESCRI_A) AS VCDESCRI,;
    MR_SEGNO_B AS MR_SEGNOE,MRTOTIMP_B AS MRTOTIMPE,;
    IIF(EMPTY(NVL(CCDESPIA_A,"")),CCDESPIA_B,CCDESPIA_A) AS CCDESPIA,;
    VACAOVAL_A AS VACAOVAL, VACAOVAL_B AS VACAOVAL;
    FROM __TMP__ ORDER BY 1,2 INTO CURSOR __TMP__
    * --- Leggo il simbolo della valuta di conto - la stampa � comunque espressa nella valuta di conto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL,VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL,VACAOVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      w_CAMBIO = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Variabili utilizzate nel Report
    L_CAMBIO=w_CAMBIO
    L_CONTO=this.oParentObject.w_CONTO
    L_SIMBOLO=this.w_SIMBOLO
    L_CONTO1=this.oParentObject.w_CONTO1
    L_DATA1=this.oParentObject.w_DATA1
    L_COMPET=this.oParentObject.w_COMPET
    L_DATA2=this.oParentObject.w_DATA2
    L_PROVE=this.oParentObject.w_PROVE
    * --- Lancio la Stampa
    CP_CHPRN("QUERY\GSCA_SPC.FRX", " ", this)
    if used("Previsio")
      select Previsio
      use
    endif
    if used("Effet")
      select Effet
      use
    endif
    if used("Ripareff")
      select Ripareff
      use
    endif
    if used("Ripaprev")
      select Ripaprev
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
