* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kdg                                                        *
*              Wizard duplicazione gadget                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-12-16                                                      *
* Last revis.: 2016-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kdg",oParentObject))

* --- Class definition
define class tgsut_kdg as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 492
  Height = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-21"
  HelpContextID=82396009
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  MOD_GADG_IDX = 0
  GRP_GADG_IDX = 0
  MYG_MAST_IDX = 0
  GADCOLOR_IDX = 0
  cPrg = "gsut_kdg"
  cComment = "Wizard duplicazione gadget"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TEST_F10 = space(1)
  w_GA__NOME = space(50)
  w_GADESCRI = space(0)
  w_GASRCPRG = space(50)
  w_GAGRPCOD = space(5)
  w_IMAGE = space(252)
  o_IMAGE = space(252)
  w_GACODICE = space(50)
  w_TITLE = space(254)
  w_HAVETITLE = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kdgPag1","gsut_kdg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGA__NOME_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='MOD_GADG'
    this.cWorkTables[2]='GRP_GADG'
    this.cWorkTables[3]='MYG_MAST'
    this.cWorkTables[4]='GADCOLOR'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TEST_F10=space(1)
      .w_GA__NOME=space(50)
      .w_GADESCRI=space(0)
      .w_GASRCPRG=space(50)
      .w_GAGRPCOD=space(5)
      .w_IMAGE=space(252)
      .w_GACODICE=space(50)
      .w_TITLE=space(254)
      .w_HAVETITLE=.f.
      .w_TEST_F10=oParentObject.w_TEST_F10
      .w_GA__NOME=oParentObject.w_GA__NOME
      .w_GADESCRI=oParentObject.w_GADESCRI
      .w_GASRCPRG=oParentObject.w_GASRCPRG
      .w_GAGRPCOD=oParentObject.w_GAGRPCOD
      .w_GACODICE=oParentObject.w_GACODICE
      .w_TITLE=oParentObject.w_TITLE
      .w_HAVETITLE=oParentObject.w_HAVETITLE
        .w_TEST_F10 = 'S'
        .DoRTCalc(2,4,.f.)
        if not(empty(.w_GASRCPRG))
          .link_1_6('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_GAGRPCOD))
          .link_1_8('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_IMAGE)
    endwith
    this.DoRTCalc(6,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TEST_F10=.w_TEST_F10
      .oParentObject.w_GA__NOME=.w_GA__NOME
      .oParentObject.w_GADESCRI=.w_GADESCRI
      .oParentObject.w_GASRCPRG=.w_GASRCPRG
      .oParentObject.w_GAGRPCOD=.w_GAGRPCOD
      .oParentObject.w_GACODICE=.w_GACODICE
      .oParentObject.w_TITLE=.w_TITLE
      .oParentObject.w_HAVETITLE=.w_HAVETITLE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_6('Full')
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_IMAGE)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_IMAGE)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTITLE_1_16.visible=!this.oPgFrm.Page1.oPag.oTITLE_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GASRCPRG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
    i_lTable = "MOD_GADG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2], .t., this.MOD_GADG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GASRCPRG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GASRCPRG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGSRCPRG,MGPREIMG";
                   +" from "+i_cTable+" "+i_lTable+" where MGSRCPRG="+cp_ToStrODBC(this.w_GASRCPRG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGSRCPRG',this.w_GASRCPRG)
            select MGSRCPRG,MGPREIMG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GASRCPRG = NVL(_Link_.MGSRCPRG,space(50))
      this.w_IMAGE = NVL(_Link_.MGPREIMG,space(252))
    else
      if i_cCtrl<>'Load'
        this.w_GASRCPRG = space(50)
      endif
      this.w_IMAGE = space(252)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])+'\'+cp_ToStr(_Link_.MGSRCPRG,1)
      cp_ShowWarn(i_cKey,this.MOD_GADG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GASRCPRG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GAGRPCOD
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRP_GADG_IDX,3]
    i_lTable = "GRP_GADG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2], .t., this.GRP_GADG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GAGRPCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRP_GADG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GGCODICE like "+cp_ToStrODBC(trim(this.w_GAGRPCOD)+"%");

          i_ret=cp_SQL(i_nConn,"select GGCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GGCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GGCODICE',trim(this.w_GAGRPCOD))
          select GGCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GGCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GAGRPCOD)==trim(_Link_.GGCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GAGRPCOD) and !this.bDontReportError
            deferred_cp_zoom('GRP_GADG','*','GGCODICE',cp_AbsName(oSource.parent,'oGAGRPCOD_1_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GGCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GGCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GGCODICE',oSource.xKey(1))
            select GGCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GAGRPCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GGCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GGCODICE="+cp_ToStrODBC(this.w_GAGRPCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GGCODICE',this.w_GAGRPCOD)
            select GGCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GAGRPCOD = NVL(_Link_.GGCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_GAGRPCOD = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2])+'\'+cp_ToStr(_Link_.GGCODICE,1)
      cp_ShowWarn(i_cKey,this.GRP_GADG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GAGRPCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGA__NOME_1_2.value==this.w_GA__NOME)
      this.oPgFrm.Page1.oPag.oGA__NOME_1_2.value=this.w_GA__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oGADESCRI_1_4.value==this.w_GADESCRI)
      this.oPgFrm.Page1.oPag.oGADESCRI_1_4.value=this.w_GADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oGASRCPRG_1_6.RadioValue()==this.w_GASRCPRG)
      this.oPgFrm.Page1.oPag.oGASRCPRG_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGAGRPCOD_1_8.RadioValue()==this.w_GAGRPCOD)
      this.oPgFrm.Page1.oPag.oGAGRPCOD_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGACODICE_1_14.value==this.w_GACODICE)
      this.oPgFrm.Page1.oPag.oGACODICE_1_14.value=this.w_GACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTITLE_1_16.value==this.w_TITLE)
      this.oPgFrm.Page1.oPag.oTITLE_1_16.value=this.w_TITLE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_GA__NOME))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGA__NOME_1_2.SetFocus()
            i_bnoObbl = !empty(.w_GA__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GACODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGACODICE_1_14.SetFocus()
            i_bnoObbl = !empty(.w_GACODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IMAGE = this.w_IMAGE
    return

enddefine

* --- Define pages as container
define class tgsut_kdgPag1 as StdContainer
  Width  = 488
  height = 225
  stdWidth  = 488
  stdheight = 225
  resizeXpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGA__NOME_1_2 as StdField with uid="WNZSKKDGZP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GA__NOME", cQueryName = "GA__NOME",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome del Gadget",;
    HelpContextID = 10751317,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=211, Top=17, InputMask=replicate('X',50)

  add object oGADESCRI_1_4 as StdMemo with uid="MKKJPXKEYB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GADESCRI", cQueryName = "GADESCRI",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della funzionalit� del gadget",;
    HelpContextID = 59785903,;
   bGlobalFont=.t.,;
    Height=84, Width=316, Left=78, Top=62, bdisablefrasimodello=.t.


  add object oGASRCPRG_1_6 as StdTableCombo with uid="ZFPBHOVQPK",rtseq=4,rtrep=.f.,left=78,top=150,width=150,height=22, enabled=.f.;
    , ToolTipText = "Modello del gadget";
    , HelpContextID = 262025901;
    , cFormVar="w_GASRCPRG",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="MOD_GADG";
    , cTable='MOD_GADG',cKey='MGSRCPRG',cValue='MGDESCRI',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  func oGASRCPRG_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oGAGRPCOD_1_8 as StdTableCombo with uid="EMYRNJKKIJ",rtseq=5,rtrep=.f.,left=333,top=150,width=150,height=22;
    , cDescEmptyElement = "Nessuna";
    , ToolTipText = "Area di appartenenza";
    , HelpContextID = 57504426;
    , cFormVar="w_GAGRPCOD",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="GRP_GADG";
    , cTable='GRP_GADG',cKey='GGCODICE',cValue='GGDESCRI',cOrderBy='GGCODICE',xDefault=space(5);
  , bGlobalFont=.t.


  func oGAGRPCOD_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oGAGRPCOD_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oBtn_1_11 as StdButton with uid="ZMIZZNTUDJ",left=383, top=175, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 82367258;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="JWIBHYOYEX",left=433, top=175, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 75078586;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oGACODICE_1_14 as StdField with uid="WVHUHHETSY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_GACODICE", cQueryName = "GACODICE",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 145371819,;
   bGlobalFont=.t.,;
    Height=21, Width=91, Left=78, Top=17, InputMask=replicate('X',50)


  add object oObj_1_15 as cp_showimage with uid="DYWSEMRHRC",left=398, top=63, width=83,height=82,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="default bitmap",stretch=2,;
    nPag=1;
    , HelpContextID = 95601638

  add object oTITLE_1_16 as StdField with uid="ROTLSRFHVI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TITLE", cQueryName = "TITLE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Titolo che verr� visualizzato sul Gadget",;
    HelpContextID = 4699338,;
   bGlobalFont=.t.,;
    Height=21, Width=405, Left=78, Top=40, InputMask=replicate('X',254)

  func oTITLE_1_16.mHide()
    with this.Parent.oContained
      return (!.w_HAVETITLE)
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="BHGUCQUBTC",Visible=.t., Left=171, Top=19,;
    Alignment=1, Width=40, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="VSPQKIQYQG",Visible=.t., Left=3, Top=62,;
    Alignment=1, Width=74, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="KMZGSQNRGB",Visible=.t., Left=27, Top=152,;
    Alignment=1, Width=50, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="GCBCPRPPKJ",Visible=.t., Left=281, Top=152,;
    Alignment=1, Width=50, Height=18,;
    Caption="Area:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="YKCLABRLBQ",Visible=.t., Left=31, Top=19,;
    Alignment=1, Width=46, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="WWIOHTOKIV",Visible=.t., Left=37, Top=42,;
    Alignment=1, Width=40, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (!.w_HAVETITLE)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kdg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
