* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_brz                                                        *
*              Controllo zoom                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-06                                                      *
* Last revis.: 2014-03-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_brz",oParentObject)
return(i_retval)

define class tgsut_brz as StdBatch
  * --- Local variables
  w_LOGFILE = space(254)
  w_HFILE = 0
  w_i = 0
  objZoom = .NULL.
  w_RESULT = space(254)
  ERR = .f.
  w_FULLPATH = space(254)
  w_BACKUPDIR = space(20)
  w_FILESEARCH = space(10)
  w_oERRORLOG = .NULL.
  w_File = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione cursore per report di log
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    cp_ReadXdc()
    this.oParentObject.w_path = Alltrim(this.oParentObject.w_path)
    this.w_FILESEARCH = "*_VZM"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_FILESEARCH = "*.VZM"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_oErrorLog.IsFullLog()
      this.w_oERRORLOG.PrintLog(this,"Log segnalazioni zoom errati")     
    else
      ah_errormsg("Non ci sono errori nei file analizzati")
    endif
    if Used("__Tmp__")
      USE IN SELECT("__TMP__")
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_BACKUPDIR = "Backup"
    * --- Creo un array con la funzione ricorsiva visitaDir
     
 DIMENSION AFNAME(1) 
 visitaDir(this.oParentObject.w_path,this.w_FILESEARCH,@AFNAME,.T.)
    * --- Scandisco prima le configurazioni di zoom
    if TYPE("AFNAME[1,1]")="C"
      this.w_i = 1
      do while this.w_i<=ALEN(AFNAME,1)
        * --- metto in w_FULLPATH il percorso del file contenuto nella posizione dell'array
        this.w_FULLPATH = AFNAME[this.w_i,1]
        * --- Prima di effettuare il Repair degli zoom, verifico di poter creare una cartella
        *     per il backup dei file; se non ci riesco, interrompo l'elaborazione
        if this.oParentObject.w_REPAIR
          this.ERR = .F.
           
 cOldErr=on("ERROR") 
 on error this.err=.t.
          * --- Verifico che w_FULLPATH non sia n� '.' n� '..' e che la cartella di backup non sia stata
          *     gi� precedentemente creata
          if !(this.w_FULLPATH=".") and NOT (DIRECTORY(alltrim(substr(this.w_FULLPATH,1,rat("\",this.w_FULLPATH)))+"\"+this.w_BACKUPDIR))
            mkdir(alltrim(substr(this.w_FULLPATH,1,rat("\",this.w_FULLPATH)))+"\"+this.w_BACKUPDIR)
          endif
          if this.ERR
            ah_errormsg("Impossibile creare la cartella %1%0%2",48,,this.w_FULLPATH+chr(47)+this.w_BACKUPDIR,Message())
            on error &cOldErr
            i_retcode = 'stop'
            return
          endif
          on error &cOldErr
        endif
        * --- Se nella colonna 5 dell'array � presente una 'D' significa che sto trattando una directory...
        if AT("D",ALLTRIM(AFNAME[this.w_i,5]))=0
          this.w_File = substr(this.w_FULLPATH,rat("\",this.w_FULLPATH)+1,LEN(this.w_FULLPATH))
          ah_msg("Esame %1 in corso...",.t.,,,Left(this.w_FULLPATH,230))
          this.objZoom=createobject("cp_zoomchecker",this.w_FULLPATH)
          this.w_RESULT = this.objZoom.checkZoom(this.oParentObject.w_CHECKTABLE,this.oParentObject.w_CHECKKEY,this.oParentObject.w_CHECKNOKEY,this.oParentObject.w_REPAIR)
          if Not Empty(this.w_RESULT)
            if this.oParentObject.w_REPAIR
              this.objZoom.Save_Zmn(this.w_BACKUPDIR)     
            endif
            * --- w_RESULT viene gi� tradotto dalla funzione checkZoom nell'area manuale
            this.w_oERRORLOG.AddMsgLog("%1",this.w_FULLPATH+" "+this.w_RESULT)     
            this.w_oERRORLOG.AddMsgLog("%1"," ")     
          endif
          this.objZoom = null
        endif
        this.w_i = this.w_i+1
      enddo
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- gsut_brz
  * --- LA SEGUENTE ENDDEFINE CHIUDE LA CLASSE DEL BATCH ---
  EndDefine
  
  Define Class Cp_ZoomChecker As Custom
  cFileName='' && nome del File
  xcg=.Null. && oggetto per lettura file zoom
  nPY=0
  nPX=0
  nWidth=0
  nHeigth=0
  cTitle=''
  cProgram=''
  bFlagOption=.t.
  cSymFile=''
  * --- Costruisco la stringa per ingannare il software
  * --- di pulizia dell'ambiente
  cNoMoreUsed='**'+chr(32)+'NON PIU UTILIZZATO'+chr(32)+'**' && Escludo i file che iniziano per questa frase dalla ricerca
    * --- campi
    nFields=0
    dimension cFields(1)
    cFields(1)=''
    
    * --- condizioni di where
    nWhere=0
    dimension cWhere(1)
    cWhere(1)=''
  
    * --- campi di order by
    nOrderBy=0
    dimension cOrderBy[1]
    cOrderBy[1]=''
    
      * --- i campi della chiave primaria per Drag&Drop verso altri form
    cKeyFields=''
      nColPositions=0
    dimension nColWidth[1]
    nColWidth[1]=0
    dimension nColOrd[1]
    nColOrd[1]=0
    dimension cColTitle[1]
    cColTitle[1]=''
    dimension cColFormat[1]
    cColFormat[1]=''
    dimension nColRW[1]
    nColRW[1]=0
    dimension nColX[1]
    nColX[1]=0
    dimension nColY[1]
    nColY[1]=0
    dimension nColH[1]
    nColH[1]=0
    dimension cForeColor[1]
    nForeColor[1]=''
    dimension cBackColor[1]
    nBackColor[1]=''
    dimension cHyperLink[1]
    cHyperLink[1]=''
    dimension cHyperLinkTarget[1]
    cHyperLinkTarget[1]=''
    dimension bIsImage[1]
    bIsImage[1]=.f.
    dimension cLayerContent[1]
    cLayerContent[1]=''
    dimension nColumnType[1]
    nColumnType[1]=0
    dimension cTooltip[1]
    cTooltip[1]=''
    
  	* --- gestione carattere testata
  	Dimension cLabelFont[1]
  	cLabelFont[1]=''
  	Dimension nDimFont[1]
  	nDimFont[1]=0
  	Dimension bItalicFont[1]
  	bItalicFont[1]=.F.
  	Dimension bBoldFont[1]
  	bBoldFont[1]=.F.
  	* --- gestione carattere colonne
  	Dimension cLabelFontC[1]
  	cLabelFontC[1]=''
  	Dimension nDimFontC[1]
  	nDimFontC[1]=0
  	Dimension bItalicFontC[1]
  	bItalicFontC[1]=.F.
  	Dimension bBoldFontC[1]
  	bBoldFontC[1]=.F.	
    
  	bAddedCheckBox=.F.
  	bDefaultHeightGrid=.T.
    nLockColumns=0
    
    bAskParam=.f.
  
    dimension bOrderDesc[1]
    bOrderDesc[1]=.f.
    
    cCpQueryName=''
    bKeepSize=.f.
    
    bModSQL=.f.       && segnala se la frase SQL e' stata modificata
    cSQL=''           && la frase SQL modidicata
  
  * --- Dimensione griglia
  i_gy=0
  i_gx=0
  i_gh=0
  i_gw=0
  * --- Dimensioni Form
  i_fw=0
  i_fh=0
  nGrdW=10
  nGrdH=35
    
   proc Init( cFileName)
     LOCAL P,oXcg
     This.cFileName=cFileName
     p=cp_GetPFile(cFileName)
     if not(empty(p))
      oXcg=createobject('pxcg','load',P)
      This.Serialize(oXcg)
      oXcg=.null.
     endif   
   ENDPROC
   
   * --- Analizza lo zoom e restituisce:
   * --- '' Tutto Ok
   * --- 'Manca nome tabella '
   * --- 'Chiave errata'
   * --- 'Campi chiave non presenti'
   * --- 'File non pi� utilizzato'
  FUNCTION checkZoom(bCheckTable,bCheckKey,bCheckNoKey,bRepair)
  	LOCAL sKey,sResult,nOcc,n_i,sTemp, n_OldPos, oMess, oPart
  	sResult=''
  	oMess = createobject("ah_message")
  	IF This.cSymFile<>this.cNoMoreUsed
  	   * --- Tabella Vuota
  		IF EMPTY(THIS.CSYMFILE) AND bCheckTable
  			oMESS.AddMsgPart("%0Nome Tabella Vuoto")
  			IF bRepair
  				This.cSymFile=Upper(SUBSTR(This.cFileName,ATC('.',This.cFileName)+1,LEN(This.cFileName)-ATC('.',This.cFileName)+1-5))
  			          oPart = oMESS.AddMsgPart("%1CORRETTO")
                  oPart.addParam(" ")
  			endif
  		ENDIF
  		
      sKey=Upper(CP_KEYTOSQL(I_DCX.GETIDXDEF(upper(THIS.CSYMFILE),1)))
      
  		* --- VERIFICA SE LA CHIAVE � CORRETTA..
  		* --- LEGGO DAL'XDC LA CHIAVE DELLA TABELLA..
  		IF bCheckKey
  			IF NOT UPPER(sKey)==UPPER(THIS.cKeyFields)
  			          oMESS.AddMsgPart("%0Manca la chiave di riferimento")
  				IF bRepair AND not empty (sKey)
  					This.cKeyFields=sKey
  					oPart = oMESS.AddMsgPart("%1CORRETTO")
            oPart.addParam(" ")
  				endif
  			ENDIF
  		endif	
      
      * --- Verifico sempre se l'eventuale Visual Query c'�..
      If Not Empty(this.cCpQueryName) And Not File(this.cCpQueryName+'.vqr')
         oPart = oMESS.AddMsgPart("%0Query %1 legata allo zoom non presente")
         oPart.addParam(this.cCpQueryName)
      Endif
      
  		* --- Campi chiavi presenti tutti ?
  		* --- Scorro la chiave (se presente o meno tra le colonne)
  		IF bCheckNoKey
  			nOcc=Occurs(",", sKey) +1
  			n_OldPos=1
  			FOR n_i=1 TO nOcc 
  				n_NewPos=ATC( ',',sKey,n_i )		
  				n_NewPos=IIF( n_NewPos=0, LEN( sKey )+1 , n_NewPos )
  				sTemp=ALLTRIM( SUBSTR( sKey, n_OldPos, n_NewPos-n_oldpos ) )	
  				* --- Esiste il campo nel tracciato dello Zoom ?
  				IF ASCAN(This.cFields, sTemp ,-1,-1,-1,1)=0
  					oPart = oMESS.AddMsgPart("%0Manca campo chiave %1")
            oPart.addParam(sTemp)
  			 		* --- Se manca il campo lo aggiungo all'elenco dei campi della griglia
  			 		* --- se lo zoom non si appogia ad una visual query
  			 		IF EMPTY(this.cCpQueryName)
  			 			IF brepair
  			 	 			This.nFields=This.nFields+1
  			 	 			dimension this.cFields[max(1,this.nFields)]
  			 	 			This.cFields[this.nFields]=sTemp
  				  	  this.nColPositions=this.nColPositions+1
  				  	  dimension this.nColWidth[max(1,this.nColPositions)]
  				  	  dimension this.nColOrd[max(1,this.nColPositions)]
  				  	  dimension this.cColTitle[max(1,this.nColPositions)]
  				  	  dimension this.cColFormat[max(1,this.nColPositions)]
  				  	  dimension this.nColRW[max(1,this.nColPositions)]
  				  	  dimension this.nColX[max(1,this.nColPositions)]
  				  	  dimension this.nColY[max(1,this.nColPositions)]
  				  	  dimension this.nColH[max(1,this.nColPositions)]
  				  	  dimension this.cForeColor[max(1,this.nColPositions)]
  				  	  dimension this.cBackColor[max(1,this.nColPositions)]	
     	  	     dimension this.cHyperLink[max(1,this.nColPositions)]      
     	  	     dimension this.cHyperLinkTarget[max(1,this.nColPositions)]      
     	  	     dimension this.bIsImage[max(1,this.nColPositions)]      
      	  	    dimension this.cLayerContent[max(1,this.nColPositions)]      
     	  	     DIMENSION this.nColumnType[max(1,this.nColPositions)]
      	  	    dimension this.cTooltip[max(1,this.nColPositions)]
  			  	  	Dimension This.cLabelFont[max(1,this.nColPositions)]
  			  	  	Dimension This.nDimFont[max(1,this.nColPositions)]
  			  	  	Dimension This.bItalicFont[max(1,this.nColPositions)]
  			  	  	Dimension This.bBoldFont[max(1,this.nColPositions)]
  			  	  	Dimension This.cLabelFontC[max(1,this.nColPositions)]
  			  	  	Dimension This.nDimFontC[max(1,this.nColPositions)]
  		  	  		Dimension This.bItalicFontC[max(1,this.nColPositions)]
  		  	  		Dimension This.bBoldFontC[max(1,this.nColPositions)]
  
  				  		this.nColWidth[this.nColPositions]=50
  				      this.nColOrd[this.nColPositions]=this.nColPositions
  				      this.cColTitle[this.nColPositions]=sTemp
  				  	  this.cColFormat[this.nColPositions]=''
                this.nColRW[this.nColPositions]=0
  				  	  this.nColX[this.nColPositions]=0
  				  	  this.nColY[this.nColPositions]=0
  				  	  this.nColH[this.nColPositions]=0
  				  	  this.cForeColor[this.nColPositions]=''
  				      this.cBackColor[this.nColPositions]=''
     	  	      this.cHyperLink[this.nColPositions]=''
     	  	      this.cHyperLinkTarget[this.nColPositions]=''
     	  	      this.bIsImage[this.nColPositions]=.f.
      	  	    this.cLayerContent[this.nColPositions]=''
     	  	      this.nColumnType[this.nColPositions]=0
      	  	    this.cTooltip[this.nColPositions]=''
                This.cLabelFont[this.nColPositions]=''
                This.nDimFont[this.nColPositions]=0
  			  	  	This.bItalicFont[this.nColPositions]=.f.
  			  	  	This.bBoldFont[this.nColPositions]=.f.
  			  	  	This.cLabelFontC[this.nColPositions]=''
  			  	  	This.nDimFontC[this.nColPositions]=0
  		  	  		This.bItalicFontC[this.nColPositions]=.f.
  		  	  		This.bBoldFontC[this.nColPositions]=.f.
                
  				      oPart = oMESS.AddMsgPart("%1CORRETTO")
                oPart.addParam(" ")
  			 			endif
  			  		ELSE
  						oPart = oMESS.AddMsgPart("%0Verificare query e visual zoom manualmente %1")
              oPart.addParam(this.cCpQueryName)
  			 		Endif
  				endif
  				n_OldPos=n_NewPos+1
  			Endfor
  		endif
  	ELSE
  		* --- Se file non pi� utilizzato per me � OK
  		oMESS.AddMsgPart("Non pi� utilizzato")
  		
  	ENDIF
  	sResult = oMESS.ComposeMessage()
  	Return(sResult)
  ENDFUNC
  
  
   PROCEDURE Save_Zmn(cDir)
     LOCAL x,oXcg,cPath,cFile
     oXcg=createobject('pxcg','save')      
     this.Serialize(oXcg)   
     IF !empty(oXcg.cText)      
      * --- Copio il file in una copia BAK
      cPath=ALLTRIM(substr(this.cFileName,1,RAT('\',this.cFileName)))
      cFile=ALLTRIM(substr(this.cFileName,RAT('\',this.cFileName)+1,LEN(this.cFileNAme)))
      COPY FILE (this.cFileName) TO (cPath+cDir+'\'+cFile+'.bak')
      h=fcreate(this.cFileName)
      fwrite(h,oXcg.cText)
      fclose(h)
     ENDIF
     oXcg=.null.   
   Endproc
   
    proc Serialize(xcg)
       local i,n,i_oc,n,i_nf
      if xcg.bIsStoring
        * --- controllo estenzione zoom: se non trovo _VZM, vuol dire che sto analizzando un .VZM
        IF AT('_VZM',UPPER(this.cFileName))=0
      	  xcg.Save('N',this.nPY)
       	  xcg.Save('N',this.nPX)
       	  xcg.Save('N',this.nWidth)
  		    xcg.Save('N',this.nHeigth)
  		    xcg.Save('C',this.cTitle)
  		    xcg.Save('C',this.cProgram)
  		    xcg.Save('L',this.bFlagOption)
  		    xcg.eoc()
        ENDIF
        xcg.Save('C',this.cSymFile)
        IF UPPER(this.cSymFile)=This.cNoMoreUsed
        	RETURN
        ENDIF      
        xcg.Save('N',this.nFields)
        for i=1 to this.nFields
          xcg.Save('C',this.cFields[i])
        next
        xcg.Save('N',this.nWhere)
        for i=1 to this.nWhere
          xcg.Save('C',trim(this.cWhere[i]))
        next
        xcg.Save('N',this.nOrderBy)
        for i=1 to this.nOrderBy
          xcg.Save('C',this.cOrderBy[i])
        next
        xcg.Save('C',this.cKeyFields)
        xcg.Save('N',this.nColPositions)
        for i=1 to this.nColPositions
          xcg.Save('N',this.nColWidth[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('N',this.nColOrd[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('C',this.cColTitle[i])
        next
        xcg.Save('L',this.bAskParam)
        xcg.Save('N',this.nOrderBy)
        for i=1 to this.nOrderBy
          xcg.Save('L',this.bOrderDesc[i])
        next
        xcg.Save('C',this.cCpQueryName)
        for i=1 to this.nColPositions
          xcg.Save('C',this.cColFormat[i])
        next
        xcg.Save('N',this.i_gy)
        xcg.Save('N',this.i_gx)
        xcg.Save('N',this.i_gH)
        xcg.Save('N',this.i_gW)
        for i=1 to this.nColPositions
          xcg.Save('N',this.nColX[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('N',this.nColY[i])
        next
        xcg.Save('C',this.cSymFile)
        xcg.Save('L',this.bKeepSize)
        xcg.Save('N',this.i_fw)
        xcg.Save('N',this.i_fh)
        for i=1 to this.nColPositions
          xcg.Save('N',this.nColH[i])
        next
        xcg.Save('L',this.bModSQL)
        xcg.Save('C',iif(this.bModSQL,this.cSQL,''))
        for i=1 to this.nColPositions
          xcg.Save('C',this.cForeColor[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('C',this.cBackColor[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('N',this.nColRW[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('C',this.cHyperLink[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('L',this.bIsImage[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('C',this.cLayerContent[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('N',this.nColumnType[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('C',this.cHyperLinkTarget[i])
        next
        for i=1 to this.nColPositions
          xcg.Save('C',this.cTooltip[i])
        next
  			xcg.Save('N',This.nLockColumns-Iif(i_AdvancedHeaderZoom And This.bAddedCheckBox,1,0))
  			* --- gestione font carattere testata			
  			For i=1 To This.nColPositions
  				xcg.Save('C',This.cLabelFont[i])
  			Next
  			For i=1 To This.nColPositions
  				xcg.Save('N',This.nDimFont[i])
  			Next
  			For i=1 To This.nColPositions
  				xcg.Save('L',This.bItalicFont[i])
  			Next
  			For i=1 To This.nColPositions
  				xcg.Save('L',This.bBoldFont[i])
  			Next
  			* --- verifico se ho modificato altezza header e altezza row per salvarle nello zoom
  			If Not This.bDefaultHeightGrid And This.grd.HeaderHeight<>i_nHeaderHeight
  				xcg.Save('N',This.grd.HeaderHeight)
  			Else
  				xcg.Save('N',0)
  			Endif
  			If Not This.bDefaultHeightGrid And This.grd.RowHeight<>i_nRowHeight
  				xcg.Save('N',This.grd.RowHeight)
  			Else
  				xcg.Save('N',0)
  			Endif
  			* --- gestione font carattere colonna			
  			For i=1 To This.nColPositions
  				xcg.Save('C',This.cLabelFontC[i])
  			Next
  			For i=1 To This.nColPositions
  				xcg.Save('N',This.nDimFontC[i])
  			Next
  			For i=1 To This.nColPositions
  				xcg.Save('L',This.bItalicFontC[i])
  			Next
  			For i=1 To This.nColPositions
  				xcg.Save('L',This.bBoldFontC[i])
  			Next			
  			* ---
        xcg.Eoc()
        ELSE
        * --- controllo estenzione zoom: se non trovo _VZM, vuol dire che sto analizzando un .VZM
        IF AT('_VZM',UPPER(this.cFileName))=0
      	  this.nPY=xcg.Load('N',0)
      	  this.nPX=xcg.Load('N',0)
      	  this.nWidth=xcg.Load('N',0)
  		    this.nHeigth=xcg.Load('N',0)
  		    this.cTitle=xcg.Load('C','')
  		    this.cProgram=xcg.Load('C','')
  		    this.bFlagOption=xcg.Load('L',.F.)
  		    xcg.eoc()
  		  ENDIF
        i_oc=this.nFields
        This.cSymFile=xcg.Load('C','',.T.)
        IF UPPER(this.cSymFile)=This.cNoMoreUsed
        	RETURN
        ENDIF
        this.nFields=xcg.Load('N',0,.T.)
        dimension this.cFields[max(1,this.nFields)]
        for i=1 to this.nFields
          this.cFields[i]=xcg.Load('C','',.T.)
        next
        this.nWhere=xcg.Load('N',0,.T.)
        dimension this.cWhere[max(1,this.nWhere)]
        for i=1 to this.nWhere
          this.cWhere[i]=xcg.Load('C','',.T.)
        next
        this.nOrderBy=xcg.Load('N',0,.T.)
        dimension this.cOrderBy[max(1,this.nOrderBy)]
        for i=1 to this.nOrderBy
          this.cOrderBy[i]=xcg.Load('C','',.T.)
        next
        this.cKeyFields=xcg.Load('C','',.T.)
        this.nColPositions=xcg.Load('N',0,.T.)
        dimension this.nColWidth[max(1,this.nColPositions)]
        dimension this.nColOrd[max(1,this.nColPositions)]
        dimension this.cColTitle[max(1,this.nColPositions)]
        dimension this.cColFormat[max(1,this.nColPositions)]
        dimension this.nColRW[max(1,this.nColPositions)]
        dimension this.nColX[max(1,this.nColPositions)]
        dimension this.nColY[max(1,this.nColPositions)]
        dimension this.nColH[max(1,this.nColPositions)]
        dimension this.cForeColor[max(1,this.nColPositions)]
        dimension this.cBackColor[max(1,this.nColPositions)]
        dimension this.cHyperLink[max(1,this.nColPositions)]      
        dimension this.cHyperLinkTarget[max(1,this.nColPositions)]      
        dimension this.bIsImage[max(1,this.nColPositions)]      
        dimension this.cLayerContent[max(1,this.nColPositions)]      
        DIMENSION this.nColumnType[max(1,this.nColPositions)]
        dimension this.cTooltip[max(1,this.nColPositions)]
  			Dimension This.cLabelFont[max(1,this.nColPositions)]
  			Dimension This.nDimFont[max(1,this.nColPositions)]
  			Dimension This.bItalicFont[max(1,this.nColPositions)]
  			Dimension This.bBoldFont[max(1,this.nColPositions)]
  			Dimension This.cLabelFontC[max(1,this.nColPositions)]
  			Dimension This.nDimFontC[max(1,this.nColPositions)]
  			Dimension This.bItalicFontC[max(1,this.nColPositions)]
  			Dimension This.bBoldFontC[max(1,this.nColPositions)]
        for i=1 to this.nColPositions
          this.nColWidth[i]=xcg.Load('N',0)
        next
        for i=1 to this.nColPositions
          this.nColOrd[i]=xcg.Load('N',0)
        next
        for i=1 to this.nColPositions
          this.cColTitle[i]=xcg.Load('C','')
        next
        this.bAskParam=xcg.Load('L',.f.)
        this.nOrderBy=xcg.Load('N',0)
        dimension this.bOrderDesc[max(1,this.nOrderBy)]
        for i=1 to this.nOrderBy
          this.bOrderDesc[i]=xcg.Load('L',.f.)
        next
        this.cCpQueryName=xcg.Load('C','')
        for i=1 to this.nColPositions
          this.cColFormat[i]=xcg.Load('C','')
        next
        this.i_gy=xcg.Load('N',0)
        this.i_gx=xcg.Load('N',0)
        this.i_gh=xcg.Load('N',0)
        this.i_gw=xcg.Load('N',0)
        if this.i_gw<>0 and this.i_gh<>0
  *        this.grd.top=this.i_gy
  *        this.grd.left=this.i_gx
          this.nGrdH=this.i_gh
          this.nGrdW=this.i_gw
        endif
        for i=1 to this.nColPositions
          this.nColX[i]=xcg.Load('N',0)
        next
        for i=1 to this.nColPositions
          this.nColY[i]=xcg.Load('N',0)
        next
        this.cSymFile=xcg.Load('C',this.cSymFile)
        this.bKeepSize=xcg.Load('L',.f.)
        this.i_fw=xcg.Load('N',0)
        this.i_fh=xcg.Load('N',0)
        for i=1 to this.nColPositions
          this.nColH[i]=xcg.Load('N',0)
        next
        this.bModSQL=xcg.Load('L',.f.)
        this.cSQL=xcg.Load('C','')
        for i=1 to this.nColPositions
          this.cForeColor[i]=xcg.Load('C','')
        next
        for i=1 to this.nColPositions
          this.cBackColor[i]=xcg.Load('C','')
        next
        for i=1 to this.nColPositions
          this.nColRW[i]=xcg.Load('N',0)
        next
        for i=1 to this.nColPositions
          this.cHyperLink[i]=xcg.Load('C','')
        next
        for i=1 to this.nColPositions
          this.bIsImage[i]=xcg.Load('L',.f.)
        next
        for i=1 to this.nColPositions
          this.cLayerContent[i]=xcg.Load('C','')
        next
        for i=1 to this.nColPositions
          this.nColumnType[i]=xcg.Load('N',0)
        next
        for i=1 to this.nColPositions
          this.cHyperLinkTarget[i]=xcg.Load('C','')
        next
        for i=1 to this.nColPositions
          this.cTooltip[i]=xcg.Load('C','')
        next
        if not xcg.Eoc(.T.)
  		  	This.nLockColumns=xcg.Load('N',0,.T.)+Iif(i_AdvancedHeaderZoom And This.bAddedCheckBox,1,0)
  		  	* --- gestione font carattere di testata
  		  	For i=1 To This.nColPositions
  		  		This.cLabelFont[i]=xcg.Load('C','',.T.)
  		  	Next
  		  	For i=1 To This.nColPositions
  		  		This.nDimFont[i]=xcg.Load('N',0,.T.)
  			  Next
  			  For i=1 To This.nColPositions
  			  	This.bItalicFont[i]=xcg.Load('L',.F.,.T.)
  		  	Next
  		  	For i=1 To This.nColPositions
  			  	This.bBoldFont[i]=xcg.Load('L',.F.,.T.)
  		  	Next
  		  	* --- verifico se ho altezza header e altezza row specifiche per lo zoom
  		  	Local nHeaderHeight, nRowHeight
  		  	This.bDefaultHeightGrid=.T.
  		  	nHeaderHeight=xcg.Load('N',0,.T.)
  		  	If nHeaderHeight<>0
  		  		This.grd.HeaderHeight=nHeaderHeight
  		  		This.bDefaultHeightGrid=.F.
  		  	Endif
  	  		nRowHeight=xcg.Load('N',0,.T.)
  		  	If nRowHeight<>0
  		  		This.grd.RowHeight=nRowHeight
  		  		This.bDefaultHeightGrid=.F.
  		  	Endif
  		  	* --- gestione font carattere di colonna
  		  	For i=1 To This.nColPositions
  		  		This.cLabelFontC[i]=xcg.Load('C','',.T.)
  		  	Next
  		  	For i=1 To This.nColPositions
  		  		This.nDimFontC[i]=xcg.Load('N',0,.T.)
  		  	Next
  		  	For i=1 To This.nColPositions
  		  		This.bItalicFontC[i]=xcg.Load('L',.F.,.T.)
  		  	Next
  		  	For i=1 To This.nColPositions
  		  		This.bBoldFontC[i]=xcg.Load('L',.F.,.T.)
  		  	Next
  			  * ---
          xcg.Eoc()
        endif
        * --- altre inizializzazioni
      endif
  ENDPROC
  
  * --- NON OCCORRE METTERE UNA ENDDEFINE PERCHE' GIA' PRESENTE NEL BATCH 
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
