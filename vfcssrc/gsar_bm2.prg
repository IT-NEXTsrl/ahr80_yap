* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bm2                                                        *
*              Manutenzione attributi - creazione TMPATTRI                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-07                                                      *
* Last revis.: 2010-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CACODMOD,w_CACODGRU,w_CACODFAM,w_AUTOMA,w_FILTER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bm2",oParentObject,m.w_CACODMOD,m.w_CACODGRU,m.w_CACODFAM,m.w_AUTOMA,m.w_FILTER)
return(i_retval)

define class tgsar_bm2 as StdBatch
  * --- Local variables
  w_CACODMOD = space(20)
  w_CACODGRU = space(10)
  w_CACODFAM = space(10)
  w_AUTOMA = space(1)
  w_FILTER = space(254)
  w_MAAUTOMA = space(1)
  w_FRTIPOLO = space(1)
  w_FR_TABLE = space(30)
  w_FR_CAMPO = space(50)
  w_FRCAMCOL = space(50)
  w_GRCODICE = space(10)
  w_FRCODICE = space(10)
  w_FIELDVALUE = space(20)
  w_OQRY = .NULL.
  * --- WorkFile variables
  TMPATTRI_idx=0
  GSAR_BM2_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruisce la tabella temporanea adibita a contenere i valori degli attributi derivanti da famiglia di tipologia 'Fisso da archivio' oppure 'Misto da archivio'.
    * --- Il risultato dell'elaborazione sar� accodato alla query QUERY\GSAR_KAS che viene utilizzata dallo zoom QUERY\GSAR_KMA.VZM che contiene i valori degli attributi
    *     derivanti da famiglia di tipologia 'Fisso da elenco' oppure 'Misto da elenco'.
    * --- La tabella temporanea TMPATTRI viene creata con la stessa struttura della query GSAR_KAS a parte per i filtri
    * --- Creazione
    * --- Create temporary table TMPATTRI
    i_nIdx=cp_AddTableDef('TMPATTRI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPATTRI_proto';
          )
    this.TMPATTRI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Popolamento
    * --- Select from QUERY\GSAR_KA1
    do vq_exec with 'QUERY\GSAR_KA1',this,'_Curs_QUERY_GSAR_KA1','',.f.,.t.
    if used('_Curs_QUERY_GSAR_KA1')
      select _Curs_QUERY_GSAR_KA1
      locate for 1=1
      do while not(eof())
      * --- Cicla sulle famiglie e per ogni famiglia di tipologia 
      *     'Fisso da archivio' (FRTIPOLO='A')
      *     'Misto da archivio' (FRTIPOLO='B')
      *     esegue l'interrogazione sull'archivio stesso per ogni record ottenuto inserisce
      *     il valore del campo FR_CAMPO neI campi CAVALATT e CAVALAPP
      this.w_MAAUTOMA = MAAUTOMA
      this.w_FRTIPOLO = FRTIPOLO
      this.w_FR_TABLE = FR_TABLE
      this.w_FR_CAMPO = FR_CAMPO
      this.w_FRCAMCOL = FRCAMCOL
      this.w_GRCODICE = GRCODICE
      this.w_FRCODICE = FRCODICE
      if this.w_FRTIPOLO = "A" OR this.w_FRTIPOLO = "B"
        * --- Esegue la query sull'archivio richiesto
        this.w_OQRY=createobject("cpquery")
        * --- Inserisce la tabella
        this.w_OQRY.mLoadFile(this.w_FR_TABLE , this.w_FR_TABLE , this.w_FR_TABLE)     
        * --- Inserisce il campo
        this.w_OQRY.mLoadField(this.w_FR_CAMPO , this.w_FR_CAMPO)     
        * --- Inserisce il filtro
        if NOT EMPTY( this.w_FILTER )
          this.w_OQRY.mLoadWhere(this.w_FILTER)     
        endif
        * --- Esegue la query
        this.w_OQRY.mDoQuery("CursQUERY","Exec",.f.,.f.,.f.,.f.,.t.)     
        * --- Scorre il risultato e inserisce i record nel cursore temporaneo
        L_OldArea = Select()
        if Used( "CursQuery" )
          do while NOT EOF()
            Select CursQuery
            GO TOP
            SCAN
            L_FIELDNAME = this.w_FR_CAMPO
            this.w_FIELDVALUE = &L_FIELDNAME
            this.w_FIELDVALUE = LEFT(this.w_FIELDVALUE,20)
            * --- Insert into TMPATTRI
            i_nConn=i_TableProp[this.TMPATTRI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPATTRI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPATTRI_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CACODGRU"+",CACODFAM"+",CAVALATT"+",MAAUTOMA"+",CAVALAPP"+",MACODICE"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_GRCODICE),'TMPATTRI','CACODGRU');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FRCODICE),'TMPATTRI','CACODFAM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FIELDVALUE),'TMPATTRI','CAVALATT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAAUTOMA),'TMPATTRI','MAAUTOMA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FIELDVALUE),'TMPATTRI','CAVALAPP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CACODMOD ),'TMPATTRI','MACODICE');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CACODGRU',this.w_GRCODICE,'CACODFAM',this.w_FRCODICE,'CAVALATT',this.w_FIELDVALUE,'MAAUTOMA',this.w_MAAUTOMA,'CAVALAPP',this.w_FIELDVALUE,'MACODICE',this.w_CACODMOD )
              insert into (i_cTable) (CACODGRU,CACODFAM,CAVALATT,MAAUTOMA,CAVALAPP,MACODICE &i_ccchkf. );
                 values (;
                   this.w_GRCODICE;
                   ,this.w_FRCODICE;
                   ,this.w_FIELDVALUE;
                   ,this.w_MAAUTOMA;
                   ,this.w_FIELDVALUE;
                   ,this.w_CACODMOD ;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            ENDSCAN
          enddo
          Select CursQuery
          Use
        endif
        Select( L_OldArea )
      endif
        select _Curs_QUERY_GSAR_KA1
        continue
      enddo
      use
    endif
    * --- Create temporary table GSAR_BM2
    i_nIdx=cp_AddTableDef('GSAR_BM2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TMPATTRI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TMPATTRI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.GSAR_BM2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Eliminazione
    * --- Drop temporary table TMPATTRI
    i_nIdx=cp_GetTableDefIdx('TMPATTRI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPATTRI')
    endif
  endproc


  proc Init(oParentObject,w_CACODMOD,w_CACODGRU,w_CACODFAM,w_AUTOMA,w_FILTER)
    this.w_CACODMOD=w_CACODMOD
    this.w_CACODGRU=w_CACODGRU
    this.w_CACODFAM=w_CACODFAM
    this.w_AUTOMA=w_AUTOMA
    this.w_FILTER=w_FILTER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMPATTRI'
    this.cWorkTables[2]='*GSAR_BM2'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_QUERY_GSAR_KA1')
      use in _Curs_QUERY_GSAR_KA1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CACODMOD,w_CACODGRU,w_CACODFAM,w_AUTOMA,w_FILTER"
endproc
