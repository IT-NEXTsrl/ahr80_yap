* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chkalleg_async                                                  *
*              chkalleg asincrona                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-01-11                                                      *
* Last revis.: 2016-01-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pArchivio,pChiave,pTaskId
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tchkalleg_async",oParentObject,m.pArchivio,m.pChiave,m.pTaskId)
return(i_retval)

define class tchkalleg_async as StdBatch
  * --- Local variables
  pArchivio = space(10)
  pChiave = space(20)
  pTaskId = space(10)
  w_nAlleg = 0
  * --- WorkFile variables
  ASYNTASK_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiama la chkalleg per testare il numero di allegati
    *     e ne salva il risultato nella tabella di confine
    this.pTaskId = Alltrim(this.pTaskId)
    * --- Write into ASYNTASK
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ASYNTASK_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASYNTASK_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ASYNTASK_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AT_DTEXE ="+cp_NullLink(cp_ToStrODBC(DateTime()),'ASYNTASK','AT_DTEXE');
          +i_ccchkf ;
      +" where ";
          +"ATCODICE = "+cp_ToStrODBC(this.pTaskId);
             )
    else
      update (i_cTable) set;
          AT_DTEXE = DateTime();
          &i_ccchkf. ;
       where;
          ATCODICE = this.pTaskId;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_nAlleg = chkalleg(this.pArchivio, this.pChiave)
    * --- Write into ASYNTASK
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ASYNTASK_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASYNTASK_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ASYNTASK_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AT_DTEND ="+cp_NullLink(cp_ToStrODBC(DateTime()),'ASYNTASK','AT_DTEND');
      +",AT_STATO ="+cp_NullLink(cp_ToStrODBC("S"),'ASYNTASK','AT_STATO');
      +",ATRETURN ="+cp_NullLink(cp_ToStrODBC(Tran(this.w_nAlleg)),'ASYNTASK','ATRETURN');
          +i_ccchkf ;
      +" where ";
          +"ATCODICE = "+cp_ToStrODBC(this.pTaskId);
             )
    else
      update (i_cTable) set;
          AT_DTEND = DateTime();
          ,AT_STATO = "S";
          ,ATRETURN = Tran(this.w_nAlleg);
          &i_ccchkf. ;
       where;
          ATCODICE = this.pTaskId;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,pArchivio,pChiave,pTaskId)
    this.pArchivio=pArchivio
    this.pChiave=pChiave
    this.pTaskId=pTaskId
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ASYNTASK'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pArchivio,pChiave,pTaskId"
endproc
