* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bsd                                                        *
*              Stampa dati procedurali e confronto                             *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_192]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-03                                                      *
* Last revis.: 2016-04-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bsd",oParentObject,m.pTIPO)
return(i_retval)

define class tgsma_bsd as StdBatch
  * --- Local variables
  pTIPO = space(1)
  w_oERRORLOG = .NULL.
  w_PUNPAD = .NULL.
  w_MESS = space(10)
  w_CODRIL = space(10)
  w_DATRIL = ctod("  /  /  ")
  w_DATASTAM = ctod("  /  /  ")
  w_MGUBIC = space(1)
  w_FLLOTT = space(1)
  w_GESMAT = space(1)
  w_DRARTIC = space(20)
  w_CODMAG = space(5)
  w_SLCODMAG = space(5)
  w_DRSERIAL = space(15)
  w_CODLOT = space(20)
  w_QTAESI = 0
  w_CODUBI = space(20)
  w_CODUBF = space(20)
  w_DRKEYSAL = space(40)
  w_KEYULO = space(40)
  w_CODMAT = space(20)
  w_DRCODRIC = space(41)
  w_CODART = space(20)
  w_DRUNIMIS = space(3)
  w_CODVAR = space(20)
  w_DESART = space(40)
  w_CODINV = space(6)
  w_CODESE = space(4)
  w_VALORIZ = space(1)
  w_DATINV = ctod("  /  /  ")
  w_VALNAZ = space(3)
  w_VADECTOT = 0
  w_VADECUNI = 0
  w_CODART1 = space(20)
  w_TIPESI = space(1)
  w_CODFAM = space(5)
  w_CODGRU = space(5)
  w_CODCAT = space(5)
  w_CODMAR = space(5)
  w_CLAMAT = space(5)
  w_CODFOR = space(15)
  w_PROINI = 0
  w_PROFIN = 0
  w_MOVINI = ctod("  /  /  ")
  w_ESCLOBSO = space(1)
  w_ARDTOBSO = ctod("  /  /  ")
  w_DATARIL = ctod("  /  /  ")
  w_PROCESS = space(1)
  w_TIPCON = space(1)
  w_KEYLOTTO = space(20)
  w_FLGRIL = space(1)
  w_FLCOMM = space(1)
  w_CODCOM = space(10)
  w_UBICAZ = space(1)
  w_ESISTECOM = .f.
  w_ARFLCOMM = space(1)
  w_COMMDEFA = space(15)
  w_VQRGESMAT = space(1)
  w_ZOOM = space(10)
  w_NUMDOC = 0
  w_OKREC = .f.
  w_APPO = space(250)
  w_FLFRAZ = space(1)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_DESART = space(40)
  w_DRCODCOM = space(15)
  * --- WorkFile variables
  ART_ICOL_idx=0
  MAGAZZIN_idx=0
  RILEVAZI_idx=0
  UNIMIS_idx=0
  KEY_ARTI_idx=0
  TMPVEND3_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Dati Procedurali , Stampa Confronto dati e Stampa Dati a Magazzino non Rilevati
    *     Generazione Dati Rilevati
    if UPPER (this.oparentobject.class) ="TGSMA_KIR"
      this.w_FLGRIL = this.oparentobject.w_FLGRIL
    else
      this.w_FLGRIL = "E"
    endif
    * --- ATTENZIONE: le stesse query sono utilizzate anche nel batch GSMA_BDR
    this.w_PUNPAD = this.oParentObject
    ah_Msg("Elaborazione dati...")
    * --- Queries per reperire i dati procedurali (Inventario Fisico)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    L_CODCOM=this.w_CODCOM
    * --- Parametro per decidere se gestire gli articoli a matricole a parte o meno.
    *     Utilizzato se la dta rilevazione � prima o dopo la data inizio matricole
    this.w_VQRGESMAT = IIF ( this.w_DATASTAM>=Nvl( g_DATMAT, cp_CharToDate( "  /  /    " )) , "S", " " )
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      * --- Variabile che indica se esiste almeno un articolo gestito a Commessa personaliizata
      this.w_ESISTECOM = .F.
      * --- Controllo se esiste almeno un articolo gestito a Commessa personaliizata
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARFLCOMM = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              ARFLCOMM = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ESISTECOM = i_Rows > 0
      * --- Mi serve per filtrare gli articoli gestiti a commessa nel caso sia piena la variabile w_CODCOM commessa
      this.w_ARFLCOMM = SPACE(1)
      * --- Se � valorizzata la commessa filtro solo gli articoli gestiti a commessa personalizzata oppure perso+nettificabile
      *     Passo la variabile w_ARFLCOMM come filtro alle query
      this.w_ARFLCOMM = IIF(this.w_ESISTECOM And !Empty(this.w_CODCOM), "S" , SPACE(1))
    else
      this.w_ESISTECOM = .F.
      * --- Controllo se esiste almeno un articolo gestito a Commessa
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARSALCOM = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              ARSALCOM = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ESISTECOM = i_Rows > 0
      this.w_ARFLCOMM = IIF(this.w_ESISTECOM, "S" , SPACE(1))
      if this.w_ESISTECOM
        * --- Leggo la commessa di default dai parametri
        * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
        this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
      endif
    endif
    * --- Creo una tabella temporanea con gli articoli corretti in base ai filtri di selezione
    * --- Create temporary table TMPVEND3
    i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSMA9SDP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if g_APPLICATION <> "ADHOC REVOLUTION" And !EMPTY(this.w_CODUBI) 
      * --- La variabile w_ESISTECOM indica se � presente almeno un articolo gestito a commessa
      *     Se non � presente almeno un articolo gestito a commessa allora li tratto tutti come non gestiti a commessa
      if this.w_ESISTECOM
        if !Empty(this.w_CODCOM)
          * --- Calcolo dato Procedurale  con ubicazioni e lotti e commessa
          vq_exec("query\GSMA4RELU",this,"UbilotC")
          vq_exec("query\GSMA1RELU",this,"UbilotN")
          Select * from UbilotC union all select * from UbilotN into cursor Ubilot readwrite
        else
          * --- Calcolo dato Procedurale  con ubicazioni e lotti e commessa
          vq_exec("query\GSMA4RELU",this,"UbilotC")
          vq_exec("query\GSMASRELUC",this,"UbilotL")
          * --- Calcolo dato Procedurale  con ubicazioni e lotti
          vq_exec("query\GSMA_RELU",this,"UbilotN")
          Select * from UbilotN union all select * from UbilotC union all select * from UbilotL into cursor Ubilot readwrite
          * --- Per il resto dell'elaborazione serve solo il cursore Ubilot, elimino gli altri due
          use in select("UbilotN")
          use in select("UbilotC")
          use in select("UbilotL")
        endif
      else
        * --- Calcolo dato Procedurale  con ubicazioni e lotti
        vq_exec("query\GSMA_RELU",this,"Ubilot")
      endif
    else
      * --- Calcolo dato Procedurale  senza ubicazioni 
      * --- Query relativa ad Articoli non gestiti a Lotti
      if this.w_ESISTECOM
        if !Empty(this.w_CODCOM) or UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE"
          * --- Estrazione saldi per commessa
          vq_exec("query\GSMA_SDPC",this,"Ubilot1C")
          * --- Estrazione movimenti
          vq_exec("query\GSMA_SDP",this,"Ubilot1N")
          Select * from Ubilot1N union all select * from Ubilot1C into cursor Ubilot1 readwrite
        else
          vq_exec("query\GSMA_SDPC",this,"Ubilot1C")
          vq_exec("query\GSMASSDPC",this,"Ubilot1L")
          vq_exec("query\GSMA_SDP",this,"Ubilot1N")
          Select * from Ubilot1N union all select * from Ubilot1C union all select * from Ubilot1L into cursor Ubilot1 readwrite
          * --- Per il resto dell'elaborazione serve solo il cursore Ubilot1, elimino gli altri due
        endif
        use in select("Ubilot1N")
        use in select("Ubilot1C")
        use in select("Ubilot1L")
      else
        vq_exec("query\GSMA_SDP",this,"Ubilot1")
      endif
      * --- Query relativa ad Articoli gestiti a Lotti
      if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
        if this.w_ESISTECOM
          if !Empty(this.w_CODCOM)
            * --- Calcolo dato Procedurale  con ubicazioni e lotti e commessa
            vq_exec("query\GSMA4RELU",this,"Ubilot2C")
            vq_exec("query\GSMA1RELU",this,"Ubilot2N")
            Select * from Ubilot2C union all select * from Ubilot2N into cursor Ubilot2 readwrite
            use in select("Ubilot2N")
            use in select("Ubilot2C")
          else
            * --- Se esiste almenu un articolo gestito a commessa eseguo entrambe le query
            * --- Calcolo dato Procedurale  con ubicazioni e lotti e commessa
            vq_exec("query\GSMA4RELU",this,"Ubilot2C")
            vq_exec("query\GSMASRELUC",this,"Ubilot2L")
            * --- Calcolo dato Procedurale  con ubicazioni e lotti
            vq_exec("query\GSMA_RELU",this,"Ubilot2N")
            * --- Devo sottrarre dal saldo per lotti/ubicazione quello per commessa 
            *     in modo da  ricavare il saldo non abbinato a commessa
            * --- Per gli articoli gestiti a commessa devo calcolare la parte non abbinata a commessa
            Select * from Ubilot2N union all select * from Ubilot2C union all select * from Ubilot2L into cursor Ubilot2 readwrite
            use in select("Ubilot2N")
            use in select("Ubilot2C")
            use in select("Ubilot2L")
          endif
        else
          vq_exec("query\GSMA_RELU",this,"Ubilot2")
        endif
        select SLCODICE,CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,ARUNMIS1,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 SLCODMAG,space(20) as MLCODUBI, space(20) as MLCODLOT, space(20) as KEYLOTTO, CODCOM, FLLOTT, MGUBIC, GESMAT, GESCOM, DESART from Ubilot1; 
 union all; 
 Select SLCODICE, CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,ARUNMIS1,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 SLCODMAG,NVL(MLCODUBI,SPACE(20)) AS MLCODUBI,NVL(MLCODLOT,SPACE(20)) AS MLCODLOT,NVL(KEYLOTTO,SPACE(20)) AS KEYLOTTO, CODCOM ,; 
 FLLOTT, MGUBIC, GESMAT, GESCOM, DESART from Ubilot2; 
 into cursor Ubilot
      else
        vq_exec("query\GSMA4SDP",this,"Ubilot2")
        select SLCODICE,CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,ARUNMIS1,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 SLCODMAG,space(20) as MLCODUBI, space(20) as MLCODLOT, codcom from Ubilot1; 
 union all; 
 Select SLCODICE, CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,ARUNMIS1,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 SLCODMAG,NVL(MLCODUBI,SPACE(20)) AS MLCODUBI,NVL(MLCODLOT,SPACE(20)) AS MLCODLOT, codcom from Ubilot2; 
 into cursor Ubilot
      endif
    endif
    * --- Gestione Matricole
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE" AND this.w_ESISTECOM
      if !Empty(this.w_CODCOM)
        vq_exec("query\GSMAMSDPC",this,"Matr")
      else
        vq_exec("query\GSMAMSDP",this,"MatrN")
        vq_exec("query\GSMAMSDPC",this,"MatrC")
        * --- Per gli articoli gestiti a commessa devo calcolare la parte non abbinata a commessa
        select SUM(slqtaper) as slqtaper , SUM(ESISPRIN) AS ESISPRIN, SUM(ESISCOLL) AS ESISCOLL, slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot, mtcodmat ; 
 FROM MatrC INTO CURSOR MatrSC GROUP BY slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot, mtcodmat
        * --- Per gli articoli gestiti a commessa devo calcolare la parte non abbinata a commessa
         SELECT MatrN.SLCODICE, MatrN.CACODART, MatrN.CACODVAR, MatrN.MLCODUBI, MatrN.MLCODLOT,MatrN.mtcodmat,; 
 MatrN.SLCODMAG, MatrN.SLQTAPER - NVL(MatrSC.SLQTAPER,0) as SLQTAPER, MatrN.ARUNMIS1, MatrN.EsisPrin, ; 
 MatrN.EsisColl, MatrN.KEYLOTTO, MatrN.CODCOM, MatrN.FLLOTT, MatrN.MGUBIC, MatrN.GESMAT, MatrN.GESCOM, ; 
 MatrN.DESART FROM MatrN left outer join MatrSC on MatrN.SLCODMAG=MatrSC.SLCODMAG AND; 
 MatrN.CACODART=MatrSC.CACODART AND MatrN.CACODVAR=MatrSC.CACODVAR ; 
 AND MatrN.MLCODUBI=MatrSC.MLCODUBI AND MatrN.MLCODLOT=MatrSC.MLCODLOT AND; 
 MatrN.MTCODMAT=MatrSC.MTCODMAT into cursor MatrSN
        use in select("MatrN")
        use in select("MatrSC")
        Select * from MatrSN union all select * from MatrC into cursor Matr readwrite
        * --- Per il resto dell'elaborazione serve solo il cursore Ubilot1, elimino gli altri due
        use in select("MatrSN")
        use in select("MatrC")
      endif
    else
      vq_exec("query\GSMAMSDP1",this,"Matr")
    endif
    * --- Ragguppo i dati procedurali per Articolo, Variante,Ubicazione, Lotto, commessa
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      Select slcodice,cacodart,nvl(cacodvar,space(20)) as cacodvar,slcodmag,mlcodubi,nvl(mlcodlot, space(20)) as mlcodlot,; 
 space(40) as mtcodmat, arunmis1,slqtaper as EsisAttu, sum(EsiCarPri - EsiScaPri) as EsisPrin,; 
 fllott, mgubic, gesmat,gescom,sum(EsiCarCol - EsiScaCol) as EsisColl, (0*Slqtaper) as Procedur, desart,nvl(keylotto, ; 
 space(20)) as keylotto, codcom ; 
 from Ubilot group by slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot, codcom ; 
 Union all; 
 Select slcodice,cacodart, cacodvar,slcodmag,mlcodubi,mlcodlot,mtcodmat,arunmis1,slqtaper as EsisAttu,EsisPrin,; 
 fllott, mgubic,gesmat,gescom,0 as EsisColl,0 as Procedur, desart, keylotto, codcom from Matr; 
 into Cursor AppUbilot 
    else
      Select slcodice,cacodart,nvl(cacodvar,space(20)) as cacodvar,slcodmag,mlcodubi,nvl(mlcodlot, space(20)) as mlcodlot,; 
 space(40) as mtcodmat, arunmis1,slqtaper as EsisAttu, sum(EsiCarPri - EsiScaPri) as EsisPrin,; 
 space(1) as fllott, space(1) as mgubic, space(1) as gesmat,sum(EsiCarCol - EsiScaCol) as EsisColl, (0*Slqtaper) as Procedur, space(40) as desart, codcom; 
 from Ubilot group by slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot, codcom; 
 Union all; 
 Select slcodice,cacodart, cacodvar,slcodmag,mlcodubi,mlcodlot,mtcodmat,arunmis1,slqtaper as EsisAttu,EsisPrin,; 
 space(1) as fllott, space(1) as mgubic,space(1) as gesmat,0 as EsisColl,0 as Procedur, space(40) as desart, codcom from Matr; 
 into Cursor AppUbilot 
    endif
    =WRCURSOR("AppUbilot")
    GO TOP
    SCAN
    this.w_DRARTIC = CACODART
    this.w_SLCODMAG = SLCODMAG
    this.w_FLLOTT = " "
    if UPPER(g_APPLICATION)="ADHOC REVOLUTION"
      * --- Verifico se l'Articolo � gestito a lotti o a Matrcole
      * --- Leggo la data di obsolescenza. Se richiesto ( w_ESCLOBSO = "E" ) 
      *     gli articoli obsoleti con dato procedurale a zeo saranno eliminati.
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARFLLOTT,ARGESMAT,ARDESART,ARDTOBSO"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_DRARTIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARFLLOTT,ARGESMAT,ARDESART,ARDTOBSO;
          from (i_cTable) where;
              ARCODART = this.w_DRARTIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
        this.w_GESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
        this.w_DESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
        this.w_ARDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Verifico se il Magazzino � gestito ad ubicazioni
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGFLUBIC"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_SLCODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGFLUBIC;
          from (i_cTable) where;
              MGCODMAG = this.w_SLCODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MGUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT AppUbilot
      * --- Calcolo del dato procedurale
      REPLACE GESMAT WITH this.w_GESMAT
      REPLACE FLLOTT WITH this.w_FLLOTT
      REPLACE DESART WITH this.w_DESART
      REPLACE MGUBIC WITH this.w_MGUBIC
    else
      * --- Leggo la data di obsolescenza. Se richiesto ( w_ESCLOBSO = "E" ) 
      *     gli articoli obsoleti con dato procedurale a zeo saranno eliminati.
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARDTOBSO"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_DRARTIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARDTOBSO;
          from (i_cTable) where;
              ARCODART = this.w_DRARTIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ARDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT AppUbilot
      * --- Calcolo del dato procedurale
      this.w_GESMAT = GESMAT
      this.w_FLLOTT = FLLOTT
      this.w_MGUBIC = IIF( NVL(MGUBIC , SPACE(1))<>"S" , SPACE(1) , "S")
    endif
    * --- ----------  Trovare un espressione unica che sostituisca il costrutto IF .. else
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      if this.w_VQRGESMAT<>"S" OR this.w_GESMAT<>"S"
        if (this.w_MGUBIC = "S" or this.w_FLLOTT = "S") And empty(nvl(MLCODLOT," ")) and empty(nvl(MLCODUBI," "))
          REPLACE Procedur WITH 0
        else
          REPLACE Procedur WITH (EsisAttu - EsisPrin - EsisColl) 
        endif
      else
        if empty(nvl(MTCODMAT," "))
          REPLACE Procedur WITH 0
        else
          REPLACE Procedur WITH EsisPrin
        endif
      endif
    else
      if this.w_VQRGESMAT<>"S" OR this.w_GESMAT<>"S"
        if this.w_MGUBIC = "S" Or this.w_FLLOTT $ "S-C"
          if empty(nvl(MLCODLOT," ")) and empty(nvl(MLCODUBI," "))
            REPLACE Procedur WITH 0
          else
            REPLACE Procedur WITH (EsisPrin + EsisColl) 
          endif
        else
          REPLACE Procedur WITH (EsisAttu - EsisPrin - EsisColl) 
        endif
      else
        if empty(nvl(MTCODMAT," "))
          REPLACE Procedur WITH 0
        else
          REPLACE Procedur WITH EsisPrin
        endif
      endif
    endif
    if PROCEDUR = 0 AND this.w_ESCLOBSO = "E" AND CP_TODATE( this.w_ARDTOBSO ) <= this.w_DATARIL AND !EMPTY( this.w_ARDTOBSO )
      * --- Sbianca gli articoli da eliminare.
      *     Saranno eliminati con una delete fuori dal ciclo SCAN
      REPLACE CACODART WITH SPACE( 20 )
    endif
    if empty(nvl(MLCODUBI," ")) AND this.w_MGUBIC = "S" 
      * --- Sbianca gli articoli da eliminare.
      *     Saranno eliminati con una delete fuori dal ciclo SCAN
      REPLACE CACODART WITH SPACE( 20 )
    endif
    ENDSCAN
    DELETE FROM AppUbilot WHERE EMPTY( CACODART )
    * --- Variabili per il Report
    L_CODRIL=this.w_CODRIL
    L_CODMAG=this.w_CODMAG
    L_DATASTAM=this.w_DATASTAM
    do case
      case this.pTIPO="P"
        * --- Stampa Dati procedurali
        L_CODUBI=this.w_CODUBI
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          this.w_TIPESI = 1
        endif
        do case
          case this.w_TIPESI=1
            * --- Solo esistenza <> 0
            if UPPER(g_APPLICATION)="AD HOC ENTERPRISE" and this.w_ESISTECOM
              * --- Ho almeno un articolo gestito a commessa personalizzato
              SELECT * from AppUbilot where nvl(procedur,0)<>0 into Cursor __TMP__ order by slcodmag,cacodart, cacodvar, mlcodubi, codcom, mlcodlot,mtcodmat
            else
              SELECT * from AppUbilot where nvl(procedur,0)<>0 into Cursor __TMP__ order by slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot,mtcodmat
            endif
          case this.w_TIPESI=2
            * --- Tutti
            SELECT * from AppUbilot into Cursor __TMP__ order by slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot,mtcodmat
          case this.w_TIPESI=3
            * --- Nell'intervallo
            SELECT * from AppUbilot where nvl(procedur,0)>=this.w_PROINI And nvl(procedur,0)<=this.w_PROFIN into Cursor __TMP__ order by slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot,mtcodmat
        endcase
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE" and this.w_ESISTECOM
          * --- Stampa con commessa nel caso c'� almeno un articolo gestito a commessa
          cp_chprn("QUERY\gsma3sdr.FRX", " ", this)
        else
          cp_chprn("QUERY\gsma_sdr.FRX", " ", this)
        endif
      case this.pTIPO="C"
        * --- Stampa Confronto dati
        this.w_CODINV = this.w_PUNPAD.w_CODINV
        this.w_CODESE = this.w_PUNPAD.w_CODESE
        this.w_DATINV = this.w_PUNPAD.w_DATINV
        this.w_VALORIZ = this.w_PUNPAD.w_VALORIZ
        this.w_VALNAZ = this.w_PUNPAD.w_VALNAZ
        this.w_VADECTOT = this.w_PUNPAD.w_VADECTOT
        this.w_VADECUNI = this.w_PUNPAD.w_VADECUNI
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          this.w_PROCESS = this.w_PUNPAD.w_PROCESS
        endif
        if EMPTY (this.w_CODINV) AND this.w_VALORIZ <> "N"
          ah_ErrorMsg("Specificare un inventario di riferimento","!")
          USE IN SELECT("UBILOT")
          USE IN SELECT("APPUBILOT")
          USE IN SELECT("UBILOT1")
          USE IN SELECT("UBILOT2")
          i_retcode = 'stop'
          return
        endif
        * --- Query per reperire i dati rilevati
        VQ_EXEC("QUERY\GSMA_SDR.VQR",this,"CONFR")
        * --- Elimino eventuali valori nulli delle varianti
        SELECT CONFR
        GO TOP
        SCAN FOR EMPTY(NVL(DRCODVAR,SPACE(20)))
        REPLACE DRCODVAR WITH SPACE(20)
        ENDSCAN
        * --- Si crea un unico cursore contenente i dati per il calcolo del dato procedurale ed il dato rilevato
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          select drcodart,drcodvar,confr.desart as desart,; 
 nvl(drubicaz,space(20)) as drubicaz,; 
 nvl(dr_lotto,space(20)) as dr_lotto, drkeyulo,nvl(drcodmat,space(40)) as drcodmat,; 
 drunimis, sum(drqtaes1) as drqtaes1,drkeysal,drcodric, drcodcom, AppUbilot.Procedur; 
 from confr left outer join AppUbilot ; 
 on drkeysal= slcodice and nvl(drubicaz,space(20)) = nvl(mlcodubi,space(20)) and; 
 nvl(dr_lotto,space(20)) = nvl(mlcodlot,space(20)) and nvl(drcodmat,space(40))=mtcodmat and ; 
 NVL(drcodcom, SPACE(15))=NVL(codcom, SPACE(15)) into cursor CurTmp1; 
 order by drkeysal, drkeyulo,drcodmat group by drkeysal,drkeyulo,drcodmat,codcom
        else
          select drcodart,drcodvar,confr.desart as desart,; 
 nvl(drubicaz,space(20)) as drubicaz,; 
 nvl(dr_lotto,space(20)) as dr_lotto, drkeyulo,nvl(drcodmat,space(40)) as drcodmat,; 
 drunimis, sum(drqtaes1) as drqtaes1,drkeysal,drcodric,AppUbilot.Procedur; 
 from confr left outer join AppUbilot ; 
 on drkeysal= slcodice and nvl(drubicaz,space(20)) = nvl(mlcodubi,space(20)) and; 
 nvl(dr_lotto,space(20)) = nvl(mlcodlot,space(20)) and nvl(drcodmat,space(40))=nvl(mtcodmat, space(40)) into cursor CurTmp1; 
 order by drkeysal, drkeyulo,drcodmat group by drkeysal,drkeyulo,drcodmat
        endif
        * --- Leggo  i dati dall'Inventario selezionato
        vq_exec("QUERY\GSMAISDR.VQR", this, "CurTmp")
        * --- Creo il Cursore per la stampa
        select a.* ,nvl(diqtaesi,0) as Saldo, nvl(dicosmpp,0) as dicosmpp, nvl(dicosult,0) as dicosult, nvl(diprzmpp,0) as diprzmpp,nvl(diprzult,0) as diprzult,; 
 nvl(dicosmpa,0) as dicosmpa, nvl(diprzmpa,0) as diprzmpa, nvl(dicossta,0) as dicossta, nvl(dicosfco,0) as dicosfco,; 
 nvl(dicoslco,0) as dicoslco,nvl(dicoslsc,0) as dicoslsc, space(3) as valuta; 
 from CurTmp1 a left join CurTmp b ; 
 on a.drcodart=b.dicodart and NVL(a.drcodvar,SPACE(20))=NVL(b.dicodvar,SPACE(20)) ; 
 order by drkeysal, drkeyulo,drcodmat into cursor __TMP__ nofilter
        L_CODART=this.w_CODART
        L_CODINV=this.w_CODINV
        L_CODESE=this.w_CODESE
        L_DATINV=this.w_DATINV
        L_VALORIZ=this.w_VALORIZ
        L_VALNAZ=this.w_VALNAZ
        L_VADECTOT=this.w_VADECTOT
        L_VADECUNI=this.w_VADECUNI
        L_PROCESS=this.w_PROCESS
        L_CODCOM=this.w_CODCOM
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE" and this.w_ESISTECOM
          * --- Stampa con commessa nel caso c'� almeno un articolo gestito a commessa
          cp_chprn("QUERY\gsma1ssd.FRX","",this)
        else
          cp_chprn("QUERY\gsma_ssd.FRX", " ", this)
        endif
      case this.pTIPO="N"
        * --- Stampa dati a Magazzino non rilevati
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          this.w_PROCESS = this.w_PUNPAD.w_PROCESS
          this.w_TIPESI = 1
        endif
        * --- Query per reperire i dati rilevati
        VQ_EXEC("QUERY\GSMA_SDR.VQR",this,"CONFR")
        * --- Elimino eventuali valori nulli delle varianti
        if UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE"
          SELECT CONFR
          GO TOP
          SCAN FOR EMPTY(NVL(DRCODVAR,SPACE(20)))
          REPLACE DRCODVAR WITH SPACE(20)
          ENDSCAN
        endif
        * --- Cusore per Dati a Magazzino non rilevati
        L_CODUBI=this.w_CODUBI
        do case
          case this.w_TIPESI=1
            * --- Solo esistenza <> 0
            if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
              SELECT * from AppUbilot where NVL(PROCEDUR,0) <> 0 and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT+CODCOM; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40))+nvl(DRCODCOM,space(15)) FROM CONFR); 
 into Cursor __TMP__
            else
              SELECT * from AppUbilot where NVL(PROCEDUR,0) <> 0 and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40)) FROM CONFR); 
 into Cursor __TMP__
            endif
          case this.w_TIPESI=2
            * --- Tutti
            if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
              SELECT * from AppUbilot where SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT+CODCOM; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40))+nvl(DRCODCOM,space(15)) FROM CONFR); 
 into Cursor __TMP__
            else
              SELECT * from AppUbilot where SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40)) FROM CONFR); 
 into Cursor __TMP__
            endif
          case this.w_TIPESI=3
            * --- Nell'intervallo
            if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
              SELECT * from AppUbilot where NVL(PROCEDUR,0) >= this.w_PROINI And NVL(PROCEDUR,0) <= this.w_PROFIN and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT+CODCOM; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40))+nvl(DRCODCOM,space(15)) FROM CONFR); 
 into Cursor __TMP__
            else
              SELECT * from AppUbilot where NVL(PROCEDUR,0) >= this.w_PROINI And NVL(PROCEDUR,0) <= this.w_PROFIN and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40)) FROM CONFR); 
 into Cursor __TMP__
            endif
        endcase
        cp_chprn("QUERY\gsma_srn.FRX", " ", this)
      case this.pTIPO="I"
        this.w_ZOOM = this.oParentObject.w_ZoomConf
        * --- Svuoto il Cursore Zoom
        Select (this.w_ZOOM.cCursor) 
 Zap
        * --- Query per reperire i dati rilevati
        if this.w_FLGRIL<>"E" 
          this.w_PROCESS = "T"
        endif
        VQ_EXEC("QUERY\GSMA_SDR.VQR",this,"CONFR")
        * --- Elimino eventuali valori nulli delle varianti
        if UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE"
          SELECT CONFR
          GO TOP
          SCAN FOR EMPTY(NVL(DRCODVAR,SPACE(20)))
          REPLACE DRCODVAR WITH SPACE(20)
          ENDSCAN
        endif
        * --- Cursore per Dati a Magazzino non rilevati
        do case
          case this.w_TIPESI=1
            * --- Solo esistenza <> 0
            do case
              case this.w_FLGRIL="R"
                if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
                  SELECT * from AppUbilot where NVL(PROCEDUR,0) <> 0 and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT+CODCOM; 
 in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40))+nvl(DRCODCOM,space(15)) FROM CONFR); 
 into Cursor __TMP__
                else
                  SELECT * from AppUbilot where NVL(PROCEDUR,0) <> 0 and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT; 
 in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40)) FROM CONFR); 
 into Cursor __TMP__
                endif
              case this.w_FLGRIL="N"
                if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
                  SELECT * from AppUbilot where NVL(PROCEDUR,0) <> 0 and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT+CODCOM; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40))+nvl(DRCODCOM,space(15)) FROM CONFR); 
 into Cursor __TMP__
                else
                  SELECT * from AppUbilot where NVL(PROCEDUR,0) <> 0 and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40)) FROM CONFR); 
 into Cursor __TMP__
                endif
              otherwise
                SELECT * from AppUbilot where NVL(PROCEDUR,0) <> 0 into Cursor __TMP__
            endcase
          case this.w_TIPESI=2
            * --- Tutti
            do case
              case this.w_FLGRIL="R"
                if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
                  SELECT * from AppUbilot where SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT+CODCOM; 
 in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40))+nvl(DRCODCOM,space(15)) FROM CONFR); 
 into Cursor __TMP__
                else
                  SELECT * from AppUbilot where SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT; 
 in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40)) FROM CONFR); 
 into Cursor __TMP__
                endif
              case this.w_FLGRIL="N"
                if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
                  SELECT * from AppUbilot where SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT+CODCOM; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40))+nvl(DRCODCOM,space(15)) FROM CONFR); 
 into Cursor __TMP__
                else
                  SELECT * from AppUbilot where SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40)) FROM CONFR); 
 into Cursor __TMP__
                endif
              otherwise
                SELECT * from AppUbilot into Cursor __TMP__
            endcase
          case this.w_TIPESI=3
            * --- Nell'intervallo
            do case
              case this.w_FLGRIL="R"
                if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
                  SELECT * from AppUbilot where NVL(PROCEDUR,0) >= this.w_PROINI and NVL(PROCEDUR,0) <= this.w_PROFIN and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT+CODCOM; 
 in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40))+nvl(DRCODCOM,space(15)) FROM CONFR); 
 into Cursor __TMP__
                else
                  SELECT * from AppUbilot where NVL(PROCEDUR,0) >= this.w_PROINI and NVL(PROCEDUR,0) <= this.w_PROFIN and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT; 
 in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40)) FROM CONFR); 
 into Cursor __TMP__
                endif
              case this.w_FLGRIL="N"
                if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
                  SELECT * from AppUbilot where NVL(PROCEDUR,0) >= this.w_PROINI and NVL(PROCEDUR,0) <= this.w_PROFIN and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT+CODCOM; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40))+nvl(DRCODCOM,space(15)) FROM CONFR); 
 into Cursor __TMP__
                else
                  SELECT * from AppUbilot where NVL(PROCEDUR,0) >= this.w_PROINI and NVL(PROCEDUR,0) <= this.w_PROFIN and SLCODMAG+SLCODICE+MLCODUBI+MLCODLOT+MTCODMAT; 
 not in (SELECT DRCODMAG+DRKEYSAL+nvl(DRUBICAZ,space(20))+nvl(DR_LOTTO,space(20))+nvl(DRCODMAT,space(40)) FROM CONFR); 
 into Cursor __TMP__
                endif
              otherwise
                SELECT * from AppUbilot where NVL(PROCEDUR,0) >= this.w_PROINI and NVL(PROCEDUR,0) <= this.w_PROFIN into Cursor __TMP__
            endcase
        endcase
        * --- Inserisco il risultato del cursore dello Zoom
        Select __TMP__ 
 Go Top 
 Scan 
 Scatter Memvar 
 m.ARCODART = m.CACODART 
 Select (this.w_ZOOM.cCursor) 
 Append blank 
 Gather memvar 
 Endscan 
 Select __TMP__ 
 use 
 Select ( this.w_ZOOM.cCursor ) 
 Go Top
        * --- Refresh della griglia
        this.w_zoom.grd.refresh
        * --- Libera il Bottone di conferma generazione
        this.oParentObject.w_ELABORATO = "N"
      case this.pTIPO="G"
        * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        * --- Inserimento dati Rilevati
        * --- Ultimo Documento generato (associato all'utente w_CODUTE)
        * --- Select from RILEVAZI
        i_nConn=i_TableProp[this.RILEVAZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(DRNUMDOC) AS NUMDOC  from "+i_cTable+" RILEVAZI ";
              +" where DRADDRIL="+cp_ToStrODBC(this.oParentObject.w_CODUTE)+"";
               ,"_Curs_RILEVAZI")
        else
          select MAX(DRNUMDOC) AS NUMDOC from (i_cTable);
           where DRADDRIL=this.oParentObject.w_CODUTE;
            into cursor _Curs_RILEVAZI
        endif
        if used('_Curs_RILEVAZI')
          select _Curs_RILEVAZI
          locate for 1=1
          do while not(eof())
          this.w_NUMDOC = INT(NVL(NUMDOC,0))
            select _Curs_RILEVAZI
            continue
          enddo
          use
        endif
        * --- Righe selezionate nello Zoom
        MM = this.oParentObject.w_ZoomConf.cCursor
        Select * from (MM) into cursor AppMM where XCHK=1
        this.w_OKREC = ( _TALLY>0 )
        this.w_oMESS=createobject("ah_message")
        if NOT(this.w_OKREC)
          ah_ErrorMsg("Nessuna rilevazione dati da generare","!")
        else
          * --- Try
          local bErr_0443A848
          bErr_0443A848=bTrsErr
          this.Try_0443A848()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Impossibile generare dati rilevati","!")
          endif
          bTrsErr=bTrsErr or bErr_0443A848
          * --- End
        endif
    endcase
    * --- Drop temporary table TMPVEND3
    i_nIdx=cp_GetTableDefIdx('TMPVEND3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND3')
    endif
    USE IN SELECT("__TMP__")
    USE IN SELECT("UBILOT")
    USE IN SELECT("APPUBILOT")
    USE IN SELECT("UBILOT1")
    USE IN SELECT("UBILOT2")
    USE IN SELECT("UBIC")
    USE IN SELECT("CurTmp")
    USE IN SELECT("CurTmp1")
    USE IN SELECT("AppMM")
    USE IN SELECT("MessErr")
    USE IN SELECT("Confr")
    USE IN SELECT("Matr")
  endproc
  proc Try_0443A848()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Generazione Movimento di Rilevazione Dati 
    * --- begin transaction
    cp_BeginTrs()
    Select AppMM 
 Go Top
    Scan
    this.w_TIPCON = "R"
    this.w_NUMDOC = this.w_NUMDOC+1
    this.w_CODART = CACODART
    this.w_CODVAR = LEFT(NVL(CACODVAR, SPACE(20)) + SPACE(20), 20) 
    this.w_DRCODRIC = rtrim(this.w_CODART)+iif(empty(nvl(this.w_CODVAR,"")),"","#"+alltrim(this.w_CODVAR))
    this.w_DRKEYSAL = SLCODICE
    this.w_DESART = NVL(DESART,SPACE(40))
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODICE"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_DRCODRIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODICE;
          from (i_cTable) where;
              CACODICE = this.w_DRCODRIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DRCODRIC = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(nvl(this.w_DRCODRIC, " "))
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODICE"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CAKEYSAL = "+cp_ToStrODBC(this.w_DRKEYSAL);
                +" and CATIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODICE;
            from (i_cTable) where;
                CAKEYSAL = this.w_DRKEYSAL;
                and CATIPCON = this.w_TIPCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DRCODRIC = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    ah_Msg("Elabora articolo: %1",.T.,.F.,.F.,ALLTRIM(this.w_DRCODRIC))
    this.w_CODLOT = iif(Empty(MLCODLOT),NULL,MLCODLOT)
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      this.w_KEYLOTTO = iif(Empty(KEYLOTTO),NULL,KEYLOTTO)
    else
      this.w_KEYLOTTO = this.w_CODLOT
    endif
    this.w_CODUBI = iif(Empty(MLCODUBI),NULL,MLCODUBI)
    this.w_KEYULO = iif(empty(nvl(this.w_CODUBI," ")),repl("#",20),this.w_CODUBI)+iif(empty(nvl(this.w_KEYLOTTO," ")),repl("#",20),this.w_KEYLOTTO)
    this.w_DRUNIMIS = ARUNMIS1
    this.w_DRCODCOM = NVL(CODCOM , SPACE(15))
    * --- Controllo se Unita di Misura Frazionabile
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMFLFRAZ"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(this.w_DRUNIMIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMFLFRAZ;
        from (i_cTable) where;
            UMCODICE = this.w_DRUNIMIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CODMAT = iif(Empty(MTCODMAT),NULL,MTCODMAT)
    this.w_QTAESI = NVL(DRQTAES1,0)
    * --- Controllo quantit� non frazionabile
    if this.w_FLFRAZ = "S" And this.w_QTAESI<>Int(this.w_QTAESI)
      this.w_oERRORLOG.AddMsgLog("Per l'articolo: %1, unit� di misura %2, la quantit� � non frazionabile", ALLTRIM(this.w_DESART), ALLTRIM(this.w_DRUNIMIS))     
      this.w_oERRORLOG.AddMsgLog("Dato rilevato non generato%0")     
    else
      if (Empty(nvl(this.w_CODMAT," ")) or this.w_QTAESI = 1 or this.w_QTAESI = 0) and this.w_QTAESI>-1
        this.w_DRSERIAL = cp_GetProg("RILEVAZI","RILSER",this.w_DRSERIAL,i_CODAZI)
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          * --- Insert into RILEVAZI
          i_nConn=i_TableProp[this.RILEVAZI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RILEVAZI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DRSERIAL"+",DRCODRIL"+",DRCODMAG"+",DRNUMDOC"+",DRADDRIL"+",DRCODRIC"+",DRCODART"+",DRCODVAR"+",DRKEYSAL"+",DRCODUTE"+",DRUBICAZ"+",DR_LOTTO"+",DRKEYULO"+",DRUNIMIS"+",DRQTAESI"+",DRQTAES1"+",DRCONFER"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",DRCODMAT"+",DRLOTART"+",DRCODCOM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_DRSERIAL),'RILEVAZI','DRSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIL),'RILEVAZI','DRCODRIL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'RILEVAZI','DRCODMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'RILEVAZI','DRNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUTE),'RILEVAZI','DRADDRIL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODRIC),'RILEVAZI','DRCODRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'RILEVAZI','DRCODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAR),'RILEVAZI','DRCODVAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DRKEYSAL),'RILEVAZI','DRKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','DRCODUTE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODUBI),'RILEVAZI','DRUBICAZ');
            +","+cp_NullLink(cp_ToStrODBC(this.w_KEYLOTTO),'RILEVAZI','DR_LOTTO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_KEYULO),'RILEVAZI','DRKEYULO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DRUNIMIS),'RILEVAZI','DRUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESI),'RILEVAZI','DRQTAESI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESI),'RILEVAZI','DRQTAES1');
            +","+cp_NullLink(cp_ToStrODBC(" "),'RILEVAZI','DRCONFER');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'RILEVAZI','UTDC');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','UTCV');
            +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'RILEVAZI','UTDV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAT),'RILEVAZI','DRCODMAT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODLOT),'RILEVAZI','DRLOTART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODCOM),'RILEVAZI','DRCODCOM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_DRSERIAL,'DRCODRIL',this.w_CODRIL,'DRCODMAG',this.w_CODMAG,'DRNUMDOC',this.w_NUMDOC,'DRADDRIL',this.oParentObject.w_CODUTE,'DRCODRIC',this.w_DRCODRIC,'DRCODART',this.w_CODART,'DRCODVAR',this.w_CODVAR,'DRKEYSAL',this.w_DRKEYSAL,'DRCODUTE',i_CODUTE,'DRUBICAZ',this.w_CODUBI,'DR_LOTTO',this.w_KEYLOTTO)
            insert into (i_cTable) (DRSERIAL,DRCODRIL,DRCODMAG,DRNUMDOC,DRADDRIL,DRCODRIC,DRCODART,DRCODVAR,DRKEYSAL,DRCODUTE,DRUBICAZ,DR_LOTTO,DRKEYULO,DRUNIMIS,DRQTAESI,DRQTAES1,DRCONFER,UTCC,UTDC,UTCV,UTDV,DRCODMAT,DRLOTART,DRCODCOM &i_ccchkf. );
               values (;
                 this.w_DRSERIAL;
                 ,this.w_CODRIL;
                 ,this.w_CODMAG;
                 ,this.w_NUMDOC;
                 ,this.oParentObject.w_CODUTE;
                 ,this.w_DRCODRIC;
                 ,this.w_CODART;
                 ,this.w_CODVAR;
                 ,this.w_DRKEYSAL;
                 ,i_CODUTE;
                 ,this.w_CODUBI;
                 ,this.w_KEYLOTTO;
                 ,this.w_KEYULO;
                 ,this.w_DRUNIMIS;
                 ,this.w_QTAESI;
                 ,this.w_QTAESI;
                 ," ";
                 ,i_CODUTE;
                 ,SetInfoDate(g_CALUTD);
                 ,i_CODUTE;
                 ,SetInfoDate(g_CALUTD);
                 ,this.w_CODMAT;
                 ,this.w_CODLOT;
                 ,this.w_DRCODCOM;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into RILEVAZI
          i_nConn=i_TableProp[this.RILEVAZI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RILEVAZI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DRSERIAL"+",DRCODRIL"+",DRCODMAG"+",DRNUMDOC"+",DRADDRIL"+",DRCODRIC"+",DRCODART"+",DRCODVAR"+",DRKEYSAL"+",DRCODUTE"+",DRUBICAZ"+",DR_LOTTO"+",DRKEYULO"+",DRUNIMIS"+",DRQTAESI"+",DRQTAES1"+",DRCONFER"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",DRCODMAT"+",DRCODCOM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_DRSERIAL),'RILEVAZI','DRSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIL),'RILEVAZI','DRCODRIL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'RILEVAZI','DRCODMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'RILEVAZI','DRNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUTE),'RILEVAZI','DRADDRIL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODRIC),'RILEVAZI','DRCODRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'RILEVAZI','DRCODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAR),'RILEVAZI','DRCODVAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DRKEYSAL),'RILEVAZI','DRKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','DRCODUTE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODUBI),'RILEVAZI','DRUBICAZ');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODLOT),'RILEVAZI','DR_LOTTO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_KEYULO),'RILEVAZI','DRKEYULO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DRUNIMIS),'RILEVAZI','DRUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESI),'RILEVAZI','DRQTAESI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESI),'RILEVAZI','DRQTAES1');
            +","+cp_NullLink(cp_ToStrODBC(" "),'RILEVAZI','DRCONFER');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','UTCV');
            +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'RILEVAZI','UTDC');
            +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'RILEVAZI','UTDV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAT),'RILEVAZI','DRCODMAT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODCOM),'RILEVAZI','DRCODCOM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_DRSERIAL,'DRCODRIL',this.w_CODRIL,'DRCODMAG',this.w_CODMAG,'DRNUMDOC',this.w_NUMDOC,'DRADDRIL',this.oParentObject.w_CODUTE,'DRCODRIC',this.w_DRCODRIC,'DRCODART',this.w_CODART,'DRCODVAR',this.w_CODVAR,'DRKEYSAL',this.w_DRKEYSAL,'DRCODUTE',i_CODUTE,'DRUBICAZ',this.w_CODUBI,'DR_LOTTO',this.w_CODLOT)
            insert into (i_cTable) (DRSERIAL,DRCODRIL,DRCODMAG,DRNUMDOC,DRADDRIL,DRCODRIC,DRCODART,DRCODVAR,DRKEYSAL,DRCODUTE,DRUBICAZ,DR_LOTTO,DRKEYULO,DRUNIMIS,DRQTAESI,DRQTAES1,DRCONFER,UTCC,UTCV,UTDC,UTDV,DRCODMAT,DRCODCOM &i_ccchkf. );
               values (;
                 this.w_DRSERIAL;
                 ,this.w_CODRIL;
                 ,this.w_CODMAG;
                 ,this.w_NUMDOC;
                 ,this.oParentObject.w_CODUTE;
                 ,this.w_DRCODRIC;
                 ,this.w_CODART;
                 ,this.w_CODVAR;
                 ,this.w_DRKEYSAL;
                 ,i_CODUTE;
                 ,this.w_CODUBI;
                 ,this.w_CODLOT;
                 ,this.w_KEYULO;
                 ,this.w_DRUNIMIS;
                 ,this.w_QTAESI;
                 ,this.w_QTAESI;
                 ," ";
                 ,i_CODUTE;
                 ,i_CODUTE;
                 ,SetInfoDate(g_CALUTD);
                 ,SetInfoDate(g_CALUTD);
                 ,this.w_CODMAT;
                 ,this.w_DRCODCOM;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      else
        if not Empty(nvl(this.w_CODVAR," ")) 
          this.w_oPart = this.w_oMess.AddMsgPart(", variante: %1")
          this.w_oPart.AddParam(ALLTRIM(this.w_CODVAR))     
        endif
        if not Empty(nvl(this.w_CODLOT," ")) 
          this.w_oPart = this.w_oMess.AddMsgPart(", lotto: %1")
          this.w_oPart.AddParam(ALLTRIM(this.w_CODLOT))     
        endif
        if not Empty(nvl(this.w_CODUBI," ")) 
          this.w_oPart = this.w_oMess.AddMsgPart(", ubicazione: %1")
          this.w_oPart.AddParam(ALLTRIM(this.w_CODUBI))     
        endif
        if not empty(this.w_DRCODCOM)
          this.w_oPart = this.w_oMess.AddMsgPart(", commessa: %1")
          this.w_oPart.AddParam(ALLTRIM(this.w_DRCODCOM))     
        endif
        this.w_MESS = this.w_oMess.ComposeMessage()
        if Empty(nvl(this.w_CODMAT," ")) 
          this.w_oERRORLOG.AddMsgLog("Per l'articolo: %1%2 � stata impostata una quantit� negativa: %3", ALLTRIM(this.w_DESART), alltrim(this.w_MESS), alltrim(str(this.w_QTAESI)))     
          this.w_oERRORLOG.AddMsgLog("Dato rilevato non generato%0")     
        else
          this.w_oERRORLOG.AddMsgLog("Per l'articolo: %1%2, matricola: %3, � stata impostata una quantit� diversa da 1 o 0 o negativa: %4", ALLTRIM(this.w_DESART), alltrim(this.w_MESS), ALLTRIM(NVL(this.w_CODMAT,SPACE(20))), alltrim(str(this.w_QTAESI)))     
          this.w_oERRORLOG.AddMsgLog("Dato rilevato non generato%0")     
        endif
      endif
    endif
    Select AppMM
    EndScan
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Generazione terminata","!")
    this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
    * --- Inibisce il Bottone di conferma generazione
    this.oParentObject.w_ELABORATO = "S"
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if vartype (this.w_PUNPAD.w_CODCOM) = "C"
      this.w_CODCOM = this.w_PUNPAD.w_CODCOM
    endif
    if vartype (this.w_PUNPAD.w_CODART) = "C"
      this.w_CODART = this.w_PUNPAD.w_CODART
    endif
    if vartype (this.w_PUNPAD.w_CODMAG) = "C"
      this.w_CODMAG = this.w_PUNPAD.w_CODMAG
    endif
    if vartype (this.w_PUNPAD.w_CODUBI) = "C"
      this.w_CODUBI = this.w_PUNPAD.w_CODUBI
    endif
    if vartype (this.w_PUNPAD.w_CODUBF) = "C"
      this.w_CODUBF = this.w_PUNPAD.w_CODUBF
    else
      this.w_CODUBF = this.w_CODUBI
    endif
    if vartype (this.w_PUNPAD.w_UBICAZ) = "C"
      this.w_UBICAZ = this.w_PUNPAD.w_UBICAZ
    endif
    if vartype (this.w_PUNPAD.w_CODRIL) = "C"
      this.w_CODRIL = this.w_PUNPAD.w_CODRIL
    endif
    if vartype (this.w_PUNPAD.w_DATASTAM) = "D"
      this.w_DATASTAM = this.w_PUNPAD.w_DATASTAM
    endif
    do case
      case vartype (this.w_PUNPAD.w_CODART1) = "C"
        this.w_CODART1 = this.w_PUNPAD.w_CODART1
      case this.pTIPO="C"
        this.w_CODART1 = this.w_CODART
    endcase
    if vartype (this.w_PUNPAD.w_TIPESI) = "N"
      this.w_TIPESI = this.w_PUNPAD.w_TIPESI
    endif
    if vartype (this.w_PUNPAD.w_CODFAM) = "C"
      this.w_CODFAM = this.w_PUNPAD.w_CODFAM
    endif
    if vartype (this.w_PUNPAD.w_CODGRU) = "C"
      this.w_CODGRU = this.w_PUNPAD.w_CODGRU
    endif
    if vartype (this.w_PUNPAD.w_CODCAT) = "C"
      this.w_CODCAT = this.w_PUNPAD.w_CODCAT
    endif
    if vartype (this.w_PUNPAD.w_CODMAR) = "C"
      this.w_CODMAR = this.w_PUNPAD.w_CODMAR
    endif
    if vartype (this.w_PUNPAD.w_CLAMAT) = "C"
      this.w_CLAMAT = this.w_PUNPAD.w_CLAMAT
    endif
    if vartype (this.w_PUNPAD.w_CODFOR) = "C"
      this.w_CODFOR = this.w_PUNPAD.w_CODFOR
    endif
    if vartype (this.w_PUNPAD.w_PROINI) = "N"
      this.w_PROINI = this.w_PUNPAD.w_PROINI
    endif
    if vartype (this.w_PUNPAD.w_PROFIN) = "N"
      this.w_PROFIN = this.w_PUNPAD.w_PROFIN
    endif
    if vartype (this.w_PUNPAD.w_MOVINI) = "D"
      this.w_MOVINI = this.w_PUNPAD.w_MOVINI
    endif
    if vartype (this.w_PUNPAD.w_ESCLOBSO) = "C"
      this.w_ESCLOBSO = this.w_PUNPAD.w_ESCLOBSO
    endif
    if vartype (this.w_PUNPAD.w_DATARIL) = "D"
      this.w_DATARIL = this.w_PUNPAD.w_DATARIL
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='RILEVAZI'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='*TMPVEND3'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_RILEVAZI')
      use in _Curs_RILEVAZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
