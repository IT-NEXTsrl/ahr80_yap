* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bc1                                                        *
*              Gestione C/Corrente di default                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-23                                                      *
* Last revis.: 2011-11-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bc1",oParentObject,m.pParam)
return(i_retval)

define class tgsar_bc1 as StdBatch
  * --- Local variables
  pParam = space(3)
  w_PADRE = .NULL.
  w_PADRE = .NULL.
  w_LEN = space(3)
  w_CONTA = 0
  w_NUMCON = 0
  w_MESS = space(100)
  w_MESS1 = space(100)
  w_CONCOR = space(12)
  w_TEST = 0
  w_CODBAN = space(10)
  w_IBAN = space(34)
  w_BBAN = space(30)
  w_CIN = space(1)
  w_CONTROLLO = 0
  w_CODPAG = space(5)
  w_FLBEST = space(1)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_CIFRA = space(2)
  w_CCCINABI = space(1)
  w_CCCONCOR2 = space(12)
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pParam="DEF"
        this.w_PADRE = this.oParentObject
        this.w_PADRE.MarkPos()     
        if lower(this.oParentObject.oParentObject.class)="tgsar_ano"
          this.oParentObject.oParentObject.w_NOCODBAN=this.oParentObject.w_CCCODBAN 
 this.oParentObject.oParentObject.w_NONUMCOR=this.oParentObject.w_CCCONCOR
        else
          this.oParentObject.oParentObject.w_ANCODBAN=this.oParentObject.w_CCCODBAN 
 this.oParentObject.oParentObject.w_ANNUMCOR=this.oParentObject.w_CCCONCOR
        endif
        this.w_PADRE.oParentObject.w_DESBAN=this.oParentObject.w_DESBAN 
 this.w_PADRE.oParentObject.w_AN__IBAN=this.oParentObject.w_CCCOIBAN 
 this.w_PADRE.oParentObject.w_AN__BBAN=this.oParentObject.w_CCCOBBAN 
 this.w_PADRE.oParentObject.w_ANCINABI=this.w_CCCINABI 
 this.w_PADRE.oParentObject.bUpdated=.t. 
 this.w_PADRE.oParentObject.SetControlsValue()
        this.oParentObject.w_ANCODBAN = this.oParentObject.w_CCCODBAN
        this.oParentObject.w_AN__IBAN = this.oParentObject.w_CCCOIBAN
        this.oParentObject.w_AN__BBAN = this.oParentObject.w_CCCOBBAN
        this.oParentObject.w_ANNUMCOR = this.oParentObject.w_CCCONCOR
        SELECT (this.w_PADRE.cTrsName)
        Go Top
        SCAN
        Replace t_DEFAULT with 0
        if t_CCCODBAN=this.oParentObject.w_ANCODBAN AND t_CCCONCOR=this.oParentObject.w_ANNUMCOR
          Replace t_DEFAULT with 1
          this.oParentObject.w_DEFAULT = "S"
        endif
        ENDSCAN
        * --- Riposizionamento sul Transitorio effettuando la SaveDependsOn()
        this.w_PADRE.RePos(.F.)     
      case this.pParam="BAN"
        if NOT EMPTY(NVL(this.oParentObject.o_CCCODBAN,""))
          this.oParentObject.w_CCCODBAN = this.oParentObject.o_CCCODBAN
          ah_ErrorMsg("Impossibile variare codice banca: eliminare la riga","!","")
        endif
      case this.pParam="CHK" and g_APPLICATION ="ADHOC REVOLUTION"
        this.w_oMESS=createobject("AH_Message")
        if lower(this.oParentObject.oParentObject.class)="tgsar_ano"
          this.w_CODPAG = this.oParentObject.oParentObject.w_NOCODPAG
        else
          this.w_CODPAG = this.oParentObject.oParentObject.w_ANCODPAG
        endif
        this.w_MESS = ""
        this.w_MESS1 = ""
        this.w_PADRE = this.oParentObject
        this.w_PADRE.MarkPos()     
        this.w_TEST = 0
        this.w_NUMCON = 0
        * --- Select from gsar_bc1
        do vq_exec with 'gsar_bc1',this,'_Curs_gsar_bc1','',.f.,.t.
        if used('_Curs_gsar_bc1')
          select _Curs_gsar_bc1
          locate for 1=1
          do while not(eof())
          this.w_CONTROLLO = _Curs_GSAR_BC1.CONTA
            select _Curs_gsar_bc1
            continue
          enddo
          use
        endif
         
 SELECT (this.oParentObject.cTrsName) 
 Go Top 
 Scan for not empty(nvl(t_CCCODBAN,"")) and not empty(nvl(t_CCCONCOR,""))
        this.w_TEST = this.w_TEST+IIF(t_DEFAULT=1, 1,0)
        this.w_CODBAN = IIF(t_DEFAULT=1, t_CCCODBAN, this.w_CODBAN)
        this.w_IBAN = IIF(t_DEFAULT=1, t_CCCOIBAN, this.w_IBAN)
        this.w_BBAN = IIF(t_DEFAULT=1, t_CCCOBBAN, this.w_BBAN)
        this.w_CIN = IIF(t_DEFAULT=1, t_CCCINABI, this.w_CIN)
        this.w_CONTA = 1
        this.w_NUMCON = this.w_NUMCON+1
        this.w_MESS = ""
        this.w_FLBEST = NVL(t_FLBEST, " ")
        this.w_CONCOR = ALLTRIM(t_CCCONCOR)
        this.w_LEN = LEN(this.w_CONCOR)
        * --- Controllo che il conto corrente si normalizzato secondo la normativa per la generazione del file C.B.I.
        if this.w_FLBEST<>"S"
          do while this.w_CONTA<=this.w_LEN
            if (ASC(SUBSTR(ALLTRIM(this.w_CONCOR), this.w_CONTA, this.w_CONTA+1)) >=65 AND ASC(SUBSTR(ALLTRIM(this.w_CONCOR), this.w_CONTA, this.w_CONTA+1))<=90) OR (ASC(SUBSTR(ALLTRIM(this.w_CONCOR), this.w_CONTA, this.w_CONTA+1)) >=48 AND ASC(SUBSTR(ALLTRIM(this.w_CONCOR), this.w_CONTA, this.w_CONTA+1))<=57)
              this.w_CONTA = this.w_CONTA+1
              this.w_MESS = ""
            else
              this.w_oPART = this.w_oMESS.AddMsgPartNL("Il codice C/C %1 non � normalizzato secondo le regole BBAN%0in quanto presenta caratteri speciali, separatori o segni di interpunzione%0")
              this.w_oPART.AddParam(ALLTRIM(t_CCCONCOR))     
              this.w_CONTA = this.w_LEN+1
            endif
          enddo
          if islita()
            if (EMPTY(t_CCCINABI) OR EMPTY(t_CODABI) OR EMPTY(t_CODCAB) OR EMPTY(t_CCCONCOR)) 
              this.w_oPART = this.w_oMESS.AddMsgPartNL("Codice %1 dati mancanti che impediscono la creazione del BBAN:%0CIN di controllo e/o codice ABI e/o codice CAB e/o C/C%0")
              this.w_oPART.AddParam(ALLTRIM(t_CCCONCOR))     
            endif
            if (EMPTY(t_CODPA) OR EMPTY(t_CIFRA)) 
              this.w_oPART = this.w_oMESS.AddMsgPartNL("Codice %1 dati mancanti nel codice IBAN:%0Codice paese e/o check di controllo")
              this.w_oPART.AddParam(ALLTRIM(t_CCCONCOR))     
            endif
            * --- Verifico correttezza codice IBAN
            this.w_CCCONCOR2 = right("000000000000"+alltrim(t_CCCONCOR),12)
            this.w_CCCINABI = CALCCIN("C",t_CODABI,t_CODCAB,this.w_CCCONCOR2)
            this.w_CIFRA = CHECKIBAN("C",t_CODPA+"00"+this.w_CCCINABI+t_CODABI+t_CODCAB+this.w_CCCONCOR2)
            if t_CCCOIBAN # CALCIBAN(t_CODPA,this.w_CIFRA,this.w_CCCINABI,t_CODABI,t_CODCAB,this.w_CCCONCOR2,t_FLBEST)
              this.w_oPART = this.w_oMESS.AddMsgPartNL("Codice %1 codice IBAN errato")
              this.w_oPART.AddParam(ALLTRIM(t_CCCONCOR))     
            endif
          endif
        else
          if EMPTY(t_CCCODBIC)
            this.w_oPART = this.w_oMESS.AddMsgPartNL("Codice %1 dati mancanti nel codice BIC")
            this.w_oPART.AddParam(ALLTRIM(t_CCCONCOR))     
          endif
          if EMPTY(t_CCCOIBAN)
            this.w_oPART = this.w_oMESS.AddMsgPartNL("Codice %1 dati mancanti nel codice IBAN/Cod. int.")
            this.w_oPART.AddParam(ALLTRIM(t_CCCONCOR))     
          endif
        endif
        if this.w_oMESS.AH_MsgParts.count<>0
          this.w_oMESS.AH_ERRORMSG()     
        endif
        ENDSCAN
        * --- Se non ho la banca del cliente ma ho unn record nel figlio, oppure la banca del cliente
        *     � diversa da quella della movimentazione, forzo l'assegnamento in modo da avere sempre un default.
        if this.w_TEST<>0 AND this.w_NUMCON>0 
          * --- Write into CONTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AN__IBAN ="+cp_NullLink(cp_ToStrODBC(this.w_IBAN),'CONTI','AN__IBAN');
            +",ANCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_CODBAN),'CONTI','ANCODBAN');
            +",AN__BBAN ="+cp_NullLink(cp_ToStrODBC(this.w_BBAN),'CONTI','AN__BBAN');
            +",ANCINABI ="+cp_NullLink(cp_ToStrODBC(this.w_CIN),'CONTI','ANCINABI');
                +i_ccchkf ;
            +" where ";
                +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODCON);
                +" and ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_CCTIPCON);
                   )
          else
            update (i_cTable) set;
                AN__IBAN = this.w_IBAN;
                ,ANCODBAN = this.w_CODBAN;
                ,AN__BBAN = this.w_BBAN;
                ,ANCINABI = this.w_CIN;
                &i_ccchkf. ;
             where;
                ANCODICE = this.oParentObject.w_CCCODCON;
                and ANTIPCON = this.oParentObject.w_CCTIPCON;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.oParentObject.w_ANCODBAN = this.w_CODBAN
        endif
        if (this.w_CONTROLLO>0 AND this.oParentObject.w_CCTIPCON="F") OR this.w_NUMCON>0
          if this.w_TEST=0
            this.w_MESS1 = ah_MsgFormat("Inserire un conto corrente di default")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS1
          endif
        endif
        * --- Riposizionamento sul Transitorio effettuando la SaveDependsOn()
        this.w_PADRE.RePos(.F.)     
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_gsar_bc1')
      use in _Curs_gsar_bc1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
