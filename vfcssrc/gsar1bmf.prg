* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bmf                                                        *
*              MODIFICA FAMIGLIE ATTRIBUTI AZIENDALI                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-18                                                      *
* Last revis.: 2009-03-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bmf",oParentObject,m.pAR)
return(i_retval)

define class tgsar1bmf as StdBatch
  * --- Local variables
  pAR = space(3)
  w_OK = .f.
  w_MESS = space(254)
  Padre = .NULL.
  w_CODICE = space(10)
  w_RECTRS = 0
  w_OBJ = .NULL.
  w_OLDAZI = space(5)
  w_AZIENDA = space(5)
  * --- WorkFile variables
  GRUDATTR_idx=0
  ASS_ATTR_idx=0
  TMPVEND1_idx=0
  ATT_AAZI_idx=0
  FAZDATTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Aggiornamento valori famiglie gruppi
    this.w_OK = .T.
    this.oParentObject.w_HASEVENT = .t.
    this.Padre = this.oParentObject
    this.w_MESS = "Transazione abbandonata"
    do case
      case this.pAR="CAF"
        * --- Update record attuale
        * --- Write into GRUDATTR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.GRUDATTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GRUDATTR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.GRUDATTR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GRMULTIP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FRMULTIP),'GRUDATTR','GRMULTIP');
          +",GROBBLIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FROBBLIG),'GRUDATTR','GROBBLIG');
              +i_ccchkf ;
          +" where ";
              +"GRFAMATT = "+cp_ToStrODBC(this.oParentObject.w_FRCODICE);
                 )
        else
          update (i_cTable) set;
              GRMULTIP = this.oParentObject.w_FRMULTIP;
              ,GROBBLIG = this.oParentObject.w_FROBBLIG;
              &i_ccchkf. ;
           where;
              GRFAMATT = this.oParentObject.w_FRCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Su aggiornamento attributi verifica se attributo gi� utilizzato
        * --- Read from ASS_ATTR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2],.t.,this.ASS_ATTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ASCODFAM"+;
            " from "+i_cTable+" ASS_ATTR where ";
                +"ASCODFAM = "+cp_ToStrODBC(this.oParentObject.w_FRCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ASCODFAM;
            from (i_cTable) where;
                ASCODFAM = this.oParentObject.w_FRCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODICE = NVL(cp_ToDate(_read_.ASCODFAM),cp_NullValue(_read_.ASCODFAM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows > 0 AND Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
          if Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
            this.oParentObject.w_FLEDIT = IIF( this.oParentObject.w_HASEVENT , "N" ," ") 
          endif
        else
          if this.oParentObject.w_FRINPUTA <> this.oParentObject.o_FRINPUTA
            if this.oParentObject.w_FRINPUTA="C"
              this.oParentObject.w_FRDIMENS = 20
            else
              if this.oParentObject.w_FRINPUTA="N"
                this.oParentObject.w_FRDIMENS = 12
                this.oParentObject.w_FRNUMDEC = 3
              endif
            endif
          endif
        endif
      case this.pAR="TIP"
        if this.oParentObject.w_FRTIPOLO$"M-F"
          this.w_OBJ = this.oparentobject.GSAR_MAF
          this.w_OBJ.MarkPos()     
          this.w_RECTRS = this.w_OBJ.NumRow()
          if this.w_RECTRS=0
            this.w_MESS = AH_MSGFORMAT("Inserire almeno una riga di dettaglio")
            this.w_OK = .F.
          endif
        endif
        if this.oparentobject.cFunction="Load" AND this.w_OK
          * --- Create temporary table TMPVEND1
          i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSAR_BMF',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVEND1_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.w_OLDAZI = i_CODAZI
          * --- Try
          local bErr_0360DE78
          bErr_0360DE78=bTrsErr
          this.Try_0360DE78()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0360DE78
          * --- End
          a=cp_CHANGEAZI(this.w_OLDAZI)
          * --- Drop temporary table TMPVEND1
          i_nIdx=cp_GetTableDefIdx('TMPVEND1')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVEND1')
          endif
        endif
        * --- Su aggiornamento attributi verifica se attributo gi� utilizzato
        if Not this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        endif
    endcase
  endproc
  proc Try_0360DE78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from ATT_AAZI
    i_nConn=i_TableProp[this.ATT_AAZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_AAZI_idx,2],.t.,this.ATT_AAZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ATCODFAM,ATCODAZI  from "+i_cTable+" ATT_AAZI ";
          +" where ATCODFAM="+cp_ToStrODBC(this.oParentObject.w_FRCODICE)+"";
           ,"_Curs_ATT_AAZI")
    else
      select ATCODFAM,ATCODAZI from (i_cTable);
       where ATCODFAM=this.oParentObject.w_FRCODICE;
        into cursor _Curs_ATT_AAZI
    endif
    if used('_Curs_ATT_AAZI')
      select _Curs_ATT_AAZI
      locate for 1=1
      do while not(eof())
      this.w_AZIENDA = _Curs_ATT_AAZI.ATCODAZI
      a=cp_CHANGEAZI(this.w_AZIENDA)
      * --- Insert into FAZDATTR
      i_nConn=i_TableProp[this.FAZDATTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FAZDATTR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.FAZDATTR_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_ATT_AAZI
        continue
      enddo
      use
    endif
    return


  proc Init(oParentObject,pAR)
    this.pAR=pAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='GRUDATTR'
    this.cWorkTables[2]='ASS_ATTR'
    this.cWorkTables[3]='*TMPVEND1'
    this.cWorkTables[4]='ATT_AAZI'
    this.cWorkTables[5]='FAZDATTR'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_ATT_AAZI')
      use in _Curs_ATT_AAZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAR"
endproc
