* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bms                                                        *
*              Conferma primanota                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_53]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-14                                                      *
* Last revis.: 2010-04-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bms",oParentObject,m.pEXEC)
return(i_retval)

define class tgscg_bms as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_OBJECT = .NULL.
  w_GSCG_MPN = .NULL.
  * --- WorkFile variables
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSCG_KCP esegue conferma primanota
    this.w_GSCG_MPN = This.oparentobject.oparentobject
    do case
      case this.pEXEC="C"
        * --- Aggiorno primanota da confermata a provvisoria
        this.w_GSCG_MPN.ecpedit()     
        this.w_GSCG_MPN.w_PNFLPROV = "N"
        this.w_GSCG_MPN.bHeaderUpdated = .T.
        this.w_GSCG_MPN.Notifyevent("w_PNFLPROV Changed")     
        this.w_GSCG_MPN.w_PNNUMPRO = this.oParentObject.w_PNNUMPRO
        this.w_GSCG_MPN.w_PNALFPRO = this.oParentObject.w_PNALFPRO
        this.w_GSCG_MPN.w_PNNUMDOC = this.oParentObject.w_PNNUMDOC
        this.w_GSCG_MPN.w_PNALFDOC = this.oParentObject.w_PNALFDOC
        this.w_GSCG_MPN.w_PNDATDOC = this.oParentObject.w_PNDATDOC
        this.w_GSCG_MPN.w_PNCOMPET = this.oParentObject.w_PNCOMPET
        this.w_GSCG_MPN.w_PNCODESE = this.oParentObject.w_PNCOMPET
        this.w_GSCG_MPN.w_INIESE = this.oParentObject.w_INIESE
        this.w_GSCG_MPN.w_FINESE = this.oParentObject.w_FINESE
        if this.oParentObject.w_PNCOMPET<>this.oParentObject.w_CODESE
          this.w_GSCG_MPN.Notifyevent("w_PNCOMPET Changed")     
        endif
        this.w_GSCG_MPN.w_PNDATREG = this.oParentObject.w_PNDATREG
        this.w_GSCG_MPN.w_PNCOMIVA = this.oParentObject.w_PNCOMIVA
        this.w_GSCG_MPN.w_PNDATPLA = this.oParentObject.w_PNCOMIVA
        if this.w_GSCG_MPN.w_PNNUMDOC<>this.w_GSCG_MPN.w_ONUMDOC AND this.w_GSCG_MPN.w_PNNUMDOC>=this.w_GSCG_MPN.op_PNNUMDOC AND NOT EMPTY(this.w_GSCG_MPN.w_PNANNDOC)
          * --- Forzo aggiornamento numero documento
          this.w_GSCG_MPN.w_OLANDO = Space(4)
        endif
        this.w_GSCG_MPN.w_AGGPRO = .T.
        this.w_GSCG_MPN.ecpsave()     
        this.w_GSCG_MPN.ecpquit()     
      case this.pEXEC="P"
        * --- Propone il Primo Progressivo Documento Disponibile Protocollo
        this.oParentObject.w_PNNUMPRO = 0
        i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
        cp_AskTableProg(this.oParentObject, i_Conn, "PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
        this.w_GSCG_MPN.OP_PNNUMPRO = this.oParentObject.w_PNNUMPRO
        this.w_GSCG_MPN.OP_PNALFPRO = this.oParentObject.w_PNALFPRO
      case this.pEXEC="D"
        * --- Propone il Primo Progressivo Documento Disponibile Documento
        this.oParentObject.w_PNNUMDOC = 0
        i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
        cp_AskTableProg(this.oParentObject, i_Conn, "PRDOC", "i_codazi,w_PNANNDOC,w_PNPRD,w_PNALFDOC,w_PNNUMDOC")
        this.w_GSCG_MPN.OP_PNNUMDOC = this.oParentObject.w_PNNUMDOC
        this.w_GSCG_MPN.OP_PNALFDOC = this.oParentObject.w_PNALFDOC
    endcase
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='PNT_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
