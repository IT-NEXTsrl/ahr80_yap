* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bvb                                                        *
*              Visualizza beneficiari                                          *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-04-11                                                      *
* Last revis.: 2007-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODBEN
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bvb",oParentObject,m.pCODBEN)
return(i_retval)

define class tgste_bvb as StdBatch
  * --- Local variables
  pCODBEN = space(20)
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per visualizzazione dati beneficiario invocata da GSTE_KMS
    * --- Definisco l'oggetto
    this.w_PROG = GSAR_ANO("B")
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      i_retcode = 'stop'
      return
    endif
    this.w_PROG.EcpFilter()     
    * --- inizializzo la chiave del beneficiario
    this.w_PROG.w_NOCODICE = this.pCODBEN
    * --- Carico il record
    this.w_PROG.EcpSave()     
  endproc


  proc Init(oParentObject,pCODBEN)
    this.pCODBEN=pCODBEN
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODBEN"
endproc
