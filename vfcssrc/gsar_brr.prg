* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_brr                                                        *
*              Controllo righe piene schede di calcolo                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_37]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-30                                                      *
* Last revis.: 2004-01-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_brr",oParentObject,m.pPAR)
return(i_retval)

define class tgsar_brr as StdBatch
  * --- Local variables
  pPAR = space(1)
  w_MSGERR = space(100)
  w_COUNTER = 0
  w_RECPOS = 0
  w_AbsRow = 0
  w_nRelRow = 0
  w_PADRE = .NULL.
  w_RECO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo righe piene in gsva_msl
    * --- codice articolo su dettaglio dela scheda
    this.w_PADRE = this.OparentObject
    * --- ===========================
    this.oParentObject.w_NORIGHE = .F.
    do case
      case this.pPAR $ "N"
        if not empty(this.oParentObject.w_MDLINGUA)
          this.w_COUNTER = 0
          do case
            case this.pPAR="N"
              this.w_PADRE.MarkPos()     
              position=recno()
              * --- CONTROLLA SE UNA LINGUA E' STATO GIA' INSERITA
              * --- conto le righe del cursore del dettaglio (diverse da quella attualmente modificata)
              * --- aventi lingua uguale a quello appena impostato.
              this.w_COUNTER = 0
              Count for (t_MDLINGUA==this.oParentObject.w_MDLINGUA and recno()<>position) And Not Deleted() To this.w_COUNTER
              * --- Mi rimetto nella riga di partenza
              this.w_PADRE.RePos()     
              if this.w_COUNTER>0
                * --- deve esistere almeno un'altra riga con quell'articolo
                this.w_MSGERR = "Cod. lingua gi� presente nel modello"
                ah_ErrorMsg(this.w_MSGERR,"!")
                this.oParentObject.w_MDLINGUA = space(3)
                this.oParentObject.w_DESCRI = space(35)
                this.w_PADRE.SaveRow()     
                this.w_PADRE.Set("w_MDLINGUA", SPACE(3))     
                this.w_PADRE.Set("w_DESCRI", SPACE(35))     
              endif
          endcase
        endif
      case this.pPAR="R"
         
 Select(this.w_Padre.cTrsName) 
 Count for Not Deleted() And Not Empty(t_MDLINGUA) To this.w_RECO
        if this.w_RECO=0
          this.oParentObject.w_NORIGHE = .T.
        endif
    endcase
  endproc


  proc Init(oParentObject,pPAR)
    this.pPAR=pPAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPAR"
endproc
