* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_scc                                                        *
*              Controllo congruenza primanota                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_39]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-27                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_scc",oParentObject))

* --- Class definition
define class tgscg_scc as StdForm
  Top    = 49
  Left   = 93

  * --- Standard Properties
  Width  = 470
  Height = 175
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-08"
  HelpContextID=90842473
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gscg_scc"
  cComment = "Controllo congruenza primanota"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sccPag1","gscg_scc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ESERCIZI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CODESE=space(4)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
        .w_CODAZI = i_CODAZI
        .w_CODESE = g_CODESE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODESE))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_DATINI =  .w_INIESE
        .w_DATFIN =  .w_FINESE
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate(AH_Msgformat("Questa funzione stampa le eventuali incongruenze nelle registrazioni contabili %0rispetto alle impostazioni sulle relative causali contabili,%0corregge su opzione alcune di queste incongruenze"))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CODESE<>.w_CODESE
            .w_DATINI =  .w_INIESE
        endif
        if .o_CODESE<>.w_CODESE
            .w_DATFIN =  .w_FINESE
        endif
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(AH_Msgformat("Questa funzione stampa le eventuali incongruenze nelle registrazioni contabili %0rispetto alle impostazioni sulle relative causali contabili,%0corregge su opzione alcune di queste incongruenze"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(AH_Msgformat("Questa funzione stampa le eventuali incongruenze nelle registrazioni contabili %0rispetto alle impostazioni sulle relative causali contabili,%0corregge su opzione alcune di queste incongruenze"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_2'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_2.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_2.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_7.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_7.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_8.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_8.value=this.w_DATFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_DATINI<=.w_DATFIN or  EMPTY(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale o precedente all'inizio dell'esercizio")
          case   not((.w_DATINI<=.w_DATFIN or EMPTY(.w_DATFIN)) OR EMPTY(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODESE = this.w_CODESE
    return

enddefine

* --- Define pages as container
define class tgscg_sccPag1 as StdContainer
  Width  = 466
  height = 175
  stdWidth  = 466
  stdheight = 175
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESE_1_2 as StdField with uid="GDKEQQYWYI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza di selezione",;
    HelpContextID = 183538138,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=90, Top=98, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDATINI_1_7 as StdField with uid="VPJLNVRTYN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale o precedente all'inizio dell'esercizio",;
    ToolTipText = "Data di registrazione di inizio selezione",;
    HelpContextID = 121348042,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=90, Top=124

  func oDATINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINI<=.w_DATFIN or  EMPTY(.w_DATFIN)))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_8 as StdField with uid="MCOUDCLKFB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data di registrazione di fine selezione",;
    HelpContextID = 42901450,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=216, Top=124

  func oDATFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINI<=.w_DATFIN or EMPTY(.w_DATFIN)) OR EMPTY(.w_DATINI))
    endwith
    return bRes
  endfunc


  add object oBtn_1_9 as StdButton with uid="PPRUTFWLAX",left=361, top=125, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 90813722;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        do GSCG_BAC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="UKELZNTFNI",left=412, top=125, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 83525050;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_13 as cp_calclbl with uid="QGOVNKOQIS",left=7, top=19, width=452,height=77,;
    caption='Stampa le eventuali incongruenze nelle registrazioni contabili',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 216402069

  add object oStr_1_3 as StdString with uid="OCDXQCRSNF",Visible=.t., Left=8, Top=4,;
    Alignment=0, Width=311, Height=15,;
    Caption="ATTENZIONE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="AJUCVXITQM",Visible=.t., Left=22, Top=98,;
    Alignment=1, Width=66, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="EHYDVQMOWB",Visible=.t., Left=54, Top=124,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ORMUKDZOZP",Visible=.t., Left=180, Top=124,;
    Alignment=1, Width=32, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_scc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
