* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kcw                                                        *
*              Selezione classe documentale                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-26                                                      *
* Last revis.: 2013-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kcw",oParentObject))

* --- Class definition
define class tgsut_kcw as StdForm
  Top    = 141
  Left   = 113

  * --- Standard Properties
  Width  = 583
  Height = 118
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-07"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  PROMCLAS_IDX = 0
  cPrg = "gsut_kcw"
  cComment = "Selezione classe documentale"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CURNAME = space(15)
  w_INFINITY = .F.
  w_CLASSE = space(15)
  o_CLASSE = space(15)
  w_VALUE = space(250)
  w_LICLADOC = space(15)
  w_DESCRI = space(40)
  w_ARCHIVIO = space(20)
  w_NOMEFILE = space(200)
  w_ARCHIVIA = space(10)
  w_MODALL = space(1)
  w_IDMSSA = space(1)
  w_CDPUBWEB = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kcwPag1","gsut_kcw",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLASSE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PROMCLAS'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CURNAME=space(15)
      .w_INFINITY=.f.
      .w_CLASSE=space(15)
      .w_VALUE=space(250)
      .w_LICLADOC=space(15)
      .w_DESCRI=space(40)
      .w_ARCHIVIO=space(20)
      .w_NOMEFILE=space(200)
      .w_ARCHIVIA=space(10)
      .w_MODALL=space(1)
      .w_IDMSSA=space(1)
      .w_CDPUBWEB=space(1)
      .w_CLASSE=oParentObject.w_CLASSE
      .w_LICLADOC=oParentObject.w_LICLADOC
      .w_DESCRI=oParentObject.w_DESCRI
      .w_ARCHIVIO=oParentObject.w_ARCHIVIO
      .w_IDMSSA=oParentObject.w_IDMSSA
        .w_CURNAME = IIF(Type("This.oParentObject .w_NAMECUR")<>'U', This.oParentObject .w_NAMECUR, "__TMP__")
        .w_INFINITY = Type("This.oParentObject .w_INFINITY")<>'U'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CLASSE))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_LICLADOC = .w_CLASSE
          .DoRTCalc(6,8,.f.)
        .w_ARCHIVIA = 'N'
    endwith
    this.DoRTCalc(10,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_kcw
    If This.w_INFINITY and g_DOCM='S'
       this.cComment = "Selezione classe documentale"
     else
       this.cComment = "Selezione classe libreria allegati"
    Endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CLASSE=.w_CLASSE
      .oParentObject.w_LICLADOC=.w_LICLADOC
      .oParentObject.w_DESCRI=.w_DESCRI
      .oParentObject.w_ARCHIVIO=.w_ARCHIVIO
      .oParentObject.w_IDMSSA=.w_IDMSSA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- gsut_kcw
    this.oParentObject.w_CONFMASK=.T.
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CLASSE<>.w_CLASSE
            .w_LICLADOC = .w_CLASSE
        endif
        if .o_CLASSE<>.w_CLASSE
          .Calculate_MSAYSPTSUC()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_MSAYSPTSUC()
    with this
          * --- Valuta il nome file
          .w_NOMEFILE = SetNameFile(.w_VALUE, .w_CURNAME)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oARCHIVIA_1_10.enabled = this.oPgFrm.Page1.oPag.oARCHIVIA_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oNOMEFILE_1_9.visible=!this.oPgFrm.Page1.oPag.oNOMEFILE_1_9.mHide()
    this.oPgFrm.Page1.oPag.oARCHIVIA_1_10.visible=!this.oPgFrm.Page1.oPag.oARCHIVIA_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CLASSE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASSE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_CLASSE)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDNOMFIL,CDMODALL,CDPUBWEB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_CLASSE))
          select CDCODCLA,CDDESCLA,CDNOMFIL,CDMODALL,CDPUBWEB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASSE)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStrODBC(trim(this.w_CLASSE)+"%");

            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDNOMFIL,CDMODALL,CDPUBWEB";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStr(trim(this.w_CLASSE)+"%");

            select CDCODCLA,CDDESCLA,CDNOMFIL,CDMODALL,CDPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLASSE) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oCLASSE_1_3'),i_cWhere,'GSUT_MCD',""+iif(iif(type('this.parent.ocontained')='O',this.parent.ocontained.w_INFINITY,oSource.parent.Parent.Parent.Parent.w_inFINITY) and g_DOCM='S',"Classi documentali","Classi allegati")+"",'GSUT_KCW.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDNOMFIL,CDMODALL,CDPUBWEB";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDNOMFIL,CDMODALL,CDPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASSE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDNOMFIL,CDMODALL,CDPUBWEB";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_CLASSE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_CLASSE)
            select CDCODCLA,CDDESCLA,CDNOMFIL,CDMODALL,CDPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASSE = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCRI = NVL(_Link_.CDDESCLA,space(40))
      this.w_VALUE = NVL(_Link_.CDNOMFIL,space(250))
      this.w_MODALL = NVL(_Link_.CDMODALL,space(1))
      this.w_CDPUBWEB = NVL(_Link_.CDPUBWEB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLASSE = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_VALUE = space(250)
      this.w_MODALL = space(1)
      this.w_CDPUBWEB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MODALL='S' OR inlist(.w_MODALL,'F','E','I') AND .w_IDMSSA<>'S' AND .w_CDPUBWEB='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida")
        endif
        this.w_CLASSE = space(15)
        this.w_DESCRI = space(40)
        this.w_VALUE = space(250)
        this.w_MODALL = space(1)
        this.w_CDPUBWEB = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASSE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCLASSE_1_3.value==this.w_CLASSE)
      this.oPgFrm.Page1.oPag.oCLASSE_1_3.value=this.w_CLASSE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_7.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_7.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE_1_9.value==this.w_NOMEFILE)
      this.oPgFrm.Page1.oPag.oNOMEFILE_1_9.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oARCHIVIA_1_10.RadioValue()==this.w_ARCHIVIA)
      this.oPgFrm.Page1.oPag.oARCHIVIA_1_10.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_MODALL='S' OR inlist(.w_MODALL,'F','E','I') AND .w_IDMSSA<>'S' AND .w_CDPUBWEB='S')  and not(empty(.w_CLASSE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLASSE_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida")
          case   (empty(.w_NOMEFILE))  and not(!.w_INFINITY)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMEFILE_1_9.SetFocus()
            i_bnoObbl = !empty(.w_NOMEFILE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_kcw
      if empty(this.w_CLASSE)
        =ah_ErrorMsg('Occorre indicare una classe documentale',48)
        i_bRes=.f.
      endif
      *--- 
      IF i_bRes and Chrtran(This.w_NOMEFILE, '\/":*?<>|', '_________')<>This.w_NOMEFILE
        =ah_ErrorMsg('I nomi di file non possono contenere i seguenti caratteri:%0        \ / " : * ? < > |',48)
        i_bRes=.f.
      endif
      IF i_bRes and this.w_MODALL $ "IS" and not isAlt()
         IF this.w_MODALL="S" 
            spclchar=Chrtran(upper(This.w_NOMEFILE), upper('^&,;'), '____')
         else
            spclchar=Chrtran(upper(This.w_NOMEFILE), upper('^&!,;�$%=�@[]{}�#������'), '_________________IEEOAU')
         endif
      IF spclchar<>upper(This.w_NOMEFILE) 
            i_bRes=ah_YesNo('Nome file non valido per pubblicazione su Infinity, nome proposto:%0%1%0Si desidera proseguire nella pubblicazione?',48,alltrim(spclchar))
            This.w_NOMEFILE=iif(i_bRes,spclchar,This.w_NOMEFILE)
         endif
      endif
      *--- 
      If i_bRes And This.w_INFINITY
         This.oParentObject.oParentObject.w_NOMEFILE = This.w_NOMEFILE
         This.oParentObject.oParentObject.w_INARCHIVIO = This.w_ARCHIVIA
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CLASSE = this.w_CLASSE
    return

enddefine

* --- Define pages as container
define class tgsut_kcwPag1 as StdContainer
  Width  = 579
  height = 118
  stdWidth  = 579
  stdheight = 118
  resizeXpos=302
  resizeYpos=33
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLASSE_1_3 as StdField with uid="SADHGOIDUV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CLASSE", cQueryName = "CLASSE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida",;
    ToolTipText = "Classe documentale",;
    HelpContextID = 77471014,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=133, Left=147, Top=11, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_CLASSE"

  func oCLASSE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASSE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASSE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oCLASSE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',""+iif(iif(type('this.parent.ocontained')='O',this.parent.ocontained.w_INFINITY,oSource.parent.Parent.Parent.Parent.w_inFINITY) and g_DOCM='S',"Classi documentali","Classi allegati")+"",'GSUT_KCW.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oCLASSE_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_CLASSE
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_7 as StdField with uid="BCQHKBKQOX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione classe",;
    HelpContextID = 142554678,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=283, Top=11, InputMask=replicate('X',40)

  add object oNOMEFILE_1_9 as StdField with uid="PISACFYZJZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Nome da attribuire al file per la pubblicazione",;
    HelpContextID = 138354405,;
   bGlobalFont=.t.,;
    Height=21, Width=425, Left=147, Top=36, InputMask=replicate('X',200)

  func oNOMEFILE_1_9.mHide()
    with this.Parent.oContained
      return (!.w_INFINITY)
    endwith
  endfunc

  add object oARCHIVIA_1_10 as StdCheck with uid="RLJXWIPVAG",rtseq=9,rtrep=.f.,left=147, top=63, caption="Archivia standard",;
    ToolTipText = "Se attivo effettua anche l'archiviazione standard",;
    HelpContextID = 185384121,;
    cFormVar="w_ARCHIVIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARCHIVIA_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oARCHIVIA_1_10.GetRadio()
    this.Parent.oContained.w_ARCHIVIA = this.RadioValue()
    return .t.
  endfunc

  func oARCHIVIA_1_10.SetRadio()
    this.Parent.oContained.w_ARCHIVIA=trim(this.Parent.oContained.w_ARCHIVIA)
    this.value = ;
      iif(this.Parent.oContained.w_ARCHIVIA=='S',1,;
      0)
  endfunc

  func oARCHIVIA_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODALL<>'I')
    endwith
   endif
  endfunc

  func oARCHIVIA_1_10.mHide()
    with this.Parent.oContained
      return (!.w_INFINITY or .w_MODALL='S')
    endwith
  endfunc


  add object oBtn_1_15 as StdButton with uid="ADLSFZBDXO",left=468, top=63, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per procedere con l'archiviazione";
    , HelpContextID = 224417302;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="THOBMELFTL",left=523, top=63, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Sospende l'operazione di archiviazione rapida";
    , HelpContextID = 91855802;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="JBWSDNOZTJ",Visible=.t., Left=7, Top=11,;
    Alignment=1, Width=138, Height=18,;
    Caption="Classe documentale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (g_DOCM#'S')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="YRIPPNDIBA",Visible=.t., Left=23, Top=36,;
    Alignment=1, Width=122, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (!.w_INFINITY)
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="VXKEDWUDCL",Visible=.t., Left=7, Top=11,;
    Alignment=1, Width=138, Height=18,;
    Caption="Classe allegati:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (g_DOCM='S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kcw','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kcw
Proc SetNameFile(pValue, pCursor)
   local l_oldErr, l_ret, l_Err
   l_ret = ""
   If !Empty(m.pValue)
     &&Salvo ON ERROR corrente
     l_oldErr = ON('ERROR')
     l_Err = .f.
     ON ERROR l_Err = .t.
     If !Empty(m.pCursor)
       Select(m.pCursor)
       go top
     EndIf
     l_ret = Eval(m.pValue)
     If l_Err
       ah_ErrorMsg("Impossibile valutare il nome file")
       l_ret = ""
     EndIf
     &&Ripristino ON ERROR
     If empty(m.l_oldErr)
       ON ERROR
     Else
       ON ERROR &l_oldErr
     Endif
   EndIf  
   return l_ret
EndProc
* --- Fine Area Manuale
