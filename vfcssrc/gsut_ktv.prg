* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ktv                                                        *
*              Automatic test for Visual query                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-16                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_ktv",oParentObject))

* --- Class definition
define class tgsut_ktv as StdForm
  Top    = 26
  Left   = 44

  * --- Standard Properties
  Width  = 619
  Height = 140
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-07-04"
  HelpContextID=82396009
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_ktv"
  cComment = "Automatic test for Visual query"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PATHSJ = space(200)
  w_LOGFILE = space(254)
  w_VQUERY = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_ktvPag1","gsut_ktv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATHSJ_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PATHSJ=space(200)
      .w_LOGFILE=space(254)
      .w_VQUERY=space(254)
        .w_PATHSJ = LEFT(sys(5)+curdir(),LEN(sys(5)+curdir())-4)
        .w_LOGFILE = ADDBS(SYS(2023))+Sys(2015)+'.dbf'
    endwith
    this.DoRTCalc(3,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPATHSJ_1_1.value==this.w_PATHSJ)
      this.oPgFrm.Page1.oPag.oPATHSJ_1_1.value=this.w_PATHSJ
    endif
    if not(this.oPgFrm.Page1.oPag.oLOGFILE_1_2.value==this.w_LOGFILE)
      this.oPgFrm.Page1.oPag.oLOGFILE_1_2.value=this.w_LOGFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oVQUERY_1_6.value==this.w_VQUERY)
      this.oPgFrm.Page1.oPag.oVQUERY_1_6.value=this.w_VQUERY
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_ktvPag1 as StdContainer
  Width  = 615
  height = 140
  stdWidth  = 615
  stdheight = 140
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPATHSJ_1_1 as StdField with uid="FWBPGWHNRK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PATHSJ", cQueryName = "PATHSJ",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Path stampe schedulatore",;
    HelpContextID = 177488630,;
   bGlobalFont=.t.,;
    Height=21, Width=549, Left=54, Top=7, InputMask=replicate('X',200), bHasZoom = .t. 

  proc oPATHSJ_1_1.mZoom
    this.parent.ocontained.w_PATHSJ=left(cp_getdir(IIF(EMPTY(this.parent.ocontained.w_PATHSJ),sys(5)+sys(2003),this.parent.ocontained.w_PATHSJ),"Percorso di destinazione")+space(200),200)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oLOGFILE_1_2 as StdField with uid="RUJMTTEGXY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LOGFILE", cQueryName = "LOGFILE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "File di log",;
    HelpContextID = 200376502,;
   bGlobalFont=.t.,;
    Height=21, Width=549, Left=54, Top=32, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oLOGFILE_1_2.mZoom
    this.parent.ocontained.w_LOGFILE=left(getfile(IIF(EMPTY(this.parent.ocontained.w_LOGFILE),sys(5)+sys(2003),this.parent.ocontained.w_LOGFILE),cp_translate("Log elaborazione"))+space(200),200)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_1_4 as StdButton with uid="ZMIZZNTUDJ",left=506, top=90, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare le impostazioni";
    , HelpContextID = 241194518;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        TestDirVqr(this.Parent.oContained, .w_PATHSJ  )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="JWIBHYOYEX",left=556, top=90, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 75078586;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oVQUERY_1_6 as StdField with uid="DKIACZKQFU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VQUERY", cQueryName = "VQUERY",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 159474518,;
   bGlobalFont=.t.,;
    Height=21, Width=549, Left=54, Top=60, InputMask=replicate('X',254)

  add object oStr_1_3 as StdString with uid="NYTFAGEZHB",Visible=.t., Left=8, Top=9,;
    Alignment=1, Width=43, Height=18,;
    Caption="Path :"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="KIWYJZIBGC",Visible=.t., Left=8, Top=33,;
    Alignment=1, Width=43, Height=18,;
    Caption="Log :"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ktv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_ktv
Function TestDirVqr
	* Copiate dalla CP_QUERY
	#define FILTERPARAMDIM 8
	#define aFP_Name    1
	#define aFP_Descr   2
	#define aFP_Type    3
	#define aFP_Len     4
	#define aFP_Dec     5
	#define aFP_Value   6
	#define aFP_HasValue 7
	#define aFP_Remove  8
	
	* --- Abstract
	* --- La procedura, partendo da una cartella esegue sul database corrente
	* --- le visual query e per ognuna salva tempo di esecuzione numero record
	* --- ed eventuale errore di esecuzione
	PARAMETERS obj,sDir
	IF PCOUNT()=0 
	 sdir=cp_GETDIR(SYS(5)+SYS(2003),'Directory','Visual query tester')
	ENDIF
	
	IF EMPTY(sDir)
	 return
	ENDIF
  	
	* ---- Esamino le query presenti nella cartella...
	LOCAL cLogfile, L_fptname
	cLogfile=obj.w_logfile
	* --- variabile per appoggiore gli errori
	PUBLIC G_TESTVAR
	* --- Creo la tabella che conterr� il responso..
	IF FILE(cLogfile)
		ERASE &cLogfile	 	 
		L_fptname=Strtran( clogFile, '.dbf','.fpt' )
		ERASE &L_fptname
	endif
	CREATE table &cLogfile(VQR C(254),numrec N(10),DURATA N(12,4), OK L ,ERROR_MSG M(10)) 
	m.oField = m.Obj.getctrl("w_VQUERY")
	Naviga( sDir , Obj,oField,cLogfile)
	Cp_Msg(Cp_Translate('Esecuzione terminata'))
	SELECT ( JUSTSTEM( cLogfile ) )
	BROWSE
	use
	g_TESTVAR=.T.
return

PROCEDURE Naviga( cDir , Obj,oField,cLogfile)
	LOCAL n_i,fso,folder
	TestallVqr(cDir,Obj,oField,cLogFile)
	fso = CreateObject("Scripting.FileSystemObject") 
	folder = fso.GetFolder(cDir)
	For Each myFile in folder.SubFolders
	IF MYFILE.ATTRIBUTES>=16
	 Naviga( Myfile.path , Obj,oField,cLogfile) 
	ENDIF
	EndFor
	fso = null
Endproc


Procedure TestallVqr(sDir,Obj,oField,cLogFile)
LOCAL nfile, bRes, i_Crs, nRec, ntime
nfile=ADIR(avqr,ADDBS(sDir)+'*.vqr')
if nfile<=0
 * --- Nessun file presente..
 return
else
	FOR n_i=1 TO nfile
		Obj.w_VQUERY='Testing '+Alltrim(ADDBS(sDir)+avqr[n_i,1])+'...'
		obj.SetControlsValue()
		obj.Draw()  
		oField.SelStart = len(alltrim(Obj.w_VQUERY)) 
		
		G_TESTVAR=''
		i_Crs=SYS(2015)
		ntime=SECONDS()
		nRec=-1  
		
		bRes=TestVqr( ADDBS(sDir)+avqr[n_i,1], i_Crs )
		ntime=SECONDS()-ntime
		IF USED(i_crs)
			nRec=RECNO( i_crs )	
			SELECT(i_crs)
			Use
		ENDIF		
		
		INSERT INTO &cLogFile values(ADDBS(sDir)+avqr[n_i,1],nrec,ntime,bres,G_TESTVAR)
		flush
		
	ENDFOR
endif
Endproc


Function TestVqr(i_cQueryFile,I_CRS)
	LOCAL i_loader,i_qry, i_oParentObject,i_indexes,bRes, n_i
	bRes=.f.	
	if At('cp_query',lower(set('proc')))=0
	  set proc to cp_query additive
	ENDIF
	* --- elimino l'estensione se presente...
	i_cQueryFile=ALLTRIM(i_cQueryFile)
	IF UPPER(RIGHT(i_cQueryFile,4))='.VQR'
		i_cQueryFile=ADDBS(JUSTPATH(i_cQueryFile))+JUSTSTEM(i_cQueryFile)
	ENDIF
	i_qry=createobject('cpquery')
	i_loader=createobject('cpqueryloader')
	i_oParentObject=createobject('oObjForAllSeason')
	i_loader.LoadQuery(i_cQueryFile,i_qry,i_oParentObject) 

	i_indexes=''

    if i_qry.mDoQuery(i_crs,'Exec',.f.,.f.,.f.,@i_indexes,.f.)
		bRes=.t.			    	
    ELSE
     	bRes=.f.
    ENDIF
    * Rimuovo gli oggetti
    i_oParentObject=.null.
	i_qry=.null.
Return(bRes)
Endfunc


* --- oParentobject per test..
DEFINE CLASS oObjForAllSeason as Custom
Func IsVar(cVarName)
return(.f.)

Enddefine
* --- Fine Area Manuale
