* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpk                                                        *
*              Primanota controlli finali 2                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_481]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-24                                                      *
* Last revis.: 2016-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpk",oParentObject)
return(i_retval)

define class tgscg_bpk as StdBatch
  * --- Local variables
  w_GSCG_MIV = .NULL.
  w_LTOTACC = 0
  w_LNUMACC = 0
  w_ROWORD = 0
  w_SERIAL = space(10)
  w_DESCRIZIONE_RIGA = space(50)
  w_PADRE = .NULL.
  w_SRV = space(1)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_TOTPNT_VAL = 0
  w_IMPDOC = .f.
  w_TOTPARCON = 0
  w_SERDIF = space(10)
  w_NUMERO = 0
  w_DATGEN = ctod("  /  /  ")
  w_CHKCOR = .f.
  w_TIPPAGAM = space(2)
  w_SERACC = space(10)
  w_ORDACC = 0
  w_RNUMACC = 0
  w_CSACC = space(1)
  w_ACTNUM = 0
  w_NUMPARACC = space(31)
  w_TOTCONF = 0
  w_TOTSOSP = 0
  w_TOTCONP = 0
  w_TOTRITE = 0
  w_IMPCONTA = 0
  w_IMPOPART = 0
  w_NUMRIGA = 0
  w_CODAZI = space(5)
  w_TOTPART = 0
  w_RESTO = 0
  w_PARTITE = .f.
  w_TOTDAVER = 0
  w_OK = .f.
  w_TOTIVA = 0
  w_OREC = 0
  w_IVADET = 0
  w_DIFIVA = 0
  w_TROV = .f.
  w_APPO = 0
  w_IVA = 0
  w_CFUNC = space(10)
  w_MESS = space(10)
  w_DATRIF = ctod("  /  /  ")
  w_PARACC = 0
  w_DATACC = ctod("  /  /  ")
  w_PAGACC = space(10)
  w_DESACC = space(50)
  w_RIFSER = space(10)
  w_RIFORD = 0
  w_RIFNUM = 0
  w_AbsRow = 0
  w_RECPOS = 0
  w_nRelRow = 0
  w_NUMCOR = space(25)
  w_CALFES = space(3)
  w_CONBAN = space(15)
  w_CAUMOV = space(5)
  w_DATVAL = ctod("  /  /  ")
  w_CCFLCRED = space(1)
  w_CCFLDEBI = space(1)
  w_CCFLCOMM = space(1)
  w_CCIMPCRE = 0
  w_CCIMPDEB = 0
  w_COSCOM = 0
  w_COMVAL = 0
  w_IMPCRE = 0
  w_IMPDEB = 0
  w_NUMRIG = 0
  w_TIPCAU1 = space(1)
  w_FLCRDE = space(1)
  w_TIPSOT = space(1)
  w_BAVAL = space(3)
  w_ROWORD = 0
  w_TOTPNT = 0
  w_CODPAG = space(5)
  w_MESE1 = 0
  w_NURATE = 0
  w_IMPDAR = 0
  w_TOTPAR = 0
  w_TIPCON = space(1)
  w_MESE2 = 0
  w_DATINI = ctod("  /  /  ")
  w_IMPAVE = 0
  w_CAMB = 0
  w_CODCON = space(15)
  w_GIORN1 = 0
  w_NETTO = 0
  w_NR = space(10)
  w_OKPAR = space(1)
  w_ROWNUM = 0
  w_GIORN2 = 0
  w_ESCL1 = space(4)
  w_OREC = 0
  w_TOTCOS = 0
  w_CCTAGG = space(1)
  w_GIOFIS = 0
  w_ESCL2 = space(4)
  w_FLABAN = space(6)
  w_APPO = space(10)
  w_FLSALD = space(1)
  w_BANAPP = space(10)
  w_IVA = 0
  w_DATCAM = ctod("  /  /  ")
  w_RIGPAR = 0
  w_RIGIVA = 0
  w_PERIVA = 0
  w_MESS = space(10)
  w_RABBIN = 0
  w_OK = .f.
  w_SEZB = space(1)
  w_OANAL = space(1)
  w_ROWABB = 0
  w_ABDAVE = space(1)
  w_CODAGE = space(1)
  w_FLVABD = space(1)
  w_PTROW = 0
  w_ABDIFF = 0
  w_IABBIN = 0
  w_RESTO = 0
  w_ROWINI = 0
  w_BANNOS = space(15)
  w_ARRSCA = space(10)
  w_INICOM = ctod("  /  /  ")
  w_FINCOM = ctod("  /  /  ")
  w_DESRIG = space(50)
  w_RIGACC = space(50)
  w_PTDATSCA = ctod("  /  /  ")
  w_MRCODICE = space(15)
  w_PTNUMPAR = space(31)
  w_MRPARAME = 0
  w_PT_SEGNO = space(1)
  w_MR_SEGNO = space(1)
  w_PTTOTIMP = 0
  w_MRTOTIMP = 0
  w_PTMODPAG = space(10)
  w_TOTPAC = 0
  w_PTBANAPP = space(10)
  w_ITOT = 0
  w_PTDATAPE = ctod("  /  /  ")
  w_RTOT = 0
  w_PTBANNOS = space(15)
  w_MRCODVOC = space(15)
  w_PTCODVAL = space(3)
  w_PTIMPDOC = 0
  w_PTCAOVAL = 0
  w_PTCAOAPE = 0
  w_PTROWNUM = 0
  w_PTTOTABB = 0
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_CONCOL = space(15)
  w_CCNUMCOR = space(15)
  w_TOTCON = 0
  w_PTDESRIG = space(50)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PTCODAGE = space(5)
  w_TOTIMP = 0
  w_ROWPAR = 0
  w_ROWCOUNT = 0
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = ctod("  /  /  ")
  w_PTNUMCOR = space(25)
  w_FIND = .f.
  w_RITDARIP = 0
  w_TOTABB = 0
  w_FLSOSP = space(1)
  w_TEST = space(1)
  w_AGGSERRIF = space(10)
  w_AGGORDRIF = 0
  w_AGGNUMRIF = 0
  w_LOKPAR = space(1)
  w_IMPORI = 0
  w_RESIDUO = 0
  w_FLVABD1 = space(1)
  w_CONTA = 0
  w_LTOTPAR = 0
  w_LRESIDUO = 0
  w_LNUMPAR = space(31)
  w_LSEGNO = space(1)
  w_ROWACCGEN = 0
  w_OKENA = .f.
  w_SEGNO = space(1)
  w_ENASARCO = 0
  w_RIFENA = 0
  w_SEGSOSP = space(1)
  w_TOTSALD = 0
  w_TOTENA = 0
  w_TOTMDR1 = 0
  w_RITERIP = 0
  w_IMPPAR = 0
  w_FIRSTROW = 0
  * --- WorkFile variables
  CCM_DETT_idx=0
  COC_MAST_idx=0
  COLLCENT_idx=0
  MOVICOST_idx=0
  PAR_TITE_idx=0
  COC_DETT_idx=0
  CCC_MAST_idx=0
  DATIRITE_idx=0
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  MOD_PAGA_idx=0
  ESI_DETT_idx=0
  ESI_DIF_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali dalla Primanota (da GSCG_MPN)
    * --- Viene eseguito, sotto Transazione, in A.M. Replace End
    * --- Legge dalla Primanota
    this.w_GSCG_MIV = this.oParentObject.GSCG_MIV
    this.w_OK = .T.
    this.w_MESS = Ah_MsgFormat("Operazione abbandonata")
    this.w_CFUNC = this.oParentObject.cFunction
    * --- Eventuale Acconto da stornare dalle Scadenze
    this.w_PARACC = 0
    this.w_DATACC = cp_CharToDate("  -  -  ")
    this.w_PAGACC = " "
    this.w_PARACC = 0
    this.w_LTOTACC = this.oParentObject.w_TOTACC
    this.w_LNUMACC = this.oParentObject.w_NUMACC
    * --- Forza i Controlli dal Castelletto IVA anche se non e' stato variato.
    if this.oParentObject.w_PNTIPREG $ "VACE" AND NOT (this.oParentObject.w_PNTIPREG="E" AND this.oParentObject.w_PNTIPDOC="CO") And this.w_CFUNC<>"Load" And (this.oParentObject.w_ODATREG<>this.oParentObject.w_PNDATREG OR this.oParentObject.w_OCOMIVA<>this.oParentObject.w_PNCOMIVA)
      * --- Esegue Controlli su Reg.IVA se Variazione e non variato GSCG_MIV
      this.w_GSCG_MIV.NotifyEvent("Controlla")     
    endif
    if Not btrserr
      if this.w_OK AND ((g_PERPAR="S" AND this.oParentObject.w_FLPART $ "CSAN") OR (this.oParentObject.w_FLANAL="S" AND g_PERCCR="S") OR ( g_BANC="S" AND this.oParentObject.w_FLMOVC="S"))
        * --- Esegue Controllo sulle Partite Collegate / Centri di Costo
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Eseguo una write sulle partite per valorizzare il campo PTDATREG sulle partite create.
      *     La Write viene eseguita sempre. Non � stato inserito il campo direttamente nelle insert in quanto non so 
      *     se la partita � stata generata in automatico oppure dal bottone calcola.
      if this.w_OK
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTDATREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATREG),'PAR_TITE','PTDATREG');
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
              +" and PTROWORD > "+cp_ToStrODBC(0);
                 )
        else
          update (i_cTable) set;
              PTDATREG = this.oParentObject.w_PNDATREG;
              &i_ccchkf. ;
           where;
              PTSERIAL = this.oParentObject.w_PNSERIAL;
              and PTROWORD > 0;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.oParentObject.w_OLDSERIAL<>this.oParentObject.w_PNSERIAL
          * --- Write into PAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
            do vq_exec with 'GSCG2BPK',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
                    +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
                    +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERRIF');
                +i_ccchkf;
                +" from "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
                    +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
                    +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 set ";
            +"PAR_TITE.PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERRIF');
                +Iif(Empty(i_ccchkf),"",",PAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="PAR_TITE.PTSERIAL = t2.PTSERIAL";
                    +" and "+"PAR_TITE.PTROWORD = t2.PTROWORD";
                    +" and "+"PAR_TITE.CPROWNUM = t2.CPROWNUM";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set (";
                +"PTSERRIF";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERRIF')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
                    +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
                    +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set ";
            +"PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERRIF');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
                    +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
                    +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERRIF');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        if Not Empty(this.oParentObject.w_CCDESSUP) or Not Empty(this.oParentObject.w_CCDESRIG)
          * --- Array elenco parametri per descrizioni di riga e testata
           
 DIMENSION ARPARAM[13,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.oParentObject.w_PNNUMDOC,15)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.oParentObject.w_PNALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.oParentObject.w_PNDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=this.oParentObject.w_PNALFPRO 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.oParentObject.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.oParentObject.w_PNCODCLF) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.oParentObject.w_PNCODAGE) 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]=ALLTRIM(STR(this.oParentObject.w_PNNUMPRO,15)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=this.oParentObject.w_PNTIPCLF 
 ARPARAM[13,1]="NUMFAT" 
 ARPARAM[13,2]=this.oParentObject.w_PNNUMFAT
          if Empty(this.oParentObject.w_PNDESSUP) and Not Empty(this.oParentObject.w_CCDESSUP)
            this.oParentObject.w_PNDESSUP = CALDESPA(this.oParentObject.w_CCDESSUP,@ARPARAM)
            * --- Write into PNT_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PNDESSUP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDESSUP),'PNT_MAST','PNDESSUP');
                  +i_ccchkf ;
              +" where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                     )
            else
              update (i_cTable) set;
                  PNDESSUP = this.oParentObject.w_PNDESSUP;
                  &i_ccchkf. ;
               where;
                  PNSERIAL = this.oParentObject.w_PNSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if Not Empty(this.oParentObject.w_CCDESRIG)
            * --- Select from gscg1bpk
            do vq_exec with 'gscg1bpk',this,'_Curs_gscg1bpk','',.f.,.t.
            if used('_Curs_gscg1bpk')
              select _Curs_gscg1bpk
              locate for 1=1
              do while not(eof())
               
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(Nvl(_Curs_gscg1bpk.PTCODAGE," ")) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=Alltrim(_Curs_gscg1bpk.PTNUMPAR) 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=DTOC(CP_TODATE(_Curs_gscg1bpk.PTDATSCA)) 
 ARPARAM[6,1]="CODCON"
              this.w_DESCRIZIONE_RIGA = CALDESPA(this.oParentObject.w_CCDESRIG,@ARPARAM)
              * --- Aggiorno descrizione parametrica
              * --- Write into PNT_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PNT_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRIZIONE_RIGA),'PNT_DETT','PNDESRIG');
                    +i_ccchkf ;
                +" where ";
                    +"PNSERIAL = "+cp_ToStrODBC(_Curs_gscg1bpk.PTSERIAL);
                    +" and CPROWNUM = "+cp_ToStrODBC(_Curs_gscg1bpk.PTROWORD);
                       )
              else
                update (i_cTable) set;
                    PNDESRIG = this.w_DESCRIZIONE_RIGA;
                    &i_ccchkf. ;
                 where;
                    PNSERIAL = _Curs_gscg1bpk.PTSERIAL;
                    and CPROWNUM = _Curs_gscg1bpk.PTROWORD;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Anche nelle partite
              * --- Write into PAR_TITE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PTDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRIZIONE_RIGA),'PAR_TITE','PTDESRIG');
                    +i_ccchkf ;
                +" where ";
                    +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                    +" and PTROWORD = "+cp_ToStrODBC(_Curs_gscg1bpk.PTROWORD);
                       )
              else
                update (i_cTable) set;
                    PTDESRIG = this.w_DESCRIZIONE_RIGA;
                    &i_ccchkf. ;
                 where;
                    PTSERIAL = this.oParentObject.w_PNSERIAL;
                    and PTROWORD = _Curs_gscg1bpk.PTROWORD;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
                select _Curs_gscg1bpk
                continue
              enddo
              use
            endif
          endif
        endif
      endif
      * --- Cerco partite in distinta contabilizzata  che non hanno la relativa partita di saldo in PN
      *     Se la trovo, azzero ptflimpe
      if this.w_OK AND this.w_CFUNC="Edit"
        * --- Try
        local bErr_0504F8D0
        bErr_0504F8D0=bTrsErr
        this.Try_0504F8D0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0504F8D0
        * --- End
      endif
      if this.oParentObject.w_FLPART $ "S-A" AND Isalt()
        * --- Aggiorno stato incasso documenti collegati
        gsal_baf(this,this.oParentObject.w_PNSERIAL,"P",Cp_chartodate("  -  -    "),Cp_chartodate("  -  -    "),"A")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Not this.w_Ok
        if Type("this.w_oMess")="O"
          this.w_MESS = this.w_oMess.ComposeMessage()
        endif
        * --- Abbandona la Transazione
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    endif
  endproc
  proc Try_0504F8D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'gscgdbck',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
      +",PTRIFIND ="+cp_NullLink(cp_ToStrODBC(Space(10)),'PAR_TITE','PTRIFIND');
          +i_ccchkf;
          +" from "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 set ";
      +"PAR_TITE.PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
      +",PAR_TITE.PTRIFIND ="+cp_NullLink(cp_ToStrODBC(Space(10)),'PAR_TITE','PTRIFIND');
          +Iif(Empty(i_ccchkf),"",",PAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="PAR_TITE.PTSERIAL = t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set (";
          +"PTFLIMPE,";
          +"PTRIFIND";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE')+",";
          +cp_NullLink(cp_ToStrODBC(Space(10)),'PAR_TITE','PTRIFIND')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set ";
      +"PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
      +",PTRIFIND ="+cp_NullLink(cp_ToStrODBC(Space(10)),'PAR_TITE','PTRIFIND');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
      +",PTRIFIND ="+cp_NullLink(cp_ToStrODBC(Space(10)),'PAR_TITE','PTRIFIND');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Partite/Centri di Costo
    * --- Riga di Riferimanto nel caso di Righe P.N. con Flag Abbina Partite
    this.w_RABBIN = 0
    this.w_IABBIN = 0
    * --- Legge Importo sulla riga Temporaneo Primanota
    this.w_ROWCOUNT = 0
    this.w_PADRE = This.oParentObject
    * --- Memorizzo la posiziono del transitorio per rimettermici al termine della Scan
    this.w_PADRE.MarkPos()     
    this.w_IMPDOC = .F.
    if this.oParentObject.w_PNCODVAL<>this.oParentObject.w_PNVALNAZ AND this.oParentObject.w_PNCAOVAL<>0
      * --- Verifico quante righe sono presenti dello stesso conto 
      *     perch� se c'� pi� di una riga il totale delle partite deve essere calcolato
      *     sulla base del valore della riga di primanota e non dal totale documento
      this.w_PADRE.Exec_Select("TMP_RIG","Count(*) As Conta","NVL(t_PNCODCON, SPACE(10)) = "+cp_ToStr( this.oParentObject.w_PNCODCLF ) + " AND NVL(t_PNTIPCON, ' ') = "+cp_ToStr( this.oParentObject.w_PNTIPCLF ),"","")
      if TMP_RIG.Conta<2 and .w_PNTOTDOC <>0
        this.w_TOTPNT_VAL = this.oParentObject.w_PNTOTDOC
        this.w_IMPDOC = .T.
      endif
      Use in TMP_RIG
    endif
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    SCAN FOR (t_PNIMPDAR<>0 OR t_PNIMPAVE<>0 OR t_PNFLZERO="S") AND NOT EMPTY(t_PNCODCON)
    this.w_ROWCOUNT = this.w_ROWCOUNT + 1
    this.w_ROWORD = CPROWNUM
    this.w_IMPDAR = cp_ROUND(t_PNIMPDAR, this.oParentObject.w_DECTOP)
    this.w_IMPAVE = cp_ROUND(t_PNIMPAVE, this.oParentObject.w_DECTOP)
    this.w_TOTPNT = this.w_IMPDAR - this.w_IMPAVE
    this.w_NR = ALLTRIM(STR(t_CPROWORD))
    this.w_SRV = Nvl( i_SRV , " ")
    this.w_OKPAR = t_PNFLPART
    if this.w_OKPAR="S"
      this.w_TOTPNT_VAL = this.w_TOTPNT
    else
      if ! this.w_IMPDOC
        if this.oParentObject.w_PNVALNAZ<>this.oParentObject.w_PNCODVAL
          this.w_TOTPNT_VAL = cp_ROUND(MON2VAL(this.w_TOTPNT, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNVALNAZ), this.oParentObject.w_DECTOT)
        else
          this.w_TOTPNT_VAL = this.w_TOTPNT
        endif
      else
        this.w_TOTPNT_VAL = this.oParentObject.w_PNTOTDOC*IIF(this.w_IMPAVE>0 OR this.w_IMPDAR<0,-1,1)
      endif
    endif
    this.w_CODPAG = IIF(this.w_OKPAR="A" AND NOT EMPTY(g_SAPAGA), g_SAPAGA, t_PNCODPAG)
    this.w_CCTAGG = t_CCTAGG
    this.w_TIPCON = t_PNTIPCON
    this.w_CODCON = t_PNCODCON
    this.w_TIPSOT = NVL(t_TIPSOT," ")
    this.w_CONBAN = NVL(t_CONBAN," ")
    this.w_CAUMOV = NVL(t_CAUMOV," ")
    this.w_CALFES = NVL(t_CALFES," ")
    this.w_BAVAL = NVL(t_BAVAL,SPACE(3))
    this.w_MESE1 = NVL(t_CLMES1, 0)
    this.w_MESE2 = NVL(t_CLMES2, 0)
    this.w_GIORN1 = NVL(t_CLGIO1, 0)
    this.w_GIORN2 = NVL(t_CLGIO2, 0)
    this.w_GIOFIS = NVL(t_GIOFIS, 0)
    this.w_BANAPP = t_BANAPP
    this.w_BANNOS = t_BANNOS
    this.w_FLABAN = t_PNFLABAN
    this.w_SEZB = t_SEZB
    this.w_OANAL = t_OANAL
    this.w_INICOM = t_PNINICOM
    this.w_FINCOM = t_PNFINCOM
    this.w_DESRIG = t_PNDESRIG
    this.w_CODAGE = NVL(t_PNCODAGE,SPACE(5))
    this.w_FLVABD = NVL(t_PNFLVABD," ")
    this.w_NUMCOR = NVL( this.w_PADRE.Get("t_NUMCOR") , SPACE(25))
    * --- Read from ESI_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESI_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESI_DETT_idx,2],.t.,this.ESI_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DPSERIAL"+;
        " from "+i_cTable+" ESI_DETT where ";
            +"DPSERMOV = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DPSERIAL;
        from (i_cTable) where;
            DPSERMOV = this.oParentObject.w_PNSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SERDIF = NVL(cp_ToDate(_read_.DPSERIAL),cp_NullValue(_read_.DPSERIAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ESI_DIF
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESI_DIF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESI_DIF_idx,2],.t.,this.ESI_DIF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "EDNUMERO,EDDATGEN"+;
        " from "+i_cTable+" ESI_DIF where ";
            +"EDSERIAL = "+cp_ToStrODBC(this.w_SERDIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        EDNUMERO,EDDATGEN;
        from (i_cTable) where;
            EDSERIAL = this.w_SERDIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NUMERO = NVL(cp_ToDate(_read_.EDNUMERO),cp_NullValue(_read_.EDNUMERO))
      this.w_DATGEN = NVL(cp_ToDate(_read_.EDDATGEN),cp_NullValue(_read_.EDDATGEN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows<>0 AND (this.oparentobject.RowStatus() $"A-U" or deleted()) and g_lemc="S" AND this.oParentObject.w_FLPDIF="S"
      this.w_MESS = Ah_MsgFormat("Movimento di storno ad esig. differita,%0Impossibile modificare la primanota contabile.",this.w_NR)
      this.w_OK = .F.
    endif
    if this.w_CFUNC="Load" AND this.w_OKPAR="C" AND this.w_TIPCON=this.oParentObject.w_PNTIPCLF AND this.w_CODCON=this.oParentObject.w_PNCODCLF
      * --- Eventuale Abbinamento Partite
      this.w_RABBIN = this.w_ROWORD
      this.w_IABBIN = this.w_TOTPNT
    endif
    if this.w_OKPAR $ "CSA"
      if this.w_OKPAR="S" and this.w_CFUNC<>"Query" and this.w_OK
        * --- Verifico la presenza di Partite di Saldo Contestuale non collegate correttamente
        *     alle relative partite di creazione (eseguito calcola in modifica nella riga di creazione) 
        * --- Select from GSCG_BPK
        do vq_exec with 'GSCG_BPK',this,'_Curs_GSCG_BPK','',.f.,.t.
        if used('_Curs_GSCG_BPK')
          select _Curs_GSCG_BPK
          locate for 1=1
          do while not(eof())
          this.w_MESS = Ah_MsgFormat("Verificare partite riga primanota %1 esistono partite di saldo non collegate correttamente alle partite di creazione,%0eseguire aggiornamento partite di saldo.",this.w_NR)
          this.w_OK = .F.
            select _Curs_GSCG_BPK
            continue
          enddo
          use
        endif
      endif
      * --- Legge Partite Associate
      this.w_TROV = .F.
      this.w_TOTPAR = 0
      this.w_CHKCOR = .F.
      if (this.w_TIPCON="F" OR (this.w_IMPAVE<>0 AND this.w_TIPCON="C" AND this.oParentObject.w_PNTIPDOC $ "NC-NE-NU" ) ) AND upper(SUBSTR(CHTIPPAG(this.w_CODPAG, "BO"),1,2))="BO" 
        this.w_CHKCOR = .T.
      endif
      * --- Select from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
            +" where PTSERIAL="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+" AND PTROWORD="+cp_ToStrODBC(this.w_ROWORD)+"";
             ,"_Curs_PAR_TITE")
      else
        select * from (i_cTable);
         where PTSERIAL=this.oParentObject.w_PNSERIAL AND PTROWORD=this.w_ROWORD;
          into cursor _Curs_PAR_TITE
      endif
      if used('_Curs_PAR_TITE')
        select _Curs_PAR_TITE
        locate for 1=1
        do while not(eof())
        this.w_PTMODPAG = NVL(_Curs_PAR_TITE.PTMODPAG, SPACE(10))
        * --- Read from MOD_PAGA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MPTIPPAG"+;
            " from "+i_cTable+" MOD_PAGA where ";
                +"MPCODICE = "+cp_ToStrODBC(this.w_PTMODPAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MPTIPPAG;
            from (i_cTable) where;
                MPCODICE = this.w_PTMODPAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPPAGAM = NVL(cp_ToDate(_read_.MPTIPPAG),cp_NullValue(_read_.MPTIPPAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_CHKCOR AND Empty(Nvl(_Curs_PAR_TITE.PTNUMCOR, Space(25))) AND this.w_TIPPAGAM="BO"
          this.w_MESS = Ah_MsgFormat("Verificare partite riga primanota %1, pagamento di tipo bonifico, inserire un conto corrente!",this.w_NR)
          this.w_OK = .F.
        endif
        this.w_DATCAM = IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC)
        this.w_PTTIPCON = NVL(_Curs_PAR_TITE.PTTIPCON, " ")
        this.w_PTCODCON = NVL(_Curs_PAR_TITE.PTCODCON, SPACE(15))
        this.w_PTCODVAL = NVL(_Curs_PAR_TITE.PTCODVAL, SPACE(3))
        * --- Valuta partite diversa dlla valuta della registrazione di primanota
        if this.oParentObject.w_PNCODVAL <> this.w_PTCODVAL AND this.w_OKPAR<>"S"
          this.w_MESS = Ah_MsgFormat("Verificare partite riga primanota %1 codice valuta diverso %0dalla valuta della registrazione di primanota",this.w_NR)
          this.w_OK = .F.
        else
          if EMPTY(this.w_PTMODPAG)
            this.w_MESS = Ah_MsgFormat("Verificare partite riga primanota %1. Codice tipo pagamento inesistente",this.w_NR)
            this.w_OK = .F.
          else
            if this.w_PTTIPCON<>this.w_TIPCON OR this.w_PTCODCON<>this.w_CODCON
              * --- Errore!
              this.w_MESS = Ah_MsgFormat("Riga primanota %1 riferimento codice conto, incongruente con le partite associate",this.w_NR)
              this.w_OK = .F.
            else
              if this.w_OKPAR = "S"
                * --- Se Chiude: Il Test di Corrispondenza Primanota/Partite va riferito, se esiste, al Cambio della Partita di Apertura (PTCAOAPE)
                this.w_CAMB = IIF(NVL(PTCAOAPE,0)=0, IIF(NVL(PTCAOVAL,0)=0, this.oParentObject.w_PNCAOVAL, PTCAOVAL), PTCAOAPE)
                * --- Se esiste il Cambio di Apertura
                this.w_DATCAM = IIF(NVL(PTCAOAPE,0)=0, this.w_DATCAM, CP_TODATE(PTDATAPE))
                this.w_RIGPAR = NVL(PTTOTIMP,0)
                * --- Converte in Moneta di Conto (va fatto riga x riga in quanto, sebbene della stessa valuta,  il cambio di apertura puo' differire)
                if this.w_PTCODVAL<>this.oParentObject.w_PNVALNAZ
                  this.w_RIGPAR = cp_ROUND(VAL2MON(this.w_RIGPAR, this.w_CAMB, this.oParentObject.w_CAONAZ, this.w_DATCAM, this.oParentObject.w_PNVALNAZ),this.oParentObject.w_DECTOP)
                endif
                this.w_TOTPAR = this.w_TOTPAR + (this.w_RIGPAR * IIF(NVL(PT_SEGNO," ")="D", 1, -1))
              else
                * --- Se Crea partite Calcola il Totale Importi Partite alla Valuta Partite
                this.w_CAMB = IIF(NVL(PTCAOVAL,0)=0, this.oParentObject.w_PNCAOVAL, PTCAOVAL)
                this.w_TOTPAR = this.w_TOTPAR + (NVL(PTTOTIMP,0) * IIF(NVL(PT_SEGNO," ")="D", 1, -1))
              endif
            endif
          endif
        endif
        * --- Sono in una riga di saldo, ho una creazione che punta ad un'altra
        *     partita, questo identifica un saldo di un acconto
        if this.w_OK And this.w_OKPAR = "S" And NVL(_Curs_PAR_TITE.PTFLCRSA,"")="C" And Not Empty( NVL(_Curs_PAR_TITE.PTSERRIF, SPACE(10)) )
          * --- Leggo se la partita puntata � di tipo acconto...
          *     (Controllo ridondante inserito per esseri certi sulla modifica)
          this.w_SERACC = NVL(_Curs_PAR_TITE.PTSERRIF, SPACE(10))
          this.w_ORDACC = NVL(_Curs_PAR_TITE.PTORDRIF, 0)
          this.w_RNUMACC = NVL(_Curs_PAR_TITE.PTNUMRIF , 0)
          this.w_ACTNUM = NVL(_Curs_PAR_TITE.CPROWNUM , 0)
          this.w_NUMPARACC = NVL(_Curs_PAR_TITE.PTNUMPAR, SPACE(31))
          * --- Read from PAR_TITE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PTFLCRSA"+;
              " from "+i_cTable+" PAR_TITE where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.w_SERACC);
                  +" and PTROWORD = "+cp_ToStrODBC(this.w_ORDACC);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RNUMACC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PTFLCRSA;
              from (i_cTable) where;
                  PTSERIAL = this.w_SERACC;
                  and PTROWORD = this.w_ORDACC;
                  and CPROWNUM = this.w_RNUMACC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CSACC = NVL(cp_ToDate(_read_.PTFLCRSA),cp_NullValue(_read_.PTFLCRSA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_CSACC = "A"
            * --- Chiudendo manualmente un acconto la partita creata ha i riferimenti 
            *     a tale acconto. Occorre sbiancarli.
            * --- Write into PAR_TITE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PTSERRIF ="+cp_NullLink(cp_ToStrODBC(Space(10)),'PAR_TITE','PTSERRIF');
              +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTORDRIF');
              +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTNUMRIF');
                  +i_ccchkf ;
              +" where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                  +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ACTNUM);
                     )
            else
              update (i_cTable) set;
                  PTSERRIF = Space(10);
                  ,PTORDRIF = 0;
                  ,PTNUMRIF = 0;
                  &i_ccchkf. ;
               where;
                  PTSERIAL = this.oParentObject.w_PNSERIAL;
                  and PTROWORD = this.w_ROWORD;
                  and CPROWNUM = this.w_ACTNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- La partita di acconto saldata deve puntare alla partita di creazione 
            *     appena creata, inoltre occorre impostare numero partita alla partita che
            *     salda.
            *     Anche l'agente della partita di chiusura � scritto nella partita di acconto
            *     originale, per individuare il tutto nelle varie stampe.
            *     Valorizzo il flag beni deperibili come sulla partita appena creata.
            * --- Write into PAR_TITE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERRIF');
              +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTORDRIF');
              +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_ACTNUM),'PAR_TITE','PTNUMRIF');
              +",PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(this.w_NUMPARACC),'PAR_TITE','PTNUMPAR');
              +",PTCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
              +",PTFLVABD ="+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'PAR_TITE','PTFLVABD');
                  +i_ccchkf ;
              +" where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.w_SERACC);
                  +" and PTROWORD = "+cp_ToStrODBC(this.w_ORDACC);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RNUMACC);
                     )
            else
              update (i_cTable) set;
                  PTSERRIF = this.oParentObject.w_PNSERIAL;
                  ,PTORDRIF = this.w_ROWORD;
                  ,PTNUMRIF = this.w_ACTNUM;
                  ,PTNUMPAR = this.w_NUMPARACC;
                  ,PTCODAGE = this.w_CODAGE;
                  ,PTFLVABD = this.w_FLVABD;
                  &i_ccchkf. ;
               where;
                  PTSERIAL = this.w_SERACC;
                  and PTROWORD = this.w_ORDACC;
                  and CPROWNUM = this.w_RNUMACC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        this.w_TROV = .T.
          select _Curs_PAR_TITE
          continue
        enddo
        use
      endif
      if this.w_OK
        if this.w_OKPAR="C" AND this.w_TOTPAR<>0 AND this.w_CFUNC="Load" And ((this.oParentObject.w_TOTACC<>0 AND this.oParentObject.w_NUMACC<>0) or ((this.oParentObject.w_RITENU$"CS" OR this.oParentObject.w_FLGRIT="S") AND this.oParentObject.w_GESRIT="S" AND this.oParentObject.w_PNFLGSTO="N"))
          * --- Imposto w_TOTPAR=0 per superare IF w_TOTPNT<>cp_ROUND(w_TOTPAR)
          *     ed eseguire pag6 (w_TOTPNT in caso di partite da creare � necessariamente
          *     diverso da zero)
          *     Nel caso
          *     - di registrazine che genera ritenute con storno differito e riga di tipo fornitore
          *     -Selezione di acconti  nel caso di accorpa da chiusura acconti  
          *     
          this.w_TOTPAR = 0
        endif
        do case
          case this.w_OKPAR $ "CA" And this.oParentObject.w_PNCODVAL<>this.oParentObject.w_PNVALNAZ AND this.w_TOTPAR<>0
            * --- Partite di Apertura o di Acconto, Converte in Moneta di Conto (tutto alla Fine)
            this.w_TOTPAR = cp_ROUND(VAL2MON(this.w_TOTPAR, this.w_CAMB, this.oParentObject.w_CAONAZ, this.w_DATCAM, this.oParentObject.w_PNVALNAZ),this.oParentObject.w_DECTOP)
          case this.w_OKPAR="S" And this.w_TIPCON=this.oParentObject.w_PNTIPCLF AND this.w_CODCON=this.oParentObject.w_PNCODCLF And this.w_IABBIN*this.w_TOTPNT<0
            * --- Partite di Chiusura, se non sono state trovate crea per abbinamento
            *     L'abbinamento automatico scatta solo se:
            *     a) Stesso Conto
            *     b) Sezione dare avere contrapposte quindi creo in dare e saldo in avere 
            *     o viceversa
            if Not this.w_TROV AND this.w_RABBIN<>0 AND this.w_IABBIN<>0 AND NOT LEFT(this.w_FLABAN, 2) $ "AN-AB"
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Usati qui sotto per eventuale conversione Valuta
              this.w_CAMB = this.oParentObject.w_PNCAOVAL
              this.w_DATCAM = IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC)
              * --- Converte in Moneta di Conto
              if this.oParentObject.w_PNCODVAL<>this.oParentObject.w_PNVALNAZ AND this.w_TOTPAR<>0
                this.w_TOTPAR = cp_ROUND(VAL2MON(this.w_TOTPAR, this.w_CAMB, this.oParentObject.w_CAONAZ, this.w_DATCAM, this.oParentObject.w_PNVALNAZ),this.oParentObject.w_DECTOP)
              endif
            endif
        endcase
        this.w_TOTPARCON = this.w_TOTPAR
      endif
      SELECT (this.oParentObject.cTrsName)
      this.w_TOTCONF = this.w_TOTPNT
      * --- Se reg in valuta controllo congruenza importo  di riga e partite con precisione 0.01
      if this.w_OK AND this.w_TOTCONF <> this.w_TOTPARCON and (Abs(this.w_TOTCONF-this.w_TOTPARCON) >0.01 or this.oParentObject.w_PNCODVAL=this.oParentObject.w_PNVALNAZ)
        do case
          case LEFT(this.w_FLABAN, 2)="AB" AND VAL(SUBSTR(this.w_FLABAN, 3, 4))<>0
            this.w_ROWABB = VAL(SUBSTR(this.w_FLABAN, 3, 4))
            * --- Se Non ci sono Partite ma e' una riga di Abbuono generata da una Chusura Partite
            * --- Crea le partite di Abbuono
            ah_Msg("Creazione automatica partite di abbuono: conto: %1",.T.,.F.,.F.,this.w_CODCON)
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT (this.oParentObject.cTrsName)
            WAIT CLEAR
          case this.w_OKPAR $ "CA" AND NOT EMPTY(this.w_CODPAG)
            * --- Se Non ci sono Partite ma e' una riga Creazione o Acconto ed e' disponibile un Pagamento
            if this.w_TOTPAR=0 AND this.w_CFUNC="Load"
              if this.w_CHKCOR AND Empty(this.w_NUMCOR)
                this.w_MESS = Ah_MsgFormat("Verificare partite riga primanota %1, pagamento di tipo bonifico, inserire un conto corrente!",this.w_NR)
                this.w_OK = .F.
              endif
              if this.w_OK
                * --- Se Non ci sono Partite ma Siamo in Caricamento e Flag Partite=CREA ed e' diponibile un Pagamento le Crea...
                ah_Msg("Creazione automatica acconti, partite conto: %1",.T.,.F.,.F.,this.w_CODCON)
                this.Page_6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                SELECT (this.oParentObject.cTrsName)
                WAIT CLEAR
              endif
            else
              * --- Errore!
              this.w_MESS = Ah_MsgFormat("L'importo di riga %1 non corrisponde alle partite, apportare le dovute correzioni",this.w_NR)
              this.w_OK = .F.
            endif
          case .T.
            this.w_oMess=createobject("Ah_Message")
            if this.w_TOTPAR=0
              this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare riga primanota %1 importi partite inesistenti")
              this.w_oPart.AddParam(this.w_NR)     
            else
              this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare riga primanota %1 importi partite errati")
              this.w_oPart.AddParam(this.w_NR)     
            endif
            if this.w_TOTPAR<>0
              this.w_oPart = this.w_oMess.AddMsgPartNL("Riga P.N.: %1")
              this.w_oPart.AddParam(ALLTRIM(STR(this.w_TOTPNT,18,this.oParentObject.w_DECTOP)))     
              this.w_oPart = this.w_oMess.AddMsgPartNL("%1Partite: %2")
              this.w_oPart.AddParam(space(5))     
              this.w_oPart.AddParam(ALLTRIM(STR(this.w_TOTPAR,18,this.oParentObject.w_DECTOP)))     
            endif
            if EMPTY(this.w_CODPAG)
              this.w_oMess.AddMsgPartNL("(Codice pagamento non definito)")     
            endif
            * --- Errore!
            this.w_OK = .F.
        endcase
      endif
    endif
    if ( g_BANC="S" AND this.w_TIPSOT="B" AND this.oParentObject.w_FLMOVC="S") 
      * --- Generazione Movimenti di C\C
      this.w_CCIMPCRE = 0
      this.w_CCIMPDEB = 0
      this.w_TOTCON = 0
      * --- Select from CCM_DETT
      i_nConn=i_TableProp[this.CCM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CCM_DETT_idx,2],.t.,this.CCM_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" CCM_DETT ";
            +" where CCSERIAL="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+" AND CCROWRIF="+cp_ToStrODBC(this.w_ROWORD)+"";
             ,"_Curs_CCM_DETT")
      else
        select * from (i_cTable);
         where CCSERIAL=this.oParentObject.w_PNSERIAL AND CCROWRIF=this.w_ROWORD;
          into cursor _Curs_CCM_DETT
      endif
      if used('_Curs_CCM_DETT')
        select _Curs_CCM_DETT
        locate for 1=1
        do while not(eof())
        this.w_CCIMPCRE = this.w_CCIMPCRE +NVL(_Curs_CCM_DETT.CCIMPCRE,0) 
        this.w_CCIMPDEB = this.w_CCIMPDEB + NVL(_Curs_CCM_DETT.CCIMPDEB,0)+ NVL(_Curs_CCM_DETT.CCCOMVAL,0)
        this.w_CCNUMCOR = NVL(_Curs_CCM_DETT.CCNUMCOR,SPACE(15))
        if NOT EMPTY(this.w_CCNUMCOR) 
          * --- Controllo dettaglio Conti Correnti
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACONCOL"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_CCNUMCOR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACONCOL;
              from (i_cTable) where;
                  BACODBAN = this.w_CCNUMCOR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONCOL = NVL(cp_ToDate(_read_.BACONCOL),cp_NullValue(_read_.BACONCOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_CODCON<>this.w_CONCOL
            * --- Errore!
            this.w_MESS = Ah_MsgFormat("Riga primanota %1 riferimento codice conto%0incongruente con i movimenti di conto corrente associati",this.w_NR)
            this.w_OK = .F.
          endif
        endif
          select _Curs_CCM_DETT
          continue
        enddo
        use
      endif
      if NOT EMPTY(this.w_CONBAN) and this.w_CFUNC="Load"
        * --- Controllo dettaglio Conti Correnti
        this.w_NUMRIG = 0
        * --- Read from COC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_DETT_idx,2],.t.,this.COC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CPROWNUM"+;
            " from "+i_cTable+" COC_DETT where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CPROWNUM;
            from (i_cTable) where;
                BACODBAN = this.w_CONBAN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NUMRIG = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BACONCOL"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BACONCOL;
            from (i_cTable) where;
                BACODBAN = this.w_CONBAN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONCOL = NVL(cp_ToDate(_read_.BACONCOL),cp_NullValue(_read_.BACONCOL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if (this.w_CCIMPCRE=0) and (this.w_CCIMPDEB=0) AND this.w_CFUNC="Load"
        * --- Generazione Movimenti di C\C
        * --- Delete from CCM_DETT
        i_nConn=i_TableProp[this.CCM_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CCM_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CCSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                +" and CCROWRIF = "+cp_ToStrODBC(this.w_ROWORD);
                 )
        else
          delete from (i_cTable) where;
                CCSERIAL = this.oParentObject.w_PNSERIAL;
                and CCROWRIF = this.w_ROWORD;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        if NOT EMPTY(this.w_CONBAN) AND NOT EMPTY(this.w_CAUMOV) AND (this.oParentObject.w_PNCODVAL=this.w_BAVAL) AND this.w_NUMRIG>0 AND this.w_CODCON=this.w_CONCOL
          * --- Select from gscg_bmc
          do vq_exec with 'gscg_bmc',this,'_Curs_gscg_bmc','',.f.,.t.
          if used('_Curs_gscg_bmc')
            select _Curs_gscg_bmc
            locate for 1=1
            do while not(eof())
            this.w_TIPCAU1 = NVL(_Curs_gscg_bmc.CATIPCON," ")
            this.w_COSCOM = IIF(this.w_IMPAVE=0,0,NVL(_Curs_gscg_bmc.CAIMPCOM,0))
            this.w_FLCRDE = NVL(_Curs_gscg_bmc.CAFLCRDE," ")
              select _Curs_gscg_bmc
              continue
            enddo
            use
          endif
          this.w_COMVAL = VAL2MON(this.w_COSCOM, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATCAM, this.oParentObject.w_PNVALNAZ,this.oParentObject.w_DECTOP)
          this.w_IMPDEB = IIF(this.w_IMPAVE=0,0,ABS(this.w_IMPAVE)-ABS(this.w_COMVAL))
          this.w_IMPCRE = IIF(this.w_IMPDAR=0,0,this.w_IMPDAR)
          this.w_CCFLCRED = IIF(this.w_FLCRDE="C", "+", " ")
          this.w_CCFLDEBI = IIF(this.w_FLCRDE="D", "+", " ")
          this.w_CCFLCOMM = "+"
          this.w_DATVAL = CALCFEST(this.oParentObject.w_PNDATREG, this.w_CALFES, this.w_CONBAN, this.w_CAUMOV, this.w_TIPCAU1, 1)
          * --- Try
          local bErr_0502CC10
          bErr_0502CC10=bTrsErr
          this.Try_0502CC10()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Errore!
            this.w_MESS = Ah_MsgFormat("Errore in scrittura movimenti di conto corrente")
            this.w_OK = .F.
          endif
          bTrsErr=bTrsErr or bErr_0502CC10
          * --- End
        else
          do case
            case EMPTY(this.w_CAUMOV) OR EMPTY(this.w_CONBAN)
              this.w_MESS = Ah_MsgFormat("Nessun modello contabile associato per la creazione di movimenti di conto corrente")
              this.w_OK = .F.
            case this.oParentObject.w_PNCODVAL<>this.w_BAVAL
              this.w_MESS = Ah_MsgFormat("Valuta c\c del modello contabile associato, incongruente con la valuta della registrazione contabile")
              this.w_OK = .F.
            case this.w_NUMRIG=0
              this.w_MESS = Ah_MsgFormat("Nessuna condizione indicata nel conto corrente indicato nel modello contabile")
              this.w_OK = .F.
            case this.w_CONCOL<>this.w_CODCON
              this.w_MESS = Ah_MsgFormat("Conto corrente diverso da quello specificato nel modello contabile")
              this.w_OK = .F.
          endcase
        endif
      else
        this.w_TOTCON = (this.w_CCIMPCRE-this.w_CCIMPDEB)
        if this.oParentObject.w_PNCODVAL<>this.oParentObject.w_PNVALNAZ
          this.w_TOTCON = VAL2MON(this.w_TOTCON, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_PNDATREG, this.oParentObject.w_PNVALNAZ,this.oParentObject.w_DECTOP)
        endif
        if (this.w_IMPDAR - this.w_IMPAVE) <> cp_ROUND(this.w_TOTCON, this.oParentObject.w_DECTOP)
          * --- Errore!
          this.w_MESS = Ah_MsgFormat("L'importo di riga non corrisponde ai movimenti di conto corrente, apportare le dovute correzioni")
          this.w_OK = .F.
        endif
      endif
    endif
    SELECT (this.oParentObject.cTrsName)
    if this.w_OK And this.oParentObject.w_FLANAL="S" AND g_PERCCR="S" AND ((this.w_TIPCON="G" AND this.w_SEZB $ "CR" AND this.w_CCTAGG $ "AM") OR this.w_OANAL="S")
      * --- Test Centri di Costo
      * --- Legge Centri di Costo Associati
      this.w_TROV = .F.
      this.w_TOTCOS = 0
      * --- Select from MOVICOST
      i_nConn=i_TableProp[this.MOVICOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2],.t.,this.MOVICOST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" MOVICOST ";
            +" where MRSERIAL="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+" AND MRROWORD="+cp_ToStrODBC(this.w_ROWORD)+"";
             ,"_Curs_MOVICOST")
      else
        select * from (i_cTable);
         where MRSERIAL=this.oParentObject.w_PNSERIAL AND MRROWORD=this.w_ROWORD;
          into cursor _Curs_MOVICOST
      endif
      if used('_Curs_MOVICOST')
        select _Curs_MOVICOST
        locate for 1=1
        do while not(eof())
        this.w_TOTCOS = this.w_TOTCOS + (NVL(MRTOTIMP,0) * IIF(NVL(MR_SEGNO," ")="D", 1,-1))
        this.w_TROV = .T.
          select _Curs_MOVICOST
          continue
        enddo
        use
      endif
      SELECT (this.oParentObject.cTrsName)
      if this.w_TIPCON="G" AND this.w_SEZB $ "CR" AND this.w_CCTAGG $ "AM"
        if this.w_TOTPNT <> this.w_TOTCOS
          if this.w_TOTCOS=0
            if Not this.w_TROV AND this.w_SRV="A" AND this.w_CCTAGG="A"
              * --- Se Non ci sono CDC e nuova riga e Flag CDC=AUTOMATICO le Crea...
              ah_Msg("Creazione automatica centri di costo conto: %1",.T.,.F.,.F.,this.w_CODCON)
              * --- Gestione Analitica
              this.w_MESS = GSAR_BRA(This,this.oParentObject.w_PNSERIAL, this.w_ROWORD, this.w_TIPCON, this.w_CODCON, this.w_TOTPNT)
              if NOT EMPTY(this.w_MESS)
                this.w_OK = .F.
              endif
              SELECT (this.oParentObject.cTrsName)
              WAIT CLEAR
            else
              this.w_MESS = Ah_MsgFormat("Verificare riga primanota %1 importi di analitica inesistenti",this.w_NR)
              * --- Errore!
              this.w_OK = .F.
            endif
          else
            this.w_MESS = Ah_MsgFormat("Verificare riga primanota %1 importi di analitica errati",this.w_NR)
            this.w_MESS = this.w_MESS+"("+ALLTRIM(STR(this.w_TOTCOS))+")"
            this.w_OK = .F.
          endif
        endif
      else
        * --- se la riga era associata a centri di costo (w_OANAL='S') ma adesso non piu' e ci sono ancora C/Costo associati, li elimina
        if this.w_TROV
          * --- Delete from MOVICOST
          i_nConn=i_TableProp[this.MOVICOST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"MRSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                  +" and MRROWORD = "+cp_ToStrODBC(this.w_ROWORD);
                   )
          else
            delete from (i_cTable) where;
                  MRSERIAL = this.oParentObject.w_PNSERIAL;
                  and MRROWORD = this.w_ROWORD;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      endif
    endif
    SELECT (this.oParentObject.cTrsName)
    if Not this.w_OK
      * --- Errore, esco comando SCAN
      * --- Ripristino Valori Variabili caller per acconti
      this.oParentObject.w_TOTACC = this.w_LTOTACC
      this.oParentObject.w_NUMACC = this.w_LNUMACC
      EXIT
    endif
    ENDSCAN
    if g_RITE="S" and not empty(this.oParentObject.w_PNCODCLF) and nvl(this.oParentObject.w_GESRIT," ")="S" and (this.oParentObject.w_RITENU$"CS" OR this.oParentObject.w_FLGRIT="S") and this.w_OK and this.oParentObject.w_SEARCHACC<>"S" and this.oParentObject.w_TESTRITE
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if g_TRAEXP $ "A-C-G" AND this.oParentObject.w_PNTIPREG $ "VACE" AND this.w_ROWCOUNT>10
      if NOT ah_YesNo("La procedura APRI non consente la registrazione %0di fatture con pi� di 10 righe di dettaglio.%0Non sar� quindi possibile esportare la registrazione.%0Confermare comunque la registrazione?")
        this.w_OK = .F.
      endif
    endif
    * --- Riposiziono il transitorio
    this.w_PADRE.RePos()     
  endproc
  proc Try_0502CC10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CCM_DETT
    i_nConn=i_TableProp[this.CCM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCM_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCM_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCSERIAL"+",CCROWRIF"+",CCCODCAU"+",CCNUMCOR"+",CCDATVAL"+",CCCOSCOM"+",CCIMPCRE"+",CCIMPDEB"+",CCCOMVAL"+",CPROWNUM"+",CPROWORD"+",CCFLDEBI"+",CCFLCRED"+",CCFLCOMM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'CCM_DETT','CCSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'CCM_DETT','CCROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAUMOV),'CCM_DETT','CCCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONBAN),'CCM_DETT','CCNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATVAL),'CCM_DETT','CCDATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COSCOM),'CCM_DETT','CCCOSCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCRE),'CCM_DETT','CCIMPCRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDEB),'CCM_DETT','CCIMPDEB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMVAL),'CCM_DETT','CCCOMVAL');
      +","+cp_NullLink(cp_ToStrODBC(1),'CCM_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(10),'CCM_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLDEBI),'CCM_DETT','CCFLDEBI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCRED),'CCM_DETT','CCFLCRED');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOMM),'CCM_DETT','CCFLCOMM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.oParentObject.w_PNSERIAL,'CCROWRIF',this.w_ROWORD,'CCCODCAU',this.w_CAUMOV,'CCNUMCOR',this.w_CONBAN,'CCDATVAL',this.w_DATVAL,'CCCOSCOM',this.w_COSCOM,'CCIMPCRE',this.w_IMPCRE,'CCIMPDEB',this.w_IMPDEB,'CCCOMVAL',this.w_COMVAL,'CPROWNUM',1,'CPROWORD',10,'CCFLDEBI',this.w_CCFLDEBI)
      insert into (i_cTable) (CCSERIAL,CCROWRIF,CCCODCAU,CCNUMCOR,CCDATVAL,CCCOSCOM,CCIMPCRE,CCIMPDEB,CCCOMVAL,CPROWNUM,CPROWORD,CCFLDEBI,CCFLCRED,CCFLCOMM &i_ccchkf. );
         values (;
           this.oParentObject.w_PNSERIAL;
           ,this.w_ROWORD;
           ,this.w_CAUMOV;
           ,this.w_CONBAN;
           ,this.w_DATVAL;
           ,this.w_COSCOM;
           ,this.w_IMPCRE;
           ,this.w_IMPDEB;
           ,this.w_COMVAL;
           ,1;
           ,10;
           ,this.w_CCFLDEBI;
           ,this.w_CCFLCRED;
           ,this.w_CCFLCOMM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili  per C\C
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Abbinamento Partite
    this.w_ABDAVE = IIF(this.w_TOTPNT>0, "D", "A")
    this.w_ABDIFF = ABS(this.w_TOTPNT)
    if this.oParentObject.w_PNCODVAL<>this.oParentObject.w_PNVALNAZ AND this.oParentObject.w_PNCAOVAL<>0
      this.w_DATINI = IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC)
      this.w_ABDIFF = MON2VAL(this.w_ABDIFF, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATINI, this.oParentObject.w_PNVALNAZ, this.oParentObject.w_DECTOT)
      this.w_IABBIN = MON2VAL(this.w_IABBIN, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATINI, this.oParentObject.w_PNVALNAZ, this.oParentObject.w_DECTOT)
    endif
    * --- Dal totale da abbinare escludo eventuali acconti...(Segno opposto)
    *     Tranne il caso in cui  ho un totale acconti pari al totale fattura
    *     ed eventuale enasarco nel caso di gestione delle ritenute
    this.w_IABBIN = this.w_IABBIN + iif(Abs(this.w_IABBIN)=Abs(this.w_LTOTACC),0,this.w_LTOTACC)+this.oParentObject.w_PNTOTENA
    if g_RITE="S" and not empty(this.oParentObject.w_PNCODCLF) and nvl(this.oParentObject.w_GESRIT," ")="S" AND (this.oParentObject.w_RITENU$"CS" OR this.oParentObject.w_FLGRIT="S") AND this.oParentObject.w_PNFLGSTO="N"
      * --- Storno l'importo della partita sospesa generata nella prima riga
      *     per non farla partecipare al calcolo dell'importo partita enasarco nel caso 
      *     di storno differito.
      * --- Select from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PTTOTIMP,PT_SEGNO  from "+i_cTable+" PAR_TITE ";
            +" where PTSERIAL="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+" AND PTROWORD="+cp_ToStrODBC(this.w_RABBIN)+" AND PTFLSOSP='S'";
             ,"_Curs_PAR_TITE")
      else
        select PTTOTIMP,PT_SEGNO from (i_cTable);
         where PTSERIAL=this.oParentObject.w_PNSERIAL AND PTROWORD=this.w_RABBIN AND PTFLSOSP="S";
          into cursor _Curs_PAR_TITE
      endif
      if used('_Curs_PAR_TITE')
        select _Curs_PAR_TITE
        locate for 1=1
        do while not(eof())
        this.w_IABBIN = this.w_IABBIN + NVL(_Curs_PAR_TITE.PTTOTIMP,0)
          select _Curs_PAR_TITE
          continue
        enddo
        use
      endif
    endif
    this.w_ROWNUM = 0
    this.w_TOTPAR = 0
    this.w_RESTO = this.w_ABDIFF
    this.w_ROWINI = 0
    this.w_FIND = .F.
    * --- Azzero totale acconti, nell'eventualit� di righe successive
    this.oParentObject.w_TOTACC = 0
    * --- Ciclo sulle partite create per il cliente/fornitore gia inserite in caricamento,
    *     escludo gli acconti.
    this.w_RITDARIP = this.w_ABDIFF-ABS(this.oParentObject.w_PNTOTENA)
    this.w_RESIDUO = this.w_ABDIFF
    this.w_TOTABB = 0
    * --- Select from GSCGABPK
    do vq_exec with 'GSCGABPK',this,'_Curs_GSCGABPK','',.f.,.t.
    if used('_Curs_GSCGABPK')
      select _Curs_GSCGABPK
      locate for 1=1
      do while not(eof())
      * --- Devo verificare se tra le partite ve ne sono alcune relative ad un acconto
      *     abbinato. Queste escono dal giro di abbinamento contestuale (Es. Ritenute).
      *     L'acconto � una partita che se esiste punta alla partita della registrazione
      *     creanda.
      if this.oParentObject.w_SEARCHACC="S"
        this.w_TEST = _Curs_GSCGABPK.TEST
      else
        this.w_TEST = ""
      endif
      if this.w_TEST<>"A" 
        * --- Cicla sulle Righe Partite di Apertura
        this.w_PTDATSCA = CP_TODATE(_Curs_GSCGABPK.PTDATSCA)
        this.w_PTNUMPAR = IIF( this.w_TEST= "A" AND Abs(this.w_IABBIN)=NVL(_Curs_GSCGABPK.PTTOTIMP, 0),CANUMPAR("S",this.oParentObject.w_PNCODESE),_Curs_GSCGABPK.PTNUMPAR)
        * --- Se la partita di origine � un acconto generico devo comunque creare delle partite di 
        *     acconto generico con lo stesso segno senza collegarle
        this.w_PT_SEGNO = IIF(NVL(_Curs_GSCGABPK.PT_SEGNO," ")="D" AND Right(Alltrim(this.w_PTNUMPAR),8)<>Ah_MsgFormat("/Acconto") , "A", "D")
        this.w_PTTOTIMP = NVL(_Curs_GSCGABPK.PTTOTIMP, 0)
        this.w_IMPORI = NVL(_Curs_GSCGABPK.PTTOTIMP, 0)
        this.w_TOTABB = this.w_TOTABB + this.w_PTTOTIMP
        this.w_PTMODPAG = _Curs_GSCGABPK.PTMODPAG
        this.w_PTBANAPP = _Curs_GSCGABPK.PTBANAPP
        this.w_PTDATAPE = CP_TODATE(_Curs_GSCGABPK.PTDATAPE)
        this.w_PTIMPDOC = NVL(_Curs_GSCGABPK.PTIMPDOC, 0)
        this.w_PTDESRIG = IIF(EMPTY(NVL(_Curs_GSCGABPK.PTDESRIG, " ")), this.w_DESRIG, _Curs_GSCGABPK.PTDESRIG)
        this.w_FLSOSP = NVL(_Curs_GSCGABPK.PTFLSOSP, " ")
        if this.w_IABBIN <> (this.w_RITDARIP*IIF(this.w_ABDAVE="D", -1, 1)) and this.w_IABBIN<>0 
          * --- 'Spalma' sulle Partite di Apertura Altrimenti Salda per l'intero Importo
          *     solo nel caso in cui la riga di creazione non sia relativa ll'enasarco
          if Nvl(_Curs_GSCGABPK.CPROWNUM,0) <> this.w_RIFENA
            this.w_PTTOTIMP = cp_ROUND((this.w_RITDARIP * this.w_PTTOTIMP) / ABS(this.w_IABBIN), this.oParentObject.w_DECTOT)
          endif
          if this.w_PTTOTIMP>this.w_IMPORI and this.oParentObject.w_SEARCHACC="S"
            * --- ho saldato pi� di quanto indicato nella relativa partita di creazione nella prima riga
            this.w_PTTOTIMP = this.w_IMPORI
          endif
          this.w_RESTO = this.w_RESTO - (this.w_PTTOTIMP * IIF(this.w_ABDAVE=this.w_PT_SEGNO, 1, -1))
        else
          this.w_RESTO = 0
        endif
        this.w_RESIDUO = this.w_RESIDUO - this.w_PTTOTIMP
        this.w_ROWNUM = this.w_ROWNUM + 1
        * --- Se acconto generico non devo inserire riferimenti
        this.w_AGGSERRIF = IIF(Right(Alltrim(this.w_PTNUMPAR),8) = Ah_MsgFormat("/Acconto") ,Space(10),_Curs_GSCGABPK.PTSERIAL)
        this.w_AGGORDRIF = IIF(Right(Alltrim(this.w_PTNUMPAR),8) = Ah_MsgFormat("/Acconto") ,0,_Curs_GSCGABPK.PTROWORD)
        this.w_AGGNUMRIF = IIF(Right(Alltrim(this.w_PTNUMPAR),8) = Ah_MsgFormat("/Acconto") ,0,_Curs_GSCGABPK.CPROWNUM)
        this.w_LOKPAR = IIF(Right(Alltrim(this.w_PTNUMPAR),8) = Ah_MsgFormat("/Acconto") ,"A",this.w_OKPAR)
        if this.w_PTTOTIMP<>0
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTNUMPAR"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTTIPCON"+",PTCODCON"+",PTDATAPE"+",PTFLSOSP"+",PTMODPAG"+",PTBANAPP"+",PTFLCRSA"+",PTCAOAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTBANNOS"+",PTTOTABB"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTNUMCOR"+",PTFLRAGG"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODVAL),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_TITE','PTTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FLSOSP),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LOKPAR),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNALFDOC),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATDOC),'PAR_TITE','PTDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_BANNOS),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'PAR_TITE','PTFLVABD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_AGGSERRIF),'PAR_TITE','PTSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_AGGORDRIF),'PAR_TITE','PTORDRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_AGGNUMRIF),'PAR_TITE','PTNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'PAR_TITE','PTNUMCOR');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.oParentObject.w_PNSERIAL,'PTROWORD',this.w_ROWORD,'CPROWNUM',this.w_ROWNUM,'PTDATSCA',this.w_PTDATSCA,'PTNUMPAR',this.w_PTNUMPAR,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.oParentObject.w_PNCODVAL,'PTCAOVAL',this.oParentObject.w_PNCAOVAL,'PTTIPCON',this.w_TIPCON,'PTCODCON',this.w_CODCON,'PTDATAPE',this.w_PTDATAPE)
            insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTNUMPAR,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTTIPCON,PTCODCON,PTDATAPE,PTFLSOSP,PTMODPAG,PTBANAPP,PTFLCRSA,PTCAOAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTBANNOS,PTTOTABB,PTDESRIG,PTCODAGE,PTFLVABD,PTSERRIF,PTORDRIF,PTNUMRIF,PTNUMCOR,PTFLRAGG &i_ccchkf. );
               values (;
                 this.oParentObject.w_PNSERIAL;
                 ,this.w_ROWORD;
                 ,this.w_ROWNUM;
                 ,this.w_PTDATSCA;
                 ,this.w_PTNUMPAR;
                 ,this.w_PT_SEGNO;
                 ,this.w_PTTOTIMP;
                 ,this.oParentObject.w_PNCODVAL;
                 ,this.oParentObject.w_PNCAOVAL;
                 ,this.w_TIPCON;
                 ,this.w_CODCON;
                 ,this.w_PTDATAPE;
                 ,this.w_FLSOSP;
                 ,this.w_PTMODPAG;
                 ,this.w_PTBANAPP;
                 ,this.w_LOKPAR;
                 ,this.oParentObject.w_PNCAOVAL;
                 ,this.oParentObject.w_PNNUMDOC;
                 ,this.oParentObject.w_PNALFDOC;
                 ,this.oParentObject.w_PNDATDOC;
                 ,this.w_PTIMPDOC;
                 ,this.w_BANNOS;
                 ,0;
                 ,this.w_PTDESRIG;
                 ,this.w_CODAGE;
                 ,this.w_FLVABD;
                 ,this.w_AGGSERRIF;
                 ,this.w_AGGORDRIF;
                 ,this.w_AGGNUMRIF;
                 ,this.w_NUMCOR;
                 ," ";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='errore Inserimento Partite Acconto Generico'
            return
          endif
        endif
        this.w_FIND = .T.
        if ((this.w_ABDIFF>=this.w_PTTOTIMP AND this.oParentObject.w_PNFLGSTO="N" ) OR (this.w_ABDIFF>=this.w_PTTOTIMP AND this.oParentObject.w_PNFLGSTO="S")) AND Nvl(_Curs_GSCGABPK.CPROWNUM,0) = this.w_RIFENA
          * --- Se importo di riga pari ad importo enasarco esco immediatamente dopo 
          *     aver fatto la relativa partita di enasarco di saldo 
          this.w_RESTO = 0
          EXIT
        endif
      endif
        select _Curs_GSCGABPK
        continue
      enddo
      use
    endif
    if Not this.w_FIND or ABS(this.w_TOTABB)<this.w_ABDIFF 
      * --- Se ho saldato pi� di quanto ho da abbinare nella prima riga
      *     devo creare partita di acconto residuo conprensivo
      *     di eventuali arrotondamenti da spalmare
      *     Altrimenti storno resto dal residuo azzerandolo e applico resto
      *     sulla prima partita
      this.w_RESIDUO = ABS(this.w_RESIDUO)
      this.w_RESTO = 0
    else
      this.w_RESIDUO = IIF(this.w_FIND,ABS(this.w_RESIDUO-this.w_RESTO) ,ABS(this.w_RESIDUO))
    endif
    if this.w_RESIDUO<>0 
      * --- Nel caso in cui ho accorpato acconti sufficenti a saldare le partite che sto 
      *     creando e comunque decido di saldare devo creare partite di acconto generico
      * --- Nel caso in cui saldo per un importo superiore alle rate calcolate nella prima 
      *     riga devo creare partita di acconto per la parte in esubero
      this.w_ROWNUM = this.w_ROWNUM + 1
      this.w_PTNUMPAR = this.oParentObject.w_PNCODESE+"/Acconto"
      this.w_PTMODPAG = SUBSTR(CHTIPPAG(g_SAPAGA, "RD"), 3)
      * --- Insert into PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTNUMPAR"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTTIPCON"+",PTCODCON"+",PTDATAPE"+",PTFLSOSP"+",PTMODPAG"+",PTBANAPP"+",PTFLCRSA"+",PTCAOAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTBANNOS"+",PTTOTABB"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTNUMCOR"+",PTFLRAGG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATREG),'PAR_TITE','PTDATSCA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ABDAVE),'PAR_TITE','PT_SEGNO');
        +","+cp_NullLink(cp_ToStrODBC(abs(this.w_RESIDUO)),'PAR_TITE','PTTOTIMP');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODVAL),'PAR_TITE','PTCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_TITE','PTTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_TITE','PTCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
        +","+cp_NullLink(cp_ToStrODBC("A"),'PAR_TITE','PTFLCRSA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNALFDOC),'PAR_TITE','PTALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATDOC),'PAR_TITE','PTDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'PAR_TITE','PTBANNOS');
        +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'PAR_TITE','PTFLVABD');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTSERRIF');
        +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTORDRIF');
        +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTNUMRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'PAR_TITE','PTNUMCOR');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.oParentObject.w_PNSERIAL,'PTROWORD',this.w_ROWORD,'CPROWNUM',this.w_ROWNUM,'PTDATSCA',this.oParentObject.w_PNDATREG,'PTNUMPAR',this.w_PTNUMPAR,'PT_SEGNO',this.w_ABDAVE,'PTTOTIMP',abs(this.w_RESIDUO),'PTCODVAL',this.oParentObject.w_PNCODVAL,'PTCAOVAL',this.oParentObject.w_PNCAOVAL,'PTTIPCON',this.w_TIPCON,'PTCODCON',this.w_CODCON,'PTDATAPE',this.w_PTDATAPE)
        insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTNUMPAR,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTTIPCON,PTCODCON,PTDATAPE,PTFLSOSP,PTMODPAG,PTBANAPP,PTFLCRSA,PTCAOAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTBANNOS,PTTOTABB,PTDESRIG,PTCODAGE,PTFLVABD,PTSERRIF,PTORDRIF,PTNUMRIF,PTNUMCOR,PTFLRAGG &i_ccchkf. );
           values (;
             this.oParentObject.w_PNSERIAL;
             ,this.w_ROWORD;
             ,this.w_ROWNUM;
             ,this.oParentObject.w_PNDATREG;
             ,this.w_PTNUMPAR;
             ,this.w_ABDAVE;
             ,abs(this.w_RESIDUO);
             ,this.oParentObject.w_PNCODVAL;
             ,this.oParentObject.w_PNCAOVAL;
             ,this.w_TIPCON;
             ,this.w_CODCON;
             ,this.w_PTDATAPE;
             ," ";
             ,this.w_PTMODPAG;
             ,this.w_PTBANAPP;
             ,"A";
             ,this.oParentObject.w_PNCAOVAL;
             ,this.oParentObject.w_PNNUMDOC;
             ,this.oParentObject.w_PNALFDOC;
             ,this.oParentObject.w_PNDATDOC;
             ,this.w_PTIMPDOC;
             ,SPACE(15);
             ,0;
             ,this.w_PTDESRIG;
             ,this.w_CODAGE;
             ,this.w_FLVABD;
             ,SPACE(10);
             ,0;
             ,0;
             ,this.w_NUMCOR;
             ," ";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='errore Inserimento Partite Acconto Generico'
        return
      endif
      this.w_RESIDUO = 0
      this.w_FIND = .T.
    endif
    if this.w_FIND
      * --- Per controllo con primanota
      this.w_TOTPAR = this.w_ABDIFF * IIF(this.w_ABDAVE="D", 1, -1)
      * --- Mette il resto sull'ultima Rata
      if this.w_RESTO<>0
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTTOTIMP =PTTOTIMP+ "+cp_ToStrODBC(this.w_RESTO);
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                 )
        else
          update (i_cTable) set;
              PTTOTIMP = PTTOTIMP + this.w_RESTO;
              &i_ccchkf. ;
           where;
              PTSERIAL = this.oParentObject.w_PNSERIAL;
              and PTROWORD = this.w_ROWORD;
              and CPROWNUM = this.w_ROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Registrazione Partite di Abbuono (Chiusura)
    * --- Leggo le Partite di Chiusura legate alla Riga di Abbuono
    this.w_PTROWNUM = 0
    * --- Select from PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
          +" where PTSERIAL="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+" AND PTROWORD="+cp_ToStrODBC(this.w_ROWABB)+" AND PTTOTABB<>0";
           ,"_Curs_PAR_TITE")
    else
      select * from (i_cTable);
       where PTSERIAL=this.oParentObject.w_PNSERIAL AND PTROWORD=this.w_ROWABB AND PTTOTABB<>0;
        into cursor _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      select _Curs_PAR_TITE
      locate for 1=1
      do while not(eof())
      this.w_PTDATSCA = _Curs_PAR_TITE.PTDATSCA
      this.w_PTNUMPAR = _Curs_PAR_TITE.PTNUMPAR
      * --- Assegno il segno dell'abbuono in funzione della sezione di primanota di appartenenza
      if Nvl(_Curs_PAR_TITE.PTTOTABB,0)<0
        this.w_PT_SEGNO = IIF(Nvl(_Curs_PAR_TITE.PT_SEGNO," ")="D" ,"A","D")
      else
        this.w_PT_SEGNO = Nvl(_Curs_PAR_TITE.PT_SEGNO," ")
      endif
      this.w_PTMODPAG = _Curs_PAR_TITE.PTMODPAG
      this.w_PTBANAPP = _Curs_PAR_TITE.PTBANAPP
      this.w_PTDATAPE = CP_TODATE(_Curs_PAR_TITE.PTDATAPE)
      this.w_PTCODVAL = _Curs_PAR_TITE.PTCODVAL
      this.w_PTTOTIMP = Abs(NVL(_Curs_PAR_TITE.PTTOTABB, 0))
      this.w_TOTIMP = NVL(_Curs_PAR_TITE.PTTOTIMP, 0)
      this.w_ROWPAR = _Curs_PAR_TITE.CPROWNUM
      this.w_PTIMPDOC = NVL(_Curs_PAR_TITE.PTIMPDOC, 0)
      this.w_PTCAOVAL = _Curs_PAR_TITE.PTCAOVAL
      this.w_PTNUMDOC = NVL(_Curs_PAR_TITE.PTNUMDOC, 0)
      this.w_PTALFDOC = NVL(_Curs_PAR_TITE.PTALFDOC, SPACE(10))
      this.w_PTDATDOC = CP_TODATE(_Curs_PAR_TITE.PTDATDOC)
      * --- Se abbuono Attivo partite riferite al Cambio di Chiusura
      this.w_PTCAOAPE = IIF(this.w_PTTOTIMP<0, this.w_PTCAOVAL, _Curs_PAR_TITE.PTCAOAPE)
      this.w_PTDESRIG = IIF(EMPTY(NVL(_Curs_PAR_TITE.PTDESRIG, " ")), this.w_DESRIG, _Curs_PAR_TITE.PTDESRIG)
      this.w_PTSERRIF = _Curs_PAR_TITE.PTSERRIF
      this.w_PTORDRIF = _Curs_PAR_TITE.PTORDRIF
      this.w_PTNUMRIF = _Curs_PAR_TITE.PTNUMRIF
      this.w_PTCODAGE = _Curs_PAR_TITE.PTCODAGE
      this.w_PTNUMCOR = _Curs_PAR_TITE.PTNUMCOR
      this.w_FLVABD1 = _Curs_PAR_TITE.PTFLVABD
      if this.w_PTTOTIMP<>0
        this.w_PTROWNUM = this.w_PTROWNUM + 1
        * --- Try
        local bErr_04F882A0
        bErr_04F882A0=bTrsErr
        this.Try_04F882A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_04F86980
          bErr_04F86980=bTrsErr
          this.Try_04F86980()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Errore!
            this.w_MESS = Ah_MsgFormat("Errore in scrittura partite di abbuono")
            this.w_OK = .F.
          endif
          bTrsErr=bTrsErr or bErr_04F86980
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_04F882A0
        * --- End
        if this.w_TOTIMP=0 
          * --- Elimino partite con importo a zero e abbuono <>0
          * --- Try
          local bErr_04F87F10
          bErr_04F87F10=bTrsErr
          this.Try_04F87F10()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Errore!
            this.w_MESS = Ah_MsgFormat("Errore in cancellazione partite di abbuono")
            this.w_OK = .F.
          endif
          bTrsErr=bTrsErr or bErr_04F87F10
          * --- End
        endif
      endif
        select _Curs_PAR_TITE
        continue
      enddo
      use
    endif
  endproc
  proc Try_04F882A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTNUMPAR"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTMODPAG"+",PTTIPCON"+",PTCODCON"+",PTNUMDOC"+",PTALFDOC"+",PTCAOAPE"+",PTDATAPE"+",PTFLSOSP"+",PTBANAPP"+",PTDATDOC"+",PTIMPDOC"+",PTBANNOS"+",PTTOTABB"+",PTFLCRSA"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTNUMCOR"+",PTFLRAGG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWNUM),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OKPAR),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLVABD1),'PAR_TITE','PTFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERRIF),'PAR_TITE','PTSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTORDRIF),'PAR_TITE','PTORDRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMRIF),'PAR_TITE','PTNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.oParentObject.w_PNSERIAL,'PTROWORD',this.w_ROWORD,'CPROWNUM',this.w_PTROWNUM,'PTDATSCA',this.w_PTDATSCA,'PTNUMPAR',this.w_PTNUMPAR,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTMODPAG',this.w_PTMODPAG,'PTTIPCON',this.w_TIPCON,'PTCODCON',this.w_CODCON)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTNUMPAR,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTMODPAG,PTTIPCON,PTCODCON,PTNUMDOC,PTALFDOC,PTCAOAPE,PTDATAPE,PTFLSOSP,PTBANAPP,PTDATDOC,PTIMPDOC,PTBANNOS,PTTOTABB,PTFLCRSA,PTDESRIG,PTCODAGE,PTFLVABD,PTSERRIF,PTORDRIF,PTNUMRIF,PTNUMCOR,PTFLRAGG &i_ccchkf. );
         values (;
           this.oParentObject.w_PNSERIAL;
           ,this.w_ROWORD;
           ,this.w_PTROWNUM;
           ,this.w_PTDATSCA;
           ,this.w_PTNUMPAR;
           ,this.w_PT_SEGNO;
           ,this.w_PTTOTIMP;
           ,this.w_PTCODVAL;
           ,this.w_PTCAOVAL;
           ,this.w_PTMODPAG;
           ,this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_PTNUMDOC;
           ,this.w_PTALFDOC;
           ,this.w_PTCAOAPE;
           ,this.w_PTDATAPE;
           ," ";
           ,this.w_PTBANAPP;
           ,this.w_PTDATDOC;
           ,this.w_PTIMPDOC;
           ,SPACE(15);
           ,0;
           ,this.w_OKPAR;
           ,this.w_PTDESRIG;
           ,this.w_PTCODAGE;
           ,this.w_FLVABD1;
           ,this.w_PTSERRIF;
           ,this.w_PTORDRIF;
           ,this.w_PTNUMRIF;
           ,this.w_PTNUMCOR;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04F86980()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTDATSCA ="+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +",PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +",PT_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
      +",PTTOTIMP ="+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
      +",PTCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
      +",PTCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
      +",PTMODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +",PTTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_TITE','PTTIPCON');
      +",PTCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_TITE','PTCODCON');
      +",PTNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
      +",PTALFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
      +",PTCAOAPE ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
      +",PTDATAPE ="+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +",PTFLSOSP ="+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
      +",PTBANAPP ="+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +",PTDATDOC ="+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
      +",PTIMPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
      +",PTBANNOS ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'PAR_TITE','PTBANNOS');
      +",PTTOTABB ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
      +",PTFLCRSA ="+cp_NullLink(cp_ToStrODBC(this.w_OKPAR),'PAR_TITE','PTFLCRSA');
      +",PTDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
      +",PTCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
      +",PTFLVABD ="+cp_NullLink(cp_ToStrODBC(this.w_FLVABD1),'PAR_TITE','PTFLVABD');
      +",PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PTSERRIF),'PAR_TITE','PTSERRIF');
      +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PTORDRIF),'PAR_TITE','PTORDRIF');
      +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PTNUMRIF),'PAR_TITE','PTNUMRIF');
          +i_ccchkf ;
      +" where ";
          +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
          +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTROWNUM);
             )
    else
      update (i_cTable) set;
          PTDATSCA = this.w_PTDATSCA;
          ,PTNUMPAR = this.w_PTNUMPAR;
          ,PT_SEGNO = this.w_PT_SEGNO;
          ,PTTOTIMP = this.w_PTTOTIMP;
          ,PTCODVAL = this.w_PTCODVAL;
          ,PTCAOVAL = this.w_PTCAOVAL;
          ,PTMODPAG = this.w_PTMODPAG;
          ,PTTIPCON = this.w_TIPCON;
          ,PTCODCON = this.w_CODCON;
          ,PTNUMDOC = this.w_PTNUMDOC;
          ,PTALFDOC = this.w_PTALFDOC;
          ,PTCAOAPE = this.w_PTCAOAPE;
          ,PTDATAPE = this.w_PTDATAPE;
          ,PTFLSOSP = " ";
          ,PTBANAPP = this.w_PTBANAPP;
          ,PTDATDOC = this.w_PTDATDOC;
          ,PTIMPDOC = this.w_PTIMPDOC;
          ,PTBANNOS = SPACE(15);
          ,PTTOTABB = 0;
          ,PTFLCRSA = this.w_OKPAR;
          ,PTDESRIG = this.w_PTDESRIG;
          ,PTCODAGE = this.w_PTCODAGE;
          ,PTFLVABD = this.w_FLVABD1;
          ,PTSERRIF = this.w_PTSERRIF;
          ,PTORDRIF = this.w_PTORDRIF;
          ,PTNUMRIF = this.w_PTNUMRIF;
          &i_ccchkf. ;
       where;
          PTSERIAL = this.oParentObject.w_PNSERIAL;
          and PTROWORD = this.w_ROWORD;
          and CPROWNUM = this.w_PTROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04F87F10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWABB);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWPAR);
             )
    else
      delete from (i_cTable) where;
            PTSERIAL = this.oParentObject.w_PNSERIAL;
            and PTROWORD = this.w_ROWABB;
            and CPROWNUM = this.w_ROWPAR;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Partite dai Movimenti di Primanota
    * --- Vettore contenente i Dati Delle Scadenze
    DIMENSION w_ARRSCA[999,6]
    this.w_DATINI = IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC)
    this.w_IVA = IIF(this.oParentObject.w_CALDOC="E", this.w_GSCG_MIV.w_TOTIVA, 0)
    this.w_NETTO = ABS(this.w_TOTPNT)
    this.w_ESCL1 = STR(this.w_MESE1, 2, 0) + STR(this.w_GIORN1, 2, 0) + STR(this.w_GIOFIS, 2, 0)
    this.w_ESCL2 = STR(this.w_MESE2, 2, 0) + STR(this.w_GIORN2, 2, 0)
    this.w_LRESIDUO = 0
    this.w_OKENA = .F.
    if this.oParentObject.w_PNCODVAL<>this.oParentObject.w_PNVALNAZ AND this.oParentObject.w_PNCAOVAL<>0
      * --- Partite in Valuta
      if this.w_TOTPNT_VAL<>0 AND ( (this.w_TIPCON=this.oParentObject.w_PNTIPCLF AND this.w_CODCON=this.oParentObject.w_PNCODCLF) OR EMPTY(this.oParentObject.w_PNCODCLF))
        * --- Partite in Valuta (se Esiste il Documento ed e' riferito al Cli/For di Riga prende Quello...
        this.w_NETTO = ABS(this.w_TOTPNT_VAL)
        if this.oParentObject.w_CALDOC="E" AND this.w_IVA<>0
          * --- Per calcolare gli importi da passare alla funzione SCADENZE (Imponibile ed imposta)
          *     ho due possibilit�, la prima calcolare l'imposta convertendo il totale imposta
          *     la seconda (se ho una sola riga nel castellettoI IVA), calcolare l'imposta direttamente
          *     applicando la percentuale IVA sul totale in valuta.
          this.w_APPO = this.w_IVA
          this.w_GSCG_MIV.MarkPos()     
           
 Select ( this.w_GSCG_MIV.cTrsName ) 
 Count For Not Deleted() And NVL(t_IVIMPIVA, 0) <>0 And NVL(t_IVIMPONI, 0) <>0 And Nvl(t_IVFLOMAG,0) < 4 To this.w_CONTA 
 Go Top
          this.w_RIGIVA = NVL(t_IVIMPIVA, 0)
          this.w_PERIVA = NVL(t_PERIVA, 0)
          this.w_GSCG_MIV.RePos(.T.)     
          if this.w_CONTA=1 AND this.w_PERIVA<>0
            * --- Se presente una sola aliquota, ricalcola direttamente sulla percentuale IVA
            this.w_IVA = cp_ROUND(this.w_NETTO - (this.w_NETTO / (1 + (this.w_PERIVA / 100)))+IIF(this.oParentObject.w_PNCODVAL=g_CODLIR, .499, 0), this.oParentObject.w_DECTOT)
          else
            * --- Converte l'IVA alla Valuta del Documento di Origine
            this.w_IVA = cp_ROUND(MON2VAL(this.w_IVA, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATINI, this.oParentObject.w_PNVALNAZ)+IIF(this.oParentObject.w_PNCODVAL=g_CODLIR, .499, 0), this.oParentObject.w_DECTOT)
          endif
        endif
      else
        this.w_NETTO = MON2VAL(this.w_NETTO, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATINI, this.oParentObject.w_PNVALNAZ, this.oParentObject.w_DECTOT)
        if this.oParentObject.w_CALDOC="E" AND this.w_IVA<>0
          * --- Converte l'IVA alla Valuta del Documento di Origine
          this.w_IVA = cp_ROUND(MON2VAL(this.w_IVA, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATINI, this.oParentObject.w_PNVALNAZ)+IIF(this.oParentObject.w_PNCODVAL=g_CODLIR, .499, 0), this.oParentObject.w_DECTOT)
        endif
      endif
    endif
    if this.w_TIPCON<>this.oParentObject.w_PNTIPCLF
      this.w_IVA = 0
    endif
    this.w_PTIMPDOC = ABS(this.w_NETTO)
    if g_RITE="S" and not empty(this.oParentObject.w_PNCODCLF) and nvl(this.oParentObject.w_GESRIT," ")="S" and (this.oParentObject.w_RITENU$"CS" OR this.oParentObject.w_FLGRIT="S") and this.w_OK and this.oParentObject.w_SEARCHACC="S" AND this.w_TIPCON$ "F-C"
      * --- Determino solo Totale Ritenute 
      this.w_TOTRITE = 0
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_PTIMPDOC-IIF(this.w_TIPCON="F",this.w_TOTRITE+this.oParentObject.w_PNTOTENA,0)-this.oParentObject.w_TOTACC <=0
        this.w_NETTO = this.w_PTIMPDOC-IIF(this.w_TIPCON$ "F-C",this.w_TOTRITE+ABS(this.oParentObject.w_PNTOTENA),0)
      endif
    endif
    if this.w_OKPAR<>"A" AND this.oParentObject.w_TOTACC<>0 AND this.oParentObject.w_NUMACC>0
      * --- L'acconto di norma ha segno opposto (ATTENZIONE: w_NETTO e' sempre in Valore Assoluto)
      this.w_APPO = ABS(this.oParentObject.w_TOTACC) * IIF(SIGN(this.w_TOTPNT)=SIGN(this.oParentObject.w_TOTACC), -1, 1)
      this.w_IVA = cp_ROUND((this.w_IVA * (this.w_NETTO-this.w_APPO)/this.w_NETTO), this.oParentObject.w_DECTOT)
      this.w_NETTO = this.w_NETTO - this.w_APPO
    endif
    if this.w_NETTO>0
      * --- Calcolo rate per il residuo non acconto
      this.w_NETTO = this.w_NETTO - this.w_IVA - IIF(this.w_TIPCON$ "F-C",this.w_TOTRITE+ABS(this.oParentObject.w_PNTOTENA),0)
      this.w_NURATE = SCADENZE("w_ARRSCA", this.w_CODPAG, this.w_DATINI, this.w_NETTO, this.w_IVA, 0, this.w_ESCL1, this.w_ESCL2, this.oParentObject.w_DECTOT)
    else
      this.w_NURATE = 1
       
 w_ARRSCA[1, 1] = this.oParentObject.w_PNDATREG 
 w_ARRSCA[1, 2] = this.w_NETTO 
 w_ARRSCA[1, 3] = this.w_IVA 
 w_ARRSCA[1, 4] = 0 
 w_ARRSCA[1, 5] = SUBSTR(CHTIPPAG(g_SAPAGA, "RD"), 3) 
 w_ARRSCA[1, 6] = " "
    endif
    this.w_ROWNUM = 0
    * --- Delete from PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
             )
    else
      delete from (i_cTable) where;
            PTSERIAL = this.oParentObject.w_PNSERIAL;
            and PTROWORD = this.w_ROWORD;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_MESS = " "
    this.w_RIGACC = SPACE(50)
    * --- Cicla sulle Rate
    FOR L_i = 1 to this.w_NURATE
    if this.w_OKPAR="A"
      * --- Acconto se l'importo dell'acconto abbinato supera il totale scadenze
      *     gia nella registrazione
      this.w_PTNUMPAR = CANUMPAR("S", this.oParentObject.w_PNCODESE)
    else
      this.w_PTNUMPAR = CANUMPAR("N", this.oParentObject.w_PNCODESE, this.oParentObject.w_PNNUMDOC, this.oParentObject.w_PNALFDOC)
    endif
    this.w_PTBANAPP = IIF(this.w_TIPCON $ "CF", this.w_BANAPP, SPACE(10))
    this.w_PTBANNOS = IIF(this.w_TIPCON $ "CF", this.w_BANNOS, SPACE(15))
    this.w_PTDATAPE = IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC)
    * --- Inserisce Acconto
    this.w_PARACC = 0
    if this.w_OKPAR<>"A" AND this.oParentObject.w_TOTACC<>0 AND this.oParentObject.w_NUMACC>0
      * --- Scorro l'array che contiene gli acconti abbinabili...
      FOR K_i = 1 TO this.oParentObject.w_NUMACC
      this.w_PTDATSCA = this.w_PADRE.AACC[K_i,1]
      this.w_PARACC = this.w_PADRE.AACC[K_i,2]
      this.w_PT_SEGNO = IIF(this.w_PARACC<0, "D", "A")
      * --- Nel caso di gestione delle ritenute ho il totale acconti decurtato dell'importo
      *     realtivo alle ritenute devo quindi controllare che il consumo degli acconti non superi
      *     tale tetto massimo
      this.w_PTTOTIMP = IIF(this.oParentObject.w_TOTACC<this.w_PARACC,ABS(this.oParentObject.w_TOTACC),ABS(this.w_PARACC))
      this.w_PTMODPAG = this.w_PADRE.AACC[K_i,3]
      this.w_RIGACC = this.w_PADRE.AACC[K_i,4]
      this.w_RIFSER = this.w_PADRE.AACC[K_i,5]
      this.w_RIFORD = this.w_PADRE.AACC[K_i,6]
      this.w_RIFNUM = this.w_PADRE.AACC[K_i,7]
      this.w_PTCODVAL = this.w_PADRE.AACC[K_i,8]
      this.w_PTCAOVAL = this.w_PADRE.AACC[K_i,9]
      this.w_PTTOTABB = this.w_PADRE.AACC[K_i,10]
      this.w_PTDESRIG = this.w_RIGACC
      * --- Leggo l'importo effettivo della partita
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PTTOTIMP"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_RIFSER);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_RIFORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PTTOTIMP;
          from (i_cTable) where;
              PTSERIAL = this.w_RIFSER;
              and PTROWORD = this.w_RIFORD;
              and CPROWNUM = this.w_RIFNUM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LTOTPAR = NVL(cp_ToDate(_read_.PTTOTIMP),cp_NullValue(_read_.PTTOTIMP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_LRESIDUO = this.w_LTOTPAR-this.w_PTTOTIMP
      this.w_PTTOTIMP = this.w_LTOTPAR
      this.w_ROWNUM = this.w_ROWNUM + 1
      * --- Try
      local bErr_0363D7C0
      bErr_0363D7C0=bTrsErr
      this.Try_0363D7C0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Errore!
        this.w_MESS = Ah_MsgFormat("Errore in scrittura partita di rilevazione acconto")
        this.w_OK = .F.
        EXIT
      endif
      bTrsErr=bTrsErr or bErr_0363D7C0
      * --- End
      if NOT EMPTY(this.w_RIFSER) AND this.w_RIFORD<>0 AND this.w_RIFNUM<>0
        * --- Rinomino Partita di Acconto e inserisco  l'agente
        * --- Creo Riga nuova per acconto generico residuo
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
          +",PTCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
          +",PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERRIF');
          +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTORDRIF');
          +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','PTNUMRIF');
          +",PTFLVABD ="+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'PAR_TITE','PTFLVABD');
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_RIFSER);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_RIFORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
                 )
        else
          update (i_cTable) set;
              PTNUMPAR = this.w_PTNUMPAR;
              ,PTCODAGE = this.w_CODAGE;
              ,PTSERRIF = this.oParentObject.w_PNSERIAL;
              ,PTORDRIF = this.w_ROWORD;
              ,PTNUMRIF = this.w_ROWNUM;
              ,PTFLVABD = this.w_FLVABD;
              &i_ccchkf. ;
           where;
              PTSERIAL = this.w_RIFSER;
              and PTROWORD = this.w_RIFORD;
              and CPROWNUM = this.w_RIFNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.w_LRESIDUO>0
          this.w_LNUMPAR = CANUMPAR("S", this.oParentObject.w_PNCODESE)
          this.w_PTMODPAG = SUBSTR(CHTIPPAG(g_SAPAGA, "RD"), 3)
          this.w_PTDATSCA = this.oParentObject.w_PNDATREG
          this.w_PTDESRIG = IIF(EMPTY(this.w_RIGACC), this.w_DESRIG, this.w_RIGACC)
          this.w_LSEGNO = IIF(this.w_PT_SEGNO="A","D","A")
          this.w_ROWNUM = this.w_ROWNUM + 1
          this.w_ROWACCGEN = this.w_ROWNUM
          * --- Try
          local bErr_036438B0
          bErr_036438B0=bTrsErr
          this.Try_036438B0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Errore!
            this.w_MESS = Ah_MsgFormat("Errore in inserimento partita di acconto generico residuo")
            this.w_OK = .F.
            EXIT
          endif
          bTrsErr=bTrsErr or bErr_036438B0
          * --- End
        endif
      endif
      this.oParentObject.w_TOTACC = this.oParentObject.w_TOTACC - this.w_PARACC
      ENDFOR
      this.oParentObject.w_NUMACC = 0
    endif
    if type("w_ARRSCA[l_i,1]")<>"U"
      this.w_PTDATSCA = w_ARRSCA[L_i, 1]
      this.w_PT_SEGNO = IIF(this.w_TOTPNT<0, "A", "D")
      this.w_PTTOTIMP = w_ARRSCA[L_i, 2] + w_ARRSCA[L_i, 3]
      this.w_PTMODPAG = w_ARRSCA[L_i, 5]
    else
      this.w_PTDATSCA = cp_chartodate("  -  -    ")
      this.w_PT_SEGNO = " "
      this.w_PTTOTIMP = 0
      this.w_PTMODPAG = ""
    endif
    this.w_PTDESRIG = this.w_DESRIG
    * --- Puo' capitare in Caso di Acconti Maggiori del importo Documento , deve invertire il segno (Attenzione: non significa che se importo negativo=Avere!)
    this.w_PT_SEGNO = IIF(this.w_PTTOTIMP<0, IIF(this.w_PT_SEGNO="A", "D", "A"), this.w_PT_SEGNO)
    if this.w_NETTO<0 and Abs(this.w_NETTO)>Abs(this.w_IVA)
      * --- Non devo farlo nel caso in cui il netto � negativo a causa  dell'iva superiore
      *     caso di pi� righe intestatario che creano partite
      this.w_PTTOTIMP = abs(this.w_NETTO)
      this.w_PARACC = abs(this.w_NETTO)
    endif
    * --- Carico Partita Enasarco Specifica
    if this.oParentObject.w_PNTOTENA<>0 AND Not this.w_OKENA AND this.w_TIPCON="F"
      this.w_ENASARCO = this.oParentObject.w_PNTOTENA
      this.w_RIFENA = this.w_ROWNUM + this.w_NURATE+2
      this.w_SEGNO = IIF(this.w_ENASARCO<0, "D", "A")
      this.w_ENASARCO = ABS(this.w_ENASARCO)
      * --- Try
      local bErr_04F8C860
      bErr_04F8C860=bTrsErr
      this.Try_04F8C860()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Errore!
        this.w_MESS = "Errore in Scrittura Partite Enasarco!"
        this.w_OK = .F.
        EXIT
      endif
      bTrsErr=bTrsErr or bErr_04F8C860
      * --- End
      this.w_OKENA = .T.
    endif
    if this.oParentObject.w_SEARCHACC="S" AND((this.oParentObject.w_FLRIFE="C" AND this.w_PT_SEGNO="A") OR (this.oParentObject.w_FLRIFE="F" AND this.w_PT_SEGNO="D")) AND this.w_PARACC<>0
      this.w_PTNUMPAR = CANUMPAR("S", this.oParentObject.w_PNCODESE)
      this.w_PTMODPAG = SUBSTR(CHTIPPAG(g_SAPAGA, "RD"), 3)
      this.w_OKPAR = "A"
      this.w_PTDATSCA = this.oParentObject.w_PNDATREG
      this.w_PTDESRIG = IIF(EMPTY(this.w_RIGACC), this.w_DESRIG, this.w_RIGACC)
    endif
    this.w_PTTOTIMP = ABS(this.w_PTTOTIMP)
    * --- Carica il Record Partite
    if this.w_PTTOTIMP<>0
      if this.w_OKPAR="A" AND this.w_LRESIDUO<>0 AND this.w_ROWACCGEN>0
        * --- Try
        local bErr_04F8F8C0
        bErr_04F8F8C0=bTrsErr
        this.Try_04F8F8C0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Errore!
          this.w_MESS = Ah_MsgFormat("Errore in scrittura partite")
          this.w_OK = .F.
          EXIT
        endif
        bTrsErr=bTrsErr or bErr_04F8F8C0
        * --- End
      else
        this.w_ROWNUM = this.w_ROWNUM + 1
        * --- Try
        local bErr_04F8E9C0
        bErr_04F8E9C0=bTrsErr
        this.Try_04F8E9C0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Errore!
          this.w_MESS = Ah_MsgFormat("Errore in scrittura partite")
          this.w_OK = .F.
          EXIT
        endif
        bTrsErr=bTrsErr or bErr_04F8E9C0
        * --- End
      endif
    endif
    ENDFOR
    if g_RITE="S" and not empty(this.oParentObject.w_PNCODCLF) and nvl(this.oParentObject.w_GESRIT," ")="S" and (this.oParentObject.w_RITENU$"CS" OR this.oParentObject.w_FLGRIT="S") and this.w_OK and this.oParentObject.w_SEARCHACC="S" AND this.oParentObject.w_PNFLGSTO="N" AND this.w_TIPCON $ "C-F"
      this.w_ROWNUM = this.w_ROWNUM + 1
      this.w_LNUMPAR = CANUMPAR("N", this.oParentObject.w_PNCODESE, this.oParentObject.w_PNNUMDOC, this.oParentObject.w_PNALFDOC)
      this.w_SEGSOSP = iif(this.oParentObject.w_PNTIPCLF="F","A","D")
      * --- Try
      local bErr_04F92A10
      bErr_04F92A10=bTrsErr
      this.Try_04F92A10()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Errore!
        this.w_MESS = Ah_MsgFormat("Errore in scrittura partite ritenute")
        this.w_OK = .F.
        EXIT
      endif
      bTrsErr=bTrsErr or bErr_04F92A10
      * --- End
    endif
  endproc
  proc Try_0363D7C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTTIPCON"+",PTCODCON"+",PTDATAPE"+",PTCAOAPE"+",PTMODPAG"+",PTBANAPP"+",PTFLSOSP"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTBANNOS"+",PTTOTABB"+",PTFLCRSA"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTNUMCOR"+",PTFLRAGG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTABB),'PAR_TITE','PTTOTABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OKPAR),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'PAR_TITE','PTFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'PAR_TITE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.oParentObject.w_PNSERIAL,'PTROWORD',this.w_ROWORD,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTTIPCON',this.w_TIPCON,'PTCODCON',this.w_CODCON,'PTDATAPE',this.w_PTDATAPE)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTTIPCON,PTCODCON,PTDATAPE,PTCAOAPE,PTMODPAG,PTBANAPP,PTFLSOSP,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTBANNOS,PTTOTABB,PTFLCRSA,PTDESRIG,PTCODAGE,PTFLVABD,PTNUMCOR,PTFLRAGG &i_ccchkf. );
         values (;
           this.oParentObject.w_PNSERIAL;
           ,this.w_ROWORD;
           ,this.w_ROWNUM;
           ,this.w_PTNUMPAR;
           ,this.w_PTDATSCA;
           ,this.w_PT_SEGNO;
           ,this.w_PTTOTIMP;
           ,this.w_PTCODVAL;
           ,this.w_PTCAOVAL;
           ,this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_PTDATAPE;
           ,this.w_PTCAOVAL;
           ,this.w_PTMODPAG;
           ,this.w_PTBANAPP;
           ," ";
           ,this.oParentObject.w_PNNUMDOC;
           ,this.oParentObject.w_PNALFDOC;
           ,this.oParentObject.w_PNDATDOC;
           ,this.w_PTIMPDOC;
           ,this.w_PTBANNOS;
           ,this.w_PTTOTABB;
           ,this.w_OKPAR;
           ,this.w_PTDESRIG;
           ,this.w_CODAGE;
           ,this.w_FLVABD;
           ,this.w_NUMCOR;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_036438B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTTIPCON"+",PTCODCON"+",PTDATAPE"+",PTCAOAPE"+",PTMODPAG"+",PTBANAPP"+",PTFLSOSP"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTBANNOS"+",PTTOTABB"+",PTFLCRSA"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTNUMCOR"+",PTFLRAGG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LSEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LRESIDUO),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
      +","+cp_NullLink(cp_ToStrODBC("A"),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'PAR_TITE','PTFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'PAR_TITE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.oParentObject.w_PNSERIAL,'PTROWORD',this.w_ROWORD,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_LNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PT_SEGNO',this.w_LSEGNO,'PTTOTIMP',this.w_LRESIDUO,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTTIPCON',this.w_TIPCON,'PTCODCON',this.w_CODCON,'PTDATAPE',this.w_PTDATAPE)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTTIPCON,PTCODCON,PTDATAPE,PTCAOAPE,PTMODPAG,PTBANAPP,PTFLSOSP,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTBANNOS,PTTOTABB,PTFLCRSA,PTDESRIG,PTCODAGE,PTFLVABD,PTNUMCOR,PTFLRAGG &i_ccchkf. );
         values (;
           this.oParentObject.w_PNSERIAL;
           ,this.w_ROWORD;
           ,this.w_ROWNUM;
           ,this.w_LNUMPAR;
           ,this.w_PTDATSCA;
           ,this.w_LSEGNO;
           ,this.w_LRESIDUO;
           ,this.w_PTCODVAL;
           ,this.w_PTCAOVAL;
           ,this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_PTDATAPE;
           ,this.w_PTCAOVAL;
           ,this.w_PTMODPAG;
           ,this.w_PTBANAPP;
           ," ";
           ,this.oParentObject.w_PNNUMDOC;
           ,this.oParentObject.w_PNALFDOC;
           ,this.oParentObject.w_PNDATDOC;
           ,this.w_PTIMPDOC;
           ,this.w_PTBANNOS;
           ,0;
           ,"A";
           ,this.w_PTDESRIG;
           ,this.w_CODAGE;
           ,this.w_FLVABD;
           ,this.w_NUMCOR;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore Inserimento riga acconto residuo'
      return
    endif
    return
  proc Try_04F8C860()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTTIPCON"+",PTCODCON"+",PTDATAPE"+",PTCAOAPE"+",PTMODPAG"+",PTBANAPP"+",PTFLSOSP"+",PTNUMDOC"+",PTALFDOC"+",PTBANNOS"+",PTCAOVAL"+",PTCODAGE"+",PTDATDOC"+",PTDESRIG"+",PTFLCRSA"+",PTFLVABD"+",PTIMPDOC"+",PTNUMCOR"+",PTTOTABB"+",PTFLRAGG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RIFENA),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATREG),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ENASARCO),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OKPAR),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'PAR_TITE','PTFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'PAR_TITE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.oParentObject.w_PNSERIAL,'PTROWORD',this.w_ROWORD,'CPROWNUM',this.w_RIFENA,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.oParentObject.w_PNDATREG,'PT_SEGNO',this.w_SEGNO,'PTTOTIMP',this.w_ENASARCO,'PTCODVAL',this.oParentObject.w_PNCODVAL,'PTTIPCON',this.w_TIPCON,'PTCODCON',this.w_CODCON,'PTDATAPE',this.w_PTDATAPE,'PTCAOAPE',this.oParentObject.w_PNCAOVAL)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PT_SEGNO,PTTOTIMP,PTCODVAL,PTTIPCON,PTCODCON,PTDATAPE,PTCAOAPE,PTMODPAG,PTBANAPP,PTFLSOSP,PTNUMDOC,PTALFDOC,PTBANNOS,PTCAOVAL,PTCODAGE,PTDATDOC,PTDESRIG,PTFLCRSA,PTFLVABD,PTIMPDOC,PTNUMCOR,PTTOTABB,PTFLRAGG &i_ccchkf. );
         values (;
           this.oParentObject.w_PNSERIAL;
           ,this.w_ROWORD;
           ,this.w_RIFENA;
           ,this.w_PTNUMPAR;
           ,this.oParentObject.w_PNDATREG;
           ,this.w_SEGNO;
           ,this.w_ENASARCO;
           ,this.oParentObject.w_PNCODVAL;
           ,this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_PTDATAPE;
           ,this.oParentObject.w_PNCAOVAL;
           ,this.w_PTMODPAG;
           ,this.w_PTBANAPP;
           ," ";
           ,this.oParentObject.w_PNNUMDOC;
           ,this.oParentObject.w_PNALFDOC;
           ,this.w_PTBANNOS;
           ,this.oParentObject.w_PNCAOVAL;
           ,this.w_CODAGE;
           ,this.oParentObject.w_PNDATDOC;
           ,this.w_PTDESRIG;
           ,this.w_OKPAR;
           ,this.w_FLVABD;
           ,this.w_PTIMPDOC;
           ,this.w_NUMCOR;
           ,0;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento Partite '
      return
    endif
    return
  proc Try_04F8F8C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTTOTIMP =PTTOTIMP+ "+cp_ToStrODBC(this.w_PTTOTIMP);
          +i_ccchkf ;
      +" where ";
          +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
          +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWACCGEN);
             )
    else
      update (i_cTable) set;
          PTTOTIMP = PTTOTIMP + this.w_PTTOTIMP;
          &i_ccchkf. ;
       where;
          PTSERIAL = this.oParentObject.w_PNSERIAL;
          and PTROWORD = this.w_ROWORD;
          and CPROWNUM = this.w_ROWACCGEN;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04F8E9C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTTIPCON"+",PTCODCON"+",PTDATAPE"+",PTCAOAPE"+",PTMODPAG"+",PTBANAPP"+",PTFLSOSP"+",PTNUMDOC"+",PTALFDOC"+",PTIMPDOC"+",PTBANNOS"+",PTTOTABB"+",PTFLCRSA"+",PTDATDOC"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTNUMCOR"+",PTFLRAGG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(cp_ROUND(this.w_PTTOTIMP,this.oParentObject.w_DECTOT)),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OKPAR),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'PAR_TITE','PTFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'PAR_TITE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.oParentObject.w_PNSERIAL,'PTROWORD',this.w_ROWORD,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',cp_ROUND(this.w_PTTOTIMP,this.oParentObject.w_DECTOT),'PTCODVAL',this.oParentObject.w_PNCODVAL,'PTCAOVAL',this.oParentObject.w_PNCAOVAL,'PTTIPCON',this.w_TIPCON,'PTCODCON',this.w_CODCON,'PTDATAPE',this.w_PTDATAPE)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTTIPCON,PTCODCON,PTDATAPE,PTCAOAPE,PTMODPAG,PTBANAPP,PTFLSOSP,PTNUMDOC,PTALFDOC,PTIMPDOC,PTBANNOS,PTTOTABB,PTFLCRSA,PTDATDOC,PTDESRIG,PTCODAGE,PTFLVABD,PTNUMCOR,PTFLRAGG &i_ccchkf. );
         values (;
           this.oParentObject.w_PNSERIAL;
           ,this.w_ROWORD;
           ,this.w_ROWNUM;
           ,this.w_PTNUMPAR;
           ,this.w_PTDATSCA;
           ,this.w_PT_SEGNO;
           ,cp_ROUND(this.w_PTTOTIMP,this.oParentObject.w_DECTOT);
           ,this.oParentObject.w_PNCODVAL;
           ,this.oParentObject.w_PNCAOVAL;
           ,this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_PTDATAPE;
           ,this.oParentObject.w_PNCAOVAL;
           ,this.w_PTMODPAG;
           ,this.w_PTBANAPP;
           ," ";
           ,this.oParentObject.w_PNNUMDOC;
           ,this.oParentObject.w_PNALFDOC;
           ,this.w_PTIMPDOC;
           ,this.w_PTBANNOS;
           ,0;
           ,this.w_OKPAR;
           ,this.oParentObject.w_PNDATDOC;
           ,this.w_PTDESRIG;
           ,this.w_CODAGE;
           ,this.w_FLVABD;
           ,this.w_NUMCOR;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento Partite '
      return
    endif
    return
  proc Try_04F92A10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CPROWNUM"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTCODCON"+",PTCODVAL"+",PTDATAPE"+",PTDATDOC"+",PTDATSCA"+",PTFLCRSA"+",PTFLSOSP"+",PTMODPAG"+",PTNUMDOC"+",PTNUMPAR"+",PTROWORD"+",PTSERIAL"+",PTTIPCON"+",PTTOTABB"+",PTTOTIMP"+",PTCODAGE"+",PTNUMCOR"+",PTFLRAGG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEGSOSP),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTRITE),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'PAR_TITE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.w_ROWNUM,'PT_SEGNO',this.w_SEGSOSP,'PTALFDOC',this.oParentObject.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS,'PTCAOAPE',this.oParentObject.w_PNCAOVAL,'PTCAOVAL',this.oParentObject.w_PNCAOVAL,'PTCODCON',this.w_PTCODCON,'PTCODVAL',this.oParentObject.w_PNCODVAL,'PTDATAPE',this.w_PTDATAPE,'PTDATDOC',this.oParentObject.w_PNDATDOC,'PTDATSCA',this.w_PTDATSCA)
      insert into (i_cTable) (CPROWNUM,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTCODCON,PTCODVAL,PTDATAPE,PTDATDOC,PTDATSCA,PTFLCRSA,PTFLSOSP,PTMODPAG,PTNUMDOC,PTNUMPAR,PTROWORD,PTSERIAL,PTTIPCON,PTTOTABB,PTTOTIMP,PTCODAGE,PTNUMCOR,PTFLRAGG &i_ccchkf. );
         values (;
           this.w_ROWNUM;
           ,this.w_SEGSOSP;
           ,this.oParentObject.w_PNALFDOC;
           ,this.w_PTBANAPP;
           ,this.w_PTBANNOS;
           ,this.oParentObject.w_PNCAOVAL;
           ,this.oParentObject.w_PNCAOVAL;
           ,this.w_PTCODCON;
           ,this.oParentObject.w_PNCODVAL;
           ,this.w_PTDATAPE;
           ,this.oParentObject.w_PNDATDOC;
           ,this.w_PTDATSCA;
           ,"C";
           ,"S";
           ,this.w_PTMODPAG;
           ,this.oParentObject.w_PNNUMDOC;
           ,this.w_LNUMPAR;
           ,this.w_ROWORD;
           ,this.oParentObject.w_PNSERIAL;
           ,this.w_PTTIPCON;
           ,0;
           ,this.w_TOTRITE;
           ,this.w_CODAGE;
           ,this.w_NUMCOR;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Il flag Partite serve per poter verificare se ho registrato i dati della prima scadenze. 
    *     La partita sospesa deve leggere i dati della prima partita caricata
    this.w_PARTITE = .F.
    * --- Controllo se il movimento di primanota ha attivo il flag "Storno Immediato".
    *     Se attivo non effettuo nessuna operazione.
    if this.oParentObject.w_PNFLGSTO="N"
      * --- Effettuo una select sul file delle PAR_TITE per avere due totali:
      *     1) Toconp :Totale delle partite di tipo Creazione senza il flag Sospeso
      *     2) Totsosp: Totale delle partite sospese
      this.w_TOTCONP = 0
      this.w_TOTSOSP = 0
      this.w_TOTSALD = 0
      if this.oParentObject.w_PNCODVAL<>this.oParentObject.w_PNVALNAZ
        this.w_TOTENA = MON2VAL(this.oParentObject.w_PNTOTENA, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATINI, this.oParentObject.w_PNVALNAZ, this.oParentObject.w_DECTOT)
      else
        this.w_TOTENA = this.oParentObject.w_PNTOTENA
      endif
      if this.w_CFUNC="Edit"
        * --- Select from PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PTTOTIMP  from "+i_cTable+" PAR_TITE ";
              +" where PTSERIAL="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+" AND PTFLCRSA='C' AND PTTOTIMP=ABS("+cp_ToStrODBC(this.w_TOTENA)+") ";
               ,"_Curs_PAR_TITE")
        else
          select PTTOTIMP from (i_cTable);
           where PTSERIAL=this.oParentObject.w_PNSERIAL AND PTFLCRSA="C" AND PTTOTIMP=ABS(this.w_TOTENA) ;
            into cursor _Curs_PAR_TITE
        endif
        if used('_Curs_PAR_TITE')
          select _Curs_PAR_TITE
          locate for 1=1
          do while not(eof())
          this.w_TOTIMP = _Curs_PAR_TITE.PTTOTIMP
          exit
            select _Curs_PAR_TITE
            continue
          enddo
          use
        endif
        if this.w_TOTIMP=0 AND this.w_TOTENA<>0
          this.w_MESS = Ah_MsgFormat("Attenzione, partita di creazione relativa all' ENASARCO (%1 %2) non inserita",ALLTRIM(STR(this.w_TOTENA)),Alltrim(this.oParentObject.w_SIMVAL))
          this.w_OK = .F.
        endif
      endif
      if this.w_OK
        * --- Select from gscg_qpk
        do vq_exec with 'gscg_qpk',this,'_Curs_gscg_qpk','',.f.,.t.
        if used('_Curs_gscg_qpk')
          select _Curs_gscg_qpk
          locate for 1=1
          do while not(eof())
          * --- Totale Partite senza considerare la partita sospesa
          * --- Se ho partite di saldo devo calcolare il totale delle saldate
          if NOT EMPTY(NVL(_Curs_GSCG_QPK.SERSALD,SPACE(10))) AND ((Nvl(_Curs_GSCG_QPK.NUMRIGA,0) <> this.w_RIFENA AND this.w_CFUNC = "Load" ) OR(Nvl(_Curs_GSCG_QPK.PTTOTIMP,0) <> this.w_TOTENA AND this.w_CFUNC = "Edit" )) 
            this.w_TOTSALD = this.w_TOTSALD + IIF(NVL(_Curs_gscg_qpk.PTFLSOSP," ")<>"S",(NVL(_Curs_gscg_qpk.PTTOTIMP,0) * IIF(NVL(_Curs_gscg_qpk.PT_SEGNO," ")="D", 1, -1)),0)
          else
            this.w_TOTCONP = this.w_TOTCONP + IIF(NVL(_Curs_gscg_qpk.PTFLSOSP," ")<>"S",(NVL(_Curs_gscg_qpk.PTTOTIMP,0) * IIF(NVL(_Curs_gscg_qpk.PT_SEGNO," ")="D", 1, -1)),0)
          endif
          * --- Totale Partite con il flag sospeso. Questa operazione la devo 
          *     effettuare perch� questo importo deve essere detratto dall'importo della riga
          *     del movimento.
          this.w_TOTSOSP = this.w_TOTSOSP+ IIF(NVL(_Curs_gscg_qpk.PTFLSOSP," ")="S",(NVL(_Curs_gscg_qpk.PTTOTIMP,0) * IIF(NVL(_Curs_gscg_qpk.PT_SEGNO," ")="D", 1, -1)),0)
            select _Curs_gscg_qpk
            continue
          enddo
          use
        endif
        * --- Scrivo i due importi in valore assoluto
        this.w_TOTCONP = ABS(this.w_TOTCONP)
        this.w_TOTSOSP = ABS(this.w_TOTSOSP)
        this.w_TOTSALD = ABS(this.w_TOTSALD)
        * --- Devo risalire al totale da versare, ed al totale delle ritenute. Questi dati li
        *     leggo dalla tabella DATIRITE
        this.w_TOTRITE = 0
        * --- Select from DATIRITE
        i_nConn=i_TableProp[this.DATIRITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATIRITE_idx,2],.t.,this.DATIRITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DATIRITE ";
              +" where DRSERIAL="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+"";
               ,"_Curs_DATIRITE")
        else
          select * from (i_cTable);
           where DRSERIAL=this.oParentObject.w_PNSERIAL;
            into cursor _Curs_DATIRITE
        endif
        if used('_Curs_DATIRITE')
          select _Curs_DATIRITE
          locate for 1=1
          do while not(eof())
          this.w_TOTRITE = this.w_TOTRITE+_Curs_DATIRITE.DRRITENU
            select _Curs_DATIRITE
            continue
          enddo
          use
        endif
        * --- Scrivo i due importi in valore assoluto
        this.oParentObject.w_TOTMDR = ABS(this.oParentObject.w_TOTMDR)
        if this.oParentObject.w_PNCODVAL<>this.oParentObject.w_PNVALNAZ
          this.w_TOTRITE = MON2VAL(Abs(this.w_TOTRITE), this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATINI, this.oParentObject.w_PNVALNAZ, this.oParentObject.w_DECTOT)
          this.w_TOTMDR1 = MON2VAL(Abs(this.oParentObject.w_TOTMDR), this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATINI, this.oParentObject.w_PNVALNAZ, this.oParentObject.w_DECTOT)
        else
          this.w_TOTMDR1 = ABS(this.oParentObject.w_TOTMDR)
          this.w_TOTRITE = ABS(this.w_TOTRITE)
        endif
        * --- Utilizzo la nuova variabile w_RITERIP per calcolare l'importo ritenuta da ripartire
        *     Se esiste gi� una partita sospesa, la devo escludere dal calcolo della ritenuta
        this.w_RITERIP = this.w_TOTRITE - this.w_TOTSOSP
        * --- Calcolo il totale da versare
        this.w_TOTDAVER = this.w_TOTMDR1-this.w_TOTRITE-ABS(this.oParentObject.w_PNTOTENA)
        * --- Confronto il totale delle partite ed il totale da pagare 
        * --- Se w_RITERIP<>0 significa che ho modificato la partita sospesa e quindi devo ricalcolare le partite sulla base di w_RITERIP.
        *     Se w_RITERIP fosse=0, significa che non ho modificato la sospesa e quindi 
        *     non ha senso fare tutti i ricalcoli
        if this.w_TOTCONP<>this.w_TOTDAVER AND this.oParentObject.w_SEARCHACC <>"S" AND this.w_RITERIP>0
          * --- Se ho accotpato acconti non devo ricalcolare proporzionalmente la partita 
          *     delle ritenute poich� la stessa viene inserita come prima partita del dettaglio
          *     e sul residuo vengono ricalcolate le altre.
          * --- Se i totali risultano differenti devo eliminare se presenti partite con il flag sospeso attivo 
          *     dato che devo ricalcolare gli importi delle partite.
          if this.w_TOTSOSP<>0
            * --- Delete from PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                    +" and PTFLCRSA = "+cp_ToStrODBC("C");
                    +" and PTFLSOSP = "+cp_ToStrODBC("S");
                     )
            else
              delete from (i_cTable) where;
                    PTSERIAL = this.oParentObject.w_PNSERIAL;
                    and PTFLCRSA = "C";
                    and PTFLSOSP = "S";

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          * --- Assegno posizione per ritorno sul fondo dei controlli
          this.w_AbsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
          this.w_nRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
          SELECT (this.oParentObject.cTrsName)
          this.w_RECPOS = IIF( Eof() , RECNO(this.oParentObject.cTrsName)-1 , RECNO(this.oParentObject.cTrsName) )
          Select (this.oParentObject.cTrsName)
          this.w_RESTO = 0
          this.w_TOTPART = 0
          GO TOP
          * --- Ciclo solo i record che hanno il flag di crea partite, importi in avere e codice conto valorizzato
          SCAN FOR (t_PNIMPAVE<>0 or t_PNIMPDAR<>0) AND t_PNFLPART="C" AND NOT EMPTY(t_PNCODCON) AND ((t_PNTIPCON="F" AND this.oParentObject.w_RITENU $"C-S") OR (t_PNTIPCON="C" AND this.oParentObject.w_FLGRIT ="S"))
          this.w_ROWORD = CPROWNUM
          this.w_IMPCONTA = t_PNIMPAVE-t_PNIMPDAR
          this.w_IMPCONTA = ABS(this.w_IMPCONTA)
          this.w_IMPCONTA = (this.w_IMPCONTA-this.w_TOTSOSP-this.w_TOTSALD-this.w_TOTENA)
          if this.oParentObject.w_PNCODVAL<>this.oParentObject.w_PNVALNAZ
            this.w_IMPCONTA = MON2VAL(this.w_IMPCONTA, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATINI, this.oParentObject.w_PNVALNAZ, this.oParentObject.w_DECTOT)
          endif
          * --- Leggo le partite associate alla riga di primanota.
          * --- Controllo se la valuta del fornitore � diversa dalla valuta di conto. Se � cos�
          *     ricalcolo il totale della riga, il totale da pagare ed il totale delle ritenute.
          this.w_FIRSTROW = 0
          * --- Select from gscg1qpk
          do vq_exec with 'gscg1qpk',this,'_Curs_gscg1qpk','',.f.,.t.
          if used('_Curs_gscg1qpk')
            select _Curs_gscg1qpk
            locate for 1=1
            do while not(eof())
            this.w_NUMRIGA = _Curs_gscg1qpk.CPROWNUM
            if EMPTY(Nvl(_Curs_gscg1qpk.SERINC," "))
              * --- Righe non pintate da partite di saldo saldate
              this.w_IMPPAR = Nvl(_Curs_gscg1qpk.PTTOTIMP,0)
              this.w_IMPOPART = Nvl(_Curs_gscg1qpk.PTTOTIMP,0)
              * --- Non devo considerare la riga enasarco 
              if _Curs_gscg1qpk.PTFLCRSA<>"A" AND ((this.w_NUMRIGA <> this.w_RIFENA AND this.w_CFUNC = "Load" ) OR(this.w_IMPPAR <> this.w_TOTENA AND this.w_CFUNC = "Edit" )) 
                if this.w_FIRSTROW=0
                  * --- Memorizzo prima riga per stornare resto
                  this.w_FIRSTROW = _Curs_gscg1qpk.CPROWNUM
                endif
                * --- Ricalcolo il totale di ogni singola partita.
                *     (Importo Partita*Totale Ritenuta)/Importo della riga di contabilit�
                this.w_IMPOPART = this.w_IMPOPART-cp_ROUND(((this.w_IMPPAR*this.w_RITERIP)/this.w_IMPCONTA),this.oParentObject.w_DECTOT)
                * --- Calcolo il totale delle partite. Questo totale mi serve per poter verificare che
                *     gli importi non siano corretti.
                this.w_TOTPART = this.w_TOTPART+ this.w_IMPOPART
                * --- Try
                local bErr_04F652E0
                bErr_04F652E0=bTrsErr
                this.Try_04F652E0()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- Errore!
                  this.w_MESS = "Errore in Scrittura Partite!"
                  * --- transaction error
                  bTrsErr=.t.
                  i_TrsMsg=this.w_MESS
                endif
                bTrsErr=bTrsErr or bErr_04F652E0
                * --- End
                * --- Salvo i dati della prima scadenza su variabili di appoggio. Effettuo
                *     questa operazione per poi poterli inserire nella partita sospesa che creer� con l'importo
                *     delle ritenute.
                if this.w_PARTITE=.F.
                  this.oParentObject.w_PNALFDOC = _Curs_gscg1qpk.PTALFDOC
                  this.w_PTBANAPP = _Curs_gscg1qpk.PTBANAPP
                  this.w_PTBANNOS = _Curs_gscg1qpk.PTBANNOS
                  this.w_PTCAOVAL = _Curs_gscg1qpk.PTCAOVAL
                  this.w_PTCAOAPE = _Curs_gscg1qpk.PTCAOAPE
                  this.w_PTCODCON = NVL(_Curs_gscg1qpk.PTCODCON, SPACE(15))
                  this.w_PTCODVAL = _Curs_gscg1qpk.PTCODVAL
                  this.w_PTDATAPE = _Curs_gscg1qpk.PTDATAPE
                  this.oParentObject.w_PNDATDOC = _Curs_gscg1qpk.PTDATDOC
                  this.w_PTDATSCA = this.oParentObject.w_PNDATREG
                  this.w_PT_SEGNO = _Curs_gscg1qpk.PT_SEGNO
                  this.w_PTMODPAG = _Curs_gscg1qpk.PTMODPAG
                  this.oParentObject.w_PNNUMDOC = _Curs_gscg1qpk.PTNUMDOC
                  this.w_PTNUMPAR = _Curs_gscg1qpk.PTNUMPAR
                  this.w_PTTIPCON = NVL(_Curs_gscg1qpk.PTTIPCON, " ")
                  this.w_CODAGE = _Curs_gscg1qpk.PTCODAGE
                  this.w_PTNUMCOR = NVL(_Curs_gscg1qpk.PTNUMCOR, " ")
                  this.w_PARTITE = .T.
                endif
              endif
              * --- azzero totena nel caso in cui avessi modificato manualmente il dettaglio
              *     partite introducendo due partite con l'importo pari all'enasarco
              this.w_TOTENA = IIF(this.w_IMPPAR = this.w_TOTENA AND this.w_CFUNC = "Edit",0,this.w_TOTENA)
            endif
              select _Curs_gscg1qpk
              continue
            enddo
            use
          endif
          ENDSCAN
          this.w_TOTPART = this.w_TOTPART+ this.w_TOTSALD
          * --- Controllo se il totale da pagare � uguale al totale delle partite.
          if this.w_FIRSTROW>0
            * --- Se esiste un'altra riga oltre all'enasarco
            do case
              case this.w_TOTDAVER>this.w_TOTPART 
                * --- Verifico se non � presente una differenza , se � presente scrivo l'importo
                *     sulla riga inserita.
                this.w_RESTO = this.w_TOTDAVER-this.w_TOTPART
                * --- Try
                local bErr_04F687F0
                bErr_04F687F0=bTrsErr
                this.Try_04F687F0()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- Errore!
                  this.w_MESS = "Errore in Scrittura Partite!"
                  * --- transaction error
                  bTrsErr=.t.
                  i_TrsMsg=this.w_MESS
                endif
                bTrsErr=bTrsErr or bErr_04F687F0
                * --- End
              case this.w_TOTDAVER<this.w_TOTPART
                * --- Verifico se non � presente una differenza , se � presente scrivo l'importo
                *     sulla riga inserita.
                this.w_RESTO = ABS(this.w_TOTPART-this.w_TOTDAVER)
                * --- Try
                local bErr_04F675C0
                bErr_04F675C0=bTrsErr
                this.Try_04F675C0()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- Errore!
                  this.w_MESS = "Errore in Scrittura Partite!"
                  * --- transaction error
                  bTrsErr=.t.
                  i_TrsMsg=this.w_MESS
                endif
                bTrsErr=bTrsErr or bErr_04F675C0
                * --- End
            endcase
          endif
          * --- Try
          local bErr_04F6A1D0
          bErr_04F6A1D0=bTrsErr
          this.Try_04F6A1D0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Errore!
            this.w_MESS = "Errore in Scrittura Partite!"
            SELECT (this.oParentObject.cTrsName)
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
          bTrsErr=bTrsErr or bErr_04F6A1D0
          * --- End
        endif
      endif
    endif
  endproc
  proc Try_04F652E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTTOTIMP ="+cp_NullLink(cp_ToStrODBC(this.w_IMPOPART),'PAR_TITE','PTTOTIMP');
          +i_ccchkf ;
      +" where ";
          +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
          +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMRIGA);
             )
    else
      update (i_cTable) set;
          PTTOTIMP = this.w_IMPOPART;
          &i_ccchkf. ;
       where;
          PTSERIAL = this.oParentObject.w_PNSERIAL;
          and PTROWORD = this.w_ROWORD;
          and CPROWNUM = this.w_NUMRIGA;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04F687F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTTOTIMP =PTTOTIMP+ "+cp_ToStrODBC(this.w_RESTO);
          +i_ccchkf ;
      +" where ";
          +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
          +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_FIRSTROW);
             )
    else
      update (i_cTable) set;
          PTTOTIMP = PTTOTIMP + this.w_RESTO;
          &i_ccchkf. ;
       where;
          PTSERIAL = this.oParentObject.w_PNSERIAL;
          and PTROWORD = this.w_ROWORD;
          and CPROWNUM = this.w_FIRSTROW;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04F675C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTTOTIMP =PTTOTIMP- "+cp_ToStrODBC(this.w_RESTO);
          +i_ccchkf ;
      +" where ";
          +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
          +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_FIRSTROW);
             )
    else
      update (i_cTable) set;
          PTTOTIMP = PTTOTIMP - this.w_RESTO;
          &i_ccchkf. ;
       where;
          PTSERIAL = this.oParentObject.w_PNSERIAL;
          and PTROWORD = this.w_ROWORD;
          and CPROWNUM = this.w_FIRSTROW;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04F6A1D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Scrivo la nuova riga con il flag sospeso
    this.w_NUMRIGA = this.w_NUMRIGA+1
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CPROWNUM"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTCODCON"+",PTCODVAL"+",PTDATAPE"+",PTDATDOC"+",PTDATSCA"+",PTFLCRSA"+",PTFLSOSP"+",PTMODPAG"+",PTNUMDOC"+",PTNUMPAR"+",PTROWORD"+",PTSERIAL"+",PTTIPCON"+",PTTOTABB"+",PTTOTIMP"+",PTCODAGE"+",PTNUMCOR"+",PTFLRAGG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTRITE),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.w_NUMRIGA,'PT_SEGNO',this.w_PT_SEGNO,'PTALFDOC',this.oParentObject.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS,'PTCAOAPE',this.w_PTCAOAPE,'PTCAOVAL',this.oParentObject.w_PNCAOVAL,'PTCODCON',this.w_PTCODCON,'PTCODVAL',this.oParentObject.w_PNCODVAL,'PTDATAPE',this.w_PTDATAPE,'PTDATDOC',this.oParentObject.w_PNDATDOC,'PTDATSCA',this.w_PTDATSCA)
      insert into (i_cTable) (CPROWNUM,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTCODCON,PTCODVAL,PTDATAPE,PTDATDOC,PTDATSCA,PTFLCRSA,PTFLSOSP,PTMODPAG,PTNUMDOC,PTNUMPAR,PTROWORD,PTSERIAL,PTTIPCON,PTTOTABB,PTTOTIMP,PTCODAGE,PTNUMCOR,PTFLRAGG &i_ccchkf. );
         values (;
           this.w_NUMRIGA;
           ,this.w_PT_SEGNO;
           ,this.oParentObject.w_PNALFDOC;
           ,this.w_PTBANAPP;
           ,this.w_PTBANNOS;
           ,this.w_PTCAOAPE;
           ,this.oParentObject.w_PNCAOVAL;
           ,this.w_PTCODCON;
           ,this.oParentObject.w_PNCODVAL;
           ,this.w_PTDATAPE;
           ,this.oParentObject.w_PNDATDOC;
           ,this.w_PTDATSCA;
           ,"C";
           ,"S";
           ,this.w_PTMODPAG;
           ,this.oParentObject.w_PNNUMDOC;
           ,this.w_PTNUMPAR;
           ,this.w_ROWORD;
           ,this.oParentObject.w_PNSERIAL;
           ,this.w_PTTIPCON;
           ,0;
           ,this.w_TOTRITE;
           ,this.w_CODAGE;
           ,this.w_PTNUMCOR;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='CCM_DETT'
    this.cWorkTables[2]='COC_MAST'
    this.cWorkTables[3]='COLLCENT'
    this.cWorkTables[4]='MOVICOST'
    this.cWorkTables[5]='PAR_TITE'
    this.cWorkTables[6]='COC_DETT'
    this.cWorkTables[7]='CCC_MAST'
    this.cWorkTables[8]='DATIRITE'
    this.cWorkTables[9]='PNT_MAST'
    this.cWorkTables[10]='PNT_DETT'
    this.cWorkTables[11]='MOD_PAGA'
    this.cWorkTables[12]='ESI_DETT'
    this.cWorkTables[13]='ESI_DIF'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_gscg1bpk')
      use in _Curs_gscg1bpk
    endif
    if used('_Curs_GSCG_BPK')
      use in _Curs_GSCG_BPK
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_CCM_DETT')
      use in _Curs_CCM_DETT
    endif
    if used('_Curs_gscg_bmc')
      use in _Curs_gscg_bmc
    endif
    if used('_Curs_MOVICOST')
      use in _Curs_MOVICOST
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_GSCGABPK')
      use in _Curs_GSCGABPK
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_gscg_qpk')
      use in _Curs_gscg_qpk
    endif
    if used('_Curs_DATIRITE')
      use in _Curs_DATIRITE
    endif
    if used('_Curs_gscg1qpk')
      use in _Curs_gscg1qpk
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
