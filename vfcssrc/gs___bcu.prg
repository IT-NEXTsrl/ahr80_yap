* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gs___bcu                                                        *
*              Caricamento utente nelle aziende selezionate                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-30                                                      *
* Last revis.: 2008-01-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgs___bcu",oParentObject,m.pParam)
return(i_retval)

define class tgs___bcu as StdBatch
  * --- Local variables
  pParam = space(5)
  w_CodNewAMHeader = 0
  CURSORE = space(10)
  w_CodAziSel = space(5)
  w_CntSel = 0
  w_ChiudiMask = .f.
  * --- WorkFile variables
  UTE_AZI_idx=0
  UTE_NTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if !LEFT(this.pParam,4) == "INIT"
      this.w_ChiudiMask = .T.
      this.CURSORE = this.oParentObject.w_GSKSU_ZOOM.cCursor
    endif
    do case
      case this.pParam == "SELEZIONA"
        if this.oParentObject.w_SELEZI=="S"
          * --- Seleziona
          UPDATE (this.CURSORE) SET XCHK = 1
        else
          * --- Deseleziona
          UPDATE (this.CURSORE) SET XCHK = 0
        endif
      case this.pParam == "ABILITA"
        * --- Abilita l'utente attivo negli studi selezionati
        SELECT (this.CURSORE)
        COUNT FOR XCHK=1 TO this.w_CntSel
        if this.w_CntSel > 0
          SCAN FOR XCHK = 1
          this.w_CodAziSel = AZCODAZI
          * --- Insert into UTE_AZI
          i_nConn=i_TableProp[this.UTE_AZI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UTE_AZI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UTE_AZI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"UACODAZI"+",UACODUTE"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CodAziSel),'UTE_AZI','UACODAZI');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CodNew),'UTE_AZI','UACODUTE');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'UACODAZI',this.w_CodAziSel,'UACODUTE',this.oParentObject.w_CodNew)
            insert into (i_cTable) (UACODAZI,UACODUTE &i_ccchkf. );
               values (;
                 this.w_CodAziSel;
                 ,this.oParentObject.w_CodNew;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          ENDSCAN
        else
          if IsAlt()
            this.w_ChiudiMask = not( ah_YesNo("Nessuno studio selezionato%0Vuoi abilitarne almeno uno?", ""))
          else
            this.w_ChiudiMask = not (ah_YesNo("Nessun'azienda selezionata%0Vuoi abilitarne almeno una?", ""))
          endif
        endif
        if this.w_ChiudiMask
          this.oParentObject.ecpQuit()
        endif
      case LEFT(this.pParam,4) == "INIT"
        * --- Controlli all'INIT
        this.w_CodNewAMHeader = VAL(SUBSTR(this.pParam,5))
        this.w_CodAziSel = ""
        * --- Select from UTE_AZI
        i_nConn=i_TableProp[this.UTE_AZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UTE_AZI_idx,2],.t.,this.UTE_AZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select UACODAZI  from "+i_cTable+" UTE_AZI ";
              +" where UACODUTE="+cp_ToStrODBC(this.w_CodNewAMHeader)+"";
               ,"_Curs_UTE_AZI")
        else
          select UACODAZI from (i_cTable);
           where UACODUTE=this.w_CodNewAMHeader;
            into cursor _Curs_UTE_AZI
        endif
        if used('_Curs_UTE_AZI')
          select _Curs_UTE_AZI
          locate for 1=1
          do while not(eof())
          this.w_CodAziSel = UACODAZI
            select _Curs_UTE_AZI
            continue
          enddo
          use
        endif
        if !empty(this.w_CodAziSel)
          * --- Almeno un'azienda selezionata, non � necessario visualizzare la maschera
          i_retcode = 'stop'
          i_retval = .T.
          return
        else
          * --- Deve visualizzare GS___KNU
          i_retcode = 'stop'
          i_retval = .F.
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='UTE_AZI'
    this.cWorkTables[2]='UTE_NTI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_UTE_AZI')
      use in _Curs_UTE_AZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
