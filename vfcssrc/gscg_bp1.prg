* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bp1                                                        *
*              Carica automatismi primanota                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_33]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bp1",oParentObject,m.pParame)
return(i_retval)

define class tgscg_bp1 as StdBatch
  * --- Local variables
  pParame = space(1)
  w_APPO = space(10)
  w_DTOB = ctod("  /  /  ")
  w_DECTOB = 0
  w_SIMVAB = space(6)
  w_CAOVAB = 0
  w_Obj = .NULL.
  * --- WorkFile variables
  CAUIVA1_idx=0
  CAUPRI1_idx=0
  MOD_CONT_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Automatismi in Primanota da Cambio Causale Contabile (Parametro = '1') o Cambio Cli/For (Parametro = '2')
    * --- Inizializza Variabili Globali
    if this.pParame="3"
      * --- Quando premo esc per chiudere il figlio
      if Type("this.oParentObject.gscg_mpa.CNT")="O"
        if this.oParentObject.gscg_mpa.CNT.BoNsCREEN=.T.
          this.oParentObject.gscg_mpa.EcpSave()
        endif
      endif
    endif
    this.oParentObject.w_PNTIPCLF = IIF(this.oParentObject.w_PNTIPREG="V" OR (this.oParentObject.w_PNTIPREG $ "CE" AND this.oParentObject.w_PNTIPDOC="FC"),"C",IIF(this.oParentObject.w_PNTIPREG="A","F", this.oParentObject.w_FLRIFE))
    if this.pParame="1" AND NOT EMPTY(NVL(this.oParentObject.w_PNCODCAU, SPACE(5)))
      * --- Eventuale Azzeramento Documento/Protocollo
      this.oParentObject.w_PNANNDOC = CALPRO(IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNCOMPET, this.oParentObject.w_FLPDOC)
      this.oParentObject.w_PNPRD = IIF(this.oParentObject.w_PNTIPREG="V" OR (this.oParentObject.w_PNTIPREG="C" AND this.oParentObject.w_PNTIPDOC="FC"), "FV", "NN")
      this.oParentObject.w_SALDOC = IIF(this.oParentObject.w_PNNUMDOC<>0, this.oParentObject.w_PNNUMDOC, this.oParentObject.w_SALDOC)
      this.oParentObject.w_PNNUMDOC = IIF(this.oParentObject.w_PNTIPREG="V" OR NOT EMPTY(NVL(this.oParentObject.w_PNANNDOC, " ")), this.oParentObject.w_SALDOC, 0)
      if EMPTY(this.oParentObject.w_PNANNDOC)
        this.oParentObject.op_PNPRD = this.oParentObject.w_PNPRD
        this.oParentObject.op_PNANNDOC = this.oParentObject.w_PNANNDOC
      endif
      this.oParentObject.w_PNANNPRO = CALPRO(this.oParentObject.w_PNDATREG, this.oParentObject.w_PNCOMPET, this.oParentObject.w_FLPPRO)
      this.oParentObject.w_PNPRP = IIF(this.oParentObject.w_PNTIPREG="A", "AC", "NN")
      this.oParentObject.w_SALPRO = IIF(this.oParentObject.w_PNNUMPRO<>0, this.oParentObject.w_PNNUMPRO, this.oParentObject.w_SALPRO)
      this.oParentObject.w_PNNUMPRO = IIF(this.oParentObject.w_PNTIPREG="A" OR NOT EMPTY(NVL(this.oParentObject.w_PNANNPRO, " ")), this.oParentObject.w_SALPRO, 0)
      if EMPTY(this.oParentObject.w_PNANNPRO)
        this.oParentObject.op_PNPRP = this.oParentObject.w_PNPRP
        this.oParentObject.op_PNANNPRO = this.oParentObject.w_PNANNPRO
      endif
    endif
    if this.pParame="4"
      * --- Scorporo l'iva calcolando il totale doc. in EURO
      this.w_Obj = this.oParentObject.GSCG_MIV
      this.w_Obj.NotifyEvent("CaricaAuto")     
    endif
    * --- Da Abilitare solo in Caricamento ...
    if this.oParentObject.cFunction="Load"
      do case
        case this.pParame="1" AND NOT EMPTY(NVL(this.oParentObject.w_PNCODCAU, SPACE(5)))
          * --- Input su Causale - Azzera dati del Cli/For
          this.oParentObject.w_PNCODCLF = SPACE(15)
          this.oParentObject.w_DESCLF = SPACE(35)
          this.oParentObject.w_DTOBSO = cp_CharToDate("  -  -  ")
          this.oParentObject.w_CODPAG = SPACE(5)
          this.oParentObject.w_TOTDAR = 0
          this.oParentObject.w_TOTAVE = 0
          this.oParentObject.w_SBILAN = 0
          * --- Riazzero w_GIACAR per ricalcolare l'automatismo se cambio la causale.
          *     (nella pagina Special Definition del campo PNTIPCON del Dettaglio viene rilanciato il GSCG_BP3
          *     che ricalcola l'automatismo)
          this.oParentObject.w_GIACAR = " "
          * --- Azzera Righe IVA
          SELECT (this.oParentObject.GSCG_MIV.cTrsName)
          if reccount()<>0
            this.oParentObject.GSCG_MIV.BlankRec()
          endif
          * --- Azzera Righe Contabili
          SELECT (this.oParentObject.cTrsName)
          if reccount()<>0
            ZAP
            * --- Nuova Riga del Temporaneo
             
 this.oParentObject.InitRow()
          endif
          * --- Legge Automatismi legati alla Causale
          * --- w_CHKAUT: 0 =No Automatismi; 1 =Automatismo su Causale
          this.oParentObject.w_CHKAUT = 0
          this.w_APPO = "#####"
          * --- Read from CAUPRI1
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAUPRI1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2],.t.,this.CAUPRI1_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "APCODCAU"+;
              " from "+i_cTable+" CAUPRI1 where ";
                  +"APCODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              APCODCAU;
              from (i_cTable) where;
                  APCODCAU = this.oParentObject.w_PNCODCAU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPO = NVL(cp_ToDate(_read_.APCODCAU),cp_NullValue(_read_.APCODCAU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_CHKAUT = IIF(i_rows<>0, 1, 0)
          if this.oParentObject.w_CHKAUT=0 AND this.oParentObject.w_PNTIPREG<>"N"
            this.w_APPO = "#####"
            * --- Read from CAUIVA1
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAUIVA1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AICODCAU"+;
                " from "+i_cTable+" CAUIVA1 where ";
                    +"AICODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AICODCAU;
                from (i_cTable) where;
                    AICODCAU = this.oParentObject.w_PNCODCAU;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APPO = NVL(cp_ToDate(_read_.AICODCAU),cp_NullValue(_read_.AICODCAU))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_CHKAUT = IIF(i_rows<>0, 1, 0)
          endif
        case this.pParame = "2" AND NOT EMPTY(NVL(this.oParentObject.w_PNCODCLF, SPACE(15)))
          * --- Variato Input su CliFor - Verifica se esiste un Modello Associato
          * --- w_CHKAUT: 0 =No Automatismi; 1 =Automatismo su Causale; 2 =Automatismo su Modello
          this.w_APPO = "###############"
          * --- Read from MOD_CONT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOD_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOD_CONT_idx,2],.t.,this.MOD_CONT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCCODCON"+;
              " from "+i_cTable+" MOD_CONT where ";
                  +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
                  +" and CCTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCLF);
                  +" and CCCODCON = "+cp_ToStrODBC(this.oParentObject.w_PNCODCLF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCCODCON;
              from (i_cTable) where;
                  CCCODICE = this.oParentObject.w_PNCODCAU;
                  and CCTIPCON = this.oParentObject.w_PNTIPCLF;
                  and CCCODCON = this.oParentObject.w_PNCODCLF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPO = NVL(cp_ToDate(_read_.CCCODCON),cp_NullValue(_read_.CCCODCON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_CHKAUT = IIF(i_rows<>0, 2, this.oParentObject.w_CHKAUT)
          if this.oParentObject.w_PNCODVAL<>this.oParentObject.w_CLFVAL AND NOT EMPTY(NVL(this.oParentObject.w_CLFVAL," "))
            * --- Se causale in Valuta, Prende la Valuta del Cli/For
            this.w_DTOB = cp_CharToDate("  -  -  ")
            this.w_DECTOB = 0
            this.w_SIMVAB = " "
            this.w_CAOVAB = 0
            * --- Read from VALUTE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VALUTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VASIMVAL,VADECTOT,VADTOBSO"+;
                " from "+i_cTable+" VALUTE where ";
                    +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_CLFVAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VASIMVAL,VADECTOT,VADTOBSO;
                from (i_cTable) where;
                    VACODVAL = this.oParentObject.w_CLFVAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SIMVAB = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
              this.w_DECTOB = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
              this.w_DTOB = NVL(cp_ToDate(_read_.VADTOBSO),cp_NullValue(_read_.VADTOBSO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_CAOVAB = GETCAM(g_PERVAL, I_DATSYS)
            if CHKDTOBS(this.w_DTOB,this.oParentObject.w_OBTEST,"Valuta obsoleta alla data attuale", .F.)
              * --- Valuta Cli/For OK
              this.oParentObject.w_PNCODVAL = this.oParentObject.w_CLFVAL
              this.oParentObject.w_CAOVAL = this.w_CAOVAB
              this.oParentObject.w_SIMVAL = this.w_SIMVAB
              this.oParentObject.w_DECTOT = this.w_DECTOB
              this.oParentObject.w_PNCAOVAL = this.w_CAOVAB
              this.bUpdateParentObject=.f.
            else
              if this.oParentObject.w_PNTIPCLF="F"
                ah_ErrorMsg("Valuta fornitore obsoleta alla data attuale (%1)",,"",this.oParentObject.w_CLFVAL)
              else
                ah_ErrorMsg("Valuta cliente obsoleta alla data attuale (%1)",,"",this.oParentObject.w_CLFVAL)
              endif
            endif
          endif
      endcase
    endif
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CAUIVA1'
    this.cWorkTables[2]='CAUPRI1'
    this.cWorkTables[3]='MOD_CONT'
    this.cWorkTables[4]='VALUTE'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
