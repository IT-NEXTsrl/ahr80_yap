* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bin                                                        *
*              Elabora inventario                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_357]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-08                                                      *
* Last revis.: 2010-08-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Evento
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bin",oParentObject,m.w_Evento)
return(i_retval)

define class tgsma_bin as StdBatch
  * --- Local variables
  w_Evento = space(10)
  w_PAG2 = space(20)
  w_MAGRAG = space(5)
  w_ULTDATPRZ = ctod("  /  /  ")
  w_ULTVALPRZ = 0
  w_ULTMAGPRZ = space(5)
  w_ULTMATPRZ = space(5)
  w_ULTAGGPRZ = space(2)
  w_ULTDATCOS = ctod("  /  /  ")
  w_ULTVALCOS = 0
  w_ULTMAGCOS = space(5)
  w_ULTMAGCOS = space(5)
  w_ULTMATCOS = space(5)
  w_ULTAGGCOS = space(2)
  w_COSMPAMOV = 0
  w_ESIMPAMOV = 0
  w_COSMPPMOV = 0
  w_INSPRZ = .f.
  w_INSCOS = .f.
  w_EMPTYAGGVAL = .f.
  w_INNUMINV = space(6)
  w_INDATINV = ctod("  /  /  ")
  w_INCODESE = space(4)
  w_INNUMPRE = space(6)
  w_INESEPRE = space(4)
  w_INCODMAG = space(5)
  w_INCATOMO = space(5)
  w_INGRUMER = space(5)
  w_INCODFAM = space(5)
  w_INDESINV = space(40)
  w_INTIPINV = space(1)
  w_INSTAINV = space(1)
  w_INELABOR = space(1)
  w_INMAGCOL = space(1)
  w_INFLGLCO = space(1)
  w_INFLGLSC = space(1)
  w_INFLGFCO = space(1)
  w_PRDATINV = ctod("  /  /  ")
  w_FLREPO = space(1)
  w_ESINIESE = ctod("  /  /  ")
  w_FLMESS = space(1)
  w_VALESE = space(3)
  w_VALPRE = space(3)
  w_DECESE = 0
  w_CAOESE = 0
  w_CAOPRE = 0
  w_RESCHK = 0
  w_ESCONT = space(1)
  w_PrzUlt = 0
  w_RecElab = 0
  w_CosMpa = 0
  w_PqtMpp = 0
  w_CosMpp = 0
  w_QtaScagl = 0
  w_CqtMpp = 0
  w_CpRowNum = 0
  w_CosUlt = 0
  w_Mess = space(10)
  w_NEWREC = space(10)
  w_Appo = space(20)
  w_TmpN = 0
  w_Appo1 = 0
  w_QtaEsiUno = 0
  w_UltimoArt = space(40)
  w_IsDatMov = ctod("  /  /  ")
  w_CosSta = 0
  w_IsDatFin = ctod("  /  /  ")
  w_CosLco = 0
  w_IsQtaEsi = ctod("  /  /  ")
  w_CosLsc = 0
  w_IsValUni = 0
  w_CosFco = 0
  w_DataPrzUlt = ctod("  /  /  ")
  w_DataCosUlt = ctod("  /  /  ")
  w_TipValor = space(1)
  w_ValQta = space(1)
  w_TipMagaz = space(1)
  w_ValTip = space(1)
  w_Segnalato = .f.
  w_DatReg = space(10)
  w_Trec = 0
  w_PrePreUlt = 0
  w_QtaVenUno = 0
  w_PreQtaEsr = 0
  w_QtaAcqUno = 0
  w_PreQtaVer = 0
  w_PreQtaAcr = 0
  w_QtaEsiWrite = 0
  w_QtaEsr = 0
  w_QtaVenWrite = 0
  w_QtaVer = 0
  w_QtaAcqWrite = 0
  w_Messaggio = space(10)
  w_FlaVal = space(10)
  w_Codice = space(40)
  w_CriVal = space(10)
  w_PreQtaEsi = 0
  w_PreQtaVen = 0
  w_QtaEsi = 0
  w_PreQtaAcq = 0
  w_QtaVen = 0
  w_PrePrzMpa = 0
  w_QtaAcq = 0
  w_PreCosMpa = 0
  w_PrzMpa = 0
  w_PreCosUlt = 0
  w_PrzMpp = 0
  w_ElenMag = space(10)
  w_QtaAcr = 0
  w_Maga = space(5)
  w_SLCODART = space(20)
  w_QtaVerWrite = 0
  w_QtaAcrWrite = 0
  w_Vendite = 0
  w_Acquisti = 0
  w_PrzMpaWrite = 0
  w_CosMpaWrite = 0
  w_ValUni = 0
  w_Reso = .f.
  w_F2CASC = space(1)
  w_CARVAL = 0
  w_SCAVAL = 0
  w_PreCarVal = 0
  w_PreScaVal = 0
  w_MovCosMpaWrite = 0
  w_MovPrzMpaWrite = 0
  w_bMovCosMpaWrite = .f.
  w_bMovPrzMpaWrite = .f.
  w_VarValoAcq = 0
  w_VarValoVen = 0
  w_Old_PrzMpp = 0
  w_Old_CosMpp = 0
  w_DESART = space(40)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_CODART = space(20)
  * --- WorkFile variables
  INVEDETT_idx=0
  LIFOSCAT_idx=0
  LIFOCONT_idx=0
  FIFOCONT_idx=0
  INVENTAR_idx=0
  VALUTE_idx=0
  MAGAZZIN_idx=0
  ESERCIZI_idx=0
  CAM_AGAZ_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Inventario (da GSMA_AIN)
    * --- L'elaborazione dell'inventario, in caso di update, viene effettuata solo se � stato modificato un campo
    * --- Valori previsti: INSERTED (dopo l'inserimento), UPDATED (dopo la variazione)
    * --- che non sia lo stato o la descrizione: questo viene fatto testando la w_UPDINV, calcolata in GSMA_BVS
    this.w_INELABOR = this.oParentObject.w_INELABOR
    this.w_PAG2 = "DICHIARAZIONI"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_PAG2 = "CURSORI"
    * --- D'ora in avanti la pag2 sar� chiamata solo per chiudere i cursori
    * --- Controlla se l'inventario � storico o usato come riferimento e blocca l'elaborazione
    if this.w_INSTAINV="S" or this.w_RESCHK<>0
      i_retcode = 'stop'
      return
    endif
    * --- Richiesta conferma
    this.w_Messaggio = "ATTENZIONE%0L'elaborazione dell'inventario pu� richiedere molto tempo in relazione alla dimensione degli archivi%0%0Confermi l'elaborazione?"
    if NOT ah_YesNo(this.w_Messaggio)
      i_retcode = 'stop'
      return
    endif
    * --- Verifica campi obbligatori e compilazione elenco magazzini
    this.w_MAGRAG = SPACE(5)
    * --- Read from MAGAZZIN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MGMAGRAG"+;
        " from "+i_cTable+" MAGAZZIN where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.w_INCODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MGMAGRAG;
        from (i_cTable) where;
            MGCODMAG = this.w_INCODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MAGRAG = NVL(cp_ToDate(_read_.MGMAGRAG),cp_NullValue(_read_.MGMAGRAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MAGRAG = IIF (EMPTY(NVL(this.w_MAGRAG,"")),this.w_INCODMAG,this.w_MAGRAG)
    this.Pag8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ah_Msg("Lettura documenti variazione valore...")
    vq_exec("query\GSMA_BIN_V",this,"Var_valo")
    Select Var_valo
    INDEX ON CODART TAG DATART_V
    if RecCount("Var_Valo")>0 And (this.w_INFLGLCO = "S" .or. this.w_INFLGFCO = "S")
      this.w_Messaggio = "ATTENZIONE%0L'elaborazione dell'inventario LIFO/FIFO continuo pu� dare risultati inattendibili,%0in quanto esistono dei documenti di variazione a valore%0Confermi l'elaborazione?"
      if NOT ah_YesNo(this.w_Messaggio)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Eliminazione elaborazione precedente
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Creazione cursore articoli compresi in inventario
     
 create cursor AppMov ; 
 (CACODART C(20), MMCODMAG C(5), CMFLAVAL C(1), CMAGGVAL C(2) ,CMFLCASC C(1), ; 
 VALORE N(18,5), MMDATREG D(8),MMCODMAT C(5) Null)
    ah_Msg("Lettura movimenti di magazzino %1...",.T.,.F.,.F.,"1")
    * --- Determino ultimo prezzo / ultimo costo, quindi il valore unitario dell'ultima 
    *     riga di scarico/scarico valutata dall'elaborazione.
    *     
    *     Inoltre se si � selezionata la modalit� di calcolo del costo medio per
    *     esercizio per movimento, qui � determinata anche quella..
    *     
    *     Il dato � ordinato per articolo e successivamente per i movimenti
    *     in ordine ascedente (l'ultimo avr� il dato del prezzo/costo).
    *     Questo ordinamento mi � necessario per determinare il costo medio per
    *     movimento.
    this.w_NEWREC = Repl("Z", 30)
    this.w_ULTVALPRZ = 0
    this.w_ULTVALCOS = 0
    this.w_COSMPAMOV = 0
    this.w_COSMPPMOV = 0
    this.w_INSPRZ = .F.
    this.w_INSCOS = .F.
    * --- Select from QUERY\GSMAIBIN
    do vq_exec with 'QUERY\GSMAIBIN',this,'_Curs_QUERY_GSMAIBIN','',.f.,.t.
    if used('_Curs_QUERY_GSMAIBIN')
      select _Curs_QUERY_GSMAIBIN
      locate for 1=1
      do while not(eof())
      * --- Al cambio articolo...
      if this.w_NEWREC <>_Curs_QUERY_GSMAIBIN.MMCODART
        * --- Messaggio a Video
        this.w_Mess = " Elabora ultimo costo / prezzo articolo: " + ALLTRIM(_Curs_QUERY_GSMAIBIN.MMCODART) + chr(13)
        wait wind this.w_Mess nowait
        if this.w_INSPRZ
           
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGPRZ , this.w_ULTVALPRZ , ; 
 "V", this.w_ULTAGGPRZ , "-", this.w_ULTDATPRZ, this.w_ULTMATPRZ )
        endif
        if this.w_INSCOS
           
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGCOS , this.w_ULTVALCOS , ; 
 "A", this.w_ULTAGGCOS , "+", this.w_ULTDATCOS, this.w_ULTMATCOS)
        endif
        this.w_ULTVALPRZ = 0
        this.w_ULTVALCOS = 0
        this.w_INSPRZ = .F.
        this.w_INSCOS = .F.
        * --- Se inventario con calcolo costo medio esercizio per movimento
        *     devo, partendo dall'inventario precedente, calcolare il costo medio
        *     non come somma dei valori di carico / somma delle quantit�
        *     di carico ma movimento per movimento come costo medio attuale per 
        *     esistenza + valore movimento / esistenza + quantit� movimento...
        if this.w_ESCONT="M"
          if this.w_NEWREC<>Repl("Z", 30)
             
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPAMOV , ; 
 "A", "XX" , "+", i_datsys , Space(5))
             
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPPMOV , ; 
 "V", "XX" , "-", i_datsys , Space(5))
          endif
          this.w_CODART = _Curs_QUERY_GSMAIBIN.MMCODART
          this.Pag11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_COSMPAMOV = this.w_PreCosMpa
          this.w_COSMPPMOV = this.w_PrePrzMpa
          this.w_ESIMPAMOV = this.w_PreQtaEsr
        endif
        this.w_NEWREC = _Curs_QUERY_GSMAIBIN.MMCODART
      endif
      if _Curs_QUERY_GSMAIBIN.CMFLAVAL$"V|S" And _Curs_QUERY_GSMAIBIN.CMFLCASC="-" And ((_Curs_QUERY_GSMAIBIN.CMAGGVAL="SN" OR _Curs_QUERY_GSMAIBIN.CMAGGVAL="NS" OR(_Curs_QUERY_GSMAIBIN.CMAGGVAL="SS" AND NOT (NVL(_Curs_QUERY_GSMAIBIN.mmcodmat,"") $ this.w_ElenMag ))))
        this.w_ULTDATPRZ = _Curs_QUERY_GSMAIBIN.MMDATREG
        this.w_ULTVALPRZ = _Curs_QUERY_GSMAIBIN.ValUni
        this.w_ULTMAGPRZ = _Curs_QUERY_GSMAIBIN.MMCODMAG
        this.w_ULTMATPRZ = NVL( _Curs_QUERY_GSMAIBIN.MMCODMAT , "     " )
        this.w_ULTAGGPRZ = _Curs_QUERY_GSMAIBIN.CMAGGVAL
        this.w_INSPRZ = .T.
      endif
      * --- Aggiornamento costi e valori: nel caso di valore da aggiornare: acquistato, visto che
      *     il check aggiornamento valori � sempre attivo, sempre.
      *     
      *     Nel caso di altri carichi, solo se attivo il check aggiornamento valori
      if _Curs_QUERY_GSMAIBIN.CMFLAVAL$"A|C" And _Curs_QUERY_GSMAIBIN.CMFLCASC="+" And ((_Curs_QUERY_GSMAIBIN.CMAGGVAL="SN" OR _Curs_QUERY_GSMAIBIN.CMAGGVAL="NS" OR(_Curs_QUERY_GSMAIBIN.CMAGGVAL="SS" AND NOT (NVL(_Curs_QUERY_GSMAIBIN.mmcodmat,"") $ this.w_ElenMag ))))
        this.w_ULTDATCOS = _Curs_QUERY_GSMAIBIN.MMDATREG
        this.w_ULTVALCOS = _Curs_QUERY_GSMAIBIN.ValUni
        this.w_ULTMAGCOS = _Curs_QUERY_GSMAIBIN.MMCODMAG
        this.w_ULTMATCOS = NVL( _Curs_QUERY_GSMAIBIN.MMCODMAT , "     " )
        this.w_ULTAGGCOS = _Curs_QUERY_GSMAIBIN.CMAGGVAL
        this.w_INSCOS = .T.
      endif
      if this.w_ESCONT="M" And ((_Curs_QUERY_GSMAIBIN.CMAGGVAL="SN" OR _Curs_QUERY_GSMAIBIN.CMAGGVAL="NS" OR(_Curs_QUERY_GSMAIBIN.CMAGGVAL="SS" AND NOT (NVL(_Curs_QUERY_GSMAIBIN.mmcodmat,"") $ this.w_ElenMag ))))
        * --- Se costo medio esercizio calcolato sui movimenti possono accadere tre cose:
        *     a) � un movimento di carico
        *     b) � un movimento di scarico
        *     c) � un movimento di storno valore
        *     Nel caso a) ricalcolo il costo medio utilizzando il totalizzatore costo ed esistenze
        *     ed i dati sul movimento andando anche a variare il totalizzatore esistenza
        *     Nel caso b) vario solo il totalizzatore Esistenza mentre nel caso c) ridetermino
        *     il totalizzatore del costo medio ponderato con la formula:
        *     
        *     Analogamente per il prezzo...
        *     (totalizzatore costo medio * totalizzatore esistenza - variazione a valore )/esistenza
        do case
          case _Curs_QUERY_GSMAIBIN.CMFLAVAL$"A|C"
            if _Curs_QUERY_GSMAIBIN.CMFLCASC$"X"
              * --- Variazione di valore...
              this.w_COSMPAMOV = iif( this.w_ESIMPAMOV<>0 , (( this.w_COSMPAMOV * this.w_ESIMPAMOV ) + _Curs_QUERY_GSMAIBIN.Valore )/ this.w_ESIMPAMOV, 0 )
            else
              if _Curs_QUERY_GSMAIBIN.Esistenza + this.w_ESIMPAMOV <>0
                this.w_COSMPAMOV = (( this.w_COSMPAMOV * this.w_ESIMPAMOV ) + _Curs_QUERY_GSMAIBIN.Valore )/ ( _Curs_QUERY_GSMAIBIN.Esistenza + this.w_ESIMPAMOV )
              else
                this.w_COSMPAMOV = 0
              endif
              this.w_ESIMPAMOV = this.w_ESIMPAMOV + _Curs_QUERY_GSMAIBIN.Esistenza
            endif
          case _Curs_QUERY_GSMAIBIN.CMFLAVAL$"V|S"
            if _Curs_QUERY_GSMAIBIN.CMFLCASC$"X"
              this.w_COSMPPMOV = iif( this.w_ESIMPAMOV<>0 , (( this.w_COSMPPMOV * this.w_ESIMPAMOV ) + _Curs_QUERY_GSMAIBIN.Valore )/ this.w_ESIMPAMOV, 0 )
            else
              if _Curs_QUERY_GSMAIBIN.Esistenza + this.w_ESIMPAMOV <>0
                this.w_COSMPPMOV = (( this.w_COSMPPMOV * this.w_ESIMPAMOV ) + _Curs_QUERY_GSMAIBIN.Valore )/ ( _Curs_QUERY_GSMAIBIN.Esistenza + this.w_ESIMPAMOV )
              else
                this.w_COSMPPMOV = 0
              endif
              this.w_ESIMPAMOV = this.w_ESIMPAMOV + _Curs_QUERY_GSMAIBIN.Esistenza
            endif
        endcase
      endif
        select _Curs_QUERY_GSMAIBIN
        continue
      enddo
      use
    endif
    if this.w_INSPRZ And this.w_NEWREC<>Repl("Z", 30)
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGPRZ , this.w_ULTVALPRZ , ; 
 "V", this.w_ULTAGGPRZ , "-", this.w_ULTDATPRZ, this.w_ULTMATPRZ )
    endif
    if this.w_INSCOS And this.w_NEWREC<>Repl("Z", 30)
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGCOS , this.w_ULTVALCOS , ; 
 "A", this.w_ULTAGGCOS , "+", this.w_ULTDATCOS, this.w_ULTMATCOS)
    endif
    if this.w_ESCONT="M" And this.w_NEWREC<>Repl("Z", 30)
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPAMOV , ; 
 "A", "XX" , "+", i_datsys , Space(5))
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPPMOV , ; 
 "V", "XX" , "-", i_datsys , Space(5))
    endif
    this.w_EMPTYAGGVAL = .F.
    if EMPTY(this.w_INNUMPRE) And Reccount("APPMOV")=0
      * --- Aggiornamento valori vuoto
      this.w_EMPTYAGGVAL = .T.
    endif
    ah_Msg("Lettura movimenti di magazzino %1...",.T.,.F.,.F.,"2")
    * --- Leggo i movimenti che costituiranno la base per calcolare inventario
    vq_exec("query\GSMA_BIN",this,"__tmp__")
    if reccount("__tmp__")=0 And this.w_EMPTYAGGVAL
      ah_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare")
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Raggruppamento dati ed eliminazione delle informazioni non necessarie
    ah_Msg("Raggruppamento movimenti per articolo...")
    * --- 1) La select seguente crea piu' records di totali per ogni articolo distinti per Tipo Movimento e Criterio di Valorizzazione.
    * --- 2) Rispetto alla visual query raggruppa record con le stesse caratteristiche che erano rimasti separati perche' provenienti
    * --- da archivi differenti (TipoRiga: A=Articoli, M=Mov.Magazzino, V=Vendite).
    * --- 3) Per le righe di tipo articolo (TipoRiga=A) la data di registrazione non deve essere considerata.
     
 select cacodart, mmcodmag, "A" as Ordine, ; 
 max(ctod("  -  -  ")) as mmdatreg, ; 
 sum(iif(cauprival $ "AC", MagPriCar-MagPriSca, MagPriCar*0)) as Acquisti, ; 
 sum(iif(cauprival $ "VS", MagPriSca-MagPriCar, MagPriSca*0)) as Vendite, ; 
 sum(iif(cauprival $ "AC", Valore, Valore*0)) as ValAcqu, ; 
 sum(iif(cauprival $ "VS", Valore, Valore*0)) as ValVend, ; 
 CauPriVal as CauPriVal, ; 
 CauAggVal as CauAggVal, ; 
 CauPriEsi as CauPriEsi, ; 
 sum(999999999999.99999*0) as Valore , nvl(mmcodmat, space(5)) as mmcodmat, ; 
 sum(iif(cauprival $ "AC", MAGPRICAV-MAGPRISCV, MAGPRICAV*0)) as CARVAL, ; 
 sum(iif(cauprival $ "VS", MAGPRISCV-MAGPRICAV, MAGPRISCV*0)) as SCAVAL; 
 from __tmp__ ; 
 group by cacodart, mmcodmag, Ordine, CauPriVal, CauAggVal, CauPriEsi, mmcodmat ; 
 union ; 
 select cacodart, mmcodmag, "Z" as Ordine, ; 
 mmdatreg as mmdatreg, ; 
 0 as Acquisti, ; 
 0 as Vendite, ; 
 0 as ValAcqu, ; 
 0 as ValVend, ; 
 CMFLAVAL as CauPriVal , ; 
 CMAGGVAL as CauAggVal , ; 
 CMFLCASC as CauPriEsi, ; 
 VALORE as Valore , MMCODMAT as mmcodmat, ; 
 0 as CARVAL, ; 
 0 as SCAVAL ; 
 from AppMov ; 
 group by cacodart, mmcodmag, Ordine, CauPriVal, CauAggVal, CauPriEsi,mmcodmat ; 
 order by 1,2,3 ; 
 into cursor __tmp__ 
 
    select AppMov
    use
    if this.w_INFLGLSC = "S" .or. this.w_INFLGLCO = "S" .or. this.w_INFLGFCO = "S"
      ah_Msg("Elabora movimenti per calcoli LIFO/FIFO...")
      * --- Ordinati per Codice Articolo, Data registrazione, Seq. Elaborazione, Serial
      vq_exec("query\GSMAABIN",this,"FifoLifo")
      SELECT FifoLifo
      INDEX ON MMCODART TAG CODART
    endif
    ah_Msg("Lettura parametri articoli/magazzini...")
    vq_exec("query\GSMAUBIN",this,"DatiArti")
    SELECT DatiArti
    INDEX ON PRCODART+PRCODMAG TAG DATART
    * --- Memorizzazione anagrafica inventario con scrittura del flag elaborato
    ah_Msg("Aggiornamento anagrafica inventario...")
    * --- Write into INVENTAR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.INVENTAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.INVENTAR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"INELABOR ="+cp_NullLink(cp_ToStrODBC("S"),'INVENTAR','INELABOR');
          +i_ccchkf ;
      +" where ";
          +"INNUMINV = "+cp_ToStrODBC(this.w_INNUMINV);
          +" and INCODESE = "+cp_ToStrODBC(this.w_INCODESE);
             )
    else
      update (i_cTable) set;
          INELABOR = "S";
          &i_ccchkf. ;
       where;
          INNUMINV = this.w_INNUMINV;
          and INCODESE = this.w_INCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elaborazione, valorizzazioni ed aggiornamento archivi storici inventario
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- LOG Errori
    if this.w_FLREPO="S" AND Used("MessErr") 
      if RECCOUNT("MessErr") > 0
        if ah_YesNo("Elaborazione inventario terminata%0Stampo messaggi di errore?")
          SELECT * FROM MessErr INTO CURSOR __TMP__
          CP_CHPRN("QUERY\GSVE_BCV.FRX", " ", this)
        endif
      else
        ah_ErrorMsg("Elaborazione inventario terminata")
      endif
    else
      ah_ErrorMsg("Elaborazione inventario terminata")
    endif
    * --- Chiusura cursori
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_PAG2="CURSORI"
        * --- Chiusura cursori
        if used("__tmp__")
          select __tmp__
          use
        endif
        if USED("MessErr")
          SELECT MessErr
          USE
        endif
        if used("FifoLifo")
          select FifoLifo
          use
        endif
        if used("DatiArti")
          select DatiArti
          use
        endif
        if used("valori")
          select valori
          use
        endif
        if used("magazzini")
          select magazzini
          use
        endif
        if used("ScLiCo")
          select ScLiCo
          use
        endif
        if used("ScLiSc")
          select ScLiSc
          use
        endif
        if used("ScFiCo")
          select ScFiCo
          use
        endif
        if used("ScPrec")
          select ScPrec
          use
        endif
        if used("AppMov")
          select AppMov
          use
        endif
        if used("Var_valo")
          select Var_valo
          use
        endif
        if USED("SommAlf")
          SELECT SommAlf
          USE
        endif
      case this.w_PAG2="DICHIARAZIONI"
        * --- Variabili locali inizializzate con il valore dei campi dell'anagrafica
        * --- Vengono inizializzate in modo diverso dal solito perche' devono essere letti i valori dei campi
        * --- dell'anagrafica al momento della chiamata del batch. Questo e' necessario perche' nelle righe
        * --- successive viene richiamato l'evento di salvataggio dell'anagrafica che, dopo aver memorizzato
        * --- i records, ripulisce il contenuto dei campi.
        this.w_ESCONT = NVL(this.oParentObject.w_INESCONT,"N")
        this.w_INNUMINV = this.oParentObject.w_INNUMINV
        this.w_INDATINV = this.oParentObject.w_INDATINV
        this.w_INCODESE = this.oParentObject.w_INCODESE
        this.w_INNUMPRE = this.oParentObject.w_INNUMPRE
        this.w_INESEPRE = this.oParentObject.w_INESEPRE
        this.w_INCODMAG = this.oParentObject.w_INCODMAG
        this.w_INCATOMO = this.oParentObject.w_INCATOMO
        this.w_INGRUMER = this.oParentObject.w_INGRUMER
        this.w_INCODFAM = this.oParentObject.w_INCODFAM
        this.w_INDESINV = this.oParentObject.w_INDESINV
        this.w_INTIPINV = this.oParentObject.w_INTIPINV
        this.w_INSTAINV = this.oParentObject.w_INSTAINV
        this.w_INMAGCOL = this.oParentObject.w_INMAGCOL
        this.w_INFLGLCO = this.oParentObject.w_INFLGLCO
        this.w_INFLGLSC = this.oParentObject.w_INFLGLSC
        this.w_INFLGFCO = this.oParentObject.w_INFLGFCO
        this.w_PRDATINV = this.oParentObject.w_PRDATINV
        this.w_ESINIESE = this.oParentObject.w_ESINIESE
        this.w_FLMESS = this.oParentObject.w_FLMESS
        this.w_VALESE = this.oParentObject.w_VALESE
        this.w_VALPRE = this.oParentObject.w_VALPRE
        this.w_FLREPO = this.oParentObject.w_FLREPO
        this.w_RESCHK = this.oParentObject.w_RESCHK
        * --- Legge i Riferimenti alle Valute Inventari
        this.w_DECESE = GETVALUT(this.w_VALESE, "VADECUNI")
        this.w_CAOESE = GETCAM(this.w_VALESE, I_DATSYS)
        this.w_CAOPRE = this.w_CAOESE
        if NOT EMPTY(this.w_INNUMPRE)
          this.w_CAOPRE = GETCAM(this.w_VALPRE, I_DATSYS)
        endif
        * --- Variabili locali
        this.w_Messaggio = ""
        this.w_FlaVal = ""
        this.w_Codice = 0
        this.w_CriVal = ""
        this.w_CODART = ""
        this.w_PreQtaEsi = 0
        this.w_PreQtaVen = 0
        this.w_QtaEsi = 0
        this.w_PreQtaAcq = 0
        this.w_QtaVen = 0
        this.w_PrePrzMpa = 0
        this.w_QtaAcq = 0
        this.w_PreCosMpa = 0
        this.w_PrzMpa = 0
        this.w_PreCosUlt = 0
        this.w_PrzMpp = 0
        this.w_ElenMag = ""
        this.w_PrzUlt = 0
        this.w_RecElab = 0
        this.w_CosMpa = 0
        this.w_PqtMpp = 0
        this.w_CosMpp = 0
        this.w_QtaScagl = 0
        this.w_CqtMpp = 0
        this.w_CpRowNum = 0
        this.w_CosUlt = 0
        this.w_IsDatMov = cp_CharToDate("  -  -  ")
        this.w_CosSta = 0
        this.w_IsDatFin = cp_CharToDate("  -  -  ")
        this.w_CosLco = 0
        this.w_IsQtaEsi = 0
        this.w_CosLsc = 0
        this.w_IsValUni = 0
        this.w_CosFco = 0
        this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
        this.w_UltimoArt = ""
        this.w_DataCosUlt = cp_CharToDate("  -  -  ")
        this.w_TipValor = ""
        this.w_ValQta = 0
        this.w_TipMagaz = ""
        this.w_ValTip = 0
        this.w_Segnalato = .F.
        this.w_DatReg = cp_CharToDate("  -  -  ")
        this.w_Trec = 0
        this.w_QtaEsiUno = 0
        this.w_PrePreUlt = 0
        this.w_QtaVenUno = 0
        this.w_PreQtaEsr = 0
        this.w_QtaAcqUno = 0
        this.w_PreQtaVer = 0
        this.w_PreQtaAcr = 0
        this.w_QtaEsiWrite = 0
        this.w_QtaEsr = 0
        this.w_QtaVenWrite = 0
        this.w_QtaVer = 0
        this.w_QtaAcqWrite = 0
        this.w_QtaAcr = 0
        this.w_QtaVerWrite = 0
        this.w_QtaAcrWrite = 0
        this.w_Vendite = 0
        this.w_Acquisti = 0
        this.w_CARVAL = 0
        this.w_SCAVAL = 0
        this.w_PreCarVal = 0
        this.w_PreScaVal = 0
        this.w_MovCosMpaWrite = 0
        this.w_MovPrzMpaWrite = 0
    endcase
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazioni dati presenti in inventario
    * --- Aggiornamento archivi
    ah_Msg("Aggiornamento archivi storici inventario...")
    * --- Creazione cursore scaglioni Lifo Continuo
    if this.w_INFLGLCO = "S"
      create cursor ScLiCo (DatIni D(8,0) , QtaEsi N(12,3) , ValUni N(18,5), AggVal C(1))
      index on dtos(DatIni) tag tag1
    endif
    * --- Creazione cursore scaglioni Lifo Scatti
    if this.w_INFLGLSC = "S"
      create cursor ScLiSc (DatIni D(8,0) , DatFin D(8,0) , QtaEsi N(12,3) , ValUni N(18,5))
      index on dtos(DatIni)+dtos(DatFin) tag tag1
    endif
    * --- Creazione cursore scaglioni Fifo Continuo
    if this.w_INFLGFCO = "S"
      create cursor ScFiCo (DatIni D(8,0) , QtaEsi N(12,3) , ValUni N(18,5), AggVal C(1))
      index on dtos(DatIni) tag tag1
    endif
    * --- Try
    local bErr_03854BE8
    bErr_03854BE8=bTrsErr
    this.Try_03854BE8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Messaggio errore aggiornamento archivi
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Impossibile aggiornare gli archivi storici dell'inventario")
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_03854BE8
    * --- End
    * --- Azzeramento messaggio di elaborazione
    wait clear
  endproc
  proc Try_03854BE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Imposta il codice dell'ultimo articolo elaborato
    this.w_UltimoArt = "<BOF>"
    * --- Imposta le data fino alle quali sono stati considerati i movimenti di prezzo e costo
    this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
    this.w_DataCosUlt = cp_CharToDate("  -  -  ")
    * --- Aggiornamento valorizzazioni articoli
    this.w_RecElab = 0
    select __tmp__
    this.w_Trec = RECCOUNT()
    go top
    SCAN
    this.w_RecElab = this.w_RecElab + 1
    * --- Lettura dati record corrente
    this.w_Codice = __tmp__.cacodart
    * --- Cambio articolo
    if this.w_Codice <> this.w_UltimoArt
      * --- Messaggio a Video
      this.w_Mess = "Elabora articolo: %1%0Num. totale articoli %2%0(Elaborato il %3%)"
      ah_Msg(this.w_Mess,.T.,.F.,.F.,ALLTRIM(this.w_Codice),ALLTRIM(STR(this.w_Trec)), alltrim(str((this.w_RecElab*100)/this.w_Trec,10,0)) )
      * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Scrittura valorizzazioni articolo
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Azzeramento totalizzatori articolo
      this.w_QtaEsr = 0
      this.w_QtaEsi = 0
      this.w_PrzUlt = 0
      this.w_CosLco = 0
      this.w_QtaEsiUno = 0
      this.w_QtaVer = 0
      this.w_QtaVen = 0
      this.w_CosMpa = 0
      this.w_CosLsc = 0
      this.w_QtaVenUno = 0
      this.w_QtaAcr = 0
      this.w_QtaAcq = 0
      this.w_CosMpp = 0
      this.w_CosFco = 0
      this.w_QtaAcqUno = 0
      this.w_PrzMpa = 0
      this.w_CosUlt = 0
      this.w_PqtMpp = 0
      this.w_PrzMpp = 0
      this.w_CosSta = 0
      this.w_CqtMpp = 0
      this.w_QtaAcrWrite = 0
      this.w_QtaVerWrite = 0
      * --- Preparo i buffer relativi ad acquisti e vendite, (aggiornati, se del caso, a pag.5) da utilizzare in pag.7
      this.w_Vendite = 0
      this.w_Acquisti = 0
      this.w_CARVAL = 0
      this.w_SCAVAL = 0
      * --- Memorizza il codice dell'ultimo articolo elaborato
      this.w_UltimoArt = this.w_Codice
      this.w_CODART = __tmp__.cacodart
      this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
      this.w_DataCosUlt = cp_CharToDate("  -  -  ")
      this.w_Segnalato = .F.
      this.w_MovCosMpaWrite = 0
      this.w_MovPrzMpaWrite = 0
      this.w_bMovCosMpaWrite = .f.
      this.w_bMovPrzMpaWrite = .f.
    endif
    this.w_Maga = NVL(__tmp__.mmcodmag,space(5))
    * --- Estraggo il raggruppamento fiscale del magazzino collegato (quello del maga principale � in w_MAGRAG)
    * --- Calcolo valorizzazioni
    if NOT EMPTY(NVL(__tmp__.mmcodmat,"")) AND((__tmp__.mmcodmat $ this.w_ElenMag AND CauAggVal="SS" ) OR CauAggVal="NN" OR ((NOT __tmp__.mmcodmat $ this.w_ElenMag) AND CauAggVal="NS" ))
      * --- Aggiorna solo Esistenza
      this.w_QtaEsi = this.w_QtaEsi + (__tmp__.Acquisti - __tmp__.Vendite)
      this.w_QtaEsr = this.w_QtaEsr + (__tmp__.Acquisti - __tmp__.Vendite)
      if CauPriVal$"V|S" AND Vendite<>0
        * --- Venduto  
        this.w_QtaVen = this.w_QtaVen + Vendite
        this.w_QtaVerWrite = this.w_QtaVerWrite + Vendite
      endif
      if CauPriVal$"A|C" AND Acquisti<>0
        * --- Acquistato
        this.w_QtaAcq = this.w_QtaAcq + Acquisti
        this.w_QtaAcrWrite = this.w_QtaAcrWrite + Acquisti
      endif
      if this.w_Maga=this.w_INCODMAG
        this.w_QtaEsiUno = this.w_QtaEsiUno + (__tmp__.Acquisti - __tmp__.Vendite)
        if CauPriVal$"V|S" AND Vendite<>0
          * --- Tot scarichi
          this.w_QtaVenUno = this.w_QtaVenUno + Vendite
        endif
        if CauPriVal$"A|C" AND Acquisti<>0
          * --- Tot carichi
          this.w_QtaAcqUno = this.w_QtaAcqUno + Acquisti
        endif
      endif
    else
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_ESCONT="M" And nvl(Ordine," ")="Z" And CauAggVal="XX"
      if nvl(__tmp__.CauPriVal," ")="A"
        this.w_MovCosMpaWrite = __Tmp__.Valore
        this.w_bMovCosMpaWrite = .t.
      endif
      if nvl(__tmp__.CauPriVal," ")="V"
        this.w_MovPrzMpaWrite = __Tmp__.Valore
        this.w_bMovPrzMpaWrite = .t.
      endif
    endif
    * --- Passaggio al record successivo
    select __tmp__
    ENDSCAN
    * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Scrittura valorizzazioni ultimo articolo
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione precedenti elaborazioni
    if this.w_INELABOR = "S"
      * --- Richiesta eliminazione
      if this.w_FLMESS="S" AND NOT ah_YesNo("Confermi azzeramento precedente elaborazione?")
        i_retcode = 'stop'
        return
      endif
      * --- Eliminazione elaborazione precedente
      ah_Msg("Eliminazione elaborazione precedente...")
      * --- Try
      local bErr_040AAAF0
      bErr_040AAAF0=bTrsErr
      this.Try_040AAAF0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Impossibile eliminare gli archivi storici dell'inventario")
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_040AAAF0
      * --- End
    endif
  endproc
  proc Try_040AAAF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- LIFO Continuo
    * --- Delete from LIFOCONT
    i_nConn=i_TableProp[this.LIFOCONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIFOCONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ISNUMINV = "+cp_ToStrODBC(this.w_INNUMINV);
            +" and ISCODESE = "+cp_ToStrODBC(this.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            ISNUMINV = this.w_INNUMINV;
            and ISCODESE = this.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- LIFO a Scatti
    * --- Delete from LIFOSCAT
    i_nConn=i_TableProp[this.LIFOSCAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIFOSCAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ISNUMINV = "+cp_ToStrODBC(this.w_INNUMINV);
            +" and ISCODESE = "+cp_ToStrODBC(this.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            ISNUMINV = this.w_INNUMINV;
            and ISCODESE = this.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- FIFO Continuo
    * --- Delete from FIFOCONT
    i_nConn=i_TableProp[this.FIFOCONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FIFOCONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ISNUMINV = "+cp_ToStrODBC(this.w_INNUMINV);
            +" and ISCODESE = "+cp_ToStrODBC(this.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            ISNUMINV = this.w_INNUMINV;
            and ISCODESE = this.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Dettaglio articoli
    * --- Delete from INVEDETT
    i_nConn=i_TableProp[this.INVEDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DINUMINV = "+cp_ToStrODBC(this.w_INNUMINV);
            +" and DICODESE = "+cp_ToStrODBC(this.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            DINUMINV = this.w_INNUMINV;
            and DICODESE = this.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Valorizzazioni Periodo
    * --- La procedura richiama questa pagina piu' volte per ogni articolo e calcola tutte le valorizzazioni di periodo.
    select __tmp__
    * --- Inizializzazione dei parametri di selezione delle visual query richiamate in questa pagina
    this.w_DatReg = mmdatreg
    * --- Quantita'
    * --- Esistenza
    this.w_QtaEsi = this.w_QtaEsi + (Acquisti - Vendite)
    this.w_QtaEsr = this.w_QtaEsr + (Acquisti - Vendite)
    * --- CauPriVal = Combo valore da Aggiornare sulle causali di magazzino
    *     V:  Venduto
    *     S: Altri Scarichi
    *     A: Acquistato
    *     C: Altri Carichi
    *     
    *     CauAggVal = Check Aggiornamento Valori
    *     S: Aggiorna valori 
    *     N: Non aggiorna valori
    *     la prima lettera riguarda il magazzino principale la seconda quello collegato
    *     Es: 
    *     'SS' entrambi i magazzini aggiornano i valori
    *     'SN' il primo magazzino aggiorna, quello collegato No
    * --- Quantita' Venduta
    if CauPriVal$"V|S" AND Vendite<>0
      this.w_QtaVen = this.w_QtaVen + Vendite
      this.w_QtaVerWrite = this.w_QtaVerWrite + Vendite
      if CauAggVal="SN" OR CauAggVal="NS" OR(CauAggVal="SS" AND NOT (NVL(mmcodmat,"") $ this.w_ElenMag ))
        this.w_QtaVer = this.w_QtaVer + Vendite
        * --- Solo se Altri Scarichi/Venduto e Aggiornamento valori Attivo (pu� essere anche sulla causale collegata),  aggiorno il campo Scarichi valorizzati
        this.w_SCAVAL = this.w_SCAVAL + SCAVAL
      endif
    endif
    * --- Quantita' Acquistata
    if CauPriVal$"A|C" AND Acquisti<>0
      this.w_QtaAcq = this.w_QtaAcq + Acquisti
      this.w_QtaAcrWrite = this.w_QtaAcrWrite + Acquisti
      if CauAggVal="SN" OR CauAggVal="NS" OR(CauAggVal="SS" AND NOT (NVL(mmcodmat,"") $ this.w_ElenMag ))
        this.w_QtaAcr = this.w_QtaAcr + Acquisti
        * --- Solo se Altri Carichi/Acquistato e Aggiornamento valori Attivo (pu� essere anche sulla causale collegata), aggiorno il campo Carichi valorizzati
        this.w_CARVAL = this.w_CARVAL + CARVAL
      endif
    endif
    if this.w_Maga=this.w_INCODMAG
      * --- Salvo i dati parziali relativi al solo magazzino specificato
      * --- Esistenza
      this.w_QtaEsiUno = this.w_QtaEsiUno + (Acquisti - Vendite)
      * --- Quantita' Venduta
      if CauPriVal$"V|S" AND Vendite<>0
        this.w_QtaVenUno = this.w_QtaVenUno + Vendite
      endif
      * --- Quantita' Acquistata
      if CauPriVal$"A|C" AND Acquisti<>0
        this.w_QtaAcqUno = this.w_QtaAcqUno + Acquisti
      endif
    endif
    * --- VALORIZZAZIONI
    * --- La valorizzazione NON viene effettuata solo nel caso in cui
    * --- 1) esista una causale collegata (movimento di trasferimento);
    * --- 2) i magazzini appartengano allo stesso raggruppamento fiscale (basta testare questa che � pi� forte)
    * --- 3) Flag Aggiorna Valore associato alla causale di magazzino non attivo
    if (CauAggVal="SN" OR CauAggVal="NS") OR(CauAggVal="SS" AND NOT (NVL(mmcodmat,"") $ this.w_ElenMag ))
      * --- Prezzi
      * --- Prezzo Medio Ponderato Periodo
      * --- Sommatoria delle quantita' per i prezzi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio del periodo (se reso diminuisce il Valore)
      if CauPriVal$"V|S" AND NVL(Vendite,0)<>0
        this.w_PrzMpp = this.w_PrzMpp + (ValVend * IIF(Vendite<0, -1, 1))
        this.w_PqtMpp = this.w_PqtMpp + Vendite
      endif
      * --- Costi
      * --- Costo Medio Ponderato Periodo
      * --- Sommatoria delle quantita' per i costi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio del periodo (se reso diminuisce il Valore)
      if CauPriVal$"A|C" AND NVL(Acquisti,0)<>0
        this.w_CosMpp = this.w_CosMpp + (ValAcqu * IIF(Acquisti<0, -1, 1))
        this.w_CqtMpp = this.w_CqtMpp + Acquisti
      endif
      if nvl(Ordine," ")="Z"
        * --- Calcola Ultimo Prezzo/Ultimo Costo
        * --- Verifica se deve eseguire la valorizzazione a Ult.Prezzo o Ult.Costo o Costo Standard
        if this.w_DatReg>this.w_DataPrzUlt AND CauPriVal$"V|S" AND nvl(Valore, 0)<>0 and CauPriEsi="-"
          * --- Ultimo Prezzo
          this.w_PrzUlt = NVL(Valore, 0)
          this.w_DataPrzUlt = this.w_DatReg
        endif
        if this.w_DatReg>this.w_DataCosUlt AND CauPriVal$"A|C" AND nvl(Valore, 0)<>0 and CauPriEsi="+"
          * --- Ultimo Costo
          * --- Considera i movimenti dall'inizio del periodo
          * --- ATTENZIONE! Se e' zero prende quello dell'inventario precedente nella pagina delle valorizzazioni d'esercizio.
          this.w_CosUlt = NVL(Valore, 0)
          this.w_DataCosUlt = this.w_DatReg
        endif
      else
        * --- Bufferizzo i dati relativi ad acquisti e vendite, da utilizzare in pag.7
        this.w_Vendite = __tmp__.vendite+this.w_Vendite
        this.w_Acquisti = __tmp__.acquisti+this.w_Acquisti
      endif
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prima di Aggiornare Calcola il Costo Standard
    * --- Scrittura archivi inventario
    if this.w_UltimoArt <> "<BOF>"
      * --- Ricerca il Costo Standard nei Dati Articolo per Magazzino
      this.w_CosSta = 0
      if used("DatiArti")
        select DatiArti
        GO TOP
        if SEEK(this.w_CODART+this.w_MAGRAG)
          this.w_CosSta = NVL(PRCOSSTA,0)
        else
          * --- Se non esiste il Costo Standard riferito al Magazzino cerca quello Generico
          GO TOP
          if SEEK(this.w_CODART)
            this.w_CosSta = NVL(PRCOSSTA,0)
          endif
        endif
        select __tmp__
      endif
      * --- Arrotondamenti in base ai decimali della valuta di conto
      this.w_PrzMpa = cp_round( this.w_PrzMpa , this.w_DECESE )
      this.w_PrzMpaWrite = cp_round( this.w_PrzMpaWrite , this.w_DECESE )
      this.w_PrzMpp = cp_round( this.w_PrzMpp , this.w_DECESE )
      this.w_PrzUlt = cp_round( this.w_PrzUlt , this.w_DECESE )
      this.w_CosMpa = cp_round( this.w_CosMpa , this.w_DECESE )
      this.w_CosMpaWrite = cp_round( this.w_CosMpaWrite , this.w_DECESE )
      this.w_CosMpp = cp_round( this.w_CosMpp , this.w_DECESE )
      this.w_CosUlt = cp_round( this.w_CosUlt , this.w_DECESE )
      this.w_CosSta = cp_round( this.w_CosSta , this.w_DECESE )
      this.w_CosLco = cp_round( this.w_CosLco , this.w_DECESE )
      this.w_CosLsc = cp_round( this.w_CosLsc , this.w_DECESE )
      this.w_CosFco = cp_round( this.w_CosFco , this.w_DECESE )
      this.w_QTAESIWRITE = iif(this.w_INMAGCOL="S",this.w_QTAESI,this.w_QTAESIUNO)
      this.w_QTAACQWRITE = iif(this.w_INMAGCOL="S",this.w_QTAACQ,this.w_QTAACQUNO)
      this.w_QTAVENWRITE = iif(this.w_INMAGCOL="S",this.w_QTAVEN,this.w_QTAVENUNO)
      * --- Scrittura Dettaglio Articolo
      * --- Insert into INVEDETT
      i_nConn=i_TableProp[this.INVEDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INVEDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DINUMINV"+",DICODICE"+",DIQTAESI"+",DIQTAVEN"+",DIQTAACQ"+",DIPRZMPA"+",DIPRZMPP"+",DICOSMPA"+",DICOSMPP"+",DICOSULT"+",DICOSSTA"+",DICOSLCO"+",DICOSLSC"+",DICOSFCO"+",DIPRZULT"+",DICODESE"+",DIQTAESR"+",DIQTAVER"+",DIQTAACR"+",DIQTACVR"+",DIQTASVR"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_INNUMINV),'INVEDETT','DINUMINV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UltimoArt),'INVEDETT','DICODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESIWRITE),'INVEDETT','DIQTAESI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAVENWRITE),'INVEDETT','DIQTAVEN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAACQWRITE),'INVEDETT','DIQTAACQ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PRZMPAWRITE),'INVEDETT','DIPRZMPA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PRZMPP),'INVEDETT','DIPRZMPP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSMPAWRITE),'INVEDETT','DICOSMPA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSMPP),'INVEDETT','DICOSMPP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSULT),'INVEDETT','DICOSULT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSSTA),'INVEDETT','DICOSSTA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSLCO),'INVEDETT','DICOSLCO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSLSC),'INVEDETT','DICOSLSC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSFCO),'INVEDETT','DICOSFCO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PRZULT),'INVEDETT','DIPRZULT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_INCODESE),'INVEDETT','DICODESE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESR),'INVEDETT','DIQTAESR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAVERWRITE),'INVEDETT','DIQTAVER');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAACRWRITE),'INVEDETT','DIQTAACR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CARVAL),'INVEDETT','DIQTACVR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCAVAL),'INVEDETT','DIQTASVR');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DINUMINV',this.w_INNUMINV,'DICODICE',this.w_UltimoArt,'DIQTAESI',this.w_QTAESIWRITE,'DIQTAVEN',this.w_QTAVENWRITE,'DIQTAACQ',this.w_QTAACQWRITE,'DIPRZMPA',this.w_PRZMPAWRITE,'DIPRZMPP',this.w_PRZMPP,'DICOSMPA',this.w_COSMPAWRITE,'DICOSMPP',this.w_COSMPP,'DICOSULT',this.w_COSULT,'DICOSSTA',this.w_COSSTA,'DICOSLCO',this.w_COSLCO)
        insert into (i_cTable) (DINUMINV,DICODICE,DIQTAESI,DIQTAVEN,DIQTAACQ,DIPRZMPA,DIPRZMPP,DICOSMPA,DICOSMPP,DICOSULT,DICOSSTA,DICOSLCO,DICOSLSC,DICOSFCO,DIPRZULT,DICODESE,DIQTAESR,DIQTAVER,DIQTAACR,DIQTACVR,DIQTASVR &i_ccchkf. );
           values (;
             this.w_INNUMINV;
             ,this.w_UltimoArt;
             ,this.w_QTAESIWRITE;
             ,this.w_QTAVENWRITE;
             ,this.w_QTAACQWRITE;
             ,this.w_PRZMPAWRITE;
             ,this.w_PRZMPP;
             ,this.w_COSMPAWRITE;
             ,this.w_COSMPP;
             ,this.w_COSULT;
             ,this.w_COSSTA;
             ,this.w_COSLCO;
             ,this.w_COSLSC;
             ,this.w_COSFCO;
             ,this.w_PRZULT;
             ,this.w_INCODESE;
             ,this.w_QTAESR;
             ,this.w_QTAVERWRITE;
             ,this.w_QTAACRWRITE;
             ,this.w_CARVAL;
             ,this.w_SCAVAL;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore in inserimento dettaglio inventari'
        return
      endif
      * --- Aggiornamento scaglioni Lifo Continuo
      if this.w_INFLGLCO = "S"
        this.w_CpRowNum = 0
        select ScLiCo
        go top
        do while (.not. eof())
          this.w_IsDatMov = DatIni
          this.w_IsQtaEsi = QtaEsi
          this.w_IsValUni = IIF(AggVal="S", cp_ROUND(ValUni, this.w_DECESE), this.w_CosLco)
          if this.w_IsQtaEsi<>0
            this.w_CpRowNum = this.w_CpRowNum + 1
            * --- Insert into LIFOCONT
            i_nConn=i_TableProp[this.LIFOCONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIFOCONT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIFOCONT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ISNUMINV"+",ISCODICE"+",CPROWNUM"+",ISDATMOV"+",ISQTAESI"+",ISVALUNI"+",ISCODESE"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_INNUMINV),'LIFOCONT','ISNUMINV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_UltimoArt),'LIFOCONT','ISCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CpRowNum),'LIFOCONT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsDatMov),'LIFOCONT','ISDATMOV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsQtaEsi),'LIFOCONT','ISQTAESI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsValUni),'LIFOCONT','ISVALUNI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_INCODESE),'LIFOCONT','ISCODESE');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ISNUMINV',this.w_INNUMINV,'ISCODICE',this.w_UltimoArt,'CPROWNUM',this.w_CpRowNum,'ISDATMOV',this.w_IsDatMov,'ISQTAESI',this.w_IsQtaEsi,'ISVALUNI',this.w_IsValUni,'ISCODESE',this.w_INCODESE)
              insert into (i_cTable) (ISNUMINV,ISCODICE,CPROWNUM,ISDATMOV,ISQTAESI,ISVALUNI,ISCODESE &i_ccchkf. );
                 values (;
                   this.w_INNUMINV;
                   ,this.w_UltimoArt;
                   ,this.w_CpRowNum;
                   ,this.w_IsDatMov;
                   ,this.w_IsQtaEsi;
                   ,this.w_IsValUni;
                   ,this.w_INCODESE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in inserimento LIFO continuo'
              return
            endif
          endif
          select ScLiCo
          skip
        enddo
        select __tmp__
      endif
      * --- Aggiornamento scaglioni Lifo Scatti
      if this.w_INFLGLSC = "S"
        this.w_CpRowNum = 0
        select ScLiSc
        go top
        do while (.not. eof())
          this.w_IsDatMov = DatIni
          this.w_IsDatFin = DatFin
          this.w_IsQtaEsi = QtaEsi
          this.w_IsValUni = cp_ROUND(ValUni, this.w_DECESE)
          if this.w_IsQtaEsi<>0
            this.w_CpRowNum = this.w_CpRowNum + 1
            * --- Insert into LIFOSCAT
            i_nConn=i_TableProp[this.LIFOSCAT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIFOSCAT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIFOSCAT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ISNUMINV"+",ISCODICE"+",CPROWNUM"+",ISDATINI"+",ISQTAESI"+",ISVALUNI"+",ISDATFIN"+",ISCODESE"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_INNUMINV),'LIFOSCAT','ISNUMINV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_UltimoArt),'LIFOSCAT','ISCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CpRowNum),'LIFOSCAT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsDatMov),'LIFOSCAT','ISDATINI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsQtaEsi),'LIFOSCAT','ISQTAESI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsValUni),'LIFOSCAT','ISVALUNI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsDatFin),'LIFOSCAT','ISDATFIN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_INCODESE),'LIFOSCAT','ISCODESE');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ISNUMINV',this.w_INNUMINV,'ISCODICE',this.w_UltimoArt,'CPROWNUM',this.w_CpRowNum,'ISDATINI',this.w_IsDatMov,'ISQTAESI',this.w_IsQtaEsi,'ISVALUNI',this.w_IsValUni,'ISDATFIN',this.w_IsDatFin,'ISCODESE',this.w_INCODESE)
              insert into (i_cTable) (ISNUMINV,ISCODICE,CPROWNUM,ISDATINI,ISQTAESI,ISVALUNI,ISDATFIN,ISCODESE &i_ccchkf. );
                 values (;
                   this.w_INNUMINV;
                   ,this.w_UltimoArt;
                   ,this.w_CpRowNum;
                   ,this.w_IsDatMov;
                   ,this.w_IsQtaEsi;
                   ,this.w_IsValUni;
                   ,this.w_IsDatFin;
                   ,this.w_INCODESE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in inserimento LIFO scatti'
              return
            endif
          endif
          select ScLiSc
          skip
        enddo
        select __tmp__
      endif
      * --- Aggiornamento scaglioni Fifo Continuo
      if this.w_INFLGFCO = "S"
        this.w_CpRowNum = 0
        select ScFiCo
        go top
        do while (.not. eof())
          this.w_IsDatMov = DatIni
          this.w_IsQtaEsi = QtaEsi
          this.w_IsValUni = IIF(AggVal="S", cp_ROUND(ValUni, this.w_DECESE), this.w_CosFco)
          if this.w_IsQtaEsi<>0
            this.w_CpRowNum = this.w_CpRowNum + 1
            * --- Insert into FIFOCONT
            i_nConn=i_TableProp[this.FIFOCONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.FIFOCONT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FIFOCONT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ISNUMINV"+",ISCODICE"+",CPROWNUM"+",ISDATMOV"+",ISQTAESI"+",ISVALUNI"+",ISCODESE"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_INNUMINV),'FIFOCONT','ISNUMINV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_UltimoArt),'FIFOCONT','ISCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CpRowNum),'FIFOCONT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsDatMov),'FIFOCONT','ISDATMOV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsQtaEsi),'FIFOCONT','ISQTAESI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsValUni),'FIFOCONT','ISVALUNI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_INCODESE),'FIFOCONT','ISCODESE');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ISNUMINV',this.w_INNUMINV,'ISCODICE',this.w_UltimoArt,'CPROWNUM',this.w_CpRowNum,'ISDATMOV',this.w_IsDatMov,'ISQTAESI',this.w_IsQtaEsi,'ISVALUNI',this.w_IsValUni,'ISCODESE',this.w_INCODESE)
              insert into (i_cTable) (ISNUMINV,ISCODICE,CPROWNUM,ISDATMOV,ISQTAESI,ISVALUNI,ISCODESE &i_ccchkf. );
                 values (;
                   this.w_INNUMINV;
                   ,this.w_UltimoArt;
                   ,this.w_CpRowNum;
                   ,this.w_IsDatMov;
                   ,this.w_IsQtaEsi;
                   ,this.w_IsValUni;
                   ,this.w_INCODESE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in inserimento FIFO continuo'
              return
            endif
          endif
          select ScFiCo
          skip
        enddo
        select __tmp__
      endif
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Valorizzazioni Esercizio
    * --- La procedura richiama questa pagina una sola volte per ogni articolo e calcola tutte le valorizzazioni d'esercizio.
    * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
    * --- Variabili relative all'inventario precedente
    this.w_PreQtaEsi = 0
    this.w_PreQtaVen = 0
    this.w_PreQtaAcq = 0
    this.w_PreQtaEsr = 0
    this.w_PreQtaVer = 0
    this.w_PreQtaAcr = 0
    this.w_PrePrzMpa = 0
    this.w_PreCosMpa = 0
    this.w_PreCosUlt = 0
    this.w_PrePreUlt = 0
    this.w_PrzMpaWrite = 0
    this.w_CosMpaWrite = 0
    this.w_PreCarVal = 0
    this.w_PreScaVal = 0
    if this.w_UltimoArt <> "<BOF>"
      * --- Legge le valorizzazioni dell'inventario precedente
      this.Pag11()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Quantita'
      * --- Esistenza
      * --- L'esistenza e' data dalla somma della quantita' presente nel periodo precedente con la differenza fra
      * --- gli acquisti e le vendite del periodo (calcolata nella pagina delle valorizzazioni di periodo).
      this.w_QtaEsi = this.w_PreQtaEsi + this.w_QtaEsi
      this.w_QtaEsr = this.w_PreQtaEsr+this.w_QtaEsr
      if this.w_INMAGCOL="N"
        this.w_QtaEsiUno = this.w_PreQtaEsi + this.w_QtaEsiUno
      endif
      * --- VALORIZZAZIONI
      * --- Cerco eventuali variazioni a valore
      this.w_VarValoAcq = 0
      this.w_VarValoVen = 0
      if used("Var_valo") And RecCount("Var_valo")>0
         
 Select Var_valo 
 Go Top
        if Seek(this.w_CODART)
          this.w_VarValoAcq = NVL(Var_Valo.Acq,0)
          this.w_VarValoVen = NVL(Var_valo.Ven,0)
        endif
      endif
      select __tmp__
      * --- La valorizzazione NON viene effettuata solo nel caso in cui
      * --- 1) esista una causale collegata (movimento di trasferimento);
      * --- 2) i magazzini appartengano allo stesso raggruppamento fiscale (basta testare questa che � pi� forte)
      * --- (in quel caso non calcolo neanche gli scaglioni)
      * --- 3) Flag Aggiorna Valore associato alla causale di magazzino non attivo
      * --- Prezzi
      * --- Prezzo Medio Ponderato Periodo
      * --- Adesso che sono stati letti tutti i prezzi e le quantit� dell'articolo � possibile calcolare la media del periodo
      * --- Vedi commento per i costi...
      this.w_Old_PrzMpp = iif( this.w_PqtMpp=0 , 0 ,this.w_PrzMpp)
      this.w_PrzMpp = iif( empty( this.w_PqtMpp ) , 0 , ( this.w_PrzMpp + this.w_VarValoVen )/ this.w_PqtMpp )
      * --- Prezzo Medio Ponderato Esercizio
      * --- Sommatoria delle quantita' per i prezzi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio dell'esercizio
      * --- Se non e' stato specificato l'inventario precedente corrisponde al valore di periodo.
      if this.w_QtaVer+this.w_PreQtaVer<>0
        * --- w_PqtMpp e w_Qtaver contengono il solito valore...
        this.w_PrzMpa = (iif( this.w_Old_PrzMpp=0 , this.w_VarValoVen , (this.w_PrzMpp*this.w_QtaVer)) + (this.w_PrePrzMpa*this.w_PreQtaVer)) / (this.w_QtaVer+this.w_PreQtaVer)
      else
        * --- Se la somma delle quantit� � zero due casi
        *     a) entrambe le qt� sono a zero come prezzo medio utilizzo quello
        *     dell'esercizio precedente
        *     b) le quantit� si annullano, prezzo medio a zero...
        if this.w_PreQtaVer=0
          this.w_PrzMpa = this.w_PrePrzMpa
        else
          * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
          this.w_PrzMpa = 0
        endif
      endif
      * --- Se attivo Flag Eserc Continuo devo sempre considerare l'esercizio precedente
      do case
        case this.w_ESCONT="S"
          * --- Vedi calcolo sopra...
          if this.w_QtaVer+this.w_PreQtaEsr<>0
            this.w_PrzMpaWrite = ((this.w_PrzMpp*this.w_QtaVer) + (this.w_PrePrzMpa*this.w_PreQtaEsr)) / (this.w_QtaVer+this.w_PreQtaEsr)
          else
            if this.w_PreQtaEsr=0
              this.w_PrzMpaWrite = this.w_PrePrzMpa
            else
              * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
              this.w_PrzMpaWrite = 0
            endif
          endif
        case this.w_ESCONT$"NM"
          this.w_PrzMpaWrite = this.w_PrzMpa
      endcase
      * --- Costi
      * --- Costo Medio Ponderato Periodo
      * --- Adesso che sono stati letti tutti i prezzi e le quantit� dell'articolo � possibile calcolare la media del periodo
      * --- Devo sapere se nel periodo ho avuto movimenti "validi" o meno.
      *     Se li ho avuti allora l'eventuale presenza di variazioni di valore
      *     non vanno ri applicate al dato proveniente dall'esercizio precedente,
      *     altrimenti ( se non ho movimenti validi ) devo applicarle (quindi nel periodo
      *     ho solo la variazione a valore)
      *     - CASO LIMITE NON GESTITO SOLA VARIAZIONE A VALORE 
      *     SENZA SALDI ARTICOLI, L'ARTICOLO NON SAREBBE PRESENTE 
      *     NELL'ELABORAZIONE INVENTARIO E QUINDI NON ENTREREBBE
      *     NELLA SCAN IN CUI SIAMO
      this.w_Old_CosMpp = iif( this.w_CqtMpp=0 , 0 ,this.w_CosMpp )
      this.w_CosMpp = iif( this.w_CqtMpp=0 , 0 , ( this.w_CosMpp + this.w_VarValoAcq ) / this.w_CqtMpp )
      * --- Costo Medio Ponderato Esercizio
      * --- Sommatoria delle quantita' per i costi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio dell'esercizio
      * --- Se non e' stato specificato l'inventario precedente o non � dello stesso Esercizio corrisponde al valore di periodo.
      if this.w_QtaAcr+this.w_PreCarVal<>0
        * --- Se il valore del periodo � zero devo comunque considerare le variazioni a valore del periodo
        this.w_CosMpa = (iif( this.w_Old_CosMpp=0 , this.w_VarValoAcq , (this.w_CosMpp*this.w_QtaAcr)) + (this.w_PreCosMpa*this.w_PreCarVal)) / (this.w_QtaAcr+this.w_PreCarVal)
      else
        if this.w_PreQtaAcr=0 or (this.w_ESCONT="N" and this.w_PreCosMpa<>0)
          * --- Nel caso in cui nell'inventario di riferimento (stesso esercizio dell'inventario da elaborare)
          *     il tot. carichi sia valorizzato ma � una rettifica d'acquisto (mvprezzo a 0) 
          *     l'inventario da elaborare prende sempre il costo Medio ponderato dell'esercizio precedente
          this.w_CosMpa = this.w_PreCosMpa
        else
          * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
          this.w_CosMpa = 0
        endif
      endif
      * --- Se attivo Flag Eserc Continuo devo sempre considerare l'esercizio precedente
      do case
        case this.w_ESCONT="S"
          if this.w_QtaAcr+ this.w_PreQtaEsr<>0
            this.w_CosMpaWrite = ((this.w_CosMpp*this.w_QtaAcr) + (this.w_PreCosMpa*this.w_PreQtaEsr)) / (this.w_QtaAcr+this.w_PreQtaEsr)
          else
            if this.w_PreQtaEsr=0
              this.w_CosMpaWrite = this.w_PreCosMpa
            else
              * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
              this.w_CosMpaWrite = 0
            endif
          endif
        case this.w_ESCONT="N"
          this.w_CosMpaWrite = this.w_CosMpa
        case this.w_ESCONT="M"
          * --- Se ho trovato tra i record del temporaneo una entrata con il costo
          *     calcolato sui movimenti lo utilizzo, altrimenti (articolo non movimentato
          *     nel periodo) riporto il prezzo presente nell'eventuale inventario 
          *     precedente, altrimenti 0...
          if this.w_bMovCosMpaWrite
            this.w_CosMpaWrite = this.w_MovCosMpaWrite
          else
            this.w_CosMpaWrite = this.w_PreCosMpa
          endif
      endcase
      * --- Ultimo Costo
      * --- Se e' zero l'ultimo costo del periodo prende quello dell'inventario precedente
      if empty( this.w_CosUlt ) .and. (.not. empty(this.w_PreCosUlt))
        this.w_CosUlt = this.w_PreCosUlt
      endif
      * --- Ultimo Prezzo
      * --- Se e' zero l'ultimo prezzo del periodo prende quello dell'inventario precedente
      if empty( this.w_PrzUlt ) .and. (.not. empty(this.w_PrePreUlt))
        this.w_PrzUlt = this.w_PrePreUlt
      endif
      this.w_QtaAcq = this.w_QtaAcq + this.w_PreQtaAcq
      this.w_QtaVen = this.w_QtaVen + this.w_PreQtaVen
      this.w_QtaAcr = this.w_QtaAcr + this.w_PreQtaAcr
      this.w_QtaVer = this.w_QtaVer + this.w_PreQtaVer
      this.w_QtaAcrWrite = this.w_QtaAcrWrite + this.w_PreQtaAcr
      this.w_QtaVerWrite = this.w_QtaVerWrite + this.w_PreQtaVer
      this.w_CARVAL = this.w_CARVAL + this.w_PreCarVal
      this.w_SCAVAL = this.w_SCAVAL + this.w_PreScaVal
      if this.w_INMAGCOL="N"
        this.w_QtaAcqUno = this.w_QtaAcqUno + iif(this.w_INCODESE<>this.w_INESEPRE,0,this.w_PreQtaAcq)
        this.w_QtaVenUno = this.w_QtaVenUno + iif(this.w_INCODESE<>this.w_INESEPRE,0,this.w_PreQtaVen)
      endif
      * --- LIFO e FIFO
      * --- Se l'esistenza e' invariata mantiene la valorizzazione dell'inventario precedente.
      * --- Se l'esistenza e' diminuita toglie la quantita' dagli scaglioni memorizzati in precedenza.
      * --- Se l'esistenza e' aumentata aggiunge uno scaglione per il nuovo periodo.
      if this.w_INFLGLSC = "S" .or. this.w_INFLGLCO = "S" .or. this.w_INFLGFCO = "S"
        * --- Le valorizzazioni vengono calcolate soltanto se l'esistenza e' cambiata.
        * --- In caso contrario la procedura si limita a copiare gli scaglioni del periodo precedente se e'
        * --- stato specificato.
        * --- Azzero sempre i cursori degli scaglioni Prima di controllare l'esistenza
        * --- (altrimenti non vengono riportati quelli precedenti)
        if this.w_INFLGLCO = "S"
          select ScLiCo
          zap
        endif
        if this.w_INFLGLSC = "S"
          select ScLiSc
          zap
        endif
        if this.w_INFLGFCO = "S"
          select ScFiCo
          zap
        endif
        if (this.w_PreQtaEsr<>this.w_QtaEsr) .or. (.not. empty( this.w_INNUMPRE ))
          * --- Lettura movimenti del periodo
          * --- Crea un nuovo cursore perch� quello dell'elaborazione principale non contiene il dettaglio dei
          * --- movimenti ma i valori raggruppati.
          SELECT mmdatreg, mmserial, cmseqcal, tipo, mmcodmag, qtaprin, valtot, cauprival, cauaggval, mmcaucol, mmcodmat ;
          FROM FifoLifo ;
          WHERE MMCODART = this.w_CODART ;
          INTO CURSOR valori ORDER BY mmdatreg,tipo,cmseqcal, mmserial
          select valori
          * --- Lifo Continuo
          if this.w_INFLGLCO = "S"
            this.w_TipValor = "LIFOCONTINUO"
            * --- Selezione cursore scaglioni
            select ScLiCo
            * --- Lettura scaglioni periodo precedente
            this.Pag10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Ai Fini delle Valorizzazioni devono essere considerati i soli Movimenti di Acquisto e Vendita
            if (this.w_PreQtaEsr<>this.w_QtaEsr)
              select valori
              go top
              SCAN
              * --- Magazzino principale
              if mmcodmag $ this.w_ElenMag AND CauPriVal $ "AVCS"
                this.w_F2CASC = "N"
                if NOT EMPTY(NVL(MMCAUCOL,"")) AND NVL(mmcodmat,"") $ this.w_ElenMag 
                  * --- Se presente magazzino collegato verifica che la causale non movimenti Carichi/Scarichi
                  this.w_APPO = MMCAUCOL
                  * --- Read from CAM_AGAZ
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "CMFLAVAL"+;
                      " from "+i_cTable+" CAM_AGAZ where ";
                          +"CMCODICE = "+cp_ToStrODBC(this.w_APPO);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      CMFLAVAL;
                      from (i_cTable) where;
                          CMCODICE = this.w_APPO;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_F2CASC = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_F2CASC = IIF(this.w_F2CASC $ "AVCS", "S", "N")
                endif
                if this.w_F2CASC="N"
                  * --- Considero Acquisti, Vendite, Carico, Scarico
                  * --- Nell'elaborazione controllo il campo Tipo='A' se Cauprival$'AC' o Tipo='V' se Cauprival$'VS'
                  this.Pag9()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
              select valori
              ENDSCAN
            endif
            * --- Calcolo valore LIFO Continuo. Due casi
            *     a) Ho almeno uno scaglione che valorizza (almeno un record con AggVal='S')
            *     b) Nessun Scaglione che valorizza
            *     Nel caso a) per valorizzare il costo LIFO Standard utilizzo gli scaglioni che
            *     valorizzano, nel caso b) utilizzo il costo medio ponderato esercizio (w_CosMpa)
            select sum(qtaesi) as qtaesi, sum(valuni*qtaesi) as valuni, Count(*) As Rec_No from ScLiCo where AggVal="S" into cursor SommaLF
            if Nvl( SommaLf.Rec_No , 0 )>0
              this.w_CosLco = iif( sommalf.qtaesi = 0 , 0 , sommalf.valuni / sommalf.qtaesi )
            else
              this.w_CosLco = cp_Round( this.w_CosMpa , this.w_DECESE ) 
            endif
          endif
          * --- Lifo Scatti
          if this.w_INFLGLSC = "S"
            this.w_TipValor = "LIFOSCATTI"
            * --- Creazione cursore scaglioni
            select ScLiSc
            * --- Lettura scaglioni periodo precedente
            this.Pag10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Aggiornamento scaglioni
            do case
              case this.w_QtaEsr > this.w_PreQtaEsr
                * --- L'esistenza e' aumentata.
                * --- Aggiunge uno scaglione per la quantita' aumentata valorizzato a costo medio ponderato dell'esercizio.
                select ScLiSc
                append blank
                replace DatIni with iif(empty(this.w_PRDATINV),this.w_ESINIESE,this.w_PRDATINV)
                replace DatFin with this.w_INDATINV
                replace QtaEsi with ( this.w_QtaEsi - this.w_PreQtaEsi )
                replace ValUni with cp_round( this.w_CosMpa , this.w_DECESE )
              case this.w_QtaEsr < this.w_PreQtaEsr
                * --- L'esistenza e' diminuita
                * --- Toglie dagli scaglione esistenti la quantita' diminuita
                this.w_TipMagaz = ""
                this.Pag9()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
            endcase
            * --- Calcolo valore LIFO Scatti
            select sum(qtaesi) as qtaesi, sum(valuni*qtaesi) as valuni from ScLiSc into cursor SommaLF
            this.w_CosLsc = iif( sommalf.qtaesi = 0 , 0 , sommalf.valuni / sommalf.qtaesi )
          endif
          * --- Fifo Continuo
          if this.w_INFLGFCO = "S"
            this.w_TipValor = "FIFOCONTINUO"
            * --- Creazione cursore scaglioni
            select ScFiCo
            * --- Lettura scaglioni periodo precedente
            this.Pag10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Ai Fini delle Valorizzazioni devono essere considerati i soli Movimenti di Acquisto e Vendita
            select valori
            go top
            if this.w_PreQtaEsr<>this.w_QtaEsr or reccount("valori")>0
              SCAN
              * --- Magazzino principale
              if mmcodmag $ this.w_ElenMag AND CauPriVal $ "AVCS"
                this.w_F2CASC = "N"
                if NOT EMPTY(NVL(MMCAUCOL,"")) AND NVL(mmcodmat,"") $ this.w_ElenMag 
                  * --- Se presente magazzino collegato e uguale a Mag.Ragguppamento verifica che la causale non movimenti Carichi/Scarichi
                  this.w_APPO = MMCAUCOL
                  * --- Read from CAM_AGAZ
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "CMFLAVAL"+;
                      " from "+i_cTable+" CAM_AGAZ where ";
                          +"CMCODICE = "+cp_ToStrODBC(this.w_APPO);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      CMFLAVAL;
                      from (i_cTable) where;
                          CMCODICE = this.w_APPO;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_F2CASC = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_F2CASC = IIF(this.w_F2CASC $ "AVCS", "S", "N")
                endif
                if this.w_F2CASC="N"
                  * --- Considero Acuisti, Vendite, Carico, Scarico
                  * --- Nell'elaborazione controllo il campo Tipo='A' se Cauprival$'AC' o Tipo='V' se Cauprival$'VS'
                  this.Pag9()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
              select valori
              ENDSCAN
            endif
            * --- Calcolo valore FIFO Continuo. Due casi
            *     a) Ho almeno uno scaglione che valorizza (almeno un record con AggVal='S')
            *     b) Nessun Scaglione che valorizza
            *     Nel caso a) per valorizzare il costo FIFO Standard utilizzo gli scaglioni che
            *     valorizzano, nel caso b) utilizzo il costo medio ponderato esercizio (w_CosMpa)
            select sum(qtaesi) as qtaesi, sum(valuni*qtaesi) as valuni, Count(*) As Rec_No from ScFiCo where AggVal="S" into cursor SommaLF
            if Nvl( SommaLf.Rec_No , 0 )>0
              this.w_CosFco = iif( sommalf.qtaesi = 0 , 0 , sommalf.valuni / sommalf.qtaesi )
            else
              this.w_CosFco = cp_Round( this.w_CosMpa , this.w_DECESE ) 
            endif
          endif
        endif
        select __tmp__
      endif
    endif
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica campi obbligatori e compilazione elenco magazzini
    * --- Verifica campi obbligatori
    if empty(this.w_INDATINV) .or. empty(this.w_INCODESE) .or. empty(this.w_INDESINV) .or. empty(this.w_INCODMAG)
      ah_ErrorMsg("Specificare il valore dei campi obbligatori")
      i_retcode = 'stop'
      return
    endif
    * --- Lettura elenco magazzini interessati
    this.w_ElenMag = ""
    vq_exec("query\GSMAXBIN",this,"magazzini")
    select magazzini
    go top
    SCAN
    if .not. empty(mgcodmag)
      this.w_ElenMag = this.w_ElenMag + "|" + mgcodmag
    endif
    ENDSCAN
    use
    if empty(this.w_ElenMag)
      ah_ErrorMsg("Il magazzino selezionato non � utilizzabile")
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna cursori scaglioni
    * --- Seleziona il cursore
    do case
      case this.w_TipValor = "LIFOCONTINUO"
        select ScLiCo
      case this.w_TipValor = "FIFOCONTINUO"
        select ScFiCo
      case this.w_TipValor = "LIFOSCATTI"
        select ScLiSc
    endcase
    * --- Aggiorna gli scaglioni
    * --- Il campo valori.Tipo assume il valore "A" sia in caso di acquisto che in caso di carico
    * --- Il campo valori.Tipo assume il valore "V" sia in caso di vendita che in caso di scarico
    this.w_ValQta = valori.qtaprin
    this.w_ValTip = valori.tipo
    this.w_Reso = .F.
    if valori.qtaprin<0
      this.w_Reso = .T.
      * --- Se Reso Inverte il Tipo
      this.w_ValQta = ABS(this.w_ValQta)
      this.w_ValTip = IIF(this.w_ValTip="A", "V", "A")
    endif
    if this.w_ValTip="A" AND this.w_TipValor <> "LIFOSCATTI"
      this.w_ValUni = IIF(this.w_ValQta<>0, valori.valtot/this.w_ValQta, 0)
      * --- Acquisto o altro carico
      * --- Aggiunge uno scaglione con la data della registrazione.
      if this.w_Reso=.T.
        * --- Se Reso Il valore non deve essere quello riportato ma ricalcolato dai carichi
        this.w_QtaScagl = this.w_ValQta
        if this.w_TipValor = "LIFOCONTINUO"
          * --- LIFO Continuo  : Legge Ultimo Scaglione Caricato
          go bottom
        else
          * --- FIFO  : Legge Primo Scaglione Caricato
          go top
        endif
        this.w_ValUni = ValUni
      endif
      append blank
      replace DatIni with valori.mmdatreg
      replace QtaEsi with this.w_ValQta
      replace ValUni with this.w_ValUni
      replace AggVal with NVL(valori.cauaggval, " ")
    else
      * --- Vendita, altro scarico o LIFO a Scatti
      * --- Toglie la quantita' dagli scaglioni caricati con gli acquisti e gli altri carichi
      * --- Se la valorizzazione corrente e' LIFO a Scatti non considera i dati presenti sul temporaneo dei movimenti ma
      * --- rimuove dagli scaglioni la quantita' diminuita rispetto al periodo precedente.
      if this.w_TipValor = "LIFOSCATTI"
        this.w_QtaScagl = this.w_PreQtaEsr - this.w_QtaEsr
      else
        this.w_QtaScagl = this.w_ValQta
      endif
      do case
        case this.w_TipValor = "LIFOCONTINUO" .or. this.w_TipValor = "LIFOSCATTI"
          * --- LIFO Continuo ed a Scatti : Toglie la quantita' dagli scaglioni finali
          go bottom
          do while (.not. bof()) .and. this.w_QtaScagl >= QtaEsi
            this.w_QtaScagl = this.w_QtaScagl - QtaEsi
            delete
            go bottom
          enddo
        case this.w_TipValor = "FIFOCONTINUO"
          * --- FIFO Continuo : Toglie la quantita' dagli scaglioni iniziali
          go top
          do while (.not. eof()) .and. this.w_QtaScagl >= QtaEsi
            this.w_QtaScagl = this.w_QtaScagl - QtaEsi
            delete
            go top
          enddo
      endcase
      * --- Toglie la quantita' residua
      if ( bof() .or. eof() ) And this.w_QtaScagl > 0
        * --- Interrompe l'elaborazione se gli scaglioni sono incongruenti (vendite maggiori degli acquisti)
        if .not. this.w_Segnalato
          * --- Leggo la descrizione dell'articolo
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARDESART"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARDESART;
              from (i_cTable) where;
                  ARCODART = this.w_CODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- oggetto per mess. incrementali
          this.w_oMESS=createobject("ah_message")
          do case
            case this.w_TipValor = "LIFOSCATTI"
              this.w_oPART = this.w_oMESS.addmsgpartNL("L'articolo %1, %2 presenta dati LIFO a scatti incongruenti")
            case this.w_TipValor = "LIFOCONTINUO" 
              this.w_oPART = this.w_oMESS.addmsgpartNL("L'articolo %1, %2 presenta dati LIFO incongruenti")
            case this.w_TipValor = "FIFOCONTINUO"
              this.w_oPART = this.w_oMESS.addmsgpartNL("L'articolo %1, %2 presenta dati FIFO incongruenti")
            otherwise
              this.w_oPART = this.w_oMESS.addmsgpartNL("L'articolo %1, %2 presenta dati incongruenti")
          endcase
          this.w_oPART.addParam(trim(this.w_CODART))     
          this.w_oPART.addParam(trim(this.w_DESART))     
          this.w_oPART = this.w_oMESS.addmsgpartNL("Documento / movimento del %1 con quantit� pari a %2")
          this.w_oPART.addParam(DtoC(valori.mmdatreg))     
          this.w_oPART.addParam(Alltrim(Tran(valori.qtaprin,v_PQ(14))))     
          this.w_oPART = this.w_oMESS.addmsgpartNL("Magazzino %1")
          this.w_oPART.addParam(Valori.mmcodmag)     
          if Not Empty(Nvl(valori.mmcodmat,""))
            this.w_oPART = this.w_oMESS.addmsgpartNL("Magazzino collegato: %1")
            this.w_oPART.addParam(valori.mmcodmat)     
          endif
          if this.w_FLREPO="S" 
            if this.w_FLMESS="S"
              * --- Mantiene l'oggetto pieno per la ah_yesno (parametro a .F.)
              this.w_Messaggio = this.w_oMESS.ComposeMessage(.F.)
            else
              * --- altrimenti dopo la composemessage viene svuotato (di default)
              this.w_Messaggio = this.w_oMESS.ComposeMessage()
            endif
          endif
          if this.w_FLMESS="S"
            this.w_oMESS.AddMsgPartNL("%0Si desidera proseguire?")     
            if NOT this.w_oMESS.ah_YesNo()
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
          endif
          if this.w_FLREPO="S"
            * --- Se cursore messaggi non ancora creato lo creo..
            if Not Used( "MessErr" )
              CREATE CURSOR MessErr (MSG C(220))
            endif
            INSERT INTO MessErr (MSG) VALUES ( this.w_Messaggio )
          endif
          this.w_Segnalato = .T.
        endif
      else
        * --- Toglie la quantita' restante
        if this.w_QtaScagl > 0
          replace QtaEsi with QtaEsi - this.w_QtaScagl
        endif
      endif
    endif
    * --- Si riposiziona sul cursore dei movimenti
    select valori
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia scaglioni periodo precedente
    if (.not. empty( this.w_INNUMPRE ))
      * --- Creazione cursore
      do case
        case this.w_TipValor = "LIFOCONTINUO"
          vq_exec("query\GSMARBIN",this,"ScPrec")
        case this.w_TipValor = "FIFOCONTINUO"
          vq_exec("query\GSMATBIN",this,"ScPrec")
        case this.w_TipValor = "LIFOSCATTI"
          vq_exec("query\GSMASBIN",this,"ScPrec")
      endcase
      * --- Trasferimento records
      select ScPrec
      go top
      SCAN
      this.w_Appo1 = ScPrec.ValUni
      if this.w_CAOPRE<>this.w_CAOESE
        * --- Se L'inventario precedente e' di una altra valuta converte i valori alla valuta del nuovo
        this.w_Appo1 = VAL2MON(this.w_Appo1,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
      endif
      * --- Selezione cursore da aggiornare
      do case
        case this.w_TipValor = "LIFOCONTINUO"
          select ScLiCo
        case this.w_TipValor = "FIFOCONTINUO"
          select ScFiCo
        case this.w_TipValor = "LIFOSCATTI"
          select ScLiSc
      endcase
      * --- Scrittura record
      append blank
      replace DatIni with ScPrec.DatIni
      replace QtaEsi with ScPrec.QtaEsi
      replace ValUni with this.w_Appo1
      if this.w_TipValor = "LIFOSCATTI"
        replace DatFin with ScPrec.DatFin
      else
        * --- Se LIFO/FIFO Continuo Aggiorna Sempre Valorizzazione
        replace AggVal with "S"
      endif
      * --- Passaggio al record successivo
      selec ScPrec
      ENDSCAN
    endif
    * --- Si riposiziona sul cursore dei movimenti
    select valori
  endproc


  procedure Pag11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura dati inventario precedente
    if NOT EMPTY(this.w_INNUMPRE)
      * --- Read from INVEDETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INVEDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DIQTAESI,DIQTAESR,DIPRZMPA,DICOSMPA,DICOSULT,DIPRZULT,DIQTAVEN,DIQTAACQ,DIQTAVER,DIQTAACR,DIQTACVR,DIQTASVR"+;
          " from "+i_cTable+" INVEDETT where ";
              +"DINUMINV = "+cp_ToStrODBC(this.w_INNUMPRE);
              +" and DICODESE = "+cp_ToStrODBC(this.w_INESEPRE );
              +" and DICODICE = "+cp_ToStrODBC(this.w_CODART );
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DIQTAESI,DIQTAESR,DIPRZMPA,DICOSMPA,DICOSULT,DIPRZULT,DIQTAVEN,DIQTAACQ,DIQTAVER,DIQTAACR,DIQTACVR,DIQTASVR;
          from (i_cTable) where;
              DINUMINV = this.w_INNUMPRE;
              and DICODESE = this.w_INESEPRE ;
              and DICODICE = this.w_CODART ;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PreQtaEsi = NVL(cp_ToDate(_read_.DIQTAESI),cp_NullValue(_read_.DIQTAESI))
        this.w_PreQtaEsr = NVL(cp_ToDate(_read_.DIQTAESR),cp_NullValue(_read_.DIQTAESR))
        this.w_PrePrzMpa = NVL(cp_ToDate(_read_.DIPRZMPA),cp_NullValue(_read_.DIPRZMPA))
        this.w_PreCosMpa = NVL(cp_ToDate(_read_.DICOSMPA),cp_NullValue(_read_.DICOSMPA))
        this.w_PreCosUlt = NVL(cp_ToDate(_read_.DICOSULT),cp_NullValue(_read_.DICOSULT))
        this.w_PrePreUlt = NVL(cp_ToDate(_read_.DIPRZULT),cp_NullValue(_read_.DIPRZULT))
        this.w_PreQtaVen = NVL(cp_ToDate(_read_.DIQTAVEN),cp_NullValue(_read_.DIQTAVEN))
        this.w_PreQtaAcq = NVL(cp_ToDate(_read_.DIQTAACQ),cp_NullValue(_read_.DIQTAACQ))
        this.w_PreQtaVer = NVL(cp_ToDate(_read_.DIQTAVER),cp_NullValue(_read_.DIQTAVER))
        this.w_PreQtaAcr = NVL(cp_ToDate(_read_.DIQTAACR),cp_NullValue(_read_.DIQTAACR))
        this.w_PreCarVal = NVL(cp_ToDate(_read_.DIQTACVR),cp_NullValue(_read_.DIQTACVR))
        this.w_PreScaVal = NVL(cp_ToDate(_read_.DIQTASVR),cp_NullValue(_read_.DIQTASVR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_INCODESE<>this.w_INESEPRE
        * --- Se esercizi diversi i totalizzatori per esercizio li azzero...
        this.w_PreQtaVen = 0
        this.w_PreQtaAcq = 0
        this.w_PreQtaVer = 0
        this.w_PreQtaAcr = 0
        this.w_PreCarVal = 0
        this.w_PreScaVal = 0
      endif
      if this.w_CAOPRE<>this.w_CAOESE
        * --- Se L'inventario precedente e' di una altra valuta converte i valori alla valuta del nuovo
        this.w_PrePrzMpa = VAL2MON(this.w_PrePrzMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
        this.w_PreCosMpa = VAL2MON(this.w_PreCosMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
        this.w_PreCosUlt = VAL2MON(this.w_PreCosUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
        this.w_PrePreUlt = VAL2MON(this.w_PrePreUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
      endif
    endif
  endproc


  proc Init(oParentObject,w_Evento)
    this.w_Evento=w_Evento
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='INVEDETT'
    this.cWorkTables[2]='LIFOSCAT'
    this.cWorkTables[3]='LIFOCONT'
    this.cWorkTables[4]='FIFOCONT'
    this.cWorkTables[5]='INVENTAR'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='CAM_AGAZ'
    this.cWorkTables[10]='ART_ICOL'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_QUERY_GSMAIBIN')
      use in _Curs_QUERY_GSMAIBIN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Evento"
endproc
