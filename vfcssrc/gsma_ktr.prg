* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_ktr                                                        *
*              Tracciabilità lotti                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-30                                                      *
* Last revis.: 2012-10-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_ktr",oParentObject))

* --- Class definition
define class tgsma_ktr as StdForm
  Top    = 6
  Left   = 19

  * --- Standard Properties
  Width  = 784
  Height = 571
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-09"
  HelpContextID=82475881
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  _IDX = 0
  LOTTIART_IDX = 0
  ART_ICOL_IDX = 0
  KEY_ARTI_IDX = 0
  cPrg = "gsma_ktr"
  cComment = "Tracciabilità lotti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODART = space(20)
  o_CODART = space(20)
  w_LOTTOE = space(20)
  o_LOTTOE = space(20)
  w_LOTTOR = space(20)
  o_LOTTOR = space(20)
  w_TIPRIC = space(1)
  w_LOTTO = space(20)
  o_LOTTO = space(20)
  w_KEYART = space(20)
  w_CODLOTR = space(20)
  o_CODLOTR = space(20)
  w_CODLOTE = space(20)
  o_CODLOTE = space(20)
  w_CODLOT = space(10)
  w_CURSORNA = space(10)
  w_FL__ELAB = .F.
  w_DESART = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATCRE = ctod('  /  /  ')
  w_KEYLOT = space(61)
  w_SERIAL = space(10)
  w_NUMRIF = 0
  w_SERDIC = space(20)
  w_KEYSAL = space(41)
  w_FLSTAT = space(1)
  w_CODRIC = space(41)
  w_CADESAR = space(40)
  w_QTAMOV = 0
  w_UNIMIS = space(3)
  w_MAGAZ = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_CODUBI = space(20)
  w_DATREG = ctod('  /  /  ')
  w_TREEVIEW = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_ktrPag1","gsma_ktr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODART_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TREEVIEW = this.oPgFrm.Pages(1).oPag.TREEVIEW
    DoDefault()
    proc Destroy()
      this.w_TREEVIEW = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='LOTTIART'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='KEY_ARTI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODART=space(20)
      .w_LOTTOE=space(20)
      .w_LOTTOR=space(20)
      .w_TIPRIC=space(1)
      .w_LOTTO=space(20)
      .w_KEYART=space(20)
      .w_CODLOTR=space(20)
      .w_CODLOTE=space(20)
      .w_CODLOT=space(10)
      .w_CURSORNA=space(10)
      .w_FL__ELAB=.f.
      .w_DESART=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATCRE=ctod("  /  /  ")
      .w_KEYLOT=space(61)
      .w_SERIAL=space(10)
      .w_NUMRIF=0
      .w_SERDIC=space(20)
      .w_KEYSAL=space(41)
      .w_FLSTAT=space(1)
      .w_CODRIC=space(41)
      .w_CADESAR=space(40)
      .w_QTAMOV=0
      .w_UNIMIS=space(3)
      .w_MAGAZ=space(5)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_CODUBI=space(20)
      .w_DATREG=ctod("  /  /  ")
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODART))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_LOTTOE))
          .link_1_2('Full')
        endif
        .w_LOTTOR = SPACE(20)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_LOTTOR))
          .link_1_3('Full')
        endif
        .w_TIPRIC = 'P'
        .w_LOTTO = IIF(g_APPLICATION <>"ADHOC REVOLUTION",.w_LOTTOE,.w_LOTTOR)
        .w_KEYART = Nvl( .w_TREEVIEW.GETVAR('KEYSAL') , Space(20))
      .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
        .w_CODLOTR = IIF(g_APPLICATION<>"ADHOC REVOLUTION",SPACE(20),Nvl( .w_TREEVIEW.GETVAR('LOTTO') , Space(20)))
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODLOTR))
          .link_1_8('Full')
        endif
        .w_CODLOTE = IIF(g_APPLICATION="ADHOC REVOLUTION",SPACE(20),Nvl( .w_TREEVIEW.GETVAR('LOTTO') , Space(20)))
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODLOTE))
          .link_1_9('Full')
        endif
        .w_CODLOT = Nvl( .w_TREEVIEW.GETVAR('LOTTO') , Space(20))
          .DoRTCalc(10,10,.f.)
        .w_FL__ELAB = .F.
          .DoRTCalc(12,12,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(14,14,.f.)
        .w_KEYLOT = Alltrim(.w_CODART)+Alltrim(.w_LOTTO)
        .w_SERIAL = IIF(Not empty(Nvl( .w_TREEVIEW.GETVAR('SERDOC') , Space(10))),Nvl( .w_TREEVIEW.GETVAR('SERDOC') , Space(10)) , Nvl( .w_TREEVIEW.GETVAR('SERMVM') , Space(10)))
        .w_NUMRIF = ICASE(Not empty(Nvl( .w_TREEVIEW.GETVAR('SERDOC') , Space(10))),-20,Not empty(Nvl( .w_TREEVIEW.GETVAR('SERMVM') , Space(10))),-10,0)
        .w_SERDIC = Nvl( .w_TREEVIEW.GETVAR('SERDIC') , Space(20))
          .DoRTCalc(19,19,.f.)
        .w_FLSTAT = Nvl( .w_TREEVIEW.GETVAR('FLSTAT') , ' ')
        .w_CODRIC = nvl(.w_TREEVIEW.getvar("CODART"),'')
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_CODRIC))
          .link_1_38('Full')
        endif
          .DoRTCalc(22,22,.f.)
        .w_QTAMOV = .w_TREEVIEW.getvar("QTAMOV")
        .w_UNIMIS = .w_TREEVIEW.getvar("UNIMIS")
        .w_MAGAZ = .w_TREEVIEW.getvar("CODMAG")
        .w_NUMDOC = .w_TREEVIEW.getvar("NUMDOC")
        .w_ALFDOC = .w_TREEVIEW.getvar("ALFDOC")
        .w_CODUBI = .w_TREEVIEW.getvar("CODUBI")
        .w_DATREG = .w_TREEVIEW.getvar("DATREG")
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CODART<>.w_CODART
            .w_LOTTOR = SPACE(20)
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.t.)
        if .o_LOTTOE<>.w_LOTTOE.or. .o_LOTTOR<>.w_LOTTOR
            .w_LOTTO = IIF(g_APPLICATION <>"ADHOC REVOLUTION",.w_LOTTOE,.w_LOTTOR)
        endif
            .w_KEYART = Nvl( .w_TREEVIEW.GETVAR('KEYSAL') , Space(20))
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
            .w_CODLOTR = IIF(g_APPLICATION<>"ADHOC REVOLUTION",SPACE(20),Nvl( .w_TREEVIEW.GETVAR('LOTTO') , Space(20)))
          .link_1_8('Full')
            .w_CODLOTE = IIF(g_APPLICATION="ADHOC REVOLUTION",SPACE(20),Nvl( .w_TREEVIEW.GETVAR('LOTTO') , Space(20)))
          .link_1_9('Full')
        if .o_CODLOTE<>.w_CODLOTE.or. .o_CODLOTR<>.w_CODLOTR
            .w_CODLOT = Nvl( .w_TREEVIEW.GETVAR('LOTTO') , Space(20))
        endif
        if .o_CODART<>.w_CODART.or. .o_LOTTO<>.w_LOTTO
          .Calculate_SBRKGDBRNL()
        endif
        .DoRTCalc(10,12,.t.)
            .w_OBTEST = i_DATSYS
        .DoRTCalc(14,14,.t.)
        if .o_CODART<>.w_CODART.or. .o_LOTTO<>.w_LOTTO
            .w_KEYLOT = Alltrim(.w_CODART)+Alltrim(.w_LOTTO)
        endif
            .w_SERIAL = IIF(Not empty(Nvl( .w_TREEVIEW.GETVAR('SERDOC') , Space(10))),Nvl( .w_TREEVIEW.GETVAR('SERDOC') , Space(10)) , Nvl( .w_TREEVIEW.GETVAR('SERMVM') , Space(10)))
            .w_NUMRIF = ICASE(Not empty(Nvl( .w_TREEVIEW.GETVAR('SERDOC') , Space(10))),-20,Not empty(Nvl( .w_TREEVIEW.GETVAR('SERMVM') , Space(10))),-10,0)
            .w_SERDIC = Nvl( .w_TREEVIEW.GETVAR('SERDIC') , Space(20))
        .DoRTCalc(19,19,.t.)
        if .o_CODLOTE<>.w_CODLOTE.or. .o_CODLOTR<>.w_CODLOTR
            .w_FLSTAT = Nvl( .w_TREEVIEW.GETVAR('FLSTAT') , ' ')
        endif
            .w_CODRIC = nvl(.w_TREEVIEW.getvar("CODART"),'')
          .link_1_38('Full')
        .DoRTCalc(22,22,.t.)
            .w_QTAMOV = .w_TREEVIEW.getvar("QTAMOV")
            .w_UNIMIS = .w_TREEVIEW.getvar("UNIMIS")
            .w_MAGAZ = .w_TREEVIEW.getvar("CODMAG")
            .w_NUMDOC = .w_TREEVIEW.getvar("NUMDOC")
            .w_ALFDOC = .w_TREEVIEW.getvar("ALFDOC")
            .w_CODUBI = .w_TREEVIEW.getvar("CODUBI")
            .w_DATREG = .w_TREEVIEW.getvar("DATREG")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
    endwith
  return

  proc Calculate_VFXKQMRIKX()
    with this
          * --- GSMA_BTR(CLOSE) - Done
          gsma_btr(this;
              ,.w_LOTTO;
              ,.w_CODART;
              ,'Close';
             )
    endwith
  endproc
  proc Calculate_IUBWKGLBYK()
    with this
          * --- GSMA_BTR(RELOAD) - Reload
          gsma_btr(this;
              ,.w_LOTTO;
              ,.w_CODART;
              ,'Reload';
             )
    endwith
  endproc
  proc Calculate_SBRKGDBRNL()
    with this
          * --- Reimposta flag elaborazione
          .w_FL__ELAB = .F.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLOTTOE_1_2.enabled = this.oPgFrm.Page1.oPag.oLOTTOE_1_2.mCond()
    this.oPgFrm.Page1.oPag.oLOTTOR_1_3.enabled = this.oPgFrm.Page1.oPag.oLOTTOR_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oLOTTOE_1_2.visible=!this.oPgFrm.Page1.oPag.oLOTTOE_1_2.mHide()
    this.oPgFrm.Page1.oPag.oLOTTOR_1_3.visible=!this.oPgFrm.Page1.oPag.oLOTTOR_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCODLOTR_1_8.visible=!this.oPgFrm.Page1.oPag.oCODLOTR_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCODLOTE_1_9.visible=!this.oPgFrm.Page1.oPag.oCODLOTE_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oALFDOC_1_47.visible=!this.oPgFrm.Page1.oPag.oALFDOC_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.TREEVIEW.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_VFXKQMRIKX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Reload")
          .Calculate_IUBWKGLBYK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODART
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_1'),i_cWhere,'GSMA_AAR',"Articoli",'lottizoom.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTTOE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTTOE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOTTOART like "+cp_ToStrODBC(trim(this.w_LOTTOE)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOTTOART,LODATCRE,LOKEYSAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOTTOART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODART;
                     ,'LOTTOART',trim(this.w_LOTTOE))
          select LOCODART,LOTTOART,LODATCRE,LOKEYSAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOTTOART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTTOE)==trim(_Link_.LOTTOART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTTOE) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOTTOART',cp_AbsName(oSource.parent,'oLOTTOE_1_2'),i_cWhere,'',"Lotti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOTTOART,LODATCRE,LOKEYSAL";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOTTOART,LODATCRE,LOKEYSAL;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOTTOART,LODATCRE,LOKEYSAL";
                     +" from "+i_cTable+" "+i_lTable+" where LOTTOART="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOTTOART',oSource.xKey(2))
            select LOCODART,LOTTOART,LODATCRE,LOKEYSAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTTOE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOTTOART,LODATCRE,LOKEYSAL";
                   +" from "+i_cTable+" "+i_lTable+" where LOTTOART="+cp_ToStrODBC(this.w_LOTTOE);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOTTOART',this.w_LOTTOE)
            select LOCODART,LOTTOART,LODATCRE,LOKEYSAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTTOE = NVL(_Link_.LOTTOART,space(20))
      this.w_DATCRE = NVL(cp_ToDate(_Link_.LODATCRE),ctod("  /  /  "))
      this.w_KEYSAL = NVL(_Link_.LOKEYSAL,space(41))
    else
      if i_cCtrl<>'Load'
        this.w_LOTTOE = space(20)
      endif
      this.w_DATCRE = ctod("  /  /  ")
      this.w_KEYSAL = space(41)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOTTOART,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTTOE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTTOR
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTTOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTTOR)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODART;
                     ,'LOCODICE',trim(this.w_LOTTOR))
          select LOCODART,LOCODICE,LODATCRE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTTOR)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTTOR) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTTOR_1_3'),i_cWhere,'',"Lotti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LODATCRE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LODATCRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTTOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTTOR);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOCODICE',this.w_LOTTOR)
            select LOCODART,LOCODICE,LODATCRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTTOR = NVL(_Link_.LOCODICE,space(20))
      this.w_DATCRE = NVL(cp_ToDate(_Link_.LODATCRE),ctod("  /  /  "))
      this.w_KEYSAL = NVL(_Link_.LOCODART,space(41))
    else
      if i_cCtrl<>'Load'
        this.w_LOTTOR = space(20)
      endif
      this.w_DATCRE = ctod("  /  /  ")
      this.w_KEYSAL = space(41)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTTOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLOTR
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLOTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLOTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_CODLOTR);
                   +" and LOCODART="+cp_ToStrODBC(this.w_KEYART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_KEYART;
                       ,'LOCODICE',this.w_CODLOTR)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLOTR = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODLOTR = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLOTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLOTE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLOTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLOTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOTTOART";
                   +" from "+i_cTable+" "+i_lTable+" where LOTTOART="+cp_ToStrODBC(this.w_CODLOTE);
                   +" and LOCODART="+cp_ToStrODBC(this.w_KEYART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_KEYART;
                       ,'LOTTOART',this.w_CODLOTE)
            select LOCODART,LOTTOART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLOTE = NVL(_Link_.LOTTOART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODLOTE = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOTTOART,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLOTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRIC
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODRIC)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRIC = NVL(_Link_.CACODICE,space(41))
      this.w_CADESAR = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODRIC = space(41)
      endif
      this.w_CADESAR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODART_1_1.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_1.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTTOE_1_2.value==this.w_LOTTOE)
      this.oPgFrm.Page1.oPag.oLOTTOE_1_2.value=this.w_LOTTOE
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTTOR_1_3.value==this.w_LOTTOR)
      this.oPgFrm.Page1.oPag.oLOTTOR_1_3.value=this.w_LOTTOR
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRIC_1_4.RadioValue()==this.w_TIPRIC)
      this.oPgFrm.Page1.oPag.oTIPRIC_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLOTR_1_8.value==this.w_CODLOTR)
      this.oPgFrm.Page1.oPag.oCODLOTR_1_8.value=this.w_CODLOTR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLOTE_1_9.value==this.w_CODLOTE)
      this.oPgFrm.Page1.oPag.oCODLOTE_1_9.value=this.w_CODLOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_23.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_23.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODRIC_1_38.value==this.w_CODRIC)
      this.oPgFrm.Page1.oPag.oCODRIC_1_38.value=this.w_CODRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESAR_1_39.value==this.w_CADESAR)
      this.oPgFrm.Page1.oPag.oCADESAR_1_39.value=this.w_CADESAR
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAMOV_1_41.value==this.w_QTAMOV)
      this.oPgFrm.Page1.oPag.oQTAMOV_1_41.value=this.w_QTAMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_42.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_42.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGAZ_1_45.value==this.w_MAGAZ)
      this.oPgFrm.Page1.oPag.oMAGAZ_1_45.value=this.w_MAGAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_46.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_46.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_47.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_47.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_51.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_51.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_53.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_53.value=this.w_DATREG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_LOTTOE))  and not(g_APPLICATION="ADHOC REVOLUTION")  and (!EMPTY(NVL(.w_CODART, ' ')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTTOE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_LOTTOE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LOTTOR))  and not(g_APPLICATION <>"ADHOC REVOLUTION")  and (!EMPTY(NVL(.w_CODART, ' ')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTTOR_1_3.SetFocus()
            i_bnoObbl = !empty(.w_LOTTOR)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODART = this.w_CODART
    this.o_LOTTOE = this.w_LOTTOE
    this.o_LOTTOR = this.w_LOTTOR
    this.o_LOTTO = this.w_LOTTO
    this.o_CODLOTR = this.w_CODLOTR
    this.o_CODLOTE = this.w_CODLOTE
    return

enddefine

* --- Define pages as container
define class tgsma_ktrPag1 as StdContainer
  Width  = 780
  height = 571
  stdWidth  = 780
  stdheight = 571
  resizeXpos=293
  resizeYpos=424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODART_1_1 as StdField with uid="CJMHWVPYIS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo",;
    HelpContextID = 193259482,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=132, Top=12, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_LOTTOE)
        bRes2=.link_1_2('Full')
      endif
      if .not. empty(.w_LOTTOR)
        bRes2=.link_1_3('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODART_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Articoli",'lottizoom.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oLOTTOE_1_2 as StdField with uid="DOKYHDKEJA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LOTTOE", cQueryName = "LOTTOE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 178317130,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=132, Top=36, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOTTOART", oKey_2_2="this.w_LOTTOE"

  func oLOTTOE_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(NVL(.w_CODART, ' ')))
    endwith
   endif
  endfunc

  func oLOTTOE_1_2.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  func oLOTTOE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTTOE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTTOE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOTTOART',cp_AbsName(this.parent,'oLOTTOE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'',this.parent.oContained
  endproc

  add object oLOTTOR_1_3 as StdField with uid="VLUFQRJBXJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LOTTOR", cQueryName = "LOTTOR",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice lotto sul quale effettuare la tracciabilità",;
    HelpContextID = 228648778,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=132, Top=36, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTTOR"

  func oLOTTOR_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(NVL(.w_CODART, ' ')))
    endwith
   endif
  endfunc

  func oLOTTOR_1_3.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oLOTTOR_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTTOR_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTTOR_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTTOR_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'',this.parent.oContained
  endproc


  add object oTIPRIC_1_4 as StdCombo with uid="PHCYPTVUQR",rtseq=4,rtrep=.f.,left=406,top=36,width=173,height=21;
    , ToolTipText = "Tipo ricerca";
    , HelpContextID = 218311882;
    , cFormVar="w_TIPRIC",RowSource=""+"Prodotto finito,"+"Componente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPRIC_1_4.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oTIPRIC_1_4.GetRadio()
    this.Parent.oContained.w_TIPRIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPRIC_1_4.SetRadio()
    this.Parent.oContained.w_TIPRIC=trim(this.Parent.oContained.w_TIPRIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRIC=='P',1,;
      iif(this.Parent.oContained.w_TIPRIC=='C',2,;
      0))
  endfunc


  add object TREEVIEW as cp_Treeview with uid="FVJYSBPBBD",left=14, top=65, width=703,height=410,;
    caption='Object',;
   bGlobalFont=.t.,;
    cCursor="cur_str",cShowFields="DESTRI",cNodeShowField="LOTTO",cLeafShowField="LOTTO",cNodeBmp="ROOT.BMP,CONTO.BMP,LEAF.BMP,NEW1.BMP",cLeafBmp="",nIndent=20,cLvlSep="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 95521766

  add object oCODLOTR_1_8 as StdField with uid="CQUQMQURRM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODLOTR", cQueryName = "CODLOTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 72751142,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=94, Top=482, InputMask=replicate('X',20), cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_KEYART", oKey_2_1="LOCODICE", oKey_2_2="this.w_CODLOTR"

  func oCODLOTR_1_8.mHide()
    with this.Parent.oContained
      return (g_APPLICATION<>"ADHOC REVOLUTION" OR  .w_FLSTAT<>'L')
    endwith
  endfunc

  func oCODLOTR_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODLOTE_1_9 as StdField with uid="NMTFIVDPTR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODLOTE", cQueryName = "CODLOTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 72751142,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=94, Top=483, InputMask=replicate('X',20), cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_KEYART", oKey_2_1="LOTTOART", oKey_2_2="this.w_CODLOTE"

  func oCODLOTE_1_9.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION" OR  .w_FLSTAT<>'L')
    endwith
  endfunc

  func oCODLOTE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oBtn_1_12 as StdButton with uid="PPWFAOCXOY",left=723, top=62, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per riempire la tree view";
    , HelpContextID = 94446358;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        .NotifyEvent("Reload")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_CODART) and Not Empty(.w_LOTTO))
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="ZVAMGQYVJH",left=723, top=162, width=48,height=45,;
    CpPicture="BMP\IMPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per chiudere la struttura";
    , HelpContextID = 159844730;
    , Caption='\<Implodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(NVL(.w_CODLOT,'')) And .w_FL__ELAB)
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="EDOPRFXLCY",left=723, top=212, width=48,height=45,;
    CpPicture="BMP\lotti.ico", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la gestione dell'elemento";
    , HelpContextID = 226412362;
    , Caption='\<Lotto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        gsma_btr(this.Parent.oContained,.w_CODLOT,.w_KEYART,"Lotto")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty ( NVL ( .w_CODLOT,'' ) ) And .w_FL__ELAB And .w_FLSTAT='L')
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="XWAVWHNZRN",left=723, top=112, width=48,height=45,;
    CpPicture="BMP\ESPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per espandere la struttura";
    , HelpContextID = 159843258;
    , Caption='\<Esplodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (NVL(.w_CODLOT,'')) And .w_FL__ELAB)
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="KOTWJAFPMC",left=723, top=427, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 240244486;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="CCGSUHUDGX",left=723, top=377, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 82455322;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        gsma_btr(this.Parent.oContained,.w_LOTTO,.w_CODART,"Stampa")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty ( NVL ( .w_CODLOT,'' ) ) And .w_FL__ELAB)
      endwith
    endif
  endfunc

  add object oDESART_1_23 as StdField with uid="MYPNUDTNTW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 193200586,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=286, Top=12, InputMask=replicate('X',40)


  add object oBtn_1_28 as StdButton with uid="DCMOPWBDMK",left=723, top=262, width=48,height=45,;
    CpPicture="BMP\Documenti.ico", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la gestione dell'elemento";
    , HelpContextID = 123654497;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        gsma_btr(this.Parent.oContained," "," ","Origin")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (.w_SERIAL ) And .w_FL__ELAB)
      endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="AQNFXSFSOL",left=723, top=312, width=48,height=45,;
    CpPicture="bmp\DM_archivi.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la gestione dell'elemento";
    , HelpContextID = 51651172;
    , Caption='\<Dichiar.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        gsma_btr(this.Parent.oContained,.w_SERDIC, "" ,"Dichia")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty ( .w_SERDIC ) And .w_FL__ELAB)
      endwith
    endif
  endfunc

  add object oCODRIC_1_38 as StdField with uid="VXOAYLFGWV",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CODRIC", cQueryName = "CODRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    HelpContextID = 218359770,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=94, Top=541, InputMask=replicate('X',41), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODRIC"

  func oCODRIC_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCADESAR_1_39 as StdField with uid="RXUHOYJINC",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CADESAR", cQueryName = "CADESAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26151462,;
   bGlobalFont=.t.,;
    Height=21, Width=351, Left=405, Top=541, InputMask=replicate('X',40)

  add object oQTAMOV_1_41 as StdField with uid="ZJAXUJZHTF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_QTAMOV", cQueryName = "QTAMOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 162075130,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=405, Top=484, cSayPict="V_PQ(14)"

  add object oUNIMIS_1_42 as StdField with uid="COHMDEMJGX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 218666938,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=715, Top=484, InputMask=replicate('X',3)

  add object oMAGAZ_1_45 as StdField with uid="ULHQATTNMM",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MAGAZ", cQueryName = "MAGAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 251970874,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=604, Top=484, InputMask=replicate('X',5)

  add object oNUMDOC_1_46 as StdField with uid="LGLFEGUFHL",rtseq=26,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 212947242,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=94, Top=511

  add object oALFDOC_1_47 as StdField with uid="DJPOAMSRPN",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 212978426,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=163, Top=511, InputMask=replicate('X',10)

  func oALFDOC_1_47.mHide()
    with this.Parent.oContained
      return (empty(.w_ALFDOC))
    endwith
  endfunc

  add object oCODUBI_1_51 as StdField with uid="YGHHKRLSXG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 124839898,;
   bGlobalFont=.t.,;
    Height=21, Width=351, Left=405, Top=511, InputMask=replicate('X',20)

  add object oDATREG_1_53 as StdField with uid="FHORQSKKJP",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 155383242,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=245, Top=511, cSayPict="V_PQ(14)"

  add object oStr_1_17 as StdString with uid="WRZDNROCOK",Visible=.t., Left=45, Top=38,;
    Alignment=1, Width=83, Height=18,;
    Caption="Codice lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="NGRLQEALIK",Visible=.t., Left=38, Top=16,;
    Alignment=1, Width=90, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPRIC<>'P')
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="TWMYFQCSRW",Visible=.t., Left=293, Top=38,;
    Alignment=1, Width=110, Height=18,;
    Caption="Tipo ricerca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="CXJBPEWGEF",Visible=.t., Left=15, Top=16,;
    Alignment=1, Width=113, Height=18,;
    Caption="Prodotto finito:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_TIPRIC<>'C')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="KIVPUHWYLM",Visible=.t., Left=7, Top=486,;
    Alignment=1, Width=83, Height=18,;
    Caption="Codice lotto :"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return ( .w_FLSTAT<>'L')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="FAADZNHFCC",Visible=.t., Left=679, Top=487,;
    Alignment=1, Width=32, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="MAWHPOBZVA",Visible=.t., Left=333, Top=486,;
    Alignment=1, Width=71, Height=18,;
    Caption="Quantità:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="LSEEDUNPIB",Visible=.t., Left=532, Top=487,;
    Alignment=1, Width=69, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="MADMCVWCKJ",Visible=.t., Left=152, Top=511,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (empty(.w_ALFDOC))
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="FEOVYMXXOC",Visible=.t., Left=32, Top=513,;
    Alignment=1, Width=58, Height=15,;
    Caption="Num.doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="NUWVNLMBAK",Visible=.t., Left=32, Top=544,;
    Alignment=1, Width=58, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="KLZPJQWRPS",Visible=.t., Left=333, Top=513,;
    Alignment=1, Width=71, Height=18,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="HEJICRMHLR",Visible=.t., Left=200, Top=513,;
    Alignment=1, Width=42, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_ktr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
