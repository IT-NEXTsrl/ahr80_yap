* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bvm                                                        *
*              Visualizza schede analitica                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_35]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2013-12-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bvm",oParentObject)
return(i_retval)

define class tgsca_bvm as StdBatch
  * --- Local variables
  w_ZOOM = .NULL.
  w_IMPORTO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizza Schede Analitica (da GSCA_SZM)
    *     GSCA1SZM.VQR => Manuali Originari
    *     GSCA_SZM.VQR => Primanota Originari
    *     GSCA4SZM.VQR => Documenti Originari
    *     GSCA3SZM.VQR => Manuali Ripartiti
    *     GSCA2SZM.VQR => Primanota Ripartiti
    *     GSCA5SZM.VQR => Documenti Ripartiti
    * --- Flgmov: (E=Effettivo - P=Previsionale)
    * --- Prove: (T=Tutti - O=Originari - E=Escluso Ripartiti)
    * --- Provconf: (N=Confermato - S=Provvisorio)
    * --- Controllo che ci sia un filtro o per Centro di costo o per commessa (se gestita)
    * --- Almeno uno dei due deve essere valorizzato.
    do case
      case EMPTY(this.oParentObject.w_CODCON) and EMPTY(this.oParentObject.w_CODVOC) and g_PERCAN<>"S" AND this.oParentObject.w_NOANALITICA="N"
        ah_ErrorMsg("Inserire un centro di costo/ricavo o una voce di costo/ricavo",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CODCON) AND EMPTY(this.oParentObject.w_CODVOC) AND EMPTY(this.oParentObject.w_CODCOM) AND g_PERCAN="S" AND this.oParentObject.w_NOANALITICA="N"
        ah_ErrorMsg("Inserire un centro di costo/ricavo o una voce di costo/ricavo o una commessa",,"")
        i_retcode = 'stop'
        return
    endcase
    this.oParentObject.NotifyEvent("Esegui")
    * --- Le conversioni non vengono pi� fatte nelle query.
    this.w_ZOOM = this.oParentObject.w_ZoomScad
    SELECT ( this.w_ZOOM.cCursor )
    GO TOP
    * --- Ciclo sul cursore dello zoom per convertire gli importi
    do while Not Eof ( this.w_ZOOM.cCursor )
      * --- Se la valuta dell'esercizio � diversa da quella dell'esercizio del movimento,
      *     converto, altrimenti non faccio nulla.
      if VALNAZ<>g_PERVAL
        if CAOVAL=0
          this.w_IMPORTO = VAL2MON(TOTIMP, GETCAM(VALNAZ, G_FINESE),1,G_FINESE, VALNAZ)
        else
          this.w_IMPORTO = VAL2MON(TOTIMP, CAOVAL,1,G_FINESE, VALNAZ)
        endif
        REPLACE TOTIMP WITH this.w_IMPORTO
      endif
       
 Select ( this.w_ZOOM.cCursor ) 
 Skip
    enddo
    SELECT ( this.w_ZOOM.cCursor )
    GO TOP
    * --- Calcolo il Saldo
    this.oParentObject.w_SALDO = 0
    SUM TOTIMP * IIF(SEGNO="D", 1, -1) TO this.oParentObject.w_SALDO FOR NOT ISNULL(TOTIMP)
    this.oParentObject.w_FLGDAVE = IIF(nvl(this.oParentObject.w_SALDO,0)<0, "A", "D")
    this.oParentObject.w_SALDO = cp_ROUND(ABS(NVL(this.oParentObject.w_SALDO,0)),g_PERPVL)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
