* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_ket                                                        *
*              Evasione righe documenti                                        *
*                                                                              *
*      Author: Zucchetti Tam S.p.A                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_181]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-03                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsve_ket
* Occorre essere amministratore per svolgere il back Up
if not cp_IsAdministrator(.F.)
   Ah_ErrorMsg("Procedura utilizzabile solo da utenti con privilegi di amministratore%0Rivolgersi al proprio amministratore di sistema",'!')
   return
endif

* --- Fine Area Manuale
return(createobject("tgsve_ket",oParentObject))

* --- Class definition
define class tgsve_ket as StdForm
  Top    = 2
  Left   = 4

  * --- Standard Properties
  Width  = 809
  Height = 437
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=65679977
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Constant Properties
  _IDX = 0
  MAGAZZIN_IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsve_ket"
  cComment = "Evasione righe documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_DOCFOR = space(1)
  o_DOCFOR = space(1)
  w_CICLO = space(1)
  w_CATEG3 = space(2)
  o_CATEG3 = space(2)
  w_CATEG2 = space(2)
  o_CATEG2 = space(2)
  w_CATEG1 = space(2)
  o_CATEG1 = space(2)
  w_TDFLVEAC = space(1)
  w_CATEGO = space(2)
  o_CATEGO = space(2)
  w_LTIPCON = space(1)
  o_LTIPCON = space(1)
  w_CATEGF = space(2)
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_CAUDOC = space(5)
  o_CAUDOC = space(5)
  w_DATA1 = ctod('  /  /  ')
  o_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_CODCON = space(15)
  w_MVSERIAL = space(10)
  w_PARAME = space(3)
  w_SELEZI = space(1)
  w_FLEVAS = space(10)
  w_DESCRI = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CALSER = space(10)
  w_SERIAL = space(10)
  w_DESDOC = space(35)
  w_ZoomMast = .NULL.
  w_ZoomDet = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsve_ket
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSVE_KET'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_ketPag1","gsve_ket",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDOCFOR_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMast = this.oPgFrm.Pages(1).oPag.ZoomMast
    this.w_ZoomDet = this.oPgFrm.Pages(1).oPag.ZoomDet
    DoDefault()
    proc Destroy()
      this.w_ZoomMast = .NULL.
      this.w_ZoomDet = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='CONTI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLVEAC=space(1)
      .w_DOCFOR=space(1)
      .w_CICLO=space(1)
      .w_CATEG3=space(2)
      .w_CATEG2=space(2)
      .w_CATEG1=space(2)
      .w_TDFLVEAC=space(1)
      .w_CATEGO=space(2)
      .w_LTIPCON=space(1)
      .w_CATEGF=space(2)
      .w_TIPCON=space(1)
      .w_CAUDOC=space(5)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_CODCON=space(15)
      .w_MVSERIAL=space(10)
      .w_PARAME=space(3)
      .w_SELEZI=space(1)
      .w_FLEVAS=space(10)
      .w_DESCRI=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CALSER=space(10)
      .w_SERIAL=space(10)
      .w_DESDOC=space(35)
        .w_FLVEAC = this.oParentObject
        .w_DOCFOR = 'N'
        .w_CICLO = IIF(.w_DOCFOR='N',.w_FLVEAC,'A')
        .w_CATEG3 = 'XX'
        .w_CATEG2 = 'DT'
        .w_CATEG1 = 'XX'
          .DoRTCalc(7,7,.f.)
        .w_CATEGO = IIF(.w_FLVEAC='A', .w_CATEG3,IIF(.w_DOCFOR='S',.w_CATEG2,.w_CATEG1))
          .DoRTCalc(9,9,.f.)
        .w_CATEGF = IIF(.w_CATEGO='XX','  ' ,.w_CATEGO)
        .w_TIPCON = IIF(.w_LTIPCON $ "CF", IIF(.w_FLVEAC='A' OR .w_DOCFOR='S','F','C'), " ")
        .w_CAUDOC = SPACE(5)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CAUDOC))
          .link_1_12('Full')
        endif
        .w_DATA1 = g_INIESE
        .w_DATA2 = g_FINESE
        .w_CODCON = SPACE(15)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CODCON))
          .link_1_15('Full')
        endif
        .w_MVSERIAL = Nvl( .w_ZoomMast.getvar('MVSERIAL') , Space(10) )
        .w_PARAME = .w_ZoomMast.getVar('MVFLVEAC')+.w_ZoomMast.getVar('MVCLADOC')
        .w_SELEZI = 'D'
          .DoRTCalc(19,20,.f.)
        .w_OBTEST = .w_DATA1
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
          .DoRTCalc(22,22,.f.)
        .w_CALSER = .w_ZoomMast.getVar('MVSERIAL')
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
      .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .w_SERIAL = IIF(EMPTY(NVL(.w_CALSER,' ')),'zzzzzzzzzz', .w_CALSER)
      .oPgFrm.Page1.oPag.ZoomDet.Calculate(.w_SERIAL)
    endwith
    this.DoRTCalc(25,25,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CATEGO<>.w_CATEGO.or. .o_DOCFOR<>.w_DOCFOR.or. .o_CATEG1<>.w_CATEG1.or. .o_FLVEAC<>.w_FLVEAC
            .w_CICLO = IIF(.w_DOCFOR='N',.w_FLVEAC,'A')
        endif
        .DoRTCalc(4,7,.t.)
        if .o_CATEG1<>.w_CATEG1.or. .o_CATEG2<>.w_CATEG2.or. .o_CATEG3<>.w_CATEG3.or. .o_DOCFOR<>.w_DOCFOR.or. .o_FLVEAC<>.w_FLVEAC
            .w_CATEGO = IIF(.w_FLVEAC='A', .w_CATEG3,IIF(.w_DOCFOR='S',.w_CATEG2,.w_CATEG1))
        endif
        .DoRTCalc(9,9,.t.)
        if .o_CATEGO<>.w_CATEGO
            .w_CATEGF = IIF(.w_CATEGO='XX','  ' ,.w_CATEGO)
        endif
        if .o_CAUDOC<>.w_CAUDOC.or. .o_FLVEAC<>.w_FLVEAC.or. .o_DOCFOR<>.w_DOCFOR.or. .o_LTIPCON<>.w_LTIPCON
            .w_TIPCON = IIF(.w_LTIPCON $ "CF", IIF(.w_FLVEAC='A' OR .w_DOCFOR='S','F','C'), " ")
        endif
        if .o_CATEGO<>.w_CATEGO.or. .o_DOCFOR<>.w_DOCFOR
            .w_CAUDOC = SPACE(5)
          .link_1_12('Full')
        endif
        .DoRTCalc(13,14,.t.)
        if .o_TIPCON<>.w_TIPCON.or. .o_CATEGO<>.w_CATEGO.or. .o_FLVEAC<>.w_FLVEAC
            .w_CODCON = SPACE(15)
          .link_1_15('Full')
        endif
            .w_MVSERIAL = Nvl( .w_ZoomMast.getvar('MVSERIAL') , Space(10) )
            .w_PARAME = .w_ZoomMast.getVar('MVFLVEAC')+.w_ZoomMast.getVar('MVCLADOC')
        .DoRTCalc(18,20,.t.)
        if .o_DATA1<>.w_DATA1
            .w_OBTEST = .w_DATA1
        endif
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .DoRTCalc(22,22,.t.)
            .w_CALSER = .w_ZoomMast.getVar('MVSERIAL')
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
            .w_SERIAL = IIF(EMPTY(NVL(.w_CALSER,' ')),'zzzzzzzzzz', .w_CALSER)
        .oPgFrm.Page1.oPag.ZoomDet.Calculate(.w_SERIAL)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .oPgFrm.Page1.oPag.ZoomDet.Calculate(.w_SERIAL)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCON_1_15.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDOCFOR_1_2.visible=!this.oPgFrm.Page1.oPag.oDOCFOR_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCATEG3_1_4.visible=!this.oPgFrm.Page1.oPag.oCATEG3_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCATEG2_1_5.visible=!this.oPgFrm.Page1.oPag.oCATEG2_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCATEG1_1_6.visible=!this.oPgFrm.Page1.oPag.oCATEG1_1_6.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomMast.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDet.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CAUDOC
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDFLVEAC,TDFLINTE,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CAUDOC))
          select TDTIPDOC,TDFLVEAC,TDFLINTE,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCAUDOC_1_12'),i_cWhere,'',"Causali documento",'GSVE0KET.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDFLVEAC,TDFLINTE,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDFLVEAC,TDFLINTE,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDFLVEAC,TDFLINTE,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUDOC)
            select TDTIPDOC,TDFLVEAC,TDFLINTE,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_LTIPCON = NVL(_Link_.TDFLINTE,space(1))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CAUDOC = space(5)
      endif
      this.w_TDFLVEAC = space(1)
      this.w_LTIPCON = space(1)
      this.w_DESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CICLO=.w_TDFLVEAC
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_CAUDOC = space(5)
        this.w_TDFLVEAC = space(1)
        this.w_LTIPCON = space(1)
        this.w_DESDOC = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_LTIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_LTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_15'),i_cWhere,'',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_LTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_LTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_LTIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido oppure obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESCRI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDOCFOR_1_2.RadioValue()==this.w_DOCFOR)
      this.oPgFrm.Page1.oPag.oDOCFOR_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATEG3_1_4.RadioValue()==this.w_CATEG3)
      this.oPgFrm.Page1.oPag.oCATEG3_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATEG2_1_5.RadioValue()==this.w_CATEG2)
      this.oPgFrm.Page1.oPag.oCATEG2_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATEG1_1_6.RadioValue()==this.w_CATEG1)
      this.oPgFrm.Page1.oPag.oCATEG1_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUDOC_1_12.value==this.w_CAUDOC)
      this.oPgFrm.Page1.oPag.oCAUDOC_1_12.value=this.w_CAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_13.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_13.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_14.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_14.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_15.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_15.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_20.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLEVAS_1_22.RadioValue()==this.w_FLEVAS)
      this.oPgFrm.Page1.oPag.oFLEVAS_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_27.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_27.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_38.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_38.value=this.w_DESDOC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CICLO=.w_TDFLVEAC)  and not(empty(.w_CAUDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUDOC_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   ((empty(.w_DATA1)) or not(empty(.w_DATA2) OR .w_DATA1<=.w_DATA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DATA1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   ((empty(.w_DATA2)) or not(empty(.w_DATA1) OR .w_DATA1<=.w_DATA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_14.SetFocus()
            i_bnoObbl = !empty(.w_DATA2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (Not Empty(.w_CAUDOC) AND .w_LTIPCON $ ('C-F'))  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC = this.w_FLVEAC
    this.o_DOCFOR = this.w_DOCFOR
    this.o_CATEG3 = this.w_CATEG3
    this.o_CATEG2 = this.w_CATEG2
    this.o_CATEG1 = this.w_CATEG1
    this.o_CATEGO = this.w_CATEGO
    this.o_LTIPCON = this.w_LTIPCON
    this.o_TIPCON = this.w_TIPCON
    this.o_CAUDOC = this.w_CAUDOC
    this.o_DATA1 = this.w_DATA1
    return

enddefine

* --- Define pages as container
define class tgsve_ketPag1 as StdContainer
  Width  = 805
  height = 437
  stdWidth  = 805
  stdheight = 437
  resizeXpos=515
  resizeYpos=287
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDOCFOR_1_2 as StdCheck with uid="ODTJSCYAXV",rtseq=2,rtrep=.f.,left=587, top=4, caption="Documenti a fornitori",;
    ToolTipText = "Permette di selezionare anche i documenti a fornitore",;
    HelpContextID = 55595318,;
    cFormVar="w_DOCFOR", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oDOCFOR_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDOCFOR_1_2.GetRadio()
    this.Parent.oContained.w_DOCFOR = this.RadioValue()
    return .t.
  endfunc

  func oDOCFOR_1_2.SetRadio()
    this.Parent.oContained.w_DOCFOR=trim(this.Parent.oContained.w_DOCFOR)
    this.value = ;
      iif(this.Parent.oContained.w_DOCFOR=='S',1,;
      0)
  endfunc

  func oDOCFOR_1_2.mHide()
    with this.Parent.oContained
      return (g_ACQU='S')
    endwith
  endfunc


  add object oCATEG3_1_4 as StdCombo with uid="AZFEXNVYDI",rtseq=4,rtrep=.f.,left=89,top=9,width=130,height=21;
    , ToolTipText = "Categoria di appartenenza del documento selezionata";
    , HelpContextID = 204451034;
    , cFormVar="w_CATEG3",RowSource=""+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ordini,"+"Tutti i documenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATEG3_1_4.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'DT',;
    iif(this.value =3,'FA',;
    iif(this.value =4,'NC',;
    iif(this.value =5,'OR',;
    iif(this.value =6,'XX',;
    space(2))))))))
  endfunc
  func oCATEG3_1_4.GetRadio()
    this.Parent.oContained.w_CATEG3 = this.RadioValue()
    return .t.
  endfunc

  func oCATEG3_1_4.SetRadio()
    this.Parent.oContained.w_CATEG3=trim(this.Parent.oContained.w_CATEG3)
    this.value = ;
      iif(this.Parent.oContained.w_CATEG3=='DI',1,;
      iif(this.Parent.oContained.w_CATEG3=='DT',2,;
      iif(this.Parent.oContained.w_CATEG3=='FA',3,;
      iif(this.Parent.oContained.w_CATEG3=='NC',4,;
      iif(this.Parent.oContained.w_CATEG3=='OR',5,;
      iif(this.Parent.oContained.w_CATEG3=='XX',6,;
      0))))))
  endfunc

  func oCATEG3_1_4.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC<>'A')
    endwith
  endfunc


  add object oCATEG2_1_5 as StdCombo with uid="MXEMUHNBIO",rtseq=5,rtrep=.f.,left=89,top=9,width=130,height=21;
    , ToolTipText = "Categoria di appartenenza del documento selezionata";
    , HelpContextID = 221228250;
    , cFormVar="w_CATEG2",RowSource=""+"Carichi a fornitore,"+"DDT a fornitore,"+"Ordini", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATEG2_1_5.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'DT',;
    iif(this.value =3,'OR',;
    space(2)))))
  endfunc
  func oCATEG2_1_5.GetRadio()
    this.Parent.oContained.w_CATEG2 = this.RadioValue()
    return .t.
  endfunc

  func oCATEG2_1_5.SetRadio()
    this.Parent.oContained.w_CATEG2=trim(this.Parent.oContained.w_CATEG2)
    this.value = ;
      iif(this.Parent.oContained.w_CATEG2=='DI',1,;
      iif(this.Parent.oContained.w_CATEG2=='DT',2,;
      iif(this.Parent.oContained.w_CATEG2=='OR',3,;
      0)))
  endfunc

  func oCATEG2_1_5.mHide()
    with this.Parent.oContained
      return (.w_DOCFOR<>'S')
    endwith
  endfunc


  add object oCATEG1_1_6 as StdCombo with uid="QDJUHAEQBR",rtseq=6,rtrep=.f.,left=89,top=9,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 238005466;
    , cFormVar="w_CATEG1",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ricevute fiscali,"+"Ordini", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATEG1_1_6.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'RF',;
    iif(this.value =7,'OR',;
    space(2)))))))))
  endfunc
  func oCATEG1_1_6.GetRadio()
    this.Parent.oContained.w_CATEG1 = this.RadioValue()
    return .t.
  endfunc

  func oCATEG1_1_6.SetRadio()
    this.Parent.oContained.w_CATEG1=trim(this.Parent.oContained.w_CATEG1)
    this.value = ;
      iif(this.Parent.oContained.w_CATEG1=='XX',1,;
      iif(this.Parent.oContained.w_CATEG1=='DI',2,;
      iif(this.Parent.oContained.w_CATEG1=='DT',3,;
      iif(this.Parent.oContained.w_CATEG1=='FA',4,;
      iif(this.Parent.oContained.w_CATEG1=='NC',5,;
      iif(this.Parent.oContained.w_CATEG1=='RF',6,;
      iif(this.Parent.oContained.w_CATEG1=='OR',7,;
      0)))))))
  endfunc

  func oCATEG1_1_6.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC<>'V' OR .w_DOCFOR='S')
    endwith
  endfunc

  add object oCAUDOC_1_12 as StdField with uid="ZWFYVYVPJF",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CAUDOC", cQueryName = "CAUDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Causale documento",;
    HelpContextID = 196123866,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=89, Top=35, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CAUDOC"

  func oCAUDOC_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUDOC_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUDOC_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCAUDOC_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documento",'GSVE0KET.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oDATA1_1_13 as StdField with uid="XGIRREYHAS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data iniziale",;
    HelpContextID = 9678026,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=587, Top=32

  func oDATA1_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA2) OR .w_DATA1<=.w_DATA2)
    endwith
    return bRes
  endfunc

  add object oDATA2_1_14 as StdField with uid="IBRSKWRGIJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data finale",;
    HelpContextID = 8629450,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=722, Top=32

  func oDATA2_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA1) OR .w_DATA1<=.w_DATA2)
    endwith
    return bRes
  endfunc

  add object oCODCON_1_15 as StdField with uid="TBNJKISWQO",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido oppure obsoleto",;
    ToolTipText = "Codice intestatario documento",;
    HelpContextID = 11706074,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=89, Top=60, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_LTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_CAUDOC) AND .w_LTIPCON $ ('C-F'))
    endwith
   endif
  endfunc

  func oCODCON_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_LTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_LTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti/fornitori",'',this.parent.oContained
  endproc


  add object oBtn_1_16 as StdButton with uid="OASZMZLCNI",left=750, top=62, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 111242262;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        gsve_bet(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_20 as StdRadio with uid="MLUVUOFHZO",rtseq=18,rtrep=.f.,left=134, top=387, width=129,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_20.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 83896282
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 83896282
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_20.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_20.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  add object oFLEVAS_1_22 as StdCheck with uid="BRMVQVOMVD",rtseq=19,rtrep=.f.,left=276, top=387, caption="Solo flag evasione",;
    ToolTipText = "Se attivo esegue evasione valorizzando solo il flag evaso",;
    HelpContextID = 58748502,;
    cFormVar="w_FLEVAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLEVAS_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLEVAS_1_22.GetRadio()
    this.Parent.oContained.w_FLEVAS = this.RadioValue()
    return .t.
  endfunc

  func oFLEVAS_1_22.SetRadio()
    this.Parent.oContained.w_FLEVAS=trim(this.Parent.oContained.w_FLEVAS)
    this.value = ;
      iif(this.Parent.oContained.w_FLEVAS=='S',1,;
      0)
  endfunc


  add object oBtn_1_25 as StdButton with uid="UXVJTLRKRH",left=700, top=388, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 65651226;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        gsve_bet(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DATA1) AND NOT EMPTY(.w_DATA2))
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="KNQTJANIEM",left=750, top=388, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 58362554;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI_1_27 as StdField with uid="ANQFDOVDCR",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 92387530,;
   bGlobalFont=.t.,;
    Height=21, Width=299, Left=198, Top=60, InputMask=replicate('X',40)


  add object oObj_1_30 as cp_runprogram with uid="QRUWIYHIMR",left=223, top=453, width=156,height=22,;
    caption='gsve_bet',;
   bGlobalFont=.t.,;
    prg="gsve_bet('S')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 74622938


  add object oObj_1_31 as cp_runprogram with uid="IKTKTRDRQJ",left=381, top=454, width=169,height=21,;
    caption='gsve_bet',;
   bGlobalFont=.t.,;
    prg="GSVE_BZM(w_MVSERIAL, w_PARAME)",;
    cEvent = "w_zoomdet selected",;
    nPag=1;
    , HelpContextID = 74622938


  add object oBtn_1_32 as StdButton with uid="IVUYXSPMRW",left=10, top=388, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato alla riga selezionata";
    , HelpContextID = 106858593;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_MVSERIAL, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVSERIAL))
      endwith
    endif
  endfunc


  add object oObj_1_34 as cp_runprogram with uid="XDLYXTTSNE",left=112, top=453, width=109,height=24,;
    caption='gsve_bet',;
   bGlobalFont=.t.,;
    prg="gsve_bet('C')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 74622938


  add object ZoomMast as cp_szoombox with uid="VJRPDOWVIW",left=7, top=109, width=269,height=272,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSVE_KET",bOptions=.f.,bQueryOnLoad=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 112317670


  add object ZoomDet as cp_zoombox with uid="NEMWPYAYDC",left=275, top=109, width=525,height=272,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_DETT",cZoomFile="GSVEDKET",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,;
    cEvent = "EseDett",;
    nPag=1;
    , HelpContextID = 112317670

  add object oDESDOC_1_38 as StdField with uid="WWFKJYCWFG",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 196131018,;
   bGlobalFont=.t.,;
    Height=21, Width=257, Left=145, Top=35, InputMask=replicate('X',35)

  add object oStr_1_17 as StdString with uid="HHVIEMZZNP",Visible=.t., Left=521, Top=34,;
    Alignment=1, Width=61, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="NDGHFRBENN",Visible=.t., Left=674, Top=34,;
    Alignment=1, Width=45, Height=18,;
    Caption="a data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="REJRUOPSEG",Visible=.t., Left=8, Top=37,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="FENWOYSAUJ",Visible=.t., Left=12, Top=61,;
    Alignment=1, Width=72, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="OOAUBIWOYB",Visible=.t., Left=24, Top=11,;
    Alignment=1, Width=60, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_ket','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
