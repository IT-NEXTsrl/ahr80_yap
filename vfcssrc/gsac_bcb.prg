* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bcb                                                        *
*              Costruisce bidoni temporali                                     *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_43]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-19                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bcb",oParentObject)
return(i_retval)

define class tgsac_bcb as StdBatch
  * --- Local variables
  w_PPNMAXPE = 0
  nPerGio = 0
  nPerMes = 0
  nPerTri = 0
  nPerSet = 0
  w_PDPERASS = space(3)
  w_PDDATEVA = ctod("  /  /  ")
  w_gDatIni = ctod("  /  /  ")
  w_gDatFin = ctod("  /  /  ")
  w_sDatIni = ctod("  /  /  ")
  w_sDatFin = ctod("  /  /  ")
  w_mDatIni = ctod("  /  /  ")
  w_mDatFin = ctod("  /  /  ")
  w_tDatIni = ctod("  /  /  ")
  w_tDatFin = ctod("  /  /  ")
  TotPer = 0
  TempN = 0
  TmpC = space(10)
  DatProgr = ctod("  /  /  ")
  NumPer = 0
  TmpN1 = 0
  TmpC1 = space(3)
  TmpD1 = ctod("  /  /  ")
  cicla = .f.
  TmpN2 = 0
  TmpC2 = space(4)
  TmpD2 = ctod("  /  /  ")
  gDI = ctod("  /  /  ")
  gDF = ctod("  /  /  ")
  sDI = ctod("  /  /  ")
  sDF = ctod("  /  /  ")
  mDI = ctod("  /  /  ")
  mDF = ctod("  /  /  ")
  tDI = ctod("  /  /  ")
  tDF = ctod("  /  /  ")
  i = 0
  w_ORAELA = space(5)
  * --- WorkFile variables
  PAR_PROD_idx=0
  PDA_TPER_idx=0
  PDA_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Bidoni Temporali
    * --- Legge da tabella parametri
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPNUMGIO,PPNUMSET,PPNUMMES,PPNUMTRI,PPNMAXPE"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("AA");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPNUMGIO,PPNUMSET,PPNUMMES,PPNUMTRI,PPNMAXPE;
        from (i_cTable) where;
            PPCODICE = "AA";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.nPerGio = NVL(cp_ToDate(_read_.PPNUMGIO),cp_NullValue(_read_.PPNUMGIO))
      this.nPerSet = NVL(cp_ToDate(_read_.PPNUMSET),cp_NullValue(_read_.PPNUMSET))
      this.nPerMes = NVL(cp_ToDate(_read_.PPNUMMES),cp_NullValue(_read_.PPNUMMES))
      this.nPerTri = NVL(cp_ToDate(_read_.PPNUMTRI),cp_NullValue(_read_.PPNUMTRI))
      this.w_PPNMAXPE = NVL(cp_ToDate(_read_.PPNMAXPE),cp_NullValue(_read_.PPNMAXPE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PPNMAXPE = iif(this.w_PPNMAXPE=0,54,this.w_PPNMAXPE)
    * --- Variabili relativi ai bidoni temporali
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Crea Array con periodi da esaminare
    this.TotPer = 100
    dimension Periodi(this.TotPer)
    dimension Datini(this.TotPer)
    dimension Datfin(this.TotPer)
    this.gDI = this.w_gDatIni
    this.gDF = this.w_gDatFin
    this.sDI = this.w_sDatIni
    this.sDF = this.w_sDatFin
    this.mDI = this.w_mDatIni
    this.mDF = this.w_mDatFin
    this.tDI = this.w_tDatIni
    this.tDF = this.w_tDatFin
    * --- Giorni
    this.DatProgr = this.gDI
    this.TmpN1 = this.DatProgr - Date(year(this.DatProgr),1,1) + 1
    do while this.DatProgr <= this.gDF
      this.NumPer = this.NumPer +1
      Periodi(this.NumPer) = "G"+right("000"+alltrim(str(this.TmpN1,3,0)),3)
      Datini(this.NumPer) = this.DatProgr
      Datfin(this.NumPer) = this.DatProgr
      this.DatProgr = this.DatProgr +1
      this.TmpN1 = this.TmpN1+1
      if this.TmpN1 = 366+iif(day(cp_CharToDate("28-02-"+str(year(this.DatProgr)+1,4,0)))=29,1,0)
        this.TmpN1 = 1
      endif
    enddo
    * --- Settimane
    * --- La settimana inizia Luned� e contiene sette giorni
    this.TmpN1 = week(this.DatProgr, 2, 2)
    do while this.DatProgr <= this.sDF
      this.NumPer = this.NumPer +1
      Periodi(this.NumPer) = "S"+right(str(year(this.DatProgr),4,0),1) +right("0"+alltrim(str(this.TmpN1,2,0)),2)
      Datini(this.NumPer) = this.DatProgr
      Datfin(this.NumPer) = this.DatProgr+6
      this.DatProgr = this.DatProgr +7
      this.TmpN1 = this.TmpN1+1
      if this.TmpN1 > 53 or (this.TmpN1=53 and week(date(year(this.DatProgr),12,31),2,2)=52)
        this.TmpN1 = 1
      endif
    enddo
    * --- Mesi
    do while this.DatProgr <= this.mDF
      this.NumPer = this.NumPer +1
      Datini(this.NumPer) = this.DatProgr
      Periodi(this.NumPer) = "M"+right(str(year(this.DatProgr),4,0),1) + right("0"+alltrim(str(month(this.DatProgr),2,0)),2)
      if month(this.DatProgr)=12
        this.DatProgr = Date(year(this.DatProgr)+1, 1, 1)
      else
        this.DatProgr = Date(year(this.DatProgr), month(this.DatProgr)+1, 1)
      endif
      Datfin(this.NumPer) = this.DatProgr-1
    enddo
    * --- Trimestri
    do while this.DatProgr <= this.tDF
      this.NumPer = this.NumPer +1
      Datini(this.NumPer) = this.DatProgr
      Periodi(this.NumPer) = "T" + right(str(year(this.DatProgr),4,0),1) + "0" + str((month(this.DatProgr)+2)/3,1,0)
      if month(this.DatProgr)=10
        this.DatProgr = Date(year(this.DatProgr)+1, 1, 1)
      else
        this.DatProgr = Date(year(this.DatProgr), month(this.DatProgr)+3, 1)
      endif
      Datfin(this.NumPer) = this.DatProgr-1
    enddo
    if this.NumPer > this.w_PPNMAXPE
      this.TmpC = "Il numero di time buckets (%1) supera il limite consentito (%2)%0Impossibile generare il nuovo orizzonte temporale" 
      ah_ErrorMsg(this.TmpC,"STOP","", alltrim(str(this.NumPer,10,0)), alltrim(str(this.w_PPNMAXPE,10,0)) )
      i_retcode = 'stop'
      return
    else
      * --- Aggiorna Tabella Periodi
      this.TmpC1 = "000"
      this.TmpC2 = "SCAD"
      this.TmpD1 = i_INIDAT
      this.TmpD2 = cp_ToDate(Datini(1)) -1
      * --- begin transaction
      cp_BeginTrs()
      * --- Try
      local bErr_038487B8
      bErr_038487B8=bTrsErr
      this.Try_038487B8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into PDA_TPER
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PDA_TPER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PDA_TPER_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PDA_TPER_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TPPERREL ="+cp_NullLink(cp_ToStrODBC(this.TmpC2),'PDA_TPER','TPPERREL');
          +",TPDATINI ="+cp_NullLink(cp_ToStrODBC(this.TmpD1),'PDA_TPER','TPDATINI');
          +",TPDATFIN ="+cp_NullLink(cp_ToStrODBC(this.TmpD2),'PDA_TPER','TPDATFIN');
          +",TPPRILAV ="+cp_NullLink(cp_ToStrODBC(this.TmpD2),'PDA_TPER','TPPRILAV');
              +i_ccchkf ;
          +" where ";
              +"TPPERASS = "+cp_ToStrODBC(this.TmpC1);
                 )
        else
          update (i_cTable) set;
              TPPERREL = this.TmpC2;
              ,TPDATINI = this.TmpD1;
              ,TPDATFIN = this.TmpD2;
              ,TPPRILAV = this.TmpD2;
              &i_ccchkf. ;
           where;
              TPPERASS = this.TmpC1;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_038487B8
      * --- End
      this.i = 1
      do while this.i <= this.TotPer
        this.TmpC1 = right("000"+alltrim(Str(this.i,3,0)),3)
        if this.i <= this.NumPer
          this.TmpC2 = Periodi(this.i)
          this.TmpD1 = Datini(this.i)
          this.TmpD2 = Datfin(this.i)
        else
          this.TmpC2 = "XXXX"
          this.TmpD1 = cp_CharToDate("  -  -  ")
          this.TmpD2 = cp_CharToDate("  -  -  ")
        endif
        * --- Try
        local bErr_03A84A60
        bErr_03A84A60=bTrsErr
        this.Try_03A84A60()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into PDA_TPER
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PDA_TPER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PDA_TPER_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PDA_TPER_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TPPERREL ="+cp_NullLink(cp_ToStrODBC(this.TmpC2),'PDA_TPER','TPPERREL');
            +",TPDATINI ="+cp_NullLink(cp_ToStrODBC(this.TmpD1),'PDA_TPER','TPDATINI');
            +",TPDATFIN ="+cp_NullLink(cp_ToStrODBC(this.TmpD2),'PDA_TPER','TPDATFIN');
            +",TPPRILAV ="+cp_NullLink(cp_ToStrODBC(this.TmpD2),'PDA_TPER','TPPRILAV');
                +i_ccchkf ;
            +" where ";
                +"TPPERASS = "+cp_ToStrODBC(this.TmpC1);
                   )
          else
            update (i_cTable) set;
                TPPERREL = this.TmpC2;
                ,TPDATINI = this.TmpD1;
                ,TPDATFIN = this.TmpD2;
                ,TPPRILAV = this.TmpD2;
                &i_ccchkf. ;
             where;
                TPPERASS = this.TmpC1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_03A84A60
        * --- End
        this.i = this.i + 1
      enddo
      * --- Try
      local bErr_03A8A520
      bErr_03A8A520=bTrsErr
      this.Try_03A8A520()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.TmpC = "Impossibile memorizzare le date relative ai time buckets"
        ah_ErrorMsg(this.TmpC,"STOP","")
      endif
      bTrsErr=bTrsErr or bErr_03A8A520
      * --- End
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_038487B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PDA_TPER
    i_nConn=i_TableProp[this.PDA_TPER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PDA_TPER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PDA_TPER_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TPPERASS"+",TPPERREL"+",TPDATINI"+",TPDATFIN"+",TPPRILAV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.TmpC1),'PDA_TPER','TPPERASS');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpC2),'PDA_TPER','TPPERREL');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD1),'PDA_TPER','TPDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD2),'PDA_TPER','TPDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD2),'PDA_TPER','TPPRILAV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TPPERASS',this.TmpC1,'TPPERREL',this.TmpC2,'TPDATINI',this.TmpD1,'TPDATFIN',this.TmpD2,'TPPRILAV',this.TmpD2)
      insert into (i_cTable) (TPPERASS,TPPERREL,TPDATINI,TPDATFIN,TPPRILAV &i_ccchkf. );
         values (;
           this.TmpC1;
           ,this.TmpC2;
           ,this.TmpD1;
           ,this.TmpD2;
           ,this.TmpD2;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03A84A60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PDA_TPER
    i_nConn=i_TableProp[this.PDA_TPER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PDA_TPER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PDA_TPER_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TPPERASS"+",TPPERREL"+",TPDATINI"+",TPDATFIN"+",TPPRILAV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.TmpC1),'PDA_TPER','TPPERASS');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpC2),'PDA_TPER','TPPERREL');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD1),'PDA_TPER','TPDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD2),'PDA_TPER','TPDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD2),'PDA_TPER','TPPRILAV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TPPERASS',this.TmpC1,'TPPERREL',this.TmpC2,'TPDATINI',this.TmpD1,'TPDATFIN',this.TmpD2,'TPPRILAV',this.TmpD2)
      insert into (i_cTable) (TPPERASS,TPPERREL,TPDATINI,TPDATFIN,TPPRILAV &i_ccchkf. );
         values (;
           this.TmpC1;
           ,this.TmpC2;
           ,this.TmpD1;
           ,this.TmpD2;
           ,this.TmpD2;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03A8A520()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_ORAELA = left(time(),5)
    * --- Write into PAR_PROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPGDTINI ="+cp_NullLink(cp_ToStrODBC(this.w_gDatIni),'PAR_PROD','PPGDTINI');
      +",PPTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_tDatFin),'PAR_PROD','PPTDTFIN');
      +",PPELAVER ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PAR_PROD','PPELAVER');
      +",PPORAVER ="+cp_NullLink(cp_ToStrODBC(this.w_ORAELA),'PAR_PROD','PPORAVER');
      +",PPNUMPER ="+cp_NullLink(cp_ToStrODBC(this.NumPer),'PAR_PROD','PPNUMPER');
      +",PPOPEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_PROD','PPOPEELA');
          +i_ccchkf ;
      +" where ";
          +"PPCODICE = "+cp_ToStrODBC("AA");
             )
    else
      update (i_cTable) set;
          PPGDTINI = this.w_gDatIni;
          ,PPTDTFIN = this.w_tDatFin;
          ,PPELAVER = i_DATSYS;
          ,PPORAVER = this.w_ORAELA;
          ,PPNUMPER = this.NumPer;
          ,PPOPEELA = i_CODUTE;
          &i_ccchkf. ;
       where;
          PPCODICE = "AA";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.nPerGio*this.nPerSet*this.nPerMes*this.nPerTri=0
      this.TmpC = "Verificare parametri time buckets. impossibile creare orizzonte temporale"
      ah_ErrorMsg(this.TmpC,"STOP","")
      i_retcode = 'stop'
      return
    endif
    * --- Bidone periodi GIORNALIERI
    this.w_gDatIni = i_DATSYS
    this.w_gDatFin = this.w_gDatIni + this.nPerGio - 1
    do while dow(this.w_gDatFin)<>1
      this.w_gDatFin = this.w_gDatFin + 1
    enddo
    * --- Bidone periodi SETTIMANALI
    this.w_sDatIni = this.w_gDatFin +1
    this.w_sDatFin = this.w_sDatIni + this.nPerSet*7 -1
    * --- Verifica se e' l'ultimo giorno del mese
    if month(this.w_sDatFin) = month(this.w_sDatFin+1)
      * --- Cerca la prima domenica del mese successivo
      this.TempN = month(this.w_sDatFin) +1
      if this.TempN > 12
        this.TempN = this.TempN - 12
        this.w_sDatFin = cp_CharToDate("01-"+str(this.TempN,2,0)+"-"+str(year(this.w_sDatFin)+1,4,0))
      else
        this.w_sDatFin = cp_CharToDate("01-"+str(this.TempN,2,0)+"-"+str(year(this.w_sDatFin),4,0))
      endif
      * --- Cerca la prima domenica del  mese
      this.w_sDatFin = this.w_sDatFin -1
      do while dow(this.w_sDatFin)<>1
        this.w_sDatFin = this.w_sDatFin + 1
      enddo
    endif
    * --- Bidone periodi MENSILI
    this.w_mDatIni = this.w_sDatFin +1
    this.TempN = month(this.w_mDatIni) +this.nPerMes
    this.w_mDatFin = Date(year(this.w_mDatIni)+int((this.TempN-1)/12), mod(this.TempN-1,12)+1, 1) -1
    * --- E' l'ultimo mese del trimestre di appartenenza ?
    if mod(month(this.w_mDatFin),3) <>0
      this.TempN = int(month(this.w_mDatFin)/3)*3+4
      this.w_mDatFin = Date(year(this.w_mDatFin)+int(this.TempN/12)+iif(mod(this.TempN,12)=0,1,0), mod(this.TempN-1,12)+1, 1) -1
    endif
    * --- Bidone periodi TRIMESTRALI
    this.w_tDatIni = this.w_mDatFin +1
    this.TempN = month(this.w_tDatIni) + 3*this.nPerTri
    this.w_tDatFin = Date(year(this.w_tDatIni)+int(this.TempN/12)+iif(mod(this.TempN,12)=0,1,0), mod(this.TempN-1,12)+1, 1) -1
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna periodi nelle PDA
    do vq_exec with "query\Gsac_bcb",this,"Periodi"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- begin transaction
    cp_BeginTrs()
    * --- Se si dovesse avere un problema di porting su qualche database a causa della DISTINCT utilizzare una VisualQuery
    * --- Select from PDA_DETT
    i_nConn=i_TableProp[this.PDA_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2],.t.,this.PDA_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DISTINCT PDDATEVA  from "+i_cTable+" PDA_DETT ";
          +" where PDSTATUS<>'O'";
          +" order by PDDATEVA";
           ,"_Curs_PDA_DETT")
    else
      select DISTINCT PDDATEVA from (i_cTable);
       where PDSTATUS<>"O";
       order by PDDATEVA;
        into cursor _Curs_PDA_DETT
    endif
    if used('_Curs_PDA_DETT')
      select _Curs_PDA_DETT
      locate for 1=1
      do while not(eof())
      this.w_PDDATEVA = _Curs_PDA_DETT.PDDATEVA
      if not empty(nvl(this.w_PDDATEVA,{}))
        Select Periodi
        Locate for tpdatini<=this.w_PDDATEVA and this.w_PDDATEVA<=tpdatfin REST
        if found()
          this.w_PDPERASS = tpperass
        else
          this.w_PDPERASS = "   "
          GO TOP
        endif
        * --- Write into PDA_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PDA_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PDA_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PDPERASS ="+cp_NullLink(cp_ToStrODBC(this.w_PDPERASS),'PDA_DETT','PDPERASS');
              +i_ccchkf ;
          +" where ";
              +"PDDATEVA = "+cp_ToStrODBC(this.w_PDDATEVA);
                 )
        else
          update (i_cTable) set;
              PDPERASS = this.w_PDPERASS;
              &i_ccchkf. ;
           where;
              PDDATEVA = this.w_PDDATEVA;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_PDA_DETT
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    if used("Periodi")
      use in Periodi
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='PDA_TPER'
    this.cWorkTables[3]='PDA_DETT'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PDA_DETT')
      use in _Curs_PDA_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
