* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bcc                                                        *
*              Controlli reg.IVA causali                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_11]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-17                                                      *
* Last revis.: 2014-01-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bcc",oParentObject,m.pTipo)
return(i_retval)

define class tgscg_bcc as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_OK = .f.
  w_MESS = space(10)
  w_CONTA = 0
  w_REGMAR = space(1)
  w_CODAZI = space(5)
  w_CODCAU = space(5)
  * --- WorkFile variables
  ATTIDETT_idx=0
  CONTROPA_idx=0
  CAU_DIST_idx=0
  CAU_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica dati Reg.IVA al Variare del Tipo Registro IVA (da GSCG_ACC)
    do case
      case this.pTipo="D"
        * --- CCTIPDOC changed
        if this.oParentObject.w_CCTIPDOC="FC" AND this.oParentObject.w_CCTIPREG $ "VAE" or !( this.oParentObject.w_CCTIPREG= "A" or (this.oParentObject.w_CCTIPREG<>"A" and Not(this.oParentObject.w_CCTIPDOC $ "AU-NU")))
          ah_ErrorMsg("Attenzione, tipo documento incongruente con il tipo registro IVA",,"")
          this.oParentObject.w_CCTIPDOC = this.oParentObject.w_OLDTIPDOC
        else
          do case
            case ((NOT this.oParentObject.w_CCTIPDOC $ "NC-NE-NU") AND this.oParentObject.w_CCTIPREG $ "VCE") OR (this.oParentObject.w_CCTIPDOC $ "NC-NE-NU" AND this.oParentObject.w_CCTIPREG="A")
              this.oParentObject.w_CCCFDAVE =  "D"
            case this.oParentObject.w_CCTIPDOC $ "CO-NO"
              this.oParentObject.w_CCCALDOC = "N"
            case .T.
              this.oParentObject.w_CCCFDAVE = IIF(this.oParentObject.w_CCTIPREG="N", " ", "A")
          endcase
        endif
      case this.pTipo="T"
        this.oParentObject.w_CCNUMREG = 1
        this.oParentObject.w_CCTIPDOC = IIF(this.oParentObject.w_CCTIPREG $ "VA", "FA", "CO")
        this.oParentObject.w_CCCALDOC = "N"
        this.oParentObject.w_CCCFDAVE = IIF(this.oParentObject.w_CCTIPREG="A", "A", "D")
        this.oParentObject.w_CCFLAUTR = " "
        do case
          case this.oParentObject.w_CCTIPREG="A"
            this.oParentObject.w_CCFLPDOC = "N"
            this.oParentObject.w_CCFLPPRO = "D"
          case this.oParentObject.w_CCTIPREG="V"
            this.oParentObject.w_CCFLPDOC = "D"
            this.oParentObject.w_CCFLPPRO = "N"
          case this.oParentObject.w_CCTIPREG="N"
            this.oParentObject.w_CCNUMREG = 0
            this.oParentObject.w_CCTIPDOC = "NO"
            this.oParentObject.w_CCCALDOC = "N"
            this.oParentObject.w_CCCFDAVE = " "
        endcase
      case this.pTipo="C"
        this.w_OK = .T.
        this.oParentObject.w_UNIREG = "S"
        if this.oParentObject.w_CCTIPREG<>"N"
          * --- Controllo che non sia presente lo stesso Registro Iva in pi� attivit�
          this.w_CONTA = 0
          * --- Select from ATTIDETT
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ATCODATT  from "+i_cTable+" ATTIDETT ";
                +" where ATTIPREG="+cp_ToStrODBC(this.oParentObject.w_CCTIPREG)+" AND ATNUMREG="+cp_ToStrODBC(this.oParentObject.w_CCNUMREG)+"";
                 ,"_Curs_ATTIDETT")
          else
            select ATCODATT from (i_cTable);
             where ATTIPREG=this.oParentObject.w_CCTIPREG AND ATNUMREG=this.oParentObject.w_CCNUMREG;
              into cursor _Curs_ATTIDETT
          endif
          if used('_Curs_ATTIDETT')
            select _Curs_ATTIDETT
            locate for 1=1
            do while not(eof())
            this.w_CONTA = this.w_CONTA+1
              select _Curs_ATTIDETT
              continue
            enddo
            use
          endif
          if this.w_CONTA>1
            * --- Esistono piu' Attivita' associate allo stesso registro; Verificare Attivit� ! 
            this.oParentObject.w_UNIREG = "M"
            this.w_OK = .F.
          endif
          if this.w_CONTA<1
            * --- Numero Registro non associato a nessuna Attivit� ! 
            this.oParentObject.w_UNIREG = "N"
            this.w_OK = .F.
          endif
          if this.w_OK=.F.
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
        endif
      case this.pTipo="E"
        * --- Verifico se la casuale � utilizzata nelle contropartite vendite e acquisto
        * --- Read from CONTROPA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTROPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "COCODAZI"+;
            " from "+i_cTable+" CONTROPA where ";
                +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and COCAUACO = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            COCODAZI;
            from (i_cTable) where;
                COCODAZI = i_CODAZI;
                and COCAUACO = this.oParentObject.w_CCCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODAZI = NVL(cp_ToDate(_read_.COCODAZI),cp_NullValue(_read_.COCODAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_ROWS<>0
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg="Causale contabile utilizzata nelle contropartite acquisti"
        else
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COCODAZI"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                  +" and COCAUVEO = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COCODAZI;
              from (i_cTable) where;
                  COCODAZI = i_CODAZI;
                  and COCAUVEO = this.oParentObject.w_CCCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODAZI = NVL(cp_ToDate(_read_.COCODAZI),cp_NullValue(_read_.COCODAZI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS<>0
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg="Causale contabile utilizzata nelle contropartite vendite"
          else
            if i_ROWS<>0
              * --- Read from CAU_DIST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAU_DIST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAU_DIST_idx,2],.t.,this.CAU_DIST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CACODCAU"+;
                  " from "+i_cTable+" CAU_DIST where ";
                      +"CACODCAU = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CACODCAU;
                  from (i_cTable) where;
                      CACODCAU = this.oParentObject.w_CCCODICE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODCAU = NVL(cp_ToDate(_read_.CACODCAU),cp_NullValue(_read_.CACODCAU))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg="Causale contabile utilizzata nelle causali distinte"
            endif
          endif
        endif
      case this.pTipo="M"
        * --- Verifico che il registro IVA abbia attivo il check sul regime del margine
        this.w_REGMAR = "N"
        * --- Read from ATTIDETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATMARREG"+;
            " from "+i_cTable+" ATTIDETT where ";
                +"ATNUMREG = "+cp_ToStrODBC(this.oParentObject.w_CCNUMREG);
                +" and ATTIPREG = "+cp_ToStrODBC(this.oParentObject.w_CCTIPREG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATMARREG;
            from (i_cTable) where;
                ATNUMREG = this.oParentObject.w_CCNUMREG;
                and ATTIPREG = this.oParentObject.w_CCTIPREG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_REGMAR = NVL(cp_ToDate(_read_.ATMARREG),cp_NullValue(_read_.ATMARREG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.w_CCREGMAR="S"
          if this.w_REGMAR # "S" 
            ah_ErrorMsg("Selezione non attivabile per il registro IVA utilizzato.%0Il registro IVA non ha il flag regime del margine attivo nell'attivit�.",,"")
            this.oParentObject.w_CCREGMAR = "N"
          endif
        else
          if this.w_REGMAR = "S"
            ah_ErrorMsg("Il registro IVA  ha il flag regime del margine attivo nell'attivit�, verr� attivato il corrispondente check nella causale",,"")
            this.oParentObject.w_CCREGMAR = "S"
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='CAU_DIST'
    this.cWorkTables[4]='CAU_CONT'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
