* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_svt                                                        *
*              Quadro vt                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-24                                                      *
* Last revis.: 2008-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_svt",oParentObject))

* --- Class definition
define class tgscg_svt as StdForm
  Top    = 9
  Left   = 13

  * --- Standard Properties
  Width  = 612
  Height = 422
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-08"
  HelpContextID=40510825
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_svt"
  cComment = "Quadro vt"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SERIAL = space(10)
  o_SERIAL = space(10)
  w_VT1IMPON = 0
  w_VT1IMPIVA = 0
  w_VT2IMPON = 0
  w_VT2IMPIVA = 0
  w_VTIMPON = 0
  w_VTIMPIVA = 0
  w_INIANNO = ctod('  /  /  ')
  w_FINANNO = ctod('  /  /  ')
  w_ANNDIC = space(4)

  * --- Children pointers
  GSCG_MVT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSCG_MVT additive
    with this
      .Pages(1).addobject("oPag","tgscg_svtPag1","gscg_svt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSCG_MVT
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSCG_MVT = CREATEOBJECT('stdDynamicChild',this,'GSCG_MVT',this.oPgFrm.Page1.oPag.oLinkPC_1_3)
    this.GSCG_MVT.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MVT)
      this.GSCG_MVT.DestroyChildrenChain()
      this.GSCG_MVT=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MVT.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MVT.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MVT.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_MVT.SetKey(;
            .w_SERIAL,"VTSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_MVT.ChangeRow(this.cRowID+'      1',1;
             ,.w_SERIAL,"VTSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_MVT)
        i_f=.GSCG_MVT.BuildFilter()
        if !(i_f==.GSCG_MVT.cQueryFilter)
          i_fnidx=.GSCG_MVT.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MVT.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MVT.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MVT.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MVT.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSCG_MVT(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSCG_MVT.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSCG_MVT(i_ask)
    if this.GSCG_MVT.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP))
      cp_BeginTrs()
      this.GSCG_MVT.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SERIAL=space(10)
      .w_VT1IMPON=0
      .w_VT1IMPIVA=0
      .w_VT2IMPON=0
      .w_VT2IMPIVA=0
      .w_VTIMPON=0
      .w_VTIMPIVA=0
      .w_INIANNO=ctod("  /  /  ")
      .w_FINANNO=ctod("  /  /  ")
      .w_ANNDIC=space(4)
      .w_INIANNO=oParentObject.w_INIANNO
      .w_FINANNO=oParentObject.w_FINANNO
      .w_ANNDIC=oParentObject.w_ANNDIC
      .GSCG_MVT.NewDocument()
      .GSCG_MVT.ChangeRow('1',1,.w_SERIAL,"VTSERIAL")
      if not(.GSCG_MVT.bLoaded)
        .GSCG_MVT.SetKey(.w_SERIAL,"VTSERIAL")
      endif
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
    this.DoRTCalc(1,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSCG_MVT.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MVT.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_INIANNO=.w_INIANNO
      .oParentObject.w_FINANNO=.w_FINANNO
      .oParentObject.w_ANNDIC=.w_ANNDIC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_SERIAL<>.o_SERIAL
          .Save_GSCG_MVT(.t.)
          .GSCG_MVT.NewDocument()
          .GSCG_MVT.ChangeRow('1',1,.w_SERIAL,"VTSERIAL")
          if not(.GSCG_MVT.bLoaded)
            .GSCG_MVT.SetKey(.w_SERIAL,"VTSERIAL")
          endif
        endif
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVT1IMPON_1_4.value==this.w_VT1IMPON)
      this.oPgFrm.Page1.oPag.oVT1IMPON_1_4.value=this.w_VT1IMPON
    endif
    if not(this.oPgFrm.Page1.oPag.oVT1IMPIVA_1_5.value==this.w_VT1IMPIVA)
      this.oPgFrm.Page1.oPag.oVT1IMPIVA_1_5.value=this.w_VT1IMPIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oVT2IMPON_1_6.value==this.w_VT2IMPON)
      this.oPgFrm.Page1.oPag.oVT2IMPON_1_6.value=this.w_VT2IMPON
    endif
    if not(this.oPgFrm.Page1.oPag.oVT2IMPIVA_1_7.value==this.w_VT2IMPIVA)
      this.oPgFrm.Page1.oPag.oVT2IMPIVA_1_7.value=this.w_VT2IMPIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oVTIMPON_1_8.value==this.w_VTIMPON)
      this.oPgFrm.Page1.oPag.oVTIMPON_1_8.value=this.w_VTIMPON
    endif
    if not(this.oPgFrm.Page1.oPag.oVTIMPIVA_1_9.value==this.w_VTIMPIVA)
      this.oPgFrm.Page1.oPag.oVTIMPIVA_1_9.value=this.w_VTIMPIVA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSCG_MVT.CheckForm()
      if i_bres
        i_bres=  .GSCG_MVT.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SERIAL = this.w_SERIAL
    * --- GSCG_MVT : Depends On
    this.GSCG_MVT.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_svtPag1 as StdContainer
  Width  = 608
  height = 422
  stdWidth  = 608
  stdheight = 422
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="SAVQGQIKMH",left=551, top=372, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "premere per iniziare l'elaborazione";
    , HelpContextID = 40482074;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oLinkPC_1_3 as stdDynamicChildContainer with uid="TFNQETKFNS",left=6, top=125, width=595, height=244, bOnScreen=.t.;


  add object oVT1IMPON_1_4 as StdField with uid="OAGXUANIMT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_VT1IMPON", cQueryName = "VT1IMPON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imponibile verso consumatori finali",;
    HelpContextID = 45237412,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=311, Top=55, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oVT1IMPIVA_1_5 as StdField with uid="IMTVRZRKCO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VT1IMPIVA", cQueryName = "VT1IMPIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposte verso consumatori finali",;
    HelpContextID = 223196996,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=457, Top=55, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oVT2IMPON_1_6 as StdField with uid="XHJVACFQGV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_VT2IMPON", cQueryName = "VT2IMPON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imponibile verso soggetti IVA",;
    HelpContextID = 45241508,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=311, Top=80, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oVT2IMPIVA_1_7 as StdField with uid="HLEYSPSUZQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_VT2IMPIVA", cQueryName = "VT2IMPIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposte verso soggetti IVA",;
    HelpContextID = 223192900,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=457, Top=80, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oVTIMPON_1_8 as StdField with uid="PHEDZMKVOL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_VTIMPON", cQueryName = "VTIMPON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imponibile",;
    HelpContextID = 236469162,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=311, Top=30, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oVTIMPIVA_1_9 as StdField with uid="KWPFJRMBUY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_VTIMPIVA", cQueryName = "VTIMPIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposte operazioni imponibili",;
    HelpContextID = 199738519,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=457, Top=30, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"


  add object oBtn_1_13 as StdButton with uid="MRLTQOJJBR",left=501, top=372, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "premere per eseguire stampa";
    , HelpContextID = 101278758;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        gscg_bvt(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_16 as cp_runprogram with uid="WQCXLDGFYC",left=3, top=491, width=170,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gscg_bvt('I')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 137486822

  add object oStr_1_10 as StdString with uid="NFAODBLUXT",Visible=.t., Left=13, Top=33,;
    Alignment=1, Width=295, Height=18,;
    Caption="Totale operazioni imponibili:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="DQRFHDMVGK",Visible=.t., Left=7, Top=59,;
    Alignment=1, Width=301, Height=18,;
    Caption="Operazioni imponibili verso consumatori finali:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="SAZGNFKOMU",Visible=.t., Left=5, Top=84,;
    Alignment=1, Width=303, Height=18,;
    Caption="Operazioni imponibili verso soggetti IVA:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="BLJZOFIKHC",Visible=.t., Left=312, Top=9,;
    Alignment=2, Width=138, Height=18,;
    Caption="Imponibile"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="RRASMYBDJE",Visible=.t., Left=458, Top=9,;
    Alignment=2, Width=138, Height=18,;
    Caption="Imposta"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_svt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
