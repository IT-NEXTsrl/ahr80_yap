* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bsp                                                        *
*              Batch stampa piano conti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2008-02-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bsp",oParentObject)
return(i_retval)

define class tgsar_bsp as StdBatch
  * --- Local variables
  w_BANCHE = space(1)
  w_TIPO = space(1)
  w_APO = space(10)
  w_ECO = 0
  w_DATSTAQ = ctod("  /  /  ")
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch Stampa Piano Dei Conti (da GSAR_SMC)
    * --- Nel caso di Mastri Significativi non vengono stampati ne i clienti ne i fornitori
    * --- Variabili passate dalla maschera
    * ---  possibili valori 'tutti', 'patrim',econ','banche' 'analit'
    * --- PARAMETRO PASSATO ALLE QUERY
    this.w_DATSTAQ = this.oParentObject.w_DATSTA
    do case
      case this.oParentObject.w_SCELTA="banche"
        this.w_BANCHE = "S"
        * --- P aggiunto per stampare anche i mastri senza conti figli
        this.w_TIPO = "BP"
        this.w_ECO = 0
        if this.oParentObject.w_OBSODAT1="N"
          * --- Tutti meno gli obsoleti
          vq_exec("query\gsar3qpc.vqr",this,"usrtmp")
        else
          * --- Solo Obsoleti
          vq_exec("query\gsar4qpc.vqr",this,"usrtmp")
        endif
      case this.oParentObject.w_SCELTA="econ" AND this.oParentObject.w_Analit="S"
        this.w_ECO = 1
        this.w_BANCHE = "N"
        * --- P aggiunto per stampare anche i mastri senza conti figli
        this.w_TIPO = "XMICBAVP"
        * --- lego i conti con i dati analitici
        if this.oParentObject.w_OBSODAT1="N"
          * --- Tutti meno gli obsoleti
          vq_exec("query\gsar_qpc.vqr",this,"usrtmp")
        else
          * --- Solo Obsoleti
          vq_exec("query\gsar2qpc.vqr",this,"usrtmp")
        endif
      otherwise
        this.w_ECO = 0
        this.w_BANCHE = "N"
        * --- P aggiunto per stampare anche i mastri senza conti figli
        this.w_TIPO = "XMICBAVP"
        if this.oParentObject.w_OBSODAT1="N"
          * --- Tutti meno gli obsoleti
          vq_exec("query\gsar5qpc.vqr",this,"usrtmp")
        else
          * --- Solo Obsoleti
          vq_exec("query\gsar6qpc.vqr",this,"usrtmp")
        endif
    endcase
    * --- w_apo mi serve per stabilire quali conti stampare in base a w_scelta
    this.w_APO = "APOCRT"
    do case
      case this.oParentObject.w_SCELTA="patrim"
        this.w_APO = "AP"
      case this.oParentObject.w_SCELTA="econ"
        this.w_APO = "CR"
    endcase
    * --- variabili del report
    L_datasta1=this.w_DATSTAQ
    l_SCELTA=this.oParentObject.w_SCELTA
    l_obsodat1=this.oParentObject.w_obsodat1
    l_mastsign=this.oParentObject.w_mastsign
    * --- costruisco l'albero (ATTENZIONE: in Revolution il numero livelli Mastri e' al massimo 4 + il Conto)
    SELECT usrtmp.*,usrtmp_a.*,usrtmp_b.*,usrtmp_c.*,usrtmp_d.*;
    FROM usrtmp LEFT OUTER JOIN usrtmp usrtmp_a;
     LEFT OUTER JOIN usrtmp usrtmp_b;
     LEFT OUTER JOIN usrtmp usrtmp_c;
     LEFT OUTER JOIN usrtmp usrtmp_d ;
    ON usrtmp_c.mccodice = usrtmp_d.mcconsup and usrtmp_c.mcnumliv<>0 ;
    ON usrtmp_b.mccodice = usrtmp_c.mcconsup and (usrtmp_b.mcnumliv<>0);
    ON usrtmp_a.mccodice = usrtmp_b.mcconsup and (usrtmp_a.mcnumliv<>0);
    ON usrtmp.mccodice = usrtmp_a.mcconsup and (usrtmp.mcnumliv<>0);
    WHERE (not empty(iif(usrtmp_b.mcsezbil="Z",usrtmp_b.mccodice,iif(usrtmp_c.mcsezbil="Z",usrtmp_c.mccodice,;
    iif(usrtmp_d.mcsezbil="Z",usrtmp_d.mccodice,"")))) or this.oParentObject.w_mastsign<>"S");
    and empty(nvl(usrtmp.mcconsup,"")) and (usrtmp.mcsezbil $ this.w_apo) AND;
    (usrtmp.tip $ this.w_tipo or usrtmp_a.tip $ this.w_tipo or usrtmp_b.tip $ this.w_tipo or usrtmp_c.tip $ this.w_tipo ;
    or usrtmp_d.tip $ this.w_tipo ) and ;
    (usrtmp.TEST=this.w_eco or usrtmp_a.TEST=this.w_eco or usrtmp_b.TEST=this.w_eco or usrtmp_c.TEST=this.w_eco ;
    or usrtmp_d.TEST=this.w_eco );
    ORDER BY usrtmp.mcseqsta DESC ,usrtmp.mccodice,usrtmp_a.mcseqsta DESC ,usrtmp_a.mccodice,usrtmp_b.mcseqsta DESC ,;
    usrtmp_b.mccodice,usrtmp_c.mcseqsta DESC ,usrtmp_c.mccodice,usrtmp_D.mcseqsta DESC,usrtmp_d.mccodice INTO cursor __TMP__
    * --- Lancio la Stampa - Verticale
    if this.oParentObject.w_ColCronol = "S"
      CP_CHPRN ("QUERY\GSAR_SPC.FRX", " ", this)
    else
      CP_CHPRN ("QUERY\GSAR_MPC.FRX", " ", this)
    endif
    * --- Rimuovo i cursori
    if  used("UsrTmp")
      select UsrTmp
      use
    endif
    if  used("__TMP__")
      select __TMP__
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
