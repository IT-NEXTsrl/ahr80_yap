* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bc1                                                        *
*              Check analitica da p.nota                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_14]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2000-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bc1",oParentObject)
return(i_retval)

define class tgscg_bc1 as StdBatch
  * --- Local variables
  w_ROWORD = 0
  w_TOTCOS = 0
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_CCTAGG = space(1)
  w_TOTPNT = 0
  w_TIPCON = space(1)
  w_SEZBIL = space(1)
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla alla conferma la correttezza dei dati  (da GSCG_MCA)
    this.oParentObject.w_CHECKRES = .T.
    this.w_oMess=createobject("Ah_Message")
    * --- Legge Importo sulla riga Temporaneo Primanota
    L_OLDA = SELECT()
    SELECT (this.oParentObject.oParentObject.cTrsName)
    L_RECNO = RECNO()
    GO TOP
    SCAN FOR (CPROWNUM= this.oParentObject.oParentObject.w_CPROWNUM )
    this.w_CCTAGG = t_CCTAGG
    this.w_ROWORD = CPROWNUM
    this.w_IMPDAR = t_PNIMPDAR
    this.w_IMPAVE = t_PNIMPAVE
    this.w_TOTPNT = this.w_IMPDAR - this.w_IMPAVE
    this.w_TIPCON = t_PNTIPCON
    this.w_SEZBIL = t_SEZB
    * --- Test Centri di Costo
    if this.w_TIPCON="G" AND this.w_SEZBIL $ "CR" AND g_PERCCR="S" AND this.w_CCTAGG $ "AM"
      * --- Legge Centri di Costo Associati
      this.w_TOTCOS = this.oParentObject.w_TOTRIG
      if this.w_TOTPNT <> this.w_TOTCOS
        if this.w_TOTCOS=0
          if NOT( this.oParentObject.oParentObject.cFunction="Load" AND this.w_CCTAGG="A")
            this.w_oMess.AddMsgPartNL("Importi di analitica inesistenti")     
            * --- La check form restituisce false
            this.oParentObject.w_CHECKRES = .F.
            EXIT
          endif
        else
          this.w_oMess.AddMsgPartNL("Importi di analitica incongruenti con la primanota")     
          this.w_oPart = this.w_oMess.AddMsgPartNL("(P.N.: %1 - C/C: %2)")
          this.w_oPart.AddParam(ALLTRIM(STR(this.w_TOTPNT)))     
          this.w_oPart.AddParam(ALLTRIM(STR(this.w_TOTCOS)))     
          * --- La check form restituisce false
          this.oParentObject.w_CHECKRES = .F.
          EXIT
        endif
      endif
    endif
    ENDSCAN
    SELECT (this.oParentObject.oParentObject.cTrsName)
    GOTO L_RECNO
    SELECT(L_OLDA)
    if this.oParentObject.w_CHECKRES=.F.
      this.w_oMess.AddMsgPartNL("Confermo ugualmente?")     
      this.w_oMess.AddMsgPartNL("(I valori andranno comunque resi congruenti prima della conferma della registrazione contabile)")     
      this.oParentObject.w_CHECKRES = this.w_oMess.ah_YesNo()
    endif
    this.bUpdateParentObject = .F.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
