* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kic                                                        *
*              Immagini catalogo                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-23                                                      *
* Last revis.: 2007-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kic",oParentObject))

* --- Class definition
define class tgsma_kic as StdForm
  Top    = 13
  Left   = 35

  * --- Standard Properties
  Width  = 579
  Height = 340
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-16"
  HelpContextID=267025257
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsma_kic"
  cComment = "Immagini catalogo"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CATEGORIA = space(1)
  w_ATTIVA = space(10)
  w_CODICE = space(10)
  w_DESCRI = space(35)
  w_ZOOMIC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kicPag1","gsma_kic",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCATEGORIA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMIC = this.oPgFrm.Pages(1).oPag.ZOOMIC
    DoDefault()
    proc Destroy()
      this.w_ZOOMIC = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CATEGORIA=space(1)
      .w_ATTIVA=space(10)
      .w_CODICE=space(10)
      .w_DESCRI=space(35)
        .w_CATEGORIA = 'G'
      .oPgFrm.Page1.oPag.ZOOMIC.Calculate()
        .w_ATTIVA = .w_ZOOMIC.GetVar("DESCRI")
        .w_CODICE = .w_ZOOMIC.GetVar("CODICE")
        .w_DESCRI = .w_ZOOMIC.GetVar("DESCRI")
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMIC.Calculate()
        .DoRTCalc(1,1,.t.)
            .w_ATTIVA = .w_ZOOMIC.GetVar("DESCRI")
            .w_CODICE = .w_ZOOMIC.GetVar("CODICE")
            .w_DESCRI = .w_ZOOMIC.GetVar("DESCRI")
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMIC.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMIC.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCATEGORIA_1_1.RadioValue()==this.w_CATEGORIA)
      this.oPgFrm.Page1.oPag.oCATEGORIA_1_1.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_kicPag1 as StdContainer
  Width  = 575
  height = 340
  stdWidth  = 575
  stdheight = 340
  resizeXpos=405
  resizeYpos=171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCATEGORIA_1_1 as StdCombo with uid="MJYVAPECUX",rtseq=1,rtrep=.f.,left=131,top=6,width=205,height=21;
    , ToolTipText = "Selezione categoria del catalogo";
    , HelpContextID = 63966847;
    , cFormVar="w_CATEGORIA",RowSource=""+"Gruppi merceologici,"+"Famiglie,"+"Marchi,"+"Categorie omogenee", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATEGORIA_1_1.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'F',;
    iif(this.value =3,'M',;
    iif(this.value =4,'O',;
    space(1))))))
  endfunc
  func oCATEGORIA_1_1.GetRadio()
    this.Parent.oContained.w_CATEGORIA = this.RadioValue()
    return .t.
  endfunc

  func oCATEGORIA_1_1.SetRadio()
    this.Parent.oContained.w_CATEGORIA=trim(this.Parent.oContained.w_CATEGORIA)
    this.value = ;
      iif(this.Parent.oContained.w_CATEGORIA=='G',1,;
      iif(this.Parent.oContained.w_CATEGORIA=='F',2,;
      iif(this.Parent.oContained.w_CATEGORIA=='M',3,;
      iif(this.Parent.oContained.w_CATEGORIA=='O',4,;
      0))))
  endfunc


  add object ZOOMIC as cp_zoombox with uid="LEJPSFLXJJ",left=5, top=33, width=482,height=302,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="gsma_kic",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="MARCHI",bQueryOnDblClick=.f.,;
    cEvent = "Interroga,Blank,w_CATEGORIA Changed",;
    nPag=1;
    , HelpContextID = 179407846


  add object oBtn_1_3 as StdButton with uid="GJZUUMSDLJ",left=505, top=235, width=48,height=45,;
    CpPicture="BMP\MOSTRA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il file selezionato";
    , HelpContextID = 240195178;
    , caption='\<Visualiz.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        gsma_b1c(this.Parent.oContained,.w_CODICE,"GSMA_KIC","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ATTIVA<>space(10))
      endwith
    endif
  endfunc


  add object oObj_1_7 as cp_runprogram with uid="TTHLCQMMCP",left=3, top=351, width=235,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsma_bic",;
    cEvent = "Blank, w_CATEGORIA Changed",;
    nPag=1;
    , HelpContextID = 179407846


  add object oBtn_1_8 as StdButton with uid="TSEJOICTJS",left=505, top=285, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 259707834;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_9 as StdString with uid="EZEXLMURWK",Visible=.t., Left=6, Top=8,;
    Alignment=1, Width=119, Height=18,;
    Caption="Categoria catalogo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kic','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
