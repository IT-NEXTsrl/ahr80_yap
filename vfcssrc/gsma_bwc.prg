* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bwc                                                        *
*              Warning causali maga                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-08                                                      *
* Last revis.: 2000-09-08                                                      *
*                                                                              *
* Segnala che i mov. non verrano considerati nel libro giornale e stampa schede*
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bwc",oParentObject)
return(i_retval)

define class tgsma_bwc as StdBatch
  * --- Local variables
  w_MESS = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Mostra a video warning sulle causali di magazzino (chiamato da GSMA_ACM)
    if this.oParentObject.w_CMFLAVAL="N" and this.oParentObject.w_CMFLCASC $"+|-"
      this.w_MESS = "Il movimento non verr� preso in considerazione%0nelle stampe del libro giornale e delle schede di magazzino"
      ah_ErrorMsg(this.w_MESS,"!")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
