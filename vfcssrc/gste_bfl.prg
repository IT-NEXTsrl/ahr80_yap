* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bfl                                                        *
*              Elaborazione cash flow                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_410]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-03                                                      *
* Last revis.: 2014-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bfl",oParentObject,m.pOper)
return(i_retval)

define class tgste_bfl as StdBatch
  * --- Local variables
  pOper = space(1)
  w_FILCER = space(10)
  w_FILEFF = space(10)
  w_FILATT = space(10)
  w_ZOOM = space(10)
  w_CODRAG = space(8)
  w_TIPRAG = space(1)
  w_FLAGIO = space(1)
  w_CODESE = space(4)
  w_CODES1 = space(4)
  w_VALPRE = space(4)
  w_CODAZI = space(4)
  w_ESEPRE = space(4)
  w_FINPRE = ctod("  /  /  ")
  w_COUNTER = 0
  w_DATVAL = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODBAN = space(15)
  w_CODPAG = space(10)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_DESRAG = space(50)
  w_CAOAPE = 0
  w_DATAPE = ctod("  /  /  ")
  w_DESRIG = space(50)
  w_KPAG = space(10)
  w_KBAN = space(15)
  w_KPUNT = 0
  w_KFOUND = .f.
  w_KPART = space(10)
  w_KSALS = 0
  w_KSALC = 0
  w_KCLAD = space(2)
  w_KSEGNO = space(1)
  w_KCODRAG = space(8)
  w_KTIPRAG = space(1)
  w_KDATVAL = ctod("  /  /  ")
  w_KNUMDOC = 0
  w_KALFDOC = space(10)
  w_KDATDOC = ctod("  /  /  ")
  w_KTIPCON = space(1)
  w_KCODCON = space(15)
  w_KCODBAN = space(15)
  w_KCODPAG = space(10)
  w_KCAOVAL = 0
  w_KIMPDAR = 0
  w_KIMPAVE = 0
  w_KDESRAG = space(50)
  w_KDESRIG = space(50)
  * --- WorkFile variables
  TMPFL1_idx=0
  TMPFL2_idx=0
  COD_TRIB_idx=0
  TMPFL3_idx=0
  ESERCIZI_idx=0
  TMPCASHATT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora il cash flow (da GSTE_KCF)
    * --- Parametro: G = Grafico ; S = Stampa
    * --- Legge i Dati di Dettaglio del Cash flow 
    *     CASH FLOW CERTO: GSTE_BFL.VQR
    *     CASH FLOW EFFETTIVO: Elaborazione pag.2
    *     CASH FLOW ATTESO: GSTE2BFL.VQR
    * --- Verifica i flag del campo Tipo che permetteranno di richiamare le rispettive query:gste_bfl per Certo; Elaborazione pag.2 per Effettivo;gste2bfl per Atteso
    this.w_FILCER = IIF(this.oParentObject.w_TIPCER="S", SPACE(10), "xxxzzzkkkw")
    this.w_FILEFF = IIF(this.oParentObject.w_TIPEFF="S", SPACE(10), "xxxzzzkkkw")
    this.w_FILATT = IIF(this.oParentObject.w_TIPATT="S", SPACE(10), "xxxzzzkkkw")
    * --- Inizializza tabella di Appoggio delle Banche Selezionate per fare poi i filtri sulle banche nelle tabelle successive
    this.w_ZOOM = this.oParentObject.w_Zoomcf
    Select Bacodban as Codban,Ordine as Ordine from (this.w_Zoom.cCursor) where xchk=1 And Not Empty(Nvl(Bacodban," ")) into cursor Filbanca
    if Reccount("Filbanca")>0
      CURTOTAB("Filbanca" ,"TMPFL2")
      * --- La procedura verifica se esiste un movimento di apertura per l'esercizio
      *     corrente
      this.w_FLAGIO = "A"
      this.w_CODESE = g_CODESE
      this.w_COUNTER = 0
      * --- Select from Gste10bfl
      do vq_exec with 'Gste10bfl',this,'_Curs_Gste10bfl','',.f.,.t.
      if used('_Curs_Gste10bfl')
        select _Curs_Gste10bfl
        locate for 1=1
        do while not(eof())
        this.w_COUNTER = Nvl ( _Curs_Gste10bfl.Conta,0)
          select _Curs_Gste10bfl
          continue
        enddo
        use
      endif
      * --- Se non esistono aperture nell'esercizio corrente controllo 
      *     se esistono chiusure relative all'esercizio precedente
      if this.w_COUNTER=0
        this.w_ESEPRE = ""
        this.w_FINPRE = g_INIESE-1
        this.w_CODAZI = i_CODAZI
        * --- Select from ESERCIZI
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ESCODESE,ESINIESE,ESFINESE,ESVALNAZ  from "+i_cTable+" ESERCIZI ";
              +" where ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+" AND ESFINESE="+cp_ToStrODBC(this.w_FINPRE)+"";
               ,"_Curs_ESERCIZI")
        else
          select ESCODESE,ESINIESE,ESFINESE,ESVALNAZ from (i_cTable);
           where ESCODAZI=this.w_CODAZI AND ESFINESE=this.w_FINPRE;
            into cursor _Curs_ESERCIZI
        endif
        if used('_Curs_ESERCIZI')
          select _Curs_ESERCIZI
          locate for 1=1
          do while not(eof())
          this.w_ESEPRE = NVL(_Curs_ESERCIZI.ESCODESE, SPACE(4))
          this.w_VALPRE = NVL(_Curs_ESERCIZI.ESVALNAZ, SPACE(3))
            select _Curs_ESERCIZI
            continue
          enddo
          use
        endif
        this.w_FLAGIO = "C"
        this.w_COUNTER = 0
        * --- La procedura verifica se sono presenti chiusure nell'esercizio precedente
        * --- Select from Gste10bfl
        do vq_exec with 'Gste10bfl',this,'_Curs_Gste10bfl','',.f.,.t.
        if used('_Curs_Gste10bfl')
          select _Curs_Gste10bfl
          locate for 1=1
          do while not(eof())
          this.w_COUNTER = Nvl ( _Curs_Gste10bfl.Conta,0)
            select _Curs_Gste10bfl
            continue
          enddo
          use
        endif
        this.w_CODESE = g_CODESE
        this.w_CODES1 = this.w_ESEPRE
        if this.w_COUNTER=0
          * --- Non � presente un movimento di apertura per l'esercizio corrente e nemmeno
          *     un movimento di chiusura per l'esercizio precedente
          * --- Create temporary table TMPFL3
          i_nIdx=cp_AddTableDef('TMPFL3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSTEVBFL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPFL3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Non � presente un movimento di apertura per l'esercizio corrente ma
          *     un movimento di chiusura per l'esercizio precedente
          * --- Create temporary table TMPFL3
          i_nIdx=cp_AddTableDef('TMPFL3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSTEZBFL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPFL3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
      else
        * --- Esiste un movimento di apertura nell'esercizio corrente
        this.w_CODESE = g_CODESE
        this.w_CODES1 = SPACE(4)
        * --- Create temporary table TMPFL3
        i_nIdx=cp_AddTableDef('TMPFL3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSTEWBFL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPFL3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      * --- Create temporary table TMPCASHATT
      i_nIdx=cp_AddTableDef('TMPCASHATT') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPCASHATT_proto';
            )
      this.TMPCASHATT_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Controllo se � stato attivato il check atteso.
      if this.oParentObject.w_TIPATT="S"
        * --- Insert into TMPCASHATT
        i_nConn=i_TableProp[this.TMPCASHATT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCASHATT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gste3ccf",this.TMPCASHATT_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      ah_Msg("Elaborazione in corso...")
      vq_exec("query\GSTE_BFL.VQR",this,"CURSALT")
      SELECT CURSALT
      SELECT CODRAG, TIPRAG, CP_TODATE(DATVAL) AS DATVAL, NUMDOC, ALFDOC, CP_TODATE(DATDOC) AS DATDOC, TIPCON, CODCON, ;
      CODBAN, CODPAG, IMPDAR, IMPAVE, DESRAG, DESRIG, CAOAPE, DATAPE ;
      FROM CURSALT WHERE NOT DELETED() INTO CURSOR CURSALT
      * --- Eseguo pagina 2 solo se l'utente ha selezionato il flag Effettivo
      if this.oParentObject.w_TIPEFF="S"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Union fra cursori (cursalt che legge dati c.f. certo e atteso e parcurs che contiene dati elaborati c.f effettivo a pag.2)
        SELECT * FROM CURSALT UNION ALL SELECT * FROM PARCURS INTO CURSOR CURSTOT ORDER BY DATVAL
      else
        SELECT * FROM CURSALT INTO CURSOR CURSTOT ORDER BY DATVAL
      endif
      * --- Drop temporary table TMPFL2
      i_nIdx=cp_GetTableDefIdx('TMPFL2')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPFL2')
      endif
      * --- Drop temporary table TMPFL3
      i_nIdx=cp_GetTableDefIdx('TMPFL3')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPFL3')
      endif
      * --- Drop temporary table TMPCASHATT
      i_nIdx=cp_GetTableDefIdx('TMPCASHATT')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPCASHATT')
      endif
      * --- Creazione tabella temporanea solo struttura da query vuota
      * --- Create temporary table TMPFL2
      i_nIdx=cp_AddTableDef('TMPFL2') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTE1BFL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPFL2_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      SELECT CURSTOT
      GO TOP
      SCAN
      this.w_TIPRAG = NVL(TIPRAG," ")
      this.w_DATVAL = Cp_Todate(Nvl(CursTot.Datval,cp_CharToDate("  -  -  ")))
      this.w_NUMDOC = NVL(NUMDOC,0)
      this.w_ALFDOC = NVL(ALFDOC,SPACE(10))
      this.w_DATDOC = NVL(DATDOC,cp_CharToDate("  -  -  "))
      this.w_TIPCON = NVL(TIPCON, " ")
      this.w_CODCON = NVL(CODCON, SPACE(15))
      this.w_CODBAN = NVL(CODBAN,SPACE(15))
      this.w_CODPAG = NVL(CODPAG,SPACE(10))
      this.w_IMPDAR = NVL(IMPDAR,0)
      this.w_IMPAVE = NVL(IMPAVE,0)
      this.w_DESRAG = NVL(DESRAG,SPACE(50))
      this.w_CAOAPE = NVL(CAOAPE,0)
      this.w_DATAPE = NVL(DATAPE,cp_CharToDate("  -  -  "))
      if this.w_TIPRAG="B"
        * --- Proveniente dai Saldi Conti Banche
        this.w_CODRAG = "00000000"
        this.w_DESRAG = ah_Msgformat("Saldo attuale conti banca")
        this.w_DESRIG = SPACE(50)
      else
        do case
          case this.oParentObject.w_TIPPER="G"
            this.w_CODRAG = Dtos(this.w_DATVAL)
            this.w_DESRAG = "PPP%1"+Dtoc(this.w_DATVAL)
          case this.oParentObject.w_TIPPER="S"
            do case
              case WEEK(this.w_DATVAL,1,2)=52
                this.w_CODRAG = Left(Dtos(this.w_DATVAL-7),4)+Right("00"+alltrim(Str(Week(this.w_DATVAL,1,2))),2)+"00"
              case WEEK(this.w_DATVAL,1,2)=1
                this.w_CODRAG = Left(Dtos(this.w_DATVAL+7),4)+Right("00"+Alltrim(Str(Week(this.w_DATVAL,1,2))),2)+"00"
              otherwise
                this.w_CODRAG = Left(Dtos(this.w_DATVAL),4)+Right("00"+Alltrim(Str(Week(this.w_DATVAL,1,2))),2)+"00"
            endcase
            this.w_DESRAG = "PSS%1"+Dtoc((this.w_DATVAL)-Dow((this.w_DATVAL),2)+1)+"%2 "+Dtoc((this.w_DATVAL)-Dow((this.w_DATVAL),2)+7) 
          case this.oParentObject.w_TIPPER="Q"
            this.w_CODRAG = Left(Dtos(this.w_DATVAL),6)+iif(Day(this.w_DATVAL)<16,"01","02")
            if DAY(this.w_DATVAL)<16
              this.w_DESRAG = "PPM%1"+g_MESE[Month(this.w_DATVAL)]+Str(Year(this.w_DATVAL))
            else
              this.w_DESRAG = "PSM%1"+g_MESE[Month(this.w_DATVAL)]+Str(Year(this.w_DATVAL))
            endif
          case this.oParentObject.w_TIPPER="M"
            this.w_CODRAG = Left(Dtos(this.w_DATVAL),6)+"00"
            this.w_DESRAG = "PMM%1"+alltrim(g_MESE[Month(this.w_DATVAL)] )+" "+Str(Year(this.w_DATVAL))
        endcase
        this.w_DESRIG = NVL(DESRIG, SPACE(50))
        this.w_TIPRAG = "D"
      endif
      this.w_IMPDAR = VAL2VAL(this.w_IMPDAR,this.w_CAOAPE,this.w_DATAPE,i_DATSYS,this.oParentObject.w_CAMBIO,this.oParentObject.w_DECTOT)
      this.w_IMPAVE = VAL2VAL(this.w_IMPAVE,this.w_CAOAPE,this.w_DATAPE,i_DATSYS,this.oParentObject.w_CAMBIO,this.oParentObject.w_DECTOT)
      this.w_DESRAG = LEFT(this.w_DESRAG,50)
      * --- Insert into TMPFL2
      i_nConn=i_TableProp[this.TMPFL2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPFL2_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPFL2_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"TIPRAG"+",CODRAG"+",DATVAL"+",NUMDOC"+",ALFDOC"+",DATDOC"+",TIPCON"+",CODCON"+",CODBAN"+",CODPAG"+",IMPDAR"+",IMPAVE"+",DESRAG"+",DESRIG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_TIPRAG),'TMPFL2','TIPRAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODRAG),'TMPFL2','CODRAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATVAL),'TMPFL2','DATVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'TMPFL2','NUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ALFDOC),'TMPFL2','ALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATDOC),'TMPFL2','DATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'TMPFL2','TIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'TMPFL2','CODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODBAN),'TMPFL2','CODBAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODPAG),'TMPFL2','CODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDAR),'TMPFL2','IMPDAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPAVE),'TMPFL2','IMPAVE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESRAG),'TMPFL2','DESRAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESRIG),'TMPFL2','DESRIG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'TIPRAG',this.w_TIPRAG,'CODRAG',this.w_CODRAG,'DATVAL',this.w_DATVAL,'NUMDOC',this.w_NUMDOC,'ALFDOC',this.w_ALFDOC,'DATDOC',this.w_DATDOC,'TIPCON',this.w_TIPCON,'CODCON',this.w_CODCON,'CODBAN',this.w_CODBAN,'CODPAG',this.w_CODPAG,'IMPDAR',this.w_IMPDAR,'IMPAVE',this.w_IMPAVE)
        insert into (i_cTable) (TIPRAG,CODRAG,DATVAL,NUMDOC,ALFDOC,DATDOC,TIPCON,CODCON,CODBAN,CODPAG,IMPDAR,IMPAVE,DESRAG,DESRIG &i_ccchkf. );
           values (;
             this.w_TIPRAG;
             ,this.w_CODRAG;
             ,this.w_DATVAL;
             ,this.w_NUMDOC;
             ,this.w_ALFDOC;
             ,this.w_DATDOC;
             ,this.w_TIPCON;
             ,this.w_CODCON;
             ,this.w_CODBAN;
             ,this.w_CODPAG;
             ,this.w_IMPDAR;
             ,this.w_IMPAVE;
             ,this.w_DESRAG;
             ,this.w_DESRIG;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ENDSCAN
      * --- Costruisce Temporaneo Raggruppato su CODRAG
      ah_Msg("Raggruppamento per periodo: elaborazione in corso; attendere...",.T.)
      * --- Create temporary table TMPFL1
      i_nIdx=cp_AddTableDef('TMPFL1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTE4BFL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPFL1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      if this.pOper="S"
        * --- Selezionata Stampa 
        *     Variabili per Stampa
        vq_exec(ALLTRIM(this.oParentObject.w_OQRY),this,"__TMP__")
        L_TIPCER=this.oParentObject.w_TIPCER
        L_TIPEFF=this.oParentObject.w_TIPEFF
        L_TIPATT=this.oParentObject.w_TIPATT
        L_TIPCLI=this.oParentObject.w_TIPCLI
        L_TIPFOR=this.oParentObject.w_TIPFOR
        L_TIPGEN=this.oParentObject.w_TIPGEN
        L_DATINI=this.oParentObject.w_DATINI
        L_DATFIN=this.oParentObject.w_DATFIN
        L_CODCLI=this.oParentObject.w_CODCLI
        L_CODFOR=this.oParentObject.w_CODFOR
        L_CODVAL=this.oParentObject.w_CODVAL
        L_NDOCIN=this.oParentObject.w_NDOCIN
        L_NDOCFI=this.oParentObject.w_NDOCFI
        L_ADOCIN=this.oParentObject.w_ADOCIN
        L_ADOCFI=this.oParentObject.w_ADOCFI
        L_DDOCIN=this.oParentObject.w_DDOCIN
        L_DDOCFI=this.oParentObject.w_DDOCFI
        L_PAGRD=this.oParentObject.w_PAGRD
        L_PAGBO=this.oParentObject.w_PAGBO
        L_PAGRB=this.oParentObject.w_PAGRB
        L_PAGRI=this.oParentObject.w_PAGRI
        L_PAGCA=this.oParentObject.w_PAGCA
        L_PAGMA=this.oParentObject.w_PAGMA
        L_SIMVAL=IIF(this.oParentObject.w_SIMVAL=" ", this.oParentObject.w_VALRAP,this.oParentObject.w_SIMVAL)
        L_CAMBIO=this.oParentObject.w_CAMBIO
        L_TIPPER=this.oParentObject.w_TIPPER
        L_DECTOT=this.oParentObject.w_DECTOT
        if USED("__TMP__")
          if Reccount("__TMP__") <>0
            SELECT __TMP__
            CP_CHPRN(ALLTRIM(this.oParentObject.w_OREP), " ", this)
          else
            ah_ErrorMsg("Per le selezioni effettuate non esistono record da stampare",,"")
          endif
        endif
      endif
      * --- Chiusura cursori e tabelle temporanee
      use in select ("__TMP__")
      use in select ("CursTot")
      use in select ("Cursalt")
      * --- Drop temporary table TMPFL1
      i_nIdx=cp_GetTableDefIdx('TMPFL1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPFL1')
      endif
      * --- Drop temporary table TMPFL2
      i_nIdx=cp_GetTableDefIdx('TMPFL2')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPFL2')
      endif
    else
      ah_ErrorMsg("Attenzione nessun conto selezionato",,"")
    endif
    * --- Drop temporary table TMPFL2
    i_nIdx=cp_GetTableDefIdx('TMPFL2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPFL2')
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili per il cursore PARCURS
    vq_exec("query\GSTE7BFL.VQR",this,"SELEPART")
    if RECCOUNT("SELEPART")>0
      Cur = WrCursor("Selepart")
      GSTE_BCP(this,"A","Selepart"," ")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Costruisco Cursore PARCURS ed eseguo filtri selezioni impostate
    Select SPACE(8) AS CODRAG,"D" AS TIPRAG,CP_TODATE(DATSCA) AS DATVAL,NUMDOC,ALFDOC,CP_TODATE(DATDOC) AS DATDOC,TIPCON,CODCON,; 
 BANNOS AS CODBAN,MODPAG AS CODPAG,; 
 IIF(Nvl(SEGNO," ")="D" AND Nvl(CAOVAL,0)<>0,ABS(TOTIMP), TOTIMP*0) AS IMPDAR,; 
 IIF(Nvl(SEGNO," ")="A" AND Nvl(CAOVAL,0)<>0,ABS(TOTIMP),TOTIMP*0) AS IMPAVE,; 
 SPACE(50) AS DESRAG, DESSUP AS DESRIG,CAOAPE,DATAPE FROM SELEPART Into Cursor ParCurs NoFilter
    * --- Elimino Cursore
    use in select ("Selepart")
    use in select ("Filbanca")
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='*TMPFL1'
    this.cWorkTables[2]='*TMPFL2'
    this.cWorkTables[3]='COD_TRIB'
    this.cWorkTables[4]='*TMPFL3'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='*TMPCASHATT'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_Gste10bfl')
      use in _Curs_Gste10bfl
    endif
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_Gste10bfl')
      use in _Curs_Gste10bfl
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
