* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bdi                                                        *
*              Stampa distinte effetti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_390]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-03                                                      *
* Last revis.: 2016-11-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bdi",oParentObject)
return(i_retval)

define class tgste_bdi as StdBatch
  * --- Local variables
  w_NUMERQ = space(10)
  w_TIPOSCA = space(1)
  w_FLBANC = space(1)
  w_PTDATSCA = ctod("  /  /  ")
  w_NOMDES = space(40)
  w_DESBAN = space(35)
  w_CODCAB = space(5)
  w_CODABI = space(5)
  w_STAMDEF = space(1)
  w_DATAVAL = ctod("  /  /  ")
  w_PTSERIAL = space(10)
  w_INDIRI = space(35)
  w_PROGRE = 0
  w_NUMERO = 0
  w_PTROWORD = 0
  w_CAP = space(5)
  w_QUEREP = space(40)
  w_PROSTA = 0
  w_PTROWNUM = 0
  w_LOCALI = space(30)
  w_DATDISM = ctod("  /  /  ")
  w_NUMCOAM = space(15)
  w_ANTIPCON = space(1)
  w_PROVIN = space(2)
  w_TIPDISM = space(2)
  w_CODESE = space(4)
  w_ANCODCON = space(15)
  w_CODNAZ = space(3)
  w_CODCAUM = space(5)
  w_CODVAL = space(3)
  w_ANFLRAGG = space(1)
  w_CODDES = space(5)
  w_SCOTRA = 0
  w_DECIMI = 0
  w_NUMDIS = space(10)
  w_MESS = space(50)
  w_CHKDEF = space(1)
  w_DATPAR = ctod("  /  /  ")
  w_TIPCON = space(1)
  w_CCDES = space(35)
  w_MODPAG = space(10)
  w_TIPCON = space(1)
  w_NEWREC = space(10)
  w_TELEFO = space(18)
  w_FLRAGG = space(1)
  w_CODCON = space(15)
  w_OLDREC = space(10)
  w_FAX = space(18)
  w_APPO = 0
  w_APPO1 = space(10)
  w_COMEFF = 0
  w_CONTA = 0
  w_CONTAEF = 0
  w_IDORDINE = space(34)
  w_VECCHIOTIPO = .f.
  w_PNAGG_01 = space(15)
  w_PNAGG_02 = space(15)
  w_PNAGG_03 = space(15)
  w_PNAGG_04 = space(15)
  w_PNAGG_05 = ctod("  /  /  ")
  w_PNAGG_06 = ctod("  /  /  ")
  w_PNNUMFAT = space(20)
  w_DINSCFRB = space(20)
  w_oERRORLOG = .NULL.
  w_DESCRIZI = space(254)
  w_FLDSPR = space(1)
  w_DESPAR = space(254)
  w_PADRE = .NULL.
  w_EFFF_ZERO = space(1)
  w_BRAG = .f.
  w_LTIPCON = space(1)
  w_LCODCON = space(15)
  w_LDATSCA = ctod("  /  /  ")
  w_LNUMPAR = space(31)
  w_LCODVAL = space(3)
  w_LFLCRSA = space(1)
  w_LSIMVAL = space(5)
  w_LDECTOT = 0
  w_LMODPAG = space(10)
  w_LNUMDOC = 0
  w_LALFDOC = space(10)
  w_LDATDOC = ctod("  /  /  ")
  w_LFLSOSP = space(1)
  w_LTOTIMP = 0
  w_LCAOVAL = 0
  w_LFLRAGG = space(1)
  w_LFLDAVE = space(1)
  w_LBANAPP = space(10)
  w_LBANNOS = space(15)
  w_LNUMGRUP = 0
  w_LROWORD = 0
  w_LROWNUM = 0
  w_LNUMEFF = 0
  w_LNUMDIS = space(10)
  w_LCODAGE = space(5)
  w_LTIPPAG = space(2)
  w_LSEGNO = space(1)
  w_LSERIAL = space(10)
  w_LANDESC = space(40)
  w_LANDES2 = space(40)
  w_LINDIRI = space(35)
  w_LINDIRI2 = space(35)
  w_L_CAP = space(8)
  w_LLOCALI = space(30)
  w_LPROVIN = space(2)
  w_LNAZION = space(3)
  w_LPERFIS = space(1)
  w_LCODFIS = space(16)
  w_LPARIVA = space(12)
  w_LCODABI = space(5)
  w_LDESBAN = space(50)
  w_LSCDES = space(35)
  w_LCODCAB = space(5)
  w_LANRAGG = space(1)
  w_LNUMCOR = space(25)
  w_LDCDES = space(5)
  w_LDNDES = space(40)
  w_LDINDI = space(35)
  w_LD_CAP = space(8)
  w_LDLOCAL = space(30)
  w_LDPROVI = space(2)
  w_LDNAZ = space(3)
  w_LDTIPRIF = space(1)
  w_LORDINE = 0
  w_LIMPVAL = 0
  w_LIMPCOD = space(3)
  w_LIMPCAO = 0
  w_LSUBTOT = 0
  w_LDATASCA = ctod("  /  /  ")
  w_LDTOBSO = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_LCHKRIF = space(1)
  w_LCONRIF = space(15)
  w_Lan__iban = space(35)
  w_Lan__bban = space(30)
  w_LANCINABI = space(1)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_OCCURS1 = 0
  w_OCCURS2 = 0
  w_OCCURS3 = 0
  w_OCCURS4 = 0
  w_OCCURS5 = 0
  w_OCCURS6 = 0
  w_OCCURS7 = 0
  w_nFindVar = 0
  w_nFindVarFin = 0
  w_MAXAGG = space(100)
  w_MINAGG = space(100)
  * --- WorkFile variables
  AZIENDA_idx=0
  DES_DIVE_idx=0
  ESERCIZI_idx=0
  PAR_TITE_idx=0
  DIS_TINT_idx=0
  PRO_NUME_idx=0
  VALUTE_idx=0
  CONTI_idx=0
  COC_MAST_idx=0
  CAU_DIST_idx=0
  BAN_CAST_idx=0
  TMPTPAR_TITE_idx=0
  DIS_BOES_idx=0
  DIS_BECV_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Distinte Effetti (da GSTE_SDI)
    this.w_VECCHIOTIPO = .F.
    this.w_DESCRIZI = Space(254)
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_NUMCOAM = SPACE(15)
    this.w_CODCAUM = SPACE(5)
    this.w_DATAVAL = cp_CharToDate("  -  -  ")
    this.w_DINSCFRB = ""
    * --- Lettura Dati Distinta Selezionata
    * --- Read from DIS_TINT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_TINT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DIDATDIS,DITIPDIS,DIDATVAL,DINUMERO,DIFLDEFI,DICAURIF,DIBANRIF,DICODESE,DITIPSCA,DISCOTRA,DICODVAL,DINSCFRB"+;
        " from "+i_cTable+" DIS_TINT where ";
            +"DINUMDIS = "+cp_ToStrODBC(this.oParentObject.w_NUMDISM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DIDATDIS,DITIPDIS,DIDATVAL,DINUMERO,DIFLDEFI,DICAURIF,DIBANRIF,DICODESE,DITIPSCA,DISCOTRA,DICODVAL,DINSCFRB;
        from (i_cTable) where;
            DINUMDIS = this.oParentObject.w_NUMDISM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATDISM = NVL(cp_ToDate(_read_.DIDATDIS),cp_NullValue(_read_.DIDATDIS))
      this.w_TIPDISM = NVL(cp_ToDate(_read_.DITIPDIS),cp_NullValue(_read_.DITIPDIS))
      this.w_DATAVAL = NVL(cp_ToDate(_read_.DIDATVAL),cp_NullValue(_read_.DIDATVAL))
      this.w_NUMERO = NVL(cp_ToDate(_read_.DINUMERO),cp_NullValue(_read_.DINUMERO))
      this.w_CHKDEF = NVL(cp_ToDate(_read_.DIFLDEFI),cp_NullValue(_read_.DIFLDEFI))
      this.w_CODCAUM = NVL(cp_ToDate(_read_.DICAURIF),cp_NullValue(_read_.DICAURIF))
      this.w_NUMCOAM = NVL(cp_ToDate(_read_.DIBANRIF),cp_NullValue(_read_.DIBANRIF))
      this.w_CODESE = NVL(cp_ToDate(_read_.DICODESE),cp_NullValue(_read_.DICODESE))
      this.w_TIPOSCA = NVL(cp_ToDate(_read_.DITIPSCA),cp_NullValue(_read_.DITIPSCA))
      this.w_SCOTRA = NVL(cp_ToDate(_read_.DISCOTRA),cp_NullValue(_read_.DISCOTRA))
      this.w_CODVAL = NVL(cp_ToDate(_read_.DICODVAL),cp_NullValue(_read_.DICODVAL))
      this.w_DINSCFRB = NVL(cp_ToDate(_read_.DINSCFRB),cp_NullValue(_read_.DINSCFRB))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAU_DIST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_DIST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_DIST_idx,2],.t.,this.CAU_DIST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CAFLBANC,CAFLDSPR,CADESPAR"+;
        " from "+i_cTable+" CAU_DIST where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CODCAUM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CAFLBANC,CAFLDSPR,CADESPAR;
        from (i_cTable) where;
            CACODICE = this.w_CODCAUM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLBANC = NVL(cp_ToDate(_read_.CAFLBANC),cp_NullValue(_read_.CAFLBANC))
      this.w_FLDSPR = NVL(cp_ToDate(_read_.CAFLDSPR),cp_NullValue(_read_.CAFLDSPR))
      this.w_DESPAR = NVL(cp_ToDate(_read_.CADESPAR),cp_NullValue(_read_.CADESPAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo le commissioni per singolo effetto
    * --- Read from BAN_CAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.BAN_CAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CAST_idx,2],.t.,this.BAN_CAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CBCOMEFF"+;
        " from "+i_cTable+" BAN_CAST where ";
            +"CBCAUDIS = "+cp_ToStrODBC(this.w_CODCAUM);
            +" and CBCODBAN = "+cp_ToStrODBC(this.w_NUMCOAM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CBCOMEFF;
        from (i_cTable) where;
            CBCAUDIS = this.w_CODCAUM;
            and CBCODBAN = this.w_NUMCOAM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COMEFF = NVL(cp_ToDate(_read_.CBCOMEFF),cp_NullValue(_read_.CBCOMEFF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controlli Preliminari
    * --- Conmtrolla che la distinta in stampa non abbia un dettaglio CVS di TIPo 'REG'
    if this.w_TIPDISM="BE" AND this.oParentObject.w_STAMPA="D"
      * --- Select from DIS_BECV
      i_nConn=i_TableProp[this.DIS_BECV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIS_BECV_idx,2],.t.,this.DIS_BECV_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT (*) as CONTA  from "+i_cTable+" DIS_BECV ";
            +" where DINUMDIS="+cp_ToStrODBC(this.oParentObject.w_NUMDISM)+" AND DITIPCVS='REG'";
             ,"_Curs_DIS_BECV")
      else
        select COUNT (*) as CONTA from (i_cTable);
         where DINUMDIS=this.oParentObject.w_NUMDISM AND DITIPCVS="REG";
          into cursor _Curs_DIS_BECV
      endif
      if used('_Curs_DIS_BECV')
        select _Curs_DIS_BECV
        locate for 1=1
        do while not(eof())
        if CONTA>0
          this.w_VECCHIOTIPO = .T.
        endif
          select _Curs_DIS_BECV
          continue
        enddo
        use
      endif
      if this.w_VECCHIOTIPO
        * --- Messaggistica di Errore e blocco la generazione.
        ah_ErrorMsg("Tipo CVS= 'Regol. di CVS gi� emessa' non ammesso!%0L' operazione � stata annullata","!","")
        this.oParentObject.w_TEST = .F.
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_STAMDEF = IIF(this.oParentObject.w_STAMPA $ "SD","N","S")
    do case
      case EMPTY(this.w_NUMCOAM)
        ah_ErrorMsg("Banca di presentazione inesistente",,"")
        this.oParentObject.w_TEST = .F.
        i_retcode = 'stop'
        return
      case EMPTY(this.w_CODCAUM)
        ah_ErrorMsg("Causale distinta inesistente",,"")
        this.oParentObject.w_TEST = .F.
        i_retcode = 'stop'
        return
      case EMPTY(this.w_DATAVAL)
        ah_ErrorMsg("Data valuta non definita",,"")
        this.oParentObject.w_TEST = .F.
        i_retcode = 'stop'
        return
      case this.w_STAMDEF="N" AND this.w_CHKDEF="S"
        ah_ErrorMsg("Selezionare una distinta non ancora definitiva",,"")
        this.oParentObject.w_TEST = .F.
        i_retcode = 'stop'
        return
      case this.w_STAMDEF="S" AND this.w_CHKDEF="N"
        ah_ErrorMsg("Selezionare una distinta definitiva",,"")
        this.oParentObject.w_TEST = .F.
        i_retcode = 'stop'
        return
    endcase
    this.w_APPO = 0
    * --- Legge Valuta Originaria Distinta
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT,VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT,VACAOVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECIMI = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_APPO = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Tutte le distinte in Valuta EMU successive al 31-12-01 andranno convertite in EURO
    this.oParentObject.w_FLEURO = IIF(this.w_DATAVAL>cp_CharToDate("31-12-2001"), "S", this.oParentObject.w_FLEURO)
    * --- Se sulla Maschera imposto il Flag Importi in euro ma la Distinta e' gia' in EURO o e' extra EMU non deve fare nulla
    this.oParentObject.w_FLEURO = IIF(this.w_CODVAL=g_PERVAL OR this.w_APPO=0, " ", this.oParentObject.w_FLEURO)
    if this.oParentObject.w_FLEURO="S"
      * --- Coverto I dati Valuta in Euro
      this.w_CODVAL = g_PERVAL
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.w_CODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECIMI = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- DESCRIZIONE c/c
    * --- Read from COC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "BADESCRI"+;
        " from "+i_cTable+" COC_MAST where ";
            +"BACODBAN = "+cp_ToStrODBC(this.w_NUMCOAM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        BADESCRI;
        from (i_cTable) where;
            BACODBAN = this.w_NUMCOAM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCDES = NVL(cp_ToDate(_read_.BADESCRI),cp_NullValue(_read_.BADESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- TELEFONO E FAX
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZTELEFO,AZTELFAX"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZTELEFO,AZTELFAX;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TELEFO = NVL(cp_ToDate(_read_.AZTELEFO),cp_NullValue(_read_.AZTELEFO))
      this.w_FAX = NVL(cp_ToDate(_read_.AZTELFAX),cp_NullValue(_read_.AZTELFAX))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Parametri da Passare alla Query
    this.w_NUMERQ = RIGHT(replicate("0",10)+this.oParentObject.w_NUMDISM,10)
    * --- variabili per il report
    L_ccdes=this.w_CCDES
    L_data=this.w_DATAVAL
    L_fax=this.w_FAX
    L_telefo=this.w_TELEFO
    L_decimi=this.w_DECIMI
    L_tipo=this.w_TIPDISM
    L_numero=this.w_NUMERO
    L_divisa=this.w_CODVAL
    L_detdocd=this.oParentObject.w_DETDOCD
    L_FLDSPR=this.w_FLDSPR
    L_DESCEST=this.oParentObject.w_DESCEST
    * --- COSTRUISCO IL PARAMETRO PER LANCIARE LA STAMPA
    do case
      case this.w_TIPDISM="RB"
        L_desdist=ah_Msgformat("Ricevute bancarie")
      case this.w_TIPDISM="MA"
        L_desdist=ah_Msgformat("M.AV.")
      case this.w_TIPDISM="CA"
        L_desdist=ah_Msgformat("Tratte")
      case this.w_TIPDISM$ "BO-SC-SE"
        L_desdist=ah_Msgformat("a mezzo bonifico")
      case this.w_TIPDISM="RD"
        L_desdist=ah_Msgformat("Rimesse dirette")
      case this.w_TIPDISM $ "RI-SD"
        L_desdist=ah_Msgformat("R.I.D.")
      case this.w_TIPDISM="CE"
        L_desdist=ah_Msgformat("Cessioni crediti")
      otherwise
        L_desdist=""
    endcase
    * --- Parametri da passare alla query per determinare la descrizione parametrica
    this.w_OCCURS1 = OCCURS("%1", this.w_DESPAR)
    this.w_OCCURS2 = OCCURS("%2", this.w_DESPAR)
    this.w_OCCURS3 = OCCURS("%3", this.w_DESPAR)
    this.w_OCCURS4 = OCCURS("%4", this.w_DESPAR)
    this.w_OCCURS5 = OCCURS("%5", this.w_DESPAR)
    this.w_OCCURS6 = OCCURS("%6", this.w_DESPAR)
    this.w_OCCURS7 = OCCURS("%7", this.w_DESPAR)
    * --- Se R nel report stampo il Numero effetto dall'archivio, altrimenti lo calcolo
    L_stampa=this.oParentObject.w_STAMPA
    * --- Determina l'ordinamento da imporre al cursore per il report...
    *     (Dichiarato a pag1 per renderlo visibile in tutte le pagine del batch)
    L_Ordinamento = ""
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_STAMPA="D"
      * --- Stampa definitiva
      * --- Try
      local bErr_044EF2A0
      bErr_044EF2A0=bTrsErr
      this.Try_044EF2A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg(i_errmsg,,"")
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_044EF2A0
      * --- End
      * --- lancio la stampa - FUORI DALLA TRANSAZIONE
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- se stampo in definitiva e va tutto bene ricalcolo il cursore
      this.w_PADRE = This.oParentObject
      this.w_PADRE.NotifyEvent("Esegui")     
    else
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_044EF2A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Se R nel report stampo il Numero effetto dall'archivio, altrimenti lo calcolo  lo metto a 'D' (w_STAMPA)
    * --- begin transaction
    cp_BeginTrs()
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa
    this.w_PROGRE = 0
    if this.oParentObject.w_STAMPA<>"R"
      * --- Calcolo il progressivo se non � una ristampa ed � una distinta attiva
      * --- progressivi anche per i passivi
      * --- Read from PRO_NUME
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRO_NUME_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2],.t.,this.PRO_NUME_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PRNUMPRO"+;
          " from "+i_cTable+" PRO_NUME where ";
              +"PRCODAZI = "+cp_ToStrODBC(i_codazi);
              +" and PRCODESE = "+cp_ToStrODBC(g_CODESE);
              +" and PRTIPPAG = "+cp_ToStrODBC(this.w_TIPDISM);
              +" and PRTIPCON = "+cp_ToStrODBC(this.w_TIPOSCA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PRNUMPRO;
          from (i_cTable) where;
              PRCODAZI = i_codazi;
              and PRCODESE = g_CODESE;
              and PRTIPPAG = this.w_TIPDISM;
              and PRTIPCON = this.w_TIPOSCA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PROGRE = NVL(cp_ToDate(_read_.PRNUMPRO),cp_NullValue(_read_.PRNUMPRO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PROGRE = nvl(this.w_PROGRE,0)
    endif
    * --- PROGRESSIVO IN CASO DI STAMPA SIMULATA
    this.w_PROSTA = this.w_PROGRE
    * --- Query lettura Scadenze della Distinta
    * --- Se disabilitato check "Dettaglio Documenti Diversi" considero il flag di accorpamento per data scadenza legato al cliente -  -ordino per data scadenza e data documento
    if this.oParentObject.w_DETDOCD="S"
      * --- Create temporary table TMPTPAR_TITE
      i_nIdx=cp_AddTableDef('TMPTPAR_TITE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTE4QDI',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPTPAR_TITE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      if this.oParentObject.w_STAMPA<>"R"
        * --- Marco eventuali scadenze comprese in effetti con totale a 0
        *     (fatture e note di credito assieme)
        *     Se distinta di tipo cambiale/tratta no!
        if this.w_TIPDISM<>"CA"
          this.w_EFFF_ZERO = "S"
          * --- Se qui svolgo una delete al posto della Write gli effetti
          *     con totale a 0 non sarebbero dettagliati
          * --- Write into TMPTPAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPTPAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="PTDATSCA,PNTIPCON,PNCODCON,PTMODPAG,PTBANAPP,PTBANNOS,PTCODRAG,PTNUMCOR"
            do vq_exec with 'GSTE2QDIB',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPTPAR_TITE_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPTPAR_TITE.PTDATSCA = _t2.PTDATSCA";
                    +" and "+"TMPTPAR_TITE.PNTIPCON = _t2.PNTIPCON";
                    +" and "+"TMPTPAR_TITE.PNCODCON = _t2.PNCODCON";
                    +" and "+"TMPTPAR_TITE.PTMODPAG = _t2.PTMODPAG";
                    +" and "+"TMPTPAR_TITE.PTBANAPP = _t2.PTBANAPP";
                    +" and "+"TMPTPAR_TITE.PTBANNOS = _t2.PTBANNOS";
                    +" and "+"TMPTPAR_TITE.PTCODRAG = _t2.PTCODRAG";
                    +" and "+"TMPTPAR_TITE.PTNUMCOR = _t2.PTNUMCOR";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTNUMEFF ="+cp_NullLink(cp_ToStrODBC(-1),'TMPTPAR_TITE','PTNUMEFF');
                +i_ccchkf;
                +" from "+i_cTable+" TMPTPAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPTPAR_TITE.PTDATSCA = _t2.PTDATSCA";
                    +" and "+"TMPTPAR_TITE.PNTIPCON = _t2.PNTIPCON";
                    +" and "+"TMPTPAR_TITE.PNCODCON = _t2.PNCODCON";
                    +" and "+"TMPTPAR_TITE.PTMODPAG = _t2.PTMODPAG";
                    +" and "+"TMPTPAR_TITE.PTBANAPP = _t2.PTBANAPP";
                    +" and "+"TMPTPAR_TITE.PTBANNOS = _t2.PTBANNOS";
                    +" and "+"TMPTPAR_TITE.PTCODRAG = _t2.PTCODRAG";
                    +" and "+"TMPTPAR_TITE.PTNUMCOR = _t2.PTNUMCOR";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTPAR_TITE, "+i_cQueryTable+" _t2 set ";
            +"TMPTPAR_TITE.PTNUMEFF ="+cp_NullLink(cp_ToStrODBC(-1),'TMPTPAR_TITE','PTNUMEFF');
                +Iif(Empty(i_ccchkf),"",",TMPTPAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPTPAR_TITE.PTDATSCA = t2.PTDATSCA";
                    +" and "+"TMPTPAR_TITE.PNTIPCON = t2.PNTIPCON";
                    +" and "+"TMPTPAR_TITE.PNCODCON = t2.PNCODCON";
                    +" and "+"TMPTPAR_TITE.PTMODPAG = t2.PTMODPAG";
                    +" and "+"TMPTPAR_TITE.PTBANAPP = t2.PTBANAPP";
                    +" and "+"TMPTPAR_TITE.PTBANNOS = t2.PTBANNOS";
                    +" and "+"TMPTPAR_TITE.PTCODRAG = t2.PTCODRAG";
                    +" and "+"TMPTPAR_TITE.PTNUMCOR = t2.PTNUMCOR";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTPAR_TITE set (";
                +"PTNUMEFF";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC(-1),'TMPTPAR_TITE','PTNUMEFF')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPTPAR_TITE.PTDATSCA = _t2.PTDATSCA";
                    +" and "+"TMPTPAR_TITE.PNTIPCON = _t2.PNTIPCON";
                    +" and "+"TMPTPAR_TITE.PNCODCON = _t2.PNCODCON";
                    +" and "+"TMPTPAR_TITE.PTMODPAG = _t2.PTMODPAG";
                    +" and "+"TMPTPAR_TITE.PTBANAPP = _t2.PTBANAPP";
                    +" and "+"TMPTPAR_TITE.PTBANNOS = _t2.PTBANNOS";
                    +" and "+"TMPTPAR_TITE.PTCODRAG = _t2.PTCODRAG";
                    +" and "+"TMPTPAR_TITE.PTNUMCOR = _t2.PTNUMCOR";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTPAR_TITE set ";
            +"PTNUMEFF ="+cp_NullLink(cp_ToStrODBC(-1),'TMPTPAR_TITE','PTNUMEFF');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".PTDATSCA = "+i_cQueryTable+".PTDATSCA";
                    +" and "+i_cTable+".PNTIPCON = "+i_cQueryTable+".PNTIPCON";
                    +" and "+i_cTable+".PNCODCON = "+i_cQueryTable+".PNCODCON";
                    +" and "+i_cTable+".PTMODPAG = "+i_cQueryTable+".PTMODPAG";
                    +" and "+i_cTable+".PTBANAPP = "+i_cQueryTable+".PTBANAPP";
                    +" and "+i_cTable+".PTBANNOS = "+i_cQueryTable+".PTBANNOS";
                    +" and "+i_cTable+".PTCODRAG = "+i_cQueryTable+".PTCODRAG";
                    +" and "+i_cTable+".PTNUMCOR = "+i_cQueryTable+".PTNUMCOR";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTNUMEFF ="+cp_NullLink(cp_ToStrODBC(-1),'TMPTPAR_TITE','PTNUMEFF');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_EFFF_ZERO = "N"
          * --- Determino per ogni effetto il numero di scadenze che lo compongono
          *     Informazione utilizzata in stampa per la dicitura Doc. Diversi...
          * --- Write into TMPTPAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPTPAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="PTDATSCA,PNTIPCON,PNCODCON,PTMODPAG,PTBANAPP,PTBANNOS,PTCODRAG,PTNUMCOR"
            do vq_exec with 'GSTE2QDIB',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPTPAR_TITE_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPTPAR_TITE.PTDATSCA = _t2.PTDATSCA";
                    +" and "+"TMPTPAR_TITE.PNTIPCON = _t2.PNTIPCON";
                    +" and "+"TMPTPAR_TITE.PNCODCON = _t2.PNCODCON";
                    +" and "+"TMPTPAR_TITE.PTMODPAG = _t2.PTMODPAG";
                    +" and "+"TMPTPAR_TITE.PTBANAPP = _t2.PTBANAPP";
                    +" and "+"TMPTPAR_TITE.PTBANNOS = _t2.PTBANNOS";
                    +" and "+"TMPTPAR_TITE.PTCODRAG = _t2.PTCODRAG";
                    +" and "+"TMPTPAR_TITE.PTNUMCOR = _t2.PTNUMCOR";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"NUMGRUP = _t2.NUMGRUP";
                +i_ccchkf;
                +" from "+i_cTable+" TMPTPAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPTPAR_TITE.PTDATSCA = _t2.PTDATSCA";
                    +" and "+"TMPTPAR_TITE.PNTIPCON = _t2.PNTIPCON";
                    +" and "+"TMPTPAR_TITE.PNCODCON = _t2.PNCODCON";
                    +" and "+"TMPTPAR_TITE.PTMODPAG = _t2.PTMODPAG";
                    +" and "+"TMPTPAR_TITE.PTBANAPP = _t2.PTBANAPP";
                    +" and "+"TMPTPAR_TITE.PTBANNOS = _t2.PTBANNOS";
                    +" and "+"TMPTPAR_TITE.PTCODRAG = _t2.PTCODRAG";
                    +" and "+"TMPTPAR_TITE.PTNUMCOR = _t2.PTNUMCOR";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTPAR_TITE, "+i_cQueryTable+" _t2 set ";
                +"TMPTPAR_TITE.NUMGRUP = _t2.NUMGRUP";
                +Iif(Empty(i_ccchkf),"",",TMPTPAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPTPAR_TITE.PTDATSCA = t2.PTDATSCA";
                    +" and "+"TMPTPAR_TITE.PNTIPCON = t2.PNTIPCON";
                    +" and "+"TMPTPAR_TITE.PNCODCON = t2.PNCODCON";
                    +" and "+"TMPTPAR_TITE.PTMODPAG = t2.PTMODPAG";
                    +" and "+"TMPTPAR_TITE.PTBANAPP = t2.PTBANAPP";
                    +" and "+"TMPTPAR_TITE.PTBANNOS = t2.PTBANNOS";
                    +" and "+"TMPTPAR_TITE.PTCODRAG = t2.PTCODRAG";
                    +" and "+"TMPTPAR_TITE.PTNUMCOR = t2.PTNUMCOR";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTPAR_TITE set (";
                +"NUMGRUP";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.NUMGRUP";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPTPAR_TITE.PTDATSCA = _t2.PTDATSCA";
                    +" and "+"TMPTPAR_TITE.PNTIPCON = _t2.PNTIPCON";
                    +" and "+"TMPTPAR_TITE.PNCODCON = _t2.PNCODCON";
                    +" and "+"TMPTPAR_TITE.PTMODPAG = _t2.PTMODPAG";
                    +" and "+"TMPTPAR_TITE.PTBANAPP = _t2.PTBANAPP";
                    +" and "+"TMPTPAR_TITE.PTBANNOS = _t2.PTBANNOS";
                    +" and "+"TMPTPAR_TITE.PTCODRAG = _t2.PTCODRAG";
                    +" and "+"TMPTPAR_TITE.PTNUMCOR = _t2.PTNUMCOR";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTPAR_TITE set ";
                +"NUMGRUP = _t2.NUMGRUP";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".PTDATSCA = "+i_cQueryTable+".PTDATSCA";
                    +" and "+i_cTable+".PNTIPCON = "+i_cQueryTable+".PNTIPCON";
                    +" and "+i_cTable+".PNCODCON = "+i_cQueryTable+".PNCODCON";
                    +" and "+i_cTable+".PTMODPAG = "+i_cQueryTable+".PTMODPAG";
                    +" and "+i_cTable+".PTBANAPP = "+i_cQueryTable+".PTBANAPP";
                    +" and "+i_cTable+".PTBANNOS = "+i_cQueryTable+".PTBANNOS";
                    +" and "+i_cTable+".PTCODRAG = "+i_cQueryTable+".PTCODRAG";
                    +" and "+i_cTable+".PTNUMCOR = "+i_cQueryTable+".PTNUMCOR";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"NUMGRUP = (select NUMGRUP from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.w_NEWREC = "XXX"
        this.w_OLDREC = "ZZZ"
        this.w_PROGRE = this.w_PROSTA
        * --- Se No Ristampa riassegna il Numero Effetto
        *     ATTENZIONE all'ordinamento, all'interno della SELECT vi � una rottura..
        * --- Select from TMPTPAR_TITE
        i_nConn=i_TableProp[this.TMPTPAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2],.t.,this.TMPTPAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TMPTPAR_TITE ";
              +" where PTNUMEFF<>-1";
              +" order by PTDATSCA, PNTIPCON, PNCODCON, PTMODPAG, PTBANAPP, PTBANNOS, PTCODRAG,PTNUMCOR";
               ,"_Curs_TMPTPAR_TITE")
        else
          select * from (i_cTable);
           where PTNUMEFF<>-1;
           order by PTDATSCA, PNTIPCON, PNCODCON, PTMODPAG, PTBANAPP, PTBANNOS, PTCODRAG,PTNUMCOR;
            into cursor _Curs_TMPTPAR_TITE
        endif
        if used('_Curs_TMPTPAR_TITE')
          select _Curs_TMPTPAR_TITE
          locate for 1=1
          do while not(eof())
          this.w_PTSERIAL = _Curs_TMPTPAR_TITE.PTSERIAL
          this.w_PTROWORD = _Curs_TMPTPAR_TITE.PTROWORD
          this.w_PTROWNUM = _Curs_TMPTPAR_TITE.CPROWNUM
          * --- Campo di Rottura in caso di Raggruppamento Effetti
          this.w_NEWREC = DTOC(CP_TODATE(_Curs_TMPTPAR_TITE.PTDATSCA))+NVL(_Curs_TMPTPAR_TITE.PNTIPCON," ")+NVL(_Curs_TMPTPAR_TITE.PNCODCON,SPACE(15))+NVL(_Curs_TMPTPAR_TITE.PTMODPAG,SPACE(10))+NVL(_Curs_TMPTPAR_TITE.PTNUMCOR,SPACE(25))
          this.w_NEWREC = this.w_NEWREC+IIF(this.w_FLBANC="A", NVL(_Curs_TMPTPAR_TITE.PTBANAPP,SPACE(10)), SPACE(10))
          this.w_NEWREC = this.w_NEWREC+IIF(this.w_FLBANC="S", NVL(_Curs_TMPTPAR_TITE.PTBANNOS,SPACE(15)), SPACE(15)) +NVL(_Curs_TMPTPAR_TITE.PTCODRAG," ")
          if Empty(NVL(_Curs_TMPTPAR_TITE.ANFLRAGG," ")) OR this.w_NEWREC<>this.w_OLDREC OR this.w_TIPDISM="CA"
            this.w_PROGRE = this.w_PROGRE+1
            this.w_OLDREC = this.w_NEWREC
          endif
          * --- Write into TMPTPAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPTPAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPTPAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTNUMEFF ="+cp_NullLink(cp_ToStrODBC(this.w_PROGRE),'TMPTPAR_TITE','PTNUMEFF');
                +i_ccchkf ;
            +" where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTROWNUM);
                   )
          else
            update (i_cTable) set;
                PTNUMEFF = this.w_PROGRE;
                &i_ccchkf. ;
             where;
                PTSERIAL = this.w_PTSERIAL;
                and PTROWORD = this.w_PTROWORD;
                and CPROWNUM = this.w_PTROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
            select _Curs_TMPTPAR_TITE
            continue
          enddo
          use
        endif
        * --- Azzero numero effetto per gli effetti a 0..
        * --- Write into TMPTPAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPTPAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPTPAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTNUMEFF ="+cp_NullLink(cp_ToStrODBC(0),'TMPTPAR_TITE','PTNUMEFF');
              +i_ccchkf ;
          +" where ";
              +"PTNUMEFF = "+cp_ToStrODBC(-1);
                 )
        else
          update (i_cTable) set;
              PTNUMEFF = 0;
              &i_ccchkf. ;
           where;
              PTNUMEFF = -1;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Costruisco il cursore risultante..
      vq_exec("QUERY\GSTE5QDI.VQR",this,"DISTINTE")
      * --- Rimuovo il temporaneo
      * --- Drop temporary table TMPTPAR_TITE
      i_nIdx=cp_GetTableDefIdx('TMPTPAR_TITE')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPTPAR_TITE')
      endif
    else
      vq_exec("QUERY\GSTE_QDI.VQR",this,"DISTINTE")
    endif
    * --- Controllo Preliminare di Congruita' C.F.P.I.
    if ! this.w_TIPDISM $ "SD-SE-SC"
       
 SELECT PNTIPCON, ANDESCRI, ANPERFIS, MAX(ANPARIVA) AS PARIVA, MAX(ANCODFIS) AS CODFIS ; 
 FROM DISTINTE ; 
 WHERE (NVL(ANPERFIS," ")="S" AND EMPTY(NVL(ANCODFIS," "))) ; 
 OR (NVL(ANPERFIS," ")<>"S" AND EMPTY(NVL(ANPARIVA," "))) ; 
 GROUP BY PNTIPCON, ANDESCRI, ANPERFIS INTO CURSOR TEMP NOFILTER
      if RECCOUNT("Temp")>0
         
 Select Temp 
 Go Top 
 
        SCAN FOR NOT EMPTY(NVL(PNTIPCON," ")) AND NOT EMPTY(NVL(ANDESCRI," "))
        * --- Controlli sul codice fiscale e sulla partiva Iva.
        * --- Se il Cliente � una persona fisica deve essere presente il codice fiscale.
        * --- Se il Cliente non � una persona fisica allora deve essere presente la partita Iva
        if NVL(ANPERFIS,"")="S"
          if PNTIPCON="C"
            this.w_MESS = "Codice fiscale mancante per il cliente %1"
          else
            this.w_MESS = "Codice fiscale mancante per il fornitore %1"
          endif
          if EMPTY(NVL(CODFIS,""))
            ah_ErrorMsg(this.w_mess,"","", alltrim(ANDESCRI) )
          endif
        else
          if PNTIPCON="C"
            this.w_MESS = "Partita IVA mancante per il cliente %1"
          else
            this.w_MESS = "Partita IVA mancante per il fornitore %1"
          endif
          if EMPTY(NVL(PARIVA,""))
            ah_ErrorMsg(this.w_mess,"","", alltrim(ANDESCRI))
          endif
        endif
        ENDSCAN
      endif
      if used ("TEMP")
        select TEMP
        use
      endif
    endif
     
 Select Distinte 
 Go Top
    SCAN
    if NVL(this.w_FLDSPR, " ")="S" AND this.oParentObject.w_DESCEST="S"
      this.w_DESCRIZI = this.w_DESPAR
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if NOT EMPTY(this.w_DESCRIZI)
      Replace DESCRIZ with this.w_DESCRIZI
    endif
    * --- Controllo se il cliente ha una sede di tipo 'PAGAMENTO'
    do case
      case NVL(DDTIPRIF,"")="PA" AND ((NVL(CP_TODATE(DDDTOBSO),cp_CharToDate("  -  -  ")) > this.w_DATAVAL) OR EMPTY(NVL(CP_TODATE(DDDTOBSO),cp_CharToDate("  -  -  "))))
        * --- Sostituisco al valore dei campi dell'anagrafica del cliente il valore presente nelle sedi.
        REPLACE ANDESCRI with DDNOMDES
        REPLACE ANDESCR2 with " "
        REPLACE ANINDIRI with DDINDIRI
        REPLACE ANINDIR2 with " "
        REPLACE AN___CAP with DD___CAP
        REPLACE ANLOCALI with DDLOCALI
        REPLACE ANPROVIN with DDPROVIN
        REPLACE ANNAZION with DDCODNAZ
      case NVL(DDTIPRIF,"")<>"PA"
        this.w_TIPCON = PNTIPCON
        this.w_CODCON = PNCODCON
        * --- Effettuo una READ all'interno di Conti per leggere dai dati anagrafici la sede se non esiste una sede di pagamento
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPCON;
                and ANCODICE = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NOMDES = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
          this.w_INDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
          this.w_CAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
          this.w_LOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
          this.w_PROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
          this.w_CODNAZ = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT DISTINTE
        * --- Se esiste almeno un record effettuo delle replace sui dati anagrafici del cliente
        if i_Rows<>0
          REPLACE ANDESCRI with this.w_NOMDES
          REPLACE ANDESCR2 with " "
          REPLACE ANINDIRI with this.w_INDIRI
          REPLACE ANINDIR2 with " "
          REPLACE AN___CAP with this.w_CAP
          REPLACE ANLOCALI with this.w_LOCALI
          REPLACE ANPROVIN with this.w_PROVIN
          REPLACE ANNAZION with this.w_CODNAZ
        endif
    endcase
    this.w_APPO1 = NVL(PTBANNOS, SPACE(15))
    this.w_LIMPVAL = NVL(PTTOTIMP,0)
    this.w_LIMPCOD = NVL(PTCODVAL,SPACE(3))
    this.w_LIMPCAO = NVL(VACAOVAL,0)
    if this.w_FLBANC="S" AND NOT EMPTY(this.w_APPO1)
      * --- Dati per Nostra banca
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "BACODABI,BACODCAB,BADESCRI"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODBAN = "+cp_ToStrODBC(this.w_APPO1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          BACODABI,BACODCAB,BADESCRI;
          from (i_cTable) where;
              BACODBAN = this.w_APPO1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
        this.w_CODCAB = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
        this.w_DESBAN = NVL(cp_ToDate(_read_.BADESCRI),cp_NullValue(_read_.BADESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT DISTINTE
      REPLACE BADESBAN with this.w_DESBAN
      REPLACE BACODABI with this.w_CODABI
      REPLACE BACODCAB with this.w_CODCAB
    endif
    * --- Converte gli Importi EMU in EURO
    if this.oParentObject.w_FLEURO="S" AND PTCODVAL<>g_PERVAL AND NVL(VACAOVAL, 0)<>0
      this.w_APPO = VACAOVAL
      REPLACE PTTOTIMP WITH VAL2MON(PTTOTIMP, this.w_APPO,1, GETVALUT(PTCODVAL, "VADATEUR"), g_PERVAL)
      REPLACE PTCODVAL WITH g_PERVAL
    endif
    ENDSCAN
    if this.w_oERRORLOG.IsFullLog()
      if AH_YESNO("Stampo situazione messaggi di errore?") 
        this.w_oERRORLOG.PrintLog(this.oParentObject.oParentObject,"Errori riscontrati",.f.)     
      endif
    endif
    L_Ordinamento = "ORDER BY PTNUMEFF,PTDATSCA,PNTIPCON,PNCODCON,PTMODPAG,PTBANAPP,PTBANNOS"
    if this.oParentObject.w_DETDOCD="S"
      * --- Creazione del cursore per i totali
       
 Select PTNUMEFF As NUMEFF, Sum(PTTOTIMP) As TOTALE ; 
 From DISTINTE Group By PTNUMEFF Into Cursor TOTALI NoFilter
      * --- Se flag dettaglio documenti attivo vado a ricercare anche le origini di ogni partita (in caso sia stata accorpata manualmente)
      this.w_LSERIAL = repl("#",11)
      this.w_LTIPCON = "I"
      this.w_LCODCON = repl("#",15)
      this.w_LCHKRIF = "F"
      this.w_LORDINE = 0
      this.w_LSUBTOT = 0
      * --- Creazione cursore di appoggio vuoto
       
 Select * From Distinte Into Cursor Appo1 Where 1=0 NoFilter 
 =WrCursor("Appo1") 
 Select Distinte 
 Go Top
      SCAN
      this.w_LORDINE = this.w_LORDINE+1
       
 REPLACE DISTINTE.ORDINE WITH this.w_LORDINE 
 REPLACE DISTINTE.SUBTOT WITH DISTINTE.PTTOTIMP 
 REPLACE DISTINTE.ANCINABI WITH NVL(DISTINTE.ANCINABI,SPACE(1)) 
 REPLACE DISTINTE.an__iban WITH NVL(DISTINTE.an__iban,SPACE(35)) 
 REPLACE DISTINTE.an__bban WITH NVL(DISTINTE.an__bban,SPACE(30))
      * --- Se scadenza raggruppata recupero il dettaglio delle scadenze che l'hanno
      *     determinata..
      if NVL(PTFLRAGG," ")="S"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      Select Distinte
      ENDSCAN
      this.w_BRAG = RecCount("APPO1") >0
      * --- Se ho inserito scadenze da raggruppamenti allora le accodo logicamente al temporaneo..
      if this.w_BRAG
         
 Select * From DISTINTE ; 
 Union All ; 
 Select * From APPO1 into Cursor Appo NoFilter
        Use In Distinte
         
 Select * FROM APPO Into Cursor DISTINTE NoFilter
        * --- Ri rendo scrivibile il temporaneo
        CreaCur=WRCURSOR("DISTINTE")
      endif
      * --- Valorizzo il campo SUBTOT per avere il totale di ogni effetto su tutte le righe.
       
 Select Totali 
 Go Top
      SCAN
      this.w_LNUMEFF = TOTALI.NUMEFF
      this.w_LSUBTOT = ABS(TOTALI.TOTALE)
       
 Select Distinte 
 Go Top
      REPLACE SUBTOT WITH this.w_LSUBTOT FOR PTNUMEFF=this.w_LNUMEFF
       
 Select Totali
      EndScan
      if USED("APPO")
        SELECT APPO
        USE
      endif
      if USED("APPO1")
        SELECT APPO1
        USE
      endif
      if USED("TOTALI")
        SELECT TOTALI
        USE
      endif
      if this.w_BRAG
        * --- Riporto il risultato del cursore DISTINTE all'interno del curosre di stampa __TMP__
        * --- Nel caso di dettaglio documenti dopo al numero effetto viene ordinato 
        *     per il campo ORDINE aggiunto appositamente per il dettaglio partite di origine
        L_Ordinamento = "ORDER BY PTNUMEFF,ORDINE,PTDATSCA,PNTIPCON,PNCODCON,PTMODPAG,PTBANAPP,PTBANNOS "
        Select * From DISTINTE Where NVL(PTFLRAGG," ")<>"S" Into Cursor __TMP__ NoFilter
      else
        * --- Riporto il risultato del cursore DISTINTE all'interno del curosre di stampa __TMP__
        L_Ordinamento = "ORDER BY PTNUMEFF,PNNUMDOC,PNDATDOC,PTDATSCA,PNTIPCON,PNCODCON,PTMODPAG,PTBANAPP,PTBANNOS "
        Select * From DISTINTE Into Cursor __TMP__ NoFilter
      endif
    else
      * --- Riporto il risultato del cursore DISTINTE all'interno del curosre di stampa __TMP__
      *     L'ordine rispetta i Gruppi di Rottura x il Calcolo Effetti + n.Effetto (20)
      Select * From DISTINTE Where Nvl( PTTOTIMP , 0 )<>0 Into Cursor __TMP__ NoFilter
    endif
    if used ("DISTINTE")
      select DISTINTE
      use
    endif
    if this.oParentObject.w_DETDOCD="S"
      this.w_QUEREP = "QUERY\GSTE1QDI.FRX"
    else
      this.w_QUEREP = "QUERY\GSTE_QDI.FRX"
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- STAMPA DEFINITIVA
    *     Caso Distinta Ric.Bancarie, Tratte, RID: Aggiorno la Numerazione Effetti
    *     AGGIORNO I NUMERI EFFETTI
    this.w_PROGRE = this.w_PROSTA
    * --- La SELECT FROM distrugge il cursore __tmp__, devo per questo salvarlo
    *     in un cursore di appoggio..
    Select * From __Tmp__ Into Cursor _Tmp NoFilter
    * --- Creo il termporaneo che contiene le scadenze da aggiornare
    * --- Create temporary table TMPTPAR_TITE
    i_nIdx=cp_AddTableDef('TMPTPAR_TITE') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSTE2QDI',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPTPAR_TITE_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Elimino eventuali scadenze comprese in effetti con totale a 0
    *     (fatture e note di credito assieme)
    *     Se distinta di tipo cambiale/tratta no!
    if this.w_TIPDISM<>"CA"
      this.w_EFFF_ZERO = "S"
      * --- Delete from TMPTPAR_TITE
      i_nConn=i_TableProp[this.TMPTPAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".PTDATSCA = "+i_cQueryTable+".PTDATSCA";
              +" and "+i_cTable+".PNTIPCON = "+i_cQueryTable+".PNTIPCON";
              +" and "+i_cTable+".PNCODCON = "+i_cQueryTable+".PNCODCON";
              +" and "+i_cTable+".PTMODPAG = "+i_cQueryTable+".PTMODPAG";
              +" and "+i_cTable+".PTBANAPP = "+i_cQueryTable+".PTBANAPP";
              +" and "+i_cTable+".PTBANNOS = "+i_cQueryTable+".PTBANNOS";
              +" and "+i_cTable+".PTCODRAG = "+i_cQueryTable+".PTCODRAG";
              +" and "+i_cTable+".PTNUMCOR = "+i_cQueryTable+".PTNUMCOR";
      
        do vq_exec with 'GSTE2QDIB',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Rileggo le Scadenze della Distinta
    this.w_NEWREC = "XXX"
    this.w_OLDREC = "ZZZ"
    * --- ATTENZIONE all'ordinamento, all'interno della SELECT vi � una rottura..
    * --- Select from TMPTPAR_TITE
    i_nConn=i_TableProp[this.TMPTPAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2],.t.,this.TMPTPAR_TITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPTPAR_TITE ";
          +" order by PTDATSCA, PNTIPCON, PNCODCON, PTMODPAG, PTBANAPP, PTBANNOS, PTCODRAG,PTNUMCOR";
           ,"_Curs_TMPTPAR_TITE")
    else
      select * from (i_cTable);
       order by PTDATSCA, PNTIPCON, PNCODCON, PTMODPAG, PTBANAPP, PTBANNOS, PTCODRAG,PTNUMCOR;
        into cursor _Curs_TMPTPAR_TITE
    endif
    if used('_Curs_TMPTPAR_TITE')
      select _Curs_TMPTPAR_TITE
      locate for 1=1
      do while not(eof())
      this.w_PTSERIAL = _Curs_TMPTPAR_TITE.PTSERIAL
      this.w_PTROWORD = _Curs_TMPTPAR_TITE.PTROWORD
      this.w_PTROWNUM = _Curs_TMPTPAR_TITE.CPROWNUM
      * --- Campo di Rottura in caso di Raggruppamento Effetti
      * --- Campo di Rottura in caso di Raggruppamento Effetti
      this.w_NEWREC = DTOC(CP_TODATE(_Curs_TMPTPAR_TITE.PTDATSCA))+NVL(_Curs_TMPTPAR_TITE.PNTIPCON," ")+NVL(_Curs_TMPTPAR_TITE.PNCODCON,SPACE(15))+NVL(_Curs_TMPTPAR_TITE.PTMODPAG,SPACE(10))+Nvl(_Curs_TMPTPAR_TITE.PTNUMCOR,"")
      this.w_NEWREC = this.w_NEWREC+IIF(this.w_FLBANC="A", NVL(_Curs_TMPTPAR_TITE.PTBANAPP,SPACE(10)), SPACE(10))
      this.w_NEWREC = this.w_NEWREC+IIF(this.w_FLBANC="S", NVL(_Curs_TMPTPAR_TITE.PTBANNOS,SPACE(15)), SPACE(15)) +NVL(_Curs_TMPTPAR_TITE.PTCODRAG," ")
      if Empty(NVL(_Curs_TMPTPAR_TITE.ANFLRAGG," ")) OR this.w_NEWREC<>this.w_OLDREC OR this.w_TIPDISM="CA"
        this.w_PROGRE = this.w_PROGRE+1
        this.w_OLDREC = this.w_NEWREC
        * --- Conto quanti effetti verranno stampati in definitiva per poter calcolare le commissioni per singolo effetto
        this.w_CONTA = this.w_CONTA+1
      endif
      * --- Calcolo le commissioni date dal prodotto tra il numero di effetti generato e l'importo della commissione
      this.w_CONTAEF = this.w_COMEFF*this.w_CONTA
      * --- Scrivo il numero EFFETTO
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTNUMEFF ="+cp_NullLink(cp_ToStrODBC(this.w_PROGRE),'PAR_TITE','PTNUMEFF');
            +i_ccchkf ;
        +" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTROWNUM);
               )
      else
        update (i_cTable) set;
            PTNUMEFF = this.w_PROGRE;
            &i_ccchkf. ;
         where;
            PTSERIAL = this.w_PTSERIAL;
            and PTROWORD = this.w_PTROWORD;
            and CPROWNUM = this.w_PTROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_IDORDINE = RIGHT(REPLICATE("0",34)+ALLTRIM(STR(NVL(this.w_PROGRE,0))),34)
      * --- Write into DIS_BOES
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIS_BOES_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIS_BOES_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_BOES_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DIORDINE ="+cp_NullLink(cp_ToStrODBC(this.w_IDORDINE),'DIS_BOES','DIORDINE');
            +i_ccchkf ;
        +" where ";
            +"DINUMDIS = "+cp_ToStrODBC(this.w_PTSERIAL);
            +" and DIROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
            +" and DIROWNUM = "+cp_ToStrODBC(this.w_PTROWNUM);
               )
      else
        update (i_cTable) set;
            DIORDINE = this.w_IDORDINE;
            &i_ccchkf. ;
         where;
            DINUMDIS = this.w_PTSERIAL;
            and DIROWORD = this.w_PTROWORD;
            and DIROWNUM = this.w_PTROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_TMPTPAR_TITE
        continue
      enddo
      use
    endif
    * --- Rimuovo il temporaneo
    * --- Drop temporary table TMPTPAR_TITE
    i_nIdx=cp_GetTableDefIdx('TMPTPAR_TITE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTPAR_TITE')
    endif
    * --- Flaggo la distinta come definitiva
    * --- Write into DIS_TINT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIS_TINT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_TINT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DIFLDEFI ="+cp_NullLink(cp_ToStrODBC("S"),'DIS_TINT','DIFLDEFI');
      +",DICOSEFF ="+cp_NullLink(cp_ToStrODBC(this.w_CONTAEF),'DIS_TINT','DICOSEFF');
          +i_ccchkf ;
      +" where ";
          +"DINUMDIS = "+cp_ToStrODBC(this.oParentObject.w_NUMDISM);
             )
    else
      update (i_cTable) set;
          DIFLDEFI = "S";
          ,DICOSEFF = this.w_CONTAEF;
          &i_ccchkf. ;
       where;
          DINUMDIS = this.oParentObject.w_NUMDISM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorno il progressivo effetti
    * --- Try
    local bErr_04479268
    bErr_04479268=bTrsErr
    this.Try_04479268()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into PRO_NUME
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRO_NUME_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_NUME_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PRNUMPRO ="+cp_NullLink(cp_ToStrODBC(this.w_PROGRE),'PRO_NUME','PRNUMPRO');
            +i_ccchkf ;
        +" where ";
            +"PRCODAZI = "+cp_ToStrODBC(i_codazi);
            +" and PRCODESE = "+cp_ToStrODBC(g_codese);
            +" and PRTIPPAG = "+cp_ToStrODBC(this.w_TIPDISM);
            +" and PRTIPCON = "+cp_ToStrODBC(this.w_TIPOSCA);
               )
      else
        update (i_cTable) set;
            PRNUMPRO = this.w_PROGRE;
            &i_ccchkf. ;
         where;
            PRCODAZI = i_codazi;
            and PRCODESE = g_codese;
            and PRTIPPAG = this.w_TIPDISM;
            and PRTIPCON = this.w_TIPOSCA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_04479268
    * --- End
    * --- Ricreo il __Tmp__ (l'ordinamento � impostato a pag5 prima della CP_CHPRN
     
 Select * From _Tmp Into Cursor __Tmp__ NoFilter 
 Use In _Tmp 
 
  endproc
  proc Try_04479268()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRO_NUME
    i_nConn=i_TableProp[this.PRO_NUME_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRO_NUME_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODAZI"+",PRCODESE"+",PRTIPPAG"+",PRTIPCON"+",PRNUMPRO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(i_codazi),'PRO_NUME','PRCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(g_codese),'PRO_NUME','PRCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPDISM),'PRO_NUME','PRTIPPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPOSCA),'PRO_NUME','PRTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PROGRE),'PRO_NUME','PRNUMPRO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODAZI',i_codazi,'PRCODESE',g_codese,'PRTIPPAG',this.w_TIPDISM,'PRTIPCON',this.w_TIPOSCA,'PRNUMPRO',this.w_PROGRE)
      insert into (i_cTable) (PRCODAZI,PRCODESE,PRTIPPAG,PRTIPCON,PRNUMPRO &i_ccchkf. );
         values (;
           i_codazi;
           ,g_codese;
           ,this.w_TIPDISM;
           ,this.w_TIPOSCA;
           ,this.w_PROGRE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per mantenere gli estremi della partita raggruppata.
    this.w_LANDESC = NVL(ANDESCRI,SPACE(40))
    this.w_LANDES2 = NVL(ANDESCR2,SPACE(40))
    this.w_LINDIRI = NVL(ANINDIRI,SPACE(35))
    this.w_LINDIRI2 = NVL(ANINDIR2,SPACE(35))
    this.w_L_CAP = NVL(AN___CAP,SPACE(8))
    this.w_LLOCALI = NVL(ANLOCALI,SPACE(30))
    this.w_LPROVIN = NVL(ANPROVIN,SPACE(2))
    this.w_LNAZION = NVL(ANNAZION,SPACE(3))
    this.w_LCODABI = NVL(BACODABI,SPACE(5))
    this.w_LDESBAN = NVL(BADESBAN,SPACE(50))
    this.w_LCODCAB = NVL(BACODCAB,SPACE(5))
    this.w_LCONRIF = SPACE(15)
    this.w_LCHKRIF = "F"
    this.w_LDATASCA = CP_TODATE(PTDATSCA)
    this.w_NUMDOC = NVL(PNNUMDOC,0)
    this.w_ALFDOC = NVL(PNALFDOC,Space(10))
    this.w_DATDOC = CP_TODATE(PNDATDOC)
    * --- Riferimenti delle partite di origine nel caso fossero state raggruppate manualmente nella Manutenzione partite.
    this.w_LSEGNO = IIF(NVL(DISTINTE.PT_SEGNO, " ")="A","D","A")
    this.w_LTIPCON = DISTINTE.PNTIPCON
    this.w_LCODCON = DISTINTE.PNCODCON
    this.w_LMODPAG = DISTINTE.PTMODPAG
    this.w_LBANAPP = DISTINTE.PTBANAPP
    this.w_LBANNOS = DISTINTE.PTBANNOS
    this.w_LNUMDOC = 0
    this.w_LALFDOC = Space(10)
    this.w_LDATDOC = CP_TODATE("01-01-1960")
    this.w_LPERFIS = NVL(DISTINTE.ANPERFIS,SPACE(1))
    this.w_LCODFIS = NVL(DISTINTE.ANCODFIS,SPACE(16))
    this.w_LPARIVA = NVL(DISTINTE.ANPARIVA,SPACE(12))
    this.w_LANRAGG = NVL(DISTINTE.ANFLRAGG,SPACE(1))
    this.w_LNUMCOR = NVL(DISTINTE.ANNUMCOR,SPACE(25))
    this.w_LSCDES = SPACE(35)
    this.w_LDCDES = NVL(DISTINTE.DDCODDES,SPACE(5))
    this.w_LDNDES = NVL(DISTINTE.DDNOMDES,SPACE(40))
    this.w_LDINDI = NVL(DISTINTE.DDINDIRI,SPACE(35))
    this.w_LD_CAP = NVL(DISTINTE.DD___CAP,SPACE(8))
    this.w_LDLOCAL = NVL(DISTINTE.DDLOCALI,SPACE(30))
    this.w_LDPROVI = NVL(DISTINTE.DDPROVIN,SPACE(2))
    this.w_LDNAZ = NVL(DISTINTE.DDCODNAZ,SPACE(3))
    this.w_LDTIPRIF = NVL(DISTINTE.DDTIPRIF,SPACE(1))
    this.w_PTSERRIF = NVL(DISTINTE.PTSERRIF,SPACE(10))
    this.w_PTORDRIF = NVL(DISTINTE.PTORDRIF,0)
    this.w_PTNUMRIF = NVL(DISTINTE.PTNUMRIF,0)
    if this.oParentObject.w_FLEURO="S" AND this.w_LIMPCOD<>g_PERVAL AND this.w_LIMPCAO<>0
      this.w_LTOTIMP = this.w_LIMPVAL
    else
      this.w_LTOTIMP = ABS(NVL(PTTOTIMP,0))
    endif
    this.w_LNUMEFF = DISTINTE.PTNUMEFF
    this.w_LNUMPAR = NVL(PTNUMPAR,SPACE(31))
    this.w_LDATSCA = CP_TODATE(PTDATSCA)
    this.w_LTIPCON = NVL(PNTIPCON," ")
    this.w_LCODCON = NVL(PNCODCON,SPACE(15))
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCONRIF"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_LTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_LCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCONRIF;
        from (i_cTable) where;
            ANTIPCON = this.w_LTIPCON;
            and ANCODICE = this.w_LCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LCONRIF = NVL(cp_ToDate(_read_.ANCONRIF),cp_NullValue(_read_.ANCONRIF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_LCONRIF)
      * --- Nel caso di Cliente con fornitore associato valorizzo LCHKRIF a'T' e la utilizo nella query
      *     in modo che non faccia filtro solo sulle partite dei clienti poich� potrebbero essere state accorpate 
      *     anche partite del fornitore
      this.w_LCHKRIF = "T"
    endif
    if Not Empty(this.w_PTSERRIF)
      this.w_LSERIAL = NVL(DISTINTE.PTSERRIF,SPACE(10))
      if this.w_PTORDRIF=-3
        * --- Nel caso di indittte effetti devo rintracciare la partita di creazione 
        *     collegata alla relativa manutenzione per risalire alla raggruppata
        * --- Read from PAR_TITE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PTSERRIF"+;
            " from "+i_cTable+" PAR_TITE where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERRIF);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_PTORDRIF);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTNUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PTSERRIF;
            from (i_cTable) where;
                PTSERIAL = this.w_PTSERRIF;
                and PTROWORD = this.w_PTORDRIF;
                and CPROWNUM = this.w_PTNUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LSERIAL = NVL(cp_ToDate(_read_.PTSERRIF),cp_NullValue(_read_.PTSERRIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    else
      * --- Caso partite pregresso
      this.w_LSERIAL = repl("#",11)
      * --- Select from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PTSERIAL  from "+i_cTable+" PAR_TITE ";
            +" where PTNUMPAR="+cp_ToStrODBC(this.w_LNUMPAR)+" AND PTDATSCA="+cp_ToStrODBC(this.w_LDATSCA)+" AND PTTIPCON="+cp_ToStrODBC(this.w_LTIPCON)+" AND PTCODCON="+cp_ToStrODBC(this.w_LCODCON)+" AND PT_SEGNO="+cp_ToStrODBC(this.w_LSEGNO)+" AND PTROWORD <> -3";
             ,"_Curs_PAR_TITE")
      else
        select PTSERIAL from (i_cTable);
         where PTNUMPAR=this.w_LNUMPAR AND PTDATSCA=this.w_LDATSCA AND PTTIPCON=this.w_LTIPCON AND PTCODCON=this.w_LCODCON AND PT_SEGNO=this.w_LSEGNO AND PTROWORD <> -3;
          into cursor _Curs_PAR_TITE
      endif
      if used('_Curs_PAR_TITE')
        select _Curs_PAR_TITE
        locate for 1=1
        do while not(eof())
        this.w_LSERIAL = _Curs_PAR_TITE.PTSERIAL
          select _Curs_PAR_TITE
          continue
        enddo
        use
      endif
    endif
    if this.w_LSERIAL<>repl("#",11)
      vq_exec("QUERY\GSTESQDI.VQR",this,"APPO")
    endif
    if USED("APPO")
      this.w_LORDINE = this.w_LORDINE+1
      SELECT APPO
      GO TOP
      SCAN
      this.w_LNUMDOC = NVL(PNNUMDOC,0)
      this.w_LALFDOC = NVL(PNALFDOC,Space(10))
      this.w_LDATDOC = CP_TODATE(PNDATDOC)
      this.w_LNUMPAR = NVL(PTNUMPAR,SPACE(31))
      this.w_LTOTIMP = NVL(PTTOTIMP,0)
      this.w_LDATSCA = CP_TODATE(PTDATSCA)
      this.w_LCODVAL = NVL(PTCODVAL, SPACE(3))
      this.w_DATPAR = CP_TODATE(DATPAR)
      this.w_LNUMGRUP = 1
      this.w_LROWORD = NVL(PTROWORD,0)
      this.w_LROWNUM = CPROWNUM
      this.w_LFLRAGG = NVL(PTFLRAGG," ")
      this.w_LCAOVAL = NVL(VACAOVAL,0)
      this.w_LSEGNO = NVL(PT_SEGNO," ")
      this.w_LSERIAL = NVL(PTSERIAL,SPACE(10))
      this.w_LDTOBSO = CP_TODATE(DDDTOBSO)
      this.w_Lan__iban = NVL(an__iban,SPACE(35))
      this.w_Lan__bban = NVL(an__bban,SPACE(30))
      this.w_LANCINABI = NVL(ANCINABI,SPACE(1))
      * --- Converto gli importi EMU in Euro
      if this.oParentObject.w_FLEURO="S" AND this.w_LCODVAL<>g_PERVAL AND this.w_LCAOVAL<>0
        this.w_APPO = this.w_LCAOVAL
        this.w_LTOTIMP = VAL2MON(this.w_LTOTIMP, this.w_APPO,1, GETVALUT(PTCODVAL, "VADATEUR"), g_PERVAL)
        this.w_LCODVAL = g_PERVAL
      endif
      this.w_LSIMVAL = NVL(VASIMVAL,"")
       
 INSERT INTO APPO1; 
 (PTDATSCA, PNTIPCON, PNCODCON, PTMODPAG, PTBANAPP, PTBANNOS, ; 
 NUMGRUP, ANDESCRI, ANDESCR2, ANINDIRI, ANINDIR2, ; 
 AN___CAP, ANLOCALI, ANPROVIN, ANNAZION, ANPARIVA, ; 
 PTTOTIMP, PNNUMDOC, PNDATDOC, PTNUMEFF, BACODABI, BADESBAN, ; 
 PTSERIAL, PTROWORD, PT_SEGNO, CPROWNUM, SCDESCRI, BACODCAB, ; 
 PTCODVAL, ANFLRAGG, ANNUMCOR, DDCODDES, DDNOMDES, ; 
 DDINDIRI, DD___CAP, DDLOCALI, DDPROVIN, DDCODNAZ, DDTIPRIF, ; 
 ANPERFIS, ANCODFIS, VACAOVAL, PNALFDOC, PTFLRAGG, PTNUMPAR, ; 
 ORDINE,SUBTOT, DDDTOBSO, DATASCA, NUMDOC, ALFDOC, DATDOC,DATPAR,AN__IBAN,AN__BBAN,ANCINABI,VASIMVAL); 
 VALUES; 
 (this.w_LDATSCA, this.w_LTIPCON, this.w_LCODCON, this.w_LMODPAG, this.w_LBANAPP, this.w_LBANNOS, ; 
 this.w_LNUMGRUP, this.w_LANDESC, this.w_LANDES2, this.w_LINDIRI, this.w_LINDIRI2, ; 
 this.w_L_CAP, this.w_LLOCALI, this.w_LPROVIN, this.w_LNAZION, this.w_LPARIVA, ; 
 this.w_LTOTIMP, this.w_LNUMDOC, this.w_LDATDOC, this.w_LNUMEFF, this.w_LCODABI, this.w_LDESBAN, ; 
 this.w_LSERIAL, this.w_LROWORD, this.w_LSEGNO, this.w_LROWNUM, this.w_LSCDES, this.w_LCODCAB, ; 
 this.w_LCODVAL, this.w_LANRAGG, this.w_LNUMCOR, this.w_LDCDES, this.w_LDNDES, ; 
 this.w_LDINDI, this.w_LD_CAP, this.w_LDLOCAL, this.w_LDPROVI, this.w_LDNAZ, this.w_LDTIPRIF, ; 
 this.w_LPERFIS, this.w_LCODFIS, this.w_LCAOVAL, this.w_LALFDOC, this.w_LFLRAGG, this.w_LNUMPAR, ; 
 this.w_LORDINE,0, this.w_LDTOBSO, this.w_LDATASCA, this.w_NUMDOC, this.w_ALFDOC, this.w_DATDOC,this.w_DATPAR,this.w_LAN__IBAN,this.w_LAN__BBAN,this.w_LANCINABI,this.w_LSIMVAL)
      SELECT APPO
      ENDSCAN
    endif
    if USED("APPO")
      SELECT APPO
      USE
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if RecCount("__Tmp__")=0
      * --- nessun dato da stampare - ESCO
      ah_ErrorMsg("Non ci sono dati da stampare",,"")
    else
       
 L_progre=this.w_PROSTA 
 L_desdist=L_desdist+"  " + ah_Msgformat("Num: %1", Alltrim(STR(L_numero,6,0)) )+"   " 
 L_tipsca=this.w_TIPOSCA
      if this.w_TIPOSCA="F"
        * --- Stampa distinte scadenze passive
        L_DESDIST=ah_Msgformat("Distinta pagamenti %1", l_desdist)
      else
        * --- Stampa distinte scadenze attive ='C'
        L_DESDIST=ah_Msgformat("Distinta incassi %1", l_desdist)
      endif
      * --- Imposto ordinamenti al termporaneo di stampa..
      if this.oParentObject.w_FLGORDAT= "S"
        * --- Dettaglio documentio o meno..
        if this.oParentObject.w_DETDOCD="S"
          Select * From __TMP__ Order By ORDINE,PNDATDOC, PNNUMDOC Into Cursor __TMP__ NoFilter
        else
          Select * From __TMP__ Order By PNDATDOC,PNNUMDOC Into Cursor __TMP__ Nofilter
        endif
      else
        Select * From __Tmp__ &L_Ordinamento Into Cursor __Tmp__ NoFilter
      endif
      CP_CHPRN(this.w_QUEREP, " ", this)
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruisco descrizione parametrica
    this.w_nFindVar = AT("<", this.w_DESCRIZI)
    do while this.w_nFindVar>0
      this.w_nFindVarFin = AT(">", this.w_DESCRIZI)
      local w_VarCurs 
 w_VarCurs = "DISTINTE."+SUBSTR(this.w_DESCRIZI, this.w_nFindVar+1, this.w_nFindVarFin-this.w_nFindVar-1)
      do case
        case TYPE(w_VarCurs)="C"
          w_VarCurs = ALLTRIM( &w_VarCurs )
        case TYPE(w_VarCurs)="N"
          w_VarCurs = ALLTRIM( STR( &w_VarCurs ) )
        case TYPE(w_VarCurs)="D" OR TYPE(w_VarCurs)="T"
          w_VarCurs = ALLTRIM( DTOC( CP_TODATE( &w_VarCurs ) ) )
        otherwise
          w_VarCurs = ""
      endcase
      this.w_DESCRIZI = LEFT(this.w_DESCRIZI, this.w_nFindVar-1)+w_VarCurs+SUBSTR(this.w_DESCRIZI,this.w_nFindVarFin+1)
      this.w_nFindVar = AT("<", this.w_DESCRIZI)
    enddo
    this.w_PNAGG_01 = NVL(DISTINTE.PNAGG_01, space(15))
    this.w_PNAGG_02 = NVL(DISTINTE.PNAGG_02, space(15))
    this.w_PNAGG_03 = NVL(DISTINTE.PNAGG_03, space(15))
    this.w_PNAGG_04 = NVL(DISTINTE.PNAGG_04, space(15))
    this.w_PNAGG_05 = NVL(DISTINTE.PNAGG_05, cp_CharToDate("  -  -    "))
    this.w_PNAGG_06 = NVL(DISTINTE.PNAGG_06, cp_CharToDate("  -  -    "))
    this.w_PNNUMFAT = NVL(DISTINTE.PNNUMFAT, space(20))
    this.w_MAXAGG = ALLTRIM(NVL(DISTINTE.MAXAGG,""))
    this.w_MINAGG = ALLTRIM(NVL(DISTINTE.MINAGG,""))
    this.w_DESCRIZI = ah_msgformat(ALLTRIM(NVL(this.w_DESCRIZI,"")),alltrim(NVL(this.w_PNAGG_01,"")),alltrim(NVL(this.w_PNAGG_02,"")),alltrim(NVL(this.w_PNAGG_03,"")),alltrim(NVL(this.w_PNAGG_04,"")),alltrim(dtoc(CP_TODATE(NVL(this.w_PNAGG_05,CP_TODATE("  -  -  "))))),alltrim(dtoc(CP_TODATE(NVL(this.w_PNAGG_06,CP_TODATE("  -  -  "))))),alltrim(NVL(this.w_PNNUMFAT,"")))
    if this.w_MINAGG<>this.w_MAXAGG
      this.w_oERRORLOG.AddMsgLog(AH_MSGFORMAT("%1 %2 : non verr� riportata la descrizione parametrica per scadenze raggruppate nella distinta n.:%3",iif(NVL(DISTINTE.PNTIPCON," ")="C",Ah_MsgFormat("Cliente"),Ah_MsgFormat("Fornitore")),ALLTRIM(NVL(DISTINTE.PNCODCON,"")),this.oParentObject.w_NUMDISM))     
      this.w_DESCRIZI = SPACE(254)
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,14)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='DES_DIVE'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='PAR_TITE'
    this.cWorkTables[5]='DIS_TINT'
    this.cWorkTables[6]='PRO_NUME'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='CONTI'
    this.cWorkTables[9]='COC_MAST'
    this.cWorkTables[10]='CAU_DIST'
    this.cWorkTables[11]='BAN_CAST'
    this.cWorkTables[12]='*TMPTPAR_TITE'
    this.cWorkTables[13]='DIS_BOES'
    this.cWorkTables[14]='DIS_BECV'
    return(this.OpenAllTables(14))

  proc CloseCursors()
    if used('_Curs_DIS_BECV')
      use in _Curs_DIS_BECV
    endif
    if used('_Curs_TMPTPAR_TITE')
      use in _Curs_TMPTPAR_TITE
    endif
    if used('_Curs_TMPTPAR_TITE')
      use in _Curs_TMPTPAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
