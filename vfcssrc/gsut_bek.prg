* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bek                                                        *
*              Elaborazioni KPI                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-07-03                                                      *
* Last revis.: 2014-12-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bek",oParentObject,m.pOper)
return(i_retval)

define class tgsut_bek as StdBatch
  * --- Local variables
  pOper = space(5)
  w_PADRE = .NULL.
  w_oQRY = .NULL.
  w_oQLD = .NULL.
  w_QUERY = space(250)
  w_CODICE = space(10)
  w_AZIENDA = space(5)
  w_MSG = space(254)
  * --- WorkFile variables
  RESELKPI_idx=0
  LOGELKPI_idx=0
  ELABKPIM_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.pOper = Alltrim(Upper(this.pOper))
    this.w_PADRE = This.oParentObject
    do case
      case this.pOper=="VQR"
        if Not Empty(this.oParentObject.w_EK_QUERY)
          this.oParentObject.w_EK_QUERY = SYS(2014, this.oParentObject.w_EK_QUERY)
          i_designfile = Alltrim(this.oParentObject.w_EK_QUERY)
          * --- Ne leggo i parametri e li carico nel dettaglio
          if this.w_PADRE.NumRow()=0 OR ah_yesno("Cancellare le righe del dettaglio e importare i parametri della query selezionata?","?")
            if this.w_PADRE.NumRow()<>0
              this.w_PADRE.FirstRow()     
              do while Not this.w_PADRE.Eof_Trs()
                this.w_PADRE.DeleteRow()     
                this.w_PADRE.NextRow()     
              enddo
              this.oParentObject.w_CPROWORD = 10
            endif
            this.w_PADRE.FirstRow()     
            this.w_QUERY = FORCEEXT(this.oParentObject.w_EK_QUERY,"")
            this.w_oQRY = CreateObject("cpquery")
            this.w_oQLD = CreateObject("cpqueryloader")
            this.w_oQLD.LoadQuery(this.w_QUERY,this.w_oQRY,.null.)     
            if this.w_oQRY.nFilterParam>0
              this.w_PADRE.MarkPos()     
              Local l_i 
 For l_i=1 To This.w_oQRY.nFilterParam
              this.w_PADRE.AddRow()     
              this.oParentObject.w_EK_PARAM = Alltrim(this.w_oQRY.i_FilterParam[l_i,1])
              do case
                case Upper(Alltrim(this.w_oQRY.i_FilterParam[l_i,3]))=="CHAR"
                  this.oParentObject.w_EKVALORE = "''"
                case Upper(Alltrim(this.w_oQRY.i_FilterParam[l_i,3]))=="DATE"
                  this.oParentObject.w_EKVALORE = "{}"
                case Upper(Alltrim(this.w_oQRY.i_FilterParam[l_i,3]))=="NUMERIC"
                  this.oParentObject.w_EKVALORE = "0"
                otherwise
                  this.oParentObject.w_EKVALORE = ""
              endcase
              this.w_PADRE.SaveRow()     
              Endfor
              this.w_PADRE.RePos()     
            endif
            * --- preparo una riga per l'inserimento
            if this.w_PADRE.NumRow()=0
              this.w_PADRE.FirstRow()     
              this.w_PADRE.AddRow()     
              this.w_PADRE.Refresh()     
            endif
            this.w_oQRY.Destroy()     
            this.w_oQRY = .null.
            this.w_oQLD.Destroy()     
            this.w_oQLD = .null.
          endif
        endif
      case this.pOper=="DEL"
        this.w_AZIENDA = i_CODAZI
        * --- Cancello i risultati
        * --- Delete from RESELKPI
        i_nConn=i_TableProp[this.RESELKPI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"RKCODKPI = "+cp_ToStrODBC(this.oParentObject.w_EKCODICE);
                 )
        else
          delete from (i_cTable) where;
                RKCODKPI = this.oParentObject.w_EKCODICE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Cancello le registrazioni di log
        * --- Delete from LOGELKPI
        i_nConn=i_TableProp[this.LOGELKPI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOGELKPI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"LKCODKPI = "+cp_ToStrODBC(this.oParentObject.w_EKCODICE);
                +" and LKCODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
                 )
        else
          delete from (i_cTable) where;
                LKCODKPI = this.oParentObject.w_EKCODICE;
                and LKCODAZI = this.w_AZIENDA;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Setto a "Nessuno" la tipologia di benchmark dell'eleaborazioni che linkavano a quella che sto cancellando
        * --- Select from ELABKPIM
        i_nConn=i_TableProp[this.ELABKPIM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select EKCODICE  from "+i_cTable+" ELABKPIM ";
              +" where EKBENCOD="+cp_ToStrODBC(this.oParentObject.w_EKCODICE)+"";
               ,"_Curs_ELABKPIM")
        else
          select EKCODICE from (i_cTable);
           where EKBENCOD=this.oParentObject.w_EKCODICE;
            into cursor _Curs_ELABKPIM
        endif
        if used('_Curs_ELABKPIM')
          select _Curs_ELABKPIM
          locate for 1=1
          do while not(eof())
          this.w_CODICE = Alltrim(_Curs_ELABKPIM.EKCODICE)
          * --- Write into ELABKPIM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ELABKPIM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ELABKPIM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"EKBENCHM ="+cp_NullLink(cp_ToStrODBC("NUL"),'ELABKPIM','EKBENCHM');
            +",EKBENCOD ="+cp_NullLink(cp_ToStrODBC(""),'ELABKPIM','EKBENCOD');
                +i_ccchkf ;
            +" where ";
                +"EKCODICE = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                EKBENCHM = "NUL";
                ,EKBENCOD = "";
                &i_ccchkf. ;
             where;
                EKCODICE = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
            select _Curs_ELABKPIM
            continue
          enddo
          use
        endif
      case this.pOper=="CHKT"
        if this.oParentObject.w_EKTIPRES="T"
          this.oParentObject.w_EKTIPSIN = "NUL"
          if this.oParentObject.w_EKBENCHM=="AVG" Or this.oParentObject.w_EKBENCHM=="MED" Or this.oParentObject.w_EKBENCHM=="SUM"
            this.w_MSG = 'Tipo benchmark "'+iCase(this.oParentObject.w_EKBENCHM=="AVG","Media",this.oParentObject.w_EKBENCHM=="MED","Mediana","Somma")+'" non compatibile con il tipo risultato "Testo"'
            this.w_MSG = this.w_MSG+CHR(10)+CHR(13)+CHR(10)+CHR(13)+"Le tipologie non compatibili sono:"
            this.w_MSG = this.w_MSG+CHR(10)+CHR(13)+"Media"
            this.w_MSG = this.w_MSG+CHR(10)+CHR(13)+"Mediana"
            this.w_MSG = this.w_MSG+CHR(10)+CHR(13)+"Somma"
            Ah_ErrorMsg(this.w_MSG,"!")
            this.oParentObject.w_EKBENCHM = "NUL"
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='RESELKPI'
    this.cWorkTables[2]='LOGELKPI'
    this.cWorkTables[3]='ELABKPIM'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_ELABKPIM')
      use in _Curs_ELABKPIM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
