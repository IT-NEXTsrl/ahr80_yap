* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsutabld                                                        *
*              Letture tabella par_vdet                                        *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_4]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-27                                                      *
* Last revis.: 2007-07-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNUMREAD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsutabld",oParentObject,m.pNUMREAD)
return(i_retval)

define class tgsutabld as StdBatch
  * --- Local variables
  pNUMREAD = space(1)
  * --- WorkFile variables
  PAR_VDET_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine di appoggio alla routine GSUT_BLD, creata per ovviare al problema della definizione
    *     relativa alla tabella "PAR_VDET" all'interno delle 'Work Tables', volutamente sono state riportate le varie read
    do case
      case this.pNUMREAD="1"
        * --- Read presente a pagina 1 routine GSUT_BLD
        * --- Read from PAR_VDET
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_VDET_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PAFLCPOS,PAMASPOS,PACODNEG,PAFLESSC,PADATESC"+;
            " from "+i_cTable+" PAR_VDET where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                +" and PAPRINEG = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PAFLCPOS,PAMASPOS,PACODNEG,PAFLESSC,PADATESC;
            from (i_cTable) where;
                PACODAZI = i_codazi;
                and PAPRINEG = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          g_FLCPOS = NVL(cp_ToDate(_read_.PAFLCPOS),cp_NullValue(_read_.PAFLCPOS))
          g_MASPOS = NVL(cp_ToDate(_read_.PAMASPOS),cp_NullValue(_read_.PAMASPOS))
          g_CODNEG = NVL(cp_ToDate(_read_.PACODNEG),cp_NullValue(_read_.PACODNEG))
          g_FLESSC = NVL(cp_ToDate(_read_.PAFLESSC),cp_NullValue(_read_.PAFLESSC))
          g_DATESC = NVL(cp_ToDate(_read_.PADATESC),cp_NullValue(_read_.PADATESC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pNUMREAD="2"
        * --- Read presente a pagina 2 della routine GSUT_BLD, ramo then dell'istruzione:
        *     Not Empty(g_CODNEG)
        * --- Imposto le variabili relative al Flag codifica numerica e alla maschera per il codice cliente
        * --- Read from PAR_VDET
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_VDET_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PAFLCPOS,PAMASPOS,PAFLESSC,PADATESC"+;
            " from "+i_cTable+" PAR_VDET where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                +" and PACODNEG = "+cp_ToStrODBC(g_CODNEG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PAFLCPOS,PAMASPOS,PAFLESSC,PADATESC;
            from (i_cTable) where;
                PACODAZI = i_codazi;
                and PACODNEG = g_CODNEG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          g_FLCPOS = NVL(cp_ToDate(_read_.PAFLCPOS),cp_NullValue(_read_.PAFLCPOS))
          g_MASPOS = NVL(cp_ToDate(_read_.PAMASPOS),cp_NullValue(_read_.PAMASPOS))
          g_FLESSC = NVL(cp_ToDate(_read_.PAFLESSC),cp_NullValue(_read_.PAFLESSC))
          g_DATESC = NVL(cp_ToDate(_read_.PADATESC),cp_NullValue(_read_.PADATESC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pNUMREAD="3"
        * --- Read presente a pagina 2 della routine GSUT_BLD, ramo else dell'istruzione:
        *     Not Empty(g_CODNEG)
        * --- Read from PAR_VDET
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_VDET_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PACODNEG,PAFLCPOS,PAMASPOS,PAFLESSC,PADATESC"+;
            " from "+i_cTable+" PAR_VDET where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                +" and PAPRINEG = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PACODNEG,PAFLCPOS,PAMASPOS,PAFLESSC,PADATESC;
            from (i_cTable) where;
                PACODAZI = i_codazi;
                and PAPRINEG = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          g_CODNEG = NVL(cp_ToDate(_read_.PACODNEG),cp_NullValue(_read_.PACODNEG))
          g_FLCPOS = NVL(cp_ToDate(_read_.PAFLCPOS),cp_NullValue(_read_.PAFLCPOS))
          g_MASPOS = NVL(cp_ToDate(_read_.PAMASPOS),cp_NullValue(_read_.PAMASPOS))
          g_FLESSC = NVL(cp_ToDate(_read_.PAFLESSC),cp_NullValue(_read_.PAFLESSC))
          g_DATESC = NVL(cp_ToDate(_read_.PADATESC),cp_NullValue(_read_.PADATESC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pNUMREAD="4"
        * --- Read presente a pagina 3 della routine GSUT_BLD, ramo then dell'istruzione:
        *     !Empty(g_CODNEG)
        * --- Se esiste il codice negozio associato alla macchina leggo anche il flag codifica numerica cliente
        *     e la maschera relativa al codice cliente........
        * --- Read from PAR_VDET
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_VDET_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PAFLCPOS,PAMASPOS,PAFLESSC,PADATESC"+;
            " from "+i_cTable+" PAR_VDET where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                +" and PACODNEG = "+cp_ToStrODBC(g_codneg);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PAFLCPOS,PAMASPOS,PAFLESSC,PADATESC;
            from (i_cTable) where;
                PACODAZI = i_codazi;
                and PACODNEG = g_codneg;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          g_FLCPOS = NVL(cp_ToDate(_read_.PAFLCPOS),cp_NullValue(_read_.PAFLCPOS))
          g_MASPOS = NVL(cp_ToDate(_read_.PAMASPOS),cp_NullValue(_read_.PAMASPOS))
          g_FLESSC = NVL(cp_ToDate(_read_.PAFLESSC),cp_NullValue(_read_.PAFLESSC))
          g_DATESC = NVL(cp_ToDate(_read_.PADATESC),cp_NullValue(_read_.PADATESC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pNUMREAD="5"
        * --- ......altrimenti definisco come codice negozio quello con il flag Negozio Predefinito attivato
        * --- Read from PAR_VDET
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_VDET_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PAFLCPOS,PAMASPOS,PACODNEG,PAFLESSC,PADATESC"+;
            " from "+i_cTable+" PAR_VDET where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                +" and PAPRINEG = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PAFLCPOS,PAMASPOS,PACODNEG,PAFLESSC,PADATESC;
            from (i_cTable) where;
                PACODAZI = i_codazi;
                and PAPRINEG = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          g_FLCPOS = NVL(cp_ToDate(_read_.PAFLCPOS),cp_NullValue(_read_.PAFLCPOS))
          g_MASPOS = NVL(cp_ToDate(_read_.PAMASPOS),cp_NullValue(_read_.PAMASPOS))
          g_CODNEG = NVL(cp_ToDate(_read_.PACODNEG),cp_NullValue(_read_.PACODNEG))
          g_FLESSC = NVL(cp_ToDate(_read_.PAFLESSC),cp_NullValue(_read_.PAFLESSC))
          g_DATESC = NVL(cp_ToDate(_read_.PADATESC),cp_NullValue(_read_.PADATESC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
    endcase
    * --- Replico il comportamento della Blackbox
    g_MASPOS = IIF(EMPTY(g_MASPOS), REPL("X", 15), ALLTRIM(g_MASPOS))
  endproc


  proc Init(oParentObject,pNUMREAD)
    this.pNUMREAD=pNUMREAD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_VDET'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNUMREAD"
endproc
