* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mtr                                                        *
*              Struttura bilancio generale                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39] [VRS_96]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-14                                                      *
* Last revis.: 2012-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mtr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mtr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mtr")
  return

* --- Class definition
define class tgscg_mtr as StdPCForm
  Width  = 801
  Height = 358
  Top    = 19
  Left   = 5
  cComment = "Struttura bilancio generale"
  cPrg = "gscg_mtr"
  HelpContextID=80356713
  add object cnt as tcgscg_mtr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mtr as PCContext
  w_TIPGES = space(1)
  w_TRCODICE = space(15)
  w_TRDESCRI = space(40)
  w_TR__NOTE = space(10)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_TRSEQCAL = 0
  w_TRTIPDET = space(1)
  w_TRTIPVOC = space(1)
  w_TRDESDET = space(50)
  w_TR__COL1 = space(1)
  w_TR__COL2 = space(1)
  w_TR__COL3 = space(1)
  w_TC__FONT = space(1)
  w_TC__FTST = space(1)
  w_TC__COLO = 0
  w_TC__INDE = 0
  w_TR__FLAG = space(1)
  w_TR_RIFUE = space(1)
  w_RTIPGES = space(1)
  proc Save(i_oFrom)
    this.w_TIPGES = i_oFrom.w_TIPGES
    this.w_TRCODICE = i_oFrom.w_TRCODICE
    this.w_TRDESCRI = i_oFrom.w_TRDESCRI
    this.w_TR__NOTE = i_oFrom.w_TR__NOTE
    this.w_CPROWNUM = i_oFrom.w_CPROWNUM
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_TRSEQCAL = i_oFrom.w_TRSEQCAL
    this.w_TRTIPDET = i_oFrom.w_TRTIPDET
    this.w_TRTIPVOC = i_oFrom.w_TRTIPVOC
    this.w_TRDESDET = i_oFrom.w_TRDESDET
    this.w_TR__COL1 = i_oFrom.w_TR__COL1
    this.w_TR__COL2 = i_oFrom.w_TR__COL2
    this.w_TR__COL3 = i_oFrom.w_TR__COL3
    this.w_TC__FONT = i_oFrom.w_TC__FONT
    this.w_TC__FTST = i_oFrom.w_TC__FTST
    this.w_TC__COLO = i_oFrom.w_TC__COLO
    this.w_TC__INDE = i_oFrom.w_TC__INDE
    this.w_TR__FLAG = i_oFrom.w_TR__FLAG
    this.w_TR_RIFUE = i_oFrom.w_TR_RIFUE
    this.w_RTIPGES = i_oFrom.w_RTIPGES
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TIPGES = this.w_TIPGES
    i_oTo.w_TRCODICE = this.w_TRCODICE
    i_oTo.w_TRDESCRI = this.w_TRDESCRI
    i_oTo.w_TR__NOTE = this.w_TR__NOTE
    i_oTo.w_CPROWNUM = this.w_CPROWNUM
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_TRSEQCAL = this.w_TRSEQCAL
    i_oTo.w_TRTIPDET = this.w_TRTIPDET
    i_oTo.w_TRTIPVOC = this.w_TRTIPVOC
    i_oTo.w_TRDESDET = this.w_TRDESDET
    i_oTo.w_TR__COL1 = this.w_TR__COL1
    i_oTo.w_TR__COL2 = this.w_TR__COL2
    i_oTo.w_TR__COL3 = this.w_TR__COL3
    i_oTo.w_TC__FONT = this.w_TC__FONT
    i_oTo.w_TC__FTST = this.w_TC__FTST
    i_oTo.w_TC__COLO = this.w_TC__COLO
    i_oTo.w_TC__INDE = this.w_TC__INDE
    i_oTo.w_TR__FLAG = this.w_TR__FLAG
    i_oTo.w_TR_RIFUE = this.w_TR_RIFUE
    i_oTo.w_RTIPGES = this.w_RTIPGES
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mtr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 801
  Height = 358
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-07"
  HelpContextID=80356713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  TIR_MAST_IDX = 0
  TIR_DETT_IDX = 0
  VOCIRICL_IDX = 0
  RIGTOTAL_IDX = 0
  ESERCIZI_IDX = 0
  cFile = "TIR_MAST"
  cFileDetail = "TIR_DETT"
  cKeySelect = "TRCODICE"
  cKeyWhere  = "TRCODICE=this.w_TRCODICE"
  cKeyDetail  = "TRCODICE=this.w_TRCODICE and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';

  cKeyDetailWhereODBC = '"TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"TIR_DETT.TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'TIR_DETT.CPROWORD '
  cPrg = "gscg_mtr"
  cComment = "Struttura bilancio generale"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPGES = space(1)
  w_TRCODICE = space(15)
  w_TRDESCRI = space(40)
  w_TR__NOTE = space(0)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_TRSEQCAL = 0
  w_TRTIPDET = space(1)
  o_TRTIPDET = space(1)
  w_TRTIPVOC = space(1)
  w_TRDESDET = space(50)
  w_TR__COL1 = space(1)
  o_TR__COL1 = space(1)
  w_TR__COL2 = space(1)
  o_TR__COL2 = space(1)
  w_TR__COL3 = space(1)
  o_TR__COL3 = space(1)
  w_TC__FONT = space(1)
  w_TC__FTST = space(1)
  w_TC__COLO = 0
  w_TC__INDE = 0
  w_TR__FLAG = space(1)
  w_TR_RIFUE = space(1)
  w_RTIPGES = space(1)

  * --- Children pointers
  GSAR_MRT = .NULL.
  GSCG_AVR = .NULL.
  GSCG_MAS = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSAR_MRT additive
    *set procedure to GSCG_AVR additive
    *set procedure to GSCG_MAS additive
    with this
      .Pages(1).addobject("oPag","tgscg_mtrPag1","gscg_mtr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRDESCRI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSAR_MRT
    *release procedure GSCG_AVR
    *release procedure GSCG_MAS
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='VOCIRICL'
    this.cWorkTables[2]='RIGTOTAL'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='TIR_MAST'
    this.cWorkTables[5]='TIR_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIR_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIR_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MRT = CREATEOBJECT('stdLazyChild',this,'GSAR_MRT')
    this.GSCG_AVR = CREATEOBJECT('stdLazyChild',this,'GSCG_AVR')
    this.GSCG_MAS = CREATEOBJECT('stdLazyChild',this,'GSCG_MAS')
    return

    procedure NewContext()
      return(createobject('tsgscg_mtr'))

    function DestroyChildrenChain()
      this.oParentObject=.NULL.
      if !ISNULL(this.GSAR_MRT)
        this.GSAR_MRT.DestroyChildrenChain()
      endif
      if !ISNULL(this.GSCG_AVR)
        this.GSCG_AVR.DestroyChildrenChain()
      endif
      if !ISNULL(this.GSCG_MAS)
        this.GSCG_MAS.DestroyChildrenChain()
      endif
      return

    function HideChildrenChain()
      *this.Hide()
      this.bOnScreen = .f.
      this.GSAR_MRT.HideChildrenChain()
      this.GSCG_AVR.HideChildrenChain()
      this.GSCG_MAS.HideChildrenChain()
      return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MRT)
      this.GSAR_MRT.DestroyChildrenChain()
      this.GSAR_MRT=.NULL.
    endif
    if !ISNULL(this.GSCG_AVR)
      this.GSCG_AVR.DestroyChildrenChain()
      this.GSCG_AVR=.NULL.
    endif
    if !ISNULL(this.GSCG_MAS)
      this.GSCG_MAS.DestroyChildrenChain()
      this.GSCG_MAS=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MRT.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AVR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MAS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MRT.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AVR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MAS.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MRT.NewDocument()
    this.GSCG_AVR.NewDocument()
    this.GSCG_MAS.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSAR_MRT.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_TRCODICE,"RTCODICE";
             ,.w_CPROWNUM,"RTNUMRIG";
             )
      .GSCG_AVR.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_TRCODICE,"VRCODVOC";
             ,.w_CPROWNUM,"VRROWORD";
             )
      .GSCG_MAS.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_TRCODICE,"TRCODVOC";
             ,.w_CPROWNUM,"TRROWORD";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from TIR_MAST where TRCODICE=KeySet.TRCODICE
    *
    i_nConn = i_TableProp[this.TIR_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2],this.bLoadRecFilter,this.TIR_MAST_IDX,"gscg_mtr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIR_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIR_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"TIR_DETT.","TIR_MAST.")
      i_cTable = i_cTable+' TIR_MAST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRCODICE',this.w_TRCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_TIPGES = This.oparentobject.pTIPGES
        .w_TRCODICE = NVL(TRCODICE,space(15))
        .w_TRDESCRI = NVL(TRDESCRI,space(40))
        .w_TR__NOTE = NVL(TR__NOTE,space(0))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TIR_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from TIR_DETT where TRCODICE=KeySet.TRCODICE
      *                            and CPROWNUM=KeySet.CPROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.TIR_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIR_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('TIR_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "TIR_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" TIR_DETT"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'TRCODICE',this.w_TRCODICE  )
        select * from (i_cTable) TIR_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_TRSEQCAL = NVL(TRSEQCAL,0)
          .w_TRTIPDET = NVL(TRTIPDET,space(1))
          .w_TRTIPVOC = NVL(TRTIPVOC,space(1))
          .w_TRDESDET = NVL(TRDESDET,space(50))
          .w_TR__COL1 = NVL(TR__COL1,space(1))
          .w_TR__COL2 = NVL(TR__COL2,space(1))
          .w_TR__COL3 = NVL(TR__COL3,space(1))
          .w_TC__FONT = NVL(TC__FONT,space(1))
          .w_TC__FTST = NVL(TC__FTST,space(1))
          .w_TC__COLO = NVL(TC__COLO,0)
          .w_TC__INDE = NVL(TC__INDE,0)
          .w_TR__FLAG = NVL(TR__FLAG,space(1))
          .w_TR_RIFUE = NVL(TR_RIFUE,space(1))
        .w_RTIPGES = This.oparentobject.pTIPGES
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_TIPGES = This.oparentobject.pTIPGES
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_8.enabled = .oPgFrm.Page1.oPag.oBtn_1_8.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_mtr
    if this.bLoaded
    with this
    .oPgFrm.Page1.oPag.oBody.SetFullFocus()
    endwith
    endif
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TIPGES=space(1)
      .w_TRCODICE=space(15)
      .w_TRDESCRI=space(40)
      .w_TR__NOTE=space(0)
      .w_CPROWNUM=0
      .w_CPROWORD=10
      .w_TRSEQCAL=0
      .w_TRTIPDET=space(1)
      .w_TRTIPVOC=space(1)
      .w_TRDESDET=space(50)
      .w_TR__COL1=space(1)
      .w_TR__COL2=space(1)
      .w_TR__COL3=space(1)
      .w_TC__FONT=space(1)
      .w_TC__FTST=space(1)
      .w_TC__COLO=0
      .w_TC__INDE=0
      .w_TR__FLAG=space(1)
      .w_TR_RIFUE=space(1)
      .w_RTIPGES=space(1)
      if .cFunction<>"Filter"
        .w_TIPGES = This.oparentobject.pTIPGES
        .w_TRCODICE = 'BILANCOGE'
        .DoRTCalc(3,6,.f.)
        .w_TRSEQCAL = .w_CPROWORD
        .w_TRTIPDET = 'V'
        .w_TRTIPVOC = 'M'
        .DoRTCalc(10,10,.f.)
        .w_TR__COL1 = ' '
        .w_TR__COL2 = ' '
        .w_TR__COL3 = ' '
        .w_TC__FONT = 'A'
        .w_TC__FTST = 'N'
        .w_TC__COLO = 0
        .DoRTCalc(17,17,.f.)
        .w_TR__FLAG = .w_TRTIPDET
        .w_TR_RIFUE = IIF(.w_TRTIPDET<>'V','N',.w_TR_RIFUE)
        .w_RTIPGES = This.oparentobject.pTIPGES
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIR_MAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oTRDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oTR__NOTE_1_4.enabled = i_bVal
      .Page1.oPag.oTC__FONT_2_11.enabled = i_bVal
      .Page1.oPag.oTC__FTST_2_12.enabled = i_bVal
      .Page1.oPag.oTC__COLO_2_13.enabled = i_bVal
      .Page1.oPag.oTC__INDE_2_14.enabled = i_bVal
      .Page1.oPag.oTR_RIFUE_2_18.enabled = i_bVal
      .Page1.oPag.oBtn_1_8.enabled = .Page1.oPag.oBtn_1_8.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSAR_MRT.SetStatus(i_cOp)
    this.GSCG_AVR.SetStatus(i_cOp)
    this.GSCG_MAS.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'TIR_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MRT.SetChildrenStatus(i_cOp)
  *  this.GSCG_AVR.SetChildrenStatus(i_cOp)
  *  this.GSCG_MAS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIR_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODICE,"TRCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRDESCRI,"TRDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TR__NOTE,"TR__NOTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_TRSEQCAL N(5);
      ,t_TRTIPDET N(3);
      ,t_TRDESDET C(50);
      ,t_TR__COL1 N(3);
      ,t_TR__COL2 N(3);
      ,t_TR__COL3 N(3);
      ,t_TC__FONT N(3);
      ,t_TC__FTST N(3);
      ,t_TC__COLO N(3);
      ,t_TC__INDE N(3);
      ,t_TR_RIFUE N(3);
      ,CPROWNUM N(10);
      ,t_CPROWNUM N(5);
      ,t_TRTIPVOC C(1);
      ,t_TR__FLAG C(1);
      ,t_RTIPGES C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mtrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRSEQCAL_2_3.controlsource=this.cTrsName+'.t_TRSEQCAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDET_2_4.controlsource=this.cTrsName+'.t_TRTIPDET'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRDESDET_2_6.controlsource=this.cTrsName+'.t_TRDESDET'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL1_2_7.controlsource=this.cTrsName+'.t_TR__COL1'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL2_2_8.controlsource=this.cTrsName+'.t_TR__COL2'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL3_2_9.controlsource=this.cTrsName+'.t_TR__COL3'
    this.oPgFRm.Page1.oPag.oTC__FONT_2_11.controlsource=this.cTrsName+'.t_TC__FONT'
    this.oPgFRm.Page1.oPag.oTC__FTST_2_12.controlsource=this.cTrsName+'.t_TC__FTST'
    this.oPgFRm.Page1.oPag.oTC__COLO_2_13.controlsource=this.cTrsName+'.t_TC__COLO'
    this.oPgFRm.Page1.oPag.oTC__INDE_2_14.controlsource=this.cTrsName+'.t_TC__INDE'
    this.oPgFRm.Page1.oPag.oTR_RIFUE_2_18.controlsource=this.cTrsName+'.t_TR_RIFUE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(53)
    this.AddVLine(109)
    this.AddVLine(228)
    this.AddVLine(581)
    this.AddVLine(611)
    this.AddVLine(639)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIR_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIR_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIR_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'TIR_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(TRCODICE,TRDESCRI,TR__NOTE"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_TRCODICE)+;
                    ","+cp_ToStrODBC(this.w_TRDESCRI)+;
                    ","+cp_ToStrODBC(this.w_TR__NOTE)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIR_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'TIR_MAST')
        cp_CheckDeletedKey(i_cTable,0,'TRCODICE',this.w_TRCODICE)
        INSERT INTO (i_cTable);
              (TRCODICE,TRDESCRI,TR__NOTE &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_TRCODICE;
                  ,this.w_TRDESCRI;
                  ,this.w_TR__NOTE;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIR_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIR_DETT_IDX,2])
      *
      * insert into TIR_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(TRCODICE,CPROWORD,TRSEQCAL,TRTIPDET,TRTIPVOC"+;
                  ",TRDESDET,TR__COL1,TR__COL2,TR__COL3,TC__FONT"+;
                  ",TC__FTST,TC__COLO,TC__INDE,TR__FLAG,TR_RIFUE,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TRCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_TRSEQCAL)+","+cp_ToStrODBC(this.w_TRTIPDET)+","+cp_ToStrODBC(this.w_TRTIPVOC)+;
             ","+cp_ToStrODBC(this.w_TRDESDET)+","+cp_ToStrODBC(this.w_TR__COL1)+","+cp_ToStrODBC(this.w_TR__COL2)+","+cp_ToStrODBC(this.w_TR__COL3)+","+cp_ToStrODBC(this.w_TC__FONT)+;
             ","+cp_ToStrODBC(this.w_TC__FTST)+","+cp_ToStrODBC(this.w_TC__COLO)+","+cp_ToStrODBC(this.w_TC__INDE)+","+cp_ToStrODBC(this.w_TR__FLAG)+","+cp_ToStrODBC(this.w_TR_RIFUE)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'TRCODICE',this.w_TRCODICE,'CPROWNUM',this.w_CPROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_TRCODICE,this.w_CPROWORD,this.w_TRSEQCAL,this.w_TRTIPDET,this.w_TRTIPVOC"+;
                ",this.w_TRDESDET,this.w_TR__COL1,this.w_TR__COL2,this.w_TR__COL3,this.w_TC__FONT"+;
                ",this.w_TC__FTST,this.w_TC__COLO,this.w_TC__INDE,this.w_TR__FLAG,this.w_TR_RIFUE,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.TIR_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update TIR_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'TIR_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " TRDESCRI="+cp_ToStrODBC(this.w_TRDESCRI)+;
             ",TR__NOTE="+cp_ToStrODBC(this.w_TR__NOTE)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'TIR_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'TRCODICE',this.w_TRCODICE  )
          UPDATE (i_cTable) SET;
              TRDESCRI=this.w_TRDESCRI;
             ,TR__NOTE=this.w_TR__NOTE;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 AND ((NOT EMPTY(t_TRDESDET)) OR t_TR__FLAG $ 'DT')) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.TIR_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.TIR_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSAR_MRT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_TRCODICE,"RTCODICE";
                     ,this.w_CPROWNUM,"RTNUMRIG";
                     )
              this.GSCG_AVR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_TRCODICE,"VRCODVOC";
                     ,this.w_CPROWNUM,"VRROWORD";
                     )
              this.GSCG_MAS.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_TRCODICE,"TRCODVOC";
                     ,this.w_CPROWNUM,"TRROWORD";
                     )
              this.GSAR_MRT.mDelete()
              this.GSCG_AVR.mDelete()
              this.GSCG_MAS.mDelete()
              *
              * delete from TIR_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TIR_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",TRSEQCAL="+cp_ToStrODBC(this.w_TRSEQCAL)+;
                     ",TRTIPDET="+cp_ToStrODBC(this.w_TRTIPDET)+;
                     ",TRTIPVOC="+cp_ToStrODBC(this.w_TRTIPVOC)+;
                     ",TRDESDET="+cp_ToStrODBC(this.w_TRDESDET)+;
                     ",TR__COL1="+cp_ToStrODBC(this.w_TR__COL1)+;
                     ",TR__COL2="+cp_ToStrODBC(this.w_TR__COL2)+;
                     ",TR__COL3="+cp_ToStrODBC(this.w_TR__COL3)+;
                     ",TC__FONT="+cp_ToStrODBC(this.w_TC__FONT)+;
                     ",TC__FTST="+cp_ToStrODBC(this.w_TC__FTST)+;
                     ",TC__COLO="+cp_ToStrODBC(this.w_TC__COLO)+;
                     ",TC__INDE="+cp_ToStrODBC(this.w_TC__INDE)+;
                     ",TR__FLAG="+cp_ToStrODBC(this.w_TR__FLAG)+;
                     ",TR_RIFUE="+cp_ToStrODBC(this.w_TR_RIFUE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,TRSEQCAL=this.w_TRSEQCAL;
                     ,TRTIPDET=this.w_TRTIPDET;
                     ,TRTIPVOC=this.w_TRTIPVOC;
                     ,TRDESDET=this.w_TRDESDET;
                     ,TR__COL1=this.w_TR__COL1;
                     ,TR__COL2=this.w_TR__COL2;
                     ,TR__COL3=this.w_TR__COL3;
                     ,TC__FONT=this.w_TC__FONT;
                     ,TC__FTST=this.w_TC__FTST;
                     ,TC__COLO=this.w_TC__COLO;
                     ,TC__INDE=this.w_TC__INDE;
                     ,TR__FLAG=this.w_TR__FLAG;
                     ,TR_RIFUE=this.w_TR_RIFUE;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (t_CPROWORD<>0 AND ((NOT EMPTY(t_TRDESDET)) OR t_TR__FLAG $ 'DT'))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSAR_MRT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_TRCODICE,"RTCODICE";
             ,this.w_CPROWNUM,"RTNUMRIG";
             )
        this.GSAR_MRT.mReplace()
        this.GSCG_AVR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_TRCODICE,"VRCODVOC";
             ,this.w_CPROWNUM,"VRROWORD";
             )
        this.GSCG_AVR.mReplace()
        this.GSCG_MAS.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_TRCODICE,"TRCODVOC";
             ,this.w_CPROWNUM,"TRROWORD";
             )
        this.GSCG_MAS.mReplace()
        this.GSAR_MRT.bSaveContext=.f.
        this.GSCG_AVR.bSaveContext=.f.
        this.GSCG_MAS.bSaveContext=.f.
      endscan
     this.GSAR_MRT.bSaveContext=.t.
     this.GSCG_AVR.bSaveContext=.t.
     this.GSCG_MAS.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND ((NOT EMPTY(t_TRDESDET)) OR t_TR__FLAG $ 'DT')) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSAR_MRT : Deleting
        this.GSAR_MRT.bSaveContext=.f.
        this.GSAR_MRT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_TRCODICE,"RTCODICE";
               ,this.w_CPROWNUM,"RTNUMRIG";
               )
        this.GSAR_MRT.bSaveContext=.t.
        this.GSAR_MRT.mDelete()
        * --- GSCG_AVR : Deleting
        this.GSCG_AVR.bSaveContext=.f.
        this.GSCG_AVR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_TRCODICE,"VRCODVOC";
               ,this.w_CPROWNUM,"VRROWORD";
               )
        this.GSCG_AVR.bSaveContext=.t.
        this.GSCG_AVR.mDelete()
        * --- GSCG_MAS : Deleting
        this.GSCG_MAS.bSaveContext=.f.
        this.GSCG_MAS.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_TRCODICE,"TRCODVOC";
               ,this.w_CPROWNUM,"TRROWORD";
               )
        this.GSCG_MAS.bSaveContext=.t.
        this.GSCG_MAS.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.TIR_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.TIR_DETT_IDX,2])
        *
        * delete TIR_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.TIR_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2])
        *
        * delete TIR_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND ((NOT EMPTY(t_TRDESDET)) OR t_TR__FLAG $ 'DT')) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIR_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2])
    if i_bUpd
      with this
          .w_TIPGES = This.oparentobject.pTIPGES
        .DoRTCalc(2,8,.t.)
          .w_TRTIPVOC = 'M'
        .DoRTCalc(10,10,.t.)
        if .o_TR__COL2<>.w_TR__COL2.or. .o_TR__COL3<>.w_TR__COL3
          .w_TR__COL1 = ' '
        endif
        if .o_TR__COL1<>.w_TR__COL1.or. .o_TR__COL3<>.w_TR__COL3
          .w_TR__COL2 = ' '
        endif
        if .o_TR__COL1<>.w_TR__COL1.or. .o_TR__COL2<>.w_TR__COL2
          .w_TR__COL3 = ' '
        endif
        .DoRTCalc(14,17,.t.)
        if .o_TRTIPDET<>.w_TRTIPDET
          .w_TR__FLAG = .w_TRTIPDET
        endif
        if .o_TRTIPDET<>.w_TRTIPDET
          .w_TR_RIFUE = IIF(.w_TRTIPDET<>'V','N',.w_TR_RIFUE)
        endif
          .w_RTIPGES = This.oparentobject.pTIPGES
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWNUM with this.w_CPROWNUM
      replace t_TRTIPVOC with this.w_TRTIPVOC
      replace t_TR__FLAG with this.w_TR__FLAG
      replace t_RTIPGES with this.w_RTIPGES
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRSEQCAL_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRSEQCAL_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRTIPDET_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRTIPDET_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRDESDET_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRDESDET_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTR__COL1_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTR__COL1_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTR__COL2_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTR__COL2_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTR__COL3_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTR__COL3_2_9.mCond()
    this.oPgFrm.Page1.oPag.oTC__FONT_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTC__FONT_2_11.mCond()
    this.oPgFrm.Page1.oPag.oTC__FTST_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTC__FTST_2_12.mCond()
    this.oPgFrm.Page1.oPag.oTC__COLO_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTC__COLO_2_13.mCond()
    this.oPgFrm.Page1.oPag.oTC__INDE_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTC__INDE_2_14.mCond()
    this.oPgFrm.Page1.oPag.oTR_RIFUE_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTR_RIFUE_2_18.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_2_10.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_10.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSAR_MRT.visible")=='L' And this.GSAR_MRT.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_10.enabled
      this.GSAR_MRT.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_16.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_16.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSCG_AVR.visible")=='L' And this.GSCG_AVR.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_16.enabled
      this.GSCG_AVR.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_19.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_19.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSCG_MAS.visible")=='L' And this.GSCG_MAS.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_19.enabled
      this.GSCG_MAS.HideChildrenChain()
    endif 
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTRDESCRI_1_3.visible=!this.oPgFrm.Page1.oPag.oTRDESCRI_1_3.mHide()
    this.oPgFrm.Page1.oPag.oTR__NOTE_1_4.visible=!this.oPgFrm.Page1.oPag.oTR__NOTE_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLinkPC_2_10.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_10.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSAR_MRT.visible")=='L' And this.GSAR_MRT.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_10.visible
      this.GSAR_MRT.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_16.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_16.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSCG_AVR.visible")=='L' And this.GSCG_AVR.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_16.visible
      this.GSCG_AVR.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_19.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_19.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSCG_MAS.visible")=='L' And this.GSCG_MAS.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_19.visible
      this.GSCG_MAS.HideChildrenChain()
    endif 
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTRDESCRI_1_3.value==this.w_TRDESCRI)
      this.oPgFrm.Page1.oPag.oTRDESCRI_1_3.value=this.w_TRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTR__NOTE_1_4.value==this.w_TR__NOTE)
      this.oPgFrm.Page1.oPag.oTR__NOTE_1_4.value=this.w_TR__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__FONT_2_11.RadioValue()==this.w_TC__FONT)
      this.oPgFrm.Page1.oPag.oTC__FONT_2_11.SetRadio()
      replace t_TC__FONT with this.oPgFrm.Page1.oPag.oTC__FONT_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__FTST_2_12.RadioValue()==this.w_TC__FTST)
      this.oPgFrm.Page1.oPag.oTC__FTST_2_12.SetRadio()
      replace t_TC__FTST with this.oPgFrm.Page1.oPag.oTC__FTST_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__COLO_2_13.RadioValue()==this.w_TC__COLO)
      this.oPgFrm.Page1.oPag.oTC__COLO_2_13.SetRadio()
      replace t_TC__COLO with this.oPgFrm.Page1.oPag.oTC__COLO_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__INDE_2_14.value==this.w_TC__INDE)
      this.oPgFrm.Page1.oPag.oTC__INDE_2_14.value=this.w_TC__INDE
      replace t_TC__INDE with this.oPgFrm.Page1.oPag.oTC__INDE_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTR_RIFUE_2_18.RadioValue()==this.w_TR_RIFUE)
      this.oPgFrm.Page1.oPag.oTR_RIFUE_2_18.SetRadio()
      replace t_TR_RIFUE with this.oPgFrm.Page1.oPag.oTR_RIFUE_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRSEQCAL_2_3.value==this.w_TRSEQCAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRSEQCAL_2_3.value=this.w_TRSEQCAL
      replace t_TRSEQCAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRSEQCAL_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDET_2_4.RadioValue()==this.w_TRTIPDET)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDET_2_4.SetRadio()
      replace t_TRTIPDET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDET_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRDESDET_2_6.value==this.w_TRDESDET)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRDESDET_2_6.value=this.w_TRDESDET
      replace t_TRDESDET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRDESDET_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL1_2_7.RadioValue()==this.w_TR__COL1)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL1_2_7.SetRadio()
      replace t_TR__COL1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL1_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL2_2_8.RadioValue()==this.w_TR__COL2)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL2_2_8.SetRadio()
      replace t_TR__COL2 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL2_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL3_2_9.RadioValue()==this.w_TR__COL3)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL3_2_9.SetRadio()
      replace t_TR__COL3 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL3_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'TIR_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_TRTIPDET $ "VDT") and (.w_TIPGES='A') and (.w_CPROWORD<>0 AND ((NOT EMPTY(.w_TRDESDET)) OR .w_TR__FLAG $ 'DT'))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDET_2_4
          i_bRes = .f.
          i_bnoChk = .f.
        case   not((.w_TR_RIFUE $ 'U/A/P' AND .w_TRTIPDET='T') OR (.w_TR_RIFUE $ 'NRO' AND .w_TRTIPDET='V') OR .w_TR_RIFUE='N') and ((.w_TRTIPDET='V' OR .w_TRTIPDET='T') AND .cFunction$'Edit/Load' AND .w_TIPGES='A') and (.w_CPROWORD<>0 AND ((NOT EMPTY(.w_TRDESDET)) OR .w_TR__FLAG $ 'DT'))
          .oNewFocus=.oPgFrm.Page1.oPag.oTR_RIFUE_2_18
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Errore di tipo rispetto (utile/perdita per totali mentre gli altri per voci)")
      endcase
      i_bRes = i_bRes .and. .GSAR_MRT.CheckForm()
      i_bRes = i_bRes .and. .GSCG_AVR.CheckForm()
      i_bRes = i_bRes .and. .GSCG_MAS.CheckForm()
      if .w_CPROWORD<>0 AND ((NOT EMPTY(.w_TRDESDET)) OR .w_TR__FLAG $ 'DT')
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TRTIPDET = this.w_TRTIPDET
    this.o_TR__COL1 = this.w_TR__COL1
    this.o_TR__COL2 = this.w_TR__COL2
    this.o_TR__COL3 = this.w_TR__COL3
    * --- GSAR_MRT : Depends On
    this.GSAR_MRT.SaveDependsOn()
    * --- GSCG_AVR : Depends On
    this.GSCG_AVR.SaveDependsOn()
    * --- GSCG_MAS : Depends On
    this.GSCG_MAS.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND ((NOT EMPTY(t_TRDESDET)) OR t_TR__FLAG $ 'DT'))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=t_RTIPGES='A'
    if !i_bRes
      cp_ErrorMsg("Operanzione non consentita!","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_TRSEQCAL=0
      .w_TRTIPDET=space(1)
      .w_TRTIPVOC=space(1)
      .w_TRDESDET=space(50)
      .w_TR__COL1=space(1)
      .w_TR__COL2=space(1)
      .w_TR__COL3=space(1)
      .w_TC__FONT=space(1)
      .w_TC__FTST=space(1)
      .w_TC__COLO=0
      .w_TC__INDE=0
      .w_TR__FLAG=space(1)
      .w_TR_RIFUE=space(1)
      .w_RTIPGES=space(1)
      .DoRTCalc(1,6,.f.)
        .w_TRSEQCAL = .w_CPROWORD
        .w_TRTIPDET = 'V'
        .w_TRTIPVOC = 'M'
      .DoRTCalc(10,10,.f.)
        .w_TR__COL1 = ' '
        .w_TR__COL2 = ' '
        .w_TR__COL3 = ' '
        .w_TC__FONT = 'A'
        .w_TC__FTST = 'N'
        .w_TC__COLO = 0
      .DoRTCalc(17,17,.f.)
        .w_TR__FLAG = .w_TRTIPDET
        .w_TR_RIFUE = IIF(.w_TRTIPDET<>'V','N',.w_TR_RIFUE)
        .w_RTIPGES = This.oparentobject.pTIPGES
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWNUM = t_CPROWNUM
    this.w_CPROWORD = t_CPROWORD
    this.w_TRSEQCAL = t_TRSEQCAL
    this.w_TRTIPDET = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDET_2_4.RadioValue(.t.)
    this.w_TRTIPVOC = t_TRTIPVOC
    this.w_TRDESDET = t_TRDESDET
    this.w_TR__COL1 = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL1_2_7.RadioValue(.t.)
    this.w_TR__COL2 = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL2_2_8.RadioValue(.t.)
    this.w_TR__COL3 = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL3_2_9.RadioValue(.t.)
    this.w_TC__FONT = this.oPgFrm.Page1.oPag.oTC__FONT_2_11.RadioValue(.t.)
    this.w_TC__FTST = this.oPgFrm.Page1.oPag.oTC__FTST_2_12.RadioValue(.t.)
    this.w_TC__COLO = this.oPgFrm.Page1.oPag.oTC__COLO_2_13.RadioValue(.t.)
    this.w_TC__INDE = t_TC__INDE
    this.w_TR__FLAG = t_TR__FLAG
    this.w_TR_RIFUE = this.oPgFrm.Page1.oPag.oTR_RIFUE_2_18.RadioValue(.t.)
    this.w_RTIPGES = t_RTIPGES
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_CPROWORD with this.w_CPROWORD
    replace t_TRSEQCAL with this.w_TRSEQCAL
    replace t_TRTIPDET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDET_2_4.ToRadio()
    replace t_TRTIPVOC with this.w_TRTIPVOC
    replace t_TRDESDET with this.w_TRDESDET
    replace t_TR__COL1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL1_2_7.ToRadio()
    replace t_TR__COL2 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL2_2_8.ToRadio()
    replace t_TR__COL3 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTR__COL3_2_9.ToRadio()
    replace t_TC__FONT with this.oPgFrm.Page1.oPag.oTC__FONT_2_11.ToRadio()
    replace t_TC__FTST with this.oPgFrm.Page1.oPag.oTC__FTST_2_12.ToRadio()
    replace t_TC__COLO with this.oPgFrm.Page1.oPag.oTC__COLO_2_13.ToRadio()
    replace t_TC__INDE with this.w_TC__INDE
    replace t_TR__FLAG with this.w_TR__FLAG
    replace t_TR_RIFUE with this.oPgFrm.Page1.oPag.oTR_RIFUE_2_18.ToRadio()
    replace t_RTIPGES with this.w_RTIPGES
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mtrPag1 as StdContainer
  Width  = 797
  height = 358
  stdWidth  = 797
  stdheight = 358
  resizeXpos=310
  resizeYpos=192
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRDESCRI_1_3 as StdField with uid="KFNOTZURNJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TRDESCRI", cQueryName = "TRDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del tipo di riclassificazione",;
    HelpContextID = 61829759,;
   bGlobalFont=.t.,;
    Height=21, Width=366, Left=111, Top=9, InputMask=replicate('X',40)

  func oTRDESCRI_1_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES='C')
    endwith
    endif
  endfunc

  add object oTR__NOTE_1_4 as StdMemo with uid="HREGABBRIS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TR__NOTE", cQueryName = "TR__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note aggiuntive",;
    HelpContextID = 259727995,;
   bGlobalFont=.t.,;
    Height=52, Width=367, Left=111, Top=37

  func oTR__NOTE_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES='C')
    endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="HOLJBWYOWZ",left=734, top=41, width=48,height=45,;
    CpPicture="BMP\TREEVIEW.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza treview";
    , HelpContextID = 167879341;
    , Caption='Treevie\<w';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      do GSCG_KGS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    with this.Parent.oContained
      return (not empty(.w_TRCODICE))
    endwith
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=96, width=678,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Rigo",Field2="TRSEQCAL",Label2="Seq.el.",Field3="TRTIPDET",Label3="Tipo",Field4="TRDESDET",Label4="Descrizione",Field5="TR__COL1",Label5="1",Field6="TR__COL2",Label6="2",Field7="TR__COL3",Label7="3",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235734138

  add object oStr_1_5 as StdString with uid="COUKTCOQEA",Visible=.t., Left=475, Top=96,;
    Alignment=1, Width=104, Height=18,;
    Caption="Colonna:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="MQEZUGNPUH",Visible=.t., Left=670, Top=98,;
    Alignment=2, Width=116, Height=15,;
    Caption="Totalizza"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="VOTNEBUDKX",Visible=.t., Left=693, Top=199,;
    Alignment=0, Width=96, Height=15,;
    Caption="Carattere"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="MDBAPFEURT",Visible=.t., Left=8, Top=9,;
    Alignment=1, Width=101, Height=15,;
    Caption="Descrizione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPGES='C')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="JMRFNWMQGF",Visible=.t., Left=685, Top=304,;
    Alignment=1, Width=61, Height=15,;
    Caption="Indentat.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="PVMBABHWLL",Visible=.t., Left=695, Top=374,;
    Alignment=0, Width=389, Height=19,;
    Caption="Attenzione esistono due bottoni dettagli sovrapposti"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_11 as StdBox with uid="ZHOTDHVIND",left=693, top=217, width=96,height=1

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=115,;
    width=674+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=116,width=673+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oTC__FONT_2_11.Refresh()
      this.Parent.oTC__FTST_2_12.Refresh()
      this.Parent.oTC__COLO_2_13.Refresh()
      this.Parent.oTC__INDE_2_14.Refresh()
      this.Parent.oTR_RIFUE_2_18.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_10 as StdButton with uid="EXKGOHIIAB",width=48,height=45,;
   left=693, top=122,;
    CpPicture="BMP\CALCOLA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per impostare le righe su cui totalizzare la riga corrente";
    , HelpContextID = 196386752;
    , Caption='\<Totaliz.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_10.Click()
      this.Parent.oContained.GSAR_MRT.LinkPCClick()
    endproc

  func oLinkPC_2_10.mCond()
    with this.Parent.oContained
      return (.w_TRTIPDET='T')
    endwith
  endfunc

  func oLinkPC_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES='C')
    endwith
   endif
  endfunc

  add object oTC__FONT_2_11 as StdTrsCombo with uid="WTWGWCUUWI",rtrep=.t.,;
    cFormVar="w_TC__FONT", RowSource=""+"Arial,"+"Times New Roman" , ;
    ToolTipText = "Font della voce",;
    HelpContextID = 17099894,;
    Height=25, Width=95, Left=695, Top=223,;
    cTotal="", cQueryName = "TC__FONT",;
    bObbl = .f. , nPag=2  , tabstop=.f.;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTC__FONT_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TC__FONT,&i_cF..t_TC__FONT),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'T',;
    space(1))))
  endfunc
  func oTC__FONT_2_11.GetRadio()
    this.Parent.oContained.w_TC__FONT = this.RadioValue()
    return .t.
  endfunc

  func oTC__FONT_2_11.ToRadio()
    this.Parent.oContained.w_TC__FONT=trim(this.Parent.oContained.w_TC__FONT)
    return(;
      iif(this.Parent.oContained.w_TC__FONT=='A',1,;
      iif(this.Parent.oContained.w_TC__FONT=='T',2,;
      0)))
  endfunc

  func oTC__FONT_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTC__FONT_2_11.mCond()
    with this.Parent.oContained
      return (.w_TIPGES='A')
    endwith
  endfunc

  add object oTC__FTST_2_12 as StdTrsCombo with uid="PUABKBLGAL",rtrep=.t.,;
    cFormVar="w_TC__FTST", RowSource=""+"Normale,"+"Grassetto,"+"Corsivo" , ;
    ToolTipText = "Stile font",;
    HelpContextID = 66786186,;
    Height=25, Width=92, Left=695, Top=250,;
    cTotal="", cQueryName = "TC__FTST",;
    bObbl = .f. , nPag=2  , tabstop=.f.;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTC__FTST_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TC__FTST,&i_cF..t_TC__FTST),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'B',;
    iif(xVal =3,'I',;
    space(1)))))
  endfunc
  func oTC__FTST_2_12.GetRadio()
    this.Parent.oContained.w_TC__FTST = this.RadioValue()
    return .t.
  endfunc

  func oTC__FTST_2_12.ToRadio()
    this.Parent.oContained.w_TC__FTST=trim(this.Parent.oContained.w_TC__FTST)
    return(;
      iif(this.Parent.oContained.w_TC__FTST=='N',1,;
      iif(this.Parent.oContained.w_TC__FTST=='B',2,;
      iif(this.Parent.oContained.w_TC__FTST=='I',3,;
      0))))
  endfunc

  func oTC__FTST_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTC__FTST_2_12.mCond()
    with this.Parent.oContained
      return (.w_TIPGES='A')
    endwith
  endfunc

  add object oTC__COLO_2_13 as StdTrsCombo with uid="URUVNUOVDM",rtrep=.t.,;
    cFormVar="w_TC__COLO", RowSource=""+"Nero,"+"Rosso,"+"Blue" , ;
    ToolTipText = "Colore della voce",;
    HelpContextID = 20245627,;
    Height=25, Width=92, Left=695, Top=277,;
    cTotal="", cQueryName = "TC__COLO",;
    bObbl = .f. , nPag=2  , tabstop=.f.;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTC__COLO_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TC__COLO,&i_cF..t_TC__COLO),this.value)
    return(iif(xVal =1,0,;
    iif(xVal =2,255,;
    iif(xVal =3,16711680,;
    0))))
  endfunc
  func oTC__COLO_2_13.GetRadio()
    this.Parent.oContained.w_TC__COLO = this.RadioValue()
    return .t.
  endfunc

  func oTC__COLO_2_13.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_TC__COLO==0,1,;
      iif(this.Parent.oContained.w_TC__COLO==255,2,;
      iif(this.Parent.oContained.w_TC__COLO==16711680,3,;
      0))))
  endfunc

  func oTC__COLO_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTC__COLO_2_13.mCond()
    with this.Parent.oContained
      return (.w_TIPGES='A')
    endwith
  endfunc

  add object oTC__INDE_2_14 as StdTrsField with uid="VWCWCEPVRI",rtseq=17,rtrep=.t.,;
    cFormVar="w_TC__INDE",value=0,;
    ToolTipText = "Caratteri di indentatura",;
    HelpContextID = 237704059,;
    cTotal="", bFixedPos=.t., cQueryName = "TC__INDE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=750, Top=304, cSayPict=["999"], cGetPict=["999"]

  func oTC__INDE_2_14.mCond()
    with this.Parent.oContained
      return (.w_TIPGES='A')
    endwith
  endfunc

  add object oLinkPC_2_16 as StdButton with uid="QGOOIQXABD",width=48,height=45,;
   left=743, top=122,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per impostare il dettaglio delle voci di riclassificazione";
    , HelpContextID = 121535329;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_16.Click()
      this.Parent.oContained.GSCG_AVR.LinkPCClick()
    endproc

  func oLinkPC_2_16.mCond()
    with this.Parent.oContained
      return (.w_TRTIPDET='V')
    endwith
  endfunc

  func oLinkPC_2_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES='C')
    endwith
   endif
  endfunc

  add object oTR_RIFUE_2_18 as StdTrsCombo with uid="JVBMXRIHQL",rtrep=.t.,;
    cFormVar="w_TR_RIFUE", RowSource=""+"Nessuno,"+"Totale attivit�,"+"Totale passivit�,"+"Riserva arrotondam.,"+"Utile/perdita (econ),"+"Oneri straodinari" , ;
    ToolTipText = "Tipo voce (utile/perdita per totalizzazione mentre att/pass/arr/one per voci)",;
    HelpContextID = 102638203,;
    Height=25, Width=127, Left=652, Top=333,;
    cTotal="", cQueryName = "TR_RIFUE",;
    bObbl = .f. , nPag=2  , sErrorMsg = "Errore di tipo rispetto (utile/perdita per totali mentre gli altri per voci)";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTR_RIFUE_2_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TR_RIFUE,&i_cF..t_TR_RIFUE),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'A',;
    iif(xVal =3,'P',;
    iif(xVal =4,'R',;
    iif(xVal =5,'U',;
    iif(xVal =6,'O',;
    space(1))))))))
  endfunc
  func oTR_RIFUE_2_18.GetRadio()
    this.Parent.oContained.w_TR_RIFUE = this.RadioValue()
    return .t.
  endfunc

  func oTR_RIFUE_2_18.ToRadio()
    this.Parent.oContained.w_TR_RIFUE=trim(this.Parent.oContained.w_TR_RIFUE)
    return(;
      iif(this.Parent.oContained.w_TR_RIFUE=='N',1,;
      iif(this.Parent.oContained.w_TR_RIFUE=='A',2,;
      iif(this.Parent.oContained.w_TR_RIFUE=='P',3,;
      iif(this.Parent.oContained.w_TR_RIFUE=='R',4,;
      iif(this.Parent.oContained.w_TR_RIFUE=='U',5,;
      iif(this.Parent.oContained.w_TR_RIFUE=='O',6,;
      0)))))))
  endfunc

  func oTR_RIFUE_2_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTR_RIFUE_2_18.mCond()
    with this.Parent.oContained
      return ((.w_TRTIPDET='V' OR .w_TRTIPDET='T') AND .cFunction$'Edit/Load' AND .w_TIPGES='A')
    endwith
  endfunc

  func oTR_RIFUE_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_TR_RIFUE $ 'U/A/P' AND .w_TRTIPDET='T') OR (.w_TR_RIFUE $ 'NRO' AND .w_TRTIPDET='V') OR .w_TR_RIFUE='N')
    endwith
    return bRes
  endfunc

  add object oLinkPC_2_19 as StdButton with uid="HVRIUMLHFS",width=48,height=45,;
   left=743, top=122,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per impostare il dettaglio delle voci di riclassificazione";
    , HelpContextID = 121535329;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_19.Click()
      this.Parent.oContained.GSCG_MAS.LinkPCClick()
    endproc

  func oLinkPC_2_19.mCond()
    with this.Parent.oContained
      return (.w_TRTIPDET='V')
    endwith
  endfunc

  func oLinkPC_2_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES='A')
    endwith
   endif
  endfunc

  add object oStr_2_17 as StdString with uid="NZHMVLRZBY",Visible=.t., Left=566, Top=333,;
    Alignment=1, Width=82, Height=15,;
    Caption="Rif. UE:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mtrBodyRow as CPBodyRowCnt
  Width=664
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_2 as StdTrsField with uid="QQJFDGSIQL",rtseq=6,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Progressivo riga",;
    HelpContextID = 268062570,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oTRSEQCAL_2_3 as StdTrsField with uid="CCHAVFWZRO",rtseq=7,rtrep=.t.,;
    cFormVar="w_TRSEQCAL",value=0,;
    ToolTipText = "Sequenza di elaborazione durante il bilancio",;
    HelpContextID = 208641406,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=54, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  func oTRSEQCAL_2_3.mCond()
    with this.Parent.oContained
      return (.w_TIPGES='A')
    endwith
  endfunc

  add object oTRTIPDET_2_4 as StdTrsCombo with uid="JSFRVLGQGP",rtrep=.t.,;
    cFormVar="w_TRTIPDET", RowSource=""+"Voce di ricl.,"+"Descrizione,"+"Totalizzazione" , ;
    ToolTipText = "Tipo riga: V =voce di riclassificazione; D =descrizione; T =totalizzazione",;
    HelpContextID = 75788938,;
    Height=21, Width=110, Left=111, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTRTIPDET_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRTIPDET,&i_cF..t_TRTIPDET),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'D',;
    iif(xVal =3,'T',;
    space(1)))))
  endfunc
  func oTRTIPDET_2_4.GetRadio()
    this.Parent.oContained.w_TRTIPDET = this.RadioValue()
    return .t.
  endfunc

  func oTRTIPDET_2_4.ToRadio()
    this.Parent.oContained.w_TRTIPDET=trim(this.Parent.oContained.w_TRTIPDET)
    return(;
      iif(this.Parent.oContained.w_TRTIPDET=='V',1,;
      iif(this.Parent.oContained.w_TRTIPDET=='D',2,;
      iif(this.Parent.oContained.w_TRTIPDET=='T',3,;
      0))))
  endfunc

  func oTRTIPDET_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTRTIPDET_2_4.mCond()
    with this.Parent.oContained
      return (.w_TIPGES='A')
    endwith
  endfunc

  func oTRTIPDET_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TRTIPDET $ "VDT")
    endwith
    return bRes
  endfunc

  add object oTRDESDET_2_6 as StdTrsField with uid="UOAWLINPEL",rtseq=10,rtrep=.t.,;
    cFormVar="w_TRDESDET",value=space(50),;
    ToolTipText = "Descrizione della riga",;
    HelpContextID = 78606986,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=346, Left=226, Top=0, InputMask=replicate('X',50)

  func oTRDESDET_2_6.mCond()
    with this.Parent.oContained
      return (.w_TIPGES='A')
    endwith
  endfunc

  add object oTR__COL1_2_7 as StdTrsCheck with uid="GSLWQZGJBR",rtrep=.t.,;
    cFormVar="w_TR__COL1",  caption="",;
    ToolTipText = "Se attivo: importo o linea sulla prima colonna",;
    HelpContextID = 20241817,;
    Left=581, Top=0, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oTR__COL1_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TR__COL1,&i_cF..t_TR__COL1),this.value)
    return(iif(xVal =1,'X',;
    ' '))
  endfunc
  func oTR__COL1_2_7.GetRadio()
    this.Parent.oContained.w_TR__COL1 = this.RadioValue()
    return .t.
  endfunc

  func oTR__COL1_2_7.ToRadio()
    this.Parent.oContained.w_TR__COL1=trim(this.Parent.oContained.w_TR__COL1)
    return(;
      iif(this.Parent.oContained.w_TR__COL1=='X',1,;
      0))
  endfunc

  func oTR__COL1_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTR__COL1_2_7.mCond()
    with this.Parent.oContained
      return (.w_TR__COL2=' ' AND .w_TR__COL3=' ' AND .w_TIPGES='A')
    endwith
  endfunc

  add object oTR__COL2_2_8 as StdTrsCheck with uid="GKTHFLIGIL",rtrep=.t.,;
    cFormVar="w_TR__COL2",  caption="",;
    ToolTipText = "Se attivo: importo o linea sulla seconda colonna",;
    HelpContextID = 20241816,;
    Left=609, Top=0, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oTR__COL2_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TR__COL2,&i_cF..t_TR__COL2),this.value)
    return(iif(xVal =1,'X',;
    ' '))
  endfunc
  func oTR__COL2_2_8.GetRadio()
    this.Parent.oContained.w_TR__COL2 = this.RadioValue()
    return .t.
  endfunc

  func oTR__COL2_2_8.ToRadio()
    this.Parent.oContained.w_TR__COL2=trim(this.Parent.oContained.w_TR__COL2)
    return(;
      iif(this.Parent.oContained.w_TR__COL2=='X',1,;
      0))
  endfunc

  func oTR__COL2_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTR__COL2_2_8.mCond()
    with this.Parent.oContained
      return (.w_TR__COL1=' ' AND .w_TR__COL3=' ' AND .w_TIPGES='A')
    endwith
  endfunc

  add object oTR__COL3_2_9 as StdTrsCheck with uid="PFNYASVINN",rtrep=.t.,;
    cFormVar="w_TR__COL3",  caption="",;
    ToolTipText = "Se attivo: importo o linea sulla terza colonna",;
    HelpContextID = 20241815,;
    Left=637, Top=0, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oTR__COL3_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TR__COL3,&i_cF..t_TR__COL3),this.value)
    return(iif(xVal =1,'X',;
    ' '))
  endfunc
  func oTR__COL3_2_9.GetRadio()
    this.Parent.oContained.w_TR__COL3 = this.RadioValue()
    return .t.
  endfunc

  func oTR__COL3_2_9.ToRadio()
    this.Parent.oContained.w_TR__COL3=trim(this.Parent.oContained.w_TR__COL3)
    return(;
      iif(this.Parent.oContained.w_TR__COL3=='X',1,;
      0))
  endfunc

  func oTR__COL3_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTR__COL3_2_9.mCond()
    with this.Parent.oContained
      return (.w_TR__COL1=' ' AND .w_TR__COL2=' ' AND .w_TIPGES='A')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_2.When()
    return(.t.)
  proc oCPROWORD_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mtr','TIR_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRCODICE=TIR_MAST.TRCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
