* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ars                                                        *
*              RSS Feed                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-18                                                      *
* Last revis.: 2010-04-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_ars"))

* --- Class definition
define class tgsut_ars as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 561
  Height = 118+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-19"
  HelpContextID=126436201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  RSS_FEED_IDX = 0
  cFile = "RSS_FEED"
  cKeySelect = "RFRSSNAM"
  cKeyWhere  = "RFRSSNAM=this.w_RFRSSNAM"
  cKeyWhereODBC = '"RFRSSNAM="+cp_ToStrODBC(this.w_RFRSSNAM)';

  cKeyWhereODBCqualified = '"RSS_FEED.RFRSSNAM="+cp_ToStrODBC(this.w_RFRSSNAM)';

  cPrg = "gsut_ars"
  cComment = "RSS Feed"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RFRSSNAM = space(15)
  w_RFDESCRI = space(254)
  w_RFRSSURL = space(254)
  w_RFMAXITM = 0
  w_RFFLGDSK = space(1)
  w_SUBSCRIBE = space(1)
  w_FOLDERPATH = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RSS_FEED','gsut_ars')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_arsPag1","gsut_ars",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("RSS Feed")
      .Pages(1).HelpContextID = 33310074
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRFRSSNAM_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='RSS_FEED'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RSS_FEED_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RSS_FEED_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RFRSSNAM = NVL(RFRSSNAM,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RSS_FEED where RFRSSNAM=KeySet.RFRSSNAM
    *
    i_nConn = i_TableProp[this.RSS_FEED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RSS_FEED')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RSS_FEED.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RSS_FEED '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RFRSSNAM',this.w_RFRSSNAM  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SUBSCRIBE = 'N'
        .w_FOLDERPATH = ""
        .w_RFRSSNAM = NVL(RFRSSNAM,space(15))
        .w_RFDESCRI = NVL(RFDESCRI,space(254))
        .w_RFRSSURL = NVL(RFRSSURL,space(254))
        .w_RFMAXITM = NVL(RFMAXITM,0)
        .w_RFFLGDSK = NVL(RFFLGDSK,space(1))
        cp_LoadRecExtFlds(this,'RSS_FEED')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RFRSSNAM = space(15)
      .w_RFDESCRI = space(254)
      .w_RFRSSURL = space(254)
      .w_RFMAXITM = 0
      .w_RFFLGDSK = space(1)
      .w_SUBSCRIBE = space(1)
      .w_FOLDERPATH = space(254)
      if .cFunction<>"Filter"
          .DoRTCalc(1,4,.f.)
        .w_RFFLGDSK = 'S'
        .w_SUBSCRIBE = 'N'
        .w_FOLDERPATH = ""
      endif
    endwith
    cp_BlankRecExtFlds(this,'RSS_FEED')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRFRSSNAM_1_1.enabled = i_bVal
      .Page1.oPag.oRFDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oRFRSSURL_1_4.enabled = i_bVal
      .Page1.oPag.oRFMAXITM_1_6.enabled = i_bVal
      .Page1.oPag.oRFFLGDSK_1_8.enabled = i_bVal
      .Page1.oPag.oSUBSCRIBE_1_9.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRFRSSNAM_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRFRSSNAM_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'RSS_FEED',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RSS_FEED_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFRSSNAM,"RFRSSNAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFDESCRI,"RFDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFRSSURL,"RFRSSURL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFMAXITM,"RFMAXITM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFFLGDSK,"RFFLGDSK",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RSS_FEED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])
    i_lTable = "RSS_FEED"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RSS_FEED_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RSS_FEED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RSS_FEED_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RSS_FEED
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RSS_FEED')
        i_extval=cp_InsertValODBCExtFlds(this,'RSS_FEED')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RFRSSNAM,RFDESCRI,RFRSSURL,RFMAXITM,RFFLGDSK "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RFRSSNAM)+;
                  ","+cp_ToStrODBC(this.w_RFDESCRI)+;
                  ","+cp_ToStrODBC(this.w_RFRSSURL)+;
                  ","+cp_ToStrODBC(this.w_RFMAXITM)+;
                  ","+cp_ToStrODBC(this.w_RFFLGDSK)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RSS_FEED')
        i_extval=cp_InsertValVFPExtFlds(this,'RSS_FEED')
        cp_CheckDeletedKey(i_cTable,0,'RFRSSNAM',this.w_RFRSSNAM)
        INSERT INTO (i_cTable);
              (RFRSSNAM,RFDESCRI,RFRSSURL,RFMAXITM,RFFLGDSK  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RFRSSNAM;
                  ,this.w_RFDESCRI;
                  ,this.w_RFRSSURL;
                  ,this.w_RFMAXITM;
                  ,this.w_RFFLGDSK;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RSS_FEED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RSS_FEED_IDX,i_nConn)
      *
      * update RSS_FEED
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RSS_FEED')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RFDESCRI="+cp_ToStrODBC(this.w_RFDESCRI)+;
             ",RFRSSURL="+cp_ToStrODBC(this.w_RFRSSURL)+;
             ",RFMAXITM="+cp_ToStrODBC(this.w_RFMAXITM)+;
             ",RFFLGDSK="+cp_ToStrODBC(this.w_RFFLGDSK)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RSS_FEED')
        i_cWhere = cp_PKFox(i_cTable  ,'RFRSSNAM',this.w_RFRSSNAM  )
        UPDATE (i_cTable) SET;
              RFDESCRI=this.w_RFDESCRI;
             ,RFRSSURL=this.w_RFRSSURL;
             ,RFMAXITM=this.w_RFMAXITM;
             ,RFFLGDSK=this.w_RFFLGDSK;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RSS_FEED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RSS_FEED_IDX,i_nConn)
      *
      * delete RSS_FEED
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RFRSSNAM',this.w_RFRSSNAM  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RSS_FEED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_FLSGOWDDTT()
    with this
          * --- Subscribe Feed
          GSUT_BRS(this;
              ,'S';
              ,.w_FOLDERPATH;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oRFFLGDSK_1_8.visible=!this.oPgFrm.Page1.oPag.oRFFLGDSK_1_8.mHide()
    this.oPgFrm.Page1.oPag.oFOLDERPATH_1_11.visible=!this.oPgFrm.Page1.oPag.oFOLDERPATH_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Record Inserted")
          .Calculate_FLSGOWDDTT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRFRSSNAM_1_1.value==this.w_RFRSSNAM)
      this.oPgFrm.Page1.oPag.oRFRSSNAM_1_1.value=this.w_RFRSSNAM
    endif
    if not(this.oPgFrm.Page1.oPag.oRFDESCRI_1_3.value==this.w_RFDESCRI)
      this.oPgFrm.Page1.oPag.oRFDESCRI_1_3.value=this.w_RFDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRFRSSURL_1_4.value==this.w_RFRSSURL)
      this.oPgFrm.Page1.oPag.oRFRSSURL_1_4.value=this.w_RFRSSURL
    endif
    if not(this.oPgFrm.Page1.oPag.oRFMAXITM_1_6.value==this.w_RFMAXITM)
      this.oPgFrm.Page1.oPag.oRFMAXITM_1_6.value=this.w_RFMAXITM
    endif
    if not(this.oPgFrm.Page1.oPag.oRFFLGDSK_1_8.RadioValue()==this.w_RFFLGDSK)
      this.oPgFrm.Page1.oPag.oRFFLGDSK_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSUBSCRIBE_1_9.RadioValue()==this.w_SUBSCRIBE)
      this.oPgFrm.Page1.oPag.oSUBSCRIBE_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFOLDERPATH_1_11.value==this.w_FOLDERPATH)
      this.oPgFrm.Page1.oPag.oFOLDERPATH_1_11.value=this.w_FOLDERPATH
    endif
    cp_SetControlsValueExtFlds(this,'RSS_FEED')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_arsPag1 as StdContainer
  Width  = 557
  height = 118
  stdWidth  = 557
  stdheight = 118
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRFRSSNAM_1_1 as StdField with uid="KAHGJAVQUB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RFRSSNAM", cQueryName = "RFRSSNAM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 201271395,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=75, Top=16, InputMask=replicate('X',15)

  add object oRFDESCRI_1_3 as StdField with uid="HAOSKYMDYN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RFDESCRI", cQueryName = "RFDESCRI",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome RSSFeed",;
    HelpContextID = 15747167,;
   bGlobalFont=.t.,;
    Height=21, Width=365, Left=188, Top=16, InputMask=replicate('X',254)

  add object oRFRSSURL_1_4 as StdField with uid="GSINTJCRPE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RFRSSURL", cQueryName = "RFRSSURL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "URL RSS feed",;
    HelpContextID = 50276450,;
   bGlobalFont=.t.,;
    Height=21, Width=478, Left=75, Top=43, InputMask=replicate('X',254)

  add object oRFMAXITM_1_6 as StdField with uid="RSTHQHGJRB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RFMAXITM", cQueryName = "RFMAXITM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero massimo di elementi scaricabili",;
    HelpContextID = 121428067,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=519, Top=69, cSayPict='"999"', cGetPict='"999"'

  add object oRFFLGDSK_1_8 as StdCheck with uid="KAYKESRIRV",rtseq=5,rtrep=.f.,left=75, top=69, caption="Appare in RSS Viewer",;
    ToolTipText = "Se attivo il feed pu� essere inserito in RSS Viewer (deskmenu)",;
    HelpContextID = 20408417,;
    cFormVar="w_RFFLGDSK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRFFLGDSK_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oRFFLGDSK_1_8.GetRadio()
    this.Parent.oContained.w_RFFLGDSK = this.RadioValue()
    return .t.
  endfunc

  func oRFFLGDSK_1_8.SetRadio()
    this.Parent.oContained.w_RFFLGDSK=trim(this.Parent.oContained.w_RFFLGDSK)
    this.value = ;
      iif(this.Parent.oContained.w_RFFLGDSK=='S',1,;
      0)
  endfunc

  func oRFFLGDSK_1_8.mHide()
    with this.Parent.oContained
      return (.w_RFFLGDSK = 'A')
    endwith
  endfunc

  add object oSUBSCRIBE_1_9 as StdCheck with uid="OFWPRRCRXH",rtseq=6,rtrep=.f.,left=75, top=93, caption="Sottoscrivi al salvataggio",;
    ToolTipText = "Sottoscrivi il Feed al salvataggio",;
    HelpContextID = 16893000,;
    cFormVar="w_SUBSCRIBE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSUBSCRIBE_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSUBSCRIBE_1_9.GetRadio()
    this.Parent.oContained.w_SUBSCRIBE = this.RadioValue()
    return .t.
  endfunc

  func oSUBSCRIBE_1_9.SetRadio()
    this.Parent.oContained.w_SUBSCRIBE=trim(this.Parent.oContained.w_SUBSCRIBE)
    this.value = ;
      iif(this.Parent.oContained.w_SUBSCRIBE=='S',1,;
      0)
  endfunc

  add object oFOLDERPATH_1_11 as StdField with uid="VBKGKRQRWL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FOLDERPATH", cQueryName = "FOLDERPATH",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 15721001,;
   bGlobalFont=.t.,;
    Height=21, Width=231, Left=322, Top=95, InputMask=replicate('X',254)

  func oFOLDERPATH_1_11.mHide()
    with this.Parent.oContained
      return (Empty(.w_FOLDERPATH))
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="GEIDOJRIEN",Visible=.t., Left=8, Top=18,;
    Alignment=1, Width=66, Height=18,;
    Caption="Feed:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="OLFVZNEHWB",Visible=.t., Left=19, Top=45,;
    Alignment=1, Width=55, Height=18,;
    Caption="URL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="BLPNQOXQPV",Visible=.t., Left=415, Top=71,;
    Alignment=1, Width=103, Height=18,;
    Caption="Max Feed item:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ZHTICPXYVR",Visible=.t., Left=269, Top=98,;
    Alignment=1, Width=53, Height=18,;
    Caption="Folder:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (Empty(.w_FOLDERPATH))
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ars','RSS_FEED','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RFRSSNAM=RSS_FEED.RFRSSNAM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
