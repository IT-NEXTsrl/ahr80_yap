* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mga                                                        *
*              Gadget                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-15                                                      *
* Last revis.: 2015-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_mga"))

* --- Class definition
define class tgsut_mga as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 666
  Height = 481+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-27"
  HelpContextID=238468247
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  GAD_MAST_IDX = 0
  GAD_DETT_IDX = 0
  UTE_NTI_IDX = 0
  MOD_GADG_IDX = 0
  cpusers_IDX = 0
  AZIENDA_IDX = 0
  GRP_GADG_IDX = 0
  cFile = "GAD_MAST"
  cFileDetail = "GAD_DETT"
  cKeySelect = "GACODICE"
  cKeyWhere  = "GACODICE=this.w_GACODICE"
  cKeyDetail  = "GACODICE=this.w_GACODICE and GA__PROP=this.w_GA__PROP and GACODUTE=this.w_GACODUTE and GACODAZI=this.w_GACODAZI"
  cKeyWhereODBC = '"GACODICE="+cp_ToStrODBC(this.w_GACODICE)';

  cKeyDetailWhereODBC = '"GACODICE="+cp_ToStrODBC(this.w_GACODICE)';
      +'+" and GA__PROP="+cp_ToStrODBC(this.w_GA__PROP)';
      +'+" and GACODUTE="+cp_ToStrODBC(this.w_GACODUTE)';
      +'+" and GACODAZI="+cp_ToStrODBC(this.w_GACODAZI)';

  cKeyWhereODBCqualified = '"GAD_DETT.GACODICE="+cp_ToStrODBC(this.w_GACODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'GAD_DETT.GACODUTE,GAD_DETT.GACODAZI,GAD_DETT.GA__PROP'
  cPrg = "gsut_mga"
  cComment = "Gadget"
  i_nRowNum = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GACODICE = space(10)
  w_GA__NOME = space(100)
  w_GADESCRI = space(0)
  w_GASRCPRG = space(50)
  w_GAGRPCOD = space(5)
  w_GAFORAZI = space(1)
  w_GACNDATT = space(254)
  w_GACHKATT = space(1)
  w_GAUTEESC = 0
  w_GA__PROP = space(50)
  w_GAVALORE = space(254)
  w_GACODUTE = 0
  w_PREIMG = space(254)
  w_GACODAZI = space(5)
  w_GA_RESET = space(1)
  w_UTENTE = space(50)
  w_DUPLICA = .F.
  w_GAGRPCOD_OLD = space(5)
  w_DESUTESC = space(20)

  * --- Autonumbered Variables
  op_GACODICE = this.W_GACODICE

  * --- Children pointers
  GSUT_MAG = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GAD_MAST','gsut_mga')
    stdPageFrame::Init()
    *set procedure to GSUT_MAG additive
    with this
      .Pages(1).addobject("oPag","tgsut_mgaPag1","gsut_mga",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Gadget Library")
      .Pages(1).HelpContextID = 159181634
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGACODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSUT_MAG
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='UTE_NTI'
    this.cWorkTables[2]='MOD_GADG'
    this.cWorkTables[3]='cpusers'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='GRP_GADG'
    this.cWorkTables[6]='GAD_MAST'
    this.cWorkTables[7]='GAD_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GAD_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GAD_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSUT_MAG = CREATEOBJECT('stdLazyChild',this,'GSUT_MAG')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_MAG)
      this.GSUT_MAG.DestroyChildrenChain()
      this.GSUT_MAG=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_MAG.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_MAG.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_MAG.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSUT_MAG.ChangeRow(this.cRowID+'      1',1;
             ,.w_GACODICE,"AGCODGAD";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_GACODICE = NVL(GACODICE,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from GAD_MAST where GACODICE=KeySet.GACODICE
    *
    i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2],this.bLoadRecFilter,this.GAD_MAST_IDX,"gsut_mga")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GAD_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GAD_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"GAD_DETT.","GAD_MAST.")
      i_cTable = i_cTable+' GAD_MAST '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GACODICE',this.w_GACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_PREIMG = space(254)
        .w_DUPLICA = .f.
        .w_GAGRPCOD_OLD = space(5)
        .w_DESUTESC = space(20)
        .w_GACODICE = NVL(GACODICE,space(10))
        .op_GACODICE = .w_GACODICE
        .w_GA__NOME = NVL(GA__NOME,space(100))
        .w_GADESCRI = NVL(GADESCRI,space(0))
        .w_GASRCPRG = NVL(GASRCPRG,space(50))
          if link_1_4_joined
            this.w_GASRCPRG = NVL(MGSRCPRG104,NVL(this.w_GASRCPRG,space(50)))
            this.w_PREIMG = NVL(MGPREIMG104,space(254))
          else
          .link_1_4('Load')
          endif
        .w_GAGRPCOD = NVL(GAGRPCOD,space(5))
          * evitabile
          *.link_1_5('Load')
        .w_GAFORAZI = NVL(GAFORAZI,space(1))
        .w_GACNDATT = NVL(GACNDATT,space(254))
        .w_GACHKATT = NVL(GACHKATT,space(1))
        .w_GAUTEESC = NVL(GAUTEESC,0)
          .link_1_9('Load')
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_PREIMG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'GAD_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from GAD_DETT where GACODICE=KeySet.GACODICE
      *                            and GA__PROP=KeySet.GA__PROP
      *                            and GACODUTE=KeySet.GACODUTE
      *                            and GACODAZI=KeySet.GACODAZI
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.GAD_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GAD_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('GAD_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "GAD_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" GAD_DETT"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'GACODICE',this.w_GACODICE  )
        select * from (i_cTable) GAD_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_UTENTE = space(50)
          .w_GA__PROP = NVL(GA__PROP,space(50))
          .w_GAVALORE = NVL(GAVALORE,space(254))
          .w_GACODUTE = NVL(GACODUTE,0)
          .link_2_3('Load')
          .w_GACODAZI = NVL(GACODAZI,space(5))
          .w_GA_RESET = NVL(GA_RESET,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace GA__PROP with .w_GA__PROP
          replace GACODUTE with .w_GACODUTE
          replace GACODAZI with .w_GACODAZI
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_PREIMG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_mga
    * --- Per caricamento rapido: eseguo i metodi usati nella BlankRec senza per�
    * --- sbiancare le variabili
    IF this.cFunction='Load' AND NOT EMPTY(this.w_GACODICE)
       * --- Sbianco i dati da non riportare sul nuovo Gadget
       this.w_GACODICE=Space(10)
       this.w_GA__NOME=Space(50)
       this.w_DUPLICA=.t.
       
       * --- Dettaglio propriet�
       this.bLoaded=.f.
       this.bUpdated=.t.
       set delete off
       update (this.cTrsName) set i_Srv='A' where 1=1
       set delete on
       this.FirstRow()
       this.SetRow()
    
      * --- Dettaglio Accessi
       *this.GSUT_MAG.bLoaded=.f.
       *this.GSUT_MAG.bUpdated=.t.
       *this.GSUT_MAG.MarkPos()
       *set delete off
       *update (this.GSUT_MAG.cTrsName) set i_Srv='A' where 1=1
       *set delete on
       *this.GSUT_MAG.RePos()
    
       * --- Fine
       this.SaveDependsOn()
       this.SetControlsValue()
       this.TrsFromWork()
       this.mHideControls()
       this.ChildrenChangeRow()
    	 this.NotifyEvent('Blank')
       this.mCalc(.t.)
       
       return
    Endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_GACODICE=space(10)
      .w_GA__NOME=space(100)
      .w_GADESCRI=space(0)
      .w_GASRCPRG=space(50)
      .w_GAGRPCOD=space(5)
      .w_GAFORAZI=space(1)
      .w_GACNDATT=space(254)
      .w_GACHKATT=space(1)
      .w_GAUTEESC=0
      .w_GA__PROP=space(50)
      .w_GAVALORE=space(254)
      .w_GACODUTE=0
      .w_PREIMG=space(254)
      .w_GACODAZI=space(5)
      .w_GA_RESET=space(1)
      .w_UTENTE=space(50)
      .w_DUPLICA=.f.
      .w_GAGRPCOD_OLD=space(5)
      .w_DESUTESC=space(20)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_GASRCPRG))
         .link_1_4('Full')
        endif
        .w_GAGRPCOD = ''
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_GAGRPCOD))
         .link_1_5('Full')
        endif
        .w_GAFORAZI = 'N'
        .DoRTCalc(7,7,.f.)
        .w_GACHKATT = 'S'
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_GAUTEESC))
         .link_1_9('Full')
        endif
        .DoRTCalc(10,11,.f.)
        .w_GACODUTE = -1
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_GACODUTE))
         .link_2_3('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_PREIMG)
        .DoRTCalc(13,13,.f.)
        .w_GACODAZI = 'xxx'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(15,16,.f.)
        .w_DUPLICA = .f.
      endif
    endwith
    cp_BlankRecExtFlds(this,'GAD_MAST')
    this.DoRTCalc(18,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGACODICE_1_1.enabled = i_bVal
      .Page1.oPag.oGA__NOME_1_2.enabled = i_bVal
      .Page1.oPag.oGADESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oGASRCPRG_1_4.enabled = i_bVal
      .Page1.oPag.oGAGRPCOD_1_5.enabled = i_bVal
      .Page1.oPag.oGAFORAZI_1_6.enabled = i_bVal
      .Page1.oPag.oGACNDATT_1_7.enabled = i_bVal
      .Page1.oPag.oGACHKATT_1_8.enabled = i_bVal
      .Page1.oPag.oGAUTEESC_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_15.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oGACODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oGACODICE_1_1.enabled = .t.
      endif
    endwith
    this.GSUT_MAG.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'GAD_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEGADG","w_GACODICE")
      .op_GACODICE = .w_GACODICE
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_MAG.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GACODICE,"GACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GA__NOME,"GA__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GADESCRI,"GADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GASRCPRG,"GASRCPRG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GAGRPCOD,"GAGRPCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GAFORAZI,"GAFORAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GACNDATT,"GACNDATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GACHKATT,"GACHKATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GAUTEESC,"GAUTEESC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])
    i_lTable = "GAD_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GAD_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_GA__PROP C(50);
      ,t_GAVALORE C(254);
      ,t_GACODUTE N(4);
      ,t_GACODAZI N(3);
      ,t_GA_RESET N(3);
      ,t_UTENTE C(50);
      ,GA__PROP C(50);
      ,GACODUTE N(4);
      ,GACODAZI C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mgabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGA__PROP_2_1.controlsource=this.cTrsName+'.t_GA__PROP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGAVALORE_2_2.controlsource=this.cTrsName+'.t_GAVALORE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGACODUTE_2_3.controlsource=this.cTrsName+'.t_GACODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGACODAZI_2_4.controlsource=this.cTrsName+'.t_GACODAZI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGA_RESET_2_5.controlsource=this.cTrsName+'.t_GA_RESET'
    this.oPgFRm.Page1.oPag.oUTENTE_2_6.controlsource=this.cTrsName+'.t_UTENTE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(220)
    this.AddVLine(474)
    this.AddVLine(533)
    this.AddVLine(615)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGA__PROP_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEGADG","w_GACODICE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GAD_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GAD_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'GAD_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(GACODICE,GA__NOME,GADESCRI,GASRCPRG,GAGRPCOD"+;
                  ",GAFORAZI,GACNDATT,GACHKATT,GAUTEESC"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_GACODICE)+;
                    ","+cp_ToStrODBC(this.w_GA__NOME)+;
                    ","+cp_ToStrODBC(this.w_GADESCRI)+;
                    ","+cp_ToStrODBCNull(this.w_GASRCPRG)+;
                    ","+cp_ToStrODBCNull(this.w_GAGRPCOD)+;
                    ","+cp_ToStrODBC(this.w_GAFORAZI)+;
                    ","+cp_ToStrODBC(this.w_GACNDATT)+;
                    ","+cp_ToStrODBC(this.w_GACHKATT)+;
                    ","+cp_ToStrODBCNull(this.w_GAUTEESC)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GAD_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'GAD_MAST')
        cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE)
        INSERT INTO (i_cTable);
              (GACODICE,GA__NOME,GADESCRI,GASRCPRG,GAGRPCOD,GAFORAZI,GACNDATT,GACHKATT,GAUTEESC &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_GACODICE;
                  ,this.w_GA__NOME;
                  ,this.w_GADESCRI;
                  ,this.w_GASRCPRG;
                  ,this.w_GAGRPCOD;
                  ,this.w_GAFORAZI;
                  ,this.w_GACNDATT;
                  ,this.w_GACHKATT;
                  ,this.w_GAUTEESC;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GAD_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GAD_DETT_IDX,2])
      *
      * insert into GAD_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(GACODICE,GA__PROP,GAVALORE,GACODUTE,GACODAZI"+;
                  ",GA_RESET,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_GACODICE)+","+cp_ToStrODBC(this.w_GA__PROP)+","+cp_ToStrODBC(this.w_GAVALORE)+","+cp_ToStrODBCNull(this.w_GACODUTE)+","+cp_ToStrODBC(this.w_GACODAZI)+;
             ","+cp_ToStrODBC(this.w_GA_RESET)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE,'GA__PROP',this.w_GA__PROP,'GACODUTE',this.w_GACODUTE,'GACODAZI',this.w_GACODAZI)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_GACODICE,this.w_GA__PROP,this.w_GAVALORE,this.w_GACODUTE,this.w_GACODAZI"+;
                ",this.w_GA_RESET,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update GAD_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'GAD_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " GA__NOME="+cp_ToStrODBC(this.w_GA__NOME)+;
             ",GADESCRI="+cp_ToStrODBC(this.w_GADESCRI)+;
             ",GASRCPRG="+cp_ToStrODBCNull(this.w_GASRCPRG)+;
             ",GAGRPCOD="+cp_ToStrODBCNull(this.w_GAGRPCOD)+;
             ",GAFORAZI="+cp_ToStrODBC(this.w_GAFORAZI)+;
             ",GACNDATT="+cp_ToStrODBC(this.w_GACNDATT)+;
             ",GACHKATT="+cp_ToStrODBC(this.w_GACHKATT)+;
             ",GAUTEESC="+cp_ToStrODBCNull(this.w_GAUTEESC)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'GAD_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'GACODICE',this.w_GACODICE  )
          UPDATE (i_cTable) SET;
              GA__NOME=this.w_GA__NOME;
             ,GADESCRI=this.w_GADESCRI;
             ,GASRCPRG=this.w_GASRCPRG;
             ,GAGRPCOD=this.w_GAGRPCOD;
             ,GAFORAZI=this.w_GAFORAZI;
             ,GACNDATT=this.w_GACNDATT;
             ,GACHKATT=this.w_GACHKATT;
             ,GAUTEESC=this.w_GAUTEESC;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_GA__PROP))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.GAD_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.GAD_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from GAD_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and GA__PROP="+cp_ToStrODBC(&i_TN.->GA__PROP)+;
                            " and GACODUTE="+cp_ToStrODBC(&i_TN.->GACODUTE)+;
                            " and GACODAZI="+cp_ToStrODBC(&i_TN.->GACODAZI)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and GA__PROP=&i_TN.->GA__PROP;
                            and GACODUTE=&i_TN.->GACODUTE;
                            and GACODAZI=&i_TN.->GACODAZI;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace GA__PROP with this.w_GA__PROP
              replace GACODUTE with this.w_GACODUTE
              replace GACODAZI with this.w_GACODAZI
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update GAD_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " GAVALORE="+cp_ToStrODBC(this.w_GAVALORE)+;
                     ",GA_RESET="+cp_ToStrODBC(this.w_GA_RESET)+;
                     " ,GA__PROP="+cp_ToStrODBC(this.w_GA__PROP)+;
                     " ,GACODUTE="+cp_ToStrODBC(this.w_GACODUTE)+;
                     " ,GACODAZI="+cp_ToStrODBC(this.w_GACODAZI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and GA__PROP="+cp_ToStrODBC(GA__PROP)+;
                             " and GACODUTE="+cp_ToStrODBC(GACODUTE)+;
                             " and GACODAZI="+cp_ToStrODBC(GACODAZI)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      GAVALORE=this.w_GAVALORE;
                     ,GA_RESET=this.w_GA_RESET;
                     ,GA__PROP=this.w_GA__PROP;
                     ,GACODUTE=this.w_GACODUTE;
                     ,GACODAZI=this.w_GACODAZI;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and GA__PROP=&i_TN.->GA__PROP;
                                      and GACODUTE=&i_TN.->GACODUTE;
                                      and GACODAZI=&i_TN.->GACODAZI;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSUT_MAG : Saving
      this.GSUT_MAG.ChangeRow(this.cRowID+'      1',0;
             ,this.w_GACODICE,"AGCODGAD";
             )
      this.GSUT_MAG.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSUT_MAG : Deleting
    this.GSUT_MAG.ChangeRow(this.cRowID+'      1',0;
           ,this.w_GACODICE,"AGCODGAD";
           )
    this.GSUT_MAG.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_GA__PROP))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.GAD_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.GAD_DETT_IDX,2])
        *
        * delete GAD_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and GA__PROP="+cp_ToStrODBC(&i_TN.->GA__PROP)+;
                            " and GACODUTE="+cp_ToStrODBC(&i_TN.->GACODUTE)+;
                            " and GACODAZI="+cp_ToStrODBC(&i_TN.->GACODAZI)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and GA__PROP=&i_TN.->GA__PROP;
                              and GACODUTE=&i_TN.->GACODUTE;
                              and GACODAZI=&i_TN.->GACODAZI;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])
        *
        * delete GAD_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_GA__PROP))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_PREIMG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_PREIMG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_PTKCJTSVAY()
    with this
          * --- Inserisce le propriet� base all'inserimento di un nuovo gadget
      if !.w_DUPLICA
          GSUT_BGG(this;
              ,'New';
             )
      endif
          .w_DUPLICA = .f.
    endwith
  endproc
  proc Calculate_IPXVTQHCSR()
    with this
          * --- Valorizza GAGRPCOD_OLD
          .w_GAGRPCOD_OLD = .w_GAGRPCOD
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGASRCPRG_1_4.enabled = this.oPgFrm.Page1.oPag.oGASRCPRG_1_4.mCond()
    this.oPgFrm.Page1.oPag.oGAGRPCOD_1_5.enabled = this.oPgFrm.Page1.oPag.oGAGRPCOD_1_5.mCond()
    this.oPgFrm.Page1.oPag.oGAFORAZI_1_6.enabled = this.oPgFrm.Page1.oPag.oGAFORAZI_1_6.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_20.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_20.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGACODAZI_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGACODAZI_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("New record")
          .Calculate_PTKCJTSVAY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_IPXVTQHCSR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GASRCPRG
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
    i_lTable = "MOD_GADG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2], .t., this.MOD_GADG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GASRCPRG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_AMG',True,'MOD_GADG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGSRCPRG like "+cp_ToStrODBC(trim(this.w_GASRCPRG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGSRCPRG,MGPREIMG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGSRCPRG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGSRCPRG',trim(this.w_GASRCPRG))
          select MGSRCPRG,MGPREIMG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGSRCPRG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GASRCPRG)==trim(_Link_.MGSRCPRG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GASRCPRG) and !this.bDontReportError
            deferred_cp_zoom('MOD_GADG','*','MGSRCPRG',cp_AbsName(oSource.parent,'oGASRCPRG_1_4'),i_cWhere,'GSUT_AMG',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGSRCPRG,MGPREIMG";
                     +" from "+i_cTable+" "+i_lTable+" where MGSRCPRG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGSRCPRG',oSource.xKey(1))
            select MGSRCPRG,MGPREIMG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GASRCPRG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGSRCPRG,MGPREIMG";
                   +" from "+i_cTable+" "+i_lTable+" where MGSRCPRG="+cp_ToStrODBC(this.w_GASRCPRG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGSRCPRG',this.w_GASRCPRG)
            select MGSRCPRG,MGPREIMG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GASRCPRG = NVL(_Link_.MGSRCPRG,space(50))
      this.w_PREIMG = NVL(_Link_.MGPREIMG,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_GASRCPRG = space(50)
      endif
      this.w_PREIMG = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])+'\'+cp_ToStr(_Link_.MGSRCPRG,1)
      cp_ShowWarn(i_cKey,this.MOD_GADG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GASRCPRG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MOD_GADG_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.MGSRCPRG as MGSRCPRG104"+ ",link_1_4.MGPREIMG as MGPREIMG104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on GAD_MAST.GASRCPRG=link_1_4.MGSRCPRG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and GAD_MAST.GASRCPRG=link_1_4.MGSRCPRG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GAGRPCOD
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRP_GADG_IDX,3]
    i_lTable = "GRP_GADG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2], .t., this.GRP_GADG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GAGRPCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_AGG',True,'GRP_GADG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GGCODICE like "+cp_ToStrODBC(trim(this.w_GAGRPCOD)+"%");

          i_ret=cp_SQL(i_nConn,"select GGCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GGCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GGCODICE',trim(this.w_GAGRPCOD))
          select GGCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GGCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GAGRPCOD)==trim(_Link_.GGCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GAGRPCOD) and !this.bDontReportError
            deferred_cp_zoom('GRP_GADG','*','GGCODICE',cp_AbsName(oSource.parent,'oGAGRPCOD_1_5'),i_cWhere,'GSUT_AGG',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GGCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GGCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GGCODICE',oSource.xKey(1))
            select GGCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GAGRPCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GGCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GGCODICE="+cp_ToStrODBC(this.w_GAGRPCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GGCODICE',this.w_GAGRPCOD)
            select GGCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GAGRPCOD = NVL(_Link_.GGCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_GAGRPCOD = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2])+'\'+cp_ToStr(_Link_.GGCODICE,1)
      cp_ShowWarn(i_cKey,this.GRP_GADG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GAGRPCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GAUTEESC
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GAUTEESC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_GAUTEESC);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_GAUTEESC)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_GAUTEESC) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oGAUTEESC_1_9'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GAUTEESC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_GAUTEESC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_GAUTEESC)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_GAUTEESC = NVL(_Link_.code,0)
      this.w_DESUTESC = NVL(_Link_.name,space(20))
    else
      this.w_DESUTESC = space(20)
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GAUTEESC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GACODUTE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GACODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_GACODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_GACODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_GACODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oGACODUTE_2_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GACODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_GACODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_GACODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_GACODUTE = NVL(_Link_.code,0)
      this.w_UTENTE = NVL(_Link_.name,space(50))
    else
      this.w_UTENTE = space(50)
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GACODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oGACODICE_1_1.value==this.w_GACODICE)
      this.oPgFrm.Page1.oPag.oGACODICE_1_1.value=this.w_GACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oGA__NOME_1_2.value==this.w_GA__NOME)
      this.oPgFrm.Page1.oPag.oGA__NOME_1_2.value=this.w_GA__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oGADESCRI_1_3.value==this.w_GADESCRI)
      this.oPgFrm.Page1.oPag.oGADESCRI_1_3.value=this.w_GADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oGASRCPRG_1_4.RadioValue()==this.w_GASRCPRG)
      this.oPgFrm.Page1.oPag.oGASRCPRG_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGAGRPCOD_1_5.RadioValue()==this.w_GAGRPCOD)
      this.oPgFrm.Page1.oPag.oGAGRPCOD_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGAFORAZI_1_6.RadioValue()==this.w_GAFORAZI)
      this.oPgFrm.Page1.oPag.oGAFORAZI_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGACNDATT_1_7.value==this.w_GACNDATT)
      this.oPgFrm.Page1.oPag.oGACNDATT_1_7.value=this.w_GACNDATT
    endif
    if not(this.oPgFrm.Page1.oPag.oGACHKATT_1_8.RadioValue()==this.w_GACHKATT)
      this.oPgFrm.Page1.oPag.oGACHKATT_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGAUTEESC_1_9.value==this.w_GAUTEESC)
      this.oPgFrm.Page1.oPag.oGAUTEESC_1_9.value=this.w_GAUTEESC
    endif
    if not(this.oPgFrm.Page1.oPag.oUTENTE_2_6.value==this.w_UTENTE)
      this.oPgFrm.Page1.oPag.oUTENTE_2_6.value=this.w_UTENTE
      replace t_UTENTE with this.oPgFrm.Page1.oPag.oUTENTE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTESC_1_26.value==this.w_DESUTESC)
      this.oPgFrm.Page1.oPag.oDESUTESC_1_26.value=this.w_DESUTESC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGA__PROP_2_1.value==this.w_GA__PROP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGA__PROP_2_1.value=this.w_GA__PROP
      replace t_GA__PROP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGA__PROP_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGAVALORE_2_2.value==this.w_GAVALORE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGAVALORE_2_2.value=this.w_GAVALORE
      replace t_GAVALORE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGAVALORE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODUTE_2_3.value==this.w_GACODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODUTE_2_3.value=this.w_GACODUTE
      replace t_GACODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODUTE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODAZI_2_4.RadioValue()==this.w_GACODAZI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODAZI_2_4.SetRadio()
      replace t_GACODAZI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODAZI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGA_RESET_2_5.RadioValue()==this.w_GA_RESET)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGA_RESET_2_5.SetRadio()
      replace t_GA_RESET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGA_RESET_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'GAD_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_GA__NOME))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oGA__NOME_1_2.SetFocus()
            i_bnoObbl = !empty(.w_GA__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GASRCPRG))  and (.cFunction = "Load")
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oGASRCPRG_1_4.SetFocus()
            i_bnoObbl = !empty(.w_GASRCPRG)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSUT_MAG.CheckForm()
      if i_bres
        i_bres=  .GSUT_MAG.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_GA__PROP))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSUT_MAG : Depends On
    this.GSUT_MAG.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_GA__PROP)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_GA__PROP=space(50)
      .w_GAVALORE=space(254)
      .w_GACODUTE=0
      .w_GACODAZI=space(5)
      .w_GA_RESET=space(1)
      .w_UTENTE=space(50)
      .DoRTCalc(1,11,.f.)
        .w_GACODUTE = -1
      .DoRTCalc(12,12,.f.)
      if not(empty(.w_GACODUTE))
        .link_2_3('Full')
      endif
      .DoRTCalc(13,13,.f.)
        .w_GACODAZI = 'xxx'
    endwith
    this.DoRTCalc(15,19,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_GA__PROP = t_GA__PROP
    this.w_GAVALORE = t_GAVALORE
    this.w_GACODUTE = t_GACODUTE
    this.w_GACODAZI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODAZI_2_4.RadioValue(.t.)
    this.w_GA_RESET = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGA_RESET_2_5.RadioValue(.t.)
    this.w_UTENTE = t_UTENTE
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_GA__PROP with this.w_GA__PROP
    replace t_GAVALORE with this.w_GAVALORE
    replace t_GACODUTE with this.w_GACODUTE
    replace t_GACODAZI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODAZI_2_4.ToRadio()
    replace t_GA_RESET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGA_RESET_2_5.ToRadio()
    replace t_UTENTE with this.w_UTENTE
    if i_srv='A'
      replace GA__PROP with this.w_GA__PROP
      replace GACODUTE with this.w_GACODUTE
      replace GACODAZI with this.w_GACODAZI
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
  func CanEdit()
    local i_res
    i_res=Vartype(oGadgetManager)<>'O'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Interfaccia gadget aperta: impossibile modificare"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=LookTab("MYG_DETT", "MGCODUTE", "MGCODGAD", this.w_GACODICE)==0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Gadget attualmente in uso da parte di uno o pi� utenti: impossibile cancellare"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsut_mgaPag1 as StdContainer
  Width  = 662
  height = 481
  stdWidth  = 662
  stdheight = 481
  resizeXpos=351
  resizeYpos=378
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGACODICE_1_1 as StdField with uid="BXLNZEZCDU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_GACODICE", cQueryName = "GACODICE",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 70634837,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=88, Top=26, InputMask=replicate('X',10)

  add object oGA__NOME_1_2 as StdField with uid="DFSTFLGNPI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GA__NOME", cQueryName = "GA__NOME",;
    bObbl = .t. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome del gadget",;
    HelpContextID = 226757973,;
   bGlobalFont=.t.,;
    Height=21, Width=269, Left=224, Top=26, InputMask=replicate('X',100)

  add object oGADESCRI_1_3 as StdMemo with uid="DOEAKKRFKB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GADESCRI", cQueryName = "GADESCRI",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della funzionalit� del gadget",;
    HelpContextID = 156220753,;
   bGlobalFont=.t.,;
    Height=149, Width=405, Left=88, Top=50, bdisablefrasimodello=.t.


  add object oGASRCPRG_1_4 as StdTableCombo with uid="UBAZTKOVWF",rtseq=4,rtrep=.f.,left=88,top=202,width=175,height=22;
    , ToolTipText = "Modello del gadget";
    , HelpContextID = 222416211;
    , cFormVar="w_GASRCPRG",tablefilter="", bObbl = .t. , nPag = 1;
    , cLinkFile="MOD_GADG";
    , cTable='MOD_GADG',cKey='MGSRCPRG',cValue='MGDESCRI',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  func oGASRCPRG_1_4.mCond()
    with this.Parent.oContained
      return (.cFunction = "Load")
    endwith
  endfunc

  func oGASRCPRG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oGASRCPRG_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oGAGRPCOD_1_5 as StdTableCombo with uid="YWIOFGVYIY",rtseq=5,rtrep=.f.,left=318,top=202,width=175,height=22;
    , cDescEmptyElement = "Nessuna";
    , ToolTipText = "Area di appartenenza";
    , HelpContextID = 158502230;
    , cFormVar="w_GAGRPCOD",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="GRP_GADG";
    , cTable='GRP_GADG',cKey='GGCODICE',cValue='GGDESCRI',cOrderBy='GGCODICE',xDefault=space(5);
  , bGlobalFont=.t.


  func oGAGRPCOD_1_5.mCond()
    with this.Parent.oContained
      return (.cFunction <> "Edit" or Empty(.w_GAGRPCOD_OLD) OR Empty(LookTab("MYG_DETT", "MGCODUTE", "MGCODGAD", .w_GACODICE, "MGCODGRP", .w_GAGRPCOD_OLD)))
    endwith
  endfunc

  func oGAGRPCOD_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oGAGRPCOD_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oGAFORAZI_1_6 as StdCheck with uid="XBKFFKVWNT",rtseq=6,rtrep=.f.,left=500, top=202, caption="Aziendale",;
    ToolTipText = "Se attivo il gadget � personalizzato per azienda",;
    HelpContextID = 78275247,;
    cFormVar="w_GAFORAZI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGAFORAZI_1_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..GAFORAZI,&i_cF..t_GAFORAZI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oGAFORAZI_1_6.GetRadio()
    this.Parent.oContained.w_GAFORAZI = this.RadioValue()
    return .t.
  endfunc

  func oGAFORAZI_1_6.ToRadio()
    this.Parent.oContained.w_GAFORAZI=trim(this.Parent.oContained.w_GAFORAZI)
    return(;
      iif(this.Parent.oContained.w_GAFORAZI=='S',1,;
      0))
  endfunc

  func oGAFORAZI_1_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oGAFORAZI_1_6.mCond()
    with this.Parent.oContained
      return (.cFunction = "Load")
    endwith
  endfunc

  add object oGACNDATT_1_7 as StdField with uid="QOZTWLMYAX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_GACNDATT", cQueryName = "GACNDATT",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Se la condizione � vera e il gadget � abilitato, verr� visualizzato nella Gadget Library",;
    HelpContextID = 63517370,;
   bGlobalFont=.t.,;
    Height=21, Width=405, Left=88, Top=226, InputMask=replicate('X',254)

  add object oGACHKATT_1_8 as StdCheck with uid="LRKKACUOCT",rtseq=8,rtrep=.f.,left=500, top=226, caption="Abilitato",;
    HelpContextID = 70464186,;
    cFormVar="w_GACHKATT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGACHKATT_1_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..GACHKATT,&i_cF..t_GACHKATT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oGACHKATT_1_8.GetRadio()
    this.Parent.oContained.w_GACHKATT = this.RadioValue()
    return .t.
  endfunc

  func oGACHKATT_1_8.ToRadio()
    this.Parent.oContained.w_GACHKATT=trim(this.Parent.oContained.w_GACHKATT)
    return(;
      iif(this.Parent.oContained.w_GACHKATT=='S',1,;
      0))
  endfunc

  func oGACHKATT_1_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oGAUTEESC_1_9 as StdField with uid="NQZKBXWTRV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_GAUTEESC", cQueryName = "GAUTEESC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente per uso esclusivo del gadget",;
    HelpContextID = 136293719,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=88, Top=251, bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_GAUTEESC"

  func oGAUTEESC_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oGAUTEESC_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGAUTEESC_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oGAUTEESC_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti",'',this.parent.oContained
  endproc


  add object oObj_1_15 as cp_showimage with uid="ISFYNPAOVG",left=500, top=50, width=150,height=150,;
    caption='Anteprima',;
   bGlobalFont=.t.,;
    default="",stretch=1,;
    nPag=1;
    , ToolTipText = "Immagine di anteprima";
    , HelpContextID = 140282493


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=279, width=648,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="GA__PROP",Label1="Propriet�",Field2="GAVALORE",Label2="Valore",Field3="GACODUTE",Label3="Utente",Field4="GACODAZI",Label4="Azienda",Field5="GA_RESET",Label5="Reset",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 17688186


  add object oLinkPC_1_20 as StdButton with uid="LUHCGMAKUR",left=601, top=0, width=49,height=46,;
    CpPicture="bmp/imp_lic.bmp", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Gestioni accessi al gadget";
    , HelpContextID = 14449008;
    , Caption="Accessi";
  , bGlobalFont=.t.

    proc oLinkPC_1_20.Click()
      this.Parent.oContained.GSUT_MAG.LinkPCClick()
    endproc

  func oLinkPC_1_20.mCond()
    with this.Parent.oContained
      return (!Empty(.w_GACODICE))
    endwith
  endfunc

  add object oDESUTESC_1_26 as StdField with uid="XIKDMURSCH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESUTESC", cQueryName = "DESUTESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 120506759,;
   bGlobalFont=.t.,;
    Height=21, Width=351, Left=142, Top=251, InputMask=replicate('X',20)

  add object oStr_1_10 as StdString with uid="JLPYWBACRU",Visible=.t., Left=37, Top=29,;
    Alignment=1, Width=50, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="QYKQHXTANA",Visible=.t., Left=176, Top=29,;
    Alignment=1, Width=47, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="HXGCBJLLPU",Visible=.t., Left=17, Top=53,;
    Alignment=1, Width=70, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="RNNEURROFH",Visible=.t., Left=13, Top=206,;
    Alignment=1, Width=74, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="TTYESBSGMF",Visible=.t., Left=28, Top=229,;
    Alignment=1, Width=60, Height=18,;
    Caption="Attivazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ZILVPVBCUI",Visible=.t., Left=269, Top=206,;
    Alignment=1, Width=48, Height=18,;
    Caption="Area:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="LHOFYQWQML",Visible=.t., Left=4, Top=253,;
    Alignment=1, Width=84, Height=18,;
    Caption="Esclusivo per:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=298,;
    width=647+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=299,width=646+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='cpusers|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oUTENTE_2_6.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='cpusers'
        oDropInto=this.oBodyCol.oRow.oGACODUTE_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oUTENTE_2_6 as StdTrsField with uid="IYMRNKAYUO",rtseq=16,rtrep=.t.,;
    cFormVar="w_UTENTE",value=space(50),enabled=.f.,;
    ToolTipText = "Descrizione dell'utente",;
    HelpContextID = 121018810,;
    cTotal="", bFixedPos=.t., cQueryName = "UTENTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=209, Left=52, Top=455, InputMask=replicate('X',50)

  add object oStr_2_7 as StdString with uid="SFFBVGDYYW",Visible=.t., Left=12, Top=457,;
    Alignment=1, Width=39, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_mgaBodyRow as CPBodyRowCnt
  Width=637
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oGA__PROP_2_1 as StdTrsField with uid="FKOSMHSMGP",rtseq=10,rtrep=.t.,;
    cFormVar="w_GA__PROP",value=space(50),isprimarykey=.t.,;
    ToolTipText = "Nome della propriet�",;
    HelpContextID = 174329162,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=211, Left=-2, Top=0, InputMask=replicate('X',50)

  add object oGAVALORE_2_2 as StdTrsField with uid="GLTCOOHPRA",rtseq=11,rtrep=.t.,;
    cFormVar="w_GAVALORE",value=space(254),;
    ToolTipText = "Valore della propriet�",;
    HelpContextID = 230858069,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=248, Left=214, Top=0, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oGAVALORE_2_2.mZoom
      with this.Parent.oContained
        GSAR_BMZ(this.Parent.oContained,this,"GSUT_KMV")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oGACODUTE_2_3 as StdTrsField with uid="DPQUMQFRAO",rtseq=12,rtrep=.t.,;
    cFormVar="w_GACODUTE",value=0,isprimarykey=.t.,;
    ToolTipText = "Codice dell'utente a cui si riferisce la propriet�",;
    HelpContextID = 130691755,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=468, Top=0, cSayPict=["9999"], cGetPict=["9999"], cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_GACODUTE"

  func oGACODUTE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oGACODUTE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  add object oGACODAZI_2_4 as stdTrsTableCombo with uid="DCXDFMFFTB",rtrep=.t.,;
    cFormVar="w_GACODAZI", tablefilter="" , ;
    ToolTipText = "Codice azienda",;
    HelpContextID = 63582895,;
    Height=22, Width=78, Left=526, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , xEmptyKey = 'xxx', cDescEmptyElement = "Nessuna";
, bIsInHeader=.f.;
    , cTable='AZIENDA',cKey='AZCODAZI',cValue='AZCODAZI',cOrderBy='AZCODAZI',xDefault=space(5);
  , bGlobalFont=.t.



  func oGACODAZI_2_4.mCond()
    with this.Parent.oContained
      return (.w_GAFORAZI='S')
    endwith
  endfunc

  add object oGA_RESET_2_5 as StdTrsCheck with uid="YLDHHXQTEN",rtrep=.t.,;
    cFormVar="w_GA_RESET",  caption="",;
    ToolTipText = "Se selezionato il valore della propriet� verr� sbiancato in fase di esportazione",;
    HelpContextID = 98497210,;
    Left=609, Top=0, Width=23,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oGA_RESET_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..GA_RESET,&i_cF..t_GA_RESET),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oGA_RESET_2_5.GetRadio()
    this.Parent.oContained.w_GA_RESET = this.RadioValue()
    return .t.
  endfunc

  func oGA_RESET_2_5.ToRadio()
    this.Parent.oContained.w_GA_RESET=trim(this.Parent.oContained.w_GA_RESET)
    return(;
      iif(this.Parent.oContained.w_GA_RESET=='S',1,;
      0))
  endfunc

  func oGA_RESET_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oGA__PROP_2_1.When()
    return(.t.)
  proc oGA__PROP_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oGA__PROP_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mga','GAD_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GACODICE=GAD_MAST.GACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_mga
*--- Se fallisce il link sulla tabella AZIENDA ritorno sempre "xxx"
Func GACODAZI_XXX(pGACODAZI)
   Return "xxx"
EndFunc
* --- Fine Area Manuale
