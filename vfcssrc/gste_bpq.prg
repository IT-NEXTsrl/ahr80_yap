* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bpq                                                        *
*              Stampa congruit� partite/mastrini                               *
*                                                                              *
*      Author: Nunzio Iorio                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_338]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-02                                                      *
* Last revis.: 2014-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bpq",oParentObject)
return(i_retval)

define class tgste_bpq as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_CODESE = space(4)
  w_FLSALI = space(1)
  w_FLSALF = space(1)
  w_TIPREG = space(1)
  w_NUMREG = 0
  w_BOOL = .f.
  w_DESCON = space(40)
  w_SQSALDI = 0
  w_SQMASTRI = 0
  w_TIPCON1 = space(1)
  w_PARTSN = space(1)
  w_VADECTOT = 0
  w_VASIMVAL = space(5)
  w_PTCODVAL = space(3)
  w_PTCAOVAL = 0
  w_PNDATDOC = ctod("  /  /  ")
  w_CODCON = space(15)
  w_ANCONSUP = space(15)
  w_MCSEZBIL = space(1)
  w_TOTPAR = 0
  w_TOTMAS = 0
  w_TOTSAL = 0
  w_CAOAPE = 0
  w_CAONAZ = 0
  w_CAOLIR = 0
  w_CODLIR = space(3)
  w_DATVAL = ctod("  /  /  ")
  w_DECTOP = 0
  w_OREPO = space(50)
  w_QUERYP = space(50)
  w_DATFIN = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_CATCON = space(5)
  w_CONSU = space(15)
  w_CONSU1 = space(15)
  w_CONSU2 = space(15)
  w_FLDAVE = space(1)
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_DECTOT = 0
  w_SIMVAL = space(6)
  w_PERCOM = space(1)
  w_TIPVAL = space(1)
  w_FLPROV = space(1)
  w_SEZBIL = space(1)
  w_INIESE = ctod("  /  /  ")
  w_FINESE = ctod("  /  /  ")
  w_FINES2 = ctod("  /  /  ")
  w_CODCAU = space(5)
  w_CODINI = space(15)
  w_CODFIN = space(15)
  w_PRIESE = space(4)
  w_DATPRE = ctod("  /  /  ")
  w_PREFIN = ctod("  /  /  ")
  w_PREINI = ctod("  /  /  ")
  w_ESEINI = space(4)
  w_PREESE = space(4)
  w_PRESUC = space(4)
  w_ESEPRE = space(4)
  w_VALINI = space(3)
  w_SALFIN = 0
  w_CAMBIO = 0
  w_APPO = space(4)
  w_APPO1 = space(4)
  w_APPO2 = 0
  w_CODCON = space(15)
  w_CONTRO = space(15)
  w_IMPCON = 0
  w_RIGCON = 0
  w_CONSUP = space(15)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_SALDO = 0
  w_SALPRO = 0
  w_SCRIVO = .f.
  w_TESTAT = .f.
  w_QUOZIENT = 0
  w_FLCALC = space(1)
  w_SERIAL = space(10)
  w_NURIGA = 0
  w_CODES1 = space(4)
  w_FLPRO1 = space(1)
  w_FLSALI = space(1)
  w_FLSALF = space(1)
  w_DATREG = ctod("  /  /  ")
  w_NUMRER = 0
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_NUMPRO = 0
  w_ALFPRO = space(10)
  w_DESSUP = space(50)
  w_DESRIG = space(50)
  w_FINES1 = ctod("  /  /  ")
  w_INICOM = ctod("  /  /  ")
  w_FINCOM = ctod("  /  /  ")
  w_OLDDAT = ctod("  /  /  ")
  w_DESCAU = space(35)
  w_DESCON = space(40)
  w_BILSEZ = space(1)
  w_OLDES1 = space(4)
  w_ULTESE = space(4)
  w_NEWCON = space(15)
  w_OLDCON = space(15)
  * --- WorkFile variables
  CONTI_idx=0
  MASTRI_idx=0
  CAU_CONT_idx=0
  VALUTE_idx=0
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora stampa Congruit� Partite/Mastrini (da GSTE_SCM)
    * --- Dichiaro le variabili della maschera
    * --- Variabili locali
    * --- Variabili passate solo perch� parametri presenti nella query GSCG_QSM (servono per un'altra gestione)
    this.w_OREPO = this.oParentObject.w_OREP
    this.w_QUERYP = ALLTRIM(this.oParentObject.w_OQRY)
    this.w_CODLIR = g_CODLIR
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODLIR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODLIR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAOLIR = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    GSTE_BPA (this, this.oParentObject.w_SCAINI , this.oParentObject.w_SCAFIN , this.oParentObject.w_TIPCON , this.w_CODCON ,this.w_CODCON , "   " , "", "RB","BO","RD","RI","MA","CA", "GSCG_BSA","N",cp_CharToDate("  -  -  "))
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    vq_exec(this.w_QUERYP, this, "PARTITE")
    * --- Solo parte aperta
    GSTE_BCP(this,"A","PARTITE" , , , , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Elabora i Cursori 
    if USED("PARTITE")
      SELECT PARTITE
      GO TOP
      SELECT ;
      TIPCON AS PNTIPCON, ;
      CODCON AS PNCODCON, ;
      DECTOT AS VADECTOT, ;
      SIMVAL AS VASIMVAL, ;
      CODVAL AS PTCODVAL, ;
      CAOVAL AS PTCAOVAL, ;
      iif(NOT EMPTY(CP_TODATE(DATDOC)),DATDOC,DATREG) AS PNDATDOC, ;
      TOTIMP AS TOTPAR, ;
      TOTIMP * 0 AS TOTMAS, ;
      TOTIMP * 0 AS TOTSAL, ;
      FLDAVE AS FLDAVE;
      FROM PARTITE WHERE NOT DELETED() INTO CURSOR PARTITE2 ORDER BY TIPCON,CODCON
      SELECT PARTITE
      USE
    endif
    * --- Crea il Temporaneo saldi mastrini
    vq_exec("QUERY\GSTE_BPQ", this, "MASTRINI")
    select * from PARTITE2 UNION ALL;
    select * from MASTRINI order by 1,2 into cursor PARTITE3
    * --- Crea il Temporaneo saldi contabili
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    select * from PARTITE3 UNION ALL;
    select * from PARSALDI order by 1,2 into cursor PARTITE1
    * --- Crea il Temporaneo di Appoggio
    CREATE CURSOR PARTVAL (TIPCON C(1), DESCON C(40), CODCON C(15), TOTPAR N(18,4), TOTMAS N(18,4), TOTSAL N(18,4), SQMASTRI N(5,2), SQSALDI N(5,2),SEZBIL C(1))
    this.w_SQSALDI = 0
    this.w_SQMASTRI = 0
    if USED("PARTITE1")
      SELECT PARTITE1
      GO TOP
      SCAN 
      this.w_TIPCON1 = PNTIPCON
      this.w_CODCON = PNCODCON
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP,ANDESCRI,ANPARTSN"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON1);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP,ANDESCRI,ANPARTSN;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPCON1;
              and ANCODICE = this.w_CODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        this.w_DESCON = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
        this.w_PARTSN = NVL(cp_ToDate(_read_.ANPARTSN),cp_NullValue(_read_.ANPARTSN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_ANCONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_ANCONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MCSEZBIL = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CODVAL = PTCODVAL
      this.w_CAOAPE = IIF(EMPTY(NVL(PTCAOVAL,0)), GETCAM(PTCODVAL, I_DATSYS), PTCAOVAL)
      this.w_DATVAL = NVL(PNDATDOC, cp_CharToDate("01-01-90"))
      if this.w_TIPCON1="G"
        * --- Setto il segno corretto nel caso di conto generico gastito a partite
        do case
          case this.w_MCSEZBIL $ "C-A"
            this.w_TOTPAR = IIF(FLDAVE="D",ABS(NVL(TOTPAR, 0)),ABS(NVL(TOTPAR,0)) *-1)
            this.w_TOTMAS = cp_round(IIF(FLDAVE="D",ABS(NVL(TOTMAS, 0)),ABS(NVL(TOTMAS,0)) *-1),g_PERPVL)
          case this.w_MCSEZBIL $ "R-P"
            this.w_TOTPAR = IIF(FLDAVE="A",ABS(NVL(TOTPAR, 0)),ABS(NVL(TOTPAR,0)) *-1)
            this.w_TOTMAS = cp_round(IIF(FLDAVE="A",ABS(NVL(TOTMAS, 0)),ABS(NVL(TOTMAS,0)) *-1),g_PERPVL)
        endcase
      else
        this.w_TOTPAR = IIF(FLDAVE="D",NVL(TOTPAR, 0),NVL(TOTPAR,0) *-1)
        this.w_TOTMAS = NVL(TOTMAS, 0)
      endif
      this.w_TOTSAL = NVL(TOTSAL, 0)
      this.w_DECTOP = g_PERPVL
      if g_PERVAL<>PTCODVAL
        * --- Converte gli Importi alla Moneta di Conto
        if this.w_TOTPAR<>0
          this.w_TOTPAR = VAL2VAL(this.w_TOTPAR, this.w_CAOAPE,this.w_DATVAL, I_DATSYS,1)
        endif
        if this.w_TOTMAS<>0 
          this.w_TOTMAS = cp_round(VAL2VAL(this.w_TOTMAS, this.w_CAOAPE,this.w_DATVAL, I_DATSYS,1),g_PERPVL)
        endif
        if this.w_TOTSAL<>0
          this.w_TOTSAL = VAL2VAL(this.w_TOTSAL, this.w_CAOAPE,this.w_DATVAL, I_DATSYS,1)
        endif
      endif
      if this.w_PARTSN = "S"
        INSERT INTO PARTVAL (TIPCON, CODCON, DESCON,TOTPAR,TOTMAS,TOTSAL,SQMASTRI,SQSALDI,SEZBIL) ;
        VALUES (this.w_TIPCON1, this.w_CODCON, this.w_DESCON,this.w_TOTPAR,this.w_TOTMAS,this.w_TOTSAL,this.w_SQMASTRI,this.w_SQSALDI,this.w_MCSEZBIL)
      endif
      SELECT PARTITE1
      ENDSCAN
    endif
    SELECT PARTVAL
    SELECT TIPCON,CODCON,DESCON,SUM(TOTPAR) AS TOTPAR,SUM(TOTMAS) AS TOTMAS,SUM(TOTSAL) AS TOTSAL, SUM(SQMASTRI) AS SQMASTRI, SUM(SQSALDI) AS SQSALDI, MAX(SEZBIL) AS SEZBIL;
    FROM PARTVAL INTO CURSOR PARTVAL1 GROUP BY TIPCON,CODCON ORDER BY TIPCON,CODCON
    * --- Rende il cursore editabile
    CreaCur =WRCURSOR("PARTVAL1")
    * --- Calcolo la mancanza di quadratura Mastrini e Saldi
    SELECT PARTVAL1
    GO TOP
    SCAN 
    if Nvl(TOTPAR,0) <> 0
      REPLACE SQMASTRI WITH (IIF(SEZBIL="P" AND TIPCON<>"G",-TOTPAR,TOTPAR)-TOTMAS) * 100 / TOTPAR
      REPLACE SQSALDI WITH (IIF(SEZBIL="P" AND TIPCON<>"G",-TOTPAR,TOTPAR)-TOTSAL) * 100 / TOTPAR
    else
      if Nvl(TOTMAS,0) <> 0
        REPLACE SQMASTRI WITH -100
      endif
      if Nvl(TOTSAL,0) <> 0
        REPLACE SQSALDI WITH -100
      endif
    endif
    ENDSCAN
    * --- Elimino i movimenti che non rispondono al check controllo
    if this.oParentObject.w_CONTROL = "QM"
      DELETE FROM PARTVAL1 WHERE SQMASTRI = 0
    endif
    if this.oParentObject.w_CONTROL = "QS"
      DELETE FROM PARTVAL1 WHERE SQSALDI = 0
    endif
    if this.oParentObject.w_CONTROL = "MM"
      DELETE FROM PARTVAL1 WHERE ABS(SQMASTRI) <= this.oParentObject.w_TOLLER
    endif
    if this.oParentObject.w_CONTROL = "MS"
      DELETE FROM PARTVAL1 WHERE ABS(SQSALDI) <= this.oParentObject.w_TOLLER
    endif
    * --- Seleziono il cursore da stampare ordinato per selezione w_ORDINAM
    do case
      case this.oParentObject.w_ORDINAM = "C"
        SELECT * FROM PARTVAL1 ;
         INTO CURSOR __tmp__ ORDER BY 1,2
      case this.oParentObject.w_ORDINAM = "D"
        SELECT * FROM PARTVAL1 ;
         INTO CURSOR __tmp__ ORDER BY 1,3
      case this.oParentObject.w_ORDINAM = "M"
        SELECT * FROM PARTVAL1 ;
         INTO CURSOR __tmp__ ORDER BY 7 desc
      case this.oParentObject.w_ORDINAM = "S"
        SELECT * FROM PARTVAL1 ;
         INTO CURSOR __tmp__ ORDER BY 8 desc
    endcase
    * --- Passo le variabili al report
    L_DACODCON=this.oParentObject.w_DACODCON
    L_ACODCON=this.oParentObject.w_ACODCON
    L_REGINI=this.oParentObject.w_REGINI
    L_REGFIN=this.oParentObject.w_REGFIN
    L_SCAINI=this.oParentObject.w_SCAINI
    L_SCAFIN=this.oParentObject.w_SCAFIN
    L_CONTROL=this.oParentObject.w_CONTROL
    L_TOLLER=this.oParentObject.w_TOLLER
    L_ORDINAM=this.oParentObject.w_ORDINAM
    L_FORSEL=IIF(this.oParentObject.w_TIPCON="F", this.oParentObject.w_DACODCON, " ")
    L_CONSEL=IIF(this.oParentObject.w_TIPCON="G", this.oParentObject.w_DACODCON, " ")
    L_CLISEL=IIF(this.oParentObject.w_TIPCON="C", this.oParentObject.w_DACODCON, " ")
    L_FORSEL1=IIF(this.oParentObject.w_TIPCON="F", this.oParentObject.w_ACODCON, " ")
    L_CONSEL1=IIF(this.oParentObject.w_TIPCON="G", this.oParentObject.w_ACODCON, " ")
    L_CLISEL1=IIF(this.oParentObject.w_TIPCON="C", this.oParentObject.w_ACODCON, " ")
    L_SCACLF=this.oParentObject.w_TIPCON
    L_DECTOP=this.w_DECTOP
    * --- Lancio il report
    CP_CHPRN( ALLTRIM(this.w_OREPO), " ", this )
    * --- Al termine Azzera i Cursori
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("PARTITE2")
      SELECT PARTITE2
      USE
    endif
    if USED("PARTITE1")
      SELECT PARTITE1
      USE
    endif
    if USED("PARTITE3")
      SELECT PARTITE3
      USE
    endif
    if USED("PARTVAL1")
      SELECT PARTVAL1
      USE
    endif
    if USED("PARTVAL")
      SELECT PARTVAL
      USE
    endif
    if USED("MASTRINI")
      SELECT MASTRINI
      USE
    endif
    if USED("SALDIESE")
      SELECT SALDIESE
      USE
    endif
    if USED("PARSALDI")
      SELECT PARSALDI
      USE
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Preparo le variabili per gestire le routine di gscg_bsm
    this.w_CODAZI = i_CODAZI
    if empty (this.oParentObject.w_REGFIN)
      this.w_DATFIN = i_FINDAT
    else
      this.w_DATFIN = this.oParentObject.w_REGFIN
    endif
    if empty (this.oParentObject.w_REGINI)
      this.w_DATINI = i_INIDAT
    else
      this.w_DATINI = this.oParentObject.w_REGINI
    endif
    this.w_CODINI = this.oParentObject.w_DACODCON
    this.w_CODFIN = this.oParentObject.w_ACODCON
    this.w_CODCAU = space (5)
    this.w_FINES2 = this.w_DATFIN
    this.w_FLPROV = "N"
    this.w_SEZBIL = "S"
    this.w_TIPVAL = "C"
    this.w_PERCOM = " "
    this.w_CODVAL = g_PERVAL
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Copio la struttura
    select * from PARTITE3 into cursor PARSALDI
    * --- Rende il cursore editabile
    CreaCur =WRCURSOR("PARSALDI")
    ZAP
    SELECT SALDIC
    GO TOP
    SCAN 
    this.w_TIPCON1 = this.oParentObject.w_TIPCON
    this.w_CODCON = CODCON
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCONSUP"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON1);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCONSUP;
        from (i_cTable) where;
            ANTIPCON = this.w_TIPCON1;
            and ANCODICE = this.w_CODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANCONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from MASTRI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MASTRI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MCSEZBIL"+;
        " from "+i_cTable+" MASTRI where ";
            +"MCCODICE = "+cp_ToStrODBC(this.w_ANCONSUP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MCSEZBIL;
        from (i_cTable) where;
            MCCODICE = this.w_ANCONSUP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MCSEZBIL = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_VADECTOT = this.w_DECTOT
    this.w_VASIMVAL = this.w_SIMVAL
    this.w_PTCODVAL = this.w_CODVAL
    this.w_PTCAOVAL = this.w_CAOVAL
    this.w_PNDATDOC = DATDOC
    this.w_TOTMAS = SALPRO * 0
    this.w_TOTPAR = SALPRO * 0
    this.w_FLDAVE = Nvl(FLDAVE," ")
    this.w_TOTSAL = cp_Round(SALPRO,g_PERPVL)
    if this.w_MCSEZBIL = "P" and this.w_TOTSAL <> 0
      this.w_TOTSAL = this.w_TOTSAL * -1
    endif
    INSERT INTO PARSALDI (PNTIPCON, PNCODCON, VADECTOT, VASIMVAL,PTCODVAL,PTCAOVAL,PNDATDOC,TOTPAR,TOTMAS,TOTSAL,FLDAVE) ;
    VALUES (this.w_TIPCON1, this.w_CODCON, this.w_VADECTOT,this.w_VASIMVAL,this.w_PTCODVAL,this.w_PTCAOVAL,this.w_PNDATDOC,this.w_TOTPAR,this.w_TOTMAS,this.w_TOTSAL,this.w_FLDAVE)
    SELECT SALDIC
    ENDSCAN
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sostanzialmente � la copia Stampa Schede Contabili (GSCG_BSM)
    *     sono state eliminate le istruzioni relative alle contropartite ed ai flag di stampa
    * --- inizio - le variabili caller sono state modificate in local
    * --- fine - le variabili caller sono state modificate in local
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se non inserito esercizio o la data di fine selezioe e' minore della fine esercizio non deve ricercare ev.reg esterne di competenza
    * --- Idem se la data di fine selezioe maggiore o uguale della data di fine esercizio successi (sarebbe compresa comunque)
    if EMPTY(this.w_CODESE) OR this.w_DATFIN<this.w_FINESE OR this.w_DATFIN>=this.w_FINES2
      this.w_FINES2 = this.w_DATFIN
    endif
    vq_exec("query\GSTE_QSM.VQR",this,"RIGDET")
    if used("RIGDET")
      CREATE CURSOR SALDICON ;
      (CODCON C(15), TIPREC C(1), DATREG D(8), NUMRER N(6,0), NUMDOC N(15,0), ALFDOC C(10), DATDOC D(8), NUMPRO N(15,0), ALFPRO C(10), ; 
      SERIAL C(10), NURIGA N(4,0), CODESE C(4), IMPDAR N(18,4), IMPAVE N(18,4), SALPRO N(18,4), FLCALC C(1), FLPROV C(1), ;
      VALNAZ C(3), FLSALI C(1), FLSALF C(1), DESSUP C(50), DESRIG C(50), FINESE D(8), SALREG N(18,4), INICOM D(8), FINCOM D(8), ;
      DESCAU C(35), DESCON C(40), CONTRO C(15), IMPCON N(18,4))
      this.w_OLDCON = "###@@@XXXZZZ"
      this.w_OLDDAT = cp_CharToDate("  -  -  ")
      this.w_OLDES1 = "xxxx"
      SELECT RIGDET
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CODCON, " ")) 
      this.w_CODCON = CODCON
      this.w_CONSUP = NVL(CONSUP, SPACE(15))
      this.w_FLDAVE = Nvl(FLDAVE," ")
      this.w_TESTAT = .F.
      this.w_SALDO = 0
      if this.w_OLDCON<>this.w_CODCON
        this.w_OLDCON = this.w_CODCON
        this.w_SCRIVO = .T.
        this.w_SALPRO = 0
        if this.w_SEZBIL<>"S"
          * --- Elimina le Sezioni di Bilancio non selezionate
          this.w_SCRIVO = IIF(CALCSEZ(this.w_CONSUP)=this.w_SEZBIL, .T. , .F.)
        endif
        if this.w_SCRIVO=.T.
          * --- Ad ogni Nuovo Codice scrive (sul primo record il Saldo Iniziale)
          this.w_TESTAT = .T.
          SELECT SALPRE
          GO TOP
          LOCATE FOR CODCON=this.w_CODCON
          if FOUND()
            this.w_PRIESE = NVL(PRIESE,SPACE(4))
            * --- Saldo Iniziale
            this.w_SALDO = SALREG
            if this.w_CODVAL<>g_PERVAL
              * --- Converte alla Valuta di Stampa
              this.w_SALDO = cp_ROUND((this.w_SALDO / g_CAOVAL) * this.w_CAOVAL, 6)
            endif
            this.w_BILSEZ = SEZBIL
            if this.oParentObject.w_TIPCON="G"
              * --- Setto il segno corretto se conto gestito a partite
              do case
                case this.w_SEZBIL $ "C-A"
                  this.w_SALPRO = IIF(this.w_FLDAVE="D",ABS(this.w_SALDO),ABS(this.w_SALDO)*-1)
                case this.w_SEZBIL $ "R-P"
                  this.w_SALPRO = IIF(this.w_FLDAVE="A",ABS(this.w_SALDO),ABS(this.w_SALDO)*-1)
              endcase
            else
              this.w_SALPRO = this.w_SALDO
            endif
          endif
        endif
        SELECT RIGDET
      endif
      if this.w_SCRIVO=.T.
        this.w_IMPDAR = NVL(IMPDAR, 0)
        this.w_IMPAVE = NVL(IMPAVE, 0)
        this.w_VALINI = NVL(VALNAZ, g_PERVAL)
        if this.w_VALINI <> g_PERVAL
          * --- Converte registrazioni in lire alla Valuta di Conto
          this.w_IMPDAR = cp_ROUND((this.w_IMPDAR / IIF(this.w_VALINI=g_CODLIR, g_CAOEUR, 1)) * g_CAOVAL, 6)
          this.w_IMPAVE = cp_ROUND((this.w_IMPAVE / IIF(this.w_VALINI=g_CODLIR, g_CAOEUR, 1)) * g_CAOVAL, 6)
        endif
        if this.w_CODVAL<>g_PERVAL
          * --- Converte alla Valuta di Stampa
          this.w_IMPDAR = cp_ROUND((this.w_IMPDAR / g_CAOVAL) * this.w_CAOVAL, 6)
          this.w_IMPAVE = cp_ROUND((this.w_IMPAVE / g_CAOVAL) * this.w_CAOVAL, 6)
        endif
        this.w_IMPDAR = this.w_IMPDAR
        this.w_IMPAVE = this.w_IMPAVE
        this.w_SERIAL = NVL(SERIAL, SPACE(10))
        this.w_NURIGA = NVL(NURIGA, 0)
        this.w_CODES1 = NVL(CODESE, SPACE(4))
        this.w_FLPRO1 = NVL(FLPROV, " ")
        this.w_FLSALI = NVL(FLSALI, " ")
        this.w_FLSALF = NVL(FLSALF, " ")
        this.w_DATREG = CP_TODATE(DATREG)
        this.w_NUMRER = NVL(NUMRER, 0)
        this.w_NUMDOC = NVL(NUMDOC, 0)
        this.w_ALFDOC = NVL(ALFDOC, Space(10))
        this.w_DATDOC = CP_TODATE(DATDOC)
        this.w_NUMPRO = NVL(NUMPRO, 0)
        this.w_ALFPRO = NVL(ALFPRO, Space(10))
        this.w_DESSUP = NVL(DESSUP, " ")
        this.w_DESRIG = NVL(DESRIG, " ")
        this.w_FINES1 = CP_TODATE(FINESE)
        this.w_INICOM = CP_TODATE(INICOM)
        this.w_FINCOM = CP_TODATE(FINCOM)
        this.w_DESCAU = NVL(DESCAU, " ")
        this.w_DESCON = NVL(DESCON, " ")
        if this.w_TESTAT=.T.
          * --- Scrive il Record di testata 
          INSERT INTO SALDICON ;
          (CODCON, TIPREC, DATREG, IMPDAR, IMPAVE, SALPRO, VALNAZ, SALREG, DESCON) VALUES ;
          (this.w_CODCON, "A", this.w_DATPRE, 0, 0, this.w_SALPRO, this.w_VALINI, this.w_SALDO, this.w_DESCON)
          this.w_OLDDAT = this.w_DATREG
          this.w_OLDES1 = this.w_CODES1
        endif
        * --- Se 'N non concorre per i calcolo del saldo progressivo
        this.w_FLCALC = "N"
        if this.w_FLSALI=" " AND this.w_FLSALF=" " AND this.w_FINES1>this.w_DATPRE AND (this.w_DATFIN=this.w_FINESE OR NOT (this.w_DATREG>this.w_DATFIN AND this.w_DATREG<=this.w_FINES2))
          this.w_FLCALC = " "
        endif
        this.w_FLCALC = IIF(this.w_CODES1=this.w_PRIESE AND NOT EMPTY(this.w_FLSALI), " ", this.w_FLCALC)
        * --- Ricalcola il Saldo Progressivo dei Conti Economici al cambio esercizio
        if this.w_BILSEZ $ "CR" AND this.w_CODES1<>this.w_OLDES1 AND this.w_DATREG<= this.w_FINES1 
          this.w_SALPRO = 0
        endif
        * --- Se e' una Reg di Chiusura e non e' Cambiato l'esercizio Azzera il Saldo Progressivo
        if EMPTY(this.w_CODESE)
          if this.w_BILSEZ $ "CR" AND this.w_FLSALF<>" " AND this.w_CODES1=this.w_OLDES1
            this.w_SALPRO = 0
          endif
        endif
        if this.w_BILSEZ $ "CR" AND this.w_DATREG<= this.w_FINES1 
          this.w_OLDES1 = this.w_CODES1
        endif
        * --- Scrive le righe di Stampa (Per il Saldo Finale, Esclude anche le Righe non Confermate)
        * --- Se 'N' non stampa il valore del saldo (cambio data)
        this.w_SALPRO = this.w_SALPRO+ IIF(this.w_FLCALC="N" OR this.w_FLPRO1="S", 0 , (this.w_IMPDAR-this.w_IMPAVE))
        * --- Se stampa , ricerca le Contropartite
        this.w_CONTRO = SPACE(15)
        this.w_IMPCON = 0
        this.w_RIGCON = -1
        INSERT INTO SALDICON ;
        (CODCON, TIPREC, DATREG, NUMRER, NUMDOC, ALFDOC, DATDOC, NUMPRO, ALFPRO, SERIAL, NURIGA, ;
        CODESE, IMPDAR, IMPAVE, SALPRO, FLCALC, FLPROV, VALNAZ, FLSALI, FLSALF, ;
        DESSUP, DESRIG, FINESE, SALREG, INICOM, FINCOM, DESCAU, CONTRO, IMPCON) ;
        VALUES (this.w_CODCON, "B", this.w_DATREG, this.w_NUMRER, this.w_NUMDOC, this.w_ALFDOC, this.w_DATDOC, this.w_NUMPRO, this.w_ALFPRO, this.w_SERIAL, this.w_NURIGA, ;
        this.w_CODES1, this.w_IMPDAR, this.w_IMPAVE, this.w_SALPRO, this.w_FLCALC, this.w_FLPRO1, this.w_VALINI, this.w_FLSALI, this.w_FLSALF, ;
        this.w_DESSUP, this.w_DESRIG, this.w_FINESE, 0, this.w_INICOM, this.w_FINCOM, this.w_DESCAU, this.w_CONTRO, this.w_IMPCON)
      endif
      SELECT RIGDET
      ENDSCAN
    endif
    select *,IIF(Nvl(IMPAVE,0)<>0,"A","D") AS FLDAVE FROM SALDICON GROUP BY CODCON INTO CURSOR SALDIC
    * --- Chiude i Cursori
    if used("RIGDET")
      select RIGDET
      use
    endif
    if used("CALSAL")
      select CALSAL
      use
    endif
    if used("SALPRE")
      select SALPRE
      use
    endif
    if used("SALDICON")
      select SALDICON
      use
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ATTEZIONE: Questo Blocco e' sostanzialmente identico a quello in GSCG_BVM
    * --- Data Fine Esercizio Successivo (serve per ricercare eventuali registrazioni non comprese nell'intervallo di ricerca ma di competenza del periodo)
    this.w_FINES2 = this.w_DATFIN
    this.w_PREINI = i_INIDAT
    this.w_PREFIN = i_INIDAT
    this.w_PREESE = "ZZXX"
    this.w_PRESUC = "XXZZ"
    this.w_ESEINI = CALCESER(this.w_DATINI," ")
    * --- Data Precedente la Selezione Iniziale
    this.w_DATPRE = this.w_DATINI - 1
    * --- Esercizio alla Data Iniziale - 1
    this.w_ESEPRE = CALCESER(this.w_DATPRE," ")
    * --- Esercizio alla Data di Fine Selezione (seve per filtrare nel saldo Finale movimenti di conti Economici di esercizi precedenti)
    this.w_ULTESE = this.w_CODESE
    if EMPTY(this.w_ULTESE)
      this.w_ULTESE = CALCESER(this.w_DATFIN, g_CODESE)
    endif
    * --- Select from ESERCIZI
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ESERCIZI ";
          +" where ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+"";
          +" order by ESINIESE";
           ,"_Curs_ESERCIZI")
    else
      select * from (i_cTable);
       where ESCODAZI=this.w_CODAZI;
       order by ESINIESE;
        into cursor _Curs_ESERCIZI
    endif
    if used('_Curs_ESERCIZI')
      select _Curs_ESERCIZI
      locate for 1=1
      do while not(eof())
      * --- Data Fine Esercizio Successivo (serve per ricercare eventuali registrazioni non comprese nell'intervallo di ricerca ma di competenza del periodo)
      if CP_TODATE(_Curs_ESERCIZI.ESINIESE)=this.w_FINESE+1
        this.w_FINES2 = CP_TODATE(_Curs_ESERCIZI.ESFINESE)
      endif
      if CP_TODATE(_Curs_ESERCIZI.ESFINESE)<=this.w_DATPRE
        this.w_PREINI = CP_TODATE(_Curs_ESERCIZI.ESINIESE)
        * --- Data di Fine dell'Ultimo Esercizio Interamente precedente la data di Inizio Selezione (Fin qui' leggo solo i Saldi)
        this.w_PREFIN = CP_TODATE(_Curs_ESERCIZI.ESFINESE)
        this.w_PREESE = NVL(_Curs_ESERCIZI.ESCODESE, "XXZZ")
      endif
        select _Curs_ESERCIZI
        continue
      enddo
      use
    endif
    * --- Per ricercare registrazioni dell'esercizio INTERAMENTE precedente la Selezione ma di competenza dell' esercizio selezionato
    * --- (devono essere considerate ai fini del Saldo iniziale)
    this.w_PRESUC = IIF(this.w_ESEINI=this.w_PREESE, "XXZZ", this.w_ESEINI)
    this.w_PRIESE = SPACE(4)
    this.w_BILSEZ = " "
    this.w_SALDO = 0
    this.w_OLDCON = "###@@@XXXZZZ"
    CREATE CURSOR SALPRE (CODCON C(15), PRIESE C(4), SEZBIL C(1), SALREG N(18,4))
    * --- Leggo Saldi fino all'Esercizio interamente la data Iniziale + le registrazioni dell'esercizio fino alla data iniziale
    vq_exec("query\GSTE1QSM.VQR",this,"CALSAL")
    SELECT CALSAL
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODESE,"    ")) AND NOT EMPTY(NVL(CODCON, " ")) 
    this.w_NEWCON = CODCON
    if this.w_OLDCON<>this.w_NEWCON
      * --- Nuovo Conto
      if this.w_OLDCON<>"###@@@XXXZZZ"
        INSERT INTO SALPRE (CODCON, PRIESE, SEZBIL, SALREG) VALUES (this.w_OLDCON, this.w_PRIESE, this.w_BILSEZ, this.w_SALDO)
        SELECT CALSAL
      endif
      this.w_OLDCON = this.w_NEWCON
      this.w_SALDO = 0
      * --- Primo esercizio utilizzato dal Conto
      this.w_PRIESE = NVL(CODESE,SPACE(4))
      * --- Se Conto Economico non considera i Saldi Iniziali (ma legge comunque il primo esercizio Movimentato)
      this.w_BILSEZ = " "
      this.w_CONSUP = NVL(CONSUP, SPACE(15))
      if NOT EMPTY(this.w_CONSUP) 
        this.w_BILSEZ = CALCSEZ(this.w_CONSUP)
      endif
    endif
    this.w_VALINI = NVL(VALNAZ, g_PERVAL)
    this.w_CAMBIO = IIF(this.w_VALINI=g_CODLIR, g_CAOEUR, 1)
    if NVL(TIPREC, " ")="A"
      * --- Legge Saldi dei periodi (interi) precedenti la data iniziale
      if CP_TODATE(FINESE)<=this.w_DATPRE AND NOT this.w_BILSEZ $ "CR"
        * --- I saldi Iniziali vanno calcolati solo se non e' Conto Economico
        * --- Legge solo i saldi del Mov. in linea tranne il primo al quale considera anche l'eventuale apertura
        this.w_APPO2 = NVL(TOTIMP,0) - IIF(CODESE=this.w_PRIESE, 0, NVL(SALINI, 0))
        if this.w_VALINI <> g_PERVAL
          this.w_APPO2 = cp_ROUND((this.w_APPO2 / this.w_CAMBIO) * g_CAOVAL, 6)
        endif
        this.w_SALDO = this.w_SALDO + this.w_APPO2
      endif
    else
      * --- Leggo i Movimenti dell'eventuale esercizio precedente la data di inizio selezione
      * --- Esclude i Movimenti di Saldo Iniziale tranne il Primo
      if EMPTY(NVL(FLSALI," ")) OR this.w_ESEPRE=this.w_PRIESE
        if this.w_VALINI = g_PERVAL
          this.w_SALDO = this.w_SALDO + NVL(TOTIMP,0)
        else
          this.w_SALDO = this.w_SALDO + cp_ROUND((NVL(TOTIMP,0) / this.w_CAMBIO) * g_CAOVAL, 6)
        endif
      endif
    endif
    ENDSCAN
    if this.w_OLDCON<>"###@@@XXXZZZ"
      * --- Ultimo Codice
      INSERT INTO SALPRE (CODCON, PRIESE, SEZBIL, SALREG) VALUES (this.w_OLDCON, this.w_PRIESE, this.w_BILSEZ, this.w_SALDO)
    endif
    if used("CALSAL")
      select CALSAL
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='ESERCIZI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
