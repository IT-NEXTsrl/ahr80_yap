* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ggk                                                        *
*              Gadget KPI Grid                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-16                                                      *
* Last revis.: 2015-12-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_ggk",oParentObject))

* --- Class definition
define class tgsut_ggk as StdGadgetForm
  Top    = 8
  Left   = 16

  * --- Standard Properties
  Width  = 147
  Height = 104
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-18"
  HelpContextID=232176791
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  ELABKPIM_IDX = 0
  cPrg = "gsut_ggk"
  cComment = "Gadget KPI Grid"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_VALUE = space(10)
  w_TITLE = space(254)
  o_TITLE = space(254)
  w_KCODICE = space(10)
  w_RET = .F.
  w_DATETIME = space(20)
  w_PROGRAM = space(250)
  w_CURSOR = space(10)
  o_CURSOR = space(10)
  w_PRGGRID = space(250)
  w_BTRSGRID = .F.
  w_VALUEFORMAT = space(50)
  w_KDATETIME = ctot('')
  w_KVALUE = 0
  w_PRGDESCRI = space(50)
  w_CODICE = space(10)
  w_TMPCUR = space(10)
  w_oGrid = .NULL.
  w_oFooter = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_ggk
  bMouseHover = .F.
  
  *--- ApplyTheme
  Proc ApplyTheme()
     DoDefault()
     Local l_oldErr, l_OnErrorMsg
     With This
       m.l_OnErrorMsg = ""
       m.l_oldErr = On("Error")
       On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
       
       .w_oGrid.ForeColor = Iif(.nFontColor<>-1, .nFontColor, Rgb(243,243,243))
       
       On Error &l_oldErr
       If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
          .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
       Endif
       
       .mCalc(.t.)
       .SaveDependsOn()
     Endwith
  EndProc
  
  *--- AddMenu
  *-- Voci a men� per la grid
  Proc AddMenu(loPopupMenu)
     DoDefault()
     *-- Oggetto i_PopupCurForm contiene Thisform
     loMenuItem = m.loPopupMenu.AddMenuItem()
     loMenuItem.Caption = cp_Translate(MSG_EXCEL_EXPORT)
     loMenuItem.BeginGroup = .T.
     loMenuItem.Visible = .T.
     loMenuItem.OnClick = "i_PopupCurForm.w_oGrid.grd.CursorExport('E')"
     loMenuItem = m.loPopupMenu.AddMenuItem()
     loMenuItem.Caption = cp_Translate(MSG_DBF_EXPORT)
     loMenuItem.BeginGroup = .F.
     loMenuItem.Visible = .T.
     loMenuItem.OnClick = "i_PopupCurForm.w_oGrid.grd.CursorExport('D')"
     loMenuItem = m.loPopupMenu.AddMenuItem()
     loMenuItem.Caption = cp_Translate(MSG_CSV_EXPORT)
     loMenuItem.BeginGroup = .F.
     loMenuItem.Visible = .T.
     loMenuItem.OnClick = "i_PopupCurForm.w_oGrid.grd.CursorExport('C')"
  EndProc
  * --- Fine Area Manuale
  *--- Define Header
  add object oHeader as cp_headergadget with Width=this.Width, Left=0, Top=0, nNumPages=1 
  Height = This.Height + this.oHeader.Height

  * --- Define Page Frame
  add object oPgFrm as cp_oPgFrm with PageCount=1, Top=this.oHeader.Height, Width=this.Width, Height=this.Height-this.oHeader.Height
  proc oPgFrm.Init
    cp_oPgFrm::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_ggkPag1","gsut_ggk",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Sintetico")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oGrid = this.oPgFrm.Pages(1).oPag.oGrid
    this.w_oFooter = this.oPgFrm.Pages(1).oPag.oFooter
    DoDefault()
    proc Destroy()
      this.w_oGrid = .NULL.
      this.w_oFooter = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ELABKPIM'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VALUE=space(10)
      .w_TITLE=space(254)
      .w_KCODICE=space(10)
      .w_RET=.f.
      .w_DATETIME=space(20)
      .w_PROGRAM=space(250)
      .w_CURSOR=space(10)
      .w_PRGGRID=space(250)
      .w_BTRSGRID=.f.
      .w_VALUEFORMAT=space(50)
      .w_KDATETIME=ctot("")
      .w_KVALUE=0
      .w_PRGDESCRI=space(50)
      .w_CODICE=space(10)
      .w_TMPCUR=space(10)
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_KCODICE))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.oGrid.Calculate(.w_CURSOR)
          .DoRTCalc(4,9,.f.)
        .w_VALUEFORMAT = ''
      .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
        .DoRTCalc(11,14,.f.)
        if not(empty(.w_CODICE))
          .link_1_18('Full')
        endif
    endwith
    this.DoRTCalc(15,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
      	*--- Assegnamento caption ad oggetto header
          If .o_TITLE <> .w_TITLE
			   .oHeader.Calculate(.oPgFrm.Page1.oPag.oContained.w_TITLE)
          EndIf
        .DoRTCalc(1,2,.t.)
          .link_1_4('Full')
        if .o_CURSOR<>.w_CURSOR
        .oPgFrm.Page1.oPag.oGrid.Calculate(.w_CURSOR)
        endif
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
        .DoRTCalc(4,13,.t.)
          .link_1_18('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oGrid.Calculate(.w_CURSOR)
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
    endwith
  return

  proc Calculate_OGRWDURRLK()
    with this
          * --- Refresh Label
          .w_RET = ReadResult(This)
    endwith
  endproc
  proc Calculate_YCUUTVASEB()
    with this
          * --- Gadget Option
          .w_RET = This.GadgetOption()
          .w_CODICE = .w_CODICE
          .link_1_18('Full')
          .w_RET = ReadResult(This)
    endwith
  endproc
  proc Calculate_LSNUCPBSNU()
    with this
          * --- Dimensioni gadget massimizzato
          .w_RET = CalcSize(This)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("GadgetOnDemand") or lower(cEvent)==lower("GadgetArranged") or lower(cEvent)==lower("GadgetOnTimer") or lower(cEvent)==lower("oheader RefreshOnDemand")
          .Calculate_OGRWDURRLK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GadgetOption")
          .Calculate_YCUUTVASEB()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oGrid.Event(cEvent)
      .oPgFrm.Page1.oPag.oFooter.Event(cEvent)
        if lower(cEvent)==lower("GadgetMaximize Init") or lower(cEvent)==lower("GadgetMinimize Init")
          .Calculate_LSNUCPBSNU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_ggk
    Do Case
       Case Lower(cEvent)='w_ogrid dblclick'
         With This
           If !Empty(.w_PRGGRID)
             Local cCursor,l_oldArea,l_oldError,ggname
             m.ggname = 'frm'+Alltrim(.Name)
             .cAlertMsg = ""
             m.l_oldError = On("Error")
             On Error &ggname..cAlertMsg=Message()
             
             m.l_oldArea = Select()
             Select(.w_CURSOR)
             Go Top
             Skip (.w_oGrid.CurrentRow-1)
             ExecScript(.w_PRGGRID)
             Select(m.l_oldArea)
             
             On error &l_oldError
           Endif
         Endwith
       Case Lower(cEvent)='done'
         Use in Select (This.w_CURSOR)
    Endcase
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=KCODICE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
    i_lTable = "ELABKPIM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2], .t., this.ELABKPIM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EKCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where EKCODICE="+cp_ToStrODBC(this.w_KCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EKCODICE',this.w_KCODICE)
            select EKCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KCODICE = NVL(_Link_.EKCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_KCODICE = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])+'\'+cp_ToStr(_Link_.EKCODICE,1)
      cp_ShowWarn(i_cKey,this.ELABKPIM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
    i_lTable = "ELABKPIM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2], .t., this.ELABKPIM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EKCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where EKCODICE="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EKCODICE',this.w_CODICE)
            select EKCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.EKCODICE,space(10))
      this.w_KCODICE = NVL(_Link_.EKCODICE,space(10))
    else
      this.w_KCODICE = space(10)
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])+'\'+cp_ToStr(_Link_.EKCODICE,1)
      cp_ShowWarn(i_cKey,this.ELABKPIM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TITLE = this.w_TITLE
    this.o_CURSOR = this.w_CURSOR
    return

enddefine

* --- Define pages as container
define class tgsut_ggkPag1 as StdContainer
  Width  = 147
  height = 104
  stdWidth  = 147
  stdheight = 104
  resizeXpos=86
  resizeYpos=64
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oGrid as cp_VirtualZoom with uid="DKKFDHJZBW",left=4, top=3, width=140,height=75,;
    caption='Griglia',;
   bGlobalFont=.t.,;
    ScrollBarsWidth=8,bTrsHeaderTitle=.f.,HeaderFontBold=.t.,HeaderFontItalic=.f.,HeaderFontSize=0,;
    nPag=1;
    , HelpContextID = 33242266


  add object oFooter as cp_FooterGadget with uid="UMXGAILDJX",left=4, top=79, width=135,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 126696474
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ggk','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_ggk
*--- Refresh
Proc ReadResult(pParent)
  Local cCursor,l_oldError,l_OnErrorMsg,l_AlertMsg
  With m.pParent
      m.l_AlertMsg = ""
      m.l_OnErrorMsg = ""
      m.l_oldErr = On("Error")
      On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
      
      *--- Aggiorno la data di ultimo aggiornamento
      .tLastUpdate = DateTime()
      
      If !Empty(.w_KCODICE)
        .w_KCODICE = Alltrim(.w_KCODICE)
        *--- Trascodifica campi griglia
        If Vartype(.w_BTRSGRID)=='L' && Gestione trascodifiche
          .w_oGrid.bTrsHeaderTitle = .w_BTRSGRID
        Endif
        
        .w_TMPCUR = 'Gadget_GGK'+Sys(2015)
        If get_result_kpi(m.pParent, .w_KCODICE, 'E', 'w_KDATETIME', 'w_KVALUE', .w_TMPCUR)
          *--- Risultato sintetico
          .w_VALUE = Iif(!IsNull(.w_KVALUE), Alltrim(Trans(.w_KVALUE,.w_VALUEFORMAT)), '--')
          
          *--- Data e ora del risultato
          .w_DATETIME = ah_msgformat('Elaborato il %1 ',Alltrim(TTOC(.w_KDATETIME)))
          
          *--- Passo il cursore alla griglia
          If !Empty(.w_CURSOR) And Used(.w_CURSOR)
            Use in Select (.w_CURSOR)
          Endif
          .w_CURSOR = .w_TMPCUR
          
       Else
          .w_VALUE = ''
          .w_DATETIME = ah_msgformat('Aggiornato il %1 ',Alltrim(TTOC(.tLastUpdate)))
          m.l_AlertMsg = cp_Translate('Nessun risultato trovato')
       EndIf
       
     Else
       m.l_AlertMsg = ah_MsgFormat('Codice KPI non definito o inesistente:%0"%1"', Alltrim(.w_CODICE))
     Endif
     
     *--- Eventuale messaggio di errore
      .cAlertMsg = m.l_AlertMsg
      
      On Error &l_oldErr
      If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
        .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
      Endif
      
     .mCalc(.T.)
     .SaveDependsOn()
  EndWith
EndProc

Proc CalcSize(pParent)
  With m.pParent
     If .MaxWidth=-1 And .MaxHeight=-1
       local nWidth,nHeight
       *--- Header e testata zoom
       m.nHeight = (.Height-.w_oGrid.Height)+.w_oGrid.nHeaderHeight+(.w_oGrid.nPadding*3)
       *--- Righe
       m.nHeight = m.nHeight + (.w_oGrid.Grd.RowHeight+.w_oGrid.Grd.nVOffset)*.w_oGrid.Grd.RowCount
       m.nHeight = m.nHeight - .w_oGrid.Grd.nVOffset + .w_oGrid.ScrollBarsWidth
       
       m.nWidth = (.Width-.w_oGrid.Width)+(.w_oGrid.nPadding*2)
       *--Colonne
       For i=1 To .w_oGrid.ColumnCount
         m.nWidth = m.nWidth + .w_oGrid.Grd.Columns(i).Width + .w_oGrid.Grd.nHOffset
       Endfor
       m.nWidth = m.nWidth - .w_oGrid.Grd.nHOffset + .w_oGrid.ScrollBarsWidth
       
       .MaxWidth = Max(.Width, m.nWidth)
       .MaxHeight = Max(.Height, m.nHeight)
     Else
       .MaxWidth = -1
       .MaxHeight = -1
     Endif
  EndWith
EndProc
* --- Fine Area Manuale
