* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bkn                                                        *
*              Elimina nominativi cliente                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-01-11                                                      *
* Last revis.: 2014-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bkn",oParentObject)
return(i_retval)

define class tgsar_bkn as StdBatch
  * --- Local variables
  w_GSAR_ANO = .NULL.
  w_NOMCOD = space(15)
  w_TOTCOD = space(15)
  w_CLIVUOTO = space(15)
  w_CODICE = space(15)
  w_NOMCOD = space(15)
  w_CODCLI = space(20)
  w_NOTIPNOM = space(1)
  w_DELERR = 0
  w_DELNOM = .f.
  w_TIPCLIVUOTO = space(1)
  * --- WorkFile variables
  OFF_NOMI_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminato Cliente: elimina Anagrafica dei Nominativi
    this.w_DELERR = 0
    this.w_TOTCOD = this.oParentObject.w_ANCODICE
    this.w_CLIVUOTO = SPACE(15)
    this.w_CODCLI = SPACE(20)
    if (g_OFFE="S" OR g_AGEN="S")
      * --- Elimino i clienti
      * --- Read from OFF_NOMI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NOCODICE"+;
          " from "+i_cTable+" OFF_NOMI where ";
              +"NOCODCLI = "+cp_ToStrODBC(this.oParentObject.w_ANCODICE);
              +" and NOTIPCLI = "+cp_ToStrODBC(this.oParentObject.w_ANTIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NOCODICE;
          from (i_cTable) where;
              NOCODCLI = this.oParentObject.w_ANCODICE;
              and NOTIPCLI = this.oParentObject.w_ANTIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NOMCOD = NVL(cp_ToDate(_read_.NOCODICE),cp_NullValue(_read_.NOCODICE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_NOMCOD)
        this.w_DELNOM = ah_yesNo("Vuoi cancellare i nominativi collegati?")
        if this.w_DELNOM
          this.w_GSAR_ANO = GSAR_ANO()
          * --- maschera la domanda di cancellazione
          PUBLIC i_NOMESSAGE 
 i_NOMESSAGE=.t.
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if this.w_GSAR_ANO.bSec1
            * --- Select from OFF_NOMI
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select NOCODICE  from "+i_cTable+" OFF_NOMI ";
                  +" where NOCODCLI="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" AND NOTIPCLI="+cp_ToStrODBC(this.oParentObject.w_ANTIPCON)+"";
                   ,"_Curs_OFF_NOMI")
            else
              select NOCODICE from (i_cTable);
               where NOCODCLI=this.oParentObject.w_ANCODICE AND NOTIPCLI=this.oParentObject.w_ANTIPCON;
                into cursor _Curs_OFF_NOMI
            endif
            if used('_Curs_OFF_NOMI')
              select _Curs_OFF_NOMI
              locate for 1=1
              do while not(eof())
              this.w_NOMCOD = NVL(_Curs_OFF_NOMI.NOCODICE,"")
              this.w_GSAR_ANO.w_NOCODICE = this.w_NOMCOD
              this.w_GSAR_ANO.QueryKeySet("NOCODICE='"+this.w_NOMCOD+ "'","")     
              * --- carico il record
              this.w_GSAR_ANO.LoadRecWarn()     
              this.w_GSAR_ANO.ecpDelete()     
                select _Curs_OFF_NOMI
                continue
              enddo
              use
            endif
            this.w_GSAR_ANO.ecpQuit()     
            RELEASE i_NOMESSAGE
          else
            Ah_ErrorMsg("Impossibile aprire i nominativi.",48,"")
          endif
        endif
        * --- Eliminato Cliente: toglie il riferimento in Anagrafica Nominativi e setta flag a Da valutare/Nominativo
        this.w_NOTIPNOM = "T"
        this.w_TIPCLIVUOTO = " "
        * --- Select from OFF_NOMI
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select NOCODICE  from "+i_cTable+" OFF_NOMI ";
              +" where NOCODCLI="+cp_ToStrODBC(this.w_TOTCOD)+" AND NOTIPCLI="+cp_ToStrODBC(this.oParentObject.w_ANTIPCON)+"";
               ,"_Curs_OFF_NOMI")
        else
          select NOCODICE from (i_cTable);
           where NOCODCLI=this.w_TOTCOD AND NOTIPCLI=this.oParentObject.w_ANTIPCON;
            into cursor _Curs_OFF_NOMI
        endif
        if used('_Curs_OFF_NOMI')
          select _Curs_OFF_NOMI
          locate for 1=1
          do while not(eof())
          this.w_NOMCOD = NVL(_Curs_OFF_NOMI.NOCODICE,"")
          if NOT EMPTY(this.w_NOMCOD)
            * --- Write into OFF_NOMI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC(this.w_NOTIPNOM),'OFF_NOMI','NOTIPNOM');
              +",NOCODCLI ="+cp_NullLink(cp_ToStrODBC(this.w_CLIVUOTO),'OFF_NOMI','NOCODCLI');
              +",NOTIPCLI ="+cp_NullLink(cp_ToStrODBC(this.w_TIPCLIVUOTO),'OFF_NOMI','NOTIPCLI');
                  +i_ccchkf ;
              +" where ";
                  +"NOCODICE = "+cp_ToStrODBC(this.w_NOMCOD);
                     )
            else
              update (i_cTable) set;
                  NOTIPNOM = this.w_NOTIPNOM;
                  ,NOCODCLI = this.w_CLIVUOTO;
                  ,NOTIPCLI = this.w_TIPCLIVUOTO;
                  &i_ccchkf. ;
               where;
                  NOCODICE = this.w_NOMCOD;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_DELERR = this.w_DELERR+1
          endif
            select _Curs_OFF_NOMI
            continue
          enddo
          use
        endif
        if this.w_DELERR>0 AND this.w_DELNOM
          Ah_ErrorMsg("Non � stato possibile eliminare %1 nominativi.",48,"", alltrim(str(this.w_DELERR)))
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_OFF_NOMI')
      use in _Curs_OFF_NOMI
    endif
    if used('_Curs_OFF_NOMI')
      use in _Curs_OFF_NOMI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
