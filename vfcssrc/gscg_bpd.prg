* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpd                                                        *
*              Controllo data partite                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_22]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-26                                                      *
* Last revis.: 2000-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpd",oParentObject)
return(i_retval)

define class tgscg_bpd as StdBatch
  * --- Local variables
  w_MESS = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo data scadenza.
    * --- Questo batch verifica se la data scadenza impostata � minore della data documento (se presente)
    * --- oppure della data registrazione.
    * --- w_DATAPE contiene la data documento se non � vuota, altrimenti la data registrazione
    if CP_TODATE(this.oParentObject.w_PTDATSCA)<CP_TODATE(this.oParentObject.w_DATAPE)
      * --- Controllo che la data documento non sia vuota
      if EMPTY(CP_TODATE(this.oParentObject.w_PTDATDOC))
        this.w_MESS = "Data scadenza antecedente a data registrazione"
      else
        this.w_MESS = "Attenzione: Data scadenza antecedente a data documento"
      endif
      ah_ErrorMsg(this.w_MESS,"","")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
