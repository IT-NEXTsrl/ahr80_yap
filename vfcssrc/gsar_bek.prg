* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bek                                                        *
*              Esercizi, controlli finali                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2012-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bek",oParentObject)
return(i_retval)

define class tgsar_bek as StdBatch
  * --- Local variables
  w_OK = .f.
  w_RECO = 0
  w_CODESE = space(4)
  w_NCODESE = space(4)
  w_APPO = 0
  w_APPO1 = ctod("  /  /  ")
  w_ESFINESE = ctod("  /  /  ")
  w_MESS = space(10)
  w_TINI = ctod("  /  /  ")
  w_TFIN = ctod("  /  /  ")
  w_INIZ = .f.
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali sull'archivio Esercizi (da GSAR_MES)
    this.w_OK = .T.
    this.w_MESS = " "
    this.w_PADRE = this.oParentObject
    * --- Legge Righe, Temporaneo
    if this.w_PADRE.cFunction<>"Query"
      * --- set deleted ripristinata al valore originario al termine del Batch
      * --- Imposta la Deleted OFF per riattivare i record Cancellati con F6
      ah_Msg("Controlli finali...",.T.)
      SELECT * FROM (this.w_PADRE.cTrsName) WHERE NOT DELETED() INTO CURSOR APPO ORDER BY t_ESINIESE DESC
      if USED("APPO")
        this.w_INIZ = .T.
        SELECT APPO
        GO TOP
        SCAN FOR NOT EMPTY(NVL(t_ESCODESE," "))
        this.w_TFIN = t_ESFINESE
        if this.w_INIZ=.F.
          if this.w_TINI<>this.w_TFIN+1
            this.w_MESS = ah_MsgFormat("Verificare esercizio %1, date di Inizio fine incongruenti con gli esercizi precedente/successivo",t_ESCODESE)
            this.w_OK = .F.
          endif
        endif
        this.w_INIZ = .F.
        this.w_TINI = t_ESINIESE
        ENDSCAN
        SELECT APPO
        USE
      endif
      SELECT (this.w_PADRE.cTrsName)
      if this.w_OK
        OLDSET = SET("DELETED")
        SET DELETED OFF
        SELECT (this.w_PADRE.cTrsName)
        this.w_PADRE.MarkPos()     
        GO TOP
        * --- Cicla sulle Righe non Aggiunte
        SCAN FOR NOT EMPTY(NVL(t_OCODESE," "))
        this.w_CODESE = t_OCODESE
        this.w_NCODESE = NVL(t_ESCODESE, "")
        do case
          case this.w_CODESE<>this.w_NCODESE OR EMPTY(this.w_NCODESE)
            this.w_MESS = ah_MsgFormat("Impossibile rinominare un codice esercizio")
            this.w_OK = .F.
          case DELETED() AND this.w_CODESE=g_CODESE
            this.w_MESS = ah_MsgFormat("Impossibile eliminare l'esercizio corrente")
            this.w_OK = .F.
          case DELETED() OR t_ESINIESE<>t_OINIESE OR t_ESVALNAZ<>t_OVALNAZ
            this.w_APPO = 0
            * --- Select from GSAR0BEK
            do vq_exec with 'GSAR0BEK',this,'_Curs_GSAR0BEK','',.f.,.t.
            if used('_Curs_GSAR0BEK')
              select _Curs_GSAR0BEK
              locate for 1=1
              do while not(eof())
              this.w_APPO = _Curs_GSAR0BEK.CONTA
                select _Curs_GSAR0BEK
                continue
              enddo
              use
            endif
            SELECT (this.w_PADRE.cTrsName)
            if this.w_APPO>0
              if DELETED()
                this.w_MESS = ah_MsgFormat("Esercizio presente in primanota; impossibile eliminare")
              else
                this.w_MESS = ah_MsgFormat("Esercizio presente in primanota; impossibile variare")
              endif
              this.w_OK = .F.
            else
              * --- Select from GSAR1BEK
              do vq_exec with 'GSAR1BEK',this,'_Curs_GSAR1BEK','',.f.,.t.
              if used('_Curs_GSAR1BEK')
                select _Curs_GSAR1BEK
                locate for 1=1
                do while not(eof())
                this.w_APPO = _Curs_GSAR1BEK.CONTA
                  select _Curs_GSAR1BEK
                  continue
                enddo
                use
              endif
              SELECT (this.w_PADRE.cTrsName)
              if this.w_APPO>0
                if DELETED()
                  this.w_MESS = ah_MsgFormat("Esercizio presente nei documenti; impossibile eliminare")
                else
                  this.w_MESS = ah_MsgFormat("Esercizio presente nei documenti; impossibile variare")
                endif
                this.w_OK = .F.
              else
                * --- Select from GSAR2BEK
                do vq_exec with 'GSAR2BEK',this,'_Curs_GSAR2BEK','',.f.,.t.
                if used('_Curs_GSAR2BEK')
                  select _Curs_GSAR2BEK
                  locate for 1=1
                  do while not(eof())
                  this.w_APPO = _Curs_GSAR2BEK.CONTA
                    select _Curs_GSAR2BEK
                    continue
                  enddo
                  use
                endif
                SELECT (this.w_PADRE.cTrsName)
                if this.w_APPO>0
                  if DELETED()
                    this.w_MESS = ah_MsgFormat("Esercizio presente nei movimenti di magazzino; impossibile eliminare")
                  else
                    this.w_MESS = ah_MsgFormat("Esercizio presente nei movimenti di magazzino; impossibile variare")
                  endif
                  this.w_OK = .F.
                endif
              endif
            endif
          case t_ESFINESE<>t_OFINESE 
            * --- Controlla Modifica Data di Fine Esercizio
            this.w_ESFINESE = CP_TODATE(t_ESFINESE)
            this.w_APPO1 = i_INIDAT
            * --- Select from GSAR0BEK
            do vq_exec with 'GSAR0BEK',this,'_Curs_GSAR0BEK','',.f.,.t.
            if used('_Curs_GSAR0BEK')
              select _Curs_GSAR0BEK
              locate for 1=1
              do while not(eof())
              this.w_APPO1 = CP_TODATE(_Curs_gsar0bek.ULTREG)
                select _Curs_GSAR0BEK
                continue
              enddo
              use
            endif
            SELECT (this.w_PADRE.cTrsName)
            if this.w_APPO1>this.w_ESFINESE
              this.w_MESS = ah_MsgFormat("Esistono registrazioni di primanota con data superiore alla data di fine esercizio (%1)%0Impossibile modificare", DTOC(this.w_APPO1)) 
              this.w_OK = .F.
            else
              * --- Select from GSAR1BEK
              do vq_exec with 'GSAR1BEK',this,'_Curs_GSAR1BEK','',.f.,.t.
              if used('_Curs_GSAR1BEK')
                select _Curs_GSAR1BEK
                locate for 1=1
                do while not(eof())
                this.w_APPO1 = CP_TODATE(_Curs_gsar1bek.ULTREG)
                  select _Curs_GSAR1BEK
                  continue
                enddo
                use
              endif
              SELECT (this.w_PADRE.cTrsName)
              if this.w_APPO1>this.w_ESFINESE
                this.w_MESS = ah_MsgFormat("Esistono documenti con data superiore alla data di fine esercizio (%1)%0Impossibile modificare", DTOC(this.w_APPO1)) 
                this.w_OK = .F.
              else
                * --- Select from GSAR2BEK
                do vq_exec with 'GSAR2BEK',this,'_Curs_GSAR2BEK','',.f.,.t.
                if used('_Curs_GSAR2BEK')
                  select _Curs_GSAR2BEK
                  locate for 1=1
                  do while not(eof())
                  this.w_APPO1 = CP_TODATE(_Curs_gsar2bek.ULTREG)
                    select _Curs_GSAR2BEK
                    continue
                  enddo
                  use
                endif
                SELECT (this.w_PADRE.cTrsName)
                if this.w_APPO1>this.w_ESFINESE
                  this.w_MESS = ah_MsgFormat("Esistono movimenti di magazzino con data superiore alla data di fine esercizio (%1)%0Impossibile modificare", DTOC(this.w_APPO1)) 
                  this.w_OK = .F.
                endif
              endif
            endif
        endcase
        if this.w_OK=.F.
          EXIT
        endif
        SELECT (this.w_PADRE.cTrsName)
        ENDSCAN
        this.w_PADRE.RePos()     
        * --- set deleted ripristinata al valore originario al ternýmine del Batch
        SET DELETED &OLDSET
      endif
      SELECT (this.w_PADRE.cTrsName)
      WAIT CLEAR
    endif
    if this.w_OK = .F.
      this.w_PADRE.oParentObject.w_RESCHK = 1
      this.w_PADRE.oParentObject.w_MESS = this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSAR0BEK')
      use in _Curs_GSAR0BEK
    endif
    if used('_Curs_GSAR1BEK')
      use in _Curs_GSAR1BEK
    endif
    if used('_Curs_GSAR2BEK')
      use in _Curs_GSAR2BEK
    endif
    if used('_Curs_GSAR0BEK')
      use in _Curs_GSAR0BEK
    endif
    if used('_Curs_GSAR1BEK')
      use in _Curs_GSAR1BEK
    endif
    if used('_Curs_GSAR2BEK')
      use in _Curs_GSAR2BEK
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
