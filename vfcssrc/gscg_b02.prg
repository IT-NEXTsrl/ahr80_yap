* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_b02                                                        *
*              Elabora S.A. rimanenze                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-12                                                      *
* Last revis.: 2011-05-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_b02",oParentObject)
return(i_retval)

define class tgscg_b02 as StdBatch
  * --- Local variables
  w_TOTIMP = 0
  w_QTAESI = 0
  w_ERRORE = .f.
  w_VALNAZ = space(3)
  w_COSTO = 0
  w_MESS = space(10)
  w_CAOVAL = 0
  w_COD001 = space(15)
  w_CODBUN = space(3)
  w_RECODICE = space(15)
  w_COD002 = space(15)
  w_TIPCON = space(10)
  w_REDESCRI = space(45)
  w_COD003 = space(15)
  w_TIPVAL = 0
  w_IMPDAR = 0
  w_CODART = space(15)
  w_CONRIM = space(15)
  w_IMPAVE = 0
  w_CONRIC = space(15)
  w_APPO = 0
  w_CODBUN = space(3)
  w_CONSUP = space(15)
  w_CCTAGG = space(1)
  w_SEZB = space(1)
  w_FLBUANAL = space(1)
  w_TOTPAR = 0
  w_MRTOTIMP = 0
  w_CODCEN = space(15)
  w_VOCCEN = space(15)
  w_CODCOM = space(15)
  w_DATCIN = space(15)
  w_DATCFI = space(15)
  w_PARAMETRO = 0
  w_IMPTOT = 0
  w_RTOT = 0
  w_NUMRESTO = 0
  w_CONTA = 0
  w_SEGNO = space(1)
  w_DATRIF = ctod("  /  /  ")
  w_CONTOAN = space(1)
  w_NUMBU = 0
  * --- WorkFile variables
  CONTI_idx=0
  MASTRI_idx=0
  BUSIUNIT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Scritture di Assestamento (da GSCG_BAT)
    * --- Viene impostato nelle regole cablate nell'Elaborazione relativi alle Rimanenze di Magazzino COGE
    this.w_ERRORE = .F.
    if USED("EXTRI001")
      * --- Crea il Temporaneo di Appoggio
      CREATE CURSOR TmpAgg1 (CODCON C(15), TOTIMP N(18,4))
      SELECT EXTRI001
      GO TOP
      SCAN FOR NVL(QTAESI,0)<>0 AND NOT EMPTY(NVL(TIPVAL," "))
      this.w_TIPVAL = VAL(TIPVAL)
      this.w_CODBUN = IIF(EMPTY(NVL(CODBUN,"")), g_CODBUN, CODBUN)
      this.w_CONRIM = NVL(CONRIM," ")
      this.w_CONRIC = NVL(CONRIC," ")
      this.w_CODART = NVL(CODART," ")
      this.w_QTAESI = QTAESI
      this.w_COSTO = 0
      this.w_TOTIMP = 0
      * --- Calcola il Costo in Funzione del Criterio
      this.w_TOTIMP = IIF(this.w_TIPVAL=1, NVL(COSMPA,0), this.w_TOTIMP)
      this.w_TOTIMP = IIF(this.w_TIPVAL=2, NVL(COSMPP,0), this.w_TOTIMP)
      this.w_TOTIMP = IIF(this.w_TIPVAL=3, NVL(COSULT,0), this.w_TOTIMP)
      this.w_TOTIMP = IIF(this.w_TIPVAL=4, NVL(COSSTA,0), this.w_TOTIMP)
      this.w_TOTIMP = IIF(this.w_TIPVAL=5, NVL(COSFCO,0), this.w_TOTIMP)
      this.w_TOTIMP = IIF(this.w_TIPVAL=6, NVL(COSLCO,0), this.w_TOTIMP)
      this.w_TOTIMP = IIF(this.w_TIPVAL=7, NVL(COSLSC,0), this.w_TOTIMP)
      this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
      if this.w_TOTIMP<>0
        * --- Riporta i Valori alla Valuta di Conto (si Presume Lira o EURO)
        if this.w_VALNAZ<>g_PERVAL
          this.w_DATRIF = IIF(vartype(this.oparentobject.oparentobject.w_ASDATREG)<>"D", i_DATSYS, this.oparentobject.oparentobject.w_ASDATREG) 
          this.w_CAOVAL = GETCAM(this.w_VALNAZ, this.w_DATRIF)
          this.w_TOTIMP = VAL2MON(this.w_TOTIMP, this.w_CAOVAL, GETCAM(g_PERVAL,this.w_DATRIF), this.w_DATRIF, g_PERVAL, GETVALUT( g_PERVAL , "VADECTOT" ) )
        else
          this.w_TOTIMP = cp_ROUND(this.w_TOTIMP, g_PERPVL)
        endif
        if NOT EMPTY(this.w_CONRIM) AND NOT EMPTY(this.w_CONRIC)
          * --- Scrive il Temporaneo di Appoggio interno al Batch CONRIM (Def.DARE)
          INSERT INTO TmpAgg1 (CODCON, TOTIMP) ;
          VALUES (this.w_CONRIM, this.w_TOTIMP)
          * --- Scrive il Temporaneo di Appoggio interno al Batch CONRIC (Def.AVERE)
          INSERT INTO TmpAgg1 (CODCON, TOTIMP) ;
          VALUES (this.w_CONRIC, -this.w_TOTIMP)
        else
          * --- Test se ci sono registrazioni senza Contropartite
          INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
          VALUES (AH_MsgFormat("Dati Rimanenze") ,"",0,AH_MsgFormat("Cod. art.: %1 non ha la categoria omogenea",this.w_CODART), "")
          this.oParentObject.w_ERROR = .T.
          this.w_ERRORE = .T.
        endif
        SELECT EXTRI001
      endif
      ENDSCAN
      * --- Elimina il Cursore della Query
      SELECT EXTRI001
      USE
      * --- Raggruppa tutto quanto per Codice Conto
      if USED("TmpAgg1")
        if this.w_ERRORE=.T.
          this.w_MESS = "Attenzione:%0alcuni articoli non sono associati a categorie omogenee. %0Proseguo?"
          this.w_ERRORE = NOT AH_YESNO(this.w_MESS)
        endif
        SELECT tmpAgg1
        if this.w_ERRORE=.F. AND RECCOUNT()>0
          SELECT CODCON, SUM(TOTIMP) AS TOTALE FROM TmpAgg1 ;
          INTO CURSOR TmpAgg1 GROUP BY CODCON
          * --- Controllo se il conto specificato gestisce i dati analitici.
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT tmpAgg1
          GO TOP
          SCAN FOR NVL(TOTALE,0)<>0 AND NOT EMPTY(NVL(CODCON," "))
          this.w_TIPCON = "G"
          this.w_COD001 = SPACE(15)
          this.w_COD002 = CODCON
          this.w_COD003 = SPACE(15)
          this.w_IMPDAR = IIF(TOTALE>0, TOTALE, 0)
          this.w_IMPAVE = IIF(TOTALE<0, ABS(TOTALE), 0)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT tmpAgg1
          ENDSCAN
        endif
        SELECT tmpAgg1
        USE
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCONSUP,ANCCTAGG"+;
        " from "+i_cTable+" CONTI where ";
            +"ANCODICE = "+cp_ToStrODBC(this.w_COD002);
            +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCONSUP,ANCCTAGG;
        from (i_cTable) where;
            ANCODICE = this.w_COD002;
            and ANTIPCON = this.w_TIPCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
      this.w_CCTAGG = NVL(cp_ToDate(_read_.ANCCTAGG),cp_NullValue(_read_.ANCCTAGG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo dai Mastri la sezione di bilancio
    if g_APPLICATION = "ADHOC REVOLUTION"
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Read from BUSIUNIT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.BUSIUNIT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BUSIUNIT_idx,2],.t.,this.BUSIUNIT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "BUFLANAL"+;
        " from "+i_cTable+" BUSIUNIT where ";
            +"BUCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and BUCODICE = "+cp_ToStrODBC(this.w_CODBUN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        BUFLANAL;
        from (i_cTable) where;
            BUCODAZI = i_CODAZI;
            and BUCODICE = this.w_CODBUN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLBUANAL = NVL(cp_ToDate(_read_.BUFLANAL),cp_NullValue(_read_.BUFLANAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CONTOAN = "N"
    * --- Effettuo l'aggiornamento dell'analitica solo se la sezione di bilancio �
    *     costi o ricavi se ho selezionato nel conto aggiornamento Automatico e
    *     se la business unit aggiorna l'analitica.
    if (this.w_SEZB $ "CR" OR g_APPLICATION <> "ADHOC REVOLUTION") AND this.w_CCTAGG $ "A" AND (this.w_FLBUANAL="S" OR g_PERBUN="N")
      * --- Devo andare a risalire alla struttura analitica associata al conto
      vq_exec("QUERY\SASRI002", this, "ANALITICA")
      if g_APPLICATION <> "ADHOC REVOLUTION"
        if RECCOUNT ()=0
          select ANALITICA 
 WRCURSOR("ANALITICA") 
 APPEND BLANK
          this.w_CONTOAN = "S"
        endif
        * --- Se sono stati associati conti di analitica relativamente  all BU specifica allora cancella i record relativi alla BU di default
        this.w_NUMBU = 0
        select ANALITICA 
 GO BOTTOM 
 Go TOP 
 COUNT FOR ! EMPTY (NVL (MRCODBUN,"")) TO this.w_NUMBU
        if this.w_NUMBU>0
          select ANALITICA 
 delete FOR EMPTY (NVL (MRCODBUN,"")) 
 Go top
        endif
      endif
      Select ANALITICA 
 GO BOTTOM 
 Go TOP
      if RECCOUNT()>0 AND (NOT( EMPTY (NVL (ANALITICA.MRCODVOC," ")) AND EMPTY ( NVL (ANALITICA.MRCODCDC," "))) OR g_APPLICATION = "ADHOC REVOLUTION")
        SUM ANALITICA.MRPARAME FOR ANALITICA.MRCODVOC<>SPACE(15) AND ANALITICA.MRCODCDC<>SPACE(15) TO L_PARAME
        COUNT ALL FOR ANALITICA.MRCODVOC<>SPACE(15) AND ANALITICA.MRCODCDC<>SPACE(15) TO L_RESTO
        this.w_TOTPAR = L_PARAME
        this.w_NUMRESTO = L_RESTO
        Select ANALITICA
        Go Top
        this.w_RTOT = 0
        this.w_CONTA = 0
        SCAN FOR MRCODVOC<>SPACE(15) AND MRCODCDC<>SPACE(15) OR this.w_CONTOAN="S"
        this.w_CONTA = this.w_CONTA+1
        this.w_SEGNO = IIF(this.w_IMPDAR>this.w_IMPAVE,"D","A")
        this.w_IMPTOT = ABS(this.w_IMPDAR-this.w_IMPAVE)
        this.w_CODCEN = MRCODCDC
        this.w_VOCCEN = MRCODVOC
        this.w_CODCOM = Space(15)
        this.w_DATCIN = cp_CharToDate("  -  -  ")
        this.w_DATCFI = cp_CharToDate("  -  -  ")
        this.w_PARAMETRO = MRPARAME
        this.w_MRTOTIMP = cp_ROUND((this.w_IMPTOT * this.w_PARAMETRO) / this.w_TOTPAR, g_PERPVL)
        * --- Effettuo calcolo del resto per inserire in primanota.
        this.w_RTOT = this.w_RTOT + this.w_MRTOTIMP
        if this.w_CONTA<>this.w_NUMRESTO
          * --- A seconda del segno moltiplico *1 oppure -1.
          this.w_MRTOTIMP = IIF(this.w_SEGNO="D",this.w_MRTOTIMP,-(this.w_MRTOTIMP))
          INSERT INTO TmpAgg (CODICE, CODBUN, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE ,;
          CODCEN, VOCCEN,CODCOM, DATCIN, DATCFI,PARAMETRO,TOTIMP);
          VALUES ("", this.w_CODBUN, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE,;
          this.w_CODCEN,this.w_VOCCEN,this.w_CODCOM,this.w_DATCIN,this.w_DATCFI,this.w_PARAMETRO,this.w_MRTOTIMP)
        else
          if this.w_RTOT<>this.w_IMPTOT
            this.w_MRTOTIMP = this.w_MRTOTIMP + (this.w_IMPTOT - this.w_RTOT)
          endif
          this.w_MRTOTIMP = IIF(this.w_SEGNO="D",this.w_MRTOTIMP,-(this.w_MRTOTIMP))
          INSERT INTO TmpAgg (CODICE, CODBUN, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE ,;
          CODCEN, VOCCEN,CODCOM, DATCIN, DATCFI,PARAMETRO,TOTIMP);
          VALUES ("", this.w_CODBUN, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE,;
          this.w_CODCEN,this.w_VOCCEN,this.w_CODCOM,this.w_DATCIN,this.w_DATCFI,this.w_PARAMETRO,this.w_MRTOTIMP)
          this.w_CONTA = 0
          this.w_RTOT = 0
        endif
        ENDSCAN
      else
        if RECCOUNT()>0 AND EMPTY (NVL (ANALITICA.MRCODVOC," ")) AND EMPTY ( NVL (ANALITICA.MRCODCDC," ")) 
          this.w_CODCEN = SPACE(15)
          this.w_VOCCEN = SPACE(15)
          this.w_CODCOM = Space(15)
          this.w_DATCIN = cp_CharToDate("  -  -  ")
          this.w_DATCFI = cp_CharToDate("  -  -  ")
          this.w_PARAMETRO = 0
          this.w_MRTOTIMP = 0
          INSERT INTO TmpAgg (CODICE, CODBUN, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE ,;
          CODCEN, VOCCEN,CODCOM, DATCIN, DATCFI,PARAMETRO,TOTIMP);
          VALUES ("", this.w_CODBUN, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE,;
          this.w_CODCEN,this.w_VOCCEN,this.w_CODCOM,this.w_DATCIN,this.w_DATCFI,this.w_PARAMETRO,this.w_MRTOTIMP)
        endif
      endif
    else
      this.w_CODCEN = SPACE(15)
      this.w_VOCCEN = SPACE(15)
      this.w_CODCOM = Space(15)
      this.w_DATCIN = cp_CharToDate("  -  -  ")
      this.w_DATCFI = cp_CharToDate("  -  -  ")
      this.w_PARAMETRO = 0
      this.w_MRTOTIMP = 0
      INSERT INTO TmpAgg (CODICE, CODBUN, DESCRI, ;
      TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE ,;
      CODCEN, VOCCEN,CODCOM, DATCIN, DATCFI,PARAMETRO,TOTIMP);
      VALUES ("", this.w_CODBUN, this.w_REDESCRI, ;
      this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE,;
      this.w_CODCEN,this.w_VOCCEN,this.w_CODCOM,this.w_DATCIN,this.w_DATCFI,this.w_PARAMETRO,this.w_MRTOTIMP)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='BUSIUNIT'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
