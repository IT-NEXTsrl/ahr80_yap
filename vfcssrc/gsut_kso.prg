* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kso                                                        *
*              Archiviazione documenti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-18                                                      *
* Last revis.: 2014-11-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kso",oParentObject))

* --- Class definition
define class tgsut_kso as StdForm
  Top    = 10
  Left   = 11

  * --- Standard Properties
  Width  = 789
  Height = 461+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-12"
  HelpContextID=169262231
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=73

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  TIP_ALLE_IDX = 0
  CLA_ALLE_IDX = 0
  PROMCLAS_IDX = 0
  PAR_ALTE_IDX = 0
  KEY_ARTI_IDX = 0
  PRE_ITER_IDX = 0
  OFF_NOMI_IDX = 0
  RAG_TIPI_IDX = 0
  cPrg = "gsut_kso"
  cComment = "Archiviazione documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_LeggePar_Alte = space(5)
  w_PATHSTD = space(250)
  w_PRPATH = space(254)
  w_SENDTO = .F.
  w_ORIGFILE = space(100)
  w_IDSERIAL = space(20)
  w_NUMFILE = 0
  w_LICLADOC = space(15)
  o_LICLADOC = space(15)
  w_CODNOM = space(15)
  w_AECODPRA = space(15)
  o_AECODPRA = space(15)
  w_TIPOALL = space(5)
  o_TIPOALL = space(5)
  w_AETIALCP = space(1)
  o_AETIALCP = space(1)
  w_CLASALL = space(5)
  o_CLASALL = space(5)
  w_AECLALCP = space(1)
  o_AECLALCP = space(1)
  w_CDMODALL = space(1)
  o_CDMODALL = space(1)
  w_AEPATHFI = space(250)
  o_AEPATHFI = space(250)
  w_AEORIFIL = space(100)
  o_AEORIFIL = space(100)
  w_AEOGGETT = space(220)
  o_AEOGGETT = space(220)
  w_AE_TESTO = space(0)
  w_CNDESCAN = space(100)
  w_DESTIPO = space(40)
  w_DESCLAAL = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_SETALL = space(1)
  w_DATAARCH = ctod('  /  /  ')
  w_Crea_Ind_Mod = space(1)
  o_Crea_Ind_Mod = space(1)
  w_CODPRE = space(20)
  o_CODPRE = space(20)
  w_DESCRI = space(40)
  w_RAGPRE = space(10)
  o_RAGPRE = space(10)
  w_IDTIPBST = space(1)
  w_TAPATCLA = space(254)
  w_TAPATTIP = space(254)
  w_CDALIASF = space(15)
  w_CDFLGDES = space(1)
  w_TESTOK = .F.
  w_CDDESCLA = space(50)
  w_CDRIFTAB = space(20)
  w_PATIPARC = space(1)
  w_FLOFFICE = .F.
  w_PATHWORD = space(254)
  w_SELCODCLA = space(15)
  w_CDCLAPRA = space(1)
  w_RCACODART = space(20)
  w_RCADTOBSO = ctod('  /  /  ')
  w_NOEDDES = space(1)
  w_TIPOENTE = space(1)
  w_COPIADESCR = space(1)
  o_COPIADESCR = space(1)
  w_ITDESCRI = space(60)
  w_DESART = space(40)
  w_AR_TABLE = space(10)
  w_DESNOM = space(100)
  w_DIRMOD = space(254)
  w_QUERYMOD = space(254)
  w_PUBWEB = space(1)
  w_P_TIPGEN = space(10)
  w_P_RAGGEN = space(10)
  w_EST_GEN = space(254)
  w_P_TIPFIR = space(10)
  w_P_RAGFIR = space(10)
  w_EST_FIR = space(254)
  w_P_TIPZIP = space(10)
  w_P_RAGZIP = space(10)
  w_EST_ZIP = space(254)
  w_ESTENSIONE = space(10)
  w_ORAARCH = space(8)
  w_TIPOALLE = space(5)
  w_CLAALLE = space(5)
  w_Def_TIP_CP = space(1)
  w_Def_CLA_CP = space(1)
  w_DefaultPath = space(250)
  w_Def_TIP_PATH = space(250)
  w_Def_CLA_PATH = space(250)
  w_CDTIPRAG = space(1)
  w_ZOOMSELE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_kso
  MaxButton = .F.
  AutoCenter = .T.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_ksoPag1","gsut_kso",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Archiviazione documenti")
      .Pages(2).addobject("oPag","tgsut_ksoPag2","gsut_kso",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Attributi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLICLADOC_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMSELE = this.oPgFrm.Pages(2).oPag.ZOOMSELE
    DoDefault()
    proc Destroy()
      this.w_ZOOMSELE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='TIP_ALLE'
    this.cWorkTables[3]='CLA_ALLE'
    this.cWorkTables[4]='PROMCLAS'
    this.cWorkTables[5]='PAR_ALTE'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='PRE_ITER'
    this.cWorkTables[8]='OFF_NOMI'
    this.cWorkTables[9]='RAG_TIPI'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_kso
    *--- Letto XDC, per poter eseguire gli zoom
    cp_ReadXdc()
    This.bUpdated = .T.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LeggePar_Alte=space(5)
      .w_PATHSTD=space(250)
      .w_PRPATH=space(254)
      .w_SENDTO=.f.
      .w_ORIGFILE=space(100)
      .w_IDSERIAL=space(20)
      .w_NUMFILE=0
      .w_LICLADOC=space(15)
      .w_CODNOM=space(15)
      .w_AECODPRA=space(15)
      .w_TIPOALL=space(5)
      .w_AETIALCP=space(1)
      .w_CLASALL=space(5)
      .w_AECLALCP=space(1)
      .w_CDMODALL=space(1)
      .w_AEPATHFI=space(250)
      .w_AEORIFIL=space(100)
      .w_AEOGGETT=space(220)
      .w_AE_TESTO=space(0)
      .w_CNDESCAN=space(100)
      .w_DESTIPO=space(40)
      .w_DESCLAAL=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_SETALL=space(1)
      .w_DATAARCH=ctod("  /  /  ")
      .w_Crea_Ind_Mod=space(1)
      .w_CODPRE=space(20)
      .w_DESCRI=space(40)
      .w_RAGPRE=space(10)
      .w_IDTIPBST=space(1)
      .w_TAPATCLA=space(254)
      .w_TAPATTIP=space(254)
      .w_CDALIASF=space(15)
      .w_CDFLGDES=space(1)
      .w_TESTOK=.f.
      .w_CDDESCLA=space(50)
      .w_CDRIFTAB=space(20)
      .w_PATIPARC=space(1)
      .w_FLOFFICE=.f.
      .w_PATHWORD=space(254)
      .w_SELCODCLA=space(15)
      .w_CDCLAPRA=space(1)
      .w_RCACODART=space(20)
      .w_RCADTOBSO=ctod("  /  /  ")
      .w_NOEDDES=space(1)
      .w_TIPOENTE=space(1)
      .w_COPIADESCR=space(1)
      .w_ITDESCRI=space(60)
      .w_DESART=space(40)
      .w_AR_TABLE=space(10)
      .w_DESNOM=space(100)
      .w_DIRMOD=space(254)
      .w_QUERYMOD=space(254)
      .w_PUBWEB=space(1)
      .w_P_TIPGEN=space(10)
      .w_P_RAGGEN=space(10)
      .w_EST_GEN=space(254)
      .w_P_TIPFIR=space(10)
      .w_P_RAGFIR=space(10)
      .w_EST_FIR=space(254)
      .w_P_TIPZIP=space(10)
      .w_P_RAGZIP=space(10)
      .w_EST_ZIP=space(254)
      .w_ESTENSIONE=space(10)
      .w_ORAARCH=space(8)
      .w_TIPOALLE=space(5)
      .w_CLAALLE=space(5)
      .w_Def_TIP_CP=space(1)
      .w_Def_CLA_CP=space(1)
      .w_DefaultPath=space(250)
      .w_Def_TIP_PATH=space(250)
      .w_Def_CLA_PATH=space(250)
      .w_CDTIPRAG=space(1)
      .w_PATHSTD=oParentObject.w_PATHSTD
      .w_PRPATH=oParentObject.w_PRPATH
      .w_SENDTO=oParentObject.w_SENDTO
      .w_ORIGFILE=oParentObject.w_ORIGFILE
      .w_IDSERIAL=oParentObject.w_IDSERIAL
      .w_NUMFILE=oParentObject.w_NUMFILE
      .w_LICLADOC=oParentObject.w_LICLADOC
      .w_AECODPRA=oParentObject.w_AECODPRA
      .w_TIPOALL=oParentObject.w_TIPOALL
      .w_AETIALCP=oParentObject.w_AETIALCP
      .w_CLASALL=oParentObject.w_CLASALL
      .w_AECLALCP=oParentObject.w_AECLALCP
      .w_CDMODALL=oParentObject.w_CDMODALL
      .w_AEPATHFI=oParentObject.w_AEPATHFI
      .w_AEORIFIL=oParentObject.w_AEORIFIL
      .w_AEOGGETT=oParentObject.w_AEOGGETT
      .w_AE_TESTO=oParentObject.w_AE_TESTO
      .w_CNDESCAN=oParentObject.w_CNDESCAN
      .w_SETALL=oParentObject.w_SETALL
      .w_DATAARCH=oParentObject.w_DATAARCH
      .w_Crea_Ind_Mod=oParentObject.w_Crea_Ind_Mod
      .w_CODPRE=oParentObject.w_CODPRE
      .w_DESCRI=oParentObject.w_DESCRI
      .w_RAGPRE=oParentObject.w_RAGPRE
      .w_IDTIPBST=oParentObject.w_IDTIPBST
      .w_CDALIASF=oParentObject.w_CDALIASF
      .w_CDFLGDES=oParentObject.w_CDFLGDES
      .w_TESTOK=oParentObject.w_TESTOK
      .w_CDRIFTAB=oParentObject.w_CDRIFTAB
      .w_PATIPARC=oParentObject.w_PATIPARC
      .w_FLOFFICE=oParentObject.w_FLOFFICE
      .w_COPIADESCR=oParentObject.w_COPIADESCR
      .w_DESART=oParentObject.w_DESART
      .w_AR_TABLE=oParentObject.w_AR_TABLE
      .w_QUERYMOD=oParentObject.w_QUERYMOD
      .w_PUBWEB=oParentObject.w_PUBWEB
      .w_ESTENSIONE=oParentObject.w_ESTENSIONE
      .w_ORAARCH=oParentObject.w_ORAARCH
      .w_TIPOALLE=oParentObject.w_TIPOALLE
      .w_CLAALLE=oParentObject.w_CLAALLE
      .w_DefaultPath=oParentObject.w_DefaultPath
        .w_LeggePar_Alte = i_CodAzi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_LeggePar_Alte))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,8,.f.)
        if not(empty(.w_LICLADOC))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODNOM))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_AECODPRA))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_TIPOALL))
          .link_1_11('Full')
        endif
        .w_AETIALCP = 'N'
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CLASALL))
          .link_1_13('Full')
        endif
        .w_AECLALCP = 'N'
        .DoRTCalc(15,27,.f.)
        if not(empty(.w_CODPRE))
          .link_1_36('Full')
        endif
        .DoRTCalc(28,29,.f.)
        if not(empty(.w_RAGPRE))
          .link_1_38('Full')
        endif
          .DoRTCalc(30,34,.f.)
        .w_TESTOK = .T.
          .DoRTCalc(36,39,.f.)
        .w_PATHWORD = iif(VARTYPE(this.oParentObject)='O',IIF(VARTYPE(this.oParentObject.w_PathDocumWord)='C',this.oParentObject.w_PathDocumWord,''),'')
        .w_SELCODCLA = .w_LICLADOC
      .oPgFrm.Page2.oPag.ZOOMSELE.Calculate(.w_LICLADOC)
          .DoRTCalc(42,54,.f.)
        .w_P_TIPGEN = 'G'
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_P_TIPGEN))
          .link_1_75('Full')
        endif
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_P_RAGGEN))
          .link_1_76('Full')
        endif
          .DoRTCalc(57,57,.f.)
        .w_P_TIPFIR = 'F'
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_P_TIPFIR))
          .link_1_78('Full')
        endif
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_P_RAGFIR))
          .link_1_79('Full')
        endif
          .DoRTCalc(60,60,.f.)
        .w_P_TIPZIP = 'Z'
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_P_TIPZIP))
          .link_1_81('Full')
        endif
        .DoRTCalc(62,62,.f.)
        if not(empty(.w_P_RAGZIP))
          .link_1_82('Full')
        endif
        .DoRTCalc(63,66,.f.)
        if not(empty(.w_TIPOALLE))
          .link_1_88('Full')
        endif
        .DoRTCalc(67,67,.f.)
        if not(empty(.w_CLAALLE))
          .link_1_89('Full')
        endif
          .DoRTCalc(68,69,.f.)
        .w_DefaultPath = UPPER(ADDBS(IIF(!Empty(.w_PRPATH), .w_PRPATH, .w_PATHSTD))+ADDBS(IIF(.w_Def_TIP_CP='S', IIF(Not Empty(.w_Def_TIP_PATH), .w_Def_TIP_PATH, .w_Def_TIP_CP),''))+ADDBS(IIF(.w_Def_CLA_CP='S', IIF(Not empty(.w_Def_CLA_PATH), .w_Def_CLA_PATH, .w_Def_CLA_CP),'')))
    endwith
    this.DoRTCalc(71,73,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PATHSTD=.w_PATHSTD
      .oParentObject.w_PRPATH=.w_PRPATH
      .oParentObject.w_SENDTO=.w_SENDTO
      .oParentObject.w_ORIGFILE=.w_ORIGFILE
      .oParentObject.w_IDSERIAL=.w_IDSERIAL
      .oParentObject.w_NUMFILE=.w_NUMFILE
      .oParentObject.w_LICLADOC=.w_LICLADOC
      .oParentObject.w_AECODPRA=.w_AECODPRA
      .oParentObject.w_TIPOALL=.w_TIPOALL
      .oParentObject.w_AETIALCP=.w_AETIALCP
      .oParentObject.w_CLASALL=.w_CLASALL
      .oParentObject.w_AECLALCP=.w_AECLALCP
      .oParentObject.w_CDMODALL=.w_CDMODALL
      .oParentObject.w_AEPATHFI=.w_AEPATHFI
      .oParentObject.w_AEORIFIL=.w_AEORIFIL
      .oParentObject.w_AEOGGETT=.w_AEOGGETT
      .oParentObject.w_AE_TESTO=.w_AE_TESTO
      .oParentObject.w_CNDESCAN=.w_CNDESCAN
      .oParentObject.w_SETALL=.w_SETALL
      .oParentObject.w_DATAARCH=.w_DATAARCH
      .oParentObject.w_Crea_Ind_Mod=.w_Crea_Ind_Mod
      .oParentObject.w_CODPRE=.w_CODPRE
      .oParentObject.w_DESCRI=.w_DESCRI
      .oParentObject.w_RAGPRE=.w_RAGPRE
      .oParentObject.w_IDTIPBST=.w_IDTIPBST
      .oParentObject.w_CDALIASF=.w_CDALIASF
      .oParentObject.w_CDFLGDES=.w_CDFLGDES
      .oParentObject.w_TESTOK=.w_TESTOK
      .oParentObject.w_CDRIFTAB=.w_CDRIFTAB
      .oParentObject.w_PATIPARC=.w_PATIPARC
      .oParentObject.w_FLOFFICE=.w_FLOFFICE
      .oParentObject.w_COPIADESCR=.w_COPIADESCR
      .oParentObject.w_DESART=.w_DESART
      .oParentObject.w_AR_TABLE=.w_AR_TABLE
      .oParentObject.w_QUERYMOD=.w_QUERYMOD
      .oParentObject.w_PUBWEB=.w_PUBWEB
      .oParentObject.w_ESTENSIONE=.w_ESTENSIONE
      .oParentObject.w_ORAARCH=.w_ORAARCH
      .oParentObject.w_TIPOALLE=.w_TIPOALLE
      .oParentObject.w_CLAALLE=.w_CLAALLE
      .oParentObject.w_DefaultPath=.w_DefaultPath
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,12,.t.)
        if .o_TIPOALL<>.w_TIPOALL
            .w_CLASALL = .w_CLASALL
          .link_1_13('Full')
        endif
        .DoRTCalc(14,15,.t.)
        if .o_AECODPRA<>.w_AECODPRA.or. .o_TIPOALL<>.w_TIPOALL.or. .o_CLASALL<>.w_CLASALL.or. .o_AETIALCP<>.w_AETIALCP.or. .o_AECLALCP<>.w_AECLALCP.or. .o_LICLADOC<>.w_LICLADOC
            .w_AEPATHFI = UPPER(ADDBS(IIF(!Empty(.w_PRPATH), .w_PRPATH, .w_PATHSTD))+ADDBS(IIF(.w_AETIALCP='S', IIF(Not Empty(.w_TAPATTIP), .w_TAPATTIP, ADDBS(.w_TIPOALL)),''))+ADDBS(IIF(.w_AECLALCP='S', IIF(Not empty(.w_TAPATCLA), .w_TAPATCLA, .w_CLASALL),'')))
        endif
        if .o_AECODPRA<>.w_AECODPRA.or. .o_CDMODALL<>.w_CDMODALL
            .w_AEORIFIL = JUSTFNAME(.w_ORIGFILE)
        endif
        .DoRTCalc(18,26,.t.)
        if .o_RAGPRE<>.w_RAGPRE
            .w_CODPRE = Space(20)
          .link_1_36('Full')
        endif
        .DoRTCalc(28,28,.t.)
        if .o_CODPRE<>.w_CODPRE
            .w_RAGPRE = Space(10)
          .link_1_38('Full')
        endif
        .DoRTCalc(30,34,.t.)
            .w_TESTOK = .T.
        if .o_AEORIFIL<>.w_AEORIFIL
          .Calculate_XQZWFKXMKW()
        endif
        .DoRTCalc(36,40,.t.)
        if .o_LICLADOC<>.w_LICLADOC
            .w_SELCODCLA = .w_LICLADOC
        endif
        if .o_LICLADOC<>.w_LICLADOC
        .oPgFrm.Page2.oPag.ZOOMSELE.Calculate(.w_LICLADOC)
        endif
        .DoRTCalc(42,46,.t.)
        if .o_RAGPRE<>.w_RAGPRE
            .w_COPIADESCR = 'N'
        endif
        .DoRTCalc(48,48,.t.)
        if .o_COPIADESCR<>.w_COPIADESCR.or. .o_CODPRE<>.w_CODPRE.or. .o_RAGPRE<>.w_RAGPRE.or. .o_AEOGGETT<>.w_AEOGGETT
            .w_DESART = IIF(!EMPTY(NVL(.w_RAGPRE,'')), SPACE(40),IIF(.w_COPIADESCR<>'S',.w_DESCRI,LEFT(.w_AEOGGETT,40)))
        endif
        if .o_Crea_Ind_Mod<>.w_Crea_Ind_Mod
          .Calculate_EJTPPLYEQZ()
        endif
        if .o_AEPATHFI<>.w_AEPATHFI.or. .o_AETIALCP<>.w_AETIALCP.or. .o_AECLALCP<>.w_AECLALCP.or. .o_Crea_Ind_Mod<>.w_Crea_Ind_Mod.or. .o_CDMODALL<>.w_CDMODALL
          .Calculate_VTNOFLEYQC()
        endif
        .DoRTCalc(50,54,.t.)
          .link_1_75('Full')
          .link_1_76('Full')
        .DoRTCalc(57,57,.t.)
          .link_1_78('Full')
          .link_1_79('Full')
        .DoRTCalc(60,60,.t.)
          .link_1_81('Full')
          .link_1_82('Full')
        .DoRTCalc(63,65,.t.)
          .link_1_88('Full')
        if .o_TIPOALL<>.w_TIPOALL
          .link_1_89('Full')
        endif
        .DoRTCalc(68,69,.t.)
        if .o_AECODPRA<>.w_AECODPRA
            .w_DefaultPath = UPPER(ADDBS(IIF(!Empty(.w_PRPATH), .w_PRPATH, .w_PATHSTD))+ADDBS(IIF(.w_Def_TIP_CP='S', IIF(Not Empty(.w_Def_TIP_PATH), .w_Def_TIP_PATH, .w_Def_TIP_CP),''))+ADDBS(IIF(.w_Def_CLA_CP='S', IIF(Not empty(.w_Def_CLA_PATH), .w_Def_CLA_PATH, .w_Def_CLA_CP),'')))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(71,73,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZOOMSELE.Calculate(.w_LICLADOC)
    endwith
  return

  proc Calculate_XQZWFKXMKW()
    with this
          * --- Aggiungo l'estensione al file originario se non presente
          .w_AEORIFIL = ForceExt(.w_AEORIFIL, JustExt(.w_ORIGFILE))
    endwith
  endproc
  proc Calculate_HWYWQOBMRM()
    with this
          * --- Completa path Init
          .w_TIPOALL = .w_TIPOALL
          .link_1_11('Full')
          .w_CLASALL = .w_CLASALL
          .link_1_13('Full')
    endwith
  endproc
  proc Calculate_ONYRIARKWV()
    with this
          * --- Valorizza w_AEPATHFI
          .w_AEPATHFI = IIF(.w_Crea_Ind_Mod='I', .w_AEPATHFI, IIF(.w_CDMODALL='L', .w_ORIGFILE, IIF(NOT EMPTY(.w_DIRMOD), alltrim(.w_DIRMOD)+JUSTFNAME(.w_ORIGFILE), space(250))))
    endwith
  endproc
  proc Calculate_EJTPPLYEQZ()
    with this
          * --- Valorizza w_AEOGGETT
          .w_AEOGGETT = IIF(.w_Crea_Ind_Mod='I', .w_AEOGGETT, Left(JUSTSTEM(JUSTFNAME(.w_ORIGFILE)),60))
    endwith
  endproc
  proc Calculate_VTNOFLEYQC()
    with this
          * --- Valorizza nome file in  w_AEPATHFI
          .w_AEPATHFI = IIF(.w_CDMODALL='L', .w_ORIGFILE, IIF(NOT EMPTY(.w_DIRMOD), alltrim(.w_DIRMOD)+JUSTFNAME(.w_ORIGFILE), space(250)))
          .w_AEPATHFI = IIF(.w_Crea_Ind_Mod='I', .w_AEPATHFI, IIF(.w_CDMODALL='L', .w_ORIGFILE, IIF(NOT EMPTY(.w_AEPATHFI) and Empty(JUSTFNAME(.w_AEPATHFI)), ADDBS(alltrim(.w_AEPATHFI))+JUSTFNAME(.w_ORIGFILE),.w_AEPATHFI)))
          .w_AEPATHFI = iif(Isalt() and .w_Crea_Ind_Mod='I',Upper(DCMPATAR(ADDBS(IIF(!Empty(.w_PRPATH), .w_PRPATH, .w_PATHSTD)),.w_CDTIPRAG,i_datsys,.w_LICLADOC,.w_TIPOALL,.w_CLASALL,.w_ORIGFILE,.f.,.w_AETIALCP,.w_AECLALCP,.t.,.f.)),.w_AEPATHFI)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLICLADOC_1_8.enabled = this.oPgFrm.Page1.oPag.oLICLADOC_1_8.mCond()
    this.oPgFrm.Page1.oPag.oAECODPRA_1_10.enabled = this.oPgFrm.Page1.oPag.oAECODPRA_1_10.mCond()
    this.oPgFrm.Page1.oPag.oAETIALCP_1_12.enabled = this.oPgFrm.Page1.oPag.oAETIALCP_1_12.mCond()
    this.oPgFrm.Page1.oPag.oAECLALCP_1_14.enabled = this.oPgFrm.Page1.oPag.oAECLALCP_1_14.mCond()
    this.oPgFrm.Page1.oPag.oAEPATHFI_1_17.enabled = this.oPgFrm.Page1.oPag.oAEPATHFI_1_17.mCond()
    this.oPgFrm.Page1.oPag.oAEORIFIL_1_18.enabled = this.oPgFrm.Page1.oPag.oAEORIFIL_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCODPRE_1_36.enabled = this.oPgFrm.Page1.oPag.oCODPRE_1_36.mCond()
    this.oPgFrm.Page1.oPag.oRAGPRE_1_38.enabled = this.oPgFrm.Page1.oPag.oRAGPRE_1_38.mCond()
    this.oPgFrm.Page1.oPag.oIDTIPBST_1_39.enabled = this.oPgFrm.Page1.oPag.oIDTIPBST_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCOPIADESCR_1_63.enabled = this.oPgFrm.Page1.oPag.oCOPIADESCR_1_63.mCond()
    this.oPgFrm.Page1.oPag.oDESART_1_65.enabled = this.oPgFrm.Page1.oPag.oDESART_1_65.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(Isalt())
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Attributi"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page1.oPag.oCODNOM_1_9.visible=!this.oPgFrm.Page1.oPag.oCODNOM_1_9.mHide()
    this.oPgFrm.Page1.oPag.oAECODPRA_1_10.visible=!this.oPgFrm.Page1.oPag.oAECODPRA_1_10.mHide()
    this.oPgFrm.Page1.oPag.oTIPOALL_1_11.visible=!this.oPgFrm.Page1.oPag.oTIPOALL_1_11.mHide()
    this.oPgFrm.Page1.oPag.oAETIALCP_1_12.visible=!this.oPgFrm.Page1.oPag.oAETIALCP_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCLASALL_1_13.visible=!this.oPgFrm.Page1.oPag.oCLASALL_1_13.mHide()
    this.oPgFrm.Page1.oPag.oAECLALCP_1_14.visible=!this.oPgFrm.Page1.oPag.oAECLALCP_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oCDMODALL_1_16.visible=!this.oPgFrm.Page1.oPag.oCDMODALL_1_16.mHide()
    this.oPgFrm.Page1.oPag.oAEPATHFI_1_17.visible=!this.oPgFrm.Page1.oPag.oAEPATHFI_1_17.mHide()
    this.oPgFrm.Page1.oPag.oAEORIFIL_1_18.visible=!this.oPgFrm.Page1.oPag.oAEORIFIL_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCNDESCAN_1_24.visible=!this.oPgFrm.Page1.oPag.oCNDESCAN_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oDESTIPO_1_29.visible=!this.oPgFrm.Page1.oPag.oDESTIPO_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDESCLAAL_1_31.visible=!this.oPgFrm.Page1.oPag.oDESCLAAL_1_31.mHide()
    this.oPgFrm.Page1.oPag.oSETALL_1_33.visible=!this.oPgFrm.Page1.oPag.oSETALL_1_33.mHide()
    this.oPgFrm.Page1.oPag.oCrea_Ind_Mod_1_35.visible=!this.oPgFrm.Page1.oPag.oCrea_Ind_Mod_1_35.mHide()
    this.oPgFrm.Page1.oPag.oCODPRE_1_36.visible=!this.oPgFrm.Page1.oPag.oCODPRE_1_36.mHide()
    this.oPgFrm.Page1.oPag.oRAGPRE_1_38.visible=!this.oPgFrm.Page1.oPag.oRAGPRE_1_38.mHide()
    this.oPgFrm.Page1.oPag.oIDTIPBST_1_39.visible=!this.oPgFrm.Page1.oPag.oIDTIPBST_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oCOPIADESCR_1_63.visible=!this.oPgFrm.Page1.oPag.oCOPIADESCR_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oDESNOM_1_68.visible=!this.oPgFrm.Page1.oPag.oDESNOM_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut_kso
    * Aggiorniamo le variabili in oParentObject perch�
    * lettura del modello di configurazione prevede la blank 
    * e azzera i valori che sono stati inseriti
    * in questo modo tuteliamo l'utente mantenendoli coerenti
    IF ALLTRIM(UPPER(cEvent))=='W_AECODPRA CHANGED'
       * Codice pratica
       THIS.oParentObject.w_AECODPRA=this.w_AECODPRA
    ENDIF 
    IF ALLTRIM(UPPER(cEvent))=='W_AEOGGETT CHANGED'
       * Descrizione
       THIS.oParentObject.w_AEOGGETT=this.w_AEOGGETT
    ENDIF 
    IF ALLTRIM(UPPER(cEvent))=='W_AE_TESTO CHANGED'
       * Note aggiuntive
       THIS.oParentObject.w_AE_TESTO=this.w_AE_TESTO
    ENDIF 
    IF ALLTRIM(UPPER(cEvent))=='W_CODPRE CHANGED'
       * Codice prestazione
       THIS.oParentObject.w_CODPRE=this.w_CODPRE
    ENDIF 
    IF ALLTRIM(UPPER(cEvent))=='W_RAGPRE CHANGED'
       * Raggruppamento
       THIS.oParentObject.w_RAGPRE=this.w_RAGPRE
    ENDIF 
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_HWYWQOBMRM()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.ZOOMSELE.Event(cEvent)
        if lower(cEvent)==lower("MMMM")
          .Calculate_ONYRIARKWV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_kso
    if cevent='w_LICLADOC Changed'
      this.NotifyEvent('Aggiorna')
    endif
    if cevent='w_CODNOM Changed'
       If Isalt() and This.w_AR_TABLE='OFF_NOMI'
           Select (This.w_ZOOMSELE.cCursor)
           Go top 
           scan
            if Upper(cdcodatt)='CODICE'
              Replace VALORE  with This.w_CODNOM
            Else
             Replace VALORE  with This.w_DESNOM
           Endif
           endscan
           
        endif
      endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LeggePar_Alte
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LeggePar_Alte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LeggePar_Alte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAPATMOD,PAQUERY,PANOEDES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LeggePar_Alte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LeggePar_Alte)
            select PACODAZI,PAPATMOD,PAQUERY,PANOEDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LeggePar_Alte = NVL(_Link_.PACODAZI,space(5))
      this.w_DIRMOD = NVL(_Link_.PAPATMOD,space(254))
      this.w_QUERYMOD = NVL(_Link_.PAQUERY,space(254))
      this.w_NOEDDES = NVL(_Link_.PANOEDES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LeggePar_Alte = space(5)
      endif
      this.w_DIRMOD = space(254)
      this.w_QUERYMOD = space(254)
      this.w_NOEDDES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LeggePar_Alte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LICLADOC
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LICLADOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_LICLADOC)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB,CDCLAPRA,CDPATSTD,CDTIPRAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_LICLADOC))
          select CDCODCLA,CDDESCLA,CDRIFTAB,CDCLAPRA,CDPATSTD,CDTIPRAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LICLADOC)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LICLADOC) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oLICLADOC_1_8'),i_cWhere,'GSUT_MCD',"Classi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB,CDCLAPRA,CDPATSTD,CDTIPRAG";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDRIFTAB,CDCLAPRA,CDPATSTD,CDTIPRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LICLADOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB,CDCLAPRA,CDPATSTD,CDTIPRAG";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_LICLADOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_LICLADOC)
            select CDCODCLA,CDDESCLA,CDRIFTAB,CDCLAPRA,CDPATSTD,CDTIPRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LICLADOC = NVL(_Link_.CDCODCLA,space(15))
      this.w_CDDESCLA = NVL(_Link_.CDDESCLA,space(50))
      this.w_CDRIFTAB = NVL(_Link_.CDRIFTAB,space(20))
      this.w_CDCLAPRA = NVL(_Link_.CDCLAPRA,space(1))
      this.w_AR_TABLE = NVL(_Link_.CDRIFTAB,space(10))
      this.w_PATHSTD = NVL(_Link_.CDPATSTD,space(250))
      this.w_CDTIPRAG = NVL(_Link_.CDTIPRAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LICLADOC = space(15)
      endif
      this.w_CDDESCLA = space(50)
      this.w_CDRIFTAB = space(20)
      this.w_CDCLAPRA = space(1)
      this.w_AR_TABLE = space(10)
      this.w_PATHSTD = space(250)
      this.w_CDTIPRAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!Isalt() or (.w_AR_TABLE='CAN_TIER' or .w_AR_TABLE='OFF_NOMI' )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, classe documentale non gestita!")
        endif
        this.w_LICLADOC = space(15)
        this.w_CDDESCLA = space(50)
        this.w_CDRIFTAB = space(20)
        this.w_CDCLAPRA = space(1)
        this.w_AR_TABLE = space(10)
        this.w_PATHSTD = space(250)
        this.w_CDTIPRAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LICLADOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_1_9'),i_cWhere,'',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_DESNOM = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AECODPRA
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AECODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_AECODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNPATHFA,CNPUBWEB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_AECODPRA))
          select CNCODCAN,CNDESCAN,CNPATHFA,CNPUBWEB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AECODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_AECODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNPATHFA,CNPUBWEB";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_AECODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNPATHFA,CNPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AECODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oAECODPRA_1_10'),i_cWhere,'',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNPATHFA,CNPUBWEB";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNPATHFA,CNPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AECODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNPATHFA,CNPUBWEB";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_AECODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_AECODPRA)
            select CNCODCAN,CNDESCAN,CNPATHFA,CNPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AECODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_CNDESCAN = NVL(_Link_.CNDESCAN,space(100))
      this.w_PRPATH = NVL(_Link_.CNPATHFA,space(254))
      this.w_PUBWEB = NVL(_Link_.CNPUBWEB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AECODPRA = space(15)
      endif
      this.w_CNDESCAN = space(100)
      this.w_PRPATH = space(254)
      this.w_PUBWEB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AECODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOALL
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'TIP_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODICE like "+cp_ToStrODBC(trim(this.w_TIPOALL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI,TAPATTIP,TACOPTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',trim(this.w_TIPOALL))
          select TACODICE,TADESCRI,TAPATTIP,TACOPTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOALL)==trim(_Link_.TACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPOALL) and !this.bDontReportError
            deferred_cp_zoom('TIP_ALLE','*','TACODICE',cp_AbsName(oSource.parent,'oTIPOALL_1_11'),i_cWhere,'GSUT_MTA',"Tipologie allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI,TAPATTIP,TACOPTIP";
                     +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1))
            select TACODICE,TADESCRI,TAPATTIP,TACOPTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI,TAPATTIP,TACOPTIP";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_TIPOALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPOALL)
            select TACODICE,TADESCRI,TAPATTIP,TACOPTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOALL = NVL(_Link_.TACODICE,space(5))
      this.w_DESTIPO = NVL(_Link_.TADESCRI,space(40))
      this.w_TAPATTIP = NVL(_Link_.TAPATTIP,space(254))
      this.w_AETIALCP = NVL(_Link_.TACOPTIP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOALL = space(5)
      endif
      this.w_DESTIPO = space(40)
      this.w_TAPATTIP = space(254)
      this.w_AETIALCP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLASALL
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'CLA_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODCLA like "+cp_ToStrODBC(trim(this.w_CLASALL)+"%");
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPOALL);

          i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE,TACODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',this.w_TIPOALL;
                     ,'TACODCLA',trim(this.w_CLASALL))
          select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE,TACODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASALL)==trim(_Link_.TACODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLASALL) and !this.bDontReportError
            deferred_cp_zoom('CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(oSource.parent,'oCLASALL_1_13'),i_cWhere,'GSUT_MTA',"Classi allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOALL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TACODICE="+cp_ToStrODBC(this.w_TIPOALL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1);
                       ,'TACODCLA',oSource.xKey(2))
            select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_CLASALL);
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPOALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPOALL;
                       ,'TACODCLA',this.w_CLASALL)
            select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASALL = NVL(_Link_.TACODCLA,space(5))
      this.w_DESCLAAL = NVL(_Link_.TACLADES,space(40))
      this.w_TAPATCLA = NVL(_Link_.TAPATCLA,space(254))
      this.w_AECLALCP = NVL(_Link_.TACOPCLA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLASALL = space(5)
      endif
      this.w_DESCLAAL = space(40)
      this.w_TAPATCLA = space(254)
      this.w_AECLALCP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRE
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODPRE))
          select CACODICE,CADESART,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODPRE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODPRE)+"%");

            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODPRE_1_36'),i_cWhere,'',"",'Inspre.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODPRE)
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRE = NVL(_Link_.CACODICE,space(20))
      this.w_DESCRI = NVL(_Link_.CADESART,space(40))
      this.w_RCACODART = NVL(_Link_.CACODART,space(20))
      this.w_RCADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRE = space(20)
      endif
      this.w_DESCRI = space(40)
      this.w_RCACODART = space(20)
      this.w_RCADTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_CODPRE = space(20)
        this.w_DESCRI = space(40)
        this.w_RCACODART = space(20)
        this.w_RCADTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAGPRE
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_lTable = "PRE_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2], .t., this.PRE_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAGPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRE_ITER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ITCODICE like "+cp_ToStrODBC(trim(this.w_RAGPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ITCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ITCODICE',trim(this.w_RAGPRE))
          select ITCODICE,ITDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ITCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RAGPRE)==trim(_Link_.ITCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStrODBC(trim(this.w_RAGPRE)+"%");

            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStr(trim(this.w_RAGPRE)+"%");

            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RAGPRE) and !this.bDontReportError
            deferred_cp_zoom('PRE_ITER','*','ITCODICE',cp_AbsName(oSource.parent,'oRAGPRE_1_38'),i_cWhere,'',"",'GSAG_MIT.PRE_ITER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',oSource.xKey(1))
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAGPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(this.w_RAGPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',this.w_RAGPRE)
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAGPRE = NVL(_Link_.ITCODICE,space(10))
      this.w_ITDESCRI = NVL(_Link_.ITDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_RAGPRE = space(10)
      endif
      this.w_ITDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])+'\'+cp_ToStr(_Link_.ITCODICE,1)
      cp_ShowWarn(i_cKey,this.PRE_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAGPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_TIPGEN
  func Link_1_75(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_TIPGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_TIPGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where RATIPBST="+cp_ToStrODBC(this.w_P_TIPGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST',this.w_P_TIPGEN)
            select RATIPBST,RASERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_TIPGEN = NVL(_Link_.RATIPBST,space(10))
      this.w_P_RAGGEN = NVL(_Link_.RASERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_P_TIPGEN = space(10)
      endif
      this.w_P_RAGGEN = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_TIPGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_RAGGEN
  func Link_1_76(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_RAGGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_RAGGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL,RAESTENS";
                   +" from "+i_cTable+" "+i_lTable+" where RASERIAL="+cp_ToStrODBC(this.w_P_RAGGEN);
                   +" and RATIPBST="+cp_ToStrODBC('G');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST','G';
                       ,'RASERIAL',this.w_P_RAGGEN)
            select RATIPBST,RASERIAL,RAESTENS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_RAGGEN = NVL(_Link_.RASERIAL,space(10))
      this.w_EST_GEN = NVL(_Link_.RAESTENS,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_P_RAGGEN = space(10)
      endif
      this.w_EST_GEN = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)+'\'+cp_ToStr(_Link_.RASERIAL,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_RAGGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_TIPFIR
  func Link_1_78(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_TIPFIR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_TIPFIR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where RATIPBST="+cp_ToStrODBC(this.w_P_TIPFIR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST',this.w_P_TIPFIR)
            select RATIPBST,RASERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_TIPFIR = NVL(_Link_.RATIPBST,space(10))
      this.w_P_RAGFIR = NVL(_Link_.RASERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_P_TIPFIR = space(10)
      endif
      this.w_P_RAGFIR = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_TIPFIR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_RAGFIR
  func Link_1_79(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_RAGFIR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_RAGFIR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL,RAESTENS";
                   +" from "+i_cTable+" "+i_lTable+" where RASERIAL="+cp_ToStrODBC(this.w_P_RAGFIR);
                   +" and RATIPBST="+cp_ToStrODBC('F');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST','F';
                       ,'RASERIAL',this.w_P_RAGFIR)
            select RATIPBST,RASERIAL,RAESTENS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_RAGFIR = NVL(_Link_.RASERIAL,space(10))
      this.w_EST_FIR = NVL(_Link_.RAESTENS,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_P_RAGFIR = space(10)
      endif
      this.w_EST_FIR = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)+'\'+cp_ToStr(_Link_.RASERIAL,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_RAGFIR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_TIPZIP
  func Link_1_81(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_TIPZIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_TIPZIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where RATIPBST="+cp_ToStrODBC(this.w_P_TIPZIP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST',this.w_P_TIPZIP)
            select RATIPBST,RASERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_TIPZIP = NVL(_Link_.RATIPBST,space(10))
      this.w_P_RAGZIP = NVL(_Link_.RASERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_P_TIPZIP = space(10)
      endif
      this.w_P_RAGZIP = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_TIPZIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_RAGZIP
  func Link_1_82(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_RAGZIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_RAGZIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL,RAESTENS";
                   +" from "+i_cTable+" "+i_lTable+" where RASERIAL="+cp_ToStrODBC(this.w_P_RAGZIP);
                   +" and RATIPBST="+cp_ToStrODBC('Z');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST','Z';
                       ,'RASERIAL',this.w_P_RAGZIP)
            select RATIPBST,RASERIAL,RAESTENS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_RAGZIP = NVL(_Link_.RASERIAL,space(10))
      this.w_EST_ZIP = NVL(_Link_.RAESTENS,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_P_RAGZIP = space(10)
      endif
      this.w_EST_ZIP = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)+'\'+cp_ToStr(_Link_.RASERIAL,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_RAGZIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOALLE
  func Link_1_88(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOALLE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOALLE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACOPTIP,TAPATTIP";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_TIPOALLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPOALLE)
            select TACODICE,TACOPTIP,TAPATTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOALLE = NVL(_Link_.TACODICE,space(5))
      this.w_Def_TIP_CP = NVL(_Link_.TACOPTIP,space(1))
      this.w_Def_TIP_PATH = NVL(_Link_.TAPATTIP,space(250))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOALLE = space(5)
      endif
      this.w_Def_TIP_CP = space(1)
      this.w_Def_TIP_PATH = space(250)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOALLE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLAALLE
  func Link_1_89(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLAALLE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLAALLE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACOPCLA,TAPATCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_CLAALLE);
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPOALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPOALL;
                       ,'TACODCLA',this.w_CLAALLE)
            select TACODICE,TACODCLA,TACOPCLA,TAPATCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLAALLE = NVL(_Link_.TACODCLA,space(5))
      this.w_Def_CLA_CP = NVL(_Link_.TACOPCLA,space(1))
      this.w_Def_CLA_PATH = NVL(_Link_.TAPATCLA,space(250))
    else
      if i_cCtrl<>'Load'
        this.w_CLAALLE = space(5)
      endif
      this.w_Def_CLA_CP = space(1)
      this.w_Def_CLA_PATH = space(250)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLAALLE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oORIGFILE_1_5.value==this.w_ORIGFILE)
      this.oPgFrm.Page1.oPag.oORIGFILE_1_5.value=this.w_ORIGFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oLICLADOC_1_8.value==this.w_LICLADOC)
      this.oPgFrm.Page1.oPag.oLICLADOC_1_8.value=this.w_LICLADOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOM_1_9.value==this.w_CODNOM)
      this.oPgFrm.Page1.oPag.oCODNOM_1_9.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oAECODPRA_1_10.value==this.w_AECODPRA)
      this.oPgFrm.Page1.oPag.oAECODPRA_1_10.value=this.w_AECODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOALL_1_11.value==this.w_TIPOALL)
      this.oPgFrm.Page1.oPag.oTIPOALL_1_11.value=this.w_TIPOALL
    endif
    if not(this.oPgFrm.Page1.oPag.oAETIALCP_1_12.RadioValue()==this.w_AETIALCP)
      this.oPgFrm.Page1.oPag.oAETIALCP_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASALL_1_13.value==this.w_CLASALL)
      this.oPgFrm.Page1.oPag.oCLASALL_1_13.value=this.w_CLASALL
    endif
    if not(this.oPgFrm.Page1.oPag.oAECLALCP_1_14.RadioValue()==this.w_AECLALCP)
      this.oPgFrm.Page1.oPag.oAECLALCP_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDMODALL_1_16.RadioValue()==this.w_CDMODALL)
      this.oPgFrm.Page1.oPag.oCDMODALL_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAEPATHFI_1_17.value==this.w_AEPATHFI)
      this.oPgFrm.Page1.oPag.oAEPATHFI_1_17.value=this.w_AEPATHFI
    endif
    if not(this.oPgFrm.Page1.oPag.oAEORIFIL_1_18.value==this.w_AEORIFIL)
      this.oPgFrm.Page1.oPag.oAEORIFIL_1_18.value=this.w_AEORIFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oAEOGGETT_1_19.value==this.w_AEOGGETT)
      this.oPgFrm.Page1.oPag.oAEOGGETT_1_19.value=this.w_AEOGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oAE_TESTO_1_21.value==this.w_AE_TESTO)
      this.oPgFrm.Page1.oPag.oAE_TESTO_1_21.value=this.w_AE_TESTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCNDESCAN_1_24.value==this.w_CNDESCAN)
      this.oPgFrm.Page1.oPag.oCNDESCAN_1_24.value=this.w_CNDESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIPO_1_29.value==this.w_DESTIPO)
      this.oPgFrm.Page1.oPag.oDESTIPO_1_29.value=this.w_DESTIPO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLAAL_1_31.value==this.w_DESCLAAL)
      this.oPgFrm.Page1.oPag.oDESCLAAL_1_31.value=this.w_DESCLAAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSETALL_1_33.RadioValue()==this.w_SETALL)
      this.oPgFrm.Page1.oPag.oSETALL_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAARCH_1_34.value==this.w_DATAARCH)
      this.oPgFrm.Page1.oPag.oDATAARCH_1_34.value=this.w_DATAARCH
    endif
    if not(this.oPgFrm.Page1.oPag.oCrea_Ind_Mod_1_35.RadioValue()==this.w_Crea_Ind_Mod)
      this.oPgFrm.Page1.oPag.oCrea_Ind_Mod_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRE_1_36.value==this.w_CODPRE)
      this.oPgFrm.Page1.oPag.oCODPRE_1_36.value=this.w_CODPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGPRE_1_38.value==this.w_RAGPRE)
      this.oPgFrm.Page1.oPag.oRAGPRE_1_38.value=this.w_RAGPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oIDTIPBST_1_39.RadioValue()==this.w_IDTIPBST)
      this.oPgFrm.Page1.oPag.oIDTIPBST_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDDESCLA_1_47.value==this.w_CDDESCLA)
      this.oPgFrm.Page1.oPag.oCDDESCLA_1_47.value=this.w_CDDESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPIADESCR_1_63.RadioValue()==this.w_COPIADESCR)
      this.oPgFrm.Page1.oPag.oCOPIADESCR_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_65.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_65.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_68.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_68.value=this.w_DESNOM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(!Isalt() or (.w_AR_TABLE='CAN_TIER' or .w_AR_TABLE='OFF_NOMI' ))  and ((!.w_SENDTO OR Isalt()) AND .w_Crea_Ind_Mod='I')  and not(empty(.w_LICLADOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLICLADOC_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, classe documentale non gestita!")
          case   (empty(.w_CODNOM))  and not(Not Isalt() or .w_AR_TABLE<>'OFF_NOMI' OR .w_Crea_Ind_Mod='M')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODNOM_1_9.SetFocus()
            i_bnoObbl = !empty(.w_CODNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AECODPRA))  and not(Not Isalt() or .w_AR_TABLE<>'CAN_TIER' OR .w_Crea_Ind_Mod='M')  and (ALLTRIM(UPPER(.w_CDRIFTAB)) = 'CAN_TIER')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAECODPRA_1_10.SetFocus()
            i_bnoObbl = !empty(.w_AECODPRA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AEPATHFI))  and not(.w_CDMODALL = 'L' AND .w_Crea_Ind_Mod='I')  and ((.w_PATIPARC = 'L'  OR !Isalt()) AND INLIST(.w_CDMODALL,'S','F') or .w_Crea_Ind_Mod='M')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAEPATHFI_1_17.SetFocus()
            i_bnoObbl = !empty(.w_AEPATHFI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATAARCH))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAARCH_1_34.SetFocus()
            i_bnoObbl = !empty(.w_DATAARCH)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys))  and not(!Isalt() or .w_AR_TABLE<>'CAN_TIER')  and (EMPTY(.w_RAGPRE))  and not(empty(.w_CODPRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPRE_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
          case   not(EMPTY(.w_IDTIPBST) OR EMPTY(.w_ESTENSIONE) OR UPPER(ALLTRIM(.w_ESTENSIONE))$UPPER(ALLTRIM(.w_EST_FIR)) OR (.w_IDTIPBST='G' AND UPPER(ALLTRIM(.w_ESTENSIONE))$(UPPER(ALLTRIM(.w_EST_GEN))+UPPER(ALLTRIM(.w_EST_FIR))+UPPER(ALLTRIM(.w_EST_ZIP)))))  and not(!IsAlt())  and (IsAlt() AND UPPER(ALLTRIM(.w_ESTENSIONE))$(UPPER(ALLTRIM(.w_EST_GEN))+UPPER(ALLTRIM(.w_EST_ZIP))+UPPER(ALLTRIM(.w_EST_FIR))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIDTIPBST_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare una tipologia allegato conforme all'estensione del file!")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_kso
      If i_bRes and !Isalt()
          Private w_oArea
          w_oArea = select()
          * passa sul cursore della movimentazione
          Select (This.w_ZOOMSELE.cCursor)
          Go top
          scan
          IF empty(VALORE) and Nvl(CDCHKOBB,' ')='S'
            i_bRes = .F.
            =ah_ErrorMsg("Riga %1 - attributo %2 obbligatorio"+chr(13)+"Occorre indicare un valore per l'attributo",48,'',str(CPROWORD,4,0), alltrim(CDCODATT))
          ENDIF
          endscan
          * Ripristina ambiente
          if not empty(w_oArea)
            select (w_oArea)
          endif
          If ! i_bRes and This.oPgFrm.ActivePage=1
            this.oPgFrm.ActivePage=2
          Endif
      endif
      If i_bRes
          If this.w_Crea_Ind_Mod='I'
            i_bRes = !Empty(this.w_AEOGGETT) OR ah_YesNo("Descrizione mancante, confermi ugualmente?")
          else
            if Empty(this.w_AEOGGETT)
              i_bRes = .F.
              Ah_errormsg("Attenzione, inserire la descrizione del modello.")
            endif
          endif
      Endif
      If i_bRes and this.w_Crea_Ind_Mod='I'
          i_bRes = MAKEDIR(ADDBS(this.w_AEPATHFI))
          If !i_bRes
            Ah_errormsg("Attenzione, path di destinazione non corretto!")
          Endif
      Endif
      If i_bRes and this.w_Crea_Ind_Mod='M'
          * -- Controlla che vi sia un'estensione con 3 o 4 caratteri
          If empty(JUSTFNAME(this.w_AEPATHFI)) OR (LEN(alltrim(this.w_AEPATHFI))- RAT('.',alltrim(this.w_AEPATHFI))<>3 AND LEN(alltrim(this.w_AEPATHFI))- RAT('.',alltrim(this.w_AEPATHFI))<>4 )
            Ah_errormsg("Attenzione, inserire il documento di destinazione con la relativa estensione!")
            i_bRes = .F.
          Endif
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LICLADOC = this.w_LICLADOC
    this.o_AECODPRA = this.w_AECODPRA
    this.o_TIPOALL = this.w_TIPOALL
    this.o_AETIALCP = this.w_AETIALCP
    this.o_CLASALL = this.w_CLASALL
    this.o_AECLALCP = this.w_AECLALCP
    this.o_CDMODALL = this.w_CDMODALL
    this.o_AEPATHFI = this.w_AEPATHFI
    this.o_AEORIFIL = this.w_AEORIFIL
    this.o_AEOGGETT = this.w_AEOGGETT
    this.o_Crea_Ind_Mod = this.w_Crea_Ind_Mod
    this.o_CODPRE = this.w_CODPRE
    this.o_RAGPRE = this.w_RAGPRE
    this.o_COPIADESCR = this.w_COPIADESCR
    return

enddefine

* --- Define pages as container
define class tgsut_ksoPag1 as StdContainer
  Width  = 785
  height = 461
  stdWidth  = 785
  stdheight = 461
  resizeXpos=637
  resizeYpos=246
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oORIGFILE_1_5 as StdField with uid="FNAOQBLGLE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ORIGFILE", cQueryName = "ORIGFILE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Documento corrente",;
    HelpContextID = 130196523,;
   bGlobalFont=.t.,;
    Height=21, Width=579, Left=174, Top=124, InputMask=replicate('X',100)

  add object oLICLADOC_1_8 as StdField with uid="DNZQCCPKJO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_LICLADOC", cQueryName = "LICLADOC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, classe documentale non gestita!",;
    ToolTipText = "Classe documentale",;
    HelpContextID = 227067143,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=173, Top=16, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_LICLADOC"

  func oLICLADOC_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((!.w_SENDTO OR Isalt()) AND .w_Crea_Ind_Mod='I')
    endwith
   endif
  endfunc

  func oLICLADOC_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oLICLADOC_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLICLADOC_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oLICLADOC_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",'',this.parent.oContained
  endproc
  proc oLICLADOC_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_LICLADOC
     i_obj.ecpSave()
  endproc

  add object oCODNOM_1_9 as StdField with uid="HFNVZPOHPZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 61255642,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=174, Top=44, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_1_9.mHide()
    with this.Parent.oContained
      return (Not Isalt() or .w_AR_TABLE<>'OFF_NOMI' OR .w_Crea_Ind_Mod='M')
    endwith
  endfunc

  func oCODNOM_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Nominativi",'',this.parent.oContained
  endproc

  add object oAECODPRA_1_10 as StdField with uid="BWRVDKGJZD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_AECODPRA", cQueryName = "AECODPRA",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice o numero della pratica",;
    HelpContextID = 22399417,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=174, Top=44, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_AECODPRA"

  func oAECODPRA_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (ALLTRIM(UPPER(.w_CDRIFTAB)) = 'CAN_TIER')
    endwith
   endif
  endfunc

  func oAECODPRA_1_10.mHide()
    with this.Parent.oContained
      return (Not Isalt() or .w_AR_TABLE<>'CAN_TIER' OR .w_Crea_Ind_Mod='M')
    endwith
  endfunc

  func oAECODPRA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oAECODPRA_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAECODPRA_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oAECODPRA_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oTIPOALL_1_11 as StdField with uid="XPQGWCLJZD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_TIPOALL", cQueryName = "TIPOALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipologia allegato",;
    HelpContextID = 175835958,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=174, Top=70, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_TIPOALL"

  func oTIPOALL_1_11.mHide()
    with this.Parent.oContained
      return (.w_Crea_Ind_Mod='M')
    endwith
  endfunc

  func oTIPOALL_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
      if .not. empty(.w_CLASALL)
        bRes2=.link_1_13('Full')
      endif
      if .not. empty(.w_CLAALLE)
        bRes2=.link_1_89('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oTIPOALL_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOALL_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_ALLE','*','TACODICE',cp_AbsName(this.parent,'oTIPOALL_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Tipologie allegato",'',this.parent.oContained
  endproc
  proc oTIPOALL_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODICE=this.parent.oContained.w_TIPOALL
     i_obj.ecpSave()
  endproc

  add object oAETIALCP_1_12 as StdCheck with uid="LIZANEXPZV",rtseq=12,rtrep=.f.,left=559, top=69, caption="Completa path file",;
    ToolTipText = "Se attivo completa il path di archiviazione file con il codice tipologia allegato",;
    HelpContextID = 92977578,;
    cFormVar="w_AETIALCP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAETIALCP_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAETIALCP_1_12.GetRadio()
    this.Parent.oContained.w_AETIALCP = this.RadioValue()
    return .t.
  endfunc

  func oAETIALCP_1_12.SetRadio()
    this.Parent.oContained.w_AETIALCP=trim(this.Parent.oContained.w_AETIALCP)
    this.value = ;
      iif(this.Parent.oContained.w_AETIALCP=='S',1,;
      0)
  endfunc

  func oAETIALCP_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_TIPOALL) And .w_CDMODALL<>'L'  and .w_CDCLAPRA#'S')
    endwith
   endif
  endfunc

  func oAETIALCP_1_12.mHide()
    with this.Parent.oContained
      return (.w_Crea_Ind_Mod='M')
    endwith
  endfunc

  add object oCLASALL_1_13 as StdField with uid="QNZTGZLJFT",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CLASALL", cQueryName = "CLASALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice classe allegato",;
    HelpContextID = 176037158,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=174, Top=96, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_TIPOALL", oKey_2_1="TACODCLA", oKey_2_2="this.w_CLASALL"

  func oCLASALL_1_13.mHide()
    with this.Parent.oContained
      return (.w_Crea_Ind_Mod='M')
    endwith
  endfunc

  func oCLASALL_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASALL_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASALL_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLA_ALLE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStrODBC(this.Parent.oContained.w_TIPOALL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStr(this.Parent.oContained.w_TIPOALL)
    endif
    do cp_zoom with 'CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(this.parent,'oCLASALL_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Classi allegato",'',this.parent.oContained
  endproc
  proc oCLASALL_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TACODICE=w_TIPOALL
     i_obj.w_TACODCLA=this.parent.oContained.w_CLASALL
     i_obj.ecpSave()
  endproc

  add object oAECLALCP_1_14 as StdCheck with uid="WWFZVXJJVE",rtseq=14,rtrep=.f.,left=559, top=92, caption="Completa path file",;
    ToolTipText = "Se attivo completa il path di archiviazione file con il codice classe allegato",;
    HelpContextID = 92850602,;
    cFormVar="w_AECLALCP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAECLALCP_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAECLALCP_1_14.GetRadio()
    this.Parent.oContained.w_AECLALCP = this.RadioValue()
    return .t.
  endfunc

  func oAECLALCP_1_14.SetRadio()
    this.Parent.oContained.w_AECLALCP=trim(this.Parent.oContained.w_AECLALCP)
    this.value = ;
      iif(this.Parent.oContained.w_AECLALCP=='S',1,;
      0)
  endfunc

  func oAECLALCP_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CLASALL) And .w_CDMODALL<>'L' and .w_CDCLAPRA#'S')
    endwith
   endif
  endfunc

  func oAECLALCP_1_14.mHide()
    with this.Parent.oContained
      return (.w_Crea_Ind_Mod='M')
    endwith
  endfunc


  add object oCDMODALL_1_16 as StdCombo with uid="EAAXTLDWMH",rtseq=15,rtrep=.f.,left=174,top=152,width=180,height=21;
    , HelpContextID = 262854002;
    , cFormVar="w_CDMODALL",RowSource=""+"Collegamento,"+"Copia file,"+"Sposta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCDMODALL_1_16.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'F',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oCDMODALL_1_16.GetRadio()
    this.Parent.oContained.w_CDMODALL = this.RadioValue()
    return .t.
  endfunc

  func oCDMODALL_1_16.SetRadio()
    this.Parent.oContained.w_CDMODALL=trim(this.Parent.oContained.w_CDMODALL)
    this.value = ;
      iif(this.Parent.oContained.w_CDMODALL=='L',1,;
      iif(this.Parent.oContained.w_CDMODALL=='F',2,;
      iif(this.Parent.oContained.w_CDMODALL=='S',3,;
      0)))
  endfunc

  func oCDMODALL_1_16.mHide()
    with this.Parent.oContained
      return (!.w_SENDTO OR .w_FLOFFICE)
    endwith
  endfunc

  add object oAEPATHFI_1_17 as StdField with uid="HTSMENUMGU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_AEPATHFI", cQueryName = "AEPATHFI",;
    bObbl = .t. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Percorso in cui archiviare il documento",;
    HelpContextID = 127731279,;
   bGlobalFont=.t.,;
    Height=21, Width=579, Left=174, Top=179, InputMask=replicate('X',250), bHasZoom = .t. 

  func oAEPATHFI_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_PATIPARC = 'L'  OR !Isalt()) AND INLIST(.w_CDMODALL,'S','F') or .w_Crea_Ind_Mod='M')
    endwith
   endif
  endfunc

  func oAEPATHFI_1_17.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL = 'L' AND .w_Crea_Ind_Mod='I')
    endwith
  endfunc

  proc oAEPATHFI_1_17.mZoom
    SetDir(this.parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAEORIFIL_1_18 as StdField with uid="XXJDKVHWAW",rtseq=17,rtrep=.f.,;
    cFormVar = "w_AEORIFIL", cQueryName = "AEORIFIL",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome del documento",;
    HelpContextID = 83752530,;
   bGlobalFont=.t.,;
    Height=21, Width=579, Left=174, Top=207, InputMask=replicate('X',100)

  func oAEORIFIL_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Isalt())
    endwith
   endif
  endfunc

  func oAEORIFIL_1_18.mHide()
    with this.Parent.oContained
      return (.w_Crea_Ind_Mod='M' OR .w_CDMODALL = 'L')
    endwith
  endfunc

  add object oAEOGGETT_1_19 as StdField with uid="VXYSMWSFMS",rtseq=18,rtrep=.f.,;
    cFormVar = "w_AEOGGETT", cQueryName = "AEOGGETT",;
    bObbl = .f. , nPag = 1, value=space(220), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto del documento da archiviare",;
    HelpContextID = 204278182,;
   bGlobalFont=.t.,;
    Height=21, Width=579, Left=174, Top=235, InputMask=replicate('X',220)

  add object oAE_TESTO_1_21 as StdMemo with uid="BESTJCOLHR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_AE_TESTO", cQueryName = "AE_TESTO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 29423189,;
   bGlobalFont=.t.,;
    Height=84, Width=579, Left=174, Top=263

  add object oCNDESCAN_1_24 as StdField with uid="YVMXFUCYTB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CNDESCAN", cQueryName = "CNDESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome della pratica",;
    HelpContextID = 225423500,;
   bGlobalFont=.t.,;
    Height=21, Width=437, Left=316, Top=43, InputMask=replicate('X',100)

  func oCNDESCAN_1_24.mHide()
    with this.Parent.oContained
      return (Not Isalt() or .w_AR_TABLE<>'CAN_TIER' OR .w_Crea_Ind_Mod='M')
    endwith
  endfunc


  add object oBtn_1_25 as StdButton with uid="YCTTRQWDEB",left=684, top=415, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per continuare con l'archiviazione";
    , HelpContextID = 44018154;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_LICLADOC))
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="TAPXNQTUEA",left=735, top=415, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 176579654;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESTIPO_1_29 as StdField with uid="IHXQIEVZXT",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESTIPO", cQueryName = "DESTIPO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 16763338,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=240, Top=70, InputMask=replicate('X',40)

  func oDESTIPO_1_29.mHide()
    with this.Parent.oContained
      return (.w_Crea_Ind_Mod='M')
    endwith
  endfunc

  add object oDESCLAAL_1_31 as StdField with uid="GNLVBCYZIJ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCLAAL", cQueryName = "DESCLAAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 266389886,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=240, Top=96, InputMask=replicate('X',40)

  func oDESCLAAL_1_31.mHide()
    with this.Parent.oContained
      return (.w_Crea_Ind_Mod='M')
    endwith
  endfunc

  add object oSETALL_1_33 as StdCheck with uid="AMLJVOFNTR",rtseq=24,rtrep=.f.,left=174, top=356, caption="Applica a tutti i documenti selezionati",;
    ToolTipText = "Se attivo applica le impostazioni a tutti i file selezionati",;
    HelpContextID = 81967322,;
    cFormVar="w_SETALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSETALL_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSETALL_1_33.GetRadio()
    this.Parent.oContained.w_SETALL = this.RadioValue()
    return .t.
  endfunc

  func oSETALL_1_33.SetRadio()
    this.Parent.oContained.w_SETALL=trim(this.Parent.oContained.w_SETALL)
    this.value = ;
      iif(this.Parent.oContained.w_SETALL=='S',1,;
      0)
  endfunc

  func oSETALL_1_33.mHide()
    with this.Parent.oContained
      return (.w_NUMFILE=1)
    endwith
  endfunc

  add object oDATAARCH_1_34 as StdField with uid="RRYPBNVJXT",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DATAARCH", cQueryName = "DATAARCH",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 261275010,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=524, Top=353


  add object oCrea_Ind_Mod_1_35 as StdCombo with uid="IPMFWVWZWS",rtseq=26,rtrep=.f.,left=632,top=353,width=121,height=22;
    , HelpContextID = 103168390;
    , cFormVar="w_Crea_Ind_Mod",RowSource=""+"Crea indice,"+"Crea modello", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCrea_Ind_Mod_1_35.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oCrea_Ind_Mod_1_35.GetRadio()
    this.Parent.oContained.w_Crea_Ind_Mod = this.RadioValue()
    return .t.
  endfunc

  func oCrea_Ind_Mod_1_35.SetRadio()
    this.Parent.oContained.w_Crea_Ind_Mod=trim(this.Parent.oContained.w_Crea_Ind_Mod)
    this.value = ;
      iif(this.Parent.oContained.w_Crea_Ind_Mod=='I',1,;
      iif(this.Parent.oContained.w_Crea_Ind_Mod=='M',2,;
      0))
  endfunc

  func oCrea_Ind_Mod_1_35.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oCODPRE_1_36 as StdField with uid="OXBDHUIRND",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CODPRE", cQueryName = "CODPRE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
    ToolTipText = "Inserire il codice della prestazione che verr� inclusa nell'inserimento provvisorio delle prestazioni in fase di creazione di un documento",;
    HelpContextID = 192196570,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=173, Top=384, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODPRE"

  func oCODPRE_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_RAGPRE))
    endwith
   endif
  endfunc

  func oCODPRE_1_36.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_AR_TABLE<>'CAN_TIER')
    endwith
  endfunc

  func oCODPRE_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRE_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRE_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODPRE_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'Inspre.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oRAGPRE_1_38 as StdField with uid="JRJWLAHHKJ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_RAGPRE", cQueryName = "RAGPRE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il raggruppamento di prestazioni che verranno incluse nell'inserimento provvisorio delle prestazioni in fase di creazione di un documento",;
    HelpContextID = 192187626,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=670, Top=384, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRE_ITER", oKey_1_1="ITCODICE", oKey_1_2="this.w_RAGPRE"

  func oRAGPRE_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODPRE))
    endwith
   endif
  endfunc

  func oRAGPRE_1_38.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_AR_TABLE<>'CAN_TIER')
    endwith
  endfunc

  func oRAGPRE_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oRAGPRE_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRAGPRE_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRE_ITER','*','ITCODICE',cp_AbsName(this.parent,'oRAGPRE_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSAG_MIT.PRE_ITER_VZM',this.parent.oContained
  endproc


  add object oIDTIPBST_1_39 as StdCombo with uid="JSEOGHXOGM",value=11,rtseq=30,rtrep=.f.,left=173,top=411,width=153,height=22;
    , ToolTipText = "Tipologia allegato per busta";
    , HelpContextID = 245021222;
    , cFormVar="w_IDTIPBST",RowSource=""+"Atto principale,"+"Dati atto,"+"Allegato semplice,"+"Nota iscrizione,"+"Procura alle liti,"+"Ricevuta telematica,"+"Rel. notificazione,"+"Scans. atto notificato,"+"Msg PEC tra avvocati,"+"Ricev.avv.consegna,"+"", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Selezionare una tipologia allegato conforme all'estensione del file!";
  , bGlobalFont=.t.


  func oIDTIPBST_1_39.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'G',;
    iif(this.value =4,'N',;
    iif(this.value =5,'L',;
    iif(this.value =6,'T',;
    iif(this.value =7,'X',;
    iif(this.value =8,'S',;
    iif(this.value =9,'A',;
    iif(this.value =10,'R',;
    iif(this.value =11,' ',;
    space(1)))))))))))))
  endfunc
  func oIDTIPBST_1_39.GetRadio()
    this.Parent.oContained.w_IDTIPBST = this.RadioValue()
    return .t.
  endfunc

  func oIDTIPBST_1_39.SetRadio()
    this.Parent.oContained.w_IDTIPBST=trim(this.Parent.oContained.w_IDTIPBST)
    this.value = ;
      iif(this.Parent.oContained.w_IDTIPBST=='P',1,;
      iif(this.Parent.oContained.w_IDTIPBST=='D',2,;
      iif(this.Parent.oContained.w_IDTIPBST=='G',3,;
      iif(this.Parent.oContained.w_IDTIPBST=='N',4,;
      iif(this.Parent.oContained.w_IDTIPBST=='L',5,;
      iif(this.Parent.oContained.w_IDTIPBST=='T',6,;
      iif(this.Parent.oContained.w_IDTIPBST=='X',7,;
      iif(this.Parent.oContained.w_IDTIPBST=='S',8,;
      iif(this.Parent.oContained.w_IDTIPBST=='A',9,;
      iif(this.Parent.oContained.w_IDTIPBST=='R',10,;
      iif(this.Parent.oContained.w_IDTIPBST=='',11,;
      0)))))))))))
  endfunc

  func oIDTIPBST_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt() AND UPPER(ALLTRIM(.w_ESTENSIONE))$(UPPER(ALLTRIM(.w_EST_GEN))+UPPER(ALLTRIM(.w_EST_ZIP))+UPPER(ALLTRIM(.w_EST_FIR))))
    endwith
   endif
  endfunc

  func oIDTIPBST_1_39.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  func oIDTIPBST_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_IDTIPBST) OR EMPTY(.w_ESTENSIONE) OR UPPER(ALLTRIM(.w_ESTENSIONE))$UPPER(ALLTRIM(.w_EST_FIR)) OR (.w_IDTIPBST='G' AND UPPER(ALLTRIM(.w_ESTENSIONE))$(UPPER(ALLTRIM(.w_EST_GEN))+UPPER(ALLTRIM(.w_EST_FIR))+UPPER(ALLTRIM(.w_EST_ZIP)))))
    endwith
    return bRes
  endfunc

  add object oCDDESCLA_1_47 as StdField with uid="RVOLOQWTAL",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CDDESCLA", cQueryName = "CDDESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 43009383,;
   bGlobalFont=.t.,;
    Height=21, Width=460, Left=293, Top=16, InputMask=replicate('X',50)

  add object oCOPIADESCR_1_63 as StdCheck with uid="SBGNTFWMPY",rtseq=47,rtrep=.f.,left=328, top=411, caption="Copia descrizione documento nella descrizione prestazione",;
    ToolTipText = "Se attivo, copia descrizione documento nella descrizione prestazione",;
    HelpContextID = 41248425,;
    cFormVar="w_COPIADESCR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOPIADESCR_1_63.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCOPIADESCR_1_63.GetRadio()
    this.Parent.oContained.w_COPIADESCR = this.RadioValue()
    return .t.
  endfunc

  func oCOPIADESCR_1_63.SetRadio()
    this.Parent.oContained.w_COPIADESCR=trim(this.Parent.oContained.w_COPIADESCR)
    this.value = ;
      iif(this.Parent.oContained.w_COPIADESCR=='S',1,;
      0)
  endfunc

  func oCOPIADESCR_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_RAGPRE))
    endwith
   endif
  endfunc

  func oCOPIADESCR_1_63.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_AR_TABLE<>'CAN_TIER')
    endwith
  endfunc

  add object oDESART_1_65 as StdField with uid="BCPEUDQLIW",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209897930,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=328, Top=384, InputMask=replicate('X',40)

  func oDESART_1_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_NOEDDES='N' or cp_IsAdministrator())  AND EMPTY(.w_RAGPRE))
    endwith
   endif
  endfunc

  add object oDESNOM_1_68 as StdField with uid="VKOJOYYLGQ",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 61196746,;
   bGlobalFont=.t.,;
    Height=21, Width=437, Left=316, Top=43, InputMask=replicate('X',100)

  func oDESNOM_1_68.mHide()
    with this.Parent.oContained
      return (Not Isalt() or .w_AR_TABLE<>'OFF_NOMI' OR .w_Crea_Ind_Mod='M')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="CSJIMHTSCS",Visible=.t., Left=3, Top=209,;
    Alignment=1, Width=167, Height=18,;
    Caption="Nome file archiviato:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_PATIPARC<>'L' Or .w_CDMODALL='L' OR .w_Crea_Ind_Mod='M')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="QRPVYMOIMG",Visible=.t., Left=3, Top=237,;
    Alignment=1, Width=167, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="SSVUXTHWUK",Visible=.t., Left=3, Top=263,;
    Alignment=1, Width=167, Height=18,;
    Caption="Note aggiuntive:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="WQJDZQKOLP",Visible=.t., Left=95, Top=47,;
    Alignment=1, Width=75, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (Not Isalt() or .w_AR_TABLE<>'CAN_TIER' OR .w_Crea_Ind_Mod='M')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="TLNDREKFKI",Visible=.t., Left=3, Top=180,;
    Alignment=1, Width=167, Height=18,;
    Caption="Percorso archiviazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL='L' AND .w_Crea_Ind_Mod='I')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="AFGTZIPGXT",Visible=.t., Left=82, Top=71,;
    Alignment=1, Width=88, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_Crea_Ind_Mod='M')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="OPLAJOUHVU",Visible=.t., Left=94, Top=98,;
    Alignment=1, Width=76, Height=18,;
    Caption="Classe:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_Crea_Ind_Mod='M')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="MNKQAARXDT",Visible=.t., Left=3, Top=124,;
    Alignment=1, Width=167, Height=18,;
    Caption="Documento da archiviare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="DYMPOKHJPP",Visible=.t., Left=47, Top=152,;
    Alignment=1, Width=123, Height=18,;
    Caption="Modalit� archiviazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (!.w_SENDTO OR .w_FLOFFICE)
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="NHSJMFNYBZ",Visible=.t., Left=55, Top=20,;
    Alignment=1, Width=118, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="GNMEHDYDVO",Visible=.t., Left=3, Top=209,;
    Alignment=1, Width=167, Height=18,;
    Caption="Nome file originario:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_PATIPARC='L' Or .w_CDMODALL='L' OR .w_Crea_Ind_Mod='M')
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="TEOZQSYYOP",Visible=.t., Left=34, Top=384,;
    Alignment=1, Width=136, Height=18,;
    Caption="Cod. prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_AR_TABLE<>'CAN_TIER')
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="REQNZKKGZK",Visible=.t., Left=565, Top=384,;
    Alignment=1, Width=100, Height=18,;
    Caption="Raggruppamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_AR_TABLE<>'CAN_TIER')
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="OTZEAIYEAC",Visible=.t., Left=82, Top=47,;
    Alignment=1, Width=88, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (Not Isalt() or .w_AR_TABLE<>'OFF_NOMI' OR .w_Crea_Ind_Mod='M')
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="PZZCQGNBBR",Visible=.t., Left=23, Top=411,;
    Alignment=1, Width=147, Height=18,;
    Caption="Tipologia per busta:"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_86 as StdString with uid="UMWENJVOGC",Visible=.t., Left=415, Top=356,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data archiviazione:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsut_ksoPag2 as StdContainer
  Width  = 785
  height = 461
  stdWidth  = 785
  stdheight = 461
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMSELE as cp_zoombox with uid="XILACTARAN",left=12, top=12, width=746,height=375,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSUT0KFG",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="PRODCLAS",cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnDblClick=.f.,bNoZoomGridShape=.f.,;
    cEvent = "Aggiorna,Blank",;
    nPag=2;
    , HelpContextID = 189611034
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kso','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kso
Proc SetDir(pParent)
  local l_oldDir
  l_oldDir = pParent.w_AEPATHFI
  pParent.w_AEPATHFI = cp_getdir()
  If Empty(pParent.w_AEPATHFI)
    pParent.w_AEPATHFI = l_oldDir
  Endif
EndProc
* --- Fine Area Manuale
