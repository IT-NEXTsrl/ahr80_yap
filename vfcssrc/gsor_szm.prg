* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_szm                                                        *
*              Visualizzazione ordini                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_142]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-20                                                      *
* Last revis.: 2014-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsor_szm",oParentObject))

* --- Class definition
define class tgsor_szm as StdForm
  Top    = 0
  Left   = 3

  * --- Standard Properties
  Width  = 784
  Height = 473+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-14"
  HelpContextID=26646167
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=47

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  TIP_DOCU_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  CAN_TIER_IDX = 0
  AGENTI_IDX = 0
  ATTIVITA_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gsor_szm"
  cComment = "Visualizzazione ordini"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLVEAC1 = space(1)
  w_FLVEAC = space(10)
  w_CODAZI = space(5)
  w_AZGESCOM = space(1)
  w_CATDOC = space(2)
  w_FILDOC = space(2)
  w_TIPDOC = space(5)
  w_DESDOC = space(35)
  w_TCATDOC = space(2)
  w_TFLVEAC = space(1)
  w_CODART = space(20)
  o_CODART = space(20)
  w_DESART = space(40)
  w_CODMAG = space(5)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_SERIALE = space(10)
  w_TOTCAR = 0
  w_DESCAU = space(35)
  w_TOTSCA = 0
  w_NUMROW = 0
  w_NUMRIF = 0
  w_SERIALE = space(10)
  w_PARAME = space(3)
  w_DESAPP = space(30)
  w_CLIFOR = space(40)
  w_UNIMIS = space(3)
  w_TIPCLF = space(1)
  o_TIPCLF = space(1)
  w_UNIMIS = space(3)
  w_CLISEL = space(15)
  w_DESCLI = space(40)
  w_CAUSEL = space(5)
  w_DESCAU2 = space(35)
  w_AGESEL = space(5)
  w_DESAGE = space(35)
  w_TIPATT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_COMMESSA = space(15)
  w_DESCOM = space(30)
  w_MVRIFCLI = space(30)
  w_FLVEAC2 = space(2)
  w_DESATT = space(35)
  w_CODATT = space(15)
  w_TIPEVA = space(1)
  w_VISIBLE = .F.
  w_ZoomDoc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsor_szmPag1","gsor_szm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsor_szmPag2","gsor_szm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLVEAC1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDoc = this.oPgFrm.Pages(1).oPag.ZoomDoc
    DoDefault()
    proc Destroy()
      this.w_ZoomDoc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='ATTIVITA'
    this.cWorkTables[9]='AZIENDA'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLVEAC1=space(1)
      .w_FLVEAC=space(10)
      .w_CODAZI=space(5)
      .w_AZGESCOM=space(1)
      .w_CATDOC=space(2)
      .w_FILDOC=space(2)
      .w_TIPDOC=space(5)
      .w_DESDOC=space(35)
      .w_TCATDOC=space(2)
      .w_TFLVEAC=space(1)
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_CODMAG=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_SERIALE=space(10)
      .w_TOTCAR=0
      .w_DESCAU=space(35)
      .w_TOTSCA=0
      .w_NUMROW=0
      .w_NUMRIF=0
      .w_SERIALE=space(10)
      .w_PARAME=space(3)
      .w_DESAPP=space(30)
      .w_CLIFOR=space(40)
      .w_UNIMIS=space(3)
      .w_TIPCLF=space(1)
      .w_UNIMIS=space(3)
      .w_CLISEL=space(15)
      .w_DESCLI=space(40)
      .w_CAUSEL=space(5)
      .w_DESCAU2=space(35)
      .w_AGESEL=space(5)
      .w_DESAGE=space(35)
      .w_TIPATT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_COMMESSA=space(15)
      .w_DESCOM=space(30)
      .w_MVRIFCLI=space(30)
      .w_FLVEAC2=space(2)
      .w_DESATT=space(35)
      .w_CODATT=space(15)
      .w_TIPEVA=space(1)
      .w_VISIBLE=.f.
        .w_FLVEAC1 = 'V'
        .w_FLVEAC = IIF(.w_FLVEAC1='E', ' ', .w_FLVEAC1)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI))
          .link_1_4('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_CATDOC = 'OR'
        .w_FILDOC = IIF(.w_CATDOC='XX', SPACE(2), .w_CATDOC)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_TIPDOC))
          .link_1_8('Full')
        endif
        .DoRTCalc(8,11,.f.)
        if not(empty(.w_CODART))
          .link_1_12('Full')
        endif
          .DoRTCalc(12,12,.f.)
        .w_CODMAG = g_MAGAZI
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODMAG))
          .link_1_14('Full')
        endif
          .DoRTCalc(14,15,.f.)
        .w_DOCINI = g_INIESE
        .w_DOCFIN = g_FINESE
      .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .w_SERIALE = .w_ZoomDoc.getVar('SERIAL')
        .w_TOTCAR = 0
        .w_DESCAU = .w_ZoomDoc.getVar('DESCAU')
        .w_TOTSCA = 0
        .w_NUMROW = .w_ZoomDoc.getVar('CPROWNUM')
        .w_NUMRIF = .w_ZoomDoc.getVar('NUMRIF')
        .w_SERIALE = .w_ZoomDoc.getVar('SERIAL')
        .w_PARAME = nvl(.w_ZoomDoc.getVar('FLVEAC'),"")+nvl(.w_ZoomDoc.getVar('CLADOC'),"")
          .DoRTCalc(26,26,.f.)
        .w_CLIFOR = .w_ZoomDoc.getVar('DESCON')
          .DoRTCalc(28,28,.f.)
        .w_TIPCLF = IIF(.w_FLVEAC1='V', 'C', IIF(.w_FLVEAC1='A', 'F', ' '))
          .DoRTCalc(30,30,.f.)
        .w_CLISEL = SPACE(15)
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_CLISEL))
          .link_2_1('Full')
        endif
        .DoRTCalc(32,33,.f.)
        if not(empty(.w_CAUSEL))
          .link_2_3('Full')
        endif
        .DoRTCalc(34,35,.f.)
        if not(empty(.w_AGESEL))
          .link_2_5('Full')
        endif
          .DoRTCalc(36,36,.f.)
        .w_TIPATT = 'A'
        .w_OBTEST = i_INIDAT
        .DoRTCalc(39,40,.f.)
        if not(empty(.w_COMMESSA))
          .link_2_14('Full')
        endif
          .DoRTCalc(41,42,.f.)
        .w_FLVEAC2 = IIF(.w_FLVEAC1='E', SPACE(1), .w_FLVEAC1)
      .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .DoRTCalc(44,45,.f.)
        if not(empty(.w_CODATT))
          .link_2_20('Full')
        endif
        .w_TIPEVA = 'T'
        .w_VISIBLE = .T.
      .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_FLVEAC = IIF(.w_FLVEAC1='E', ' ', .w_FLVEAC1)
          .link_1_4('Full')
        .DoRTCalc(4,4,.t.)
            .w_CATDOC = 'OR'
            .w_FILDOC = IIF(.w_CATDOC='XX', SPACE(2), .w_CATDOC)
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .DoRTCalc(7,17,.t.)
            .w_SERIALE = .w_ZoomDoc.getVar('SERIAL')
        .DoRTCalc(19,19,.t.)
            .w_DESCAU = .w_ZoomDoc.getVar('DESCAU')
        .DoRTCalc(21,21,.t.)
            .w_NUMROW = .w_ZoomDoc.getVar('CPROWNUM')
            .w_NUMRIF = .w_ZoomDoc.getVar('NUMRIF')
            .w_SERIALE = .w_ZoomDoc.getVar('SERIAL')
            .w_PARAME = nvl(.w_ZoomDoc.getVar('FLVEAC'),"")+nvl(.w_ZoomDoc.getVar('CLADOC'),"")
        .DoRTCalc(26,26,.t.)
            .w_CLIFOR = .w_ZoomDoc.getVar('DESCON')
        .DoRTCalc(28,28,.t.)
            .w_TIPCLF = IIF(.w_FLVEAC1='V', 'C', IIF(.w_FLVEAC1='A', 'F', ' '))
        .DoRTCalc(30,30,.t.)
        if .o_TIPCLF<>.w_TIPCLF
            .w_CLISEL = SPACE(15)
          .link_2_1('Full')
        endif
        .DoRTCalc(32,42,.t.)
            .w_FLVEAC2 = IIF(.w_FLVEAC1='E', SPACE(1), .w_FLVEAC1)
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(44,47,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
    endwith
  return

  proc Calculate_VNBIIUKZXF()
    with this
          * --- Carico temporaneo
          gsor_bvm(this;
              ,"Init";
             )
    endwith
  endproc
  proc Calculate_MHCQEURGDG()
    with this
          * --- Done
          gsor_bvm(this;
              ,"Done";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCLISEL_2_1.enabled = this.oPgFrm.Page2.oPag.oCLISEL_2_1.mCond()
    this.oPgFrm.Page2.oPag.oCOMMESSA_2_14.enabled = this.oPgFrm.Page2.oPag.oCOMMESSA_2_14.mCond()
    this.oPgFrm.Page2.oPag.oCODATT_2_20.enabled = this.oPgFrm.Page2.oPag.oCODATT_2_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTOTCAR_1_24.visible=!this.oPgFrm.Page1.oPag.oTOTCAR_1_24.mHide()
    this.oPgFrm.Page1.oPag.oTOTSCA_1_27.visible=!this.oPgFrm.Page1.oPag.oTOTSCA_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oUNIMIS_1_41.visible=!this.oPgFrm.Page1.oPag.oUNIMIS_1_41.mHide()
    this.oPgFrm.Page1.oPag.oUNIMIS_1_45.visible=!this.oPgFrm.Page1.oPag.oUNIMIS_1_45.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_13.visible=!this.oPgFrm.Page2.oPag.oStr_2_13.mHide()
    this.oPgFrm.Page2.oPag.oCOMMESSA_2_14.visible=!this.oPgFrm.Page2.oPag.oCOMMESSA_2_14.mHide()
    this.oPgFrm.Page2.oPag.oDESCOM_2_15.visible=!this.oPgFrm.Page2.oPag.oDESCOM_2_15.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_18.visible=!this.oPgFrm.Page2.oPag.oStr_2_18.mHide()
    this.oPgFrm.Page2.oPag.oDESATT_2_19.visible=!this.oPgFrm.Page2.oPag.oDESATT_2_19.mHide()
    this.oPgFrm.Page2.oPag.oCODATT_2_20.visible=!this.oPgFrm.Page2.oPag.oCODATT_2_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("FormLoad")
          .Calculate_VNBIIUKZXF()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZoomDoc.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_MHCQEURGDG()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZGESCOM";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZGESCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZGESCOM = NVL(_Link_.AZGESCOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZGESCOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPDOC
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOR_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_1_8'),i_cWhere,'GSOR_ATD',"Causali ordini",'GSOR1SDV.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_TCATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_TFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_TCATDOC = space(2)
      this.w_TFLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TFLVEAC=.w_FLVEAC1 OR .w_FLVEAC1='E') AND .w_TCATDOC='OR'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_TIPDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_TCATDOC = space(2)
        this.w_TFLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_12'),i_cWhere,'GSMA_BZA',"Codici articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_UNIMIS = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_UNIMIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_UNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_14'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESAPP = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESAPP = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_CODMAG = space(5)
        this.w_DESAPP = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLISEL
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLISEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLF;
                     ,'ANCODICE',trim(this.w_CLISEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLISEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLISEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLISEL_2_1'),i_cWhere,'',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLISEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLISEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLF;
                       ,'ANCODICE',this.w_CLISEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLISEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLISEL = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        endif
        this.w_CLISEL = space(15)
        this.w_DESCLI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLISEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSEL
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CAUSEL)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CAUSEL))
          select CMCODICE,CMDESCRI,CMDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUSEL)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_CAUSEL)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_CAUSEL)+"%");

            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUSEL) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCAUSEL_2_3'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUSEL)
            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSEL = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAU2 = NVL(_Link_.CMDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSEL = space(5)
      endif
      this.w_DESCAU2 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
        endif
        this.w_CAUSEL = space(5)
        this.w_DESCAU2 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGESEL
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGESEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGESEL)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGESEL))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGESEL)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AGESEL)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AGESEL)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGESEL) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGESEL_2_5'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGESEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGESEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGESEL)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGESEL = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AGESEL = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endif
        this.w_AGESEL = space(5)
        this.w_DESAGE = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGESEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMMESSA
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMESSA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMESSA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMESSA))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMESSA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COMMESSA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COMMESSA)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COMMESSA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMESSA_2_14'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMESSA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMESSA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMESSA)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMESSA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COMMESSA = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_COMMESSA = space(15)
        this.w_DESCOM = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMESSA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_COMMESSA);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_COMMESSA;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_COMMESSA);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_COMMESSA);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_2_20'),i_cWhere,'GSPC_BZZ',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COMMESSA<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_COMMESSA);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_COMMESSA);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_COMMESSA;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLVEAC1_1_2.RadioValue()==this.w_FLVEAC1)
      this.oPgFrm.Page1.oPag.oFLVEAC1_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_8.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_8.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_9.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_9.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_12.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_12.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_13.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_13.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_14.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_14.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_17.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_17.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_18.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_18.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCAR_1_24.value==this.w_TOTCAR)
      this.oPgFrm.Page1.oPag.oTOTCAR_1_24.value=this.w_TOTCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_26.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_26.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSCA_1_27.value==this.w_TOTSCA)
      this.oPgFrm.Page1.oPag.oTOTSCA_1_27.value=this.w_TOTSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAPP_1_38.value==this.w_DESAPP)
      this.oPgFrm.Page1.oPag.oDESAPP_1_38.value=this.w_DESAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIFOR_1_39.value==this.w_CLIFOR)
      this.oPgFrm.Page1.oPag.oCLIFOR_1_39.value=this.w_CLIFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_41.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_41.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_45.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_45.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page2.oPag.oCLISEL_2_1.value==this.w_CLISEL)
      this.oPgFrm.Page2.oPag.oCLISEL_2_1.value=this.w_CLISEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLI_2_2.value==this.w_DESCLI)
      this.oPgFrm.Page2.oPag.oDESCLI_2_2.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUSEL_2_3.value==this.w_CAUSEL)
      this.oPgFrm.Page2.oPag.oCAUSEL_2_3.value=this.w_CAUSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU2_2_4.value==this.w_DESCAU2)
      this.oPgFrm.Page2.oPag.oDESCAU2_2_4.value=this.w_DESCAU2
    endif
    if not(this.oPgFrm.Page2.oPag.oAGESEL_2_5.value==this.w_AGESEL)
      this.oPgFrm.Page2.oPag.oAGESEL_2_5.value=this.w_AGESEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_6.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_6.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMMESSA_2_14.value==this.w_COMMESSA)
      this.oPgFrm.Page2.oPag.oCOMMESSA_2_14.value=this.w_COMMESSA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_15.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_15.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oMVRIFCLI_2_17.value==this.w_MVRIFCLI)
      this.oPgFrm.Page2.oPag.oMVRIFCLI_2_17.value=this.w_MVRIFCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT_2_19.value==this.w_DESATT)
      this.oPgFrm.Page2.oPag.oDESATT_2_19.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page2.oPag.oCODATT_2_20.value==this.w_CODATT)
      this.oPgFrm.Page2.oPag.oCODATT_2_20.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPEVA_2_22.RadioValue()==this.w_TIPEVA)
      this.oPgFrm.Page2.oPag.oTIPEVA_2_22.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FLVEAC1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFLVEAC1_1_2.SetFocus()
            i_bnoObbl = !empty(.w_FLVEAC1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_TFLVEAC=.w_FLVEAC1 OR .w_FLVEAC1='E') AND .w_TCATDOC='OR')  and not(empty(.w_TIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPDOC_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   ((empty(.w_DOCINI)) or not(.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_17.SetFocus()
            i_bnoObbl = !empty(.w_DOCINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DOCFIN)) or not(.w_DOCINI<=.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DOCFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_TIPCLF $ 'CF')  and not(empty(.w_CLISEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLISEL_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CAUSEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCAUSEL_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_AGESEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAGESEL_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(g_COMM<>'S' And .w_AZGESCOM <> 'S')  and (g_COMM='S' Or .w_AZGESCOM = 'S')  and not(empty(.w_COMMESSA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOMMESSA_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODART = this.w_CODART
    this.o_TIPCLF = this.w_TIPCLF
    return

enddefine

* --- Define pages as container
define class tgsor_szmPag1 as StdContainer
  Width  = 780
  height = 475
  stdWidth  = 780
  stdheight = 475
  resizeXpos=337
  resizeYpos=225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLVEAC1_1_2 as StdCombo with uid="CJWKASNQRD",rtseq=1,rtrep=.f.,left=73,top=7,width=123,height=21;
    , ToolTipText = "Tipo gestione interessata";
    , HelpContextID = 118405290;
    , cFormVar="w_FLVEAC1",RowSource=""+"Impegni da cliente,"+"Ordini a fornitore,"+"Entrambi", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC1_1_2.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oFLVEAC1_1_2.GetRadio()
    this.Parent.oContained.w_FLVEAC1 = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC1_1_2.SetRadio()
    this.Parent.oContained.w_FLVEAC1=trim(this.Parent.oContained.w_FLVEAC1)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC1=='V',1,;
      iif(this.Parent.oContained.w_FLVEAC1=='A',2,;
      iif(this.Parent.oContained.w_FLVEAC1=='E',3,;
      0)))
  endfunc

  add object oTIPDOC_1_8 as StdField with uid="IJMQFEFGJI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Causale ordine di selezione (vuoto = no selezione)",;
    HelpContextID = 103815882,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=277, Top=7, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOR_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOR_ATD',"Causali ordini",'GSOR1SDV.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPDOC_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSOR_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPDOC
     i_obj.ecpSave()
  endproc

  add object oDESDOC_1_9 as StdField with uid="QECPCAZADD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 103804874,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=341, Top=7, InputMask=replicate('X',35)

  add object oCODART_1_12 as StdField with uid="CBIOIQRQYC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Codice articolo di selezione (vuoto = no selezione)",;
    HelpContextID = 84137434,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=73, Top=34, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli/servizi",'',this.parent.oContained
  endproc
  proc oCODART_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_13 as StdField with uid="EAOTDSFLRS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 84078538,;
   bGlobalFont=.t.,;
    Height=21, Width=327, Left=226, Top=34, InputMask=replicate('X',40)

  add object oCODMAG_1_14 as StdField with uid="SRJIBZMYUQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice magazzino di selezione (vuoto = no selezione)",;
    HelpContextID = 50845146,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=73, Top=59, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDOCINI_1_17 as StdField with uid="MIFEJBCNZW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data ordine di inizio selezione",;
    HelpContextID = 3925450,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=636, Top=7

  func oDOCINI_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_1_18 as StdField with uid="QHBUQOMEEN",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data ordine di fine selezione",;
    HelpContextID = 193914314,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=636, Top=34

  func oDOCFIN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc


  add object oBtn_1_19 as StdButton with uid="DXKTUFUCYW",left=722, top=7, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 64867050;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSOR_BVM(this.Parent.oContained,"O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="PSWDLJKBSX",left=676, top=430, width=48,height=45,;
    CpPicture="BMP\doc1.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato alla riga selezionata";
    , HelpContextID = 35238426;
    , Caption='\<Ordine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_SERIALE, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="DIHQWVDEWH",left=725, top=430, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 33963590;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomDoc as cp_zoombox with uid="AMNTRQXZGR",left=3, top=83, width=770,height=296,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_DETT",cZoomFile="GSOR_SZM",bOptions=.f.,bQueryOnLoad=.f.,cZoomOnZoom="GSZM_BZM",bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",bRetriveAllRows=.f.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 63791642

  add object oTOTCAR_1_24 as StdField with uid="BKPKHMCASJ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_TOTCAR", cQueryName = "TOTCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale carichi dei movimenti selezionati",;
    HelpContextID = 135320778,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=616, Top=382, cSayPict="v_PQ(16)", cGetPict="v_GQ(16)"

  func oTOTCAR_1_24.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc

  add object oDESCAU_1_26 as StdField with uid="QGYHPJFPBO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del cliente o fornitore selezionato",;
    HelpContextID = 84996042,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=89, Top=382, InputMask=replicate('X',35)

  add object oTOTSCA_1_27 as StdField with uid="STYLXWMTVB",rtseq=21,rtrep=.f.,;
    cFormVar = "w_TOTSCA", cQueryName = "TOTSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale scarichi dei movimenti selezionati",;
    HelpContextID = 148952266,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=616, Top=406, cSayPict="v_PQ(16)", cGetPict="v_GQ(16)"

  func oTOTSCA_1_27.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc

  add object oDESAPP_1_38 as StdField with uid="BABQLTUQLR",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESAPP", cQueryName = "DESAPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 153284554,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=139, Top=59, InputMask=replicate('X',30)

  add object oCLIFOR_1_39 as StdField with uid="DJSGGREKZV",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CLIFOR", cQueryName = "CLIFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del cliente o fornitore selezionato",;
    HelpContextID = 120490202,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=89, Top=406, InputMask=replicate('X',40)

  add object oUNIMIS_1_41 as StdField with uid="NBMUISZFMG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 109544890,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=736, Top=382, InputMask=replicate('X',3)

  func oUNIMIS_1_41.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc

  add object oUNIMIS_1_45 as StdField with uid="FQCNQHZMEU",rtseq=30,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 109544890,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=736, Top=406, InputMask=replicate('X',3)

  func oUNIMIS_1_45.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc


  add object oObj_1_47 as cp_runprogram with uid="EJGYZWXGXP",left=0, top=486, width=202,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSOR_BVM("O")',;
    cEvent = "LanciaCLI",;
    nPag=1;
    , HelpContextID = 63791642


  add object oObj_1_49 as cp_runprogram with uid="TGKDFMAWLW",left=205, top=487, width=325,height=19,;
    caption='GSVE_BZM(w_SERIALE w_PARAME)',;
   bGlobalFont=.t.,;
    prg="GSVE_BZM(w_SERIALE, w_PARAME)",;
    cEvent = "w_zoomdoc selected",;
    nPag=1;
    , HelpContextID = 149838954


  add object oObj_1_51 as cp_outputCombo with uid="ZLERXIVPWT",left=89, top=430, width=380,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 63791642


  add object oBtn_1_52 as StdButton with uid="PYYBFJPVEW",left=627, top=430, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 99999706;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      with this.Parent.oContained
        GSOR_BVM(this.Parent.oContained,"Stampa")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_52.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc

  add object oStr_1_25 as StdString with uid="GCOTVXRFKE",Visible=.t., Left=1, Top=34,;
    Alignment=1, Width=70, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="BWAJSIGIRU",Visible=.t., Left=1, Top=59,;
    Alignment=1, Width=70, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="GIQZZPYXTY",Visible=.t., Left=508, Top=382,;
    Alignment=1, Width=106, Height=15,;
    Caption="Totale ordinato:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="JPFDWSHJIH",Visible=.t., Left=508, Top=406,;
    Alignment=1, Width=106, Height=15,;
    Caption="Totale impegnato:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="ISOELZCLWA",Visible=.t., Left=1, Top=7,;
    Alignment=1, Width=70, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="FPFVMNQOLY",Visible=.t., Left=557, Top=7,;
    Alignment=1, Width=78, Height=15,;
    Caption="Ordini dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="JNONRJVUML",Visible=.t., Left=560, Top=34,;
    Alignment=1, Width=75, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="UJMFSTFYKT",Visible=.t., Left=12, Top=382,;
    Alignment=1, Width=75, Height=15,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="CSTGAWKAXO",Visible=.t., Left=200, Top=7,;
    Alignment=1, Width=76, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="VBDYIIJADD",Visible=.t., Left=12, Top=406,;
    Alignment=1, Width=75, Height=15,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="HRTFFGQQPR",Visible=.t., Left=-6, Top=430,;
    Alignment=1, Width=93, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsor_szmPag2 as StdContainer
  Width  = 780
  height = 475
  stdWidth  = 780
  stdheight = 475
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLISEL_2_1 as StdField with uid="OLVXBDRVUA",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CLISEL", cQueryName = "CLISEL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice cliente inesistente oppure obsoleto",;
    ToolTipText = "Codice intestatario dell'ordine di selezione (vuoto = no selezione)",;
    HelpContextID = 230787290,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=158, Top=14, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLISEL"

  func oCLISEL_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLF $ 'CF')
    endwith
   endif
  endfunc

  func oCLISEL_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLISEL_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLISEL_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLISEL_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCLI_2_2 as StdField with uid="CYRRYPDESQ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 6352842,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=297, Top=14, InputMask=replicate('X',40)

  add object oCAUSEL_2_3 as StdField with uid="SRCSXYXYBQ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CAUSEL", cQueryName = "CAUSEL",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale di magazzino inesistente oppure obsoleta",;
    ToolTipText = "Causale di magazzino di selezione (vuoto=no selezione)",;
    HelpContextID = 230740954,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=158, Top=45, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CAUSEL"

  func oCAUSEL_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUSEL_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUSEL_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCAUSEL_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oCAUSEL_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CAUSEL
     i_obj.ecpSave()
  endproc

  add object oDESCAU2_2_4 as StdField with uid="IDIHVAUWCC",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESCAU2", cQueryName = "DESCAU2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 84996042,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=223, Top=45, InputMask=replicate('X',35)

  add object oAGESEL_2_5 as StdField with uid="ABGHUDDXBG",rtseq=35,rtrep=.f.,;
    cFormVar = "w_AGESEL", cQueryName = "AGESEL",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente oppure obsoleto",;
    ToolTipText = "Codice agente di selezione (vuoto=no selezione)",;
    HelpContextID = 230804986,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=158, Top=76, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGESEL"

  func oAGESEL_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGESEL_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGESEL_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGESEL_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oAGESEL_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGESEL
     i_obj.ecpSave()
  endproc

  add object oDESAGE_2_6 as StdField with uid="GHEZEFRERL",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 78835658,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=223, Top=76, InputMask=replicate('X',35)

  add object oCOMMESSA_2_14 as StdField with uid="EFUZVYXOTE",rtseq=40,rtrep=.f.,;
    cFormVar = "w_COMMESSA", cQueryName = "COMMESSA",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice commessa di selezione (vuoto = no selezione)",;
    HelpContextID = 113722777,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=158, Top=138, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMESSA"

  func oCOMMESSA_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S' Or .w_AZGESCOM = 'S')
    endwith
   endif
  endfunc

  func oCOMMESSA_2_14.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' And .w_AZGESCOM <> 'S')
    endwith
  endfunc

  func oCOMMESSA_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_2_20('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCOMMESSA_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMESSA_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMESSA_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCOMMESSA_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_COMMESSA
     i_obj.ecpSave()
  endproc

  add object oDESCOM_2_15 as StdField with uid="PFJJAHONNO",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 204533706,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=295, Top=138, InputMask=replicate('X',30)

  func oDESCOM_2_15.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' And .w_AZGESCOM <> 'S')
    endwith
  endfunc

  add object oMVRIFCLI_2_17 as StdField with uid="VWBZQAQARC",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MVRIFCLI", cQueryName = "MVRIFCLI",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento codice descrittivo",;
    HelpContextID = 155521551,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=158, Top=107, InputMask=replicate('X',30)

  add object oDESATT_2_19 as StdField with uid="MPOQOJPHVZ",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 81981386,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=295, Top=169, InputMask=replicate('X',35)

  func oDESATT_2_19.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' OR EMPTY (.w_COMMESSA))
    endwith
  endfunc

  add object oCODATT_2_20 as StdField with uid="ULBQAHZXRW",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 82040282,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=158, Top=169, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_COMMESSA", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc

  func oCODATT_2_20.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' OR EMPTY (.w_COMMESSA))
    endwith
  endfunc

  func oCODATT_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_COMMESSA)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_COMMESSA)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'',this.parent.oContained
  endproc
  proc oCODATT_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_COMMESSA
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATT
     i_obj.ecpSave()
  endproc


  add object oTIPEVA_2_22 as StdCombo with uid="RJUTOFBIMA",rtseq=46,rtrep=.f.,left=158,top=200,width=156,height=21;
    , ToolTipText = "Selezione: tutti i documenti, solo righe inevase, solo documenti con righe non evase totalmente";
    , HelpContextID = 129964746;
    , cFormVar="w_TIPEVA",RowSource=""+"Tutti,"+"Righe inevase,"+"Documenti inevasi", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPEVA_2_22.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'R',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oTIPEVA_2_22.GetRadio()
    this.Parent.oContained.w_TIPEVA = this.RadioValue()
    return .t.
  endfunc

  func oTIPEVA_2_22.SetRadio()
    this.Parent.oContained.w_TIPEVA=trim(this.Parent.oContained.w_TIPEVA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPEVA=='T',1,;
      iif(this.Parent.oContained.w_TIPEVA=='R',2,;
      iif(this.Parent.oContained.w_TIPEVA=='D',3,;
      0)))
  endfunc

  add object oStr_2_8 as StdString with uid="JTCOGZKEKJ",Visible=.t., Left=5, Top=16,;
    Alignment=1, Width=151, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="GXIIOKIUMJ",Visible=.t., Left=5, Top=78,;
    Alignment=1, Width=151, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="NQIBGIGFCN",Visible=.t., Left=5, Top=47,;
    Alignment=1, Width=151, Height=18,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="JLAFKLEZZF",Visible=.t., Left=5, Top=140,;
    Alignment=1, Width=151, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_13.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' And .w_AZGESCOM <> 'S')
    endwith
  endfunc

  add object oStr_2_16 as StdString with uid="ZOTHAXBXGW",Visible=.t., Left=5, Top=108,;
    Alignment=1, Width=151, Height=18,;
    Caption="Riferimento descrittivo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="GNPJTXSYTP",Visible=.t., Left=36, Top=172,;
    Alignment=1, Width=120, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' OR EMPTY (.w_COMMESSA))
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="DVTJLGBIGM",Visible=.t., Left=98, Top=201,;
    Alignment=1, Width=58, Height=18,;
    Caption="Evasione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsor_szm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
