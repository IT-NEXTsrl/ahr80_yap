* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_kim                                                        *
*              Invio messaggi da controllo flussi                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-08                                                      *
* Last revis.: 2012-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscf_kim",oParentObject))

* --- Class definition
define class tgscf_kim as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 580
  Height = 382
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-02-27"
  HelpContextID=1428119
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  cpusers_IDX = 0
  cpgroups_IDX = 0
  REF_MAST_IDX = 0
  XDC_TABLE_IDX = 0
  cPrg = "gscf_kim"
  cComment = "Invio messaggi da controllo flussi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_REGINI = space(10)
  w_DESINI = space(50)
  w_REGFIN = space(10)
  w_DESFIN = space(50)
  w_TABELLA = space(20)
  w_TBCOMMENT = space(60)
  w_REBUSOBJ = space(15)
  w_CODUTE = 0
  w_UTENAME = space(30)
  w_CODGRU = 0
  w_GRUNAME = space(30)
  w_POSTIN = space(1)
  w_EMAIL = space(1)
  w_SHOWKEY = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscf_kimPag1","gscf_kim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oREGINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='cpgroups'
    this.cWorkTables[3]='REF_MAST'
    this.cWorkTables[4]='XDC_TABLE'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do gscf_bim with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_REGINI=space(10)
      .w_DESINI=space(50)
      .w_REGFIN=space(10)
      .w_DESFIN=space(50)
      .w_TABELLA=space(20)
      .w_TBCOMMENT=space(60)
      .w_REBUSOBJ=space(15)
      .w_CODUTE=0
      .w_UTENAME=space(30)
      .w_CODGRU=0
      .w_GRUNAME=space(30)
      .w_POSTIN=space(1)
      .w_EMAIL=space(1)
      .w_SHOWKEY=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_REGINI))
          .link_1_3('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_REGFIN))
          .link_1_5('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_TABELLA))
          .link_1_9('Full')
        endif
        .DoRTCalc(6,8,.f.)
        if not(empty(.w_CODUTE))
          .link_1_16('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_CODGRU))
          .link_1_18('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_POSTIN = 'S'
        .w_EMAIL = 'S'
        .w_SHOWKEY = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=REGINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REF_MAST_IDX,3]
    i_lTable = "REF_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2], .t., this.REF_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_REGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCF_MRE',True,'REF_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RESERIAL like "+cp_ToStrODBC(trim(this.w_REGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RESERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RESERIAL',trim(this.w_REGINI))
          select RESERIAL,REDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RESERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_REGINI)==trim(_Link_.RESERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStrODBC(trim(this.w_REGINI)+"%");

            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStr(trim(this.w_REGINI)+"%");

            select RESERIAL,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_REGINI) and !this.bDontReportError
            deferred_cp_zoom('REF_MAST','*','RESERIAL',cp_AbsName(oSource.parent,'oREGINI_1_3'),i_cWhere,'GSCF_MRE',"Regole di log",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RESERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RESERIAL',oSource.xKey(1))
            select RESERIAL,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_REGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RESERIAL="+cp_ToStrODBC(this.w_REGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RESERIAL',this.w_REGINI)
            select RESERIAL,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_REGINI = NVL(_Link_.RESERIAL,space(10))
      this.w_DESINI = NVL(_Link_.REDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_REGINI = space(10)
      endif
      this.w_DESINI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_REGINI<=.w_REGFIN or empty(.w_REGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_REGINI = space(10)
        this.w_DESINI = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])+'\'+cp_ToStr(_Link_.RESERIAL,1)
      cp_ShowWarn(i_cKey,this.REF_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_REGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=REGFIN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REF_MAST_IDX,3]
    i_lTable = "REF_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2], .t., this.REF_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_REGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCF_MRE',True,'REF_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RESERIAL like "+cp_ToStrODBC(trim(this.w_REGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RESERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RESERIAL',trim(this.w_REGFIN))
          select RESERIAL,REDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RESERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_REGFIN)==trim(_Link_.RESERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStrODBC(trim(this.w_REGFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStr(trim(this.w_REGFIN)+"%");

            select RESERIAL,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_REGFIN) and !this.bDontReportError
            deferred_cp_zoom('REF_MAST','*','RESERIAL',cp_AbsName(oSource.parent,'oREGFIN_1_5'),i_cWhere,'GSCF_MRE',"Regole di log",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RESERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RESERIAL',oSource.xKey(1))
            select RESERIAL,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_REGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RESERIAL="+cp_ToStrODBC(this.w_REGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RESERIAL',this.w_REGFIN)
            select RESERIAL,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_REGFIN = NVL(_Link_.RESERIAL,space(10))
      this.w_DESFIN = NVL(_Link_.REDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_REGFIN = space(10)
      endif
      this.w_DESFIN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_REGINI<=.w_REGFIN or empty(.w_REGINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_REGFIN = space(10)
        this.w_DESFIN = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])+'\'+cp_ToStr(_Link_.RESERIAL,1)
      cp_ShowWarn(i_cKey,this.REF_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_REGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TABELLA
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TABELLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Tabelle',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_TABELLA)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_TABELLA))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TABELLA)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStrODBC(trim(this.w_TABELLA)+"%");

            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStr(trim(this.w_TABELLA)+"%");

            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TABELLA) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oTABELLA_1_9'),i_cWhere,'Tabelle',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TABELLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_TABELLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TABELLA)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TABELLA = NVL(_Link_.TBNAME,space(20))
      this.w_TBCOMMENT = NVL(_Link_.TBCOMMENT,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_TABELLA = space(20)
      endif
      this.w_TBCOMMENT = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TABELLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oCODUTE_1_16'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.code,0)
      this.w_UTENAME = NVL(_Link_.name,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_UTENAME = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','code',cp_AbsName(oSource.parent,'oCODGRU_1_18'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.code,0)
      this.w_GRUNAME = NVL(_Link_.name,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = 0
      endif
      this.w_GRUNAME = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oREGINI_1_3.value==this.w_REGINI)
      this.oPgFrm.Page1.oPag.oREGINI_1_3.value=this.w_REGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_4.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_4.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oREGFIN_1_5.value==this.w_REGFIN)
      this.oPgFrm.Page1.oPag.oREGFIN_1_5.value=this.w_REGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_6.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_6.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTABELLA_1_9.value==this.w_TABELLA)
      this.oPgFrm.Page1.oPag.oTABELLA_1_9.value=this.w_TABELLA
    endif
    if not(this.oPgFrm.Page1.oPag.oTBCOMMENT_1_10.value==this.w_TBCOMMENT)
      this.oPgFrm.Page1.oPag.oTBCOMMENT_1_10.value=this.w_TBCOMMENT
    endif
    if not(this.oPgFrm.Page1.oPag.oREBUSOBJ_1_12.value==this.w_REBUSOBJ)
      this.oPgFrm.Page1.oPag.oREBUSOBJ_1_12.value=this.w_REBUSOBJ
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_16.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_16.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oUTENAME_1_17.value==this.w_UTENAME)
      this.oPgFrm.Page1.oPag.oUTENAME_1_17.value=this.w_UTENAME
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_18.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_18.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUNAME_1_20.value==this.w_GRUNAME)
      this.oPgFrm.Page1.oPag.oGRUNAME_1_20.value=this.w_GRUNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oPOSTIN_1_24.RadioValue()==this.w_POSTIN)
      this.oPgFrm.Page1.oPag.oPOSTIN_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEMAIL_1_25.RadioValue()==this.w_EMAIL)
      this.oPgFrm.Page1.oPag.oEMAIL_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHOWKEY_1_30.RadioValue()==this.w_SHOWKEY)
      this.oPgFrm.Page1.oPag.oSHOWKEY_1_30.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_REGINI<=.w_REGFIN or empty(.w_REGFIN))  and not(empty(.w_REGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oREGINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_REGINI<=.w_REGFIN or empty(.w_REGINI))  and not(empty(.w_REGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oREGFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscf_kimPag1 as StdContainer
  Width  = 576
  height = 382
  stdWidth  = 576
  stdheight = 382
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oREGINI_1_3 as StdField with uid="EEAIKEKGKG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_REGINI", cQueryName = "REGINI",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Regola di inizio selezione",;
    HelpContextID = 29129450,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=95, Top=31, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="REF_MAST", cZoomOnZoom="GSCF_MRE", oKey_1_1="RESERIAL", oKey_1_2="this.w_REGINI"

  func oREGINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oREGINI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oREGINI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REF_MAST','*','RESERIAL',cp_AbsName(this.parent,'oREGINI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCF_MRE',"Regole di log",'',this.parent.oContained
  endproc
  proc oREGINI_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSCF_MRE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RESERIAL=this.parent.oContained.w_REGINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_4 as StdField with uid="VZQELFRZEB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 29080522,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=198, Top=31, InputMask=replicate('X',50)

  add object oREGFIN_1_5 as StdField with uid="MAZTXNVNIH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_REGFIN", cQueryName = "REGFIN",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Regola di fine selezione",;
    HelpContextID = 219118314,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=95, Top=53, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="REF_MAST", cZoomOnZoom="GSCF_MRE", oKey_1_1="RESERIAL", oKey_1_2="this.w_REGFIN"

  proc oREGFIN_1_5.mDefault
    with this.Parent.oContained
      if empty(.w_REGFIN)
        .w_REGFIN = .w_REGINI
      endif
    endwith
  endproc

  func oREGFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oREGFIN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oREGFIN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REF_MAST','*','RESERIAL',cp_AbsName(this.parent,'oREGFIN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCF_MRE',"Regole di log",'',this.parent.oContained
  endproc
  proc oREGFIN_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCF_MRE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RESERIAL=this.parent.oContained.w_REGFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_6 as StdField with uid="JMXZKRYWLG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 219069386,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=198, Top=53, InputMask=replicate('X',50)

  add object oTABELLA_1_9 as StdField with uid="HMBHKDPHIH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TABELLA", cQueryName = "TABELLA",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tabella associata alla regola",;
    HelpContextID = 249614026,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=95, Top=80, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="XDC_TABLE", cZoomOnZoom="Tabelle", oKey_1_1="TBNAME", oKey_1_2="this.w_TABELLA"

  func oTABELLA_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oTABELLA_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTABELLA_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oTABELLA_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'Tabelle',"",'',this.parent.oContained
  endproc
  proc oTABELLA_1_9.mZoomOnZoom
    local i_obj
    i_obj=Tabelle()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TBNAME=this.parent.oContained.w_TABELLA
     i_obj.ecpSave()
  endproc

  add object oTBCOMMENT_1_10 as StdField with uid="WWMCZHAKPZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TBCOMMENT", cQueryName = "TBCOMMENT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 231127100,;
   bGlobalFont=.t.,;
    Height=21, Width=309, Left=252, Top=80, InputMask=replicate('X',60)

  add object oREBUSOBJ_1_12 as StdField with uid="VNQPBQPOYQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_REBUSOBJ", cQueryName = "REBUSOBJ",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Programma associato alla regola",;
    HelpContextID = 190892704,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=95, Top=107, InputMask=replicate('X',15)

  add object oCODUTE_1_16 as StdField with uid="SVBRNFLEIB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente destinatario dei messaggi",;
    HelpContextID = 89170394,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=95, Top=170, bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oCODUTE_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oUTENAME_1_17 as StdField with uid="WRRMDQPDNP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_UTENAME", cQueryName = "UTENAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 243764154,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=157, Top=170, InputMask=replicate('X',30)

  add object oCODGRU_1_18 as StdField with uid="JFWMVAJSYT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Gruppo degli utenti destinatari del messaggio",;
    HelpContextID = 92185050,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=95, Top=200, bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="code", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpgroups','*','code',cp_AbsName(this.parent,'oCODGRU_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oGRUNAME_1_20 as StdField with uid="AMOLYWDLGS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_GRUNAME", cQueryName = "GRUNAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 243699354,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=157, Top=200, InputMask=replicate('X',30)

  add object oPOSTIN_1_24 as StdCheck with uid="IVQTWDUKYU",rtseq=12,rtrep=.f.,left=95, top=261, caption="Post-IN",;
    ToolTipText = "Seleziona i destinatari a cui inviare il post-IN",;
    HelpContextID = 218149130,;
    cFormVar="w_POSTIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPOSTIN_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPOSTIN_1_24.GetRadio()
    this.Parent.oContained.w_POSTIN = this.RadioValue()
    return .t.
  endfunc

  func oPOSTIN_1_24.SetRadio()
    this.Parent.oContained.w_POSTIN=trim(this.Parent.oContained.w_POSTIN)
    this.value = ;
      iif(this.Parent.oContained.w_POSTIN=='S',1,;
      0)
  endfunc

  add object oEMAIL_1_25 as StdCheck with uid="KKEPELKXYN",rtseq=13,rtrep=.f.,left=230, top=261, caption="E-mail",;
    ToolTipText = "Seleziona i destinatari a cui inviare l'E-mail",;
    HelpContextID = 86191174,;
    cFormVar="w_EMAIL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEMAIL_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oEMAIL_1_25.GetRadio()
    this.Parent.oContained.w_EMAIL = this.RadioValue()
    return .t.
  endfunc

  func oEMAIL_1_25.SetRadio()
    this.Parent.oContained.w_EMAIL=trim(this.Parent.oContained.w_EMAIL)
    this.value = ;
      iif(this.Parent.oContained.w_EMAIL=='S',1,;
      0)
  endfunc


  add object oBtn_1_26 as StdButton with uid="VMPFRPKBFV",left=466, top=330, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare l'inserimento";
    , HelpContextID = 1456870;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_27 as StdButton with uid="MTXNONHMHL",left=519, top=330, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 8745542;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSHOWKEY_1_30 as StdCheck with uid="EOTZIDOCSZ",rtseq=14,rtrep=.f.,left=95, top=313, caption="Chiave di ricerca",;
    ToolTipText = "Include la chiave di ricerca nel testo del messaggio",;
    HelpContextID = 170002470,;
    cFormVar="w_SHOWKEY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSHOWKEY_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSHOWKEY_1_30.GetRadio()
    this.Parent.oContained.w_SHOWKEY = this.RadioValue()
    return .t.
  endfunc

  func oSHOWKEY_1_30.SetRadio()
    this.Parent.oContained.w_SHOWKEY=trim(this.Parent.oContained.w_SHOWKEY)
    this.value = ;
      iif(this.Parent.oContained.w_SHOWKEY=='S',1,;
      0)
  endfunc

  add object oStr_1_1 as StdString with uid="XPRGKZKAND",Visible=.t., Left=16, Top=8,;
    Alignment=0, Width=115, Height=18,;
    Caption="Selezione regole"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="XJMOJJPKWI",Visible=.t., Left=16, Top=33,;
    Alignment=1, Width=76, Height=18,;
    Caption="Da regola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="UPTGHEQEVM",Visible=.t., Left=16, Top=55,;
    Alignment=1, Width=76, Height=18,;
    Caption="A regola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="VUIYGKCHII",Visible=.t., Left=48, Top=82,;
    Alignment=1, Width=44, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="YYIWDWULLZ",Visible=.t., Left=9, Top=109,;
    Alignment=1, Width=83, Height=18,;
    Caption="Programma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="ITTROUZLMG",Visible=.t., Left=16, Top=147,;
    Alignment=0, Width=115, Height=18,;
    Caption="Selezione destinatari"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="VEGGZMQZJG",Visible=.t., Left=53, Top=173,;
    Alignment=1, Width=39, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="WATUGLVSSB",Visible=.t., Left=48, Top=203,;
    Alignment=1, Width=44, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="VGLUJGNWUL",Visible=.t., Left=16, Top=240,;
    Alignment=0, Width=143, Height=18,;
    Caption="Selezione tipo messaggio"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="XBHKLNXCFU",Visible=.t., Left=16, Top=292,;
    Alignment=0, Width=143, Height=18,;
    Caption="Opzioni messaggio"  ;
  , bGlobalFont=.t.

  add object oBox_1_2 as StdBox with uid="HCWXQIBYIU",left=9, top=24, width=557,height=1

  add object oBox_1_21 as StdBox with uid="NHLGJYMJPL",left=9, top=163, width=557,height=1

  add object oBox_1_23 as StdBox with uid="RDLWDIOXRL",left=9, top=256, width=557,height=1

  add object oBox_1_29 as StdBox with uid="MVOFISHECD",left=9, top=308, width=557,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscf_kim','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
