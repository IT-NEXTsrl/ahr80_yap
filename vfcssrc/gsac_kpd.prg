* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_kpd                                                        *
*              Manutenzione PDA                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_382]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-20                                                      *
* Last revis.: 2017-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsac_kpd
* Array per periodi PDA
dimension PeriodiPDA(100)
* --- Fine Area Manuale
return(createobject("tgsac_kpd",oParentObject))

* --- Class definition
define class tgsac_kpd as StdForm
  Top    = 0
  Left   = 5

  * --- Standard Properties
  Width  = 790
  Height = 473+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-06-21"
  HelpContextID=149579625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=118

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  cpusers_IDX = 0
  ART_ICOL_IDX = 0
  PAR_RIOR_IDX = 0
  PDA_TPER_IDX = 0
  CONTI_IDX = 0
  KEY_ARTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  MAGAZZIN_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gsac_kpd"
  cComment = "Manutenzione PDA"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ReadPar = space(2)
  w_LargCol = 0
  w_NMAXPE = 0
  w_DimFont = 0
  w_CALSTA = space(5)
  w_NumPer = 0
  w_ZoomEspanso = .F.
  w_Zoom1Height = 0
  w_PPCOLSUG = 0
  w_PPCOLCON = 0
  w_PPCOLMIS = 0
  w_PPCOLMCO = 0
  w_DESCSEL = space(40)
  w_SELART = space(20)
  w_SELVAR = space(20)
  w_sCODART = space(20)
  w_sCODVAR = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_TIPGES = space(1)
  w_TIPOARTI = space(1)
  w_ACTIVECOL = 0
  w_PPCOLFES = 0
  w_COLSEL = 0
  w_sTIPGES = space(1)
  w_sCODPLA = space(5)
  w_PLDES = space(35)
  w_sLINKVAR = space(20)
  w_sSCOMIN = 0
  w_cPERDTF = space(3)
  w_PPCOLPIA = 0
  w_PPCOLLAN = 0
  w_PPODLMIS = 0
  w_sPUNRIO = 0
  w_sQTAMIN = 0
  w_sLOTRIO = 0
  w_PSELR = space(4)
  w_TipoPeriodo = space(20)
  w_PSELA = space(3)
  w_DIP = ctod('  /  /  ')
  w_DFP = ctod('  /  /  ')
  w_PDASEL = space(10)
  w_PDANUM = 0
  w_DESFOR = space(40)
  w_DESCAR = space(40)
  w_sCODART = space(20)
  w_sDESART = space(40)
  w_pCVAR = space(5)
  w_sCODVAR = space(20)
  w_sDESVAR = space(40)
  w_DataElab = ctod('  /  /  ')
  w_OraElab = space(8)
  w_OpeElab = 0
  w_DESUTE = space(20)
  w_ELAVER = ctod('  /  /  ')
  w_DINVER = ctod('  /  /  ')
  w_DFIVER = ctod('  /  /  ')
  w_ORAVER = space(8)
  w_OPEVER = 0
  w_DESUTV = space(20)
  w_BASSUG = 0
  w_TESSUG = 0
  w_BASCON = 0
  w_TESCON = 0
  w_BASMIS = 0
  w_TESMIS = 0
  w_BASPIA = 0
  w_TESPIA = 0
  w_RET = .F.
  w_TIPATT = space(1)
  w_TIPGES = space(1)
  w_fPROFIN = space(2)
  w_fSEMLAV = space(2)
  w_fMATPRI = space(2)
  w_fDISINI = space(20)
  w_fDISFIN = space(20)
  w_fCODFAM = space(5)
  w_fCODFAF = space(5)
  w_CRIELA = space(1)
  o_CRIELA = space(1)
  w_fGRUMER = space(5)
  w_fGRUMEF = space(5)
  w_fCATOMO = space(5)
  w_fCATOMF = space(5)
  w_fCODMAR = space(5)
  w_fCODMAF = space(5)
  w_fMAGINI = space(5)
  w_fMAGFIN = space(5)
  w_fCODCOM = space(15)
  o_fCODCOM = space(15)
  w_fCODCOF = space(15)
  o_fCODCOF = space(15)
  w_fCODATT = space(15)
  w_fCODATF = space(15)
  w_fCODFOR = space(15)
  w_fCODFOF = space(15)
  w_DESINI = space(40)
  w_SELEZM = space(1)
  o_SELEZM = space(1)
  w_fTIPGES = space(1)
  w_DESFIN = space(40)
  w_DESGRU = space(35)
  w_DESCAT = space(35)
  w_DESCAN = space(30)
  w_DESATT = space(30)
  w_DESFORN = space(40)
  w_DESFAMA = space(35)
  w_DESMAR = space(35)
  w_DESGRF = space(35)
  w_DESCAF = space(35)
  w_DESFAMF = space(35)
  w_DESMAF = space(35)
  w_DESFOF = space(40)
  w_DESCANF = space(30)
  w_DESATF = space(30)
  w_DESMAGI = space(30)
  w_DESMAGF = space(30)
  w_RIGALIS = 0
  w_CACODICE = space(20)
  w_TIPCON = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_KEYRIF = space(10)
  w_PPCRIELA = space(1)
  w_ZoomPDA = .NULL.
  w_DettPDA = .NULL.
  w_PeriodoPDA = .NULL.
  w_SUGGE = .NULL.
  w_ZOOMMAGA = .NULL.
  w_LBLMAGA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsac_kpd
  o_ACTIVECOL = 0
  o_CACODICE = space(20)
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_kpdPag1","gsac_kpd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("PDA")
      .Pages(2).addobject("oPag","tgsac_kpdPag2","gsac_kpd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettaglio periodo")
      .Pages(3).addobject("oPag","tgsac_kpdPag3","gsac_kpd",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Informazioni")
      .Pages(4).addobject("oPag","tgsac_kpdPag4","gsac_kpd",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Selezioni")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsac_kpd
    this.Pages(1).oPag.DettPDA.visible=.f.
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomPDA = this.oPgFrm.Pages(1).oPag.ZoomPDA
    this.w_DettPDA = this.oPgFrm.Pages(1).oPag.DettPDA
    this.w_PeriodoPDA = this.oPgFrm.Pages(2).oPag.PeriodoPDA
    this.w_SUGGE = this.oPgFrm.Pages(3).oPag.SUGGE
    this.w_ZOOMMAGA = this.oPgFrm.Pages(4).oPag.ZOOMMAGA
    this.w_LBLMAGA = this.oPgFrm.Pages(4).oPag.LBLMAGA
    DoDefault()
    proc Destroy()
      this.w_ZoomPDA = .NULL.
      this.w_DettPDA = .NULL.
      this.w_PeriodoPDA = .NULL.
      this.w_SUGGE = .NULL.
      this.w_ZOOMMAGA = .NULL.
      this.w_LBLMAGA = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='cpusers'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='PAR_RIOR'
    this.cWorkTables[5]='PDA_TPER'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='FAM_ARTI'
    this.cWorkTables[9]='GRUMERC'
    this.cWorkTables[10]='CATEGOMO'
    this.cWorkTables[11]='MARCHI'
    this.cWorkTables[12]='MAGAZZIN'
    this.cWorkTables[13]='CAN_TIER'
    this.cWorkTables[14]='ATTIVITA'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ReadPar=space(2)
      .w_LargCol=0
      .w_NMAXPE=0
      .w_DimFont=0
      .w_CALSTA=space(5)
      .w_NumPer=0
      .w_ZoomEspanso=.f.
      .w_Zoom1Height=0
      .w_PPCOLSUG=0
      .w_PPCOLCON=0
      .w_PPCOLMIS=0
      .w_PPCOLMCO=0
      .w_DESCSEL=space(40)
      .w_SELART=space(20)
      .w_SELVAR=space(20)
      .w_sCODART=space(20)
      .w_sCODVAR=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPGES=space(1)
      .w_TIPOARTI=space(1)
      .w_ACTIVECOL=0
      .w_PPCOLFES=0
      .w_COLSEL=0
      .w_sTIPGES=space(1)
      .w_sCODPLA=space(5)
      .w_PLDES=space(35)
      .w_sLINKVAR=space(20)
      .w_sSCOMIN=0
      .w_cPERDTF=space(3)
      .w_PPCOLPIA=0
      .w_PPCOLLAN=0
      .w_PPODLMIS=0
      .w_sPUNRIO=0
      .w_sQTAMIN=0
      .w_sLOTRIO=0
      .w_PSELR=space(4)
      .w_TipoPeriodo=space(20)
      .w_PSELA=space(3)
      .w_DIP=ctod("  /  /  ")
      .w_DFP=ctod("  /  /  ")
      .w_PDASEL=space(10)
      .w_PDANUM=0
      .w_DESFOR=space(40)
      .w_DESCAR=space(40)
      .w_sCODART=space(20)
      .w_sDESART=space(40)
      .w_pCVAR=space(5)
      .w_sCODVAR=space(20)
      .w_sDESVAR=space(40)
      .w_DataElab=ctod("  /  /  ")
      .w_OraElab=space(8)
      .w_OpeElab=0
      .w_DESUTE=space(20)
      .w_ELAVER=ctod("  /  /  ")
      .w_DINVER=ctod("  /  /  ")
      .w_DFIVER=ctod("  /  /  ")
      .w_ORAVER=space(8)
      .w_OPEVER=0
      .w_DESUTV=space(20)
      .w_BASSUG=0
      .w_TESSUG=0
      .w_BASCON=0
      .w_TESCON=0
      .w_BASMIS=0
      .w_TESMIS=0
      .w_BASPIA=0
      .w_TESPIA=0
      .w_RET=.f.
      .w_TIPATT=space(1)
      .w_TIPGES=space(1)
      .w_fPROFIN=space(2)
      .w_fSEMLAV=space(2)
      .w_fMATPRI=space(2)
      .w_fDISINI=space(20)
      .w_fDISFIN=space(20)
      .w_fCODFAM=space(5)
      .w_fCODFAF=space(5)
      .w_CRIELA=space(1)
      .w_fGRUMER=space(5)
      .w_fGRUMEF=space(5)
      .w_fCATOMO=space(5)
      .w_fCATOMF=space(5)
      .w_fCODMAR=space(5)
      .w_fCODMAF=space(5)
      .w_fMAGINI=space(5)
      .w_fMAGFIN=space(5)
      .w_fCODCOM=space(15)
      .w_fCODCOF=space(15)
      .w_fCODATT=space(15)
      .w_fCODATF=space(15)
      .w_fCODFOR=space(15)
      .w_fCODFOF=space(15)
      .w_DESINI=space(40)
      .w_SELEZM=space(1)
      .w_fTIPGES=space(1)
      .w_DESFIN=space(40)
      .w_DESGRU=space(35)
      .w_DESCAT=space(35)
      .w_DESCAN=space(30)
      .w_DESATT=space(30)
      .w_DESFORN=space(40)
      .w_DESFAMA=space(35)
      .w_DESMAR=space(35)
      .w_DESGRF=space(35)
      .w_DESCAF=space(35)
      .w_DESFAMF=space(35)
      .w_DESMAF=space(35)
      .w_DESFOF=space(40)
      .w_DESCANF=space(30)
      .w_DESATF=space(30)
      .w_DESMAGI=space(30)
      .w_DESMAGF=space(30)
      .w_RIGALIS=0
      .w_CACODICE=space(20)
      .w_TIPCON=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_KEYRIF=space(10)
      .w_PPCRIELA=space(1)
        .w_ReadPar = 'AA'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ReadPar))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,3,.f.)
        .w_DimFont = 8
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
          .DoRTCalc(5,6,.f.)
        .w_ZoomEspanso = .t.
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
      .oPgFrm.Page1.oPag.ZoomPDA.Calculate()
      .oPgFrm.Page1.oPag.DettPDA.Calculate()
          .DoRTCalc(8,13,.f.)
        .w_SELART = NVL(.w_ZoomPDA.GetVar('CODART'), SPACE(20))
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_SELART))
          .link_1_25('Full')
        endif
        .w_SELVAR = .w_ZoomPDA.GetVar('CODVAR')
        .w_sCODART = .w_SELART
        .w_sCODVAR = .w_SELVAR
        .w_OBTEST = i_DATSYS
        .w_TIPGES = "G"
        .w_TIPOARTI = ''
        .w_ACTIVECOL = .w_ZoomPDA.GRD.ActiveColumn
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
          .DoRTCalc(22,22,.f.)
        .w_COLSEL = iif(.w_ZoomPDA.GRD.ActiveColumn=0,.w_COLSEL,.w_ZoomPDA.GRD.ActiveColumn)
          .DoRTCalc(24,26,.f.)
        .w_sLINKVAR = .w_sCODART
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_sLINKVAR))
          .link_1_40('Full')
        endif
          .DoRTCalc(28,28,.f.)
        .w_cPERDTF = '000'
      .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
      .oPgFrm.Page2.oPag.PeriodoPDA.Calculate()
          .DoRTCalc(30,40,.f.)
        .w_PDASEL = .w_PeriodoPDA.GetVar("PDSERIAL")
        .w_PDANUM = .w_PeriodoPDA.GetVar("PDROWNUM")
        .w_DESFOR = .w_PeriodoPDA.GetVar("ANDESCRI")
        .w_DESCAR = .w_PeriodoPDA.GetVar("CADESART")
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_sCODART))
          .link_2_15('Full')
        endif
      .oPgFrm.Page2.oPag.oObj_2_20.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_27.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_28.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_1.Calculate()
      .oPgFrm.Page3.oPag.SUGGE.Calculate('      1234567890     ',.w_TESSUG,.w_BASSUG)
      .oPgFrm.Page3.oPag.oObj_3_5.Calculate('      1234567890     ',.w_TESCON,.w_BASCON)
      .oPgFrm.Page3.oPag.oObj_3_7.Calculate('      1234567890     ',.w_TESMIS,.w_BASMIS)
        .DoRTCalc(46,52,.f.)
        if not(empty(.w_OpeElab))
          .link_3_16('Full')
        endif
        .DoRTCalc(53,58,.f.)
        if not(empty(.w_OPEVER))
          .link_3_26('Full')
        endif
          .DoRTCalc(59,59,.f.)
        .w_BASSUG = mod(.w_PPCOLSUG, (rgb(255,255,255)+1))
        .w_TESSUG = int(.w_PPCOLSUG/(rgb(255,255,255)+1))
        .w_BASCON = mod(.w_PPCOLCON, (rgb(255,255,255)+1))
        .w_TESCON = int(.w_PPCOLCON/(rgb(255,255,255)+1))
        .w_BASMIS = mod(.w_PPCOLMIS, (rgb(255,255,255)+1))
        .w_TESMIS = int(.w_PPCOLMIS/(rgb(255,255,255)+1))
        .w_BASPIA = mod(.w_PPCOLPIA, (rgb(255,255,255)+1))
        .w_TESPIA = int(.w_PPCOLPIA/(rgb(255,255,255)+1))
        .w_RET = .F.
        .w_TIPATT = "A"
          .DoRTCalc(70,70,.f.)
        .w_fPROFIN = 'PF'
        .w_fSEMLAV = 'SE'
        .w_fMATPRI = 'MP'
        .DoRTCalc(74,74,.f.)
        if not(empty(.w_fDISINI))
          .link_4_7('Full')
        endif
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_fDISFIN))
          .link_4_8('Full')
        endif
        .DoRTCalc(76,76,.f.)
        if not(empty(.w_fCODFAM))
          .link_4_9('Full')
        endif
        .DoRTCalc(77,77,.f.)
        if not(empty(.w_fCODFAF))
          .link_4_10('Full')
        endif
        .w_CRIELA = .w_PPCRIELA
        .DoRTCalc(79,79,.f.)
        if not(empty(.w_fGRUMER))
          .link_4_12('Full')
        endif
        .DoRTCalc(80,80,.f.)
        if not(empty(.w_fGRUMEF))
          .link_4_13('Full')
        endif
        .DoRTCalc(81,81,.f.)
        if not(empty(.w_fCATOMO))
          .link_4_14('Full')
        endif
        .DoRTCalc(82,82,.f.)
        if not(empty(.w_fCATOMF))
          .link_4_15('Full')
        endif
        .DoRTCalc(83,83,.f.)
        if not(empty(.w_fCODMAR))
          .link_4_16('Full')
        endif
        .DoRTCalc(84,84,.f.)
        if not(empty(.w_fCODMAF))
          .link_4_17('Full')
        endif
        .DoRTCalc(85,85,.f.)
        if not(empty(.w_fMAGINI))
          .link_4_18('Full')
        endif
        .DoRTCalc(86,86,.f.)
        if not(empty(.w_fMAGFIN))
          .link_4_19('Full')
        endif
        .DoRTCalc(87,87,.f.)
        if not(empty(.w_fCODCOM))
          .link_4_20('Full')
        endif
        .DoRTCalc(88,88,.f.)
        if not(empty(.w_fCODCOF))
          .link_4_21('Full')
        endif
        .w_fCODATT = " "
        .DoRTCalc(89,89,.f.)
        if not(empty(.w_fCODATT))
          .link_4_22('Full')
        endif
        .w_fCODATF = .w_fCODATT
        .DoRTCalc(90,90,.f.)
        if not(empty(.w_fCODATF))
          .link_4_23('Full')
        endif
        .DoRTCalc(91,91,.f.)
        if not(empty(.w_fCODFOR))
          .link_4_24('Full')
        endif
        .DoRTCalc(92,92,.f.)
        if not(empty(.w_fCODFOF))
          .link_4_25('Full')
        endif
      .oPgFrm.Page4.oPag.ZOOMMAGA.Calculate()
          .DoRTCalc(93,93,.f.)
        .w_SELEZM = "S"
        .w_fTIPGES = 'X'
      .oPgFrm.Page4.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di elaborazione")))
          .DoRTCalc(96,113,.f.)
        .w_CACODICE = .w_ZoomPDA.GETVAR('CACODICE')
        .w_TIPCON = 'F'
          .DoRTCalc(116,116,.f.)
        .w_KEYRIF = SYS(2015)
    endwith
    this.DoRTCalc(118,118,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_22.enabled = this.oPgFrm.Page2.oPag.oBtn_2_22.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_23.enabled = this.oPgFrm.Page2.oPag.oBtn_2_23.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_24.enabled = this.oPgFrm.Page2.oPag.oBtn_2_24.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_25.enabled = this.oPgFrm.Page2.oPag.oBtn_2_25.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_2.enabled = this.oPgFrm.Page3.oPag.oBtn_3_2.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_6.enabled = this.oPgFrm.Page4.oPag.oBtn_4_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsac_kpd
      if this.w_CRIELA $ 'G-M'
        this.w_ZOOMMAGA.grd.enabled=.T.
        this.w_ZOOMMAGA.enabled=.T.
        this.w_SELEZM='S'
        this.notifyevent('w_SELEZM Changed')
      else
        this.w_ZOOMMAGA.grd.enabled=.F.
        this.w_ZOOMMAGA.enabled=.F.
        this.w_SELEZM='D'
        this.notifyevent('w_SELEZM Changed')
      endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.ZoomPDA.Calculate()
        .oPgFrm.Page1.oPag.DettPDA.Calculate()
        .DoRTCalc(2,13,.t.)
            .w_SELART = NVL(.w_ZoomPDA.GetVar('CODART'), SPACE(20))
          .link_1_25('Full')
            .w_SELVAR = .w_ZoomPDA.GetVar('CODVAR')
            .w_sCODART = .w_SELART
            .w_sCODVAR = .w_SELVAR
        .DoRTCalc(18,20,.t.)
            .w_ACTIVECOL = .w_ZoomPDA.GRD.ActiveColumn
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(22,22,.t.)
            .w_COLSEL = iif(.w_ZoomPDA.GRD.ActiveColumn=0,.w_COLSEL,.w_ZoomPDA.GRD.ActiveColumn)
        .DoRTCalc(24,26,.t.)
            .w_sLINKVAR = .w_sCODART
          .link_1_40('Full')
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page2.oPag.PeriodoPDA.Calculate()
        .DoRTCalc(28,40,.t.)
            .w_PDASEL = .w_PeriodoPDA.GetVar("PDSERIAL")
            .w_PDANUM = .w_PeriodoPDA.GetVar("PDROWNUM")
            .w_DESFOR = .w_PeriodoPDA.GetVar("ANDESCRI")
            .w_DESCAR = .w_PeriodoPDA.GetVar("CADESART")
          .link_2_15('Full')
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_27.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_28.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_1.Calculate()
        .oPgFrm.Page3.oPag.SUGGE.Calculate('      1234567890     ',.w_TESSUG,.w_BASSUG)
        .oPgFrm.Page3.oPag.oObj_3_5.Calculate('      1234567890     ',.w_TESCON,.w_BASCON)
        .oPgFrm.Page3.oPag.oObj_3_7.Calculate('      1234567890     ',.w_TESMIS,.w_BASMIS)
        .DoRTCalc(46,51,.t.)
          .link_3_16('Full')
        .DoRTCalc(53,57,.t.)
          .link_3_26('Full')
        .DoRTCalc(59,88,.t.)
        if .o_fCODCOM<>.w_fCODCOM.or. .o_fCODCOF<>.w_fCODCOF
            .w_fCODATT = " "
          .link_4_22('Full')
        endif
        if .o_fCODCOM<>.w_fCODCOM.or. .o_fCODCOF<>.w_fCODCOF
            .w_fCODATF = .w_fCODATT
          .link_4_23('Full')
        endif
        if .o_CRIELA<>.w_CRIELA
          .Calculate_VASGIKTUPU()
        endif
        if .o_SELEZM<>.w_SELEZM
          .Calculate_LOGQKNSSGF()
        endif
        .oPgFrm.Page4.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page4.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di elaborazione")))
        .DoRTCalc(91,113,.t.)
            .w_CACODICE = .w_ZoomPDA.GETVAR('CACODICE')
        * --- Area Manuale = Calculate
        * --- gsac_kpd
              .w_ACTIVECOL = .w_ZoomPDA.GRD.ActiveColumn
                if .w_ACTIVECOL <> .o_ACTIVECOL and .oPgFrm.ActivePage=1
                    .NotifyEvent('w_zoompda scrolled')			
                    .O_ACTIVECOL = .w_ACTIVECOL
                    select(.w_ZoomPDA.ccursor)
                    local l_Row
                    l_Row=recno()
                    if type('i_CurForm')='O' and !IsNull(i_CurForm)
                      if upper(i_CurForm.class) = upper(.Class)
                       .w_ZoomPDA.GRD.SetFocus()
                      endif
                    endif
                endif 
                
                if .w_CACODICE<>.o_CACODICE and .w_ZoomEspanso
                  .o_CACODICE = .w_CACODICE
                endif
                
                if .w_CACODICE<>.o_CACODICE and Not .w_ZoomEspanso
                  .o_CACODICE = .w_CACODICE
                  .NotifyEvent('SHOWDETTC')
                endif
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(115,118,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.ZoomPDA.Calculate()
        .oPgFrm.Page1.oPag.DettPDA.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page2.oPag.PeriodoPDA.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_27.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_28.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_1.Calculate()
        .oPgFrm.Page3.oPag.SUGGE.Calculate('      1234567890     ',.w_TESSUG,.w_BASSUG)
        .oPgFrm.Page3.oPag.oObj_3_5.Calculate('      1234567890     ',.w_TESCON,.w_BASCON)
        .oPgFrm.Page3.oPag.oObj_3_7.Calculate('      1234567890     ',.w_TESMIS,.w_BASMIS)
        .oPgFrm.Page4.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page4.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di elaborazione")))
    endwith
  return

  proc Calculate_GGOBXLBQED()
    with this
          * --- Condizione di editing Label e zoom
          .w_ZOOMMAGA.cZoomFile = IIF(.w_CRIELA = 'G', "GSVEGKGF" , "GSVEMKGF")
          .w_ZOOMMAGA.cCpQueryName = IIF(.w_CRIELA = 'G', "QUERY\GSVEGKGF" , "QUERY\GSVEFKGF")
    endwith
  endproc
  proc Calculate_XOHCHPGRKK()
    with this
          * --- Condizione di editing Label e zoom
          .w_LBLMAGA.Enabled = IIF(.w_CRIELA $ 'G-M', .T., .F.)
          .w_ZOOMMAGA.Enabled = IIF(.w_CRIELA $ 'G-M', .T., .F.)
          .w_ZOOMMAGA.GRD.Enabled = .w_ZOOMMAGA.Enabled
    endwith
  endproc
  proc Calculate_LRBMCECHHD()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"AGGIORNA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_LYAVVYYQCV()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"ESCI";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_VASGIKTUPU()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA
          GSAR_BFM(this;
              ,"APERTURA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,"N";
             )
          .w_ZOOMMAGA.enabled = .w_CRIELA $ 'G-M'
          .w_ZOOMMAGA.grd.enabled = .w_CRIELA $ 'G-M'
          .w_RET = .NotifyEvent("InterrogaMaga")
          .w_SELEZM = IIF(.w_CRIELA $ 'G-M' , 'S', SPACE(1) )
      if .w_CRIELA $ 'G-M'
          .w_RET = .w_ZOOMMAGA.CheckAll()
      endif
      if .w_CRIELA = 'A'
          .w_RET = .w_ZOOMMAGA.UnCheckAll()
      endif
    endwith
  endproc
  proc Calculate_LOGQKNSSGF()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA
      if .w_SELEZM='S'
          .w_RET = .w_ZOOMMAGA.checkall()
      endif
      if .w_SELEZM='D'
          .w_RET = .w_ZOOMMAGA.uncheckall()
      endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page4.oPag.ofCODCOM_4_20.enabled = this.oPgFrm.Page4.oPag.ofCODCOM_4_20.mCond()
    this.oPgFrm.Page4.oPag.ofCODCOF_4_21.enabled = this.oPgFrm.Page4.oPag.ofCODCOF_4_21.mCond()
    this.oPgFrm.Page4.oPag.ofCODATT_4_22.enabled = this.oPgFrm.Page4.oPag.ofCODATT_4_22.mCond()
    this.oPgFrm.Page4.oPag.ofCODATF_4_23.enabled = this.oPgFrm.Page4.oPag.ofCODATF_4_23.mCond()
    this.oPgFrm.Page4.oPag.oSELEZM_4_34.enabled_(this.oPgFrm.Page4.oPag.oSELEZM_4_34.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.osSCOMIN_1_41.visible=!this.oPgFrm.Page1.oPag.osSCOMIN_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.osPUNRIO_1_47.visible=!this.oPgFrm.Page1.oPag.osPUNRIO_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.osQTAMIN_1_50.visible=!this.oPgFrm.Page1.oPag.osQTAMIN_1_50.mHide()
    this.oPgFrm.Page1.oPag.osLOTRIO_1_51.visible=!this.oPgFrm.Page1.oPag.osLOTRIO_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsac_kpd
    if inlist(cEvent , 'w_SELEZM Changed' , 'w_CRIELA Changed')
        if this.w_SELEZM='S'
          Select (this.w_zoommaga.cCursor)
          this.w_RIGALIS = Recno()
          update (this.w_zoommaga.cCursor) set XCHK=1
          local m_olderr
          m_olderr=on('ERROR')
          on error =.t.
          Select (this.w_zoommaga.cCursor)
          GO this.w_RIGALIS
          on error &m_olderr
        else
          Select (this.w_zoommaga.cCursor)
          this.w_RIGALIS = Recno()
          update (this.w_zoommaga.cCursor) set XCHK=0
          local m_olderr
          m_olderr=on('ERROR')
          on error =.t.
          Select (this.w_zoommaga.cCursor)
          GO this.w_RIGALIS
          on error &m_olderr    
        endif
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomPDA.Event(cEvent)
      .oPgFrm.Page1.oPag.DettPDA.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
      .oPgFrm.Page2.oPag.PeriodoPDA.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_20.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_27.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_28.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_1.Event(cEvent)
      .oPgFrm.Page3.oPag.SUGGE.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_5.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_7.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_CRIELA Changed")
          .Calculate_GGOBXLBQED()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_CRIELA Changed") or lower(cEvent)==lower("w_ZOOMMAGA after query")
          .Calculate_XOHCHPGRKK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMMAGA menucheck") or lower(cEvent)==lower("w_ZOOMMAGA row checked") or lower(cEvent)==lower("w_ZOOMMAGA row unchecked") or lower(cEvent)==lower("w_zoommaga rowcheckall") or lower(cEvent)==lower("w_zoommaga rowuncheckall")
          .Calculate_LRBMCECHHD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_LYAVVYYQCV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Init")
          .Calculate_VASGIKTUPU()
          bRefresh=.t.
        endif
      .oPgFrm.Page4.oPag.ZOOMMAGA.Event(cEvent)
      .oPgFrm.Page4.oPag.LBLMAGA.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsac_kpd
    if upper(cEvent)='FORMLOAD'
      this.w_ZOOMMAGA.GRD.SCROLLBARS=2
    endif
    if upper(cEvent)='W_CRIELA CHANGED'
      if this.w_CRIELA $ 'G-M'
        this.w_ZOOMMAGA.enabled=.T.
        this.w_SELEZM='S'
        this.notifyevent('w_SELEZM Changed')
      else
        this.w_ZOOMMAGA.enabled=.F.
        this.w_SELEZM=' '
        this.notifyevent('w_SELEZM Changed')
      endif
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ReadPar
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadPar) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadPar)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCOLMPS,PPFONMPS,PPNUMPER,PPELAODL,PPORAODL,PPOPEELA,PPCOLSUG,PPCOLCON,PPORAMPS,PPDINVER,PPDFIVER,PPCOLMIS,PPCOLMCO,PPCOLFES,PPELAMPS,PPNMAXPE,PPCALSTA,PPCOLLAN,PPCOLPIA,PPODLMIS,PPCRIELA";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_ReadPar);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_ReadPar)
            select PPCODICE,PPCOLMPS,PPFONMPS,PPNUMPER,PPELAODL,PPORAODL,PPOPEELA,PPCOLSUG,PPCOLCON,PPORAMPS,PPDINVER,PPDFIVER,PPCOLMIS,PPCOLMCO,PPCOLFES,PPELAMPS,PPNMAXPE,PPCALSTA,PPCOLLAN,PPCOLPIA,PPODLMIS,PPCRIELA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadPar = NVL(_Link_.PPCODICE,space(2))
      this.w_LARGCOL = NVL(_Link_.PPCOLMPS,0)
      this.w_DIMFONT = NVL(_Link_.PPFONMPS,0)
      this.w_NUMPER = NVL(_Link_.PPNUMPER,0)
      this.w_DataElab = NVL(cp_ToDate(_Link_.PPELAODL),ctod("  /  /  "))
      this.w_OraElab = NVL(_Link_.PPORAODL,space(8))
      this.w_OpeElab = NVL(_Link_.PPOPEELA,0)
      this.w_PPCOLSUG = NVL(_Link_.PPCOLSUG,0)
      this.w_PPCOLCON = NVL(_Link_.PPCOLCON,0)
      this.w_ORAVER = NVL(_Link_.PPORAMPS,space(8))
      this.w_DINVER = NVL(cp_ToDate(_Link_.PPDINVER),ctod("  /  /  "))
      this.w_DFIVER = NVL(cp_ToDate(_Link_.PPDFIVER),ctod("  /  /  "))
      this.w_PPCOLMIS = NVL(_Link_.PPCOLMIS,0)
      this.w_PPCOLMCO = NVL(_Link_.PPCOLMCO,0)
      this.w_PPCOLFES = NVL(_Link_.PPCOLFES,0)
      this.w_OPEVER = NVL(_Link_.PPOPEELA,0)
      this.w_ELAVER = NVL(cp_ToDate(_Link_.PPELAMPS),ctod("  /  /  "))
      this.w_NMAXPE = NVL(_Link_.PPNMAXPE,0)
      this.w_CALSTA = NVL(_Link_.PPCALSTA,space(5))
      this.w_PPCOLLAN = NVL(_Link_.PPCOLLAN,0)
      this.w_PPCOLPIA = NVL(_Link_.PPCOLPIA,0)
      this.w_PPODLMIS = NVL(_Link_.PPODLMIS,0)
      this.w_PPCRIELA = NVL(_Link_.PPCRIELA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ReadPar = space(2)
      endif
      this.w_LARGCOL = 0
      this.w_DIMFONT = 0
      this.w_NUMPER = 0
      this.w_DataElab = ctod("  /  /  ")
      this.w_OraElab = space(8)
      this.w_OpeElab = 0
      this.w_PPCOLSUG = 0
      this.w_PPCOLCON = 0
      this.w_ORAVER = space(8)
      this.w_DINVER = ctod("  /  /  ")
      this.w_DFIVER = ctod("  /  /  ")
      this.w_PPCOLMIS = 0
      this.w_PPCOLMCO = 0
      this.w_PPCOLFES = 0
      this.w_OPEVER = 0
      this.w_ELAVER = ctod("  /  /  ")
      this.w_NMAXPE = 0
      this.w_CALSTA = space(5)
      this.w_PPCOLLAN = 0
      this.w_PPCOLPIA = 0
      this.w_PPODLMIS = 0
      this.w_PPCRIELA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadPar Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELART
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPGES";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_SELART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_SELART)
            select ARCODART,ARDESART,ARTIPGES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELART = NVL(_Link_.ARCODART,space(20))
      this.w_DESCSEL = NVL(_Link_.ARDESART,space(40))
      this.w_sTIPGES = NVL(_Link_.ARTIPGES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SELART = space(20)
      endif
      this.w_DESCSEL = space(40)
      this.w_sTIPGES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=sLINKVAR
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
    i_lTable = "PAR_RIOR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2], .t., this.PAR_RIOR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_sLINKVAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_sLINKVAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODART,PRSCOMIN,PRPUNRIO,PRLOTRIO,PRQTAMIN";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODART="+cp_ToStrODBC(this.w_sLINKVAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODART',this.w_sLINKVAR)
            select PRCODART,PRSCOMIN,PRPUNRIO,PRLOTRIO,PRQTAMIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_sLINKVAR = NVL(_Link_.PRCODART,space(20))
      this.w_sSCOMIN = NVL(_Link_.PRSCOMIN,0)
      this.w_sPUNRIO = NVL(_Link_.PRPUNRIO,0)
      this.w_sLOTRIO = NVL(_Link_.PRLOTRIO,0)
      this.w_sQTAMIN = NVL(_Link_.PRQTAMIN,0)
    else
      if i_cCtrl<>'Load'
        this.w_sLINKVAR = space(20)
      endif
      this.w_sSCOMIN = 0
      this.w_sPUNRIO = 0
      this.w_sLOTRIO = 0
      this.w_sQTAMIN = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])+'\'+cp_ToStr(_Link_.PRCODART,1)
      cp_ShowWarn(i_cKey,this.PAR_RIOR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_sLINKVAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=sCODART
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_sCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_sCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_sCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_sCODART)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_sCODART = NVL(_Link_.ARCODART,space(20))
      this.w_sDESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_sCODART = space(20)
      endif
      this.w_sDESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_sCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OpeElab
  func Link_3_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OpeElab) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OpeElab)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_OpeElab);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_OpeElab)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OpeElab = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OpeElab = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OpeElab Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OPEVER
  func Link_3_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OPEVER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OPEVER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_OPEVER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_OPEVER)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OPEVER = NVL(_Link_.code,0)
      this.w_DESUTV = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OPEVER = 0
      endif
      this.w_DESUTV = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OPEVER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fDISINI
  func Link_4_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fDISINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_fDISINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_fDISINI))
          select CACODICE,CADESART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fDISINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fDISINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'ofDISINI_4_7'),i_cWhere,'GSMA_ACA',"Codici articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fDISINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_fDISINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_fDISINI)
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fDISINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_fDISINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_fDISFIN)) OR  (.w_fDISINI <= .w_fDISFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
        endif
        this.w_fDISINI = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fDISINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fDISFIN
  func Link_4_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fDISFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_fDISFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_fDISFIN))
          select CACODICE,CADESART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fDISFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fDISFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'ofDISFIN_4_8'),i_cWhere,'GSMA_ACA',"Codici articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fDISFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_fDISFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_fDISFIN)
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fDISFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_fDISFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_fDISINI <= .w_fDISFIN) or (empty(.w_fDISFIN))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
        endif
        this.w_fDISFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fDISFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFAM
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_fCODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_fCODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'ofCODFAM_4_9'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_fCODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_fCODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMA = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFAM = space(5)
      endif
      this.w_DESFAMA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODFAM<=.w_fCODFAF or empty(.w_fCODFAF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODFAM = space(5)
        this.w_DESFAMA = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFAF
  func Link_4_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_fCODFAF)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_fCODFAF))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFAF)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFAF) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'ofCODFAF_4_10'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_fCODFAF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_fCODFAF)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFAF = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFAF = space(5)
      endif
      this.w_DESFAMF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODFAM<=.w_fCODFAF or empty(.w_fCODFAM)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODFAF = space(5)
        this.w_DESFAMF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fGRUMER
  func Link_4_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_fGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_fGRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'ofGRUMER_4_12'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_fGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_fGRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fGRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fGRUMER = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fGRUMER<=.w_fGRUMEF or empty(.w_fGRUMEF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fGRUMER = space(5)
        this.w_DESGRU = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fGRUMEF
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fGRUMEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_fGRUMEF)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_fGRUMEF))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fGRUMEF)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fGRUMEF) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'ofGRUMEF_4_13'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fGRUMEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_fGRUMEF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_fGRUMEF)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fGRUMEF = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fGRUMEF = space(5)
      endif
      this.w_DESGRF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fGRUMER<=.w_fGRUMEF or empty(.w_fGRUMER)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fGRUMEF = space(5)
        this.w_DESGRF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fGRUMEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCATOMO
  func Link_4_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCATOMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_fCATOMO)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_fCATOMO))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCATOMO)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCATOMO) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'ofCATOMO_4_14'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCATOMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_fCATOMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_fCATOMO)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCATOMO = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCATOMO = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCATOMO<=.w_fCATOMF or empty(.w_fCATOMF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCATOMO = space(5)
        this.w_DESCAT = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCATOMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCATOMF
  func Link_4_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCATOMF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_fCATOMF)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_fCATOMF))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCATOMF)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCATOMF) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'ofCATOMF_4_15'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCATOMF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_fCATOMF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_fCATOMF)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCATOMF = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCATOMF = space(5)
      endif
      this.w_DESCAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCATOMO<=.w_fCATOMF or empty(.w_fCATOMO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCATOMF = space(5)
        this.w_DESCAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCATOMF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODMAR
  func Link_4_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_fCODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_fCODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'ofCODMAR_4_16'),i_cWhere,'',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_fCODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_fCODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODMAR<=.w_fCODMAF or empty(.w_fCODMAF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODMAR = space(5)
        this.w_DESMAR = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODMAF
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODMAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_fCODMAF)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_fCODMAF))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODMAF)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODMAF) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'ofCODMAF_4_17'),i_cWhere,'',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODMAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_fCODMAF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_fCODMAF)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODMAF = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAF = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCODMAF = space(5)
      endif
      this.w_DESMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODMAR<=.w_fCODMAF or empty(.w_fCODMAR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODMAF = space(5)
        this.w_DESMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODMAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fMAGINI
  func Link_4_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fMAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_fMAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_fMAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fMAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fMAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'ofMAGINI_4_18'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fMAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_fMAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_fMAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fMAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fMAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fMAGINI <= .w_fMAGFIN OR EMPTY(.w_fMAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fMAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fMAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fMAGFIN
  func Link_4_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fMAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_fMAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_fMAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fMAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fMAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'ofMAGFIN_4_19'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fMAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_fMAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_fMAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fMAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fMAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fMAGINI <= .w_fMAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fMAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fMAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODCOM
  func Link_4_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_fCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_fCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_fCODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_fCODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_fCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'ofCODCOM_4_20'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_fCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_fCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fCODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODCOM<=.w_fCODCOF or empty(.w_fCODCOF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODCOM = space(15)
        this.w_DESCAN = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODCOF
  func Link_4_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODCOF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_fCODCOF)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_fCODCOF))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODCOF)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_fCODCOF)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_fCODCOF)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_fCODCOF) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'ofCODCOF_4_21'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODCOF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_fCODCOF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_fCODCOF)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODCOF = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCANF = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fCODCOF = space(15)
      endif
      this.w_DESCANF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODCOM<=.w_fCODCOF or empty(.w_fCODCOM)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODCOF = space(15)
        this.w_DESCANF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODCOF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODATT
  func Link_4_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_fCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_fCODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_fCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_fCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_fCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_fCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'ofCODATT_4_22'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_fCODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_fCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_fCODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_fCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fCODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODATT<=.w_fCODATF or empty(.w_fCODATF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODATT = space(15)
        this.w_DESATT = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODATF
  func Link_4_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODATF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_fCODATF)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_fCODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_fCODATF))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODATF)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_fCODATF)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_fCODATF)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_fCODATF) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'ofCODATF_4_23'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_fCODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODATF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_fCODATF);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_fCODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_fCODATF)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODATF = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATF = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fCODATF = space(15)
      endif
      this.w_DESATF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODATT<=.w_fCODATF or empty(.w_fCODATT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODATF = space(15)
        this.w_DESATF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODATF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFOR
  func Link_4_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_fCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_fCODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'ofCODFOR_4_24'),i_cWhere,'',"Elenco fornitori",'gsco1kcs.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_fCODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_fCODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFORN = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFOR = space(15)
      endif
      this.w_DESFORN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_fCODFOR) AND .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente")
        endif
        this.w_fCODFOR = space(15)
        this.w_DESFORN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFOF
  func Link_4_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFOF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_fCODFOF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_fCODFOF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFOF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFOF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'ofCODFOF_4_25'),i_cWhere,'',"Elenco fornitori",'gsco1kcs.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFOF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_fCODFOF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_fCODFOF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFOF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFOF = space(15)
      endif
      this.w_DESFOF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_fCODFOF) AND .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente")
        endif
        this.w_fCODFOF = space(15)
        this.w_DESFOF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFOF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDESCSEL_1_23.value==this.w_DESCSEL)
      this.oPgFrm.Page1.oPag.oDESCSEL_1_23.value=this.w_DESCSEL
    endif
    if not(this.oPgFrm.Page1.oPag.osTIPGES_1_36.RadioValue()==this.w_sTIPGES)
      this.oPgFrm.Page1.oPag.osTIPGES_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.osSCOMIN_1_41.value==this.w_sSCOMIN)
      this.oPgFrm.Page1.oPag.osSCOMIN_1_41.value=this.w_sSCOMIN
    endif
    if not(this.oPgFrm.Page1.oPag.osPUNRIO_1_47.value==this.w_sPUNRIO)
      this.oPgFrm.Page1.oPag.osPUNRIO_1_47.value=this.w_sPUNRIO
    endif
    if not(this.oPgFrm.Page1.oPag.osQTAMIN_1_50.value==this.w_sQTAMIN)
      this.oPgFrm.Page1.oPag.osQTAMIN_1_50.value=this.w_sQTAMIN
    endif
    if not(this.oPgFrm.Page1.oPag.osLOTRIO_1_51.value==this.w_sLOTRIO)
      this.oPgFrm.Page1.oPag.osLOTRIO_1_51.value=this.w_sLOTRIO
    endif
    if not(this.oPgFrm.Page2.oPag.oPSELR_2_2.value==this.w_PSELR)
      this.oPgFrm.Page2.oPag.oPSELR_2_2.value=this.w_PSELR
    endif
    if not(this.oPgFrm.Page2.oPag.oTipoPeriodo_2_4.value==this.w_TipoPeriodo)
      this.oPgFrm.Page2.oPag.oTipoPeriodo_2_4.value=this.w_TipoPeriodo
    endif
    if not(this.oPgFrm.Page2.oPag.oDIP_2_6.value==this.w_DIP)
      this.oPgFrm.Page2.oPag.oDIP_2_6.value=this.w_DIP
    endif
    if not(this.oPgFrm.Page2.oPag.oDFP_2_7.value==this.w_DFP)
      this.oPgFrm.Page2.oPag.oDFP_2_7.value=this.w_DFP
    endif
    if not(this.oPgFrm.Page2.oPag.oPDASEL_2_11.value==this.w_PDASEL)
      this.oPgFrm.Page2.oPag.oPDASEL_2_11.value=this.w_PDASEL
    endif
    if not(this.oPgFrm.Page2.oPag.oPDANUM_2_12.value==this.w_PDANUM)
      this.oPgFrm.Page2.oPag.oPDANUM_2_12.value=this.w_PDANUM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFOR_2_13.value==this.w_DESFOR)
      this.oPgFrm.Page2.oPag.oDESFOR_2_13.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAR_2_14.value==this.w_DESCAR)
      this.oPgFrm.Page2.oPag.oDESCAR_2_14.value=this.w_DESCAR
    endif
    if not(this.oPgFrm.Page2.oPag.osCODART_2_15.value==this.w_sCODART)
      this.oPgFrm.Page2.oPag.osCODART_2_15.value=this.w_sCODART
    endif
    if not(this.oPgFrm.Page2.oPag.osDESART_2_16.value==this.w_sDESART)
      this.oPgFrm.Page2.oPag.osDESART_2_16.value=this.w_sDESART
    endif
    if not(this.oPgFrm.Page3.oPag.oDataElab_3_13.value==this.w_DataElab)
      this.oPgFrm.Page3.oPag.oDataElab_3_13.value=this.w_DataElab
    endif
    if not(this.oPgFrm.Page3.oPag.oOraElab_3_14.value==this.w_OraElab)
      this.oPgFrm.Page3.oPag.oOraElab_3_14.value=this.w_OraElab
    endif
    if not(this.oPgFrm.Page3.oPag.oOpeElab_3_16.value==this.w_OpeElab)
      this.oPgFrm.Page3.oPag.oOpeElab_3_16.value=this.w_OpeElab
    endif
    if not(this.oPgFrm.Page3.oPag.oDESUTE_3_17.value==this.w_DESUTE)
      this.oPgFrm.Page3.oPag.oDESUTE_3_17.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page4.oPag.ofPROFIN_4_3.RadioValue()==this.w_fPROFIN)
      this.oPgFrm.Page4.oPag.ofPROFIN_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofSEMLAV_4_4.RadioValue()==this.w_fSEMLAV)
      this.oPgFrm.Page4.oPag.ofSEMLAV_4_4.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofMATPRI_4_5.RadioValue()==this.w_fMATPRI)
      this.oPgFrm.Page4.oPag.ofMATPRI_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofDISINI_4_7.value==this.w_fDISINI)
      this.oPgFrm.Page4.oPag.ofDISINI_4_7.value=this.w_fDISINI
    endif
    if not(this.oPgFrm.Page4.oPag.ofDISFIN_4_8.value==this.w_fDISFIN)
      this.oPgFrm.Page4.oPag.ofDISFIN_4_8.value=this.w_fDISFIN
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFAM_4_9.value==this.w_fCODFAM)
      this.oPgFrm.Page4.oPag.ofCODFAM_4_9.value=this.w_fCODFAM
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFAF_4_10.value==this.w_fCODFAF)
      this.oPgFrm.Page4.oPag.ofCODFAF_4_10.value=this.w_fCODFAF
    endif
    if not(this.oPgFrm.Page4.oPag.oCRIELA_4_11.RadioValue()==this.w_CRIELA)
      this.oPgFrm.Page4.oPag.oCRIELA_4_11.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofGRUMER_4_12.value==this.w_fGRUMER)
      this.oPgFrm.Page4.oPag.ofGRUMER_4_12.value=this.w_fGRUMER
    endif
    if not(this.oPgFrm.Page4.oPag.ofGRUMEF_4_13.value==this.w_fGRUMEF)
      this.oPgFrm.Page4.oPag.ofGRUMEF_4_13.value=this.w_fGRUMEF
    endif
    if not(this.oPgFrm.Page4.oPag.ofCATOMO_4_14.value==this.w_fCATOMO)
      this.oPgFrm.Page4.oPag.ofCATOMO_4_14.value=this.w_fCATOMO
    endif
    if not(this.oPgFrm.Page4.oPag.ofCATOMF_4_15.value==this.w_fCATOMF)
      this.oPgFrm.Page4.oPag.ofCATOMF_4_15.value=this.w_fCATOMF
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODMAR_4_16.value==this.w_fCODMAR)
      this.oPgFrm.Page4.oPag.ofCODMAR_4_16.value=this.w_fCODMAR
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODMAF_4_17.value==this.w_fCODMAF)
      this.oPgFrm.Page4.oPag.ofCODMAF_4_17.value=this.w_fCODMAF
    endif
    if not(this.oPgFrm.Page4.oPag.ofMAGINI_4_18.value==this.w_fMAGINI)
      this.oPgFrm.Page4.oPag.ofMAGINI_4_18.value=this.w_fMAGINI
    endif
    if not(this.oPgFrm.Page4.oPag.ofMAGFIN_4_19.value==this.w_fMAGFIN)
      this.oPgFrm.Page4.oPag.ofMAGFIN_4_19.value=this.w_fMAGFIN
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODCOM_4_20.value==this.w_fCODCOM)
      this.oPgFrm.Page4.oPag.ofCODCOM_4_20.value=this.w_fCODCOM
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODCOF_4_21.value==this.w_fCODCOF)
      this.oPgFrm.Page4.oPag.ofCODCOF_4_21.value=this.w_fCODCOF
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODATT_4_22.value==this.w_fCODATT)
      this.oPgFrm.Page4.oPag.ofCODATT_4_22.value=this.w_fCODATT
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODATF_4_23.value==this.w_fCODATF)
      this.oPgFrm.Page4.oPag.ofCODATF_4_23.value=this.w_fCODATF
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFOR_4_24.value==this.w_fCODFOR)
      this.oPgFrm.Page4.oPag.ofCODFOR_4_24.value=this.w_fCODFOR
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFOF_4_25.value==this.w_fCODFOF)
      this.oPgFrm.Page4.oPag.ofCODFOF_4_25.value=this.w_fCODFOF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESINI_4_26.value==this.w_DESINI)
      this.oPgFrm.Page4.oPag.oDESINI_4_26.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page4.oPag.oSELEZM_4_34.RadioValue()==this.w_SELEZM)
      this.oPgFrm.Page4.oPag.oSELEZM_4_34.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofTIPGES_4_35.RadioValue()==this.w_fTIPGES)
      this.oPgFrm.Page4.oPag.ofTIPGES_4_35.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDESFIN_4_36.value==this.w_DESFIN)
      this.oPgFrm.Page4.oPag.oDESFIN_4_36.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESGRU_4_37.value==this.w_DESGRU)
      this.oPgFrm.Page4.oPag.oDESGRU_4_37.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCAT_4_38.value==this.w_DESCAT)
      this.oPgFrm.Page4.oPag.oDESCAT_4_38.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCAN_4_39.value==this.w_DESCAN)
      this.oPgFrm.Page4.oPag.oDESCAN_4_39.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESATT_4_40.value==this.w_DESATT)
      this.oPgFrm.Page4.oPag.oDESATT_4_40.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page4.oPag.oDESFORN_4_41.value==this.w_DESFORN)
      this.oPgFrm.Page4.oPag.oDESFORN_4_41.value=this.w_DESFORN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESFAMA_4_42.value==this.w_DESFAMA)
      this.oPgFrm.Page4.oPag.oDESFAMA_4_42.value=this.w_DESFAMA
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAR_4_43.value==this.w_DESMAR)
      this.oPgFrm.Page4.oPag.oDESMAR_4_43.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page4.oPag.oDESGRF_4_44.value==this.w_DESGRF)
      this.oPgFrm.Page4.oPag.oDESGRF_4_44.value=this.w_DESGRF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCAF_4_45.value==this.w_DESCAF)
      this.oPgFrm.Page4.oPag.oDESCAF_4_45.value=this.w_DESCAF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESFAMF_4_46.value==this.w_DESFAMF)
      this.oPgFrm.Page4.oPag.oDESFAMF_4_46.value=this.w_DESFAMF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAF_4_47.value==this.w_DESMAF)
      this.oPgFrm.Page4.oPag.oDESMAF_4_47.value=this.w_DESMAF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESFOF_4_48.value==this.w_DESFOF)
      this.oPgFrm.Page4.oPag.oDESFOF_4_48.value=this.w_DESFOF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCANF_4_49.value==this.w_DESCANF)
      this.oPgFrm.Page4.oPag.oDESCANF_4_49.value=this.w_DESCANF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESATF_4_50.value==this.w_DESATF)
      this.oPgFrm.Page4.oPag.oDESATF_4_50.value=this.w_DESATF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAGI_4_52.value==this.w_DESMAGI)
      this.oPgFrm.Page4.oPag.oDESMAGI_4_52.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAGF_4_53.value==this.w_DESMAGF)
      this.oPgFrm.Page4.oPag.oDESMAGF_4_53.value=this.w_DESMAGF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_fDISFIN)) OR  (.w_fDISINI <= .w_fDISFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_fDISINI))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofDISINI_4_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
          case   not(((.w_fDISINI <= .w_fDISFIN) or (empty(.w_fDISFIN))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_fDISFIN))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofDISFIN_4_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
          case   not(.w_fCODFAM<=.w_fCODFAF or empty(.w_fCODFAF))  and not(empty(.w_fCODFAM))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFAM_4_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODFAM<=.w_fCODFAF or empty(.w_fCODFAM))  and not(empty(.w_fCODFAF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFAF_4_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fGRUMER<=.w_fGRUMEF or empty(.w_fGRUMEF))  and not(empty(.w_fGRUMER))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofGRUMER_4_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fGRUMER<=.w_fGRUMEF or empty(.w_fGRUMER))  and not(empty(.w_fGRUMEF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofGRUMEF_4_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCATOMO<=.w_fCATOMF or empty(.w_fCATOMF))  and not(empty(.w_fCATOMO))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCATOMO_4_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCATOMO<=.w_fCATOMF or empty(.w_fCATOMO))  and not(empty(.w_fCATOMF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCATOMF_4_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODMAR<=.w_fCODMAF or empty(.w_fCODMAF))  and not(empty(.w_fCODMAR))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODMAR_4_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODMAR<=.w_fCODMAF or empty(.w_fCODMAR))  and not(empty(.w_fCODMAF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODMAF_4_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fMAGINI <= .w_fMAGFIN OR EMPTY(.w_fMAGFIN))  and not(empty(.w_fMAGINI))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofMAGINI_4_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fMAGINI <= .w_fMAGFIN)  and not(empty(.w_fMAGFIN))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofMAGFIN_4_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODCOM<=.w_fCODCOF or empty(.w_fCODCOF))  and (g_COMM="S" OR g_PERCAN="S")  and not(empty(.w_fCODCOM))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODCOM_4_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODCOM<=.w_fCODCOF or empty(.w_fCODCOM))  and (g_COMM="S" OR g_PERCAN="S")  and not(empty(.w_fCODCOF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODCOF_4_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODATT<=.w_fCODATF or empty(.w_fCODATF))  and (!empty(nvl(.w_fCODCOM,"")) and !empty(nvl(.w_fCODCOF,"")) and .w_fCODCOM=.w_fCODCOF)  and not(empty(.w_fCODATT))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODATT_4_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODATT<=.w_fCODATF or empty(.w_fCODATT))  and (!empty(nvl(.w_fCODCOM,"")) and !empty(nvl(.w_fCODCOF,"")) and .w_fCODCOM=.w_fCODCOF)  and not(empty(.w_fCODATF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODATF_4_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(NOT EMPTY(.w_fCODFOR) AND .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOF))  and not(empty(.w_fCODFOR))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFOR_4_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente")
          case   not(NOT EMPTY(.w_fCODFOF) AND .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOR))  and not(empty(.w_fCODFOF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFOF_4_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CRIELA = this.w_CRIELA
    this.o_fCODCOM = this.w_fCODCOM
    this.o_fCODCOF = this.w_fCODCOF
    this.o_SELEZM = this.w_SELEZM
    return

enddefine

* --- Define pages as container
define class tgsac_kpdPag1 as StdContainer
  Width  = 786
  height = 473
  stdWidth  = 786
  stdheight = 473
  resizeXpos=654
  resizeYpos=177
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_7 as cp_runprogram with uid="FGHVCXORGZ",left=304, top=606, width=97,height=29,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BPD("INTESTAZIONI")',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 28418022


  add object oBtn_1_8 as StdButton with uid="JWEXAPSWKJ",left=106, top=426, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per nascondere il dettaglio della manutenzione delle PDA";
    , HelpContextID = 208678719;
    , Caption='\<Nascondi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSAC_BPD(this.Parent.oContained,"HIDEDETT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not .w_ZOOMEspanso)
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="PWOLDYBHMJ",left=56, top=426, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il dettaglio della manutenzione delle PDA";
    , HelpContextID = 190758241;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSAC_BPD(this.Parent.oContained,"SHOWDETT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="XZCIGHXAQG",left=156, top=426, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare le PDA";
    , HelpContextID = 260645414;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      do GSAC_SPD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="DFRIDKMVRP",left=206, top=426, width=48,height=45,;
    CpPicture="bmp\verifica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la verifica temporale";
    , HelpContextID = 115745719;
    , Caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSAC_BPD(this.Parent.oContained,"VERIF_TEMP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="MJNXVZGNOX",left=736, top=426, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142262202;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="MZXVNEFMNO",left=6, top=426, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare le proposte d'acquisto";
    , HelpContextID = 27342614;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSAC_BPD(this.Parent.oContained,"Interroga")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_16 as cp_runprogram with uid="VFLVJLASUD",left=405, top=606, width=122,height=29,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BPD("SELEZIONE")',;
    cEvent = "w_zoompda selected",;
    nPag=1;
    , HelpContextID = 28418022


  add object ZoomPDA as cp_zoombox with uid="ZAZTYPHGDA",left=-1, top=2, width=788,height=369,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ART_ICOL",cZoomFile="GSAC_KPD",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSMA_AAR",bRetriveAllRows=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 28418022


  add object DettPDA as cp_zoombox with uid="MNXEYPRUED",left=-1, top=228, width=788,height=143,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="KEY_ARTI",cZoomFile="GSACDKPD",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",bRetriveAllRows=.f.,cZoomOnZoom="GSMA_ACA",;
    nPag=1;
    , HelpContextID = 28418022

  add object oDESCSEL_1_23 as StdField with uid="ZMJICRICBF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCSEL", cQueryName = "DESCSEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 242347466,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=243, Left=69, Top=377, InputMask=replicate('X',40)


  add object oObj_1_34 as cp_runprogram with uid="ALRRGZHEMA",left=180, top=606, width=120,height=29,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BPD("XSCROLL")',;
    cEvent = "w_zoompda scrolled",;
    nPag=1;
    , HelpContextID = 28418022

  add object osTIPGES_1_36 as StdRadio with uid="WDFPLHXHEP",rtseq=24,rtrep=.f.,left=319, top=401, width=154,height=15, enabled=.f.;
    , ToolTipText = "Modalit� di gestione dell'articolo";
    , cFormVar="w_sTIPGES", ButtonCount=2, bObbl=.f., nPag=1;
    , FntName="Arial", FntSize=8, FntBold=.f., FntItalic=.f., FntUnderline=.f., FntStrikeThru=.f.

    proc osTIPGES_1_36.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Fabbisogno"
      this.Buttons(1).HelpContextID = 14320678
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Fabbisogno","Arial",8,"")*FontMetric(6,"Arial",8,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Scorta"
      this.Buttons(2).HelpContextID = 14320678
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Scorta","Arial",8,"")*FontMetric(6,"Arial",8,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",15)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Modalit� di gestione dell'articolo")
      StdRadio::init()
    endproc

  func osTIPGES_1_36.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func osTIPGES_1_36.GetRadio()
    this.Parent.oContained.w_sTIPGES = this.RadioValue()
    return .t.
  endfunc

  func osTIPGES_1_36.SetRadio()
    this.Parent.oContained.w_sTIPGES=trim(this.Parent.oContained.w_sTIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_sTIPGES=='F',1,;
      iif(this.Parent.oContained.w_sTIPGES=='S',2,;
      0))
  endfunc

  add object osSCOMIN_1_41 as StdField with uid="HEQTTCUNCW",rtseq=28,rtrep=.f.,;
    cFormVar = "w_sSCOMIN", cQueryName = "sSCOMIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Scorta di sicurezza",;
    HelpContextID = 180804826,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=80, Left=565, Top=378, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  func osSCOMIN_1_41.mHide()
    with this.Parent.oContained
      return (.w_sTIPGES<>"F")
    endwith
  endfunc

  add object osPUNRIO_1_47 as StdField with uid="PZNRVXGHNV",rtseq=33,rtrep=.f.,;
    cFormVar = "w_sPUNRIO", cQueryName = "sPUNRIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 175554522,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=80, Left=565, Top=401, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func osPUNRIO_1_47.mHide()
    with this.Parent.oContained
      return (.w_sTIPGES<>"S")
    endwith
  endfunc


  add object oObj_1_49 as cp_runprogram with uid="YPMSWAPKSA",left=180, top=636, width=109,height=29,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BPD("Interroga")',;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 28418022

  add object osQTAMIN_1_50 as StdField with uid="ZFXTPYCIPR",rtseq=34,rtrep=.f.,;
    cFormVar = "w_sQTAMIN", cQueryName = "sQTAMIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� minima di riordino",;
    HelpContextID = 181653210,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=80, Left=704, Top=378, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  func osQTAMIN_1_50.mHide()
    with this.Parent.oContained
      return (.w_sTIPGES<>"F")
    endwith
  endfunc

  add object osLOTRIO_1_51 as StdField with uid="XRBWXLNSXE",rtseq=35,rtrep=.f.,;
    cFormVar = "w_sLOTRIO", cQueryName = "sLOTRIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lotto minimo di riordino",;
    HelpContextID = 175186906,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=80, Left=704, Top=401, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func osLOTRIO_1_51.mHide()
    with this.Parent.oContained
      return (not .w_sTIPGES $ "SF")
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="DRIFHZIBWN",Visible=.t., Left=3, Top=377,;
    Alignment=1, Width=61, Height=13,;
    Caption="Descrizione:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="UVFMQHJJHU",Visible=.t., Left=315, Top=378,;
    Alignment=1, Width=67, Height=13,;
    Caption="Tipo gestione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="PHRHPOBLZK",Visible=.t., Left=475, Top=378,;
    Alignment=1, Width=87, Height=13,;
    Caption="Scorta minima:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_sTIPGES<>"F")
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="WWNFWUVYDF",Visible=.t., Left=436, Top=401,;
    Alignment=1, Width=126, Height=13,;
    Caption="Punto di riordino:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_sTIPGES<>"S")
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="MHWSRXTOFJ",Visible=.t., Left=649, Top=378,;
    Alignment=1, Width=52, Height=13,;
    Caption="Qta min:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (.w_sTIPGES<>"F")
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="MKWGPPFSDF",Visible=.t., Left=674, Top=401,;
    Alignment=1, Width=27, Height=13,;
    Caption="Lotto:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (not .w_sTIPGES $ "SF")
    endwith
  endfunc
enddefine
define class tgsac_kpdPag2 as StdContainer
  Width  = 786
  height = 473
  stdWidth  = 786
  stdheight = 473
  resizeXpos=354
  resizeYpos=293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPSELR_2_2 as StdField with uid="YMPNOEGIMB",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PSELR", cQueryName = "PSELR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 58310410,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=135, Top=11, InputMask=replicate('X',4)

  add object oTipoPeriodo_2_4 as StdField with uid="EGMTBXGWAT",rtseq=37,rtrep=.f.,;
    cFormVar = "w_TipoPeriodo", cQueryName = "TipoPeriodo",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 26436239,;
   bGlobalFont=.t.,;
    Height=21, Width=96, Left=264, Top=11, InputMask=replicate('X',20)

  add object oDIP_2_6 as StdField with uid="TJYYOXSNGV",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DIP", cQueryName = "DIP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 149232074,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=491, Top=11

  add object oDFP_2_7 as StdField with uid="PNWHUGLTWK",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DFP", cQueryName = "DFP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 149232842,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=663, Top=11


  add object PeriodoPDA as cp_zoombox with uid="PNXWMALQTO",left=-1, top=32, width=787,height=339,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='PDA_DETT',cZoomFile='GSAC_KPD',bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t., bQueryOnLoad=.f.,;
    cEvent = "ListaPDA",;
    nPag=2;
    , HelpContextID = 28418022

  add object oPDASEL_2_11 as StdField with uid="EKOGKUYEAP",rtseq=41,rtrep=.f.,;
    cFormVar = "w_PDASEL", cQueryName = "PDASEL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "PDA selezionata",;
    HelpContextID = 129823222,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=65, Top=379, InputMask=replicate('X',10)

  add object oPDANUM_2_12 as StdField with uid="GKFHCZGVWQ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_PDANUM", cQueryName = "PDANUM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "PDA selezionata",;
    HelpContextID = 163049974,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=155, Top=379, cSayPict='"@Z 99999999"', cGetPict='"@Z 99999999"'

  add object oDESFOR_2_13 as StdField with uid="NTUXJJGYER",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione fornitore",;
    HelpContextID = 240194102,;
   bGlobalFont=.t.,;
    Height=21, Width=225, Left=289, Top=378, InputMask=replicate('X',40)

  add object oDESCAR_2_14 as StdField with uid="XBQNAPQGUE",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESCAR", cQueryName = "DESCAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 225317430,;
   bGlobalFont=.t.,;
    Height=21, Width=217, Left=565, Top=377, InputMask=replicate('X',40)

  add object osCODART_2_15 as StdField with uid="IRUFFAGDWV",rtseq=45,rtrep=.f.,;
    cFormVar = "w_sCODART", cQueryName = "sCODART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 225366822,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=65, Top=401, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_sCODART"

  func osCODART_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object osDESART_2_16 as StdField with uid="WZIVYZVZFE",rtseq=46,rtrep=.f.,;
    cFormVar = "w_sDESART", cQueryName = "sDESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 226309158,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=225, Top=401, InputMask=replicate('X',40)


  add object oObj_2_20 as cp_runprogram with uid="IYACFXDFAY",left=4, top=491, width=113,height=34,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BPD("VISUALIZZA_PDA")',;
    cEvent = "w_periodopda selected",;
    nPag=2;
    , HelpContextID = 28418022


  add object oBtn_2_22 as StdButton with uid="HZFZLLGEOC",left=365, top=427, width=48,height=45,;
    CpPicture="bmp\carica.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per caricare una nuova PDA";
    , HelpContextID = 246816294;
    , Caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_22.Click()
      with this.Parent.oContained
        GSAC_BPD(this.Parent.oContained,"CARICA_PDA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_23 as StdButton with uid="SOFRLFQUUN",left=415, top=427, width=48,height=45,;
    CpPicture="bmp\modifica.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per modificare la PDA selezionata";
    , HelpContextID = 40493226;
    , Caption='\<Varia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_23.Click()
      with this.Parent.oContained
        GSAC_BPD(this.Parent.oContained,"MODIFICA_PDA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_24 as StdButton with uid="VTFRAXPECG",left=466, top=427, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per cancellare la PDA selezionata";
    , HelpContextID = 203004230;
    , Caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_24.Click()
      with this.Parent.oContained
        GSAC_BPD(this.Parent.oContained,"ELIMINA_PDA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_25 as StdButton with uid="LCIZCVPRLY",left=734, top=427, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142262202;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_2_27 as cp_runprogram with uid="RYQERVKBYT",left=197, top=493, width=147,height=34,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BPD("MENU_LISTAPDA")',;
    cEvent = "w_periodopda MouseRightClick",;
    nPag=2;
    , HelpContextID = 28418022


  add object oObj_2_28 as cp_runprogram with uid="XTWVVRKIEH",left=347, top=493, width=113,height=34,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BPD("SELEZIONE")',;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 28418022

  add object oStr_2_1 as StdString with uid="NTHSJZBRHI",Visible=.t., Left=6, Top=11,;
    Alignment=1, Width=125, Height=15,;
    Caption="Periodo selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="ARJXDDJZOC",Visible=.t., Left=187, Top=11,;
    Alignment=1, Width=73, Height=15,;
    Caption="Tipo periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="VRUVMCGBHH",Visible=.t., Left=371, Top=11,;
    Alignment=1, Width=116, Height=15,;
    Caption="Data inizio periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="IEGZRHZMDE",Visible=.t., Left=575, Top=11,;
    Alignment=1, Width=84, Height=15,;
    Caption="Fine periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="JUQHMHSQPX",Visible=.t., Left=9, Top=381,;
    Alignment=1, Width=53, Height=15,;
    Caption="PDA sel.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="OUCAOLKBQT",Visible=.t., Left=9, Top=402,;
    Alignment=1, Width=53, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="MHKMHUBWHK",Visible=.t., Left=225, Top=379,;
    Alignment=1, Width=61, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="APCMEFFRJL",Visible=.t., Left=520, Top=378,;
    Alignment=1, Width=43, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsac_kpdPag3 as StdContainer
  Width  = 786
  height = 473
  stdWidth  = 786
  stdheight = 473
  resizeXpos=653
  resizeYpos=354
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_3_1 as cp_zoombox with uid="ETYLMDKPKI",left=522, top=64, width=243,height=399,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='PDA_TPER',cZoomFile='GSAC_KPD',bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "Periodi",;
    nPag=3;
    , HelpContextID = 28418022


  add object oBtn_3_2 as StdButton with uid="SZYOXXWAPH",left=717, top=12, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per visualizzare il dettaglio dei periodi elaborati";
    , HelpContextID = 227138826;
    , Caption='\<Periodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_2.Click()
      with this.Parent.oContained
        .notifyevent("Periodi")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object SUGGE as cp_calclbl with uid="WHVDTETLZN",left=159, top=59, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=' ',;
    nPag=3;
    , HelpContextID = 28418022


  add object oObj_3_5 as cp_calclbl with uid="HLVWJHTWKZ",left=159, top=92, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=' ',;
    nPag=3;
    , HelpContextID = 28418022


  add object oObj_3_7 as cp_calclbl with uid="DMQIBOGZUI",left=159, top=125, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=' ',;
    nPag=3;
    , HelpContextID = 28418022

  add object oDataElab_3_13 as StdField with uid="BWKTNTTGQI",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DataElab", cQueryName = "DataElab",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 130956952,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=160, Top=224

  add object oOraElab_3_14 as StdField with uid="VIMUIKMHJN",rtseq=51,rtrep=.f.,;
    cFormVar = "w_OraElab", cQueryName = "OraElab",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 253829094,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=287, Top=224, InputMask=replicate('X',8)

  add object oOpeElab_3_16 as StdField with uid="TQRJQSRHEA",rtseq=52,rtrep=.f.,;
    cFormVar = "w_OpeElab", cQueryName = "OpeElab",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 253844966,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=160, Top=256, cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_OpeElab"

  func oOpeElab_3_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESUTE_3_17 as StdField with uid="BEUOBZGTYM",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 28316214,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=203, Top=256, InputMask=replicate('X',20)

  add object oStr_3_4 as StdString with uid="FUNXGYIOVV",Visible=.t., Left=6, Top=60,;
    Alignment=1, Width=148, Height=15,;
    Caption="Solo PDA suggerite:"  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="OTILXVZAEX",Visible=.t., Left=6, Top=93,;
    Alignment=1, Width=148, Height=15,;
    Caption="Solo PDA confermate:"  ;
  , bGlobalFont=.t.

  add object oStr_3_8 as StdString with uid="JFSHBDTAYD",Visible=.t., Left=6, Top=125,;
    Alignment=1, Width=148, Height=15,;
    Caption="Composizione mista:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="SWLKCTBDXQ",Visible=.t., Left=11, Top=27,;
    Alignment=0, Width=215, Height=15,;
    Caption="Legenda colori"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="OJEBVAAFOC",Visible=.t., Left=523, Top=47,;
    Alignment=0, Width=92, Height=15,;
    Caption="Tabella periodi"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="YFSCDXUUQD",Visible=.t., Left=6, Top=224,;
    Alignment=1, Width=148, Height=15,;
    Caption="Data elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="SWERSJCGDZ",Visible=.t., Left=251, Top=224,;
    Alignment=1, Width=32, Height=15,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="UOSSMXOEXK",Visible=.t., Left=6, Top=256,;
    Alignment=1, Width=148, Height=15,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="EJSPQNGPSB",Visible=.t., Left=11, Top=193,;
    Alignment=0, Width=215, Height=15,;
    Caption="Parametri generazione PDA"  ;
  , bGlobalFont=.t.

  add object oBox_3_9 as StdBox with uid="IXGDICOKNH",left=9, top=42, width=446,height=0

  add object oBox_3_11 as StdBox with uid="IXQRTNIXHC",left=522, top=61, width=244,height=1

  add object oBox_3_24 as StdBox with uid="IHTNVXRYUH",left=9, top=208, width=446,height=0
enddefine
define class tgsac_kpdPag4 as StdContainer
  Width  = 786
  height = 473
  stdWidth  = 786
  stdheight = 473
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object ofPROFIN_4_3 as StdCheck with uid="IHWROQAYED",rtseq=71,rtrep=.f.,left=57, top=6, caption="Prodotti finiti",;
    ToolTipText = "Se attivo, considera i prodotti finiti",;
    HelpContextID = 188084394,;
    cFormVar="w_fPROFIN", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func ofPROFIN_4_3.RadioValue()
    return(iif(this.value =1,'PF',;
    'XX'))
  endfunc
  func ofPROFIN_4_3.GetRadio()
    this.Parent.oContained.w_fPROFIN = this.RadioValue()
    return .t.
  endfunc

  func ofPROFIN_4_3.SetRadio()
    this.Parent.oContained.w_fPROFIN=trim(this.Parent.oContained.w_fPROFIN)
    this.value = ;
      iif(this.Parent.oContained.w_fPROFIN=='PF',1,;
      0)
  endfunc

  add object ofSEMLAV_4_4 as StdCheck with uid="KTWFDIHQED",rtseq=72,rtrep=.f.,left=210, top=6, caption="Semilavorati",;
    ToolTipText = "Se attivo, considera i semilavorati",;
    HelpContextID = 220676694,;
    cFormVar="w_fSEMLAV", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func ofSEMLAV_4_4.RadioValue()
    return(iif(this.value =1,'SE',;
    'XX'))
  endfunc
  func ofSEMLAV_4_4.GetRadio()
    this.Parent.oContained.w_fSEMLAV = this.RadioValue()
    return .t.
  endfunc

  func ofSEMLAV_4_4.SetRadio()
    this.Parent.oContained.w_fSEMLAV=trim(this.Parent.oContained.w_fSEMLAV)
    this.value = ;
      iif(this.Parent.oContained.w_fSEMLAV=='SE',1,;
      0)
  endfunc

  add object ofMATPRI_4_5 as StdCheck with uid="SHIEYEJPCW",rtseq=73,rtrep=.f.,left=363, top=6, caption="Materie prime",;
    ToolTipText = "Se attivo, considera le materie prime",;
    HelpContextID = 26346410,;
    cFormVar="w_fMATPRI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func ofMATPRI_4_5.RadioValue()
    return(iif(this.value =1,'MP',;
    'XX'))
  endfunc
  func ofMATPRI_4_5.GetRadio()
    this.Parent.oContained.w_fMATPRI = this.RadioValue()
    return .t.
  endfunc

  func ofMATPRI_4_5.SetRadio()
    this.Parent.oContained.w_fMATPRI=trim(this.Parent.oContained.w_fMATPRI)
    this.value = ;
      iif(this.Parent.oContained.w_fMATPRI=='MP',1,;
      0)
  endfunc


  add object oBtn_4_6 as StdButton with uid="THRFVLAJCD",left=732, top=9, width=48,height=45,;
    CpPicture="BMP\REQUERY.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per visualizzare il piano principale di produzione";
    , HelpContextID = 209801750;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_6.Click()
      with this.Parent.oContained
        GSAC_BPD(this.Parent.oContained,"Interroga")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object ofDISINI_4_7 as StdField with uid="KHKMPFSALM",rtseq=74,rtrep=.f.,;
    cFormVar = "w_fDISINI", cQueryName = "fDISINI",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale",;
    ToolTipText = "Codice articolo iniziale",;
    HelpContextID = 100830378,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=124, Top=29, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_fDISINI"

  func ofDISINI_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_7('Part',this)
    endwith
    return bRes
  endfunc

  proc ofDISINI_4_7.ecpDrop(oSource)
    this.Parent.oContained.link_4_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofDISINI_4_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'ofDISINI_4_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici articoli",'',this.parent.oContained
  endproc
  proc ofDISINI_4_7.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_fDISINI
     i_obj.ecpSave()
  endproc

  add object ofDISFIN_4_8 as StdField with uid="FVVRYKENZQ",rtseq=75,rtrep=.f.,;
    cFormVar = "w_fDISFIN", cQueryName = "fDISFIN",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale",;
    ToolTipText = "Codice articolo finale",;
    HelpContextID = 187862186,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=124, Top=53, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_fDISFIN"

  proc ofDISFIN_4_8.mDefault
    with this.Parent.oContained
      if empty(.w_fDISFIN)
        .w_fDISFIN = .w_fDISINI
      endif
    endwith
  endproc

  func ofDISFIN_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_8('Part',this)
    endwith
    return bRes
  endfunc

  proc ofDISFIN_4_8.ecpDrop(oSource)
    this.Parent.oContained.link_4_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofDISFIN_4_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'ofDISFIN_4_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici articoli",'',this.parent.oContained
  endproc
  proc ofDISFIN_4_8.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_fDISFIN
     i_obj.ecpSave()
  endproc

  add object ofCODFAM_4_9 as StdField with uid="XGZLMVEIRB",rtseq=76,rtrep=.f.,;
    cFormVar = "w_fCODFAM", cQueryName = "fCODFAM",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 54603178,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=77, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_fCODFAM"

  func ofCODFAM_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_9('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFAM_4_9.ecpDrop(oSource)
    this.Parent.oContained.link_4_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFAM_4_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'ofCODFAM_4_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object ofCODFAF_4_10 as StdField with uid="XFEHNCKXQA",rtseq=77,rtrep=.f.,;
    cFormVar = "w_fCODFAF", cQueryName = "fCODFAF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 213832278,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=101, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_fCODFAF"

  proc ofCODFAF_4_10.mDefault
    with this.Parent.oContained
      if empty(.w_fCODFAF)
        .w_fCODFAF = .w_fCODFAM
      endif
    endwith
  endproc

  func ofCODFAF_4_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_10('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFAF_4_10.ecpDrop(oSource)
    this.Parent.oContained.link_4_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFAF_4_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'ofCODFAF_4_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc


  add object oCRIELA_4_11 as StdCombo with uid="VSDAZQQBWZ",rtseq=78,rtrep=.f.,left=622,top=77,width=157,height=21;
    , ToolTipText = "Criterio di pianificazione";
    , HelpContextID = 48267482;
    , cFormVar="w_CRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCRIELA_4_11.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oCRIELA_4_11.GetRadio()
    this.Parent.oContained.w_CRIELA = this.RadioValue()
    return .t.
  endfunc

  func oCRIELA_4_11.SetRadio()
    this.Parent.oContained.w_CRIELA=trim(this.Parent.oContained.w_CRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_CRIELA=='A',1,;
      iif(this.Parent.oContained.w_CRIELA=='M',2,;
      iif(this.Parent.oContained.w_CRIELA=='G',3,;
      0)))
  endfunc

  add object ofGRUMER_4_12 as StdField with uid="YCLNJRBZDM",rtseq=79,rtrep=.f.,;
    cFormVar = "w_fGRUMER", cQueryName = "fGRUMER",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico di inizio selezione",;
    HelpContextID = 20973142,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=124, Top=125, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_fGRUMER"

  func ofGRUMER_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_12('Part',this)
    endwith
    return bRes
  endfunc

  proc ofGRUMER_4_12.ecpDrop(oSource)
    this.Parent.oContained.link_4_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofGRUMER_4_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'ofGRUMER_4_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object ofGRUMEF_4_13 as StdField with uid="QRNYKYSQLT",rtseq=80,rtrep=.f.,;
    cFormVar = "w_fGRUMEF", cQueryName = "fGRUMEF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico di fine selezione",;
    HelpContextID = 20973142,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=124, Top=149, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_fGRUMEF"

  proc ofGRUMEF_4_13.mDefault
    with this.Parent.oContained
      if empty(.w_fGRUMEF)
        .w_fGRUMEF = .w_fGRUMER
      endif
    endwith
  endproc

  func ofGRUMEF_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc ofGRUMEF_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofGRUMEF_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'ofGRUMEF_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object ofCATOMO_4_14 as StdField with uid="JUJNKWFJYM",rtseq=81,rtrep=.f.,;
    cFormVar = "w_fCATOMO", cQueryName = "fCATOMO",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea di inizio selezione",;
    HelpContextID = 111283626,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=124, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_fCATOMO"

  func ofCATOMO_4_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_14('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCATOMO_4_14.ecpDrop(oSource)
    this.Parent.oContained.link_4_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCATOMO_4_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'ofCATOMO_4_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object ofCATOMF_4_15 as StdField with uid="HXLMXBDRQP",rtseq=82,rtrep=.f.,;
    cFormVar = "w_fCATOMF", cQueryName = "fCATOMF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea di fine selezione",;
    HelpContextID = 157151830,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=124, Top=197, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_fCATOMF"

  proc ofCATOMF_4_15.mDefault
    with this.Parent.oContained
      if empty(.w_fCATOMF)
        .w_fCATOMF = .w_fCATOMO
      endif
    endwith
  endproc

  func ofCATOMF_4_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_15('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCATOMF_4_15.ecpDrop(oSource)
    this.Parent.oContained.link_4_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCATOMF_4_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'ofCATOMF_4_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object ofCODMAR_4_16 as StdField with uid="NLIAPNFUHE",rtseq=83,rtrep=.f.,;
    cFormVar = "w_fCODMAR", cQueryName = "fCODMAR",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di inizio selezione",;
    HelpContextID = 221172310,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=221, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_fCODMAR"

  func ofCODMAR_4_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_16('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODMAR_4_16.ecpDrop(oSource)
    this.Parent.oContained.link_4_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODMAR_4_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'ofCODMAR_4_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Marchi",'',this.parent.oContained
  endproc

  add object ofCODMAF_4_17 as StdField with uid="VPVQKOFTGH",rtseq=84,rtrep=.f.,;
    cFormVar = "w_fCODMAF", cQueryName = "fCODMAF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di fine selezione",;
    HelpContextID = 221172310,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=245, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_fCODMAF"

  proc ofCODMAF_4_17.mDefault
    with this.Parent.oContained
      if empty(.w_fCODMAF)
        .w_fCODMAF = .w_fCODMAR
      endif
    endwith
  endproc

  func ofCODMAF_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODMAF_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODMAF_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'ofCODMAF_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Marchi",'',this.parent.oContained
  endproc

  add object ofMAGINI_4_18 as StdField with uid="SLNUVWMBLE",rtseq=85,rtrep=.f.,;
    cFormVar = "w_fMAGINI", cQueryName = "fMAGINI",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 101647274,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=269, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_fMAGINI"

  func ofMAGINI_4_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_18('Part',this)
    endwith
    return bRes
  endfunc

  proc ofMAGINI_4_18.ecpDrop(oSource)
    this.Parent.oContained.link_4_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofMAGINI_4_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'ofMAGINI_4_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object ofMAGFIN_4_19 as StdField with uid="ASPAKVFGCX",rtseq=86,rtrep=.f.,;
    cFormVar = "w_fMAGFIN", cQueryName = "fMAGFIN",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 188679082,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=293, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_fMAGFIN"

  proc ofMAGFIN_4_19.mDefault
    with this.Parent.oContained
      if empty(.w_fMAGFIN)
        .w_fMAGFIN = .w_fMAGINI
      endif
    endwith
  endproc

  func ofMAGFIN_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_19('Part',this)
    endwith
    return bRes
  endfunc

  proc ofMAGFIN_4_19.ecpDrop(oSource)
    this.Parent.oContained.link_4_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofMAGFIN_4_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'ofMAGFIN_4_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object ofCODCOM_4_20 as StdField with uid="PVVMICHNRA",rtseq=87,rtrep=.f.,;
    cFormVar = "w_fCODCOM", cQueryName = "fCODCOM",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di inizio selezione",;
    HelpContextID = 91303338,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=124, Top=317, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_fCODCOM"

  func ofCODCOM_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" OR g_PERCAN="S")
    endwith
   endif
  endfunc

  func ofCODCOM_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_20('Part',this)
      if .not. empty(.w_fCODATT)
        bRes2=.link_4_22('Full')
      endif
      if .not. empty(.w_fCODATF)
        bRes2=.link_4_23('Full')
      endif
    endwith
    return bRes
  endfunc

  proc ofCODCOM_4_20.ecpDrop(oSource)
    this.Parent.oContained.link_4_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODCOM_4_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'ofCODCOM_4_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object ofCODCOF_4_21 as StdField with uid="BJEAZWHKXG",rtseq=88,rtrep=.f.,;
    cFormVar = "w_fCODCOF", cQueryName = "fCODCOF",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di fine selezione",;
    HelpContextID = 177132118,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=124, Top=341, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_fCODCOF"

  proc ofCODCOF_4_21.mDefault
    with this.Parent.oContained
      if empty(.w_fCODCOF)
        .w_fCODCOF = .w_fCODCOM
      endif
    endwith
  endproc

  func ofCODCOF_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" OR g_PERCAN="S")
    endwith
   endif
  endfunc

  func ofCODCOF_4_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_21('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODCOF_4_21.ecpDrop(oSource)
    this.Parent.oContained.link_4_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODCOF_4_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'ofCODCOF_4_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object ofCODATT_4_22 as StdField with uid="ZIKVMSSXJZ",rtseq=89,rtrep=.f.,;
    cFormVar = "w_fCODATT", cQueryName = "fCODATT",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivita di inizio selezione",;
    HelpContextID = 258921046,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=124, Top=365, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_fCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_fCODATT"

  func ofCODATT_4_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(nvl(.w_fCODCOM,"")) and !empty(nvl(.w_fCODCOF,"")) and .w_fCODCOM=.w_fCODCOF)
    endwith
   endif
  endfunc

  func ofCODATT_4_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_22('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODATT_4_22.ecpDrop(oSource)
    this.Parent.oContained.link_4_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODATT_4_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_fCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_fCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'ofCODATT_4_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object ofCODATF_4_23 as StdField with uid="LNKODMPNRF",rtseq=90,rtrep=.f.,;
    cFormVar = "w_fCODATF", cQueryName = "fCODATF",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivita di fine selezione",;
    HelpContextID = 258921046,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=124, Top=389, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_fCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_fCODATF"

  func ofCODATF_4_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(nvl(.w_fCODCOM,"")) and !empty(nvl(.w_fCODCOF,"")) and .w_fCODCOM=.w_fCODCOF)
    endwith
   endif
  endfunc

  func ofCODATF_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_23('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODATF_4_23.ecpDrop(oSource)
    this.Parent.oContained.link_4_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODATF_4_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_fCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_fCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'ofCODATF_4_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object ofCODFOR_4_24 as StdField with uid="FRYOZPVPMZ",rtseq=91,rtrep=.f.,;
    cFormVar = "w_fCODFOR", cQueryName = "fCODFOR",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente",;
    ToolTipText = "Codice fornitore di inizio selezione",;
    HelpContextID = 180277846,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=124, Top=413, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_fCODFOR"

  func ofCODFOR_4_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_24('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFOR_4_24.ecpDrop(oSource)
    this.Parent.oContained.link_4_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFOR_4_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'ofCODFOR_4_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco fornitori",'gsco1kcs.CONTI_VZM',this.parent.oContained
  endproc

  add object ofCODFOF_4_25 as StdField with uid="HNEKKHWJXJ",rtseq=92,rtrep=.f.,;
    cFormVar = "w_fCODFOF", cQueryName = "fCODFOF",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente",;
    ToolTipText = "Codice fornitore di fine selezione",;
    HelpContextID = 180277846,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=124, Top=437, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_fCODFOF"

  proc ofCODFOF_4_25.mDefault
    with this.Parent.oContained
      if empty(.w_fCODFOF)
        .w_fCODFOF = .w_fCODFOR
      endif
    endwith
  endproc

  func ofCODFOF_4_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_25('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFOF_4_25.ecpDrop(oSource)
    this.Parent.oContained.link_4_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFOF_4_25.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'ofCODFOF_4_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco fornitori",'gsco1kcs.CONTI_VZM',this.parent.oContained
  endproc

  add object oDESINI_4_26 as StdField with uid="KNHKVOWXPR",rtseq=93,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 88347190,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=278, Top=29, InputMask=replicate('X',40)


  add object ZOOMMAGA as cp_szoombox with uid="VPMEOXAFHG",left=432, top=120, width=352,height=250,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.t.,cZoomFile="GSVEMKGF",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="MAGAZZIN",bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "InterrogaMaga",;
    nPag=4;
    , HelpContextID = 28418022

  add object oSELEZM_4_34 as StdRadio with uid="LTQWYYFJNE",rtseq=94,rtrep=.f.,left=544, top=373, width=239,height=20;
    , cFormVar="w_SELEZM", ButtonCount=2, bObbl=.f., nPag=4;
  , bGlobalFont=.t.

    proc oSELEZM_4_34.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 167748390
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 167748390
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",20)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZM_4_34.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZM_4_34.GetRadio()
    this.Parent.oContained.w_SELEZM = this.RadioValue()
    return .t.
  endfunc

  func oSELEZM_4_34.SetRadio()
    this.Parent.oContained.w_SELEZM=trim(this.Parent.oContained.w_SELEZM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZM=="S",1,;
      iif(this.Parent.oContained.w_SELEZM=="D",2,;
      0))
  endfunc

  func oSELEZM_4_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CRIELA $ 'G-M')
    endwith
   endif
  endfunc


  add object ofTIPGES_4_35 as StdCombo with uid="QFVODWEYFZ",rtseq=95,rtrep=.f.,left=670,top=412,width=109,height=21;
    , ToolTipText = "Tipo gestione articolo";
    , HelpContextID = 14320470;
    , cFormVar="w_fTIPGES",RowSource=""+"Fabbisogno,"+"Scorta,"+"Tutti", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func ofTIPGES_4_35.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func ofTIPGES_4_35.GetRadio()
    this.Parent.oContained.w_fTIPGES = this.RadioValue()
    return .t.
  endfunc

  func ofTIPGES_4_35.SetRadio()
    this.Parent.oContained.w_fTIPGES=trim(this.Parent.oContained.w_fTIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_fTIPGES=='F',1,;
      iif(this.Parent.oContained.w_fTIPGES=='S',2,;
      iif(this.Parent.oContained.w_fTIPGES=='X',3,;
      0)))
  endfunc

  add object oDESFIN_4_36 as StdField with uid="DEJRYNJSQL",rtseq=96,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 166793782,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=278, Top=53, InputMask=replicate('X',40)

  add object oDESGRU_4_37 as StdField with uid="OMNXRCVTQU",rtseq=97,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 25301558,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=176, Top=125, InputMask=replicate('X',35)

  add object oDESCAT_4_38 as StdField with uid="TWDGWFYFEP",rtseq=98,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 258871862,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=176, Top=173, InputMask=replicate('X',35)

  add object oDESCAN_4_39 as StdField with uid="PJHERXXTYJ",rtseq=99,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158208566,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=235, Top=317, InputMask=replicate('X',30)

  add object oDESATT_4_40 as StdField with uid="JRUPUPZCWV",rtseq=100,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 10228278,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=235, Top=365, InputMask=replicate('X',30)

  add object oDESFORN_4_41 as StdField with uid="QITDWTVWPI",rtseq=101,rtrep=.f.,;
    cFormVar = "w_DESFORN", cQueryName = "DESFORN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 28241354,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=278, Top=413, InputMask=replicate('X',40)

  add object oDESFAMA_4_42 as StdField with uid="AOYBYAMKXI",rtseq=102,rtrep=.f.,;
    cFormVar = "w_DESFAMA", cQueryName = "DESFAMA",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 141627958,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=187, Top=77, InputMask=replicate('X',35)

  add object oDESMAR_4_43 as StdField with uid="MXNLJGFEDS",rtseq=103,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 225972790,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=187, Top=221, InputMask=replicate('X',35)

  add object oDESGRF_4_44 as StdField with uid="AJBTWZFPAJ",rtseq=104,rtrep=.f.,;
    cFormVar = "w_DESGRF", cQueryName = "DESGRF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 42078774,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=176, Top=149, InputMask=replicate('X',35)

  add object oDESCAF_4_45 as StdField with uid="AVFSGGPYLJ",rtseq=105,rtrep=.f.,;
    cFormVar = "w_DESCAF", cQueryName = "DESCAF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 23990838,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=176, Top=197, InputMask=replicate('X',35)

  add object oDESFAMF_4_46 as StdField with uid="ETEOEUZMNA",rtseq=106,rtrep=.f.,;
    cFormVar = "w_DESFAMF", cQueryName = "DESFAMF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 141627958,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=187, Top=101, InputMask=replicate('X',35)

  add object oDESMAF_4_47 as StdField with uid="EAHDNETTTB",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DESMAF", cQueryName = "DESMAF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 24646198,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=187, Top=245, InputMask=replicate('X',35)

  add object oDESFOF_4_48 as StdField with uid="HZPHPWHVCM",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DESFOF", cQueryName = "DESFOF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38867510,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=278, Top=437, InputMask=replicate('X',40)

  add object oDESCANF_4_49 as StdField with uid="HXSGZCDNIS",rtseq=109,rtrep=.f.,;
    cFormVar = "w_DESCANF", cQueryName = "DESCANF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158208566,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=235, Top=341, InputMask=replicate('X',30)

  add object oDESATF_4_50 as StdField with uid="DQYXKCQONM",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DESATF", cQueryName = "DESATF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 43782710,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=235, Top=389, InputMask=replicate('X',30)

  add object oDESMAGI_4_52 as StdField with uid="QONUNYBHAK",rtseq=111,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 227012042,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=187, Top=269, InputMask=replicate('X',30)

  add object oDESMAGF_4_53 as StdField with uid="EBXVFVWORM",rtseq=112,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 41423414,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=187, Top=293, InputMask=replicate('X',30)


  add object LBLMAGA as cp_calclbl with uid="UIGUTRTDRQ",left=439, top=110, width=222,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=0,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,;
    nPag=4;
    , HelpContextID = 28418022

  add object oStr_4_51 as StdString with uid="TJZDCQSUZV",Visible=.t., Left=435, Top=77,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_54 as StdString with uid="CJIPWEAGWS",Visible=.t., Left=559, Top=413,;
    Alignment=1, Width=107, Height=15,;
    Caption="Tipo gestione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_56 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=3, Top=293,;
    Alignment=1, Width=118, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_57 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=3, Top=269,;
    Alignment=1, Width=118, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_58 as StdString with uid="EAPXXFINWA",Visible=.t., Left=14, Top=389,;
    Alignment=1, Width=107, Height=15,;
    Caption="A attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_4_59 as StdString with uid="XKQOJLGOQV",Visible=.t., Left=14, Top=341,;
    Alignment=1, Width=107, Height=15,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_4_60 as StdString with uid="LEZVOAVKGD",Visible=.t., Left=11, Top=437,;
    Alignment=1, Width=110, Height=15,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_61 as StdString with uid="YOAHAQTVAM",Visible=.t., Left=36, Top=245,;
    Alignment=1, Width=85, Height=15,;
    Caption="A marca:"  ;
  , bGlobalFont=.t.

  add object oStr_4_62 as StdString with uid="QCTHNSNMNF",Visible=.t., Left=18, Top=101,;
    Alignment=1, Width=103, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_4_63 as StdString with uid="RMFZDVMSDO",Visible=.t., Left=-2, Top=197,;
    Alignment=1, Width=123, Height=15,;
    Caption="A cat.omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_4_64 as StdString with uid="TRPYWGVGAK",Visible=.t., Left=11, Top=149,;
    Alignment=1, Width=110, Height=15,;
    Caption="A gr.merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_4_65 as StdString with uid="SGSTIMCHVV",Visible=.t., Left=36, Top=221,;
    Alignment=1, Width=85, Height=15,;
    Caption="Da marca:"  ;
  , bGlobalFont=.t.

  add object oStr_4_66 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=18, Top=77,;
    Alignment=1, Width=103, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_4_67 as StdString with uid="EAQXXFQMDK",Visible=.t., Left=11, Top=413,;
    Alignment=1, Width=110, Height=15,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_68 as StdString with uid="QLYZPBJZIB",Visible=.t., Left=14, Top=365,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_4_69 as StdString with uid="BKQWSHAOGE",Visible=.t., Left=14, Top=317,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_4_70 as StdString with uid="OTZSPAQGTS",Visible=.t., Left=-2, Top=173,;
    Alignment=1, Width=123, Height=15,;
    Caption="Da cat.omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_4_71 as StdString with uid="FNLHDDRZDW",Visible=.t., Left=0, Top=125,;
    Alignment=1, Width=121, Height=15,;
    Caption="Da gr.merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_4_72 as StdString with uid="GSERYQDUJG",Visible=.t., Left=14, Top=53,;
    Alignment=1, Width=107, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_4_73 as StdString with uid="QSJPSSNCLY",Visible=.t., Left=10, Top=29,;
    Alignment=1, Width=111, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_kpd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
