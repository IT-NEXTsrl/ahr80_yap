* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kpo                                                        *
*              Opzioni post in                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-01-13                                                      *
* Last revis.: 2015-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kpo",oParentObject))

* --- Class definition
define class tgsut_kpo as StdForm
  Top    = 18
  Left   = 10

  * --- Standard Properties
  Width  = 199
  Height = 130
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-30"
  HelpContextID=118930583
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kpo"
  cComment = "Opzioni post in"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TITLE = space(254)
  w_WORKINI = space(4)
  o_WORKINI = space(4)
  w_WORKFIN = space(4)
  o_WORKFIN = space(4)
  w_INTERVAL = space(4)
  o_INTERVAL = space(4)
  w_MODIFYMODE = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kpoPag1","gsut_kpo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oWORKINI_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TITLE=space(254)
      .w_WORKINI=space(4)
      .w_WORKFIN=space(4)
      .w_INTERVAL=space(4)
      .w_MODIFYMODE=.f.
      .w_TITLE=oParentObject.w_TITLE
      .w_WORKINI=oParentObject.w_WORKINI
      .w_WORKFIN=oParentObject.w_WORKFIN
      .w_INTERVAL=oParentObject.w_INTERVAL
          .DoRTCalc(1,4,.f.)
        .w_MODIFYMODE = Vartype(oGadgetManager.oCharmBar)='O' And oGadgetManager.oCharmBar.bModifyMode
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TITLE=.w_TITLE
      .oParentObject.w_WORKINI=.w_WORKINI
      .oParentObject.w_WORKFIN=.w_WORKFIN
      .oParentObject.w_INTERVAL=.w_INTERVAL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_WORKINI<>.w_WORKINI
            .w_WORKINI = Right("00"+Alltrim((Left(.w_WORKINI,At(':',.w_WORKINI)-1))),2)+":"+Right(Alltrim(("00"+Right(.w_WORKINI,At(':',.w_WORKINI)-1))),2)
        endif
        if .o_WORKFIN<>.w_WORKFIN.or. .o_WORKINI<>.w_WORKINI
            .w_WORKFIN = IIF(Empty(.w_WORKFIN) OR Val(.w_WORKINI)>Val(.w_WORKFIN), .w_WORKINI, Right("00"+Alltrim((Left(.w_WORKFIN,At(':',.w_WORKFIN)-1))),2)+":"+Right(Alltrim(("00"+Right(.w_WORKFIN,At(':',.w_WORKFIN)-1))),2))
        endif
        if .o_INTERVAL<>.w_INTERVAL
            .w_INTERVAL = Right("00"+Alltrim((Left(.w_INTERVAL,At(':',.w_INTERVAL)-1))),2)+":"+Right(Alltrim(("00"+Right(.w_INTERVAL,At(':',.w_INTERVAL)-1))),2)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .cComment = ah_msgformat("Opzioni %1", Lower(.w_TITLE))
          .Caption = .cComment
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oWORKINI_1_5.enabled = this.oPgFrm.Page1.oPag.oWORKINI_1_5.mCond()
    this.oPgFrm.Page1.oPag.oWORKFIN_1_6.enabled = this.oPgFrm.Page1.oPag.oWORKFIN_1_6.mCond()
    this.oPgFrm.Page1.oPag.oINTERVAL_1_7.enabled = this.oPgFrm.Page1.oPag.oINTERVAL_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oWORKINI_1_5.value==this.w_WORKINI)
      this.oPgFrm.Page1.oPag.oWORKINI_1_5.value=this.w_WORKINI
    endif
    if not(this.oPgFrm.Page1.oPag.oWORKFIN_1_6.value==this.w_WORKFIN)
      this.oPgFrm.Page1.oPag.oWORKFIN_1_6.value=this.w_WORKFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oINTERVAL_1_7.value==this.w_INTERVAL)
      this.oPgFrm.Page1.oPag.oINTERVAL_1_7.value=this.w_INTERVAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(!Empty(Left(.w_WORKINI,At(':',.w_WORKINI)-1)) And Val(Left(.w_WORKINI,At(':',.w_WORKINI)-1))<24 And !Empty(Right(.w_WORKINI,At(':',.w_WORKINI)-1)) And Val(Right(.w_WORKINI,At(':',.w_WORKINI)-1))<60)  and (.w_MODIFYMODE)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oWORKINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!Empty(Left(.w_WORKFIN,At(':',.w_WORKFIN)-1)) And Val(Left(.w_WORKFIN,At(':',.w_WORKFIN)-1))<24 And !Empty(Right(.w_WORKFIN,At(':',.w_WORKFIN)-1)) And Val(Right(.w_WORKFIN,At(':',.w_WORKFIN)-1))<60)  and (.w_MODIFYMODE)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oWORKFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!Empty(Left(.w_WORKFIN,At(':',.w_WORKFIN)-1)) And !Empty(Right(.w_INTERVAL,At(':',.w_INTERVAL)-1)) And Val(Right(.w_INTERVAL,At(':',.w_INTERVAL)-1))<60)  and (.w_MODIFYMODE)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINTERVAL_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_WORKINI = this.w_WORKINI
    this.o_WORKFIN = this.w_WORKFIN
    this.o_INTERVAL = this.w_INTERVAL
    return

enddefine

* --- Define pages as container
define class tgsut_kpoPag1 as StdContainer
  Width  = 195
  height = 130
  stdWidth  = 195
  stdheight = 130
  resizeXpos=189
  resizeYpos=74
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oWORKINI_1_5 as StdField with uid="PLMDJHXSTJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_WORKINI", cQueryName = "WORKINI",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 167194982,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=146, Top=5, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',4)

  func oWORKINI_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  func oWORKINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(Left(.w_WORKINI,At(':',.w_WORKINI)-1)) And Val(Left(.w_WORKINI,At(':',.w_WORKINI)-1))<24 And !Empty(Right(.w_WORKINI,At(':',.w_WORKINI)-1)) And Val(Right(.w_WORKINI,At(':',.w_WORKINI)-1))<60)
    endwith
    return bRes
  endfunc

  add object oWORKFIN_1_6 as StdField with uid="YZPPTZTLJF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_WORKFIN", cQueryName = "WORKFIN",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 188272282,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=146, Top=28, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',4)

  func oWORKFIN_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  func oWORKFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(Left(.w_WORKFIN,At(':',.w_WORKFIN)-1)) And Val(Left(.w_WORKFIN,At(':',.w_WORKFIN)-1))<24 And !Empty(Right(.w_WORKFIN,At(':',.w_WORKFIN)-1)) And Val(Right(.w_WORKFIN,At(':',.w_WORKFIN)-1))<60)
    endwith
    return bRes
  endfunc

  add object oINTERVAL_1_7 as StdField with uid="FAFEPCVPYX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_INTERVAL", cQueryName = "INTERVAL",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 226406446,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=146, Top=51, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',4)

  func oINTERVAL_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  func oINTERVAL_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(Left(.w_WORKFIN,At(':',.w_WORKFIN)-1)) And !Empty(Right(.w_INTERVAL,At(':',.w_INTERVAL)-1)) And Val(Right(.w_INTERVAL,At(':',.w_INTERVAL)-1))<60)
    endwith
    return bRes
  endfunc


  add object oBtn_1_10 as StdButton with uid="OXLJQSTOTY",left=89, top=80, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 118959334;
    , Caption=MSG_OK;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="DKNBURUTRO",left=139, top=80, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 126248006;
    , Caption=MSG_CANCEL_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="ZCMDREYYYY",Visible=.t., Left=25, Top=9,;
    Alignment=1, Width=120, Height=18,;
    Caption="Inizio orario di lavoro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="JMXZPXZCMX",Visible=.t., Left=32, Top=32,;
    Alignment=1, Width=113, Height=18,;
    Caption="Fine orario di lavoro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="TCQIGXSBUW",Visible=.t., Left=4, Top=55,;
    Alignment=1, Width=141, Height=18,;
    Caption="Durata minima impegno:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kpo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
