* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_adp                                                        *
*              Risorse umane                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_116]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-21                                                      *
* Last revis.: 2018-07-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsar_adp
PARAMETERS pTipRis
* --- Fine Area Manuale
return(createobject("tgsar_adp"))

* --- Class definition
define class tgsar_adp as StdForm
  Top    = 1
  Left   = 11

  * --- Standard Properties
  Width  = 778
  Height = 535+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-10"
  HelpContextID=175540375
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=147

  * --- Constant Properties
  DIPENDEN_IDX = 0
  CONTI_IDX = 0
  RIS_ORSE_IDX = 0
  MANSIONI_IDX = 0
  NAZIONI_IDX = 0
  SEDIAZIE_IDX = 0
  CPUSERS_IDX = 0
  KEY_ARTI_IDX = 0
  CPAR_DEF_IDX = 0
  ANAGRAFE_IDX = 0
  ANAG_PRO_IDX = 0
  BUSIUNIT_IDX = 0
  cpusers_IDX = 0
  TAB_CALE_IDX = 0
  CENCOST_IDX = 0
  DIPENDEN_IDX = 0
  ART_ICOL_IDX = 0
  PAR_AGEN_IDX = 0
  cFile = "DIPENDEN"
  cKeySelect = "DPCODICE"
  cKeyWhere  = "DPCODICE=this.w_DPCODICE"
  cKeyWhereODBC = '"DPCODICE="+cp_ToStrODBC(this.w_DPCODICE)';

  cKeyWhereODBCqualified = '"DIPENDEN.DPCODICE="+cp_ToStrODBC(this.w_DPCODICE)';

  cPrg = "gsar_adp"
  cComment = "Risorse umane"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DPCODICE = space(5)
  o_DPCODICE = space(5)
  w_DPTIPRIS = space(1)
  w_DCLMAST = space(40)
  w_DPCODANA = space(15)
  o_DPCODANA = space(15)
  w_ORIGANA = space(15)
  w_NAZION = space(3)
  w_PRONAS = space(2)
  w_PROVIN = space(2)
  w_ANAZION = space(3)
  o_ANAZION = space(3)
  w_APRONAS = space(2)
  w_APROVIN = space(2)
  w_PRONAS1 = space(2)
  w_CODAZI = space(5)
  w_PROVIN1 = space(2)
  w_DPBADGE = space(10)
  w_DPDESCRI = space(60)
  w_DPCODCEN = space(15)
  w_DPSERPRE = space(41)
  w_DPGRUPRE = space(5)
  w_DPCOSORA = 0
  w_DPCOGNOM = space(50)
  o_DPCOGNOM = space(50)
  w_DPFLVISI = space(1)
  o_DPFLVISI = space(1)
  w_DPNOME = space(50)
  o_DPNOME = space(50)
  w_DPFLINEX = space(1)
  o_DPFLINEX = space(1)
  w_DPREPART = space(20)
  w_DESRIS = space(40)
  w_DPDITTA = space(15)
  w_DPCODDES = space(5)
  w_DESCON = space(40)
  w_DPMANSION = space(5)
  w_DESMAN = space(40)
  w_DPCOSORA = 0
  w_DPCODCEN = space(15)
  w_DPCODCAL = space(5)
  w_DPMODALL = space(1)
  w_DPTIPCON = space(1)
  w_DPSERVIZ = space(41)
  w_DESSER = space(40)
  w_DPSERPRE = space(41)
  w_DPPATHPR = space(254)
  o_DPPATHPR = space(254)
  w_DP__PATH = space(254)
  o_DP__PATH = space(254)
  w_DP__DIKE = space(254)
  w_OBTEST = ctod('  /  /  ')
  w_TIPORIS = space(10)
  w_TIPRIS = space(2)
  w_DESSED = space(40)
  w_PERFIS = space(1)
  w_DPKEYPAR = space(10)
  w_DPUMDEFA = space(3)
  w_TIPART = space(1)
  w_ARTICOLO = space(20)
  w_CODANA = space(15)
  w_CODRAP = 0
  w_AZIENDA = space(5)
  w_CODSOC = space(15)
  w_CODANA = space(15)
  w_RAPDET = .F.
  w_RAPOBSO = ctod('  /  /  ')
  w_DPCODFIS = space(16)
  o_DPCODFIS = space(16)
  w_DPPARIVA = space(12)
  w_CODIFISC = space(16)
  o_CODIFISC = space(16)
  w_DPNUMCAR = space(18)
  w_DP_SESSO = space(1)
  w_DPNASLOC = space(40)
  w_DPNASPRO = space(2)
  w_DPNASNAZ = space(3)
  w_DESNAZ1 = space(35)
  w_DPDATNAS = ctod('  /  /  ')
  w_DPTITSTU = space(40)
  w_DPQUALIF = space(40)
  w_DPLIVELL = space(40)
  w_DPDATASS = ctod('  /  /  ')
  w_DPDATFRA = ctod('  /  /  ')
  w_DPRESVIA = space(35)
  w_DPINDIR2 = space(35)
  w_DPRESCAP = space(9)
  w_DPRESLOC = space(30)
  w_DPRESPRO = space(2)
  w_DPNAZION = space(3)
  w_DPDOMVIA = space(40)
  w_DPDOMCAP = space(9)
  w_DPDOMLOC = space(30)
  w_DPDOMPRO = space(2)
  w_DPTELEF1 = space(18)
  w_DPTELEF2 = space(18)
  w_DPINDMAI = space(254)
  w_DESNAZ = space(35)
  w_CODISO = space(3)
  w_DPNUMCEL = space(18)
  w_DPNATGIU = space(10)
  w_DPPERFIS = space(1)
  w_DPINDPEC = space(254)
  w_DPCODUTE = 0
  o_DPCODUTE = 0
  w_DESUTE = space(20)
  w_DPCHKING = space(1)
  w_DPDATINI = ctod('  /  /  ')
  w_DPGGBLOC = 0
  w_DP_FIRMA = space(200)
  w_DPASSDAL = ctod('  /  /  ')
  w_DPASSFIN = ctod('  /  /  ')
  w_DPCODSOS = space(5)
  w_COGNSOS = space(40)
  w_DPCDUTAL = 0
  w_DPFLCONF = space(1)
  w_DPCODBUN = space(3)
  w_FLANAL = space(1)
  w_DP__NOTE = space(0)
  w_ERR = .F.
  w_DPCODUTE = 0
  w_DESUTE = space(20)
  w_DP__NOTE = space(0)
  w_DPFLAVVI = space(1)
  w_DPINDWEB = space(50)
  w_DPCODCAL = space(5)
  w_CCDESPIA = space(40)
  w_TCDESCRI = space(40)
  w_FLPERSONA = space(1)
  w_DPDESCRI2 = space(60)
  w_CCDESPIA = space(40)
  w_DPDESGRU = space(40)
  w_TIPGRU = space(1)
  w_TCDESCRI = space(40)
  w_OLDFLVISI = space(1)
  w_DESPRE = space(40)
  w_DESPRE = space(40)
  w_TIPAR1 = space(1)
  w_DPDTOBSO = ctod('  /  /  ')
  w_OB_TEST = ctod('  /  /  ')
  w_CODCOM = space(4)
  w_CHKVARVI = .F.
  w_COGNRES = space(10)
  w_NOMERES = space(10)
  w_DPVCSICS = space(1)
  w_LPATH = .F.
  w_CONTA = 0
  o_CONTA = 0
  w_LetturaParAgen = space(5)
  o_LetturaParAgen = space(5)
  w_DPRSFASC = space(1)
  w_DPCODRES = space(5)
  o_DPCODRES = space(5)
  w_DPPERFAT = 0
  o_DPPERFAT = 0
  w_DPPERFAC = 0
  w_DPTIPRIG = space(1)
  o_DPTIPRIG = space(1)
  w_DENOMRES = space(40)
  w_DPCTRPRE = space(1)
  w_DPGG_PRE = 0
  w_DPGG_SUC = 0
  w_PAFLPRES = space(1)
  w_DPFLPROM = space(1)

  * --- Children pointers
  GSAR_MPH = .NULL.
  GSAR_ARD = .NULL.
  GSAR_MBR = .NULL.
  GSAR_MSR = .NULL.
  GSAR_MPD = .NULL.
  w_LBLVAL = .NULL.
  w_FIRMAIMG = .NULL.
  w_BTNQRY = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_adp
  cTipRis = 'P'
  cTipgES = 'N'
  w_cTipRis = 'P'
  w_PaginaNote = 0
  w_BckpForClr = 0
  
  * --- Per gestione sicurezza procedure con parametri
    func getSecuritycode()
    return(lower('GSAR_ADP,' + This.cTIPRIS))
    EndFunc
    
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=8, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIPENDEN','gsar_adp')
    stdPageFrame::Init()
    *set procedure to GSAR_MPH additive
    *set procedure to GSAR_ARD additive
    *set procedure to GSAR_MSR additive
    with this
      .Pages(1).addobject("oPag","tgsar_adpPag1","gsar_adp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generali")
      .Pages(1).HelpContextID = 217918769
      .Pages(2).addobject("oPag","tgsar_adpPag2","gsar_adp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Anagrafici")
      .Pages(2).HelpContextID = 50625183
      .Pages(3).addobject("oPag","tgsar_adpPag3","gsar_adp",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Recapiti")
      .Pages(3).HelpContextID = 182330239
      .Pages(4).addobject("oPag","tgsar_adpPag4","gsar_adp",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Identificazione")
      .Pages(4).HelpContextID = 9193759
      .Pages(5).addobject("oPag","tgsar_adpPag5","gsar_adp",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Budget")
      .Pages(5).HelpContextID = 181124586
      .Pages(6).addobject("oPag","tgsar_adpPag6","gsar_adp",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Note")
      .Pages(6).HelpContextID = 182664406
      .Pages(7).addobject("oPag","tgsar_adpPag7","gsar_adp",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Prestazioni")
      .Pages(7).HelpContextID = 53957967
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDPCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAR_MPH
    *release procedure GSAR_ARD
    *release procedure GSAR_MSR
    * --- Area Manuale = Init Page Frame
    * --- gsar_adp
    WITH THIS.PARENT
       IF LEN(pTipRis) = 2 
         .cTipgES = Left(pTipRis,1)
         .cTipRis = Right(pTipRis,1)
       else
         .cTipRis = pTipRis
       endif
       .w_cTipRis = .cTipRis
       .cQueryFilter = iif(empty(nvl(.cQueryFilter,'')),"DPTIPRIS='"+.cTipRis+"'","("+.cQueryFilter+ ") AND DPTIPRIS='"+.cTipRis+"'")
       do case
         case .cTipRis=='P'
           .cComment = AH_MSGFORMAT('Persone')
           .cAutoZoom = "GSARTADP"
         case .cTipRis=='R'
           .cComment = AH_MSGFORMAT('Risorse')
           .cAutoZoom = "GSARRADP"
         case .cTipRis=='G'
           .cComment = AH_MSGFORMAT('Gruppi')
           .cAutoZoom = "GSARGADP"
       endcase
       if .cTipgES='S'
          .cComment = AH_MSGFORMAT("Associazione risorse/aree")
       endif
       If .cTipRis<>'P'
         Local nCtrl
         For nCtrl=1 to this.page1.Controls(1).ControlCount
           If UPPER(this.page1.Controls(1).Controls(nCtrl).Class)=='STDBOX'
             this.page1.Controls(1).Controls(nCtrl).Visible=.F.
           EndIf
         Next
       EndIf
    Endwith
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_LBLVAL = this.oPgFrm.Pages(1).oPag.LBLVAL
      this.w_FIRMAIMG = this.oPgFrm.Pages(4).oPag.FIRMAIMG
      this.w_BTNQRY = this.oPgFrm.Pages(1).oPag.BTNQRY
      DoDefault()
    proc Destroy()
      this.w_LBLVAL = .NULL.
      this.w_FIRMAIMG = .NULL.
      this.w_BTNQRY = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[18]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='RIS_ORSE'
    this.cWorkTables[3]='MANSIONI'
    this.cWorkTables[4]='NAZIONI'
    this.cWorkTables[5]='SEDIAZIE'
    this.cWorkTables[6]='CPUSERS'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='CPAR_DEF'
    this.cWorkTables[9]='ANAGRAFE'
    this.cWorkTables[10]='ANAG_PRO'
    this.cWorkTables[11]='BUSIUNIT'
    this.cWorkTables[12]='cpusers'
    this.cWorkTables[13]='TAB_CALE'
    this.cWorkTables[14]='CENCOST'
    this.cWorkTables[15]='DIPENDEN'
    this.cWorkTables[16]='ART_ICOL'
    this.cWorkTables[17]='PAR_AGEN'
    this.cWorkTables[18]='DIPENDEN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(18))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIPENDEN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIPENDEN_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MPH = CREATEOBJECT('stdLazyChild',this,'GSAR_MPH')
    this.GSAR_ARD = CREATEOBJECT('stdDynamicChild',this,'GSAR_ARD',this.oPgFrm.Page1.oPag.oLinkPC_1_76)
    this.GSAR_ARD.createrealchild()
    this.GSAR_MBR = CREATEOBJECT('stdDynamicChild',this,'GSAR_MBR',this.oPgFrm.Page5.oPag.oLinkPC_5_6)
    this.GSAR_MSR = CREATEOBJECT('stdDynamicChild',this,'GSAR_MSR',this.oPgFrm.Page1.oPag.oLinkPC_1_85)
    this.GSAR_MSR.createrealchild()
    this.GSAR_MPD = CREATEOBJECT('stdLazyChild',this,'GSAR_MPD')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MPH)
      this.GSAR_MPH.DestroyChildrenChain()
      this.GSAR_MPH=.NULL.
    endif
    if !ISNULL(this.GSAR_ARD)
      this.GSAR_ARD.DestroyChildrenChain()
      this.GSAR_ARD=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_76')
    if !ISNULL(this.GSAR_MBR)
      this.GSAR_MBR.DestroyChildrenChain()
      this.GSAR_MBR=.NULL.
    endif
    this.oPgFrm.Page5.oPag.RemoveObject('oLinkPC_5_6')
    if !ISNULL(this.GSAR_MSR)
      this.GSAR_MSR.DestroyChildrenChain()
      this.GSAR_MSR=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_85')
    if !ISNULL(this.GSAR_MPD)
      this.GSAR_MPD.DestroyChildrenChain()
      this.GSAR_MPD=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MPH.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_ARD.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MBR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MSR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MPD.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MPH.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_ARD.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MBR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MSR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MPD.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MPH.NewDocument()
    this.GSAR_ARD.NewDocument()
    this.GSAR_MBR.NewDocument()
    this.GSAR_MSR.NewDocument()
    this.GSAR_MPD.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MPH.SetKey(;
            .w_DPCODICE,"APCODGRU";
            ,.w_DPCODICE,"APPERSON";
            )
      this.GSAR_ARD.SetKey(;
            .w_CODANA,"DRCODANA";
            ,.w_AZIENDA,"DRCODAZI";
            ,.w_CODRAP,"DRCODRAP";
            ,.w_CODSOC,"DRCODSOC";
            )
      this.GSAR_MBR.SetKey(;
            .w_DPCODICE,"RBCODICE";
            )
      this.GSAR_MSR.SetKey(;
            .w_DPCODICE,"SRCODICE";
            )
      this.GSAR_MPD.SetKey(;
            .w_DPCODICE,"PDCODPER";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MPH.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPCODICE,"APCODGRU";
             ,.w_DPCODICE,"APPERSON";
             )
      .GSAR_ARD.ChangeRow(this.cRowID+'      1',1;
             ,.w_CODANA,"DRCODANA";
             ,.w_AZIENDA,"DRCODAZI";
             ,.w_CODRAP,"DRCODRAP";
             ,.w_CODSOC,"DRCODSOC";
             )
      .GSAR_MBR.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPCODICE,"RBCODICE";
             )
      .GSAR_MSR.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPCODICE,"SRCODICE";
             )
      .WriteTo_GSAR_MSR()
      .GSAR_MPD.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPCODICE,"PDCODPER";
             )
      .WriteTo_GSAR_MPD()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_ARD)
        i_f=.GSAR_ARD.BuildFilter()
        if !(i_f==.GSAR_ARD.cQueryFilter)
          i_fnidx=.GSAR_ARD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_ARD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_ARD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_ARD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_ARD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MBR)
        i_f=.GSAR_MBR.BuildFilter()
        if !(i_f==.GSAR_MBR.cQueryFilter)
          i_fnidx=.GSAR_MBR.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MBR.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MBR.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MBR.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MBR.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MSR)
        i_f=.GSAR_MSR.BuildFilter()
        if !(i_f==.GSAR_MSR.cQueryFilter)
          i_fnidx=.GSAR_MSR.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MSR.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MSR.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MSR.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MSR.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSAR_MSR()
  if at('gsar_msr',lower(this.GSAR_MSR.class))<>0
    if this.GSAR_MSR.w_SRCODPAD<>this.w_DPCODICE
      this.GSAR_MSR.w_SRCODPAD = this.w_DPCODICE
      this.GSAR_MSR.mCalc(.t.)
    endif
  endif
  return

procedure WriteTo_GSAR_MPD()
  if at('gsar_mpd',lower(this.GSAR_MPD.class))<>0
    if this.GSAR_MPD.cnt.w_DPCOGNOM<>this.w_DPCOGNOM or this.GSAR_MPD.cnt.w_DPNOME<>this.w_DPNOME
      this.GSAR_MPD.cnt.w_DPCOGNOM = this.w_DPCOGNOM
      this.GSAR_MPD.cnt.w_DPNOME = this.w_DPNOME
      this.GSAR_MPD.cnt.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DPCODICE = NVL(DPCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_28_joined
    link_1_28_joined=.f.
    local link_1_31_joined
    link_1_31_joined=.f.
    local link_1_34_joined
    link_1_34_joined=.f.
    local link_1_35_joined
    link_1_35_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    local link_1_40_joined
    link_1_40_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    local link_3_9_joined
    link_3_9_joined=.f.
    local link_1_86_joined
    link_1_86_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIPENDEN where DPCODICE=KeySet.DPCODICE
    *
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIPENDEN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIPENDEN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIPENDEN '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_28_joined=this.AddJoinedLink_1_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_31_joined=this.AddJoinedLink_1_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_34_joined=this.AddJoinedLink_1_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_35_joined=this.AddJoinedLink_1_35(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_40_joined=this.AddJoinedLink_1_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_9_joined=this.AddJoinedLink_3_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_86_joined=this.AddJoinedLink_1_86(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DPCODICE',this.w_DPCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DCLMAST = space(40)
        .w_ORIGANA = .w_DPCODANA
        .w_NAZION = space(3)
        .w_PRONAS = space(2)
        .w_PROVIN = space(2)
        .w_ANAZION = space(3)
        .w_APRONAS = space(2)
        .w_APROVIN = space(2)
        .w_PRONAS1 = space(2)
        .w_CODAZI = i_CODAZI
        .w_PROVIN1 = space(2)
        .w_DESRIS = space(40)
        .w_DESCON = space(40)
        .w_DESMAN = space(40)
        .w_DESSER = space(40)
        .w_OBTEST = ctod("  /  /  ")
        .w_TIPORIS = space(10)
        .w_TIPRIS = 'RE'
        .w_DESSED = space(40)
        .w_PERFIS = space(1)
        .w_DPUMDEFA = space(3)
        .w_TIPART = space(1)
        .w_ARTICOLO = space(20)
        .w_CODRAP = 16
        .w_AZIENDA = I_CODAZI
        .w_RAPDET = .F.
        .w_DESNAZ1 = space(35)
        .w_DESNAZ = space(35)
        .w_CODISO = space(3)
        .w_DESUTE = space(20)
        .w_COGNSOS = space(40)
        .w_FLANAL = space(1)
        .w_ERR = .f.
        .w_DESUTE = space(20)
        .w_CCDESPIA = space(40)
        .w_TCDESCRI = space(40)
        .w_FLPERSONA = 'P'
        .w_CCDESPIA = space(40)
        .w_DPDESGRU = space(40)
        .w_TIPGRU = space(1)
        .w_TCDESCRI = space(40)
        .w_OLDFLVISI = .w_DPFLVISI
        .w_DESPRE = space(40)
        .w_DESPRE = space(40)
        .w_TIPAR1 = space(1)
        .w_OB_TEST = i_INIDAT
        .w_CODCOM = space(4)
        .w_CHKVARVI = .F.
        .w_COGNRES = space(10)
        .w_NOMERES = space(10)
        .w_LPATH = .f.
        .w_CONTA = .w_CONTA
        .w_PAFLPRES = space(1)
        .w_DPCODICE = NVL(DPCODICE,space(5))
        .w_DPTIPRIS = NVL(DPTIPRIS,space(1))
        .w_DPCODANA = NVL(DPCODANA,space(15))
          if link_1_4_joined
            this.w_DPCODANA = NVL(ANCODICE104,NVL(this.w_DPCODANA,space(15)))
            this.w_DCLMAST = NVL(ANDESCRI104,space(40))
            this.w_DPRESVIA = NVL(ANINDIRI104,space(35))
            this.w_DPRESLOC = NVL(ANLOCALI104,space(30))
            this.w_PROVIN = NVL(ANPROVIN104,space(2))
            this.w_PROVIN1 = NVL(ANPROVIN104,space(2))
            this.w_DPTELEF1 = NVL(ANTELEFO104,space(18))
            this.w_DPTELEF2 = NVL(ANTELFAX104,space(18))
            this.w_DPINDMAI = NVL(AN_EMAIL104,space(254))
            this.w_DPINDPEC = NVL(AN_EMPEC104,space(254))
            this.w_DPNOME = NVL(AN__NOME104,space(50))
            this.w_DPCOGNOM = NVL(ANCOGNOM104,space(50))
            this.w_DPCODFIS = NVL(ANCODFIS104,space(16))
            this.w_DP_SESSO = NVL(AN_SESSO104,space(1))
            this.w_DPDATNAS = NVL(cp_ToDate(ANDATNAS104),ctod("  /  /  "))
            this.w_DPNASLOC = NVL(ANLOCNAS104,space(40))
            this.w_PRONAS = NVL(ANPRONAS104,space(2))
            this.w_PRONAS1 = NVL(ANPRONAS104,space(2))
            this.w_DPRESCAP = NVL(AN___CAP104,space(9))
            this.w_DPDESCRI = NVL(ANDESCRI104,space(60))
            this.w_DPDESCRI2 = NVL(ANDESCR2104,space(60))
            this.w_NAZION = NVL(ANNAZION104,space(3))
            this.w_DPNUMCEL = NVL(ANNUMCEL104,space(18))
            this.w_DPPERFIS = NVL(ANPERFIS104,space(1))
            this.w_DPPARIVA = NVL(ANPARIVA104,space(12))
            this.w_DP__NOTE = NVL(AN__NOTE104,space(0))
            this.w_DPINDWEB = NVL(ANINDWEB104,space(50))
            this.w_DPNATGIU = NVL(ANNATGIU104,space(10))
            this.w_DPNUMCAR = NVL(ANNUMCAR104,space(18))
            this.w_DPINDIR2 = NVL(ANINDIR2104,space(35))
            this.w_DPDTOBSO = NVL(cp_ToDate(ANDTOBSO104),ctod("  /  /  "))
          else
          .link_1_4('Load')
          endif
          .link_1_7('Load')
          .link_1_8('Load')
          .link_1_9('Load')
        .w_DPBADGE = NVL(DPBADGE,space(10))
        .w_DPDESCRI = NVL(DPDESCRI,space(60))
        .w_DPCODCEN = NVL(DPCODCEN,space(15))
          if link_1_18_joined
            this.w_DPCODCEN = NVL(CC_CONTO118,NVL(this.w_DPCODCEN,space(15)))
            this.w_CCDESPIA = NVL(CCDESPIA118,space(40))
          else
          .link_1_18('Load')
          endif
        .w_DPSERPRE = NVL(DPSERPRE,space(41))
          if link_1_19_joined
            this.w_DPSERPRE = NVL(ARCODART119,NVL(this.w_DPSERPRE,space(41)))
            this.w_DESPRE = NVL(ARDESART119,space(40))
            this.w_TIPAR1 = NVL(ARTIPART119,space(1))
          else
          .link_1_19('Load')
          endif
        .w_DPGRUPRE = NVL(DPGRUPRE,space(5))
          if link_1_20_joined
            this.w_DPGRUPRE = NVL(DPCODICE120,NVL(this.w_DPGRUPRE,space(5)))
            this.w_DPDESGRU = NVL(DPDESCRI120,space(40))
            this.w_TIPGRU = NVL(DPTIPRIS120,space(1))
          else
          .link_1_20('Load')
          endif
        .w_DPCOSORA = NVL(DPCOSORA,0)
        .w_DPCOGNOM = NVL(DPCOGNOM,space(50))
        .w_DPFLVISI = NVL(DPFLVISI,space(1))
        .w_DPNOME = NVL(DPNOME,space(50))
        .w_DPFLINEX = NVL(DPFLINEX,space(1))
        .w_DPREPART = NVL(DPREPART,space(20))
          .link_1_26('Load')
        .w_DPDITTA = NVL(DPDITTA,space(15))
          if link_1_28_joined
            this.w_DPDITTA = NVL(ANCODICE128,NVL(this.w_DPDITTA,space(15)))
            this.w_DESCON = NVL(ANDESCRI128,space(40))
            this.w_DPDATNAS = NVL(cp_ToDate(ANDATNAS128),ctod("  /  /  "))
            this.w_DPNASLOC = NVL(ANLOCNAS128,space(40))
            this.w_DPNASPRO = NVL(ANPRONAS128,space(2))
            this.w_DP_SESSO = NVL(AN_SESSO128,space(1))
            this.w_PERFIS = NVL(ANPERFIS128,space(1))
            this.w_DPCODFIS = NVL(ANCODFIS128,space(16))
          else
          .link_1_28('Load')
          endif
        .w_DPCODDES = NVL(DPCODDES,space(5))
          .link_1_29('Load')
        .w_DPMANSION = NVL(DPMANSION,space(5))
          if link_1_31_joined
            this.w_DPMANSION = NVL(MNCODICE131,NVL(this.w_DPMANSION,space(5)))
            this.w_DESMAN = NVL(MNDESCRI131,space(40))
          else
          .link_1_31('Load')
          endif
        .w_DPCOSORA = NVL(DPCOSORA,0)
        .w_DPCODCEN = NVL(DPCODCEN,space(15))
          if link_1_34_joined
            this.w_DPCODCEN = NVL(CC_CONTO134,NVL(this.w_DPCODCEN,space(15)))
            this.w_CCDESPIA = NVL(CCDESPIA134,space(40))
          else
          .link_1_34('Load')
          endif
        .w_DPCODCAL = NVL(DPCODCAL,space(5))
          if link_1_35_joined
            this.w_DPCODCAL = NVL(TCCODICE135,NVL(this.w_DPCODCAL,space(5)))
            this.w_TCDESCRI = NVL(TCDESCRI135,space(40))
          else
          .link_1_35('Load')
          endif
        .w_DPMODALL = NVL(DPMODALL,space(1))
        .w_DPTIPCON = NVL(DPTIPCON,space(1))
        .w_DPSERVIZ = NVL(DPSERVIZ,space(41))
          if link_1_38_joined
            this.w_DPSERVIZ = NVL(CACODICE138,NVL(this.w_DPSERVIZ,space(41)))
            this.w_DESSER = NVL(CADESART138,space(40))
            this.w_TIPART = NVL(CA__TIPO138,space(1))
            this.w_ARTICOLO = NVL(CACODART138,space(20))
          else
          .link_1_38('Load')
          endif
        .w_DPSERPRE = NVL(DPSERPRE,space(41))
          if link_1_40_joined
            this.w_DPSERPRE = NVL(ARCODART140,NVL(this.w_DPSERPRE,space(41)))
            this.w_DESPRE = NVL(ARDESART140,space(40))
            this.w_TIPAR1 = NVL(ARTIPART140,space(1))
          else
          .link_1_40('Load')
          endif
        .w_DPPATHPR = NVL(DPPATHPR,space(254))
        .w_DP__PATH = NVL(DP__PATH,space(254))
        .w_DP__DIKE = NVL(DP__DIKE,space(254))
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .w_DPKEYPAR = 'TAM'
          .link_1_64('Load')
        .oPgFrm.Page1.oPag.LBLVAL.Calculate(IIF(.w_DPTIPRIS<>'P', "", "[" + ALLTRIM(GETVALUT(g_PERVAL, "VASIMVAL")) + "]"))
        .w_CODANA = .w_DPCODANA
        .w_CODSOC = .w_DPCODICE
        .w_CODANA = .w_DPCODANA
        .w_RAPOBSO = .w_DPDATFRA
        .w_DPCODFIS = NVL(DPCODFIS,space(16))
        .w_DPPARIVA = NVL(DPPARIVA,space(12))
        .w_CODIFISC = iif(g_ANACOM='S',.w_CODIFISC,.w_DPCODFIS)
        .w_DPNUMCAR = NVL(DPNUMCAR,space(18))
        .w_DP_SESSO = NVL(DP_SESSO,space(1))
        .w_DPNASLOC = NVL(DPNASLOC,space(40))
        .w_DPNASPRO = NVL(DPNASPRO,space(2))
        .w_DPNASNAZ = NVL(DPNASNAZ,space(3))
          if link_2_11_joined
            this.w_DPNASNAZ = NVL(NACODNAZ211,NVL(this.w_DPNASNAZ,space(3)))
            this.w_DESNAZ1 = NVL(NADESNAZ211,space(35))
          else
          .link_2_11('Load')
          endif
        .w_DPDATNAS = NVL(cp_ToDate(DPDATNAS),ctod("  /  /  "))
        .w_DPTITSTU = NVL(DPTITSTU,space(40))
        .w_DPQUALIF = NVL(DPQUALIF,space(40))
        .w_DPLIVELL = NVL(DPLIVELL,space(40))
        .w_DPDATASS = NVL(cp_ToDate(DPDATASS),ctod("  /  /  "))
        .w_DPDATFRA = NVL(cp_ToDate(DPDATFRA),ctod("  /  /  "))
        .w_DPRESVIA = NVL(DPRESVIA,space(35))
        .w_DPINDIR2 = NVL(DPINDIR2,space(35))
        .w_DPRESCAP = NVL(DPRESCAP,space(9))
        .w_DPRESLOC = NVL(DPRESLOC,space(30))
        .w_DPRESPRO = NVL(DPRESPRO,space(2))
        .w_DPNAZION = NVL(DPNAZION,space(3))
          if link_3_9_joined
            this.w_DPNAZION = NVL(NACODNAZ309,NVL(this.w_DPNAZION,space(3)))
            this.w_DESNAZ = NVL(NADESNAZ309,space(35))
            this.w_CODISO = NVL(NACODISO309,space(3))
          else
          .link_3_9('Load')
          endif
        .w_DPDOMVIA = NVL(DPDOMVIA,space(40))
        .w_DPDOMCAP = NVL(DPDOMCAP,space(9))
        .w_DPDOMLOC = NVL(DPDOMLOC,space(30))
        .w_DPDOMPRO = NVL(DPDOMPRO,space(2))
        .w_DPTELEF1 = NVL(DPTELEF1,space(18))
        .w_DPTELEF2 = NVL(DPTELEF2,space(18))
        .w_DPINDMAI = NVL(DPINDMAI,space(254))
        .w_DPNUMCEL = NVL(DPNUMCEL,space(18))
        .w_DPNATGIU = NVL(DPNATGIU,space(10))
        .w_DPPERFIS = NVL(DPPERFIS,space(1))
        .w_DPINDPEC = NVL(DPINDPEC,space(254))
        .w_DPCODUTE = NVL(DPCODUTE,0)
          .link_4_1('Load')
        .w_DPCHKING = NVL(DPCHKING,space(1))
        .w_DPDATINI = NVL(cp_ToDate(DPDATINI),ctod("  /  /  "))
        .w_DPGGBLOC = NVL(DPGGBLOC,0)
        .w_DP_FIRMA = NVL(DP_FIRMA,space(200))
        .oPgFrm.Page4.oPag.FIRMAIMG.Calculate(.w_DP_FIRMA)
        .oPgFrm.Page4.oPag.oObj_4_11.Calculate()
        .w_DPASSDAL = NVL(cp_ToDate(DPASSDAL),ctod("  /  /  "))
        .w_DPASSFIN = NVL(cp_ToDate(DPASSFIN),ctod("  /  /  "))
        .w_DPCODSOS = NVL(DPCODSOS,space(5))
          .link_4_16('Load')
        .w_DPCDUTAL = NVL(DPCDUTAL,0)
          * evitabile
          *.link_5_1('Load')
        .w_DPFLCONF = NVL(DPFLCONF,space(1))
        .w_DPCODBUN = NVL(DPCODBUN,space(3))
          .link_5_4('Load')
        .w_DP__NOTE = NVL(DP__NOTE,space(0))
        .w_DPCODUTE = NVL(DPCODUTE,0)
          .link_1_80('Load')
        .w_DP__NOTE = NVL(DP__NOTE,space(0))
        .w_DPFLAVVI = NVL(DPFLAVVI,space(1))
        .w_DPINDWEB = NVL(DPINDWEB,space(50))
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate(IIF(IsAlt(), "Costo orario standard (espresso in Euro)", "Costo orario standard (espresso in valuta di conto)"))
        .w_DPCODCAL = NVL(DPCODCAL,space(5))
          if link_1_86_joined
            this.w_DPCODCAL = NVL(TCCODICE186,NVL(this.w_DPCODCAL,space(5)))
            this.w_TCDESCRI = NVL(TCDESCRI186,space(40))
          else
          .link_1_86('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .w_DPDESCRI2 = NVL(DPDESCRI2,space(60))
        .w_DPDTOBSO = NVL(cp_ToDate(DPDTOBSO),ctod("  /  /  "))
        .w_DPVCSICS = NVL(DPVCSICS,space(1))
        .w_LetturaParAgen = i_CodAzi
          .link_7_1('Load')
        .w_DPRSFASC = NVL(DPRSFASC,space(1))
        .w_DPCODRES = NVL(DPCODRES,space(5))
          .link_7_3('Load')
        .w_DPPERFAT = NVL(DPPERFAT,0)
        .w_DPPERFAC = NVL(DPPERFAC,0)
        .w_DPTIPRIG = NVL(DPTIPRIG,space(1))
        .w_DENOMRES = alltrim(.w_COGNRES)+ space(1)+alltrim(.w_NOMERES)
        .w_DPCTRPRE = NVL(DPCTRPRE,space(1))
        .w_DPGG_PRE = NVL(DPGG_PRE,0)
        .w_DPGG_SUC = NVL(DPGG_SUC,0)
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .w_DPFLPROM = NVL(DPFLPROM,space(1))
        cp_LoadRecExtFlds(this,'DIPENDEN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page4.oPag.oBtn_4_9.enabled = this.oPgFrm.Page4.oPag.oBtn_4_9.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_108.enabled = this.oPgFrm.Page1.oPag.oBtn_1_108.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_122.enabled = this.oPgFrm.Page1.oPag.oBtn_1_122.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DPCODICE = space(5)
      .w_DPTIPRIS = space(1)
      .w_DCLMAST = space(40)
      .w_DPCODANA = space(15)
      .w_ORIGANA = space(15)
      .w_NAZION = space(3)
      .w_PRONAS = space(2)
      .w_PROVIN = space(2)
      .w_ANAZION = space(3)
      .w_APRONAS = space(2)
      .w_APROVIN = space(2)
      .w_PRONAS1 = space(2)
      .w_CODAZI = space(5)
      .w_PROVIN1 = space(2)
      .w_DPBADGE = space(10)
      .w_DPDESCRI = space(60)
      .w_DPCODCEN = space(15)
      .w_DPSERPRE = space(41)
      .w_DPGRUPRE = space(5)
      .w_DPCOSORA = 0
      .w_DPCOGNOM = space(50)
      .w_DPFLVISI = space(1)
      .w_DPNOME = space(50)
      .w_DPFLINEX = space(1)
      .w_DPREPART = space(20)
      .w_DESRIS = space(40)
      .w_DPDITTA = space(15)
      .w_DPCODDES = space(5)
      .w_DESCON = space(40)
      .w_DPMANSION = space(5)
      .w_DESMAN = space(40)
      .w_DPCOSORA = 0
      .w_DPCODCEN = space(15)
      .w_DPCODCAL = space(5)
      .w_DPMODALL = space(1)
      .w_DPTIPCON = space(1)
      .w_DPSERVIZ = space(41)
      .w_DESSER = space(40)
      .w_DPSERPRE = space(41)
      .w_DPPATHPR = space(254)
      .w_DP__PATH = space(254)
      .w_DP__DIKE = space(254)
      .w_OBTEST = ctod("  /  /  ")
      .w_TIPORIS = space(10)
      .w_TIPRIS = space(2)
      .w_DESSED = space(40)
      .w_PERFIS = space(1)
      .w_DPKEYPAR = space(10)
      .w_DPUMDEFA = space(3)
      .w_TIPART = space(1)
      .w_ARTICOLO = space(20)
      .w_CODANA = space(15)
      .w_CODRAP = 0
      .w_AZIENDA = space(5)
      .w_CODSOC = space(15)
      .w_CODANA = space(15)
      .w_RAPDET = .f.
      .w_RAPOBSO = ctod("  /  /  ")
      .w_DPCODFIS = space(16)
      .w_DPPARIVA = space(12)
      .w_CODIFISC = space(16)
      .w_DPNUMCAR = space(18)
      .w_DP_SESSO = space(1)
      .w_DPNASLOC = space(40)
      .w_DPNASPRO = space(2)
      .w_DPNASNAZ = space(3)
      .w_DESNAZ1 = space(35)
      .w_DPDATNAS = ctod("  /  /  ")
      .w_DPTITSTU = space(40)
      .w_DPQUALIF = space(40)
      .w_DPLIVELL = space(40)
      .w_DPDATASS = ctod("  /  /  ")
      .w_DPDATFRA = ctod("  /  /  ")
      .w_DPRESVIA = space(35)
      .w_DPINDIR2 = space(35)
      .w_DPRESCAP = space(9)
      .w_DPRESLOC = space(30)
      .w_DPRESPRO = space(2)
      .w_DPNAZION = space(3)
      .w_DPDOMVIA = space(40)
      .w_DPDOMCAP = space(9)
      .w_DPDOMLOC = space(30)
      .w_DPDOMPRO = space(2)
      .w_DPTELEF1 = space(18)
      .w_DPTELEF2 = space(18)
      .w_DPINDMAI = space(254)
      .w_DESNAZ = space(35)
      .w_CODISO = space(3)
      .w_DPNUMCEL = space(18)
      .w_DPNATGIU = space(10)
      .w_DPPERFIS = space(1)
      .w_DPINDPEC = space(254)
      .w_DPCODUTE = 0
      .w_DESUTE = space(20)
      .w_DPCHKING = space(1)
      .w_DPDATINI = ctod("  /  /  ")
      .w_DPGGBLOC = 0
      .w_DP_FIRMA = space(200)
      .w_DPASSDAL = ctod("  /  /  ")
      .w_DPASSFIN = ctod("  /  /  ")
      .w_DPCODSOS = space(5)
      .w_COGNSOS = space(40)
      .w_DPCDUTAL = 0
      .w_DPFLCONF = space(1)
      .w_DPCODBUN = space(3)
      .w_FLANAL = space(1)
      .w_DP__NOTE = space(0)
      .w_ERR = .f.
      .w_DPCODUTE = 0
      .w_DESUTE = space(20)
      .w_DP__NOTE = space(0)
      .w_DPFLAVVI = space(1)
      .w_DPINDWEB = space(50)
      .w_DPCODCAL = space(5)
      .w_CCDESPIA = space(40)
      .w_TCDESCRI = space(40)
      .w_FLPERSONA = space(1)
      .w_DPDESCRI2 = space(60)
      .w_CCDESPIA = space(40)
      .w_DPDESGRU = space(40)
      .w_TIPGRU = space(1)
      .w_TCDESCRI = space(40)
      .w_OLDFLVISI = space(1)
      .w_DESPRE = space(40)
      .w_DESPRE = space(40)
      .w_TIPAR1 = space(1)
      .w_DPDTOBSO = ctod("  /  /  ")
      .w_OB_TEST = ctod("  /  /  ")
      .w_CODCOM = space(4)
      .w_CHKVARVI = .f.
      .w_COGNRES = space(10)
      .w_NOMERES = space(10)
      .w_DPVCSICS = space(1)
      .w_LPATH = .f.
      .w_CONTA = 0
      .w_LetturaParAgen = space(5)
      .w_DPRSFASC = space(1)
      .w_DPCODRES = space(5)
      .w_DPPERFAT = 0
      .w_DPPERFAC = 0
      .w_DPTIPRIG = space(1)
      .w_DENOMRES = space(40)
      .w_DPCTRPRE = space(1)
      .w_DPGG_PRE = 0
      .w_DPGG_SUC = 0
      .w_PAFLPRES = space(1)
      .w_DPFLPROM = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DPTIPRIS = this.cTipRis
        .DoRTCalc(3,4,.f.)
          if not(empty(.w_DPCODANA))
          .link_1_4('Full')
          endif
        .w_ORIGANA = .w_DPCODANA
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_NAZION))
          .link_1_7('Full')
          endif
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_PRONAS))
          .link_1_8('Full')
          endif
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_PROVIN))
          .link_1_9('Full')
          endif
          .DoRTCalc(9,12,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(14,17,.f.)
          if not(empty(.w_DPCODCEN))
          .link_1_18('Full')
          endif
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_DPSERPRE))
          .link_1_19('Full')
          endif
        .DoRTCalc(19,19,.f.)
          if not(empty(.w_DPGRUPRE))
          .link_1_20('Full')
          endif
          .DoRTCalc(20,21,.f.)
        .w_DPFLVISI = 'T'
          .DoRTCalc(23,23,.f.)
        .w_DPFLINEX = IIF((VARTYPE(g_AGEN)='C' AND g_AGEN='S') OR IsAlt(), 'I', 'E')
        .w_DPREPART = iif(.w_DPFLINEX='E',space(15),.w_DPREPART)
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_DPREPART))
          .link_1_26('Full')
          endif
          .DoRTCalc(26,26,.f.)
        .w_DPDITTA = iif(.w_DPFLINEX='I',space(15),.w_DPDITTA)
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_DPDITTA))
          .link_1_28('Full')
          endif
        .w_DPCODDES = iif(.w_DPFLINEX="E",space(15),.w_DPCODDES)
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_DPCODDES))
          .link_1_29('Full')
          endif
        .DoRTCalc(29,30,.f.)
          if not(empty(.w_DPMANSION))
          .link_1_31('Full')
          endif
        .DoRTCalc(31,33,.f.)
          if not(empty(.w_DPCODCEN))
          .link_1_34('Full')
          endif
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_DPCODCAL))
          .link_1_35('Full')
          endif
        .w_DPMODALL = 'N'
        .w_DPTIPCON = 'F'
        .DoRTCalc(37,37,.f.)
          if not(empty(.w_DPSERVIZ))
          .link_1_38('Full')
          endif
        .DoRTCalc(38,39,.f.)
          if not(empty(.w_DPSERPRE))
          .link_1_40('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
          .DoRTCalc(40,44,.f.)
        .w_TIPRIS = 'RE'
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
          .DoRTCalc(46,47,.f.)
        .w_DPKEYPAR = 'TAM'
        .DoRTCalc(48,48,.f.)
          if not(empty(.w_DPKEYPAR))
          .link_1_64('Full')
          endif
        .oPgFrm.Page1.oPag.LBLVAL.Calculate(IIF(.w_DPTIPRIS<>'P', "", "[" + ALLTRIM(GETVALUT(g_PERVAL, "VASIMVAL")) + "]"))
          .DoRTCalc(49,51,.f.)
        .w_CODANA = .w_DPCODANA
        .w_CODRAP = 16
        .w_AZIENDA = I_CODAZI
        .w_CODSOC = .w_DPCODICE
        .w_CODANA = .w_DPCODANA
        .w_RAPDET = .F.
        .w_RAPOBSO = .w_DPDATFRA
          .DoRTCalc(59,60,.f.)
        .w_CODIFISC = iif(g_ANACOM='S',.w_CODIFISC,.w_DPCODFIS)
          .DoRTCalc(62,62,.f.)
        .w_DP_SESSO = 'M'
          .DoRTCalc(64,64,.f.)
        .w_DPNASPRO = iif(g_ANACOM<>'S',.w_DPNASPRO,iif(empty(nvl(.w_APRONAS,'')),.w_PRONAS1,.w_APRONAS))
        .DoRTCalc(66,66,.f.)
          if not(empty(.w_DPNASNAZ))
          .link_2_11('Full')
          endif
          .DoRTCalc(67,77,.f.)
        .w_DPRESPRO = iif(g_ANACOM<>'S',.w_DPRESPRO,iif(empty(nvl(.w_APROVIN,'')),.w_PROVIN1,.w_APROVIN))
        .w_DPNAZION = iif(g_ANACOM<>'S',.w_DPNAZION,.w_ANAZION)
        .DoRTCalc(79,79,.f.)
          if not(empty(.w_DPNAZION))
          .link_3_9('Full')
          endif
          .DoRTCalc(80,90,.f.)
        .w_DPPERFIS = .w_PERFIS
        .DoRTCalc(92,93,.f.)
          if not(empty(.w_DPCODUTE))
          .link_4_1('Full')
          endif
          .DoRTCalc(94,94,.f.)
        .w_DPCHKING = 'N'
        .oPgFrm.Page4.oPag.FIRMAIMG.Calculate(.w_DP_FIRMA)
        .oPgFrm.Page4.oPag.oObj_4_11.Calculate()
        .DoRTCalc(96,101,.f.)
          if not(empty(.w_DPCODSOS))
          .link_4_16('Full')
          endif
        .DoRTCalc(102,103,.f.)
          if not(empty(.w_DPCDUTAL))
          .link_5_1('Full')
          endif
        .DoRTCalc(104,105,.f.)
          if not(empty(.w_DPCODBUN))
          .link_5_4('Full')
          endif
        .DoRTCalc(106,109,.f.)
          if not(empty(.w_DPCODUTE))
          .link_1_80('Full')
          endif
          .DoRTCalc(110,111,.f.)
        .w_DPFLAVVI = ' '
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate(IIF(IsAlt(), "Costo orario standard (espresso in Euro)", "Costo orario standard (espresso in valuta di conto)"))
        .DoRTCalc(113,114,.f.)
          if not(empty(.w_DPCODCAL))
          .link_1_86('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
          .DoRTCalc(115,116,.f.)
        .w_FLPERSONA = 'P'
          .DoRTCalc(118,122,.f.)
        .w_OLDFLVISI = .w_DPFLVISI
          .DoRTCalc(124,127,.f.)
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(129,129,.f.)
        .w_CHKVARVI = .F.
          .DoRTCalc(131,132,.f.)
        .w_DPVCSICS = 'A'
          .DoRTCalc(134,134,.f.)
        .w_CONTA = .w_CONTA
        .w_LetturaParAgen = i_CodAzi
        .DoRTCalc(136,136,.f.)
          if not(empty(.w_LetturaParAgen))
          .link_7_1('Full')
          endif
        .w_DPRSFASC = 'N'
        .DoRTCalc(138,138,.f.)
          if not(empty(.w_DPCODRES))
          .link_7_3('Full')
          endif
          .DoRTCalc(139,139,.f.)
        .w_DPPERFAC = .w_DPPERFAT
        .w_DPTIPRIG = IIF(EMPTY(.w_DPTIPRIG),'D',.w_DPTIPRIG)
        .w_DENOMRES = alltrim(.w_COGNRES)+ space(1)+alltrim(.w_NOMERES)
        .w_DPCTRPRE = 'N'
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
          .DoRTCalc(144,146,.f.)
        .w_DPFLPROM = ' '
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIPENDEN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page4.oPag.oBtn_4_9.enabled = this.oPgFrm.Page4.oPag.oBtn_4_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_108.enabled = this.oPgFrm.Page1.oPag.oBtn_1_108.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_122.enabled = this.oPgFrm.Page1.oPag.oBtn_1_122.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDPCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oDCLMAST_1_3.enabled = i_bVal
      .Page1.oPag.oDPCODANA_1_4.enabled = i_bVal
      .Page1.oPag.oDPBADGE_1_16.enabled = i_bVal
      .Page1.oPag.oDPDESCRI_1_17.enabled = i_bVal
      .Page1.oPag.oDPCODCEN_1_18.enabled = i_bVal
      .Page1.oPag.oDPSERPRE_1_19.enabled = i_bVal
      .Page1.oPag.oDPGRUPRE_1_20.enabled = i_bVal
      .Page1.oPag.oDPCOSORA_1_21.enabled = i_bVal
      .Page1.oPag.oDPCOGNOM_1_22.enabled = i_bVal
      .Page1.oPag.oDPFLVISI_1_23.enabled = i_bVal
      .Page1.oPag.oDPNOME_1_24.enabled = i_bVal
      .Page1.oPag.oDPFLINEX_1_25.enabled_(i_bVal)
      .Page1.oPag.oDPREPART_1_26.enabled = i_bVal
      .Page1.oPag.oDPDITTA_1_28.enabled = i_bVal
      .Page1.oPag.oDPCODDES_1_29.enabled = i_bVal
      .Page1.oPag.oDPMANSION_1_31.enabled = i_bVal
      .Page1.oPag.oDPCOSORA_1_33.enabled = i_bVal
      .Page1.oPag.oDPCODCEN_1_34.enabled = i_bVal
      .Page1.oPag.oDPCODCAL_1_35.enabled = i_bVal
      .Page1.oPag.oDPMODALL_1_36.enabled = i_bVal
      .Page1.oPag.oDPSERVIZ_1_38.enabled = i_bVal
      .Page1.oPag.oDPSERPRE_1_40.enabled = i_bVal
      .Page1.oPag.oDPPATHPR_1_41.enabled = i_bVal
      .Page1.oPag.oDP__PATH_1_42.enabled = i_bVal
      .Page1.oPag.oDP__DIKE_1_43.enabled = i_bVal
      .Page2.oPag.oDPCODFIS_2_1.enabled = i_bVal
      .Page2.oPag.oDPPARIVA_2_2.enabled = i_bVal
      .Page2.oPag.oDPNUMCAR_2_4.enabled = i_bVal
      .Page2.oPag.oDP_SESSO_2_5.enabled = i_bVal
      .Page2.oPag.oDPNASLOC_2_7.enabled = i_bVal
      .Page2.oPag.oDPNASPRO_2_10.enabled = i_bVal
      .Page2.oPag.oDPNASNAZ_2_11.enabled = i_bVal
      .Page2.oPag.oDPDATNAS_2_14.enabled = i_bVal
      .Page2.oPag.oDPTITSTU_2_16.enabled = i_bVal
      .Page2.oPag.oDPQUALIF_2_17.enabled = i_bVal
      .Page2.oPag.oDPLIVELL_2_18.enabled = i_bVal
      .Page2.oPag.oDPDATASS_2_21.enabled = i_bVal
      .Page2.oPag.oDPDATFRA_2_22.enabled = i_bVal
      .Page3.oPag.oDPRESVIA_3_1.enabled = i_bVal
      .Page3.oPag.oDPINDIR2_3_2.enabled = i_bVal
      .Page3.oPag.oDPRESCAP_3_5.enabled = i_bVal
      .Page3.oPag.oDPRESLOC_3_6.enabled = i_bVal
      .Page3.oPag.oDPRESPRO_3_8.enabled = i_bVal
      .Page3.oPag.oDPNAZION_3_9.enabled = i_bVal
      .Page3.oPag.oDPDOMVIA_3_14.enabled = i_bVal
      .Page3.oPag.oDPDOMCAP_3_17.enabled = i_bVal
      .Page3.oPag.oDPDOMLOC_3_18.enabled = i_bVal
      .Page3.oPag.oDPDOMPRO_3_20.enabled = i_bVal
      .Page3.oPag.oDPTELEF1_3_24.enabled = i_bVal
      .Page3.oPag.oDPTELEF2_3_25.enabled = i_bVal
      .Page3.oPag.oDPINDMAI_3_27.enabled = i_bVal
      .Page3.oPag.oDPNUMCEL_3_31.enabled = i_bVal
      .Page3.oPag.oDPINDPEC_3_35.enabled = i_bVal
      .Page4.oPag.oDPCODUTE_4_1.enabled = i_bVal
      .Page4.oPag.oDPCHKING_4_4.enabled = i_bVal
      .Page4.oPag.oDPDATINI_4_5.enabled = i_bVal
      .Page4.oPag.oDPGGBLOC_4_6.enabled = i_bVal
      .Page4.oPag.oDP_FIRMA_4_8.enabled = i_bVal
      .Page4.oPag.oDPASSDAL_4_12.enabled = i_bVal
      .Page4.oPag.oDPASSFIN_4_13.enabled = i_bVal
      .Page4.oPag.oDPCODSOS_4_16.enabled = i_bVal
      .Page5.oPag.oDPCDUTAL_5_1.enabled = i_bVal
      .Page5.oPag.oDPFLCONF_5_3.enabled = i_bVal
      .Page5.oPag.oDPCODBUN_5_4.enabled = i_bVal
      .Page6.oPag.oDP__NOTE_6_1.enabled = i_bVal
      .Page1.oPag.oDPCODUTE_1_80.enabled = i_bVal
      .Page1.oPag.oDP__NOTE_1_82.enabled = i_bVal
      .Page3.oPag.oDPFLAVVI_3_37.enabled = i_bVal
      .Page3.oPag.oDPINDWEB_3_38.enabled = i_bVal
      .Page1.oPag.oDPCODCAL_1_86.enabled = i_bVal
      .Page1.oPag.oDPDTOBSO_1_113.enabled = i_bVal
      .Page3.oPag.oDPVCSICS_3_42.enabled = i_bVal
      .Page7.oPag.oDPRSFASC_7_2.enabled = i_bVal
      .Page7.oPag.oDPCODRES_7_3.enabled = i_bVal
      .Page7.oPag.oDPPERFAT_7_4.enabled = i_bVal
      .Page7.oPag.oDPPERFAC_7_5.enabled = i_bVal
      .Page7.oPag.oDPTIPRIG_7_7.enabled = i_bVal
      .Page7.oPag.oDPCTRPRE_7_12.enabled = i_bVal
      .Page7.oPag.oDPGG_PRE_7_14.enabled = i_bVal
      .Page7.oPag.oDPGG_SUC_7_16.enabled = i_bVal
      .Page3.oPag.oDPFLPROM_3_44.enabled = i_bVal
      .Page4.oPag.oBtn_4_9.enabled = .Page4.oPag.oBtn_4_9.mCond()
      .Page2.oPag.oBtn_2_31.enabled = i_bVal
      .Page1.oPag.oBtn_1_108.enabled = .Page1.oPag.oBtn_1_108.mCond()
      .Page1.oPag.oBtn_1_122.enabled = .Page1.oPag.oBtn_1_122.mCond()
      .Page1.oPag.oObj_1_52.enabled = i_bVal
      .Page1.oPag.oObj_1_60.enabled = i_bVal
      .Page1.oPag.LBLVAL.enabled = i_bVal
      .Page4.oPag.FIRMAIMG.enabled = i_bVal
      .Page4.oPag.oObj_4_11.enabled = i_bVal
      .Page1.oPag.oObj_1_88.enabled = i_bVal
      .Page1.oPag.BTNQRY.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDPCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDPCODICE_1_1.enabled = .t.
      endif
    endwith
    this.GSAR_MPH.SetStatus(i_cOp)
    this.GSAR_ARD.SetStatus(i_cOp)
    this.GSAR_MBR.SetStatus(i_cOp)
    this.GSAR_MSR.SetStatus(i_cOp)
    this.GSAR_MPD.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DIPENDEN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MPH.SetChildrenStatus(i_cOp)
  *  this.GSAR_ARD.SetChildrenStatus(i_cOp)
  *  this.GSAR_MBR.SetChildrenStatus(i_cOp)
  *  this.GSAR_MSR.SetChildrenStatus(i_cOp)
  *  this.GSAR_MPD.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODICE,"DPCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPTIPRIS,"DPTIPRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODANA,"DPCODANA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPBADGE,"DPBADGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDESCRI,"DPDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODCEN,"DPCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPSERPRE,"DPSERPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPGRUPRE,"DPGRUPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCOSORA,"DPCOSORA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCOGNOM,"DPCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPFLVISI,"DPFLVISI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNOME,"DPNOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPFLINEX,"DPFLINEX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPREPART,"DPREPART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDITTA,"DPDITTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODDES,"DPCODDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPMANSION,"DPMANSION",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCOSORA,"DPCOSORA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODCEN,"DPCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODCAL,"DPCODCAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPMODALL,"DPMODALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPTIPCON,"DPTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPSERVIZ,"DPSERVIZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPSERPRE,"DPSERPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPPATHPR,"DPPATHPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DP__PATH,"DP__PATH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DP__DIKE,"DP__DIKE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODFIS,"DPCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPPARIVA,"DPPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNUMCAR,"DPNUMCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DP_SESSO,"DP_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNASLOC,"DPNASLOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNASPRO,"DPNASPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNASNAZ,"DPNASNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDATNAS,"DPDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPTITSTU,"DPTITSTU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPQUALIF,"DPQUALIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPLIVELL,"DPLIVELL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDATASS,"DPDATASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDATFRA,"DPDATFRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRESVIA,"DPRESVIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPINDIR2,"DPINDIR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRESCAP,"DPRESCAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRESLOC,"DPRESLOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRESPRO,"DPRESPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNAZION,"DPNAZION",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDOMVIA,"DPDOMVIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDOMCAP,"DPDOMCAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDOMLOC,"DPDOMLOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDOMPRO,"DPDOMPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPTELEF1,"DPTELEF1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPTELEF2,"DPTELEF2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPINDMAI,"DPINDMAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNUMCEL,"DPNUMCEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNATGIU,"DPNATGIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPPERFIS,"DPPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPINDPEC,"DPINDPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODUTE,"DPCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCHKING,"DPCHKING",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDATINI,"DPDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPGGBLOC,"DPGGBLOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DP_FIRMA,"DP_FIRMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPASSDAL,"DPASSDAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPASSFIN,"DPASSFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODSOS,"DPCODSOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCDUTAL,"DPCDUTAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPFLCONF,"DPFLCONF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODBUN,"DPCODBUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DP__NOTE,"DP__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODUTE,"DPCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DP__NOTE,"DP__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPFLAVVI,"DPFLAVVI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPINDWEB,"DPINDWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODCAL,"DPCODCAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDESCRI2,"DPDESCRI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDTOBSO,"DPDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPVCSICS,"DPVCSICS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRSFASC,"DPRSFASC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODRES,"DPCODRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPPERFAT,"DPPERFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPPERFAC,"DPPERFAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPTIPRIG,"DPTIPRIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCTRPRE,"DPCTRPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPGG_PRE,"DPGG_PRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPGG_SUC,"DPGG_SUC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPFLPROM,"DPFLPROM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsar_adp
    IF Not Empty(i_cWhere)
    i_cWhere=i_cWhere + " AND DPTIPRIS='" + Alltrim(this.w_DPTIPRIS)+"'"
    Else
    i_cWhere="DPTIPRIS='" + Alltrim(this.w_DPTIPRIS)+"'"
    Endif
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    i_lTable = "DIPENDEN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIPENDEN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        PrintConto(this,this.cTipRis)
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIPENDEN_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIPENDEN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIPENDEN')
        i_extval=cp_InsertValODBCExtFlds(this,'DIPENDEN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DPCODICE,DPTIPRIS,DPCODANA,DPBADGE,DPDESCRI"+;
                  ",DPCODCEN,DPSERPRE,DPGRUPRE,DPCOSORA,DPCOGNOM"+;
                  ",DPFLVISI,DPNOME,DPFLINEX,DPREPART,DPDITTA"+;
                  ",DPCODDES,DPMANSION,DPCODCAL,DPMODALL,DPTIPCON"+;
                  ",DPSERVIZ,DPPATHPR,DP__PATH,DP__DIKE,DPCODFIS"+;
                  ",DPPARIVA,DPNUMCAR,DP_SESSO,DPNASLOC,DPNASPRO"+;
                  ",DPNASNAZ,DPDATNAS,DPTITSTU,DPQUALIF,DPLIVELL"+;
                  ",DPDATASS,DPDATFRA,DPRESVIA,DPINDIR2,DPRESCAP"+;
                  ",DPRESLOC,DPRESPRO,DPNAZION,DPDOMVIA,DPDOMCAP"+;
                  ",DPDOMLOC,DPDOMPRO,DPTELEF1,DPTELEF2,DPINDMAI"+;
                  ",DPNUMCEL,DPNATGIU,DPPERFIS,DPINDPEC,DPCHKING"+;
                  ",DPDATINI,DPGGBLOC,DP_FIRMA,DPASSDAL,DPASSFIN"+;
                  ",DPCODSOS,DPCDUTAL,DPFLCONF,DPCODBUN,DPCODUTE"+;
                  ",DP__NOTE,DPFLAVVI,DPINDWEB,DPDESCRI2,DPDTOBSO"+;
                  ",DPVCSICS,DPRSFASC,DPCODRES,DPPERFAT,DPPERFAC"+;
                  ",DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC,DPFLPROM "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DPCODICE)+;
                  ","+cp_ToStrODBC(this.w_DPTIPRIS)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODANA)+;
                  ","+cp_ToStrODBC(this.w_DPBADGE)+;
                  ","+cp_ToStrODBC(this.w_DPDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODCEN)+;
                  ","+cp_ToStrODBCNull(this.w_DPSERPRE)+;
                  ","+cp_ToStrODBCNull(this.w_DPGRUPRE)+;
                  ","+cp_ToStrODBC(this.w_DPCOSORA)+;
                  ","+cp_ToStrODBC(this.w_DPCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_DPFLVISI)+;
                  ","+cp_ToStrODBC(this.w_DPNOME)+;
                  ","+cp_ToStrODBC(this.w_DPFLINEX)+;
                  ","+cp_ToStrODBCNull(this.w_DPREPART)+;
                  ","+cp_ToStrODBCNull(this.w_DPDITTA)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODDES)+;
                  ","+cp_ToStrODBCNull(this.w_DPMANSION)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODCAL)+;
                  ","+cp_ToStrODBC(this.w_DPMODALL)+;
                  ","+cp_ToStrODBC(this.w_DPTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_DPSERVIZ)+;
                  ","+cp_ToStrODBC(this.w_DPPATHPR)+;
                  ","+cp_ToStrODBC(this.w_DP__PATH)+;
                  ","+cp_ToStrODBC(this.w_DP__DIKE)+;
                  ","+cp_ToStrODBC(this.w_DPCODFIS)+;
                  ","+cp_ToStrODBC(this.w_DPPARIVA)+;
                  ","+cp_ToStrODBC(this.w_DPNUMCAR)+;
                  ","+cp_ToStrODBC(this.w_DP_SESSO)+;
                  ","+cp_ToStrODBC(this.w_DPNASLOC)+;
                  ","+cp_ToStrODBC(this.w_DPNASPRO)+;
                  ","+cp_ToStrODBCNull(this.w_DPNASNAZ)+;
                  ","+cp_ToStrODBC(this.w_DPDATNAS)+;
                  ","+cp_ToStrODBC(this.w_DPTITSTU)+;
                  ","+cp_ToStrODBC(this.w_DPQUALIF)+;
                  ","+cp_ToStrODBC(this.w_DPLIVELL)+;
                  ","+cp_ToStrODBC(this.w_DPDATASS)+;
                  ","+cp_ToStrODBC(this.w_DPDATFRA)+;
                  ","+cp_ToStrODBC(this.w_DPRESVIA)+;
                  ","+cp_ToStrODBC(this.w_DPINDIR2)+;
                  ","+cp_ToStrODBC(this.w_DPRESCAP)+;
                  ","+cp_ToStrODBC(this.w_DPRESLOC)+;
                  ","+cp_ToStrODBC(this.w_DPRESPRO)+;
                  ","+cp_ToStrODBCNull(this.w_DPNAZION)+;
                  ","+cp_ToStrODBC(this.w_DPDOMVIA)+;
                  ","+cp_ToStrODBC(this.w_DPDOMCAP)+;
                  ","+cp_ToStrODBC(this.w_DPDOMLOC)+;
                  ","+cp_ToStrODBC(this.w_DPDOMPRO)+;
                  ","+cp_ToStrODBC(this.w_DPTELEF1)+;
                  ","+cp_ToStrODBC(this.w_DPTELEF2)+;
                  ","+cp_ToStrODBC(this.w_DPINDMAI)+;
                  ","+cp_ToStrODBC(this.w_DPNUMCEL)+;
                  ","+cp_ToStrODBC(this.w_DPNATGIU)+;
                  ","+cp_ToStrODBC(this.w_DPPERFIS)+;
                  ","+cp_ToStrODBC(this.w_DPINDPEC)+;
                  ","+cp_ToStrODBC(this.w_DPCHKING)+;
                  ","+cp_ToStrODBC(this.w_DPDATINI)+;
                  ","+cp_ToStrODBC(this.w_DPGGBLOC)+;
                  ","+cp_ToStrODBC(this.w_DP_FIRMA)+;
                  ","+cp_ToStrODBC(this.w_DPASSDAL)+;
                  ","+cp_ToStrODBC(this.w_DPASSFIN)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODSOS)+;
                  ","+cp_ToStrODBCNull(this.w_DPCDUTAL)+;
                  ","+cp_ToStrODBC(this.w_DPFLCONF)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODBUN)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODUTE)+;
                  ","+cp_ToStrODBC(this.w_DP__NOTE)+;
                  ","+cp_ToStrODBC(this.w_DPFLAVVI)+;
                  ","+cp_ToStrODBC(this.w_DPINDWEB)+;
                  ","+cp_ToStrODBC(this.w_DPDESCRI2)+;
                  ","+cp_ToStrODBC(this.w_DPDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_DPVCSICS)+;
                  ","+cp_ToStrODBC(this.w_DPRSFASC)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODRES)+;
                  ","+cp_ToStrODBC(this.w_DPPERFAT)+;
                  ","+cp_ToStrODBC(this.w_DPPERFAC)+;
                  ","+cp_ToStrODBC(this.w_DPTIPRIG)+;
                  ","+cp_ToStrODBC(this.w_DPCTRPRE)+;
                  ","+cp_ToStrODBC(this.w_DPGG_PRE)+;
                  ","+cp_ToStrODBC(this.w_DPGG_SUC)+;
                  ","+cp_ToStrODBC(this.w_DPFLPROM)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIPENDEN')
        i_extval=cp_InsertValVFPExtFlds(this,'DIPENDEN')
        cp_CheckDeletedKey(i_cTable,0,'DPCODICE',this.w_DPCODICE)
        INSERT INTO (i_cTable);
              (DPCODICE,DPTIPRIS,DPCODANA,DPBADGE,DPDESCRI,DPCODCEN,DPSERPRE,DPGRUPRE,DPCOSORA,DPCOGNOM,DPFLVISI,DPNOME,DPFLINEX,DPREPART,DPDITTA,DPCODDES,DPMANSION,DPCODCAL,DPMODALL,DPTIPCON,DPSERVIZ,DPPATHPR,DP__PATH,DP__DIKE,DPCODFIS,DPPARIVA,DPNUMCAR,DP_SESSO,DPNASLOC,DPNASPRO,DPNASNAZ,DPDATNAS,DPTITSTU,DPQUALIF,DPLIVELL,DPDATASS,DPDATFRA,DPRESVIA,DPINDIR2,DPRESCAP,DPRESLOC,DPRESPRO,DPNAZION,DPDOMVIA,DPDOMCAP,DPDOMLOC,DPDOMPRO,DPTELEF1,DPTELEF2,DPINDMAI,DPNUMCEL,DPNATGIU,DPPERFIS,DPINDPEC,DPCHKING,DPDATINI,DPGGBLOC,DP_FIRMA,DPASSDAL,DPASSFIN,DPCODSOS,DPCDUTAL,DPFLCONF,DPCODBUN,DPCODUTE,DP__NOTE,DPFLAVVI,DPINDWEB,DPDESCRI2,DPDTOBSO,DPVCSICS,DPRSFASC,DPCODRES,DPPERFAT,DPPERFAC,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC,DPFLPROM  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DPCODICE;
                  ,this.w_DPTIPRIS;
                  ,this.w_DPCODANA;
                  ,this.w_DPBADGE;
                  ,this.w_DPDESCRI;
                  ,this.w_DPCODCEN;
                  ,this.w_DPSERPRE;
                  ,this.w_DPGRUPRE;
                  ,this.w_DPCOSORA;
                  ,this.w_DPCOGNOM;
                  ,this.w_DPFLVISI;
                  ,this.w_DPNOME;
                  ,this.w_DPFLINEX;
                  ,this.w_DPREPART;
                  ,this.w_DPDITTA;
                  ,this.w_DPCODDES;
                  ,this.w_DPMANSION;
                  ,this.w_DPCODCAL;
                  ,this.w_DPMODALL;
                  ,this.w_DPTIPCON;
                  ,this.w_DPSERVIZ;
                  ,this.w_DPPATHPR;
                  ,this.w_DP__PATH;
                  ,this.w_DP__DIKE;
                  ,this.w_DPCODFIS;
                  ,this.w_DPPARIVA;
                  ,this.w_DPNUMCAR;
                  ,this.w_DP_SESSO;
                  ,this.w_DPNASLOC;
                  ,this.w_DPNASPRO;
                  ,this.w_DPNASNAZ;
                  ,this.w_DPDATNAS;
                  ,this.w_DPTITSTU;
                  ,this.w_DPQUALIF;
                  ,this.w_DPLIVELL;
                  ,this.w_DPDATASS;
                  ,this.w_DPDATFRA;
                  ,this.w_DPRESVIA;
                  ,this.w_DPINDIR2;
                  ,this.w_DPRESCAP;
                  ,this.w_DPRESLOC;
                  ,this.w_DPRESPRO;
                  ,this.w_DPNAZION;
                  ,this.w_DPDOMVIA;
                  ,this.w_DPDOMCAP;
                  ,this.w_DPDOMLOC;
                  ,this.w_DPDOMPRO;
                  ,this.w_DPTELEF1;
                  ,this.w_DPTELEF2;
                  ,this.w_DPINDMAI;
                  ,this.w_DPNUMCEL;
                  ,this.w_DPNATGIU;
                  ,this.w_DPPERFIS;
                  ,this.w_DPINDPEC;
                  ,this.w_DPCHKING;
                  ,this.w_DPDATINI;
                  ,this.w_DPGGBLOC;
                  ,this.w_DP_FIRMA;
                  ,this.w_DPASSDAL;
                  ,this.w_DPASSFIN;
                  ,this.w_DPCODSOS;
                  ,this.w_DPCDUTAL;
                  ,this.w_DPFLCONF;
                  ,this.w_DPCODBUN;
                  ,this.w_DPCODUTE;
                  ,this.w_DP__NOTE;
                  ,this.w_DPFLAVVI;
                  ,this.w_DPINDWEB;
                  ,this.w_DPDESCRI2;
                  ,this.w_DPDTOBSO;
                  ,this.w_DPVCSICS;
                  ,this.w_DPRSFASC;
                  ,this.w_DPCODRES;
                  ,this.w_DPPERFAT;
                  ,this.w_DPPERFAC;
                  ,this.w_DPTIPRIG;
                  ,this.w_DPCTRPRE;
                  ,this.w_DPGG_PRE;
                  ,this.w_DPGG_SUC;
                  ,this.w_DPFLPROM;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIPENDEN_IDX,i_nConn)
      *
      * update DIPENDEN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIPENDEN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS)+;
             ",DPCODANA="+cp_ToStrODBCNull(this.w_DPCODANA)+;
             ",DPBADGE="+cp_ToStrODBC(this.w_DPBADGE)+;
             ",DPDESCRI="+cp_ToStrODBC(this.w_DPDESCRI)+;
             ",DPCODCEN="+cp_ToStrODBCNull(this.w_DPCODCEN)+;
             ",DPSERPRE="+cp_ToStrODBCNull(this.w_DPSERPRE)+;
             ",DPGRUPRE="+cp_ToStrODBCNull(this.w_DPGRUPRE)+;
             ",DPCOSORA="+cp_ToStrODBC(this.w_DPCOSORA)+;
             ",DPCOGNOM="+cp_ToStrODBC(this.w_DPCOGNOM)+;
             ",DPFLVISI="+cp_ToStrODBC(this.w_DPFLVISI)+;
             ",DPNOME="+cp_ToStrODBC(this.w_DPNOME)+;
             ",DPFLINEX="+cp_ToStrODBC(this.w_DPFLINEX)+;
             ",DPREPART="+cp_ToStrODBCNull(this.w_DPREPART)+;
             ",DPDITTA="+cp_ToStrODBCNull(this.w_DPDITTA)+;
             ",DPCODDES="+cp_ToStrODBCNull(this.w_DPCODDES)+;
             ",DPMANSION="+cp_ToStrODBCNull(this.w_DPMANSION)+;
             ",DPCODCAL="+cp_ToStrODBCNull(this.w_DPCODCAL)+;
             ",DPMODALL="+cp_ToStrODBC(this.w_DPMODALL)+;
             ",DPTIPCON="+cp_ToStrODBC(this.w_DPTIPCON)+;
             ",DPSERVIZ="+cp_ToStrODBCNull(this.w_DPSERVIZ)+;
             ",DPPATHPR="+cp_ToStrODBC(this.w_DPPATHPR)+;
             ",DP__PATH="+cp_ToStrODBC(this.w_DP__PATH)+;
             ",DP__DIKE="+cp_ToStrODBC(this.w_DP__DIKE)+;
             ",DPCODFIS="+cp_ToStrODBC(this.w_DPCODFIS)+;
             ",DPPARIVA="+cp_ToStrODBC(this.w_DPPARIVA)+;
             ",DPNUMCAR="+cp_ToStrODBC(this.w_DPNUMCAR)+;
             ",DP_SESSO="+cp_ToStrODBC(this.w_DP_SESSO)+;
             ",DPNASLOC="+cp_ToStrODBC(this.w_DPNASLOC)+;
             ",DPNASPRO="+cp_ToStrODBC(this.w_DPNASPRO)+;
             ",DPNASNAZ="+cp_ToStrODBCNull(this.w_DPNASNAZ)+;
             ",DPDATNAS="+cp_ToStrODBC(this.w_DPDATNAS)+;
             ",DPTITSTU="+cp_ToStrODBC(this.w_DPTITSTU)+;
             ",DPQUALIF="+cp_ToStrODBC(this.w_DPQUALIF)+;
             ",DPLIVELL="+cp_ToStrODBC(this.w_DPLIVELL)+;
             ",DPDATASS="+cp_ToStrODBC(this.w_DPDATASS)+;
             ",DPDATFRA="+cp_ToStrODBC(this.w_DPDATFRA)+;
             ",DPRESVIA="+cp_ToStrODBC(this.w_DPRESVIA)+;
             ",DPINDIR2="+cp_ToStrODBC(this.w_DPINDIR2)+;
             ",DPRESCAP="+cp_ToStrODBC(this.w_DPRESCAP)+;
             ",DPRESLOC="+cp_ToStrODBC(this.w_DPRESLOC)+;
             ",DPRESPRO="+cp_ToStrODBC(this.w_DPRESPRO)+;
             ",DPNAZION="+cp_ToStrODBCNull(this.w_DPNAZION)+;
             ",DPDOMVIA="+cp_ToStrODBC(this.w_DPDOMVIA)+;
             ",DPDOMCAP="+cp_ToStrODBC(this.w_DPDOMCAP)+;
             ",DPDOMLOC="+cp_ToStrODBC(this.w_DPDOMLOC)+;
             ",DPDOMPRO="+cp_ToStrODBC(this.w_DPDOMPRO)+;
             ",DPTELEF1="+cp_ToStrODBC(this.w_DPTELEF1)+;
             ",DPTELEF2="+cp_ToStrODBC(this.w_DPTELEF2)+;
             ",DPINDMAI="+cp_ToStrODBC(this.w_DPINDMAI)+;
             ",DPNUMCEL="+cp_ToStrODBC(this.w_DPNUMCEL)+;
             ",DPNATGIU="+cp_ToStrODBC(this.w_DPNATGIU)+;
             ",DPPERFIS="+cp_ToStrODBC(this.w_DPPERFIS)+;
             ",DPINDPEC="+cp_ToStrODBC(this.w_DPINDPEC)+;
             ",DPCHKING="+cp_ToStrODBC(this.w_DPCHKING)+;
             ",DPDATINI="+cp_ToStrODBC(this.w_DPDATINI)+;
             ",DPGGBLOC="+cp_ToStrODBC(this.w_DPGGBLOC)+;
             ",DP_FIRMA="+cp_ToStrODBC(this.w_DP_FIRMA)+;
             ",DPASSDAL="+cp_ToStrODBC(this.w_DPASSDAL)+;
             ",DPASSFIN="+cp_ToStrODBC(this.w_DPASSFIN)+;
             ",DPCODSOS="+cp_ToStrODBCNull(this.w_DPCODSOS)+;
             ",DPCDUTAL="+cp_ToStrODBCNull(this.w_DPCDUTAL)+;
             ",DPFLCONF="+cp_ToStrODBC(this.w_DPFLCONF)+;
             ",DPCODBUN="+cp_ToStrODBCNull(this.w_DPCODBUN)+;
             ",DPCODUTE="+cp_ToStrODBCNull(this.w_DPCODUTE)+;
             ",DP__NOTE="+cp_ToStrODBC(this.w_DP__NOTE)+;
             ",DPFLAVVI="+cp_ToStrODBC(this.w_DPFLAVVI)+;
             ",DPINDWEB="+cp_ToStrODBC(this.w_DPINDWEB)+;
             ",DPDESCRI2="+cp_ToStrODBC(this.w_DPDESCRI2)+;
             ",DPDTOBSO="+cp_ToStrODBC(this.w_DPDTOBSO)+;
             ",DPVCSICS="+cp_ToStrODBC(this.w_DPVCSICS)+;
             ",DPRSFASC="+cp_ToStrODBC(this.w_DPRSFASC)+;
             ",DPCODRES="+cp_ToStrODBCNull(this.w_DPCODRES)+;
             ",DPPERFAT="+cp_ToStrODBC(this.w_DPPERFAT)+;
             ",DPPERFAC="+cp_ToStrODBC(this.w_DPPERFAC)+;
             ",DPTIPRIG="+cp_ToStrODBC(this.w_DPTIPRIG)+;
             ",DPCTRPRE="+cp_ToStrODBC(this.w_DPCTRPRE)+;
             ",DPGG_PRE="+cp_ToStrODBC(this.w_DPGG_PRE)+;
             ",DPGG_SUC="+cp_ToStrODBC(this.w_DPGG_SUC)+;
             ",DPFLPROM="+cp_ToStrODBC(this.w_DPFLPROM)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIPENDEN')
        i_cWhere = cp_PKFox(i_cTable  ,'DPCODICE',this.w_DPCODICE  )
        UPDATE (i_cTable) SET;
              DPTIPRIS=this.w_DPTIPRIS;
             ,DPCODANA=this.w_DPCODANA;
             ,DPBADGE=this.w_DPBADGE;
             ,DPDESCRI=this.w_DPDESCRI;
             ,DPCODCEN=this.w_DPCODCEN;
             ,DPSERPRE=this.w_DPSERPRE;
             ,DPGRUPRE=this.w_DPGRUPRE;
             ,DPCOSORA=this.w_DPCOSORA;
             ,DPCOGNOM=this.w_DPCOGNOM;
             ,DPFLVISI=this.w_DPFLVISI;
             ,DPNOME=this.w_DPNOME;
             ,DPFLINEX=this.w_DPFLINEX;
             ,DPREPART=this.w_DPREPART;
             ,DPDITTA=this.w_DPDITTA;
             ,DPCODDES=this.w_DPCODDES;
             ,DPMANSION=this.w_DPMANSION;
             ,DPCODCAL=this.w_DPCODCAL;
             ,DPMODALL=this.w_DPMODALL;
             ,DPTIPCON=this.w_DPTIPCON;
             ,DPSERVIZ=this.w_DPSERVIZ;
             ,DPPATHPR=this.w_DPPATHPR;
             ,DP__PATH=this.w_DP__PATH;
             ,DP__DIKE=this.w_DP__DIKE;
             ,DPCODFIS=this.w_DPCODFIS;
             ,DPPARIVA=this.w_DPPARIVA;
             ,DPNUMCAR=this.w_DPNUMCAR;
             ,DP_SESSO=this.w_DP_SESSO;
             ,DPNASLOC=this.w_DPNASLOC;
             ,DPNASPRO=this.w_DPNASPRO;
             ,DPNASNAZ=this.w_DPNASNAZ;
             ,DPDATNAS=this.w_DPDATNAS;
             ,DPTITSTU=this.w_DPTITSTU;
             ,DPQUALIF=this.w_DPQUALIF;
             ,DPLIVELL=this.w_DPLIVELL;
             ,DPDATASS=this.w_DPDATASS;
             ,DPDATFRA=this.w_DPDATFRA;
             ,DPRESVIA=this.w_DPRESVIA;
             ,DPINDIR2=this.w_DPINDIR2;
             ,DPRESCAP=this.w_DPRESCAP;
             ,DPRESLOC=this.w_DPRESLOC;
             ,DPRESPRO=this.w_DPRESPRO;
             ,DPNAZION=this.w_DPNAZION;
             ,DPDOMVIA=this.w_DPDOMVIA;
             ,DPDOMCAP=this.w_DPDOMCAP;
             ,DPDOMLOC=this.w_DPDOMLOC;
             ,DPDOMPRO=this.w_DPDOMPRO;
             ,DPTELEF1=this.w_DPTELEF1;
             ,DPTELEF2=this.w_DPTELEF2;
             ,DPINDMAI=this.w_DPINDMAI;
             ,DPNUMCEL=this.w_DPNUMCEL;
             ,DPNATGIU=this.w_DPNATGIU;
             ,DPPERFIS=this.w_DPPERFIS;
             ,DPINDPEC=this.w_DPINDPEC;
             ,DPCHKING=this.w_DPCHKING;
             ,DPDATINI=this.w_DPDATINI;
             ,DPGGBLOC=this.w_DPGGBLOC;
             ,DP_FIRMA=this.w_DP_FIRMA;
             ,DPASSDAL=this.w_DPASSDAL;
             ,DPASSFIN=this.w_DPASSFIN;
             ,DPCODSOS=this.w_DPCODSOS;
             ,DPCDUTAL=this.w_DPCDUTAL;
             ,DPFLCONF=this.w_DPFLCONF;
             ,DPCODBUN=this.w_DPCODBUN;
             ,DPCODUTE=this.w_DPCODUTE;
             ,DP__NOTE=this.w_DP__NOTE;
             ,DPFLAVVI=this.w_DPFLAVVI;
             ,DPINDWEB=this.w_DPINDWEB;
             ,DPDESCRI2=this.w_DPDESCRI2;
             ,DPDTOBSO=this.w_DPDTOBSO;
             ,DPVCSICS=this.w_DPVCSICS;
             ,DPRSFASC=this.w_DPRSFASC;
             ,DPCODRES=this.w_DPCODRES;
             ,DPPERFAT=this.w_DPPERFAT;
             ,DPPERFAC=this.w_DPPERFAC;
             ,DPTIPRIG=this.w_DPTIPRIG;
             ,DPCTRPRE=this.w_DPCTRPRE;
             ,DPGG_PRE=this.w_DPGG_PRE;
             ,DPGG_SUC=this.w_DPGG_SUC;
             ,DPFLPROM=this.w_DPFLPROM;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAR_MPH : Saving
      this.GSAR_MPH.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPCODICE,"APCODGRU";
             ,this.w_DPCODICE,"APPERSON";
             )
      this.GSAR_MPH.mReplace()
      * --- GSAR_ARD : Saving
      this.GSAR_ARD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CODANA,"DRCODANA";
             ,this.w_AZIENDA,"DRCODAZI";
             ,this.w_CODRAP,"DRCODRAP";
             ,this.w_CODSOC,"DRCODSOC";
             )
      this.GSAR_ARD.mReplace()
      * --- GSAR_MBR : Saving
      this.GSAR_MBR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPCODICE,"RBCODICE";
             )
      this.GSAR_MBR.mReplace()
      * --- GSAR_MSR : Saving
      this.GSAR_MSR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPCODICE,"SRCODICE";
             )
      this.GSAR_MSR.mReplace()
      * --- GSAR_MPD : Saving
      this.GSAR_MPD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPCODICE,"PDCODPER";
             )
      this.GSAR_MPD.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAR_MPH : Deleting
    this.GSAR_MPH.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPCODICE,"APCODGRU";
           ,this.w_DPCODICE,"APPERSON";
           )
    this.GSAR_MPH.mDelete()
    * --- GSAR_ARD : Deleting
    this.GSAR_ARD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CODANA,"DRCODANA";
           ,this.w_AZIENDA,"DRCODAZI";
           ,this.w_CODRAP,"DRCODRAP";
           ,this.w_CODSOC,"DRCODSOC";
           )
    this.GSAR_ARD.mDelete()
    * --- GSAR_MBR : Deleting
    this.GSAR_MBR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPCODICE,"RBCODICE";
           )
    this.GSAR_MBR.mDelete()
    * --- GSAR_MSR : Deleting
    this.GSAR_MSR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPCODICE,"SRCODICE";
           )
    this.GSAR_MSR.mDelete()
    * --- GSAR_MPD : Deleting
    this.GSAR_MPD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPCODICE,"PDCODPER";
           )
    this.GSAR_MPD.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIPENDEN_IDX,i_nConn)
      *
      * delete DIPENDEN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DPCODICE',this.w_DPCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_DPTIPRIS = this.cTipRis
        .DoRTCalc(3,5,.t.)
          .link_1_7('Full')
          .link_1_8('Full')
          .link_1_9('Full')
        .DoRTCalc(9,24,.t.)
        if .o_DPFLINEX<>.w_DPFLINEX
            .w_DPREPART = iif(.w_DPFLINEX='E',space(15),.w_DPREPART)
          .link_1_26('Full')
        endif
        .DoRTCalc(26,26,.t.)
        if .o_DPFLINEX<>.w_DPFLINEX
            .w_DPDITTA = iif(.w_DPFLINEX='I',space(15),.w_DPDITTA)
          .link_1_28('Full')
        endif
        if .o_DPFLINEX<>.w_DPFLINEX
            .w_DPCODDES = iif(.w_DPFLINEX="E",space(15),.w_DPCODDES)
          .link_1_29('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .DoRTCalc(29,47,.t.)
            .w_DPKEYPAR = 'TAM'
          .link_1_64('Full')
        .oPgFrm.Page1.oPag.LBLVAL.Calculate(IIF(.w_DPTIPRIS<>'P', "", "[" + ALLTRIM(GETVALUT(g_PERVAL, "VASIMVAL")) + "]"))
        .DoRTCalc(49,51,.t.)
            .w_CODANA = .w_DPCODANA
        .DoRTCalc(53,54,.t.)
            .w_CODSOC = .w_DPCODICE
            .w_CODANA = .w_DPCODANA
        .DoRTCalc(57,57,.t.)
            .w_RAPOBSO = .w_DPDATFRA
        .DoRTCalc(59,60,.t.)
        if .o_DPCODFIS<>.w_DPCODFIS
            .w_CODIFISC = iif(g_ANACOM='S',.w_CODIFISC,.w_DPCODFIS)
        endif
        .DoRTCalc(62,64,.t.)
        if .o_DPCODANA<>.w_DPCODANA
            .w_DPNASPRO = iif(g_ANACOM<>'S',.w_DPNASPRO,iif(empty(nvl(.w_APRONAS,'')),.w_PRONAS1,.w_APRONAS))
        endif
        .DoRTCalc(66,77,.t.)
        if .o_DPCODANA<>.w_DPCODANA
            .w_DPRESPRO = iif(g_ANACOM<>'S',.w_DPRESPRO,iif(empty(nvl(.w_APROVIN,'')),.w_PROVIN1,.w_APROVIN))
        endif
        if .o_ANAZION<>.w_ANAZION
            .w_DPNAZION = iif(g_ANACOM<>'S',.w_DPNAZION,.w_ANAZION)
          .link_3_9('Full')
        endif
        .DoRTCalc(80,90,.t.)
            .w_DPPERFIS = .w_PERFIS
        .oPgFrm.Page4.oPag.FIRMAIMG.Calculate(.w_DP_FIRMA)
        .oPgFrm.Page4.oPag.oObj_4_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate(IIF(IsAlt(), "Costo orario standard (espresso in Euro)", "Costo orario standard (espresso in valuta di conto)"))
        if  .o_DPCODICE<>.w_DPCODICE
          .WriteTo_GSAR_MSR()
        endif
        if .o_CODIFISC<>.w_CODIFISC
          .Calculate_WAZFTOAGEQ()
        endif
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        if .o_DPFLVISI<>.w_DPFLVISI
          .Calculate_ZRNFGZTAZL()
        endif
        if .o_DPCODUTE<>.w_DPCODUTE
          .Calculate_SXWNQXAXNP()
        endif
        if .o_DPPATHPR<>.w_DPPATHPR
          .Calculate_XEHSDROKJL()
        endif
        if .o_DP__PATH<>.w_DP__PATH
          .Calculate_FORBCAQGLL()
        endif
        if  .o_DPCOGNOM<>.w_DPCOGNOM.or. .o_DPNOME<>.w_DPNOME
          .WriteTo_GSAR_MPD()
        endif
        .DoRTCalc(92,135,.t.)
        if .o_LetturaParAgen<>.w_LetturaParAgen
            .w_LetturaParAgen = i_CodAzi
          .link_7_1('Full')
        endif
        .DoRTCalc(137,139,.t.)
        if .o_DPPERFAT<>.w_DPPERFAT
            .w_DPPERFAC = .w_DPPERFAT
        endif
        if .o_DPTIPRIG<>.w_DPTIPRIG
            .w_DPTIPRIG = IIF(EMPTY(.w_DPTIPRIG),'D',.w_DPTIPRIG)
        endif
        if .o_DPCODRES<>.w_DPCODRES
            .w_DENOMRES = alltrim(.w_COGNRES)+ space(1)+alltrim(.w_NOMERES)
        endif
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(143,147,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .oPgFrm.Page1.oPag.LBLVAL.Calculate(IIF(.w_DPTIPRIS<>'P', "", "[" + ALLTRIM(GETVALUT(g_PERVAL, "VASIMVAL")) + "]"))
        .oPgFrm.Page4.oPag.FIRMAIMG.Calculate(.w_DP_FIRMA)
        .oPgFrm.Page4.oPag.oObj_4_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate(IIF(IsAlt(), "Costo orario standard (espresso in Euro)", "Costo orario standard (espresso in valuta di conto)"))
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
    endwith
  return

  proc Calculate_FWZWGRCBNC()
    with this
          * --- VERIFICA ASSOCIAZIONE GRUPPO - GSAR_BDP(CANCGRUPPO) E CODICE PERSONA
          gsar_bdp(this;
              ,'CANCGRUPPO';
             )
    endwith
  endproc
  proc Calculate_WAZFTOAGEQ()
    with this
          * --- w_CODIFISC changed
          .w_ERR = cfanag(.w_DPCODFIS,'V')
          .w_DPDATNAS = iif(.w_ERR, .w_DPDATNAS, IIF (.w_CONTA<>.o_CONTA, .w_DPDATNAS ,cfanag(.w_DPCODFIS,'D') ))
          .w_DP_SESSO = iif(.w_ERR, .w_DP_SESSO, cfanag(.w_DPCODFIS,'S'))
          .w_DPNASLOC = iif(.w_ERR,.w_DPNASLOC, cfanag(.w_DPCODFIS,'L'))
          .w_DPNASPRO = iif(.w_ERR, .w_DPNASPRO, cfanag(.w_DPCODFIS,'P'))
          .w_CODCOM = iif(.w_ERR, .w_CODCOM , SUBSTR(.w_DPCODFIS,12,4))
    endwith
  endproc
  proc Calculate_ZYAELGIHGA()
    with this
          * --- Richiesta manutenzione struttura aziendale - GSAR_BDP(MANUTSTRAZ)
          gsar_bdp(this;
              ,'MANUTSTRAZ';
             )
    endwith
  endproc
  proc Calculate_YRLCIPZNOW()
    with this
          * --- New record
          .GSAR_ARD.bUpdated = IIF(isAHE() AND .w_DPTIPRIS='P' AND g_ANACOM='S',.t.,.f.)
    endwith
  endproc
  proc Calculate_ZRNFGZTAZL()
    with this
          * --- Flag modifica visibilit� per richiesta elaborazione struttura
          .w_CHKVARVI = .T.
    endwith
  endproc
  proc Calculate_HDGHWRKUWL()
    with this
          * --- Azzera il campo Giorni
          .w_DPCHKING = IIF(EMPTY(NVL(.w_DPCHKING, '')), 'N', .w_DPCHKING)
          .w_DPGGBLOC = IIF(.w_DPCHKING<>'B', 0, .w_DPGGBLOC)
          .w_DPDATINI = IIF(.w_DPCHKING='N', Cp_CharToDate('  -  -    '), .w_DPDATINI)
    endwith
  endproc
  proc Calculate_SXWNQXAXNP()
    with this
          * --- Resetta campi
          .w_DPGGBLOC = IIF(EMPTY(.w_DPCODUTE), 0, .w_DPGGBLOC)
          .w_DPCHKING = IIF(EMPTY(.w_DPCODUTE), 'N', .w_DPCHKING)
          .w_DPDATINI = IIF(EMPTY(.w_DPCODUTE), Cp_CharToDate('  -  -    '), .w_DPDATINI)
    endwith
  endproc
  proc Calculate_PGQNPFGCXZ()
    with this
          * --- Colora pagina note
          GSUT_BCN(this;
              ,"COLORA";
              ,.w_DP__NOTE;
             )
    endwith
  endproc
  proc Calculate_GRJFLMWOLU()
    with this
          * --- Numero pagina note
          GSUT_BCN(this;
              ,"PAGINA";
              ,"Note";
             )
    endwith
  endproc
  proc Calculate_XEHSDROKJL()
    with this
          * --- Controllo path prelevamento
          .w_DPPATHPR = IIF(right(alltrim(.w_DPPATHPR),1)='\' or empty(.w_DPPATHPR),.w_DPPATHPR,alltrim(.w_DPPATHPR)+iif(len(alltrim(.w_DPPATHPR))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_DPPATHPR,'F')
    endwith
  endproc
  proc Calculate_FORBCAQGLL()
    with this
          * --- Controllo path print system
          .w_DP__PATH = IIF(right(alltrim(.w_DP__PATH),1)='\' or empty(.w_DP__PATH),.w_DP__PATH,alltrim(.w_DP__PATH)+iif(len(alltrim(.w_DP__PATH))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_DP__PATH,'F')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDCLMAST_1_3.enabled = this.oPgFrm.Page1.oPag.oDCLMAST_1_3.mCond()
    this.oPgFrm.Page1.oPag.oDPDESCRI_1_17.enabled = this.oPgFrm.Page1.oPag.oDPDESCRI_1_17.mCond()
    this.oPgFrm.Page1.oPag.oDPCODCEN_1_18.enabled = this.oPgFrm.Page1.oPag.oDPCODCEN_1_18.mCond()
    this.oPgFrm.Page1.oPag.oDPGRUPRE_1_20.enabled = this.oPgFrm.Page1.oPag.oDPGRUPRE_1_20.mCond()
    this.oPgFrm.Page1.oPag.oDPCOGNOM_1_22.enabled = this.oPgFrm.Page1.oPag.oDPCOGNOM_1_22.mCond()
    this.oPgFrm.Page1.oPag.oDPNOME_1_24.enabled = this.oPgFrm.Page1.oPag.oDPNOME_1_24.mCond()
    this.oPgFrm.Page1.oPag.oDPREPART_1_26.enabled = this.oPgFrm.Page1.oPag.oDPREPART_1_26.mCond()
    this.oPgFrm.Page1.oPag.oDPCODCAL_1_35.enabled = this.oPgFrm.Page1.oPag.oDPCODCAL_1_35.mCond()
    this.oPgFrm.Page2.oPag.oDPCODFIS_2_1.enabled = this.oPgFrm.Page2.oPag.oDPCODFIS_2_1.mCond()
    this.oPgFrm.Page2.oPag.oDPPARIVA_2_2.enabled = this.oPgFrm.Page2.oPag.oDPPARIVA_2_2.mCond()
    this.oPgFrm.Page2.oPag.oDPNUMCAR_2_4.enabled = this.oPgFrm.Page2.oPag.oDPNUMCAR_2_4.mCond()
    this.oPgFrm.Page2.oPag.oDP_SESSO_2_5.enabled = this.oPgFrm.Page2.oPag.oDP_SESSO_2_5.mCond()
    this.oPgFrm.Page2.oPag.oDPNASLOC_2_7.enabled = this.oPgFrm.Page2.oPag.oDPNASLOC_2_7.mCond()
    this.oPgFrm.Page2.oPag.oDPNASPRO_2_10.enabled = this.oPgFrm.Page2.oPag.oDPNASPRO_2_10.mCond()
    this.oPgFrm.Page2.oPag.oDPDATNAS_2_14.enabled = this.oPgFrm.Page2.oPag.oDPDATNAS_2_14.mCond()
    this.oPgFrm.Page3.oPag.oDPRESVIA_3_1.enabled = this.oPgFrm.Page3.oPag.oDPRESVIA_3_1.mCond()
    this.oPgFrm.Page3.oPag.oDPINDIR2_3_2.enabled = this.oPgFrm.Page3.oPag.oDPINDIR2_3_2.mCond()
    this.oPgFrm.Page3.oPag.oDPRESCAP_3_5.enabled = this.oPgFrm.Page3.oPag.oDPRESCAP_3_5.mCond()
    this.oPgFrm.Page3.oPag.oDPRESLOC_3_6.enabled = this.oPgFrm.Page3.oPag.oDPRESLOC_3_6.mCond()
    this.oPgFrm.Page3.oPag.oDPRESPRO_3_8.enabled = this.oPgFrm.Page3.oPag.oDPRESPRO_3_8.mCond()
    this.oPgFrm.Page3.oPag.oDPNAZION_3_9.enabled = this.oPgFrm.Page3.oPag.oDPNAZION_3_9.mCond()
    this.oPgFrm.Page3.oPag.oDPTELEF1_3_24.enabled = this.oPgFrm.Page3.oPag.oDPTELEF1_3_24.mCond()
    this.oPgFrm.Page3.oPag.oDPTELEF2_3_25.enabled = this.oPgFrm.Page3.oPag.oDPTELEF2_3_25.mCond()
    this.oPgFrm.Page3.oPag.oDPINDMAI_3_27.enabled = this.oPgFrm.Page3.oPag.oDPINDMAI_3_27.mCond()
    this.oPgFrm.Page3.oPag.oDPNUMCEL_3_31.enabled = this.oPgFrm.Page3.oPag.oDPNUMCEL_3_31.mCond()
    this.oPgFrm.Page3.oPag.oDPINDPEC_3_35.enabled = this.oPgFrm.Page3.oPag.oDPINDPEC_3_35.mCond()
    this.oPgFrm.Page1.oPag.oDP__NOTE_1_82.enabled = this.oPgFrm.Page1.oPag.oDP__NOTE_1_82.mCond()
    this.oPgFrm.Page3.oPag.oDPINDWEB_3_38.enabled = this.oPgFrm.Page3.oPag.oDPINDWEB_3_38.mCond()
    this.oPgFrm.Page3.oPag.oDPVCSICS_3_42.enabled = this.oPgFrm.Page3.oPag.oDPVCSICS_3_42.mCond()
    this.oPgFrm.Page7.oPag.oDPRSFASC_7_2.enabled = this.oPgFrm.Page7.oPag.oDPRSFASC_7_2.mCond()
    this.oPgFrm.Page7.oPag.oDPCODRES_7_3.enabled = this.oPgFrm.Page7.oPag.oDPCODRES_7_3.mCond()
    this.oPgFrm.Page2.oPag.oLinkPC_2_35.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_35.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(this.cTipRis<>'P')
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Anagrafici"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    local i_show2
    i_show2=not(this.cTipRis<>'P')
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Recapiti"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    local i_show3
    i_show3=not(this.cTipRis<>'P')
    this.oPgFrm.Pages(4).enabled=i_show3
    this.oPgFrm.Pages(4).caption=iif(i_show3,cp_translate("Identificazione"),"")
    this.oPgFrm.Pages(4).oPag.visible=this.oPgFrm.Pages(4).enabled
    local i_show4
    i_show4=not(this.cTipRis<>'P' OR VARTYPE(g_BUAN)<>'C' OR g_BUAN<>'S')
    this.oPgFrm.Pages(5).enabled=i_show4
    this.oPgFrm.Pages(5).caption=iif(i_show4,cp_translate("Budget"),"")
    this.oPgFrm.Pages(5).oPag.visible=this.oPgFrm.Pages(5).enabled
    local i_show5
    i_show5=not(this.cTipRis=='R')
    this.oPgFrm.Pages(6).enabled=i_show5
    this.oPgFrm.Pages(6).caption=iif(i_show5,cp_translate("Note"),"")
    this.oPgFrm.Pages(6).oPag.visible=this.oPgFrm.Pages(6).enabled
    local i_show6
    i_show6=not(NOT ISALT() OR this.w_cTipRis<>'P')
    this.oPgFrm.Pages(7).enabled=i_show6
    this.oPgFrm.Pages(7).caption=iif(i_show6,cp_translate("Prestazioni"),"")
    this.oPgFrm.Pages(7).oPag.visible=this.oPgFrm.Pages(7).enabled
    this.oPgFrm.Page1.oPag.oDCLMAST_1_3.visible=!this.oPgFrm.Page1.oPag.oDCLMAST_1_3.mHide()
    this.oPgFrm.Page1.oPag.oDPCODANA_1_4.visible=!this.oPgFrm.Page1.oPag.oDPCODANA_1_4.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_5.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_5.mHide()
    this.oPgFrm.Page1.oPag.oDPBADGE_1_16.visible=!this.oPgFrm.Page1.oPag.oDPBADGE_1_16.mHide()
    this.oPgFrm.Page1.oPag.oDPDESCRI_1_17.visible=!this.oPgFrm.Page1.oPag.oDPDESCRI_1_17.mHide()
    this.oPgFrm.Page1.oPag.oDPCODCEN_1_18.visible=!this.oPgFrm.Page1.oPag.oDPCODCEN_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDPSERPRE_1_19.visible=!this.oPgFrm.Page1.oPag.oDPSERPRE_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDPGRUPRE_1_20.visible=!this.oPgFrm.Page1.oPag.oDPGRUPRE_1_20.mHide()
    this.oPgFrm.Page1.oPag.oDPCOSORA_1_21.visible=!this.oPgFrm.Page1.oPag.oDPCOSORA_1_21.mHide()
    this.oPgFrm.Page1.oPag.oDPCOGNOM_1_22.visible=!this.oPgFrm.Page1.oPag.oDPCOGNOM_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDPFLVISI_1_23.visible=!this.oPgFrm.Page1.oPag.oDPFLVISI_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDPNOME_1_24.visible=!this.oPgFrm.Page1.oPag.oDPNOME_1_24.mHide()
    this.oPgFrm.Page1.oPag.oDPFLINEX_1_25.visible=!this.oPgFrm.Page1.oPag.oDPFLINEX_1_25.mHide()
    this.oPgFrm.Page1.oPag.oDPREPART_1_26.visible=!this.oPgFrm.Page1.oPag.oDPREPART_1_26.mHide()
    this.oPgFrm.Page1.oPag.oDESRIS_1_27.visible=!this.oPgFrm.Page1.oPag.oDESRIS_1_27.mHide()
    this.oPgFrm.Page1.oPag.oDPDITTA_1_28.visible=!this.oPgFrm.Page1.oPag.oDPDITTA_1_28.mHide()
    this.oPgFrm.Page1.oPag.oDPCODDES_1_29.visible=!this.oPgFrm.Page1.oPag.oDPCODDES_1_29.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_30.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDPMANSION_1_31.visible=!this.oPgFrm.Page1.oPag.oDPMANSION_1_31.mHide()
    this.oPgFrm.Page1.oPag.oDESMAN_1_32.visible=!this.oPgFrm.Page1.oPag.oDESMAN_1_32.mHide()
    this.oPgFrm.Page1.oPag.oDPCOSORA_1_33.visible=!this.oPgFrm.Page1.oPag.oDPCOSORA_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDPCODCEN_1_34.visible=!this.oPgFrm.Page1.oPag.oDPCODCEN_1_34.mHide()
    this.oPgFrm.Page1.oPag.oDPCODCAL_1_35.visible=!this.oPgFrm.Page1.oPag.oDPCODCAL_1_35.mHide()
    this.oPgFrm.Page1.oPag.oDPMODALL_1_36.visible=!this.oPgFrm.Page1.oPag.oDPMODALL_1_36.mHide()
    this.oPgFrm.Page1.oPag.oDPSERVIZ_1_38.visible=!this.oPgFrm.Page1.oPag.oDPSERVIZ_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDESSER_1_39.visible=!this.oPgFrm.Page1.oPag.oDESSER_1_39.mHide()
    this.oPgFrm.Page1.oPag.oDPSERPRE_1_40.visible=!this.oPgFrm.Page1.oPag.oDPSERPRE_1_40.mHide()
    this.oPgFrm.Page1.oPag.oDPPATHPR_1_41.visible=!this.oPgFrm.Page1.oPag.oDPPATHPR_1_41.mHide()
    this.oPgFrm.Page1.oPag.oDP__PATH_1_42.visible=!this.oPgFrm.Page1.oPag.oDP__PATH_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDP__DIKE_1_43.visible=!this.oPgFrm.Page1.oPag.oDP__DIKE_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oDESSED_1_62.visible=!this.oPgFrm.Page1.oPag.oDESSED_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_76.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_76.mHide()
    this.oPgFrm.Page3.oPag.oDPINDPEC_3_35.visible=!this.oPgFrm.Page3.oPag.oDPINDPEC_3_35.mHide()
    this.oPgFrm.Page4.oPag.oDPCHKING_4_4.visible=!this.oPgFrm.Page4.oPag.oDPCHKING_4_4.mHide()
    this.oPgFrm.Page4.oPag.oDPDATINI_4_5.visible=!this.oPgFrm.Page4.oPag.oDPDATINI_4_5.mHide()
    this.oPgFrm.Page4.oPag.oDPGGBLOC_4_6.visible=!this.oPgFrm.Page4.oPag.oDPGGBLOC_4_6.mHide()
    this.oPgFrm.Page5.oPag.oDPCODBUN_5_4.visible=!this.oPgFrm.Page5.oPag.oDPCODBUN_5_4.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_5.visible=!this.oPgFrm.Page5.oPag.oStr_5_5.mHide()
    this.oPgFrm.Page5.oPag.oLinkPC_5_6.visible=!this.oPgFrm.Page5.oPag.oLinkPC_5_6.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_31.visible=!this.oPgFrm.Page2.oPag.oBtn_2_31.mHide()
    this.oPgFrm.Page1.oPag.oDPCODUTE_1_80.visible=!this.oPgFrm.Page1.oPag.oDPCODUTE_1_80.mHide()
    this.oPgFrm.Page1.oPag.oDESUTE_1_81.visible=!this.oPgFrm.Page1.oPag.oDESUTE_1_81.mHide()
    this.oPgFrm.Page1.oPag.oDP__NOTE_1_82.visible=!this.oPgFrm.Page1.oPag.oDP__NOTE_1_82.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_39.visible=!this.oPgFrm.Page3.oPag.oStr_3_39.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_85.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_85.mHide()
    this.oPgFrm.Page1.oPag.oDPCODCAL_1_86.visible=!this.oPgFrm.Page1.oPag.oDPCODCAL_1_86.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    this.oPgFrm.Page1.oPag.oCCDESPIA_1_91.visible=!this.oPgFrm.Page1.oPag.oCCDESPIA_1_91.mHide()
    this.oPgFrm.Page1.oPag.oTCDESCRI_1_92.visible=!this.oPgFrm.Page1.oPag.oTCDESCRI_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_95.visible=!this.oPgFrm.Page1.oPag.oStr_1_95.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_96.visible=!this.oPgFrm.Page1.oPag.oStr_1_96.mHide()
    this.oPgFrm.Page1.oPag.oCCDESPIA_1_97.visible=!this.oPgFrm.Page1.oPag.oCCDESPIA_1_97.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_98.visible=!this.oPgFrm.Page1.oPag.oStr_1_98.mHide()
    this.oPgFrm.Page1.oPag.oDPDESGRU_1_99.visible=!this.oPgFrm.Page1.oPag.oDPDESGRU_1_99.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    this.oPgFrm.Page1.oPag.oTCDESCRI_1_102.visible=!this.oPgFrm.Page1.oPag.oTCDESCRI_1_102.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_103.visible=!this.oPgFrm.Page1.oPag.oStr_1_103.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_104.visible=!this.oPgFrm.Page1.oPag.oStr_1_104.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_106.visible=!this.oPgFrm.Page1.oPag.oStr_1_106.mHide()
    this.oPgFrm.Page1.oPag.oDESPRE_1_107.visible=!this.oPgFrm.Page1.oPag.oDESPRE_1_107.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_108.visible=!this.oPgFrm.Page1.oPag.oBtn_1_108.mHide()
    this.oPgFrm.Page1.oPag.oDESPRE_1_109.visible=!this.oPgFrm.Page1.oPag.oDESPRE_1_109.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_117.visible=!this.oPgFrm.Page1.oPag.oStr_1_117.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_21.visible=!this.oPgFrm.Page4.oPag.oStr_4_21.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_22.visible=!this.oPgFrm.Page4.oPag.oStr_4_22.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_23.visible=!this.oPgFrm.Page4.oPag.oStr_4_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_120.visible=!this.oPgFrm.Page1.oPag.oStr_1_120.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_121.visible=!this.oPgFrm.Page1.oPag.oStr_1_121.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_122.visible=!this.oPgFrm.Page1.oPag.oBtn_1_122.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_41.visible=!this.oPgFrm.Page3.oPag.oStr_3_41.mHide()
    this.oPgFrm.Page3.oPag.oDPVCSICS_3_42.visible=!this.oPgFrm.Page3.oPag.oDPVCSICS_3_42.mHide()
    this.oPgFrm.Page2.oPag.oLinkPC_2_35.visible=!this.oPgFrm.Page2.oPag.oLinkPC_2_35.mHide()
    this.oPgFrm.Page7.oPag.oDPRSFASC_7_2.visible=!this.oPgFrm.Page7.oPag.oDPRSFASC_7_2.mHide()
    this.oPgFrm.Page7.oPag.oDPCODRES_7_3.visible=!this.oPgFrm.Page7.oPag.oDPCODRES_7_3.mHide()
    this.oPgFrm.Page7.oPag.oDPPERFAT_7_4.visible=!this.oPgFrm.Page7.oPag.oDPPERFAT_7_4.mHide()
    this.oPgFrm.Page7.oPag.oDPPERFAC_7_5.visible=!this.oPgFrm.Page7.oPag.oDPPERFAC_7_5.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_6.visible=!this.oPgFrm.Page7.oPag.oStr_7_6.mHide()
    this.oPgFrm.Page7.oPag.oDPTIPRIG_7_7.visible=!this.oPgFrm.Page7.oPag.oDPTIPRIG_7_7.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_8.visible=!this.oPgFrm.Page7.oPag.oStr_7_8.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_9.visible=!this.oPgFrm.Page7.oPag.oStr_7_9.mHide()
    this.oPgFrm.Page7.oPag.oDENOMRES_7_10.visible=!this.oPgFrm.Page7.oPag.oDENOMRES_7_10.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_11.visible=!this.oPgFrm.Page7.oPag.oStr_7_11.mHide()
    this.oPgFrm.Page7.oPag.oDPCTRPRE_7_12.visible=!this.oPgFrm.Page7.oPag.oDPCTRPRE_7_12.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_13.visible=!this.oPgFrm.Page7.oPag.oStr_7_13.mHide()
    this.oPgFrm.Page7.oPag.oDPGG_PRE_7_14.visible=!this.oPgFrm.Page7.oPag.oDPGG_PRE_7_14.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_15.visible=!this.oPgFrm.Page7.oPag.oStr_7_15.mHide()
    this.oPgFrm.Page7.oPag.oDPGG_SUC_7_16.visible=!this.oPgFrm.Page7.oPag.oDPGG_SUC_7_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_128.visible=!this.oPgFrm.Page1.oPag.oStr_1_128.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_43.visible=!this.oPgFrm.Page3.oPag.oStr_3_43.mHide()
    this.oPgFrm.Page3.oPag.oDPFLPROM_3_44.visible=!this.oPgFrm.Page3.oPag.oDPFLPROM_3_44.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_adp
    this.w_DPTIPRIS=this.cTipRis
    this.w_cTipRis=this.cTipRis
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
        if lower(cEvent)==lower("Delete start")
          .Calculate_FWZWGRCBNC()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
      .oPgFrm.Page1.oPag.LBLVAL.Event(cEvent)
      .oPgFrm.Page4.oPag.FIRMAIMG.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_84.Event(cEvent)
        if lower(cEvent)==lower("AggiornaCF") or lower(cEvent)==lower("w_DPCODFIS LostFocus")
          .Calculate_WAZFTOAGEQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_88.Event(cEvent)
        if lower(cEvent)==lower("Record Deleted") or lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_ZYAELGIHGA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("New record")
          .Calculate_YRLCIPZNOW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_DPCHKING Changed")
          .Calculate_HDGHWRKUWL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load") or lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_PGQNPFGCXZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_GRJFLMWOLU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.BTNQRY.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_adp
    with this
      .w_BTNQRY.Visible = .w_cTipRis='P' And Isalt()
    endwith
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DPCODANA
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_lTable = "ANAGRAFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2], .t., this.ANAGRAFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODANA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ANAGRAFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DPCODANA)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANCODICE',trim(this.w_DPCODANA))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODANA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DPCODANA)+"%");

            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DPCODANA)+"%");

            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DPCODANA) and !this.bDontReportError
            deferred_cp_zoom('ANAGRAFE','*','ANCODICE',cp_AbsName(oSource.parent,'oDPCODANA_1_4'),i_cWhere,'',"ANAGRAFE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODANA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DPCODANA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',this.w_DPCODANA)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODANA = NVL(_Link_.ANCODICE,space(15))
      this.w_DCLMAST = NVL(_Link_.ANDESCRI,space(40))
      this.w_DPRESVIA = NVL(_Link_.ANINDIRI,space(35))
      this.w_DPRESLOC = NVL(_Link_.ANLOCALI,space(30))
      this.w_PROVIN = NVL(_Link_.ANPROVIN,space(2))
      this.w_PROVIN1 = NVL(_Link_.ANPROVIN,space(2))
      this.w_DPTELEF1 = NVL(_Link_.ANTELEFO,space(18))
      this.w_DPTELEF2 = NVL(_Link_.ANTELFAX,space(18))
      this.w_DPINDMAI = NVL(_Link_.AN_EMAIL,space(254))
      this.w_DPINDPEC = NVL(_Link_.AN_EMPEC,space(254))
      this.w_DPNOME = NVL(_Link_.AN__NOME,space(50))
      this.w_DPCOGNOM = NVL(_Link_.ANCOGNOM,space(50))
      this.w_DPCODFIS = NVL(_Link_.ANCODFIS,space(16))
      this.w_DP_SESSO = NVL(_Link_.AN_SESSO,space(1))
      this.w_DPDATNAS = NVL(cp_ToDate(_Link_.ANDATNAS),ctod("  /  /  "))
      this.w_DPNASLOC = NVL(_Link_.ANLOCNAS,space(40))
      this.w_PRONAS = NVL(_Link_.ANPRONAS,space(2))
      this.w_PRONAS1 = NVL(_Link_.ANPRONAS,space(2))
      this.w_DPRESCAP = NVL(_Link_.AN___CAP,space(9))
      this.w_DPDESCRI = NVL(_Link_.ANDESCRI,space(60))
      this.w_DPDESCRI2 = NVL(_Link_.ANDESCR2,space(60))
      this.w_NAZION = NVL(_Link_.ANNAZION,space(3))
      this.w_DPNUMCEL = NVL(_Link_.ANNUMCEL,space(18))
      this.w_DPPERFIS = NVL(_Link_.ANPERFIS,space(1))
      this.w_DPPARIVA = NVL(_Link_.ANPARIVA,space(12))
      this.w_DP__NOTE = NVL(_Link_.AN__NOTE,space(0))
      this.w_DPINDWEB = NVL(_Link_.ANINDWEB,space(50))
      this.w_DPNATGIU = NVL(_Link_.ANNATGIU,space(10))
      this.w_DPNUMCAR = NVL(_Link_.ANNUMCAR,space(18))
      this.w_DPINDIR2 = NVL(_Link_.ANINDIR2,space(35))
      this.w_DPDTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODANA = space(15)
      endif
      this.w_DCLMAST = space(40)
      this.w_DPRESVIA = space(35)
      this.w_DPRESLOC = space(30)
      this.w_PROVIN = space(2)
      this.w_PROVIN1 = space(2)
      this.w_DPTELEF1 = space(18)
      this.w_DPTELEF2 = space(18)
      this.w_DPINDMAI = space(254)
      this.w_DPINDPEC = space(254)
      this.w_DPNOME = space(50)
      this.w_DPCOGNOM = space(50)
      this.w_DPCODFIS = space(16)
      this.w_DP_SESSO = space(1)
      this.w_DPDATNAS = ctod("  /  /  ")
      this.w_DPNASLOC = space(40)
      this.w_PRONAS = space(2)
      this.w_PRONAS1 = space(2)
      this.w_DPRESCAP = space(9)
      this.w_DPDESCRI = space(60)
      this.w_DPDESCRI2 = space(60)
      this.w_NAZION = space(3)
      this.w_DPNUMCEL = space(18)
      this.w_DPPERFIS = space(1)
      this.w_DPPARIVA = space(12)
      this.w_DP__NOTE = space(0)
      this.w_DPINDWEB = space(50)
      this.w_DPNATGIU = space(10)
      this.w_DPNUMCAR = space(18)
      this.w_DPINDIR2 = space(35)
      this.w_DPDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.ANAGRAFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODANA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 31 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ANAGRAFE_IDX,3] and i_nFlds+31<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.ANCODICE as ANCODICE104"+ ",link_1_4.ANDESCRI as ANDESCRI104"+ ",link_1_4.ANINDIRI as ANINDIRI104"+ ",link_1_4.ANLOCALI as ANLOCALI104"+ ",link_1_4.ANPROVIN as ANPROVIN104"+ ",link_1_4.ANPROVIN as ANPROVIN104"+ ",link_1_4.ANTELEFO as ANTELEFO104"+ ",link_1_4.ANTELFAX as ANTELFAX104"+ ",link_1_4.AN_EMAIL as AN_EMAIL104"+ ",link_1_4.AN_EMPEC as AN_EMPEC104"+ ",link_1_4.AN__NOME as AN__NOME104"+ ",link_1_4.ANCOGNOM as ANCOGNOM104"+ ",link_1_4.ANCODFIS as ANCODFIS104"+ ",link_1_4.AN_SESSO as AN_SESSO104"+ ",link_1_4.ANDATNAS as ANDATNAS104"+ ",link_1_4.ANLOCNAS as ANLOCNAS104"+ ",link_1_4.ANPRONAS as ANPRONAS104"+ ",link_1_4.ANPRONAS as ANPRONAS104"+ ",link_1_4.AN___CAP as AN___CAP104"+ ",link_1_4.ANDESCRI as ANDESCRI104"+ ",link_1_4.ANDESCR2 as ANDESCR2104"+ ",link_1_4.ANNAZION as ANNAZION104"+ ",link_1_4.ANNUMCEL as ANNUMCEL104"+ ",link_1_4.ANPERFIS as ANPERFIS104"+ ",link_1_4.ANPARIVA as ANPARIVA104"+ ",link_1_4.AN__NOTE as AN__NOTE104"+ ",link_1_4.ANINDWEB as ANINDWEB104"+ ",link_1_4.ANNATGIU as ANNATGIU104"+ ",link_1_4.ANNUMCAR as ANNUMCAR104"+ ",link_1_4.ANINDIR2 as ANINDIR2104"+ ",link_1_4.ANDTOBSO as ANDTOBSO104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on DIPENDEN.DPCODANA=link_1_4.ANCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+31
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and DIPENDEN.DPCODANA=link_1_4.ANCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+31
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NAZION
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODISO,NACODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODISO="+cp_ToStrODBC(this.w_NAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODISO',this.w_NAZION)
            select NACODISO,NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NAZION = NVL(_Link_.NACODISO,space(3))
      this.w_ANAZION = NVL(_Link_.NACODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_NAZION = space(3)
      endif
      this.w_ANAZION = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODISO,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRONAS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
    i_lTable = "ANAG_PRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2], .t., this.ANAG_PRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRONAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRONAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODANA,PRCODPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODANA="+cp_ToStrODBC(this.w_PRONAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODANA',this.w_PRONAS)
            select PRCODANA,PRCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRONAS = NVL(_Link_.PRCODANA,space(2))
      this.w_APRONAS = NVL(_Link_.PRCODPRO,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PRONAS = space(2)
      endif
      this.w_APRONAS = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])+'\'+cp_ToStr(_Link_.PRCODANA,1)
      cp_ShowWarn(i_cKey,this.ANAG_PRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRONAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PROVIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
    i_lTable = "ANAG_PRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2], .t., this.ANAG_PRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROVIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROVIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODANA,PRCODPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODANA="+cp_ToStrODBC(this.w_PROVIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODANA',this.w_PROVIN)
            select PRCODANA,PRCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROVIN = NVL(_Link_.PRCODANA,space(2))
      this.w_APROVIN = NVL(_Link_.PRCODPRO,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PROVIN = space(2)
      endif
      this.w_APROVIN = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])+'\'+cp_ToStr(_Link_.PRCODANA,1)
      cp_ShowWarn(i_cKey,this.ANAG_PRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROVIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODCEN
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_DPCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_DPCODCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oDPCODCEN_1_18'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_DPCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_DPCODCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODCEN = space(15)
      endif
      this.w_CCDESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.CC_CONTO as CC_CONTO118"+ ",link_1_18.CCDESPIA as CCDESPIA118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on DIPENDEN.DPCODCEN=link_1_18.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and DIPENDEN.DPCODCEN=link_1_18.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPSERPRE
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPSERPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_DPSERPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_DPSERPRE))
          select ARCODART,ARDESART,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPSERPRE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPSERPRE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oDPSERPRE_1_19'),i_cWhere,'GSMA_AAS',"Servizi",'GSMA0AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPSERPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DPSERPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DPSERPRE)
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPSERPRE = NVL(_Link_.ARCODART,space(41))
      this.w_DESPRE = NVL(_Link_.ARDESART,space(40))
      this.w_TIPAR1 = NVL(_Link_.ARTIPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPSERPRE = space(41)
      endif
      this.w_DESPRE = space(40)
      this.w_TIPAR1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPAR1<>'PF'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DPSERPRE = space(41)
        this.w_DESPRE = space(40)
        this.w_TIPAR1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPSERPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.ARCODART as ARCODART119"+ ",link_1_19.ARDESART as ARDESART119"+ ",link_1_19.ARTIPART as ARTIPART119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on DIPENDEN.DPSERPRE=link_1_19.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and DIPENDEN.DPSERPRE=link_1_19.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPGRUPRE
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPGRUPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_DPGRUPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_DPGRUPRE))
          select DPCODICE,DPDESCRI,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPGRUPRE)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPGRUPRE) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oDPGRUPRE_1_20'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPGRUPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_DPGRUPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_DPGRUPRE)
            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPGRUPRE = NVL(_Link_.DPCODICE,space(5))
      this.w_DPDESGRU = NVL(_Link_.DPDESCRI,space(40))
      this.w_TIPGRU = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPGRUPRE = space(5)
      endif
      this.w_DPDESGRU = space(40)
      this.w_TIPGRU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPGRU='G' AND GSAR1BGP( '' , 'CHK', .w_DPCODICE , .w_DPGRUPRE)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DPGRUPRE = space(5)
        this.w_DPDESGRU = space(40)
        this.w_TIPGRU = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPGRUPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.DPCODICE as DPCODICE120"+ ",link_1_20.DPDESCRI as DPDESCRI120"+ ",link_1_20.DPTIPRIS as DPTIPRIS120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on DIPENDEN.DPGRUPRE=link_1_20.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and DIPENDEN.DPGRUPRE=link_1_20.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPREPART
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPREPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_ARP',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_DPREPART)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPRIS);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_TIPRIS;
                     ,'RLCODICE',trim(this.w_DPREPART))
          select RL__TIPO,RLCODICE,RLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPREPART)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPREPART) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oDPREPART_1_26'),i_cWhere,'GSDS_ARP',"Reparto",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPREPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_DPREPART);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_TIPRIS;
                       ,'RLCODICE',this.w_DPREPART)
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPREPART = NVL(_Link_.RLCODICE,space(20))
      this.w_DESRIS = NVL(_Link_.RLDESCRI,space(40))
      this.w_TIPORIS = NVL(_Link_.RL__TIPO,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_DPREPART = space(20)
      endif
      this.w_DESRIS = space(40)
      this.w_TIPORIS = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPORIS='RE'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DPREPART = space(20)
        this.w_DESRIS = space(40)
        this.w_TIPORIS = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPREPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPDITTA
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPDITTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DPDITTA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DPTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDATNAS,ANLOCNAS,ANPRONAS,AN_SESSO,ANPERFIS,ANCODFIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DPTIPCON;
                     ,'ANCODICE',trim(this.w_DPDITTA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDATNAS,ANLOCNAS,ANPRONAS,AN_SESSO,ANPERFIS,ANCODFIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPDITTA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPDITTA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDPDITTA_1_28'),i_cWhere,'GSAR_AFR',"Ditta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDATNAS,ANLOCNAS,ANPRONAS,AN_SESSO,ANPERFIS,ANCODFIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDATNAS,ANLOCNAS,ANPRONAS,AN_SESSO,ANPERFIS,ANCODFIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDATNAS,ANLOCNAS,ANPRONAS,AN_SESSO,ANPERFIS,ANCODFIS";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DPTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDATNAS,ANLOCNAS,ANPRONAS,AN_SESSO,ANPERFIS,ANCODFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPDITTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDATNAS,ANLOCNAS,ANPRONAS,AN_SESSO,ANPERFIS,ANCODFIS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DPDITTA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DPTIPCON;
                       ,'ANCODICE',this.w_DPDITTA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDATNAS,ANLOCNAS,ANPRONAS,AN_SESSO,ANPERFIS,ANCODFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPDITTA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DPDATNAS = NVL(cp_ToDate(_Link_.ANDATNAS),ctod("  /  /  "))
      this.w_DPNASLOC = NVL(_Link_.ANLOCNAS,space(40))
      this.w_DPNASPRO = NVL(_Link_.ANPRONAS,space(2))
      this.w_DP_SESSO = NVL(_Link_.AN_SESSO,space(1))
      this.w_PERFIS = NVL(_Link_.ANPERFIS,space(1))
      this.w_DPCODFIS = NVL(_Link_.ANCODFIS,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_DPDITTA = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DPDATNAS = ctod("  /  /  ")
      this.w_DPNASLOC = space(40)
      this.w_DPNASPRO = space(2)
      this.w_DP_SESSO = space(1)
      this.w_PERFIS = space(1)
      this.w_DPCODFIS = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPDITTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_28.ANCODICE as ANCODICE128"+ ",link_1_28.ANDESCRI as ANDESCRI128"+ ",link_1_28.ANDATNAS as ANDATNAS128"+ ",link_1_28.ANLOCNAS as ANLOCNAS128"+ ",link_1_28.ANPRONAS as ANPRONAS128"+ ",link_1_28.AN_SESSO as AN_SESSO128"+ ",link_1_28.ANPERFIS as ANPERFIS128"+ ",link_1_28.ANCODFIS as ANCODFIS128"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_28 on DIPENDEN.DPDITTA=link_1_28.ANCODICE"+" and DIPENDEN.DPTIPCON=link_1_28.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_28"
          i_cKey=i_cKey+'+" and DIPENDEN.DPDITTA=link_1_28.ANCODICE(+)"'+'+" and DIPENDEN.DPTIPCON=link_1_28.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODDES
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BU2',True,'SEDIAZIE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODDES like "+cp_ToStrODBC(trim(this.w_DPCODDES)+"%");
                   +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SENOMSED";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODAZI,SECODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODAZI',this.w_CODAZI;
                     ,'SECODDES',trim(this.w_DPCODDES))
          select SECODAZI,SECODDES,SENOMSED;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODAZI,SECODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODDES)==trim(_Link_.SECODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODDES) and !this.bDontReportError
            deferred_cp_zoom('SEDIAZIE','*','SECODAZI,SECODDES',cp_AbsName(oSource.parent,'oDPCODDES_1_29'),i_cWhere,'GSAR_BU2',"Sedi / unit� organizzative",'GSAR_AU2.SEDIAZIE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SENOMSED";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODAZI,SECODDES,SENOMSED;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SENOMSED";
                     +" from "+i_cTable+" "+i_lTable+" where SECODDES="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',oSource.xKey(1);
                       ,'SECODDES',oSource.xKey(2))
            select SECODAZI,SECODDES,SENOMSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SENOMSED";
                   +" from "+i_cTable+" "+i_lTable+" where SECODDES="+cp_ToStrODBC(this.w_DPCODDES);
                   +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',this.w_CODAZI;
                       ,'SECODDES',this.w_DPCODDES)
            select SECODAZI,SECODDES,SENOMSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODDES = NVL(_Link_.SECODDES,space(5))
      this.w_DESSED = NVL(_Link_.SENOMSED,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODDES = space(5)
      endif
      this.w_DESSED = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SECODAZI,1)+'\'+cp_ToStr(_Link_.SECODDES,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPMANSION
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MANSIONI_IDX,3]
    i_lTable = "MANSIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MANSIONI_IDX,2], .t., this.MANSIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MANSIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPMANSION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMN',True,'MANSIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MNCODICE like "+cp_ToStrODBC(trim(this.w_DPMANSION)+"%");

          i_ret=cp_SQL(i_nConn,"select MNCODICE,MNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MNCODICE',trim(this.w_DPMANSION))
          select MNCODICE,MNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPMANSION)==trim(_Link_.MNCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPMANSION) and !this.bDontReportError
            deferred_cp_zoom('MANSIONI','*','MNCODICE',cp_AbsName(oSource.parent,'oDPMANSION_1_31'),i_cWhere,'GSAR_AMN',"Mansione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MNCODICE,MNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MNCODICE',oSource.xKey(1))
            select MNCODICE,MNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPMANSION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MNCODICE,MNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MNCODICE="+cp_ToStrODBC(this.w_DPMANSION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MNCODICE',this.w_DPMANSION)
            select MNCODICE,MNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPMANSION = NVL(_Link_.MNCODICE,space(5))
      this.w_DESMAN = NVL(_Link_.MNDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPMANSION = space(5)
      endif
      this.w_DESMAN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MANSIONI_IDX,2])+'\'+cp_ToStr(_Link_.MNCODICE,1)
      cp_ShowWarn(i_cKey,this.MANSIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPMANSION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MANSIONI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MANSIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_31.MNCODICE as MNCODICE131"+ ",link_1_31.MNDESCRI as MNDESCRI131"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_31 on DIPENDEN.DPMANSION=link_1_31.MNCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_31"
          i_cKey=i_cKey+'+" and DIPENDEN.DPMANSION=link_1_31.MNCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODCEN
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_DPCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_DPCODCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oDPCODCEN_1_34'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_DPCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_DPCODCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODCEN = space(15)
      endif
      this.w_CCDESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_34.CC_CONTO as CC_CONTO134"+ ",link_1_34.CCDESPIA as CCDESPIA134"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_34 on DIPENDEN.DPCODCEN=link_1_34.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_34"
          i_cKey=i_cKey+'+" and DIPENDEN.DPCODCEN=link_1_34.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODCAL
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODCAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATL',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_DPCODCAL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_DPCODCAL))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODCAL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStrODBC(trim(this.w_DPCODCAL)+"%");

            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStr(trim(this.w_DPCODCAL)+"%");

            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DPCODCAL) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oDPCODCAL_1_35'),i_cWhere,'GSAR_ATL',"Calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODCAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_DPCODCAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_DPCODCAL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODCAL = NVL(_Link_.TCCODICE,space(5))
      this.w_TCDESCRI = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODCAL = space(5)
      endif
      this.w_TCDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODCAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_35(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_CALE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_35.TCCODICE as TCCODICE135"+ ",link_1_35.TCDESCRI as TCDESCRI135"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_35 on DIPENDEN.DPCODCAL=link_1_35.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_35"
          i_cKey=i_cKey+'+" and DIPENDEN.DPCODCAL=link_1_35.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPSERVIZ
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPSERVIZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DPSERVIZ)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DPSERVIZ))
          select CACODICE,CADESART,CA__TIPO,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPSERVIZ)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPSERVIZ) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDPSERVIZ_1_38'),i_cWhere,'GSMA_ACA',"Servizi",'gspc_zse.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CA__TIPO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPSERVIZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DPSERVIZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DPSERVIZ)
            select CACODICE,CADESART,CA__TIPO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPSERVIZ = NVL(_Link_.CACODICE,space(41))
      this.w_DESSER = NVL(_Link_.CADESART,space(40))
      this.w_TIPART = NVL(_Link_.CA__TIPO,space(1))
      this.w_ARTICOLO = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DPSERVIZ = space(41)
      endif
      this.w_DESSER = space(40)
      this.w_TIPART = space(1)
      this.w_ARTICOLO = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPART='F' or .w_TIPART='M')  and (looktab('ART_ICOL','ARUNMIS1','ARCODART',.w_ARTICOLO)=.w_DPUMDEFA or looktab('ART_ICOL','ARUNMIS2','ARCODART',.w_ARTICOLO)=.w_DPUMDEFA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DPSERVIZ = space(41)
        this.w_DESSER = space(40)
        this.w_TIPART = space(1)
        this.w_ARTICOLO = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPSERVIZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.CACODICE as CACODICE138"+ ",link_1_38.CADESART as CADESART138"+ ",link_1_38.CA__TIPO as CA__TIPO138"+ ",link_1_38.CACODART as CACODART138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on DIPENDEN.DPSERVIZ=link_1_38.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and DIPENDEN.DPSERVIZ=link_1_38.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPSERPRE
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPSERPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_DPSERPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_DPSERPRE))
          select ARCODART,ARDESART,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPSERPRE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPSERPRE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oDPSERPRE_1_40'),i_cWhere,'GSMA_AAS',"Servizi",'GSMA0AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPSERPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DPSERPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DPSERPRE)
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPSERPRE = NVL(_Link_.ARCODART,space(41))
      this.w_DESPRE = NVL(_Link_.ARDESART,space(40))
      this.w_TIPAR1 = NVL(_Link_.ARTIPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPSERPRE = space(41)
      endif
      this.w_DESPRE = space(40)
      this.w_TIPAR1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPAR1<>'PF'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DPSERPRE = space(41)
        this.w_DESPRE = space(40)
        this.w_TIPAR1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPSERPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_40.ARCODART as ARCODART140"+ ",link_1_40.ARDESART as ARDESART140"+ ",link_1_40.ARTIPART as ARTIPART140"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_40 on DIPENDEN.DPSERPRE=link_1_40.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_40"
          i_cKey=i_cKey+'+" and DIPENDEN.DPSERPRE=link_1_40.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPKEYPAR
  func Link_1_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPKEYPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPKEYPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDUMCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_DPKEYPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_DPKEYPAR)
            select PDCHIAVE,PDUMCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPKEYPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_DPUMDEFA = NVL(_Link_.PDUMCOMM,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_DPKEYPAR = space(10)
      endif
      this.w_DPUMDEFA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPKEYPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPNASNAZ
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPNASNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_DPNASNAZ)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_DPNASNAZ))
          select NACODNAZ,NADESNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPNASNAZ)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPNASNAZ) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oDPNASNAZ_2_11'),i_cWhere,'GSAR_ANZ',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPNASNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_DPNASNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_DPNASNAZ)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPNASNAZ = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ1 = NVL(_Link_.NADESNAZ,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_DPNASNAZ = space(3)
      endif
      this.w_DESNAZ1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPNASNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.NACODNAZ as NACODNAZ211"+ ",link_2_11.NADESNAZ as NADESNAZ211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on DIPENDEN.DPNASNAZ=link_2_11.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and DIPENDEN.DPNASNAZ=link_2_11.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPNAZION
  func Link_3_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_DPNAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_DPNAZION))
          select NACODNAZ,NADESNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPNAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPNAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oDPNAZION_3_9'),i_cWhere,'GSAR_ANZ',"NAZIONI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_DPNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_DPNAZION)
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ = NVL(_Link_.NADESNAZ,space(35))
      this.w_CODISO = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_DPNAZION = space(3)
      endif
      this.w_DESNAZ = space(35)
      this.w_CODISO = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_9.NACODNAZ as NACODNAZ309"+ ",link_3_9.NADESNAZ as NADESNAZ309"+ ",link_3_9.NACODISO as NACODISO309"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_9 on DIPENDEN.DPNAZION=link_3_9.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_9"
          i_cKey=i_cKey+'+" and DIPENDEN.DPNAZION=link_3_9.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODUTE
  func Link_4_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_DPCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_DPCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DPCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oDPCODUTE_4_1'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_DPCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_DPCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODSOS
  func Link_4_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODSOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_DPCODSOS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_FLPERSONA);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_FLPERSONA;
                     ,'DPCODICE',trim(this.w_DPCODSOS))
          select DPTIPRIS,DPCODICE,DPCOGNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODSOS)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_DPCODSOS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_FLPERSONA);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_DPCODSOS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_FLPERSONA);

            select DPTIPRIS,DPCODICE,DPCOGNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DPCODSOS) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oDPCODSOS_4_16'),i_cWhere,'',"Dipendenti",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLPERSONA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il sostituto deve essere diverso dalla persona")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_FLPERSONA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODSOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_DPCODSOS);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_FLPERSONA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_FLPERSONA;
                       ,'DPCODICE',this.w_DPCODSOS)
            select DPTIPRIS,DPCODICE,DPCOGNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODSOS = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNSOS = NVL(_Link_.DPCOGNOM,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODSOS = space(5)
      endif
      this.w_COGNSOS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DPCODSOS <> .w_DPCODICE
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il sostituto deve essere diverso dalla persona")
        endif
        this.w_DPCODSOS = space(5)
        this.w_COGNSOS = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODSOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCDUTAL
  func Link_5_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCDUTAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_DPCDUTAL);

          i_ret=cp_SQL(i_nConn,"select CODE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_DPCDUTAL)
          select CODE;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DPCDUTAL) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oDPCDUTAL_5_1'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCDUTAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_DPCDUTAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_DPCDUTAL)
            select CODE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCDUTAL = NVL(_Link_.CODE,0)
    else
      if i_cCtrl<>'Load'
        this.w_DPCDUTAL = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCDUTAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODBUN
  func Link_5_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KBU',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_DPCODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_CODAZI;
                     ,'BUCODICE',trim(this.w_DPCODBUN))
          select BUCODAZI,BUCODICE,BUFLANAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODBUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODBUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oDPCODBUN_5_4'),i_cWhere,'GSAR_KBU',"Business Unit",'GSCA_ACM.BUSIUNIT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUFLANAL;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Business unit di analitica")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_DPCODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_CODAZI;
                       ,'BUCODICE',this.w_DPCODBUN)
            select BUCODAZI,BUCODICE,BUFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_FLANAL = NVL(_Link_.BUFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODBUN = space(3)
      endif
      this.w_FLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLANAL='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Business unit di analitica")
        endif
        this.w_DPCODBUN = space(3)
        this.w_FLANAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODUTE
  func Link_1_80(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_DPCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_DPCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DPCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oDPCODUTE_1_80'),i_cWhere,'',"Utenti",'GSAR_ADP.CPUSERS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_DPCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_DPCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODCAL
  func Link_1_86(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODCAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATL',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_DPCODCAL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_DPCODCAL))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODCAL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStrODBC(trim(this.w_DPCODCAL)+"%");

            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStr(trim(this.w_DPCODCAL)+"%");

            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DPCODCAL) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oDPCODCAL_1_86'),i_cWhere,'GSAR_ATL',"Calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODCAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_DPCODCAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_DPCODCAL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODCAL = NVL(_Link_.TCCODICE,space(5))
      this.w_TCDESCRI = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODCAL = space(5)
      endif
      this.w_TCDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODCAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_86(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_CALE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_86.TCCODICE as TCCODICE186"+ ",link_1_86.TCDESCRI as TCDESCRI186"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_86 on DIPENDEN.DPCODCAL=link_1_86.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_86"
          i_cKey=i_cKey+'+" and DIPENDEN.DPCODCAL=link_1_86.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LetturaParAgen
  func Link_7_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAgen) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAgen)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLPRES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAgen);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAgen)
            select PACODAZI,PAFLPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAgen = NVL(_Link_.PACODAZI,space(5))
      this.w_PAFLPRES = NVL(_Link_.PAFLPRES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAgen = space(5)
      endif
      this.w_PAFLPRES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAgen Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODRES
  func Link_7_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODRES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_DPCODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_FLPERSONA);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_FLPERSONA;
                     ,'DPCODICE',trim(this.w_DPCODRES))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODRES)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_DPCODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_FLPERSONA);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_DPCODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_FLPERSONA);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DPCODRES) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oDPCODRES_7_3'),i_cWhere,'',"Persone",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLPERSONA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_FLPERSONA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODRES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_DPCODRES);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_FLPERSONA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_FLPERSONA;
                       ,'DPCODICE',this.w_DPCODRES)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODRES = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNRES = NVL(_Link_.DPCOGNOM,space(10))
      this.w_NOMERES = NVL(_Link_.DPNOME,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODRES = space(5)
      endif
      this.w_COGNRES = space(10)
      this.w_NOMERES = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODRES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDPCODICE_1_1.value==this.w_DPCODICE)
      this.oPgFrm.Page1.oPag.oDPCODICE_1_1.value=this.w_DPCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDCLMAST_1_3.value==this.w_DCLMAST)
      this.oPgFrm.Page1.oPag.oDCLMAST_1_3.value=this.w_DCLMAST
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODANA_1_4.value==this.w_DPCODANA)
      this.oPgFrm.Page1.oPag.oDPCODANA_1_4.value=this.w_DPCODANA
    endif
    if not(this.oPgFrm.Page1.oPag.oDPBADGE_1_16.value==this.w_DPBADGE)
      this.oPgFrm.Page1.oPag.oDPBADGE_1_16.value=this.w_DPBADGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESCRI_1_17.value==this.w_DPDESCRI)
      this.oPgFrm.Page1.oPag.oDPDESCRI_1_17.value=this.w_DPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODCEN_1_18.value==this.w_DPCODCEN)
      this.oPgFrm.Page1.oPag.oDPCODCEN_1_18.value=this.w_DPCODCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDPSERPRE_1_19.value==this.w_DPSERPRE)
      this.oPgFrm.Page1.oPag.oDPSERPRE_1_19.value=this.w_DPSERPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPGRUPRE_1_20.value==this.w_DPGRUPRE)
      this.oPgFrm.Page1.oPag.oDPGRUPRE_1_20.value=this.w_DPGRUPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCOSORA_1_21.value==this.w_DPCOSORA)
      this.oPgFrm.Page1.oPag.oDPCOSORA_1_21.value=this.w_DPCOSORA
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCOGNOM_1_22.value==this.w_DPCOGNOM)
      this.oPgFrm.Page1.oPag.oDPCOGNOM_1_22.value=this.w_DPCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDPFLVISI_1_23.RadioValue()==this.w_DPFLVISI)
      this.oPgFrm.Page1.oPag.oDPFLVISI_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDPNOME_1_24.value==this.w_DPNOME)
      this.oPgFrm.Page1.oPag.oDPNOME_1_24.value=this.w_DPNOME
    endif
    if not(this.oPgFrm.Page1.oPag.oDPFLINEX_1_25.RadioValue()==this.w_DPFLINEX)
      this.oPgFrm.Page1.oPag.oDPFLINEX_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDPREPART_1_26.value==this.w_DPREPART)
      this.oPgFrm.Page1.oPag.oDPREPART_1_26.value=this.w_DPREPART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIS_1_27.value==this.w_DESRIS)
      this.oPgFrm.Page1.oPag.oDESRIS_1_27.value=this.w_DESRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDITTA_1_28.value==this.w_DPDITTA)
      this.oPgFrm.Page1.oPag.oDPDITTA_1_28.value=this.w_DPDITTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODDES_1_29.value==this.w_DPCODDES)
      this.oPgFrm.Page1.oPag.oDPCODDES_1_29.value=this.w_DPCODDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_30.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_30.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDPMANSION_1_31.value==this.w_DPMANSION)
      this.oPgFrm.Page1.oPag.oDPMANSION_1_31.value=this.w_DPMANSION
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAN_1_32.value==this.w_DESMAN)
      this.oPgFrm.Page1.oPag.oDESMAN_1_32.value=this.w_DESMAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCOSORA_1_33.value==this.w_DPCOSORA)
      this.oPgFrm.Page1.oPag.oDPCOSORA_1_33.value=this.w_DPCOSORA
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODCEN_1_34.value==this.w_DPCODCEN)
      this.oPgFrm.Page1.oPag.oDPCODCEN_1_34.value=this.w_DPCODCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODCAL_1_35.value==this.w_DPCODCAL)
      this.oPgFrm.Page1.oPag.oDPCODCAL_1_35.value=this.w_DPCODCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDPMODALL_1_36.RadioValue()==this.w_DPMODALL)
      this.oPgFrm.Page1.oPag.oDPMODALL_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDPSERVIZ_1_38.value==this.w_DPSERVIZ)
      this.oPgFrm.Page1.oPag.oDPSERVIZ_1_38.value=this.w_DPSERVIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSER_1_39.value==this.w_DESSER)
      this.oPgFrm.Page1.oPag.oDESSER_1_39.value=this.w_DESSER
    endif
    if not(this.oPgFrm.Page1.oPag.oDPSERPRE_1_40.value==this.w_DPSERPRE)
      this.oPgFrm.Page1.oPag.oDPSERPRE_1_40.value=this.w_DPSERPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPPATHPR_1_41.value==this.w_DPPATHPR)
      this.oPgFrm.Page1.oPag.oDPPATHPR_1_41.value=this.w_DPPATHPR
    endif
    if not(this.oPgFrm.Page1.oPag.oDP__PATH_1_42.value==this.w_DP__PATH)
      this.oPgFrm.Page1.oPag.oDP__PATH_1_42.value=this.w_DP__PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oDP__DIKE_1_43.value==this.w_DP__DIKE)
      this.oPgFrm.Page1.oPag.oDP__DIKE_1_43.value=this.w_DP__DIKE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSED_1_62.value==this.w_DESSED)
      this.oPgFrm.Page1.oPag.oDESSED_1_62.value=this.w_DESSED
    endif
    if not(this.oPgFrm.Page2.oPag.oDPCODFIS_2_1.value==this.w_DPCODFIS)
      this.oPgFrm.Page2.oPag.oDPCODFIS_2_1.value=this.w_DPCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDPPARIVA_2_2.value==this.w_DPPARIVA)
      this.oPgFrm.Page2.oPag.oDPPARIVA_2_2.value=this.w_DPPARIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oDPNUMCAR_2_4.value==this.w_DPNUMCAR)
      this.oPgFrm.Page2.oPag.oDPNUMCAR_2_4.value=this.w_DPNUMCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oDP_SESSO_2_5.RadioValue()==this.w_DP_SESSO)
      this.oPgFrm.Page2.oPag.oDP_SESSO_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDPNASLOC_2_7.value==this.w_DPNASLOC)
      this.oPgFrm.Page2.oPag.oDPNASLOC_2_7.value=this.w_DPNASLOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDPNASPRO_2_10.value==this.w_DPNASPRO)
      this.oPgFrm.Page2.oPag.oDPNASPRO_2_10.value=this.w_DPNASPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oDPNASNAZ_2_11.value==this.w_DPNASNAZ)
      this.oPgFrm.Page2.oPag.oDPNASNAZ_2_11.value=this.w_DPNASNAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNAZ1_2_12.value==this.w_DESNAZ1)
      this.oPgFrm.Page2.oPag.oDESNAZ1_2_12.value=this.w_DESNAZ1
    endif
    if not(this.oPgFrm.Page2.oPag.oDPDATNAS_2_14.value==this.w_DPDATNAS)
      this.oPgFrm.Page2.oPag.oDPDATNAS_2_14.value=this.w_DPDATNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oDPTITSTU_2_16.value==this.w_DPTITSTU)
      this.oPgFrm.Page2.oPag.oDPTITSTU_2_16.value=this.w_DPTITSTU
    endif
    if not(this.oPgFrm.Page2.oPag.oDPQUALIF_2_17.value==this.w_DPQUALIF)
      this.oPgFrm.Page2.oPag.oDPQUALIF_2_17.value=this.w_DPQUALIF
    endif
    if not(this.oPgFrm.Page2.oPag.oDPLIVELL_2_18.value==this.w_DPLIVELL)
      this.oPgFrm.Page2.oPag.oDPLIVELL_2_18.value=this.w_DPLIVELL
    endif
    if not(this.oPgFrm.Page2.oPag.oDPDATASS_2_21.value==this.w_DPDATASS)
      this.oPgFrm.Page2.oPag.oDPDATASS_2_21.value=this.w_DPDATASS
    endif
    if not(this.oPgFrm.Page2.oPag.oDPDATFRA_2_22.value==this.w_DPDATFRA)
      this.oPgFrm.Page2.oPag.oDPDATFRA_2_22.value=this.w_DPDATFRA
    endif
    if not(this.oPgFrm.Page3.oPag.oDPRESVIA_3_1.value==this.w_DPRESVIA)
      this.oPgFrm.Page3.oPag.oDPRESVIA_3_1.value=this.w_DPRESVIA
    endif
    if not(this.oPgFrm.Page3.oPag.oDPINDIR2_3_2.value==this.w_DPINDIR2)
      this.oPgFrm.Page3.oPag.oDPINDIR2_3_2.value=this.w_DPINDIR2
    endif
    if not(this.oPgFrm.Page3.oPag.oDPRESCAP_3_5.value==this.w_DPRESCAP)
      this.oPgFrm.Page3.oPag.oDPRESCAP_3_5.value=this.w_DPRESCAP
    endif
    if not(this.oPgFrm.Page3.oPag.oDPRESLOC_3_6.value==this.w_DPRESLOC)
      this.oPgFrm.Page3.oPag.oDPRESLOC_3_6.value=this.w_DPRESLOC
    endif
    if not(this.oPgFrm.Page3.oPag.oDPRESPRO_3_8.value==this.w_DPRESPRO)
      this.oPgFrm.Page3.oPag.oDPRESPRO_3_8.value=this.w_DPRESPRO
    endif
    if not(this.oPgFrm.Page3.oPag.oDPNAZION_3_9.value==this.w_DPNAZION)
      this.oPgFrm.Page3.oPag.oDPNAZION_3_9.value=this.w_DPNAZION
    endif
    if not(this.oPgFrm.Page3.oPag.oDPDOMVIA_3_14.value==this.w_DPDOMVIA)
      this.oPgFrm.Page3.oPag.oDPDOMVIA_3_14.value=this.w_DPDOMVIA
    endif
    if not(this.oPgFrm.Page3.oPag.oDPDOMCAP_3_17.value==this.w_DPDOMCAP)
      this.oPgFrm.Page3.oPag.oDPDOMCAP_3_17.value=this.w_DPDOMCAP
    endif
    if not(this.oPgFrm.Page3.oPag.oDPDOMLOC_3_18.value==this.w_DPDOMLOC)
      this.oPgFrm.Page3.oPag.oDPDOMLOC_3_18.value=this.w_DPDOMLOC
    endif
    if not(this.oPgFrm.Page3.oPag.oDPDOMPRO_3_20.value==this.w_DPDOMPRO)
      this.oPgFrm.Page3.oPag.oDPDOMPRO_3_20.value=this.w_DPDOMPRO
    endif
    if not(this.oPgFrm.Page3.oPag.oDPTELEF1_3_24.value==this.w_DPTELEF1)
      this.oPgFrm.Page3.oPag.oDPTELEF1_3_24.value=this.w_DPTELEF1
    endif
    if not(this.oPgFrm.Page3.oPag.oDPTELEF2_3_25.value==this.w_DPTELEF2)
      this.oPgFrm.Page3.oPag.oDPTELEF2_3_25.value=this.w_DPTELEF2
    endif
    if not(this.oPgFrm.Page3.oPag.oDPINDMAI_3_27.value==this.w_DPINDMAI)
      this.oPgFrm.Page3.oPag.oDPINDMAI_3_27.value=this.w_DPINDMAI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESNAZ_3_28.value==this.w_DESNAZ)
      this.oPgFrm.Page3.oPag.oDESNAZ_3_28.value=this.w_DESNAZ
    endif
    if not(this.oPgFrm.Page3.oPag.oCODISO_3_29.value==this.w_CODISO)
      this.oPgFrm.Page3.oPag.oCODISO_3_29.value=this.w_CODISO
    endif
    if not(this.oPgFrm.Page3.oPag.oDPNUMCEL_3_31.value==this.w_DPNUMCEL)
      this.oPgFrm.Page3.oPag.oDPNUMCEL_3_31.value=this.w_DPNUMCEL
    endif
    if not(this.oPgFrm.Page3.oPag.oDPINDPEC_3_35.value==this.w_DPINDPEC)
      this.oPgFrm.Page3.oPag.oDPINDPEC_3_35.value=this.w_DPINDPEC
    endif
    if not(this.oPgFrm.Page4.oPag.oDPCODUTE_4_1.value==this.w_DPCODUTE)
      this.oPgFrm.Page4.oPag.oDPCODUTE_4_1.value=this.w_DPCODUTE
    endif
    if not(this.oPgFrm.Page4.oPag.oDESUTE_4_2.value==this.w_DESUTE)
      this.oPgFrm.Page4.oPag.oDESUTE_4_2.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page4.oPag.oDPCHKING_4_4.RadioValue()==this.w_DPCHKING)
      this.oPgFrm.Page4.oPag.oDPCHKING_4_4.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDPDATINI_4_5.value==this.w_DPDATINI)
      this.oPgFrm.Page4.oPag.oDPDATINI_4_5.value=this.w_DPDATINI
    endif
    if not(this.oPgFrm.Page4.oPag.oDPGGBLOC_4_6.value==this.w_DPGGBLOC)
      this.oPgFrm.Page4.oPag.oDPGGBLOC_4_6.value=this.w_DPGGBLOC
    endif
    if not(this.oPgFrm.Page4.oPag.oDP_FIRMA_4_8.value==this.w_DP_FIRMA)
      this.oPgFrm.Page4.oPag.oDP_FIRMA_4_8.value=this.w_DP_FIRMA
    endif
    if not(this.oPgFrm.Page4.oPag.oDPASSDAL_4_12.value==this.w_DPASSDAL)
      this.oPgFrm.Page4.oPag.oDPASSDAL_4_12.value=this.w_DPASSDAL
    endif
    if not(this.oPgFrm.Page4.oPag.oDPASSFIN_4_13.value==this.w_DPASSFIN)
      this.oPgFrm.Page4.oPag.oDPASSFIN_4_13.value=this.w_DPASSFIN
    endif
    if not(this.oPgFrm.Page4.oPag.oDPCODSOS_4_16.value==this.w_DPCODSOS)
      this.oPgFrm.Page4.oPag.oDPCODSOS_4_16.value=this.w_DPCODSOS
    endif
    if not(this.oPgFrm.Page4.oPag.oCOGNSOS_4_18.value==this.w_COGNSOS)
      this.oPgFrm.Page4.oPag.oCOGNSOS_4_18.value=this.w_COGNSOS
    endif
    if not(this.oPgFrm.Page5.oPag.oDPCDUTAL_5_1.value==this.w_DPCDUTAL)
      this.oPgFrm.Page5.oPag.oDPCDUTAL_5_1.value=this.w_DPCDUTAL
    endif
    if not(this.oPgFrm.Page5.oPag.oDPFLCONF_5_3.RadioValue()==this.w_DPFLCONF)
      this.oPgFrm.Page5.oPag.oDPFLCONF_5_3.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oDPCODBUN_5_4.value==this.w_DPCODBUN)
      this.oPgFrm.Page5.oPag.oDPCODBUN_5_4.value=this.w_DPCODBUN
    endif
    if not(this.oPgFrm.Page6.oPag.oDP__NOTE_6_1.value==this.w_DP__NOTE)
      this.oPgFrm.Page6.oPag.oDP__NOTE_6_1.value=this.w_DP__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODUTE_1_80.value==this.w_DPCODUTE)
      this.oPgFrm.Page1.oPag.oDPCODUTE_1_80.value=this.w_DPCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_81.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_81.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDP__NOTE_1_82.value==this.w_DP__NOTE)
      this.oPgFrm.Page1.oPag.oDP__NOTE_1_82.value=this.w_DP__NOTE
    endif
    if not(this.oPgFrm.Page3.oPag.oDPFLAVVI_3_37.RadioValue()==this.w_DPFLAVVI)
      this.oPgFrm.Page3.oPag.oDPFLAVVI_3_37.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDPINDWEB_3_38.value==this.w_DPINDWEB)
      this.oPgFrm.Page3.oPag.oDPINDWEB_3_38.value=this.w_DPINDWEB
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODCAL_1_86.value==this.w_DPCODCAL)
      this.oPgFrm.Page1.oPag.oDPCODCAL_1_86.value=this.w_DPCODCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_91.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_91.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDESCRI_1_92.value==this.w_TCDESCRI)
      this.oPgFrm.Page1.oPag.oTCDESCRI_1_92.value=this.w_TCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_97.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_97.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESGRU_1_99.value==this.w_DPDESGRU)
      this.oPgFrm.Page1.oPag.oDPDESGRU_1_99.value=this.w_DPDESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDESCRI_1_102.value==this.w_TCDESCRI)
      this.oPgFrm.Page1.oPag.oTCDESCRI_1_102.value=this.w_TCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRE_1_107.value==this.w_DESPRE)
      this.oPgFrm.Page1.oPag.oDESPRE_1_107.value=this.w_DESPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRE_1_109.value==this.w_DESPRE)
      this.oPgFrm.Page1.oPag.oDESPRE_1_109.value=this.w_DESPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDTOBSO_1_113.value==this.w_DPDTOBSO)
      this.oPgFrm.Page1.oPag.oDPDTOBSO_1_113.value=this.w_DPDTOBSO
    endif
    if not(this.oPgFrm.Page3.oPag.oDPVCSICS_3_42.RadioValue()==this.w_DPVCSICS)
      this.oPgFrm.Page3.oPag.oDPVCSICS_3_42.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oDPRSFASC_7_2.RadioValue()==this.w_DPRSFASC)
      this.oPgFrm.Page7.oPag.oDPRSFASC_7_2.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oDPCODRES_7_3.value==this.w_DPCODRES)
      this.oPgFrm.Page7.oPag.oDPCODRES_7_3.value=this.w_DPCODRES
    endif
    if not(this.oPgFrm.Page7.oPag.oDPPERFAT_7_4.value==this.w_DPPERFAT)
      this.oPgFrm.Page7.oPag.oDPPERFAT_7_4.value=this.w_DPPERFAT
    endif
    if not(this.oPgFrm.Page7.oPag.oDPPERFAC_7_5.value==this.w_DPPERFAC)
      this.oPgFrm.Page7.oPag.oDPPERFAC_7_5.value=this.w_DPPERFAC
    endif
    if not(this.oPgFrm.Page7.oPag.oDPTIPRIG_7_7.RadioValue()==this.w_DPTIPRIG)
      this.oPgFrm.Page7.oPag.oDPTIPRIG_7_7.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oDENOMRES_7_10.value==this.w_DENOMRES)
      this.oPgFrm.Page7.oPag.oDENOMRES_7_10.value=this.w_DENOMRES
    endif
    if not(this.oPgFrm.Page7.oPag.oDPCTRPRE_7_12.RadioValue()==this.w_DPCTRPRE)
      this.oPgFrm.Page7.oPag.oDPCTRPRE_7_12.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oDPGG_PRE_7_14.value==this.w_DPGG_PRE)
      this.oPgFrm.Page7.oPag.oDPGG_PRE_7_14.value=this.w_DPGG_PRE
    endif
    if not(this.oPgFrm.Page7.oPag.oDPGG_SUC_7_16.value==this.w_DPGG_SUC)
      this.oPgFrm.Page7.oPag.oDPGG_SUC_7_16.value=this.w_DPGG_SUC
    endif
    if not(this.oPgFrm.Page3.oPag.oDPFLPROM_3_44.RadioValue()==this.w_DPFLPROM)
      this.oPgFrm.Page3.oPag.oDPFLPROM_3_44.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'DIPENDEN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DPCODICE)) or not(.w_DPTIPRIS<>'P' OR .w_DPCODICE<>"#@ZA�"))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DPCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice #@ZA� non utilizzabile")
          case   (empty(.w_DPCODANA))  and not(.T.)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODANA_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DPCODANA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPAR1<>'PF')  and not(.w_cTipRis<>'R' OR IsAlt())  and not(empty(.w_DPSERPRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPSERPRE_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPGRU='G' AND GSAR1BGP( '' , 'CHK', .w_DPCODICE , .w_DPGRUPRE))  and not(.w_cTipRis<>'P')  and (.cFunction<>'Load')  and not(empty(.w_DPGRUPRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPGRUPRE_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPORIS='RE')  and not(.w_cTipRis<>'P' OR IIF(isAHE(), g_DIBA<>"S", g_PROD<>"S"))  and (.w_DPFLINEX='I')  and not(empty(.w_DPREPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPREPART_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_TIPART='F' or .w_TIPART='M')  and (looktab('ART_ICOL','ARUNMIS1','ARCODART',.w_ARTICOLO)=.w_DPUMDEFA or looktab('ART_ICOL','ARUNMIS2','ARCODART',.w_ARTICOLO)=.w_DPUMDEFA))  and not(.w_cTipRis<>'P' OR IsAlt())  and not(empty(.w_DPSERVIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPSERVIZ_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPAR1<>'PF')  and not(.w_cTipRis<>'P')  and not(empty(.w_DPSERPRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPSERPRE_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(g_ANACOM<>'S' and .w_DPTIPRIG='P',CHKCFP(.w_DPCODFIS, 'CF', 'R',0,.w_DPCODICE),.T.))  and (.w_PERFIS<>'S' and g_ANACOM<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDPCODFIS_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DPCHKING)) or not(IIF(.w_DPCHKING='B', NOT IsUteAdmin(.w_DPCODUTE), .T.)))  and not(EMPTY(.w_DPCODUTE) OR g_AGEN<>'S')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDPCHKING_4_4.SetFocus()
            i_bnoObbl = !empty(.w_DPCHKING)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso per utente amministratore")
          case   (empty(.w_DPDATINI))  and not(EMPTY(.w_DPCHKING) OR .w_DPCHKING ='N' OR EMPTY(.w_DPCODUTE) OR g_AGEN<>'S')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDPDATINI_4_5.SetFocus()
            i_bnoObbl = !empty(.w_DPDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DPGGBLOC>=0)  and not(.w_DPCHKING <> 'B' OR EMPTY(.w_DPCODUTE) OR g_AGEN<>'S')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDPGGBLOC_4_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore positivo o nullo.")
          case   not(.w_DPASSFIN >= .w_DPASSDAL or EMPTY(.w_DPASSDAL))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDPASSDAL_4_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Date di assenza incongruenti")
          case   not(.w_DPASSFIN >= .w_DPASSDAL)
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDPASSFIN_4_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Date di assenza incongruenti")
          case   not(.w_DPCODSOS <> .w_DPCODICE)  and not(empty(.w_DPCODSOS))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDPCODSOS_4_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il sostituto deve essere diverso dalla persona")
          case   not(.w_FLANAL='S')  and not(g_PERBUN='N' or not IsAHE())  and not(empty(.w_DPCODBUN))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oDPCODBUN_5_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Business unit di analitica")
          case   not(.w_DPPERFAT>=0  AND .w_DPPERFAT<=100)  and not(NOT ISALT() OR .w_cTipRis<>'P')
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oDPPERFAT_7_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DPPERFAC>=0  AND .w_DPPERFAC<=100)  and not(NOT ISALT() OR .w_cTipRis<>'P')
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oDPPERFAC_7_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_DPCTRPRE='S' AND NOT Readgroup(.w_DPCODUTE)) OR .w_DPCTRPRE='N')  and not(.w_PAFLPRES<>'S')
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oDPCTRPRE_7_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso per un utente amministratore")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAR_MPH.CheckForm()
      if i_bres
        i_bres=  .GSAR_MPH.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_ARD.CheckForm()
      if i_bres
        i_bres=  .GSAR_ARD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MBR.CheckForm()
      if i_bres
        i_bres=  .GSAR_MBR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=5
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MSR.CheckForm()
      if i_bres
        i_bres=  .GSAR_MSR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MPD.CheckForm()
      if i_bres
        i_bres=  .GSAR_MPD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsar_adp
      If isAHE()
         if EMPTY(nvl(this.w_DPCODICE,''))
      	   i_bnoChk = .f.
           i_bRes = .f.
      	   i_cErrorMsg = Ah_MsgFormat("Codice persona: campo obbligatorio")
         endif
         if this.w_DPTIPRIS='P'
          If g_Anacom='S' and empty(this.w_Dclmast)
            i_bnoChk = .f.
            i_bRes = .f.
      	    i_cErrorMsg = Ah_MsgFormat("Inserire codice anagrafico")
          endif
        endif
      endif
      
      if i_bRes And (g_ANACOM<>'S' OR isAHR())
         IF not(this.w_DPTIPRIS<>'P' Or IIF (.w_CODISO='IT' OR EMPTY (.w_CODISO) , CHKCFP(.w_DPCODFIS, 'CFA', 'R',1,.w_DPCODICE,' ',.w_DPCOGNOM,.w_DPNOME,.w_DPNASLOC,.w_DPNASPRO,.w_DPDATNAS,.w_DP_SESSO,'S',,.w_CODCOM),.T.))
            i_bRes = .f.
          	i_bnoChk = .t.
         ENDIF
      ENDIF
      
      if i_bRes
         IF not(.w_DPTIPRIS<>'P' or empty(.w_DP__PATH) or directory(alltrim(.w_DP__PATH)))
            i_bRes = .f.
          	i_bnoChk = .f.
            i_cErrorMsg =Ah_MsgFormat( "La cartella impostata per il path per salvataggio files da print system non esiste")
         ENDIF
      ENDIF
      if i_bRes
         IF not(.w_DPTIPRIS<>'P' or empty(.w_DPPATHPR) or directory(alltrim(.w_DPPATHPR)))
            i_bRes = .f.
          	i_bnoChk = .f.
            i_cErrorMsg = Ah_MsgFormat("La cartella impostata per il path di prelevamento per archiviazione file non esiste")
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DPCODICE = this.w_DPCODICE
    this.o_DPCODANA = this.w_DPCODANA
    this.o_ANAZION = this.w_ANAZION
    this.o_DPCOGNOM = this.w_DPCOGNOM
    this.o_DPFLVISI = this.w_DPFLVISI
    this.o_DPNOME = this.w_DPNOME
    this.o_DPFLINEX = this.w_DPFLINEX
    this.o_DPPATHPR = this.w_DPPATHPR
    this.o_DP__PATH = this.w_DP__PATH
    this.o_DPCODFIS = this.w_DPCODFIS
    this.o_CODIFISC = this.w_CODIFISC
    this.o_DPCODUTE = this.w_DPCODUTE
    this.o_CONTA = this.w_CONTA
    this.o_LetturaParAgen = this.w_LetturaParAgen
    this.o_DPCODRES = this.w_DPCODRES
    this.o_DPPERFAT = this.w_DPPERFAT
    this.o_DPTIPRIG = this.w_DPTIPRIG
    * --- GSAR_MPH : Depends On
    this.GSAR_MPH.SaveDependsOn()
    * --- GSAR_ARD : Depends On
    this.GSAR_ARD.SaveDependsOn()
    * --- GSAR_MBR : Depends On
    this.GSAR_MBR.SaveDependsOn()
    * --- GSAR_MSR : Depends On
    this.GSAR_MSR.SaveDependsOn()
    * --- GSAR_MPD : Depends On
    this.GSAR_MPD.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsar_adpPag1 as StdContainer
  Width  = 779
  height = 535
  stdWidth  = 779
  stdheight = 535
  resizeXpos=405
  resizeYpos=184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDPCODICE_1_1 as StdField with uid="OTRKSURMJQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DPCODICE", cQueryName = "DPCODICE",nZero=5,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice #@ZA� non utilizzabile",;
    ToolTipText = "Codice",;
    HelpContextID = 133558917,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=48, Left=157, Top=40, InputMask=replicate('X',5)

  func oDPCODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DPTIPRIS<>'P' OR .w_DPCODICE<>"#@ZA�")
    endwith
    return bRes
  endfunc

  add object oDCLMAST_1_3 as StdField with uid="OVVXYWQFQS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DCLMAST", cQueryName = "DCLMAST",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 30969910,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=157, Top=11, InputMask=replicate('X',40), bHasZoom = .t. , bAnagrafica = .T.

  func oDCLMAST_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' or empty(.w_DPCODANA))
    endwith
   endif
  endfunc

  func oDCLMAST_1_3.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR !isAHE() or g_ANACOM<>'S')
    endwith
  endfunc

  func oDCLMAST_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      .w_DPCODANA = .w_DCLMAST
      * bres=this.parent.oDPCODANA_1_4.Check()
      bres=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDCLMAST_1_3.mZoom
    this.parent.oDPCODANA_1_4.mZoom()
  endproc

  add object oDPCODANA_1_4 as StdField with uid="EYCSZSXEHJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DPCODANA", cQueryName = "DPCODANA",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 267776649,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=693, Top=13, InputMask=replicate('X',15), bHasZoom = .t. , bAnagrafica = .T., cLinkFile="ANAGRAFE", oKey_1_1="ANCODICE", oKey_1_2="this.w_DPCODANA"

  func oDPCODANA_1_4.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  func oDPCODANA_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODANA_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODANA_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ANAGRAFE','*','ANCODICE',cp_AbsName(this.parent,'oDPCODANA_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ANAGRAFE",'',this.parent.oContained
  endproc


  add object oLinkPC_1_5 as StdButton with uid="KRAVBGFBCC",left=717, top=67, width=48,height=45,;
    CpPicture="bmp\folder.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla gestione path import eventi";
    , HelpContextID = 64033631;
    , caption='\<Path';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_5.Click()
      this.Parent.oContained.GSAR_MPH.LinkPCClick()
    endproc

  func oLinkPC_1_5.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_cTipRis<>'G' or g_AGFA<>'S')
     endwith
    endif
  endfunc

  add object oDPBADGE_1_16 as StdField with uid="WKDLRCKMXM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DPBADGE", cQueryName = "DPBADGE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice badge",;
    HelpContextID = 100400438,;
   bGlobalFont=.t.,;
    Height=21, Width=103, Left=157, Top=66, InputMask=replicate('X',10)

  func oDPBADGE_1_16.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oDPDESCRI_1_17 as StdField with uid="QDDZRIYJTZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DPDESCRI", cQueryName = "DPDESCRI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 219144833,;
   bGlobalFont=.t.,;
    Height=21, Width=344, Left=421, Top=40, InputMask=replicate('X',60)

  func oDPDESCRI_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
   endif
  endfunc

  func oDPDESCRI_1_17.mHide()
    with this.Parent.oContained
      return (.w_cTipRis=='P')
    endwith
  endfunc

  add object oDPCODCEN_1_18 as StdField with uid="QLTOIPACTB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DPCODCEN", cQueryName = "DPCODCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 34213252,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=157, Top=66, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_DPCODCEN"

  func oDPCODCEN_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_cTipRis=='R')
    endwith
   endif
  endfunc

  func oDPCODCEN_1_18.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR .w_cTipRis<>'R')
    endwith
  endfunc

  func oDPCODCEN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODCEN_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODCEN_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oDPCODCEN_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oDPCODCEN_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_DPCODCEN
     i_obj.ecpSave()
  endproc

  add object oDPSERPRE_1_19 as StdField with uid="KWNOKBWYFC",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DPSERPRE", cQueryName = "DPSERPRE",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Prestazione predefinita",;
    HelpContextID = 2028165,;
   bGlobalFont=.t.,;
    Height=21, Width=314, Left=157, Top=89, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_DPSERPRE"

  func oDPSERPRE_1_19.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'R' OR IsAlt())
    endwith
  endfunc

  func oDPSERPRE_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPSERPRE_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPSERPRE_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oDPSERPRE_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSMA0AAS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oDPSERPRE_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_DPSERPRE
     i_obj.ecpSave()
  endproc

  add object oDPGRUPRE_1_20 as StdField with uid="HRWLKQOLPE",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DPGRUPRE", cQueryName = "DPGRUPRE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 266515077,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=89, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_DPGRUPRE"

  func oDPGRUPRE_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oDPGRUPRE_1_20.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  func oDPGRUPRE_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPGRUPRE_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPGRUPRE_1_20.mZoom
      with this.Parent.oContained
        GSAR1BGP(this.Parent.oContained,  "", .w_DPCODICE, "", "DPGRUPRE")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDPCOSORA_1_21 as StdField with uid="ULKKDZQMYY",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DPCOSORA", cQueryName = "DPCOSORA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo orario standard",;
    HelpContextID = 17166985,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=157, Top=124, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oDPCOSORA_1_21.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'R')
    endwith
  endfunc

  add object oDPCOGNOM_1_22 as StdField with uid="UATZPEHOXD",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DPCOGNOM", cQueryName = "DPCOGNOM",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome persona",;
    HelpContextID = 46527101,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=421, Top=40, InputMask=replicate('X',50)

  func oDPCOGNOM_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S' AND .w_cTipRis=='P')
    endwith
   endif
  endfunc

  func oDPCOGNOM_1_22.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc


  add object oDPFLVISI_1_23 as StdCombo with uid="KBOISDOECR",rtseq=22,rtrep=.f.,left=421,top=67,width=188,height=21;
    , HelpContextID = 114868865;
    , cFormVar="w_DPFLVISI",RowSource=""+"Solo responsabili,"+"Tutti i membri del gruppo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDPFLVISI_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oDPFLVISI_1_23.GetRadio()
    this.Parent.oContained.w_DPFLVISI = this.RadioValue()
    return .t.
  endfunc

  func oDPFLVISI_1_23.SetRadio()
    this.Parent.oContained.w_DPFLVISI=trim(this.Parent.oContained.w_DPFLVISI)
    this.value = ;
      iif(this.Parent.oContained.w_DPFLVISI=='S',1,;
      iif(this.Parent.oContained.w_DPFLVISI=='T',2,;
      0))
  endfunc

  func oDPFLVISI_1_23.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'G')
    endwith
  endfunc

  add object oDPNOME_1_24 as StdField with uid="UHFBUHUPDX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DPNOME", cQueryName = "DPNOME",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome persona",;
    HelpContextID = 191185610,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=421, Top=66, InputMask=replicate('X',50)

  func oDPNOME_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  func oDPNOME_1_24.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oDPFLINEX_1_25 as StdRadio with uid="KOHMLGLKHX",rtseq=24,rtrep=.f.,left=673, top=189, width=108,height=59;
    , ToolTipText = "Tipologia: interna (reparto) o esterna (fornitore)";
    , cFormVar="w_DPFLINEX", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDPFLINEX_1_25.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Interno"
      this.Buttons(1).HelpContextID = 223821198
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Esterno"
      this.Buttons(2).HelpContextID = 223821198
      this.Buttons(2).Top=28
      this.SetAll("Width",106)
      this.SetAll("Height",30)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipologia: interna (reparto) o esterna (fornitore)")
      StdRadio::init()
    endproc

  func oDPFLINEX_1_25.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oDPFLINEX_1_25.GetRadio()
    this.Parent.oContained.w_DPFLINEX = this.RadioValue()
    return .t.
  endfunc

  func oDPFLINEX_1_25.SetRadio()
    this.Parent.oContained.w_DPFLINEX=trim(this.Parent.oContained.w_DPFLINEX)
    this.value = ;
      iif(this.Parent.oContained.w_DPFLINEX=='I',1,;
      iif(this.Parent.oContained.w_DPFLINEX=='E',2,;
      0))
  endfunc

  func oDPFLINEX_1_25.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR IsAlt())
    endwith
  endfunc

  add object oDPREPART_1_26 as StdField with uid="IHDZKVSIIF",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DPREPART", cQueryName = "DPREPART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice reparto",;
    HelpContextID = 255787638,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=157, Top=187, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", cZoomOnZoom="GSDS_ARP", oKey_1_1="RL__TIPO", oKey_1_2="this.w_TIPRIS", oKey_2_1="RLCODICE", oKey_2_2="this.w_DPREPART"

  func oDPREPART_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPFLINEX='I')
    endwith
   endif
  endfunc

  func oDPREPART_1_26.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR IIF(isAHE(), g_DIBA<>"S", g_PROD<>"S"))
    endwith
  endfunc

  func oDPREPART_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPREPART_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPREPART_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPRIS)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oDPREPART_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_ARP',"Reparto",'',this.parent.oContained
  endproc
  proc oDPREPART_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSDS_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.RL__TIPO=w_TIPRIS
     i_obj.w_RLCODICE=this.parent.oContained.w_DPREPART
     i_obj.ecpSave()
  endproc

  add object oDESRIS_1_27 as StdField with uid="KMPVFXGCKN",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESRIS", cQueryName = "DESRIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 228720074,;
   bGlobalFont=.t.,;
    Height=21, Width=266, Left=329, Top=187, InputMask=replicate('X',40)

  func oDESRIS_1_27.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR IIF(isAHE(), g_DIBA<>"S", g_PROD<>"S"))
    endwith
  endfunc

  add object oDPDITTA_1_28 as StdField with uid="SXTEAVZALH",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DPDITTA", cQueryName = "DPDITTA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido",;
    ToolTipText = "Codice fornitore",;
    HelpContextID = 201056970,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=157, Top=214, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DPTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DPDITTA"

  func oDPDITTA_1_28.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR .w_DPFLINEX="I")
    endwith
  endfunc

  func oDPDITTA_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPDITTA_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPDITTA_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DPTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DPTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDPDITTA_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Ditta",'',this.parent.oContained
  endproc
  proc oDPDITTA_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DPTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DPDITTA
     i_obj.ecpSave()
  endproc

  add object oDPCODDES_1_29 as StdField with uid="MZYMBTZEKW",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DPCODDES", cQueryName = "DPCODDES",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Unit� organizzativa",;
    HelpContextID = 50990473,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=214, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SEDIAZIE", cZoomOnZoom="GSAR_BU2", oKey_1_1="SECODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="SECODDES", oKey_2_2="this.w_DPCODDES"

  func oDPCODDES_1_29.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR .w_DPFLINEX="E")
    endwith
  endfunc

  func oDPCODDES_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODDES_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODDES_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SEDIAZIE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'SEDIAZIE','*','SECODAZI,SECODDES',cp_AbsName(this.parent,'oDPCODDES_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BU2',"Sedi / unit� organizzative",'GSAR_AU2.SEDIAZIE_VZM',this.parent.oContained
  endproc
  proc oDPCODDES_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BU2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODAZI=w_CODAZI
     i_obj.w_SECODDES=this.parent.oContained.w_DPCODDES
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_30 as StdField with uid="DKMNJJIBWZ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38862282,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=292, Top=214, InputMask=replicate('X',40)

  func oDESCON_1_30.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR .w_DPFLINEX="I")
    endwith
  endfunc

  add object oDPMANSION_1_31 as StdField with uid="VKRNKYRIBR",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DPMANSION", cQueryName = "DPMANSION",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Mansione persona",;
    HelpContextID = 43823717,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=241, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MANSIONI", cZoomOnZoom="GSAR_AMN", oKey_1_1="MNCODICE", oKey_1_2="this.w_DPMANSION"

  func oDPMANSION_1_31.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  func oDPMANSION_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPMANSION_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPMANSION_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MANSIONI','*','MNCODICE',cp_AbsName(this.parent,'oDPMANSION_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMN',"Mansione",'',this.parent.oContained
  endproc
  proc oDPMANSION_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MNCODICE=this.parent.oContained.w_DPMANSION
     i_obj.ecpSave()
  endproc

  add object oDESMAN_1_32 as StdField with uid="AQYLXQTSWJ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESMAN", cQueryName = "DESMAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 52886986,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=222, Top=241, InputMask=replicate('X',40)

  func oDESMAN_1_32.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oDPCOSORA_1_33 as StdField with uid="DZNJNLYPZT",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DPCOSORA", cQueryName = "DPCOSORA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 17166985,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=157, Top=268, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oDPCOSORA_1_33.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oDPCODCEN_1_34 as StdField with uid="XMAJYYJCZO",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DPCODCEN", cQueryName = "DPCODCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 34213252,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=157, Top=295, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_DPCODCEN"

  func oDPCODCEN_1_34.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR .w_cTipRis<>'P')
    endwith
  endfunc

  func oDPCODCEN_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODCEN_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODCEN_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oDPCODCEN_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oDPCODCEN_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_DPCODCEN
     i_obj.ecpSave()
  endproc

  add object oDPCODCAL_1_35 as StdField with uid="EOOZPFJOEX",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DPCODCAL", cQueryName = "DPCODCAL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 234222206,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=157, Top=321, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", cZoomOnZoom="GSAR_ATL", oKey_1_1="TCCODICE", oKey_1_2="this.w_DPCODCAL"

  func oDPCODCAL_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPFLINEX='I')
    endwith
   endif
  endfunc

  func oDPCODCAL_1_35.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  func oDPCODCAL_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODCAL_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODCAL_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oDPCODCAL_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATL',"Calendari",'',this.parent.oContained
  endproc
  proc oDPCODCAL_1_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_DPCODCAL
     i_obj.ecpSave()
  endproc


  add object oDPMODALL_1_36 as StdCombo with uid="PGGSIVYWMZ",rtseq=35,rtrep=.f.,left=157,top=350,width=123,height=21;
    , HelpContextID = 699778;
    , cFormVar="w_DPMODALL",RowSource=""+"Collegamento,"+"Copia file,"+"Sposta,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDPMODALL_1_36.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'F',;
    iif(this.value =3,'S',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oDPMODALL_1_36.GetRadio()
    this.Parent.oContained.w_DPMODALL = this.RadioValue()
    return .t.
  endfunc

  func oDPMODALL_1_36.SetRadio()
    this.Parent.oContained.w_DPMODALL=trim(this.Parent.oContained.w_DPMODALL)
    this.value = ;
      iif(this.Parent.oContained.w_DPMODALL=='L',1,;
      iif(this.Parent.oContained.w_DPMODALL=='F',2,;
      iif(this.Parent.oContained.w_DPMODALL=='S',3,;
      iif(this.Parent.oContained.w_DPMODALL=='N',4,;
      0))))
  endfunc

  func oDPMODALL_1_36.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR !IsAlt())
    endwith
  endfunc

  add object oDPSERVIZ_1_38 as StdField with uid="FRYJLXNNCR",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DPSERVIZ", cQueryName = "DPSERVIZ",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Codice servizio (manodopera)",;
    HelpContextID = 98635152,;
   bGlobalFont=.t.,;
    Height=21, Width=314, Left=157, Top=348, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_DPSERVIZ"

  func oDPSERVIZ_1_38.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR IsAlt())
    endwith
  endfunc

  func oDPSERVIZ_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPSERVIZ_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPSERVIZ_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDPSERVIZ_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Servizi",'gspc_zse.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDPSERVIZ_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DPSERVIZ
     i_obj.ecpSave()
  endproc

  add object oDESSER_1_39 as StdField with uid="NGHQLIDTQN",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESSER", cQueryName = "DESSER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 249626058,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=482, Top=349, InputMask=replicate('X',40)

  func oDESSER_1_39.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR g_COMM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oDPSERPRE_1_40 as StdField with uid="DJXMBZSJYB",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DPSERPRE", cQueryName = "DPSERPRE",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Prestazione predefinita",;
    HelpContextID = 2028165,;
   bGlobalFont=.t.,;
    Height=21, Width=314, Left=157, Top=374, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_DPSERPRE"

  func oDPSERPRE_1_40.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  func oDPSERPRE_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPSERPRE_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPSERPRE_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oDPSERPRE_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSMA0AAS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oDPSERPRE_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_DPSERPRE
     i_obj.ecpSave()
  endproc

  add object oDPPATHPR_1_41 as StdField with uid="XPJYYXJQQL",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DPPATHPR", cQueryName = "DPPATHPR",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path di prelevamento per archiviazione file",;
    HelpContextID = 134423160,;
   bGlobalFont=.t.,;
    Height=21, Width=586, Left=157, Top=410, InputMask=replicate('X',254)

  func oDPPATHPR_1_41.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oDP__PATH_1_42 as StdField with uid="XTMOHABFNJ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DP__PATH", cQueryName = "DP__PATH",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path predefinito per salvataggio files da print system",;
    HelpContextID = 254030466,;
   bGlobalFont=.t.,;
    Height=21, Width=586, Left=157, Top=432, InputMask=replicate('X',254)

  func oDP__PATH_1_42.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oDP__DIKE_1_43 as StdField with uid="VMUGUAYBML",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DP__DIKE", cQueryName = "DP__DIKE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path predefinito per programma di firma ",;
    HelpContextID = 136039803,;
   bGlobalFont=.t.,;
    Height=21, Width=586, Left=157, Top=455, InputMask=replicate('X',254)

  func oDP__DIKE_1_43.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' or !Isalt())
    endwith
  endfunc


  add object oObj_1_52 as cp_runprogram with uid="IHCQZRHEQI",left=-4, top=556, width=282,height=20,;
    caption='GSAR_BDP(BGD)',;
   bGlobalFont=.t.,;
    prg="GSAR_BDP('BGD')",;
    cEvent = "Insert start,Update start",;
    nPag=1;
    , HelpContextID = 174742218


  add object oObj_1_60 as cp_runprogram with uid="BRTKWQIYWN",left=-4, top=578, width=317,height=19,;
    caption='GSAR_BDP(COFR)',;
   bGlobalFont=.t.,;
    prg="GSAR_BDP('COFR')",;
    cEvent = "Insert start,Update start,Delete start",;
    nPag=1;
    , HelpContextID = 19408438

  add object oDESSED_1_62 as StdField with uid="PHTPZWKCDE",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESSED", cQueryName = "DESSED",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 216071626,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=222, Top=214, InputMask=replicate('X',40)

  func oDESSED_1_62.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR .w_DPFLINEX="E")
    endwith
  endfunc


  add object LBLVAL as cp_calclbl with uid="MMPJSRZHIB",left=307, top=267, width=100,height=17,;
    caption='LBLVAL',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 85880906


  add object oLinkPC_1_76 as stdDynamicChildContainer with uid="JVAQFICSLI",left=717, top=298, width=48, height=45, bOnScreen=.t.;


  func oLinkPC_1_76.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.t.)
     endwith
    endif
  endfunc

  add object oDPCODUTE_1_80 as StdField with uid="YRWMYDMYFK",rtseq=109,rtrep=.f.,;
    cFormVar = "w_DPCODUTE", cQueryName = "DPCODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 67767675,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=425, Top=126, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_DPCODUTE"

  func oDPCODUTE_1_80.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR !IsAlt())
    endwith
  endfunc

  func oDPCODUTE_1_80.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_80('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODUTE_1_80.ecpDrop(oSource)
    this.Parent.oContained.link_1_80('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODUTE_1_80.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oDPCODUTE_1_80'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'GSAR_ADP.CPUSERS_VZM',this.parent.oContained
  endproc

  add object oDESUTE_1_81 as StdField with uid="MUTDIECZUX",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 183434698,;
   bGlobalFont=.t.,;
    Height=21, Width=284, Left=481, Top=126, InputMask=replicate('X',20)

  func oDESUTE_1_81.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR !IsAlt())
    endwith
  endfunc

  add object oDP__NOTE_1_82 as StdMemo with uid="DLEYLOLRGX",rtseq=111,rtrep=.f.,;
    cFormVar = "w_DP__NOTE", cQueryName = "DP__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 21246597,;
   bGlobalFont=.t.,;
    Height=278, Width=610, Left=157, Top=150

  func oDP__NOTE_1_82.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_cTipRis=='R')
    endwith
   endif
  endfunc

  func oDP__NOTE_1_82.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'R')
    endwith
  endfunc


  add object oObj_1_84 as cp_setobjprop with uid="ZUTJKMVIEY",left=599, top=572, width=119,height=19,;
    caption='ToolTipText',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_DPCOSORA",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 114185254


  add object oLinkPC_1_85 as stdDynamicChildContainer with uid="DYXDDTDWIZ",left=157, top=124, width=586, height=278, bOnScreen=.t.;


  func oLinkPC_1_85.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_cTipRis<>'G')
     endwith
    endif
  endfunc

  add object oDPCODCAL_1_86 as StdField with uid="VJWUPMODCR",rtseq=114,rtrep=.f.,;
    cFormVar = "w_DPCODCAL", cQueryName = "DPCODCAL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Calendario associato",;
    HelpContextID = 234222206,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=157, Top=410, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", cZoomOnZoom="GSAR_ATL", oKey_1_1="TCCODICE", oKey_1_2="this.w_DPCODCAL"

  func oDPCODCAL_1_86.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'G')
    endwith
  endfunc

  func oDPCODCAL_1_86.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_86('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODCAL_1_86.ecpDrop(oSource)
    this.Parent.oContained.link_1_86('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODCAL_1_86.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oDPCODCAL_1_86'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATL',"Calendari",'',this.parent.oContained
  endproc
  proc oDPCODCAL_1_86.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_DPCODCAL
     i_obj.ecpSave()
  endproc


  add object oObj_1_88 as cp_runprogram with uid="HAKIRFHDTD",left=399, top=572, width=184,height=19,;
    caption='GSAR_BDP(AGGL)',;
   bGlobalFont=.t.,;
    prg="GSAR_BDP('AGGL')",;
    cEvent = "AggLang",;
    nPag=1;
    , HelpContextID = 13149238

  add object oCCDESPIA_1_91 as StdField with uid="QVKOSEQBBL",rtseq=115,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 267391079,;
   bGlobalFont=.t.,;
    Height=21, Width=309, Left=291, Top=295, InputMask=replicate('X',40)

  func oCCDESPIA_1_91.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR .w_cTipRis<>'P')
    endwith
  endfunc

  add object oTCDESCRI_1_92 as StdField with uid="SFTWIIHWTS",rtseq=116,rtrep=.f.,;
    cFormVar = "w_TCDESCRI", cQueryName = "TCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219147905,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=244, Top=321, InputMask=replicate('X',40)

  func oTCDESCRI_1_92.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oCCDESPIA_1_97 as StdField with uid="DXFLTOOQAS",rtseq=119,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 267391079,;
   bGlobalFont=.t.,;
    Height=21, Width=467, Left=298, Top=66, InputMask=replicate('X',40)

  func oCCDESPIA_1_97.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR .w_cTipRis<>'R')
    endwith
  endfunc

  add object oDPDESGRU_1_99 as StdField with uid="MCCOJZOIXQ",rtseq=120,rtrep=.f.,;
    cFormVar = "w_DPDESGRU", cQueryName = "DPDESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 152035957,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=226, Top=89, InputMask=replicate('X',40)

  func oDPDESGRU_1_99.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oTCDESCRI_1_102 as StdField with uid="ONDQACYLDF",rtseq=122,rtrep=.f.,;
    cFormVar = "w_TCDESCRI", cQueryName = "TCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219147905,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=252, Top=410, InputMask=replicate('X',40)

  func oTCDESCRI_1_102.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'G')
    endwith
  endfunc

  add object oDESPRE_1_107 as StdField with uid="ILFOWTVCGF",rtseq=124,rtrep=.f.,;
    cFormVar = "w_DESPRE", cQueryName = "DESPRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 185859530,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=482, Top=374, InputMask=replicate('X',40)

  func oDESPRE_1_107.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc


  add object oBtn_1_108 as StdButton with uid="TWIMIXDREE",left=747, top=412, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 175741398;
  , bGlobalFont=.t.

    proc oBtn_1_108.Click()
      with this.Parent.oContained
        .w_DPPATHPR=left(cp_getdir(IIF(EMPTY(.w_DPPATHPR),sys(5)+sys(2003),.w_DPPATHPR),"Percorso di prelevamento")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_108.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_cTipRis<>'P')
     endwith
    endif
  endfunc

  add object oDESPRE_1_109 as StdField with uid="DCJAJMCOGL",rtseq=125,rtrep=.f.,;
    cFormVar = "w_DESPRE", cQueryName = "DESPRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 185859530,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=479, Top=89, InputMask=replicate('X',40)

  func oDESPRE_1_109.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'R' OR IsAlt())
    endwith
  endfunc

  add object oDPDTOBSO_1_113 as StdField with uid="LVPBAEQRKW",rtseq=127,rtrep=.f.,;
    cFormVar = "w_DPDTOBSO", cQueryName = "DPDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 239133307,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=687, Top=511, tabstop=.f.


  add object oBtn_1_122 as StdButton with uid="RVGGFFIQOS",left=747, top=436, width=19,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Assegna path predefinito per salvataggio dei files";
    , HelpContextID = 175741398;
  , bGlobalFont=.t.

    proc oBtn_1_122.Click()
      with this.Parent.oContained
        .w_DP__PATH=left(cp_getdir(IIF(EMPTY(.w_DPPATHPR),sys(5)+sys(2003),.w_DPPATHPR),"Percorso di prelevamento")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_122.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_cTipRis<>'P')
     endwith
    endif
  endfunc


  add object BTNQRY as cp_askfile with uid="SVNANQOYOT",left=748, top=459, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_DP__DIKE",;
    nPag=1;
    , ToolTipText = "Premere per selezionare programma";
    , HelpContextID = 175741398

  add object oStr_1_44 as StdString with uid="SHDLNQSJOI",Visible=.t., Left=88, Top=40,;
    Alignment=1, Width=68, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="UMBSFFWXPL",Visible=.t., Left=95, Top=66,;
    Alignment=1, Width=61, Height=18,;
    Caption="Badge:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="MTTINMRFIJ",Visible=.t., Left=350, Top=40,;
    Alignment=1, Width=66, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="DGUEWTPEHS",Visible=.t., Left=379, Top=73,;
    Alignment=1, Width=37, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="IOXLWMTEMQ",Visible=.t., Left=100, Top=217,;
    Alignment=1, Width=56, Height=15,;
    Caption="Ditta:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR .w_DPFLINEX="I" OR EMPTY (.w_DPFLINEX))
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="LAQWYZQJNM",Visible=.t., Left=86, Top=189,;
    Alignment=1, Width=70, Height=15,;
    Caption="Reparto:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR IIF(isAHE(), g_DIBA<>"S", g_PROD<>"S"))
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="UNUNGPODSU",Visible=.t., Left=99, Top=241,;
    Alignment=1, Width=57, Height=15,;
    Caption="Mansione:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="ARWGGDJUWZ",Visible=.t., Left=70, Top=268,;
    Alignment=1, Width=86, Height=18,;
    Caption="Costo orario:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="VTMBPWGXGW",Visible=.t., Left=600, Top=189,;
    Alignment=1, Width=68, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR IsAlt())
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="LWNAYJTRXG",Visible=.t., Left=7, Top=156,;
    Alignment=0, Width=69, Height=18,;
    Caption="Principali"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="JXSRZMWFXO",Visible=.t., Left=12, Top=214,;
    Alignment=1, Width=144, Height=18,;
    Caption="Unit� organizzativa/sede:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR .w_DPFLINEX="E"  )
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="QIQNVXWTJF",Visible=.t., Left=98, Top=351,;
    Alignment=1, Width=58, Height=15,;
    Caption="Servizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR IsAlt())
    endwith
  endfunc

  add object oStr_1_71 as StdString with uid="JJIIKCKMDO",Visible=.t., Left=80, Top=15,;
    Alignment=1, Width=76, Height=18,;
    Caption="Anagrafico:"  ;
  , bGlobalFont=.t.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR !isAHE() or g_ANACOM<>'S')
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="KMHFOPVJQP",Visible=.t., Left=1, Top=696,;
    Alignment=0, Width=771, Height=21,;
    Caption="ATTENZIONE: non inserire obbligatoriet� sul campo DCLMAST altrimenti non funziona la linked using"  ;
    , FontName = "Arial", FontSize = 11, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="RJGCWLKBXA",Visible=.t., Left=301, Top=128,;
    Alignment=1, Width=122, Height=18,;
    Caption="Codice utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR !IsAlt())
    endwith
  endfunc

  add object oStr_1_87 as StdString with uid="EKIPDBBYYG",Visible=.t., Left=302, Top=40,;
    Alignment=1, Width=114, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (.w_cTipRis=='P')
    endwith
  endfunc

  add object oStr_1_89 as StdString with uid="KUHLICDXXO",Visible=.t., Left=13, Top=325,;
    Alignment=1, Width=143, Height=18,;
    Caption="Calendario associato:"  ;
  , bGlobalFont=.t.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_90 as StdString with uid="UPPNGAYCVJ",Visible=.t., Left=93, Top=297,;
    Alignment=1, Width=63, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR .w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="PWFNLTANWE",Visible=.t., Left=82, Top=150,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'R')
    endwith
  endfunc

  add object oStr_1_95 as StdString with uid="XONQNKVZDM",Visible=.t., Left=241, Top=70,;
    Alignment=1, Width=175, Height=18,;
    Caption="Visibilit� intragruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_95.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'G')
    endwith
  endfunc

  add object oStr_1_96 as StdString with uid="MFRQRVUMNG",Visible=.t., Left=93, Top=70,;
    Alignment=1, Width=63, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  func oStr_1_96.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR .w_cTipRis<>'R')
    endwith
  endfunc

  add object oStr_1_98 as StdString with uid="UCIWQRUCQX",Visible=.t., Left=38, Top=93,;
    Alignment=1, Width=118, Height=18,;
    Caption="Gruppo predefinito:"  ;
  , bGlobalFont=.t.

  func oStr_1_98.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_101 as StdString with uid="LMGLJVWBZK",Visible=.t., Left=29, Top=413,;
    Alignment=1, Width=127, Height=18,;
    Caption="Calendario associato:"  ;
  , bGlobalFont=.t.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'G')
    endwith
  endfunc

  add object oStr_1_103 as StdString with uid="UHXFHMTUHI",Visible=.t., Left=70, Top=127,;
    Alignment=1, Width=86, Height=18,;
    Caption="Costo orario:"  ;
  , bGlobalFont=.t.

  func oStr_1_103.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'R')
    endwith
  endfunc

  add object oStr_1_104 as StdString with uid="PPBBWJEPYS",Visible=.t., Left=27, Top=376,;
    Alignment=1, Width=129, Height=18,;
    Caption="Prestazione predefinita:"  ;
  , bGlobalFont=.t.

  func oStr_1_104.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_106 as StdString with uid="TUIBENLSIE",Visible=.t., Left=27, Top=93,;
    Alignment=1, Width=129, Height=18,;
    Caption="Prestazione predefinita:"  ;
  , bGlobalFont=.t.

  func oStr_1_106.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'R' OR IsAlt())
    endwith
  endfunc

  add object oStr_1_112 as StdString with uid="XXVHLTXLSK",Visible=.t., Left=556, Top=513,;
    Alignment=1, Width=128, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_117 as StdString with uid="RUDPMMKEHL",Visible=.t., Left=5, Top=413,;
    Alignment=1, Width=151, Height=18,;
    Caption="Path di prelevamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_117.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_120 as StdString with uid="LLQWTFCZLN",Visible=.t., Left=20, Top=348,;
    Alignment=1, Width=136, Height=18,;
    Caption="Modalit� di archiviazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_120.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR !IsAlt())
    endwith
  endfunc

  add object oStr_1_121 as StdString with uid="YHQTUDPMHT",Visible=.t., Left=26, Top=436,;
    Alignment=1, Width=130, Height=18,;
    Caption="Path di salvataggio:"  ;
  , bGlobalFont=.t.

  func oStr_1_121.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_1_128 as StdString with uid="INEFGPVFWB",Visible=.t., Left=10, Top=457,;
    Alignment=1, Width=146, Height=18,;
    Caption="Software per firma digitale:"  ;
  , bGlobalFont=.t.

  func oStr_1_128.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' or !Isalt())
    endwith
  endfunc

  add object oBox_1_56 as StdBox with uid="OXZENFZFPU",left=1, top=174, width=685,height=2
enddefine
define class tgsar_adpPag2 as StdContainer
  Width  = 779
  height = 535
  stdWidth  = 779
  stdheight = 535
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDPCODFIS_2_1 as StdField with uid="LRFWZDDUQH",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DPCODFIS", cQueryName = "DPCODFIS",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale - Doppio click per eseguire il calcolo del codice fiscale",;
    HelpContextID = 84544905,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=140, Top=18, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16), bHasZoom = .t. 

  func oDPCODFIS_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERFIS<>'S' and g_ANACOM<>'S')
    endwith
   endif
  endfunc

  func oDPCODFIS_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(g_ANACOM<>'S' and .w_DPTIPRIG='P',CHKCFP(.w_DPCODFIS, 'CF', 'R',0,.w_DPCODICE),.T.))
    endwith
    return bRes
  endfunc

  proc oDPCODFIS_2_1.mZoom
    ZoomCF (  this.parent.oContained )
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDPPARIVA_2_2 as StdField with uid="OYJSPCMCDI",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DPPARIVA", cQueryName = "DPPARIVA",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 148692343,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=435, Top=18, InputMask=replicate('X',12)

  func oDPPARIVA_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  add object oDPNUMCAR_2_4 as StdField with uid="TUUIRRQAZJ",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DPNUMCAR", cQueryName = "DPNUMCAR",;
    bObbl = .f. , nPag = 2, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 224346744,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=141, Top=48, InputMask=replicate('X',18)

  func oDPNUMCAR_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc


  add object oDP_SESSO_2_5 as StdCombo with uid="JIOWUUYHNU",rtseq=63,rtrep=.f.,left=142,top=88,width=88,height=21;
    , ToolTipText = "Sesso";
    , HelpContextID = 232796795;
    , cFormVar="w_DP_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDP_SESSO_2_5.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDP_SESSO_2_5.GetRadio()
    this.Parent.oContained.w_DP_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oDP_SESSO_2_5.SetRadio()
    this.Parent.oContained.w_DP_SESSO=trim(this.Parent.oContained.w_DP_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_DP_SESSO=='M',1,;
      iif(this.Parent.oContained.w_DP_SESSO=='F',2,;
      0))
  endfunc

  func oDP_SESSO_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERFIS<>'S' and g_ANACOM<>'S')
    endwith
   endif
  endfunc

  add object oDPNASLOC_2_7 as StdField with uid="HJEAYJGHRT",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DPNASLOC", cQueryName = "DPNASLOC",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di nascita",;
    HelpContextID = 68371079,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=140, Top=116, InputMask=replicate('X',40), bHasZoom = .t. 

  func oDPNASLOC_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERFIS<>'S' and g_ANACOM<>'S')
    endwith
   endif
  endfunc

  proc oDPNASLOC_2_7.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_DPNASLOC",".w_DPNASPRO",,,".w_CODCOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDPNASPRO_2_10 as StdField with uid="ENEDQMGIKP",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DPNASPRO", cQueryName = "DPNASPRO",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 1262203,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=493, Top=116, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oDPNASPRO_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERFIS<>'S' and g_ANACOM<>'S')
    endwith
   endif
  endfunc

  proc oDPNASPRO_2_10.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_DPNASPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDPNASNAZ_2_11 as StdField with uid="HEFSECUGTZ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DPNASNAZ", cQueryName = "DPNASNAZ",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Nazione (di nascita)",;
    HelpContextID = 34816624,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=140, Top=142, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_DPNASNAZ"

  func oDPNASNAZ_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPNASNAZ_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPNASNAZ_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oDPNASNAZ_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"",'',this.parent.oContained
  endproc
  proc oDPNASNAZ_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_DPNASNAZ
     i_obj.ecpSave()
  endproc

  add object oDESNAZ1_2_12 as StdField with uid="FCKLIRQHRI",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESNAZ1", cQueryName = "DESNAZ1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 119930314,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=190, Top=142, InputMask=replicate('X',35)

  add object oDPDATNAS_2_14 as StdField with uid="LQIUMXCLUE",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DPDATNAS", cQueryName = "DPDATNAS",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 33809015,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=140, Top=169

  func oDPDATNAS_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERFIS<>'S' and g_ANACOM<>'S')
    endwith
   endif
  endfunc

  add object oDPTITSTU_2_16 as StdField with uid="AMULFEYCXQ",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DPTITSTU", cQueryName = "DPTITSTU",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Titolo di studio",;
    HelpContextID = 50666891,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=140, Top=219, InputMask=replicate('X',40)

  add object oDPQUALIF_2_17 as StdField with uid="UKXNSRERGC",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DPQUALIF", cQueryName = "DPQUALIF",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Qualifica",;
    HelpContextID = 182513020,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=140, Top=246, InputMask=replicate('X',40)

  add object oDPLIVELL_2_18 as StdField with uid="NUHMPJTFMX",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DPLIVELL", cQueryName = "DPLIVELL",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Livello",;
    HelpContextID = 86285698,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=140, Top=273, InputMask=replicate('X',40)

  add object oDPDATASS_2_21 as StdField with uid="SCYLXSLKGQ",rtseq=72,rtrep=.f.,;
    cFormVar = "w_DPDATASS", cQueryName = "DPDATASS",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data assunzione",;
    HelpContextID = 251912823,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=609, Top=227

  add object oDPDATFRA_2_22 as StdField with uid="IUIIVWCPYB",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DPDATFRA", cQueryName = "DPDATFRA",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine rapporto",;
    HelpContextID = 168026761,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=609, Top=255


  add object oBtn_2_31 as StdButton with uid="YXOXMAGUJB",left=622, top=10, width=48,height=45,;
    CpPicture="bmp\ImportCF.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per aggiornare i dati anagrafici dal codice fiscale";
    , HelpContextID = 101513932;
    , tabstop=.f., caption='\<Cod.Fisc';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_31.Click()
      with this.Parent.oContained
        iif(AH_yesno("Si vuole aggiornare sesso, luogo, provincia e data di nascita in base al codice fiscale?"), .notifyevent("AggiornaCF"),'')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_31.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_ANACOM='S' OR IsAlt())
     endwith
    endif
  endfunc


  add object oLinkPC_2_35 as StdButton with uid="AVAVFGSVLV",left=140, top=300, width=48,height=45,;
    CpPicture="bmp\DM_polisweb.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per configurare i parametri di connessione a PolisWeb";
    , HelpContextID = 35044598;
    , Caption='\<Polisweb';
  , bGlobalFont=.t.

    proc oLinkPC_2_35.Click()
      this.Parent.oContained.GSAR_MPD.LinkPCClick()
      this.Parent.oContained.WriteTo_GSAR_MPD()
    endproc

  func oLinkPC_2_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (IsAlt() AND .w_DPTIPRIS='P')
      endwith
    endif
  endfunc

  func oLinkPC_2_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!IsAlt() OR .w_DPTIPRIS<>'P')
     endwith
    endif
  endfunc

  add object oStr_2_6 as StdString with uid="VCXZAEHBHL",Visible=.t., Left=41, Top=18,;
    Alignment=1, Width=93, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="HGXTQNBONN",Visible=.t., Left=68, Top=116,;
    Alignment=1, Width=66, Height=18,;
    Caption="Nato a:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="CGPOMRJXSI",Visible=.t., Left=458, Top=116,;
    Alignment=1, Width=30, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="SCJLHGANON",Visible=.t., Left=38, Top=169,;
    Alignment=1, Width=96, Height=18,;
    Caption="Data di nascita:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="MONWNWDRAK",Visible=.t., Left=35, Top=219,;
    Alignment=1, Width=99, Height=18,;
    Caption="Titolo di studio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="RDPKMWMXCD",Visible=.t., Left=76, Top=144,;
    Alignment=1, Width=58, Height=18,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="JWYSYQMODN",Visible=.t., Left=494, Top=229,;
    Alignment=1, Width=110, Height=18,;
    Caption="Data assunzione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="IQWFUHZBPS",Visible=.t., Left=61, Top=246,;
    Alignment=1, Width=73, Height=18,;
    Caption="Qualifica:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="DYFTADIZZK",Visible=.t., Left=76, Top=273,;
    Alignment=1, Width=58, Height=18,;
    Caption="Livello:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="SNXEAUNIBG",Visible=.t., Left=85, Top=87,;
    Alignment=1, Width=52, Height=18,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="QQMNFYSKMK",Visible=.t., Left=439, Top=255,;
    Alignment=1, Width=165, Height=18,;
    Caption="Data fine rapporto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="GGXVTYGCBH",Visible=.t., Left=325, Top=20,;
    Alignment=1, Width=102, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="DNHQCDSLXF",Visible=.t., Left=29, Top=50,;
    Alignment=1, Width=105, Height=18,;
    Caption="Carta d'identit�:"  ;
  , bGlobalFont=.t.

  add object oBox_2_15 as StdBox with uid="RZZMDCACBD",left=30, top=207, width=655,height=1
enddefine
define class tgsar_adpPag3 as StdContainer
  Width  = 779
  height = 535
  stdWidth  = 779
  stdheight = 535
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDPRESVIA_3_1 as StdField with uid="VRAQEBJMNP",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DPRESVIA", cQueryName = "DPRESVIA",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di residenza",;
    HelpContextID = 99679607,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=113, Top=35, InputMask=replicate('X',35)

  func oDPRESVIA_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  add object oDPINDIR2_3_2 as StdField with uid="RCPZMDNZTP",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DPINDIR2", cQueryName = "DPINDIR2",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi all'indirizzo di residenza",;
    HelpContextID = 133599896,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=113, Top=62, InputMask=replicate('X',35)

  func oDPINDIR2_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  add object oDPRESCAP_3_5 as StdField with uid="DYRNJXFPDN",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DPRESCAP", cQueryName = "DPRESCAP",;
    bObbl = .f. , nPag = 3, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P.",;
    HelpContextID = 219087482,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=113, Top=89, InputMask=replicate('X',9), bHasZoom = .t. 

  func oDPRESCAP_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  proc oDPRESCAP_3_5.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_DPRESCAP",".w_DPRESLOC",".w_DPRESPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDPRESLOC_3_6 as StdField with uid="WLKRVYWWOE",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DPRESLOC", cQueryName = "DPRESLOC",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit�",;
    HelpContextID = 68092551,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=207, Top=89, InputMask=replicate('X',30), bHasZoom = .t. 

  func oDPRESLOC_3_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  proc oDPRESLOC_3_6.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_DPRESCAP",".w_DPRESLOC",".w_DPRESPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDPRESPRO_3_8 as StdField with uid="RGUVSVHAOM",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DPRESPRO", cQueryName = "DPRESPRO",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 983675,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=592, Top=89, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oDPRESPRO_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  proc oDPRESPRO_3_8.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_DPRESPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDPNAZION_3_9 as StdField with uid="KBSTCDEWAB",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DPNAZION", cQueryName = "DPNAZION",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della nazione di appartenenza",;
    HelpContextID = 111362684,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=113, Top=115, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_DPNAZION"

  func oDPNAZION_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  func oDPNAZION_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPNAZION_3_9.ecpDrop(oSource)
    this.Parent.oContained.link_3_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPNAZION_3_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oDPNAZION_3_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"NAZIONI",'',this.parent.oContained
  endproc
  proc oDPNAZION_3_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_DPNAZION
     i_obj.ecpSave()
  endproc

  add object oDPDOMVIA_3_14 as StdField with uid="TTIJCYRXEH",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DPDOMVIA", cQueryName = "DPDOMVIA",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo domicilio",;
    HelpContextID = 93986167,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=113, Top=164, InputMask=replicate('X',40)

  add object oDPDOMCAP_3_17 as StdField with uid="IKLWAPSWFJ",rtseq=81,rtrep=.f.,;
    cFormVar = "w_DPDOMCAP", cQueryName = "DPDOMCAP",;
    bObbl = .f. , nPag = 3, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. domicilio",;
    HelpContextID = 224780922,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=113, Top=189, InputMask=replicate('X',9), bHasZoom = .t. 

  proc oDPDOMCAP_3_17.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_DPDOMCAP",".w_DPDOMLOC",".w_DPDOMPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDPDOMLOC_3_18 as StdField with uid="ZIQYBGEHGY",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DPDOMLOC", cQueryName = "DPDOMLOC",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� domicilio",;
    HelpContextID = 73785991,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=207, Top=189, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oDPDOMLOC_3_18.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_DPDOMCAP",".w_DPDOMLOC",".w_DPDOMPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDPDOMPRO_3_20 as StdField with uid="DJLBLRNTXC",rtseq=83,rtrep=.f.,;
    cFormVar = "w_DPDOMPRO", cQueryName = "DPDOMPRO",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di domicilio",;
    HelpContextID = 6677115,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=592, Top=189, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  proc oDPDOMPRO_3_20.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_DPDOMPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDPTELEF1_3_24 as StdField with uid="EXTHBKZCDM",rtseq=84,rtrep=.f.,;
    cFormVar = "w_DPTELEF1", cQueryName = "DPTELEF1",;
    bObbl = .f. , nPag = 3, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Primo telefono",;
    HelpContextID = 75570535,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=113, Top=240, InputMask=replicate('X',18)

  func oDPTELEF1_3_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  add object oDPTELEF2_3_25 as StdField with uid="XWAEYLAYTE",rtseq=85,rtrep=.f.,;
    cFormVar = "w_DPTELEF2", cQueryName = "DPTELEF2",;
    bObbl = .f. , nPag = 3, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Secondo telefono",;
    HelpContextID = 75570536,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=113, Top=267, InputMask=replicate('X',18)

  func oDPTELEF2_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  add object oDPINDMAI_3_27 as StdField with uid="ICUCHQEVCW",rtseq=86,rtrep=.f.,;
    cFormVar = "w_DPINDMAI", cQueryName = "DPINDMAI",;
    bObbl = .f. , nPag = 3, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 66491009,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=435, Top=267, InputMask=replicate('X',254)

  func oDPINDMAI_3_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  add object oDESNAZ_3_28 as StdField with uid="LEQXCNMWEH",rtseq=87,rtrep=.f.,;
    cFormVar = "w_DESNAZ", cQueryName = "DESNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 119930314,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=166, Top=115, InputMask=replicate('X',35)

  add object oCODISO_3_29 as StdField with uid="GLGKWGFGOA",rtseq=88,rtrep=.f.,;
    cFormVar = "w_CODISO", cQueryName = "CODISO",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice ISO nazione",;
    HelpContextID = 17556442,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=394, Top=115, InputMask=replicate('X',3)

  add object oDPNUMCEL_3_31 as StdField with uid="YQAISXKJIW",rtseq=89,rtrep=.f.,;
    cFormVar = "w_DPNUMCEL", cQueryName = "DPNUMCEL",;
    bObbl = .f. , nPag = 3, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 44088706,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=113, Top=294, InputMask=replicate('X',18)

  func oDPNUMCEL_3_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  add object oDPINDPEC_3_35 as StdField with uid="KTMGPYEMRK",rtseq=92,rtrep=.f.,;
    cFormVar = "w_DPINDPEC", cQueryName = "DPINDPEC",;
    bObbl = .f. , nPag = 3, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 252276089,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=435, Top=294, InputMask=replicate('X',254)

  func oDPINDPEC_3_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc

  func oDPINDPEC_3_35.mHide()
    with this.Parent.oContained
      return (not isahe())
    endwith
  endfunc


  add object oDPFLAVVI_3_37 as StdCombo with uid="OUGEJJHEPB",value=1,rtseq=112,rtrep=.f.,left=113,top=321,width=211,height=21;
    , ToolTipText = "Gestione avvisi";
    , HelpContextID = 81214847;
    , cFormVar="w_DPFLAVVI",RowSource=""+"Nessun tipo di avviso,"+"Invia avvisi tramite e-mail,"+"Invia avvisi tramite post-in,"+"Invia avvisi tramite e-mail e post-in", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oDPFLAVVI_3_37.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'P',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oDPFLAVVI_3_37.GetRadio()
    this.Parent.oContained.w_DPFLAVVI = this.RadioValue()
    return .t.
  endfunc

  func oDPFLAVVI_3_37.SetRadio()
    this.Parent.oContained.w_DPFLAVVI=trim(this.Parent.oContained.w_DPFLAVVI)
    this.value = ;
      iif(this.Parent.oContained.w_DPFLAVVI=='',1,;
      iif(this.Parent.oContained.w_DPFLAVVI=='M',2,;
      iif(this.Parent.oContained.w_DPFLAVVI=='P',3,;
      iif(this.Parent.oContained.w_DPFLAVVI=='E',4,;
      0))))
  endfunc

  add object oDPINDWEB_3_38 as StdField with uid="HJTNKNRFEH",rtseq=113,rtrep=.f.,;
    cFormVar = "w_DPINDWEB", cQueryName = "DPINDWEB",;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 101281144,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=435, Top=321, InputMask=replicate('X',50)

  func oDPINDWEB_3_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
   endif
  endfunc


  add object oDPVCSICS_3_42 as StdCombo with uid="NOCHVQRDNH",rtseq=133,rtrep=.f.,left=113,top=348,width=211,height=21;
    , ToolTipText = "Formato allegato di avviso";
    , HelpContextID = 118538871;
    , cFormVar="w_DPVCSICS",RowSource=""+"Da attivit�,"+"Nessun allegato,"+"Avviso con VCS,"+"Avviso con ICS,"+"Avviso con VCS e ICS", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oDPVCSICS_3_42.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'N',;
    iif(this.value =3,'V',;
    iif(this.value =4,'I',;
    iif(this.value =5,'E',;
    space(1)))))))
  endfunc
  func oDPVCSICS_3_42.GetRadio()
    this.Parent.oContained.w_DPVCSICS = this.RadioValue()
    return .t.
  endfunc

  func oDPVCSICS_3_42.SetRadio()
    this.Parent.oContained.w_DPVCSICS=trim(this.Parent.oContained.w_DPVCSICS)
    this.value = ;
      iif(this.Parent.oContained.w_DPVCSICS=='A',1,;
      iif(this.Parent.oContained.w_DPVCSICS=='N',2,;
      iif(this.Parent.oContained.w_DPVCSICS=='V',3,;
      iif(this.Parent.oContained.w_DPVCSICS=='I',4,;
      iif(this.Parent.oContained.w_DPVCSICS=='E',5,;
      0)))))
  endfunc

  func oDPVCSICS_3_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPFLAVVI='M' OR .w_DPFLAVVI='E')
    endwith
   endif
  endfunc

  func oDPVCSICS_3_42.mHide()
    with this.Parent.oContained
      return (!(.w_DPFLAVVI='M' OR .w_DPFLAVVI='E'))
    endwith
  endfunc


  add object oDPFLPROM_3_44 as StdCombo with uid="WPRPLMBBXE",value=1,rtseq=147,rtrep=.f.,left=436,top=348,width=292,height=21;
    , ToolTipText = "Gestione promemoria";
    , HelpContextID = 238600829;
    , cFormVar="w_DPFLPROM",RowSource=""+"Nessun tipo di promemoria,"+"Invia promemoria tramite e-mail,"+"Invia promemoria tramite post-in,"+"Invia promemoria tramite e-mail e post-in", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oDPFLPROM_3_44.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'P',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oDPFLPROM_3_44.GetRadio()
    this.Parent.oContained.w_DPFLPROM = this.RadioValue()
    return .t.
  endfunc

  func oDPFLPROM_3_44.SetRadio()
    this.Parent.oContained.w_DPFLPROM=trim(this.Parent.oContained.w_DPFLPROM)
    this.value = ;
      iif(this.Parent.oContained.w_DPFLPROM=='',1,;
      iif(this.Parent.oContained.w_DPFLPROM=='M',2,;
      iif(this.Parent.oContained.w_DPFLPROM=='P',3,;
      iif(this.Parent.oContained.w_DPFLPROM=='E',4,;
      0))))
  endfunc

  func oDPFLPROM_3_44.mHide()
    with this.Parent.oContained
      return (!g_JBSH=='S')
    endwith
  endfunc

  add object oStr_3_3 as StdString with uid="EONQGYTYGE",Visible=.t., Left=47, Top=35,;
    Alignment=1, Width=62, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="CDRHFGWYSP",Visible=.t., Left=10, Top=90,;
    Alignment=1, Width=99, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_3_7 as StdString with uid="LEOBLCIIEJ",Visible=.t., Left=532, Top=89,;
    Alignment=1, Width=56, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="WAEJXUVREC",Visible=.t., Left=12, Top=10,;
    Alignment=0, Width=92, Height=18,;
    Caption="Residenza"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="FBOUWOKDOQ",Visible=.t., Left=12, Top=139,;
    Alignment=0, Width=92, Height=18,;
    Caption="Domicilio"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="SPAIVBFLZI",Visible=.t., Left=47, Top=164,;
    Alignment=1, Width=62, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="TNJPSAIKLY",Visible=.t., Left=10, Top=189,;
    Alignment=1, Width=99, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="VPXWNPYFWK",Visible=.t., Left=532, Top=189,;
    Alignment=1, Width=56, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="VCKYYUDNDQ",Visible=.t., Left=9, Top=215,;
    Alignment=0, Width=92, Height=18,;
    Caption="Domicilio"  ;
  , bGlobalFont=.t.

  add object oStr_3_23 as StdString with uid="RUNEFQPAFU",Visible=.t., Left=38, Top=240,;
    Alignment=1, Width=71, Height=18,;
    Caption="Telefoni:"  ;
  , bGlobalFont=.t.

  add object oStr_3_26 as StdString with uid="CPMEHQSUCC",Visible=.t., Left=328, Top=267,;
    Alignment=1, Width=105, Height=18,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_3_30 as StdString with uid="NNCFPFBBKL",Visible=.t., Left=31, Top=115,;
    Alignment=1, Width=78, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_32 as StdString with uid="URBRNIRSDG",Visible=.t., Left=31, Top=294,;
    Alignment=1, Width=78, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_3_36 as StdString with uid="SDAFEAQULJ",Visible=.t., Left=328, Top=321,;
    Alignment=1, Width=105, Height=18,;
    Caption="Indirizzo web:"  ;
  , bGlobalFont=.t.

  add object oStr_3_39 as StdString with uid="XHMVDLXTQG",Visible=.t., Left=328, Top=348,;
    Alignment=1, Width=105, Height=18,;
    Caption="Promemoria:"  ;
  , bGlobalFont=.t.

  func oStr_3_39.mHide()
    with this.Parent.oContained
      return (!g_JBSH=='S')
    endwith
  endfunc

  add object oStr_3_40 as StdString with uid="EJCDDJUPCS",Visible=.t., Left=18, Top=321,;
    Alignment=1, Width=91, Height=18,;
    Caption="Avvisi:"  ;
  , bGlobalFont=.t.

  add object oStr_3_41 as StdString with uid="OKDDGWYHIE",Visible=.t., Left=2, Top=348,;
    Alignment=1, Width=105, Height=18,;
    Caption="Formato allegato:"  ;
  , bGlobalFont=.t.

  func oStr_3_41.mHide()
    with this.Parent.oContained
      return (!(.w_DPFLAVVI='M' OR .w_DPFLAVVI='E'))
    endwith
  endfunc

  add object oStr_3_43 as StdString with uid="TQVUBUPVKC",Visible=.t., Left=328, Top=294,;
    Alignment=1, Width=105, Height=18,;
    Caption="PEC:"  ;
  , bGlobalFont=.t.

  func oStr_3_43.mHide()
    with this.Parent.oContained
      return (not isahe())
    endwith
  endfunc

  add object oBox_3_10 as StdBox with uid="PEBWUPFWTZ",left=10, top=28, width=725,height=2

  add object oBox_3_12 as StdBox with uid="AYCLJRQBKZ",left=10, top=158, width=725,height=2

  add object oBox_3_21 as StdBox with uid="IGCKZGQMCX",left=7, top=234, width=725,height=2
enddefine
define class tgsar_adpPag4 as StdContainer
  Width  = 779
  height = 535
  stdWidth  = 779
  stdheight = 535
  resizeXpos=314
  resizeYpos=176
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDPCODUTE_4_1 as StdField with uid="ZPLHMFSRMB",rtseq=93,rtrep=.f.,;
    cFormVar = "w_DPCODUTE", cQueryName = "DPCODUTE",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 67767675,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=168, Top=14, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_DPCODUTE"

  func oDPCODUTE_4_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODUTE_4_1.ecpDrop(oSource)
    this.Parent.oContained.link_4_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODUTE_4_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oDPCODUTE_4_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDESUTE_4_2 as StdField with uid="XXRKGFCXKH",rtseq=94,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 183434698,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=228, Top=14, InputMask=replicate('X',20)


  add object oDPCHKING_4_4 as StdCombo with uid="GEXHCXHCTC",rtseq=95,rtrep=.f.,left=168,top=45,width=122,height=21;
    , ToolTipText = "Selezionare la modalit� di controllo dell'utente all'ingresso nella procedura";
    , HelpContextID = 126677635;
    , cFormVar="w_DPCHKING",RowSource=""+"Nessun controllo,"+"Blocco all'ingresso,"+"Solo avvertimento", bObbl = .t. , nPag = 4;
    , sErrorMsg = "Valore non ammesso per utente amministratore";
  , bGlobalFont=.t.


  func oDPCHKING_4_4.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'B',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oDPCHKING_4_4.GetRadio()
    this.Parent.oContained.w_DPCHKING = this.RadioValue()
    return .t.
  endfunc

  func oDPCHKING_4_4.SetRadio()
    this.Parent.oContained.w_DPCHKING=trim(this.Parent.oContained.w_DPCHKING)
    this.value = ;
      iif(this.Parent.oContained.w_DPCHKING=='N',1,;
      iif(this.Parent.oContained.w_DPCHKING=='B',2,;
      iif(this.Parent.oContained.w_DPCHKING=='A',3,;
      0)))
  endfunc

  func oDPCHKING_4_4.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DPCODUTE) OR g_AGEN<>'S')
    endwith
  endfunc

  func oDPCHKING_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(.w_DPCHKING='B', NOT IsUteAdmin(.w_DPCODUTE), .T.))
    endwith
    return bRes
  endfunc

  add object oDPDATINI_4_5 as StdField with uid="GLURKQJOJH",rtseq=96,rtrep=.f.,;
    cFormVar = "w_DPDATINI", cQueryName = "DPDATINI",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Inserire la data da cui avr� inizio il controllo dei resoconti giornalieri dell'utente",;
    HelpContextID = 117695105,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=449, Top=45

  func oDPDATINI_4_5.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DPCHKING) OR .w_DPCHKING ='N' OR EMPTY(.w_DPCODUTE) OR g_AGEN<>'S')
    endwith
  endfunc

  add object oDPGGBLOC_4_6 as StdField with uid="FWIYEGRUAC",rtseq=97,rtrep=.f.,;
    cFormVar = "w_DPGGBLOC", cQueryName = "DPGGBLOC",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore positivo o nullo.",;
    ToolTipText = "Inserire il numero di giorni oltre il quale bloccare l'utente all'ingresso nel caso in cui non abbia caricato in tali giorni il resoconto giornaliero in modo completo rispetto al proprio calendario.",;
    HelpContextID = 85832327,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=601, Top=45, cSayPict="'999'", cGetPict="'999'"

  func oDPGGBLOC_4_6.mHide()
    with this.Parent.oContained
      return (.w_DPCHKING <> 'B' OR EMPTY(.w_DPCODUTE) OR g_AGEN<>'S')
    endwith
  endfunc

  func oDPGGBLOC_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DPGGBLOC>=0)
    endwith
    return bRes
  endfunc

  add object oDP_FIRMA_4_8 as StdField with uid="PNHKRHCMGN",rtseq=98,rtrep=.f.,;
    cFormVar = "w_DP_FIRMA", cQueryName = "DP_FIRMA",;
    bObbl = .f. , nPag = 4, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Bitmap firma",;
    HelpContextID = 246231689,;
   bGlobalFont=.t.,;
    Height=21, Width=615, Left=101, Top=82, cSayPict='REPL("!",200)', cGetPict='REPL("!",200)', InputMask=replicate('X',200), bHasZoom = .t. 

  proc oDP_FIRMA_4_8.mZoom
      with this.Parent.oContained
        GSAR_BD2(this.Parent.oContained,"BUT")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_4_9 as StdButton with uid="FMFAMDCLLL",left=722, top=80, width=24,height=25,;
    caption="...", nPag=4;
    , ToolTipText = "Assegna bitmap con firma del dipendente.";
    , HelpContextID = 175741398;
  , bGlobalFont=.t.

    proc oBtn_4_9.Click()
      with this.Parent.oContained
        GSAR_BD2(this.Parent.oContained,"BUT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object FIRMAIMG as cp_showimage with uid="SQBTQTHDFN",left=102, top=113, width=301,height=157,;
    caption='',;
   bGlobalFont=.t.,;
    default="BMP\WFLFIRMA.BMP",stretch=1,;
    nPag=4;
    , HelpContextID = 175540470


  add object oObj_4_11 as cp_runprogram with uid="MTQYHUAZIO",left=7, top=561, width=291,height=19,;
    caption='GSAR_BD2(INI)',;
   bGlobalFont=.t.,;
    prg="GSAR_BD2('INI')",;
    cEvent = "Init,Load,w_DP_FIRMA Changed",;
    nPag=4;
    , HelpContextID = 174384104

  add object oDPASSDAL_4_12 as StdField with uid="RJTYARXSVS",rtseq=99,rtrep=.f.,;
    cFormVar = "w_DPASSDAL", cQueryName = "DPASSDAL",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Date di assenza incongruenti",;
    ToolTipText = "Assente dalla data",;
    HelpContextID = 201462398,;
   bGlobalFont=.t.,;
    Height=21, Width=89, Left=161, Top=282

  func oDPASSDAL_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DPASSFIN >= .w_DPASSDAL or EMPTY(.w_DPASSDAL))
    endwith
    return bRes
  endfunc

  add object oDPASSFIN_4_13 as StdField with uid="FROAYZIKCQ",rtseq=100,rtrep=.f.,;
    cFormVar = "w_DPASSFIN", cQueryName = "DPASSFIN",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Date di assenza incongruenti",;
    ToolTipText = "Assente fino alla data",;
    HelpContextID = 100527492,;
   bGlobalFont=.t.,;
    Height=21, Width=91, Left=428, Top=281

  func oDPASSFIN_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DPASSFIN >= .w_DPASSDAL)
    endwith
    return bRes
  endfunc

  add object oDPCODSOS_4_16 as StdField with uid="XOXLAFZWGI",rtseq=101,rtrep=.f.,;
    cFormVar = "w_DPCODSOS", cQueryName = "DPCODSOS",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il sostituto deve essere diverso dalla persona",;
    ToolTipText = "Codice sostituto",;
    HelpContextID = 234222199,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=161, Top=308, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_FLPERSONA", oKey_2_1="DPCODICE", oKey_2_2="this.w_DPCODSOS"

  func oDPCODSOS_4_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODSOS_4_16.ecpDrop(oSource)
    this.Parent.oContained.link_4_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODSOS_4_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_FLPERSONA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_FLPERSONA)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oDPCODSOS_4_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dipendenti",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oCOGNSOS_4_18 as StdField with uid="IJRFWOGIRA",rtseq=102,rtrep=.f.,;
    cFormVar = "w_COGNSOS", cQueryName = "COGNSOS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 17216474,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=226, Top=308, InputMask=replicate('X',40)

  add object oStr_4_3 as StdString with uid="FTACSGMXQY",Visible=.t., Left=73, Top=16,;
    Alignment=1, Width=92, Height=18,;
    Caption="Codice utente:"  ;
  , bGlobalFont=.t.

  add object oStr_4_7 as StdString with uid="UFFQUITEQM",Visible=.t., Left=31, Top=84,;
    Alignment=1, Width=68, Height=18,;
    Caption="Firma:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="YUTHXAJEWF",Visible=.t., Left=315, Top=285,;
    Alignment=1, Width=109, Height=18,;
    Caption="Assente fino al:"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="MJZHFJKLRD",Visible=.t., Left=29, Top=286,;
    Alignment=1, Width=129, Height=18,;
    Caption="Assente dal:"  ;
  , bGlobalFont=.t.

  add object oStr_4_17 as StdString with uid="MQFJBHYMON",Visible=.t., Left=37, Top=308,;
    Alignment=1, Width=121, Height=18,;
    Caption="Codice sostituto:"  ;
  , bGlobalFont=.t.

  add object oStr_4_21 as StdString with uid="YYXRUNJUJD",Visible=.t., Left=7, Top=46,;
    Alignment=1, Width=157, Height=18,;
    Caption="Controllo utente all'ingresso:"  ;
  , bGlobalFont=.t.

  func oStr_4_21.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DPCODUTE) OR g_AGEN<>'S')
    endwith
  endfunc

  add object oStr_4_22 as StdString with uid="WZBMHABCKT",Visible=.t., Left=560, Top=46,;
    Alignment=1, Width=36, Height=18,;
    Caption="Giorni:"  ;
  , bGlobalFont=.t.

  func oStr_4_22.mHide()
    with this.Parent.oContained
      return (.w_DPCHKING <> 'B' OR EMPTY(.w_DPCODUTE) OR g_AGEN<>'S')
    endwith
  endfunc

  add object oStr_4_23 as StdString with uid="RHIGDGMTWR",Visible=.t., Left=335, Top=46,;
    Alignment=1, Width=110, Height=18,;
    Caption="Data inizio controllo:"  ;
  , bGlobalFont=.t.

  func oStr_4_23.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DPCHKING) OR .w_DPCHKING ='N' OR EMPTY(.w_DPCODUTE) OR g_AGEN<>'S')
    endwith
  endfunc
enddefine
define class tgsar_adpPag5 as StdContainer
  Width  = 779
  height = 535
  stdWidth  = 779
  stdheight = 535
  resizeXpos=419
  resizeYpos=156
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDPCDUTAL_5_1 as StdField with uid="WBFCQKZWLS",rtseq=103,rtrep=.f.,;
    cFormVar = "w_DPCDUTAL", cQueryName = "DPCDUTAL",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Identifica il codice utente ad hoc abilitato al caricamento dati su una certa area budget",;
    HelpContextID = 200340094,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=189, Top=18, bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_DPCDUTAL"

  func oDPCDUTAL_5_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCDUTAL_5_1.ecpDrop(oSource)
    this.Parent.oContained.link_5_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCDUTAL_5_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oDPCDUTAL_5_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDPFLCONF_5_3 as StdCheck with uid="IHFAJOCCVU",rtseq=104,rtrep=.f.,left=261, top=16, caption="Conferma movimento",;
    ToolTipText = "Se attivo abilita conferma movimenti budget",;
    HelpContextID = 34128516,;
    cFormVar="w_DPFLCONF", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oDPFLCONF_5_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDPFLCONF_5_3.GetRadio()
    this.Parent.oContained.w_DPFLCONF = this.RadioValue()
    return .t.
  endfunc

  func oDPFLCONF_5_3.SetRadio()
    this.Parent.oContained.w_DPFLCONF=trim(this.Parent.oContained.w_DPFLCONF)
    this.value = ;
      iif(this.Parent.oContained.w_DPFLCONF=='S',1,;
      0)
  endfunc

  add object oDPCODBUN_5_4 as StdField with uid="AUWZLBYVXI",rtseq=105,rtrep=.f.,;
    cFormVar = "w_DPCODBUN", cQueryName = "DPCODBUN",;
    bObbl = .f. , nPag = 5, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Business unit di analitica",;
    ToolTipText = "Business unit di appartenenza",;
    HelpContextID = 17436036,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=478, Top=19, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSAR_KBU", oKey_1_1="BUCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="BUCODICE", oKey_2_2="this.w_DPCODBUN"

  func oDPCODBUN_5_4.mHide()
    with this.Parent.oContained
      return (g_PERBUN='N' or not IsAHE())
    endwith
  endfunc

  func oDPCODBUN_5_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODBUN_5_4.ecpDrop(oSource)
    this.Parent.oContained.link_5_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODBUN_5_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oDPCODBUN_5_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KBU',"Business Unit",'GSCA_ACM.BUSIUNIT_VZM',this.parent.oContained
  endproc
  proc oDPCODBUN_5_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KBU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_CODAZI
     i_obj.w_BUCODICE=this.parent.oContained.w_DPCODBUN
     i_obj.ecpSave()
  endproc


  add object oLinkPC_5_6 as stdDynamicChildContainer with uid="PKPKVXOHVB",left=46, top=54, width=465, height=185, bOnScreen=.t.;


  func oLinkPC_5_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not IsAHE())
     endwith
    endif
  endfunc

  add object oStr_5_2 as StdString with uid="YNLWLEKUUJ",Visible=.t., Left=9, Top=18,;
    Alignment=1, Width=175, Height=18,;
    Caption="Codice utente alternativo:"  ;
  , bGlobalFont=.t.

  add object oStr_5_5 as StdString with uid="UTBPKGLAFH",Visible=.t., Left=426, Top=21,;
    Alignment=1, Width=47, Height=18,;
    Caption="B.unit:"  ;
  , bGlobalFont=.t.

  func oStr_5_5.mHide()
    with this.Parent.oContained
      return (g_PERBUN='N' or not IsAHE())
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_mbr",lower(this.oContained.GSAR_MBR.class))=0
        this.oContained.GSAR_MBR.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsar_adpPag6 as StdContainer
  Width  = 779
  height = 535
  stdWidth  = 779
  stdheight = 535
  resizeXpos=536
  resizeYpos=191
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDP__NOTE_6_1 as StdMemo with uid="SBAAIDRILR",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DP__NOTE", cQueryName = "DP__NOTE",;
    bObbl = .f. , nPag = 6, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 21246597,;
   bGlobalFont=.t.,;
    Height=497, Width=683, Left=83, Top=9

  add object oStr_6_2 as StdString with uid="IIGPSAIDWE",Visible=.t., Left=6, Top=9,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsar_adpPag7 as StdContainer
  Width  = 779
  height = 535
  stdWidth  = 779
  stdheight = 535
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDPRSFASC_7_2 as StdCheck with uid="HXMNBZVIOQ",rtseq=137,rtrep=.f.,left=22, top=27, caption="Responsabile prestazioni da fascicolo",;
    ToolTipText = "Se attivo, sar� proposto come responsabile delle prestazioni il soggetto interno della pratica con il numero di ordinamento pi� basso, indipendentemente dal ruolo, escludendo le persone obsolete e quelle con il flag 'Partecipa alle attivit�' disattivo",;
    HelpContextID = 265355911,;
    cFormVar="w_DPRSFASC", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oDPRSFASC_7_2.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oDPRSFASC_7_2.GetRadio()
    this.Parent.oContained.w_DPRSFASC = this.RadioValue()
    return .t.
  endfunc

  func oDPRSFASC_7_2.SetRadio()
    this.Parent.oContained.w_DPRSFASC=trim(this.Parent.oContained.w_DPRSFASC)
    this.value = ;
      iif(this.Parent.oContained.w_DPRSFASC=='S',1,;
      0)
  endfunc

  func oDPRSFASC_7_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!Empty(.w_DPCODUTE) and Empty(.w_DPCODRES))
    endwith
   endif
  endfunc

  func oDPRSFASC_7_2.mHide()
    with this.Parent.oContained
      return (.w_cTipRis<>'P' OR !IsAlt())
    endwith
  endfunc

  add object oDPCODRES_7_3 as StdField with uid="RRZUOGCKXT",rtseq=138,rtrep=.f.,;
    cFormVar = "w_DPCODRES", cQueryName = "DPCODRES",;
    bObbl = .f. , nPag = 7, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice responsabile prestazioni per inserimento provvisorio",;
    HelpContextID = 17436041,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=407, Top=27, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_FLPERSONA", oKey_2_1="DPCODICE", oKey_2_2="this.w_DPCODRES"

  func oDPCODRES_7_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPRSFASC<>'S')
    endwith
   endif
  endfunc

  func oDPCODRES_7_3.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR .w_cTipRis<>'P')
    endwith
  endfunc

  func oDPCODRES_7_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODRES_7_3.ecpDrop(oSource)
    this.Parent.oContained.link_7_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODRES_7_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_FLPERSONA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_FLPERSONA)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oDPCODRES_7_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Persone",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oDPPERFAT_7_4 as StdField with uid="MRBHAQXAJL",rtseq=139,rtrep=.f.,;
    cFormVar = "w_DPPERFAT", cQueryName = "DPPERFAT",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale sui compensi per fatture a saldo",;
    HelpContextID = 169812598,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=168, Top=75, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDPPERFAT_7_4.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR .w_cTipRis<>'P')
    endwith
  endfunc

  func oDPPERFAT_7_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DPPERFAT>=0  AND .w_DPPERFAT<=100)
    endwith
    return bRes
  endfunc

  add object oDPPERFAC_7_5 as StdField with uid="TMTTCOGVOO",rtseq=140,rtrep=.f.,;
    cFormVar = "w_DPPERFAC", cQueryName = "DPPERFAC",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale sui compensi per fatture in acconto",;
    HelpContextID = 169812615,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=448, Top=75, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDPPERFAC_7_5.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR .w_cTipRis<>'P')
    endwith
  endfunc

  func oDPPERFAC_7_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DPPERFAC>=0  AND .w_DPPERFAC<=100)
    endwith
    return bRes
  endfunc


  add object oDPTIPRIG_7_7 as StdCombo with uid="AZBHLUKGXU",rtseq=141,rtrep=.f.,left=666,top=75,width=100,height=21;
    , ToolTipText = "Tipologia riga per la generazione del documento";
    , HelpContextID = 29695357;
    , cFormVar="w_DPTIPRIG",RowSource=""+"Non fatturabile,"+"Fatturabile", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oDPTIPRIG_7_7.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oDPTIPRIG_7_7.GetRadio()
    this.Parent.oContained.w_DPTIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oDPTIPRIG_7_7.SetRadio()
    this.Parent.oContained.w_DPTIPRIG=trim(this.Parent.oContained.w_DPTIPRIG)
    this.value = ;
      iif(this.Parent.oContained.w_DPTIPRIG=='N',1,;
      iif(this.Parent.oContained.w_DPTIPRIG=='D',2,;
      0))
  endfunc

  func oDPTIPRIG_7_7.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  add object oDENOMRES_7_10 as StdField with uid="VHLKZRFBDE",rtseq=142,rtrep=.f.,;
    cFormVar = "w_DENOMRES", cQueryName = "DENOMRES",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26915465,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=473, Top=27, InputMask=replicate('X',40)

  func oDENOMRES_7_10.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR .w_cTipRis<>'P')
    endwith
  endfunc

  add object oDPCTRPRE_7_12 as StdCheck with uid="LELGHYISDN",rtseq=143,rtrep=.f.,left=22, top=121, caption="Controllo su prestazioni",;
    ToolTipText = "Se attivo, impedisce il caricamento o la variazione di prestazioni da parte della persona al di fuori dell'intervallo di giorni specificato",;
    HelpContextID = 1110661,;
    cFormVar="w_DPCTRPRE", bObbl = .f. , nPag = 7;
    ,sErrorMsg = "Valore non ammesso per un utente amministratore";
   , bGlobalFont=.t.


  func oDPCTRPRE_7_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDPCTRPRE_7_12.GetRadio()
    this.Parent.oContained.w_DPCTRPRE = this.RadioValue()
    return .t.
  endfunc

  func oDPCTRPRE_7_12.SetRadio()
    this.Parent.oContained.w_DPCTRPRE=trim(this.Parent.oContained.w_DPCTRPRE)
    this.value = ;
      iif(this.Parent.oContained.w_DPCTRPRE=='S',1,;
      0)
  endfunc

  func oDPCTRPRE_7_12.mHide()
    with this.Parent.oContained
      return (.w_PAFLPRES<>'S')
    endwith
  endfunc

  func oDPCTRPRE_7_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DPCTRPRE='S' AND NOT Readgroup(.w_DPCODUTE)) OR .w_DPCTRPRE='N')
    endwith
    return bRes
  endfunc

  add object oDPGG_PRE_7_14 as StdField with uid="XGHBNHPOAS",rtseq=144,rtrep=.f.,;
    cFormVar = "w_DPGG_PRE", cQueryName = "DPGG_PRE",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Definire il n. di giorni di calendario precedenti alla data di sistema entro i quali non viene applicato il blocco",;
    HelpContextID = 256750213,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=483, Top=122, cSayPict='"9999"', cGetPict='"9999"'

  func oDPGG_PRE_7_14.mHide()
    with this.Parent.oContained
      return (.w_PAFLPRES<>'S')
    endwith
  endfunc

  add object oDPGG_SUC_7_16 as StdField with uid="CPSDZMSKGN",rtseq=145,rtrep=.f.,;
    cFormVar = "w_DPGG_SUC", cQueryName = "DPGG_SUC",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Definire il n. di giorni di calendario successivi alla data di sistema entro i quali non viene applicato il blocco",;
    HelpContextID = 62016889,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=483, Top=146, cSayPict='"9999"', cGetPict='"9999"'

  func oDPGG_SUC_7_16.mHide()
    with this.Parent.oContained
      return (.w_PAFLPRES<>'S')
    endwith
  endfunc

  add object oStr_7_6 as StdString with uid="CSBCUTLCNV",Visible=.t., Left=-6, Top=77,;
    Alignment=1, Width=170, Height=18,;
    Caption="% sui comp. per fatt. a saldo:"  ;
  , bGlobalFont=.t.

  func oStr_7_6.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR .w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_7_8 as StdString with uid="ROPPIDVPIJ",Visible=.t., Left=260, Top=77,;
    Alignment=1, Width=183, Height=18,;
    Caption="% sui comp. per fatt. in acconto:"  ;
  , bGlobalFont=.t.

  func oStr_7_8.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR .w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_7_9 as StdString with uid="JDPUIKGWNR",Visible=.t., Left=257, Top=29,;
    Alignment=1, Width=146, Height=18,;
    Caption="Responsabile prestazioni:"  ;
  , bGlobalFont=.t.

  func oStr_7_9.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR .w_cTipRis<>'P')
    endwith
  endfunc

  add object oStr_7_11 as StdString with uid="HAVUQAKWDJ",Visible=.t., Left=560, Top=77,;
    Alignment=1, Width=100, Height=18,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  func oStr_7_11.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  add object oStr_7_13 as StdString with uid="RYIUZXTXVF",Visible=.t., Left=193, Top=124,;
    Alignment=1, Width=282, Height=18,;
    Caption="Giorni di calendario precedenti alla data di sistema:"  ;
  , bGlobalFont=.t.

  func oStr_7_13.mHide()
    with this.Parent.oContained
      return (.w_PAFLPRES<>'S')
    endwith
  endfunc

  add object oStr_7_15 as StdString with uid="ZSIZBMQJLI",Visible=.t., Left=193, Top=148,;
    Alignment=1, Width=282, Height=18,;
    Caption="Giorni di calendario successivi alla data di sistema:"  ;
  , bGlobalFont=.t.

  func oStr_7_15.mHide()
    with this.Parent.oContained
      return (.w_PAFLPRES<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_adp','DIPENDEN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DPCODICE=DIPENDEN.DPCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_adp
func  ZoomCF
param maschera
*la variabile conta serve per disattivaer il ricalcolo della data di nascita 
maschera.w_conta = maschera.w_conta + 1 
return GSAR_BFC( maschera , 'w_DPCODFIS'  , maschera.w_DPCOGNOM,maschera.w_DPNOME,maschera.w_DPNASLOC,maschera.w_DPNASPRO,maschera.w_DPDATNAS,maschera.w_DP_SESSO, , ,maschera.w_CODCOM )
endfunc

*--- Lancia stampa
*--- Sono costretto ad utilizzare una funzione perch� non funziona la User Program
proc PrintConto(pParent, cType)
   GSAR_SDP(cType)
endproc

* --- Fine Area Manuale
