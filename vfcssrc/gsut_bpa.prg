* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bpa                                                        *
*              Autenticazione Windows                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-12-22                                                      *
* Last revis.: 2013-05-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- gsut_bpa
#DEFINE FILTER_NORMAL_ACCOUNT 2
#DEFINE NERR_Success  0
#DEFINE ERROR_MORE_DATA  234
#DEFINE USERINFO_0_SIZE  4
#DEFINE USERINFO_1_SIZE  32
#DEFINE USERINFO_20_SIZE  20
#DEFINE MAX_PREFERRED_LENGTH -1
#DEFINE UF_ACCOUNTDISABLE	0x2
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bpa",oParentObject,m.pOPER)
return(i_retval)

define class tgsut_bpa as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_PADRE = .NULL.
  w_CODUTE = 0
  w_DOMINIO = space(50)
  w_SERVERNAME = space(250)
  w_lnEntry = 0
  w_lnBufLen = 0
  w_lcUserInfo = space(254)
  w_ACCDISABLE = space(1)
  w_LOGIN = space(254)
  w_DESCRI = space(254)
  w_ACCOUNT = space(254)
  w_CHECKACCOUNT = .f.
  * --- WorkFile variables
  cpusers_idx=0
  TMP_ACCOUNT_idx=0
  POL_ACC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica utenti nel dettaglio account utente delle politiche di sicurezza,
    *     se gi� presenti nel dettaglio non aggiunge
    * --- Dichiarazione API
    This.DeclareAPI()
    do case
      case this.pOPER = "L"
        this.w_PADRE = This.oParentObject.GSUT_MPS
        this.w_PADRE.MarkPos()     
        * --- Select from cpusers
        i_nConn=i_TableProp[this.cpusers_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2],.t.,this.cpusers_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select code, name  from "+i_cTable+" cpusers ";
              +" order by code";
               ,"_Curs_cpusers")
        else
          select code, name from (i_cTable);
           order by code;
            into cursor _Curs_cpusers
        endif
        if used('_Curs_cpusers')
          select _Curs_cpusers
          locate for 1=1
          do while not(eof())
          this.w_CODUTE = _Curs_cpusers.code
          this.w_PADRE.Exec_Select("_TMP_POL_ACC_", "Count(*) as CONTA", "t_PACODUTE = " + cp_ToStrODBC(this.w_CODUTE), "","","")     
          if NVL(_TMP_POL_ACC_.CONTA, 0) = 0
            * --- Codice utente non presente, lo aggiungo
            this.w_PADRE.AddRow()     
            this.w_PADRE.w_PACODUTE = this.w_CODUTE
            this.w_PADRE.w_UTDESCRI = _Curs_cpusers.name
            this.w_PADRE.w_PA_LOGIN = this.w_PADRE.w_EMPTYACC
            this.w_PADRE.SaveRow()     
          endif
          * --- Chiudo il cursore
          USE IN SELECT("_TMP_POL_ACC_")
            select _Curs_cpusers
            continue
          enddo
          use
        endif
        * --- Sbianco il campo login
        UPDATE (this.w_PADRE.cTrsName) SET t_PA_LOGIN = "" WHERE t_PA_LOGIN = this.w_PADRE.w_EMPTYACC
        this.w_PADRE.RePos()     
      case this.pOPER = "D"
        * --- Drop temporary table TMP_ACCOUNT
        i_nIdx=cp_GetTableDefIdx('TMP_ACCOUNT')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_ACCOUNT')
        endif
      case this.pOPER = "Z"
        * --- Mostra account utente macchina e dominio
        * --- Create temporary table TMP_ACCOUNT
        i_nIdx=cp_AddTableDef('TMP_ACCOUNT') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMP_ACCOUNT_proto';
              )
        this.TMP_ACCOUNT_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.w_DOMINIO = Alltrim(This.oParentObject.w_DOMINIO)
        this.w_SERVERNAME = StrConv(this.w_DOMINIO+Chr(0), 5)
         
 LOCAL bufptr, prefmaxlen, entriesread , totalentries , resume_handle, lcBuffer 
 STORE 0 TO bufptr, prefmaxlen, entriesread , totalentries , resume_handle 
        ah_Msg("Recupero account in corso")
        if NetUserEnum(this.w_SERVERNAME, 20, FILTER_NORMAL_ACCOUNT, @bufptr, MAX_PREFERRED_LENGTH, @entriesread, @totalentries, @resume_handle) = NERR_Success
          this.w_lnBufLen = entriesread * USERINFO_20_SIZE
           
 lcBuffer = Repli(Chr(0), this.w_lnBufLen) 
 = Heap2String(@lcBuffer, bufptr, this.w_lnBufLen)
          * --- Scannig returned entries - UserInfo structures
          this.w_lnEntry = 1
          do while this.w_lnEntry <= entriesread
            * --- Copying the UserInfo structure to a VFP string
            this.w_lcUserInfo = SUBSTR(lcBuffer, (this.w_lnEntry-1)*USERINFO_20_SIZE+1, USERINFO_20_SIZE)
            this.w_LOGIN = This.GetStrFromMem( This.buf2dword(SUBSTR(this.w_lcUserInfo, 1,4)))
            this.w_DESCRI = This.GetStrFromMem( This.buf2dword(SUBSTR(this.w_lcUserInfo, 5,4)))
            this.w_ACCDISABLE = IIF(BITAND(This.buf2dword(SUBSTR(this.w_lcUserInfo, 13,4)), UF_ACCOUNTDISABLE) = UF_ACCOUNTDISABLE, "S", "N")
            * --- Insert into TMP_ACCOUNT
            i_nConn=i_TableProp[this.TMP_ACCOUNT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACCOUNT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_ACCOUNT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PA_LOGIN"+",PADESCRI"+",PA__FLAG"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_LOGIN),'TMP_ACCOUNT','PA_LOGIN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'TMP_ACCOUNT','PADESCRI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ACCDISABLE),'TMP_ACCOUNT','PA__FLAG');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PA_LOGIN',this.w_LOGIN,'PADESCRI',this.w_DESCRI,'PA__FLAG',this.w_ACCDISABLE)
              insert into (i_cTable) (PA_LOGIN,PADESCRI,PA__FLAG &i_ccchkf. );
                 values (;
                   this.w_LOGIN;
                   ,this.w_DESCRI;
                   ,this.w_ACCDISABLE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            this.w_lnEntry = this.w_lnEntry + 1
          enddo
          = NetApiBufferFree(bufptr)
        endif
      case this.pOPER = "A"
        this.w_CHECKACCOUNT = .T.
        if !EMPTY(this.oParentObject.w_PA_LOGIN) And this.oParentObject.w_PA_LOGIN<> this.oParentObject.w_EMPTYACC
          this.w_DOMINIO = JUSTFNAME(JUSTPATH(this.oParentObject.w_PA_LOGIN))
          if EMPTY(this.w_DOMINIO)
            * --- Dominio non presente
            this.w_DOMINIO = Alltrim(This.oParentObject.oParentObject.w_DOMINIO)
          endif
          this.w_ACCOUNT = JUSTFNAME(this.oParentObject.w_PA_LOGIN)
          this.oParentObject.w_PA_LOGIN = this.w_DOMINIO + "\" + this.w_ACCOUNT
          this.w_PADRE = This.oParentObject
          this.w_PADRE.Exec_Select("_TMP_POL_ACC_", "t_PACODUTE as CODUTE", "UPPER(t_PA_LOGIN) == " + UPPER(cp_ToStrODBC(this.oParentObject.w_PA_LOGIN)), "","","")     
          if NVL(_TMP_POL_ACC_.CODUTE, 0) <> 0 And NVL(_TMP_POL_ACC_.CODUTE, 0) <> this.w_PADRE.w_PACODUTE
            ah_ErrorMsg("Account gi� associato al codice utente %1", "!","", _TMP_POL_ACC_.CODUTE)
            this.w_CHECKACCOUNT = .F.
            this.oParentObject.w_PA_LOGIN = ""
          endif
          * --- Chiudo il cursore
          USE IN SELECT("_TMP_POL_ACC_")
          if this.w_CHECKACCOUNT
            LOCAL hSid, nSidsize, cDomain, nDomainsize, peUse
             
 nSidsize=128 
 hSid=LocalAlloc(0, nSidsize)
             
 nDomainsize=250 
 cDomain=REPLICATE(CHR(0), nDomainsize)
             
 peUse=0
            * --- Retrieve security identifier (SID) for the account name
             
 = LookupAccountName(NULL, this.oParentObject.w_PA_LOGIN, hSid, @nSidSize, @cDomain, @nDomainSize, @peUse)
            * --- Convert the SID to string format
             
 LOCAL nBuffer, nBufsize, cSid 
 nBuffer=0 
 = ConvertSidToStringSid(hSid, @nBuffer) 
 nBufsize = LocalSize(nBuffer)
             
 cSid = REPLICATE(CHR(0), nBufsize) 
 = Heap2String(@cSid, nBuffer, nBufsize)
            if EMPTY(cSid)
              Ah_ErrorMsg("Controllo account fallito%0L'account potrebbe essere inesistente")
            endif
             
 = LocalFree(nBuffer) 
 = LocalFree(hSid)
          endif
        endif
      case this.pOPER = "W"
        * --- Da GS___UTE
        if this.oParentObject.w_NOCODUTE = "W"
          this.oParentObject.w_NOMEUTE = GetUserAccount()
          * --- Leggo il codice utente associato all'account
          this.oParentObject.w_UTE = 0
          * --- Read from POL_ACC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.POL_ACC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.POL_ACC_idx,2],.t.,this.POL_ACC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PACODUTE"+;
              " from "+i_cTable+" POL_ACC where ";
                  +"PASERIAL = "+cp_ToStrODBC("0000000001");
                  +" and PA_LOGIN = "+cp_ToStrODBC(this.oParentObject.w_NOMEUTE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PACODUTE;
              from (i_cTable) where;
                  PASERIAL = "0000000001";
                  and PA_LOGIN = this.oParentObject.w_NOMEUTE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_UTE = NVL(cp_ToDate(_read_.PACODUTE),cp_NullValue(_read_.PACODUTE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_UTE = 0
            * --- Account utente non associato a nessun codice utente
            ah_ErrorMsg("Impossibile entrare nella procedura%0L'account %1 non � associato a nessun codice utente", "stop", "", this.oParentObject.w_NOMEUTE)
          endif
        endif
        this.bUpdateParentObject=.f.
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='*TMP_ACCOUNT'
    this.cWorkTables[3]='POL_ACC'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_cpusers')
      use in _Curs_cpusers
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsut_bpa
  *--- Converting allocated in memory Unicode string to a VFP string
  Func GetStrFromMem(lnMemBlock)
     #DEFINE StrBufferLength 250
     LOCAL lcBuffer
     lcBuffer = SPACE(StrBufferLength)
     = Heap2String(@lcBuffer, lnMemBlock, StrBufferLength)
     lcBuffer = SUBSTR(lcBuffer, 1, AT(Chr(0)+Chr(0),lcBuffer)-1)
  Return STRTRAN(lcBuffer, Chr(0),"")
  
  Function buf2dword(lcBuffer)
  Return Asc(SUBSTR(lcBuffer, 1,1)) + ;
         Asc(SUBSTR(lcBuffer, 2,1)) * 256 +;
         Asc(SUBSTR(lcBuffer, 3,1)) * 65536 +;
         Asc(SUBSTR(lcBuffer, 4,1)) * 16777216
         
  Proc DeclareAPI()
    DECLARE INTEGER NetUserEnum IN netapi32;
       STRING   servername,;
       INTEGER   level,;
       INTEGER   filter,;
       INTEGER @ bufptr,;
       INTEGER   prefmaxlen,;
       INTEGER @ entriesread,;
       INTEGER @ totalentries,;
       INTEGER @ resume_handle
       
    DECLARE RtlMoveMemory IN kernel32 As Heap2String;
       STRING @ Destination, INTEGER Source, INTEGER nLength
       
    DECLARE INTEGER NetApiBufferFree IN netapi32 INTEGER Buffer
      
    DECLARE INTEGER ConvertSidToStringSid IN advapi32;
       INTEGER Sid, INTEGER @StringSid
       
    DECLARE INTEGER LocalFree IN kernel32 INTEGER hMem
    DECLARE LONG LocalSize IN kernel32 INTEGER hMem     
    DECLARE INTEGER LocalAlloc IN kernel32;
       INTEGER uFlags, INTEGER uBytes
    
    DECLARE INTEGER LookupAccountName IN advapi32;
       STRING lpSystemName, STRING lpAccountName,;
       INTEGER Sid, INTEGER @cbSid,;
       STRING @RefDomainName, INTEGER @cchRefDomainName,;
       INTEGER @peUse  
  EndProc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
