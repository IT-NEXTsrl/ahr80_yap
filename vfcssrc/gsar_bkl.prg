* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bkl                                                        *
*              Aggiornamento listini kit                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_463]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-18                                                      *
* Last revis.: 2010-01-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bkl",oParentObject,m.pPARAM)
return(i_retval)

define class tgsar_bkl as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_MESS = space(10)
  w_CODART = space(20)
  w_NUMROW = 0
  w_NUMSCA = 0
  w_QUANTI = 0
  w_PREZZO = 0
  w_ARTCOM = space(20)
  w_QTASCA = 0
  w_PERSCO = 0
  w_VALSCO = 0
  w_PERSCR = 0
  w_VALSCR = 0
  w_PREZZOFI = 0
  w_QTAUM1 = 0
  w_KIDATINI = ctod("  /  /  ")
  w_KIDATFIN = ctod("  /  /  ")
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_TROV = .f.
  w_CODICE = space(20)
  w_UMLIS = space(3)
  * --- WorkFile variables
  LIS_SCAG_idx=0
  LIS_TINI_idx=0
  RIC_PREZ_idx=0
  DISMBASE_idx=0
  DISTBASE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalla maschera Aggiornamento Listini KIT (GSPS_KLK)
    * --- Controllo campi obbligatori
    this.w_TROV = .F.
    if this.pPARAM="B"
      if NOT this.oParentObject.CheckForm()
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Lettura Articoli Selezionati
    VQ_EXEC("QUERY\GSAR_BLK.VQR",this,"LISTDATA")
    SELECT LISTDATA
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODART, ""))
    this.w_CODART = CODART
    this.w_CODICE = CODICE
    this.w_UMLIS = NVL(UMLIS,SPACE(3))
    * --- vado ad aggiornare gli Articoli del listino prezzi di arrivo
    this.w_MESS = ALLTR(this.w_CODART)
    ah_Msg("Aggiorna listini articolo: %1",.T.,.F.,.F., this.w_MESS )
    this.w_NUMROW = 0
    this.w_NUMSCA = 0
    * --- Read from LIS_TINI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LIS_TINI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CPROWNUM"+;
        " from "+i_cTable+" LIS_TINI where ";
            +"LICODART = "+cp_ToStrODBC(this.w_CODART);
            +" and LICODLIS = "+cp_ToStrODBC(this.oParentObject.w_LISTINO);
            +" and LIDATATT = "+cp_ToStrODBC(this.oParentObject.w_DATLISIN);
            +" and LIDATDIS = "+cp_ToStrODBC(this.oParentObject.w_DATLISFI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CPROWNUM;
        from (i_cTable) where;
            LICODART = this.w_CODART;
            and LICODLIS = this.oParentObject.w_LISTINO;
            and LIDATATT = this.oParentObject.w_DATLISIN;
            and LIDATDIS = this.oParentObject.w_DATLISFI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NUMROW = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_NUMROW=0
      * --- Il Listino non Esiste Se Previsto, Creo il nuovo Listino (vuoto)
      if this.oParentObject.w_CREALIS<>"S"
        this.w_NUMROW = 0
        * --- Select from LIS_TINI
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" LIS_TINI ";
              +" where LICODART="+cp_ToStrODBC(this.w_CODART)+"";
               ,"_Curs_LIS_TINI")
        else
          select * from (i_cTable);
           where LICODART=this.w_CODART;
            into cursor _Curs_LIS_TINI
        endif
        if used('_Curs_LIS_TINI')
          select _Curs_LIS_TINI
          locate for 1=1
          do while not(eof())
          this.w_NUMROW = MAX(NVL(_Curs_LIS_TINI.CPROWNUM,0), this.w_NUMROW)
            select _Curs_LIS_TINI
            continue
          enddo
          use
        endif
        this.w_NUMROW = this.w_NUMROW + 1
        * --- Try
        local bErr_03F2B990
        bErr_03F2B990=bTrsErr
        this.Try_03F2B990()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          Ah_ErrorMsg("Errore inserimento listino%0%1",,"", MESSAGE() )
        endif
        bTrsErr=bTrsErr or bErr_03F2B990
        * --- End
        * --- Try
        local bErr_03ED9050
        bErr_03ED9050=bTrsErr
        this.Try_03ED9050()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Errore inserimento scaglioni listino%0%1",,"",MESSAGE() )
        endif
        bTrsErr=bTrsErr or bErr_03ED9050
        * --- End
        this.w_NUMSCA = 1
      endif
    else
      * --- Select from LIS_SCAG
      i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2],.t.,this.LIS_SCAG_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" LIS_SCAG ";
            +" where LICODART="+cp_ToStrODBC(this.w_CODART)+" AND LIROWNUM="+cp_ToStrODBC(this.w_NUMROW)+"";
             ,"_Curs_LIS_SCAG")
      else
        select * from (i_cTable);
         where LICODART=this.w_CODART AND LIROWNUM=this.w_NUMROW;
          into cursor _Curs_LIS_SCAG
      endif
      if used('_Curs_LIS_SCAG')
        select _Curs_LIS_SCAG
        locate for 1=1
        do while not(eof())
        * --- Verifica Numero Scaglioni esistenti
        this.w_NUMSCA = this.w_NUMSCA + 1
          select _Curs_LIS_SCAG
          continue
        enddo
        use
      endif
    endif
    if this.w_NUMROW<>0
      * --- Cicla sugli Scaglioni
      * --- Lettura Componenti KIT
      this.w_PERSCO = 0
      this.w_VALSCO = 0
      * --- Read from DISMBASE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DISMBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2],.t.,this.DISMBASE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DBPERSCO,DBVALSCO"+;
          " from "+i_cTable+" DISMBASE where ";
              +"DBCODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DBPERSCO,DBVALSCO;
          from (i_cTable) where;
              DBCODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERSCO = NVL(cp_ToDate(_read_.DBPERSCO),cp_NullValue(_read_.DBPERSCO))
        this.w_VALSCO = NVL(cp_ToDate(_read_.DBVALSCO),cp_NullValue(_read_.DBVALSCO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Lettura Articoli Dai Componenti
      VQ_EXEC("QUERY\GSAR1BLK.VQR",this,"LISTCOMP")
      * --- Cicla sugli Scaglioni del KIT
      * --- Select from LIS_SCAG
      i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2],.t.,this.LIS_SCAG_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select LIQUANTI  from "+i_cTable+" LIS_SCAG ";
            +" where LICODART="+cp_ToStrODBC(this.w_CODART)+" AND LIROWNUM="+cp_ToStrODBC(this.w_NUMROW)+"";
             ,"_Curs_LIS_SCAG")
      else
        select LIQUANTI from (i_cTable);
         where LICODART=this.w_CODART AND LIROWNUM=this.w_NUMROW;
          into cursor _Curs_LIS_SCAG
      endif
      if used('_Curs_LIS_SCAG')
        select _Curs_LIS_SCAG
        locate for 1=1
        do while not(eof())
        this.w_QUANTI = NVL(_Curs_LIS_SCAG.LIQUANTI, 0)
        this.w_PREZZOFI = 0
        * --- Cicla sui Component del KIT
        * --- Select from DISTBASE
        i_nConn=i_TableProp[this.DISTBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DISTBASE ";
              +" where DBCODICE="+cp_ToStrODBC(this.w_CODICE)+"";
               ,"_Curs_DISTBASE")
        else
          select * from (i_cTable);
           where DBCODICE=this.w_CODICE;
            into cursor _Curs_DISTBASE
        endif
        if used('_Curs_DISTBASE')
          select _Curs_DISTBASE
          locate for 1=1
          do while not(eof())
          if NVL(_Curs_DISTBASE.DBFLOMAG, "X")<>"O"
            * --- Se no omaggio
            this.w_ARTCOM = NVL(_Curs_DISTBASE.DBARTCOM, "")
            this.w_QTAUM1 = NVL(_Curs_DISTBASE.DBCOEUM1, 0)
            this.w_VALSCR = NVL(_Curs_DISTBASE.DBVALSCR, 0)
            this.w_KIDATINI = CP_TODATE(_Curs_DISTBASE.DBINIVAL)
            this.w_KIDATFIN = CP_TODATE(_Curs_DISTBASE.DBFINVAL)
            if (this.w_KIDATINI<=this.oParentObject.w_DATVAL OR EMPTY(this.w_KIDATINI)) AND (this.w_KIDATFIN>=this.oParentObject.w_DATVAL OR EMPTY(this.w_KIDATFIN))
              * --- Qta da ricercare sullo scaglione
              if this.w_QUANTI=0
                if this.w_NUMSCA=1
                  this.w_QTASCA = this.w_QTAUM1
                else
                  this.w_QTASCA = 9999999999
                endif
              else
                this.w_QTASCA = this.w_QTAUM1 * this.w_QUANTI
              endif
              if NOT EMPTY(this.w_ARTCOM) AND this.w_QTASCA<>0
                * --- Lettura dei Dati Associati al Listino Componenti
                this.w_PREZZO = 0
                this.w_LISCONT1 = 0
                this.w_LISCONT2 = 0
                this.w_LISCONT3 = 0
                this.w_LISCONT4 = 0
                SELECT LISTCOMP
                GO TOP
                LOCATE FOR LICODART=this.w_ARTCOM AND LIQUANTI>=this.w_QTASCA
                if FOUND()
                  this.w_PREZZO = this.w_QTAUM1 * NVL(LIPREZZO, 0)
                  this.w_PERSCR = NVL(KIPERSCR, 0)
                  this.w_VALSCR = NVL(KIVALSCR, 0)
                  this.w_LISCONT1 = NVL(LISCONT1, 0)
                  this.w_LISCONT2 = NVL(LISCONT2, 0)
                  this.w_LISCONT3 = NVL(LISCONT3, 0)
                  this.w_LISCONT4 = NVL(LISCONT4, 0)
                  if this.oParentObject.w_FLSCON="S"
                    this.w_PREZZO = cp_ROUND(this.w_PREZZO * (1+this.w_LISCONT1/100) * (1+this.w_LISCONT2/100) * (1+this.w_LISCONT3/100) * (1+this.w_LISCONT4/100),6)
                  endif
                  this.w_PREZZO = cp_ROUND(this.w_PREZZO * (1+this.w_PERSCR/100),6)
                endif
                this.w_PREZZOFI = this.w_PREZZOFI + (this.w_PREZZO + this.w_VALSCR)
              endif
            endif
          endif
            select _Curs_DISTBASE
            continue
          enddo
          use
        endif
        if this.w_PREZZOFI<>0
          this.w_PREZZOFI = cp_ROUND(this.w_PREZZOFI * (1+this.w_PERSCO/100),6) + this.w_VALSCO
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TROV = .T.
        * --- Write into LIS_SCAG
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LIPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
              +i_ccchkf ;
          +" where ";
              +"LICODART = "+cp_ToStrODBC(this.w_CODART);
              +" and LIROWNUM = "+cp_ToStrODBC(this.w_NUMROW);
              +" and LIQUANTI = "+cp_ToStrODBC(this.w_QUANTI);
                 )
        else
          update (i_cTable) set;
              LIPREZZO = this.w_PREZZOFI;
              &i_ccchkf. ;
           where;
              LICODART = this.w_CODART;
              and LIROWNUM = this.w_NUMROW;
              and LIQUANTI = this.w_QUANTI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_LIS_SCAG
          continue
        enddo
        use
      endif
    endif
    SELECT LISTDATA
    ENDSCAN 
    if this.w_TROV=.T.
      * --- Aggiorno i prezzi dei listini
      ah_ErrorMsg("Aggiornamento listini kit completato",,"")
    else
      ah_ErrorMsg("Per le selezioni impostate non esistono listini kit da aggiornare",,"")
    endif
    * --- Chiusura cursori
    if used("LISTDATA")
      select LISTDATA
      use
    endif
    if used("LISTCOMP")
      select LISTCOMP
      use
    endif
  endproc
  proc Try_03F2B990()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into LIS_TINI
    i_nConn=i_TableProp[this.LIS_TINI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LICODART"+",CPROWNUM"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+",LIUNIMIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_TINI','LICODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMROW),'LIS_TINI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LISTINO),'LIS_TINI','LICODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISIN),'LIS_TINI','LIDATATT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISFI),'LIS_TINI','LIDATDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UMLIS),'LIS_TINI','LIUNIMIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'CPROWNUM',this.w_NUMROW,'LICODLIS',this.oParentObject.w_LISTINO,'LIDATATT',this.oParentObject.w_DATLISIN,'LIDATDIS',this.oParentObject.w_DATLISFI,'LIUNIMIS',this.w_UMLIS)
      insert into (i_cTable) (LICODART,CPROWNUM,LICODLIS,LIDATATT,LIDATDIS,LIUNIMIS &i_ccchkf. );
         values (;
           this.w_CODART;
           ,this.w_NUMROW;
           ,this.oParentObject.w_LISTINO;
           ,this.oParentObject.w_DATLISIN;
           ,this.oParentObject.w_DATLISFI;
           ,this.w_UMLIS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03ED9050()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into LIS_SCAG
    i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMROW),'LIS_SCAG','LIROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LIQUANTI');
      +","+cp_NullLink(cp_ToStrODBC(1),'LIS_SCAG','LIPREZZO');
      +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LISCONT1');
      +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LISCONT2');
      +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LISCONT3');
      +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LISCONT4');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_NUMROW,'LIQUANTI',0,'LIPREZZO',1,'LISCONT1',0,'LISCONT2',0,'LISCONT3',0,'LISCONT4',0)
      insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
         values (;
           this.w_CODART;
           ,this.w_NUMROW;
           ,0;
           ,1;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- calcola Prezzo Finale
    * --- Controllo se sono stati inseriti degli arrotondamenti
    if this.oParentObject.w_ARROT1<>0
      * --- Controllo se il prezzo finale � minore del primo arrotondamento
      if this.w_PREZZOFI<=this.oParentObject.w_VALORIN
        * --- Applico l'arrotondamento selezionato.
        this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI/this.oParentObject.w_ARROT1),0)*this.oParentObject.w_ARROT1
      else
        * --- Controllo se il prezzo finale � minore del secondo arrotondamento
        if this.w_PREZZOFI<=this.oParentObject.w_VALOR2IN
          if this.oParentObject.w_ARROT2<>0
            * --- Applico l'arrotondamento selezionato.
            this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI/this.oParentObject.w_ARROT2),0)*this.oParentObject.w_ARROT2
          endif
        else
          * --- Controllo se il prezzo finale � minore del terzo arrotondamento
          if this.w_PREZZOFI<=this.oParentObject.w_VALOR3IN
            if this.oParentObject.w_ARROT3<>0
              * --- Applico l'arrotondamento selezionato.
              this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI/this.oParentObject.w_ARROT3),0)*this.oParentObject.w_ARROT3
            endif
          else
            * --- Controllo se il prezzo finale � minore del quarto arrotondamento
            if this.oParentObject.w_ARROT4<>0
              * --- Applico l'ultimo arrotondamento selezionato.
              this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI/this.oParentObject.w_ARROT4),0)*this.oParentObject.w_ARROT4
            else
              * --- Il prezzo � maggiore dei vari valori degli arrotondamenti quindi non applico nessun arrotondamento.
              this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI), g_PERPUL)
            endif
          endif
        endif
      endif
    else
      * --- Non ho applicato nessun arrotondamento.Il prezzo finale viene calcolato in base alla valuta del listino finale
      this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI), g_PERPUL)
    endif
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='LIS_SCAG'
    this.cWorkTables[2]='LIS_TINI'
    this.cWorkTables[3]='RIC_PREZ'
    this.cWorkTables[4]='DISMBASE'
    this.cWorkTables[5]='DISTBASE'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_LIS_TINI')
      use in _Curs_LIS_TINI
    endif
    if used('_Curs_LIS_SCAG')
      use in _Curs_LIS_SCAG
    endif
    if used('_Curs_LIS_SCAG')
      use in _Curs_LIS_SCAG
    endif
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
