* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bac                                                        *
*              Ricalcola rate e acconto documento                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_218]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2000-06-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bac",oParentObject)
return(i_retval)

define class tgsve_bac as StdBatch
  * --- Local variables
  RA = space(10)
  w_TOTIMPOS = 0
  w_TOTIMPON = 0
  w_MVSERIAL = space(10)
  w_MVAFLOM1 = space(1)
  w_MVAFLOM2 = space(1)
  w_MVAFLOM3 = space(1)
  w_MVAFLOM4 = space(1)
  w_MVAFLOM5 = space(1)
  w_MVAFLOM6 = space(1)
  w_MVAIMPN1 = 0
  w_MVAIMPS1 = 0
  w_MVAIMPN2 = 0
  w_MVAIMPS2 = 0
  w_MVAIMPN3 = 0
  w_MVAIMPS3 = 0
  w_MVAIMPN4 = 0
  w_MVAIMPS4 = 0
  w_MVAIMPN5 = 0
  w_MVAIMPS5 = 0
  w_MVAIMPN6 = 0
  w_MVAIMPS6 = 0
  w_MVSPEBOL = 0
  w_MVFLSCOR = space(1)
  w_MVFLRINC = space(1)
  w_MVFLRIMB = space(1)
  w_MVFLRTRA = space(1)
  w_MVSPEINC = 0
  w_MVSPEIMB = 0
  w_MVSPETRA = 0
  w_MVACCONT = 0
  w_MVACCPRE = 0
  w_MVTOTRIT = 0
  w_MVTOTENA = 0
  w_MVRITPRE = 0
  w_MVCODPAG = space(5)
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVCODVAL = space(3)
  w_MVACCOLD = 0
  w_APPO = 0
  w_APPO1 = 0
  w_APPO2 = 0
  w_ESCL1 = space(4)
  w_ESCL2 = space(4)
  w_NURATE = 0
  w_MESE1 = 0
  w_MESE2 = 0
  w_GIORN1 = 0
  w_GIORN2 = 0
  w_GIOFIS = 0
  w_DECTOT = 0
  w_RSNUMRAT = 0
  w_RSDATRAT = ctod("  /  /  ")
  w_RSIMPRAT = 0
  w_RSMODPAG = space(10)
  w_RSFLPROV = space(1)
  w_MVCODORN = space(15)
  w_XCONORN = space(15)
  * --- WorkFile variables
  CONTI_idx=0
  DOC_MAST_idx=0
  VALUTE_idx=0
  DOC_RATE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcola Le Rate e Storna gli Acconti Precedenti nei Documenti in cancellazione primanota (da GSCG_BCK)
    this.w_MVSERIAL = this.oParentObject.oParentObject.w_PNRIFDOC
    DIMENSION RA[51, 6]
    * --- Legge Dati Documento
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            MVSERIAL = this.w_MVSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVAIMPN1 = NVL(cp_ToDate(_read_.MVAIMPN1),cp_NullValue(_read_.MVAIMPN1))
      this.w_MVAIMPN2 = NVL(cp_ToDate(_read_.MVAIMPN2),cp_NullValue(_read_.MVAIMPN2))
      this.w_MVAIMPN3 = NVL(cp_ToDate(_read_.MVAIMPN3),cp_NullValue(_read_.MVAIMPN3))
      this.w_MVAIMPN4 = NVL(cp_ToDate(_read_.MVAIMPN4),cp_NullValue(_read_.MVAIMPN4))
      this.w_MVAIMPN5 = NVL(cp_ToDate(_read_.MVAIMPN5),cp_NullValue(_read_.MVAIMPN5))
      this.w_MVAIMPN6 = NVL(cp_ToDate(_read_.MVAIMPN6),cp_NullValue(_read_.MVAIMPN6))
      this.w_MVAIMPS1 = NVL(cp_ToDate(_read_.MVAIMPS1),cp_NullValue(_read_.MVAIMPS1))
      this.w_MVAIMPS2 = NVL(cp_ToDate(_read_.MVAIMPS2),cp_NullValue(_read_.MVAIMPS2))
      this.w_MVAIMPS3 = NVL(cp_ToDate(_read_.MVAIMPS3),cp_NullValue(_read_.MVAIMPS3))
      this.w_MVAIMPS4 = NVL(cp_ToDate(_read_.MVAIMPS4),cp_NullValue(_read_.MVAIMPS4))
      this.w_MVAIMPS5 = NVL(cp_ToDate(_read_.MVAIMPS5),cp_NullValue(_read_.MVAIMPS5))
      this.w_MVAIMPS6 = NVL(cp_ToDate(_read_.MVAIMPS6),cp_NullValue(_read_.MVAIMPS6))
      this.w_MVAFLOM1 = NVL(cp_ToDate(_read_.MVAFLOM1),cp_NullValue(_read_.MVAFLOM1))
      this.w_MVAFLOM2 = NVL(cp_ToDate(_read_.MVAFLOM2),cp_NullValue(_read_.MVAFLOM2))
      this.w_MVAFLOM3 = NVL(cp_ToDate(_read_.MVAFLOM3),cp_NullValue(_read_.MVAFLOM3))
      this.w_MVAFLOM4 = NVL(cp_ToDate(_read_.MVAFLOM4),cp_NullValue(_read_.MVAFLOM4))
      this.w_MVAFLOM5 = NVL(cp_ToDate(_read_.MVAFLOM5),cp_NullValue(_read_.MVAFLOM5))
      this.w_MVAFLOM6 = NVL(cp_ToDate(_read_.MVAFLOM6),cp_NullValue(_read_.MVAFLOM6))
      this.w_MVSPEBOL = NVL(cp_ToDate(_read_.MVSPEBOL),cp_NullValue(_read_.MVSPEBOL))
      this.w_MVFLSCOR = NVL(cp_ToDate(_read_.MVFLSCOR),cp_NullValue(_read_.MVFLSCOR))
      this.w_MVFLRINC = NVL(cp_ToDate(_read_.MVFLRINC),cp_NullValue(_read_.MVFLRINC))
      this.w_MVFLRIMB = NVL(cp_ToDate(_read_.MVFLRIMB),cp_NullValue(_read_.MVFLRIMB))
      this.w_MVFLRTRA = NVL(cp_ToDate(_read_.MVFLRTRA),cp_NullValue(_read_.MVFLRTRA))
      this.w_MVSPEINC = NVL(cp_ToDate(_read_.MVSPEINC),cp_NullValue(_read_.MVSPEINC))
      this.w_MVSPEIMB = NVL(cp_ToDate(_read_.MVSPEIMB),cp_NullValue(_read_.MVSPEIMB))
      this.w_MVSPETRA = NVL(cp_ToDate(_read_.MVSPETRA),cp_NullValue(_read_.MVSPETRA))
      this.w_MVACCONT = NVL(cp_ToDate(_read_.MVACCONT),cp_NullValue(_read_.MVACCONT))
      this.w_MVACCPRE = NVL(cp_ToDate(_read_.MVACCPRE),cp_NullValue(_read_.MVACCPRE))
      this.w_MVTOTRIT = NVL(cp_ToDate(_read_.MVTOTRIT),cp_NullValue(_read_.MVTOTRIT))
      this.w_MVTOTENA = NVL(cp_ToDate(_read_.MVTOTENA),cp_NullValue(_read_.MVTOTENA))
      this.w_MVCODPAG = NVL(cp_ToDate(_read_.MVCODPAG),cp_NullValue(_read_.MVCODPAG))
      this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
      this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
      this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      this.w_MVDATDIV = NVL(cp_ToDate(_read_.MVDATDIV),cp_NullValue(_read_.MVDATDIV))
      this.w_MVCODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
      this.w_MVACCOLD = NVL(cp_ToDate(_read_.MVACCOLD),cp_NullValue(_read_.MVACCOLD))
      this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODORN),cp_NullValue(_read_.MVCODORN))
      this.w_MVRITPRE = NVL(cp_ToDate(_read_.MVRITPRE),cp_NullValue(_read_.MVRITPRE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_XCONORN = IIF(Not Empty(this.w_MVCODORN) And g_XCONDI = "S",this.w_MVCODORN,this.w_MVCODCON)
    * --- Nuovo Acconto Precedente ripristinando MVACCONT da prima contabilizzazione
    this.w_MVACCPRE = this.w_MVACCOLD
    * --- Totale Documento
    this.w_TOTIMPOS = 0
    this.w_TOTIMPON = 0
    this.w_TOTIMPOS = IIF(this.w_MVAFLOM1 $ "XI", this.w_MVAIMPS1, 0) + IIF(this.w_MVAFLOM2 $ "XI", this.w_MVAIMPS2, 0) + IIF(this.w_MVAFLOM3 $ "XI", this.w_MVAIMPS3, 0)
    this.w_TOTIMPOS = this.w_TOTIMPOS + IIF(this.w_MVAFLOM4 $ "XI", this.w_MVAIMPS4, 0) + IIF(this.w_MVAFLOM5 $ "XI", this.w_MVAIMPS5, 0) + IIF(this.w_MVAFLOM6 $ "XI", this.w_MVAIMPS6, 0)
    this.w_TOTIMPON = IIF(this.w_MVAFLOM1 = "X", this.w_MVAIMPN1, 0) + IIF(this.w_MVAFLOM2 = "X", this.w_MVAIMPN2, 0) + IIF(this.w_MVAFLOM3 = "X", this.w_MVAIMPN3, 0)
    this.w_TOTIMPON = this.w_TOTIMPON + IIF(this.w_MVAFLOM4 = "X", this.w_MVAIMPN4, 0) + IIF(this.w_MVAFLOM5 = "X", this.w_MVAIMPN5, 0) + IIF(this.w_MVAFLOM6 = "X", this.w_MVAIMPN6, 0)
    this.w_TOTIMPON = this.w_TOTIMPON - this.w_MVSPEBOL
    * --- Netto Rata: Tot.Imponibile+Spese Bolli-Acconti-Spese non Rip.
    this.w_APPO2 = IIF(this.w_MVFLSCOR="S", 0, IIF(this.w_MVFLRINC="S", 0, this.w_MVSPEINC) + IIF(this.w_MVFLRIMB="S", 0, this.w_MVSPEIMB) + IIF(this.w_MVFLRTRA="S", 0, this.w_MVSPETRA))
    this.w_APPO = (this.w_TOTIMPON+this.w_MVSPEBOL)-(this.w_APPO2+this.w_MVACCONT+this.w_MVACCPRE+this.w_MVTOTRIT+this.w_MVTOTENA+this.w_MVRITPRE)
    this.w_APPO1 = this.w_TOTIMPOS
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_XCONORN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS;
        from (i_cTable) where;
            ANTIPCON = this.w_MVTIPCON;
            and ANCODICE = this.w_XCONORN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MESE1 = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
      this.w_MESE2 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
      this.w_GIORN1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
      this.w_GIORN2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
      this.w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ESCL1 = STR(this.w_MESE1, 2, 0) + STR(this.w_GIORN1, 2, 0) + STR(this.w_GIOFIS, 2, 0)
    this.w_ESCL2 = STR(this.w_MESE2, 2, 0) + STR(this.w_GIORN2, 2, 0)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_MVCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_NURATE = 0
    if NOT EMPTY(this.w_MVCODPAG)
      this.w_NURATE = SCADENZE("RA",this.w_MVCODPAG,this.w_MVDATDOC,this.w_APPO,this.w_APPO1,this.w_APPO2,this.w_ESCL1,this.w_ESCL2,this.w_DECTOT,this.w_MVDATDIV)
      this.w_NURATE = IIF(this.w_NURATE>50, 50, this.w_NURATE)
    endif
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVRIFCON ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFCON');
      +",MVFLCONT ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
      +",MVACCPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCPRE),'DOC_MAST','MVACCPRE');
      +",MVACCOLD ="+cp_NullLink(cp_ToStrODBC(0),'DOC_MAST','MVACCOLD');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVRIFCON = SPACE(10);
          ,MVFLCONT = " ";
          ,MVACCPRE = this.w_MVACCPRE;
          ,MVACCOLD = 0;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura DOC_MAST da GSVE_BAC'
      return
    endif
    * --- Delete from DOC_RATE
    i_nConn=i_TableProp[this.DOC_RATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RSSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      delete from (i_cTable) where;
            RSSERIAL = this.w_MVSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore in cancellazione DOC_RATE'
      return
    endif
    * --- Cicla Sul Vettore delle Rate (definito in GSAR_BFA)
    FOR L_i=1 TO this.w_NURATE
    this.w_RSDATRAT = RA[L_i, 1]
    this.w_RSIMPRAT = RA[L_i, 2] + RA[L_i, 3] + RA[L_i, 4]
    if NOT EMPTY(this.w_RSDATRAT) AND this.w_RSIMPRAT<>0
      this.w_RSNUMRAT = L_i
      this.w_RSMODPAG = RA[L_i, 5]
      this.w_RSFLPROV = RA[L_i, 6]
      * --- Insert into DOC_RATE
      i_nConn=i_TableProp[this.DOC_RATE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+",RSFLPROV"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_RATE','RSSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSNUMRAT),'DOC_RATE','RSNUMRAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSDATRAT),'DOC_RATE','RSDATRAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSIMPRAT),'DOC_RATE','RSIMPRAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSMODPAG),'DOC_RATE','RSMODPAG');
        +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_RATE','RSFLSOSP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSFLPROV),'DOC_RATE','RSFLPROV');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.w_MVSERIAL,'RSNUMRAT',this.w_RSNUMRAT,'RSDATRAT',this.w_RSDATRAT,'RSIMPRAT',this.w_RSIMPRAT,'RSMODPAG',this.w_RSMODPAG,'RSFLSOSP'," ",'RSFLPROV',this.w_RSFLPROV)
        insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP,RSFLPROV &i_ccchkf. );
           values (;
             this.w_MVSERIAL;
             ,this.w_RSNUMRAT;
             ,this.w_RSDATRAT;
             ,this.w_RSIMPRAT;
             ,this.w_RSMODPAG;
             ," ";
             ,this.w_RSFLPROV;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore in inserimento DOC_RATE'
        return
      endif
    endif
    NEXT
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='DOC_RATE'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
