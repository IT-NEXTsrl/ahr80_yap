* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_afq                                                        *
*              Modello F24 sez. erario                                         *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_205]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-14                                                      *
* Last revis.: 2008-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_afq")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_afq")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_afq")
  return

* --- Class definition
define class tgscg_afq as StdPCForm
  Width  = 714
  Height = 290
  Top    = 68
  Left   = 84
  cComment = "Modello F24 sez. erario"
  cPrg = "gscg_afq"
  HelpContextID=209050263
  add object cnt as tcgscg_afq
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_afq as PCContext
  w_EFSERIAL = space(10)
  w_VALUTA = space(3)
  w_EFTRIER1 = space(5)
  w_EFRATER1 = space(4)
  w_EFANNER1 = space(4)
  w_EFIMDER1 = 0
  w_EFIMCER1 = 0
  w_EFTRIER2 = space(5)
  w_EFRATER2 = space(4)
  w_EFANNER2 = space(4)
  w_EFIMDER2 = 0
  w_EFIMCER2 = 0
  w_EFTRIER3 = space(5)
  w_EFRATER3 = space(4)
  w_EFANNER3 = space(4)
  w_EFIMDER3 = 0
  w_EFIMCER3 = 0
  w_EFTRIER4 = space(5)
  w_EFRATER4 = space(4)
  w_EFANNER4 = space(4)
  w_EFIMDER4 = 0
  w_EFIMCER4 = 0
  w_EFTRIER5 = space(5)
  w_EFRATER5 = space(4)
  w_EFANNER5 = space(4)
  w_EFIMDER5 = 0
  w_EFIMCER5 = 0
  w_EFTRIER6 = space(5)
  w_EFRATER6 = space(4)
  w_EFANNER6 = space(4)
  w_EFIMDER6 = 0
  w_EFIMCER6 = 0
  w_EFTOTDER = 0
  w_EFTOTCER = 0
  w_EFSALDER = 0
  proc Save(oFrom)
    this.w_EFSERIAL = oFrom.w_EFSERIAL
    this.w_VALUTA = oFrom.w_VALUTA
    this.w_EFTRIER1 = oFrom.w_EFTRIER1
    this.w_EFRATER1 = oFrom.w_EFRATER1
    this.w_EFANNER1 = oFrom.w_EFANNER1
    this.w_EFIMDER1 = oFrom.w_EFIMDER1
    this.w_EFIMCER1 = oFrom.w_EFIMCER1
    this.w_EFTRIER2 = oFrom.w_EFTRIER2
    this.w_EFRATER2 = oFrom.w_EFRATER2
    this.w_EFANNER2 = oFrom.w_EFANNER2
    this.w_EFIMDER2 = oFrom.w_EFIMDER2
    this.w_EFIMCER2 = oFrom.w_EFIMCER2
    this.w_EFTRIER3 = oFrom.w_EFTRIER3
    this.w_EFRATER3 = oFrom.w_EFRATER3
    this.w_EFANNER3 = oFrom.w_EFANNER3
    this.w_EFIMDER3 = oFrom.w_EFIMDER3
    this.w_EFIMCER3 = oFrom.w_EFIMCER3
    this.w_EFTRIER4 = oFrom.w_EFTRIER4
    this.w_EFRATER4 = oFrom.w_EFRATER4
    this.w_EFANNER4 = oFrom.w_EFANNER4
    this.w_EFIMDER4 = oFrom.w_EFIMDER4
    this.w_EFIMCER4 = oFrom.w_EFIMCER4
    this.w_EFTRIER5 = oFrom.w_EFTRIER5
    this.w_EFRATER5 = oFrom.w_EFRATER5
    this.w_EFANNER5 = oFrom.w_EFANNER5
    this.w_EFIMDER5 = oFrom.w_EFIMDER5
    this.w_EFIMCER5 = oFrom.w_EFIMCER5
    this.w_EFTRIER6 = oFrom.w_EFTRIER6
    this.w_EFRATER6 = oFrom.w_EFRATER6
    this.w_EFANNER6 = oFrom.w_EFANNER6
    this.w_EFIMDER6 = oFrom.w_EFIMDER6
    this.w_EFIMCER6 = oFrom.w_EFIMCER6
    this.w_EFTOTDER = oFrom.w_EFTOTDER
    this.w_EFTOTCER = oFrom.w_EFTOTCER
    this.w_EFSALDER = oFrom.w_EFSALDER
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_EFSERIAL = this.w_EFSERIAL
    oTo.w_VALUTA = this.w_VALUTA
    oTo.w_EFTRIER1 = this.w_EFTRIER1
    oTo.w_EFRATER1 = this.w_EFRATER1
    oTo.w_EFANNER1 = this.w_EFANNER1
    oTo.w_EFIMDER1 = this.w_EFIMDER1
    oTo.w_EFIMCER1 = this.w_EFIMCER1
    oTo.w_EFTRIER2 = this.w_EFTRIER2
    oTo.w_EFRATER2 = this.w_EFRATER2
    oTo.w_EFANNER2 = this.w_EFANNER2
    oTo.w_EFIMDER2 = this.w_EFIMDER2
    oTo.w_EFIMCER2 = this.w_EFIMCER2
    oTo.w_EFTRIER3 = this.w_EFTRIER3
    oTo.w_EFRATER3 = this.w_EFRATER3
    oTo.w_EFANNER3 = this.w_EFANNER3
    oTo.w_EFIMDER3 = this.w_EFIMDER3
    oTo.w_EFIMCER3 = this.w_EFIMCER3
    oTo.w_EFTRIER4 = this.w_EFTRIER4
    oTo.w_EFRATER4 = this.w_EFRATER4
    oTo.w_EFANNER4 = this.w_EFANNER4
    oTo.w_EFIMDER4 = this.w_EFIMDER4
    oTo.w_EFIMCER4 = this.w_EFIMCER4
    oTo.w_EFTRIER5 = this.w_EFTRIER5
    oTo.w_EFRATER5 = this.w_EFRATER5
    oTo.w_EFANNER5 = this.w_EFANNER5
    oTo.w_EFIMDER5 = this.w_EFIMDER5
    oTo.w_EFIMCER5 = this.w_EFIMCER5
    oTo.w_EFTRIER6 = this.w_EFTRIER6
    oTo.w_EFRATER6 = this.w_EFRATER6
    oTo.w_EFANNER6 = this.w_EFANNER6
    oTo.w_EFIMDER6 = this.w_EFIMDER6
    oTo.w_EFIMCER6 = this.w_EFIMCER6
    oTo.w_EFTOTDER = this.w_EFTOTDER
    oTo.w_EFTOTCER = this.w_EFTOTCER
    oTo.w_EFSALDER = this.w_EFSALDER
    PCContext::Load(oTo)
enddefine

define class tcgscg_afq as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 714
  Height = 290
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-17"
  HelpContextID=209050263
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Constant Properties
  MODEPAG_IDX = 0
  CODI_UFF_IDX = 0
  COD_TRIB_IDX = 0
  cFile = "MODEPAG"
  cKeySelect = "EFSERIAL"
  cKeyWhere  = "EFSERIAL=this.w_EFSERIAL"
  cKeyWhereODBC = '"EFSERIAL="+cp_ToStrODBC(this.w_EFSERIAL)';

  cKeyWhereODBCqualified = '"MODEPAG.EFSERIAL="+cp_ToStrODBC(this.w_EFSERIAL)';

  cPrg = "gscg_afq"
  cComment = "Modello F24 sez. erario"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_EFSERIAL = space(10)
  w_VALUTA = space(3)
  w_EFTRIER1 = space(5)
  o_EFTRIER1 = space(5)
  w_EFRATER1 = space(4)
  w_EFANNER1 = space(4)
  w_EFIMDER1 = 0
  w_EFIMCER1 = 0
  w_EFTRIER2 = space(5)
  o_EFTRIER2 = space(5)
  w_EFRATER2 = space(4)
  w_EFANNER2 = space(4)
  w_EFIMDER2 = 0
  w_EFIMCER2 = 0
  w_EFTRIER3 = space(5)
  o_EFTRIER3 = space(5)
  w_EFRATER3 = space(4)
  w_EFANNER3 = space(4)
  w_EFIMDER3 = 0
  w_EFIMCER3 = 0
  w_EFTRIER4 = space(5)
  o_EFTRIER4 = space(5)
  w_EFRATER4 = space(4)
  w_EFANNER4 = space(4)
  w_EFIMDER4 = 0
  w_EFIMCER4 = 0
  w_EFTRIER5 = space(5)
  o_EFTRIER5 = space(5)
  w_EFRATER5 = space(4)
  w_EFANNER5 = space(4)
  w_EFIMDER5 = 0
  w_EFIMCER5 = 0
  w_EFTRIER6 = space(5)
  o_EFTRIER6 = space(5)
  w_EFRATER6 = space(4)
  w_EFANNER6 = space(4)
  w_EFIMDER6 = 0
  w_EFIMCER6 = 0
  w_EFTOTDER = 0
  w_EFTOTCER = 0
  w_EFSALDER = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_afqPag1","gscg_afq",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 267497718
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oEFTRIER1_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CODI_UFF'
    this.cWorkTables[2]='COD_TRIB'
    this.cWorkTables[3]='MODEPAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MODEPAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MODEPAG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_afq'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MODEPAG where EFSERIAL=KeySet.EFSERIAL
    *
    i_nConn = i_TableProp[this.MODEPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODEPAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MODEPAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MODEPAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MODEPAG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'EFSERIAL',this.w_EFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_EFSERIAL = NVL(EFSERIAL,space(10))
        .w_VALUTA = .oParentObject .w_MFVALUTA
        .w_EFTRIER1 = NVL(EFTRIER1,space(5))
          * evitabile
          *.link_1_3('Load')
        .w_EFRATER1 = NVL(EFRATER1,space(4))
        .w_EFANNER1 = NVL(EFANNER1,space(4))
        .w_EFIMDER1 = NVL(EFIMDER1,0)
        .w_EFIMCER1 = NVL(EFIMCER1,0)
        .w_EFTRIER2 = NVL(EFTRIER2,space(5))
          * evitabile
          *.link_1_8('Load')
        .w_EFRATER2 = NVL(EFRATER2,space(4))
        .w_EFANNER2 = NVL(EFANNER2,space(4))
        .w_EFIMDER2 = NVL(EFIMDER2,0)
        .w_EFIMCER2 = NVL(EFIMCER2,0)
        .w_EFTRIER3 = NVL(EFTRIER3,space(5))
          * evitabile
          *.link_1_13('Load')
        .w_EFRATER3 = NVL(EFRATER3,space(4))
        .w_EFANNER3 = NVL(EFANNER3,space(4))
        .w_EFIMDER3 = NVL(EFIMDER3,0)
        .w_EFIMCER3 = NVL(EFIMCER3,0)
        .w_EFTRIER4 = NVL(EFTRIER4,space(5))
          * evitabile
          *.link_1_18('Load')
        .w_EFRATER4 = NVL(EFRATER4,space(4))
        .w_EFANNER4 = NVL(EFANNER4,space(4))
        .w_EFIMDER4 = NVL(EFIMDER4,0)
        .w_EFIMCER4 = NVL(EFIMCER4,0)
        .w_EFTRIER5 = NVL(EFTRIER5,space(5))
          * evitabile
          *.link_1_27('Load')
        .w_EFRATER5 = NVL(EFRATER5,space(4))
        .w_EFANNER5 = NVL(EFANNER5,space(4))
        .w_EFIMDER5 = NVL(EFIMDER5,0)
        .w_EFIMCER5 = NVL(EFIMCER5,0)
        .w_EFTRIER6 = NVL(EFTRIER6,space(5))
          * evitabile
          *.link_1_32('Load')
        .w_EFRATER6 = NVL(EFRATER6,space(4))
        .w_EFANNER6 = NVL(EFANNER6,space(4))
        .w_EFIMDER6 = NVL(EFIMDER6,0)
        .w_EFIMCER6 = NVL(EFIMCER6,0)
        .w_EFTOTDER = NVL(EFTOTDER,0)
        .w_EFTOTCER = NVL(EFTOTCER,0)
        .w_EFSALDER = NVL(EFSALDER,0)
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        cp_LoadRecExtFlds(this,'MODEPAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_afq
    local lTot
    lTot=0
    with this.oParentObject
       lTot = .w_MFSALDPS + .w_MFSALDRE  + .w_MFSALINA + .w_MFSALAEN + .w_APPOIMP2
    endwith
    * aggiorna variabile sul padre
    this.oParentObject.w_APPOIMP = this.w_EFSALDER
    this.oParentObject.w_MFSALFIN = this.w_EFSALDER + lTot
    this.oParentObject.SetControlsValue()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_EFSERIAL = space(10)
      .w_VALUTA = space(3)
      .w_EFTRIER1 = space(5)
      .w_EFRATER1 = space(4)
      .w_EFANNER1 = space(4)
      .w_EFIMDER1 = 0
      .w_EFIMCER1 = 0
      .w_EFTRIER2 = space(5)
      .w_EFRATER2 = space(4)
      .w_EFANNER2 = space(4)
      .w_EFIMDER2 = 0
      .w_EFIMCER2 = 0
      .w_EFTRIER3 = space(5)
      .w_EFRATER3 = space(4)
      .w_EFANNER3 = space(4)
      .w_EFIMDER3 = 0
      .w_EFIMCER3 = 0
      .w_EFTRIER4 = space(5)
      .w_EFRATER4 = space(4)
      .w_EFANNER4 = space(4)
      .w_EFIMDER4 = 0
      .w_EFIMCER4 = 0
      .w_EFTRIER5 = space(5)
      .w_EFRATER5 = space(4)
      .w_EFANNER5 = space(4)
      .w_EFIMDER5 = 0
      .w_EFIMCER5 = 0
      .w_EFTRIER6 = space(5)
      .w_EFRATER6 = space(4)
      .w_EFANNER6 = space(4)
      .w_EFIMDER6 = 0
      .w_EFIMCER6 = 0
      .w_EFTOTDER = 0
      .w_EFTOTCER = 0
      .w_EFSALDER = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_VALUTA = .oParentObject .w_MFVALUTA
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_EFTRIER1))
          .link_1_3('Full')
          endif
        .w_EFRATER1 = iif(empty(.w_EFTRIER1),' ',.w_EFRATER1)
        .w_EFANNER1 = iif(empty(.w_EFTRIER1),' ',iif(left(.w_EFTRIER1,4) = '1668','0000',.w_EFANNER1))
        .w_EFIMDER1 = iif(empty(.w_EFTRIER1),0,.w_EFIMDER1)
        .w_EFIMCER1 = iif(empty(.w_EFTRIER1),0,.w_EFIMCER1)
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_EFTRIER2))
          .link_1_8('Full')
          endif
        .w_EFRATER2 = iif(empty(.w_EFTRIER2),' ',.w_EFRATER2)
        .w_EFANNER2 = iif(empty(.w_EFTRIER2),' ',iif(left(.w_EFTRIER2,4) = '1668','0000',.w_EFANNER2))
        .w_EFIMDER2 = iif(empty(.w_EFTRIER2),0,.w_EFIMDER2)
        .w_EFIMCER2 = iif(empty(.w_EFTRIER2),0,.w_EFIMCER2)
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_EFTRIER3))
          .link_1_13('Full')
          endif
        .w_EFRATER3 = iif(empty(.w_EFTRIER3),' ',.w_EFRATER3)
        .w_EFANNER3 = iif(empty(.w_EFTRIER3),' ',iif(left(.w_EFTRIER3,4) = '1668','0000',.w_EFANNER3))
        .w_EFIMDER3 = iif(empty(.w_EFTRIER3),0,.w_EFIMDER3)
        .w_EFIMCER3 = iif(empty(.w_EFTRIER3),0,.w_EFIMCER3)
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_EFTRIER4))
          .link_1_18('Full')
          endif
        .w_EFRATER4 = iif(empty(.w_EFTRIER4),' ',.w_EFRATER4)
        .w_EFANNER4 = iif(empty(.w_EFTRIER4),' ',iif(left(.w_EFTRIER4,4) = '1668','0000',.w_EFANNER4))
        .w_EFIMDER4 = iif(empty(.w_EFTRIER4),0,.w_EFIMDER4)
        .w_EFIMCER4 = iif(empty(.w_EFTRIER4),0,.w_EFIMCER4)
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_EFTRIER5))
          .link_1_27('Full')
          endif
        .w_EFRATER5 = iif(empty(.w_EFTRIER5),' ',.w_EFRATER5)
        .w_EFANNER5 = iif(empty(.w_EFTRIER5),' ',iif(left(.w_EFTRIER5,4) = '1668','0000',.w_EFANNER5))
        .w_EFIMDER5 = iif(empty(.w_EFTRIER5),0,.w_EFIMDER5)
        .w_EFIMCER5 = iif(empty(.w_EFTRIER5),0,.w_EFIMCER5)
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_EFTRIER6))
          .link_1_32('Full')
          endif
        .w_EFRATER6 = iif(empty(.w_EFTRIER6),' ',.w_EFRATER6)
        .w_EFANNER6 = iif(empty(.w_EFTRIER6),' ',iif(left(.w_EFTRIER6,4) = '1668','0000',.w_EFANNER6))
        .w_EFIMDER6 = iif(empty(.w_EFTRIER6),0,.w_EFIMDER6)
        .w_EFIMCER6 = iif(empty(.w_EFTRIER6),0,.w_EFIMCER6)
        .w_EFTOTDER = .w_EFIMDER1+.w_EFIMDER2+.w_EFIMDER3+.w_EFIMDER4+.w_EFIMDER5+.w_EFIMDER6
        .w_EFTOTCER = .w_EFIMCER1+.w_EFIMCER2+.w_EFIMCER3+.w_EFIMCER4+.w_EFIMCER5+.w_EFIMCER6
        .w_EFSALDER = .w_EFTOTDER-.w_EFTOTCER
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
      endif
    endwith
    cp_BlankRecExtFlds(this,'MODEPAG')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_afq
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oEFTRIER1_1_3.enabled = i_bVal
      .Page1.oPag.oEFRATER1_1_4.enabled = i_bVal
      .Page1.oPag.oEFANNER1_1_5.enabled = i_bVal
      .Page1.oPag.oEFIMDER1_1_6.enabled = i_bVal
      .Page1.oPag.oEFIMCER1_1_7.enabled = i_bVal
      .Page1.oPag.oEFTRIER2_1_8.enabled = i_bVal
      .Page1.oPag.oEFRATER2_1_9.enabled = i_bVal
      .Page1.oPag.oEFANNER2_1_10.enabled = i_bVal
      .Page1.oPag.oEFIMDER2_1_11.enabled = i_bVal
      .Page1.oPag.oEFIMCER2_1_12.enabled = i_bVal
      .Page1.oPag.oEFTRIER3_1_13.enabled = i_bVal
      .Page1.oPag.oEFRATER3_1_14.enabled = i_bVal
      .Page1.oPag.oEFANNER3_1_15.enabled = i_bVal
      .Page1.oPag.oEFIMDER3_1_16.enabled = i_bVal
      .Page1.oPag.oEFIMCER3_1_17.enabled = i_bVal
      .Page1.oPag.oEFTRIER4_1_18.enabled = i_bVal
      .Page1.oPag.oEFRATER4_1_19.enabled = i_bVal
      .Page1.oPag.oEFANNER4_1_20.enabled = i_bVal
      .Page1.oPag.oEFIMDER4_1_21.enabled = i_bVal
      .Page1.oPag.oEFIMCER4_1_22.enabled = i_bVal
      .Page1.oPag.oEFTRIER5_1_27.enabled = i_bVal
      .Page1.oPag.oEFRATER5_1_28.enabled = i_bVal
      .Page1.oPag.oEFANNER5_1_29.enabled = i_bVal
      .Page1.oPag.oEFIMDER5_1_30.enabled = i_bVal
      .Page1.oPag.oEFIMCER5_1_31.enabled = i_bVal
      .Page1.oPag.oEFTRIER6_1_32.enabled = i_bVal
      .Page1.oPag.oEFRATER6_1_33.enabled = i_bVal
      .Page1.oPag.oEFANNER6_1_34.enabled = i_bVal
      .Page1.oPag.oEFIMDER6_1_35.enabled = i_bVal
      .Page1.oPag.oEFIMCER6_1_36.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MODEPAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MODEPAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFSERIAL,"EFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER1,"EFTRIER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER1,"EFRATER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER1,"EFANNER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER1,"EFIMDER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER1,"EFIMCER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER2,"EFTRIER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER2,"EFRATER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER2,"EFANNER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER2,"EFIMDER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER2,"EFIMCER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER3,"EFTRIER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER3,"EFRATER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER3,"EFANNER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER3,"EFIMDER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER3,"EFIMCER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER4,"EFTRIER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER4,"EFRATER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER4,"EFANNER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER4,"EFIMDER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER4,"EFIMCER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER5,"EFTRIER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER5,"EFRATER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER5,"EFANNER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER5,"EFIMDER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER5,"EFIMCER5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTRIER6,"EFTRIER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFRATER6,"EFRATER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFANNER6,"EFANNER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMDER6,"EFIMDER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFIMCER6,"EFIMCER6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTOTDER,"EFTOTDER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFTOTCER,"EFTOTCER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EFSALDER,"EFSALDER",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODEPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODEPAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MODEPAG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MODEPAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MODEPAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MODEPAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(EFSERIAL,EFTRIER1,EFRATER1,EFANNER1,EFIMDER1"+;
                  ",EFIMCER1,EFTRIER2,EFRATER2,EFANNER2,EFIMDER2"+;
                  ",EFIMCER2,EFTRIER3,EFRATER3,EFANNER3,EFIMDER3"+;
                  ",EFIMCER3,EFTRIER4,EFRATER4,EFANNER4,EFIMDER4"+;
                  ",EFIMCER4,EFTRIER5,EFRATER5,EFANNER5,EFIMDER5"+;
                  ",EFIMCER5,EFTRIER6,EFRATER6,EFANNER6,EFIMDER6"+;
                  ",EFIMCER6,EFTOTDER,EFTOTCER,EFSALDER "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_EFSERIAL)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER1)+;
                  ","+cp_ToStrODBC(this.w_EFRATER1)+;
                  ","+cp_ToStrODBC(this.w_EFANNER1)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER1)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER1)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER2)+;
                  ","+cp_ToStrODBC(this.w_EFRATER2)+;
                  ","+cp_ToStrODBC(this.w_EFANNER2)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER2)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER2)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER3)+;
                  ","+cp_ToStrODBC(this.w_EFRATER3)+;
                  ","+cp_ToStrODBC(this.w_EFANNER3)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER3)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER3)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER4)+;
                  ","+cp_ToStrODBC(this.w_EFRATER4)+;
                  ","+cp_ToStrODBC(this.w_EFANNER4)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER4)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER4)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER5)+;
                  ","+cp_ToStrODBC(this.w_EFRATER5)+;
                  ","+cp_ToStrODBC(this.w_EFANNER5)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER5)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER5)+;
                  ","+cp_ToStrODBCNull(this.w_EFTRIER6)+;
                  ","+cp_ToStrODBC(this.w_EFRATER6)+;
                  ","+cp_ToStrODBC(this.w_EFANNER6)+;
                  ","+cp_ToStrODBC(this.w_EFIMDER6)+;
                  ","+cp_ToStrODBC(this.w_EFIMCER6)+;
                  ","+cp_ToStrODBC(this.w_EFTOTDER)+;
                  ","+cp_ToStrODBC(this.w_EFTOTCER)+;
                  ","+cp_ToStrODBC(this.w_EFSALDER)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MODEPAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MODEPAG')
        cp_CheckDeletedKey(i_cTable,0,'EFSERIAL',this.w_EFSERIAL)
        INSERT INTO (i_cTable);
              (EFSERIAL,EFTRIER1,EFRATER1,EFANNER1,EFIMDER1,EFIMCER1,EFTRIER2,EFRATER2,EFANNER2,EFIMDER2,EFIMCER2,EFTRIER3,EFRATER3,EFANNER3,EFIMDER3,EFIMCER3,EFTRIER4,EFRATER4,EFANNER4,EFIMDER4,EFIMCER4,EFTRIER5,EFRATER5,EFANNER5,EFIMDER5,EFIMCER5,EFTRIER6,EFRATER6,EFANNER6,EFIMDER6,EFIMCER6,EFTOTDER,EFTOTCER,EFSALDER  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_EFSERIAL;
                  ,this.w_EFTRIER1;
                  ,this.w_EFRATER1;
                  ,this.w_EFANNER1;
                  ,this.w_EFIMDER1;
                  ,this.w_EFIMCER1;
                  ,this.w_EFTRIER2;
                  ,this.w_EFRATER2;
                  ,this.w_EFANNER2;
                  ,this.w_EFIMDER2;
                  ,this.w_EFIMCER2;
                  ,this.w_EFTRIER3;
                  ,this.w_EFRATER3;
                  ,this.w_EFANNER3;
                  ,this.w_EFIMDER3;
                  ,this.w_EFIMCER3;
                  ,this.w_EFTRIER4;
                  ,this.w_EFRATER4;
                  ,this.w_EFANNER4;
                  ,this.w_EFIMDER4;
                  ,this.w_EFIMCER4;
                  ,this.w_EFTRIER5;
                  ,this.w_EFRATER5;
                  ,this.w_EFANNER5;
                  ,this.w_EFIMDER5;
                  ,this.w_EFIMCER5;
                  ,this.w_EFTRIER6;
                  ,this.w_EFRATER6;
                  ,this.w_EFANNER6;
                  ,this.w_EFIMDER6;
                  ,this.w_EFIMCER6;
                  ,this.w_EFTOTDER;
                  ,this.w_EFTOTCER;
                  ,this.w_EFSALDER;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MODEPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODEPAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MODEPAG_IDX,i_nConn)
      *
      * update MODEPAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MODEPAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " EFTRIER1="+cp_ToStrODBCNull(this.w_EFTRIER1)+;
             ",EFRATER1="+cp_ToStrODBC(this.w_EFRATER1)+;
             ",EFANNER1="+cp_ToStrODBC(this.w_EFANNER1)+;
             ",EFIMDER1="+cp_ToStrODBC(this.w_EFIMDER1)+;
             ",EFIMCER1="+cp_ToStrODBC(this.w_EFIMCER1)+;
             ",EFTRIER2="+cp_ToStrODBCNull(this.w_EFTRIER2)+;
             ",EFRATER2="+cp_ToStrODBC(this.w_EFRATER2)+;
             ",EFANNER2="+cp_ToStrODBC(this.w_EFANNER2)+;
             ",EFIMDER2="+cp_ToStrODBC(this.w_EFIMDER2)+;
             ",EFIMCER2="+cp_ToStrODBC(this.w_EFIMCER2)+;
             ",EFTRIER3="+cp_ToStrODBCNull(this.w_EFTRIER3)+;
             ",EFRATER3="+cp_ToStrODBC(this.w_EFRATER3)+;
             ",EFANNER3="+cp_ToStrODBC(this.w_EFANNER3)+;
             ",EFIMDER3="+cp_ToStrODBC(this.w_EFIMDER3)+;
             ",EFIMCER3="+cp_ToStrODBC(this.w_EFIMCER3)+;
             ",EFTRIER4="+cp_ToStrODBCNull(this.w_EFTRIER4)+;
             ",EFRATER4="+cp_ToStrODBC(this.w_EFRATER4)+;
             ",EFANNER4="+cp_ToStrODBC(this.w_EFANNER4)+;
             ",EFIMDER4="+cp_ToStrODBC(this.w_EFIMDER4)+;
             ",EFIMCER4="+cp_ToStrODBC(this.w_EFIMCER4)+;
             ",EFTRIER5="+cp_ToStrODBCNull(this.w_EFTRIER5)+;
             ",EFRATER5="+cp_ToStrODBC(this.w_EFRATER5)+;
             ",EFANNER5="+cp_ToStrODBC(this.w_EFANNER5)+;
             ",EFIMDER5="+cp_ToStrODBC(this.w_EFIMDER5)+;
             ",EFIMCER5="+cp_ToStrODBC(this.w_EFIMCER5)+;
             ",EFTRIER6="+cp_ToStrODBCNull(this.w_EFTRIER6)+;
             ",EFRATER6="+cp_ToStrODBC(this.w_EFRATER6)+;
             ",EFANNER6="+cp_ToStrODBC(this.w_EFANNER6)+;
             ",EFIMDER6="+cp_ToStrODBC(this.w_EFIMDER6)+;
             ",EFIMCER6="+cp_ToStrODBC(this.w_EFIMCER6)+;
             ",EFTOTDER="+cp_ToStrODBC(this.w_EFTOTDER)+;
             ",EFTOTCER="+cp_ToStrODBC(this.w_EFTOTCER)+;
             ",EFSALDER="+cp_ToStrODBC(this.w_EFSALDER)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MODEPAG')
        i_cWhere = cp_PKFox(i_cTable  ,'EFSERIAL',this.w_EFSERIAL  )
        UPDATE (i_cTable) SET;
              EFTRIER1=this.w_EFTRIER1;
             ,EFRATER1=this.w_EFRATER1;
             ,EFANNER1=this.w_EFANNER1;
             ,EFIMDER1=this.w_EFIMDER1;
             ,EFIMCER1=this.w_EFIMCER1;
             ,EFTRIER2=this.w_EFTRIER2;
             ,EFRATER2=this.w_EFRATER2;
             ,EFANNER2=this.w_EFANNER2;
             ,EFIMDER2=this.w_EFIMDER2;
             ,EFIMCER2=this.w_EFIMCER2;
             ,EFTRIER3=this.w_EFTRIER3;
             ,EFRATER3=this.w_EFRATER3;
             ,EFANNER3=this.w_EFANNER3;
             ,EFIMDER3=this.w_EFIMDER3;
             ,EFIMCER3=this.w_EFIMCER3;
             ,EFTRIER4=this.w_EFTRIER4;
             ,EFRATER4=this.w_EFRATER4;
             ,EFANNER4=this.w_EFANNER4;
             ,EFIMDER4=this.w_EFIMDER4;
             ,EFIMCER4=this.w_EFIMCER4;
             ,EFTRIER5=this.w_EFTRIER5;
             ,EFRATER5=this.w_EFRATER5;
             ,EFANNER5=this.w_EFANNER5;
             ,EFIMDER5=this.w_EFIMDER5;
             ,EFIMCER5=this.w_EFIMCER5;
             ,EFTRIER6=this.w_EFTRIER6;
             ,EFRATER6=this.w_EFRATER6;
             ,EFANNER6=this.w_EFANNER6;
             ,EFIMDER6=this.w_EFIMDER6;
             ,EFIMCER6=this.w_EFIMCER6;
             ,EFTOTDER=this.w_EFTOTDER;
             ,EFTOTCER=this.w_EFTOTCER;
             ,EFSALDER=this.w_EFSALDER;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MODEPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODEPAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MODEPAG_IDX,i_nConn)
      *
      * delete MODEPAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'EFSERIAL',this.w_EFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MODEPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODEPAG_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_VALUTA = .oParentObject .w_MFVALUTA
        .DoRTCalc(3,3,.t.)
        if .o_EFTRIER1<>.w_EFTRIER1
            .w_EFRATER1 = iif(empty(.w_EFTRIER1),' ',.w_EFRATER1)
        endif
        if .o_EFTRIER1<>.w_EFTRIER1
            .w_EFANNER1 = iif(empty(.w_EFTRIER1),' ',iif(left(.w_EFTRIER1,4) = '1668','0000',.w_EFANNER1))
        endif
        if .o_EFTRIER1<>.w_EFTRIER1
            .w_EFIMDER1 = iif(empty(.w_EFTRIER1),0,.w_EFIMDER1)
        endif
        if .o_EFTRIER1<>.w_EFTRIER1
            .w_EFIMCER1 = iif(empty(.w_EFTRIER1),0,.w_EFIMCER1)
        endif
        .DoRTCalc(8,8,.t.)
        if .o_EFTRIER2<>.w_EFTRIER2
            .w_EFRATER2 = iif(empty(.w_EFTRIER2),' ',.w_EFRATER2)
        endif
        if .o_EFTRIER2<>.w_EFTRIER2
            .w_EFANNER2 = iif(empty(.w_EFTRIER2),' ',iif(left(.w_EFTRIER2,4) = '1668','0000',.w_EFANNER2))
        endif
        if .o_EFTRIER2<>.w_EFTRIER2
            .w_EFIMDER2 = iif(empty(.w_EFTRIER2),0,.w_EFIMDER2)
        endif
        if .o_EFTRIER2<>.w_EFTRIER2
            .w_EFIMCER2 = iif(empty(.w_EFTRIER2),0,.w_EFIMCER2)
        endif
        .DoRTCalc(13,13,.t.)
        if .o_EFTRIER3<>.w_EFTRIER3
            .w_EFRATER3 = iif(empty(.w_EFTRIER3),' ',.w_EFRATER3)
        endif
        if .o_EFTRIER3<>.w_EFTRIER3
            .w_EFANNER3 = iif(empty(.w_EFTRIER3),' ',iif(left(.w_EFTRIER3,4) = '1668','0000',.w_EFANNER3))
        endif
        if .o_EFTRIER3<>.w_EFTRIER3
            .w_EFIMDER3 = iif(empty(.w_EFTRIER3),0,.w_EFIMDER3)
        endif
        if .o_EFTRIER3<>.w_EFTRIER3
            .w_EFIMCER3 = iif(empty(.w_EFTRIER3),0,.w_EFIMCER3)
        endif
        .DoRTCalc(18,18,.t.)
        if .o_EFTRIER4<>.w_EFTRIER4
            .w_EFRATER4 = iif(empty(.w_EFTRIER4),' ',.w_EFRATER4)
        endif
        if .o_EFTRIER4<>.w_EFTRIER4
            .w_EFANNER4 = iif(empty(.w_EFTRIER4),' ',iif(left(.w_EFTRIER4,4) = '1668','0000',.w_EFANNER4))
        endif
        if .o_EFTRIER4<>.w_EFTRIER4
            .w_EFIMDER4 = iif(empty(.w_EFTRIER4),0,.w_EFIMDER4)
        endif
        if .o_EFTRIER4<>.w_EFTRIER4
            .w_EFIMCER4 = iif(empty(.w_EFTRIER4),0,.w_EFIMCER4)
        endif
        .DoRTCalc(23,23,.t.)
        if .o_EFTRIER5<>.w_EFTRIER5
            .w_EFRATER5 = iif(empty(.w_EFTRIER5),' ',.w_EFRATER5)
        endif
        if .o_EFTRIER5<>.w_EFTRIER5
            .w_EFANNER5 = iif(empty(.w_EFTRIER5),' ',iif(left(.w_EFTRIER5,4) = '1668','0000',.w_EFANNER5))
        endif
        if .o_EFTRIER5<>.w_EFTRIER5
            .w_EFIMDER5 = iif(empty(.w_EFTRIER5),0,.w_EFIMDER5)
        endif
        if .o_EFTRIER5<>.w_EFTRIER5
            .w_EFIMCER5 = iif(empty(.w_EFTRIER5),0,.w_EFIMCER5)
        endif
        .DoRTCalc(28,28,.t.)
        if .o_EFTRIER6<>.w_EFTRIER6
            .w_EFRATER6 = iif(empty(.w_EFTRIER6),' ',.w_EFRATER6)
        endif
        if .o_EFTRIER6<>.w_EFTRIER6
            .w_EFANNER6 = iif(empty(.w_EFTRIER6),' ',iif(left(.w_EFTRIER6,4) = '1668','0000',.w_EFANNER6))
        endif
        if .o_EFTRIER6<>.w_EFTRIER6
            .w_EFIMDER6 = iif(empty(.w_EFTRIER6),0,.w_EFIMDER6)
        endif
        if .o_EFTRIER6<>.w_EFTRIER6
            .w_EFIMCER6 = iif(empty(.w_EFTRIER6),0,.w_EFIMCER6)
        endif
            .w_EFTOTDER = .w_EFIMDER1+.w_EFIMDER2+.w_EFIMDER3+.w_EFIMDER4+.w_EFIMDER5+.w_EFIMDER6
            .w_EFTOTCER = .w_EFIMCER1+.w_EFIMCER2+.w_EFIMCER3+.w_EFIMCER4+.w_EFIMCER5+.w_EFIMCER6
            .w_EFSALDER = .w_EFTOTDER-.w_EFTOTCER
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        * --- Area Manuale = Calculate
        * --- gscg_afq
        * aggiorna variabile sul padre
        .oParentObject.w_APPOIMP = .w_EFSALDER
        .oParentObject.w_MFSALFIN = .w_EFSALDER + .oParentObject.w_APPOIMP2+ .oParentObject.w_MFSALDPS + .oParentObject.w_MFSALDRE +  .oParentObject.w_MFSALINA + .oParentObject.w_MFSALAEN
        * aggiorna variabile sul padre
        .oParentObject.SetControlsValue()
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oEFRATER1_1_4.enabled = this.oPgFrm.Page1.oPag.oEFRATER1_1_4.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER1_1_5.enabled = this.oPgFrm.Page1.oPag.oEFANNER1_1_5.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER1_1_6.enabled = this.oPgFrm.Page1.oPag.oEFIMDER1_1_6.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER1_1_7.enabled = this.oPgFrm.Page1.oPag.oEFIMCER1_1_7.mCond()
    this.oPgFrm.Page1.oPag.oEFRATER2_1_9.enabled = this.oPgFrm.Page1.oPag.oEFRATER2_1_9.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER2_1_10.enabled = this.oPgFrm.Page1.oPag.oEFANNER2_1_10.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER2_1_11.enabled = this.oPgFrm.Page1.oPag.oEFIMDER2_1_11.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER2_1_12.enabled = this.oPgFrm.Page1.oPag.oEFIMCER2_1_12.mCond()
    this.oPgFrm.Page1.oPag.oEFRATER3_1_14.enabled = this.oPgFrm.Page1.oPag.oEFRATER3_1_14.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER3_1_15.enabled = this.oPgFrm.Page1.oPag.oEFANNER3_1_15.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER3_1_16.enabled = this.oPgFrm.Page1.oPag.oEFIMDER3_1_16.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER3_1_17.enabled = this.oPgFrm.Page1.oPag.oEFIMCER3_1_17.mCond()
    this.oPgFrm.Page1.oPag.oEFRATER4_1_19.enabled = this.oPgFrm.Page1.oPag.oEFRATER4_1_19.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER4_1_20.enabled = this.oPgFrm.Page1.oPag.oEFANNER4_1_20.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER4_1_21.enabled = this.oPgFrm.Page1.oPag.oEFIMDER4_1_21.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER4_1_22.enabled = this.oPgFrm.Page1.oPag.oEFIMCER4_1_22.mCond()
    this.oPgFrm.Page1.oPag.oEFRATER5_1_28.enabled = this.oPgFrm.Page1.oPag.oEFRATER5_1_28.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER5_1_29.enabled = this.oPgFrm.Page1.oPag.oEFANNER5_1_29.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER5_1_30.enabled = this.oPgFrm.Page1.oPag.oEFIMDER5_1_30.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER5_1_31.enabled = this.oPgFrm.Page1.oPag.oEFIMCER5_1_31.mCond()
    this.oPgFrm.Page1.oPag.oEFRATER6_1_33.enabled = this.oPgFrm.Page1.oPag.oEFRATER6_1_33.mCond()
    this.oPgFrm.Page1.oPag.oEFANNER6_1_34.enabled = this.oPgFrm.Page1.oPag.oEFANNER6_1_34.mCond()
    this.oPgFrm.Page1.oPag.oEFIMDER6_1_35.enabled = this.oPgFrm.Page1.oPag.oEFIMDER6_1_35.mCond()
    this.oPgFrm.Page1.oPag.oEFIMCER6_1_36.enabled = this.oPgFrm.Page1.oPag.oEFIMCER6_1_36.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=EFTRIER1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER1))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER1)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER1) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER1_1_3'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER1)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER1 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EFTRIER2
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER2))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER2)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER2) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER2_1_8'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER2)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER2 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EFTRIER3
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER3))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER3)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER3) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER3_1_13'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER3)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER3 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EFTRIER4
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER4))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER4)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER4) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER4_1_18'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER4)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER4 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER4 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EFTRIER5
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER5)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER5))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER5)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER5) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER5_1_27'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER5)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER5 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER5 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EFTRIER6
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EFTRIER6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_EFTRIER6)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_EFTRIER6))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EFTRIER6)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EFTRIER6) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oEFTRIER6_1_32'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EFTRIER6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_EFTRIER6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_EFTRIER6)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EFTRIER6 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EFTRIER6 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EFTRIER6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oEFTRIER1_1_3.value==this.w_EFTRIER1)
      this.oPgFrm.Page1.oPag.oEFTRIER1_1_3.value=this.w_EFTRIER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER1_1_4.value==this.w_EFRATER1)
      this.oPgFrm.Page1.oPag.oEFRATER1_1_4.value=this.w_EFRATER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER1_1_5.value==this.w_EFANNER1)
      this.oPgFrm.Page1.oPag.oEFANNER1_1_5.value=this.w_EFANNER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER1_1_6.value==this.w_EFIMDER1)
      this.oPgFrm.Page1.oPag.oEFIMDER1_1_6.value=this.w_EFIMDER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER1_1_7.value==this.w_EFIMCER1)
      this.oPgFrm.Page1.oPag.oEFIMCER1_1_7.value=this.w_EFIMCER1
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTRIER2_1_8.value==this.w_EFTRIER2)
      this.oPgFrm.Page1.oPag.oEFTRIER2_1_8.value=this.w_EFTRIER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER2_1_9.value==this.w_EFRATER2)
      this.oPgFrm.Page1.oPag.oEFRATER2_1_9.value=this.w_EFRATER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER2_1_10.value==this.w_EFANNER2)
      this.oPgFrm.Page1.oPag.oEFANNER2_1_10.value=this.w_EFANNER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER2_1_11.value==this.w_EFIMDER2)
      this.oPgFrm.Page1.oPag.oEFIMDER2_1_11.value=this.w_EFIMDER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER2_1_12.value==this.w_EFIMCER2)
      this.oPgFrm.Page1.oPag.oEFIMCER2_1_12.value=this.w_EFIMCER2
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTRIER3_1_13.value==this.w_EFTRIER3)
      this.oPgFrm.Page1.oPag.oEFTRIER3_1_13.value=this.w_EFTRIER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER3_1_14.value==this.w_EFRATER3)
      this.oPgFrm.Page1.oPag.oEFRATER3_1_14.value=this.w_EFRATER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER3_1_15.value==this.w_EFANNER3)
      this.oPgFrm.Page1.oPag.oEFANNER3_1_15.value=this.w_EFANNER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER3_1_16.value==this.w_EFIMDER3)
      this.oPgFrm.Page1.oPag.oEFIMDER3_1_16.value=this.w_EFIMDER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER3_1_17.value==this.w_EFIMCER3)
      this.oPgFrm.Page1.oPag.oEFIMCER3_1_17.value=this.w_EFIMCER3
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTRIER4_1_18.value==this.w_EFTRIER4)
      this.oPgFrm.Page1.oPag.oEFTRIER4_1_18.value=this.w_EFTRIER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER4_1_19.value==this.w_EFRATER4)
      this.oPgFrm.Page1.oPag.oEFRATER4_1_19.value=this.w_EFRATER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER4_1_20.value==this.w_EFANNER4)
      this.oPgFrm.Page1.oPag.oEFANNER4_1_20.value=this.w_EFANNER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER4_1_21.value==this.w_EFIMDER4)
      this.oPgFrm.Page1.oPag.oEFIMDER4_1_21.value=this.w_EFIMDER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER4_1_22.value==this.w_EFIMCER4)
      this.oPgFrm.Page1.oPag.oEFIMCER4_1_22.value=this.w_EFIMCER4
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTRIER5_1_27.value==this.w_EFTRIER5)
      this.oPgFrm.Page1.oPag.oEFTRIER5_1_27.value=this.w_EFTRIER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER5_1_28.value==this.w_EFRATER5)
      this.oPgFrm.Page1.oPag.oEFRATER5_1_28.value=this.w_EFRATER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER5_1_29.value==this.w_EFANNER5)
      this.oPgFrm.Page1.oPag.oEFANNER5_1_29.value=this.w_EFANNER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER5_1_30.value==this.w_EFIMDER5)
      this.oPgFrm.Page1.oPag.oEFIMDER5_1_30.value=this.w_EFIMDER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER5_1_31.value==this.w_EFIMCER5)
      this.oPgFrm.Page1.oPag.oEFIMCER5_1_31.value=this.w_EFIMCER5
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTRIER6_1_32.value==this.w_EFTRIER6)
      this.oPgFrm.Page1.oPag.oEFTRIER6_1_32.value=this.w_EFTRIER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFRATER6_1_33.value==this.w_EFRATER6)
      this.oPgFrm.Page1.oPag.oEFRATER6_1_33.value=this.w_EFRATER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFANNER6_1_34.value==this.w_EFANNER6)
      this.oPgFrm.Page1.oPag.oEFANNER6_1_34.value=this.w_EFANNER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMDER6_1_35.value==this.w_EFIMDER6)
      this.oPgFrm.Page1.oPag.oEFIMDER6_1_35.value=this.w_EFIMDER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFIMCER6_1_36.value==this.w_EFIMCER6)
      this.oPgFrm.Page1.oPag.oEFIMCER6_1_36.value=this.w_EFIMCER6
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTOTDER_1_37.value==this.w_EFTOTDER)
      this.oPgFrm.Page1.oPag.oEFTOTDER_1_37.value=this.w_EFTOTDER
    endif
    if not(this.oPgFrm.Page1.oPag.oEFTOTCER_1_38.value==this.w_EFTOTCER)
      this.oPgFrm.Page1.oPag.oEFTOTCER_1_38.value=this.w_EFTOTCER
    endif
    if not(this.oPgFrm.Page1.oPag.oEFSALDER_1_39.value==this.w_EFSALDER)
      this.oPgFrm.Page1.oPag.oEFSALDER_1_39.value=this.w_EFSALDER
    endif
    cp_SetControlsValueExtFlds(this,'MODEPAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(not empty(.w_EFANNER1) and (val(.w_EFANNER1)>=1996  and val(.w_EFANNER1)<=2050 or val(.w_EFANNER1)=0))  and (!empty(.w_EFTRIER1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(not empty(.w_EFANNER2) and (val(.w_EFANNER2)>=1996  and val(.w_EFANNER2)<=2050  or val(.w_EFANNER2)=0))  and (!empty(.w_EFTRIER2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER2_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(not empty(.w_EFANNER3) and (val(.w_EFANNER3)>=1996  and val(.w_EFANNER3)<=2050  or val(.w_EFANNER3)=0))  and (!empty(.w_EFTRIER3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER3_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(not empty(.w_EFANNER4) and (val(.w_EFANNER4)>=1996  and val(.w_EFANNER4)<=2050  or val(.w_EFANNER4)=0))  and (!empty(.w_EFTRIER4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER4_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(not empty(.w_EFANNER5) and (val(.w_EFANNER5)>=1996  and val(.w_EFANNER5)<=2050  or val(.w_EFANNER5)=0))  and (!empty(.w_EFTRIER5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER5_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(not empty(.w_EFANNER6) and (val(.w_EFANNER6)>=1996  and val(.w_EFANNER6)<=2050  or val(.w_EFANNER6)=0))  and (!empty(.w_EFTRIER6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFANNER6_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_EFTRIER1 = this.w_EFTRIER1
    this.o_EFTRIER2 = this.w_EFTRIER2
    this.o_EFTRIER3 = this.w_EFTRIER3
    this.o_EFTRIER4 = this.w_EFTRIER4
    this.o_EFTRIER5 = this.w_EFTRIER5
    this.o_EFTRIER6 = this.w_EFTRIER6
    return

enddefine

* --- Define pages as container
define class tgscg_afqPag1 as StdContainer
  Width  = 710
  height = 290
  stdWidth  = 710
  stdheight = 290
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oEFTRIER1_1_3 as StdField with uid="GSXFTYQRTX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_EFTRIER1", cQueryName = "EFTRIER1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651337,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=17, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER1"

  func oEFTRIER1_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER1_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER1_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER1_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER1_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER1
     i_obj.ecpSave()
  endproc

  add object oEFRATER1_1_4 as StdField with uid="KQMBCQJQBN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_EFRATER1", cQueryName = "EFRATER1",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/regione/provincia",;
    HelpContextID = 151239305,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=85, Top=84, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER1_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER1))
    endwith
   endif
  endfunc

  add object oEFANNER1_1_5 as StdField with uid="SQVPJIBPBZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_EFANNER1", cQueryName = "EFANNER1",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748425,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=184, Top=84, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER1_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER1))
    endwith
   endif
  endfunc

  func oEFANNER1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER1) and (val(.w_EFANNER1)>=1996  and val(.w_EFANNER1)<=2050 or val(.w_EFANNER1)=0))
    endwith
    return bRes
  endfunc

  add object oEFIMDER1_1_6 as StdField with uid="SQAUCACRJC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_EFIMDER1", cQueryName = "EFIMDER1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266953,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=260, Top=84, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER1_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER1))
    endwith
   endif
  endfunc

  add object oEFIMCER1_1_7 as StdField with uid="XRKAAUKORU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_EFIMCER1", cQueryName = "EFIMCER1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315529,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=438, Top=84, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER1_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER1))
    endwith
   endif
  endfunc

  add object oEFTRIER2_1_8 as StdField with uid="GMMDAWOXYY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_EFTRIER2", cQueryName = "EFTRIER2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651336,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=17, Top=112, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER2"

  func oEFTRIER2_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER2_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER2_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER2_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER2_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER2
     i_obj.ecpSave()
  endproc

  add object oEFRATER2_1_9 as StdField with uid="BILLKRJQQT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_EFRATER2", cQueryName = "EFRATER2",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/regione/provincia",;
    HelpContextID = 151239304,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=85, Top=112, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER2_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER2))
    endwith
   endif
  endfunc

  add object oEFANNER2_1_10 as StdField with uid="LQHGQKKJZS",rtseq=10,rtrep=.f.,;
    cFormVar = "w_EFANNER2", cQueryName = "EFANNER2",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748424,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=184, Top=112, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER2_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER2))
    endwith
   endif
  endfunc

  func oEFANNER2_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER2) and (val(.w_EFANNER2)>=1996  and val(.w_EFANNER2)<=2050  or val(.w_EFANNER2)=0))
    endwith
    return bRes
  endfunc

  add object oEFIMDER2_1_11 as StdField with uid="NMSDTZDSVA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_EFIMDER2", cQueryName = "EFIMDER2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266952,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=260, Top=112, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER2_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER2))
    endwith
   endif
  endfunc

  add object oEFIMCER2_1_12 as StdField with uid="SQOCUCZTRB",rtseq=12,rtrep=.f.,;
    cFormVar = "w_EFIMCER2", cQueryName = "EFIMCER2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315528,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=438, Top=112, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER2_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER2))
    endwith
   endif
  endfunc

  add object oEFTRIER3_1_13 as StdField with uid="MFZMLAQVCU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_EFTRIER3", cQueryName = "EFTRIER3",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651335,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=17, Top=140, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER3"

  func oEFTRIER3_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER3_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER3_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER3_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER3_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER3
     i_obj.ecpSave()
  endproc

  add object oEFRATER3_1_14 as StdField with uid="QWIXKXZATJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_EFRATER3", cQueryName = "EFRATER3",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/regione/provincia",;
    HelpContextID = 151239303,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=85, Top=140, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER3_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER3))
    endwith
   endif
  endfunc

  add object oEFANNER3_1_15 as StdField with uid="WERPQWHZHU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_EFANNER3", cQueryName = "EFANNER3",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748423,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=184, Top=140, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER3_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER3))
    endwith
   endif
  endfunc

  func oEFANNER3_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER3) and (val(.w_EFANNER3)>=1996  and val(.w_EFANNER3)<=2050  or val(.w_EFANNER3)=0))
    endwith
    return bRes
  endfunc

  add object oEFIMDER3_1_16 as StdField with uid="EVIACYTGOE",rtseq=16,rtrep=.f.,;
    cFormVar = "w_EFIMDER3", cQueryName = "EFIMDER3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266951,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=260, Top=140, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER3_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER3))
    endwith
   endif
  endfunc

  add object oEFIMCER3_1_17 as StdField with uid="KYWEOQHJWB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_EFIMCER3", cQueryName = "EFIMCER3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315527,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=438, Top=140, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER3_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER3))
    endwith
   endif
  endfunc

  add object oEFTRIER4_1_18 as StdField with uid="FHTKXGXMTV",rtseq=18,rtrep=.f.,;
    cFormVar = "w_EFTRIER4", cQueryName = "EFTRIER4",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651334,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=17, Top=168, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER4"

  func oEFTRIER4_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER4_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER4_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER4_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER4_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER4
     i_obj.ecpSave()
  endproc

  add object oEFRATER4_1_19 as StdField with uid="MHDCGUZYUG",rtseq=19,rtrep=.f.,;
    cFormVar = "w_EFRATER4", cQueryName = "EFRATER4",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/regione/provincia",;
    HelpContextID = 151239302,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=85, Top=168, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER4_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER4))
    endwith
   endif
  endfunc

  add object oEFANNER4_1_20 as StdField with uid="LLSDYYHTVX",rtseq=20,rtrep=.f.,;
    cFormVar = "w_EFANNER4", cQueryName = "EFANNER4",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748422,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=184, Top=168, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER4_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER4))
    endwith
   endif
  endfunc

  func oEFANNER4_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER4) and (val(.w_EFANNER4)>=1996  and val(.w_EFANNER4)<=2050  or val(.w_EFANNER4)=0))
    endwith
    return bRes
  endfunc

  add object oEFIMDER4_1_21 as StdField with uid="UYXLSVSCJI",rtseq=21,rtrep=.f.,;
    cFormVar = "w_EFIMDER4", cQueryName = "EFIMDER4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266950,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=260, Top=168, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER4_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER4))
    endwith
   endif
  endfunc

  add object oEFIMCER4_1_22 as StdField with uid="VGSQFEWENW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_EFIMCER4", cQueryName = "EFIMCER4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315526,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=438, Top=168, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER4_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER4))
    endwith
   endif
  endfunc

  add object oEFTRIER5_1_27 as StdField with uid="YGEOOYBGAA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_EFTRIER5", cQueryName = "EFTRIER5",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651333,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=17, Top=196, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER5"

  func oEFTRIER5_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER5_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER5_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER5_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER5_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER5
     i_obj.ecpSave()
  endproc

  add object oEFRATER5_1_28 as StdField with uid="XPFNCWVBBM",rtseq=24,rtrep=.f.,;
    cFormVar = "w_EFRATER5", cQueryName = "EFRATER5",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/regione/provincia",;
    HelpContextID = 151239301,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=85, Top=196, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER5_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER5))
    endwith
   endif
  endfunc

  add object oEFANNER5_1_29 as StdField with uid="VYFYZQIRTB",rtseq=25,rtrep=.f.,;
    cFormVar = "w_EFANNER5", cQueryName = "EFANNER5",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748421,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=184, Top=196, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER5_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER5))
    endwith
   endif
  endfunc

  func oEFANNER5_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER5) and (val(.w_EFANNER5)>=1996  and val(.w_EFANNER5)<=2050  or val(.w_EFANNER5)=0))
    endwith
    return bRes
  endfunc

  add object oEFIMDER5_1_30 as StdField with uid="VBSWBTOJXK",rtseq=26,rtrep=.f.,;
    cFormVar = "w_EFIMDER5", cQueryName = "EFIMDER5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266949,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=260, Top=196, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER5_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER5))
    endwith
   endif
  endfunc

  add object oEFIMCER5_1_31 as StdField with uid="ZGLAFDKFUK",rtseq=27,rtrep=.f.,;
    cFormVar = "w_EFIMCER5", cQueryName = "EFIMCER5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315525,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=438, Top=196, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER5_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER5))
    endwith
   endif
  endfunc

  add object oEFTRIER6_1_32 as StdField with uid="ZKHBXXIESY",rtseq=28,rtrep=.f.,;
    cFormVar = "w_EFTRIER6", cQueryName = "EFTRIER6",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 161651332,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=17, Top=224, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_EFTRIER6"

  func oEFTRIER6_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oEFTRIER6_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEFTRIER6_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oEFTRIER6_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG_ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oEFTRIER6_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_EFTRIER6
     i_obj.ecpSave()
  endproc

  add object oEFRATER6_1_33 as StdField with uid="SVKBMSIANH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_EFRATER6", cQueryName = "EFRATER6",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rateazione/regione/provincia",;
    HelpContextID = 151239300,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=85, Top=224, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  func oEFRATER6_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER6))
    endwith
   endif
  endfunc

  add object oEFANNER6_1_34 as StdField with uid="DGWOOFFUSO",rtseq=30,rtrep=.f.,;
    cFormVar = "w_EFANNER6", cQueryName = "EFANNER6",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione erario: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 156748420,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=184, Top=224, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oEFANNER6_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_EFTRIER6))
    endwith
   endif
  endfunc

  func oEFANNER6_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_EFANNER6) and (val(.w_EFANNER6)>=1996  and val(.w_EFANNER6)<=2050  or val(.w_EFANNER6)=0))
    endwith
    return bRes
  endfunc

  add object oEFIMDER6_1_35 as StdField with uid="OBZSCVYYPC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_EFIMDER6", cQueryName = "EFIMDER6",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167266948,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=260, Top=224, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMDER6_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER6))
    endwith
   endif
  endfunc

  add object oEFIMCER6_1_36 as StdField with uid="VIFTXJXKHF",rtseq=32,rtrep=.f.,;
    cFormVar = "w_EFIMCER6", cQueryName = "EFIMCER6",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168315524,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=438, Top=224, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oEFIMCER6_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_EFTRIER6))
    endwith
   endif
  endfunc

  add object oEFTOTDER_1_37 as StdField with uid="INJBGREDMF",rtseq=33,rtrep=.f.,;
    cFormVar = "w_EFTOTDER", cQueryName = "EFTOTDER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 101344664,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=260, Top=258, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oEFTOTCER_1_38 as StdField with uid="YQLREAFJDS",rtseq=34,rtrep=.f.,;
    cFormVar = "w_EFTOTCER", cQueryName = "EFTOTCER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 84567448,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=438, Top=259, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oEFSALDER_1_39 as StdField with uid="GUHTOPIQNX",rtseq=35,rtrep=.f.,;
    cFormVar = "w_EFSALDER", cQueryName = "EFSALDER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 92034456,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=572, Top=260, cSayPict="v_PV(78)", cGetPict="v_GV(78)"


  add object oObj_1_45 as cp_calclbl with uid="GKGLWDZFVF",left=184, top=45, width=64,height=35,;
    caption='Anno di riferimento',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 209753973


  add object oObj_1_46 as cp_calclbl with uid="ZUOHGMEAFA",left=17, top=45, width=52,height=35,;
    caption='Cod. tributo',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 52513199

  add object oStr_1_23 as StdString with uid="SLXROAECXG",Visible=.t., Left=85, Top=42,;
    Alignment=0, Width=92, Height=18,;
    Caption="Rateazione\"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="RFJHOYEAWG",Visible=.t., Left=85, Top=58,;
    Alignment=0, Width=96, Height=18,;
    Caption="Regione\Prov."  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="SDTAHTJKXO",Visible=.t., Left=260, Top=61,;
    Alignment=0, Width=172, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="QOYRTPTDQN",Visible=.t., Left=438, Top=61,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="URWTKLQWUH",Visible=.t., Left=162, Top=261,;
    Alignment=1, Width=94, Height=15,;
    Caption="Totale A"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="KKRETUTZTW",Visible=.t., Left=572, Top=239,;
    Alignment=2, Width=132, Height=15,;
    Caption="Saldo (A-B)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="MIBHACUXWQ",Visible=.t., Left=420, Top=261,;
    Alignment=0, Width=15, Height=15,;
    Caption="B"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_44 as StdString with uid="QECVEKZDVA",Visible=.t., Left=5, Top=12,;
    Alignment=0, Width=128, Height=15,;
    Caption="Sezione erario"  ;
  , bGlobalFont=.t.

  add object oBox_1_43 as StdBox with uid="SKPDLHLIGC",left=3, top=29, width=702,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_afq','MODEPAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".EFSERIAL=MODEPAG.EFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
