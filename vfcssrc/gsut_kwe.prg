* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kwe                                                        *
*              Ad hoc su web                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-21                                                      *
* Last revis.: 2012-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kwe",oParentObject))

* --- Class definition
define class tgsut_kwe as StdForm
  Top    = 15
  Left   = 33

  * --- Standard Properties
  Width  = 384
  Height = 157
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-08"
  HelpContextID=32064361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=0

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kwe"
  cComment = "Ad hoc su web"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kwePag1","gsut_kwe",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsut_kwe
    * -- valorizzo il titolo della maschera
    this.parent.cComment=i_MSGTITLE+' '+cp_Translate('su Web')
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate(iif( isAhe(), "bmp\ahe.ico" , iif( isApa(), "bmp\ahpa.ico" , iif( isAhr(), "bmp\ahr.ico" ,  "bmp\alt.ico" )   ) ))
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate("bmp\ico2_postalite_g2.ico")
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(iif( isAhe(), "bmp\ahe.ico" , iif( isApa(), "bmp\ahpa.ico" , iif( isAhr(), "bmp\ahr.ico" ,  "bmp\alt.ico" )   ) ))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate("bmp\ico2_postalite_g2.ico")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(iif( isAhe(), "bmp\ahe.ico" , iif( isApa(), "bmp\ahpa.ico" , iif( isAhr(), "bmp\ahr.ico" ,  "bmp\alt.ico" )   ) ))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate("bmp\ico2_postalite_g2.ico")
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_2.visible=!this.oPgFrm.Page1.oPag.oStr_1_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kwePag1 as StdContainer
  Width  = 380
  height = 157
  stdWidth  = 380
  stdheight = 157
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="JWIBHYOYEX",left=324, top=103, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 24746938;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_5 as cp_showimage with uid="MLRUQMZIHQ",left=14, top=81, width=33,height=31,;
    caption='Object',;
   bGlobalFont=.t.,;
    stretch=1,default="BMP\ZINFO.BMP",BackStyle=0,ImgSize=32,;
    nPag=1;
    , HelpContextID = 145933286


  add object oObj_1_6 as cp_showimage with uid="UPQJKNWPSM",left=14, top=9, width=33,height=31,;
    caption='Object',;
   bGlobalFont=.t.,;
    stretch=1,default="bmp\super.bmp",ImgSize=32,;
    nPag=1;
    , HelpContextID = 145933286


  add object oObj_1_7 as cp_showimage with uid="DBKMLNWUFC",left=14, top=45, width=33,height=31,;
    caption='Object',;
   bGlobalFont=.t.,;
    stretch=1,default="",BackStyle=0,ImgSize=32,;
    nPag=1;
    , HelpContextID = 145933286


  add object oObj_1_11 as cp_showimage with uid="LJQIRQVPTI",left=14, top=117, width=33,height=31,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="",stretch=1,BackStyle=0,ImgSize=32,;
    nPag=1;
    , HelpContextID = 145933286

  add object oStr_1_2 as WebLabel with uid="FBRUGGXDKR",Visible=.t., Left=68, Top=16,;
    Alignment=0, Width=309, Height=18,;
    Caption="Supermercato dell'informazione"    , cLink='http://adhocrevolution.supermercato.it';
  ;
  , bGlobalFont=.t.

  func oStr_1_2.mHide()
    with this.Parent.oContained
      return (!IsAhr())
    endwith
  endfunc

  add object oStr_1_3 as WebLabel with uid="BIPMGPENXW",Visible=.t., Left=68, Top=52,;
    Alignment=0, Width=309, Height=18,;
    Caption="Zucchetti SpA Home Page (sede di Aulla)"    , cLink='http://www.zucchettitam.it';
  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as WebLabel with uid="VDBUXVYTTS",Visible=.t., Left=68, Top=88,;
    Alignment=0, Width=246, Height=18,;
    Caption="Zucchetti SpA Home Page"    , cLink='http://www.zucchetti.it/';
  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as WebLabel with uid="ZWVQAKPBZB",Visible=.t., Left=68, Top=16,;
    Alignment=0, Width=309, Height=18,;
    Caption="Supermercato dell'informazione"    , cLink='http://adhocenterprise.supermercato.it';
  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (!IsAhe())
    endwith
  endfunc

  add object oStr_1_9 as WebLabel with uid="DGWNBIZYXS",Visible=.t., Left=68, Top=16,;
    Alignment=0, Width=309, Height=18,;
    Caption="Supermercato dell'informazione"    , cLink='http://alterego.supermercato.it';
  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="LRKYIXCKVQ",Visible=.t., Left=0, Top=180,;
    Alignment=0, Width=828, Height=18,;
    Caption="Attenzione label sovrapposte per differenza sito da aprire (Impossibile usare funzioni IsXXX() poich� inserito nella creazione dell'oggetto darebbe errore."  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_12 as WebLabel with uid="KRFYIAOTLU",Visible=.t., Left=66, Top=124,;
    Alignment=0, Width=117, Height=18,;
    Caption="Postalite"    , cLink='https://www.postalite.it/Home.aspx?txtIdApplication=adhocrevolution';
  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (!IsAhr())
    endwith
  endfunc

  add object oStr_1_13 as WebLabel with uid="QWWSSYQVGO",Visible=.t., Left=66, Top=124,;
    Alignment=0, Width=117, Height=18,;
    Caption="Postalite"    , cLink='https://www.postalite.it/Home.aspx?txtIdApplication=adhocenterprise';
  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (!IsAhe())
    endwith
  endfunc

  add object oStr_1_14 as WebLabel with uid="HFJMMKRJGE",Visible=.t., Left=66, Top=124,;
    Alignment=0, Width=117, Height=18,;
    Caption="Postalite"    , cLink='https://www.postalite.it/Home.aspx?txtIdApplication=alteregotop';
  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kwe','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kwe
Define Class WebLabel As StdString
   * Puntatore mouse ditino
   MousePointer=15
   PROCEDURE init()
     DODEFAULT()
     * Colorato di azzurro
     this.ForeColor=Rgb(0,0,255)
     this.ToolTipText = this.cLink
   Endproc

   * all'entrata del mouse mostro la sottolineatura
   PROCEDURE MouseEnter
     LPARAMETERS nButton, nShift, nXCoord, nYCoord
     This.FontUnderline=.t.
   ENDPROC

   * all'uscita del mouse nascondo la sottolineatura
   PROCEDURE MouseLeave
     LPARAMETERS nButton, nShift, nXCoord, nYCoord
     This.FontUnderline=.f.
   Endproc

   Procedure Click
     * lancio hyper link...
     gsar_bsf(this.clink,'','')
   Endproc

Enddefine
* --- Fine Area Manuale
