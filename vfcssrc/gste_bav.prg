* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bav                                                        *
*              Legge partite della distinta                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_166]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-01                                                      *
* Last revis.: 2015-05-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bav",oParentObject,m.pParam)
return(i_retval)

define class tgste_bav as StdBatch
  * --- Local variables
  pParam = space(1)
  w_GSTE_ADI = .NULL.
  w_GSTE_MPD = .NULL.
  w_CHIAVE = space(100)
  w_MESS = space(40)
  w_MESS1 = space(40)
  w_OK = .f.
  w_TEST = .f.
  w_TIPDIS = space(1)
  w_ROWNUM = 0
  w_DATVAL = ctod("  /  /  ")
  w_OBJCTRL = .NULL.
  w_OBJ = .NULL.
  w_RIFIND = space(10)
  w_OBJ = .NULL.
  w_CONTROL = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue la lettura delle Partite/Scadenze in Distinta (Lanciato da GSTE_ADI,Gste_Mpd) durante l'evento Load
    * --- T:  Aggiorna Totali
    *     C:  Aggiorna variabile Colore per colorazione righe
    *     S: Separa righe partite
    *     R: Raggruppa righe partite
    *     D: Aggiorna la data valuta sulle partite con la data valuta di testata
    *     V: Aggiorna la Data valuta sulle righe che faranno parte dello stesso raggruppamento
    *     M: Aggiorna PTDATREG con w_DIDATDIS
    *     A: Aggiorno la data valuta sulle partite con la data valuta della distinta
    *     E: Aggiorno la data valuta sulle partite dello stesso Raggruppamento
    *     F: Bottone Elimina..
    *     Z: 
    *     P: Aggiorno la Combo al cambio del tipo distinta
    *     W: Aggiorno la descrizione CBI sulle partite
    this.w_GSTE_ADI = this.oParentObject
    this.w_GSTE_MPD = this.oParentObject
    do case
      case this.pParam="C" OR this.pParam="V"
        this.w_TEST = .T.
        this.w_TIPDIS = this.w_GSTE_MPD.w_TIPDIS
        * --- Cambio sfondo alle scadenze
        this.w_GSTE_MPD.Markpos()     
         
 SELECT DTOC(CP_TODATE(t_PTDATSCA))+NVL(t_PTTIPCON," ")+; 
 NVL(t_PTCODCON,SPACE(15))+NVL(t_PTMODPAG,SPACE(10))+; 
 IIF(this.w_TIPDIS $ "BO-SC",NVL(t_PTNUMCOR,Space(25)),"")+; 
 IIF(this.oParentObject.w_FLBANC="A", NVL(t_PTBANAPP,SPACE(10)), SPACE(10))+; 
 IIF(this.oParentObject.w_FLBANC="S", NVL(t_PTBANNOS,SPACE(15)), SPACE(15))+NVL(t_PTCODRAG,Space(10)) AS CHIAVE,; 
 COUNT(CPROWNUM) as NUMPAR,MAX(t_PTDATVAL) as PTDATVAL,MIN(CPROWNUM) AS COLORE FROM (this.w_GSTE_MPD.ctrsname); 
 Where Not Empty(t_FLRAGG); 
 Group BY CHIAVE; 
 Having NUMPAR > 1; 
 order by CHIAVE; 
 INTO CURSOR TEMP NoFilter 
 SELECT (this.w_GSTE_MPD.ctrsname) 
 Go top 
 Scan For Not Empty(Nvl(t_FLRAGG," ")) 
 this.w_CHIAVE=DTOC(CP_TODATE(t_PTDATSCA))+NVL(t_PTTIPCON," ")+; 
 NVL(t_PTCODCON,SPACE(15))+NVL(t_PTMODPAG,SPACE(10))+; 
 IIF(this.w_TIPDIS $ "BO-SC",NVL(t_PTNUMCOR,Space(25)),"")+; 
 IIF(this.oParentObject.w_FLBANC="A", NVL(t_PTBANAPP,SPACE(10)), SPACE(10))+; 
 IIF(this.oParentObject.w_FLBANC="S", NVL(t_PTBANNOS,SPACE(15)), SPACE(15))+NVL(t_PTCODRAG,Space(10)) 
 SELECT Temp 
 Locate for this.w_CHIAVE=Temp.CHIAVE
        if this.pParam="C"
          if Found()
            * --- Colore sar� utilizzato come indice per sceglire colore nell'array ARCOL[]
             
 Punt=RECNO() 
 SELECT (this.w_GSTE_MPD.ctrsname) 
 Replace t_COLORE with Punt 
 Replace t_TEXCOL with IIF(t_COLORE>0,iif(mod(Nvl(t_COLORE,0),15)=0,This.oParentobject.ARCOL[15],This.oParentobject.ARCOL[Mod(t_COLORE,15)]),0) 
 Replace t_PTCODRAG with SPACE(10)
          else
            SELECT (this.w_GSTE_MPD.ctrsname) 
 Replace t_COLORE with 0
          endif
        else
          if Found()
             
 SELECT (this.w_GSTE_MPD.ctrsname) 
 Replace t_PTDATVAL with temp.PTDATVAL 
 Replace i_srv with "U"
            this.w_TEST = .F.
          endif
        endif
        ENDSCAN
        if NOT this.w_TEST
          ah_ErrorMsg("Aggiornate altre partite collegate a seguito di flag raggruppa scadenze nel cliente",,"")
        endif
        this.w_GSTE_MPD.Repos()     
        if Used("TEMP")
           
 SELECT TEMP 
 USE
        endif
      case this.pParam="S" or this.pParam="R" 
        this.w_GSTE_MPD.Markpos()     
        if this.pParam="R" 
          this.oParentObject.w_PTCODRAG = SPACE(10)
           
 SELECT (this.w_GSTE_MPD.ctrsname) 
 Replace t_PTCODRAG with SPACE(10)
        else
          this.oParentObject.w_PTCODRAG = SYS(2015)
          SELECT (this.w_GSTE_MPD.ctrsname) 
 Replace t_PTCODRAG with Sys(2015) 
 Replace t_COLORE with 0
        endif
        * --- Verifico la presenza di altre partite con stesse caratteristiche
        *     se unica provvedo a omologarla alla corrente (Separata\Raggruppata)
        this.w_CHIAVE = DTOC(CP_TODATE(t_PTDATSCA))+NVL(t_PTTIPCON," ")+NVL(t_PTCODCON,SPACE(15))+NVL(t_PTMODPAG,SPACE(10))+IIF(this.w_TIPDIS $ "BO-SC",NVL(t_PTNUMCOR,Space(25)),"")
        this.w_CHIAVE = this.w_CHIAVE+IIF(this.oParentObject.w_FLBANC="A", NVL(t_PTBANAPP,SPACE(10)), SPACE(10))
        this.w_CHIAVE = this.w_CHIAVE+IIF(this.oParentObject.w_FLBANC="S", NVL(t_PTBANNOS,SPACE(15)), SPACE(15))
         
 Select CPROWNUM FROM (this.w_GSTE_MPD.ctrsname) ; 
 Where DTOC(CP_TODATE(t_PTDATSCA))+NVL(t_PTTIPCON,"")+; 
 NVL(t_PTCODCON,SPACE(15))+NVL(t_PTMODPAG,SPACE(10))+; 
 IIF(this.w_TIPDIS $ "BO-SC",NVL(t_PTNUMCOR,Space(25)),"")+; 
 IIF(this.oParentObject.w_FLBANC="A", NVL(t_PTBANAPP,SPACE(10)), SPACE(10))+; 
 IIF(this.oParentObject.w_FLBANC="S", NVL(t_PTBANNOS,SPACE(15)), SPACE(15)) = this.w_CHIAVE And; 
 CPROWNUM<>this.oParentObject.w_CPROWNUM and IIF(this.pParam="R" ,Not Empty(t_PTCODRAG),Empty(t_PTCODRAG)); 
 into cursor TEMP
        if Reccoun("TEMP")=1
          this.w_ROWNUM = Nvl(Temp.CPROWNUM,0)
           
 SELECT (this.w_GSTE_MPD.ctrsname) 
 Locate For this.w_ROWNUM=CPROWNUM
          if Found()
            if this.pParam="R" 
              Replace t_PTCODRAG with Space(10) 
 Replace i_SRV with "U"
            else
              Replace t_PTCODRAG with Sys(2015) 
 Replace t_COLORE with 0 
 Replace i_SRV with "U"
            endif
          endif
        endif
        this.w_GSTE_MPD.Repos()     
        if Used("TEMP")
           
 SELECT TEMP 
 USE
        endif
        this.w_GSTE_MPD.Notifyevent("Colora")     
      case this.pParam="D" OR this.pParam="M" OR this.pParam="A" OR this.pParam="B"
        * --- Aggiorno la data valuta sulle partite con la data valuta della distinta
        if this.pParam="B"
          if this.oparentobject.cfunction="Edit" and ALLTRIM(this.oParentObject.w_DICAUBON)$"48000-27000"
            this.w_OK = ah_YesNo("La causale di pagamento � stata cambiata.%0Si desidera sbiancare la dicitura CBI nel bottone dettagli?")
          endif
        else
          this.w_OK = ah_YesNo("Si vuole cambiare la data valuta presente sulle righe?")
        endif
        this.w_GSTE_MPD = this.oParentObject.GSTE_MPD
        this.w_GSTE_MPD.bUpdated = .T.
        if this.w_OK
          this.w_GSTE_MPD.Markpos()     
           
 SELECT (this.w_GSTE_MPD.ctrsname) 
 Go Top
          do case
            case this.pParam="M"
               
 Replace t_PTDATREG with this.oParentObject.w_DIDATDIS, i_srv with iif(i_srv="A", "A", "U") for not deleted() 
 Replace t_PTDATVAL with this.oParentObject.w_DIDATDIS, i_srv with iif(i_srv="A", "A", "U") for not deleted()
            case this.pParam="D"
               
 Replace t_PTDATVAL with this.oParentObject.w_DIDATCON, i_srv with iif(i_srv="A", "A", "U") for not deleted()
            case this.pParam="A"
               
 Replace t_PTDATVAL with this.oParentObject.w_DIDATVAL, i_srv with iif(i_srv="A", "A", "U") for not deleted()
            case this.pParam="B"
               
 Replace t_PTDESRIG with " ", i_srv with iif(i_srv="A", "A", "U") for not deleted()
          endcase
          this.w_GSTE_MPD.Repos()     
          this.w_GSTE_MPD.Repos()     
        endif
      case this.pParam="E" 
        * --- Aggiorno la data valuta sulle partite dello stesso Raggruppamento
        this.w_GSTE_MPD = this.oParentObject.GSTE_MPD
        this.w_GSTE_MPD.SaveRow()     
        this.w_GSTE_MPD.bUpdated = .T.
        this.w_DATVAL = this.w_GSTE_MPD.w_PTDATVAL
        this.w_GSTE_MPD.Markpos()     
         
 SELECT (this.w_GSTE_MPD.ctrsname)
        this.w_CHIAVE = DTOC(CP_TODATE(t_PTDATSCA))+NVL(t_PTTIPCON," ")+NVL(t_PTCODCON,SPACE(15))+NVL(t_PTMODPAG,SPACE(10))+IIF(this.w_TIPDIS $ "BO-SC",NVL(t_PTNUMCOR,Space(25)),"")
        this.w_CHIAVE = this.w_CHIAVE+IIF(this.oParentObject.w_FLBANC="A", NVL(t_PTBANAPP,SPACE(10)), SPACE(10))
        this.w_CHIAVE = this.w_CHIAVE+IIF(this.oParentObject.w_FLBANC="S", NVL(t_PTBANNOS,SPACE(15)), SPACE(15)) +NVL(t_PTCODRAG,Space(10))
         
 SELECT (this.w_GSTE_MPD.ctrsname) 
 Go Top 
 Replace t_PTDATVAL with this.w_DATVAL, i_srv with iif(i_srv="A", "A", "U") for not deleted() AND; 
 this.w_CHIAVE=DTOC(CP_TODATE(t_PTDATSCA))+NVL(t_PTTIPCON," ")+; 
 NVL(t_PTCODCON,SPACE(15))+NVL(t_PTMODPAG,SPACE(10))+; 
 IIF(this.w_TIPDIS $ "BO-SC",NVL(t_PTNUMCOR,Space(25)),"")+; 
 IIF(this.oParentObject.w_FLBANC="A", NVL(t_PTBANAPP,SPACE(10)), SPACE(10))+; 
 IIF(this.oParentObject.w_FLBANC="S", NVL(t_PTBANNOS,SPACE(15)), SPACE(15))+NVL(t_PTCODRAG,Space(10))
        this.w_GSTE_MPD.Repos()     
        this.w_GSTE_MPD.Repos()     
      case this.pParam="F" 
        this.w_OBJCTRL = this.w_GSTE_MPD.GetBodyCtrl( "w_RIGA" )
        this.w_OBJCTRL.Setfocus()     
        this.w_GSTE_MPD.ecpF6()     
      case this.pParam="Z" and this.oparentobject.cFunction<>"Load"
        * --- Select from gste_bav
        do vq_exec with 'gste_bav',this,'_Curs_gste_bav','',.f.,.t.
        if used('_Curs_gste_bav')
          select _Curs_gste_bav
          locate for 1=1
          do while not(eof())
          this.w_RIFIND = _Curs_gste_bav.PTRIFIND
          if EMPTY(nvl(this.w_RIFIND,""))
            this.oParentObject.w_CONTAB = "N"
          else
            this.oParentObject.w_CONTAB = "S"
          endif
            select _Curs_gste_bav
            continue
          enddo
          use
        endif
        this.w_OBJ = this.w_GSTE_ADI.getctrl("QBJIAEIARX")
        this.w_OBJ.Visible = Not this.w_OBJ.mHide()
      case this.pParam="P"
        * --- Aggiorno la Combo al cambio del tipo distinta
        this.w_CONTROL = This.oParentObject.GetCtrl("w_DICAUBON")
        this.oParentObject.w_CATIPDIS = this.oParentObject.w_DITIPDIS
        this.w_CONTROL.Popola()     
        * --- Rilancio la query per sapere se ho un valore per la causale
        this.oParentObject.w_TEST1 = this.w_CONTROL.LISTCOUNT>0
    endcase
    * --- --
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_gste_bav')
      use in _Curs_gste_bav
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
