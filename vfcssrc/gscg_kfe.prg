* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kfe                                                        *
*              Visualizzazione fatture IVA ad esigibilit� differita            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-31                                                      *
* Last revis.: 2018-10-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kfe",oParentObject))

* --- Class definition
define class tgscg_kfe as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 772
  Height = 497
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-10-03"
  HelpContextID=48899433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gscg_kfe"
  cComment = "Visualizzazione fatture IVA ad esigibilit� differita"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLSALD = space(1)
  o_FLSALD = space(1)
  w_STORNO = space(1)
  w_TIPCON = space(1)
  w_CODICE = space(15)
  w_DESCRI = space(40)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_SERIALE = space(10)
  w_SERMOV = space(10)
  w_FLIVAD = space(1)
  w_VALUTA = space(5)
  w_FLSALD1 = space(10)
  w_DATINC1 = ctod('  /  /  ')
  o_DATINC1 = ctod('  /  /  ')
  w_DATINC2 = ctod('  /  /  ')
  o_DATINC2 = ctod('  /  /  ')
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kfePag1","gscg_kfe",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLSALD_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ESERCIZI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLSALD=space(1)
      .w_STORNO=space(1)
      .w_TIPCON=space(1)
      .w_CODICE=space(15)
      .w_DESCRI=space(40)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_SERIALE=space(10)
      .w_SERMOV=space(10)
      .w_FLIVAD=space(1)
      .w_VALUTA=space(5)
      .w_FLSALD1=space(10)
      .w_DATINC1=ctod("  /  /  ")
      .w_DATINC2=ctod("  /  /  ")
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
        .w_FLSALD = 'R'
        .w_STORNO = 'T'
      .oPgFrm.Page1.oPag.Zoom.Calculate()
        .w_TIPCON = 'C'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODICE))
          .link_1_6('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_DATINI = CTOD(RIGHT('00'+STR(DAY(I_DATSYS)),2)+'-'+RIGHT('00'+STR(MONTH(i_DATSYS)),2)+'-'+STR((YEAR(I_DATSYS)-1)))
        .w_DATFIN = i_DATSYS
        .w_OBTEST = I_DATSYS
        .w_SERIALE = .w_Zoom.getVar('PNSERIAL')
        .w_SERMOV = .w_Zoom.getVar('DPSERMOV')
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
          .DoRTCalc(11,11,.f.)
        .w_VALUTA = .w_Zoom.getVar('PNCODVAL')
        .w_FLSALD1 = .w_FLSALD
          .DoRTCalc(14,15,.f.)
        .w_DATA1 = IIF(EMPTY(NVL(.w_DATINC1,CP_CHARTODATE('  -  -  '))),CP_CHARTODATE('01-01-1900'),.w_DATINC1)
        .w_DATA2 = IIF(EMPTY(NVL(.w_DATINC2,CP_CHARTODATE('  -  -  '))),CP_CHARTODATE('31-12-3000'),.w_DATINC2)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .DoRTCalc(1,8,.t.)
            .w_SERIALE = .w_Zoom.getVar('PNSERIAL')
            .w_SERMOV = .w_Zoom.getVar('DPSERMOV')
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .DoRTCalc(11,11,.t.)
            .w_VALUTA = .w_Zoom.getVar('PNCODVAL')
        if .o_FLSALD<>.w_FLSALD
            .w_FLSALD1 = .w_FLSALD
        endif
        .DoRTCalc(14,15,.t.)
        if .o_DATINC1<>.w_DATINC1
            .w_DATA1 = IIF(EMPTY(NVL(.w_DATINC1,CP_CHARTODATE('  -  -  '))),CP_CHARTODATE('01-01-1900'),.w_DATINC1)
        endif
        if .o_DATINC2<>.w_DATINC2
            .w_DATA2 = IIF(EMPTY(NVL(.w_DATINC2,CP_CHARTODATE('  -  -  '))),CP_CHARTODATE('31-12-3000'),.w_DATINC2)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
  return

  proc Calculate_SATNIHJYAT()
    with this
          * --- Modifico query zoom
          .w_Zoom.ccpqueryname = IIF('SOLL' $ UPPER(i_cModules) ,"QUERY\GSCG_KFE_12", "QUERY\GSCG_KFE_13")
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_18.visible=!this.oPgFrm.Page1.oPag.oBtn_1_18.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_19.visible=!this.oPgFrm.Page1.oPag.oBtn_1_19.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_SATNIHJYAT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANFLIVAD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODICE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANFLIVAD;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANFLIVAD";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANFLIVAD;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODICE_1_6'),i_cWhere,'GSAR_BZC',"Elenco",'GSCG_KFE.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANFLIVAD";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANFLIVAD;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANFLIVAD";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANFLIVAD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANFLIVAD";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODICE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANFLIVAD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_FLIVAD = NVL(_Link_.ANFLIVAD,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_FLIVAD = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPCON='G' AND NVL(.w_FLIVAD,'N')='S') OR .w_TIPCON<>'G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODICE = space(15)
        this.w_DESCRI = space(40)
        this.w_FLIVAD = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLSALD_1_1.RadioValue()==this.w_FLSALD)
      this.oPgFrm.Page1.oPag.oFLSALD_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTORNO_1_2.RadioValue()==this.w_STORNO)
      this.oPgFrm.Page1.oPag.oSTORNO_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_5.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_6.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_6.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_8.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_8.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_11.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_11.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_12.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_12.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINC1_1_29.value==this.w_DATINC1)
      this.oPgFrm.Page1.oPag.oDATINC1_1_29.value=this.w_DATINC1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINC2_1_30.value==this.w_DATINC2)
      this.oPgFrm.Page1.oPag.oDATINC2_1_30.value=this.w_DATINC2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_TIPCON='G' AND NVL(.w_FLIVAD,'N')='S') OR .w_TIPCON<>'G')  and not(empty(.w_CODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not((.w_DATINI<=.w_DATFIN) or (empty(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale o precedente all'inizio dell'esercizio")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLSALD = this.w_FLSALD
    this.o_DATINC1 = this.w_DATINC1
    this.o_DATINC2 = this.w_DATINC2
    return

enddefine

* --- Define pages as container
define class tgscg_kfePag1 as StdContainer
  Width  = 768
  height = 497
  stdWidth  = 768
  stdheight = 497
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLSALD_1_1 as StdCombo with uid="RZKVWQRZLP",rtseq=1,rtrep=.f.,left=407,top=70,width=119,height=21;
    , ToolTipText = "Ricerca le scadenze saldate, non ancora saldate o tutte";
    , HelpContextID = 102521686;
    , cFormVar="w_FLSALD",RowSource=""+"Tutte,"+"Chiuse,"+"Aperte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSALD_1_1.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oFLSALD_1_1.GetRadio()
    this.Parent.oContained.w_FLSALD = this.RadioValue()
    return .t.
  endfunc

  func oFLSALD_1_1.SetRadio()
    this.Parent.oContained.w_FLSALD=trim(this.Parent.oContained.w_FLSALD)
    this.value = ;
      iif(this.Parent.oContained.w_FLSALD=='T',1,;
      iif(this.Parent.oContained.w_FLSALD=='S',2,;
      iif(this.Parent.oContained.w_FLSALD=='R',3,;
      0)))
  endfunc


  add object oSTORNO_1_2 as StdCombo with uid="WFQOVVXZUM",rtseq=2,rtrep=.f.,left=652,top=70,width=104,height=21;
    , HelpContextID = 21832742;
    , cFormVar="w_STORNO",RowSource=""+"Generato,"+"Non generato,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTORNO_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSTORNO_1_2.GetRadio()
    this.Parent.oContained.w_STORNO = this.RadioValue()
    return .t.
  endfunc

  func oSTORNO_1_2.SetRadio()
    this.Parent.oContained.w_STORNO=trim(this.Parent.oContained.w_STORNO)
    this.value = ;
      iif(this.Parent.oContained.w_STORNO=='S',1,;
      iif(this.Parent.oContained.w_STORNO=='N',2,;
      iif(this.Parent.oContained.w_STORNO=='T',3,;
      0)))
  endfunc


  add object Zoom as cp_zoombox with uid="NZJKJVBMWF",left=7, top=98, width=755,height=322,;
    caption='Object',;
   bGlobalFont=.t.,;
    bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",cTable="PNT_MAST",bRetriveAllRows=.f.,cZoomFile="GSCG_KFE_1",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 129098214


  add object oBtn_1_4 as StdButton with uid="ZVARLVJFTI",left=714, top=446, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41582010;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTIPCON_1_5 as StdCombo with uid="TKQYZYBPVV",rtseq=3,rtrep=.f.,left=84,top=10,width=80,height=21;
    , ToolTipText = "Tipo di conto selezionato";
    , HelpContextID = 5122358;
    , cFormVar="w_TIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    ' ')))
  endfunc
  func oTIPCON_1_5.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_5.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      0))
  endfunc

  func oTIPCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODICE)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODICE_1_6 as StdField with uid="ZZFIMKSUCD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Conto selezionato",;
    HelpContextID = 110325286,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=172, Top=10, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODICE"

  func oCODICE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODICE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco",'GSCG_KFE.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODICE_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_8 as StdField with uid="YISYJVIDXY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 192828470,;
   bGlobalFont=.t.,;
    Height=21, Width=348, Left=308, Top=10, InputMask=replicate('X',40)

  add object oDATINI_1_11 as StdField with uid="VPJLNVRTYN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale o precedente all'inizio dell'esercizio",;
    ToolTipText = "Data iniziale di registrazione delle fatture/note di credito",;
    HelpContextID = 189030454,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=84, Top=37

  func oDATINI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINI<=.w_DATFIN) or (empty(.w_DATFIN)))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_12 as StdField with uid="MCOUDCLKFB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data finale di registrazione delle fatture/note di credito",;
    HelpContextID = 267477046,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=231, Top=37

  func oDATFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATINI))
    endwith
    return bRes
  endfunc


  add object oBtn_1_15 as StdButton with uid="ISKAWSSZTY",left=663, top=10, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 128022806;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      this.parent.oContained.NotifyEvent("Calcola")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DATINI) AND NOT EMPTY(.w_DATFIN))
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="OUEOXFACNW",left=6, top=446, width=48,height=45,;
    CpPicture="bmp\TESO.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione di prima nota";
    , HelpContextID = 97119734;
    , Caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_SERIALE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_COGE='S' AND NOT EMPTY(NVL(.w_SERIALE,' ')))
      endwith
    endif
  endfunc

  func oBtn_1_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="CHNMSDVAYY",left=57, top=446, width=48,height=45,;
    CpPicture="bmp\SoldiT.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione di prima nota";
    , HelpContextID = 57623590;
    , Caption='\<Storno';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      do GSCG_KFE_1 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_COGE='S' AND NOT EMPTY(NVL(.w_SERMOV,' ')))
      endwith
    endif
  endfunc

  func oBtn_1_19.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc


  add object oObj_1_21 as cp_outputCombo with uid="ZLERXIVPWT",left=230, top=465, width=419,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 129098214


  add object oBtn_1_22 as StdButton with uid="VCZMCHUPGB",left=714, top=10, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 92890150;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_DATINI))
      endwith
    endif
  endfunc

  add object oDATINC1_1_29 as StdField with uid="EOBLZRINZM",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATINC1", cQueryName = "DATINC1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale di incassi/pagamenti ",;
    HelpContextID = 88367158,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=407, Top=37

  add object oDATINC2_1_30 as StdField with uid="BZAMDZLZBS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATINC2", cQueryName = "DATINC2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale di incassi/pagamenti ",;
    HelpContextID = 88367158,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=580, Top=37

  add object oStr_1_7 as StdString with uid="VMDITUXAKY",Visible=.t., Left=41, Top=10,;
    Alignment=1, Width=40, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="CUIDVCGGWS",Visible=.t., Left=361, Top=70,;
    Alignment=1, Width=44, Height=18,;
    Caption="Partite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="SAKZLGPUNF",Visible=.t., Left=529, Top=70,;
    Alignment=1, Width=120, Height=18,;
    Caption="Movimento di storno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="EHYDVQMOWB",Visible=.t., Left=10, Top=37,;
    Alignment=1, Width=71, Height=18,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="ORMUKDZOZP",Visible=.t., Left=162, Top=37,;
    Alignment=1, Width=67, Height=18,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="OWTYVVWXNA",Visible=.t., Left=114, Top=465,;
    Alignment=1, Width=113, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="WSHZFRXYSI",Visible=.t., Left=8, Top=424,;
    Alignment=0, Width=192, Height=17,;
    Caption="Fattura seguita da insoluto"    , BackStyle=1, BackColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_31 as StdString with uid="VIDPUGOGYF",Visible=.t., Left=301, Top=37,;
    Alignment=1, Width=104, Height=18,;
    Caption="Da data inc./pag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="OQSXYCOLJQ",Visible=.t., Left=485, Top=37,;
    Alignment=1, Width=94, Height=18,;
    Caption="A data inc./pag.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kfe','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
