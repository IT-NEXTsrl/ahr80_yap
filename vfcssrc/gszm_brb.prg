* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_brb                                                        *
*              Zoom dettaglio voci                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_10]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-13                                                      *
* Last revis.: 2005-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_brb",oParentObject)
return(i_retval)

define class tgszm_brb as StdBatch
  * --- Local variables
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_OBJECT = .NULL.
  w_PADRE = .NULL.
  w_AZIONE = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per avvio funzionalit� legate ai CONTI/MASTRI, da men� contestuale
    *     Lanciato da GSCG_KRB (ELENCO VOCI - BASILEA) 
    this.w_PADRE = IIF(Type("g_omenu.okey")<>"U",g_omenu,This)
    this.w_AZIONE = IIF(Type("g_omenu.okey")<>"U",g_oMenu.cBatchType,"A")
    this.w_ANTIPCON = this.w_padre.oParentobject.w_DVFLMACO
    this.w_ANCODICE = this.w_padre.oParentobject.w_CODICE
    if this.w_ANTIPCON="G"
      this.w_OBJECT = "GSAR_API"
      OpenGest(this.w_AZIONE,this.w_OBJECT,"ANTIPCON", this.w_ANTIPCON, "ANCODICE",this.w_ANCODICE)
    else
      this.w_OBJECT = "GSAR_AMC"
      OpenGest(this.w_AZIONE,this.w_OBJECT, "MCCODICE",this.w_ANCODICE)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
