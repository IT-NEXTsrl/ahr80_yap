* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_cca                                                        *
*              check sui periodi di ripartizione                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-03-28                                                      *
* Last revis.: 2013-04-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_cca",oParentObject)
return(i_retval)

define class tgsca_cca as StdBatch
  * --- Local variables
  w_NRECORD = 0
  w_MESSAGE = space(100)
  * --- WorkFile variables
  MOVIRIPA_idx=0
  MOVICOST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla se per la ripartizione che stiamo modificando sia gi� presente un movimento e in caso affermativo chiede se si intende cancellarla
    this.w_MESSAGE = "Attenzione, questo periodo ha generato movimenti di ripartizione, si vuole procedere alla loro cancellazione?"
    if (this.oParentObject.w_CADATINI<>this.oParentObject.w_CAOLDINI OR this.oParentObject.w_CADATFIN<>this.oParentObject.w_CAOLDFIN)
      * --- Select from MOVIRIPA
      i_nConn=i_TableProp[this.MOVIRIPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT(*) AS TOT  from "+i_cTable+" MOVIRIPA ";
            +" where MRPERIOD="+cp_ToStrODBC(this.oParentObject.w_CAPERIOD)+"";
             ,"_Curs_MOVIRIPA")
      else
        select COUNT(*) AS TOT from (i_cTable);
         where MRPERIOD=this.oParentObject.w_CAPERIOD;
          into cursor _Curs_MOVIRIPA
      endif
      if used('_Curs_MOVIRIPA')
        select _Curs_MOVIRIPA
        locate for 1=1
        do while not(eof())
        this.w_NRECORD = TOT
          select _Curs_MOVIRIPA
          continue
        enddo
        use
      endif
      if this.w_NRECORD<>0 AND ah_yesno(this.w_MESSAGE,"?",.T.)
        * --- Aggiornamento del flag MRFLRIPA
        * --- Select from query\gsca_cca
        do vq_exec with 'query\gsca_cca',this,'_Curs_query_gsca_cca','',.f.,.t.
        if used('_Curs_query_gsca_cca')
          select _Curs_query_gsca_cca
          locate for 1=1
          do while not(eof())
          if this.oParentObject.w_CAPERIOD=PERIODO AND NMOV=1
            * --- Write into MOVICOST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MOVICOST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVICOST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MRFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'MOVICOST','MRFLRIPA');
                  +i_ccchkf ;
              +" where ";
                  +"MRSERIAL = "+cp_ToStrODBC(SERIALE);
                  +" and MRROWORD = "+cp_ToStrODBC(ORDRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(NUMRIF);
                     )
            else
              update (i_cTable) set;
                  MRFLRIPA = " ";
                  &i_ccchkf. ;
               where;
                  MRSERIAL = SERIALE;
                  and MRROWORD = ORDRIF;
                  and CPROWNUM = NUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
            select _Curs_query_gsca_cca
            continue
          enddo
          use
        endif
        * --- Delete from MOVIRIPA
        i_nConn=i_TableProp[this.MOVIRIPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MRPERIOD = "+cp_ToStrODBC(this.oParentObject.w_CAPERIOD);
                 )
        else
          delete from (i_cTable) where;
                MRPERIOD = this.oParentObject.w_CAPERIOD;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        cp_msg(cp_msgformat("cancellati %1 movimenti",ALLTRIM(STR(this.w_NRECORD))))
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOVIRIPA'
    this.cWorkTables[2]='MOVICOST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_MOVIRIPA')
      use in _Curs_MOVIRIPA
    endif
    if used('_Curs_query_gsca_cca')
      use in _Curs_query_gsca_cca
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
