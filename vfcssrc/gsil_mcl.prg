* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsil_mcl                                                        *
*              Categorie listini                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_147]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-07                                                      *
* Last revis.: 2015-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsil_mcl"))

* --- Class definition
define class tgsil_mcl as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 700
  Height = 327+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-08"
  HelpContextID=171323543
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CATMLIST_IDX = 0
  CATELIST_IDX = 0
  STRUTABE_IDX = 0
  XDC_FIELDS_IDX = 0
  XDC_TABLE_IDX = 0
  VALUTE_IDX = 0
  cFile = "CATMLIST"
  cFileDetail = "CATELIST"
  cKeySelect = "CTCODCAT"
  cKeyWhere  = "CTCODCAT=this.w_CTCODCAT"
  cKeyDetail  = "CTCODCAT=this.w_CTCODCAT"
  cKeyWhereODBC = '"CTCODCAT="+cp_ToStrODBC(this.w_CTCODCAT)';

  cKeyDetailWhereODBC = '"CTCODCAT="+cp_ToStrODBC(this.w_CTCODCAT)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CATELIST.CTCODCAT="+cp_ToStrODBC(this.w_CTCODCAT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CATELIST.CTPOSINI,CATELIST.CTPOSFIN'
  cPrg = "gsil_mcl"
  cComment = "Categorie listini"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CTCODCAT = space(15)
  w_CTDESCAT = space(30)
  w_CTDIMREC = 0
  w_CTPOSINI = 0
  w_CTPOSFIN = 0
  w_CTCODTAB = space(8)
  w_CTCODCAM = space(8)
  w_CTFLLIST = space(1)
  w_CTNUMDEC = 0
  w_CTDESLIS = space(30)
  w_CTCODISO = space(3)
  w_CTFLGIVA = space(1)
  w_CTFLRIGT = space(1)
  o_CTFLRIGT = space(1)
  w_CTCODISO = space(3)
  w_CTNRRIGT = 0
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CATMLIST','gsil_mcl')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsil_mclPag1","gsil_mcl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Categorie listini")
      .Pages(1).HelpContextID = 72170531
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCTCODCAT_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='STRUTABE'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='XDC_TABLE'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='CATMLIST'
    this.cWorkTables[6]='CATELIST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CATMLIST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CATMLIST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CTCODCAT = NVL(CTCODCAT,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CATMLIST where CTCODCAT=KeySet.CTCODCAT
    *
    i_nConn = i_TableProp[this.CATMLIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2],this.bLoadRecFilter,this.CATMLIST_IDX,"gsil_mcl")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CATMLIST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CATMLIST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CATELIST.","CATMLIST.")
      i_cTable = i_cTable+' CATMLIST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CTCODCAT',this.w_CTCODCAT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CTCODCAT = NVL(CTCODCAT,space(15))
        .w_CTDESCAT = NVL(CTDESCAT,space(30))
        .w_CTDIMREC = NVL(CTDIMREC,0)
        .w_CTFLRIGT = NVL(CTFLRIGT,space(1))
        .w_CTNRRIGT = NVL(CTNRRIGT,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        cp_LoadRecExtFlds(this,'CATMLIST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CATELIST where CTCODCAT=KeySet.CTCODCAT
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CATELIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CATELIST_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CATELIST')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CATELIST.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CATELIST"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CTCODCAT',this.w_CTCODCAT  )
        select * from (i_cTable) CATELIST where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CTPOSINI = NVL(CTPOSINI,0)
          .w_CTPOSFIN = NVL(CTPOSFIN,0)
          .w_CTCODTAB = NVL(CTCODTAB,space(8))
          * evitabile
          *.link_2_3('Load')
          .w_CTCODCAM = NVL(CTCODCAM,space(8))
          * evitabile
          *.link_2_4('Load')
          .w_CTFLLIST = NVL(CTFLLIST,space(1))
          .w_CTNUMDEC = NVL(CTNUMDEC,0)
          .w_CTDESLIS = NVL(CTDESLIS,space(30))
          .w_CTCODISO = NVL(CTCODISO,space(3))
          * evitabile
          *.link_2_8('Load')
          .w_CTFLGIVA = NVL(CTFLGIVA,space(1))
          .w_CTCODISO = NVL(CTCODISO,space(3))
          * evitabile
          *.link_2_10('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CTCODCAT=space(15)
      .w_CTDESCAT=space(30)
      .w_CTDIMREC=0
      .w_CTPOSINI=0
      .w_CTPOSFIN=0
      .w_CTCODTAB=space(8)
      .w_CTCODCAM=space(8)
      .w_CTFLLIST=space(1)
      .w_CTNUMDEC=0
      .w_CTDESLIS=space(30)
      .w_CTCODISO=space(3)
      .w_CTFLGIVA=space(1)
      .w_CTFLRIGT=space(1)
      .w_CTCODISO=space(3)
      .w_CTNRRIGT=0
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_CTDIMREC = 0
        .DoRTCalc(4,6,.f.)
        if not(empty(.w_CTCODTAB))
         .link_2_3('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CTCODCAM))
         .link_2_4('Full')
        endif
        .DoRTCalc(8,11,.f.)
        if not(empty(.w_CTCODISO))
         .link_2_8('Full')
        endif
        .DoRTCalc(12,12,.f.)
        .w_CTFLRIGT = 'N'
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CTCODISO))
         .link_2_10('Full')
        endif
        .w_CTNRRIGT = IIF(.w_CTFLRIGT='S',IIF(.w_CTNRRIGT=0,1,.w_CTNRRIGT),0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CATMLIST')
    this.DoRTCalc(16,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCTCODCAT_1_2.enabled = i_bVal
      .Page1.oPag.oCTDESCAT_1_4.enabled = i_bVal
      .Page1.oPag.oCTDIMREC_1_6.enabled = i_bVal
      .Page1.oPag.oCTFLRIGT_1_11.enabled = i_bVal
      .Page1.oPag.oCTNRRIGT_1_12.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCTCODCAT_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCTCODCAT_1_2.enabled = .t.
        .Page1.oPag.oCTDESCAT_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CATMLIST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CATMLIST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CTCODCAT,"CTCODCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CTDESCAT,"CTDESCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CTDIMREC,"CTDIMREC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CTFLRIGT,"CTFLRIGT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CTNRRIGT,"CTNRRIGT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CATMLIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])
    i_lTable = "CATMLIST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CATMLIST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsil_scl with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CTPOSINI N(3);
      ,t_CTPOSFIN N(3);
      ,t_CTCODTAB C(8);
      ,t_CTCODCAM C(8);
      ,t_CTFLLIST N(3);
      ,t_CTNUMDEC N(1);
      ,t_CTDESLIS C(30);
      ,t_CTCODISO C(3);
      ,t_CTFLGIVA N(3);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsil_mclbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSINI_2_1.controlsource=this.cTrsName+'.t_CTPOSINI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSFIN_2_2.controlsource=this.cTrsName+'.t_CTPOSFIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODTAB_2_3.controlsource=this.cTrsName+'.t_CTCODTAB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODCAM_2_4.controlsource=this.cTrsName+'.t_CTCODCAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLLIST_2_5.controlsource=this.cTrsName+'.t_CTFLLIST'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCTNUMDEC_2_6.controlsource=this.cTrsName+'.t_CTNUMDEC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCTDESLIS_2_7.controlsource=this.cTrsName+'.t_CTDESLIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_8.controlsource=this.cTrsName+'.t_CTCODISO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLGIVA_2_9.controlsource=this.cTrsName+'.t_CTFLGIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_10.controlsource=this.cTrsName+'.t_CTCODISO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(52)
    this.AddVLine(91)
    this.AddVLine(176)
    this.AddVLine(265)
    this.AddVLine(311)
    this.AddVLine(350)
    this.AddVLine(576)
    this.AddVLine(627)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSINI_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CATMLIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CATMLIST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CATMLIST')
        i_extval=cp_InsertValODBCExtFlds(this,'CATMLIST')
        local i_cFld
        i_cFld=" "+;
                  "(CTCODCAT,CTDESCAT,CTDIMREC,CTFLRIGT,CTNRRIGT"+;
                  ",UTCC,UTDC,UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CTCODCAT)+;
                    ","+cp_ToStrODBC(this.w_CTDESCAT)+;
                    ","+cp_ToStrODBC(this.w_CTDIMREC)+;
                    ","+cp_ToStrODBC(this.w_CTFLRIGT)+;
                    ","+cp_ToStrODBC(this.w_CTNRRIGT)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CATMLIST')
        i_extval=cp_InsertValVFPExtFlds(this,'CATMLIST')
        cp_CheckDeletedKey(i_cTable,0,'CTCODCAT',this.w_CTCODCAT)
        INSERT INTO (i_cTable);
              (CTCODCAT,CTDESCAT,CTDIMREC,CTFLRIGT,CTNRRIGT,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CTCODCAT;
                  ,this.w_CTDESCAT;
                  ,this.w_CTDIMREC;
                  ,this.w_CTFLRIGT;
                  ,this.w_CTNRRIGT;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CATELIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CATELIST_IDX,2])
      *
      * insert into CATELIST
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CTCODCAT,CTPOSINI,CTPOSFIN,CTCODTAB,CTCODCAM"+;
                  ",CTFLLIST,CTNUMDEC,CTDESLIS,CTCODISO,CTFLGIVA,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CTCODCAT)+","+cp_ToStrODBC(this.w_CTPOSINI)+","+cp_ToStrODBC(this.w_CTPOSFIN)+","+cp_ToStrODBCNull(this.w_CTCODTAB)+","+cp_ToStrODBCNull(this.w_CTCODCAM)+;
             ","+cp_ToStrODBC(this.w_CTFLLIST)+","+cp_ToStrODBC(this.w_CTNUMDEC)+","+cp_ToStrODBC(this.w_CTDESLIS)+","+cp_ToStrODBCNull(this.w_CTCODISO)+","+cp_ToStrODBC(this.w_CTFLGIVA)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CTCODCAT',this.w_CTCODCAT)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CTCODCAT,this.w_CTPOSINI,this.w_CTPOSFIN,this.w_CTCODTAB,this.w_CTCODCAM"+;
                ",this.w_CTFLLIST,this.w_CTNUMDEC,this.w_CTDESLIS,this.w_CTCODISO,this.w_CTFLGIVA,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CATMLIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CATMLIST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CATMLIST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CTDESCAT="+cp_ToStrODBC(this.w_CTDESCAT)+;
             ",CTDIMREC="+cp_ToStrODBC(this.w_CTDIMREC)+;
             ",CTFLRIGT="+cp_ToStrODBC(this.w_CTFLRIGT)+;
             ",CTNRRIGT="+cp_ToStrODBC(this.w_CTNRRIGT)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CATMLIST')
          i_cWhere = cp_PKFox(i_cTable  ,'CTCODCAT',this.w_CTCODCAT  )
          UPDATE (i_cTable) SET;
              CTDESCAT=this.w_CTDESCAT;
             ,CTDIMREC=this.w_CTDIMREC;
             ,CTFLRIGT=this.w_CTFLRIGT;
             ,CTNRRIGT=this.w_CTNRRIGT;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CTPOSINI>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CATELIST_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CATELIST_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CATELIST
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CATELIST
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CTPOSINI="+cp_ToStrODBC(this.w_CTPOSINI)+;
                     ",CTPOSFIN="+cp_ToStrODBC(this.w_CTPOSFIN)+;
                     ",CTCODTAB="+cp_ToStrODBCNull(this.w_CTCODTAB)+;
                     ",CTCODCAM="+cp_ToStrODBCNull(this.w_CTCODCAM)+;
                     ",CTFLLIST="+cp_ToStrODBC(this.w_CTFLLIST)+;
                     ",CTNUMDEC="+cp_ToStrODBC(this.w_CTNUMDEC)+;
                     ",CTDESLIS="+cp_ToStrODBC(this.w_CTDESLIS)+;
                     ",CTCODISO="+cp_ToStrODBCNull(this.w_CTCODISO)+;
                     ",CTFLGIVA="+cp_ToStrODBC(this.w_CTFLGIVA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CTPOSINI=this.w_CTPOSINI;
                     ,CTPOSFIN=this.w_CTPOSFIN;
                     ,CTCODTAB=this.w_CTCODTAB;
                     ,CTCODCAM=this.w_CTCODCAM;
                     ,CTFLLIST=this.w_CTFLLIST;
                     ,CTNUMDEC=this.w_CTNUMDEC;
                     ,CTDESLIS=this.w_CTDESLIS;
                     ,CTCODISO=this.w_CTCODISO;
                     ,CTFLGIVA=this.w_CTFLGIVA;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CTPOSINI>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CATELIST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CATELIST_IDX,2])
        *
        * delete CATELIST
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CATMLIST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])
        *
        * delete CATMLIST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CTPOSINI>0) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CATMLIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,14,.t.)
        if .o_CTFLRIGT<>.w_CTFLRIGT
          .w_CTNRRIGT = IIF(.w_CTFLRIGT='S',IIF(.w_CTNRRIGT=0,1,.w_CTNRRIGT),0)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCTNRRIGT_1_12.enabled = this.oPgFrm.Page1.oPag.oCTNRRIGT_1_12.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCTNUMDEC_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCTNUMDEC_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCTDESLIS_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCTDESLIS_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCTCODISO_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCTCODISO_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCTFLGIVA_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCTFLGIVA_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCTCODISO_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCTCODISO_2_10.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_8.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_8.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_10.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_10.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CTCODTAB
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STRUTABE_IDX,3]
    i_lTable = "STRUTABE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2], .t., this.STRUTABE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CTCODTAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIL_MSD',True,'STRUTABE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STNOMTAB like "+cp_ToStrODBC(trim(this.w_CTCODTAB)+"%");

          i_ret=cp_SQL(i_nConn,"select STNOMTAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STNOMTAB","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STNOMTAB',trim(this.w_CTCODTAB))
          select STNOMTAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STNOMTAB into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CTCODTAB)==trim(_Link_.STNOMTAB) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CTCODTAB) and !this.bDontReportError
            deferred_cp_zoom('STRUTABE','*','STNOMTAB',cp_AbsName(oSource.parent,'oCTCODTAB_2_3'),i_cWhere,'GSIL_MSD',"Nome tabella",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STNOMTAB";
                     +" from "+i_cTable+" "+i_lTable+" where STNOMTAB="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STNOMTAB',oSource.xKey(1))
            select STNOMTAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CTCODTAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STNOMTAB";
                   +" from "+i_cTable+" "+i_lTable+" where STNOMTAB="+cp_ToStrODBC(this.w_CTCODTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STNOMTAB',this.w_CTCODTAB)
            select STNOMTAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CTCODTAB = NVL(_Link_.STNOMTAB,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_CTCODTAB = space(8)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])+'\'+cp_ToStr(_Link_.STNOMTAB,1)
      cp_ShowWarn(i_cKey,this.STRUTABE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CTCODTAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CTCODCAM
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STRUTABE_IDX,3]
    i_lTable = "STRUTABE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2], .t., this.STRUTABE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CTCODCAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIL_MSD',True,'STRUTABE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STNOMCAM like "+cp_ToStrODBC(trim(this.w_CTCODCAM)+"%");
                   +" and STNOMTAB="+cp_ToStrODBC(this.w_CTCODTAB);

          i_ret=cp_SQL(i_nConn,"select STNOMTAB,STNOMCAM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STNOMTAB,STNOMCAM","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STNOMTAB',this.w_CTCODTAB;
                     ,'STNOMCAM',trim(this.w_CTCODCAM))
          select STNOMTAB,STNOMCAM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STNOMTAB,STNOMCAM into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CTCODCAM)==trim(_Link_.STNOMCAM) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CTCODCAM) and !this.bDontReportError
            deferred_cp_zoom('STRUTABE','*','STNOMTAB,STNOMCAM',cp_AbsName(oSource.parent,'oCTCODCAM_2_4'),i_cWhere,'GSIL_MSD',"Nome campo",'GSIL_ZNC.STRUTABE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CTCODTAB<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STNOMTAB,STNOMCAM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select STNOMTAB,STNOMCAM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Manca il nome campo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STNOMTAB,STNOMCAM";
                     +" from "+i_cTable+" "+i_lTable+" where STNOMCAM="+cp_ToStrODBC(oSource.xKey(2));
                     +" and STNOMTAB="+cp_ToStrODBC(this.w_CTCODTAB);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STNOMTAB',oSource.xKey(1);
                       ,'STNOMCAM',oSource.xKey(2))
            select STNOMTAB,STNOMCAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CTCODCAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STNOMTAB,STNOMCAM";
                   +" from "+i_cTable+" "+i_lTable+" where STNOMCAM="+cp_ToStrODBC(this.w_CTCODCAM);
                   +" and STNOMTAB="+cp_ToStrODBC(this.w_CTCODTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STNOMTAB',this.w_CTCODTAB;
                       ,'STNOMCAM',this.w_CTCODCAM)
            select STNOMTAB,STNOMCAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CTCODCAM = NVL(_Link_.STNOMCAM,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_CTCODCAM = space(8)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CTCODCAM<>'UTCC' AND .w_CTCODCAM<>'UTCV' AND .w_CTCODCAM<>'UTDC' AND .w_CTCODCAM<>'UTDV'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Manca il nome campo")
        endif
        this.w_CTCODCAM = space(8)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])+'\'+cp_ToStr(_Link_.STNOMTAB,1)+'\'+cp_ToStr(_Link_.STNOMCAM,1)
      cp_ShowWarn(i_cKey,this.STRUTABE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CTCODCAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CTCODISO
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CTCODISO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODISO like "+cp_ToStrODBC(trim(this.w_CTCODISO)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODISO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODISO',trim(this.w_CTCODISO))
          select VACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODISO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CTCODISO)==trim(_Link_.VACODISO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CTCODISO) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODISO',cp_AbsName(oSource.parent,'oCTCODISO_2_8'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODISO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODISO',oSource.xKey(1))
            select VACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CTCODISO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODISO="+cp_ToStrODBC(this.w_CTCODISO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODISO',this.w_CTCODISO)
            select VACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CTCODISO = NVL(_Link_.VACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CTCODISO = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODISO,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CTCODISO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CTCODISO
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CTCODISO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODUIC like "+cp_ToStrODBC(trim(this.w_CTCODISO)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODUIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODUIC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODUIC',trim(this.w_CTCODISO))
          select VACODUIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODUIC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CTCODISO)==trim(_Link_.VACODUIC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CTCODISO) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODUIC',cp_AbsName(oSource.parent,'oCTCODISO_2_10'),i_cWhere,'GSAR_AVL',"Valute",'GSIL_MCV.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODUIC";
                     +" from "+i_cTable+" "+i_lTable+" where VACODUIC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODUIC',oSource.xKey(1))
            select VACODUIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CTCODISO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODUIC";
                   +" from "+i_cTable+" "+i_lTable+" where VACODUIC="+cp_ToStrODBC(this.w_CTCODISO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODUIC',this.w_CTCODISO)
            select VACODUIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CTCODISO = NVL(_Link_.VACODUIC,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CTCODISO = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODUIC,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CTCODISO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCTCODCAT_1_2.value==this.w_CTCODCAT)
      this.oPgFrm.Page1.oPag.oCTCODCAT_1_2.value=this.w_CTCODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCTDESCAT_1_4.value==this.w_CTDESCAT)
      this.oPgFrm.Page1.oPag.oCTDESCAT_1_4.value=this.w_CTDESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCTDIMREC_1_6.value==this.w_CTDIMREC)
      this.oPgFrm.Page1.oPag.oCTDIMREC_1_6.value=this.w_CTDIMREC
    endif
    if not(this.oPgFrm.Page1.oPag.oCTFLRIGT_1_11.RadioValue()==this.w_CTFLRIGT)
      this.oPgFrm.Page1.oPag.oCTFLRIGT_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCTNRRIGT_1_12.value==this.w_CTNRRIGT)
      this.oPgFrm.Page1.oPag.oCTNRRIGT_1_12.value=this.w_CTNRRIGT
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSINI_2_1.value==this.w_CTPOSINI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSINI_2_1.value=this.w_CTPOSINI
      replace t_CTPOSINI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSINI_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSFIN_2_2.value==this.w_CTPOSFIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSFIN_2_2.value=this.w_CTPOSFIN
      replace t_CTPOSFIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSFIN_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODTAB_2_3.value==this.w_CTCODTAB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODTAB_2_3.value=this.w_CTCODTAB
      replace t_CTCODTAB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODTAB_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODCAM_2_4.value==this.w_CTCODCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODCAM_2_4.value=this.w_CTCODCAM
      replace t_CTCODCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODCAM_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLLIST_2_5.RadioValue()==this.w_CTFLLIST)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLLIST_2_5.SetRadio()
      replace t_CTFLLIST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLLIST_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTNUMDEC_2_6.value==this.w_CTNUMDEC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTNUMDEC_2_6.value=this.w_CTNUMDEC
      replace t_CTNUMDEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTNUMDEC_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTDESLIS_2_7.value==this.w_CTDESLIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTDESLIS_2_7.value=this.w_CTDESLIS
      replace t_CTDESLIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTDESLIS_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_8.value==this.w_CTCODISO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_8.value=this.w_CTCODISO
      replace t_CTCODISO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLGIVA_2_9.RadioValue()==this.w_CTFLGIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLGIVA_2_9.SetRadio()
      replace t_CTFLGIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLGIVA_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_10.value==this.w_CTCODISO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_10.value=this.w_CTCODISO
      replace t_CTCODISO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODISO_2_10.value
    endif
    cp_SetControlsValueExtFlds(this,'CATMLIST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CTCODCAT))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCTCODCAT_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CTCODCAT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CTDIMREC) or not(.w_CTDIMREC>0))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCTDIMREC_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CTDIMREC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La dimensione del record deve essere maggiore di 0")
          case   not(.w_CTFLRIGT='N' OR .w_CTNRRIGT>0)  and (.w_CTFLRIGT='S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCTNRRIGT_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero di righe di testata deve essere maggiore di 0")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (t_CTPOSINI>0);
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_CTPOSINI<.w_CTDIMREC AND .w_CTPOSINI>0) and (.w_CTPOSINI>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSINI_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("La posizione iniziale deve essere maggiore di 0, minore della posizione finale e della dimensione massima del record")
        case   (empty(.w_CTPOSFIN) or not(.w_CTPOSFIN>0 AND .w_CTPOSFIN<=.w_CTDIMREC AND (EMPTY(.w_CTPOSINI) OR .w_CTPOSINI<=.w_CTPOSFIN))) and (.w_CTPOSINI>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTPOSFIN_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("La posizione finale deve essere maggiore di 0, maggiore o uguale della posizione iniziale e minore della dimensione massima del record")
        case   empty(.w_CTCODTAB) and (.w_CTPOSINI>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODTAB_2_3
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Manca il nome tabella")
        case   (empty(.w_CTCODCAM) or not(.w_CTCODCAM<>'UTCC' AND .w_CTCODCAM<>'UTCV' AND .w_CTCODCAM<>'UTDC' AND .w_CTCODCAM<>'UTDV')) and (.w_CTPOSINI>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTCODCAM_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Manca il nome campo")
        case   not(.w_CTNUMDEC<6) and (.w_CTFLLIST='S') and (.w_CTPOSINI>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTNUMDEC_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il numero dei decimali del listino deve essere minore di 6")
      endcase
      if .w_CTPOSINI>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CTFLRIGT = this.w_CTFLRIGT
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CTPOSINI>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CTPOSINI=0
      .w_CTPOSFIN=0
      .w_CTCODTAB=space(8)
      .w_CTCODCAM=space(8)
      .w_CTFLLIST=space(1)
      .w_CTNUMDEC=0
      .w_CTDESLIS=space(30)
      .w_CTCODISO=space(3)
      .w_CTFLGIVA=space(1)
      .w_CTCODISO=space(3)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_CTCODTAB))
        .link_2_3('Full')
      endif
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_CTCODCAM))
        .link_2_4('Full')
      endif
      .DoRTCalc(8,11,.f.)
      if not(empty(.w_CTCODISO))
        .link_2_8('Full')
      endif
      .DoRTCalc(12,14,.f.)
      if not(empty(.w_CTCODISO))
        .link_2_10('Full')
      endif
    endwith
    this.DoRTCalc(15,19,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CTPOSINI = t_CTPOSINI
    this.w_CTPOSFIN = t_CTPOSFIN
    this.w_CTCODTAB = t_CTCODTAB
    this.w_CTCODCAM = t_CTCODCAM
    this.w_CTFLLIST = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLLIST_2_5.RadioValue(.t.)
    this.w_CTNUMDEC = t_CTNUMDEC
    this.w_CTDESLIS = t_CTDESLIS
    this.w_CTCODISO = t_CTCODISO
    this.w_CTFLGIVA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLGIVA_2_9.RadioValue(.t.)
    this.w_CTCODISO = t_CTCODISO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CTPOSINI with this.w_CTPOSINI
    replace t_CTPOSFIN with this.w_CTPOSFIN
    replace t_CTCODTAB with this.w_CTCODTAB
    replace t_CTCODCAM with this.w_CTCODCAM
    replace t_CTFLLIST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLLIST_2_5.ToRadio()
    replace t_CTNUMDEC with this.w_CTNUMDEC
    replace t_CTDESLIS with this.w_CTDESLIS
    replace t_CTCODISO with this.w_CTCODISO
    replace t_CTFLGIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCTFLGIVA_2_9.ToRadio()
    replace t_CTCODISO with this.w_CTCODISO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsil_mclPag1 as StdContainer
  Width  = 696
  height = 327
  stdWidth  = 696
  stdheight = 327
  resizeXpos=371
  resizeYpos=243
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCTCODCAT_1_2 as StdField with uid="JGZFMHHGML",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CTCODCAT", cQueryName = "CTCODCAT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria listini",;
    HelpContextID = 238438022,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=137, Top=13, InputMask=replicate('X',15)

  add object oCTDESCAT_1_4 as StdField with uid="DDYELECPLZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CTDESCAT", cQueryName = "CTDESCAT",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione categoria listini",;
    HelpContextID = 223360646,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=391, Top=13, InputMask=replicate('X',30)

  add object oCTDIMREC_1_6 as StdField with uid="EWSILBEGDO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CTDIMREC", cQueryName = "CTDIMREC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La dimensione del record deve essere maggiore di 0",;
    ToolTipText = "Dimensione del record del file di import",;
    HelpContextID = 246167191,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=137, Top=40

  func oCTDIMREC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CTDIMREC>0)
    endwith
    return bRes
  endfunc

  add object oCTFLRIGT_1_11 as StdCheck with uid="BXSNYHNCXY",rtseq=13,rtrep=.f.,left=391, top=39, caption="Riga di testata",;
    ToolTipText = "Flag listino con riga di testata (S/N, Metel='S')",;
    HelpContextID = 123278982,;
    cFormVar="w_CTFLRIGT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCTFLRIGT_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CTFLRIGT,&i_cF..t_CTFLRIGT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCTFLRIGT_1_11.GetRadio()
    this.Parent.oContained.w_CTFLRIGT = this.RadioValue()
    return .t.
  endfunc

  func oCTFLRIGT_1_11.ToRadio()
    this.Parent.oContained.w_CTFLRIGT=trim(this.Parent.oContained.w_CTFLRIGT)
    return(;
      iif(this.Parent.oContained.w_CTFLRIGT=='S',1,;
      0))
  endfunc

  func oCTFLRIGT_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCTNRRIGT_1_12 as StdField with uid="DYOJQTEPJV",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CTNRRIGT", cQueryName = "CTNRRIGT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero di righe di testata deve essere maggiore di 0",;
    ToolTipText = "Numero di righe di testata presenti nel file",;
    HelpContextID = 122852998,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=528, Top=40, cSayPict='"99"', cGetPict='"99"'

  func oCTNRRIGT_1_12.mCond()
    with this.Parent.oContained
      return (.w_CTFLRIGT='S')
    endwith
  endfunc

  func oCTNRRIGT_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CTFLRIGT='N' OR .w_CTNRRIGT>0)
    endwith
    return bRes
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=97, width=677,height=21,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=10,Field1="CTPOSINI",Label1="Da",Field2="CTPOSFIN",Label2="A",Field3="CTCODTAB",Label3="Tabella",Field4="CTCODCAM",Label4="Campo",Field5="CTFLLIST",Label5="Listino",Field6="CTNUMDEC",Label6="Dec.",Field7="CTDESLIS",Label7="Descrizione",Field8="CTCODISO",Label8="iif(Isahe(), ah_MsgFormat('UIC'), ah_MsgFormat('ISO'))",Field9="CTCODISO",Label9="",Field10="CTFLGIVA",Label10="IVA",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49456518

  add object oStr_1_1 as StdString with uid="LHBUIZSRHX",Visible=.t., Left=5, Top=13,;
    Alignment=1, Width=128, Height=18,;
    Caption="Categoria listini:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="YZQAYWAGYO",Visible=.t., Left=281, Top=13,;
    Alignment=1, Width=107, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="HGLCSPFBJZ",Visible=.t., Left=5, Top=40,;
    Alignment=1, Width=128, Height=18,;
    Caption="Dimensione record:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="FTUUREKYPZ",Visible=.t., Left=8, Top=78,;
    Alignment=2, Width=81, Height=17,;
    Caption="Origine"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="PSYKKTUSCX",Visible=.t., Left=103, Top=78,;
    Alignment=2, Width=562, Height=17,;
    Caption="Destinazione"  ;
  , bGlobalFont=.t.

  add object oBox_1_7 as StdBox with uid="HQNZGLWZAT",left=6, top=74, width=677,height=24

  add object oBox_1_8 as StdBox with uid="YLKODVIOCL",left=91, top=74, width=1,height=24

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=5,top=117,;
    width=663+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=6,top=118,width=662+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='STRUTABE|VALUTE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='STRUTABE'
        oDropInto=this.oBodyCol.oRow.oCTCODTAB_2_3
      case cFile='STRUTABE'
        oDropInto=this.oBodyCol.oRow.oCTCODCAM_2_4
      case cFile='VALUTE'
        oDropInto=this.oBodyCol.oRow.oCTCODISO_2_8
      case cFile='VALUTE'
        oDropInto=this.oBodyCol.oRow.oCTCODISO_2_10
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsil_mclBodyRow as CPBodyRowCnt
  Width=653
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCTPOSINI_2_1 as StdTrsField with uid="XHPZLKYHLE",rtseq=4,rtrep=.t.,;
    cFormVar="w_CTPOSINI",value=0,;
    ToolTipText = "Posizione iniziale",;
    HelpContextID = 146442607,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "La posizione iniziale deve essere maggiore di 0, minore della posizione finale e della dimensione massima del record",;
   bGlobalFont=.t.,;
    Height=17, Width=36, Left=-2, Top=0, cSayPict=["999"], cGetPict=["999"]

  func oCTPOSINI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CTPOSINI<.w_CTDIMREC AND .w_CTPOSINI>0)
    endwith
    return bRes
  endfunc

  add object oCTPOSFIN_2_2 as StdTrsField with uid="PISFAMESWA",rtseq=5,rtrep=.t.,;
    cFormVar="w_CTPOSFIN",value=0,;
    ToolTipText = "Posizione finale",;
    HelpContextID = 96110964,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "La posizione finale deve essere maggiore di 0, maggiore o uguale della posizione iniziale e minore della dimensione massima del record",;
   bGlobalFont=.t.,;
    Height=17, Width=36, Left=37, Top=0, cSayPict=["999"], cGetPict=["999"]

  func oCTPOSFIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CTPOSFIN>0 AND .w_CTPOSFIN<=.w_CTDIMREC AND (EMPTY(.w_CTPOSINI) OR .w_CTPOSINI<=.w_CTPOSFIN))
    endwith
    return bRes
  endfunc

  add object oCTCODTAB_2_3 as StdTrsField with uid="ODRIFJXADS",rtseq=6,rtrep=.t.,;
    cFormVar="w_CTCODTAB",value=space(8),;
    ToolTipText = "Codice tabella",;
    HelpContextID = 221660824,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Manca il nome tabella",;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=75, Top=0, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="STRUTABE", cZoomOnZoom="GSIL_MSD", oKey_1_1="STNOMTAB", oKey_1_2="this.w_CTCODTAB"

  func oCTCODTAB_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
      if .not. empty(.w_CTCODCAM)
        bRes2=.link_2_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCTCODTAB_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCTCODTAB_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STRUTABE','*','STNOMTAB',cp_AbsName(this.parent,'oCTCODTAB_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIL_MSD',"Nome tabella",'',this.parent.oContained
  endproc
  proc oCTCODTAB_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSIL_MSD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STNOMTAB=this.parent.oContained.w_CTCODTAB
    i_obj.ecpSave()
  endproc

  add object oCTCODCAM_2_4 as StdTrsField with uid="UCCVPWJFJN",rtseq=7,rtrep=.t.,;
    cFormVar="w_CTCODCAM",value=space(8),;
    ToolTipText = "Codice campo",;
    HelpContextID = 238438029,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Manca il nome campo",;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=160, Top=0, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="STRUTABE", cZoomOnZoom="GSIL_MSD", oKey_1_1="STNOMTAB", oKey_1_2="this.w_CTCODTAB", oKey_2_1="STNOMCAM", oKey_2_2="this.w_CTCODCAM"

  func oCTCODCAM_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCTCODCAM_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCTCODCAM_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.STRUTABE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"STNOMTAB="+cp_ToStrODBC(this.Parent.oContained.w_CTCODTAB)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"STNOMTAB="+cp_ToStr(this.Parent.oContained.w_CTCODTAB)
    endif
    do cp_zoom with 'STRUTABE','*','STNOMTAB,STNOMCAM',cp_AbsName(this.parent,'oCTCODCAM_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIL_MSD',"Nome campo",'GSIL_ZNC.STRUTABE_VZM',this.parent.oContained
  endproc
  proc oCTCODCAM_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSIL_MSD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.STNOMTAB=w_CTCODTAB
     i_obj.w_STNOMCAM=this.parent.oContained.w_CTCODCAM
    i_obj.ecpSave()
  endproc

  add object oCTFLLIST_2_5 as StdTrsCheck with uid="VOCWKBJDEZ",rtrep=.t.,;
    cFormVar="w_CTFLLIST",  caption="",;
    ToolTipText = "Flag listino",;
    HelpContextID = 129570438,;
    Left=254, Top=0, Width=39,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oCTFLLIST_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CTFLLIST,&i_cF..t_CTFLLIST),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCTFLLIST_2_5.GetRadio()
    this.Parent.oContained.w_CTFLLIST = this.RadioValue()
    return .t.
  endfunc

  func oCTFLLIST_2_5.ToRadio()
    this.Parent.oContained.w_CTFLLIST=trim(this.Parent.oContained.w_CTFLLIST)
    return(;
      iif(this.Parent.oContained.w_CTFLLIST=='S',1,;
      0))
  endfunc

  func oCTFLLIST_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCTNUMDEC_2_6 as StdTrsField with uid="QHIGVKSHTT",rtseq=9,rtrep=.t.,;
    cFormVar="w_CTNUMDEC",value=0,;
    ToolTipText = "Numero decimali del listino",;
    HelpContextID = 211785367,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il numero dei decimali del listino deve essere minore di 6",;
   bGlobalFont=.t.,;
    Height=17, Width=36, Left=296, Top=0, cSayPict=["9"], cGetPict=["9"]

  func oCTNUMDEC_2_6.mCond()
    with this.Parent.oContained
      return (.w_CTFLLIST='S')
    endwith
  endfunc

  func oCTNUMDEC_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CTNUMDEC<6)
    endwith
    return bRes
  endfunc

  add object oCTDESLIS_2_7 as StdTrsField with uid="JYYVPOLWMP",rtseq=10,rtrep=.t.,;
    cFormVar="w_CTDESLIS",value=space(30),;
    ToolTipText = "Descrizione",;
    HelpContextID = 196069753,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=335, Top=0, InputMask=replicate('X',30)

  func oCTDESLIS_2_7.mCond()
    with this.Parent.oContained
      return (.w_CTFLLIST='S')
    endwith
  endfunc

  add object oCTCODISO_2_8 as StdTrsField with uid="LXIZOQBUCL",rtseq=11,rtrep=.t.,;
    cFormVar="w_CTCODISO",value=space(3),;
    ToolTipText = "Codice valuta",;
    HelpContextID = 137774731,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=31, Left=564, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODISO", oKey_1_2="this.w_CTCODISO"

  func oCTCODISO_2_8.mCond()
    with this.Parent.oContained
      return (.w_CTFLLIST='S')
    endwith
  endfunc

  func oCTCODISO_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION='ad hoc ENTERPRISE')
    endwith
    endif
  endfunc

  func oCTCODISO_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCTCODISO_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCTCODISO_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODISO',cp_AbsName(this.parent,'oCTCODISO_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCTCODISO_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODISO=this.parent.oContained.w_CTCODISO
    i_obj.ecpSave()
  endproc

  add object oCTFLGIVA_2_9 as StdTrsCheck with uid="WFZINEKSXS",rtrep=.t.,;
    cFormVar="w_CTFLGIVA",  caption="",;
    ToolTipText = "Test lordo/netto",;
    HelpContextID = 134813337,;
    Left=613, Top=0, Width=35,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oCTFLGIVA_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CTFLGIVA,&i_cF..t_CTFLGIVA),this.value)
    return(iif(xVal =1,'L',;
    'N'))
  endfunc
  func oCTFLGIVA_2_9.GetRadio()
    this.Parent.oContained.w_CTFLGIVA = this.RadioValue()
    return .t.
  endfunc

  func oCTFLGIVA_2_9.ToRadio()
    this.Parent.oContained.w_CTFLGIVA=trim(this.Parent.oContained.w_CTFLGIVA)
    return(;
      iif(this.Parent.oContained.w_CTFLGIVA=='L',1,;
      0))
  endfunc

  func oCTFLGIVA_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCTFLGIVA_2_9.mCond()
    with this.Parent.oContained
      return (.w_CTFLLIST='S')
    endwith
  endfunc

  add object oCTCODISO_2_10 as StdTrsField with uid="WKGOMBJUDR",rtseq=14,rtrep=.t.,;
    cFormVar="w_CTCODISO",value=space(3),;
    ToolTipText = "Codice valuta",;
    HelpContextID = 137774731,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=560, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODUIC", oKey_1_2="this.w_CTCODISO"

  func oCTCODISO_2_10.mCond()
    with this.Parent.oContained
      return (.w_CTFLLIST='S')
    endwith
  endfunc

  func oCTCODISO_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION<>'ad hoc ENTERPRISE')
    endwith
    endif
  endfunc

  func oCTCODISO_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCTCODISO_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCTCODISO_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODUIC',cp_AbsName(this.parent,'oCTCODISO_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'GSIL_MCV.VALUTE_VZM',this.parent.oContained
  endproc
  proc oCTCODISO_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODUIC=this.parent.oContained.w_CTCODISO
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover
  * ---
  func oCTPOSINI_2_1.When()
    return(.t.)
  proc oCTPOSINI_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCTPOSINI_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsil_mcl','CATMLIST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CTCODCAT=CATMLIST.CTCODCAT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
