* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mdr                                                        *
*              Dati ritenute                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_324]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-10                                                      *
* Last revis.: 2018-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mdr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mdr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mdr")
  return

* --- Class definition
define class tgscg_mdr as StdPCForm
  Width  = 645
  Height = 475
  Top    = 6
  Left   = 3
  cComment = "Dati ritenute"
  cPrg = "gscg_mdr"
  HelpContextID=80356713
  add object cnt as tcgscg_mdr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mdr as PCContext
  w_DRSERIAL = space(10)
  w_DRCODTRI = space(5)
  w_MODIFICA = space(1)
  w_VCODTRI = space(5)
  w_DRCODCAU = space(2)
  w_DRNONSOG = 0
  w_DRIMPONI = 0
  w_DRRITENU = 0
  w_DRCODBUN = space(3)
  w_FLACON = space(1)
  w_DRTIPFOR = space(1)
  w_DRPERPRO = 0
  w_IMPINPS = 0
  w_RITINPS = 0
  w_PERINPS = 0
  w_TIPORITE = space(1)
  w_DRNONSO1 = 0
  w_DRDATREG = space(8)
  w_DRSOMESC = 0
  w_DRTOTDOC = 0
  w_DRIMPOSTA = 0
  w_DRSOGGE = 0
  w_DRFODPRO = 0
  w_TIPCLF = space(1)
  w_TOTENA = 0
  w_DRDAVERS = 0
  w_DRTOTRIT = 0
  w_DRESIDUO = 0
  w_AZIENDA = space(5)
  w_DRESIDU1 = 0
  w_DECTOP = 0
  w_DRNSOGGI = 0
  w_DRNSOGGP = 0
  w_TIPRITE = space(1)
  w_RESCHK = 0
  w_DRRIINPS = 0
  w_DRPERRIT = 0
  w_DRDATCON = space(1)
  w_DRSPERIM = 0
  w_DRCODSNS = space(1)
  w_CODSNS = space(1)
  proc Save(i_oFrom)
    this.w_DRSERIAL = i_oFrom.w_DRSERIAL
    this.w_DRCODTRI = i_oFrom.w_DRCODTRI
    this.w_MODIFICA = i_oFrom.w_MODIFICA
    this.w_VCODTRI = i_oFrom.w_VCODTRI
    this.w_DRCODCAU = i_oFrom.w_DRCODCAU
    this.w_DRNONSOG = i_oFrom.w_DRNONSOG
    this.w_DRIMPONI = i_oFrom.w_DRIMPONI
    this.w_DRRITENU = i_oFrom.w_DRRITENU
    this.w_DRCODBUN = i_oFrom.w_DRCODBUN
    this.w_FLACON = i_oFrom.w_FLACON
    this.w_DRTIPFOR = i_oFrom.w_DRTIPFOR
    this.w_DRPERPRO = i_oFrom.w_DRPERPRO
    this.w_IMPINPS = i_oFrom.w_IMPINPS
    this.w_RITINPS = i_oFrom.w_RITINPS
    this.w_PERINPS = i_oFrom.w_PERINPS
    this.w_TIPORITE = i_oFrom.w_TIPORITE
    this.w_DRNONSO1 = i_oFrom.w_DRNONSO1
    this.w_DRDATREG = i_oFrom.w_DRDATREG
    this.w_DRSOMESC = i_oFrom.w_DRSOMESC
    this.w_DRTOTDOC = i_oFrom.w_DRTOTDOC
    this.w_DRIMPOSTA = i_oFrom.w_DRIMPOSTA
    this.w_DRSOGGE = i_oFrom.w_DRSOGGE
    this.w_DRFODPRO = i_oFrom.w_DRFODPRO
    this.w_TIPCLF = i_oFrom.w_TIPCLF
    this.w_TOTENA = i_oFrom.w_TOTENA
    this.w_DRDAVERS = i_oFrom.w_DRDAVERS
    this.w_DRTOTRIT = i_oFrom.w_DRTOTRIT
    this.w_DRESIDUO = i_oFrom.w_DRESIDUO
    this.w_AZIENDA = i_oFrom.w_AZIENDA
    this.w_DRESIDU1 = i_oFrom.w_DRESIDU1
    this.w_DECTOP = i_oFrom.w_DECTOP
    this.w_DRNSOGGI = i_oFrom.w_DRNSOGGI
    this.w_DRNSOGGP = i_oFrom.w_DRNSOGGP
    this.w_TIPRITE = i_oFrom.w_TIPRITE
    this.w_RESCHK = i_oFrom.w_RESCHK
    this.w_DRRIINPS = i_oFrom.w_DRRIINPS
    this.w_DRPERRIT = i_oFrom.w_DRPERRIT
    this.w_DRDATCON = i_oFrom.w_DRDATCON
    this.w_DRSPERIM = i_oFrom.w_DRSPERIM
    this.w_DRCODSNS = i_oFrom.w_DRCODSNS
    this.w_CODSNS = i_oFrom.w_CODSNS
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DRSERIAL = this.w_DRSERIAL
    i_oTo.w_DRCODTRI = this.w_DRCODTRI
    i_oTo.w_MODIFICA = this.w_MODIFICA
    i_oTo.w_VCODTRI = this.w_VCODTRI
    i_oTo.w_DRCODCAU = this.w_DRCODCAU
    i_oTo.w_DRNONSOG = this.w_DRNONSOG
    i_oTo.w_DRIMPONI = this.w_DRIMPONI
    i_oTo.w_DRRITENU = this.w_DRRITENU
    i_oTo.w_DRCODBUN = this.w_DRCODBUN
    i_oTo.w_FLACON = this.w_FLACON
    i_oTo.w_DRTIPFOR = this.w_DRTIPFOR
    i_oTo.w_DRPERPRO = this.w_DRPERPRO
    i_oTo.w_IMPINPS = this.w_IMPINPS
    i_oTo.w_RITINPS = this.w_RITINPS
    i_oTo.w_PERINPS = this.w_PERINPS
    i_oTo.w_TIPORITE = this.w_TIPORITE
    i_oTo.w_DRNONSO1 = this.w_DRNONSO1
    i_oTo.w_DRDATREG = this.w_DRDATREG
    i_oTo.w_DRSOMESC = this.w_DRSOMESC
    i_oTo.w_DRTOTDOC = this.w_DRTOTDOC
    i_oTo.w_DRIMPOSTA = this.w_DRIMPOSTA
    i_oTo.w_DRSOGGE = this.w_DRSOGGE
    i_oTo.w_DRFODPRO = this.w_DRFODPRO
    i_oTo.w_TIPCLF = this.w_TIPCLF
    i_oTo.w_TOTENA = this.w_TOTENA
    i_oTo.w_DRDAVERS = this.w_DRDAVERS
    i_oTo.w_DRTOTRIT = this.w_DRTOTRIT
    i_oTo.w_DRESIDUO = this.w_DRESIDUO
    i_oTo.w_AZIENDA = this.w_AZIENDA
    i_oTo.w_DRESIDU1 = this.w_DRESIDU1
    i_oTo.w_DECTOP = this.w_DECTOP
    i_oTo.w_DRNSOGGI = this.w_DRNSOGGI
    i_oTo.w_DRNSOGGP = this.w_DRNSOGGP
    i_oTo.w_TIPRITE = this.w_TIPRITE
    i_oTo.w_RESCHK = this.w_RESCHK
    i_oTo.w_DRRIINPS = this.w_DRRIINPS
    i_oTo.w_DRPERRIT = this.w_DRPERRIT
    i_oTo.w_DRDATCON = this.w_DRDATCON
    i_oTo.w_DRSPERIM = this.w_DRSPERIM
    i_oTo.w_DRCODSNS = this.w_DRCODSNS
    i_oTo.w_CODSNS = this.w_CODSNS
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mdr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 645
  Height = 475
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-01-22"
  HelpContextID=80356713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DATIRITE_IDX = 0
  TRI_BUTI_IDX = 0
  CONTI_IDX = 0
  BUSIUNIT_IDX = 0
  cFile = "DATIRITE"
  cKeySelect = "DRSERIAL"
  cKeyWhere  = "DRSERIAL=this.w_DRSERIAL"
  cKeyDetail  = "DRSERIAL=this.w_DRSERIAL and DRCODCAU=this.w_DRCODCAU"
  cKeyWhereODBC = '"DRSERIAL="+cp_ToStrODBC(this.w_DRSERIAL)';

  cKeyDetailWhereODBC = '"DRSERIAL="+cp_ToStrODBC(this.w_DRSERIAL)';
      +'+" and DRCODCAU="+cp_ToStrODBC(this.w_DRCODCAU)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DATIRITE.DRSERIAL="+cp_ToStrODBC(this.w_DRSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DATIRITE.CPROWNUM '
  cPrg = "gscg_mdr"
  cComment = "Dati ritenute"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 4
  icon = "movi.ico"
  i_lastcheckrow = 0
  windowtype = 1
  minbutton = .f.
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DRSERIAL = space(10)
  w_DRCODTRI = space(5)
  o_DRCODTRI = space(5)
  w_MODIFICA = space(1)
  w_VCODTRI = space(5)
  w_DRCODCAU = space(2)
  w_DRNONSOG = 0
  w_DRIMPONI = 0
  w_DRRITENU = 0
  w_DRCODBUN = space(3)
  w_FLACON = space(1)
  w_DRTIPFOR = space(1)
  w_DRPERPRO = 0
  w_IMPINPS = 0
  w_RITINPS = 0
  w_PERINPS = 0
  w_TIPORITE = space(1)
  w_DRNONSO1 = 0
  w_DRDATREG = ctod('  /  /  ')
  w_DRSOMESC = 0
  w_DRTOTDOC = 0
  w_DRIMPOSTA = 0
  w_DRSOGGE = 0
  w_DRFODPRO = 0
  w_TIPCLF = space(1)
  w_TOTENA = 0
  w_DRDAVERS = 0
  w_DRTOTRIT = 0
  w_DRESIDUO = 0
  w_AZIENDA = space(5)
  w_DRESIDU1 = 0
  w_DECTOP = 0
  w_DRNSOGGI = 0
  w_DRNSOGGP = 0
  w_TIPRITE = space(1)
  w_RESCHK = 0
  w_DRRIINPS = 0
  w_DRPERRIT = 0
  w_DRDATCON = space(1)
  w_DRSPERIM = 0
  w_DRCODSNS = space(1)
  w_CODSNS = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mdrPag1","gscg_mdr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDRSOMESC_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='TRI_BUTI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='BUSIUNIT'
    this.cWorkTables[4]='DATIRITE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATIRITE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATIRITE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mdr'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DATIRITE where DRSERIAL=KeySet.DRSERIAL
    *                            and DRCODCAU=KeySet.DRCODCAU
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DATIRITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATIRITE_IDX,2],this.bLoadRecFilter,this.DATIRITE_IDX,"gscg_mdr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATIRITE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATIRITE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATIRITE '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DRSERIAL',this.w_DRSERIAL  )
      select * from (i_cTable) DATIRITE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DRTIPFOR = space(1)
        .w_DRPERPRO = 0
        .w_IMPINPS = 0
        .w_RITINPS = 0
        .w_PERINPS = 0
        .w_TIPORITE = space(1)
        .w_DRDATREG = This.oParentObject .w_Pndatreg
        .w_DRTOTDOC = 0
        .w_DRIMPOSTA = 0
        .w_DRSOGGE = 0
        .w_TIPCLF = space(1)
        .w_TOTENA = this.oparentobject .w_PNTOTENA
        .w_DRDAVERS = 0
        .w_DRTOTRIT = 0
        .w_DRESIDUO = 0
        .w_AZIENDA = i_CODAZI
        .w_DRESIDU1 = 0
        .w_DECTOP = This.oParentObject .w_Dectop
        .w_DRNSOGGI = 0
        .w_DRNSOGGP = 0
        .w_RESCHK = 0
        .w_CODSNS = space(1)
        .w_DRSERIAL = NVL(DRSERIAL,space(10))
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .w_DRNONSO1 = NVL(DRNONSO1,0)
        .w_DRSOMESC = NVL(DRSOMESC,0)
        .w_DRFODPRO = NVL(DRFODPRO,0)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_TIPRITE = this.oparentobject .w_RITENU
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .w_DRDATCON = NVL(DRDATCON,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DATIRITE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
        .w_MODIFICA = IIF(EMPTY(.w_DRCODTRI),'S','N')
        .w_VCODTRI = .w_DRCODTRI
          .w_FLACON = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DRCODTRI = NVL(DRCODTRI,space(5))
          if link_2_1_joined
            this.w_DRCODTRI = NVL(TRCODTRI201,NVL(this.w_DRCODTRI,space(5)))
            this.w_FLACON = NVL(TRFLACON201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_DRCODCAU = NVL(DRCODCAU,space(2))
          .w_DRNONSOG = NVL(DRNONSOG,0)
          .w_DRIMPONI = NVL(DRIMPONI,0)
          .w_DRRITENU = NVL(DRRITENU,0)
          .w_DRCODBUN = NVL(DRCODBUN,space(3))
          * evitabile
          *.link_2_8('Load')
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate()
          .w_DRRIINPS = NVL(DRRIINPS,0)
          .w_DRPERRIT = NVL(DRPERRIT,0)
          .w_DRSPERIM = NVL(DRSPERIM,0)
          .w_DRCODSNS = NVL(DRCODSNS,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_mdr
    This.Notifyevent("Varia")
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DRSERIAL=space(10)
      .w_DRCODTRI=space(5)
      .w_MODIFICA=space(1)
      .w_VCODTRI=space(5)
      .w_DRCODCAU=space(2)
      .w_DRNONSOG=0
      .w_DRIMPONI=0
      .w_DRRITENU=0
      .w_DRCODBUN=space(3)
      .w_FLACON=space(1)
      .w_DRTIPFOR=space(1)
      .w_DRPERPRO=0
      .w_IMPINPS=0
      .w_RITINPS=0
      .w_PERINPS=0
      .w_TIPORITE=space(1)
      .w_DRNONSO1=0
      .w_DRDATREG=ctod("  /  /  ")
      .w_DRSOMESC=0
      .w_DRTOTDOC=0
      .w_DRIMPOSTA=0
      .w_DRSOGGE=0
      .w_DRFODPRO=0
      .w_TIPCLF=space(1)
      .w_TOTENA=0
      .w_DRDAVERS=0
      .w_DRTOTRIT=0
      .w_DRESIDUO=0
      .w_AZIENDA=space(5)
      .w_DRESIDU1=0
      .w_DECTOP=0
      .w_DRNSOGGI=0
      .w_DRNSOGGP=0
      .w_TIPRITE=space(1)
      .w_RESCHK=0
      .w_DRRIINPS=0
      .w_DRPERRIT=0
      .w_DRDATCON=space(1)
      .w_DRSPERIM=0
      .w_DRCODSNS=space(1)
      .w_CODSNS=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_DRCODTRI))
         .link_2_1('Full')
        endif
        .w_MODIFICA = IIF(EMPTY(.w_DRCODTRI),'S','N')
        .w_VCODTRI = .w_DRCODTRI
        .DoRTCalc(5,8,.f.)
        .w_DRCODBUN = g_CODBUN
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_DRCODBUN))
         .link_2_8('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .DoRTCalc(10,17,.f.)
        .w_DRDATREG = This.oParentObject .w_Pndatreg
        .DoRTCalc(19,24,.f.)
        .w_TOTENA = this.oparentobject .w_PNTOTENA
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .DoRTCalc(26,28,.f.)
        .w_AZIENDA = i_CODAZI
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate()
        .DoRTCalc(30,30,.f.)
        .w_DECTOP = This.oParentObject .w_Dectop
        .DoRTCalc(32,33,.f.)
        .w_TIPRITE = this.oparentobject .w_RITENU
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATIRITE')
    this.DoRTCalc(36,41,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDRSOMESC_1_11.enabled = i_bVal
      .Page1.oPag.oDRFODPRO_1_24.enabled = i_bVal
      .Page1.oPag.oTOTENA_1_27.enabled = i_bVal
      .Page1.oPag.oDRRIINPS_2_13.enabled = i_bVal
      .Page1.oPag.oDRPERRIT_2_14.enabled = i_bVal
      .Page1.oPag.oDRDATCON_1_62.enabled = i_bVal
      .Page1.oPag.oDRSPERIM_2_15.enabled = i_bVal
      .Page1.oPag.oDRCODSNS_2_16.enabled = i_bVal
      .Page1.oPag.oBtn_1_53.enabled = i_bVal
      .Page1.oPag.oObj_1_2.enabled = i_bVal
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_32.enabled = i_bVal
      .Page1.oPag.oObj_1_36.enabled = i_bVal
      .Page1.oPag.oObj_1_43.enabled = i_bVal
      .Page1.oPag.oObj_1_44.enabled = i_bVal
      .Page1.oPag.oObj_2_11.enabled = i_bVal
      .Page1.oPag.oObj_1_54.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      .Page1.oPag.oObj_1_58.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DATIRITE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATIRITE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRSERIAL,"DRSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRNONSO1,"DRNONSO1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRSOMESC,"DRSOMESC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRFODPRO,"DRFODPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRDATCON,"DRDATCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DRCODTRI C(5);
      ,t_DRCODCAU N(3);
      ,t_DRNONSOG N(18,4);
      ,t_DRIMPONI N(18,4);
      ,t_DRRITENU N(18,4);
      ,t_DRRIINPS N(6,2);
      ,t_DRPERRIT N(6,2);
      ,t_DRSPERIM N(18,4);
      ,t_DRCODSNS N(3);
      ,CPROWNUM N(10);
      ,t_MODIFICA C(1);
      ,t_VCODTRI C(5);
      ,t_DRCODBUN C(3);
      ,t_FLACON C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mdrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODTRI_2_1.controlsource=this.cTrsName+'.t_DRCODTRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODCAU_2_4.controlsource=this.cTrsName+'.t_DRCODCAU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRNONSOG_2_5.controlsource=this.cTrsName+'.t_DRNONSOG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRIMPONI_2_6.controlsource=this.cTrsName+'.t_DRIMPONI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRRITENU_2_7.controlsource=this.cTrsName+'.t_DRRITENU'
    this.oPgFRm.Page1.oPag.oDRRIINPS_2_13.controlsource=this.cTrsName+'.t_DRRIINPS'
    this.oPgFRm.Page1.oPag.oDRPERRIT_2_14.controlsource=this.cTrsName+'.t_DRPERRIT'
    this.oPgFRm.Page1.oPag.oDRSPERIM_2_15.controlsource=this.cTrsName+'.t_DRSPERIM'
    this.oPgFRm.Page1.oPag.oDRCODSNS_2_16.controlsource=this.cTrsName+'.t_DRCODSNS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(79)
    this.AddVLine(179)
    this.AddVLine(326)
    this.AddVLine(471)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODTRI_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATIRITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATIRITE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATIRITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATIRITE_IDX,2])
      *
      * insert into DATIRITE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATIRITE')
        i_extval=cp_InsertValODBCExtFlds(this,'DATIRITE')
        i_cFldBody=" "+;
                  "(DRSERIAL,DRCODTRI,DRCODCAU,DRNONSOG,DRIMPONI"+;
                  ",DRRITENU,DRCODBUN,DRNONSO1,DRSOMESC,DRFODPRO"+;
                  ",DRRIINPS,DRPERRIT,DRDATCON,DRSPERIM,DRCODSNS,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DRSERIAL)+","+cp_ToStrODBCNull(this.w_DRCODTRI)+","+cp_ToStrODBC(this.w_DRCODCAU)+","+cp_ToStrODBC(this.w_DRNONSOG)+","+cp_ToStrODBC(this.w_DRIMPONI)+;
             ","+cp_ToStrODBC(this.w_DRRITENU)+","+cp_ToStrODBCNull(this.w_DRCODBUN)+","+cp_ToStrODBC(this.w_DRNONSO1)+","+cp_ToStrODBC(this.w_DRSOMESC)+","+cp_ToStrODBC(this.w_DRFODPRO)+;
             ","+cp_ToStrODBC(this.w_DRRIINPS)+","+cp_ToStrODBC(this.w_DRPERRIT)+","+cp_ToStrODBC(this.w_DRDATCON)+","+cp_ToStrODBC(this.w_DRSPERIM)+","+cp_ToStrODBC(this.w_DRCODSNS)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATIRITE')
        i_extval=cp_InsertValVFPExtFlds(this,'DATIRITE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DRSERIAL',this.w_DRSERIAL,'DRCODCAU',this.w_DRCODCAU)
        INSERT INTO (i_cTable) (;
                   DRSERIAL;
                  ,DRCODTRI;
                  ,DRCODCAU;
                  ,DRNONSOG;
                  ,DRIMPONI;
                  ,DRRITENU;
                  ,DRCODBUN;
                  ,DRNONSO1;
                  ,DRSOMESC;
                  ,DRFODPRO;
                  ,DRRIINPS;
                  ,DRPERRIT;
                  ,DRDATCON;
                  ,DRSPERIM;
                  ,DRCODSNS;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DRSERIAL;
                  ,this.w_DRCODTRI;
                  ,this.w_DRCODCAU;
                  ,this.w_DRNONSOG;
                  ,this.w_DRIMPONI;
                  ,this.w_DRRITENU;
                  ,this.w_DRCODBUN;
                  ,this.w_DRNONSO1;
                  ,this.w_DRSOMESC;
                  ,this.w_DRFODPRO;
                  ,this.w_DRRIINPS;
                  ,this.w_DRPERRIT;
                  ,this.w_DRDATCON;
                  ,this.w_DRSPERIM;
                  ,this.w_DRCODSNS;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DATIRITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATIRITE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not Empty(t_DRCODTRI) and not empty(t_DRCODCAU) ) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DATIRITE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " DRNONSO1="+cp_ToStrODBC(this.w_DRNONSO1)+;
                 ",DRSOMESC="+cp_ToStrODBC(this.w_DRSOMESC)+;
                 ",DRFODPRO="+cp_ToStrODBC(this.w_DRFODPRO)+;
                 ",DRDATCON="+cp_ToStrODBC(this.w_DRDATCON)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DATIRITE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  DRNONSO1=this.w_DRNONSO1;
                 ,DRSOMESC=this.w_DRSOMESC;
                 ,DRFODPRO=this.w_DRFODPRO;
                 ,DRDATCON=this.w_DRDATCON;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not Empty(t_DRCODTRI) and not empty(t_DRCODCAU) ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DATIRITE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DATIRITE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DRCODTRI="+cp_ToStrODBCNull(this.w_DRCODTRI)+;
                     ",DRNONSOG="+cp_ToStrODBC(this.w_DRNONSOG)+;
                     ",DRIMPONI="+cp_ToStrODBC(this.w_DRIMPONI)+;
                     ",DRRITENU="+cp_ToStrODBC(this.w_DRRITENU)+;
                     ",DRCODBUN="+cp_ToStrODBCNull(this.w_DRCODBUN)+;
                     ",DRNONSO1="+cp_ToStrODBC(this.w_DRNONSO1)+;
                     ",DRSOMESC="+cp_ToStrODBC(this.w_DRSOMESC)+;
                     ",DRFODPRO="+cp_ToStrODBC(this.w_DRFODPRO)+;
                     ",DRRIINPS="+cp_ToStrODBC(this.w_DRRIINPS)+;
                     ",DRPERRIT="+cp_ToStrODBC(this.w_DRPERRIT)+;
                     ",DRDATCON="+cp_ToStrODBC(this.w_DRDATCON)+;
                     ",DRSPERIM="+cp_ToStrODBC(this.w_DRSPERIM)+;
                     ",DRCODSNS="+cp_ToStrODBC(this.w_DRCODSNS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DATIRITE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DRCODTRI=this.w_DRCODTRI;
                     ,DRNONSOG=this.w_DRNONSOG;
                     ,DRIMPONI=this.w_DRIMPONI;
                     ,DRRITENU=this.w_DRRITENU;
                     ,DRCODBUN=this.w_DRCODBUN;
                     ,DRNONSO1=this.w_DRNONSO1;
                     ,DRSOMESC=this.w_DRSOMESC;
                     ,DRFODPRO=this.w_DRFODPRO;
                     ,DRRIINPS=this.w_DRRIINPS;
                     ,DRPERRIT=this.w_DRPERRIT;
                     ,DRDATCON=this.w_DRDATCON;
                     ,DRSPERIM=this.w_DRSPERIM;
                     ,DRCODSNS=this.w_DRCODSNS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATIRITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATIRITE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not Empty(t_DRCODTRI) and not empty(t_DRCODCAU) ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DATIRITE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not Empty(t_DRCODTRI) and not empty(t_DRCODCAU) ) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATIRITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATIRITE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
          .link_2_8('Full')
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate()
        .DoRTCalc(10,33,.t.)
          .w_TIPRITE = this.oparentobject .w_RITENU
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(35,41,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MODIFICA with this.w_MODIFICA
      replace t_VCODTRI with this.w_VCODTRI
      replace t_DRCODBUN with this.w_DRCODBUN
      replace t_FLACON with this.w_FLACON
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate()
    endwith
  return
  proc Calculate_LCQEAUQMKD()
    with this
          * --- Aggiorna campo ENASARCO
          .oParentObject.w_PNTOTENA = .w_TOTENA
          .oParentObject.bHeaderUpdated = .T.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDRFODPRO_1_24.enabled = this.oPgFrm.Page1.oPag.oDRFODPRO_1_24.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDRCODCAU_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDRCODCAU_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTOTENA_1_27.visible=!this.oPgFrm.Page1.oPag.oTOTENA_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oDRESIDUO_1_34.visible=!this.oPgFrm.Page1.oPag.oDRESIDUO_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oDRESIDU1_1_48.visible=!this.oPgFrm.Page1.oPag.oDRESIDU1_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oDRRIINPS_2_13.visible=!this.oPgFrm.Page1.oPag.oDRRIINPS_2_13.mHide()
    this.oPgFrm.Page1.oPag.oDRSPERIM_2_15.visible=!this.oPgFrm.Page1.oPag.oDRSPERIM_2_15.mHide()
    this.oPgFrm.Page1.oPag.oDRCODSNS_2_16.visible=!this.oPgFrm.Page1.oPag.oDRCODSNS_2_16.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_2.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
        if lower(cEvent)==lower("w_TOTENA Changed")
          .Calculate_LCQEAUQMKD()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DRCODTRI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCODTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_DRCODTRI)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_DRCODTRI))
          select TRCODTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRCODTRI)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRCODTRI) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oDRCODTRI_2_1'),i_cWhere,'',"Elenco tributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCODTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_DRCODTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_DRCODTRI)
            select TRCODTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCODTRI = NVL(_Link_.TRCODTRI,space(5))
      this.w_FLACON = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DRCODTRI = space(5)
      endif
      this.w_FLACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCODTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRI_BUTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.TRCODTRI as TRCODTRI201"+ ",link_2_1.TRFLACON as TRFLACON201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on DATIRITE.DRCODTRI=link_2_1.TRCODTRI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and DATIRITE.DRCODTRI=link_2_1.TRCODTRI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRCODBUN
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_DRCODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_AZIENDA;
                       ,'BUCODICE',this.w_DRCODBUN)
            select BUCODAZI,BUCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCODBUN = NVL(_Link_.BUCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_DRCODBUN = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDRSOMESC_1_11.value==this.w_DRSOMESC)
      this.oPgFrm.Page1.oPag.oDRSOMESC_1_11.value=this.w_DRSOMESC
    endif
    if not(this.oPgFrm.Page1.oPag.oDRTOTDOC_1_13.value==this.w_DRTOTDOC)
      this.oPgFrm.Page1.oPag.oDRTOTDOC_1_13.value=this.w_DRTOTDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDRIMPOSTA_1_15.value==this.w_DRIMPOSTA)
      this.oPgFrm.Page1.oPag.oDRIMPOSTA_1_15.value=this.w_DRIMPOSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDRSOGGE_1_22.value==this.w_DRSOGGE)
      this.oPgFrm.Page1.oPag.oDRSOGGE_1_22.value=this.w_DRSOGGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDRFODPRO_1_24.value==this.w_DRFODPRO)
      this.oPgFrm.Page1.oPag.oDRFODPRO_1_24.value=this.w_DRFODPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTENA_1_27.value==this.w_TOTENA)
      this.oPgFrm.Page1.oPag.oTOTENA_1_27.value=this.w_TOTENA
    endif
    if not(this.oPgFrm.Page1.oPag.oDRDAVERS_1_29.value==this.w_DRDAVERS)
      this.oPgFrm.Page1.oPag.oDRDAVERS_1_29.value=this.w_DRDAVERS
    endif
    if not(this.oPgFrm.Page1.oPag.oDRTOTRIT_1_33.value==this.w_DRTOTRIT)
      this.oPgFrm.Page1.oPag.oDRTOTRIT_1_33.value=this.w_DRTOTRIT
    endif
    if not(this.oPgFrm.Page1.oPag.oDRESIDUO_1_34.value==this.w_DRESIDUO)
      this.oPgFrm.Page1.oPag.oDRESIDUO_1_34.value=this.w_DRESIDUO
    endif
    if not(this.oPgFrm.Page1.oPag.oDRESIDU1_1_48.value==this.w_DRESIDU1)
      this.oPgFrm.Page1.oPag.oDRESIDU1_1_48.value=this.w_DRESIDU1
    endif
    if not(this.oPgFrm.Page1.oPag.oDRRIINPS_2_13.value==this.w_DRRIINPS)
      this.oPgFrm.Page1.oPag.oDRRIINPS_2_13.value=this.w_DRRIINPS
      replace t_DRRIINPS with this.oPgFrm.Page1.oPag.oDRRIINPS_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDRPERRIT_2_14.value==this.w_DRPERRIT)
      this.oPgFrm.Page1.oPag.oDRPERRIT_2_14.value=this.w_DRPERRIT
      replace t_DRPERRIT with this.oPgFrm.Page1.oPag.oDRPERRIT_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDRDATCON_1_62.RadioValue()==this.w_DRDATCON)
      this.oPgFrm.Page1.oPag.oDRDATCON_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDRSPERIM_2_15.value==this.w_DRSPERIM)
      this.oPgFrm.Page1.oPag.oDRSPERIM_2_15.value=this.w_DRSPERIM
      replace t_DRSPERIM with this.oPgFrm.Page1.oPag.oDRSPERIM_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDRCODSNS_2_16.RadioValue()==this.w_DRCODSNS)
      this.oPgFrm.Page1.oPag.oDRCODSNS_2_16.SetRadio()
      replace t_DRCODSNS with this.oPgFrm.Page1.oPag.oDRCODSNS_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODTRI_2_1.value==this.w_DRCODTRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODTRI_2_1.value=this.w_DRCODTRI
      replace t_DRCODTRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODTRI_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODCAU_2_4.RadioValue()==this.w_DRCODCAU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODCAU_2_4.SetRadio()
      replace t_DRCODCAU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODCAU_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRNONSOG_2_5.value==this.w_DRNONSOG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRNONSOG_2_5.value=this.w_DRNONSOG
      replace t_DRNONSOG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRNONSOG_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRIMPONI_2_6.value==this.w_DRIMPONI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRIMPONI_2_6.value=this.w_DRIMPONI
      replace t_DRIMPONI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRIMPONI_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRRITENU_2_7.value==this.w_DRRITENU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRRITENU_2_7.value=this.w_DRRITENU
      replace t_DRRITENU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRRITENU_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'DATIRITE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_mdr
      if i_bRes=.t.
         .w_RESCHK=0
         .NotifyEvent('Controlli')
         if .w_RESCHK<>0
           i_bRes=.f.
         endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not Empty(.w_DRCODTRI) and not empty(.w_DRCODCAU) 
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DRCODTRI = this.w_DRCODTRI
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not Empty(t_DRCODTRI) and not empty(t_DRCODCAU) )
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DRCODTRI=space(5)
      .w_MODIFICA=space(1)
      .w_VCODTRI=space(5)
      .w_DRCODCAU=space(2)
      .w_DRNONSOG=0
      .w_DRIMPONI=0
      .w_DRRITENU=0
      .w_DRCODBUN=space(3)
      .w_FLACON=space(1)
      .w_DRRIINPS=0
      .w_DRPERRIT=0
      .w_DRSPERIM=0
      .w_DRCODSNS=space(1)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_DRCODTRI))
        .link_2_1('Full')
      endif
        .w_MODIFICA = IIF(EMPTY(.w_DRCODTRI),'S','N')
        .w_VCODTRI = .w_DRCODTRI
      .DoRTCalc(5,8,.f.)
        .w_DRCODBUN = g_CODBUN
      .DoRTCalc(9,9,.f.)
      if not(empty(.w_DRCODBUN))
        .link_2_8('Full')
      endif
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate()
    endwith
    this.DoRTCalc(10,41,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DRCODTRI = t_DRCODTRI
    this.w_MODIFICA = t_MODIFICA
    this.w_VCODTRI = t_VCODTRI
    this.w_DRCODCAU = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODCAU_2_4.RadioValue(.t.)
    this.w_DRNONSOG = t_DRNONSOG
    this.w_DRIMPONI = t_DRIMPONI
    this.w_DRRITENU = t_DRRITENU
    this.w_DRCODBUN = t_DRCODBUN
    this.w_FLACON = t_FLACON
    this.w_DRRIINPS = t_DRRIINPS
    this.w_DRPERRIT = t_DRPERRIT
    this.w_DRSPERIM = t_DRSPERIM
    this.w_DRCODSNS = this.oPgFrm.Page1.oPag.oDRCODSNS_2_16.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DRCODTRI with this.w_DRCODTRI
    replace t_MODIFICA with this.w_MODIFICA
    replace t_VCODTRI with this.w_VCODTRI
    replace t_DRCODCAU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCODCAU_2_4.ToRadio()
    replace t_DRNONSOG with this.w_DRNONSOG
    replace t_DRIMPONI with this.w_DRIMPONI
    replace t_DRRITENU with this.w_DRRITENU
    replace t_DRCODBUN with this.w_DRCODBUN
    replace t_FLACON with this.w_FLACON
    replace t_DRRIINPS with this.w_DRRIINPS
    replace t_DRPERRIT with this.w_DRPERRIT
    replace t_DRSPERIM with this.w_DRSPERIM
    replace t_DRCODSNS with this.oPgFrm.Page1.oPag.oDRCODSNS_2_16.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mdrPag1 as StdContainer
  Width  = 641
  height = 475
  stdWidth  = 641
  stdheight = 475
  resizeXpos=280
  resizeYpos=213
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_2 as cp_runprogram with uid="JWYNRCMEIG",left=5, top=480, width=368,height=19,;
    caption='GSCG_BRT(PI)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRT('PI')",;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 207665862

  add object oDRSOMESC_1_11 as StdField with uid="CLCDFHDCXF",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DRSOMESC", cQueryName = "DRSOMESC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Somme escluse",;
    HelpContextID = 89809273,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=358, Top=61, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oDRTOTDOC_1_13 as StdField with uid="EEONSEHLFT",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DRTOTDOC", cQueryName = "DRTOTDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale documento",;
    HelpContextID = 188059271,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=358, Top=10, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oDRIMPOSTA_1_15 as StdField with uid="YYNUFKEHLC",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DRIMPOSTA", cQueryName = "DRIMPOSTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imposta",;
    HelpContextID = 260556186,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=358, Top=37, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oDRSOGGE_1_22 as StdField with uid="GKFRXUTRYM",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DRSOGGE", cQueryName = "DRSOGGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo soggetto a ritenuta",;
    HelpContextID = 117072182,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=357, Top=117, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oDRFODPRO_1_24 as StdField with uid="NIRZHLQJPW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DRFODPRO", cQueryName = "DRFODPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "F.do professionisti",;
    HelpContextID = 3567227,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=358, Top=87, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oDRFODPRO_1_24.mCond()
    with this.Parent.oContained
      return (.w_TIPCLF='F' OR Isalt())
    endwith
  endfunc

  add object oTOTENA_1_27 as StdField with uid="JERKHTOHAW",rtseq=25,rtrep=.f.,;
    cFormVar = "w_TOTENA", cQueryName = "TOTENA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 245338314,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=470, Top=418, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oTOTENA_1_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
    endif
  endfunc

  add object oDRDAVERS_1_29 as StdField with uid="LJOBVKOYCM",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DRDAVERS", cQueryName = "DRDAVERS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale da pagare/incassare",;
    HelpContextID = 170167927,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=470, Top=446, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"


  add object oObj_1_30 as cp_runprogram with uid="PJWGGOTQEJ",left=5, top=500, width=368,height=19,;
    caption='GSCG_BRT(PC)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRT('PC')",;
    cEvent = "w_DRRITPRE Changed,w_DRRITENU Changed",;
    nPag=1;
    , HelpContextID = 207690438


  add object oObj_1_31 as cp_runprogram with uid="AYYERCTJCO",left=5, top=540, width=368,height=19,;
    caption='GSCG_BRT(PE)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRT('PE')",;
    cEvent = "w_DRIMPONI Changed,w_DRPERRIT Changed",;
    nPag=1;
    , HelpContextID = 207682246


  add object oObj_1_32 as cp_runprogram with uid="TZXSVBNXIG",left=5, top=560, width=368,height=19,;
    caption='GSCG_BRT(PF)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRT('PF')",;
    cEvent = "Varia",;
    nPag=1;
    , HelpContextID = 207678150

  add object oDRTOTRIT_1_33 as StdField with uid="OZZVWQFVKO",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DRTOTRIT", cQueryName = "DRTOTRIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale ritenute",;
    HelpContextID = 46821770,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=474, Top=320, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oDRESIDUO_1_34 as StdField with uid="FTTFIVIPJC",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DRESIDUO", cQueryName = "DRESIDUO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile residuo soggetto a ritenuta (vedi nota N)",;
    HelpContextID = 69042565,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=329, Top=321, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oDRESIDUO_1_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
    endif
  endfunc


  add object oObj_1_36 as cp_runprogram with uid="OWXJZUPKVX",left=387, top=480, width=254,height=19,;
    caption='GSCG_BRT(PA)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRT('PA')",;
    cEvent = "w_DRSOMESC Changed",;
    nPag=1;
    , HelpContextID = 207698630


  add object oObj_1_43 as cp_runprogram with uid="GIGHQDBJLO",left=387, top=500, width=254,height=19,;
    caption='GSCG_BRT(PH)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRT('PH')",;
    cEvent = "w_DRNONSOG Changed",;
    nPag=1;
    , HelpContextID = 207669958


  add object oObj_1_44 as cp_runprogram with uid="JSJZUQEYJU",left=387, top=520, width=254,height=19,;
    caption='GSCG_BRT(PB)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRT('PB')",;
    cEvent = "w_DRFODPRO Changed",;
    nPag=1;
    , HelpContextID = 207694534

  add object oDRESIDU1_1_48 as StdField with uid="UKHGBDREVJ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DRESIDU1", cQueryName = "DRESIDU1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile residuo soggetto a ritenuta (vedi nota O)",;
    HelpContextID = 69042535,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=329, Top=348, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oDRESIDU1_1_48.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
    endif
  endfunc


  add object oBtn_1_53 as StdButton with uid="BTGHEPOPZV",left=586, top=99, width=48,height=45,;
    CpPicture="BMP\CALCOLA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare il calcolo delle righe";
    , HelpContextID = 244318246;
    , TabStop=.f., Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      with this.Parent.oContained
        GSCG_BRT(this.Parent.oContained,"PG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_54 as cp_runprogram with uid="ULOSZLXTBU",left=5, top=520, width=368,height=19,;
    caption='GSCG_BRT(PD)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRT('PD')",;
    cEvent = "w_DRCODTRI Changed",;
    nPag=1;
    , HelpContextID = 207686342


  add object oObj_1_57 as cp_runprogram with uid="PRXNKAWFAA",left=387, top=540, width=254,height=19,;
    caption='GSCG_BRT(PT)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRT('PT')",;
    cEvent = "w_TOTENA Changed",;
    nPag=1;
    , HelpContextID = 207620806


  add object oObj_1_58 as cp_runprogram with uid="IEGRETYCXM",left=387, top=585, width=254,height=19,;
    caption='GSCG_BDR',;
   bGlobalFont=.t.,;
    prg="GSCG_BDR",;
    cEvent = "Controlli",;
    nPag=1;
    , HelpContextID = 57762488

  add object oDRDATCON_1_62 as StdCheck with uid="UEXPGTAZNV",rtseq=38,rtrep=.f.,left=8, top=120, caption="Dati confermati",;
    ToolTipText = "Se attivo conferma i dati inseriti",;
    HelpContextID = 205819516,;
    cFormVar="w_DRDATCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDRDATCON_1_62.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DRDATCON,&i_cF..t_DRDATCON),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oDRDATCON_1_62.GetRadio()
    this.Parent.oContained.w_DRDATCON = this.RadioValue()
    return .t.
  endfunc

  func oDRDATCON_1_62.ToRadio()
    this.Parent.oContained.w_DRDATCON=trim(this.Parent.oContained.w_DRDATCON)
    return(;
      iif(this.Parent.oContained.w_DRDATCON=='S',1,;
      0))
  endfunc

  func oDRDATCON_1_62.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=155, width=624,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="DRCODTRI",Label1="Cod. tributo",Field2="DRCODCAU",Label2="Causale tributo",Field3="DRNONSOG",Label3="Somme non sogg.",Field4="DRIMPONI",Label4="Imp. ritenuta",Field5="DRRITENU",Label5="Ritenuta di acconto",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235734138

  add object oStr_1_12 as StdString with uid="ERULPARSFF",Visible=.t., Left=179, Top=14,;
    Alignment=1, Width=175, Height=18,;
    Caption="Totale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="EBBREPZQIA",Visible=.t., Left=297, Top=39,;
    Alignment=1, Width=57, Height=18,;
    Caption="Imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="FOSRERXJVF",Visible=.t., Left=203, Top=63,;
    Alignment=1, Width=151, Height=18,;
    Caption="Somme escluse:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="DYFXHRTJWQ",Visible=.t., Left=124, Top=120,;
    Alignment=1, Width=230, Height=19,;
    Caption="Base calcolo ritenuta:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="PGDHMMTLOH",Visible=.t., Left=503, Top=14,;
    Alignment=0, Width=18, Height=19,;
    Caption="A"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="VORTDHVLVV",Visible=.t., Left=503, Top=38,;
    Alignment=0, Width=18, Height=19,;
    Caption="B"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="JGRJTAYYRR",Visible=.t., Left=503, Top=92,;
    Alignment=0, Width=18, Height=19,;
    Caption="D"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="ISQONGMLJE",Visible=.t., Left=502, Top=121,;
    Alignment=0, Width=80, Height=19,;
    Caption="F=A-B-C-D"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="TBEFTKMIUX",Visible=.t., Left=229, Top=89,;
    Alignment=1, Width=125, Height=18,;
    Caption="Fondo professionisti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="SFCCXHKNRO",Visible=.t., Left=264, Top=448,;
    Alignment=1, Width=198, Height=19,;
    Caption="Totale da pagare:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="EVUEVTVQMF",Visible=.t., Left=38, Top=375,;
    Alignment=0, Width=500, Height=18,;
    Caption="Soggette a ritenuta - somma imponibili ritenuta IRPEF - somme non soggette IRPEF"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="HXPWTQOZLW",Visible=.t., Left=617, Top=322,;
    Alignment=0, Width=18, Height=19,;
    Caption="M"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="MQQVIRDHUV",Visible=.t., Left=6, Top=357,;
    Alignment=0, Width=27, Height=19,;
    Caption="M ="  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="AGUPNEOJHS",Visible=.t., Left=38, Top=358,;
    Alignment=0, Width=234, Height=18,;
    Caption="Ritenute previdenziali + ritenute IRPEF"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="INPNURXDSY",Visible=.t., Left=6, Top=374,;
    Alignment=0, Width=25, Height=19,;
    Caption="N ="  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="WYBDQFEUNS",Visible=.t., Left=315, Top=322,;
    Alignment=0, Width=13, Height=19,;
    Caption="N"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="KMBRGFXXQD",Visible=.t., Left=503, Top=65,;
    Alignment=0, Width=18, Height=19,;
    Caption="C"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="CBPYJBDGKH",Visible=.t., Left=38, Top=393,;
    Alignment=0, Width=460, Height=18,;
    Caption="Soggette a ritenuta - somma imponibili ritenuta prev. - somme non soggette prev."  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="KYOQOUYVDA",Visible=.t., Left=6, Top=392,;
    Alignment=0, Width=26, Height=19,;
    Caption="O ="  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="PTJZBNUUPH",Visible=.t., Left=315, Top=350,;
    Alignment=0, Width=13, Height=19,;
    Caption="O"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="FXQKXHVSNQ",Visible=.t., Left=309, Top=418,;
    Alignment=1, Width=153, Height=18,;
    Caption="Totale ENASARCO:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="PJCCUQKBVH",Visible=.t., Left=149, Top=256,;
    Alignment=1, Width=106, Height=18,;
    Caption="% Rit. percip.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="EKIEDAZOTR",Visible=.t., Left=7, Top=256,;
    Alignment=1, Width=83, Height=18,;
    Caption="% Ritenute:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_FLACON='R')
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="OAECMTASPX",Visible=.t., Left=352, Top=256,;
    Alignment=1, Width=130, Height=18,;
    Caption="Spese rimborsate:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (.w_FLACON='R')
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="JUOSHUKOVG",Visible=.t., Left=311, Top=447,;
    Alignment=1, Width=151, Height=19,;
    Caption="Totale da incassare:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='F')
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="AXMLWMDVFI",Visible=.t., Left=9, Top=291,;
    Alignment=1, Width=184, Height=18,;
    Caption="Codice somme non soggette:"  ;
  , bGlobalFont=.t.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (.w_FLACON<>'R' OR .w_TIPCLF='C' OR .w_DRNONSOG=0 )
    endwith
  endfunc

  add object oBox_1_23 as StdBox with uid="KDZMDKRNSV",left=359, top=112, width=137,height=2

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=174,;
    width=620+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*4*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=175,width=619+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*4*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TRI_BUTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDRRIINPS_2_13.Refresh()
      this.Parent.oDRPERRIT_2_14.Refresh()
      this.Parent.oDRSPERIM_2_15.Refresh()
      this.Parent.oDRCODSNS_2_16.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TRI_BUTI'
        oDropInto=this.oBodyCol.oRow.oDRCODTRI_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oObj_2_11 as cp_runprogram with uid="IYYLRRTSDW",width=254,height=19,;
   left=387, top=561,;
    caption='GSCG_BRT(PC)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRT('PC')",;
    cEvent = "Row deleted",;
    nPag=2;
    , HelpContextID = 207690438

  add object oDRRIINPS_2_13 as StdTrsField with uid="MIBVLWRREZ",rtseq=36,rtrep=.t.,;
    cFormVar="w_DRRIINPS",value=0,;
    ToolTipText = "Percentuale ritenuta complessiva",;
    HelpContextID = 32222839,;
    cTotal="", bFixedPos=.t., cQueryName = "DRRIINPS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=91, Top=255, cSayPict=["999.99"], cGetPict=["999.99"]

  func oDRRIINPS_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLACON='R')
    endwith
    endif
  endfunc

  add object oDRPERRIT_2_14 as StdTrsField with uid="UBJVIATFQI",rtseq=37,rtrep=.t.,;
    cFormVar="w_DRPERRIT",value=0,;
    ToolTipText = "Percentuale ritenute a carico del percipiente",;
    HelpContextID = 44052874,;
    cTotal="", bFixedPos=.t., cQueryName = "DRPERRIT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=257, Top=255, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oDRSPERIM_2_15 as StdTrsField with uid="JIEOXIZJLY",rtseq=39,rtrep=.t.,;
    cFormVar="w_DRSPERIM",value=0,;
    ToolTipText = "Importo spese rimborsate",;
    HelpContextID = 31154563,;
    cTotal="", bFixedPos=.t., cQueryName = "DRSPERIM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=486, Top=255, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  func oDRSPERIM_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLACON='R')
    endwith
    endif
  endfunc

  add object oDRCODSNS_2_16 as StdTrsCombo with uid="YWJPZBUMZA",rtrep=.t.,;
    cFormVar="w_DRCODSNS", RowSource=""+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Assegni di servizio civile,"+"7 - Erogazione di altri redditi non soggetti a ritenuta,"+"Erogazione di altri redditi non soggetti a ritenuta (Certificazione unica 2016),"+"Erogazione di altri redditi non soggetti a ritenuta (Certificazione unica 2017)" , ;
    ToolTipText = "Codice che identifica la tipologia di somme non soggette",;
    HelpContextID = 221683319,;
    Height=26, Width=388, Left=196, Top=286,;
    cTotal="", cQueryName = "DRCODSNS",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDRCODSNS_2_16.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DRCODSNS,&i_cF..t_DRCODSNS),this.value)
    return(iif(xVal =1,'1',;
    iif(xVal =2,'2',;
    iif(xVal =3,'5',;
    iif(xVal =4,'A',;
    iif(xVal =5,'7',;
    iif(xVal =6,'3',;
    iif(xVal =7,'6',;
    ' '))))))))
  endfunc
  func oDRCODSNS_2_16.GetRadio()
    this.Parent.oContained.w_DRCODSNS = this.RadioValue()
    return .t.
  endfunc

  func oDRCODSNS_2_16.ToRadio()
    this.Parent.oContained.w_DRCODSNS=trim(this.Parent.oContained.w_DRCODSNS)
    return(;
      iif(this.Parent.oContained.w_DRCODSNS=='1',1,;
      iif(this.Parent.oContained.w_DRCODSNS=='2',2,;
      iif(this.Parent.oContained.w_DRCODSNS=='5',3,;
      iif(this.Parent.oContained.w_DRCODSNS=='A',4,;
      iif(this.Parent.oContained.w_DRCODSNS=='7',5,;
      iif(this.Parent.oContained.w_DRCODSNS=='3',6,;
      iif(this.Parent.oContained.w_DRCODSNS=='6',7,;
      0))))))))
  endfunc

  func oDRCODSNS_2_16.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDRCODSNS_2_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLACON<>'R' OR .w_TIPCLF='C' OR .w_DRNONSOG=0)
    endwith
    endif
  endfunc

  add object oBox_2_9 as StdBox with uid="GJEKKPSOLV",left=472, top=442, width=137,height=2

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mdrBodyRow as CPBodyRowCnt
  Width=610
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDRCODTRI_2_1 as StdTrsField with uid="GTXFXYKZUS",rtseq=2,rtrep=.t.,;
    cFormVar="w_DRCODTRI",value=space(5),;
    ToolTipText = "Codice tributo",;
    HelpContextID = 63529343,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", oKey_1_1="TRCODTRI", oKey_1_2="this.w_DRCODTRI"

  func oDRCODTRI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRCODTRI_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDRCODTRI_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oDRCODTRI_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco tributi",'',this.parent.oContained
  endproc

  add object oDRCODCAU_2_4 as StdTrsCombo with uid="TDJNHXSRDD",rtrep=.t.,;
    cFormVar="w_DRCODCAU", RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO" , ;
    ToolTipText = "Causale tributo (di default letto dall'anagrafica del fornitore)",;
    HelpContextID = 221683317,;
    Height=21, Width=90, Left=75, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDRCODCAU_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DRCODCAU,&i_cF..t_DRCODCAU),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'B',;
    iif(xVal =3,'C',;
    iif(xVal =4,'D',;
    iif(xVal =5,'E',;
    iif(xVal =6,'F',;
    iif(xVal =7,'G',;
    iif(xVal =8,'H',;
    iif(xVal =9,'I',;
    iif(xVal =10,'J',;
    iif(xVal =11,'K',;
    iif(xVal =12,'L',;
    iif(xVal =13,'L1',;
    iif(xVal =14,'M',;
    iif(xVal =15,'M1',;
    iif(xVal =16,'M2',;
    iif(xVal =17,'N',;
    iif(xVal =18,'O',;
    iif(xVal =19,'O1',;
    iif(xVal =20,'P',;
    iif(xVal =21,'Q',;
    iif(xVal =22,'R',;
    iif(xVal =23,'S',;
    iif(xVal =24,'T',;
    iif(xVal =25,'U',;
    iif(xVal =26,'V',;
    iif(xVal =27,'V1',;
    iif(xVal =28,'V2',;
    iif(xVal =29,'W',;
    iif(xVal =30,'X',;
    iif(xVal =31,'Y',;
    iif(xVal =32,'Z',;
    iif(xVal =33,'ZO',;
    space(2)))))))))))))))))))))))))))))))))))
  endfunc
  func oDRCODCAU_2_4.GetRadio()
    this.Parent.oContained.w_DRCODCAU = this.RadioValue()
    return .t.
  endfunc

  func oDRCODCAU_2_4.ToRadio()
    this.Parent.oContained.w_DRCODCAU=trim(this.Parent.oContained.w_DRCODCAU)
    return(;
      iif(this.Parent.oContained.w_DRCODCAU=='A',1,;
      iif(this.Parent.oContained.w_DRCODCAU=='B',2,;
      iif(this.Parent.oContained.w_DRCODCAU=='C',3,;
      iif(this.Parent.oContained.w_DRCODCAU=='D',4,;
      iif(this.Parent.oContained.w_DRCODCAU=='E',5,;
      iif(this.Parent.oContained.w_DRCODCAU=='F',6,;
      iif(this.Parent.oContained.w_DRCODCAU=='G',7,;
      iif(this.Parent.oContained.w_DRCODCAU=='H',8,;
      iif(this.Parent.oContained.w_DRCODCAU=='I',9,;
      iif(this.Parent.oContained.w_DRCODCAU=='J',10,;
      iif(this.Parent.oContained.w_DRCODCAU=='K',11,;
      iif(this.Parent.oContained.w_DRCODCAU=='L',12,;
      iif(this.Parent.oContained.w_DRCODCAU=='L1',13,;
      iif(this.Parent.oContained.w_DRCODCAU=='M',14,;
      iif(this.Parent.oContained.w_DRCODCAU=='M1',15,;
      iif(this.Parent.oContained.w_DRCODCAU=='M2',16,;
      iif(this.Parent.oContained.w_DRCODCAU=='N',17,;
      iif(this.Parent.oContained.w_DRCODCAU=='O',18,;
      iif(this.Parent.oContained.w_DRCODCAU=='O1',19,;
      iif(this.Parent.oContained.w_DRCODCAU=='P',20,;
      iif(this.Parent.oContained.w_DRCODCAU=='Q',21,;
      iif(this.Parent.oContained.w_DRCODCAU=='R',22,;
      iif(this.Parent.oContained.w_DRCODCAU=='S',23,;
      iif(this.Parent.oContained.w_DRCODCAU=='T',24,;
      iif(this.Parent.oContained.w_DRCODCAU=='U',25,;
      iif(this.Parent.oContained.w_DRCODCAU=='V',26,;
      iif(this.Parent.oContained.w_DRCODCAU=='V1',27,;
      iif(this.Parent.oContained.w_DRCODCAU=='V2',28,;
      iif(this.Parent.oContained.w_DRCODCAU=='W',29,;
      iif(this.Parent.oContained.w_DRCODCAU=='X',30,;
      iif(this.Parent.oContained.w_DRCODCAU=='Y',31,;
      iif(this.Parent.oContained.w_DRCODCAU=='Z',32,;
      iif(this.Parent.oContained.w_DRCODCAU=='ZO',33,;
      0))))))))))))))))))))))))))))))))))
  endfunc

  func oDRCODCAU_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDRCODCAU_2_4.mCond()
    with this.Parent.oContained
      return (.w_FLACON='R')
    endwith
  endfunc

  add object oDRNONSOG_2_5 as StdTrsField with uid="LLOZLRIFHZ",rtseq=6,rtrep=.t.,;
    cFormVar="w_DRNONSOG",value=0,;
    ToolTipText = "Somme non soggette",;
    HelpContextID = 211152515,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=177, Top=0, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oDRIMPONI_2_6 as StdTrsField with uid="OKJSRSMGOD",rtseq=7,rtrep=.t.,;
    cFormVar="w_DRIMPONI",value=0,;
    ToolTipText = "Imponibile ritenuta",;
    HelpContextID = 7880321,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=321, Top=0, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oDRRITENU_2_7 as StdTrsField with uid="IYQYOXPUCM",rtseq=8,rtrep=.t.,;
    cFormVar="w_DRRITENU",value=0,;
    ToolTipText = "Ritenuta di acconto",;
    HelpContextID = 171683445,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=466, Top=0, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDRCODTRI_2_1.When()
    return(.t.)
  proc oDRCODTRI_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDRCODTRI_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=3
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mdr','DATIRITE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DRSERIAL=DATIRITE.DRSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_mdr
* Ridefinisco la classe GSCG_MDR effettuo questa operazione
* per poter gestire il caso in cui l'utente abbia premuto ESC
define class tgscg_mdr as StdPCForm
  Width  = 645
  Height = 435
  Top    = 6
  Left   = 3
  cComment = "Dati ritenute"
  HelpContextID=80356713

proc ecpQuit()
    * Zucchetti Aulla - Inizio
    * Eliminato IF esterno per consentire gestine F10 non bloccante su figli da bottone
  if this.cnt.oParentObject.cFunction='Load'
      if this.IsAChildUpdated()
        this.cnt.bUpdated=!cp_YesNo(MSG_DISCARD_CHANGES_RECOVERY_DB_QP)
        * Zucchetti Aulla - Inizio - Premendo Esc su figli da bottone e rispondendo Abbandoni
        * le modifiche al successivo rientro l'applicazione visualizza i dati abbandonati
        * Se abbandono le modifiche mi preparo per ricaricare i dati
        * dal database alla prossima apertura
        this.cnt.nDeferredFillRec=1
        this.cnt.oParentObject.w_TESTRITE=.t.
        this.cnt.oParentObject.w_TOTMDR=0
        * Zucchetti Aulla - Fine
      endif
      if !this.cnt.bUpdated
        this.cnt.bDontReportError=.t.
        this.TerminateEdit()
        this.cnt.bUpdated=.f.
      endif
    this.LinkPCClick()
  else
   dodefault()
  endif
    return

  proc ecpSave()
    if this.cnt.bOnScreen and inlist(this.cFunction,'Load','Edit')
      if !this.cnt.CheckForm()
        this.cnt.setFocus()
        return
      endif
    endif
    this.cnt.bF10=.t.
    this.LinkPCClick()

    * Se il totale del documento � diverso da 0
    * assegno alla variabile Totmdr il valore Drtotdoc
    * ed effettuo la funzione ecpsave della primanota
    if this.cnt.w_DRTOTDOC<>0
     if this.cnt.w_DRTOTRIT<>0 OR (this.cnt.w_DRNSOGGI=this.cnt.w_DRSOGGE) OR (this.cnt.w_DRNSOGGP=this.cnt.w_DRSOGGE)
      this.cnt.oParentObject.w_TOTMDR=this.cnt.w_DRTOTDOC
      this.cnt.oParentObject.w_PCCLICK=.T.
      * In modifica rendo condizionale il salvataggio della Prima Nota
      IF this.cnt.oParentObject.cFunction='Load'
         this.cnt.oParentObject.ecpsave()
      Endif
     else
      this.cnt.oParentObject.w_TOTMDR=0
     endif
    endif
    * Forzo il salvataggio a seguito di un F10
    This.cnt.bUpdated=.t.

    return
  Endproc


  add object cnt as tcgscg_mdr
enddefine
* --- Fine Area Manuale
