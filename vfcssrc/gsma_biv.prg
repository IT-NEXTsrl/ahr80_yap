* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_biv                                                        *
*              Elabora invenduto                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_104]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-25                                                      *
* Last revis.: 2010-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_biv",oParentObject)
return(i_retval)

define class tgsma_biv as StdBatch
  * --- Local variables
  w_DATA = ctod("  /  /  ")
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Invenduto (da GSMA_SIV)
    * --- Variabili della maschera
    * --- Variabili per il report
    CODMAG = this.oParentObject.w_CODMAG
    CODCAT = this.oParentObject.w_CODCAT
    CODFAM = this.oParentObject.w_CODFAM
    CODMAR = this.oParentObject.w_CODMAR
    CODGRU = this.oParentObject.w_CODGRU
    CODPRO = this.oParentObject.w_CODPRO
    DATASTAM = this.oParentObject.w_DATASTAM
    SELINV = this.oParentObject.w_SELINV
    * --- Creazione cursore con i movimenti degli articoli
    ah_Msg("Selezione articoli invenduti...")
    vq_exec("query\GSMA_SIV",this,"__tmp__")
    this.w_DATA = cp_CharToDate("  -  -  ")
    * --- Sbianca date acquisti
    select __TMP__
    go top
    scan for (tipo ="DA" OR tipo="MA")
    replace MMDATA with this.w_DATA
    endscan
    select mmcodart as codart, magazzino, ardesart ,max(mmdata) as DataMov ;
    group by codart, magazzino from __tmp__ into cursor __tmp__
    * --- Creazione cursore con numero massimo giorni invenduto per magazzino
    ah_Msg("Lettura valorizzazioni di magazzino...")
    vq_exec("query\GSMA7SIV",this,"__inv__xmag")
    select prcodmag as codmag, prcodart as articolo, ;
    prgioinv from __inv__xmag into cursor __inv__xmag
    * --- Creazione cursore con numero massimo giorni invenduto e join coi magazzini
    ah_Msg("Lettura valorizzazioni di magazzino...")
    vq_exec("query\GSMA6SIV",this,"__inv__")
    select prcodart as articolo, ;
    prgioinv from __inv__ into cursor __inv__
    vq_exec("query\GSMA8SIV",this,"maga")
    select mgcodmag as codmag, articolo, prgioinv from __inv__, maga into cursor __inv__
    L_T2=reccount("__inv__")
    * --- gestione del filtro magazzino e magazzini collegati
    if not empty(this.oParentObject.W_CODMAG)
      select * from __inv__xmag ;
      where codmag in (select mgcodmag from maga) into cursor __inv__xmag
    endif
    * --- Numero di righe selezionate
    l_t1=reccount("__inv__xmag")
    * --- Cursore che considera tutti i magazzini, tenendo conto della movimentazione
    * --- dei dati articolo per magazzino.
    do case
      case L_T1>0 and L_T2>0
        * --- Ci sono righe sia di __inv__ che di __inv__xmag
        select * from __inv__xmag union;
        (select * from __inv__ ;
        where codmag+articolo not in ;
        (select codmag+articolo from __inv__xmag)) ;
        into cursor Giorni
      case L_T1>0 and L_T2=0
        * --- Ci sono righe solo in __inv__xmag
        select * from __inv__xmag into cursor Giorni
      case L_T1=0 and L_T2>0
        * --- Ci sono righe solo in __inv__
        select * from __inv__ into cursor Giorni
      case L_T1=0 and L_T2=0
        ah_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
    endcase
    if this.oParentObject.w_ARTMOV="S"
      select articolo, codmag as magazzino, DataMov, prgioinv as MaxGGinv, ;
      looktab("KEY_ARTI","CADESART","CACODICE",articolo) as desart ;
      from Giorni inner join __tmp__ on codmag=magazzino and ;
      articolo=codart into cursor __tmp__
    else
      select articolo, codmag as magazzino, DataMov, prgioinv as MaxGGinv, ;
      looktab("KEY_ARTI","CADESART","CACODICE",articolo) as desart ;
      from Giorni left outer join __tmp__ on codmag=magazzino and ;
      articolo=codart into cursor __tmp__
    endif
    * --- Inserisce dato sull esistenza
    vq_exec("query\GSMAESIV",this,"Esistenza")
    select __tmp__.*, Esistenza.SLQTAPER as SLQTAPER from __tmp__ left outer join Esistenza on SLCODICE=articolo AND SLCODMAG=magazzino into cursor __tmp__ 
    * --- Filtra articoli sulla disponibilitÓ 
    if this.oParentObject.w_ARTESI="P"
      select * from __tmp__ where NVL (SLQTAPER,0) >0 into cursor __tmp__ 
    endif
    if this.oParentObject.w_ARTESI="N"
      select * from __tmp__ where NVL (SLQTAPER,0) < 0 into cursor __tmp__ 
    endif
    if this.oParentObject.w_ARTESI="U"
      select * from __tmp__ where NVL (SLQTAPER,0) = 0 into cursor __tmp__ 
    endif
    * --- Imposta il criterio di ordinamento ed elimina eventuali date nulle
    if this.oParentObject.w_SelOrd = "A"
      * --- Per articolo
      w_Ordina = "magazzino, articolo"
    else
      * --- Per Descrizione
      w_Ordina = "magazzino, desart"
    endif
    ah_Msg("Totalizzazione per gruppi di magazzini...")
    select magazzino, articolo, cp_todate(DataMov) as DataMov,SLQTAPER, ;
    MaxGGInv, desart from __tmp__ where MaxGGinv <>0;
    order by &w_Ordina ;
    into cursor __tmp__
    * --- Gestione opzione CONSIDERA SOLO ARTICOLI INVENDUTI
    if this.oParentObject.w_SelInv = "I"
      ah_Msg("Selezione articoli invenduti...")
      select * ;
      from __tmp__ ;
      where ((DataStam-DataMov) >= MaxGGInv) or empty(DataMov);
      into cursor __tmp__
    endif
    * --- Esecuzione report
    cp_chprn("QUERY\gsma_siv.FRX", " ", this)
    * --- Chiusura cursori
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursori
    if used("__tmp__")
      select __tmp__
      use
    endif
    if used("__inv__")
      select __inv__
      use
    endif
    if used("__inv__xmag")
      select __inv__xmag
      use
    endif
    if used("Giorni")
      select Giorni
      use
    endif
    if used("MAGA")
      select MAGA
      use
    endif
    if used("Esistenza")
      select Esistenza
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
