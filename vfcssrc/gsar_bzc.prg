* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bzc                                                        *
*              Lancia anagrafiche cli/for/con                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2000-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bzc",oParentObject)
return(i_retval)

define class tgsar_bzc as StdBatch
  * --- Local variables
  w_TIPCON = space(1)
  w_PROG = space(8)
  w_CONTI = space(1)
  w_TIPO = space(1)
  w_DXBTN = .f.
  w_CODICE = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia l'anagrafica Clienti, Forintori, Conti a seconda del Tipo Conto (da GSCG_MPN ,zoom on zoom)
    * --- Viene lanciato anche da GSCG_ADI - Dichiarazioni di Intento
    * --- Se la variabile che riferisce all'ultimo form aperto � vuota esco
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_TIPCON = Nvl(g_oMenu.oKey(1,3),"")
      this.w_CODICE = Nvl(g_oMenu.oKey(2,3),"")
    else
      if i_curform=.NULL.
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Tipo di Conto
      this.w_TIPCON = &cCurs..ANTIPCON
    endif
    if (EMPTY(this.w_TIPCON) OR NOT this.w_TIPCON $ "CFG") And Not this.w_DXBTN
      * --- Se non � stato riconosciuto in precedenza
      * --- legge il tipo di conto partendo dalla espressione di Where della select..
      cInit=i_curform.z1.o_initsel
      if TYPE ("cInit") ="C"
        cInit=UPPER(cInit)
        this.w_CONTI = AT("ANTIPCON=", UPPER(cInit))
        if this.w_CONTI<>0
          this.w_CONTI = this.w_CONTI+10
          if LEN(ALLTRIM(cInit))>=this.w_CONTI
            this.w_TIPO = SUBSTR(cInit, this.w_CONTI,1)
            this.w_TIPCON = IIF(this.w_TIPO $ "CFG", this.w_TIPO, this.w_TIPCON)
          endif
        endif
      endif
    endif
    do case
      case this.w_TIPCON="C"
        * --- Lancia Clienti
        this.w_PROG = "GSAR_ACL"
      case this.w_TIPCON="F"
        * --- Lancia Fornitori
        this.w_PROG = "GSAR_AFR"
      case this.w_TIPCON="G"
        * --- Lancia Conti
        this.w_PROG = "GSAR_API"
    endcase
    * --- Se w_PROG � vuoto non apro nessuna gestione e segnalo che non � possibile 
    *     individuare il tipo C/F/G
    if Not Empty(this.w_PROG)
      OpenGest(iif(this.w_DXBTN,g_oMenu.cBatchType,"S"),this.w_PROG,"ANCODICE",this.w_CODICE)
    else
      ah_ErrorMsg("Impossibile individuare il tipo conto",,"")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
