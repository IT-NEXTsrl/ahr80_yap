* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bcn                                                        *
*              Contabilizzazione acconti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_272]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-23                                                      *
* Last revis.: 2012-06-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bcn",oParentObject)
return(i_retval)

define class tgscg_bcn as StdBatch
  * --- Local variables
  w_APPO = space(10)
  w_NUDOC = 0
  w_OKDOC = 0
  w_ZOOM = space(10)
  w_oSEGNLOG = .NULL.
  w_RIFCONT = 0
  w_oERRORLOG = .NULL.
  w_RIFCON = space(10)
  w_LCODAGE = space(5)
  w_PNSERIAL = space(10)
  w_PNCODCAU = space(5)
  w_PNVALNAZ = space(3)
  w_PNCODUTE = 0
  w_PNNUMRER = 0
  w_PNTIPREG = space(1)
  w_PNCODVAL = space(3)
  w_PNDATREG = ctod("  /  /  ")
  w_PNNUMPRO = 0
  w_PNFLIVDF = space(1)
  w_PNCAOVAL = 0
  w_PNALFDOC = space(10)
  w_PNTOTDOC = 0
  w_PNNUMREG = 0
  w_PNCOMIVA = ctod("  /  /  ")
  w_PNDATDOC = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_PNTIPDOC = space(2)
  w_PNTIPCLF = space(1)
  w_PNFLREGI = space(1)
  w_CPROWORD = 0
  w_PNALFPRO = space(10)
  w_PNCODCLF = space(15)
  w_PNPRD = space(2)
  w_PNCODESE = space(4)
  w_PNFLPROV = space(1)
  w_PNTIPCLF = space(1)
  w_PNPRP = space(2)
  w_PNCOMPET = space(4)
  w_PNNUMDOC = 0
  w_CATDOC = space(2)
  w_ARRSUP = 0
  w_PNFLZERO = space(1)
  w_OSERIAL = space(10)
  w_FLPDOC = space(1)
  w_TIPCLF = space(1)
  w_CODCLF = space(15)
  w_TROV = .f.
  w_FLPPRO = space(1)
  w_PNCODPAG = space(5)
  w_DECTOT = 0
  w_CODPAG = space(5)
  w_PNIMPAVE = 0
  w_PNFLPART = space(1)
  w_TOTDOC = 0
  w_SEZCLF = space(1)
  w_PNIMPDAR = 0
  w_PNDESSUP = space(50)
  w_TOTVAL = 0
  w_APPSEZ = space(1)
  w_PNTIPCON = space(1)
  w_APPO1 = 0
  w_CAONAZ = 0
  w_PNPRG = space(8)
  w_PNCODCON = space(15)
  w_ACCONT = 0
  w_DECTOP = 0
  w_APPVAL = 0
  w_PNANNDOC = space(4)
  w_PNINICOM = ctod("  /  /  ")
  w_PARTSN = space(1)
  w_APPO3 = 0
  w_PNANNPRO = space(4)
  w_PNFINCOM = ctod("  /  /  ")
  w_APPO2 = 0
  w_PARFOR = 0
  w_FLPART = space(1)
  w_DATOBSO = ctod("  /  /  ")
  w_PTFLSOSP = space(1)
  w_PNFLABAN = space(6)
  w_PTBANAPP = space(10)
  w_INICOM = ctod("  /  /  ")
  w_TESVAL = 0
  w_PTNUMPAR = space(31)
  w_DATBLO = ctod("  /  /  ")
  w_FINCOM = ctod("  /  /  ")
  w_MESS = space(90)
  w_PTDATSCA = ctod("  /  /  ")
  w_MAXDAT = ctod("  /  /  ")
  w_CODCAU = space(5)
  w_CODCON = space(15)
  w_PTMODPAG = space(10)
  w_MVRIFACC = space(10)
  w_CODAZI = space(5)
  w_MESS1 = space(10)
  w_PTBANNOS = space(15)
  w_MVRIFCON = space(10)
  w_TIPO = space(1)
  w_ASSEG = .f.
  w_PTTOTIMP = 0
  w_PT_SEGNO = space(1)
  w_FLRIFE = space(1)
  w_STALIG = ctod("  /  /  ")
  w_CONTA = 0
  w_PTFLCRSA = space(1)
  w_VABENE = .f.
  w_CLADOC = space(2)
  w_PTCODAGE = space(5)
  w_PNCODAGE = space(5)
  w_CODAGE = space(5)
  w_CCFLANAL = space(1)
  w_MESS_ERR = space(200)
  w_PTNUMCOR = space(25)
  w_CON_PAGA = .f.
  w_DATTEST = ctod("  /  /  ")
  w_STALIG = ctod("  /  /  ")
  w_DATBLO = ctod("  /  /  ")
  w_CCDESRIG = space(254)
  w_CCDESSUP = space(254)
  w_DESSUP = space(50)
  w_SEGNALA = space(254)
  w_PNDESRIG = space(50)
  * --- WorkFile variables
  CAU_CONT_idx=0
  PNT_MAST_idx=0
  DOC_MAST_idx=0
  VALUTE_idx=0
  PNT_DETT_idx=0
  SALDICON_idx=0
  PAR_TITE_idx=0
  AZIENDA_idx=0
  CONTI_idx=0
  BAN_CONTI_idx=0
  CON_PAGA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametro Esegue: I = Non Spunta Documenti; D = Spunta Documenti
    * --- Contabilizzazione Acconti Attivi e Passivi (da GSCG_KCN)
    * --- Parametri dalla Maschera
    this.w_ZOOM = this.oParentObject.w_CalcZoom
     
 DIMENSION ARPARAM[12,2]
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    NC = this.w_ZOOM.cCursor
    * --- Messaggistica di Errore
    * --- Carica i Documenti da Contabilizzare
    ah_Msg("Ricerca documenti da contabilizzare...",.T.)
    SELECT * FROM &NC WHERE XCHK = 1 INTO CURSOR CONTADOC
    if USED("CONTADOC")
      * --- Inizio Aggiornamento vero e proprio
      this.w_NUDOC = 0
      this.w_OKDOC = 0
      if RECCOUNT("CONTADOC") > 0
        SELECT CONTADOC
        ah_Msg("Inizio fase di contabilizzazione...",.T.)
        * --- Fase di Blocco
        * --- 1 - Blocco la Prima Nota con data = Max (data reg.)
        this.w_CODAZI = i_CODAZI
        * --- Calcolo il Max Data reg.
        if this.oParentObject.w_FLDATC="U"
          this.w_MAXDAT = this.oParentObject.w_DATCON
        else
          CALCULATE MAX(MVDATREG) FOR NOT EMPTY(NVL(MVSERIAL," ")) AND ;
          NOT EMPTY(NVL(MVCODCON," ")) TO this.w_MAXDAT
        endif
        * --- Try
        local bErr_03F1D210
        bErr_03F1D210=bTrsErr
        this.Try_03F1D210()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg(i_errmsg,,"")
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_03F1D210
        * --- End
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Rimuovo il blocco Stampa Libro giornale
        * --- Try
        local bErr_03845D28
        bErr_03845D28=bTrsErr
        this.Try_03845D28()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Impossibile rimuovere i blocchi. Rimuoverli dai dati azienda",,"")
        endif
        bTrsErr=bTrsErr or bErr_03845D28
        * --- End
        if this.w_RIFCONT=0
          ah_ErrorMsg("Documenti contabilizzati da un altro utente")
        else
          ah_ErrorMsg("Operazione completata%0N. %1 documenti contabilizzati su %2 documenti da contabilizzare",,"",ALLTRIM(STR(this.w_OKDOC)),ALLTRIM(STR(this.w_NUDOC)))
        endif
        * --- LOG Errori
        if Vartype(g_log)="O"
           
 g_log=this.w_oERRORLOG
        else
          if this.w_oErrorLog.IsFullLog()
            this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
          endif
        endif
        if Not EMpty(this.w_SEGNALA) and Isalt() and this.w_OKDOC>0 and !(Vartype(g_SCHEDULER)="C" and g_SCHEDULER="S") 
          this.w_oSEGNLOG.AddMsgLogNoTranslate(Ah_msgformat("Elenco acconti contabilizzati%0"))     
          this.w_oSEGNLOG.AddMsgLogNoTranslate(this.w_SEGNALA)     
          this.w_oSEGNLOG.PrintLog(This,"Elenco acconti contabilizzati","Si desidera stampare log generazione","Si desidera stampare log generazione")     
          this.w_oSEGNLOG = .Null.
        endif
      else
        if Not (Isalt() AND VARTYPE(g_SCHEDULER)="C" AND g_SCHEDULER="S")
          ah_ErrorMsg("Selezionare almeno un documento",,"")
        endif
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Riaggiorna il Cursore
      This.oParentObject.notifyevent("Calcola")
      * --- Esegue Refresh a video dello Zoom
      SELECT &NC
      GO TOP
    endif
  endproc
  proc Try_03F1D210()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(NVL(this.w_datblo,cp_CharToDate("  -  -  ")))
      * --- Inserisce <Blocco> per Primanota
      * --- Write into AZIENDA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDAT),'AZIENDA','AZDATBLO');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               )
      else
        update (i_cTable) set;
            AZDATBLO = this.w_MAXDAT;
            &i_ccchkf. ;
         where;
            AZCODAZI = this.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Un altro utente ha impostato il blocco - controllo concorrenza
      this.w_VABENE = .F.
      * --- Raise
      i_Error="Prima nota bloccata, verificare semaforo bollati in dati azienda. Impossibile contabilizzare"
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03845D28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera i Documenti da Contabilizzare
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_RIFCONT = 0
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_oSEGNLOG=createobject("AH_ERRORLOG")
    this.w_SEGNALA = ""
    SELECT CONTADOC
    GO TOP
    * --- Cicla sui Documenti
    SCAN FOR NOT EMPTY(NVL(MVSERIAL," ")) AND NVL(MVACCONT,0)<>0
    this.w_OSERIAL = MVSERIAL
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVRIFACC"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVRIFACC;
        from (i_cTable) where;
            MVSERIAL = this.w_OSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_RIFCON = NVL(cp_ToDate(_read_.MVRIFACC),cp_NullValue(_read_.MVRIFACC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_RIFCON)
      * --- Inizializza i dati di Testata della Nuova Registrazione P.N.
      this.w_RIFCONT = this.w_RIFCONT+1
      this.w_PNCAOVAL = NVL(MVCAOVAL, 1)
      this.w_DECTOT = NVL(VADECTOT, 0)
      this.w_TESVAL = NVL(VACAOVAL, 0)
      this.w_PNDATDOC = CP_TODATE(MVDATDOC)
      this.w_PNNUMDOC = NVL(MVNUMDOC, 0)
      this.w_PNALFDOC = NVL(MVALFDOC, "  ")
      this.w_PNCODVAL = NVL(MVCODVAL, g_PERVAL)
      this.w_PNDATREG = IIF(this.oParentObject.w_FLDATC="U", this.oParentObject.w_DATCON, MVDATREG)
      this.w_PNVALNAZ = NVL(MVVALNAZ, g_PERVAL)
      this.w_PNDESSUP = NVL(MVDESDOC, SPACE(50))
      this.w_ACCONT = NVL(MVACCONT, 0)
      this.w_TIPCLF = NVL(MVTIPCON, " ")
      this.w_CODCLF = NVL(MVCODCON, SPACE(15))
      this.w_NUDOC = this.w_NUDOC + 1
      this.w_CATDOC = NVL(TDCATDOC, " ")
      * --- Genera la Reg.Contabile
      this.w_PNTIPCLF = this.w_FLRIFE
      this.w_PNCODCLF = IIF(this.w_FLRIFE $ "CF", NVL(MVCODCON, SPACE(15)), SPACE(15))
      if Not Empty(this.w_CODCLF)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODAG1"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODAG1;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPCLF;
                and ANCODICE = this.w_CODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LCODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_PNCODCAU = this.w_CODCAU
      * --- Se esercizio per data calcolo esercizio della registrazione contabile in funzione della data registrazione
      if this.oParentObject.w_FLESER="D"
        this.w_PNCODESE = CALCESER(this.w_PNDATREG)
        this.w_PNCOMPET = this.w_PNCODESE
      else
        this.w_PNCODESE = NVL(MVCODESE, SPACE(4))
        this.w_PNCOMPET = NVL(MVCODESE, SPACE(4))
      endif
      this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
      this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
      this.w_INICOM = CP_TODATE(MVTINCOM)
      this.w_FINCOM = CP_TODATE(MVTFICOM)
      this.w_PNCOMIVA = this.w_PNDATREG
      this.w_PTBANNOS = NVL(MVCODBA2, SPACE(15))
      this.w_PNPRD = "NN"
      this.w_PNPRP = "NN"
      this.w_PNANNPRO = "    "
      this.w_PNALFPRO = Space(10)
      this.w_PNNUMPRO = 0
      this.w_PTBANAPP = NVL(MVCODBAN, SPACE(10))
      this.w_PTNUMCOR = NVL(MVNUMCOR, SPACE(25))
      this.w_MVRIFCON = NVL(MVRIFCON, SPACE(10))
      this.w_CLADOC = NVL(MVCLADOC,SPACE(2))
      this.w_PNFLREGI = " "
      this.w_PNFLPROV = "N"
      this.w_PARTSN = "N"
      this.w_CODAGE = NVL(MVCODAGE,SPACE(5))
      if g_PERPAR="S" AND NVL(ANPARTSN," ")="S"
        * --- Se Abilitate Partite Forza sempre il Flag 'A'
        this.w_PARTSN = IIF(this.w_FLPART $ "ACS", "A", "N")
      endif
      this.w_CAONAZ = GETCAM(g_PERVAL, I_DATSYS)
      this.w_DECTOP = g_PERPVL
      * --- Se altra Valuta di Esercizio
      if this.w_PNVALNAZ<>g_PERVAL
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_PNVALNAZ);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_PNVALNAZ;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECTOP = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.w_PNDATREG)
        SELECT CONTADOC
      endif
      this.w_PNTOTDOC = 0
      * --- Sezione Cli/For (Dare/Avere)
      this.w_SEZCLF = IIF(this.oParentObject.w_FLVEAC="A", IIF(this.w_CATDOC="NC","A","D"), IIF(this.w_CATDOC="NC","D","A"))
      * --- Controllo se esistono per il documento delle Contropartite sui Pagamenti
      this.w_CON_PAGA = .F.
      * --- Select from CON_PAGA
      i_nConn=i_TableProp[this.CON_PAGA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_PAGA_idx,2],.t.,this.CON_PAGA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" CON_PAGA ";
            +" where CPSERIAL = "+cp_ToStrODBC(this.w_OSERIAL)+" And (CPIMPPRE <> 0 Or CPIMPCON <>0 Or CPIMPASS <>0 Or CPIMPCAR <>0 Or CPIMPFIN <>0 )";
             ,"_Curs_CON_PAGA")
      else
        select * from (i_cTable);
         where CPSERIAL = this.w_OSERIAL And (CPIMPPRE <> 0 Or CPIMPCON <>0 Or CPIMPASS <>0 Or CPIMPCAR <>0 Or CPIMPFIN <>0 );
          into cursor _Curs_CON_PAGA
      endif
      if used('_Curs_CON_PAGA')
        select _Curs_CON_PAGA
        locate for 1=1
        do while not(eof())
        this.w_CON_PAGA = .T.
          select _Curs_CON_PAGA
          continue
        enddo
        use
      endif
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    SELECT CONTADOC
    ENDSCAN
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Locali
    this.w_CODPAG = this.oParentObject.w_SAPAGA
    this.w_FLRIFE = "N"
    this.w_CCFLANAL = " "
    this.w_MESS_ERR = SPACE(200)
    * --- Controllo che tutte le contropartite non siano obsolete alla data fine selezione
    this.w_TIPO = "G"
    this.w_CODCAU = IIF(this.oParentObject.w_FLVEAC="A", this.oParentObject.w_CACPAG, this.oParentObject.w_CACINC)
    this.w_CODCON = IIF(this.oParentObject.w_FLVEAC="A", this.oParentObject.w_COCPAG, this.oParentObject.w_COCINC)
    if NOT EMPTY(this.w_CODCAU)
      this.w_FLPART = " "
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCDTOBSO,CCTIPREG,CCFLIVDF,CCNUMREG,CCTIPDOC,CCFLPDOC,CCFLPPRO,CCFLPART,CCFLRIFE,CCFLANAL,CCDESSUP,CCDESRIG"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_CODCAU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCDTOBSO,CCTIPREG,CCFLIVDF,CCNUMREG,CCTIPDOC,CCFLPDOC,CCFLPPRO,CCFLPART,CCFLRIFE,CCFLANAL,CCDESSUP,CCDESRIG;
          from (i_cTable) where;
              CCCODICE = this.w_CODCAU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.CCDTOBSO),cp_NullValue(_read_.CCDTOBSO))
        this.w_PNTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
        this.w_PNFLIVDF = NVL(cp_ToDate(_read_.CCFLIVDF),cp_NullValue(_read_.CCFLIVDF))
        this.w_PNNUMREG = NVL(cp_ToDate(_read_.CCNUMREG),cp_NullValue(_read_.CCNUMREG))
        this.w_PNTIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
        this.w_FLPDOC = NVL(cp_ToDate(_read_.CCFLPDOC),cp_NullValue(_read_.CCFLPDOC))
        this.w_FLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
        this.w_FLPART = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
        this.w_FLRIFE = NVL(cp_ToDate(_read_.CCFLRIFE),cp_NullValue(_read_.CCFLRIFE))
        this.w_CCFLANAL = NVL(cp_ToDate(_read_.CCFLANAL),cp_NullValue(_read_.CCFLANAL))
        this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
        this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Causale contabile acconti obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
      if NOT this.w_FLPART $ "CSA" AND g_PERPAR="S"
        ah_ErrorMsg("La causale contabile acconti non gestisce le partite",,"")
        i_retcode = 'stop'
        return
      endif
    else
      ah_ErrorMsg("Causale contabile acconti inesistente",,"")
      i_retcode = 'stop'
      return
    endif
    if NOT EMPTY(this.w_CODCON)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.w_CODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Contropartita contabile acconti obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    else
      ah_ErrorMsg("Contropartita contabile acconti inesistente",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_SAPAGA)
      ah_ErrorMsg("Pagamento per abbuoni/anticipi inesistente",,"")
      i_retcode = 'stop'
      return
    endif
    this.w_DATTEST = this.oParentObject.w_DATCON
    this.w_STALIG = cp_CharToDate("  -  -  ")
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZSTALIG,AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZSTALIG,AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.w_DATTEST <= this.w_STALIG
        * --- data Registrazione Inferiore all' Ultima Data Reg. stampata su L.G.
        ah_ErrorMsg("Data registrazione inferiore o uguale a ultima stampa libro giornale")
        i_retcode = 'stop'
        return
      case this.w_DATTEST <= this.w_DATBLO AND NOT EMPTY(this.w_DATBLO)
        * --- mentre stampo il giornale non devo inserire registrazioni <= alla data di blocco
        ah_ErrorMsg("Stampa libro giornale in corso con selezione comprendente la registrazione")
        i_retcode = 'stop'
        return
    endcase
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola MVSERIAL, MVNUMREG, MVNUMDOC
    this.w_PNSERIAL = SPACE(10)
    this.w_PNNUMRER = 0
    this.w_PNANNDOC = CALPRO(IIF(EMPTY(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC), this.w_PNCOMPET, this.w_FLPDOC)
    this.w_PNINICOM = cp_CharToDate("  -  -  ")
    this.w_PNFINCOM = cp_CharToDate("  -  -  ")
    this.w_PNFLABAN = SPACE(6)
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    * --- Try
    local bErr_03838FF8
    bErr_03838FF8=bTrsErr
    this.Try_03838FF8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Errore non Compreso nei Controlli
      if Empty( i_Errmsg )
        i_ErrMsg=Message(1)+" "+Message()
      endif
      this.w_oERRORLOG.AddMsgLog("Verificare doc. n.: %1%2 del.: %3 %4", ALLTRIM(STR(this.w_PNNUMDOC,15)), IIF(EMPTY(this.w_PNALFDOC),"","/"+Alltrim(this.w_PNALFDOC)),DTOC(this.w_PNDATDOC), Alltrim( i_ERRMSG))     
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_03838FF8
    * --- End
  endproc
  proc Try_03838FF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if Not Empty(CHKCONS("P",this.w_PNDATREG,"B","N"))
      * --- Eseguo Controllo data Consolidamento
      * --- Raise
      i_Error=SUBSTR(CHKCONS("P",this.w_PNDATREG,"B","N"),11,120)
      return
    endif
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
    cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    ah_Msg("Scrivo reg. n.: %1 del %2",.T.,.F.,.F.,ALLTRIM(STR(this.w_PNNUMRER)),DTOC(this.w_PNDATREG))
    * --- Scrive la Testata
    this.w_DESSUP = this.w_PNDESSUP
    if Empty(this.w_DESSUP) and (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
      * --- Array elenco parametri per descrizioni di riga e testata
       
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PNNUMDOC)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PNALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PNDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=Alltrim(this.w_PNCODCLF) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=Alltrim(this.w_PNTIPCLF)
      this.w_DESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
    endif
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",PNCODESE"+",PNCODUTE"+",PNNUMRER"+",PNDATREG"+",PNCODCAU"+",PNCOMPET"+",PNTIPREG"+",PNFLIVDF"+",PNNUMREG"+",PNTIPDOC"+",PNPRD"+",PNALFDOC"+",PNNUMDOC"+",PNDATDOC"+",PNALFPRO"+",PNNUMPRO"+",PNVALNAZ"+",PNCODVAL"+",PNPRG"+",PNCAOVAL"+",PNDESSUP"+",PNTIPCLF"+",PNCODCLF"+",PNTOTDOC"+",PNCOMIVA"+",PNFLREGI"+",PNFLPROV"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",PNRIFDOC"+",PNANNDOC"+",PNANNPRO"+",PNPRP"+",PNRIFACC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLIVDF),'PNT_MAST','PNFLIVDF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_MAST','PNNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRD),'PNT_MAST','PNPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PNT_MAST','PNALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PNT_MAST','PNNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PNT_MAST','PNDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFPRO),'PNT_MAST','PNALFPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMPRO),'PNT_MAST','PNNUMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'PNT_MAST','PNDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PNT_MAST','PNTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PNT_MAST','PNCODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PNT_MAST','PNTOTDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMIVA),'PNT_MAST','PNCOMIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLREGI),'PNT_MAST','PNFLREGI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPROV),'PNT_MAST','PNFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PNT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PNT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PNT_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PNT_MAST','PNRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNDOC),'PNT_MAST','PNANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNPRO),'PNT_MAST','PNANNPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRP),'PNT_MAST','PNPRP');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PNT_MAST','PNRIFACC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNCODESE',this.w_PNCODESE,'PNCODUTE',this.w_PNCODUTE,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_PNDATREG,'PNCODCAU',this.w_PNCODCAU,'PNCOMPET',this.w_PNCOMPET,'PNTIPREG',this.w_PNTIPREG,'PNFLIVDF',this.w_PNFLIVDF,'PNNUMREG',this.w_PNNUMREG,'PNTIPDOC',this.w_PNTIPDOC,'PNPRD',this.w_PNPRD)
      insert into (i_cTable) (PNSERIAL,PNCODESE,PNCODUTE,PNNUMRER,PNDATREG,PNCODCAU,PNCOMPET,PNTIPREG,PNFLIVDF,PNNUMREG,PNTIPDOC,PNPRD,PNALFDOC,PNNUMDOC,PNDATDOC,PNALFPRO,PNNUMPRO,PNVALNAZ,PNCODVAL,PNPRG,PNCAOVAL,PNDESSUP,PNTIPCLF,PNCODCLF,PNTOTDOC,PNCOMIVA,PNFLREGI,PNFLPROV,UTCC,UTDC,UTCV,UTDV,PNRIFDOC,PNANNDOC,PNANNPRO,PNPRP,PNRIFACC &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_PNCODESE;
           ,this.w_PNCODUTE;
           ,this.w_PNNUMRER;
           ,this.w_PNDATREG;
           ,this.w_PNCODCAU;
           ,this.w_PNCOMPET;
           ,this.w_PNTIPREG;
           ,this.w_PNFLIVDF;
           ,this.w_PNNUMREG;
           ,this.w_PNTIPDOC;
           ,this.w_PNPRD;
           ,this.w_PNALFDOC;
           ,this.w_PNNUMDOC;
           ,this.w_PNDATDOC;
           ,this.w_PNALFPRO;
           ,this.w_PNNUMPRO;
           ,this.w_PNVALNAZ;
           ,this.w_PNCODVAL;
           ,this.w_PNPRG;
           ,this.w_PNCAOVAL;
           ,this.w_DESSUP;
           ,this.w_PNTIPCLF;
           ,this.w_PNCODCLF;
           ,this.w_PNTOTDOC;
           ,this.w_PNCOMIVA;
           ,this.w_PNFLREGI;
           ,this.w_PNFLPROV;
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,this.w_OSERIAL;
           ,this.w_PNANNDOC;
           ,this.w_PNANNPRO;
           ,this.w_PNPRP;
           ,"S";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Scrive il Dettaglio: Riga Cliente/Fornitore
    this.w_PNTIPCON = this.w_TIPCLF
    this.w_PNCODCON = this.w_CODCLF
    this.w_APPVAL = this.w_ACCONT
    this.w_APPSEZ = this.w_SEZCLF
    this.w_PNFLPART = this.w_PARTSN
    this.w_PNCODPAG = this.w_CODPAG
    this.w_PNCODAGE = this.w_CODAGE
    this.w_PNINICOM = cp_CharToDate("  -  -  ")
    this.w_PNFINCOM = cp_CharToDate("  -  -  ")
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- ATTENZIONE: 
    *     Se si entra nella Sezione w_CON_PAGA e viene lanciato il batch GSCG_BRC 
    *     il batch stesso ritorna il CPROWNUM aggiornato con l'ultima riga inserita.
    if this.w_CON_PAGA
      * --- Array utilizzato nella contabilizzazione Corrispettivi per tenere il totale pagamenti.
      *     In questo caso serve solo per congruenza
       
 DIMENSION TOTCALC (5) 
 CREATE CURSOR TmpSDoc (SERDOC C(10), ACCONTO N(18,4),TOTDOC N(18,4),CONCAF C(15),CONCLI C(15)) 
 INSERT INTO TmpSDoc (SERDOC, ACCONTO,TOTDOC,CONCAF,CONCLI) VALUES ( this.w_OSERIAL , this.w_ACCONT,0,"","" )
      * --- Se sul documento ci sono delle Contropartite per Pagamenti lancio il batch
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      if Not GSCG_BRC(this,"TmpsDoc", this.w_PNSERIAL, this.w_APPSEZ, this.w_PNINICOM, this.w_PNFINCOM, this.w_PNCODCAU, this.w_CCFLANAL , this.w_PNDESRIG ,@TOTCALC)
        * --- Raise
        i_Error=TotCalc(1)
        return
      else
        this.w_CPROWNUM = TotCalc(2)
        * --- Se ho un warning dalla generazione movimenti analitica lo
        *     inserisco come Warning
        if Not Empty( TotCalc(5) )
          this.w_oERRORLOG.AddMsgLog(TotCalc(5))     
        endif
      endif
      * --- Rimuovo il cursore...
      if Used("TmpSDoc")
         
 Select("TmpSDoc") 
 Use
      endif
    else
      * --- Scrive il Dettaglio: Riga Contropartite
      this.w_PNFLPART = "N"
      this.w_PNCODPAG = SPACE(5)
      this.w_PNCODAGE = SPACE(5)
      this.w_PNTIPCON = "G"
      this.w_PNCODCON = this.w_CODCON
      this.w_APPVAL = this.w_ACCONT
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_PNINICOM = this.w_INICOM
      this.w_PNFINCOM = this.w_FINCOM
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_PARTSN="A"
      * --- Aggiorna le Partite
      this.w_PTDATSCA = this.w_PNDATDOC
      if this.w_CLADOC <> "FA"
        this.w_PTNUMPAR = CANUMPAR("S", this.w_PNCODESE)
      else
        this.w_PTNUMPAR = CANUMPAR("N", this.w_PNCODESE, this.w_PNNUMDOC, this.w_PNALFDOC)
      endif
      this.w_PTCODAGE = this.w_CODAGE
      this.w_PTFLSOSP = " "
      this.w_PTMODPAG = this.oParentObject.w_TIPPAB
      this.w_PTFLCRSA = "A"
      this.w_PT_SEGNO = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_PTTOTIMP = this.w_ACCONT
      * --- Partita di Acconto
      * --- Insert into PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLIMPE"+",PTFLCRSA"+",PTTOTABB"+",PTDESRIG"+",PTCODAGE"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTROWORD');
        +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCLF),'PAR_TITE','PTTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCLF),'PAR_TITE','PTCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SEZCLF),'PAR_TITE','PT_SEGNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
        +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLCRSA),'PAR_TITE','PTFLCRSA');
        +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',1,'CPROWNUM',1,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_TIPCLF,'PTCODCON',this.w_CODCLF,'PT_SEGNO',this.w_SEZCLF,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PNCODVAL,'PTCAOVAL',this.w_PNCAOVAL,'PTCAOAPE',this.w_PNCAOVAL)
        insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLIMPE,PTFLCRSA,PTTOTABB,PTDESRIG,PTCODAGE,PTDATREG,PTNUMCOR &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,1;
             ,1;
             ,this.w_PTNUMPAR;
             ,this.w_PTDATSCA;
             ,this.w_TIPCLF;
             ,this.w_CODCLF;
             ,this.w_SEZCLF;
             ,this.w_PTTOTIMP;
             ,this.w_PNCODVAL;
             ,this.w_PNCAOVAL;
             ,this.w_PNCAOVAL;
             ,this.w_PNDATDOC;
             ,this.w_PNNUMDOC;
             ,this.w_PNALFDOC;
             ,this.w_PNDATDOC;
             ,this.w_PNTOTDOC;
             ,this.w_PTMODPAG;
             ,this.w_PTFLSOSP;
             ,this.w_PTBANAPP;
             ," ";
             ," ";
             ,this.w_PTBANNOS;
             ," ";
             ,"  ";
             ,this.w_PTFLCRSA;
             ,0;
             ,this.w_PNDESSUP;
             ,this.w_PTCODAGE;
             ,this.w_PNDATREG;
             ,this.w_PTNUMCOR;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Scrive Flag Contabilizzato sul Documento Origine
    this.w_MVRIFACC = this.w_PNSERIAL
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVRIFACC ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIFACC),'DOC_MAST','MVRIFACC');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
             )
    else
      update (i_cTable) set;
          MVRIFACC = this.w_MVRIFACC;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_OSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_SEGNALA = this.w_SEGNALA + ah_MsgFormat("Reg. %1 del. %2 cliente %3 %0",ALLTRIM(STR(this.w_PNNUMRER,15)),DTOC(this.w_PNDATREG),Alltrim(this.w_CODCLF))
    this.w_OKDOC = this.w_OKDOC + 1
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Dettaglio P.N. e Saldi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    if this.w_PNCODVAL<>this.w_PNVALNAZ
      * --- Se arriva un Importo in Valuta e il Documento e' in Valuta Converte in Moneta di Conto
      this.w_APPVAL = VAL2MON(this.w_APPVAL,this.w_PNCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
    endif
    * --- Inverte la sezione se Negativo..
    this.w_APPSEZ = IIF(this.w_APPVAL<0, IIF(this.w_APPSEZ="D", "A", "D"), this.w_APPSEZ)
    if this.w_APPSEZ="D"
      this.w_PNIMPDAR = ABS(this.w_APPVAL)
      this.w_PNIMPAVE = 0
    else
      this.w_PNIMPDAR = 0
      this.w_PNIMPAVE = ABS(this.w_APPVAL)
    endif
    * --- Eventuali Righe non Valorizzate
    this.w_PNDESRIG = this.w_PNDESSUP
    if Empty(this.w_PNDESRIG) and Not EMPTY(this.w_CCDESRIG)
       
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.w_PNCODAGE) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=Alltrim(this.w_PTNUMPAR) 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=DTOC(this.w_PTDATSCA)
      this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
    endif
    this.w_PNFLZERO = IIF(this.w_PNIMPDAR=0 AND this.w_PNIMPAVE=0, "S", " ")
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNCODPAG"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNINICOM"+",PNFINCOM"+",PNFLABAN"+",PNIMPIND"+",PNCODAGE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODPAG),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC("+"),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLABAN),'PNT_DETT','PNFLABAN');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_DETT','PNIMPIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODAGE),'PNT_DETT','PNCODAGE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',this.w_PNFLPART,'PNFLZERO',this.w_PNFLZERO,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCODCAU,'PNCODPAG',this.w_PNCODPAG)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNCODPAG,PNFLSALD,PNFLSALI,PNFLSALF,PNINICOM,PNFINCOM,PNFLABAN,PNIMPIND,PNCODAGE &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,this.w_PNFLPART;
           ,this.w_PNFLZERO;
           ,this.w_PNDESRIG;
           ,this.w_PNCODCAU;
           ,this.w_PNCODPAG;
           ,"+";
           ," ";
           ," ";
           ,this.w_PNINICOM;
           ,this.w_PNFINCOM;
           ,this.w_PNFLABAN;
           ,0;
           ,this.w_PNCODAGE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_PNFLABAN = SPACE(6)
    * --- Aggiorna Saldo
    * --- Try
    local bErr_04026870
    bErr_04026870=bTrsErr
    this.Try_04026870()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04026870
    * --- End
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
      +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
          +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
             )
    else
      update (i_cTable) set;
          SLDARPER = SLDARPER + this.w_PNIMPDAR;
          ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
          &i_ccchkf. ;
       where;
          SLTIPCON = this.w_PNTIPCON;
          and SLCODICE = this.w_PNCODCON;
          and SLCODESE = this.w_PNCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Gestione Analitica
    if this.w_CCFLANAL="S" And this.w_PNTIPCON="G" And g_PERCCR="S"
      this.w_MESS_ERR = GSAR_BRA(This,this.w_PNSERIAL, this.w_CPROWNUM, this.w_PNTIPCON, this.w_PNCODCON, this.w_APPVAL)
      if Not EMPTY(this.w_MESS_ERR)
        ah_ErrorMsg("%1",,"",this.w_MESS_ERR)
      endif
    endif
  endproc
  proc Try_04026870()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Cursori di Appoggio
    if USED("CONTADOC")
      SELECT CONTADOC
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='PNT_DETT'
    this.cWorkTables[6]='SALDICON'
    this.cWorkTables[7]='PAR_TITE'
    this.cWorkTables[8]='AZIENDA'
    this.cWorkTables[9]='CONTI'
    this.cWorkTables[10]='BAN_CONTI'
    this.cWorkTables[11]='CON_PAGA'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_CON_PAGA')
      use in _Curs_CON_PAGA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
