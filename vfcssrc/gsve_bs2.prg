* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bs2                                                        *
*              Dettaglio fatture differite                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_33]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2000-04-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bs2",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bs2 as StdBatch
  * --- Local variables
  pOper = space(1)
  w_MESS = space(10)
  w_OK = .f.
  w_WARN = .f.
  w_RIFDOC = space(30)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dettaglio Fatture Differite da (GSVE_KSF)
    * --- U = Conferma la Maschera (Update End) ; A = Abbandona la Maschera (Edit Aborted)
    if USED("APPCUR")
      SELECT APPCUR
      USE
    endif
    this.oParentObject.oParentObject.w_FLPACK=" "
    do case
      case this.pOper="U"
        * --- Conferma Import
        * --- Salva le righe del dettaglio su GeneApp
        this.oParentObject.NotifyEvent("ZOOMDETT before query")
        NM = this.oParentObject.w_ZoomMast.cCursor
        if USED("GeneApp")
          this.w_OK = .T.
          this.w_WARN = .F.
          * --- Cerca se selezionati Documenti Importabili per Intero ma selezionati parzialmente
          SELECT GeneApp
          GO TOP
          SCAN FOR CPROWNUM<>9999
          SELECT (NM)
          LOCATE FOR XCHK=1 AND MVSERIAL=GeneApp.MVSERIAL AND ; 
 (NVL(MVFLSCAF," ")="S" OR NVL(MVFLFOSC," ")="S" OR NVL(MVSPEIMB, 0)<>0 OR NVL(MVSPETRA, 0)<>0 OR ; 
 NVL(MVACCONT, 0)<>0)
          if FOUND()
            this.w_RIFDOC = Ah_Msgformat("Documento di origine: n. %1 del %2", ALLTRIM(STR(NVL(MVNUMDOC,0),15))+IIF(EMPTY(NVL(MVALFDOC,"")),"","/"+Alltrim(MVALFDOC)), DTOC(CP_TODATE(MVDATDOC)) )
            do case
              case NVL(MVFLSCAF," ")="S" 
                this.w_MESS = "%1%0Un documento con scadenze fissate non pu� essere fatturato parzialmente"
                this.w_OK = .F.
              case NVL(MVFLFOSC," ")="S"
                this.w_MESS = "%1%0Un documento con sconti di piede forzati non pu� essere fatturato parzialmente"
                this.w_OK = .F.
              case NVL(MVACCONT, 0)<>0
                this.w_MESS = "%1%0Un documento con acconti contestuali non pu� essere fatturato parzialmente"
                this.w_OK = .F.
            endcase
            if this.w_OK=.T.
              if NVL(MVSPEIMB, 0)<>0 OR NVL(MVSPETRA, 0)<>0
                this.w_WARN = .T.
                this.w_MESS = "Attenzione:%0Documento con spese accessorie importato parzialmente%0(Solo warning)"
              endif
            else
              ah_ErrorMsg(this.w_MESS,,"", this.w_RIFDOC)
              this.oParentObject.w_TESTERR=.F.
              i_retcode = 'stop'
              return
            endif
          endif
          SELECT GeneApp
          ENDSCAN 
          if this.w_OK=.T. AND this.w_WARN=.T.
            ah_ErrorMsg(this.w_MESS)
          endif
          * --- Se controlli OK Unisce I documenti Deselezionati per Intero con Quelli delle Singole Righe deselezionate
           SELECT MVSERIAL, CPROWNUM FROM GeneApp WHERE MVSERIAL NOT IN (SELECT MVSERIAL FROM (NM) WHERE XCHK=0) UNION ; 
 SELECT MVSERIAL, 9999 AS CPROWNUM FROM (NM) WHERE XCHK = 0 INTO CURSOR GeneApp
        else
          SELECT MVSERIAL, 9999 AS CPROWNUM FROM (NM) WHERE XCHK = 0 INTO CURSOR GeneApp
        endif
        if RECCOUNT("GeneApp")<>0
          this.oParentObject.oParentObject.w_FLPACK="D"
        else
          this.oParentObject.oParentObject.w_FLPACK=" "
        endif
      case this.pOper="A"
        * --- Abbandona Import (chiude i Cursori)
        if USED("GeneApp")
          SELECT GeneApp
          USE
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
