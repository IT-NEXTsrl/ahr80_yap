* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste1bps                                                        *
*              Analisi scaduto per partita                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_380]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-02                                                      *
* Last revis.: 2000-06-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste1bps",oParentObject)
return(i_retval)

define class tgste1bps as StdBatch
  * --- Local variables
  w_TIPORD = space(1)
  w_APPO = space(10)
  w_APPO1 = space(10)
  w_APPO2 = space(10)
  w_CANC = .f.
  w_CODAGE = space(5)
  w_PUNT = 0
  w_SALREC = 0
  w_SALDO = 0
  w_PARTITA = space(31)
  w_OLDPAR = space(31)
  w_TOTALE = 0
  w_TOT1 = 0
  w_FLCRSA = space(1)
  w_OREPO = space(50)
  w_SERIAL = space(10)
  w_STAMPA = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora stampa Partite/Scadenze (da GSTE_SCA)
    * --- Dichiaro le variabili della maschera
    * --- Ordine di Stampa
    this.oParentObject.w_FLGSOS = IIF(this.oParentObject.w_FLSOSP $ "TN", " ", "S")
    this.oParentObject.w_FLGNSO = IIF(this.oParentObject.w_FLSOSP $ "TS", " ", "S")
    vq_exec("query\GSTE_SCA", this, "PARTITE")
    this.w_OLDPAR = "1900"
    SELECT PARTITE
    GO BOTTOM
    do while NOT BOF()
      this.w_PARTITA = PARTITE.NUMPAR
      this.w_FLCRSA = PARTITE.FLCRSA
      if this.w_PARTITA<>this.w_OLDPAR
        this.w_TOTALE = 0
        this.w_OLDPAR = PARTITE.NUMPAR
      endif
      if this.w_FLCRSA ="C"
        this.w_TOTALE = this.w_TOTALE+PARTITE.TOTIMP
      endif
      REPLACE PARTITE.PTTOTIMP WITH this.w_TOTALE
      SKIP -1
    enddo
    SELECT PARTITE
    this.w_OLDPAR = "1900"
    GO TOP
    do while NOT EOF()
      this.w_PARTITA = PARTITE.NUMPAR
      this.w_FLCRSA = PARTITE.FLCRSA
      if this.w_PARTITA<>this.w_OLDPAR
        this.w_TOTALE = 0
        this.w_TOT1 = 0
        this.w_OLDPAR = PARTITE.NUMPAR
      endif
      if this.w_FLCRSA ="C"
        this.w_TOT1 = this.w_TOT1+PARTITE.TOTIMP
      endif
      if this.w_FLCRSA ="S"
        this.w_TOTALE = this.w_TOTALE+PARTITE.TOTIMP
      endif
      REPLACE PARTITE.PTTOTIMP1 WITH this.w_TOTALE
      REPLACE PARTITE.PTTOTIMP2 WITH this.w_TOT1
      SKIP
    enddo
    SELECT * FROM PARTITE INTO CURSOR TEMP
    SELECT PARTITE
    GO TOP
    SCAN
    this.w_STAMPA = "A"
    this.w_APPO2 = DTOS(CP_TODATE(PARTITE.DATSCA))+LEFT(ALLTRIM(PARTITE.NUMPAR)+SPACE(31),31)+PARTITE.TIPCON+LEFT(ALLTRIM(PARTITE.CODCON)+SPACE(15),15)+PARTITE.CODVAL
    this.w_SALDO = NVL(PARTITE.TOTIMP,0)
    this.w_FLCRSA = PARTITE.FLCRSA 
    SELECT TEMP
    LOCATE FOR DTOS(CP_TODATE(TEMP.DATSCA))+LEFT(ALLTRIM(TEMP.NUMPAR)+SPACE(31),31)+TEMP.TIPCON+LEFT(ALLTRIM(TEMP.CODCON)+SPACE(15),15)+TEMP.CODVAL=this.w_APPO2 AND (NVL(TEMP.TOTIMP,0))=this.w_SALDO AND TEMP.FLCRSA<>this.w_FLCRSA
    if FOUND()
      SELECT PARTITE
      if PARTITE.FLCRSA="C"
        REPLACE PARTITE.FLCRSA WITH this.w_STAMPA
      endif
    endif
    ENDSCAN
    * --- Stampa Standard
    * --- Attenzione: Quelle di chiusura non hanno riferimenti all'Agente, prende quello presente nella partita di apertura
    this.w_APPO = "XXXXX"
    this.w_CANC = .F.
    this.w_CODAGE = SPACE(5)
    SELECT PARTITE
    GO TOP
    SCAN FOR NOT DELETED() AND TIPCON<>"K"
    this.w_APPO1 = DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(31),31)+TIPCON+LEFT(ALLTRIM(CODCON)+SPACE(15),15)+CODVAL
    * --- Nuova Partita/Scadenza
    if this.w_APPO<>this.w_APPO1
      this.w_CODAGE = NVL(CODAGE, SPACE(5))
      if EMPTY(this.w_CODAGE)
        * --- Se Manca l'Agente cerca la prima scadenza (di creazione) con un agente valido
        this.w_SALREC = RECNO()
        SCAN WHILE this.w_APPO1=DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(31),31)+TIPCON+LEFT(ALLTRIM(CODCON)+SPACE(15),15)+CODVAL
        if NOT EMPTY(NVL(CODAGE,"")) AND NVL(FLCRSA," ")="C"
          this.w_CODAGE = NVL(CODAGE, SPACE(5))
        endif
        ENDSCAN
        GOTO this.w_SALREC
        REPLACE CODAGE WITH this.w_CODAGE
      endif
      this.w_APPO = this.w_APPO1
    else
      REPLACE CODAGE WITH this.w_CODAGE
    endif
    ENDSCAN
    GO TOP
    * --- Attenzione: Quelle di chiusura non hanno riferimenti all'Agente, prende quello presente nella partita di apertura
    this.w_APPO = "XXXXX"
    this.w_CANC = .F.
    this.w_CODAGE = SPACE(5)
    SELECT PARTITE
    GO TOP
    SCAN FOR NOT DELETED() AND TIPCON<>"K"
    this.w_APPO1 = DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(31),31)+TIPCON+LEFT(ALLTRIM(CODCON)+SPACE(15),15)+CODVAL
    * --- Nuova Partita/Scadenza
    if this.w_APPO<>this.w_APPO1
      this.w_CODAGE = NVL(CODAGE, SPACE(5))
      this.w_CANC = IIF(this.w_CODAGE<>this.oParentObject.w_AGENTE AND NOT EMPTY(this.oParentObject.w_AGENTE), .T., .F.)
      this.w_APPO = this.w_APPO1
    endif
    if this.w_CANC=.T.
      * --- Elimina le partite non associate ad alcun Agente
      DELETE
    endif
    ENDSCAN
    * --- Elabora i Cursori e ritorna il Temporaneo per la Stampa
    SELECT PARTITE
    GO TOP
    SELECT ;
    CODVAL AS PTCODVAL, ;
    CP_TODATE(DATSCA) AS PTDATSCA, ;
    NUMPAR AS PTNUMPAR, ;
    TIPCON AS PNTIPCON, ;
    CODCON AS PNCODCON, ;
    FLCRSA AS PTFLCRSA, ;
    DATDOC AS PNDATDOC, ;
    NUMDOC AS PNNUMDOC, ;
    ALFDOC AS PNALFDOC, ;
    NVL(CODAGE, SPACE(5)) AS PTCODAGE, ;
    FLDAVE AS FLDAVE, ;
    TOTIMP AS TOTIMP, ;
    MODPAG AS PTMODPAG, ;
    BANAPP AS PTBANAPP, ;
    NUMDIS AS PTNUMDIS, FLSOSP AS PTFLSOSP, ;
    DECTOT AS VADECTOT, ;
    SIMVAL AS VASIMVAL, ;
    PTSERIAL AS PTSERIAL, PTROWORD AS PTROWORD, CPROWNUM AS CPROWNUM, ;
    FLRAGG AS PTFLRAGG, ;
    CAOVAL AS PTCAOVAL, ;
    PTTOTIMP, ;
    PTTOTIMP1, ;
    PTTOTIMP2 ;
    FROM PARTITE WHERE NOT DELETED() INTO CURSOR PARTITE1
    if USED("PARTITE")
      SELECT PARTITE
      USE
    endif
    SELECT * FROM PARTITE1 ;
     INTO CURSOR __tmp__ ORDER BY 5,4,1,3,2,6,7,8,9
    * --- Passo le variabili al report
    L_SOSP=this.oParentObject.w_FLSOSP
    L_NUMPAR=this.oParentObject.w_NUMPAR
    L_ADOINI=this.oParentObject.w_ADOFIN
    L_ADOFIN=this.oParentObject.w_ADOFIN
    L_NDOINI=this.oParentObject.w_NDOINI
    L_SCAINI=this.oParentObject.w_SCAINI
    L_NDOFIN=this.oParentObject.w_NDOFIN
    L_SCAFIN=this.oParentObject.w_SCAFIN
    L_DDOINI=this.oParentObject.w_DDOINI
    L_BANNOS=this.oParentObject.w_BANNOS
    L_DDOFIN=this.oParentObject.w_DDOFIN
    L_VALUTA=this.oParentObject.w_VALUTA
    L_AGENTE=this.oParentObject.w_AGENTE
    L_BANAPP=this.oParentObject.w_BANAPP
    L_FORSEL=IIF(this.oParentObject.w_TIPCON="F", this.oParentObject.w_CODCON, " ")
    L_FORSEL1=IIF(this.oParentObject.w_TIPCON="F", this.oParentObject.w_CODFIN, " ")
    L_CLISEL=IIF(this.oParentObject.w_TIPCON="C", this.oParentObject.w_CODCON, " ")
    L_CLISEL1=IIF(this.oParentObject.w_TIPCON="C", this.oParentObject.w_CODFIN, " ")
    L_SCACLF=this.oParentObject.w_TIPCON
    L_DATSTA=this.oParentObject.w_DATSTA
    * --- Lancio il report
    CP_CHPRN( ALLTRIM("QUERY\GSTE_SCA.FRX"), " ", this)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
