* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bo4                                                        *
*              Crea anagrafica cliente                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-10                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCCCODICE,pANCODICE,pCCMASCON,pCCCATCON,pCCCODLIN,pCCCODIVA,pCCCODPAG,pCCFLRITE,pCCCODIRP,pCCNAZION,pCCPARIVA,pAzione,pArrCalc,pTIPOCL,pCCFLPRIV,pCCFLESIG,pCCCODFIS,pCCIVASOS,pCliFor
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bo4",oParentObject,m.pCCCODICE,m.pANCODICE,m.pCCMASCON,m.pCCCATCON,m.pCCCODLIN,m.pCCCODIVA,m.pCCCODPAG,m.pCCFLRITE,m.pCCCODIRP,m.pCCNAZION,m.pCCPARIVA,m.pAzione,m.pArrCalc,m.pTIPOCL,m.pCCFLPRIV,m.pCCFLESIG,m.pCCCODFIS,m.pCCIVASOS,m.pCliFor)
return(i_retval)

define class tgsar_bo4 as StdBatch
  * --- Local variables
  pCCCODICE = space(15)
  pANCODICE = space(15)
  pCCMASCON = space(15)
  pCCCATCON = space(5)
  pCCCODLIN = space(3)
  pCCCODIVA = space(5)
  pCCCODPAG = space(5)
  pCCFLRITE = space(1)
  pCCCODIRP = space(5)
  pCCNAZION = space(3)
  pCCPARIVA = space(12)
  pAzione = space(10)
  pArrCalc = space(40)
  pTIPOCL = space(5)
  pCCFLPRIV = space(1)
  pCCFLESIG = space(1)
  pCCCODFIS = space(16)
  pCCIVASOS = space(1)
  pCliFor = space(1)
  w_CCCODICE = space(15)
  w_ANCODICE = space(15)
  w_CCMASCON = space(15)
  w_CCCATCON = space(5)
  w_CCCODLIN = space(3)
  w_CCCODIVA = space(5)
  w_CCCODPAG = space(5)
  w_CCFLRITE = space(1)
  w_CCCODIRP = space(5)
  w_CCNAZION = space(3)
  w_CCPARIVA = space(12)
  w_AZIONE = space(10)
  w_CODCL = space(15)
  w_TIPCON = space(1)
  w_TIPCON = space(1)
  w_CCDESCRI = space(40)
  w_CCDESCR2 = space(40)
  w_CCINDIRI = space(35)
  w_CCINDIR2 = space(35)
  w_CC___CAP = space(8)
  w_CCLOCALI = space(30)
  w_CCPROVIN = space(2)
  w_CCCODFIS = space(16)
  w_CCTELEFO = space(18)
  w_CCTELFAX = space(18)
  w_CCINDWEB = space(50)
  w_CC_EMAIL = space(50)
  w_CC_EMPEC = space(50)
  w_CCDTINVA = ctod("  /  /  ")
  w_CCDTOBSO = ctod("  /  /  ")
  w_CCCODZON = space(3)
  w_CCCODVAL = space(3)
  w_CCCODAGE = space(5)
  w_CCCODSAL = space(5)
  w_CCCAURIT = space(1)
  w_CCNUMLIS = space(5)
  w_CCCATCOM = space(3)
  w_CCCODBAN = space(10)
  w_CCCODBA2 = space(15)
  w_SESSO = space(1)
  w_NOTE = space(0)
  w_CHKERR = .f.
  w_NOCHKSTA = space(1)
  w_NOCHKMAI = space(1)
  w_NOCHKPEC = space(1)
  w_NOCHKFAX = space(1)
  w_ANCODSTU = space(6)
  w_CODSTU = space(3)
  w_CC_SKYPE = space(50)
  w_CODICE = space(15)
  w_ERRF = .f.
  w_AUTN = 0
  w_CODN = space(15)
  w_LENC = 0
  w_CONCON = space(1)
  w_INDWEB = space(254)
  w_NOSOGGET = space(2)
  w_NONUMCEL = space(18)
  w_NOCOGNOM = space(50)
  w_NO__NOME = space(50)
  w_NOLOCNAS = space(30)
  w_NOPRONAS = space(2)
  w_NODATNAS = ctod("  /  /  ")
  w_NONUMCAR = space(18)
  w_NOCATCOM = space(5)
  w_ANAGRAFICA = .NULL.
  w_NUMLIS = space(5)
  w_SOGG = space(2)
  w_NCPERSON = space(125)
  w_CPROWNUM = 0
  w_NC_EMAIL = space(254)
  w_NC_EMPEC = space(254)
  w_NCTELEFO = space(20)
  w_NCNUMCEL = space(20)
  w_NC_SKYPE = space(50)
  w_MCPROSTU = space(8)
  * --- WorkFile variables
  CONTI_idx=0
  OFF_NOMI_idx=0
  DES_DIVE_idx=0
  TRI_BUTI_idx=0
  PAR_OFFE_idx=0
  MASTRI_idx=0
  NOM_CONT_idx=0
  CONTATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea L'anagrafica Clienti (da GSOF_KCC)
    * --- Parametri
    * --- Codice nominativo
    * --- Dati per la creazione del cliente
    * --- Azioni da eseguire : "CONTROLLI" , "GENERAZIONE" , "ENTRAMBI"
    * --- Passato per riferimento, utilizzato per restituire valori
    * --- Tipologia clientela
    if TYPE("pCliFor")="C"
      * --- Cliente o fornitore come specificato nei parametri
      this.w_TIPCON = this.pCliFor
    else
      this.w_TIPCON = "C"
    endif
    * --- Valore restituito dalla funzione: se .t. c'� stato un errore
    * --- --Variabili per docm
    this.w_CCCODICE = this.pCCCODICE
    * --- Read from OFF_NOMI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" OFF_NOMI where ";
            +"NOCODICE = "+cp_ToStrODBC(this.w_CCCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            NOCODICE = this.w_CCCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCDESCRI = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
      this.w_CCDESCR2 = NVL(cp_ToDate(_read_.NODESCR2),cp_NullValue(_read_.NODESCR2))
      this.w_CCINDIRI = NVL(cp_ToDate(_read_.NOINDIRI),cp_NullValue(_read_.NOINDIRI))
      this.w_CCINDIR2 = NVL(cp_ToDate(_read_.NOINDI_2),cp_NullValue(_read_.NOINDI_2))
      this.w_CC___CAP = NVL(cp_ToDate(_read_.NO___CAP),cp_NullValue(_read_.NO___CAP))
      this.w_CCLOCALI = NVL(cp_ToDate(_read_.NOLOCALI),cp_NullValue(_read_.NOLOCALI))
      this.w_CCPROVIN = NVL(cp_ToDate(_read_.NOPROVIN),cp_NullValue(_read_.NOPROVIN))
      this.w_CCNAZION = NVL(cp_ToDate(_read_.NONAZION),cp_NullValue(_read_.NONAZION))
      this.w_CCCODFIS = NVL(cp_ToDate(_read_.NOCODFIS),cp_NullValue(_read_.NOCODFIS))
      this.w_CCPARIVA = NVL(cp_ToDate(_read_.NOPARIVA),cp_NullValue(_read_.NOPARIVA))
      this.w_CCTELEFO = NVL(cp_ToDate(_read_.NOTELEFO),cp_NullValue(_read_.NOTELEFO))
      this.w_CCTELFAX = NVL(cp_ToDate(_read_.NOTELFAX),cp_NullValue(_read_.NOTELFAX))
      this.w_CCINDWEB = NVL(cp_ToDate(_read_.NOINDWEB),cp_NullValue(_read_.NOINDWEB))
      this.w_CC_EMAIL = NVL(cp_ToDate(_read_.NO_EMAIL),cp_NullValue(_read_.NO_EMAIL))
      this.w_CC_EMPEC = NVL(cp_ToDate(_read_.NO_EMPEC),cp_NullValue(_read_.NO_EMPEC))
      this.w_CCDTINVA = NVL(cp_ToDate(_read_.NODTINVA),cp_NullValue(_read_.NODTINVA))
      this.w_CCDTOBSO = NVL(cp_ToDate(_read_.NODTOBSO),cp_NullValue(_read_.NODTOBSO))
      this.w_CCCODZON = NVL(cp_ToDate(_read_.NOCODZON),cp_NullValue(_read_.NOCODZON))
      this.w_CCCODLIN = NVL(cp_ToDate(_read_.NOCODLIN),cp_NullValue(_read_.NOCODLIN))
      this.w_CCCODVAL = NVL(cp_ToDate(_read_.NOCODVAL),cp_NullValue(_read_.NOCODVAL))
      this.w_CCCODAGE = NVL(cp_ToDate(_read_.NOCODAGE),cp_NullValue(_read_.NOCODAGE))
      this.w_CCCODSAL = NVL(cp_ToDate(_read_.NOCODSAL),cp_NullValue(_read_.NOCODSAL))
      this.w_CCNUMLIS = NVL(cp_ToDate(_read_.NONUMLIS),cp_NullValue(_read_.NONUMLIS))
      this.w_CCCODBAN = NVL(cp_ToDate(_read_.NOCODBAN),cp_NullValue(_read_.NOCODBAN))
      this.w_CCCODBA2 = NVL(cp_ToDate(_read_.NOCODBA2),cp_NullValue(_read_.NOCODBA2))
      this.w_CCCATCOM = NVL(cp_ToDate(_read_.NOCATCOM),cp_NullValue(_read_.NOCATCOM))
      this.w_NOSOGGET = NVL(cp_ToDate(_read_.NOSOGGET),cp_NullValue(_read_.NOSOGGET))
      this.w_NONUMCEL = NVL(cp_ToDate(_read_.NONUMCEL),cp_NullValue(_read_.NONUMCEL))
      this.w_NOCOGNOM = NVL(cp_ToDate(_read_.NOCOGNOM),cp_NullValue(_read_.NOCOGNOM))
      this.w_NO__NOME = NVL(cp_ToDate(_read_.NO__NOME),cp_NullValue(_read_.NO__NOME))
      this.w_NOLOCNAS = NVL(cp_ToDate(_read_.NOLOCNAS),cp_NullValue(_read_.NOLOCNAS))
      this.w_NOPRONAS = NVL(cp_ToDate(_read_.NOPRONAS),cp_NullValue(_read_.NOPRONAS))
      this.w_NODATNAS = NVL(cp_ToDate(_read_.NODATNAS),cp_NullValue(_read_.NODATNAS))
      this.w_NONUMCAR = NVL(cp_ToDate(_read_.NONUMCAR),cp_NullValue(_read_.NONUMCAR))
      this.w_SESSO = NVL(cp_ToDate(_read_.NO_SESSO),cp_NullValue(_read_.NO_SESSO))
      this.w_NOCHKSTA = NVL(cp_ToDate(_read_.NOCHKSTA),cp_NullValue(_read_.NOCHKSTA))
      this.w_NOCHKMAI = NVL(cp_ToDate(_read_.NOCHKMAI),cp_NullValue(_read_.NOCHKMAI))
      this.w_NOCHKPEC = NVL(cp_ToDate(_read_.NOCHKPEC),cp_NullValue(_read_.NOCHKPEC))
      this.w_NOCHKFAX = NVL(cp_ToDate(_read_.NOCHKFAX),cp_NullValue(_read_.NOCHKFAX))
      this.w_NOTE = NVL(cp_ToDate(_read_.NO__NOTE),cp_NullValue(_read_.NO__NOTE))
      this.w_SOGG = NVL(cp_ToDate(_read_.NOSOGGET),cp_NullValue(_read_.NOSOGGET))
      this.w_CC_SKYPE = NVL(cp_ToDate(_read_.NO_SKYPE),cp_NullValue(_read_.NO_SKYPE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ANCODICE = this.pANCODICE
    this.w_CCMASCON = this.pCCMASCON
    this.w_CCCATCON = this.pCCCATCON
    if !EMPTY( this.pCCCODLIN )
      this.w_CCCODLIN = this.pCCCODLIN
    endif
    this.w_CCCODIVA = this.pCCCODIVA
    this.w_CCCODPAG = this.pCCCODPAG
    this.w_CCFLRITE = this.pCCFLRITE
    this.w_CCCODIRP = this.pCCCODIRP
    if !EMPTY( this.pCCNAZION )
      this.w_CCNAZION = this.pCCNAZION
    endif
    this.w_CCPARIVA = this.pCCPARIVA
    this.w_CCCODFIS = this.pCCCODFIS
    this.w_AZIONE = IIF( EMPTY( this.pAZIONE ) , "ENTRAMBI" , this.pAZIONE )
    if EMPTY(this.w_CCCODLIN)
      this.w_CCCODLIN = g_CODLIN
    endif
    this.w_CCCODVAL = LEFT(this.w_CCCODVAL, 3)
    * --- Read from TRI_BUTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TRI_BUTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRI_BUTI_idx,2],.t.,this.TRI_BUTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TRCAUPRE2"+;
        " from "+i_cTable+" TRI_BUTI where ";
            +"TRCODTRI = "+cp_ToStrODBC(this.w_CCCODIRP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TRCAUPRE2;
        from (i_cTable) where;
            TRCODTRI = this.w_CCCODIRP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCCAURIT = NVL(cp_ToDate(_read_.TRCAUPRE2),cp_NullValue(_read_.TRCAUPRE2))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- w_CHKERR, Se .T. Notifica alla .M. CheckForm il Fallimento dell'operazione, riportando l'input sulla Maschera
    this.w_NUMLIS = this.w_CCNUMLIS
    this.w_NOSOGGET = IIF (this.w_NOSOGGET="PF","S","N")
    this.w_LENC = MIN(LEN(ALLTRIM(p_CLF)), 15)
    * --- Controlli del Form
    if this.w_AZIONE = "CONTROLLI" OR this.w_AZIONE = "ENTRAMBI"
      this.w_CHKERR = .F.
      if EMPTY(this.w_CCMASCON)
        AH_ErrorMsg("Mastro contabile obbligatorio",48)
        this.w_CHKERR = .T.
        i_retcode = 'stop'
        i_retval = ""
        return
      endif
      if EMPTY(this.w_CCCATCON) AND this.w_TIPCON="C" 
        * --- Campo obbligatorio solo per i clienti
        AH_ErrorMsg("Categoria contabile obbligatoria",48)
        this.w_CHKERR = .T.
        i_retcode = 'stop'
        i_retval = ""
        return
      endif
      if EMPTY(this.w_CCCODLIN)
        AH_ErrorMsg("Codice lingua obbligatorio",48)
        this.w_CHKERR = .T.
        i_retcode = 'stop'
        i_retval = ""
        return
      endif
      * --- Select from CONTI
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ANCODICE  from "+i_cTable+" CONTI ";
            +" where ANTIPCON="+cp_ToStrODBC(this.w_TIPCON)+" AND ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)+"";
             ,"_Curs_CONTI")
      else
        select ANCODICE from (i_cTable);
         where ANTIPCON=this.w_TIPCON AND ANCODICE=this.w_ANCODICE;
          into cursor _Curs_CONTI
      endif
      if used('_Curs_CONTI')
        select _Curs_CONTI
        locate for 1=1
        do while not(eof())
        AH_ErrorMsg("Codice %1 gi� presente in anagrafica",48,,IIF(this.w_TIPCON="C","cliente","fornitore"))
        this.w_CHKERR = .T.
        i_retcode = 'stop'
        i_retval = ""
        return
          select _Curs_CONTI
          continue
        enddo
        use
      endif
      * --- CONTROLLA che non esistano clienti con la stessa partita iva del cliente che si st� creando
      if EMPTY(this.w_CCPARIVA) AND this.w_SOGG="EN"
        if NOT AH_YESNO ("Il %1 che verr� creato non ha la partita iva.%0Vuoi proseguire ?", "", IIF(this.w_TIPCON="C","cliente","fornitore"))
          this.oParentObject.w_CHKERR = .T.
          i_retcode = 'stop'
          i_retval = ""
          return
        endif
      endif
    endif
    if this.w_AZIONE = "GENERAZIONE" OR this.w_AZIONE = "ENTRAMBI"
      * --- Previene eventuale inserimento di Codici non numerici dal calcolo Autonumber
      this.w_AUTN = 0
      this.w_CODN = ALLTRIM(this.w_ANCODICE)
      FOR p_POS=1 TO LEN(this.w_CODN)
      this.w_AUTN = IIF(SUBSTR(this.w_CODN, p_POS, 1) $ "0123456789", this.w_AUTN, 1)
      ENDFOR
      * --- Try
      local bErr_038B9E38
      bErr_038B9E38=bTrsErr
      this.Try_038B9E38()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        AH_ErrorMsg("Errore: il %1 non pu� essere creato%0%2",48,,IIF(this.w_TIPCON="C","cliente","fornitore"), MESSAGE())
        this.w_CHKERR = .T.
      endif
      bTrsErr=bTrsErr or bErr_038B9E38
      * --- End
      if NOT this.w_CHKERR
        * --- Se non ci sono errori restituisce il codice del cliente
        if VARTYPE( this.oparentobject.w_ANCODICE ) = "C"
          this.oparentobject.w_ANCODICE = this.w_ANCODICE
        endif
        * --- Se alcuni campi sono stati variati durante la generazione, il nominativo viene allineato
        GSAR_BOL(this,"V",this.w_ANCODICE, this.w_TIPCON)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_CHKERR
        i_retcode = 'stop'
        i_retval = ""
        return
      else
        i_retcode = 'stop'
        i_retval = this.w_ANCODICE
        return
      endif
    endif
  endproc
  proc Try_038B9E38()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiorna il progressivo cliente (se codifica numerica)
    * --- begin transaction
    cp_BeginTrs()
    this.w_CODICE = this.w_CODN
    if g_CFNUME="S" AND this.w_AUTN=0
      i_nConn = i_TableProp[this.CONTI_IDX,3]
      if IsAlt() AND this.w_TIPCON<>"C"
        cp_NextTableProg(this,i_nConn,"PRNUFO","i_CODAZI,w_CODICE") 
      else
        cp_NextTableProg(this,i_nConn,"PRNUCL","i_CODAZI,w_CODICE") 
      endif
      this.w_ANCODICE = RIGHT(this.w_CODICE, this.w_LENC)
    endif
    this.w_CONCON = IIF(g_ISONAZ="ITA", "E", "1")
    this.w_INDWEB = IIF(UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", LEFT(this.w_CCINDWEB, 50), this.w_CCINDWEB)
    if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
      this.w_CCINDIRI = left ( this.w_CCINDIRI , 35)
      this.w_CCINDIR2 = left ( this.w_CCINDIR2 , 35)
      * --- Insert into CONTI
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ANTIPCON"+",ANCODICE"+",ANDESCRI"+",ANDESCR2"+",AN___CAP"+",ANPROVIN"+",ANINDIRI"+",ANINDIR2"+",ANCODVAL"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",AFFLINTR"+",AN__NOTE"+",AN_EMAIL"+",AN_EMPEC"+",AN_SESSO"+",ANBOLFAT"+",ANCATCON"+",ANCODAG1"+",ANCODFIS"+",ANCODIVA"+",ANCODLIN"+",ANCODZON"+",ANCONCON"+",ANCONSUP"+",ANDTINVA"+",ANDTOBSO"+",ANFLBLVE"+",ANFLCODI"+",ANFLCONA"+",ANFLESIG"+",ANFLFIDO"+",ANFLGAVV"+",ANFLRAGG"+",ANINDWEB"+",ANLOCALI"+",ANLOCNAS"+",ANNAZION"+",ANNUMCEL"+",ANNUMCOR"+",ANPARIVA"+",ANPARTSN"+",ANPERFIS"+",ANPREBOL"+",ANPRONAS"+",ANSCORPO"+",ANTELEFO"+",ANTELFAX"+",ANTIPFAT"+",ANTIPRIF"+",ANTIPSOT"+",ANCATOPE"+",ANDATNAS"+",ANNUMCAR"+",ANCOGNOM"+",AN__NOME"+",ANCODSAL"+",ANCODPAG"+",ANCATCOM"+",ANCODBAN"+",ANCODBA2"+",ANCHKFAX"+",ANCHKMAI"+",ANCHKPEC"+",ANCHKSTA"+",ANFLPRIV"+",AN_SKYPE"+",ANIVASOS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("C"),'CONTI','ANTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'CONTI','ANCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CONTI','ANDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCR2),'CONTI','ANDESCR2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CC___CAP),'CONTI','AN___CAP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCPROVIN),'CONTI','ANPROVIN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCINDIRI),'CONTI','ANINDIRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCINDIR2),'CONTI','ANINDIR2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODVAL),'CONTI','ANCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'CONTI','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(0),'CONTI','UTCV');
        +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'CONTI','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'CONTI','UTDV');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','AFFLINTR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOTE),'CONTI','AN__NOTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CC_EMAIL),'CONTI','AN_EMAIL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CC_EMPEC),'CONTI','AN_EMPEC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SESSO),'CONTI','AN_SESSO');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANBOLFAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCATCON),'CONTI','ANCATCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODAGE),'CONTI','ANCODAG1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODFIS),'CONTI','ANCODFIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODIVA),'CONTI','ANCODIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODLIN),'CONTI','ANCODLIN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODZON),'CONTI','ANCODZON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CONCON),'CONTI','ANCONCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCMASCON),'CONTI','ANCONSUP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDTINVA),'CONTI','ANDTINVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDTOBSO),'CONTI','ANDTOBSO');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLBLVE');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLCODI');
        +","+cp_NullLink(cp_ToStrODBC("U"),'CONTI','ANFLCONA');
        +","+cp_NullLink(cp_ToStrODBC(this.pCCFLESIG),'CONTI','ANFLESIG');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLFIDO');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANFLGAVV');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLRAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_INDWEB),'CONTI','ANINDWEB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCLOCALI),'CONTI','ANLOCALI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOLOCNAS),'CONTI','ANLOCNAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCNAZION),'CONTI','ANNAZION');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NONUMCEL),'CONTI','ANNUMCEL');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANNUMCOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCPARIVA),'CONTI','ANPARIVA');
        +","+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANPARTSN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOSOGGET),'CONTI','ANPERFIS');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANPREBOL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOPRONAS),'CONTI','ANPRONAS');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANSCORPO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCTELEFO),'CONTI','ANTELEFO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCTELFAX),'CONTI','ANTELFAX');
        +","+cp_NullLink(cp_ToStrODBC("R"),'CONTI','ANTIPFAT');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANTIPRIF');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANTIPSOT');
        +","+cp_NullLink(cp_ToStrODBC("CF"),'CONTI','ANCATOPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NODATNAS),'CONTI','ANDATNAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NONUMCAR),'CONTI','ANNUMCAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOCOGNOM),'CONTI','ANCOGNOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NO__NOME ),'CONTI','AN__NOME');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODSAL),'CONTI','ANCODSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODPAG),'CONTI','ANCODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCATCOM),'CONTI','ANCATCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODBAN),'CONTI','ANCODBAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODBA2),'CONTI','ANCODBA2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOCHKFAX),'CONTI','ANCHKFAX');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOCHKMAI),'CONTI','ANCHKMAI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOCHKPEC),'CONTI','ANCHKPEC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOCHKSTA),'CONTI','ANCHKSTA');
        +","+cp_NullLink(cp_ToStrODBC(this.pCCFLPRIV),'CONTI','ANFLPRIV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CC_SKYPE),'CONTI','AN_SKYPE');
        +","+cp_NullLink(cp_ToStrODBC(this.pCCIVASOS),'CONTI','ANIVASOS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',"C",'ANCODICE',this.w_ANCODICE,'ANDESCRI',this.w_CCDESCRI,'ANDESCR2',this.w_CCDESCR2,'AN___CAP',this.w_CC___CAP,'ANPROVIN',this.w_CCPROVIN,'ANINDIRI',this.w_CCINDIRI,'ANINDIR2',this.w_CCINDIR2,'ANCODVAL',this.w_CCCODVAL,'UTCC',i_CODUTE,'UTCV',0,'UTDC',SetInfoDate( g_CALUTD ))
        insert into (i_cTable) (ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,AN___CAP,ANPROVIN,ANINDIRI,ANINDIR2,ANCODVAL,UTCC,UTCV,UTDC,UTDV,AFFLINTR,AN__NOTE,AN_EMAIL,AN_EMPEC,AN_SESSO,ANBOLFAT,ANCATCON,ANCODAG1,ANCODFIS,ANCODIVA,ANCODLIN,ANCODZON,ANCONCON,ANCONSUP,ANDTINVA,ANDTOBSO,ANFLBLVE,ANFLCODI,ANFLCONA,ANFLESIG,ANFLFIDO,ANFLGAVV,ANFLRAGG,ANINDWEB,ANLOCALI,ANLOCNAS,ANNAZION,ANNUMCEL,ANNUMCOR,ANPARIVA,ANPARTSN,ANPERFIS,ANPREBOL,ANPRONAS,ANSCORPO,ANTELEFO,ANTELFAX,ANTIPFAT,ANTIPRIF,ANTIPSOT,ANCATOPE,ANDATNAS,ANNUMCAR,ANCOGNOM,AN__NOME,ANCODSAL,ANCODPAG,ANCATCOM,ANCODBAN,ANCODBA2,ANCHKFAX,ANCHKMAI,ANCHKPEC,ANCHKSTA,ANFLPRIV,AN_SKYPE,ANIVASOS &i_ccchkf. );
           values (;
             "C";
             ,this.w_ANCODICE;
             ,this.w_CCDESCRI;
             ,this.w_CCDESCR2;
             ,this.w_CC___CAP;
             ,this.w_CCPROVIN;
             ,this.w_CCINDIRI;
             ,this.w_CCINDIR2;
             ,this.w_CCCODVAL;
             ,i_CODUTE;
             ,0;
             ,SetInfoDate( g_CALUTD );
             ,cp_CharToDate("  -  -    ");
             ," ";
             ,this.w_NOTE;
             ,this.w_CC_EMAIL;
             ,this.w_CC_EMPEC;
             ,this.w_SESSO;
             ,"N";
             ,this.w_CCCATCON;
             ,this.w_CCCODAGE;
             ,this.w_CCCODFIS;
             ,this.w_CCCODIVA;
             ,this.w_CCCODLIN;
             ,this.w_CCCODZON;
             ,this.w_CONCON;
             ,this.w_CCMASCON;
             ,this.w_CCDTINVA;
             ,this.w_CCDTOBSO;
             ," ";
             ," ";
             ,"U";
             ,this.pCCFLESIG;
             ," ";
             ,"N";
             ," ";
             ,this.w_INDWEB;
             ,this.w_CCLOCALI;
             ,this.w_NOLOCNAS;
             ,this.w_CCNAZION;
             ,this.w_NONUMCEL;
             ," ";
             ,this.w_CCPARIVA;
             ,"S";
             ,this.w_NOSOGGET;
             ,"N";
             ,this.w_NOPRONAS;
             ,"N";
             ,this.w_CCTELEFO;
             ,this.w_CCTELFAX;
             ,"R";
             ," ";
             ," ";
             ,"CF";
             ,this.w_NODATNAS;
             ,this.w_NONUMCAR;
             ,this.w_NOCOGNOM;
             ,this.w_NO__NOME ;
             ,this.w_CCCODSAL;
             ,this.w_CCCODPAG;
             ,this.w_CCCATCOM;
             ,this.w_CCCODBAN;
             ,this.w_CCCODBA2;
             ,this.w_NOCHKFAX;
             ,this.w_NOCHKMAI;
             ,this.w_NOCHKPEC;
             ,this.w_NOCHKSTA;
             ,this.pCCFLPRIV;
             ,this.w_CC_SKYPE;
             ,this.pCCIVASOS;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Se � stato definito un listino per il nominativo allora viene assegnato anche al nuovo cliente 
      this.w_CODCL = this.w_ANCODICE
      if NOT EMPTY( this.w_NUMLIS )
        do GSAR1BLK with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      * --- Insert into CONTI
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ANTIPCON"+",ANCODICE"+",ANDESCRI"+",ANDESCR2"+",AN___CAP"+",ANPROVIN"+",ANINDIRI"+",ANINDIR2"+",ANCODVAL"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",AFFLINTR"+",AN__NOTE"+",AN_EMAIL"+",AN_EMPEC"+",AN_SESSO"+",ANBOLFAT"+",ANCATCON"+",ANCODAG1"+",ANCODFIS"+",ANCODIVA"+",ANCODLIN"+",ANCODZON"+",ANCONCON"+",ANCONSUP"+",ANDTINVA"+",ANDTOBSO"+",ANFLBLVE"+",ANFLCODI"+",ANFLCONA"+",ANFLESIG"+",ANFLFIDO"+",ANFLGAVV"+",ANFLRAGG"+",ANINDWEB"+",ANLOCALI"+",ANLOCNAS"+",ANNAZION"+",ANNUMCEL"+",ANNUMCOR"+",ANPARIVA"+",ANPARTSN"+",ANPERFIS"+",ANPREBOL"+",ANPRONAS"+",ANSCORPO"+",ANTELEFO"+",ANTELFAX"+",ANTIPFAT"+",ANTIPRIF"+",ANTIPSOT"+",ANCATOPE"+",ANDATNAS"+",ANNUMCAR"+",ANCOGNOM"+",AN__NOME"+",ANCODSAL"+",ANCODPAG"+",ANNUMLIS"+",ANCATCOM"+",ANCODBAN"+",ANCODBA2"+",ANCHKFAX"+",ANCHKMAI"+",ANCHKPEC"+",ANCHKSTA"+",ANTIPOCL"+",ANFLPRIV"+",AN_SKYPE"+",ANIVASOS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'CONTI','ANTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'CONTI','ANCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CONTI','ANDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCR2),'CONTI','ANDESCR2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CC___CAP),'CONTI','AN___CAP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCPROVIN),'CONTI','ANPROVIN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCINDIRI),'CONTI','ANINDIRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCINDIR2),'CONTI','ANINDIR2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODVAL),'CONTI','ANCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'CONTI','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(0),'CONTI','UTCV');
        +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'CONTI','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'CONTI','UTDV');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','AFFLINTR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOTE),'CONTI','AN__NOTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CC_EMAIL),'CONTI','AN_EMAIL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CC_EMPEC),'CONTI','AN_EMPEC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SESSO),'CONTI','AN_SESSO');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANBOLFAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCATCON),'CONTI','ANCATCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODAGE),'CONTI','ANCODAG1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODFIS),'CONTI','ANCODFIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODIVA),'CONTI','ANCODIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODLIN),'CONTI','ANCODLIN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODZON),'CONTI','ANCODZON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CONCON),'CONTI','ANCONCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCMASCON),'CONTI','ANCONSUP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDTINVA),'CONTI','ANDTINVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDTOBSO),'CONTI','ANDTOBSO');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLBLVE');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLCODI');
        +","+cp_NullLink(cp_ToStrODBC("U"),'CONTI','ANFLCONA');
        +","+cp_NullLink(cp_ToStrODBC(this.pCCFLESIG),'CONTI','ANFLESIG');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLFIDO');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANFLGAVV');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLRAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_INDWEB),'CONTI','ANINDWEB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCLOCALI),'CONTI','ANLOCALI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOLOCNAS),'CONTI','ANLOCNAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCNAZION),'CONTI','ANNAZION');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NONUMCEL),'CONTI','ANNUMCEL');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANNUMCOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCPARIVA),'CONTI','ANPARIVA');
        +","+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANPARTSN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOSOGGET),'CONTI','ANPERFIS');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANPREBOL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOPRONAS),'CONTI','ANPRONAS');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANSCORPO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCTELEFO),'CONTI','ANTELEFO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCTELFAX),'CONTI','ANTELFAX');
        +","+cp_NullLink(cp_ToStrODBC("R"),'CONTI','ANTIPFAT');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANTIPRIF');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANTIPSOT');
        +","+cp_NullLink(cp_ToStrODBC("CF"),'CONTI','ANCATOPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NODATNAS),'CONTI','ANDATNAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NONUMCAR),'CONTI','ANNUMCAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOCOGNOM),'CONTI','ANCOGNOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NO__NOME ),'CONTI','AN__NOME');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODSAL),'CONTI','ANCODSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODPAG),'CONTI','ANCODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCNUMLIS),'CONTI','ANNUMLIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCATCOM),'CONTI','ANCATCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODBAN),'CONTI','ANCODBAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODBA2),'CONTI','ANCODBA2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOCHKFAX),'CONTI','ANCHKFAX');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOCHKMAI),'CONTI','ANCHKMAI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOCHKPEC),'CONTI','ANCHKPEC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOCHKSTA),'CONTI','ANCHKSTA');
        +","+cp_NullLink(cp_ToStrODBC(this.pTIPOCL),'CONTI','ANTIPOCL');
        +","+cp_NullLink(cp_ToStrODBC(this.pCCFLPRIV),'CONTI','ANFLPRIV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CC_SKYPE),'CONTI','AN_SKYPE');
        +","+cp_NullLink(cp_ToStrODBC(this.pCCIVASOS),'CONTI','ANIVASOS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',this.w_TIPCON,'ANCODICE',this.w_ANCODICE,'ANDESCRI',this.w_CCDESCRI,'ANDESCR2',this.w_CCDESCR2,'AN___CAP',this.w_CC___CAP,'ANPROVIN',this.w_CCPROVIN,'ANINDIRI',this.w_CCINDIRI,'ANINDIR2',this.w_CCINDIR2,'ANCODVAL',this.w_CCCODVAL,'UTCC',i_CODUTE,'UTCV',0,'UTDC',SetInfoDate( g_CALUTD ))
        insert into (i_cTable) (ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,AN___CAP,ANPROVIN,ANINDIRI,ANINDIR2,ANCODVAL,UTCC,UTCV,UTDC,UTDV,AFFLINTR,AN__NOTE,AN_EMAIL,AN_EMPEC,AN_SESSO,ANBOLFAT,ANCATCON,ANCODAG1,ANCODFIS,ANCODIVA,ANCODLIN,ANCODZON,ANCONCON,ANCONSUP,ANDTINVA,ANDTOBSO,ANFLBLVE,ANFLCODI,ANFLCONA,ANFLESIG,ANFLFIDO,ANFLGAVV,ANFLRAGG,ANINDWEB,ANLOCALI,ANLOCNAS,ANNAZION,ANNUMCEL,ANNUMCOR,ANPARIVA,ANPARTSN,ANPERFIS,ANPREBOL,ANPRONAS,ANSCORPO,ANTELEFO,ANTELFAX,ANTIPFAT,ANTIPRIF,ANTIPSOT,ANCATOPE,ANDATNAS,ANNUMCAR,ANCOGNOM,AN__NOME,ANCODSAL,ANCODPAG,ANNUMLIS,ANCATCOM,ANCODBAN,ANCODBA2,ANCHKFAX,ANCHKMAI,ANCHKPEC,ANCHKSTA,ANTIPOCL,ANFLPRIV,AN_SKYPE,ANIVASOS &i_ccchkf. );
           values (;
             this.w_TIPCON;
             ,this.w_ANCODICE;
             ,this.w_CCDESCRI;
             ,this.w_CCDESCR2;
             ,this.w_CC___CAP;
             ,this.w_CCPROVIN;
             ,this.w_CCINDIRI;
             ,this.w_CCINDIR2;
             ,this.w_CCCODVAL;
             ,i_CODUTE;
             ,0;
             ,SetInfoDate( g_CALUTD );
             ,cp_CharToDate("  -  -    ");
             ," ";
             ,this.w_NOTE;
             ,this.w_CC_EMAIL;
             ,this.w_CC_EMPEC;
             ,this.w_SESSO;
             ,"N";
             ,this.w_CCCATCON;
             ,this.w_CCCODAGE;
             ,this.w_CCCODFIS;
             ,this.w_CCCODIVA;
             ,this.w_CCCODLIN;
             ,this.w_CCCODZON;
             ,this.w_CONCON;
             ,this.w_CCMASCON;
             ,this.w_CCDTINVA;
             ,this.w_CCDTOBSO;
             ," ";
             ," ";
             ,"U";
             ,this.pCCFLESIG;
             ," ";
             ,"N";
             ," ";
             ,this.w_INDWEB;
             ,this.w_CCLOCALI;
             ,this.w_NOLOCNAS;
             ,this.w_CCNAZION;
             ,this.w_NONUMCEL;
             ," ";
             ,this.w_CCPARIVA;
             ,"S";
             ,this.w_NOSOGGET;
             ,"N";
             ,this.w_NOPRONAS;
             ,"N";
             ,this.w_CCTELEFO;
             ,this.w_CCTELFAX;
             ,"R";
             ," ";
             ," ";
             ,"CF";
             ,this.w_NODATNAS;
             ,this.w_NONUMCAR;
             ,this.w_NOCOGNOM;
             ,this.w_NO__NOME ;
             ,this.w_CCCODSAL;
             ,this.w_CCCODPAG;
             ,this.w_CCNUMLIS;
             ,this.w_CCCATCOM;
             ,this.w_CCCODBAN;
             ,this.w_CCCODBA2;
             ,this.w_NOCHKFAX;
             ,this.w_NOCHKMAI;
             ,this.w_NOCHKPEC;
             ,this.w_NOCHKSTA;
             ,this.pTIPOCL;
             ,this.pCCFLPRIV;
             ,this.w_CC_SKYPE;
             ,this.pCCIVASOS;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Inserimento persone
      DIMENSION aPersInserite(1)
      aPersInserite(1) = ""
      this.w_CPROWNUM = 1
      * --- Select from NOM_CONT
      i_nConn=i_TableProp[this.NOM_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2],.t.,this.NOM_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select NCPERSON,NC_EMAIL,NC_EMPEC,NCTELEFO,NCNUMCEL,NC_SKYPE  from "+i_cTable+" NOM_CONT ";
            +" where NCCODICE = "+cp_ToStrODBC(this.w_CCCODICE)+"";
             ,"_Curs_NOM_CONT")
      else
        select NCPERSON,NC_EMAIL,NC_EMPEC,NCTELEFO,NCNUMCEL,NC_SKYPE from (i_cTable);
         where NCCODICE = this.w_CCCODICE;
          into cursor _Curs_NOM_CONT
      endif
      if used('_Curs_NOM_CONT')
        select _Curs_NOM_CONT
        locate for 1=1
        do while not(eof())
        this.w_NCPERSON = ALLTRIM(_Curs_NOM_CONT.NCPERSON)
        this.w_NC_EMAIL = ALLTRIM(_Curs_NOM_CONT.NC_EMAIL)
        this.w_NC_EMPEC = ALLTRIM(_Curs_NOM_CONT.NC_EMPEC)
        this.w_NCTELEFO = ALLTRIM(_Curs_NOM_CONT.NCTELEFO)
        this.w_NCNUMCEL = ALLTRIM(_Curs_NOM_CONT.NCNUMCEL)
        this.w_NC_SKYPE = ALLTRIM(_Curs_NOM_CONT.NC_SKYPE)
        if ASCAN(aPersInserite, this.w_NCPERSON )=0
          * --- Persona non ancora inserita
          DIMENSION aPersInserite(ALEN(aPersInserite,1)+1)
          aPersInserite(ALEN(aPersInserite,1)) = this.w_NCPERSON
          * --- Insert into CONTATTI
          i_nConn=i_TableProp[this.CONTATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTATTI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTATTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"COTIPCON"+",COCODCON"+",CPROWNUM"+",CPROWORD"+",COINDMAI"+",COINDPEC"+",CONUMTEL"+",CORIFPER"+",CONUMCEL"+",CO_SKYPE"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'CONTATTI','COTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'CONTATTI','COCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'CONTATTI','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM * 10),'CONTATTI','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NC_EMAIL),'CONTATTI','COINDMAI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NC_EMPEC),'CONTATTI','COINDPEC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NCTELEFO),'CONTATTI','CONUMTEL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NCPERSON),'CONTATTI','CORIFPER');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NCNUMCEL),'CONTATTI','CONUMCEL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NC_SKYPE),'CONTATTI','CO_SKYPE');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'COTIPCON',this.w_TIPCON,'COCODCON',this.w_ANCODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWNUM * 10,'COINDMAI',this.w_NC_EMAIL,'COINDPEC',this.w_NC_EMPEC,'CONUMTEL',this.w_NCTELEFO,'CORIFPER',this.w_NCPERSON,'CONUMCEL',this.w_NCNUMCEL,'CO_SKYPE',this.w_NC_SKYPE)
            insert into (i_cTable) (COTIPCON,COCODCON,CPROWNUM,CPROWORD,COINDMAI,COINDPEC,CONUMTEL,CORIFPER,CONUMCEL,CO_SKYPE &i_ccchkf. );
               values (;
                 this.w_TIPCON;
                 ,this.w_ANCODICE;
                 ,this.w_CPROWNUM;
                 ,this.w_CPROWNUM * 10;
                 ,this.w_NC_EMAIL;
                 ,this.w_NC_EMPEC;
                 ,this.w_NCTELEFO;
                 ,this.w_NCPERSON;
                 ,this.w_NCNUMCEL;
                 ,this.w_NC_SKYPE;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_CPROWNUM = this.w_CPROWNUM + 1
        endif
          select _Curs_NOM_CONT
          continue
        enddo
        use
      endif
      RELEASE aPersInserite
    endif
    if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
      * --- Sono in ENTERPRISE quindi faccio un update diversa da REVOLUTION
      * --- Write into CONTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ANINTMOR ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANINTMOR');
            +i_ccchkf ;
        +" where ";
            +"ANTIPCON = "+cp_ToStrODBC("C");
            +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
               )
      else
        update (i_cTable) set;
            ANINTMOR = "N";
            &i_ccchkf. ;
         where;
            ANTIPCON = "C";
            and ANCODICE = this.w_ANCODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      this.w_ANCODSTU = " "
      if g_LEMC="S" AND this.w_TIPCON="C"
        * --- Calcola il valore di ANCODSTU.
        *     Prima leggiamo dal mastro se c'� un conto studio
        * --- Read from MASTRI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MASTRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MCCODSTU,MCPROSTU"+;
            " from "+i_cTable+" MASTRI where ";
                +"MCCODICE = "+cp_ToStrODBC(this.w_CCMASCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MCCODSTU,MCPROSTU;
            from (i_cTable) where;
                MCCODICE = this.w_CCMASCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODSTU = NVL(cp_ToDate(_read_.MCCODSTU),cp_NullValue(_read_.MCCODSTU))
          this.w_MCPROSTU = NVL(cp_ToDate(_read_.MCPROSTU),cp_NullValue(_read_.MCPROSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ANCODSTU = IIF(!EMPTY(this.w_CODSTU),PADL(ALLTRIM(GSLM_BCF(this, "C", this.w_ANCODICE, this.w_CODSTU, this.w_CCMASCON , , this.w_MCPROSTU )),6,"0")," ")
      endif
      * --- Write into CONTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ANCODSTU ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODSTU),'CONTI','ANCODSTU');
        +",ANFLAACC ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLAACC');
        +",ANFLGCON ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLGCON');
        +",ANGESCON ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANGESCON');
        +",ANIBARID ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANIBARID');
        +",ANFLRITE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLRITE),'CONTI','ANFLRITE');
        +",ANCODIRP ="+cp_NullLink(cp_ToStrODBC(this.w_CCCODIRP),'CONTI','ANCODIRP');
        +",ANCAURIT ="+cp_NullLink(cp_ToStrODBC(this.w_CCCAURIT),'CONTI','ANCAURIT');
        +",ANTIPOCL ="+cp_NullLink(cp_ToStrODBC(this.pTIPOCL),'CONTI','ANTIPOCL');
            +i_ccchkf ;
        +" where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
               )
      else
        update (i_cTable) set;
            ANCODSTU = this.w_ANCODSTU;
            ,ANFLAACC = " ";
            ,ANFLGCON = " ";
            ,ANGESCON = "N";
            ,ANIBARID = " ";
            ,ANFLRITE = this.w_CCFLRITE;
            ,ANCODIRP = this.w_CCCODIRP;
            ,ANCAURIT = this.w_CCCAURIT;
            ,ANTIPOCL = this.pTIPOCL;
            &i_ccchkf. ;
         where;
            ANTIPCON = this.w_TIPCON;
            and ANCODICE = this.w_ANCODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if NOT EMPTY(this.w_CCCODICE)
      * --- Write into OFF_NOMI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"NOTIPCLI ="+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'OFF_NOMI','NOTIPCLI');
        +",NOCODCLI ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'OFF_NOMI','NOCODCLI');
        +",NOTIPNOM ="+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'OFF_NOMI','NOTIPNOM');
            +i_ccchkf ;
        +" where ";
            +"NOCODICE = "+cp_ToStrODBC(this.w_CCCODICE);
               )
      else
        update (i_cTable) set;
            NOTIPCLI = this.w_TIPCON;
            ,NOCODCLI = this.w_ANCODICE;
            ,NOTIPNOM = this.w_TIPCON;
            &i_ccchkf. ;
         where;
            NOCODICE = this.w_CCCODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Notifica al Batch GSAR_BO1 il corretto inserimento Cliente
    AH_ErrorMsg("%1 creato correttamente",64,,IIF(this.w_TIPCON="C","Cliente","Fornitore"))
    this.w_CHKERR = .F.
    return


  proc Init(oParentObject,pCCCODICE,pANCODICE,pCCMASCON,pCCCATCON,pCCCODLIN,pCCCODIVA,pCCCODPAG,pCCFLRITE,pCCCODIRP,pCCNAZION,pCCPARIVA,pAzione,pArrCalc,pTIPOCL,pCCFLPRIV,pCCFLESIG,pCCCODFIS,pCCIVASOS,pCliFor)
    this.pCCCODICE=pCCCODICE
    this.pANCODICE=pANCODICE
    this.pCCMASCON=pCCMASCON
    this.pCCCATCON=pCCCATCON
    this.pCCCODLIN=pCCCODLIN
    this.pCCCODIVA=pCCCODIVA
    this.pCCCODPAG=pCCCODPAG
    this.pCCFLRITE=pCCFLRITE
    this.pCCCODIRP=pCCCODIRP
    this.pCCNAZION=pCCNAZION
    this.pCCPARIVA=pCCPARIVA
    this.pAzione=pAzione
    this.pArrCalc=pArrCalc
    this.pTIPOCL=pTIPOCL
    this.pCCFLPRIV=pCCFLPRIV
    this.pCCFLESIG=pCCFLESIG
    this.pCCCODFIS=pCCCODFIS
    this.pCCIVASOS=pCCIVASOS
    this.pCliFor=pCliFor
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='DES_DIVE'
    this.cWorkTables[4]='TRI_BUTI'
    this.cWorkTables[5]='PAR_OFFE'
    this.cWorkTables[6]='MASTRI'
    this.cWorkTables[7]='NOM_CONT'
    this.cWorkTables[8]='CONTATTI'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    if used('_Curs_NOM_CONT')
      use in _Curs_NOM_CONT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCCCODICE,pANCODICE,pCCMASCON,pCCCATCON,pCCCODLIN,pCCCODIVA,pCCCODPAG,pCCFLRITE,pCCCODIRP,pCCNAZION,pCCPARIVA,pAzione,pArrCalc,pTIPOCL,pCCFLPRIV,pCCFLESIG,pCCCODFIS,pCCIVASOS,pCliFor"
endproc
