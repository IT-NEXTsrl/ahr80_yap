* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bat                                                        *
*              Genera assestamento                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_456]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-06                                                      *
* Last revis.: 2010-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bat",oParentObject)
return(i_retval)

define class tgscg_bat as StdBatch
  * --- Local variables
  w_ACNUMREG = space(6)
  w_CCDESSUP = space(254)
  w_CCDESRIG = space(254)
  w_PNDESSUP = space(50)
  w_PNPRG = space(8)
  w_PNDESRIG = space(50)
  w_MESS_ERR = space(200)
  w_QUERY8 = space(200)
  w_QUERY3 = space(200)
  w_QUERY1 = space(200)
  w_QUERY7 = space(200)
  w_QUERY2 = space(200)
  w_QUERY4 = space(200)
  w_QUERY6 = space(200)
  w_QUERY5 = space(200)
  w_ERROR = .f.
  w_TIPCON = space(1)
  w_PNSERIAL = space(10)
  w_CODICE = space(15)
  w_PNNUMRER = 0
  w_CODBUN = space(3)
  w_PNCODESE = space(4)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_REDESOPE = space(10)
  w_PNCODUTE = 0
  w_CONTA1 = 0
  w_CONTA2 = 0
  w_CONTA3 = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_DESCRI = space(50)
  w_REGOLA = space(50)
  w_PNCODCAU = space(5)
  w_PNCODCAS = space(5)
  w_PNSCRASS = space(1)
  w_STORNO = space(1)
  w_MESS = space(250)
  w_VALNAZ = space(3)
  w_PNDATREG = ctod("  /  /  ")
  w_ONLYRatRis = .f.
  w_PNINICOM = ctod("  /  /  ")
  w_PNFINCOM = space(1)
  w_SERIAL = space(10)
  o_SERIAL = space(10)
  w_FLANAL = space(1)
  w_TOTIMP = 0
  w_PARAMETRO = 0
  w_CODCEN = space(15)
  o_CODICE = space(15)
  w_VOCCEN = space(15)
  o_TIPCON = space(1)
  w_CODCOM = space(15)
  w_NUM = 0
  w_DATCIN = ctod("  /  /  ")
  w_DATCFI = ctod("  /  /  ")
  w_CONSUP = space(15)
  w_CCTAGG = space(1)
  w_SEZB = space(1)
  w_CODBUN = space(3)
  w_FLBUANAL = space(1)
  w_NoAnalitica = .f.
  w_SEGNO = space(1)
  w_IMPORTO = 0
  w_CNVALNAZ = space(3)
  w_SERIALE = space(10)
  w_NUMRIGA = 0
  o_PNSERIAL = space(10)
  o_CPROWNUM = 0
  o_CODBUN = space(3)
  w_FLRIFE = space(1)
  w_FLRIFE1 = space(1)
  w_TIPO = space(1)
  w_CODTESTA = space(15)
  o_NUMRIG = 0
  w_NUMRIG = 0
  w_CAMBIO = 0
  w_NUOVOCONTO = .f.
  w_TIPO = space(1)
  w_DATOBSO = ctod("  /  /  ")
  w_PERIVA = 0
  * --- WorkFile variables
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  CAU_CONT_idx=0
  CONTI_idx=0
  BUSIUNIT_idx=0
  MOVICOST_idx=0
  SALDICON_idx=0
  ESERCIZI_idx=0
  VALUTE_idx=0
  MASTRI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH PER LA GENERAZIONE DELLE SCRITTURE DI ASSESTAMENTO
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Cursore per la stampa degli errori durante l'elaborazione
    CREATE CURSOR CursError (REGOLA C(40), CODICE C(15), CPROWNUM N(6), DESCRI C(250), TIPCON C(1))
    b = WRCURSOR("CursError")
    * --- Crea il Temporaneo di Appoggio.
    *     Il temporaneo viene utilizzato per tutte le scritture.
    CREATE CURSOR TmpAgg ; 
 (CODICE C(15), CODBUN C(3), DESCRI C(45), TIPCON C(1), COD001 C(15), COD002 C(15), ; 
 COD003 C(15), IMPDAR N(18,4), IMPAVE N(18,4), INICOM D(8), FINCOM D(8), SERIAL C(10), DESCRI2 C(50),PNSERIAL C(10),CPROWNUM N(5,0) ,; 
 CODCEN C(15), VOCCEN C(15), CODCOM C(15), DATCIN D(8), DATCFI D(8),PARAMETRO N(8,4),TOTIMP N(18,4),TIPO C(1),CODCON C(15))
    * --- Dati per Generazione cespiti
    if this.oParentObject.w_ASFLCESP="S"
      this.w_ACNUMREG = this.oParentObject.w_PANUMREG
      this.w_REGOLA = "Generazione cespiti"
      vq_exec(ALLTRIM(this.w_QUERY2), this, "EXTCE001")
      * --- Esegue batch di Elaborazione Cespiti
      do GSCG_B04 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      do GSCG_B08 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PNCODCAU = this.oParentObject.w_COCAUCES
      this.w_PNCODCAS = this.oParentObject.w_COCASCES
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Dati per Generazione rimanenze
    if this.oParentObject.w_ASFLRIMA="S"
      this.w_REGOLA = "Generazione rimanenze"
      vq_exec(ALLTRIM(this.w_QUERY5), this, "EXTRI001")
      * --- Esegue batch di Elaborazione Rimanenze
      do GSCG_B02 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PNCODCAU = this.oParentObject.w_COCAURIM
      this.w_PNCODCAS = this.oParentObject.w_COCASRIM
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Dati per Generazione Ratei/Risconti
    if this.oParentObject.w_ASFLRATE="S"
      this.w_ONLYRatRis = .T.
      this.w_REGOLA = "Generazione ratei e risconti"
      vq_exec(ALLTRIM(this.w_QUERY3), this, "EXTCG001")
      * --- Esegue batch di Elaborazione Ratei Risconti
      do GSCG_B01 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PNCODCAU = this.oParentObject.w_COCAURER
      this.w_PNCODCAS = IIF(this.oParentObject.w_ASFLSTOR="S" AND this.oParentObject.w_ASDATFIN=this.oParentObject.w_FINESE,this.oParentObject.w_COCAFRER,this.oParentObject.w_COCASRER)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_ONLYRatRis = .F.
    endif
    * --- Controllo contropartite
    if this.oParentObject.w_ASFLDOCR="S" or this.oParentObject.w_ASFLDOCE="S"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Dati per Generazione fatture da emettere
    if this.oParentObject.w_ASFLDOCE="S"
      this.w_REGOLA = "Generazione fatture da emettere"
      vq_exec(ALLTRIM(this.w_QUERY1), this, "DOCUMENTI")
      ah_Msg("Selezioni documenti da eliminare",.T.)
      vq_exec("QUERY\SASFA012.VQR", this, "SERIALI")
      ah_Msg("Selezioni fatture da ricevere",.T.) 
 Select * From DOCUMENTI Where MVSERIAL Not In (Select MVSERIAL From SERIALI) Into Cursor EXTFA003
      if USED("DOCUMENTI")
        SELECT DOCUMENTI 
 USE
      endif
      if USED("SERIALI")
        SELECT SERIALI 
 USE
      endif
      * --- Esegue batch di Elaborazione Fatture da Emettere
      do GSCG_B07 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      vq_exec(ALLTRIM(this.w_QUERY7), this, "EXTFA003")
      * --- Esegue batch di Elaborazione Documenti Parzialmente Evasi da Emettere
      do GSCG_B07 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      do GSCG_B11 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PNCODCAU = this.oParentObject.w_COCAUFDE
      this.w_PNCODCAS = this.oParentObject.w_COCASFDE
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Dati per Generazione fatture da ricevere
    if this.oParentObject.w_ASFLDOCR="S"
      this.w_REGOLA = "Generazione fatture da ricevere"
      ah_Msg("Selezioni documenti competenza",.T.)
      vq_exec(ALLTRIM(this.w_QUERY4), this, "DOCUMENTI")
      ah_Msg("Selezioni documenti da eliminare",.T.)
      vq_exec("QUERY\SASFA002.VQR", this, "SERIALI")
      ah_Msg("Selezioni fatture da ricevere",.T.) 
 Select * From DOCUMENTI Where MVSERIAL Not In (Select MVSERIAL From SERIALI) Into Cursor EXTFA003
      if USED("DOCUMENTI")
        SELECT DOCUMENTI 
 USE
      endif
      if USED("SERIALI")
        SELECT SERIALI 
 USE
      endif
      * --- Esegue batch di Elaborazione Fatture da Ricevere
      do GSCG_B07 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      vq_exec(ALLTRIM(this.w_QUERY6), this, "EXTFA003")
      * --- Esegue batch di Elaborazione Documenti Parzialmente Evasi da Emettere
      do GSCG_B07 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      do GSCG_B11 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PNCODCAU = this.oParentObject.w_COCAUFDR
      this.w_PNCODCAS = this.oParentObject.w_COCASFDR
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Gestione Errori
    if RecCount("CursError")=0
      this.w_MESS = AH_Msgformat("Non ci sono movimenti da generare")
      if this.w_CONTA1>0
        ah_ErrorMsg("Operazione completata%0n. %1 registrazioni generate","!","",ALLTRIM(STR(this.w_CONTA2)))
      else
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    else
      if ah_YesNo("Operazione completata%0n. %1 registrazioni generate%0si sono verificati degli errori%0si vuole procedere con la stampa?",,ALLTRIM(STR(this.w_CONTA2)))
        * --- Vi sono alcuni messaggi di errori ripetuti (es. mancanza contropartite generali)
        *     che sono ripetuti per tutti i documenti
        SELECT DISTINCT * FROM CursError INTO CURSOR __TMP__ ORDER BY CPROWNUM
        CP_CHPRN("QUERY\GSCG_BAT.FRX","",this.oParentObject)
      endif
    endif
    if USED("TmpAgg")
      * --- Azzera temporaneo
      SELECT TmpAgg
      USE
    endif
    if used("CursError")
      select CursError
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED("TmpAgg")
      * --- Try
      local bErr_03EDD620
      bErr_03EDD620=bTrsErr
      this.Try_03EDD620()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        this.w_CONTA2 = this.w_CONTA2 - 1
        this.w_ERROR = .T.
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(AH_Msgformat(this.w_REGOLA),this.w_CODICE,this.w_CPROWNUM,ah_MsgFormat("Errore nell'insert"),this.w_TIPCON)
      endif
      bTrsErr=bTrsErr or bErr_03EDD620
      * --- End
    endif
  endproc
  proc Try_03EDD620()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Genera scritture di primanota
    this.w_CPROWNUM = 0
    ah_Msg("Genera registrazioni contabili...",.T.)
    SELECT TmpAgg
    GO TOP
    SCAN FOR NOT EMPTY(NVL(COD002," "))
    this.w_CODICE = NVL(COD002,"")
    this.w_CODBUN = NVL(CODBUN, " ")
    this.w_TIPCON = NVL(TIPCON," ")
    this.w_NUMRIG = NVL(CPROWNUM,0)
    this.w_NUOVOCONTO = .F.
    * --- Nel caso di Generazione fatture da emettere oppure da ricevere devo riportare
    *     il tipo conto e il codice conto se la causale ha abilitato il riferimento.
    if this.w_REGOLA="Generazione fatture da emettere" OR this.w_REGOLA="Generazione fatture da ricevere"
      if this.w_STORNO
        * --- Verifico se devo riportare nella registrazione di primanota creata il riferimento
        *     al cliente/fornitore
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCFLRIFE"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCFLRIFE;
            from (i_cTable) where;
                CCCODICE = this.w_PNCODCAS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLRIFE1 = NVL(cp_ToDate(_read_.CCFLRIFE),cp_NullValue(_read_.CCFLRIFE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NVL(this.w_FLRIFE1,"N")<>"N" AND this.oParentObject.w_RAGGSCRI<>"S" AND this.w_FLRIFE1=TIPO
          this.w_TIPO = TIPO
          this.w_CODTESTA = CODCON
        else
          this.w_TIPO = SPACE(1)
          this.w_CODTESTA = SPACE(15)
        endif
      else
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCFLRIFE"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCFLRIFE;
            from (i_cTable) where;
                CCCODICE = this.w_PNCODCAU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLRIFE = NVL(cp_ToDate(_read_.CCFLRIFE),cp_NullValue(_read_.CCFLRIFE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NVL(this.w_FLRIFE,"N")<>"N" AND this.oParentObject.w_RAGGSCRI<>"S" AND this.w_FLRIFE=TIPO
          this.w_TIPO = TIPO
          this.w_CODTESTA = CODCON
        else
          this.w_TIPO = SPACE(1)
          this.w_CODTESTA = SPACE(15)
        endif
      endif
    else
      this.w_TIPO = SPACE(1)
      this.w_CODTESTA = SPACE(15)
    endif
    this.w_IMPDAR = IIF(this.w_STORNO, NVL(IMPAVE, 0), NVL(IMPDAR, 0))
    this.w_IMPAVE = IIF(this.w_STORNO, NVL(IMPDAR, 0), NVL(IMPAVE, 0))
    this.w_DESCRI = iif(NOT EMPTY(this.oParentObject.w_ASDESCRI), this.oParentObject.w_ASDESCRI, DESCRI2)
    this.w_SERIAL = NVL(SERIAL, "")
    * --- Dati analitica
    this.w_CODCEN = NVL(CODCEN, SPACE(15))
    this.w_VOCCEN = NVL(VOCCEN, SPACE(15))
    this.w_CODCOM = NVL(CODCOM, SPACE(15))
    this.w_DATCIN = NVL(DATCIN, cp_CharToDate("  -  -    "))
    this.w_DATCFI = NVL(DATCFI, cp_CharToDate("  -  -    "))
    if this.w_STORNO
      this.w_SEGNO = IIF(TOTIMP>0,"A","D")
    else
      this.w_SEGNO = IIF(TOTIMP>0,"D","A")
    endif
    this.w_TOTIMP = ABS(NVL(TOTIMP,0))
    this.w_PARAMETRO = NVL(PARAMETRO,0)
    this.w_SERIALE = PNSERIAL
    this.w_NUMRIGA = CPROWNUM
    if this.oParentObject.w_ASFLSTOR="S" AND this.oParentObject.w_ASDATFIN=this.oParentObject.w_FINESE AND this.w_REGOLA="Generazione ratei e risconti" AND FINCOM>this.oParentObject.w_FINESE
      this.w_PNINICOM = NVL(INICOM,cp_CharToDate("  -  -    "))
      this.w_PNFINCOM = NVL(FINCOM,cp_CharToDate("  -  -    "))
    else
      this.w_PNINICOM = cp_CharToDate("  -  -    ")
      this.w_PNFINCOM = cp_CharToDate("  -  -    ")
    endif
    if this.w_CPROWNUM=0 OR this.w_SERIAL<>this.o_SERIAL
      * --- Prima Riga: Scrive la Testata
      this.w_CONTA1 = this.w_CONTA1 + 1
      this.w_CONTA2 = this.w_CONTA2 + 1
      this.o_SERIAL = this.w_SERIAL
      this.w_CPROWNUM = 0
      * --- Scrive Movimenti Contabilita' Generale
      this.w_PNSERIAL = SPACE(10)
      this.w_PNNUMRER = 0
      i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SEPNT", "i_CODAZI,w_PNSERIAL")
      if g_APPLICATION = "ADHOC REVOLUTION"
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCFLANAL,CCDESSUP,CCDESRIG"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCFLANAL,CCDESSUP,CCDESRIG;
            from (i_cTable) where;
                CCCODICE = this.w_PNCODCAU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLANAL = NVL(cp_ToDate(_read_.CCFLANAL),cp_NullValue(_read_.CCFLANAL))
          this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
          this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Empty(this.w_PNDESSUP) and (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
          * --- Array elenco parametri per descrizioni di riga e testata
           
 DIMENSION ARPARAM[12,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=Alltrim(STR(this.oParentObject.w_ASNUMDOC)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=Alltrim(this.oParentObject.w_ASALFDOC) 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.oParentObject.w_ASDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.w_CODTESTA) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=this.w_TIPO
        endif
        this.w_PNDESSUP = this.w_DESCRI
        if Not Empty(this.w_CCDESSUP) and Empty(this.w_PNDESSUP)
          this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
        endif
        * --- Le scritture di assestamento non gestiscono le numerazioni documento / protocollo..
        this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
        cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
        * --- Insert into PNT_MAST
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PNSERIAL"+",PNNUMRER"+",PNDATREG"+",PNCODESE"+",PNCOMPET"+",PNCODCAU"+",PNALFDOC"+",PNNUMDOC"+",PNDATDOC"+",PNVALNAZ"+",PNCODVAL"+",PNCAOVAL"+",PNFLPROV"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",PNCODUTE"+",PNDESSUP"+",PNSCRASS"+",PNRIFSAS"+",PNTIPREG"+",PNFLIVDF"+",PNTIPDOC"+",PNPRD"+",PNPRP"+",PNTIPCLF"+",PNCODCLF"+",PNPRG"+",PNANNPRO"+",PNANNDOC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCOMPET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASALFDOC),'PNT_MAST','PNALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASNUMDOC),'PNT_MAST','PNNUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASDATDOC),'PNT_MAST','PNDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'PNT_MAST','PNVALNAZ');
          +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'PNT_MAST','PNCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(g_CAOVAL),'PNT_MAST','PNCAOVAL');
          +","+cp_NullLink(cp_ToStrODBC("S"),'PNT_MAST','PNFLPROV');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PNT_MAST','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PNT_MAST','UTDC');
          +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','UTCV');
          +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PNT_MAST','UTDV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNSCRASS),'PNT_MAST','PNSCRASS');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASSERIAL),'PNT_MAST','PNRIFSAS');
          +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNTIPREG');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
          +","+cp_NullLink(cp_ToStrODBC("NO"),'PNT_MAST','PNTIPDOC');
          +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
          +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TIPO),'PNT_MAST','PNTIPCLF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODTESTA),'PNT_MAST','PNCODCLF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
          +","+cp_NullLink(cp_ToStrODBC(Space(4)),'PNT_MAST','PNANNPRO');
          +","+cp_NullLink(cp_ToStrODBC(Space(4)),'PNT_MAST','PNANNDOC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_PNDATREG,'PNCODESE',this.w_PNCODESE,'PNCOMPET',this.w_PNCODESE,'PNCODCAU',this.w_PNCODCAU,'PNALFDOC',this.oParentObject.w_ASALFDOC,'PNNUMDOC',this.oParentObject.w_ASNUMDOC,'PNDATDOC',this.oParentObject.w_ASDATDOC,'PNVALNAZ',g_PERVAL,'PNCODVAL',g_PERVAL,'PNCAOVAL',g_CAOVAL)
          insert into (i_cTable) (PNSERIAL,PNNUMRER,PNDATREG,PNCODESE,PNCOMPET,PNCODCAU,PNALFDOC,PNNUMDOC,PNDATDOC,PNVALNAZ,PNCODVAL,PNCAOVAL,PNFLPROV,UTCC,UTDC,UTCV,UTDV,PNCODUTE,PNDESSUP,PNSCRASS,PNRIFSAS,PNTIPREG,PNFLIVDF,PNTIPDOC,PNPRD,PNPRP,PNTIPCLF,PNCODCLF,PNPRG,PNANNPRO,PNANNDOC &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_PNNUMRER;
               ,this.w_PNDATREG;
               ,this.w_PNCODESE;
               ,this.w_PNCODESE;
               ,this.w_PNCODCAU;
               ,this.oParentObject.w_ASALFDOC;
               ,this.oParentObject.w_ASNUMDOC;
               ,this.oParentObject.w_ASDATDOC;
               ,g_PERVAL;
               ,g_PERVAL;
               ,g_CAOVAL;
               ,"S";
               ,i_CODUTE;
               ,SetInfoDate( g_CALUTD );
               ,0;
               ,cp_CharToDate("  -  -    ");
               ,this.w_PNCODUTE;
               ,this.w_PNDESSUP;
               ,this.w_PNSCRASS;
               ,this.oParentObject.w_ASSERIAL;
               ,"N";
               ," ";
               ,"NO";
               ,"NN";
               ,"NN";
               ,this.w_TIPO;
               ,this.w_CODTESTA;
               ,this.w_PNPRG;
               ,Space(4);
               ,Space(4);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCFLANAL"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCFLANAL;
            from (i_cTable) where;
                CCCODICE = this.w_PNCODCAU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLANAL = NVL(cp_ToDate(_read_.CCFLANAL),cp_NullValue(_read_.CCFLANAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        cp_NextTableProg(this, i_Conn, "PRPNT", "i_CODAZI,w_PNCODESE,w_PNCODUTE,w_PNNUMRER")
        * --- Insert into PNT_MAST
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PNSERIAL"+",PNNUMRER"+",PNDATREG"+",PNCODESE"+",PNCOMPET"+",PNCODCAU"+",PNALFDOC"+",PNNUMDOC"+",PNDATDOC"+",PNVALNAZ"+",PNCODVAL"+",PNSIMVAL"+",PNCAOVAL"+",PNFLPROV"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",PNCODUTE"+",PNDESSUP"+",PNSCRASS"+",PNRIFSAS"+",PNTIPREG"+",PNFLIVDF"+",PNTIPDOC"+",PNPRD"+",PNPRP"+",PNTIPCLF"+",PNCODCLF"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCOMPET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASALFDOC),'PNT_MAST','PNALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASNUMDOC),'PNT_MAST','PNNUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASDATDOC),'PNT_MAST','PNDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'PNT_MAST','PNVALNAZ');
          +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'PNT_MAST','PNCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(g_VALSIM),'PNT_MAST','PNSIMVAL');
          +","+cp_NullLink(cp_ToStrODBC(g_CAOVAL),'PNT_MAST','PNCAOVAL');
          +","+cp_NullLink(cp_ToStrODBC("S"),'PNT_MAST','PNFLPROV');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PNT_MAST','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PNT_MAST','UTDC');
          +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','UTCV');
          +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PNT_MAST','UTDV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'PNT_MAST','PNDESSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNSCRASS),'PNT_MAST','PNSCRASS');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASSERIAL),'PNT_MAST','PNRIFSAS');
          +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNTIPREG');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
          +","+cp_NullLink(cp_ToStrODBC("NO"),'PNT_MAST','PNTIPDOC');
          +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
          +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TIPO),'PNT_MAST','PNTIPCLF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODTESTA),'PNT_MAST','PNCODCLF');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_PNDATREG,'PNCODESE',this.w_PNCODESE,'PNCOMPET',this.w_PNCODESE,'PNCODCAU',this.w_PNCODCAU,'PNALFDOC',this.oParentObject.w_ASALFDOC,'PNNUMDOC',this.oParentObject.w_ASNUMDOC,'PNDATDOC',this.oParentObject.w_ASDATDOC,'PNVALNAZ',g_PERVAL,'PNCODVAL',g_PERVAL,'PNSIMVAL',g_VALSIM)
          insert into (i_cTable) (PNSERIAL,PNNUMRER,PNDATREG,PNCODESE,PNCOMPET,PNCODCAU,PNALFDOC,PNNUMDOC,PNDATDOC,PNVALNAZ,PNCODVAL,PNSIMVAL,PNCAOVAL,PNFLPROV,UTCC,UTDC,UTCV,UTDV,PNCODUTE,PNDESSUP,PNSCRASS,PNRIFSAS,PNTIPREG,PNFLIVDF,PNTIPDOC,PNPRD,PNPRP,PNTIPCLF,PNCODCLF &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_PNNUMRER;
               ,this.w_PNDATREG;
               ,this.w_PNCODESE;
               ,this.w_PNCODESE;
               ,this.w_PNCODCAU;
               ,this.oParentObject.w_ASALFDOC;
               ,this.oParentObject.w_ASNUMDOC;
               ,this.oParentObject.w_ASDATDOC;
               ,g_PERVAL;
               ,g_PERVAL;
               ,g_VALSIM;
               ,g_CAOVAL;
               ,"S";
               ,i_CODUTE;
               ,SetInfoDate( g_CALUTD );
               ,0;
               ,cp_CharToDate("  -  -    ");
               ,this.w_PNCODUTE;
               ,this.w_DESCRI;
               ,this.w_PNSCRASS;
               ,this.oParentObject.w_ASSERIAL;
               ,"N";
               ," ";
               ,"NO";
               ,"NN";
               ,"NN";
               ,this.w_TIPO;
               ,this.w_CODTESTA;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      this.w_CONTA3 = 0
      this.o_NUMRIG = 0
      this.o_CODICE = space(15)
      this.o_TIPCON = " "
      this.o_CODBUN = "   "
    endif
    if this.w_CODICE<>this.o_CODICE OR this.w_TIPCON<>this.w_TIPCON OR this.w_CODBUN<>this.o_CODBUN OR this.w_NUMRIG<>this.o_NUMRIG
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_CPROWORD = this.w_CPROWNUM * 10
      this.w_NUM = 0
      this.w_NUOVOCONTO = .T.
      * --- Scrive il Detail
      if g_APPLICATION = "ADHOC REVOLUTION"
        this.w_PNDESRIG = this.w_DESCRI
        if Not Empty(this.w_CCDESRIG) and Empty(this.w_PNDESRIG)
          this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
        endif
        * --- Insert into PNT_DETT
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PNSERIAL"+",CPROWNUM"+",PNTIPCON"+",CPROWORD"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNINICOM"+",PNFINCOM"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PNT_DETT','PNTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'PNT_DETT','PNCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDAR),'PNT_DETT','PNIMPDAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPAVE),'PNT_DETT','PNIMPAVE');
          +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
          +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLZERO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALD');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'PNTIPCON',this.w_TIPCON,'CPROWORD',this.w_CPROWORD,'PNCODCON',this.w_CODICE,'PNIMPDAR',this.w_IMPDAR,'PNIMPAVE',this.w_IMPAVE,'PNFLPART',"N",'PNFLZERO',"N",'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCODCAU,'PNINICOM',this.w_PNINICOM)
          insert into (i_cTable) (PNSERIAL,CPROWNUM,PNTIPCON,CPROWORD,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNINICOM,PNFINCOM,PNFLSALD,PNFLSALI,PNFLSALF &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_CPROWNUM;
               ,this.w_TIPCON;
               ,this.w_CPROWORD;
               ,this.w_CODICE;
               ,this.w_IMPDAR;
               ,this.w_IMPAVE;
               ,"N";
               ,"N";
               ,this.w_PNDESRIG;
               ,this.w_PNCODCAU;
               ,this.w_PNINICOM;
               ,this.w_PNFINCOM;
               ," ";
               ," ";
               ," ";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        * --- Insert into PNT_DETT
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PNSERIAL"+",CPROWNUM"+",PNTIPCON"+",CPROWORD"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNCODBUN"+",PNINICOM"+",PNFINCOM"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PNT_DETT','PNTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'PNT_DETT','PNCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDAR),'PNT_DETT','PNIMPDAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPAVE),'PNT_DETT','PNIMPAVE');
          +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
          +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLZERO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'PNT_DETT','PNDESRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODBUN),'PNT_DETT','PNCODBUN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALD');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'PNTIPCON',this.w_TIPCON,'CPROWORD',this.w_CPROWORD,'PNCODCON',this.w_CODICE,'PNIMPDAR',this.w_IMPDAR,'PNIMPAVE',this.w_IMPAVE,'PNFLPART',"N",'PNFLZERO',"N",'PNDESRIG',this.w_DESCRI,'PNCAURIG',this.w_PNCODCAU,'PNCODBUN',this.w_CODBUN)
          insert into (i_cTable) (PNSERIAL,CPROWNUM,PNTIPCON,CPROWORD,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNCODBUN,PNINICOM,PNFINCOM,PNFLSALD,PNFLSALI,PNFLSALF &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_CPROWNUM;
               ,this.w_TIPCON;
               ,this.w_CPROWORD;
               ,this.w_CODICE;
               ,this.w_IMPDAR;
               ,this.w_IMPAVE;
               ,"N";
               ,"N";
               ,this.w_DESCRI;
               ,this.w_PNCODCAU;
               ,this.w_CODBUN;
               ,this.w_PNINICOM;
               ,this.w_PNFINCOM;
               ," ";
               ," ";
               ," ";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      this.o_NUMRIG = this.w_NUMRIG
      this.o_CODICE = this.w_CODICE
      this.o_TIPCON = this.w_TIPCON
      this.o_PNSERIAL = this.w_SERIALE
      this.o_CPROWNUM = this.w_NUMRIGA
      this.o_CODBUN = this.w_CODBUN
    else
      do case
        case this.w_REGOLA="Generazione cespiti" 
          if this.o_PNSERIAL<>this.w_SERIALE
            * --- Write into PNT_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PNT_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PNIMPDAR =PNIMPDAR+ "+cp_ToStrODBC(this.w_IMPDAR);
              +",PNIMPAVE =PNIMPAVE+ "+cp_ToStrODBC(this.w_IMPAVE);
                  +i_ccchkf ;
              +" where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  PNIMPDAR = PNIMPDAR + this.w_IMPDAR;
                  ,PNIMPAVE = PNIMPAVE + this.w_IMPAVE;
                  &i_ccchkf. ;
               where;
                  PNSERIAL = this.w_PNSERIAL;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        case this.w_REGOLA="Generazione fatture da emettere" OR this.w_REGOLA="Generazione fatture da ricevere"
          if this.o_PNSERIAL<>this.w_SERIALE
            * --- Write into PNT_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PNT_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PNIMPDAR =PNIMPDAR+ "+cp_ToStrODBC(this.w_IMPDAR);
              +",PNIMPAVE =PNIMPAVE+ "+cp_ToStrODBC(this.w_IMPAVE);
                  +i_ccchkf ;
              +" where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  PNIMPDAR = PNIMPDAR + this.w_IMPDAR;
                  ,PNIMPAVE = PNIMPAVE + this.w_IMPAVE;
                  &i_ccchkf. ;
               where;
                  PNSERIAL = this.w_PNSERIAL;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        case .T.
          if this.o_PNSERIAL<>this.w_SERIALE OR this.o_CPROWNUM<>this.w_NUMRIGA
            * --- Write into PNT_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PNT_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PNIMPDAR =PNIMPDAR+ "+cp_ToStrODBC(this.w_IMPDAR);
              +",PNIMPAVE =PNIMPAVE+ "+cp_ToStrODBC(this.w_IMPAVE);
                  +i_ccchkf ;
              +" where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  PNIMPDAR = PNIMPDAR + this.w_IMPDAR;
                  ,PNIMPAVE = PNIMPAVE + this.w_IMPAVE;
                  &i_ccchkf. ;
               where;
                  PNSERIAL = this.w_PNSERIAL;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.o_PNSERIAL = this.w_SERIALE
            this.o_CPROWNUM = this.w_NUMRIGA
          endif
      endcase
    endif
    * --- Try
    local bErr_03F65850
    bErr_03F65850=bTrsErr
    this.Try_03F65850()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03F65850
    * --- End
    * --- Gestione Analitica
    if this.w_TIPCON="G" AND g_PERCCR="S" AND this.w_FLANAL="S" AND (this.w_TOTIMP<>0 OR this.w_REGOLA="Generazione rimanenze")
      * --- Leggo dal conto  il tipo di aggiornamento relativo al c. costo
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP,ANCCTAGG"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP,ANCCTAGG;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPCON;
              and ANCODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        this.w_CCTAGG = NVL(cp_ToDate(_read_.ANCCTAGG),cp_NullValue(_read_.ANCCTAGG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo dai Mastri la sezione di bilancio
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if g_APPLICATION = "ad hoc ENTERPRISE"
        * --- Read from BUSIUNIT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.BUSIUNIT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BUSIUNIT_idx,2],.t.,this.BUSIUNIT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BUFLANAL"+;
            " from "+i_cTable+" BUSIUNIT where ";
                +"BUCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and BUCODICE = "+cp_ToStrODBC(this.w_CODBUN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BUFLANAL;
            from (i_cTable) where;
                BUCODAZI = i_CODAZI;
                and BUCODICE = this.w_CODBUN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLBUANAL = NVL(cp_ToDate(_read_.BUFLANAL),cp_NullValue(_read_.BUFLANAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SEZB $ "CR" AND this.w_FLANAL="S" AND this.w_CCTAGG $ "AM" AND (this.w_FLBUANAL="S" OR g_PERBUN="N")
          if NOT EMPTY(this.w_CODCEN)
            this.w_NUM = this.w_NUM + 1
            * --- Aggiorna Analitica
            * --- Try
            local bErr_03F6A3B0
            bErr_03F6A3B0=bTrsErr
            this.Try_03F6A3B0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(AH_Msgformat(this.w_REGOLA),this.w_CODICE,this.w_CPROWNUM,ah_MsgFormat("Reg.N. %1 del %2 causale %3 manca analitica! (provvedere manualmente)",ALLTRIM(STR(this.w_PNNUMRER)),DTOC(this.w_PNDATREG),ALLTRIM(this.w_PNCODCAU)),this.w_TIPCON)
              INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ; 
 VALUES (this.w_REGOLA,this.w_CODICE,this.w_CPROWNUM,message(), this.w_TIPCON)
            endif
            bTrsErr=bTrsErr or bErr_03F6A3B0
            * --- End
          else
            INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(AH_Msgformat(this.w_REGOLA),this.w_CODICE,this.w_CPROWNUM,ah_MsgFormat("Reg.N. %1 del %2 causale %3 manca analitica! (provvedere manualmente)",ALLTRIM(STR(this.w_PNNUMRER)),DTOC(this.w_PNDATREG),ALLTRIM(this.w_PNCODCAU)),this.w_TIPCON)
          endif
        endif
      else
        if this.w_SEZB $ "CR" AND this.w_CCTAGG $ "AM" AND this.w_NUOVOCONTO
          this.w_MESS_ERR = GSAR_BRA(This,this.w_PNSERIAL, this.w_CPROWNUM, this.w_TIPCON, this.w_CODICE, this.w_TOTIMP)
          if Not EMPTY(this.w_MESS_ERR)
            INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(AH_Msgformat(this.w_REGOLA),this.w_CODICE,this.w_CPROWNUM,ah_MsgFormat("Reg. n. %1 del %2 causale %3 %4",ALLTRIM(STR(this.w_PNNUMRER)),DTOC(this.w_PNDATREG),ALLTRIM(this.w_PNCODCAU),this.w_MESS_ERR),this.w_TIPCON)
          endif
        endif
      endif
    endif
    this.w_CONTA3 = this.w_CONTA3 + 1
    SELECT TmpAgg
    ENDSCAN
    return
  proc Try_03F65850()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Faccio l'inizializzazione del saldo nel caso non sia mai stato movimentato il conto
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_TIPCON,'SLCODICE',this.w_CODICE,'SLCODESE',this.w_PNCODESE)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE &i_ccchkf. );
         values (;
           this.w_TIPCON;
           ,this.w_CODICE;
           ,this.w_PNCODESE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03F6A3B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVICOST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MRSERIAL"+",MRROWORD"+",CPROWNUM"+",MRCODVOC"+",MRCODICE"+",MRCODCOM"+",MR_SEGNO"+",MRTOTIMP"+",MRINICOM"+",MRFINCOM"+",MRFLRIPA"+",MRPARAME"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'MOVICOST','MRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOVICOST','MRROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUM),'MOVICOST','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VOCCEN),'MOVICOST','MRCODVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCEN),'MOVICOST','MRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'MOVICOST','MRCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEGNO),'MOVICOST','MR_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTIMP),'MOVICOST','MRTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATCIN),'MOVICOST','MRINICOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATCFI),'MOVICOST','MRFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(""),'MOVICOST','MRFLRIPA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PARAMETRO),'MOVICOST','MRPARAME');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',this.w_PNSERIAL,'MRROWORD',this.w_CPROWNUM,'CPROWNUM',this.w_NUM,'MRCODVOC',this.w_VOCCEN,'MRCODICE',this.w_CODCEN,'MRCODCOM',this.w_CODCOM,'MR_SEGNO',this.w_SEGNO,'MRTOTIMP',this.w_TOTIMP,'MRINICOM',this.w_DATCIN,'MRFINCOM',this.w_DATCFI,'MRFLRIPA',"",'MRPARAME',this.w_PARAMETRO)
      insert into (i_cTable) (MRSERIAL,MRROWORD,CPROWNUM,MRCODVOC,MRCODICE,MRCODCOM,MR_SEGNO,MRTOTIMP,MRINICOM,MRFINCOM,MRFLRIPA,MRPARAME &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_NUM;
           ,this.w_VOCCEN;
           ,this.w_CODCEN;
           ,this.w_CODCOM;
           ,this.w_SEGNO;
           ,this.w_TOTIMP;
           ,this.w_DATCIN;
           ,this.w_DATCFI;
           ,"";
           ,this.w_PARAMETRO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione Variabili
    * --- Movimenti provvisori per la genrazione dei Ratei e Risconti
    * --- Generazione scritture di storno
    * --- Periodo di selezione
    * --- Data Registrazione
    * --- Numero e alfa documento
    * --- Data documento
    * --- Dati per documenti da emettere
    * --- Dati per documenti da ricevere
    * --- Dati per rimanenze
    * --- Dati per risconti/ratei
    * --- Dati per cespiti
    this.w_ERROR = .f.
    this.w_PNCODUTE = IIF(g_UNIUTE="S", 0, i_CODUTE)
    this.w_CONTA1 = 0
    this.w_CONTA2 = 0
    this.w_CONTA3 = 0
    this.w_STORNO = .F.
    this.w_VALNAZ = g_PERVAL
    * --- Campi gestione Analitica
    * --- Generazione fatture da emettere
    this.w_QUERY1 = "QUERY\SASFA011.VQR"
    this.w_QUERY7 = "QUERY\SASFA013.VQR"
    * --- Generazione da Cespiti
    this.w_QUERY2 = "QUERY\SASCE001.VQR"
    * --- Generazione da Ratei / Risconti
    this.w_QUERY3 = "QUERY\SASCG001.VQR"
    this.w_QUERY8 = "QUERY\SASCG002.VQR"
    * --- Generazione fatture da ricevere
    this.w_QUERY4 = "QUERY\SASFA001.VQR"
    this.w_QUERY6 = "QUERY\SASFA003.VQR"
    * --- Generazione da Rimanenze
    this.w_QUERY5 = "QUERY\SASRI001.VQR"
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera Temporaneo per scritture e aggiorna primanota
    if EMPTY(this.oParentObject.w_COFATEME) AND this.oParentObject.w_ASFLDOCE="S" AND this.w_REGOLA="Generazione fatture da emettere"
      this.w_ERROR = .T.
      INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
      VALUES(AH_Msgformat(this.w_REGOLA),"",this.w_CPROWNUM,ah_MsgFormat("Manca la contropartita fatture da emettere"),"")
    endif
    if EMPTY(this.oParentObject.w_COIVADEB) AND this.oParentObject.w_ASFLDOCE="S" AND this.w_REGOLA="Generazione fatture da emettere"
      this.w_ERROR = .T.
      INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
      VALUES(AH_Msgformat(this.w_REGOLA),"",this.w_CPROWNUM,ah_MsgFormat("Manca la contropartita IVA fatture da emettere"),"")
    endif
    if EMPTY(this.oParentObject.w_COFATRIC) AND this.oParentObject.w_ASFLDOCR="S" AND this.w_REGOLA="Generazione fatture da ricevere"
      this.w_ERROR = .T.
      INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
      VALUES(AH_Msgformat(this.w_REGOLA),"",this.w_CPROWNUM,ah_MsgFormat("Manca la contropartita fatture da ricevere"),"")
    endif
    if EMPTY(this.oParentObject.w_CORATATT) AND this.oParentObject.w_ASFLRATE="S" AND this.w_REGOLA="Generazione ratei e risconti"
      this.w_ERROR = .T.
      INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
      VALUES(AH_Msgformat(this.w_REGOLA),"",this.w_CPROWNUM,ah_MsgFormat("Manca la contropartita ratei attivi"),"")
    endif
    if EMPTY(this.oParentObject.w_CORATPAS) AND this.oParentObject.w_ASFLRATE="S" AND this.w_REGOLA="Generazione ratei e risconti"
      this.w_ERROR = .T.
      INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
      VALUES(AH_Msgformat(this.w_REGOLA),"",this.w_CPROWNUM,ah_MsgFormat("Manca la contropartita ratei passivi"),"")
    endif
    if EMPTY(this.oParentObject.w_CORISATT) AND this.oParentObject.w_ASFLRATE="S" AND this.w_REGOLA="Generazione ratei e risconti"
      this.w_ERROR = .T.
      INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
      VALUES(AH_Msgformat(this.w_REGOLA),"",this.w_CPROWNUM,ah_MsgFormat("Manca la contropartita risconti attivi"),"")
    endif
    if EMPTY(this.oParentObject.w_CORISPAS) AND this.oParentObject.w_ASFLRATE="S" AND this.w_REGOLA="Generazione ratei e risconti"
      this.w_ERROR = .T.
      INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
      VALUES(AH_Msgformat(this.w_REGOLA),"",this.w_CPROWNUM,ah_MsgFormat("Manca la contropartita risconti passivi"),"")
    endif
    if EMPTY(NVL(this.w_PNCODCAU,""))
      this.w_ERROR = .T.
      INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
      VALUES(AH_Msgformat(this.w_REGOLA),"",this.w_CPROWNUM,ah_MsgFormat("Manca la causale contabile di assestamento"),"")
    endif
    if this.w_ERROR
      this.w_CONTA1 = this.w_CONTA1 + 1
    else
      if (this.oParentObject.w_ASFLSTOR="S" AND NOT EMPTY(NVL(this.w_PNCODCAS,""))) OR this.oParentObject.w_ASFLSTOR<>"S"
        this.w_PNDATREG = this.oParentObject.w_ASDATREG
        this.w_PNCODESE = g_CODESE
        this.w_PNSCRASS = "S"
        * --- Genera le registrazioni di primanota la regola
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.oParentObject.w_ASFLSTOR="S" AND (this.oParentObject.w_ASDATFIN<>this.oParentObject.w_FINESE OR this.w_REGOLA="Generazione ratei e risconti")
          * --- Metto Storno a True per girare gli importi
          *     Se data fine esercizio genero le scritture di storno solo per i Ratei e Risconti
          this.w_STORNO = .T.
          this.w_PNSCRASS = IIF(this.oParentObject.w_ASDATFIN=this.oParentObject.w_FINESE,"","S")
          this.w_PNCODCAU = this.w_PNCODCAS
          this.w_PNDATREG = this.oParentObject.w_ASDTRGST
          this.w_PNCODESE = IIF(this.oParentObject.w_ASDATFIN=this.oParentObject.w_FINESE,CALCESER(this.w_PNDATREG,this.w_PNCODESE),this.w_PNCODESE)
          if this.w_PNCODESE<>g_CODESE
            * --- Controllo se per il nuovo esercizio � cambiata la valuta di conto
            this.w_CNVALNAZ = g_PERVAL
            * --- Read from ESERCIZI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ESERCIZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ESVALNAZ"+;
                " from "+i_cTable+" ESERCIZI where ";
                    +"ESCODAZI = "+cp_ToStrODBC(i_codazi);
                    +" and ESCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ESVALNAZ;
                from (i_cTable) where;
                    ESCODAZI = i_codazi;
                    and ESCODESE = this.w_PNCODESE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              g_PERVAL = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_CNVALNAZ<>g_PERVAL
              * --- Read from VALUTE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VALUTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "VADECUNI,VADECTOT,VASIMVAL"+;
                  " from "+i_cTable+" VALUTE where ";
                      +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  VADECUNI,VADECTOT,VASIMVAL;
                  from (i_cTable) where;
                      VACODVAL = g_PERVAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                g_PERPUL = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
                g_PERPVL = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
                g_VALSIM = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_CAMBIO = GETCAM(g_PERVAL, I_DATSYS)
            endif
          endif
          if (this.oParentObject.w_ASDATFIN=this.oParentObject.w_FINESE AND this.w_REGOLA="Generazione ratei e risconti")
            * --- Per fare il calcolo solo dei risconti nel caso di storno di apertura
            *     elabora solo la query sui risconti e rilancio il batch di generazione
            if USED("TmpAgg")
              * --- Azzera temporaneo
              SELECT TmpAgg 
 USE
              CREATE CURSOR TmpAgg ; 
 (CODICE C(15), CODBUN C(3), DESCRI C(45), TIPCON C(1), COD001 C(15), COD002 C(15), ; 
 COD003 C(15), IMPDAR N(18,4), IMPAVE N(18,4), INICOM D(8), FINCOM D(8), SERIAL C(10), DESCRI2 C(50),PNSERIAL C(10),CPROWNUM N(5,0) ,; 
 CODCEN C(15), VOCCEN C(15), CODCOM C(15), DATCIN D(8), DATCFI D(8),PARAMETRO N(8,4),TOTIMP N(18,4),TIPO C(1),CODCON C(15))
            endif
            vq_exec(ALLTRIM(this.w_QUERY8), this, "EXTCG001")
            * --- Esegue batch di Elaborazione Ratei Risconti
            do GSCG_B01 with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Genera le registrazioni di primanota la regola
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_CNVALNAZ<>g_PERVAL AND NOT EMPTY(this.w_CNVALNAZ)
            g_PERVAL = this.w_CNVALNAZ
            this.w_CNVALNAZ = space(3)
            * --- Riporto i dati alla valuta di conto
            * --- Read from VALUTE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VALUTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VADECUNI,VADECTOT,VASIMVAL"+;
                " from "+i_cTable+" VALUTE where ";
                    +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VADECUNI,VADECTOT,VASIMVAL;
                from (i_cTable) where;
                    VACODVAL = g_PERVAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              g_PERPUL = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
              g_PERPVL = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
              g_VALSIM = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_CAMBIO = GETCAM(g_PERVAL, I_DATSYS)
          endif
          this.w_STORNO = .F.
        endif
        if USED("TmpAgg")
          * --- Azzera temporaneo
          SELECT TmpAgg 
 USE
          CREATE CURSOR TmpAgg ; 
 (CODICE C(15), CODBUN C(3), DESCRI C(45), TIPCON C(1), COD001 C(15), COD002 C(15), ; 
 COD003 C(15), IMPDAR N(18,4), IMPAVE N(18,4), INICOM D(8), FINCOM D(8), SERIAL C(10), DESCRI2 C(50),PNSERIAL C(10),CPROWNUM N(5,0) ,; 
 CODCEN C(15), VOCCEN C(15), CODCOM C(15), DATCIN D(8), DATCFI D(8),PARAMETRO N(8,4),TOTIMP N(18,4),TIPO C(1),CODCON C(15))
        endif
      else
        this.w_CONTA1 = this.w_CONTA1 + 1
        this.w_ERROR = .T.
        INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
        VALUES(AH_Msgformat(this.w_REGOLA),"",this.w_CPROWNUM,ah_MsgFormat("Manca la causale contabile di storno assestamento"),"")
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo che tutte le contropartite non siano obsolete alla data fine selezione
    this.w_TIPO = "G"
    if this.oParentObject.w_ASFLDOCR="S"
      if NOT EMPTY(this.oParentObject.w_COACQINC)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDTOBSO"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COACQINC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDTOBSO;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPO;
                and ANCODICE = this.oParentObject.w_COACQINC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DATOBSO<=this.oParentObject.w_ASDATFIN AND NOT EMPTY(this.w_DATOBSO)
          this.oParentObject.w_ASFLDOCR = " "
          this.w_ERROR = .t.
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese incasso acquisti obsoleta"),"")
        endif
      endif
      if NOT EMPTY(this.oParentObject.w_COACQIMB)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDTOBSO"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COACQIMB);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDTOBSO;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPO;
                and ANCODICE = this.oParentObject.w_COACQIMB;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DATOBSO<=this.oParentObject.w_ASDATFIN AND NOT EMPTY(this.w_DATOBSO)
          this.oParentObject.w_ASFLDOCR = " "
          this.w_ERROR = .t.
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese imballo acquisti obsoleta"),"")
        endif
      endif
      if NOT EMPTY(this.oParentObject.w_COACQTRA)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDTOBSO"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COACQTRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDTOBSO;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPO;
                and ANCODICE = this.oParentObject.w_COACQTRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DATOBSO<=this.oParentObject.w_ASDATFIN AND NOT EMPTY(this.w_DATOBSO)
          this.oParentObject.w_ASFLDOCR = " "
          this.w_ERROR = .t.
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese trasporto acquisti obsoleta"),"")
        endif
      endif
      if NOT EMPTY(this.oParentObject.w_COACQBOL)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDTOBSO"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COACQBOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDTOBSO;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPO;
                and ANCODICE = this.oParentObject.w_COACQBOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DATOBSO<=this.oParentObject.w_ASDATFIN AND NOT EMPTY(this.w_DATOBSO)
          this.oParentObject.w_ASFLDOCR = " "
          this.w_ERROR = .t.
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese bolli acquisti obsoleta"),"")
        endif
      endif
    endif
    if this.oParentObject.w_ASFLDOCE="S"
      if NOT EMPTY(this.oParentObject.w_COCONINC)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDTOBSO"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCONINC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDTOBSO;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPO;
                and ANCODICE = this.oParentObject.w_COCONINC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DATOBSO<=this.oParentObject.w_ASDATFIN AND NOT EMPTY(this.w_DATOBSO)
          this.oParentObject.w_ASFLDOCE = " "
          this.w_ERROR = .t.
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese incasso vendite obsoleta"),"")
        endif
      endif
      if NOT EMPTY(this.oParentObject.w_COCONIMB)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDTOBSO"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCONIMB);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDTOBSO;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPO;
                and ANCODICE = this.oParentObject.w_COCONIMB;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DATOBSO<=this.oParentObject.w_ASDATFIN AND NOT EMPTY(this.w_DATOBSO)
          this.oParentObject.w_ASFLDOCE = " "
          this.w_ERROR = .t.
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese imballo vendite obsoleta"),"")
        endif
      endif
      if NOT EMPTY(this.oParentObject.w_COCONTRA)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDTOBSO"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCONTRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDTOBSO;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPO;
                and ANCODICE = this.oParentObject.w_COCONTRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DATOBSO<=this.oParentObject.w_ASDATFIN AND NOT EMPTY(this.w_DATOBSO)
          this.oParentObject.w_ASFLDOCE = " "
          this.w_ERROR = .t.
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese trasporto vendite obsoleta"),"")
        endif
      endif
      if NOT EMPTY(this.oParentObject.w_COCONBOL)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDTOBSO"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCONBOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDTOBSO;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPO;
                and ANCODICE = this.oParentObject.w_COCONBOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DATOBSO<=this.oParentObject.w_ASDATFIN AND NOT EMPTY(this.w_DATOBSO)
          this.oParentObject.w_ASFLDOCE = " "
          this.w_ERROR = .t.
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese bolli vendite obsoleta"),"")
        endif
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_COCONDIC)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCONDIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_COCONDIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<=this.oParentObject.w_ASDATFIN AND NOT EMPTY(this.w_DATOBSO)
        this.oParentObject.w_ASFLDOCE = " "
        this.oParentObject.w_ASFLDOCR = " "
        this.w_ERROR = .t.
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita diff. di conversione obsoleta"),"")
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_COCONARR)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCONARR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_COCONARR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<=this.oParentObject.w_ASDATFIN AND NOT EMPTY(this.w_DATOBSO)
        this.oParentObject.w_ASFLDOCE = " "
        this.oParentObject.w_ASFLDOCR = " "
        this.w_ERROR = .t.
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita arrotondamenti obsoleta"),"")
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='PNT_DETT'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='BUSIUNIT'
    this.cWorkTables[6]='MOVICOST'
    this.cWorkTables[7]='SALDICON'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='VALUTE'
    this.cWorkTables[10]='MASTRI'
    return(this.OpenAllTables(10))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
