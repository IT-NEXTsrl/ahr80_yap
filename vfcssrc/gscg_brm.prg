* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_brm                                                        *
*              Calcolo regime del margine                                      *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-15                                                      *
* Last revis.: 2009-09-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_brm",oParentObject)
return(i_retval)

define class tgscg_brm as StdBatch
  * --- Local variables
  w_CURSORE = space(10)
  w_PERIODOPRE = 0
  w_ANNOPRE = space(4)
  w_EMPTY = .f.
  w_MARGINE = 0
  w_MARGPER = 0
  w_TOTCORR = 0
  w_COUNTER = 0
  w_IMPOSTA = 0
  w_PERSCOIMP = 0
  w_TOTALEIVA = 0
  * --- WorkFile variables
  TMP_MAR_idx=0
  REG_MAR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Regime del Margine
    this.w_EMPTY = .T.
    * --- Verifico se la selezione presente in maschera � interna all'esercizio attivo
    if this.oParentObject.w_DATINI < g_INIESE or this.oParentObject.w_DATFIN > g_FINESE
      Ah_ErrorMsg("Il periodo selezionato non � di competenza dell'esercizio corrente",48,"")
      i_retcode = 'stop'
      return
    endif
    * --- Calcolo il periodo precedente all'attuale
    if this.oParentObject.w_NUMPER = 1
      this.w_PERIODOPRE = IIF(g_TIPDEN="T",4,12)
      this.w_ANNOPRE = str(val(this.oParentObject.w_ANNO) - 1,4) 
    else
      this.w_PERIODOPRE = this.oParentObject.w_NUMPER - 1
      this.w_ANNOPRE = this.oParentObject.w_ANNO 
    endif
    * --- Leggo il margine del periodo precedente, dalla tabella dei parametri associati all'attivit�
    * --- Read from REG_MAR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.REG_MAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REG_MAR_idx,2],.t.,this.REG_MAR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MAMARCAL"+;
        " from "+i_cTable+" REG_MAR where ";
            +"MACODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
            +" and MAPERRIF = "+cp_ToStrODBC(this.w_PERIODOPRE);
            +" and MAANNORI = "+cp_ToStrODBC(this.w_ANNOPRE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MAMARCAL;
        from (i_cTable) where;
            MACODATT = this.oParentObject.w_CODATT;
            and MAPERRIF = this.w_PERIODOPRE;
            and MAANNORI = this.w_ANNOPRE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MARGINE = NVL(cp_ToDate(_read_.MAMARCAL),cp_NullValue(_read_.MAMARCAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Create temporary table TMP_MAR
    i_nIdx=cp_AddTableDef('TMP_MAR') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCG_BRM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_MAR_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from TMP_MAR
    i_nConn=i_TableProp[this.TMP_MAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAR_idx,2],.t.,this.TMP_MAR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_MAR ";
           ,"_Curs_TMP_MAR")
    else
      select * from (i_cTable);
        into cursor _Curs_TMP_MAR
    endif
    if used('_Curs_TMP_MAR')
      select _Curs_TMP_MAR
      locate for 1=1
      do while not(eof())
      this.w_EMPTY = .F.
        select _Curs_TMP_MAR
        continue
      enddo
      use
    endif
    if this.w_EMPTY
      * --- Non ho saldi per il periodo da esaminare, interrompo l'elaborazione
      Ah_ErrorMsg("Per il periodo selezionato non esistono dati da stampare",16,"")
      i_retcode = 'stop'
      return
    endif
    Create cursor MARGINE (NAMEFIEL C(10),ALIQUOTA N(10,2),PERCENT N(8,4),IMPONIBI N(18,4),IMPOSTA N(18,4),CORRISPE N(18,4),MARGINE N(18,4),SEZIONE C(2))
    * --- NOTA:
    *     
    *     Ho preferito trattare separatamente le varie sezioni per un motivo di leggibilit� e
    *     manutenibilit� del codice stesso.
    * --- Per ottenere l'ordinamento corretto nella stampa divido l'estrazione in base al tipo registro
    vq_exec("QUERY\GSCG1BRM",this,"SEZIONE1")
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    vq_exec("QUERY\GSCG1BRM_Bis",this,"SEZIONE1")
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    vq_exec("QUERY\GSCG2BRM",this,"SEZIONE2")
     
 Select SEZIONE2 
 Go Top
    Scan
    Insert into MARGINE (NAMEFIEL,ALIQUOTA,IMPONIBI,IMPOSTA,SEZIONE) VALUES ("Aliquota",SEZIONE2.IVPERMAR,SEZIONE2.TOTIMPON,SEZIONE2.TOTIMPOS,"2")
    Select SEZIONE2
    Endscan
    Insert into MARGINE (NAMEFIEL,IMPONIBI,SEZIONE) VALUES ("Margine",this.w_MARGINE,"3")
    this.w_MARGPER = this.w_MARGPER + iif(this.w_MARGINE < 0,this.w_MARGINE,0)
    if reccount("SEZIONE2") = 0
      * --- Non ho corrispettivi
      this.w_TOTALEIVA = this.w_MARGPER
    endif
    if this.w_MARGPER >0
      * --- Posso continuare con le altre sezioni della stampa solo sel il margine del periodo precedente � positivo
      this.w_COUNTER = 1
      Select Sezione2
      SUM TOTIMPON to this.w_TOTCORR
       
 Select Sezione2 
 Go Top
      Scan
      Dimension PERCRAP (this.w_COUNTER) 
      PERCRAP(this.w_COUNTER) = cp_round((100*TOTIMPON)/this.w_TOTCORR,2)
      Insert into MARGINE (NAMEFIEL,ALIQUOTA,PERCENT,CORRISPE,IMPONIBI,SEZIONE) VALUES ("PercAliq",SEZIONE2.IVPERMAR,PERCRAP(this.w_COUNTER),SEZIONE2.TOTIMPON,this.w_TOTCORR,"4")
      this.w_COUNTER = this.w_COUNTER + 1
      Endscan
      this.w_COUNTER = 1
       
 Select Sezione2 
 Go Top
      Scan
      Dimension MARGALIQ (this.w_COUNTER) 
      MARGALIQ(this.w_COUNTER) = cp_round((this.w_MARGPER*PERCRAP(this.w_COUNTER))/100,2)
      Insert into MARGINE (NAMEFIEL,ALIQUOTA,PERCENT,CORRISPE,IMPONIBI,SEZIONE) VALUES ("MargRel",SEZIONE2.IVPERMAR,PERCRAP(this.w_COUNTER),MARGALIQ(this.w_COUNTER),this.w_MARGPER,"5")
      this.w_COUNTER = this.w_COUNTER + 1
      Endscan
      this.w_COUNTER = 1
       
 Select Sezione2 
 Go Top
      Scan
      this.w_PERSCOIMP = (SEZIONE2.IVPERMAR/(100+SEZIONE2.IVPERMAR))
      this.w_IMPOSTA = MARGALIQ(this.w_COUNTER) * this.w_PERSCOIMP
      this.w_TOTALEIVA = this.w_TOTALEIVA + this.w_IMPOSTA
      Insert into MARGINE (NAMEFIEL,ALIQUOTA,MARGINE,PERCENT,IMPOSTA,SEZIONE) VALUES ("ScorImp",SEZIONE2.IVPERMAR,MARGALIQ(this.w_COUNTER),this.w_PERSCOIMP,this.w_IMPOSTA,"6")
      this.w_COUNTER = this.w_COUNTER + 1
      Endscan
      USE IN SELECT("SEZIONE2")
    endif
    Select * from MARGINE into cursor __TMP__
    USE IN SELECT("MARGINE")
    L_CODATT = this.oParentObject.w_CODATT
    L_NUMPER = this.oParentObject.w_NUMPER
    L_ANNO = this.oParentObject.w_ANNO
    Select __TMP__
    Cp_Chprn("QUERY\GSCG_SRM.frx")
    USE IN SELECT("__TMP__")
    * --- Drop temporary table TMP_MAR
    i_nIdx=cp_GetTableDefIdx('TMP_MAR')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_MAR')
    endif
    do case
      case this.oParentObject.w_FLDEFI="N"
        * --- Try
        local bErr_03945590
        bErr_03945590=bTrsErr
        this.Try_03945590()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into REG_MAR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.REG_MAR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.REG_MAR_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.REG_MAR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MAMARCAL ="+cp_NullLink(cp_ToStrODBC(this.w_MARGPER),'REG_MAR','MAMARCAL');
                +i_ccchkf ;
            +" where ";
                +"MACODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                +" and MAANNORI = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                +" and MAPERRIF = "+cp_ToStrODBC(this.oParentObject.w_NUMPER);
                   )
          else
            update (i_cTable) set;
                MAMARCAL = this.w_MARGPER;
                &i_ccchkf. ;
             where;
                MACODATT = this.oParentObject.w_CODATT;
                and MAANNORI = this.oParentObject.w_ANNO;
                and MAPERRIF = this.oParentObject.w_NUMPER;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_03945590
        * --- End
      case this.oParentObject.w_FLDEFI="S"
        * --- Stampa in definitiva, devo inserire un nuovo record nei parametri attivit�
        if Ah_YesNo("Vuoi aggiornare la data di ultima stampa del margine per il periodo in esame?")
          * --- Try
          local bErr_039438B0
          bErr_039438B0=bTrsErr
          this.Try_039438B0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into REG_MAR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.REG_MAR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.REG_MAR_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.REG_MAR_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MAMARCAL ="+cp_NullLink(cp_ToStrODBC(this.w_MARGPER),'REG_MAR','MAMARCAL');
              +",MADATSTA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'REG_MAR','MADATSTA');
                  +i_ccchkf ;
              +" where ";
                  +"MACODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                  +" and MAANNORI = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                  +" and MAPERRIF = "+cp_ToStrODBC(this.oParentObject.w_NUMPER);
                     )
            else
              update (i_cTable) set;
                  MAMARCAL = this.w_MARGPER;
                  ,MADATSTA = this.oParentObject.w_DATFIN;
                  &i_ccchkf. ;
               where;
                  MACODATT = this.oParentObject.w_CODATT;
                  and MAANNORI = this.oParentObject.w_ANNO;
                  and MAPERRIF = this.oParentObject.w_NUMPER;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_039438B0
          * --- End
        endif
    endcase
  endproc
  proc Try_03945590()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into REG_MAR
    i_nConn=i_TableProp[this.REG_MAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REG_MAR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REG_MAR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MACODATT"+",MAANNORI"+",MAPERRIF"+",MAMARCAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODATT),'REG_MAR','MACODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNO),'REG_MAR','MAANNORI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMPER),'REG_MAR','MAPERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MARGPER),'REG_MAR','MAMARCAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MACODATT',this.oParentObject.w_CODATT,'MAANNORI',this.oParentObject.w_ANNO,'MAPERRIF',this.oParentObject.w_NUMPER,'MAMARCAL',this.w_MARGPER)
      insert into (i_cTable) (MACODATT,MAANNORI,MAPERRIF,MAMARCAL &i_ccchkf. );
         values (;
           this.oParentObject.w_CODATT;
           ,this.oParentObject.w_ANNO;
           ,this.oParentObject.w_NUMPER;
           ,this.w_MARGPER;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_039438B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into REG_MAR
    i_nConn=i_TableProp[this.REG_MAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REG_MAR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REG_MAR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MACODATT"+",MAANNORI"+",MAPERRIF"+",MAMARCAL"+",MADATSTA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODATT),'REG_MAR','MACODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNO),'REG_MAR','MAANNORI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMPER),'REG_MAR','MAPERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MARGPER),'REG_MAR','MAMARCAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'REG_MAR','MADATSTA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MACODATT',this.oParentObject.w_CODATT,'MAANNORI',this.oParentObject.w_ANNO,'MAPERRIF',this.oParentObject.w_NUMPER,'MAMARCAL',this.w_MARGPER,'MADATSTA',this.oParentObject.w_DATFIN)
      insert into (i_cTable) (MACODATT,MAANNORI,MAPERRIF,MAMARCAL,MADATSTA &i_ccchkf. );
         values (;
           this.oParentObject.w_CODATT;
           ,this.oParentObject.w_ANNO;
           ,this.oParentObject.w_NUMPER;
           ,this.w_MARGPER;
           ,this.oParentObject.w_DATFIN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     
 Select SEZIONE1 
 Go Top
    Scan
    Insert into MARGINE (NAMEFIEL,IMPONIBI,IMPOSTA,SEZIONE) VALUES (SEZIONE1.IVREGMAR,SEZIONE1.TOTIMPON,SEZIONE1.TOTIMPOS,"1")
    Select SEZIONE1
    do case
      case SEZIONE1.IVREGMAR = "A"
        this.w_MARGPER = this.w_MARGPER - SEZIONE1.TOTIMPON
      case SEZIONE1.IVREGMAR = "S"
        this.w_MARGPER = this.w_MARGPER - SEZIONE1.TOTIMPON - SEZIONE1.TOTIMPOS
      case SEZIONE1.IVREGMAR = "C"
        this.w_MARGPER = this.w_MARGPER + SEZIONE1.TOTIMPON
    endcase
    Endscan
    USE IN SELECT("SEZIONE1")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMP_MAR'
    this.cWorkTables[2]='REG_MAR'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_TMP_MAR')
      use in _Curs_TMP_MAR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
