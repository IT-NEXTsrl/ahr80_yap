* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mdd                                                        *
*              Dettaglio dimensioni                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-23                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mdd")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mdd")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mdd")
  return

* --- Class definition
define class tgscg_mdd as StdPCForm
  Width  = 724
  Height = 245
  Top    = 114
  Left   = 141
  cComment = "Dettaglio dimensioni"
  cPrg = "gscg_mdd"
  HelpContextID=80356713
  add object cnt as tcgscg_mdd
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mdd as PCContext
  w_DICODTOT = space(10)
  w_DI_FONTE = space(10)
  w_CPROWORD = 0
  w_DICODICE = space(15)
  w_DIARLINK = space(15)
  w_DIDESCRI = space(80)
  w_DITIPDAT = space(1)
  w_DITIPDIM = space(2)
  w_DIVLCOMN = 0
  w_TIPCON = space(1)
  w_DIVALNUM = 0
  w_DIVALDAT = space(8)
  w_DIVALCAU = space(5)
  w_DIVLCOMC = space(10)
  w_DIVALCHR = space(50)
  w_DIVALLIN = space(5)
  w_DIVALATT = space(5)
  w_DIVALCON = space(15)
  w_ADESCRI = space(80)
  w_NDESCRI = space(80)
  w_CDESCRI = space(80)
  w_KDESCRI = space(80)
  w_VDESCRI = space(80)
  w_DESCRP = space(10)
  w_UDESCRI = space(80)
  w_DESCRC = space(10)
  w_DESCRK = space(10)
  w_DESCRL = space(10)
  w_DIFLZERO = space(1)
  w_DESCRI = space(80)
  w_TIPO = space(1)
  w_OBTEST = space(10)
  proc Save(i_oFrom)
    this.w_DICODTOT = i_oFrom.w_DICODTOT
    this.w_DI_FONTE = i_oFrom.w_DI_FONTE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DICODICE = i_oFrom.w_DICODICE
    this.w_DIARLINK = i_oFrom.w_DIARLINK
    this.w_DIDESCRI = i_oFrom.w_DIDESCRI
    this.w_DITIPDAT = i_oFrom.w_DITIPDAT
    this.w_DITIPDIM = i_oFrom.w_DITIPDIM
    this.w_DIVLCOMN = i_oFrom.w_DIVLCOMN
    this.w_TIPCON = i_oFrom.w_TIPCON
    this.w_DIVALNUM = i_oFrom.w_DIVALNUM
    this.w_DIVALDAT = i_oFrom.w_DIVALDAT
    this.w_DIVALCAU = i_oFrom.w_DIVALCAU
    this.w_DIVLCOMC = i_oFrom.w_DIVLCOMC
    this.w_DIVALCHR = i_oFrom.w_DIVALCHR
    this.w_DIVALLIN = i_oFrom.w_DIVALLIN
    this.w_DIVALATT = i_oFrom.w_DIVALATT
    this.w_DIVALCON = i_oFrom.w_DIVALCON
    this.w_ADESCRI = i_oFrom.w_ADESCRI
    this.w_NDESCRI = i_oFrom.w_NDESCRI
    this.w_CDESCRI = i_oFrom.w_CDESCRI
    this.w_KDESCRI = i_oFrom.w_KDESCRI
    this.w_VDESCRI = i_oFrom.w_VDESCRI
    this.w_DESCRP = i_oFrom.w_DESCRP
    this.w_UDESCRI = i_oFrom.w_UDESCRI
    this.w_DESCRC = i_oFrom.w_DESCRC
    this.w_DESCRK = i_oFrom.w_DESCRK
    this.w_DESCRL = i_oFrom.w_DESCRL
    this.w_DIFLZERO = i_oFrom.w_DIFLZERO
    this.w_DESCRI = i_oFrom.w_DESCRI
    this.w_TIPO = i_oFrom.w_TIPO
    this.w_OBTEST = i_oFrom.w_OBTEST
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DICODTOT = this.w_DICODTOT
    i_oTo.w_DI_FONTE = this.w_DI_FONTE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DICODICE = this.w_DICODICE
    i_oTo.w_DIARLINK = this.w_DIARLINK
    i_oTo.w_DIDESCRI = this.w_DIDESCRI
    i_oTo.w_DITIPDAT = this.w_DITIPDAT
    i_oTo.w_DITIPDIM = this.w_DITIPDIM
    i_oTo.w_DIVLCOMN = this.w_DIVLCOMN
    i_oTo.w_TIPCON = this.w_TIPCON
    i_oTo.w_DIVALNUM = this.w_DIVALNUM
    i_oTo.w_DIVALDAT = this.w_DIVALDAT
    i_oTo.w_DIVALCAU = this.w_DIVALCAU
    i_oTo.w_DIVLCOMC = this.w_DIVLCOMC
    i_oTo.w_DIVALCHR = this.w_DIVALCHR
    i_oTo.w_DIVALLIN = this.w_DIVALLIN
    i_oTo.w_DIVALATT = this.w_DIVALATT
    i_oTo.w_DIVALCON = this.w_DIVALCON
    i_oTo.w_ADESCRI = this.w_ADESCRI
    i_oTo.w_NDESCRI = this.w_NDESCRI
    i_oTo.w_CDESCRI = this.w_CDESCRI
    i_oTo.w_KDESCRI = this.w_KDESCRI
    i_oTo.w_VDESCRI = this.w_VDESCRI
    i_oTo.w_DESCRP = this.w_DESCRP
    i_oTo.w_UDESCRI = this.w_UDESCRI
    i_oTo.w_DESCRC = this.w_DESCRC
    i_oTo.w_DESCRK = this.w_DESCRK
    i_oTo.w_DESCRL = this.w_DESCRL
    i_oTo.w_DIFLZERO = this.w_DIFLZERO
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.w_TIPO = this.w_TIPO
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mdd as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 724
  Height = 245
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=80356713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DET_DIME_IDX = 0
  TAB_DIME_IDX = 0
  DETCOMBO_IDX = 0
  CAU_CONT_IDX = 0
  VOCIIVA_IDX = 0
  ATTIMAST_IDX = 0
  CONTI_IDX = 0
  TAB_FONT_IDX = 0
  cFile = "DET_DIME"
  cKeySelect = "DICODTOT"
  cKeyWhere  = "DICODTOT=this.w_DICODTOT"
  cKeyDetail  = "DICODTOT=this.w_DICODTOT and DICODICE=this.w_DICODICE"
  cKeyWhereODBC = '"DICODTOT="+cp_ToStrODBC(this.w_DICODTOT)';

  cKeyDetailWhereODBC = '"DICODTOT="+cp_ToStrODBC(this.w_DICODTOT)';
      +'+" and DICODICE="+cp_ToStrODBC(this.w_DICODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DET_DIME.DICODTOT="+cp_ToStrODBC(this.w_DICODTOT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DET_DIME.CPROWORD '
  cPrg = "gscg_mdd"
  cComment = "Dettaglio dimensioni"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DICODTOT = space(10)
  w_DI_FONTE = space(10)
  w_CPROWORD = 0
  w_DICODICE = space(15)
  o_DICODICE = space(15)
  w_DIARLINK = space(15)
  w_DIDESCRI = space(80)
  w_DITIPDAT = space(1)
  w_DITIPDIM = space(2)
  w_DIVLCOMN = 0
  o_DIVLCOMN = 0
  w_TIPCON = space(1)
  w_DIVALNUM = 0
  o_DIVALNUM = 0
  w_DIVALDAT = ctod('  /  /  ')
  o_DIVALDAT = ctod('  /  /  ')
  w_DIVALCAU = space(5)
  o_DIVALCAU = space(5)
  w_DIVLCOMC = space(10)
  o_DIVLCOMC = space(10)
  w_DIVALCHR = space(50)
  o_DIVALCHR = space(50)
  w_DIVALLIN = space(5)
  o_DIVALLIN = space(5)
  w_DIVALATT = space(5)
  o_DIVALATT = space(5)
  w_DIVALCON = space(15)
  o_DIVALCON = space(15)
  w_ADESCRI = space(80)
  w_NDESCRI = space(80)
  w_CDESCRI = space(80)
  w_KDESCRI = space(80)
  w_VDESCRI = space(80)
  w_DESCRP = space(10)
  w_UDESCRI = space(80)
  w_DESCRC = space(10)
  w_DESCRK = space(10)
  w_DESCRL = space(10)
  w_DIFLZERO = space(1)
  o_DIFLZERO = space(1)
  w_DESCRI = space(80)
  w_TIPO = space(1)
  w_OBTEST = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mddPag1","gscg_mdd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='TAB_DIME'
    this.cWorkTables[2]='DETCOMBO'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='VOCIIVA'
    this.cWorkTables[5]='ATTIMAST'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='TAB_FONT'
    this.cWorkTables[8]='DET_DIME'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DET_DIME_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DET_DIME_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mdd'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_2_10_joined
    link_2_10_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    local link_2_14_joined
    link_2_14_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DET_DIME where DICODTOT=KeySet.DICODTOT
    *                            and DICODICE=KeySet.DICODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DET_DIME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_DIME_IDX,2],this.bLoadRecFilter,this.DET_DIME_IDX,"gscg_mdd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DET_DIME')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DET_DIME.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DET_DIME '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_14_joined=this.AddJoinedLink_2_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DICODTOT',this.w_DICODTOT  )
      select * from (i_cTable) DET_DIME where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPO = space(1)
        .w_DICODTOT = NVL(DICODTOT,space(10))
        .w_DI_FONTE = NVL(DI_FONTE,space(10))
          if link_1_2_joined
            this.w_DI_FONTE = NVL(FOCODICE102,NVL(this.w_DI_FONTE,space(10)))
            this.w_TIPO = NVL(FO__TIPO102,space(1))
          else
          .link_1_2('Load')
          endif
        .w_TIPCON = 'G'
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DET_DIME')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_ADESCRI = space(80)
          .w_NDESCRI = space(80)
          .w_CDESCRI = space(80)
          .w_KDESCRI = space(80)
          .w_VDESCRI = space(80)
          .w_UDESCRI = space(80)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DICODICE = NVL(DICODICE,space(15))
          * evitabile
          *.link_2_2('Load')
          .w_DIARLINK = NVL(DIARLINK,space(15))
          .w_DIDESCRI = NVL(DIDESCRI,space(80))
          .w_DITIPDAT = NVL(DITIPDAT,space(1))
          .w_DITIPDIM = NVL(DITIPDIM,space(2))
          .w_DIVLCOMN = NVL(DIVLCOMN,0)
          .link_2_7('Load')
          .w_DIVALNUM = NVL(DIVALNUM,0)
          .w_DIVALDAT = NVL(cp_ToDate(DIVALDAT),ctod("  /  /  "))
          .w_DIVALCAU = NVL(DIVALCAU,space(5))
          if link_2_10_joined
            this.w_DIVALCAU = NVL(CCCODICE210,NVL(this.w_DIVALCAU,space(5)))
            this.w_UDESCRI = NVL(CCDESCRI210,space(80))
          else
          .link_2_10('Load')
          endif
          .w_DIVLCOMC = NVL(DIVLCOMC,space(10))
          .link_2_11('Load')
          .w_DIVALCHR = NVL(DIVALCHR,space(50))
          .w_DIVALLIN = NVL(DIVALLIN,space(5))
          if link_2_13_joined
            this.w_DIVALLIN = NVL(IVCODIVA213,NVL(this.w_DIVALLIN,space(5)))
            this.w_VDESCRI = NVL(IVDESIVA213,space(80))
          else
          .link_2_13('Load')
          endif
          .w_DIVALATT = NVL(DIVALATT,space(5))
          if link_2_14_joined
            this.w_DIVALATT = NVL(ATCODATT214,NVL(this.w_DIVALATT,space(5)))
            this.w_ADESCRI = NVL(ATDESATT214,space(80))
          else
          .link_2_14('Load')
          endif
          .w_DIVALCON = NVL(DIVALCON,space(15))
          .link_2_15('Load')
        .w_DESCRP = ICASE(.w_DITIPDIM='CI','Data iniziale',.w_DITIPDIM='CF','Data Finale',.w_DITIPDIM='PE','Periodo',.w_DITIPDIM='AN','Anno','')
        .w_DESCRC = ICASE(.w_DITIPDIM='CC' AND .w_DITIPDAT='C',.w_KDESCRI,.w_DITIPDIM='CC' AND .w_DITIPDAT='N',.w_NDESCRI,'')
        .w_DESCRK = ICASE(.w_DITIPDIM='LK' AND .w_DIARLINK='CONTI',.w_CDESCRI,.w_DITIPDIM='LK' AND .w_DIARLINK='VOCIIVA',.w_VDESCRI ,.w_DITIPDIM='LK' AND .w_DIARLINK='CAU_CONT',.w_UDESCRI,.w_DITIPDIM='AT' ,.w_ADESCRI,'')
        .w_DESCRL = ICASE(.w_DITIPDIM='LB' AND .w_DITIPDAT='C',.w_DIVALCHR,.w_DITIPDIM $ 'LB-AN-PE' AND .w_DITIPDAT='N',Alltrim(str(.w_DIVALNUM,20,5)),.w_DITIPDIM $ 'LB-CI-CF' AND .w_DITIPDAT='D',DTOC(.w_DIVALDAT),'')
          .w_DIFLZERO = NVL(DIFLZERO,space(1))
        .w_DESCRI = ICASE(.w_DIFLZERO='S','Valore di default',.w_DITIPDIM $ 'LB-CI-CF-AN-PE',.w_DESCRL,.w_DITIPDIM $ 'LK-AT',.w_DESCRK,.w_DESCRC)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TIPCON = 'G'
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DICODTOT=space(10)
      .w_DI_FONTE=space(10)
      .w_CPROWORD=10
      .w_DICODICE=space(15)
      .w_DIARLINK=space(15)
      .w_DIDESCRI=space(80)
      .w_DITIPDAT=space(1)
      .w_DITIPDIM=space(2)
      .w_DIVLCOMN=0
      .w_TIPCON=space(1)
      .w_DIVALNUM=0
      .w_DIVALDAT=ctod("  /  /  ")
      .w_DIVALCAU=space(5)
      .w_DIVLCOMC=space(10)
      .w_DIVALCHR=space(50)
      .w_DIVALLIN=space(5)
      .w_DIVALATT=space(5)
      .w_DIVALCON=space(15)
      .w_ADESCRI=space(80)
      .w_NDESCRI=space(80)
      .w_CDESCRI=space(80)
      .w_KDESCRI=space(80)
      .w_VDESCRI=space(80)
      .w_DESCRP=space(10)
      .w_UDESCRI=space(80)
      .w_DESCRC=space(10)
      .w_DESCRK=space(10)
      .w_DESCRL=space(10)
      .w_DIFLZERO=space(1)
      .w_DESCRI=space(80)
      .w_TIPO=space(1)
      .w_OBTEST=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_DI_FONTE = THIS.oParentObject .w_TI_FONTE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_DI_FONTE))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_DICODICE))
         .link_2_2('Full')
        endif
        .DoRTCalc(5,6,.f.)
        .w_DITIPDAT = 'C'
        .w_DITIPDIM = 'K'
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_DIVLCOMN))
         .link_2_7('Full')
        endif
        .w_TIPCON = 'G'
        .w_DIVALNUM = 0
        .w_DIVALDAT = cp_CharToDate('  -  -  ')
        .w_DIVALCAU = SPACE(5)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_DIVALCAU))
         .link_2_10('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_DIVLCOMC))
         .link_2_11('Full')
        endif
        .w_DIVALCHR = SPACE(50)
        .w_DIVALLIN = space(5)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_DIVALLIN))
         .link_2_13('Full')
        endif
        .w_DIVALATT = SPACE(5)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_DIVALATT))
         .link_2_14('Full')
        endif
        .w_DIVALCON = space(15)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_DIVALCON))
         .link_2_15('Full')
        endif
        .DoRTCalc(19,23,.f.)
        .w_DESCRP = ICASE(.w_DITIPDIM='CI','Data iniziale',.w_DITIPDIM='CF','Data Finale',.w_DITIPDIM='PE','Periodo',.w_DITIPDIM='AN','Anno','')
        .DoRTCalc(25,25,.f.)
        .w_DESCRC = ICASE(.w_DITIPDIM='CC' AND .w_DITIPDAT='C',.w_KDESCRI,.w_DITIPDIM='CC' AND .w_DITIPDAT='N',.w_NDESCRI,'')
        .w_DESCRK = ICASE(.w_DITIPDIM='LK' AND .w_DIARLINK='CONTI',.w_CDESCRI,.w_DITIPDIM='LK' AND .w_DIARLINK='VOCIIVA',.w_VDESCRI ,.w_DITIPDIM='LK' AND .w_DIARLINK='CAU_CONT',.w_UDESCRI,.w_DITIPDIM='AT' ,.w_ADESCRI,'')
        .w_DESCRL = ICASE(.w_DITIPDIM='LB' AND .w_DITIPDAT='C',.w_DIVALCHR,.w_DITIPDIM $ 'LB-AN-PE' AND .w_DITIPDAT='N',Alltrim(str(.w_DIVALNUM,20,5)),.w_DITIPDIM $ 'LB-CI-CF' AND .w_DITIPDAT='D',DTOC(.w_DIVALDAT),'')
        .w_DIFLZERO = iif(Not Empty(.w_DIVALCHR) OR .w_DIVALNUM>0 OR Not Empty(.w_DIVALDAT) OR Not Empty(.w_DIVALLIN) OR Not Empty(.w_DIVALATT) OR Not Empty(.w_DIVALCAU) OR Not Empty(.w_DIVALCON) ,'N',.w_DIFLZERO)   
        .w_DESCRI = ICASE(.w_DIFLZERO='S','Valore di default',.w_DITIPDIM $ 'LB-CI-CF-AN-PE',.w_DESCRL,.w_DITIPDIM $ 'LK-AT',.w_DESCRK,.w_DESCRC)
        .DoRTCalc(31,31,.f.)
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DET_DIME')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
    this.Calculate_QSMWXWJKJX()
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    this.Calculate_QSMWXWJKJX()
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDIVLCOMN_2_7.enabled = i_bVal
      .Page1.oPag.oDIVALNUM_2_8.enabled = i_bVal
      .Page1.oPag.oDIVALDAT_2_9.enabled = i_bVal
      .Page1.oPag.oDIVALCAU_2_10.enabled = i_bVal
      .Page1.oPag.oDIVLCOMC_2_11.enabled = i_bVal
      .Page1.oPag.oDIVALCHR_2_12.enabled = i_bVal
      .Page1.oPag.oDIVALLIN_2_13.enabled = i_bVal
      .Page1.oPag.oDIVALATT_2_14.enabled = i_bVal
      .Page1.oPag.oDIVALCON_2_15.enabled = i_bVal
      .Page1.oPag.oDIFLZERO_2_26.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DET_DIME',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DET_DIME_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODTOT,"DICODTOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DI_FONTE,"DI_FONTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_DICODICE C(15);
      ,t_DIDESCRI C(80);
      ,t_DITIPDAT N(3);
      ,t_DITIPDIM N(3);
      ,t_DIVLCOMN N(10);
      ,t_DIVALNUM N(20,5);
      ,t_DIVALDAT D(8);
      ,t_DIVALCAU C(5);
      ,t_DIVLCOMC C(10);
      ,t_DIVALCHR C(50);
      ,t_DIVALLIN C(5);
      ,t_DIVALATT C(5);
      ,t_DIVALCON C(15);
      ,t_DIFLZERO N(3);
      ,t_DESCRI C(80);
      ,CPROWNUM N(10);
      ,t_DIARLINK C(15);
      ,t_ADESCRI C(80);
      ,t_NDESCRI C(80);
      ,t_CDESCRI C(80);
      ,t_KDESCRI C(80);
      ,t_VDESCRI C(80);
      ,t_DESCRP C(10);
      ,t_UDESCRI C(80);
      ,t_DESCRC C(10);
      ,t_DESCRK C(10);
      ,t_DESCRL C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mddbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDICODICE_2_2.controlsource=this.cTrsName+'.t_DICODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIDESCRI_2_4.controlsource=this.cTrsName+'.t_DIDESCRI'
    this.oPgFRm.Page1.oPag.oDITIPDAT_2_5.controlsource=this.cTrsName+'.t_DITIPDAT'
    this.oPgFRm.Page1.oPag.oDITIPDIM_2_6.controlsource=this.cTrsName+'.t_DITIPDIM'
    this.oPgFRm.Page1.oPag.oDIVLCOMN_2_7.controlsource=this.cTrsName+'.t_DIVLCOMN'
    this.oPgFRm.Page1.oPag.oDIVALNUM_2_8.controlsource=this.cTrsName+'.t_DIVALNUM'
    this.oPgFRm.Page1.oPag.oDIVALDAT_2_9.controlsource=this.cTrsName+'.t_DIVALDAT'
    this.oPgFRm.Page1.oPag.oDIVALCAU_2_10.controlsource=this.cTrsName+'.t_DIVALCAU'
    this.oPgFRm.Page1.oPag.oDIVLCOMC_2_11.controlsource=this.cTrsName+'.t_DIVLCOMC'
    this.oPgFRm.Page1.oPag.oDIVALCHR_2_12.controlsource=this.cTrsName+'.t_DIVALCHR'
    this.oPgFRm.Page1.oPag.oDIVALLIN_2_13.controlsource=this.cTrsName+'.t_DIVALLIN'
    this.oPgFRm.Page1.oPag.oDIVALATT_2_14.controlsource=this.cTrsName+'.t_DIVALATT'
    this.oPgFRm.Page1.oPag.oDIVALCON_2_15.controlsource=this.cTrsName+'.t_DIVALCON'
    this.oPgFRm.Page1.oPag.oDIFLZERO_2_26.controlsource=this.cTrsName+'.t_DIFLZERO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_27.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(49)
    this.AddVLine(171)
    this.AddVLine(377)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DET_DIME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_DIME_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DET_DIME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_DIME_IDX,2])
      *
      * insert into DET_DIME
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DET_DIME')
        i_extval=cp_InsertValODBCExtFlds(this,'DET_DIME')
        i_cFldBody=" "+;
                  "(DICODTOT,DI_FONTE,CPROWORD,DICODICE,DIARLINK"+;
                  ",DIDESCRI,DITIPDAT,DITIPDIM,DIVLCOMN,DIVALNUM"+;
                  ",DIVALDAT,DIVALCAU,DIVLCOMC,DIVALCHR,DIVALLIN"+;
                  ",DIVALATT,DIVALCON,DIFLZERO,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DICODTOT)+","+cp_ToStrODBCNull(this.w_DI_FONTE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_DICODICE)+","+cp_ToStrODBC(this.w_DIARLINK)+;
             ","+cp_ToStrODBC(this.w_DIDESCRI)+","+cp_ToStrODBC(this.w_DITIPDAT)+","+cp_ToStrODBC(this.w_DITIPDIM)+","+cp_ToStrODBCNull(this.w_DIVLCOMN)+","+cp_ToStrODBC(this.w_DIVALNUM)+;
             ","+cp_ToStrODBC(this.w_DIVALDAT)+","+cp_ToStrODBCNull(this.w_DIVALCAU)+","+cp_ToStrODBCNull(this.w_DIVLCOMC)+","+cp_ToStrODBC(this.w_DIVALCHR)+","+cp_ToStrODBCNull(this.w_DIVALLIN)+;
             ","+cp_ToStrODBCNull(this.w_DIVALATT)+","+cp_ToStrODBCNull(this.w_DIVALCON)+","+cp_ToStrODBC(this.w_DIFLZERO)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DET_DIME')
        i_extval=cp_InsertValVFPExtFlds(this,'DET_DIME')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DICODTOT',this.w_DICODTOT,'DICODICE',this.w_DICODICE)
        INSERT INTO (i_cTable) (;
                   DICODTOT;
                  ,DI_FONTE;
                  ,CPROWORD;
                  ,DICODICE;
                  ,DIARLINK;
                  ,DIDESCRI;
                  ,DITIPDAT;
                  ,DITIPDIM;
                  ,DIVLCOMN;
                  ,DIVALNUM;
                  ,DIVALDAT;
                  ,DIVALCAU;
                  ,DIVLCOMC;
                  ,DIVALCHR;
                  ,DIVALLIN;
                  ,DIVALATT;
                  ,DIVALCON;
                  ,DIFLZERO;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DICODTOT;
                  ,this.w_DI_FONTE;
                  ,this.w_CPROWORD;
                  ,this.w_DICODICE;
                  ,this.w_DIARLINK;
                  ,this.w_DIDESCRI;
                  ,this.w_DITIPDAT;
                  ,this.w_DITIPDIM;
                  ,this.w_DIVLCOMN;
                  ,this.w_DIVALNUM;
                  ,this.w_DIVALDAT;
                  ,this.w_DIVALCAU;
                  ,this.w_DIVLCOMC;
                  ,this.w_DIVALCHR;
                  ,this.w_DIVALLIN;
                  ,this.w_DIVALATT;
                  ,this.w_DIVALCON;
                  ,this.w_DIFLZERO;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DET_DIME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_DIME_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DICODICE))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DET_DIME')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " DI_FONTE="+cp_ToStrODBCNull(this.w_DI_FONTE)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DET_DIME')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  DI_FONTE=this.w_DI_FONTE;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DICODICE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DET_DIME
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DET_DIME')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DI_FONTE="+cp_ToStrODBCNull(this.w_DI_FONTE)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DIARLINK="+cp_ToStrODBC(this.w_DIARLINK)+;
                     ",DIDESCRI="+cp_ToStrODBC(this.w_DIDESCRI)+;
                     ",DITIPDAT="+cp_ToStrODBC(this.w_DITIPDAT)+;
                     ",DITIPDIM="+cp_ToStrODBC(this.w_DITIPDIM)+;
                     ",DIVLCOMN="+cp_ToStrODBCNull(this.w_DIVLCOMN)+;
                     ",DIVALNUM="+cp_ToStrODBC(this.w_DIVALNUM)+;
                     ",DIVALDAT="+cp_ToStrODBC(this.w_DIVALDAT)+;
                     ",DIVALCAU="+cp_ToStrODBCNull(this.w_DIVALCAU)+;
                     ",DIVLCOMC="+cp_ToStrODBCNull(this.w_DIVLCOMC)+;
                     ",DIVALCHR="+cp_ToStrODBC(this.w_DIVALCHR)+;
                     ",DIVALLIN="+cp_ToStrODBCNull(this.w_DIVALLIN)+;
                     ",DIVALATT="+cp_ToStrODBCNull(this.w_DIVALATT)+;
                     ",DIVALCON="+cp_ToStrODBCNull(this.w_DIVALCON)+;
                     ",DIFLZERO="+cp_ToStrODBC(this.w_DIFLZERO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DET_DIME')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DI_FONTE=this.w_DI_FONTE;
                     ,CPROWORD=this.w_CPROWORD;
                     ,DIARLINK=this.w_DIARLINK;
                     ,DIDESCRI=this.w_DIDESCRI;
                     ,DITIPDAT=this.w_DITIPDAT;
                     ,DITIPDIM=this.w_DITIPDIM;
                     ,DIVLCOMN=this.w_DIVLCOMN;
                     ,DIVALNUM=this.w_DIVALNUM;
                     ,DIVALDAT=this.w_DIVALDAT;
                     ,DIVALCAU=this.w_DIVALCAU;
                     ,DIVLCOMC=this.w_DIVLCOMC;
                     ,DIVALCHR=this.w_DIVALCHR;
                     ,DIVALLIN=this.w_DIVALLIN;
                     ,DIVALATT=this.w_DIVALATT;
                     ,DIVALCON=this.w_DIVALCON;
                     ,DIFLZERO=this.w_DIFLZERO;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DET_DIME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_DIME_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DICODICE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DET_DIME
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DICODICE))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DET_DIME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_DIME_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .w_DI_FONTE = THIS.oParentObject .w_TI_FONTE
          .link_1_2('Full')
        .DoRTCalc(3,9,.t.)
          .w_TIPCON = 'G'
        if .o_DICODICE<>.w_DICODICE
          .w_DIVALNUM = 0
        endif
        if .o_DICODICE<>.w_DICODICE
          .w_DIVALDAT = cp_CharToDate('  -  -  ')
        endif
        if .o_DICODICE<>.w_DICODICE
          .w_DIVALCAU = SPACE(5)
          .link_2_10('Full')
        endif
        .DoRTCalc(14,14,.t.)
        if .o_DICODICE<>.w_DICODICE
          .w_DIVALCHR = SPACE(50)
        endif
        if .o_DICODICE<>.w_DICODICE
          .w_DIVALLIN = space(5)
          .link_2_13('Full')
        endif
        if .o_DICODICE<>.w_DICODICE
          .w_DIVALATT = SPACE(5)
          .link_2_14('Full')
        endif
        if .o_DICODICE<>.w_DICODICE
          .w_DIVALCON = space(15)
          .link_2_15('Full')
        endif
        .DoRTCalc(19,23,.t.)
        if .o_DICODICE<>.w_DICODICE
          .w_DESCRP = ICASE(.w_DITIPDIM='CI','Data iniziale',.w_DITIPDIM='CF','Data Finale',.w_DITIPDIM='PE','Periodo',.w_DITIPDIM='AN','Anno','')
        endif
        .DoRTCalc(25,25,.t.)
        if .o_DICODICE<>.w_DICODICE.or. .o_DIVLCOMN<>.w_DIVLCOMN.or. .o_DIVLCOMC<>.w_DIVLCOMC
          .w_DESCRC = ICASE(.w_DITIPDIM='CC' AND .w_DITIPDAT='C',.w_KDESCRI,.w_DITIPDIM='CC' AND .w_DITIPDAT='N',.w_NDESCRI,'')
        endif
        if .o_DICODICE<>.w_DICODICE.or. .o_DIVALLIN<>.w_DIVALLIN.or. .o_DIVALATT<>.w_DIVALATT.or. .o_DIVALCAU<>.w_DIVALCAU.or. .o_DIVALCON<>.w_DIVALCON
          .w_DESCRK = ICASE(.w_DITIPDIM='LK' AND .w_DIARLINK='CONTI',.w_CDESCRI,.w_DITIPDIM='LK' AND .w_DIARLINK='VOCIIVA',.w_VDESCRI ,.w_DITIPDIM='LK' AND .w_DIARLINK='CAU_CONT',.w_UDESCRI,.w_DITIPDIM='AT' ,.w_ADESCRI,'')
        endif
        if .o_DICODICE<>.w_DICODICE.or. .o_DIVALCHR<>.w_DIVALCHR.or. .o_DIVALNUM<>.w_DIVALNUM.or. .o_DIVALDAT<>.w_DIVALDAT
          .w_DESCRL = ICASE(.w_DITIPDIM='LB' AND .w_DITIPDAT='C',.w_DIVALCHR,.w_DITIPDIM $ 'LB-AN-PE' AND .w_DITIPDAT='N',Alltrim(str(.w_DIVALNUM,20,5)),.w_DITIPDIM $ 'LB-CI-CF' AND .w_DITIPDAT='D',DTOC(.w_DIVALDAT),'')
        endif
        Local l_Dep1,l_Dep2
        l_Dep1= .o_DICODICE<>.w_DICODICE .or. .o_DIVALCHR<>.w_DIVALCHR .or. .o_DIVALDAT<>.w_DIVALDAT .or. .o_DIVALNUM<>.w_DIVALNUM .or. .o_DIVALCAU<>.w_DIVALCAU
        l_Dep2= .o_DIVALATT<>.w_DIVALATT .or. .o_DIVALCON<>.w_DIVALCON .or. .o_DIVALLIN<>.w_DIVALLIN
        if m.l_Dep1 .or. m.l_Dep2
          .w_DIFLZERO = iif(Not Empty(.w_DIVALCHR) OR .w_DIVALNUM>0 OR Not Empty(.w_DIVALDAT) OR Not Empty(.w_DIVALLIN) OR Not Empty(.w_DIVALATT) OR Not Empty(.w_DIVALCAU) OR Not Empty(.w_DIVALCON) ,'N',.w_DIFLZERO)   
        endif
        Local l_Dep1,l_Dep2,l_Dep3
        l_Dep1= .o_DICODICE<>.w_DICODICE .or. .o_DIVALNUM<>.w_DIVALNUM .or. .o_DIVALCHR<>.w_DIVALCHR .or. .o_DIVALDAT<>.w_DIVALDAT .or. .o_DIVALLIN<>.w_DIVALLIN
        l_Dep2= .o_DIVALCON<>.w_DIVALCON .or. .o_DIVALCAU<>.w_DIVALCAU .or. .o_DIVALATT<>.w_DIVALATT .or. .o_DIVLCOMC<>.w_DIVLCOMC .or. .o_DIVLCOMN<>.w_DIVLCOMN
        l_Dep3= .o_DIFLZERO<>.w_DIFLZERO .or. .o_DIVALATT<>.w_DIVALATT
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3
          .w_DESCRI = ICASE(.w_DIFLZERO='S','Valore di default',.w_DITIPDIM $ 'LB-CI-CF-AN-PE',.w_DESCRL,.w_DITIPDIM $ 'LK-AT',.w_DESCRK,.w_DESCRC)
        endif
        .DoRTCalc(31,31,.t.)
          .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DIARLINK with this.w_DIARLINK
      replace t_ADESCRI with this.w_ADESCRI
      replace t_NDESCRI with this.w_NDESCRI
      replace t_CDESCRI with this.w_CDESCRI
      replace t_KDESCRI with this.w_KDESCRI
      replace t_VDESCRI with this.w_VDESCRI
      replace t_DESCRP with this.w_DESCRP
      replace t_UDESCRI with this.w_UDESCRI
      replace t_DESCRC with this.w_DESCRC
      replace t_DESCRK with this.w_DESCRK
      replace t_DESCRL with this.w_DESCRL
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_QSMWXWJKJX()
    with this
          * --- Assegno Fonte di testata
          .w_DI_FONTE = .oParentObject .w_TI_FONTE
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDICODICE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDICODICE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oDIFLZERO_2_26.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDIFLZERO_2_26.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oDIVLCOMN_2_7.visible=!this.oPgFrm.Page1.oPag.oDIVLCOMN_2_7.mHide()
    this.oPgFrm.Page1.oPag.oDIVALNUM_2_8.visible=!this.oPgFrm.Page1.oPag.oDIVALNUM_2_8.mHide()
    this.oPgFrm.Page1.oPag.oDIVALDAT_2_9.visible=!this.oPgFrm.Page1.oPag.oDIVALDAT_2_9.mHide()
    this.oPgFrm.Page1.oPag.oDIVALCAU_2_10.visible=!this.oPgFrm.Page1.oPag.oDIVALCAU_2_10.mHide()
    this.oPgFrm.Page1.oPag.oDIVLCOMC_2_11.visible=!this.oPgFrm.Page1.oPag.oDIVLCOMC_2_11.mHide()
    this.oPgFrm.Page1.oPag.oDIVALCHR_2_12.visible=!this.oPgFrm.Page1.oPag.oDIVALCHR_2_12.mHide()
    this.oPgFrm.Page1.oPag.oDIVALLIN_2_13.visible=!this.oPgFrm.Page1.oPag.oDIVALLIN_2_13.mHide()
    this.oPgFrm.Page1.oPag.oDIVALATT_2_14.visible=!this.oPgFrm.Page1.oPag.oDIVALATT_2_14.mHide()
    this.oPgFrm.Page1.oPag.oDIVALCON_2_15.visible=!this.oPgFrm.Page1.oPag.oDIVALCON_2_15.mHide()
    this.oPgFrm.Page1.oPag.oDIFLZERO_2_26.visible=!this.oPgFrm.Page1.oPag.oDIFLZERO_2_26.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DI_FONTE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
    i_lTable = "TAB_FONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2], .t., this.TAB_FONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DI_FONTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DI_FONTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODICE,FO__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(this.w_DI_FONTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODICE',this.w_DI_FONTE)
            select FOCODICE,FO__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DI_FONTE = NVL(_Link_.FOCODICE,space(10))
      this.w_TIPO = NVL(_Link_.FO__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DI_FONTE = space(10)
      endif
      this.w_TIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])+'\'+cp_ToStr(_Link_.FOCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_FONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DI_FONTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_FONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.FOCODICE as FOCODICE102"+ ",link_1_2.FO__TIPO as FO__TIPO102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on DET_DIME.DI_FONTE=link_1_2.FOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and DET_DIME.DI_FONTE=link_1_2.FOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DICODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_DIME_IDX,3]
    i_lTable = "TAB_DIME"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_DIME_IDX,2], .t., this.TAB_DIME_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_DIME_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ADM',True,'TAB_DIME')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DICODICE like "+cp_ToStrODBC(trim(this.w_DICODICE)+"%");
                   +" and DI_FONTE="+cp_ToStrODBC(this.w_DI_FONTE);

          i_ret=cp_SQL(i_nConn,"select DI_FONTE,DICODICE,DIDESCRI,DIARLINK,DITIPDAT,DITIPDIM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DI_FONTE,DICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DI_FONTE',this.w_DI_FONTE;
                     ,'DICODICE',trim(this.w_DICODICE))
          select DI_FONTE,DICODICE,DIDESCRI,DIARLINK,DITIPDAT,DITIPDIM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DI_FONTE,DICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODICE)==trim(_Link_.DICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICODICE) and !this.bDontReportError
            deferred_cp_zoom('TAB_DIME','*','DI_FONTE,DICODICE',cp_AbsName(oSource.parent,'oDICODICE_2_2'),i_cWhere,'GSCG_ADM',"ELENCO DIMENSIONI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DI_FONTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DI_FONTE,DICODICE,DIDESCRI,DIARLINK,DITIPDAT,DITIPDIM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DI_FONTE,DICODICE,DIDESCRI,DIARLINK,DITIPDAT,DITIPDIM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DI_FONTE,DICODICE,DIDESCRI,DIARLINK,DITIPDAT,DITIPDIM";
                     +" from "+i_cTable+" "+i_lTable+" where DICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DI_FONTE="+cp_ToStrODBC(this.w_DI_FONTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DI_FONTE',oSource.xKey(1);
                       ,'DICODICE',oSource.xKey(2))
            select DI_FONTE,DICODICE,DIDESCRI,DIARLINK,DITIPDAT,DITIPDIM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DI_FONTE,DICODICE,DIDESCRI,DIARLINK,DITIPDAT,DITIPDIM";
                   +" from "+i_cTable+" "+i_lTable+" where DICODICE="+cp_ToStrODBC(this.w_DICODICE);
                   +" and DI_FONTE="+cp_ToStrODBC(this.w_DI_FONTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DI_FONTE',this.w_DI_FONTE;
                       ,'DICODICE',this.w_DICODICE)
            select DI_FONTE,DICODICE,DIDESCRI,DIARLINK,DITIPDAT,DITIPDIM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODICE = NVL(_Link_.DICODICE,space(15))
      this.w_DIDESCRI = NVL(_Link_.DIDESCRI,space(80))
      this.w_DIARLINK = NVL(_Link_.DIARLINK,space(15))
      this.w_DITIPDAT = NVL(_Link_.DITIPDAT,space(1))
      this.w_DITIPDIM = NVL(_Link_.DITIPDIM,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_DICODICE = space(15)
      endif
      this.w_DIDESCRI = space(80)
      this.w_DIARLINK = space(15)
      this.w_DITIPDAT = space(1)
      this.w_DITIPDIM = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_DIME_IDX,2])+'\'+cp_ToStr(_Link_.DI_FONTE,1)+'\'+cp_ToStr(_Link_.DICODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_DIME_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIVLCOMN
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DETCOMBO_IDX,3]
    i_lTable = "DETCOMBO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DETCOMBO_IDX,2], .t., this.DETCOMBO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DETCOMBO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVLCOMN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DETCOMBO')
        if i_nConn<>0
          i_cWhere = " DCVALNUM="+cp_ToStrODBC(this.w_DIVLCOMN);
                   +" and DC_FONTE="+cp_ToStrODBC(this.w_DI_FONTE);
                   +" and DCCODDIS="+cp_ToStrODBC(this.w_DICODICE);

          i_ret=cp_SQL(i_nConn,"select DC_FONTE,DCCODDIS,DCVALNUM,DCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DC_FONTE',this.w_DI_FONTE;
                     ,'DCCODDIS',this.w_DICODICE;
                     ,'DCVALNUM',this.w_DIVLCOMN)
          select DC_FONTE,DCCODDIS,DCVALNUM,DCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DIVLCOMN) and !this.bDontReportError
            deferred_cp_zoom('DETCOMBO','*','DC_FONTE,DCCODDIS,DCVALNUM',cp_AbsName(oSource.parent,'oDIVLCOMN_2_7'),i_cWhere,'',"",'GSCG_MDD.DETCOMBO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DI_FONTE<>oSource.xKey(1);
           .or. this.w_DICODICE<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DC_FONTE,DCCODDIS,DCVALNUM,DCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DC_FONTE,DCCODDIS,DCVALNUM,DCDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DC_FONTE,DCCODDIS,DCVALNUM,DCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DCVALNUM="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DC_FONTE="+cp_ToStrODBC(this.w_DI_FONTE);
                     +" and DCCODDIS="+cp_ToStrODBC(this.w_DICODICE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DC_FONTE',oSource.xKey(1);
                       ,'DCCODDIS',oSource.xKey(2);
                       ,'DCVALNUM',oSource.xKey(3))
            select DC_FONTE,DCCODDIS,DCVALNUM,DCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVLCOMN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DC_FONTE,DCCODDIS,DCVALNUM,DCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DCVALNUM="+cp_ToStrODBC(this.w_DIVLCOMN);
                   +" and DC_FONTE="+cp_ToStrODBC(this.w_DI_FONTE);
                   +" and DCCODDIS="+cp_ToStrODBC(this.w_DICODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DC_FONTE',this.w_DI_FONTE;
                       ,'DCCODDIS',this.w_DICODICE;
                       ,'DCVALNUM',this.w_DIVLCOMN)
            select DC_FONTE,DCCODDIS,DCVALNUM,DCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVLCOMN = NVL(_Link_.DCVALNUM,0)
      this.w_NDESCRI = NVL(_Link_.DCDESCRI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_DIVLCOMN = 0
      endif
      this.w_NDESCRI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DETCOMBO_IDX,2])+'\'+cp_ToStr(_Link_.DC_FONTE,1)+'\'+cp_ToStr(_Link_.DCCODDIS,1)+'\'+cp_ToStr(_Link_.DCVALNUM,1)
      cp_ShowWarn(i_cKey,this.DETCOMBO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVLCOMN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIVALCAU
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVALCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_DIVALCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_DIVALCAU))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVALCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_DIVALCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_DIVALCAU)+"%");

            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DIVALCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oDIVALCAU_2_10'),i_cWhere,'GSCG_ACC',"CAUSALI CONTABILI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVALCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_DIVALCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_DIVALCAU)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVALCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_UDESCRI = NVL(_Link_.CCDESCRI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_DIVALCAU = space(5)
      endif
      this.w_UDESCRI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVALCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.CCCODICE as CCCODICE210"+ ",link_2_10.CCDESCRI as CCDESCRI210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on DET_DIME.DIVALCAU=link_2_10.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and DET_DIME.DIVALCAU=link_2_10.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DIVLCOMC
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DETCOMBO_IDX,3]
    i_lTable = "DETCOMBO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DETCOMBO_IDX,2], .t., this.DETCOMBO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DETCOMBO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVLCOMC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DETCOMBO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DCVALCHR like "+cp_ToStrODBC(trim(this.w_DIVLCOMC)+"%");
                   +" and DC_FONTE="+cp_ToStrODBC(this.w_DI_FONTE);
                   +" and DCCODDIS="+cp_ToStrODBC(this.w_DICODICE);

          i_ret=cp_SQL(i_nConn,"select DC_FONTE,DCCODDIS,DCVALCHR,DCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DC_FONTE,DCCODDIS,DCVALCHR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DC_FONTE',this.w_DI_FONTE;
                     ,'DCCODDIS',this.w_DICODICE;
                     ,'DCVALCHR',trim(this.w_DIVLCOMC))
          select DC_FONTE,DCCODDIS,DCVALCHR,DCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DC_FONTE,DCCODDIS,DCVALCHR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVLCOMC)==trim(_Link_.DCVALCHR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIVLCOMC) and !this.bDontReportError
            deferred_cp_zoom('DETCOMBO','*','DC_FONTE,DCCODDIS,DCVALCHR',cp_AbsName(oSource.parent,'oDIVLCOMC_2_11'),i_cWhere,'',"ELENCO OPZIONI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DI_FONTE<>oSource.xKey(1);
           .or. this.w_DICODICE<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DC_FONTE,DCCODDIS,DCVALCHR,DCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DC_FONTE,DCCODDIS,DCVALCHR,DCDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DC_FONTE,DCCODDIS,DCVALCHR,DCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DCVALCHR="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DC_FONTE="+cp_ToStrODBC(this.w_DI_FONTE);
                     +" and DCCODDIS="+cp_ToStrODBC(this.w_DICODICE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DC_FONTE',oSource.xKey(1);
                       ,'DCCODDIS',oSource.xKey(2);
                       ,'DCVALCHR',oSource.xKey(3))
            select DC_FONTE,DCCODDIS,DCVALCHR,DCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVLCOMC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DC_FONTE,DCCODDIS,DCVALCHR,DCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DCVALCHR="+cp_ToStrODBC(this.w_DIVLCOMC);
                   +" and DC_FONTE="+cp_ToStrODBC(this.w_DI_FONTE);
                   +" and DCCODDIS="+cp_ToStrODBC(this.w_DICODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DC_FONTE',this.w_DI_FONTE;
                       ,'DCCODDIS',this.w_DICODICE;
                       ,'DCVALCHR',this.w_DIVLCOMC)
            select DC_FONTE,DCCODDIS,DCVALCHR,DCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVLCOMC = NVL(_Link_.DCVALCHR,space(10))
      this.w_KDESCRI = NVL(_Link_.DCDESCRI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_DIVLCOMC = space(10)
      endif
      this.w_KDESCRI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DETCOMBO_IDX,2])+'\'+cp_ToStr(_Link_.DC_FONTE,1)+'\'+cp_ToStr(_Link_.DCCODDIS,1)+'\'+cp_ToStr(_Link_.DCVALCHR,1)
      cp_ShowWarn(i_cKey,this.DETCOMBO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVLCOMC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIVALLIN
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVALLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_DIVALLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_DIVALLIN))
          select IVCODIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVALLIN)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_DIVALLIN)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_DIVALLIN)+"%");

            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DIVALLIN) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oDIVALLIN_2_13'),i_cWhere,'GSAR_AIV',"CODICI IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVALLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_DIVALLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_DIVALLIN)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVALLIN = NVL(_Link_.IVCODIVA,space(5))
      this.w_VDESCRI = NVL(_Link_.IVDESIVA,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_DIVALLIN = space(5)
      endif
      this.w_VDESCRI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVALLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.IVCODIVA as IVCODIVA213"+ ",link_2_13.IVDESIVA as IVDESIVA213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on DET_DIME.DIVALLIN=link_2_13.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and DET_DIME.DIVALLIN=link_2_13.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DIVALATT
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVALATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_DIVALATT)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_DIVALATT))
          select ATCODATT,ATDESATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVALATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIVALATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIMAST','*','ATCODATT',cp_AbsName(oSource.parent,'oDIVALATT_2_14'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVALATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_DIVALATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_DIVALATT)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVALATT = NVL(_Link_.ATCODATT,space(5))
      this.w_ADESCRI = NVL(_Link_.ATDESATT,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_DIVALATT = space(5)
      endif
      this.w_ADESCRI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVALATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ATTIMAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_14.ATCODATT as ATCODATT214"+ ",link_2_14.ATDESATT as ATDESATT214"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_14 on DET_DIME.DIVALATT=link_2_14.ATCODATT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_14"
          i_cKey=i_cKey+'+" and DET_DIME.DIVALATT=link_2_14.ATCODATT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DIVALCON
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVALCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DIVALCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_DIVALCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVALCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIVALCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDIVALCON_2_15'),i_cWhere,'GSAR_API',"CONTI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVALCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DIVALCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_DIVALCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVALCON = NVL(_Link_.ANCODICE,space(15))
      this.w_CDESCRI = NVL(_Link_.ANDESCRI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_DIVALCON = space(15)
      endif
      this.w_CDESCRI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVALCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDITIPDAT_2_5.RadioValue()==this.w_DITIPDAT)
      this.oPgFrm.Page1.oPag.oDITIPDAT_2_5.SetRadio()
      replace t_DITIPDAT with this.oPgFrm.Page1.oPag.oDITIPDAT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPDIM_2_6.RadioValue()==this.w_DITIPDIM)
      this.oPgFrm.Page1.oPag.oDITIPDIM_2_6.SetRadio()
      replace t_DITIPDIM with this.oPgFrm.Page1.oPag.oDITIPDIM_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVLCOMN_2_7.value==this.w_DIVLCOMN)
      this.oPgFrm.Page1.oPag.oDIVLCOMN_2_7.value=this.w_DIVLCOMN
      replace t_DIVLCOMN with this.oPgFrm.Page1.oPag.oDIVLCOMN_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVALNUM_2_8.value==this.w_DIVALNUM)
      this.oPgFrm.Page1.oPag.oDIVALNUM_2_8.value=this.w_DIVALNUM
      replace t_DIVALNUM with this.oPgFrm.Page1.oPag.oDIVALNUM_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVALDAT_2_9.value==this.w_DIVALDAT)
      this.oPgFrm.Page1.oPag.oDIVALDAT_2_9.value=this.w_DIVALDAT
      replace t_DIVALDAT with this.oPgFrm.Page1.oPag.oDIVALDAT_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVALCAU_2_10.value==this.w_DIVALCAU)
      this.oPgFrm.Page1.oPag.oDIVALCAU_2_10.value=this.w_DIVALCAU
      replace t_DIVALCAU with this.oPgFrm.Page1.oPag.oDIVALCAU_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVLCOMC_2_11.value==this.w_DIVLCOMC)
      this.oPgFrm.Page1.oPag.oDIVLCOMC_2_11.value=this.w_DIVLCOMC
      replace t_DIVLCOMC with this.oPgFrm.Page1.oPag.oDIVLCOMC_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVALCHR_2_12.value==this.w_DIVALCHR)
      this.oPgFrm.Page1.oPag.oDIVALCHR_2_12.value=this.w_DIVALCHR
      replace t_DIVALCHR with this.oPgFrm.Page1.oPag.oDIVALCHR_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVALLIN_2_13.value==this.w_DIVALLIN)
      this.oPgFrm.Page1.oPag.oDIVALLIN_2_13.value=this.w_DIVALLIN
      replace t_DIVALLIN with this.oPgFrm.Page1.oPag.oDIVALLIN_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVALATT_2_14.value==this.w_DIVALATT)
      this.oPgFrm.Page1.oPag.oDIVALATT_2_14.value=this.w_DIVALATT
      replace t_DIVALATT with this.oPgFrm.Page1.oPag.oDIVALATT_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVALCON_2_15.value==this.w_DIVALCON)
      this.oPgFrm.Page1.oPag.oDIVALCON_2_15.value=this.w_DIVALCON
      replace t_DIVALCON with this.oPgFrm.Page1.oPag.oDIVALCON_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFLZERO_2_26.RadioValue()==this.w_DIFLZERO)
      this.oPgFrm.Page1.oPag.oDIFLZERO_2_26.SetRadio()
      replace t_DIFLZERO with this.oPgFrm.Page1.oPag.oDIFLZERO_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODICE_2_2.value==this.w_DICODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODICE_2_2.value=this.w_DICODICE
      replace t_DICODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIDESCRI_2_4.value==this.w_DIDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIDESCRI_2_4.value=this.w_DIDESCRI
      replace t_DIDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIDESCRI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_27.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_27.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_27.value
    endif
    cp_SetControlsValueExtFlds(this,'DET_DIME')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_DICODICE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DICODICE = this.w_DICODICE
    this.o_DIVLCOMN = this.w_DIVLCOMN
    this.o_DIVALNUM = this.w_DIVALNUM
    this.o_DIVALDAT = this.w_DIVALDAT
    this.o_DIVALCAU = this.w_DIVALCAU
    this.o_DIVLCOMC = this.w_DIVLCOMC
    this.o_DIVALCHR = this.w_DIVALCHR
    this.o_DIVALLIN = this.w_DIVALLIN
    this.o_DIVALATT = this.w_DIVALATT
    this.o_DIVALCON = this.w_DIVALCON
    this.o_DIFLZERO = this.w_DIFLZERO
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DICODICE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_DICODICE=space(15)
      .w_DIARLINK=space(15)
      .w_DIDESCRI=space(80)
      .w_DITIPDAT=space(1)
      .w_DITIPDIM=space(2)
      .w_DIVLCOMN=0
      .w_DIVALNUM=0
      .w_DIVALDAT=ctod("  /  /  ")
      .w_DIVALCAU=space(5)
      .w_DIVLCOMC=space(10)
      .w_DIVALCHR=space(50)
      .w_DIVALLIN=space(5)
      .w_DIVALATT=space(5)
      .w_DIVALCON=space(15)
      .w_ADESCRI=space(80)
      .w_NDESCRI=space(80)
      .w_CDESCRI=space(80)
      .w_KDESCRI=space(80)
      .w_VDESCRI=space(80)
      .w_DESCRP=space(10)
      .w_UDESCRI=space(80)
      .w_DESCRC=space(10)
      .w_DESCRK=space(10)
      .w_DESCRL=space(10)
      .w_DIFLZERO=space(1)
      .w_DESCRI=space(80)
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_DICODICE))
        .link_2_2('Full')
      endif
      .DoRTCalc(5,6,.f.)
        .w_DITIPDAT = 'C'
        .w_DITIPDIM = 'K'
      .DoRTCalc(9,9,.f.)
      if not(empty(.w_DIVLCOMN))
        .link_2_7('Full')
      endif
      .DoRTCalc(10,10,.f.)
        .w_DIVALNUM = 0
        .w_DIVALDAT = cp_CharToDate('  -  -  ')
        .w_DIVALCAU = SPACE(5)
      .DoRTCalc(13,13,.f.)
      if not(empty(.w_DIVALCAU))
        .link_2_10('Full')
      endif
      .DoRTCalc(14,14,.f.)
      if not(empty(.w_DIVLCOMC))
        .link_2_11('Full')
      endif
        .w_DIVALCHR = SPACE(50)
        .w_DIVALLIN = space(5)
      .DoRTCalc(16,16,.f.)
      if not(empty(.w_DIVALLIN))
        .link_2_13('Full')
      endif
        .w_DIVALATT = SPACE(5)
      .DoRTCalc(17,17,.f.)
      if not(empty(.w_DIVALATT))
        .link_2_14('Full')
      endif
        .w_DIVALCON = space(15)
      .DoRTCalc(18,18,.f.)
      if not(empty(.w_DIVALCON))
        .link_2_15('Full')
      endif
      .DoRTCalc(19,23,.f.)
        .w_DESCRP = ICASE(.w_DITIPDIM='CI','Data iniziale',.w_DITIPDIM='CF','Data Finale',.w_DITIPDIM='PE','Periodo',.w_DITIPDIM='AN','Anno','')
      .DoRTCalc(25,25,.f.)
        .w_DESCRC = ICASE(.w_DITIPDIM='CC' AND .w_DITIPDAT='C',.w_KDESCRI,.w_DITIPDIM='CC' AND .w_DITIPDAT='N',.w_NDESCRI,'')
        .w_DESCRK = ICASE(.w_DITIPDIM='LK' AND .w_DIARLINK='CONTI',.w_CDESCRI,.w_DITIPDIM='LK' AND .w_DIARLINK='VOCIIVA',.w_VDESCRI ,.w_DITIPDIM='LK' AND .w_DIARLINK='CAU_CONT',.w_UDESCRI,.w_DITIPDIM='AT' ,.w_ADESCRI,'')
        .w_DESCRL = ICASE(.w_DITIPDIM='LB' AND .w_DITIPDAT='C',.w_DIVALCHR,.w_DITIPDIM $ 'LB-AN-PE' AND .w_DITIPDAT='N',Alltrim(str(.w_DIVALNUM,20,5)),.w_DITIPDIM $ 'LB-CI-CF' AND .w_DITIPDAT='D',DTOC(.w_DIVALDAT),'')
        .w_DIFLZERO = iif(Not Empty(.w_DIVALCHR) OR .w_DIVALNUM>0 OR Not Empty(.w_DIVALDAT) OR Not Empty(.w_DIVALLIN) OR Not Empty(.w_DIVALATT) OR Not Empty(.w_DIVALCAU) OR Not Empty(.w_DIVALCON) ,'N',.w_DIFLZERO)   
        .w_DESCRI = ICASE(.w_DIFLZERO='S','Valore di default',.w_DITIPDIM $ 'LB-CI-CF-AN-PE',.w_DESCRL,.w_DITIPDIM $ 'LK-AT',.w_DESCRK,.w_DESCRC)
    endwith
    this.DoRTCalc(31,32,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DICODICE = t_DICODICE
    this.w_DIARLINK = t_DIARLINK
    this.w_DIDESCRI = t_DIDESCRI
    this.w_DITIPDAT = this.oPgFrm.Page1.oPag.oDITIPDAT_2_5.RadioValue(.t.)
    this.w_DITIPDIM = this.oPgFrm.Page1.oPag.oDITIPDIM_2_6.RadioValue(.t.)
    this.w_DIVLCOMN = t_DIVLCOMN
    this.w_DIVALNUM = t_DIVALNUM
    this.w_DIVALDAT = t_DIVALDAT
    this.w_DIVALCAU = t_DIVALCAU
    this.w_DIVLCOMC = t_DIVLCOMC
    this.w_DIVALCHR = t_DIVALCHR
    this.w_DIVALLIN = t_DIVALLIN
    this.w_DIVALATT = t_DIVALATT
    this.w_DIVALCON = t_DIVALCON
    this.w_ADESCRI = t_ADESCRI
    this.w_NDESCRI = t_NDESCRI
    this.w_CDESCRI = t_CDESCRI
    this.w_KDESCRI = t_KDESCRI
    this.w_VDESCRI = t_VDESCRI
    this.w_DESCRP = t_DESCRP
    this.w_UDESCRI = t_UDESCRI
    this.w_DESCRC = t_DESCRC
    this.w_DESCRK = t_DESCRK
    this.w_DESCRL = t_DESCRL
    this.w_DIFLZERO = this.oPgFrm.Page1.oPag.oDIFLZERO_2_26.RadioValue(.t.)
    this.w_DESCRI = t_DESCRI
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DICODICE with this.w_DICODICE
    replace t_DIARLINK with this.w_DIARLINK
    replace t_DIDESCRI with this.w_DIDESCRI
    replace t_DITIPDAT with this.oPgFrm.Page1.oPag.oDITIPDAT_2_5.ToRadio()
    replace t_DITIPDIM with this.oPgFrm.Page1.oPag.oDITIPDIM_2_6.ToRadio()
    replace t_DIVLCOMN with this.w_DIVLCOMN
    replace t_DIVALNUM with this.w_DIVALNUM
    replace t_DIVALDAT with this.w_DIVALDAT
    replace t_DIVALCAU with this.w_DIVALCAU
    replace t_DIVLCOMC with this.w_DIVLCOMC
    replace t_DIVALCHR with this.w_DIVALCHR
    replace t_DIVALLIN with this.w_DIVALLIN
    replace t_DIVALATT with this.w_DIVALATT
    replace t_DIVALCON with this.w_DIVALCON
    replace t_ADESCRI with this.w_ADESCRI
    replace t_NDESCRI with this.w_NDESCRI
    replace t_CDESCRI with this.w_CDESCRI
    replace t_KDESCRI with this.w_KDESCRI
    replace t_VDESCRI with this.w_VDESCRI
    replace t_DESCRP with this.w_DESCRP
    replace t_UDESCRI with this.w_UDESCRI
    replace t_DESCRC with this.w_DESCRC
    replace t_DESCRK with this.w_DESCRK
    replace t_DESCRL with this.w_DESCRL
    replace t_DIFLZERO with this.oPgFrm.Page1.oPag.oDIFLZERO_2_26.ToRadio()
    replace t_DESCRI with this.w_DESCRI
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mddPag1 as StdContainer
  Width  = 720
  height = 245
  stdWidth  = 720
  stdheight = 245
  resizeXpos=299
  resizeYpos=118
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=2, width=709,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Riga",Field2="DICODICE",Label2="   Dimensione",Field3="DIDESCRI",Label3="                Descrizione",Field4="DESCRI",Label4="                Descrizione valore",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235734138

  add object oStr_1_4 as StdString with uid="NXGFRGZQJW",Visible=.t., Left=328, Top=218,;
    Alignment=1, Width=46, Height=18,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (Empty(.w_DICODICE) )
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=21,;
    width=705+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=22,width=704+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TAB_DIME|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDITIPDAT_2_5.Refresh()
      this.Parent.oDITIPDIM_2_6.Refresh()
      this.Parent.oDIVLCOMN_2_7.Refresh()
      this.Parent.oDIVALNUM_2_8.Refresh()
      this.Parent.oDIVALDAT_2_9.Refresh()
      this.Parent.oDIVALCAU_2_10.Refresh()
      this.Parent.oDIVLCOMC_2_11.Refresh()
      this.Parent.oDIVALCHR_2_12.Refresh()
      this.Parent.oDIVALLIN_2_13.Refresh()
      this.Parent.oDIVALATT_2_14.Refresh()
      this.Parent.oDIVALCON_2_15.Refresh()
      this.Parent.oDIFLZERO_2_26.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TAB_DIME'
        oDropInto=this.oBodyCol.oRow.oDICODICE_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDITIPDAT_2_5 as StdTrsCombo with uid="BPWOBGCWIP",rtrep=.t.,;
    cFormVar="w_DITIPDAT", RowSource=""+"Char,"+"Numerico,"+"Data" , enabled=.f.,;
    HelpContextID = 75786378,;
    Height=25, Width=125, Left=6, Top=217,;
    cTotal="", cQueryName = "DITIPDAT",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDITIPDAT_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DITIPDAT,&i_cF..t_DITIPDAT),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'N',;
    iif(xVal =3,'D',;
    space(1)))))
  endfunc
  func oDITIPDAT_2_5.GetRadio()
    this.Parent.oContained.w_DITIPDAT = this.RadioValue()
    return .t.
  endfunc

  func oDITIPDAT_2_5.ToRadio()
    this.Parent.oContained.w_DITIPDAT=trim(this.Parent.oContained.w_DITIPDAT)
    return(;
      iif(this.Parent.oContained.w_DITIPDAT=='C',1,;
      iif(this.Parent.oContained.w_DITIPDAT=='N',2,;
      iif(this.Parent.oContained.w_DITIPDAT=='D',3,;
      0))))
  endfunc

  func oDITIPDAT_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDITIPDIM_2_6 as StdTrsCombo with uid="HETIRIAGZE",rtrep=.t.,;
    cFormVar="w_DITIPDIM", RowSource=""+"Libera,"+"Link,"+"Combo\Check,"+"Data Comp. IVA iniz.,"+"Data Comp. IVA fin.,"+"Anno,"+"Periodo,"+"Attivit�" , enabled=.f.,;
    ToolTipText = "Tipologia della dimensione libera\link\combo",;
    HelpContextID = 192649085,;
    Height=25, Width=159, Left=171, Top=217,;
    cTotal="", cQueryName = "DITIPDIM",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDITIPDIM_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DITIPDIM,&i_cF..t_DITIPDIM),this.value)
    return(iif(xVal =1,'LB',;
    iif(xVal =2,'LK',;
    iif(xVal =3,'CC',;
    iif(xVal =4,'CI',;
    iif(xVal =5,'CF',;
    iif(xVal =6,'AN',;
    iif(xVal =7,'PE',;
    iif(xVal =8,'AT',;
    space(2))))))))))
  endfunc
  func oDITIPDIM_2_6.GetRadio()
    this.Parent.oContained.w_DITIPDIM = this.RadioValue()
    return .t.
  endfunc

  func oDITIPDIM_2_6.ToRadio()
    this.Parent.oContained.w_DITIPDIM=trim(this.Parent.oContained.w_DITIPDIM)
    return(;
      iif(this.Parent.oContained.w_DITIPDIM=='LB',1,;
      iif(this.Parent.oContained.w_DITIPDIM=='LK',2,;
      iif(this.Parent.oContained.w_DITIPDIM=='CC',3,;
      iif(this.Parent.oContained.w_DITIPDIM=='CI',4,;
      iif(this.Parent.oContained.w_DITIPDIM=='CF',5,;
      iif(this.Parent.oContained.w_DITIPDIM=='AN',6,;
      iif(this.Parent.oContained.w_DITIPDIM=='PE',7,;
      iif(this.Parent.oContained.w_DITIPDIM=='AT',8,;
      0)))))))))
  endfunc

  func oDITIPDIM_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDIVLCOMN_2_7 as StdTrsField with uid="MHYXPKVVNT",rtseq=9,rtrep=.t.,;
    cFormVar="w_DIVLCOMN",value=0,;
    HelpContextID = 21526396,;
    cTotal="", bFixedPos=.t., cQueryName = "DIVLCOMN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=223, Left=377, Top=217, bHasZoom = .t. , cLinkFile="DETCOMBO", oKey_1_1="DC_FONTE", oKey_1_2="this.w_DI_FONTE", oKey_2_1="DCCODDIS", oKey_2_2="this.w_DICODICE", oKey_3_1="DCVALNUM", oKey_3_2="this.w_DIVLCOMN"

  func oDIVLCOMN_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDIM<>'CC' OR .w_DITIPDAT<>'N')
    endwith
    endif
  endfunc

  func oDIVLCOMN_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVLCOMN_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDIVLCOMN_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DETCOMBO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DC_FONTE="+cp_ToStrODBC(this.Parent.oContained.w_DI_FONTE)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DCCODDIS="+cp_ToStrODBC(this.Parent.oContained.w_DICODICE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DC_FONTE="+cp_ToStr(this.Parent.oContained.w_DI_FONTE)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DCCODDIS="+cp_ToStr(this.Parent.oContained.w_DICODICE)
    endif
    do cp_zoom with 'DETCOMBO','*','DC_FONTE,DCCODDIS,DCVALNUM',cp_AbsName(this.parent,'oDIVLCOMN_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSCG_MDD.DETCOMBO_VZM',this.parent.oContained
  endproc

  add object oDIVALNUM_2_8 as StdTrsField with uid="PHITCZZERZ",rtseq=11,rtrep=.t.,;
    cFormVar="w_DIVALNUM",value=0,;
    HelpContextID = 238848131,;
    cTotal="", bFixedPos=.t., cQueryName = "DIVALNUM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=377, Top=217, cSayPict=["99999999999999.99999"], cGetPict=["99999999999999.99999"]

  func oDIVALNUM_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDAT<>'N' OR Not(.w_DITIPDIM $ 'LB-AN-PE'))
    endwith
    endif
  endfunc

  add object oDIVALDAT_2_9 as StdTrsField with uid="HDUXYMXCIO",rtseq=12,rtrep=.t.,;
    cFormVar="w_DIVALDAT",value=ctod("  /  /  "),;
    HelpContextID = 71075978,;
    cTotal="", bFixedPos=.t., cQueryName = "DIVALDAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=377, Top=217

  func oDIVALDAT_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDAT<>'D' OR Not(.w_DITIPDIM $ 'LB-CI-CF'))
    endwith
    endif
  endfunc

  add object oDIVALCAU_2_10 as StdTrsField with uid="FCHQJIVPWH",rtseq=13,rtrep=.t.,;
    cFormVar="w_DIVALCAU",value=space(5),;
    ToolTipText = "valore associato dimensioni di tipo Link",;
    HelpContextID = 54298763,;
    cTotal="", bFixedPos=.t., cQueryName = "DIVALCAU",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=223, Left=377, Top=217, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_DIVALCAU"

  func oDIVALCAU_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDIM<>'LK' or .w_DIARLINK<>'CAU_CONT')
    endwith
    endif
  endfunc

  func oDIVALCAU_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVALCAU_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDIVALCAU_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oDIVALCAU_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"CAUSALI CONTABILI",'',this.parent.oContained
  endproc
  proc oDIVALCAU_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_DIVALCAU
    i_obj.ecpSave()
  endproc

  add object oDIVLCOMC_2_11 as StdTrsField with uid="VOKDATJVYY",rtseq=14,rtrep=.t.,;
    cFormVar="w_DIVLCOMC",value=space(10),;
    HelpContextID = 21526407,;
    cTotal="", bFixedPos=.t., cQueryName = "DIVLCOMC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=223, Left=377, Top=217, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="DETCOMBO", oKey_1_1="DC_FONTE", oKey_1_2="this.w_DI_FONTE", oKey_2_1="DCCODDIS", oKey_2_2="this.w_DICODICE", oKey_3_1="DCVALCHR", oKey_3_2="this.w_DIVLCOMC"

  func oDIVLCOMC_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDIM<>'CC' OR .w_DITIPDAT<>'C')
    endwith
    endif
  endfunc

  func oDIVLCOMC_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVLCOMC_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDIVLCOMC_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DETCOMBO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DC_FONTE="+cp_ToStrODBC(this.Parent.oContained.w_DI_FONTE)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DCCODDIS="+cp_ToStrODBC(this.Parent.oContained.w_DICODICE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DC_FONTE="+cp_ToStr(this.Parent.oContained.w_DI_FONTE)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DCCODDIS="+cp_ToStr(this.Parent.oContained.w_DICODICE)
    endif
    do cp_zoom with 'DETCOMBO','*','DC_FONTE,DCCODDIS,DCVALCHR',cp_AbsName(this.parent,'oDIVLCOMC_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO OPZIONI",'',this.parent.oContained
  endproc

  add object oDIVALCHR_2_12 as StdTrsField with uid="XYTKPKFMCK",rtseq=15,rtrep=.t.,;
    cFormVar="w_DIVALCHR",value=space(50),;
    HelpContextID = 54298760,;
    cTotal="", bFixedPos=.t., cQueryName = "DIVALCHR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=377, Top=217, InputMask=replicate('X',50)

  func oDIVALCHR_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDAT<>'C' OR Not(.w_DITIPDIM $ 'LB-AN'))
    endwith
    endif
  endfunc

  add object oDIVALLIN_2_13 as StdTrsField with uid="VNIZTCUQBL",rtseq=16,rtrep=.t.,;
    cFormVar="w_DIVALLIN",value=space(5),;
    ToolTipText = "valore associato dimensioni di tipo Link",;
    HelpContextID = 63141756,;
    cTotal="", bFixedPos=.t., cQueryName = "DIVALLIN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=223, Left=377, Top=217, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_DIVALLIN"

  func oDIVALLIN_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDIM<>'LK' or .w_DIARLINK<>'VOCIIVA')
    endwith
    endif
  endfunc

  func oDIVALLIN_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVALLIN_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDIVALLIN_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oDIVALLIN_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"CODICI IVA",'',this.parent.oContained
  endproc
  proc oDIVALLIN_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_DIVALLIN
    i_obj.ecpSave()
  endproc

  add object oDIVALATT_2_14 as StdTrsField with uid="HGCHCJOIOZ",rtseq=17,rtrep=.t.,;
    cFormVar="w_DIVALATT",value=space(5),;
    ToolTipText = "valore associato dimensioni di tipo link attivita",;
    HelpContextID = 20744330,;
    cTotal="", bFixedPos=.t., cQueryName = "DIVALATT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=223, Left=377, Top=217, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ATTIMAST", oKey_1_1="ATCODATT", oKey_1_2="this.w_DIVALATT"

  func oDIVALATT_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDIM<>'AT')
    endwith
    endif
  endfunc

  func oDIVALATT_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVALATT_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDIVALATT_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIMAST','*','ATCODATT',cp_AbsName(this.parent,'oDIVALATT_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDIVALCON_2_15 as StdTrsField with uid="TLQHFPVBFL",rtseq=18,rtrep=.t.,;
    cFormVar="w_DIVALCON",value=space(15),;
    ToolTipText = "valore associato dimensioni di tipo link attivita",;
    HelpContextID = 214136700,;
    cTotal="", bFixedPos=.t., cQueryName = "DIVALCON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=10, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=223, Left=377, Top=217, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DIVALCON"

  func oDIVALCON_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDIM<>'LK' or .w_DIARLINK<>'CONTI')
    endwith
    endif
  endfunc

  func oDIVALCON_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVALCON_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDIVALCON_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDIVALCON_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"CONTI",'',this.parent.oContained
  endproc
  proc oDIVALCON_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DIVALCON
    i_obj.ecpSave()
  endproc

  add object oDIFLZERO_2_26 as StdTrsCheck with uid="FANQAQQPJW",rtrep=.t.,;
    cFormVar="w_DIFLZERO",  caption="Filtra default",;
    ToolTipText = "Se attivo filtra per il valore di Defualt",;
    HelpContextID = 103188613,;
    Left=607, Top=217,;
    cTotal="", cQueryName = "DIFLZERO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDIFLZERO_2_26.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DIFLZERO,&i_cF..t_DIFLZERO),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDIFLZERO_2_26.GetRadio()
    this.Parent.oContained.w_DIFLZERO = this.RadioValue()
    return .t.
  endfunc

  func oDIFLZERO_2_26.ToRadio()
    this.Parent.oContained.w_DIFLZERO=trim(this.Parent.oContained.w_DIFLZERO)
    return(;
      iif(this.Parent.oContained.w_DIFLZERO=='S',1,;
      0))
  endfunc

  func oDIFLZERO_2_26.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDIFLZERO_2_26.mCond()
    with this.Parent.oContained
      return (Empty(.w_DIVALCHR) AND .w_DIVALNUM=0 AND Empty(.w_DIVALDAT) AND Empty(.w_DIVALLIN) AND Empty(.w_DIVALATT) AND Empty(.w_DIVALCAU) AND Empty(.w_DIVALCON)   )
    endwith
  endfunc

  func oDIFLZERO_2_26.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDIM='CC' OR .w_TIPO='T' OR EMPTY(.w_DICODICE))
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mddBodyRow as CPBodyRowCnt
  Width=695
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="FLGZPZZVJT",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 268062570,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0

  add object oDICODICE_2_2 as StdTrsField with uid="NQRWWVRCKQ",rtseq=4,rtrep=.t.,;
    cFormVar="w_DICODICE",value=space(15),isprimarykey=.t.,;
    HelpContextID = 147413115,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=44, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TAB_DIME", cZoomOnZoom="GSCG_ADM", oKey_1_1="DI_FONTE", oKey_1_2="this.w_DI_FONTE", oKey_2_1="DICODICE", oKey_2_2="this.w_DICODICE"

  func oDICODICE_2_2.mCond()
    with this.Parent.oContained
      return (Empty(.w_DICODICE))
    endwith
  endfunc

  func oDICODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
      if .not. empty(.w_DIVLCOMN)
        bRes2=.link_2_7('Full')
      endif
      if .not. empty(.w_DIVLCOMC)
        bRes2=.link_2_11('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDICODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDICODICE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oDICODICE_2_2.readonly and this.parent.oDICODICE_2_2.isprimarykey)
    if i_TableProp[this.parent.oContained.TAB_DIME_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DI_FONTE="+cp_ToStrODBC(this.Parent.oContained.w_DI_FONTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DI_FONTE="+cp_ToStr(this.Parent.oContained.w_DI_FONTE)
    endif
    do cp_zoom with 'TAB_DIME','*','DI_FONTE,DICODICE',cp_AbsName(this.parent,'oDICODICE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ADM',"ELENCO DIMENSIONI",'',this.parent.oContained
   endif
  endproc
  proc oDICODICE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ADM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DI_FONTE=w_DI_FONTE
     i_obj.w_DICODICE=this.parent.oContained.w_DICODICE
    i_obj.ecpSave()
  endproc

  add object oDIDESCRI_2_4 as StdTrsField with uid="QLIIFMZQGL",rtseq=6,rtrep=.t.,;
    cFormVar="w_DIDESCRI",value=space(80),enabled=.f.,;
    HelpContextID = 61827199,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=203, Left=165, Top=0, InputMask=replicate('X',80)

  add object oDESCRI_2_27 as StdTrsField with uid="VJCETGUKNE",rtseq=30,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(80),enabled=.f.,;
    HelpContextID = 107064266,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=319, Left=371, Top=0, InputMask=replicate('X',80)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mdd','DET_DIME','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DICODTOT=DET_DIME.DICODTOT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
