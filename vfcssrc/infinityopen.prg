* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: infinityopen                                                    *
*              Gestione file Infinity D.M.S.                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-07                                                      *
* Last revis.: 2017-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPERAT,w_NUMBATTR,pCLASSEDOCU
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tinfinityopen",oParentObject,m.w_OPERAT,m.w_NUMBATTR,m.pCLASSEDOCU)
return(i_retval)

define class tinfinityopen as StdBatch
  * --- Local variables
  w_OPERAT = space(1)
  w_NUMBATTR = 0
  pCLASSEDOCU = space(20)
  w_CDINFURL = space(254)
  w_UTIDPSSW = space(254)
  w_UTIDUSER = space(254)
  w_CDAPPCOD = space(5)
  w_EXTCODE = space(0)
  w_IDWEBFIL = space(254)
  w_IDWEBRET = space(1)
  w_IdxCONTA = 0
  w_OPERATION = space(10)
  w_cLink = space(255)
  w_SHELLEXEC = 0
  w_OLDSETCENTURY = space(3)
  w_OLDSETMARK = space(3)
  w_VALATTRIB = space(10)
  * --- WorkFile variables
  PROMCLAS_idx=0
  PARA_EDS_idx=0
  CONTROPA_idx=0
  PROMINDI_idx=0
  UTE_NTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Funzione per estrarre il file archiviato in e.d.s nella cartella temporanea
    this.w_NUMBATTR = this.w_NUMBATTR - 1
    * --- variabili Infinity D.M.S.
    do case
      case g_REVI="S"
        * --- NOTA BENE: SE g_REVI="S" anche g_DMIP="S"
        this.w_cLink = ""
        if NOT EMPTY(NVL(this.pCLASSEDOCU,""))
          * --- Applico la trascodifica della classe
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CDCLAINF"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDCODCLA = "+cp_ToStrODBC(this.pCLASSEDOCU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CDCLAINF;
              from (i_cTable) where;
                  CDCODCLA = this.pCLASSEDOCU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.pCLASSEDOCU = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if EMPTY(NVL(this.pCLASSEDOCU,""))
            AH_ERRORMSG("Errore: per la classe documentale non � inserita nesssuna trascodifica classe Infinity",48,"")
            i_retcode = 'stop'
            return
          endif
        endif
      case g_DMIP="S"
        * --- Read from PARA_EDS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PARA_EDS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PARA_EDS_idx,2],.t.,this.PARA_EDS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDINFURL,CDAPPCOD"+;
            " from "+i_cTable+" PARA_EDS where ";
                +"CDCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDINFURL,CDAPPCOD;
            from (i_cTable) where;
                CDCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CDINFURL = NVL(cp_ToDate(_read_.CDINFURL),cp_NullValue(_read_.CDINFURL))
          this.w_CDAPPCOD = NVL(cp_ToDate(_read_.CDAPPCOD),cp_NullValue(_read_.CDAPPCOD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from UTE_NTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UTE_NTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2],.t.,this.UTE_NTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UTIDPSSW,UTIDUSER"+;
            " from "+i_cTable+" UTE_NTI where ";
                +"UTCODICE = "+cp_ToStrODBC(i_CODUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UTIDPSSW,UTIDUSER;
            from (i_cTable) where;
                UTCODICE = i_CODUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UTIDPSSW = NVL(cp_ToDate(_read_.UTIDPSSW),cp_NullValue(_read_.UTIDPSSW))
          this.w_UTIDUSER = NVL(cp_ToDate(_read_.UTIDUSER),cp_NullValue(_read_.UTIDUSER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_UTIDPSSW = urladdr(CifraCnf( ALLTRIM(this.w_UTIDPSSW) , "D" ), "E")
        this.w_UTIDUSER = urladdr(alltrim(this.w_UTIDUSER), "E")
        this.w_CDAPPCOD = urladdr(alltrim(this.w_CDAPPCOD), "E")
        this.w_cLink = ALLTRIM(this.w_CDINFURL) + iif(RIGHT(ALLTRIM(this.w_CDINFURL),1)="/","","/")+"servlet/gsda_bentrypoint_dmseasy?p_User="+ALLTRIM(this.w_UTIDUSER)+"&p_Pwd="+ALLTRIM(this.w_UTIDPSSW)+"&p_AppReg="+ALLTRIM(this.w_CDAPPCOD)
      otherwise
        AH_ERRORMSG("Impossibile utilizzare funzionalit� avanzata Infinity D.M.S.",48,"")
        i_retcode = 'stop'
        return
    endcase
    if NOT EMPTY(NVL(this.pCLASSEDOCU,""))
      this.w_cLink = this.w_cLink + "&CLASSEDOC="+ALLTRIM(this.pCLASSEDOCU)
    endif
    if this.w_NUMBATTR>0
      * --- passaggio attributi
      * --- &ATTRIBID=<codice attributo1>,<codice attributo2>&ATVALUE=<valore attributo1>,<valore attributo2>
      this.w_cLink = this.w_cLink + "&ATATTRIBID="
      FOR this.w_IdxCONTA=1 TO this.w_NUMBATTR
      if NOT EMPTY(L_ARRAYATTRIBUTI(this.w_IdxCONTA,3))
        this.w_cLink = this.w_cLink + urladdr(ALLTRIM(L_ARRAYATTRIBUTI(this.w_IdxCONTA,1)), "E") + ","
      endif
      ENDFOR
      this.w_cLink = Left(this.w_cLink, Len(this.w_cLink)-1) + "&ATVALUE="
      this.w_OLDSETCENTURY = SET("CENTURY")
      this.w_OLDSETMARK = SET("MARK")
      if this.w_OLDSETCENTURY <> "ON"
        SET CENTURY ON
      endif
      if this.w_OLDSETMARK<>"-"
        SET MARK TO "-"
      endif
      FOR this.w_IdxCONTA=1 TO this.w_NUMBATTR
      this.w_VALATTRIB = L_ARRAYATTRIBUTI[this.w_IdxCONTA,3]
      if NOT EMPTY(this.w_VALATTRIB)
        if vartype(this.w_VALATTRIB)<>"C"
          this.w_VALATTRIB = Transform(this.w_VALATTRIB)
        endif
        this.w_cLink = this.w_cLink + urladdr(ALLTRIM(this.w_VALATTRIB), "B") + ","
      endif
      ENDFOR
      this.w_cLink = Left(this.w_cLink, Len(this.w_cLink)-1)
      if this.w_OLDSETCENTURY <> "ON"
        SET CENTURY OFF
      endif
      if this.w_OLDSETMARK<>"-"
        SET MARK TO this.w_OLDSETMARK
      endif
    endif
    do case
      case this.w_OPERAT="D"
        this.w_OPERATION = "nuovo_documento"
      case this.w_OPERAT="C"
        this.w_OPERATION = "acquisizione_file"
      case this.w_OPERAT="S" OR this.w_OPERAT="U"
        this.w_OPERATION = "acquisizione_scanner"
      otherwise
        this.w_OPERATION = "ricerca"
    endcase
    if g_REVI="S"
      this.w_cLink = this.w_OPERATION + this.w_cLink
      =infinitylauncher(this.w_cLink)
    else
      this.w_cLink = this.w_cLink + "&p_Operation=" + this.w_OPERATION
      * --- Apertura url
      this.w_SHELLEXEC = ah_ShellEx("open", alltrim(this.w_cLink), "")
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_OPERAT,w_NUMBATTR,pCLASSEDOCU)
    this.w_OPERAT=w_OPERAT
    this.w_NUMBATTR=w_NUMBATTR
    this.pCLASSEDOCU=pCLASSEDOCU
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='PARA_EDS'
    this.cWorkTables[3]='CONTROPA'
    this.cWorkTables[4]='PROMINDI'
    this.cWorkTables[5]='UTE_NTI'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPERAT,w_NUMBATTR,pCLASSEDOCU"
endproc
