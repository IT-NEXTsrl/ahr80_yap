* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_brb                                                        *
*              Attiva caricamento righe bilancio IVA                           *
*                                                                              *
*      Author:                                                                 *
*      Client: Zucchetti Spa                                                   *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-02                                                      *
* Last revis.: 2009-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Par
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_brb",oParentObject,m.Par)
return(i_retval)

define class tgscg_brb as StdBatch
  * --- Local variables
  Par = 0
  w_MRNUMBIL = space(6)
  w_MRCODESE = space(4)
  w_ARROPSG = space(1)
  w_PADRE = .NULL.
  w_LCODTOT = space(10)
  w_CCURS = space(100)
  w_NUMREC = 0
  w_GEST_MANU = .NULL.
  w_NUMREC = 0
  w_NUMERO = 0
  w_ESERCI = space(4)
  w_DATABIL = ctod("  /  /  ")
  w_DESCRIBI = space(35)
  w_MONETE = space(1)
  w_DIVISA = space(3)
  w_SIMVAL = space(5)
  w_EXTRAEUR = 0
  w_DECIMI = 0
  w_TIPOSTAM = space(1)
  w_RIGHESI = space(1)
  w_VALAPP = space(3)
  w_DESCRI = space(40)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_PERIODO = space(15)
  w_ANNO = 0
  w_ARROT = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Par
    *     1: carica righe
    *     2. esegue stampa
    this.w_PADRE = this.oParentObject
    do case
      case this.Par = 1 AND this.oParentObject.w_SALVA
        this.w_CCURS = Sys(2015)
        this.w_GEST_MANU = This.oparentobject.GSCG_MDM
        this.w_GEST_MANU.Markpos()     
        this.w_GEST_MANU.Exec_Select(this.w_CCURS , "t_MRCODTOT,t_MRCODMIS,Count( * ) As TOTMIS" , "not deleted()","t_MRCODTOT,t_MRCODMIS","t_MRCODTOT,t_MRCODMIS","TOTMIS>1")     
        Select ( this.w_CCURS )
        this.w_NUMREC = Nvl( TOTMIS, 0 )
        if this.w_NUMREC >1
          Ah_ErrorMsg ("Attenzione esistono misure ripetute",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_GEST_MANU.Repos()     
        Select ( this.w_CCURS ) 
 Use
        this.w_PADRE.GSCG_MDB.NotifyEvent("Salva")     
      case this.Par = 2
        * --- Avvio Stampa
        this.w_NUMERO = this.oParentObject.w_GBNUMBIL
        this.w_ESERCI = this.oParentObject.w_GBBILESE
        this.w_DATABIL = this.oParentObject.w_GBDATBIL
        this.w_DESCRIBI = this.oParentObject.w_GBDESCRI
        this.w_MONETE = "c"
        this.w_DIVISA = this.oParentObject.w_VALNAZ
        this.w_SIMVAL = this.oParentObject.w_SIMVAL
        this.w_EXTRAEUR = this.oParentObject.w_EXTRAEUR
        this.w_DECIMI = this.oParentObject.w_DECIMI
        this.w_TIPOSTAM = this.oParentObject.w_GBTIPSTA
        this.w_RIGHESI = this.oParentObject.w_GBRIGHES
        this.w_VALAPP = this.oParentObject.w_VALNAZ
        this.w_DESCRI = this.oParentObject.w_DESCRI
        this.w_DATINI = this.oParentObject.w_GBDATINI
        this.w_DATFIN = this.oParentObject.w_GBDATFIN
        this.w_PERIODO = this.oParentObject.w_GBPERIOD
        this.w_ANNO = this.oParentObject.w_GB__ANNO
        this.w_ARROT = this.oParentObject.w_GBARROTO
        * --- Questo batch viene lanciata dalla stampa Bilancio Contabile (GSBI_SBI)
        * --- In questo batch vengono effettuate tutte le operazioni di cambio.
        * --- Eseguo la query dove leggo i dati del bilancio che ho deciso di stampare.
        VQ_EXEC("QUERY\GSCG_SBI",this,"BILANCIO")
        * --- Variabili da passare al Report, Cambio, Divisa, Formato ,Descrizioni
        * --- il cambio viene calcolato in modo diverso a seconda se i_DATSYS e minore di g_DATEUR
        * --- se maggiore ho l'euro e due cambi (in caso di valuta intra euroepa)
        * --- q cambio che divide, L_molti cambio che moltiplica (1 se prima di g_dateur)
        if this.w_VALAPP=this.w_DIVISA
          L_MOLTI=1
          L_CAMVAL=1
        else
          L_MOLTI=iif(w_CAMVAL=0,1,w_CAMVAL)
          L_CAMVAL=GETCAM(this.w_VALAPP,i_DATSYS,0)
        endif
        * --- Formato importi
        L_DECIMI=this.w_DECIMI
        s=20*(l_decimi+2)
        L_NUMERO=this.w_NUMERO
        L_ESERCI=this.w_ESERCI
        L_DATABIL=this.w_DATABIL
        L_DESCRIBI=this.w_DESCRIBI
        L_PERIODO=this.w_PERIODO
        L_DESCRI=this.w_DESCRI
        L_DATINI=this.w_DATINI
        L_DATFIN=this.w_DATFIN
        L_ANNO=this.w_ANNO
        if this.w_RIGHESI="S"
          SELECT * FROM BILANCIO WHERE MRIMPORT<>0 OR VRFLDESC="D";
           INTO CURSOR __TMP__ ORDER BY CPROWORD
        else
          SELECT * FROM BILANCIO INTO CURSOR __TMP__ ORDER BY CPROWORD
        endif
        * --- Controllo il tipo di stampa selezionata
        L_SIMVAL= this.w_SIMVAL
         
 L_MONETE= this.w_MONETE 
 L_RIGHESI=this.w_RIGHESI 
 L_ARROT=this.w_ARROT
        do case
          case this.w_TIPOSTAM="S"
            * --- Stampa Standard
            * --- Lancio il report assocciato
            CP_CHPRN("QUERY\GSCG1SBI.FRX","",this.oParentObject)
          case this.w_TIPOSTAM="D"
            * --- Stampa Dettagliata
            CP_CHPRN("QUERY\GSCG2SBI.FRX","",this.oParentObject)
          otherwise
            * --- Su EXCEL - Creo il cursore da quello di stampa
            select CPROWORD AS CPROWORD ,IIF(MR__INDE>0,"_"+SPACE(MR__INDE-1),"")+MRDESCRI as MRDESCRI,;
            cp_round((MRIMPORT/l_camval)*l_molti,this.w_decimi) AS MRIMPORT,;
            cp_round((MRIMPONI/l_camval)*l_molti,this.w_decimi) AS MRIMPONI,;
            cp_round((MRIMPOST/l_camval)*l_molti,this.w_decimi) AS MRIMPOST,;
            cp_round(((MRIMPONI+MRIMPOST)/l_camval)*l_molti,this.w_decimi) AS TOTALE,;
            MRPERCEN AS MRPERCEN,MR__FTST AS MR__FTST, MR__FONT AS MR__FONT,;
            MR__COLO AS MR__COLO, MR__NUCO AS MR__NUCO FROM __TMP__ INTO CURSOR __TMP__ ORDER BY 1
            * --- Lancio la stampa su Excell
            if Islron()
              CP_CHPRN("QUERY\GSCGRSBI.XLT","",this.oParentObject)
            else
              CP_CHPRN("QUERY\GSCG_SBI.XLT","",this.oParentObject)
            endif
        endcase
        if USED("BILANCIO")
          SELECT BILANCIO
          USE
        endif
    endcase
  endproc


  proc Init(oParentObject,Par)
    this.Par=Par
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Par"
endproc
