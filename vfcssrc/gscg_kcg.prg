* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kcg                                                        *
*              Dettagli Garante\Terzo Datore F24                               *
*                                                                              *
*      Author: Zucchetti Tam Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-10                                                      *
* Last revis.: 2018-03-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kcg",oParentObject))

* --- Class definition
define class tgscg_kcg as StdForm
  Top    = 41
  Left   = 55

  * --- Standard Properties
  Width  = 698
  Height = 234
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-12"
  HelpContextID=99231081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  MOD_PAG_IDX = 0
  MODVPAG_IDX = 0
  cPrg = "gscg_kcg"
  cComment = "Dettagli Garante\Terzo Datore F24"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CFSECCOD = space(16)
  w_CFSESFIR = space(1)
  w_CFCOGFIR = space(24)
  w_CFNOMFIR = space(20)
  w_CFDATFIR = ctod('  /  /  ')
  w_CFCOMFIR = space(40)
  w_CFPROFIR = space(2)
  w_CFINDFIR = space(35)
  o_CFINDFIR = space(35)
  w_CFPRDFIR = space(2)
  w_CFCAPFIR = space(5)
  w_CFDOMFIR = space(40)
  w_CFCOGFIR = space(60)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kcgPag1","gscg_kcg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCFSECCOD_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='MOD_PAG'
    this.cWorkTables[2]='MODVPAG'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CFSECCOD=space(16)
      .w_CFSESFIR=space(1)
      .w_CFCOGFIR=space(24)
      .w_CFNOMFIR=space(20)
      .w_CFDATFIR=ctod("  /  /  ")
      .w_CFCOMFIR=space(40)
      .w_CFPROFIR=space(2)
      .w_CFINDFIR=space(35)
      .w_CFPRDFIR=space(2)
      .w_CFCAPFIR=space(5)
      .w_CFDOMFIR=space(40)
      .w_CFCOGFIR=space(60)
      .w_CFSECCOD=oParentObject.w_CFSECCOD
      .w_CFSESFIR=oParentObject.w_CFSESFIR
      .w_CFCOGFIR=oParentObject.w_CFCOGFIR
      .w_CFNOMFIR=oParentObject.w_CFNOMFIR
      .w_CFDATFIR=oParentObject.w_CFDATFIR
      .w_CFCOMFIR=oParentObject.w_CFCOMFIR
      .w_CFPROFIR=oParentObject.w_CFPROFIR
      .w_CFINDFIR=oParentObject.w_CFINDFIR
      .w_CFPRDFIR=oParentObject.w_CFPRDFIR
      .w_CFCAPFIR=oParentObject.w_CFCAPFIR
      .w_CFDOMFIR=oParentObject.w_CFDOMFIR
      .w_CFCOGFIR=oParentObject.w_CFCOGFIR
          .DoRTCalc(1,1,.f.)
        .w_CFSESFIR = 'M'
    endwith
    this.DoRTCalc(3,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCFSECCOD_1_1.enabled = i_bVal
      .Page1.oPag.oCFSESFIR_1_2.enabled = i_bVal
      .Page1.oPag.oCFCOGFIR_1_3.enabled = i_bVal
      .Page1.oPag.oCFNOMFIR_1_4.enabled = i_bVal
      .Page1.oPag.oCFDATFIR_1_5.enabled = i_bVal
      .Page1.oPag.oCFCOMFIR_1_6.enabled = i_bVal
      .Page1.oPag.oCFPROFIR_1_7.enabled = i_bVal
      .Page1.oPag.oCFINDFIR_1_8.enabled = i_bVal
      .Page1.oPag.oCFPRDFIR_1_10.enabled = i_bVal
      .Page1.oPag.oCFCAPFIR_1_12.enabled = i_bVal
      .Page1.oPag.oCFDOMFIR_1_15.enabled = i_bVal
      .Page1.oPag.oCFCOGFIR_1_28.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CFSECCOD=.w_CFSECCOD
      .oParentObject.w_CFSESFIR=.w_CFSESFIR
      .oParentObject.w_CFCOGFIR=.w_CFCOGFIR
      .oParentObject.w_CFNOMFIR=.w_CFNOMFIR
      .oParentObject.w_CFDATFIR=.w_CFDATFIR
      .oParentObject.w_CFCOMFIR=.w_CFCOMFIR
      .oParentObject.w_CFPROFIR=.w_CFPROFIR
      .oParentObject.w_CFINDFIR=.w_CFINDFIR
      .oParentObject.w_CFPRDFIR=.w_CFPRDFIR
      .oParentObject.w_CFCAPFIR=.w_CFCAPFIR
      .oParentObject.w_CFDOMFIR=.w_CFDOMFIR
      .oParentObject.w_CFCOGFIR=.w_CFCOGFIR
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
        if .o_CFINDFIR<>.w_CFINDFIR
            .w_CFPRDFIR = IIF(EMPTY(.w_CFINDFIR),' ',.w_CFPRDFIR)
        endif
        if .o_CFINDFIR<>.w_CFINDFIR
            .w_CFCAPFIR = IIF(EMPTY(.w_CFINDFIR),' ',.w_CFCAPFIR)
        endif
        if .o_CFINDFIR<>.w_CFINDFIR
            .w_CFDOMFIR = IIF(EMPTY(.w_CFINDFIR),' ',.w_CFDOMFIR)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCFDATFIR_1_5.enabled = this.oPgFrm.Page1.oPag.oCFDATFIR_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCFCOMFIR_1_6.enabled = this.oPgFrm.Page1.oPag.oCFCOMFIR_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCFPROFIR_1_7.enabled = this.oPgFrm.Page1.oPag.oCFPROFIR_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCFSESFIR_1_2.visible=!this.oPgFrm.Page1.oPag.oCFSESFIR_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCFCOGFIR_1_3.visible=!this.oPgFrm.Page1.oPag.oCFCOGFIR_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCFNOMFIR_1_4.visible=!this.oPgFrm.Page1.oPag.oCFNOMFIR_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oCFCOGFIR_1_28.visible=!this.oPgFrm.Page1.oPag.oCFCOGFIR_1_28.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCFSECCOD_1_1.value==this.w_CFSECCOD)
      this.oPgFrm.Page1.oPag.oCFSECCOD_1_1.value=this.w_CFSECCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oCFSESFIR_1_2.RadioValue()==this.w_CFSESFIR)
      this.oPgFrm.Page1.oPag.oCFSESFIR_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCOGFIR_1_3.value==this.w_CFCOGFIR)
      this.oPgFrm.Page1.oPag.oCFCOGFIR_1_3.value=this.w_CFCOGFIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCFNOMFIR_1_4.value==this.w_CFNOMFIR)
      this.oPgFrm.Page1.oPag.oCFNOMFIR_1_4.value=this.w_CFNOMFIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDATFIR_1_5.value==this.w_CFDATFIR)
      this.oPgFrm.Page1.oPag.oCFDATFIR_1_5.value=this.w_CFDATFIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCOMFIR_1_6.value==this.w_CFCOMFIR)
      this.oPgFrm.Page1.oPag.oCFCOMFIR_1_6.value=this.w_CFCOMFIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPROFIR_1_7.value==this.w_CFPROFIR)
      this.oPgFrm.Page1.oPag.oCFPROFIR_1_7.value=this.w_CFPROFIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCFINDFIR_1_8.value==this.w_CFINDFIR)
      this.oPgFrm.Page1.oPag.oCFINDFIR_1_8.value=this.w_CFINDFIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPRDFIR_1_10.value==this.w_CFPRDFIR)
      this.oPgFrm.Page1.oPag.oCFPRDFIR_1_10.value=this.w_CFPRDFIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCAPFIR_1_12.value==this.w_CFCAPFIR)
      this.oPgFrm.Page1.oPag.oCFCAPFIR_1_12.value=this.w_CFCAPFIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDOMFIR_1_15.value==this.w_CFDOMFIR)
      this.oPgFrm.Page1.oPag.oCFDOMFIR_1_15.value=this.w_CFDOMFIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCOGFIR_1_28.value==this.w_CFCOGFIR)
      this.oPgFrm.Page1.oPag.oCFCOGFIR_1_28.value=this.w_CFCOGFIR
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CFSECCOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFSECCOD_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CFSECCOD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CFCOGFIR))  and not(isdigit(left (.w_CFSECCOD,1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFCOGFIR_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CFCOGFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CFNOMFIR))  and not(isdigit(left (.w_CFSECCOD,1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFNOMFIR_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CFNOMFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CFDATFIR))  and (NOT isdigit(left (.w_CFSECCOD,1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFDATFIR_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CFDATFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CFCOMFIR))  and (NOT isdigit(left (.w_CFSECCOD,1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFCOMFIR_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CFCOMFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CFPROFIR))  and (NOT isdigit(left (.w_CFSECCOD,1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFPROFIR_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CFPROFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CFCOGFIR))  and not(NOT isdigit(left (.w_CFSECCOD,1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFCOGFIR_1_28.SetFocus()
            i_bnoObbl = !empty(.w_CFCOGFIR)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CFINDFIR = this.w_CFINDFIR
    return

enddefine

* --- Define pages as container
define class tgscg_kcgPag1 as StdContainer
  Width  = 694
  height = 234
  stdWidth  = 694
  stdheight = 234
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCFSECCOD_1_1 as StdField with uid="QIUOMONGUH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CFSECCOD", cQueryName = "CFSECCOD",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del Garante\Terzo datore",;
    HelpContextID = 26236266,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=106, Top=14, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)


  add object oCFSESFIR_1_2 as StdCombo with uid="ZWQWTSSEFV",rtseq=2,rtrep=.f.,left=359,top=13,width=153,height=21;
    , ToolTipText = "Sesso del Garante\Terzo datore";
    , HelpContextID = 175090312;
    , cFormVar="w_CFSESFIR",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCFSESFIR_1_2.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oCFSESFIR_1_2.GetRadio()
    this.Parent.oContained.w_CFSESFIR = this.RadioValue()
    return .t.
  endfunc

  func oCFSESFIR_1_2.SetRadio()
    this.Parent.oContained.w_CFSESFIR=trim(this.Parent.oContained.w_CFSESFIR)
    this.value = ;
      iif(this.Parent.oContained.w_CFSESFIR=='M',1,;
      iif(this.Parent.oContained.w_CFSESFIR=='F',2,;
      0))
  endfunc

  func oCFSESFIR_1_2.mHide()
    with this.Parent.oContained
      return (isdigit(left (.w_CFSECCOD,1)))
    endwith
  endfunc

  add object oCFCOGFIR_1_3 as StdField with uid="UJEONPCNMD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CFCOGFIR", cQueryName = "CFCOGFIR",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del Garante\Terzo datore",;
    HelpContextID = 187083400,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=106, Top=40, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oCFCOGFIR_1_3.mHide()
    with this.Parent.oContained
      return (isdigit(left (.w_CFSECCOD,1)))
    endwith
  endfunc

  add object oCFNOMFIR_1_4 as StdField with uid="QIHZISTHUY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CFNOMFIR", cQueryName = "CFNOMFIR",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del Garante\Terzo datore",;
    HelpContextID = 180746888,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=359, Top=41, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  func oCFNOMFIR_1_4.mHide()
    with this.Parent.oContained
      return (isdigit(left (.w_CFSECCOD,1)))
    endwith
  endfunc

  add object oCFDATFIR_1_5 as StdField with uid="UPSANOFKLO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CFDATFIR", cQueryName = "CFDATFIR",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data nascita del Garante\Terzo datore",;
    HelpContextID = 174365320,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=106, Top=107

  func oCFDATFIR_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT isdigit(left (.w_CFSECCOD,1)))
    endwith
   endif
  endfunc

  add object oCFCOMFIR_1_6 as StdField with uid="AMOBIHLKQS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CFCOMFIR", cQueryName = "CFCOMFIR",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune o stato estero di nascita del Garante\Terzo datore",;
    HelpContextID = 180791944,;
   bGlobalFont=.t.,;
    Height=21, Width=235, Left=312, Top=107, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oCFCOMFIR_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT isdigit(left (.w_CFSECCOD,1)))
    endwith
   endif
  endfunc

  add object oCFPROFIR_1_7 as StdField with uid="DWTJZMGPWF",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CFPROFIR", cQueryName = "CFPROFIR",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita del Garante\Terzo datore",;
    HelpContextID = 178444936,;
   bGlobalFont=.t.,;
    Height=21, Width=39, Left=633, Top=107, cSayPict='repl("!",2)', cGetPict='repl("!",2)', InputMask=replicate('X',2), bHasZoom = .t. 

  func oCFPROFIR_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT isdigit(left (.w_CFSECCOD,1)))
    endwith
   endif
  endfunc

  proc oCFPROFIR_1_7.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_CFPROFIR")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCFINDFIR_1_8 as StdField with uid="JDSTQIJLKB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CFINDFIR", cQueryName = "CFINDFIR",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo del domicilio fiscale del Garante\Terzo datore",;
    HelpContextID = 190270088,;
   bGlobalFont=.t.,;
    Height=21, Width=270, Left=106, Top=172, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  add object oCFPRDFIR_1_10 as StdField with uid="OBZIQKNCRE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CFPRDFIR", cQueryName = "CFPRDFIR",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia del domicilio fiscale del Garante\Terzo datore",;
    HelpContextID = 189979272,;
   bGlobalFont=.t.,;
    Height=21, Width=39, Left=465, Top=172, cSayPict='repl("!",2)', cGetPict='repl("!",2)', InputMask=replicate('X',2), bHasZoom = .t. 

  proc oCFPRDFIR_1_10.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_CFPRDFIR")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCFCAPFIR_1_12 as StdField with uid="GCAXLYGPDK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CFCAPFIR", cQueryName = "CFCAPFIR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "CAP del domicilio fiscale del Garante\Terzo datore",;
    HelpContextID = 178563720,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=573, Top=172, cSayPict='repl("!",5)', cGetPict='repl("!",5)', InputMask=replicate('X',5)

  add object oCFDOMFIR_1_15 as StdField with uid="TRMFZLKUBO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CFDOMFIR", cQueryName = "CFDOMFIR",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune del domicilio fiscale del Garante\Terzo datore",;
    HelpContextID = 180787848,;
   bGlobalFont=.t.,;
    Height=21, Width=270, Left=106, Top=197, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  add object oCFCOGFIR_1_28 as StdField with uid="BMGGTHZAOK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CFCOGFIR", cQueryName = "CFCOGFIR",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del Garante\Terzo datore",;
    HelpContextID = 187083400,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=106, Top=40, cSayPict='repl("!",60)', cGetPict='repl("!",60)', InputMask=replicate('X',60)

  func oCFCOGFIR_1_28.mHide()
    with this.Parent.oContained
      return (NOT isdigit(left (.w_CFSECCOD,1)))
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="HLTFUEIMSS",Visible=.t., Left=48, Top=172,;
    Alignment=1, Width=55, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="OUFXKVQDCD",Visible=.t., Left=19, Top=16,;
    Alignment=1, Width=84, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="WDHWDQAXSI",Visible=.t., Left=25, Top=40,;
    Alignment=1, Width=78, Height=18,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (isdigit(left (.w_CFSECCOD,1)))
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="REXTSCILXO",Visible=.t., Left=295, Top=41,;
    Alignment=1, Width=57, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (isdigit(left (.w_CFSECCOD,1)))
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="XUHJXOYZOH",Visible=.t., Left=254, Top=14,;
    Alignment=1, Width=98, Height=18,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (isdigit(left (.w_CFSECCOD,1)))
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="QAEPQCNYCW",Visible=.t., Left=16, Top=107,;
    Alignment=1, Width=87, Height=18,;
    Caption="Data di nascita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="CNXPBYZDFR",Visible=.t., Left=193, Top=107,;
    Alignment=1, Width=115, Height=18,;
    Caption="Comune di nascita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="WJIAKBWWDV",Visible=.t., Left=402, Top=172,;
    Alignment=1, Width=57, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="NJXCAYPDMX",Visible=.t., Left=39, Top=197,;
    Alignment=1, Width=64, Height=18,;
    Caption="Comune:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="PNQVTCNWWJ",Visible=.t., Left=566, Top=107,;
    Alignment=1, Width=63, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="KSQZRXPMIB",Visible=.t., Left=522, Top=172,;
    Alignment=1, Width=48, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="FARPKBIGXI",Visible=.t., Left=15, Top=75,;
    Alignment=0, Width=180, Height=18,;
    Caption="Dati di nascita"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="WVJGNCEIXV",Visible=.t., Left=15, Top=143,;
    Alignment=0, Width=180, Height=18,;
    Caption="Domicilio Fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="OYTQRWQBVU",Visible=.t., Left=13, Top=40,;
    Alignment=1, Width=90, Height=18,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (NOT isdigit(left (.w_CFSECCOD,1)))
    endwith
  endfunc

  add object oBox_1_23 as StdBox with uid="CFDPFIVXMB",left=15, top=95, width=670,height=45

  add object oBox_1_24 as StdBox with uid="KVLWDLXGTZ",left=15, top=162, width=670,height=67
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kcg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
