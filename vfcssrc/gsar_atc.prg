* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_atc                                                        *
*              Tipi confezioni                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_16]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2008-08-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_atc"))

* --- Class definition
define class tgsar_atc as StdForm
  Top    = 51
  Left   = 28

  * --- Standard Properties
  Width  = 431
  Height = 60+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-08-27"
  HelpContextID=92895081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  TIPICONF_IDX = 0
  cFile = "TIPICONF"
  cKeySelect = "TCCODICE"
  cKeyWhere  = "TCCODICE=this.w_TCCODICE"
  cKeyWhereODBC = '"TCCODICE="+cp_ToStrODBC(this.w_TCCODICE)';

  cKeyWhereODBCqualified = '"TIPICONF.TCCODICE="+cp_ToStrODBC(this.w_TCCODICE)';

  cPrg = "gsar_atc"
  cComment = "Tipi confezioni"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0ATC'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TCCODICE = space(3)
  w_TCDESCRI = space(30)
  w_TCFLCOVA = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIPICONF','gsar_atc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_atcPag1","gsar_atc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tipo")
      .Pages(1).HelpContextID = 85133514
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTCCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TIPICONF'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIPICONF_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIPICONF_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TCCODICE = NVL(TCCODICE,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TIPICONF where TCCODICE=KeySet.TCCODICE
    *
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIPICONF')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIPICONF.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TIPICONF '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TCCODICE = NVL(TCCODICE,space(3))
        .w_TCDESCRI = NVL( cp_TransLoadField('TCDESCRI') ,space(30))
        .w_TCFLCOVA = NVL(TCFLCOVA,space(1))
        cp_LoadRecExtFlds(this,'TIPICONF')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TCCODICE = space(3)
      .w_TCDESCRI = space(30)
      .w_TCFLCOVA = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_TCFLCOVA = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIPICONF')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTCCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTCDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oTCFLCOVA_1_3.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTCCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTCCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TIPICONF',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCCODICE,"TCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCDESCRI,"TCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCFLCOVA,"TCFLCOVA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
    i_lTable = "TIPICONF"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIPICONF_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSMA_QT1.VQR,QUERY\GSMA_SAA.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIPICONF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TIPICONF_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIPICONF
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIPICONF')
        i_extval=cp_InsertValODBCExtFlds(this,'TIPICONF')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TCCODICE,TCDESCRI"+cp_TransInsFldName('TCDESCRI')+",TCFLCOVA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TCCODICE)+;
                  ","+cp_ToStrODBC(this.w_TCDESCRI)+cp_TransInsFldValue(this.w_TCDESCRI)+;
                  ","+cp_ToStrODBC(this.w_TCFLCOVA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIPICONF')
        i_extval=cp_InsertValVFPExtFlds(this,'TIPICONF')
        cp_CheckDeletedKey(i_cTable,0,'TCCODICE',this.w_TCCODICE)
        INSERT INTO (i_cTable);
              (TCCODICE,TCDESCRI,TCFLCOVA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TCCODICE;
                  ,this.w_TCDESCRI;
                  ,this.w_TCFLCOVA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TIPICONF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TIPICONF_IDX,i_nConn)
      *
      * update TIPICONF
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TIPICONF')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TCDESCRI="+cp_TransUpdFldName('TCDESCRI',this.w_TCDESCRI)+;
             ",TCFLCOVA="+cp_ToStrODBC(this.w_TCFLCOVA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TIPICONF')
        i_cWhere = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
        UPDATE (i_cTable) SET;
              TCDESCRI=this.w_TCDESCRI;
             ,TCFLCOVA=this.w_TCFLCOVA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TIPICONF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TIPICONF_IDX,i_nConn)
      *
      * delete TIPICONF
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTCCODICE_1_1.value==this.w_TCCODICE)
      this.oPgFrm.Page1.oPag.oTCCODICE_1_1.value=this.w_TCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDESCRI_1_2.value==this.w_TCDESCRI)
      this.oPgFrm.Page1.oPag.oTCDESCRI_1_2.value=this.w_TCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTCFLCOVA_1_3.RadioValue()==this.w_TCFLCOVA)
      this.oPgFrm.Page1.oPag.oTCFLCOVA_1_3.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'TIPICONF')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TCCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTCCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_atcPag1 as StdContainer
  Width  = 427
  height = 60
  stdWidth  = 427
  stdheight = 60
  resizeXpos=270
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTCCODICE_1_1 as StdField with uid="CNPWMEYTCX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TCCODICE", cQueryName = "TCCODICE",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice del tipo di confezione",;
    HelpContextID = 134873467,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=123, Top=10, InputMask=replicate('X',3)

  add object oTCDESCRI_1_2 as StdField with uid="BILUTOCKSA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TCDESCRI", cQueryName = "TCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .t.,;
    ToolTipText = "Descrizione del tipo di confezione",;
    HelpContextID = 49287551,;
   bGlobalFont=.t.,;
    Height=21, Width=255, Left=163, Top=10, InputMask=replicate('X',30)

  add object oTCFLCOVA_1_3 as StdCheck with uid="XLNAXDSRVP",rtseq=3,rtrep=.f.,left=124, top=36, caption="Contenuto variabile",;
    ToolTipText = "Se attivo la confezione � a contenuto variabile",;
    HelpContextID = 234303863,;
    cFormVar="w_TCFLCOVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTCFLCOVA_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTCFLCOVA_1_3.GetRadio()
    this.Parent.oContained.w_TCFLCOVA = this.RadioValue()
    return .t.
  endfunc

  func oTCFLCOVA_1_3.SetRadio()
    this.Parent.oContained.w_TCFLCOVA=trim(this.Parent.oContained.w_TCFLCOVA)
    this.value = ;
      iif(this.Parent.oContained.w_TCFLCOVA=='S',1,;
      0)
  endfunc

  add object oStr_1_4 as StdString with uid="IUNAFWDRWC",Visible=.t., Left=13, Top=10,;
    Alignment=1, Width=108, Height=18,;
    Caption="Codice confezione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_atc','TIPICONF','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TCCODICE=TIPICONF.TCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
