* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bdg                                                        *
*              Driver pdt600z generale code                                    *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-05-19                                                      *
* Last revis.: 2004-05-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bdg",oParentObject,m.pCursore,m.pTXTFile,m.pObjMsg,m.p_ArrayFieldName,m.p_ArrayFieldPosition)
return(i_retval)

define class tgsut_bdg as StdBatch
  * --- Local variables
  w_FileOutTxt = space(254)
  pCursore = space(10)
  pTXTFile = space(254)
  pObjMsg = .NULL.
  p_ArrayFieldName = space(40)
  p_ArrayFieldPosition = space(40)
  w_MESS = space(250)
  w_NumRip = 0
  w_NTerm = space(1)
  W_FileOut = space(250)
  w_FileHandle = 0
  w_FileEnd = 0
  w_FileTop = 0
  w_Err1 = 0
  w_StrLetta = space(250)
  w_RecTot = 0
  w_RecErrati = 0
  w_LOGWriteHandle = space(12)
  w_Buffer = space(250)
  w_TimeOut = 0
  w_NCom = 0
  w_DataBit = 0
  w_BitStop = 0
  w_StrDaIns = space(0)
  w_ErrLOG = space(250)
  w_NUltimoRec = 0
  w_AttStr = space(250)
  w_FileIN = space(250)
  w_ESCDelay = 0
  w_NAKDelay = 0
  w_Vel = 0
  w_Parita = 0
  w_TmpParita = space(10)
  w_Err = 0
  w_VeraVel = 0
  w_OriVel = 0
  * --- WorkFile variables
  DIS_HARD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Driver per penna ottica PDT600Z General Code (utilizza MULTIDLL.DLL)
    *     =========================================================
    *     Utilizzare questo batch come driver per caricare dati dalla penna ottica
    *     tramite l'INSERIMENTO DATI (GSAR_BPG=>GSAR_MPG).
    *     
    *     La procedura riceve come parametro il nome del cursore che dovr� riempire.
    *     La struttura del cursore � fissa ed �:
    *     
    *     Create Cursor ( w_CURSORE ) ( CODICE C(20) NULL, UNIMIS C(3) NULL, QTAMOV N(12,3) NULL, PREZZO N(18,5) NULL,;
    *     LOTTO CHAR(20) NULL , CODMAG C(5) NULL,UBICAZIONE C(20) NULL , MATRICOLA C(40) NULL )
    *     
    *     Il secondo parametro � il nome del file contenuto nella penna da scaricare
    *     (occorre impostarlo nell'anagrafica dispositivi come parametro)
    *     
    *     Il terzo parametro rappresenta l'oggetto da utilizzare per visualizzare i messaggi (deve contenere un campo memo w_MSG).
    *     
    *     Il Driver scarica il contenuto della penna in un file di testo, utilizza poi il Driver
    *     per file di testo (GSUT_BDT) per caricare la movimentazione.
    *     
    *     Il terzo parametro rappresenta l'oggetto da utilizzare per visualizzare i messaggi (deve contenere un campo memo w_MSG).
    *     Il quarto e quinto parametro contengono le informazioni riguardanti posizione e nome campo (sono array passati come riferimento)
    *     (gia cablati all'interno del codice da passare vuoti)
    * --- Dichiarazione funzioni contenute nella DLL Generale Code
    L_OLDERR= ON("ERROR") 
 
    L_ErrDll=.F.
    ON ERROR L_ErrDll=.T.
     
 Declare integer HM_set_time_out IN multidll.DLL long _Timeout 
 Declare integer HM_set_ESC_delay IN multidll.DLL long Delay 
 Declare integer HM_set_NAK_delay IN multidll.DLL long Nak_Delay 
 Declare integer HM_OpenCommPort IN multidll.DLL long NumCom, long, long, long, long 
 Declare long HM_upload IN multidll.DLL long, long, String, String 
 Declare long HM_upload1 IN multidll.DLL long, long, String, String 
 Declare long HM_upload2 IN multidll.DLL long @ IByteCount, long @ IPacketCount 
 Declare integer HM_CloseCommPort IN multidll.DLL long 
 Declare long HM_dir IN multidll.DLL Long, long, String @ Buffer
    ON ERROR &L_OLDERR
    if L_ErrDll
      ah_msg( "Libreria di collegamento mancante",.f.)
      i_retcode = 'stop'
      return
    endif
    this.w_NumRip = 60
    ah_ErrorMsg("Inserire la penna nel calamaio",48)
    AddMsgNL(REPLICATE(chr(45),this.w_NumRip),this.pObjMsg)
    AddMsgNL("Fase 1: collegamento alla penna%0%1",REPLICATE(chr(45),this.w_NumRip),this.pObjMsg)
    * --- Leggo i dati dall'anagrafica dispositivi
    * --- Read from DIS_HARD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_HARD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_HARD_idx,2],.t.,this.DIS_HARD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DHNUMPOR,DH__BAUD,DHBITDAT,DHPARITA,DHBITSTP,DHRITARD,DHNAKDEL,DHESCDEL,DHNUMTER"+;
        " from "+i_cTable+" DIS_HARD where ";
            +"DHCODICE = "+cp_ToStrODBC(g_CODPEN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DHNUMPOR,DH__BAUD,DHBITDAT,DHPARITA,DHBITSTP,DHRITARD,DHNAKDEL,DHESCDEL,DHNUMTER;
        from (i_cTable) where;
            DHCODICE = g_CODPEN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NCom = NVL(cp_ToDate(_read_.DHNUMPOR),cp_NullValue(_read_.DHNUMPOR))
      this.w_Vel = NVL(cp_ToDate(_read_.DH__BAUD),cp_NullValue(_read_.DH__BAUD))
      this.w_DataBit = NVL(cp_ToDate(_read_.DHBITDAT),cp_NullValue(_read_.DHBITDAT))
      this.w_TmpParita = NVL(cp_ToDate(_read_.DHPARITA),cp_NullValue(_read_.DHPARITA))
      this.w_BitStop = NVL(cp_ToDate(_read_.DHBITSTP),cp_NullValue(_read_.DHBITSTP))
      this.w_TimeOut = NVL(cp_ToDate(_read_.DHRITARD),cp_NullValue(_read_.DHRITARD))
      this.w_NAKDelay = NVL(cp_ToDate(_read_.DHNAKDEL),cp_NullValue(_read_.DHNAKDEL))
      this.w_ESCDelay = NVL(cp_ToDate(_read_.DHESCDEL),cp_NullValue(_read_.DHESCDEL))
      this.w_NTerm = NVL(cp_ToDate(_read_.DHNUMTER),cp_NullValue(_read_.DHNUMTER))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- File da leggere sul terminale
    if EMPTY( this.pTXTFile )
      ah_ErrorMsg("Specificare nel parametro del dispositivo il nome del file da importare dalla penna",48)
      i_retcode = 'stop'
      return
    else
      this.w_FileIN = Alltrim(this.pTXTFile)
    endif
    * --- File da scrivere sull'HD, utilizzo la cartella temporanea di fox, do un nome unico
    this.W_FileOut = tempadhoc()+"\"+SYS(2015)
    AddMsgNL("Verifica file di esportazione...",this.pObjMsg)
    if FILE(this.w_FileOut)
      this.w_FileWriteHandle = FOPEN(this.w_FileOut,1)
      if this.w_FileWriteHandle<0
        this.w_NFile = 1
        do while this.w_FileWriteHandle<0
          FCLOSE(this.w_FileWriteHandle) 
          if FILE(ALLTRIM(this.w_FileOut)+ALLTRIM(str(this.w_NFile)))
            this.w_FileWriteHandle = FOPEN(ALLTRIM(this.w_FileOut)+ALLTRIM(str(this.w_NFile)),1)
          else
            this.w_FileWriteHandle = FCREATE(ALLTRIM(this.w_FileOut)+ALLTRIM(str(this.w_NFile)),0)
          endif
          this.w_NFile = this.w_NFile+1
        enddo
        FCLOSE(this.w_FileWriteHandle) 
        ERASE ALLTRIM(this.w_FileOut)+ALLTRIM(Str(this.w_NFile-1))
        this.W_FileOut = ALLTRIM(this.w_FileOut)+ALLTRIM(Str(this.w_NFile-1))
      else
        FCLOSE(this.w_FileWriteHandle) 
        ERASE (this.w_FileOut)
      endif
    endif
    AddMsgNL("Impostazone parametri di comunicazione...",this.pObjMsg)
    AddMsgNL("%1File da leggere... %1%2",this.pObjMsg,chr(9),ALLTRIM(this.w_FileIN))
    AddMsgNL("%1TimeOut porta... %1%2",this.pObjMsg,chr(9),ALLTRIM(str(this.w_TimeOut)))
    * --- Porta COM collegata alla penna
    AddMsgNL("%1Porta COM... %1%1%2",this.pObjMsg,chr(9),ALLTRIM(str(this.w_NCom)))
    AddMsgNL("%1Trasmissione a... %1%2 BAUD",this.pObjMsg,chr(9),ALLTRIM(str(this.w_Vel)))
    * --- La velocit� si esprime con un numero da 1 a 9
    *     Velocit� porta 7=19200 8=38400 9=57600....
    if this.w_VEL=57600
      this.w_Vel = 9
    else
      this.w_OriVel = this.w_Vel
      this.w_VeraVel = 38400
      this.w_Vel = 8
      do while this.w_VeraVel<>this.w_OriVel
        this.w_VeraVel = this.w_VeraVel/2
        this.w_Vel = this.w_Vel - 1
      enddo
    endif
    AddMsgNL("%1Codificata in... %1%2",this.pObjMsg,chr(9),ALLTRIM(str(this.w_Vel)))
    AddMsgNL("%1Data bit... %1%1%2",this.pObjMsg,chr(9),ALLTRIM(str(this.w_DataBit)))
    do case
      case this.w_Tmpparita="0"
        this.w_Parita = 0
        AddMsgNL("%1Controllo parit�... %1nessuno",this.pObjMsg,chr(9))
      case this.w_Tmpparita="1"
        this.w_Parita = 1
        AddMsgNL("%1Controllo parit�... %1pari",this.pObjMsg,chr(9))
      case this.w_Tmpparita="2"
        this.w_Parita = 2
        AddMsgNL("%1Controllo parit�... %1dispari",this.pObjMsg,chr(9))
    endcase
    * --- Bit di stop (0,1,2)
    AddMsgNL("%1Stop bits... %1%1%2",this.pObjMsg,chr(9),ALLTRIM(str(this.w_BitStop)))
    this.w_Buffer = ""
    IByteCount=0
    IPacketCount=0
    L_OldError = on("ERROR")
    ON ERROR w_Errore=.T.
    * --- Imosto il time out di comunicazione
    this.w_Err = HM_set_time_out(this.w_TimeOut)
    * --- Attendo per verificare la comunicazine
    this.w_Err = HM_set_esc_delay(this.w_ESCDelay)
    AddMsgNL("%1ESC delay... %1%1%2",this.pObjMsg,chr(9),ALLTRIM(str(this.w_ESCDelay)))
    * --- Setto il ritardo ACK/NAK
    this.w_Err = HM_set_NAK_delay(this.w_NAKDelay)
    AddMsgNL("%1NAK delay... %1%1%2",this.pObjMsg,chr(9),ALLTRIM(str(this.w_NAKDelay)))
    * --- Apro la porta di comunicazione
    AddMsgNL("Apertura canale di comunicazione...",this.pObjMsg)
    this.w_Err = HM_OpenCommPort(this.w_NCom,this.w_Vel,this.w_DataBit,this.w_parita,this.w_BitStop)
    AddMsgNL(REPLICATE(chr(45),this.w_NumRip),this.pObjMsg)
    AddMsgNL("Fase 2: scaricamento informazioni%0%1",this.pObjMsg,REPLICATE(chr(45),this.w_NumRip))
    this.w_Err1 = -1
    this.w_NUltimoRec = 1
    this.w_MESS = Ah_MsgFormat("Ricerca file")
    do while this.w_err1<>0 AND this.w_NUltimorec<=20
      if this.w_Err1<>0
        if AT(this.w_MESS, this.pObjMsg.w_Msg )>0
          this.w_StrDaIns = LEFT( this.pObjMsg.w_Msg , AT(this.w_MESS, this.pObjMsg.w_Msg )-1)
          this.pObjMsg.w_Msg = ""
        else
          this.w_StrDaIns = ""
        endif
        AddMsgNL("%1Ricerca file%2",this.pObjMsg,this.w_StrDaIns,chr(32)+REPLICATE(chr(35),this.w_NUltimoRec)+REPLICATE(chr(46),20-this.w_NUltimoRec))
        WAIT "" TIMEOUT 1
      endif
      this.w_Err1 = HM_Upload(this.w_NCom,Asc(this.w_NTerm),this.w_FileIn,this.w_FileOut)
      this.w_NUltimoRec = this.w_NUltimoRec+1
    enddo
    w_ERRORE=this.w_Err1<>0
    on error &L_OldError 
    AddMsgNL("Scaricamento informazioni terminato...",this.pObjMsg)
    * --- Chiudo la porta di comunicazione
    AddMsgNL("Chiusura canale di comunicazione...",this.pObjMsg)
    this.w_Err = HM_CloseCommPort(this.w_NCom)
    * --- Definisco la struttura del file di testo generato dalla penna, 
    *     queste informazioni sono utilizzate dal driver per file di testo
    *     per decifrare il file di testo...
    if Upper(g_APPLICATION)<>"ADHOC REVOLUTION" And RIGHT(this.w_FILEIN,3)="DAT"
       
 Dimension L_FieldName[13], L_FieldPosition [13] 
 L_FieldPosition =0 
 L_FieldName="" 
 L_FieldPosition [1]=1 
 L_FieldPosition [2]=5 
 L_FieldPosition [3]=11 
 L_FieldPosition [4]=16 
 L_FieldPosition [5]=36 
 L_FieldPosition [6]=56 
 L_FieldPosition [7]=97 
 L_FieldPosition [8]=117 
 L_FieldPosition [9]=120 
 L_FieldPosition [10]=135 
 L_FieldPosition [11]=142 
 L_FieldPosition [12]=157 
 L_FieldPosition [13]=0 
 * 
 L_FieldName[1]="CODTER" 
 L_FieldName[2]="NUMDOC" 
 L_FieldName[3]="ADDRIL" 
 L_FieldName[4]="CODMAG" 
 L_FieldName[5]="UBICAZ" 
 L_FieldName[6]="MATRIC" 
 L_FieldName[7]="CODRIC" 
 L_FieldName[8]="LOTTO" 
 L_FieldName[9]="UNIMIS" 
 L_FieldName[10]="QTAESI" 
 L_FieldName[11]="DATCRE" 
 L_FieldName[12]="SERIAL" 
 L_FieldName[13]=""
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
       
 Dimension L_FieldName[7], L_FieldPosition [7] 
 L_FieldPosition =0 
 L_FieldName="" 
 L_FieldPosition [1]=1 
 L_FieldPosition [2]=24 
 L_FieldPosition [3]=38 
 L_FieldPosition [4]=57 
 L_FieldPosition [5]=87 
 L_FieldPosition [6]=128 
 L_FieldPosition [7]=0 
 * 
 L_FieldName[1]="CODICE" 
 L_FieldName[2]="QTAMOV" 
 L_FieldName[3]="PREZZO" 
 L_FieldName[4]="LOTTO" 
 L_FieldName[5]="MATRICOLA" 
 L_FieldName[6]="SERRIF" 
 L_FieldName[7]="" 
 
    endif
    * --- Riempio il cursore con il file TXT creato, utilzzo il DRIVER file di testo
    *     
    GSUT_BDT (This, this.pCursore , this.w_FILEOUT , this.pObjMsg, @L_FieldName, @L_FieldPosition)
    * --- Rimuovo il file di testo creato
    Erase (this.w_FILEOUT)
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    CREATE CURSOR _TmpAgg_ (CODTER C(5), NUMDOC N(6,0), ADDRIL C(5), CODMAG C(5), UBICAZ C(20), MATRIC C(40),; 
 CODRIC C(41), LOTTO C(20), UNIMIS C(3), QTAESI N(12,3), DATCRE D, SERIAL C(15) )
    this.W_FileOutTxt = tempadhoc()+"\"+SYS(2015)
    if used ("_TmpAgg_")
      select _TmpAgg_
      L_MACRO="append from "+this.w_FileOut+" SDF"
      &L_MACRO
      select _TmpAgg_
      GO TOP
      N_File=FCREATE(this.w_FileOutTxt)
      if N_File>0
        SCAN
        FPUTS(N_File,LEFT(CODTER+SPACE(5),5)+LEFT(STR(NUMDOC)+SPACE(6),6)+LEFT(ADDRIL+SPACE(5),5)+LEFT(CODMAG+SPACE(5),5)+LEFT(UBICAZ+SPACE(20),20),LEFT(MATRIC+SPACE(40),40)+LEFT(CODRIC+SPACE(41),41)+LEFT(LOTTO+SPACE(20),20)+LEFT(UNIMIS+SPACE(3),3)+LEFT(STR(QTAESI)+SPACE(15),15)+LEFT(DTOC(DATCRE)+SPACE(8),8)+LEFT(SERIAL+SPACE(15),15))
        ENDSCAN
        FCLOSE(N_File)
      endif
      * --- Cancella File ASCII
      USE IN _TmpAgg_
      erase this.w_FileOut
      this.W_FileOut = this.w_FileoutTxt
    endif
  endproc


  proc Init(oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition)
    this.pCursore=pCursore
    this.pTXTFile=pTXTFile
    this.pObjMsg=pObjMsg
    this.p_ArrayFieldName=p_ArrayFieldName
    this.p_ArrayFieldPosition=p_ArrayFieldPosition
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIS_HARD'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition"
endproc
