* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_avp                                                        *
*              Liquidazione periodica IVA                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_145]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-30                                                      *
* Last revis.: 2008-10-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_avp"))

* --- Class definition
define class tgsar_avp as StdForm
  Top    = 1
  Left   = 8

  * --- Standard Properties
  Width  = 683
  Height = 399+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-09"
  HelpContextID=209094807
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=69

  * --- Constant Properties
  IVA_PERI_IDX = 0
  AZIENDA_IDX = 0
  ATTIMAST_IDX = 0
  COD_ABI_IDX = 0
  COD_CAB_IDX = 0
  cFile = "IVA_PERI"
  cKeySelect = "VP__ANNO,VPPERIOD,VPKEYATT"
  cKeyWhere  = "VP__ANNO=this.w_VP__ANNO and VPPERIOD=this.w_VPPERIOD and VPKEYATT=this.w_VPKEYATT"
  cKeyWhereODBC = '"VP__ANNO="+cp_ToStrODBC(this.w_VP__ANNO)';
      +'+" and VPPERIOD="+cp_ToStrODBC(this.w_VPPERIOD)';
      +'+" and VPKEYATT="+cp_ToStrODBC(this.w_VPKEYATT)';

  cKeyWhereODBCqualified = '"IVA_PERI.VP__ANNO="+cp_ToStrODBC(this.w_VP__ANNO)';
      +'+" and IVA_PERI.VPPERIOD="+cp_ToStrODBC(this.w_VPPERIOD)';
      +'+" and IVA_PERI.VPKEYATT="+cp_ToStrODBC(this.w_VPKEYATT)';

  cPrg = "gsar_avp"
  cComment = "Liquidazione periodica IVA"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_VP__ANNO = space(4)
  w_VPPERIOD = 0
  w_CODATT = space(5)
  w_PERIODO = 0
  w_INTERE = 0
  w_CRESPEC = 0
  w_MAGTRI = 0
  w_VPIMPOR7 = 0
  o_VPIMPOR7 = 0
  w_VPIMPOR8 = 0
  o_VPIMPOR8 = 0
  w_VPIMPOR9 = 0
  o_VPIMPOR9 = 0
  w_VPCODATT = space(5)
  w_VPCODVAL = space(1)
  o_VPCODVAL = space(1)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_VPVARIMP = space(1)
  w_VPCORTER = space(1)
  w_VPDICGRU = space(1)
  w_VPDICSOC = space(1)
  w_DIVIDO = 0
  w_VPIMPOR1 = 0
  w_VPCESINT = 0
  w_VPIMPOR2 = 0
  w_VPACQINT = 0
  w_VPIMPON3 = 0
  w_VPIMPOS3 = 0
  w_VPIMPOR5 = 0
  w_VPIMPOR6 = 0
  w_IMPDEB7 = 0
  o_IMPDEB7 = 0
  w_IMPCRE7 = 0
  o_IMPCRE7 = 0
  w_IMPDEB8 = 0
  o_IMPDEB8 = 0
  w_IMPCRE8 = 0
  o_IMPCRE8 = 0
  w_IMPDEB9 = 0
  o_IMPDEB9 = 0
  w_IMPCRE9 = 0
  o_IMPCRE9 = 0
  w_VPIMPO10 = 0
  o_VPIMPO10 = 0
  w_IMPDEB10 = 0
  o_IMPDEB10 = 0
  w_IMPCRE10 = 0
  o_IMPCRE10 = 0
  w_VPIMPO11 = 0
  w_VPIMPO12 = 0
  o_VPIMPO12 = 0
  w_IMPDEB12 = 0
  o_IMPDEB12 = 0
  w_IMPCRE12 = 0
  o_IMPCRE12 = 0
  w_VPIMPO15 = 0
  o_VPIMPO15 = 0
  w_VPIMPO16 = 0
  w_VPIMPO13 = 0
  o_VPIMPO13 = 0
  w_VPIMPO14 = 0
  o_VPIMPO14 = 0
  w_VPIMPVER = 0
  w_VPVEREUR = space(1)
  w_VPDATVER = ctod('  /  /  ')
  w_VPCODAZI = space(5)
  w_VPCODCAB = space(5)
  w_VPVERNEF = space(1)
  w_VPAR74C5 = space(1)
  w_VPAR74C4 = space(1)
  w_VPOPMEDI = space(1)
  w_VPOPNOIM = space(1)
  w_VPACQBEA = space(1)
  w_VPCRERIM = 0
  w_VPEURO19 = space(3)
  w_VPCREUTI = 0
  w_VPCODCAR = space(1)
  w_VPCODFIS = space(16)
  w_VPDATPRE = ctod('  /  /  ')
  w_VPKEYATT = space(5)
  w_DESATT = space(35)
  w_VPDATINO = ctod('  /  /  ')
  w_DESABI = space(80)
  w_DESFIL = space(40)
  w_INDIRI = space(50)
  w_CAP = space(8)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'IVA_PERI','gsar_avp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_avpPag1","gsar_avp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati IVA")
      .Pages(1).HelpContextID = 132591223
      .Pages(2).addobject("oPag","tgsar_avpPag2","gsar_avp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dichiarante")
      .Pages(2).HelpContextID = 75231607
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVP__ANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ATTIMAST'
    this.cWorkTables[3]='COD_ABI'
    this.cWorkTables[4]='COD_CAB'
    this.cWorkTables[5]='IVA_PERI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.IVA_PERI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.IVA_PERI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_VP__ANNO = NVL(VP__ANNO,space(4))
      .w_VPPERIOD = NVL(VPPERIOD,0)
      .w_VPKEYATT = NVL(VPKEYATT,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from IVA_PERI where VP__ANNO=KeySet.VP__ANNO
    *                            and VPPERIOD=KeySet.VPPERIOD
    *                            and VPKEYATT=KeySet.VPKEYATT
    *
    i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('IVA_PERI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "IVA_PERI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' IVA_PERI '
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'VP__ANNO',this.w_VP__ANNO  ,'VPPERIOD',this.w_VPPERIOD  ,'VPKEYATT',this.w_VPKEYATT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_CODATT = g_CATAZI
        .w_PERIODO = .w_VPPERIOD
        .w_INTERE = 0
        .w_CRESPEC = 0
        .w_MAGTRI = 0
        .w_DESATT = space(35)
        .w_DESABI = space(80)
        .w_DESFIL = space(40)
        .w_INDIRI = space(50)
        .w_CAP = space(8)
          .link_1_1('Load')
        .w_VP__ANNO = NVL(VP__ANNO,space(4))
        .w_VPPERIOD = NVL(VPPERIOD,0)
        .w_VPIMPOR7 = NVL(VPIMPOR7,0)
        .w_VPIMPOR8 = NVL(VPIMPOR8,0)
        .w_VPIMPOR9 = NVL(VPIMPOR9,0)
        .w_VPCODATT = NVL(VPCODATT,space(5))
          if link_1_13_joined
            this.w_VPCODATT = NVL(ATCODATT113,NVL(this.w_VPCODATT,space(5)))
            this.w_DESATT = NVL(ATDESATT113,space(35))
          else
          .link_1_13('Load')
          endif
        .w_VPCODVAL = NVL(VPCODVAL,space(1))
        .w_DECTOT = IIF(VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO), 2, 0)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_VPVARIMP = NVL(VPVARIMP,space(1))
        .w_VPCORTER = NVL(VPCORTER,space(1))
        .w_VPDICGRU = NVL(VPDICGRU,space(1))
        .w_VPDICSOC = NVL(VPDICSOC,space(1))
        .w_DIVIDO = IIF ( .w_VPCODVAL<> 'S' , 1000 , 1 )
        .w_VPIMPOR1 = NVL(VPIMPOR1,0)
        .w_VPCESINT = NVL(VPCESINT,0)
        .w_VPIMPOR2 = NVL(VPIMPOR2,0)
        .w_VPACQINT = NVL(VPACQINT,0)
        .w_VPIMPON3 = NVL(VPIMPON3,0)
        .w_VPIMPOS3 = NVL(VPIMPOS3,0)
        .w_VPIMPOR5 = NVL(VPIMPOR5,0)
        .w_VPIMPOR6 = NVL(VPIMPOR6,0)
        .w_IMPDEB7 = IIF( .w_VPIMPOR7>0 ,  .w_VPIMPOR7, 0)
        .w_IMPCRE7 = IIF( .w_VPIMPOR7<0 ,  ABS(.w_VPIMPOR7) ,0 )
        .w_IMPDEB8 = IIF( .w_VPIMPOR8>0 ,  .w_VPIMPOR8, 0)
        .w_IMPCRE8 = IIF( .w_VPIMPOR8<0 ,  ABS(.w_VPIMPOR8) ,0 )
        .w_IMPDEB9 = IIF( .w_VPIMPOR9>0 ,  .w_VPIMPOR9, 0)
        .w_IMPCRE9 = IIF( .w_VPIMPOR9<0 ,  ABS(.w_VPIMPOR9) ,0 )
        .w_VPIMPO10 = NVL(VPIMPO10,0)
        .w_IMPDEB10 = IIF( .w_VPIMPO10>0,  .w_VPIMPO10 ,0)
        .w_IMPCRE10 = IIF( .w_VPIMPO10<0 ,  ABS(.w_VPIMPO10) ,0)
        .w_VPIMPO11 = NVL(VPIMPO11,0)
        .w_VPIMPO12 = NVL(VPIMPO12,0)
        .w_IMPDEB12 = IIF(.w_VPIMPO12>0, .w_VPIMPO12 , 0 )
        .w_IMPCRE12 = IIF(.w_VPIMPO12<0, ABS(.w_VPIMPO12) , 0 )
        .w_VPIMPO15 = NVL(VPIMPO15,0)
        .w_VPIMPO16 = NVL(VPIMPO16,0)
        .w_VPIMPO13 = NVL(VPIMPO13,0)
        .w_VPIMPO14 = NVL(VPIMPO14,0)
        .w_VPIMPVER = NVL(VPIMPVER,0)
        .w_VPVEREUR = NVL(VPVEREUR,space(1))
        .w_VPDATVER = NVL(cp_ToDate(VPDATVER),ctod("  /  /  "))
        .w_VPCODAZI = NVL(VPCODAZI,space(5))
          if link_2_4_joined
            this.w_VPCODAZI = NVL(ABCODABI204,NVL(this.w_VPCODAZI,space(5)))
            this.w_DESABI = NVL(ABDESABI204,space(80))
          else
          .link_2_4('Load')
          endif
        .w_VPCODCAB = NVL(VPCODCAB,space(5))
          .link_2_5('Load')
        .w_VPVERNEF = NVL(VPVERNEF,space(1))
        .w_VPAR74C5 = NVL(VPAR74C5,space(1))
        .w_VPAR74C4 = NVL(VPAR74C4,space(1))
        .w_VPOPMEDI = NVL(VPOPMEDI,space(1))
        .w_VPOPNOIM = NVL(VPOPNOIM,space(1))
        .w_VPACQBEA = NVL(VPACQBEA,space(1))
        .w_VPCRERIM = NVL(VPCRERIM,0)
        .w_VPEURO19 = NVL(VPEURO19,space(3))
        .w_VPCREUTI = NVL(VPCREUTI,0)
        .w_VPCODCAR = NVL(VPCODCAR,space(1))
        .w_VPCODFIS = NVL(VPCODFIS,space(16))
        .w_VPDATPRE = NVL(cp_ToDate(VPDATPRE),ctod("  /  /  "))
        .w_VPKEYATT = NVL(VPKEYATT,space(5))
        .w_VPDATINO = NVL(cp_ToDate(VPDATINO),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
        cp_LoadRecExtFlds(this,'IVA_PERI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_VP__ANNO = space(4)
      .w_VPPERIOD = 0
      .w_CODATT = space(5)
      .w_PERIODO = 0
      .w_INTERE = 0
      .w_CRESPEC = 0
      .w_MAGTRI = 0
      .w_VPIMPOR7 = 0
      .w_VPIMPOR8 = 0
      .w_VPIMPOR9 = 0
      .w_VPCODATT = space(5)
      .w_VPCODVAL = space(1)
      .w_DECTOT = 0
      .w_CALCPICT = 0
      .w_VPVARIMP = space(1)
      .w_VPCORTER = space(1)
      .w_VPDICGRU = space(1)
      .w_VPDICSOC = space(1)
      .w_DIVIDO = 0
      .w_VPIMPOR1 = 0
      .w_VPCESINT = 0
      .w_VPIMPOR2 = 0
      .w_VPACQINT = 0
      .w_VPIMPON3 = 0
      .w_VPIMPOS3 = 0
      .w_VPIMPOR5 = 0
      .w_VPIMPOR6 = 0
      .w_IMPDEB7 = 0
      .w_IMPCRE7 = 0
      .w_IMPDEB8 = 0
      .w_IMPCRE8 = 0
      .w_IMPDEB9 = 0
      .w_IMPCRE9 = 0
      .w_VPIMPO10 = 0
      .w_IMPDEB10 = 0
      .w_IMPCRE10 = 0
      .w_VPIMPO11 = 0
      .w_VPIMPO12 = 0
      .w_IMPDEB12 = 0
      .w_IMPCRE12 = 0
      .w_VPIMPO15 = 0
      .w_VPIMPO16 = 0
      .w_VPIMPO13 = 0
      .w_VPIMPO14 = 0
      .w_VPIMPVER = 0
      .w_VPVEREUR = space(1)
      .w_VPDATVER = ctod("  /  /  ")
      .w_VPCODAZI = space(5)
      .w_VPCODCAB = space(5)
      .w_VPVERNEF = space(1)
      .w_VPAR74C5 = space(1)
      .w_VPAR74C4 = space(1)
      .w_VPOPMEDI = space(1)
      .w_VPOPNOIM = space(1)
      .w_VPACQBEA = space(1)
      .w_VPCRERIM = 0
      .w_VPEURO19 = space(3)
      .w_VPCREUTI = 0
      .w_VPCODCAR = space(1)
      .w_VPCODFIS = space(16)
      .w_VPDATPRE = ctod("  /  /  ")
      .w_VPKEYATT = space(5)
      .w_DESATT = space(35)
      .w_VPDATINO = ctod("  /  /  ")
      .w_DESABI = space(80)
      .w_DESFIL = space(40)
      .w_INDIRI = space(50)
      .w_CAP = space(8)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_CODATT = g_CATAZI
        .w_PERIODO = .w_VPPERIOD
          .DoRTCalc(6,8,.f.)
        .w_VPIMPOR7 = .w_IMPDEB7-.w_IMPCRE7
        .w_VPIMPOR8 = .w_IMPDEB8 - .w_IMPCRE8
        .w_VPIMPOR9 = .w_IMPDEB9 - .w_IMPCRE9
        .w_VPCODATT = g_CATAZI
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_VPCODATT))
          .link_1_13('Full')
          endif
        .w_VPCODVAL = IIF(g_PERVAL = g_CODEUR , 'S' , 'N' )
        .w_DECTOT = IIF(VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO), 2, 0)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_VPVARIMP = 'N'
        .w_VPCORTER = ' '
        .w_VPDICGRU = 'N'
        .w_VPDICSOC = 'N'
        .w_DIVIDO = IIF ( .w_VPCODVAL<> 'S' , 1000 , 1 )
          .DoRTCalc(21,28,.f.)
        .w_IMPDEB7 = IIF( .w_VPIMPOR7>0 ,  .w_VPIMPOR7, 0)
        .w_IMPCRE7 = IIF( .w_VPIMPOR7<0 ,  ABS(.w_VPIMPOR7) ,0 )
        .w_IMPDEB8 = IIF( .w_VPIMPOR8>0 ,  .w_VPIMPOR8, 0)
        .w_IMPCRE8 = IIF( .w_VPIMPOR8<0 ,  ABS(.w_VPIMPOR8) ,0 )
        .w_IMPDEB9 = IIF( .w_VPIMPOR9>0 ,  .w_VPIMPOR9, 0)
        .w_IMPCRE9 = IIF( .w_VPIMPOR9<0 ,  ABS(.w_VPIMPOR9) ,0 )
        .w_VPIMPO10 = .w_IMPDEB10 - .w_IMPCRE10
        .w_IMPDEB10 = IIF( .w_VPIMPO10>0,  .w_VPIMPO10 ,0)
        .w_IMPCRE10 = IIF( .w_VPIMPO10<0 ,  ABS(.w_VPIMPO10) ,0)
          .DoRTCalc(38,38,.f.)
        .w_VPIMPO12 = .w_IMPDEB12 - .w_IMPCRE12
        .w_IMPDEB12 = IIF(.w_VPIMPO12>0, .w_VPIMPO12 , 0 )
        .w_IMPCRE12 = IIF(.w_VPIMPO12<0, ABS(.w_VPIMPO12) , 0 )
          .DoRTCalc(42,42,.f.)
        .w_VPIMPO16 = IIF((.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15)<0, 0, (.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15))
          .DoRTCalc(44,44,.f.)
        .w_VPIMPO14 = IIF(g_TIPDEN='T' And (.w_VPIMPO12-(.w_VPIMPO13+.w_IMPDEB10))>0 And NOT (g_TIPDEN='T'And .w_VPPERIOD=4), cp_Round(Abs(.w_VPIMPO12-(.w_VPIMPO13+.w_IMPDEB10))*.w_MAGTRI/100, 0), 0)
          .DoRTCalc(46,46,.f.)
        .w_VPVEREUR = IIF(.w_VPCODVAL='S', 'S', ' ')
        .DoRTCalc(48,49,.f.)
          if not(empty(.w_VPCODAZI))
          .link_2_4('Full')
          endif
        .DoRTCalc(50,50,.f.)
          if not(empty(.w_VPCODCAB))
          .link_2_5('Full')
          endif
          .DoRTCalc(51,57,.f.)
        .w_VPEURO19 = IIF(.w_VPCODVAL='S', 'S', 'N')
          .DoRTCalc(59,62,.f.)
        .w_VPKEYATT = IIF(EMPTY(.w_VPCODATT), '#####', .w_VPCODATT)
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'IVA_PERI')
    this.DoRTCalc(64,69,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oVP__ANNO_1_2.enabled = i_bVal
      .Page1.oPag.oVPPERIOD_1_3.enabled = i_bVal
      .Page1.oPag.oVPCODATT_1_13.enabled = i_bVal
      .Page1.oPag.oVPCODVAL_1_14.enabled = i_bVal
      .Page1.oPag.oVPVARIMP_1_17.enabled = i_bVal
      .Page1.oPag.oVPCORTER_1_18.enabled = i_bVal
      .Page1.oPag.oVPDICGRU_1_19.enabled = i_bVal
      .Page1.oPag.oVPDICSOC_1_20.enabled = i_bVal
      .Page1.oPag.oVPIMPOR1_1_24.enabled = i_bVal
      .Page1.oPag.oVPCESINT_1_25.enabled = i_bVal
      .Page1.oPag.oVPIMPOR2_1_27.enabled = i_bVal
      .Page1.oPag.oVPACQINT_1_30.enabled = i_bVal
      .Page1.oPag.oVPIMPON3_1_32.enabled = i_bVal
      .Page1.oPag.oVPIMPOS3_1_34.enabled = i_bVal
      .Page1.oPag.oVPIMPOR5_1_36.enabled = i_bVal
      .Page1.oPag.oVPIMPOR6_1_37.enabled = i_bVal
      .Page1.oPag.oIMPDEB7_1_38.enabled = i_bVal
      .Page1.oPag.oIMPCRE7_1_39.enabled = i_bVal
      .Page1.oPag.oIMPDEB8_1_40.enabled = i_bVal
      .Page1.oPag.oIMPCRE8_1_41.enabled = i_bVal
      .Page1.oPag.oIMPDEB9_1_42.enabled = i_bVal
      .Page1.oPag.oIMPCRE9_1_43.enabled = i_bVal
      .Page1.oPag.oIMPDEB10_1_56.enabled = i_bVal
      .Page1.oPag.oIMPCRE10_1_57.enabled = i_bVal
      .Page1.oPag.oVPIMPO11_1_65.enabled = i_bVal
      .Page1.oPag.oIMPDEB12_1_67.enabled = i_bVal
      .Page1.oPag.oIMPCRE12_1_68.enabled = i_bVal
      .Page1.oPag.oVPIMPO15_1_71.enabled = i_bVal
      .Page1.oPag.oVPIMPO16_1_76.enabled = i_bVal
      .Page1.oPag.oVPIMPO13_1_105.enabled = i_bVal
      .Page1.oPag.oVPIMPO14_1_106.enabled = i_bVal
      .Page2.oPag.oVPIMPVER_2_1.enabled = i_bVal
      .Page2.oPag.oVPDATVER_2_3.enabled = i_bVal
      .Page2.oPag.oVPCODAZI_2_4.enabled = i_bVal
      .Page2.oPag.oVPCODCAB_2_5.enabled = i_bVal
      .Page2.oPag.oVPVERNEF_2_6.enabled = i_bVal
      .Page2.oPag.oVPAR74C5_2_7.enabled = i_bVal
      .Page2.oPag.oVPAR74C4_2_8.enabled = i_bVal
      .Page2.oPag.oVPOPMEDI_2_11.enabled = i_bVal
      .Page2.oPag.oVPOPNOIM_2_12.enabled = i_bVal
      .Page2.oPag.oVPACQBEA_2_13.enabled = i_bVal
      .Page2.oPag.oVPCRERIM_2_14.enabled = i_bVal
      .Page2.oPag.oVPCREUTI_2_17.enabled = i_bVal
      .Page2.oPag.oVPCODCAR_2_19.enabled = i_bVal
      .Page2.oPag.oVPCODFIS_2_22.enabled = i_bVal
      .Page2.oPag.oVPDATPRE_2_23.enabled = i_bVal
      .Page2.oPag.oVPDATINO_2_37.enabled = i_bVal
      .Page1.oPag.oObj_1_110.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oVP__ANNO_1_2.enabled = .f.
        .Page1.oPag.oVPPERIOD_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oVP__ANNO_1_2.enabled = .t.
        .Page1.oPag.oVPPERIOD_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'IVA_PERI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VP__ANNO,"VP__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPPERIOD,"VPPERIOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPOR7,"VPIMPOR7",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPOR8,"VPIMPOR8",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPOR9,"VPIMPOR9",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODATT,"VPCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODVAL,"VPCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPVARIMP,"VPVARIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCORTER,"VPCORTER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPDICGRU,"VPDICGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPDICSOC,"VPDICSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPOR1,"VPIMPOR1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCESINT,"VPCESINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPOR2,"VPIMPOR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPACQINT,"VPACQINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPON3,"VPIMPON3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPOS3,"VPIMPOS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPOR5,"VPIMPOR5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPOR6,"VPIMPOR6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPO10,"VPIMPO10",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPO11,"VPIMPO11",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPO12,"VPIMPO12",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPO15,"VPIMPO15",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPO16,"VPIMPO16",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPO13,"VPIMPO13",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPO14,"VPIMPO14",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPVER,"VPIMPVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPVEREUR,"VPVEREUR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPDATVER,"VPDATVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODAZI,"VPCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODCAB,"VPCODCAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPVERNEF,"VPVERNEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPAR74C5,"VPAR74C5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPAR74C4,"VPAR74C4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPOPMEDI,"VPOPMEDI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPOPNOIM,"VPOPNOIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPACQBEA,"VPACQBEA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCRERIM,"VPCRERIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPEURO19,"VPEURO19",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCREUTI,"VPCREUTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODCAR,"VPCODCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODFIS,"VPCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPDATPRE,"VPDATPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPKEYATT,"VPKEYATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPDATINO,"VPDATINO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])
    i_lTable = "IVA_PERI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.IVA_PERI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gscg_sdp with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.IVA_PERI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into IVA_PERI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'IVA_PERI')
        i_extval=cp_InsertValODBCExtFlds(this,'IVA_PERI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(VP__ANNO,VPPERIOD,VPIMPOR7,VPIMPOR8,VPIMPOR9"+;
                  ",VPCODATT,VPCODVAL,VPVARIMP,VPCORTER,VPDICGRU"+;
                  ",VPDICSOC,VPIMPOR1,VPCESINT,VPIMPOR2,VPACQINT"+;
                  ",VPIMPON3,VPIMPOS3,VPIMPOR5,VPIMPOR6,VPIMPO10"+;
                  ",VPIMPO11,VPIMPO12,VPIMPO15,VPIMPO16,VPIMPO13"+;
                  ",VPIMPO14,VPIMPVER,VPVEREUR,VPDATVER,VPCODAZI"+;
                  ",VPCODCAB,VPVERNEF,VPAR74C5,VPAR74C4,VPOPMEDI"+;
                  ",VPOPNOIM,VPACQBEA,VPCRERIM,VPEURO19,VPCREUTI"+;
                  ",VPCODCAR,VPCODFIS,VPDATPRE,VPKEYATT,VPDATINO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_VP__ANNO)+;
                  ","+cp_ToStrODBC(this.w_VPPERIOD)+;
                  ","+cp_ToStrODBC(this.w_VPIMPOR7)+;
                  ","+cp_ToStrODBC(this.w_VPIMPOR8)+;
                  ","+cp_ToStrODBC(this.w_VPIMPOR9)+;
                  ","+cp_ToStrODBCNull(this.w_VPCODATT)+;
                  ","+cp_ToStrODBC(this.w_VPCODVAL)+;
                  ","+cp_ToStrODBC(this.w_VPVARIMP)+;
                  ","+cp_ToStrODBC(this.w_VPCORTER)+;
                  ","+cp_ToStrODBC(this.w_VPDICGRU)+;
                  ","+cp_ToStrODBC(this.w_VPDICSOC)+;
                  ","+cp_ToStrODBC(this.w_VPIMPOR1)+;
                  ","+cp_ToStrODBC(this.w_VPCESINT)+;
                  ","+cp_ToStrODBC(this.w_VPIMPOR2)+;
                  ","+cp_ToStrODBC(this.w_VPACQINT)+;
                  ","+cp_ToStrODBC(this.w_VPIMPON3)+;
                  ","+cp_ToStrODBC(this.w_VPIMPOS3)+;
                  ","+cp_ToStrODBC(this.w_VPIMPOR5)+;
                  ","+cp_ToStrODBC(this.w_VPIMPOR6)+;
                  ","+cp_ToStrODBC(this.w_VPIMPO10)+;
                  ","+cp_ToStrODBC(this.w_VPIMPO11)+;
                  ","+cp_ToStrODBC(this.w_VPIMPO12)+;
                  ","+cp_ToStrODBC(this.w_VPIMPO15)+;
                  ","+cp_ToStrODBC(this.w_VPIMPO16)+;
                  ","+cp_ToStrODBC(this.w_VPIMPO13)+;
                  ","+cp_ToStrODBC(this.w_VPIMPO14)+;
                  ","+cp_ToStrODBC(this.w_VPIMPVER)+;
                  ","+cp_ToStrODBC(this.w_VPVEREUR)+;
                  ","+cp_ToStrODBC(this.w_VPDATVER)+;
                  ","+cp_ToStrODBCNull(this.w_VPCODAZI)+;
                  ","+cp_ToStrODBCNull(this.w_VPCODCAB)+;
                  ","+cp_ToStrODBC(this.w_VPVERNEF)+;
                  ","+cp_ToStrODBC(this.w_VPAR74C5)+;
                  ","+cp_ToStrODBC(this.w_VPAR74C4)+;
                  ","+cp_ToStrODBC(this.w_VPOPMEDI)+;
                  ","+cp_ToStrODBC(this.w_VPOPNOIM)+;
                  ","+cp_ToStrODBC(this.w_VPACQBEA)+;
                  ","+cp_ToStrODBC(this.w_VPCRERIM)+;
                  ","+cp_ToStrODBC(this.w_VPEURO19)+;
                  ","+cp_ToStrODBC(this.w_VPCREUTI)+;
                  ","+cp_ToStrODBC(this.w_VPCODCAR)+;
                  ","+cp_ToStrODBC(this.w_VPCODFIS)+;
                  ","+cp_ToStrODBC(this.w_VPDATPRE)+;
                  ","+cp_ToStrODBC(this.w_VPKEYATT)+;
                  ","+cp_ToStrODBC(this.w_VPDATINO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'IVA_PERI')
        i_extval=cp_InsertValVFPExtFlds(this,'IVA_PERI')
        cp_CheckDeletedKey(i_cTable,0,'VP__ANNO',this.w_VP__ANNO,'VPPERIOD',this.w_VPPERIOD,'VPKEYATT',this.w_VPKEYATT)
        INSERT INTO (i_cTable);
              (VP__ANNO,VPPERIOD,VPIMPOR7,VPIMPOR8,VPIMPOR9,VPCODATT,VPCODVAL,VPVARIMP,VPCORTER,VPDICGRU,VPDICSOC,VPIMPOR1,VPCESINT,VPIMPOR2,VPACQINT,VPIMPON3,VPIMPOS3,VPIMPOR5,VPIMPOR6,VPIMPO10,VPIMPO11,VPIMPO12,VPIMPO15,VPIMPO16,VPIMPO13,VPIMPO14,VPIMPVER,VPVEREUR,VPDATVER,VPCODAZI,VPCODCAB,VPVERNEF,VPAR74C5,VPAR74C4,VPOPMEDI,VPOPNOIM,VPACQBEA,VPCRERIM,VPEURO19,VPCREUTI,VPCODCAR,VPCODFIS,VPDATPRE,VPKEYATT,VPDATINO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_VP__ANNO;
                  ,this.w_VPPERIOD;
                  ,this.w_VPIMPOR7;
                  ,this.w_VPIMPOR8;
                  ,this.w_VPIMPOR9;
                  ,this.w_VPCODATT;
                  ,this.w_VPCODVAL;
                  ,this.w_VPVARIMP;
                  ,this.w_VPCORTER;
                  ,this.w_VPDICGRU;
                  ,this.w_VPDICSOC;
                  ,this.w_VPIMPOR1;
                  ,this.w_VPCESINT;
                  ,this.w_VPIMPOR2;
                  ,this.w_VPACQINT;
                  ,this.w_VPIMPON3;
                  ,this.w_VPIMPOS3;
                  ,this.w_VPIMPOR5;
                  ,this.w_VPIMPOR6;
                  ,this.w_VPIMPO10;
                  ,this.w_VPIMPO11;
                  ,this.w_VPIMPO12;
                  ,this.w_VPIMPO15;
                  ,this.w_VPIMPO16;
                  ,this.w_VPIMPO13;
                  ,this.w_VPIMPO14;
                  ,this.w_VPIMPVER;
                  ,this.w_VPVEREUR;
                  ,this.w_VPDATVER;
                  ,this.w_VPCODAZI;
                  ,this.w_VPCODCAB;
                  ,this.w_VPVERNEF;
                  ,this.w_VPAR74C5;
                  ,this.w_VPAR74C4;
                  ,this.w_VPOPMEDI;
                  ,this.w_VPOPNOIM;
                  ,this.w_VPACQBEA;
                  ,this.w_VPCRERIM;
                  ,this.w_VPEURO19;
                  ,this.w_VPCREUTI;
                  ,this.w_VPCODCAR;
                  ,this.w_VPCODFIS;
                  ,this.w_VPDATPRE;
                  ,this.w_VPKEYATT;
                  ,this.w_VPDATINO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.IVA_PERI_IDX,i_nConn)
      *
      * update IVA_PERI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'IVA_PERI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " VPIMPOR7="+cp_ToStrODBC(this.w_VPIMPOR7)+;
             ",VPIMPOR8="+cp_ToStrODBC(this.w_VPIMPOR8)+;
             ",VPIMPOR9="+cp_ToStrODBC(this.w_VPIMPOR9)+;
             ",VPCODATT="+cp_ToStrODBCNull(this.w_VPCODATT)+;
             ",VPCODVAL="+cp_ToStrODBC(this.w_VPCODVAL)+;
             ",VPVARIMP="+cp_ToStrODBC(this.w_VPVARIMP)+;
             ",VPCORTER="+cp_ToStrODBC(this.w_VPCORTER)+;
             ",VPDICGRU="+cp_ToStrODBC(this.w_VPDICGRU)+;
             ",VPDICSOC="+cp_ToStrODBC(this.w_VPDICSOC)+;
             ",VPIMPOR1="+cp_ToStrODBC(this.w_VPIMPOR1)+;
             ",VPCESINT="+cp_ToStrODBC(this.w_VPCESINT)+;
             ",VPIMPOR2="+cp_ToStrODBC(this.w_VPIMPOR2)+;
             ",VPACQINT="+cp_ToStrODBC(this.w_VPACQINT)+;
             ",VPIMPON3="+cp_ToStrODBC(this.w_VPIMPON3)+;
             ",VPIMPOS3="+cp_ToStrODBC(this.w_VPIMPOS3)+;
             ",VPIMPOR5="+cp_ToStrODBC(this.w_VPIMPOR5)+;
             ",VPIMPOR6="+cp_ToStrODBC(this.w_VPIMPOR6)+;
             ",VPIMPO10="+cp_ToStrODBC(this.w_VPIMPO10)+;
             ",VPIMPO11="+cp_ToStrODBC(this.w_VPIMPO11)+;
             ",VPIMPO12="+cp_ToStrODBC(this.w_VPIMPO12)+;
             ",VPIMPO15="+cp_ToStrODBC(this.w_VPIMPO15)+;
             ",VPIMPO16="+cp_ToStrODBC(this.w_VPIMPO16)+;
             ",VPIMPO13="+cp_ToStrODBC(this.w_VPIMPO13)+;
             ",VPIMPO14="+cp_ToStrODBC(this.w_VPIMPO14)+;
             ",VPIMPVER="+cp_ToStrODBC(this.w_VPIMPVER)+;
             ",VPVEREUR="+cp_ToStrODBC(this.w_VPVEREUR)+;
             ",VPDATVER="+cp_ToStrODBC(this.w_VPDATVER)+;
             ",VPCODAZI="+cp_ToStrODBCNull(this.w_VPCODAZI)+;
             ",VPCODCAB="+cp_ToStrODBCNull(this.w_VPCODCAB)+;
             ",VPVERNEF="+cp_ToStrODBC(this.w_VPVERNEF)+;
             ",VPAR74C5="+cp_ToStrODBC(this.w_VPAR74C5)+;
             ",VPAR74C4="+cp_ToStrODBC(this.w_VPAR74C4)+;
             ",VPOPMEDI="+cp_ToStrODBC(this.w_VPOPMEDI)+;
             ",VPOPNOIM="+cp_ToStrODBC(this.w_VPOPNOIM)+;
             ",VPACQBEA="+cp_ToStrODBC(this.w_VPACQBEA)+;
             ",VPCRERIM="+cp_ToStrODBC(this.w_VPCRERIM)+;
             ",VPEURO19="+cp_ToStrODBC(this.w_VPEURO19)+;
             ",VPCREUTI="+cp_ToStrODBC(this.w_VPCREUTI)+;
             ",VPCODCAR="+cp_ToStrODBC(this.w_VPCODCAR)+;
             ",VPCODFIS="+cp_ToStrODBC(this.w_VPCODFIS)+;
             ",VPDATPRE="+cp_ToStrODBC(this.w_VPDATPRE)+;
             ",VPDATINO="+cp_ToStrODBC(this.w_VPDATINO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'IVA_PERI')
        i_cWhere = cp_PKFox(i_cTable  ,'VP__ANNO',this.w_VP__ANNO  ,'VPPERIOD',this.w_VPPERIOD  ,'VPKEYATT',this.w_VPKEYATT  )
        UPDATE (i_cTable) SET;
              VPIMPOR7=this.w_VPIMPOR7;
             ,VPIMPOR8=this.w_VPIMPOR8;
             ,VPIMPOR9=this.w_VPIMPOR9;
             ,VPCODATT=this.w_VPCODATT;
             ,VPCODVAL=this.w_VPCODVAL;
             ,VPVARIMP=this.w_VPVARIMP;
             ,VPCORTER=this.w_VPCORTER;
             ,VPDICGRU=this.w_VPDICGRU;
             ,VPDICSOC=this.w_VPDICSOC;
             ,VPIMPOR1=this.w_VPIMPOR1;
             ,VPCESINT=this.w_VPCESINT;
             ,VPIMPOR2=this.w_VPIMPOR2;
             ,VPACQINT=this.w_VPACQINT;
             ,VPIMPON3=this.w_VPIMPON3;
             ,VPIMPOS3=this.w_VPIMPOS3;
             ,VPIMPOR5=this.w_VPIMPOR5;
             ,VPIMPOR6=this.w_VPIMPOR6;
             ,VPIMPO10=this.w_VPIMPO10;
             ,VPIMPO11=this.w_VPIMPO11;
             ,VPIMPO12=this.w_VPIMPO12;
             ,VPIMPO15=this.w_VPIMPO15;
             ,VPIMPO16=this.w_VPIMPO16;
             ,VPIMPO13=this.w_VPIMPO13;
             ,VPIMPO14=this.w_VPIMPO14;
             ,VPIMPVER=this.w_VPIMPVER;
             ,VPVEREUR=this.w_VPVEREUR;
             ,VPDATVER=this.w_VPDATVER;
             ,VPCODAZI=this.w_VPCODAZI;
             ,VPCODCAB=this.w_VPCODCAB;
             ,VPVERNEF=this.w_VPVERNEF;
             ,VPAR74C5=this.w_VPAR74C5;
             ,VPAR74C4=this.w_VPAR74C4;
             ,VPOPMEDI=this.w_VPOPMEDI;
             ,VPOPNOIM=this.w_VPOPNOIM;
             ,VPACQBEA=this.w_VPACQBEA;
             ,VPCRERIM=this.w_VPCRERIM;
             ,VPEURO19=this.w_VPEURO19;
             ,VPCREUTI=this.w_VPCREUTI;
             ,VPCODCAR=this.w_VPCODCAR;
             ,VPCODFIS=this.w_VPCODFIS;
             ,VPDATPRE=this.w_VPDATPRE;
             ,VPDATINO=this.w_VPDATINO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.IVA_PERI_IDX,i_nConn)
      *
      * delete IVA_PERI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'VP__ANNO',this.w_VP__ANNO  ,'VPPERIOD',this.w_VPPERIOD  ,'VPKEYATT',this.w_VPKEYATT  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,8,.t.)
        if .o_IMPCRE7<>.w_IMPCRE7.or. .o_IMPDEB7<>.w_IMPDEB7
            .w_VPIMPOR7 = .w_IMPDEB7-.w_IMPCRE7
        endif
        if .o_IMPCRE8<>.w_IMPCRE8.or. .o_IMPDEB8<>.w_IMPDEB8
            .w_VPIMPOR8 = .w_IMPDEB8 - .w_IMPCRE8
        endif
        if .o_IMPCRE9<>.w_IMPCRE9.or. .o_IMPDEB9<>.w_IMPDEB9
            .w_VPIMPOR9 = .w_IMPDEB9 - .w_IMPCRE9
        endif
        .DoRTCalc(12,13,.t.)
            .w_DECTOT = IIF(VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO), 2, 0)
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(16,19,.t.)
        if .o_VPCODVAL<>.w_VPCODVAL
            .w_DIVIDO = IIF ( .w_VPCODVAL<> 'S' , 1000 , 1 )
        endif
        .DoRTCalc(21,28,.t.)
        if .o_VPIMPOR7<>.w_VPIMPOR7
            .w_IMPDEB7 = IIF( .w_VPIMPOR7>0 ,  .w_VPIMPOR7, 0)
        endif
        if .o_VPIMPOR7<>.w_VPIMPOR7
            .w_IMPCRE7 = IIF( .w_VPIMPOR7<0 ,  ABS(.w_VPIMPOR7) ,0 )
        endif
        if .o_VPIMPOR8<>.w_VPIMPOR8
            .w_IMPDEB8 = IIF( .w_VPIMPOR8>0 ,  .w_VPIMPOR8, 0)
        endif
        if .o_VPIMPOR8<>.w_VPIMPOR8
            .w_IMPCRE8 = IIF( .w_VPIMPOR8<0 ,  ABS(.w_VPIMPOR8) ,0 )
        endif
        if .o_VPIMPOR9<>.w_VPIMPOR9
            .w_IMPDEB9 = IIF( .w_VPIMPOR9>0 ,  .w_VPIMPOR9, 0)
        endif
        if .o_VPIMPOR9<>.w_VPIMPOR9
            .w_IMPCRE9 = IIF( .w_VPIMPOR9<0 ,  ABS(.w_VPIMPOR9) ,0 )
        endif
        if .o_IMPCRE10<>.w_IMPCRE10.or. .o_IMPDEB10<>.w_IMPDEB10
            .w_VPIMPO10 = .w_IMPDEB10 - .w_IMPCRE10
        endif
        if .o_VPIMPO10<>.w_VPIMPO10
            .w_IMPDEB10 = IIF( .w_VPIMPO10>0,  .w_VPIMPO10 ,0)
        endif
        if .o_VPIMPOR8<>.w_VPIMPOR8
            .w_IMPCRE10 = IIF( .w_VPIMPO10<0 ,  ABS(.w_VPIMPO10) ,0)
        endif
        .DoRTCalc(38,38,.t.)
        if .o_IMPCRE12<>.w_IMPCRE12.or. .o_IMPDEB12<>.w_IMPDEB12
            .w_VPIMPO12 = .w_IMPDEB12 - .w_IMPCRE12
        endif
        if .o_VPIMPO12<>.w_VPIMPO12
            .w_IMPDEB12 = IIF(.w_VPIMPO12>0, .w_VPIMPO12 , 0 )
        endif
        if .o_VPIMPO12<>.w_VPIMPO12
            .w_IMPCRE12 = IIF(.w_VPIMPO12<0, ABS(.w_VPIMPO12) , 0 )
        endif
        .DoRTCalc(42,42,.t.)
        if .o_VPIMPO12<>.w_VPIMPO12.or. .o_VPIMPO15<>.w_VPIMPO15.or. .o_VPIMPO14<>.w_VPIMPO14.or. .o_VPIMPO13<>.w_VPIMPO13
            .w_VPIMPO16 = IIF((.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15)<0, 0, (.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15))
        endif
        .DoRTCalc(44,44,.t.)
        if .o_VPIMPO12<>.w_VPIMPO12.or. .o_VPIMPO13<>.w_VPIMPO13.or. .o_IMPDEB10<>.w_IMPDEB10
            .w_VPIMPO14 = IIF(g_TIPDEN='T' And (.w_VPIMPO12-(.w_VPIMPO13+.w_IMPDEB10))>0 And NOT (g_TIPDEN='T'And .w_VPPERIOD=4), cp_Round(Abs(.w_VPIMPO12-(.w_VPIMPO13+.w_IMPDEB10))*.w_MAGTRI/100, 0), 0)
        endif
        .DoRTCalc(46,46,.t.)
            .w_VPVEREUR = IIF(.w_VPCODVAL='S', 'S', ' ')
        .DoRTCalc(48,57,.t.)
            .w_VPEURO19 = IIF(.w_VPCODVAL='S', 'S', 'N')
        .DoRTCalc(59,62,.t.)
            .w_VPKEYATT = IIF(EMPTY(.w_VPCODATT), '#####', .w_VPCODATT)
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(64,69,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oVPCODATT_1_13.enabled = this.oPgFrm.Page1.oPag.oVPCODATT_1_13.mCond()
    this.oPgFrm.Page1.oPag.oIMPDEB7_1_38.enabled = this.oPgFrm.Page1.oPag.oIMPDEB7_1_38.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE7_1_39.enabled = this.oPgFrm.Page1.oPag.oIMPCRE7_1_39.mCond()
    this.oPgFrm.Page1.oPag.oIMPDEB8_1_40.enabled = this.oPgFrm.Page1.oPag.oIMPDEB8_1_40.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE8_1_41.enabled = this.oPgFrm.Page1.oPag.oIMPCRE8_1_41.mCond()
    this.oPgFrm.Page1.oPag.oIMPDEB9_1_42.enabled = this.oPgFrm.Page1.oPag.oIMPDEB9_1_42.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE9_1_43.enabled = this.oPgFrm.Page1.oPag.oIMPCRE9_1_43.mCond()
    this.oPgFrm.Page1.oPag.oIMPDEB10_1_56.enabled = this.oPgFrm.Page1.oPag.oIMPDEB10_1_56.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE10_1_57.enabled = this.oPgFrm.Page1.oPag.oIMPCRE10_1_57.mCond()
    this.oPgFrm.Page1.oPag.oIMPDEB12_1_67.enabled = this.oPgFrm.Page1.oPag.oIMPDEB12_1_67.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE12_1_68.enabled = this.oPgFrm.Page1.oPag.oIMPCRE12_1_68.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPO15_1_71.enabled = this.oPgFrm.Page1.oPag.oVPIMPO15_1_71.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPO13_1_105.enabled = this.oPgFrm.Page1.oPag.oVPIMPO13_1_105.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPO14_1_106.enabled = this.oPgFrm.Page1.oPag.oVPIMPO14_1_106.mCond()
    this.oPgFrm.Page2.oPag.oVPIMPVER_2_1.enabled = this.oPgFrm.Page2.oPag.oVPIMPVER_2_1.mCond()
    this.oPgFrm.Page2.oPag.oVPCRERIM_2_14.enabled = this.oPgFrm.Page2.oPag.oVPCRERIM_2_14.mCond()
    this.oPgFrm.Page2.oPag.oVPCREUTI_2_17.enabled = this.oPgFrm.Page2.oPag.oVPCREUTI_2_17.mCond()
    this.oPgFrm.Page2.oPag.oVPDATINO_2_37.enabled = this.oPgFrm.Page2.oPag.oVPDATINO_2_37.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_74.visible=!this.oPgFrm.Page1.oPag.oStr_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_78.visible=!this.oPgFrm.Page1.oPag.oStr_1_78.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_79.visible=!this.oPgFrm.Page1.oPag.oStr_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_81.visible=!this.oPgFrm.Page1.oPag.oStr_1_81.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_82.visible=!this.oPgFrm.Page1.oPag.oStr_1_82.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_86.visible=!this.oPgFrm.Page1.oPag.oStr_1_86.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_95.visible=!this.oPgFrm.Page1.oPag.oStr_1_95.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_98.visible=!this.oPgFrm.Page1.oPag.oStr_1_98.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_103.visible=!this.oPgFrm.Page1.oPag.oStr_1_103.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_104.visible=!this.oPgFrm.Page1.oPag.oStr_1_104.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_10.visible=!this.oPgFrm.Page2.oPag.oStr_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_26.visible=!this.oPgFrm.Page2.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_27.visible=!this.oPgFrm.Page2.oPag.oStr_2_27.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_28.visible=!this.oPgFrm.Page2.oPag.oStr_2_28.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_34.visible=!this.oPgFrm.Page2.oPag.oStr_2_34.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_40.visible=!this.oPgFrm.Page2.oPag.oStr_2_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_42.visible=!this.oPgFrm.Page2.oPag.oStr_2_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_111.visible=!this.oPgFrm.Page1.oPag.oStr_1_111.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_112.visible=!this.oPgFrm.Page1.oPag.oStr_1_112.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_110.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZDENMAG";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZDENMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_MAGTRI = NVL(_Link_.AZDENMAG,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_MAGTRI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODATT
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_VPCODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_VPCODATT))
          select ATCODATT,ATDESATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIMAST','*','ATCODATT',cp_AbsName(oSource.parent,'oVPCODATT_1_13'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_VPCODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_VPCODATT)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_DESATT = NVL(_Link_.ATDESATT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODATT = space(5)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ATTIMAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.ATCODATT as ATCODATT113"+ ",link_1_13.ATDESATT as ATDESATT113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on IVA_PERI.VPCODATT=link_1_13.ATCODATT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and IVA_PERI.VPCODATT=link_1_13.ATCODATT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VPCODAZI
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABI',True,'COD_ABI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ABCODABI like "+cp_ToStrODBC(trim(this.w_VPCODAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ABCODABI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ABCODABI',trim(this.w_VPCODAZI))
          select ABCODABI,ABDESABI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ABCODABI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPCODAZI)==trim(_Link_.ABCODABI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPCODAZI) and !this.bDontReportError
            deferred_cp_zoom('COD_ABI','*','ABCODABI',cp_AbsName(oSource.parent,'oVPCODAZI_2_4'),i_cWhere,'GSAR_ABI',"Codici ABI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                     +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',oSource.xKey(1))
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_VPCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_VPCODAZI)
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODAZI = NVL(_Link_.ABCODABI,space(5))
      this.w_DESABI = NVL(_Link_.ABDESABI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODAZI = space(5)
      endif
      this.w_DESABI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_ABI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.ABCODABI as ABCODABI204"+ ",link_2_4.ABDESABI as ABDESABI204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on IVA_PERI.VPCODAZI=link_2_4.ABCODABI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and IVA_PERI.VPCODAZI=link_2_4.ABCODABI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VPCODCAB
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_lTable = "COD_CAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2], .t., this.COD_CAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODCAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFI',True,'COD_CAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FICODCAB like "+cp_ToStrODBC(trim(this.w_VPCODCAB)+"%");

          i_ret=cp_SQL(i_nConn,"select FICODCAB,FIDESFIL,FIINDIRI,FI___CAP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FICODCAB","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FICODCAB',trim(this.w_VPCODCAB))
          select FICODCAB,FIDESFIL,FIINDIRI,FI___CAP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FICODCAB into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPCODCAB)==trim(_Link_.FICODCAB) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPCODCAB) and !this.bDontReportError
            deferred_cp_zoom('COD_CAB','*','FICODCAB',cp_AbsName(oSource.parent,'oVPCODCAB_2_5'),i_cWhere,'GSAR_AFI',"Codici CAB",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODCAB,FIDESFIL,FIINDIRI,FI___CAP";
                     +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODCAB',oSource.xKey(1))
            select FICODCAB,FIDESFIL,FIINDIRI,FI___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODCAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODCAB,FIDESFIL,FIINDIRI,FI___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(this.w_VPCODCAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODCAB',this.w_VPCODCAB)
            select FICODCAB,FIDESFIL,FIINDIRI,FI___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODCAB = NVL(_Link_.FICODCAB,space(5))
      this.w_DESFIL = NVL(_Link_.FIDESFIL,space(40))
      this.w_INDIRI = NVL(_Link_.FIINDIRI,space(50))
      this.w_CAP = NVL(_Link_.FI___CAP,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODCAB = space(5)
      endif
      this.w_DESFIL = space(40)
      this.w_INDIRI = space(50)
      this.w_CAP = space(8)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])+'\'+cp_ToStr(_Link_.FICODCAB,1)
      cp_ShowWarn(i_cKey,this.COD_CAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODCAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVP__ANNO_1_2.value==this.w_VP__ANNO)
      this.oPgFrm.Page1.oPag.oVP__ANNO_1_2.value=this.w_VP__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oVPPERIOD_1_3.value==this.w_VPPERIOD)
      this.oPgFrm.Page1.oPag.oVPPERIOD_1_3.value=this.w_VPPERIOD
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCODATT_1_13.value==this.w_VPCODATT)
      this.oPgFrm.Page1.oPag.oVPCODATT_1_13.value=this.w_VPCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCODVAL_1_14.RadioValue()==this.w_VPCODVAL)
      this.oPgFrm.Page1.oPag.oVPCODVAL_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVPVARIMP_1_17.RadioValue()==this.w_VPVARIMP)
      this.oPgFrm.Page1.oPag.oVPVARIMP_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCORTER_1_18.RadioValue()==this.w_VPCORTER)
      this.oPgFrm.Page1.oPag.oVPCORTER_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVPDICGRU_1_19.RadioValue()==this.w_VPDICGRU)
      this.oPgFrm.Page1.oPag.oVPDICGRU_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVPDICSOC_1_20.RadioValue()==this.w_VPDICSOC)
      this.oPgFrm.Page1.oPag.oVPDICSOC_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPOR1_1_24.value==this.w_VPIMPOR1)
      this.oPgFrm.Page1.oPag.oVPIMPOR1_1_24.value=this.w_VPIMPOR1
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCESINT_1_25.value==this.w_VPCESINT)
      this.oPgFrm.Page1.oPag.oVPCESINT_1_25.value=this.w_VPCESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPOR2_1_27.value==this.w_VPIMPOR2)
      this.oPgFrm.Page1.oPag.oVPIMPOR2_1_27.value=this.w_VPIMPOR2
    endif
    if not(this.oPgFrm.Page1.oPag.oVPACQINT_1_30.value==this.w_VPACQINT)
      this.oPgFrm.Page1.oPag.oVPACQINT_1_30.value=this.w_VPACQINT
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPON3_1_32.value==this.w_VPIMPON3)
      this.oPgFrm.Page1.oPag.oVPIMPON3_1_32.value=this.w_VPIMPON3
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPOS3_1_34.value==this.w_VPIMPOS3)
      this.oPgFrm.Page1.oPag.oVPIMPOS3_1_34.value=this.w_VPIMPOS3
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPOR5_1_36.value==this.w_VPIMPOR5)
      this.oPgFrm.Page1.oPag.oVPIMPOR5_1_36.value=this.w_VPIMPOR5
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPOR6_1_37.value==this.w_VPIMPOR6)
      this.oPgFrm.Page1.oPag.oVPIMPOR6_1_37.value=this.w_VPIMPOR6
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB7_1_38.value==this.w_IMPDEB7)
      this.oPgFrm.Page1.oPag.oIMPDEB7_1_38.value=this.w_IMPDEB7
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE7_1_39.value==this.w_IMPCRE7)
      this.oPgFrm.Page1.oPag.oIMPCRE7_1_39.value=this.w_IMPCRE7
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB8_1_40.value==this.w_IMPDEB8)
      this.oPgFrm.Page1.oPag.oIMPDEB8_1_40.value=this.w_IMPDEB8
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE8_1_41.value==this.w_IMPCRE8)
      this.oPgFrm.Page1.oPag.oIMPCRE8_1_41.value=this.w_IMPCRE8
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB9_1_42.value==this.w_IMPDEB9)
      this.oPgFrm.Page1.oPag.oIMPDEB9_1_42.value=this.w_IMPDEB9
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE9_1_43.value==this.w_IMPCRE9)
      this.oPgFrm.Page1.oPag.oIMPCRE9_1_43.value=this.w_IMPCRE9
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB10_1_56.value==this.w_IMPDEB10)
      this.oPgFrm.Page1.oPag.oIMPDEB10_1_56.value=this.w_IMPDEB10
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE10_1_57.value==this.w_IMPCRE10)
      this.oPgFrm.Page1.oPag.oIMPCRE10_1_57.value=this.w_IMPCRE10
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPO11_1_65.value==this.w_VPIMPO11)
      this.oPgFrm.Page1.oPag.oVPIMPO11_1_65.value=this.w_VPIMPO11
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB12_1_67.value==this.w_IMPDEB12)
      this.oPgFrm.Page1.oPag.oIMPDEB12_1_67.value=this.w_IMPDEB12
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE12_1_68.value==this.w_IMPCRE12)
      this.oPgFrm.Page1.oPag.oIMPCRE12_1_68.value=this.w_IMPCRE12
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPO15_1_71.value==this.w_VPIMPO15)
      this.oPgFrm.Page1.oPag.oVPIMPO15_1_71.value=this.w_VPIMPO15
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPO16_1_76.value==this.w_VPIMPO16)
      this.oPgFrm.Page1.oPag.oVPIMPO16_1_76.value=this.w_VPIMPO16
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPO13_1_105.value==this.w_VPIMPO13)
      this.oPgFrm.Page1.oPag.oVPIMPO13_1_105.value=this.w_VPIMPO13
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPO14_1_106.value==this.w_VPIMPO14)
      this.oPgFrm.Page1.oPag.oVPIMPO14_1_106.value=this.w_VPIMPO14
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPVER_2_1.value==this.w_VPIMPVER)
      this.oPgFrm.Page2.oPag.oVPIMPVER_2_1.value=this.w_VPIMPVER
    endif
    if not(this.oPgFrm.Page2.oPag.oVPVEREUR_2_2.RadioValue()==this.w_VPVEREUR)
      this.oPgFrm.Page2.oPag.oVPVEREUR_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPDATVER_2_3.value==this.w_VPDATVER)
      this.oPgFrm.Page2.oPag.oVPDATVER_2_3.value=this.w_VPDATVER
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCODAZI_2_4.value==this.w_VPCODAZI)
      this.oPgFrm.Page2.oPag.oVPCODAZI_2_4.value=this.w_VPCODAZI
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCODCAB_2_5.value==this.w_VPCODCAB)
      this.oPgFrm.Page2.oPag.oVPCODCAB_2_5.value=this.w_VPCODCAB
    endif
    if not(this.oPgFrm.Page2.oPag.oVPVERNEF_2_6.RadioValue()==this.w_VPVERNEF)
      this.oPgFrm.Page2.oPag.oVPVERNEF_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPAR74C5_2_7.RadioValue()==this.w_VPAR74C5)
      this.oPgFrm.Page2.oPag.oVPAR74C5_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPAR74C4_2_8.RadioValue()==this.w_VPAR74C4)
      this.oPgFrm.Page2.oPag.oVPAR74C4_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPOPMEDI_2_11.RadioValue()==this.w_VPOPMEDI)
      this.oPgFrm.Page2.oPag.oVPOPMEDI_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPOPNOIM_2_12.RadioValue()==this.w_VPOPNOIM)
      this.oPgFrm.Page2.oPag.oVPOPNOIM_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPACQBEA_2_13.RadioValue()==this.w_VPACQBEA)
      this.oPgFrm.Page2.oPag.oVPACQBEA_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCRERIM_2_14.value==this.w_VPCRERIM)
      this.oPgFrm.Page2.oPag.oVPCRERIM_2_14.value=this.w_VPCRERIM
    endif
    if not(this.oPgFrm.Page2.oPag.oVPEURO19_2_15.RadioValue()==this.w_VPEURO19)
      this.oPgFrm.Page2.oPag.oVPEURO19_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCREUTI_2_17.value==this.w_VPCREUTI)
      this.oPgFrm.Page2.oPag.oVPCREUTI_2_17.value=this.w_VPCREUTI
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCODCAR_2_19.RadioValue()==this.w_VPCODCAR)
      this.oPgFrm.Page2.oPag.oVPCODCAR_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCODFIS_2_22.value==this.w_VPCODFIS)
      this.oPgFrm.Page2.oPag.oVPCODFIS_2_22.value=this.w_VPCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oVPDATPRE_2_23.value==this.w_VPDATPRE)
      this.oPgFrm.Page2.oPag.oVPDATPRE_2_23.value=this.w_VPDATPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_109.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_109.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page2.oPag.oVPDATINO_2_37.value==this.w_VPDATINO)
      this.oPgFrm.Page2.oPag.oVPDATINO_2_37.value=this.w_VPDATINO
    endif
    cp_SetControlsValueExtFlds(this,'IVA_PERI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_VP__ANNO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVP__ANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_VP__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_VPPERIOD)) or not(.w_VPPERIOD>0 And .w_VPPERIOD<=IIF(g_TIPDEN='T',4,12)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPPERIOD_1_3.SetFocus()
            i_bnoObbl = !empty(.w_VPPERIOD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Periodo non congruente")
          case   (empty(.w_VPCODVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPCODVAL_1_14.SetFocus()
            i_bnoObbl = !empty(.w_VPCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VPIMPO13>=0 And .w_VPIMPO13<=.w_IMPDEB12)  and (.w_IMPDEB12>0 And Not (g_TIPDEN='T' And .w_VPPERIOD=4) And .w_VPDICSOC<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPIMPO13_1_105.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il credito non pu� superare il debito per il periodo")
          case   not(.w_VPIMPO14>=0)  and (g_TIPDEN='T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPIMPO14_1_106.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gli interessi per le liquidazioni trimestrali non possono essere di segno negativo")
          case   (empty(.w_VPEURO19))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVPEURO19_2_15.SetFocus()
            i_bnoObbl = !empty(.w_VPEURO19)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VPIMPOR7 = this.w_VPIMPOR7
    this.o_VPIMPOR8 = this.w_VPIMPOR8
    this.o_VPIMPOR9 = this.w_VPIMPOR9
    this.o_VPCODVAL = this.w_VPCODVAL
    this.o_IMPDEB7 = this.w_IMPDEB7
    this.o_IMPCRE7 = this.w_IMPCRE7
    this.o_IMPDEB8 = this.w_IMPDEB8
    this.o_IMPCRE8 = this.w_IMPCRE8
    this.o_IMPDEB9 = this.w_IMPDEB9
    this.o_IMPCRE9 = this.w_IMPCRE9
    this.o_VPIMPO10 = this.w_VPIMPO10
    this.o_IMPDEB10 = this.w_IMPDEB10
    this.o_IMPCRE10 = this.w_IMPCRE10
    this.o_VPIMPO12 = this.w_VPIMPO12
    this.o_IMPDEB12 = this.w_IMPDEB12
    this.o_IMPCRE12 = this.w_IMPCRE12
    this.o_VPIMPO15 = this.w_VPIMPO15
    this.o_VPIMPO13 = this.w_VPIMPO13
    this.o_VPIMPO14 = this.w_VPIMPO14
    return

enddefine

* --- Define pages as container
define class tgsar_avpPag1 as StdContainer
  Width  = 679
  height = 399
  stdWidth  = 679
  stdheight = 399
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVP__ANNO_1_2 as StdField with uid="VRGNRUYAQA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_VP__ANNO", cQueryName = "VP__ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno solare di competenza",;
    HelpContextID = 18100571,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=44, Left=55, Top=7, InputMask=replicate('X',4)

  add object oVPPERIOD_1_3 as StdField with uid="VUENUBPFFC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VPPERIOD", cQueryName = "VP__ANNO,VPPERIOD",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Periodo non congruente",;
    ToolTipText = "Periodo liquidazione IVA",;
    HelpContextID = 85926246,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=30, Left=173, Top=7, cSayPict='"99"', cGetPict='"99"'

  func oVPPERIOD_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPPERIOD>0 And .w_VPPERIOD<=IIF(g_TIPDEN='T',4,12))
    endwith
    return bRes
  endfunc

  add object oVPCODATT_1_13 as StdField with uid="HANFLIHFPY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_VPCODATT", cQueryName = "VPCODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 34213546,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=267, Top=7, InputMask=replicate('X',5), cLinkFile="ATTIMAST", oKey_1_1="ATCODATT", oKey_1_2="this.w_VPCODATT"

  func oVPCODATT_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ATTIVI='S')
    endwith
   endif
  endfunc

  func oVPCODATT_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPCODATT_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oVPCODVAL_1_14 as StdCombo with uid="FSXTWSZIER",rtseq=13,rtrep=.f.,left=577,top=8,width=99,height=21;
    , ToolTipText = "Valuta della liquidazione";
    , HelpContextID = 150335838;
    , cFormVar="w_VPCODVAL",RowSource=""+"Valuta nazionale,"+"Valuta di conto", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oVPCODVAL_1_14.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oVPCODVAL_1_14.GetRadio()
    this.Parent.oContained.w_VPCODVAL = this.RadioValue()
    return .t.
  endfunc

  func oVPCODVAL_1_14.SetRadio()
    this.Parent.oContained.w_VPCODVAL=trim(this.Parent.oContained.w_VPCODVAL)
    this.value = ;
      iif(this.Parent.oContained.w_VPCODVAL=='N',1,;
      iif(this.Parent.oContained.w_VPCODVAL=='S',2,;
      0))
  endfunc

  add object oVPVARIMP_1_17 as StdCheck with uid="UTKNLDOZQB",rtseq=16,rtrep=.f.,left=55, top=33, caption="Variazioni di imponibile",;
    ToolTipText = "Variazioni di imponibile",;
    HelpContextID = 86163802,;
    cFormVar="w_VPVARIMP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVPVARIMP_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPVARIMP_1_17.GetRadio()
    this.Parent.oContained.w_VPVARIMP = this.RadioValue()
    return .t.
  endfunc

  func oVPVARIMP_1_17.SetRadio()
    this.Parent.oContained.w_VPVARIMP=trim(this.Parent.oContained.w_VPVARIMP)
    this.value = ;
      iif(this.Parent.oContained.w_VPVARIMP=='S',1,;
      0)
  endfunc

  add object oVPCORTER_1_18 as StdCheck with uid="ZZLIHFXEDA",rtseq=17,rtrep=.f.,left=55, top=51, caption="Correttiva nei termini",;
    ToolTipText = "Variazioni di imponibile",;
    HelpContextID = 99225256,;
    cFormVar="w_VPCORTER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVPCORTER_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPCORTER_1_18.GetRadio()
    this.Parent.oContained.w_VPCORTER = this.RadioValue()
    return .t.
  endfunc

  func oVPCORTER_1_18.SetRadio()
    this.Parent.oContained.w_VPCORTER=trim(this.Parent.oContained.w_VPCORTER)
    this.value = ;
      iif(this.Parent.oContained.w_VPCORTER=='S',1,;
      0)
  endfunc

  add object oVPDICGRU_1_19 as StdCheck with uid="SLFWCLEHJX",rtseq=18,rtrep=.f.,left=336, top=33, caption="Dichiarazione gruppo",;
    ToolTipText = "Dichiarazione di gruppo",;
    HelpContextID = 134996309,;
    cFormVar="w_VPDICGRU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVPDICGRU_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPDICGRU_1_19.GetRadio()
    this.Parent.oContained.w_VPDICGRU = this.RadioValue()
    return .t.
  endfunc

  func oVPDICGRU_1_19.SetRadio()
    this.Parent.oContained.w_VPDICGRU=trim(this.Parent.oContained.w_VPDICGRU)
    this.value = ;
      iif(this.Parent.oContained.w_VPDICGRU=='S',1,;
      0)
  endfunc

  add object oVPDICSOC_1_20 as StdCheck with uid="AKAARBVJVE",rtseq=19,rtrep=.f.,left=517, top=33, caption="Societ� del gruppo",;
    ToolTipText = "Dichiarazione societ� aderente al gruppo",;
    HelpContextID = 202105191,;
    cFormVar="w_VPDICSOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVPDICSOC_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPDICSOC_1_20.GetRadio()
    this.Parent.oContained.w_VPDICSOC = this.RadioValue()
    return .t.
  endfunc

  func oVPDICSOC_1_20.SetRadio()
    this.Parent.oContained.w_VPDICSOC=trim(this.Parent.oContained.w_VPDICSOC)
    this.value = ;
      iif(this.Parent.oContained.w_VPDICSOC=='S',1,;
      0)
  endfunc

  add object oVPIMPOR1_1_24 as StdField with uid="VPUOSADYZX",rtseq=21,rtrep=.f.,;
    cFormVar = "w_VPIMPOR1", cQueryName = "VPIMPOR1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 255299961,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=193, Top=77, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oVPCESINT_1_25 as StdField with uid="FNJJMIHOML",rtseq=22,rtrep=.f.,;
    cFormVar = "w_VPCESINT", cQueryName = "VPCESINT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 84930902,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=77, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oVPIMPOR2_1_27 as StdField with uid="UKAPSLQUVL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_VPIMPOR2", cQueryName = "VPIMPOR2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 255299960,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=193, Top=98, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oVPACQINT_1_30 as StdField with uid="ICXNCRNHGZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_VPACQINT", cQueryName = "VPACQINT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 87167318,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=98, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oVPIMPON3_1_32 as StdField with uid="LDPAHIQMHV",rtseq=25,rtrep=.f.,;
    cFormVar = "w_VPIMPON3", cQueryName = "VPIMPON3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 255299959,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=193, Top=119, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oVPIMPOS3_1_34 as StdField with uid="GLWXEOJFTC",rtseq=26,rtrep=.f.,;
    cFormVar = "w_VPIMPOS3", cQueryName = "VPIMPOS3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 13135497,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=119, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oVPIMPOR5_1_36 as StdField with uid="YWROBMKFLC",rtseq=27,rtrep=.f.,;
    cFormVar = "w_VPIMPOR5", cQueryName = "VPIMPOR5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 255299957,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=193, Top=139, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oVPIMPOR6_1_37 as StdField with uid="LQMLOTPKHL",rtseq=28,rtrep=.f.,;
    cFormVar = "w_VPIMPOR6", cQueryName = "VPIMPOR6",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 255299956,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=161, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oIMPDEB7_1_38 as StdField with uid="OCGXBLERTT",rtseq=29,rtrep=.f.,;
    cFormVar = "w_IMPDEB7", cQueryName = "IMPDEB7",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 51370630,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=193, Top=182, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oIMPDEB7_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPCRE7=0)
    endwith
   endif
  endfunc

  add object oIMPCRE7_1_39 as StdField with uid="STQEOKHJUB",rtseq=30,rtrep=.f.,;
    cFormVar = "w_IMPCRE7", cQueryName = "IMPCRE7",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 115268230,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=182, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oIMPCRE7_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB7=0)
    endwith
   endif
  endfunc

  add object oIMPDEB8_1_40 as StdField with uid="BZTOQTAVMK",rtseq=31,rtrep=.f.,;
    cFormVar = "w_IMPDEB8", cQueryName = "IMPDEB8",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 51370630,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=193, Top=203, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oIMPDEB8_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPCRE8=0)
    endwith
   endif
  endfunc

  add object oIMPCRE8_1_41 as StdField with uid="TUNIUJUTCE",rtseq=32,rtrep=.f.,;
    cFormVar = "w_IMPCRE8", cQueryName = "IMPCRE8",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 115268230,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=203, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oIMPCRE8_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB8=0)
    endwith
   endif
  endfunc

  add object oIMPDEB9_1_42 as StdField with uid="UWBBPVJXOB",rtseq=33,rtrep=.f.,;
    cFormVar = "w_IMPDEB9", cQueryName = "IMPDEB9",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 51370630,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=193, Top=224, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oIMPDEB9_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPCRE9=0)
    endwith
   endif
  endfunc

  add object oIMPCRE9_1_43 as StdField with uid="PXABWHHJHP",rtseq=34,rtrep=.f.,;
    cFormVar = "w_IMPCRE9", cQueryName = "IMPCRE9",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 115268230,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=495, Top=224, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oIMPCRE9_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB9=0)
    endwith
   endif
  endfunc

  add object oIMPDEB10_1_56 as StdField with uid="KMZJLHMUAY",rtseq=36,rtrep=.f.,;
    cFormVar = "w_IMPDEB10", cQueryName = "IMPDEB10",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 217064778,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=193, Top=245, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oIMPDEB10_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_IMPCRE10=0)
    endwith
   endif
  endfunc

  add object oIMPCRE10_1_57 as StdField with uid="TPLGUKEXER",rtseq=37,rtrep=.f.,;
    cFormVar = "w_IMPCRE10", cQueryName = "IMPCRE10",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 153167178,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=245, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oIMPCRE10_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_IMPDEB10=0)
    endwith
   endif
  endfunc

  add object oVPIMPO11_1_65 as StdField with uid="NPTMYCJJQG",rtseq=38,rtrep=.f.,;
    cFormVar = "w_VPIMPO11", cQueryName = "VPIMPO11",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 255299961,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=266, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oIMPDEB12_1_67 as StdField with uid="SKWZYJBXJU",rtseq=40,rtrep=.f.,;
    cFormVar = "w_IMPDEB12", cQueryName = "IMPDEB12",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 217064776,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=193, Top=287, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oIMPDEB12_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPCRE12=0)
    endwith
   endif
  endfunc

  add object oIMPCRE12_1_68 as StdField with uid="GTMKADDXDK",rtseq=41,rtrep=.f.,;
    cFormVar = "w_IMPCRE12", cQueryName = "IMPCRE12",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 153167176,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=287, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oIMPCRE12_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB12=0)
    endwith
   endif
  endfunc

  add object oVPIMPO15_1_71 as StdField with uid="QYTUWMYSCN",rtseq=42,rtrep=.f.,;
    cFormVar = "w_VPIMPO15", cQueryName = "VPIMPO15",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 255299957,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=347, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oVPIMPO15_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  add object oVPIMPO16_1_76 as StdField with uid="ERGJBQWAOD",rtseq=43,rtrep=.f.,;
    cFormVar = "w_VPIMPO16", cQueryName = "VPIMPO16",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 255299956,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=191, Top=377, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oVPIMPO13_1_105 as StdField with uid="SNRTIQKGLZ",rtseq=44,rtrep=.f.,;
    cFormVar = "w_VPIMPO13", cQueryName = "VPIMPO13",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il credito non pu� superare il debito per il periodo",;
    ToolTipText = "Valore riferito ai crediti d'imposta utilizzati ("+g_perval+")",;
    HelpContextID = 255299959,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=494, Top=308, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oVPIMPO13_1_105.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB12>0 And Not (g_TIPDEN='T' And .w_VPPERIOD=4) And .w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  func oVPIMPO13_1_105.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPIMPO13>=0 And .w_VPIMPO13<=.w_IMPDEB12)
    endwith
    return bRes
  endfunc

  add object oVPIMPO14_1_106 as StdField with uid="XSEFOYKZQB",rtseq=45,rtrep=.f.,;
    cFormVar = "w_VPIMPO14", cQueryName = "VPIMPO14",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Gli interessi per le liquidazioni trimestrali non possono essere di segno negativo",;
    ToolTipText = "Interessi dovuti per liquidazioni trimestrali",;
    HelpContextID = 255299958,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=193, Top=328, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oVPIMPO14_1_106.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TIPDEN='T')
    endwith
   endif
  endfunc

  func oVPIMPO14_1_106.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPIMPO14>=0)
    endwith
    return bRes
  endfunc

  add object oDESATT_1_109 as StdField with uid="QJHBUVIQMT",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 167968202,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=334, Top=7, InputMask=replicate('X',35)


  add object oObj_1_110 as cp_runprogram with uid="BXFXXDHTNS",left=90, top=420, width=232,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BVP",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 149778458

  add object oStr_1_12 as StdString with uid="EOECVDJAWA",Visible=.t., Left=2, Top=9,;
    Alignment=1, Width=51, Height=15,;
    Caption="Anno:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="HNUWLFTPDW",Visible=.t., Left=102, Top=9,;
    Alignment=1, Width=66, Height=15,;
    Caption="Periodo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="GFDCXWYNPH",Visible=.t., Left=517, Top=8,;
    Alignment=1, Width=59, Height=18,;
    Caption="Importi in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="VREZYRCDYJ",Visible=.t., Left=46, Top=77,;
    Alignment=1, Width=144, Height=22,;
    Caption="Op. attive:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="JKJXVCFDPN",Visible=.t., Left=46, Top=98,;
    Alignment=1, Width=144, Height=22,;
    Caption="Op. passive:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="IFRYZJDXWZ",Visible=.t., Left=372, Top=77,;
    Alignment=1, Width=121, Height=22,;
    Caption="di cui cessioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="ORPJWCOGPO",Visible=.t., Left=383, Top=98,;
    Alignment=1, Width=110, Height=22,;
    Caption="di cui acquisti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="OQXJKGLMUP",Visible=.t., Left=46, Top=119,;
    Alignment=1, Width=144, Height=22,;
    Caption="Imponibile oro/argento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="JZZGGWHLGH",Visible=.t., Left=383, Top=119,;
    Alignment=1, Width=110, Height=22,;
    Caption="Imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="XJNMOQDHNF",Visible=.t., Left=46, Top=139,;
    Alignment=1, Width=144, Height=22,;
    Caption="IVA esigibile periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="LHVJCRUBFE",Visible=.t., Left=348, Top=161,;
    Alignment=1, Width=142, Height=22,;
    Caption="IVA detratta periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="GFGOQTSLQI",Visible=.t., Left=46, Top=182,;
    Alignment=1, Width=144, Height=22,;
    Caption="IVA periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="KQUYBMCSTF",Visible=.t., Left=46, Top=203,;
    Alignment=1, Width=144, Height=22,;
    Caption="Variazioni d'imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="XWSXCFYWWB",Visible=.t., Left=16, Top=77,;
    Alignment=0, Width=23, Height=22,;
    Caption="VP1"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="PUJXRWDQAH",Visible=.t., Left=16, Top=98,;
    Alignment=0, Width=23, Height=22,;
    Caption="VP2"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="GANUWUNDTK",Visible=.t., Left=14, Top=119,;
    Alignment=0, Width=25, Height=22,;
    Caption="VP3"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="FSHQWWBAIC",Visible=.t., Left=9, Top=139,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP10"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="ZYWXPHHFGV",Visible=.t., Left=9, Top=161,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP11"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="QSUDXECTBZ",Visible=.t., Left=9, Top=182,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP12"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="YHGLTWTLWG",Visible=.t., Left=9, Top=203,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP13"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="NEIXOVCQPM",Visible=.t., Left=46, Top=224,;
    Alignment=1, Width=144, Height=22,;
    Caption="IVA non versata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="XNUIGIHGAX",Visible=.t., Left=46, Top=245,;
    Alignment=1, Width=144, Height=22,;
    Caption="Debito / credito rip.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="PKSRQCOTKE",Visible=.t., Left=383, Top=266,;
    Alignment=1, Width=110, Height=22,;
    Caption="Credito IVA comp.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="FKGDYKTDDV",Visible=.t., Left=46, Top=287,;
    Alignment=1, Width=144, Height=22,;
    Caption="IVA dovuta / credito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="DKCKIDKHVP",Visible=.t., Left=56, Top=377,;
    Alignment=1, Width=128, Height=22,;
    Caption="Importo da versare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="WRRCZRNLZG",Visible=.t., Left=9, Top=224,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP14"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="USJCVZBWWP",Visible=.t., Left=9, Top=245,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP15"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="GEYTVTVHPW",Visible=.t., Left=332, Top=56,;
    Alignment=0, Width=49, Height=15,;
    Caption="DEBITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_72 as StdString with uid="TOXDSNMUTM",Visible=.t., Left=612, Top=56,;
    Alignment=0, Width=60, Height=15,;
    Caption="CREDITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_73 as StdString with uid="SOSVBHKZMP",Visible=.t., Left=346, Top=77,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_74 as StdString with uid="IAUYCXWSLS",Visible=.t., Left=9, Top=266,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP16"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_74.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_75 as StdString with uid="TZFJHSUHSV",Visible=.t., Left=9, Top=287,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP17"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_78 as StdString with uid="DLCXKMYDUU",Visible=.t., Left=9, Top=377,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP21"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_78.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_79 as StdString with uid="VVJYSOGNHG",Visible=.t., Left=346, Top=98,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_79.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="SZIIUNBNKE",Visible=.t., Left=346, Top=119,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="IDFWREUKAO",Visible=.t., Left=346, Top=139,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_81.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_82 as StdString with uid="XRCDCIRTHZ",Visible=.t., Left=346, Top=182,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_82.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="KUKQADQJBC",Visible=.t., Left=346, Top=203,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="XRWRXSCOAT",Visible=.t., Left=346, Top=245,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_85 as StdString with uid="VFLJIMKGDW",Visible=.t., Left=648, Top=245,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_86 as StdString with uid="PDCKMXXDFH",Visible=.t., Left=648, Top=266,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_86.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_87 as StdString with uid="PIZTRUJIXT",Visible=.t., Left=648, Top=287,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="FUQLKKXKVZ",Visible=.t., Left=346, Top=287,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_89 as StdString with uid="XLGICXFGRH",Visible=.t., Left=648, Top=161,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_90 as StdString with uid="OEIGIETVGT",Visible=.t., Left=648, Top=182,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="GPBEPGLNHL",Visible=.t., Left=648, Top=203,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="PIDXEFROLN",Visible=.t., Left=648, Top=77,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="XHNFWDHHOS",Visible=.t., Left=648, Top=98,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_94 as StdString with uid="ZTOPKZWMXG",Visible=.t., Left=648, Top=119,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_95 as StdString with uid="XVJUGBEPSH",Visible=.t., Left=344, Top=377,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_95.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_96 as StdString with uid="BJOEIFBCKA",Visible=.t., Left=383, Top=308,;
    Alignment=1, Width=110, Height=22,;
    Caption="Crediti speciali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="ZKMOHMZAIM",Visible=.t., Left=9, Top=308,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP18"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_98 as StdString with uid="UKKZDXGUGP",Visible=.t., Left=648, Top=308,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_98.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_99 as StdString with uid="RKJEEWYSIZ",Visible=.t., Left=383, Top=347,;
    Alignment=1, Width=110, Height=22,;
    Caption="Acconto versato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_100 as StdString with uid="IPQCDBGKUO",Visible=.t., Left=9, Top=347,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP20"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_101 as StdString with uid="ITRLZUMJFW",Visible=.t., Left=648, Top=347,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_102 as StdString with uid="XFKWWJMEDI",Visible=.t., Left=46, Top=328,;
    Alignment=1, Width=144, Height=22,;
    Caption="Interessi dovuti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_103 as StdString with uid="PHITCKFCJM",Visible=.t., Left=9, Top=328,;
    Alignment=0, Width=30, Height=22,;
    Caption="VP19"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_103.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_1_104 as StdString with uid="PZFQRHJBGF",Visible=.t., Left=346, Top=328,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_104.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_108 as StdString with uid="WOLYXTOMXR",Visible=.t., Left=208, Top=7,;
    Alignment=1, Width=57, Height=18,;
    Caption="Attivit�:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_111 as StdString with uid="WWCXDAFGWX",Visible=.t., Left=346, Top=224,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_111.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_112 as StdString with uid="NTYHDDLPPK",Visible=.t., Left=648, Top=224,;
    Alignment=0, Width=24, Height=22,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_112.mHide()
    with this.Parent.oContained
      return (IIF(.w_VPCODVAL='S' , g_CODEUR , g_CODLIR ) = g_CODEUR)
    endwith
  endfunc

  add object oStr_1_113 as StdString with uid="VDZLBBPZQQ",Visible=.t., Left=382, Top=224,;
    Alignment=1, Width=111, Height=22,;
    Caption="Vers. in eccesso:"  ;
  , bGlobalFont=.t.

  add object oBox_1_69 as StdBox with uid="QVPLZRFHMT",left=7, top=72, width=671,height=1

  add object oBox_1_77 as StdBox with uid="MYBKNBYYWF",left=6, top=372, width=671,height=1
enddefine
define class tgsar_avpPag2 as StdContainer
  Width  = 679
  height = 399
  stdWidth  = 679
  stdheight = 399
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVPIMPVER_2_1 as StdField with uid="XMIBSHMPGI",rtseq=46,rtrep=.f.,;
    cFormVar = "w_VPIMPVER", cQueryName = "VPIMPVER",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo versato",;
    HelpContextID = 130576040,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=256, Top=17, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oVPIMPVER_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  add object oVPVEREUR_2_2 as StdCheck with uid="ZJPYZJSJDL",rtseq=47,rtrep=.f.,left=460, top=17, caption="Versamento in Euro", enabled=.f.,;
    ToolTipText = "Se attivo: versamento eseguito in Euro",;
    HelpContextID = 115424936,;
    cFormVar="w_VPVEREUR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPVEREUR_2_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPVEREUR_2_2.GetRadio()
    this.Parent.oContained.w_VPVEREUR = this.RadioValue()
    return .t.
  endfunc

  func oVPVEREUR_2_2.SetRadio()
    this.Parent.oContained.w_VPVEREUR=trim(this.Parent.oContained.w_VPVEREUR)
    this.value = ;
      iif(this.Parent.oContained.w_VPVEREUR=='S',1,;
      0)
  endfunc

  add object oVPDATVER_2_3 as StdField with uid="FTWCIPGKHR",rtseq=48,rtrep=.f.,;
    cFormVar = "w_VPDATVER", cQueryName = "VPDATVER",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del versamento",;
    HelpContextID = 133963432,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=256, Top=49

  add object oVPCODAZI_2_4 as StdField with uid="FYYZHQZEWN",rtseq=49,rtrep=.f.,;
    cFormVar = "w_VPCODAZI", cQueryName = "VPCODAZI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 34213535,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=478, Top=49, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_VPCODAZI"

  func oVPCODAZI_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPCODAZI_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPCODAZI_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_ABI','*','ABCODABI',cp_AbsName(this.parent,'oVPCODAZI_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABI',"Codici ABI",'',this.parent.oContained
  endproc
  proc oVPCODAZI_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ABCODABI=this.parent.oContained.w_VPCODAZI
     i_obj.ecpSave()
  endproc

  add object oVPCODCAB_2_5 as StdField with uid="BTNJLSBXWS",rtseq=50,rtrep=.f.,;
    cFormVar = "w_VPCODCAB", cQueryName = "VPCODCAB",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 200667496,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=599, Top=49, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_CAB", cZoomOnZoom="GSAR_AFI", oKey_1_1="FICODCAB", oKey_1_2="this.w_VPCODCAB"

  func oVPCODCAB_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPCODCAB_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPCODCAB_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_CAB','*','FICODCAB',cp_AbsName(this.parent,'oVPCODCAB_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFI',"Codici CAB",'',this.parent.oContained
  endproc
  proc oVPCODCAB_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FICODCAB=this.parent.oContained.w_VPCODCAB
     i_obj.ecpSave()
  endproc

  add object oVPVERNEF_2_6 as StdCheck with uid="DJIVAIMQNK",rtseq=51,rtrep=.f.,left=61, top=96, caption="Versamento non effettuato",;
    HelpContextID = 266419868,;
    cFormVar="w_VPVERNEF", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPVERNEF_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPVERNEF_2_6.GetRadio()
    this.Parent.oContained.w_VPVERNEF = this.RadioValue()
    return .t.
  endfunc

  func oVPVERNEF_2_6.SetRadio()
    this.Parent.oContained.w_VPVERNEF=trim(this.Parent.oContained.w_VPVERNEF)
    this.value = ;
      iif(this.Parent.oContained.w_VPVERNEF=='S',1,;
      0)
  endfunc

  add object oVPAR74C5_2_7 as StdCheck with uid="OPZITJKJVL",rtseq=52,rtrep=.f.,left=61, top=120, caption="Subfornitori agevolati (art.74 comma 5)",;
    HelpContextID = 197333365,;
    cFormVar="w_VPAR74C5", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPAR74C5_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPAR74C5_2_7.GetRadio()
    this.Parent.oContained.w_VPAR74C5 = this.RadioValue()
    return .t.
  endfunc

  func oVPAR74C5_2_7.SetRadio()
    this.Parent.oContained.w_VPAR74C5=trim(this.Parent.oContained.w_VPAR74C5)
    this.value = ;
      iif(this.Parent.oContained.w_VPAR74C5=='S',1,;
      0)
  endfunc

  add object oVPAR74C4_2_8 as StdCheck with uid="DFOPRGPHYA",rtseq=53,rtrep=.f.,left=61, top=144, caption="Contribuenti liq.trim. (art.74 comma 4)",;
    HelpContextID = 197333366,;
    cFormVar="w_VPAR74C4", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPAR74C4_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPAR74C4_2_8.GetRadio()
    this.Parent.oContained.w_VPAR74C4 = this.RadioValue()
    return .t.
  endfunc

  func oVPAR74C4_2_8.SetRadio()
    this.Parent.oContained.w_VPAR74C4=trim(this.Parent.oContained.w_VPAR74C4)
    this.value = ;
      iif(this.Parent.oContained.w_VPAR74C4=='S',1,;
      0)
  endfunc

  add object oVPOPMEDI_2_11 as StdCheck with uid="NMHIKBDDSS",rtseq=54,rtrep=.f.,left=61, top=188, caption="Aliquota media",;
    HelpContextID = 110874271,;
    cFormVar="w_VPOPMEDI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPOPMEDI_2_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPOPMEDI_2_11.GetRadio()
    this.Parent.oContained.w_VPOPMEDI = this.RadioValue()
    return .t.
  endfunc

  func oVPOPMEDI_2_11.SetRadio()
    this.Parent.oContained.w_VPOPMEDI=trim(this.Parent.oContained.w_VPOPMEDI)
    this.value = ;
      iif(this.Parent.oContained.w_VPOPMEDI=='S',1,;
      0)
  endfunc

  add object oVPOPNOIM_2_12 as StdCheck with uid="SBBGZQFMLD",rtseq=55,rtrep=.f.,left=206, top=188, caption="Operazioni non imponibili",;
    HelpContextID = 11259555,;
    cFormVar="w_VPOPNOIM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPOPNOIM_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPOPNOIM_2_12.GetRadio()
    this.Parent.oContained.w_VPOPNOIM = this.RadioValue()
    return .t.
  endfunc

  func oVPOPNOIM_2_12.SetRadio()
    this.Parent.oContained.w_VPOPNOIM=trim(this.Parent.oContained.w_VPOPNOIM)
    this.value = ;
      iif(this.Parent.oContained.w_VPOPNOIM=='S',1,;
      0)
  endfunc

  add object oVPACQBEA_2_13 as StdCheck with uid="KZCGQZXSUU",rtseq=56,rtrep=.f.,left=413, top=188, caption="Acquisto di beni ammortizzabili",;
    HelpContextID = 63827607,;
    cFormVar="w_VPACQBEA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPACQBEA_2_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPACQBEA_2_13.GetRadio()
    this.Parent.oContained.w_VPACQBEA = this.RadioValue()
    return .t.
  endfunc

  func oVPACQBEA_2_13.SetRadio()
    this.Parent.oContained.w_VPACQBEA=trim(this.Parent.oContained.w_VPACQBEA)
    this.value = ;
      iif(this.Parent.oContained.w_VPACQBEA=='S',1,;
      0)
  endfunc

  add object oVPCRERIM_2_14 as StdField with uid="UGLQRKFRTU",rtseq=57,rtrep=.f.,;
    cFormVar = "w_VPCRERIM", cQueryName = "VPCRERIM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 52235939,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=251, Top=230, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oVPCRERIM_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND (.w_VPOPMEDI='S' OR .w_VPOPNOIM='S' OR .w_VPACQBEA='S'))
    endwith
   endif
  endfunc


  add object oVPEURO19_2_15 as StdCombo with uid="TZYXRNLSAI",rtseq=58,rtrep=.f.,left=544,top=230,width=77,height=21, enabled=.f.;
    , ToolTipText = "Valuta del credito richiesto";
    , HelpContextID = 252694897;
    , cFormVar="w_VPEURO19",RowSource=""+"Lire,"+"Euro", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oVPEURO19_2_15.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(3))))
  endfunc
  func oVPEURO19_2_15.GetRadio()
    this.Parent.oContained.w_VPEURO19 = this.RadioValue()
    return .t.
  endfunc

  func oVPEURO19_2_15.SetRadio()
    this.Parent.oContained.w_VPEURO19=trim(this.Parent.oContained.w_VPEURO19)
    this.value = ;
      iif(this.Parent.oContained.w_VPEURO19=='N',1,;
      iif(this.Parent.oContained.w_VPEURO19=='S',2,;
      0))
  endfunc

  add object oVPCREUTI_2_17 as StdField with uid="NIZPHXQQRK",rtseq=59,rtrep=.f.,;
    cFormVar = "w_VPCREUTI", cQueryName = "VPCREUTI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 102567583,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=251, Top=274, cSayPict="V_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  func oVPCREUTI_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND (.w_VPOPMEDI='S' OR .w_VPOPNOIM='S' OR .w_VPACQBEA='S'))
    endwith
   endif
  endfunc


  add object oVPCODCAR_2_19 as StdCombo with uid="TJRJHFFKNG",rtseq=60,rtrep=.f.,left=164,top=320,width=219,height=21;
    , HelpContextID = 200667480;
    , cFormVar="w_VPCODCAR",RowSource=""+"1) Rappresentante legale o negoziale,"+"2) Socio amministratore,"+"3) Curatore fallimentare,"+"4) Commissario liquidatore,"+"5) Commissario giudiziale,"+"6) Rappresentante fiscale,"+"7) Eredi del contribuente,"+"8) Liquidatore,"+"9) Societ� beneficiaria o incorporante", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oVPCODCAR_2_19.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    space(1)))))))))))
  endfunc
  func oVPCODCAR_2_19.GetRadio()
    this.Parent.oContained.w_VPCODCAR = this.RadioValue()
    return .t.
  endfunc

  func oVPCODCAR_2_19.SetRadio()
    this.Parent.oContained.w_VPCODCAR=trim(this.Parent.oContained.w_VPCODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_VPCODCAR=='1',1,;
      iif(this.Parent.oContained.w_VPCODCAR=='2',2,;
      iif(this.Parent.oContained.w_VPCODCAR=='3',3,;
      iif(this.Parent.oContained.w_VPCODCAR=='4',4,;
      iif(this.Parent.oContained.w_VPCODCAR=='5',5,;
      iif(this.Parent.oContained.w_VPCODCAR=='6',6,;
      iif(this.Parent.oContained.w_VPCODCAR=='7',7,;
      iif(this.Parent.oContained.w_VPCODCAR=='8',8,;
      iif(this.Parent.oContained.w_VPCODCAR=='9',9,;
      0)))))))))
  endfunc

  add object oVPCODFIS_2_22 as StdField with uid="MTWBNESFKS",rtseq=61,rtrep=.f.,;
    cFormVar = "w_VPCODFIS", cQueryName = "VPCODFIS",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 118099625,;
   bGlobalFont=.t.,;
    Height=21, Width=150, Left=488, Top=320, InputMask=replicate('X',16)

  add object oVPDATPRE_2_23 as StdField with uid="EERNRGBHUZ",rtseq=62,rtrep=.f.,;
    cFormVar = "w_VPDATPRE", cQueryName = "VPDATPRE",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 235135333,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=164, Top=351

  add object oVPDATINO_2_37 as StdField with uid="HACGOCEPGY",rtseq=65,rtrep=.f.,;
    cFormVar = "w_VPDATINO", cQueryName = "VPDATINO",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di avvenuto inoltro telematico",;
    HelpContextID = 84140379,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=488, Top=351

  func oVPDATINO_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ATTIVI<>'S' OR .w_VPKEYATT='#####')
    endwith
   endif
  endfunc

  add object oStr_2_10 as StdString with uid="KUTYOLXHHI",Visible=.t., Left=6, Top=93,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP23"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_10.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_2_16 as StdString with uid="PKJQPMWGGO",Visible=.t., Left=78, Top=230,;
    Alignment=1, Width=170, Height=15,;
    Caption="Credito chiesto rimborso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="IMQPNLTSAS",Visible=.t., Left=55, Top=274,;
    Alignment=1, Width=193, Height=15,;
    Caption="Credito da utilizzare con mod. F24:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="BNNFMFQRKR",Visible=.t., Left=19, Top=320,;
    Alignment=1, Width=142, Height=15,;
    Caption="Codice carica:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="JQTWKDNREB",Visible=.t., Left=6, Top=188,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP30"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_2_24 as StdString with uid="WLOYDNNDVK",Visible=.t., Left=19, Top=351,;
    Alignment=1, Width=142, Height=15,;
    Caption="Data presentazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="CQPTKGRUZK",Visible=.t., Left=396, Top=320,;
    Alignment=1, Width=90, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="LDVZOYRMNE",Visible=.t., Left=388, Top=230,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (g_CODEUR = IIF (.w_VPEURO19='S',g_CODEUR , g_CODLIR))
    endwith
  endfunc

  add object oStr_2_27 as StdString with uid="BVXLKTCQJJ",Visible=.t., Left=388, Top=274,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_27.mHide()
    with this.Parent.oContained
      return (g_CODEUR = IIF (.w_VPEURO19='S',g_CODEUR , g_CODLIR))
    endwith
  endfunc

  add object oStr_2_28 as StdString with uid="WECCJOAECL",Visible=.t., Left=6, Top=14,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP22"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_28.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_2_29 as StdString with uid="XQZKYCQRZP",Visible=.t., Left=368, Top=49,;
    Alignment=1, Width=108, Height=15,;
    Caption="Cod.azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="ULJBYGCWLD",Visible=.t., Left=548, Top=49,;
    Alignment=1, Width=50, Height=15,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="MEUMBPDBST",Visible=.t., Left=6, Top=49,;
    Alignment=0, Width=154, Height=18,;
    Caption="Estremi del versamento"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="FTGIGEGQQP",Visible=.t., Left=141, Top=17,;
    Alignment=1, Width=112, Height=18,;
    Caption="Importo versato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="PGKPUIPDVP",Visible=.t., Left=393, Top=17,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_34.mHide()
    with this.Parent.oContained
      return (g_CODEUR = IIF (.w_VPEURO19='S',g_CODEUR , g_CODLIR))
    endwith
  endfunc

  add object oStr_2_35 as StdString with uid="GSOETMSDFC",Visible=.t., Left=47, Top=17,;
    Alignment=0, Width=86, Height=18,;
    Caption="Versamento"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="FGPQPFNHGI",Visible=.t., Left=185, Top=49,;
    Alignment=1, Width=68, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="HTNWDKATFX",Visible=.t., Left=289, Top=351,;
    Alignment=1, Width=197, Height=18,;
    Caption="Data di inoltro telematico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="WWQVPBGMJW",Visible=.t., Left=6, Top=230,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP31"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_40.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_2_42 as StdString with uid="JXULNJOMSJ",Visible=.t., Left=6, Top=274,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP32"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_42.mHide()
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)>2001 OR EMPTY(.w_VP__ANNO))
    endwith
  endfunc

  add object oStr_2_44 as StdString with uid="HHSQOAWVSU",Visible=.t., Left=461, Top=230,;
    Alignment=1, Width=80, Height=18,;
    Caption="Richiesto in:"  ;
  , bGlobalFont=.t.

  add object oBox_2_9 as StdBox with uid="SRFOOQXURX",left=5, top=175, width=666,height=1

  add object oBox_2_31 as StdBox with uid="INFVELFEXN",left=5, top=84, width=666,height=1

  add object oBox_2_39 as StdBox with uid="TDVEFMCYHB",left=5, top=219, width=666,height=1

  add object oBox_2_41 as StdBox with uid="GFLAQOCEUU",left=5, top=263, width=666,height=1

  add object oBox_2_43 as StdBox with uid="OFAGLMODQH",left=5, top=308, width=666,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_avp','IVA_PERI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".VP__ANNO=IVA_PERI.VP__ANNO";
  +" and "+i_cAliasName2+".VPPERIOD=IVA_PERI.VPPERIOD";
  +" and "+i_cAliasName2+".VPKEYATT=IVA_PERI.VPKEYATT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
