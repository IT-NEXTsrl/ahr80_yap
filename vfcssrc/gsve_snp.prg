* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_snp                                                        *
*              Stampa note provvigioni                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_17]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2014-01-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_snp",oParentObject))

* --- Class definition
define class tgsve_snp as StdForm
  Top    = 42
  Left   = 91

  * --- Standard Properties
  Width  = 438
  Height = 249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-01-14"
  HelpContextID=93703575
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  AGENTI_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsve_snp"
  cComment = "Stampa note provvigioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZE = space(5)
  w_FINDATA = ctod('  /  /  ')
  w_STAMPA = space(1)
  o_STAMPA = space(1)
  w_SCELTA = space(1)
  w_NUMERO1 = 0
  w_NUMERO2 = 0
  w_NUMERO = 0
  w_ALFDOC = space(2)
  w_DATNOTA = ctod('  /  /  ')
  o_DATNOTA = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_ANNDOC = 0
  w_AGEINI = space(5)
  w_AGEFIN = space(5)
  w_CODFOR = space(15)
  w_CODFOR1 = space(15)
  w_DATOBSO = ctod('  /  /  ')
  w_INIDATA = ctod('  /  /  ')
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_VALUTA = space(3)
  w_DESAGE = space(35)
  w_DESAG2 = space(35)
  w_DESAPP = space(35)
  w_AGGPROS = space(1)
  w_CAPOINI = space(5)
  w_CAPOFIN = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_snpPag1","gsve_snp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTAMPA_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AGENTI'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsve_snp
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZE=space(5)
      .w_FINDATA=ctod("  /  /  ")
      .w_STAMPA=space(1)
      .w_SCELTA=space(1)
      .w_NUMERO1=0
      .w_NUMERO2=0
      .w_NUMERO=0
      .w_ALFDOC=space(2)
      .w_DATNOTA=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_ANNDOC=0
      .w_AGEINI=space(5)
      .w_AGEFIN=space(5)
      .w_CODFOR=space(15)
      .w_CODFOR1=space(15)
      .w_DATOBSO=ctod("  /  /  ")
      .w_INIDATA=ctod("  /  /  ")
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_VALUTA=space(3)
      .w_DESAGE=space(35)
      .w_DESAG2=space(35)
      .w_DESAPP=space(35)
      .w_AGGPROS=space(1)
      .w_CAPOINI=space(5)
      .w_CAPOFIN=space(5)
        .w_CODAZE = i_codazi
        .w_FINDATA = MESESKIP(i_datsys,-1,'F')
        .w_STAMPA = 'F'
        .w_SCELTA = iif(.w_STAMPA=' ','S','')
          .DoRTCalc(5,8,.f.)
        .w_DATNOTA = i_datsys
        .w_OBTEST = .w_DATNOTA
        .w_ANNDOC = YEAR(.w_DATNOTA)
        .w_AGEINI = SPACE(5)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_AGEINI))
          .link_1_12('Full')
        endif
        .w_AGEFIN = SPACE(5)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_AGEFIN))
          .link_1_13('Full')
        endif
          .DoRTCalc(14,16,.f.)
        .w_INIDATA = MESESKIP(i_datsys,-1,'I')
        .w_DATA1 = .w_INIDATA
        .w_DATA2 = .w_FINDATA
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_VALUTA))
          .link_1_21('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
          .DoRTCalc(21,23,.f.)
        .w_AGGPROS = ' '
        .w_CAPOINI = '     '
        .w_CAPOFIN = '     '
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_STAMPA<>.w_STAMPA
            .w_SCELTA = iif(.w_STAMPA=' ','S','')
        endif
        .DoRTCalc(5,9,.t.)
        if .o_DATNOTA<>.w_DATNOTA
            .w_OBTEST = .w_DATNOTA
        endif
            .w_ANNDOC = YEAR(.w_DATNOTA)
        if .o_STAMPA<>.w_STAMPA
            .w_AGEINI = SPACE(5)
          .link_1_12('Full')
        endif
        if .o_STAMPA<>.w_STAMPA
            .w_AGEFIN = SPACE(5)
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSCELTA_1_4.enabled = this.oPgFrm.Page1.oPag.oSCELTA_1_4.mCond()
    this.oPgFrm.Page1.oPag.oNUMERO1_1_5.enabled = this.oPgFrm.Page1.oPag.oNUMERO1_1_5.mCond()
    this.oPgFrm.Page1.oPag.oNUMERO2_1_6.enabled = this.oPgFrm.Page1.oPag.oNUMERO2_1_6.mCond()
    this.oPgFrm.Page1.oPag.oNUMERO_1_7.enabled = this.oPgFrm.Page1.oPag.oNUMERO_1_7.mCond()
    this.oPgFrm.Page1.oPag.oALFDOC_1_8.enabled = this.oPgFrm.Page1.oPag.oALFDOC_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNUMERO1_1_5.visible=!this.oPgFrm.Page1.oPag.oNUMERO1_1_5.mHide()
    this.oPgFrm.Page1.oPag.oNUMERO2_1_6.visible=!this.oPgFrm.Page1.oPag.oNUMERO2_1_6.mHide()
    this.oPgFrm.Page1.oPag.oNUMERO_1_7.visible=!this.oPgFrm.Page1.oPag.oNUMERO_1_7.mHide()
    this.oPgFrm.Page1.oPag.oALFDOC_1_8.visible=!this.oPgFrm.Page1.oPag.oALFDOC_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDATNOTA_1_9.visible=!this.oPgFrm.Page1.oPag.oDATNOTA_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AGEINI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGEINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGEINI)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGEINI))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGEINI)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AGEINI)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AGEINI)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGEINI) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGEINI_1_12'),i_cWhere,'GSAR_AGE',"Agenti",'gsve0snp.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGEINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGEINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGEINI)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGEINI = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_CODFOR = NVL(_Link_.AGFLFARI,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_AGEINI = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CODFOR = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_agefin) or (.w_agefin>=.w_ageini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente incongruente oppure obsoleto")
        endif
        this.w_AGEINI = space(5)
        this.w_DESAGE = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CODFOR = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGEINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGEFIN
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGEFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGEFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGEFIN))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGEFIN)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AGEFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AGEFIN)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGEFIN) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGEFIN_1_13'),i_cWhere,'GSAR_AGE',"Agenti",'gsve0snp.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGEFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGEFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGEFIN)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGFLFARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGEFIN = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAG2 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_CODFOR1 = NVL(_Link_.AGFLFARI,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_AGEFIN = space(5)
      endif
      this.w_DESAG2 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CODFOR1 = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_agefin) or (.w_agefin>=.w_ageini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente incongruente oppure obsoleto")
        endif
        this.w_AGEFIN = space(5)
        this.w_DESAG2 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CODFOR1 = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGEFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALUTA))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALUTA_1_21'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DESAPP = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTAMPA_1_3.RadioValue()==this.w_STAMPA)
      this.oPgFrm.Page1.oPag.oSTAMPA_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCELTA_1_4.RadioValue()==this.w_SCELTA)
      this.oPgFrm.Page1.oPag.oSCELTA_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO1_1_5.value==this.w_NUMERO1)
      this.oPgFrm.Page1.oPag.oNUMERO1_1_5.value=this.w_NUMERO1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO2_1_6.value==this.w_NUMERO2)
      this.oPgFrm.Page1.oPag.oNUMERO2_1_6.value=this.w_NUMERO2
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_7.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_7.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_8.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_8.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATNOTA_1_9.value==this.w_DATNOTA)
      this.oPgFrm.Page1.oPag.oDATNOTA_1_9.value=this.w_DATNOTA
    endif
    if not(this.oPgFrm.Page1.oPag.oAGEINI_1_12.value==this.w_AGEINI)
      this.oPgFrm.Page1.oPag.oAGEINI_1_12.value=this.w_AGEINI
    endif
    if not(this.oPgFrm.Page1.oPag.oAGEFIN_1_13.value==this.w_AGEFIN)
      this.oPgFrm.Page1.oPag.oAGEFIN_1_13.value=this.w_AGEFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_18.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_18.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_19.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_19.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_21.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_21.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_28.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_28.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAG2_1_29.value==this.w_DESAG2)
      this.oPgFrm.Page1.oPag.oDESAG2_1_29.value=this.w_DESAG2
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGPROS_1_40.RadioValue()==this.w_AGGPROS)
      this.oPgFrm.Page1.oPag.oAGGPROS_1_40.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SCELTA))  and (.w_STAMPA=' ')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCELTA_1_4.SetFocus()
            i_bnoObbl = !empty(.w_SCELTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NUMERO))  and not(.w_STAMPA='F' OR .w_STAMPA='R' OR .w_SCELTA='R')  and (.w_STAMPA=' ' and .w_SCELTA<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO_1_7.SetFocus()
            i_bnoObbl = !empty(.w_NUMERO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATNOTA))  and not(.w_SCELTA='R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATNOTA_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DATNOTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AGEINI)) or not((empty(.w_agefin) or (.w_agefin>=.w_ageini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGEINI_1_12.SetFocus()
            i_bnoObbl = !empty(.w_AGEINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Agente incongruente oppure obsoleto")
          case   ((empty(.w_AGEFIN)) or not((empty(.w_agefin) or (.w_agefin>=.w_ageini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGEFIN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_AGEFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Agente incongruente oppure obsoleto")
          case   (empty(.w_DATA1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DATA1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_19.SetFocus()
            i_bnoObbl = !empty(.w_DATA2)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_STAMPA = this.w_STAMPA
    this.o_DATNOTA = this.w_DATNOTA
    return

enddefine

* --- Define pages as container
define class tgsve_snpPag1 as StdContainer
  Width  = 434
  height = 249
  stdWidth  = 434
  stdheight = 249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSTAMPA_1_3 as StdCombo with uid="ROGAQJZVJU",value=1,rtseq=3,rtrep=.f.,left=100,top=8,width=116,height=21;
    , ToolTipText = "Scelta documento da stampare";
    , HelpContextID = 68733146;
    , cFormVar="w_STAMPA",RowSource=""+"Nota provvigioni,"+"Fattura,"+"Ricevuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAMPA_1_3.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'F',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oSTAMPA_1_3.GetRadio()
    this.Parent.oContained.w_STAMPA = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPA_1_3.SetRadio()
    this.Parent.oContained.w_STAMPA=trim(this.Parent.oContained.w_STAMPA)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPA=='',1,;
      iif(this.Parent.oContained.w_STAMPA=='F',2,;
      iif(this.Parent.oContained.w_STAMPA=='R',3,;
      0)))
  endfunc


  add object oSCELTA_1_4 as StdCombo with uid="IGSOYSKGUK",rtseq=4,rtrep=.f.,left=295,top=8,width=116,height=21;
    , ToolTipText = "Tipo di stampa selezionata";
    , HelpContextID = 64592346;
    , cFormVar="w_SCELTA",RowSource=""+"Simulata,"+"Definitiva,"+"Ristampa", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oSCELTA_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'R',;
    ''))))
  endfunc
  func oSCELTA_1_4.GetRadio()
    this.Parent.oContained.w_SCELTA = this.RadioValue()
    return .t.
  endfunc

  func oSCELTA_1_4.SetRadio()
    this.Parent.oContained.w_SCELTA=trim(this.Parent.oContained.w_SCELTA)
    this.value = ;
      iif(this.Parent.oContained.w_SCELTA=='S',1,;
      iif(this.Parent.oContained.w_SCELTA=='D',2,;
      iif(this.Parent.oContained.w_SCELTA=='R',3,;
      0)))
  endfunc

  func oSCELTA_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPA=' ')
    endwith
   endif
  endfunc

  add object oNUMERO1_1_5 as StdField with uid="VWWTXCHLLG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMERO1", cQueryName = "NUMERO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della prima nota da stampare",;
    HelpContextID = 100665386,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=100, Top=39, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO1_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPA=' ' and .w_SCELTA='R')
    endwith
   endif
  endfunc

  func oNUMERO1_1_5.mHide()
    with this.Parent.oContained
      return (.w_STAMPA='F' OR .w_STAMPA='R' OR .w_SCELTA<>'R')
    endwith
  endfunc

  add object oNUMERO2_1_6 as StdField with uid="IKMENZOZTK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NUMERO2", cQueryName = "NUMERO2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'ultima nota da ristampare",;
    HelpContextID = 100665386,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=295, Top=39, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO2_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPA=' ' and .w_SCELTA='R')
    endwith
   endif
  endfunc

  func oNUMERO2_1_6.mHide()
    with this.Parent.oContained
      return (.w_STAMPA='F' OR .w_STAMPA='R' OR .w_SCELTA<>'R')
    endwith
  endfunc

  add object oNUMERO_1_7 as StdField with uid="KAHSNRRFBW",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della prima nota da stampare",;
    HelpContextID = 100665386,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=100, Top=39, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPA=' ' and .w_SCELTA<>'R')
    endwith
   endif
  endfunc

  func oNUMERO_1_7.mHide()
    with this.Parent.oContained
      return (.w_STAMPA='F' OR .w_STAMPA='R' OR .w_SCELTA='R')
    endwith
  endfunc

  add object oALFDOC_1_8 as StdField with uid="BYIRWIMKVK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Serie della prima nota da stampare",;
    HelpContextID = 36798970,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=182, Top=39, InputMask=replicate('X',2)

  func oALFDOC_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPA=' ' and .w_SCELTA<>'R')
    endwith
   endif
  endfunc

  func oALFDOC_1_8.mHide()
    with this.Parent.oContained
      return (.w_STAMPA='F' OR .w_STAMPA='R' OR .w_SCELTA='R')
    endwith
  endfunc

  add object oDATNOTA_1_9 as StdField with uid="TEBZLAEOHC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATNOTA", cQueryName = "DATNOTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di stampa selezionata",;
    HelpContextID = 19311818,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=295, Top=39

  func oDATNOTA_1_9.mHide()
    with this.Parent.oContained
      return (.w_SCELTA='R')
    endwith
  endfunc

  add object oAGEINI_1_12 as StdField with uid="MBJTPHHTTR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_AGEINI", cQueryName = "AGEINI",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Agente incongruente oppure obsoleto",;
    ToolTipText = "Agente iniziale selezionato",;
    HelpContextID = 205297402,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=100, Top=71, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGEINI"

  func oAGEINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGEINI_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGEINI_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGEINI_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'gsve0snp.AGENTI_VZM',this.parent.oContained
  endproc
  proc oAGEINI_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGEINI
     i_obj.ecpSave()
  endproc

  add object oAGEFIN_1_13 as StdField with uid="VARMWEHIMD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_AGEFIN", cQueryName = "AGEFIN",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Agente incongruente oppure obsoleto",;
    ToolTipText = "Agente finale selezionato",;
    HelpContextID = 126850810,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=100, Top=101, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGEFIN"

  func oAGEFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGEFIN_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGEFIN_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGEFIN_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'gsve0snp.AGENTI_VZM',this.parent.oContained
  endproc
  proc oAGEFIN_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGEFIN
     i_obj.ecpSave()
  endproc

  add object oDATA1_1_18 as StdField with uid="KIZVHGTZYE",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio stampa",;
    HelpContextID = 149705526,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=100, Top=131

  add object oDATA2_1_19 as StdField with uid="WXLCDNPBFK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine stampa",;
    HelpContextID = 150754102,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=100, Top=161

  add object oVALUTA_1_21 as StdField with uid="AHTOMBGLJP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta documento di selezione",;
    HelpContextID = 63974314,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=100, Top=191, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALUTA_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALUTA_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALUTA_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oVALUTA_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALUTA
     i_obj.ecpSave()
  endproc

  add object oDESAGE_1_28 as StdField with uid="FSUKJGYHRZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 11778250,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=164, Top=71, InputMask=replicate('X',35)

  add object oDESAG2_1_29 as StdField with uid="LTTTGQELHS",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESAG2", cQueryName = "DESAG2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 62109898,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=164, Top=101, InputMask=replicate('X',35)


  add object oBtn_1_31 as StdButton with uid="VBUEVRCIXT",left=312, top=200, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 93732326;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        do GSVE_BNP with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_32 as StdButton with uid="ICODEAEJMR",left=363, top=200, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 101020998;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_33 as cp_runprogram with uid="VZCCBMFXQK",left=-10, top=270, width=511,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BN2',;
    cEvent = "w_ALFDOC Changed,w_DATNOTA Changed,w_STAMPA Changed,w_SCELTA Changed",;
    nPag=1;
    , HelpContextID = 265169690

  add object oAGGPROS_1_40 as StdCheck with uid="VDLCMIVXID",rtseq=24,rtrep=.f.,left=253, top=130, caption="Prospetto provv.liquidate",;
    ToolTipText = "Se attivo: stampa anche il prospetto delle provvigioni liquidate",;
    HelpContextID = 99972858,;
    cFormVar="w_AGGPROS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGPROS_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAGGPROS_1_40.GetRadio()
    this.Parent.oContained.w_AGGPROS = this.RadioValue()
    return .t.
  endfunc

  func oAGGPROS_1_40.SetRadio()
    this.Parent.oContained.w_AGGPROS=trim(this.Parent.oContained.w_AGGPROS)
    this.value = ;
      iif(this.Parent.oContained.w_AGGPROS=='S',1,;
      0)
  endfunc

  add object oStr_1_20 as StdString with uid="JVQFXVQSOM",Visible=.t., Left=9, Top=8,;
    Alignment=1, Width=88, Height=15,;
    Caption="Scelta stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="HZVCLYHCEE",Visible=.t., Left=-133, Top=-112,;
    Alignment=1, Width=75, Height=15,;
    Caption="Data stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="LXNSRBQCAI",Visible=.t., Left=9, Top=71,;
    Alignment=1, Width=88, Height=15,;
    Caption="Da agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="DFOYCWAWCA",Visible=.t., Left=9, Top=101,;
    Alignment=1, Width=88, Height=15,;
    Caption="A agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="YFCGYYNZYC",Visible=.t., Left=9, Top=131,;
    Alignment=1, Width=88, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="GTWEMYDFPQ",Visible=.t., Left=9, Top=161,;
    Alignment=1, Width=88, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="QEBSFHRNLW",Visible=.t., Left=9, Top=191,;
    Alignment=1, Width=88, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="RUDMAHAUXZ",Visible=.t., Left=217, Top=8,;
    Alignment=1, Width=75, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="VWJYMJVHAV",Visible=.t., Left=9, Top=39,;
    Alignment=1, Width=88, Height=15,;
    Caption="Primo numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_STAMPA='F' OR .w_STAMPA='R' OR .w_SCELTA='R')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="JGNMTJTTID",Visible=.t., Left=170, Top=39,;
    Alignment=0, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_STAMPA='F' OR .w_STAMPA='R' OR .w_SCELTA='R')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="NMMHECHHMZ",Visible=.t., Left=270, Top=39,;
    Alignment=1, Width=22, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_SCELTA='R')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="MCIAJMQPJV",Visible=.t., Left=14, Top=39,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_STAMPA='F' OR .w_STAMPA='R' OR .w_SCELTA<>'R')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="WSTFRHGCOG",Visible=.t., Left=224, Top=39,;
    Alignment=1, Width=68, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_STAMPA='F' OR .w_STAMPA='R' OR .w_SCELTA<>'R')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_snp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
