* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kci                                                        *
*              Configurazione interfaccia                                      *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_46]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-04                                                      *
* Last revis.: 2014-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_kci
*--- Carico il dizionario
cp_ReadXdc()
* --- Fine Area Manuale
return(createobject("tgsut_kci",oParentObject))

* --- Class definition
define class tgsut_kci as StdForm
  Top    = 2
  Left   = 2

  * --- Standard Properties
  Width  = 729
  Height = 624
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-13"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  CONF_INT_IDX = 0
  cPrg = "gsut_kci"
  cComment = "Configurazione interfaccia"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODUTE1 = 0
  o_CODUTE1 = 0
  w_CODUTE2 = 0
  o_CODUTE2 = 0
  w_CODUTE = 0
  o_CODUTE = 0
  w_CODUTE3 = 0
  w_CODUTE4 = 0
  w_DESUTE3 = space(20)
  w_CONFIG = space(20)
  w_FLDELCAC = space(1)
  w_DESUTE = space(20)

  * --- Children pointers
  GSUT_ACU = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_kci
  Dimension aConfigArray[1]
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSUT_ACU additive
    with this
      .Pages(1).addobject("oPag","tgsut_kciPag1","gsut_kci",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODUTE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSUT_ACU
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='CONF_INT'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSUT_ACU = CREATEOBJECT('stdDynamicChild',this,'GSUT_ACU',this.oPgFrm.Page1.oPag.oLinkPC_1_8)
    this.GSUT_ACU.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_ACU)
      this.GSUT_ACU.DestroyChildrenChain()
      this.GSUT_ACU=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_8')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_ACU.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_ACU.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_ACU.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSUT_ACU.SetKey(;
            .w_CODUTE,"CICODUTE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSUT_ACU.ChangeRow(this.cRowID+'      1',1;
             ,.w_CODUTE,"CICODUTE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSUT_ACU)
        i_f=.GSUT_ACU.BuildFilter()
        if !(i_f==.GSUT_ACU.cQueryFilter)
          i_fnidx=.GSUT_ACU.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_ACU.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_ACU.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_ACU.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_ACU.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSUT_ACU(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSUT_ACU.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSUT_ACU(i_ask)
    if this.GSUT_ACU.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (Configurazione utente)'))
      cp_BeginTrs()
      this.GSUT_ACU.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODUTE1=0
      .w_CODUTE2=0
      .w_CODUTE=0
      .w_CODUTE3=0
      .w_CODUTE4=0
      .w_DESUTE3=space(20)
      .w_CONFIG=space(20)
      .w_FLDELCAC=space(1)
      .w_DESUTE=space(20)
        .w_CODUTE1 = i_CODUTE
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODUTE1))
          .link_1_1('Full')
        endif
        .w_CODUTE2 = i_CODUTE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODUTE2))
          .link_1_2('Full')
        endif
        .w_CODUTE = IIF( Empty( .w_CODUTE2), -1 , IIF( i_bMobileMode, -10, 1) * .w_CODUTE2 )
        .w_CODUTE3 = iif(.w_CODUTE<-2, .w_CODUTE / -10, .w_CODUTE)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODUTE3))
          .link_1_4('Full')
        endif
        .w_CODUTE4 = .w_CODUTE
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODUTE4))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_CONFIG = .aConfigArray[1]
      .GSUT_ACU.NewDocument()
      .GSUT_ACU.ChangeRow('1',1,.w_CODUTE,"CICODUTE")
      if not(.GSUT_ACU.bLoaded)
        .GSUT_ACU.SetKey(.w_CODUTE,"CICODUTE")
      endif
        .w_FLDELCAC = 'N'
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
    this.DoRTCalc(9,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSUT_ACU.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_ACU.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
          .link_1_2('Full')
        if .o_CODUTE1<>.w_CODUTE1.or. .o_CODUTE2<>.w_CODUTE2
            .w_CODUTE = IIF( Empty( .w_CODUTE2), -1 , IIF( i_bMobileMode, -10, 1) * .w_CODUTE2 )
        endif
            .w_CODUTE3 = iif(.w_CODUTE<-2, .w_CODUTE / -10, .w_CODUTE)
          .link_1_4('Full')
        if .o_CODUTE<>.w_CODUTE
            .w_CODUTE4 = .w_CODUTE
          .link_1_5('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        if .o_CODUTE<>.w_CODUTE
          .Calculate_MRVTGALGAU()
        endif
        * --- Area Manuale = Calculate
        * --- gsut_kci
        if .o_CODUTE<>.w_CODUTE
            Local obj, i
            m.obj = .GetCtrl("w_CONFIG")
            m.i = 1
            Do While m.i <= Alen(m.obj.ComboValues)
              *-- Una volta eliminato il primo oggetto i rimanenti scalano verso l'alto, per rimuoverli tutti bisogna rimuovere sempre il primo
              m.obj.RemoveItem(1)
              m.i = m.i+1
            EndDo
            m.obj.CreateArray()
        endif
        * --- Fine Area Manuale
        if .w_CODUTE<>.o_CODUTE
          .Save_GSUT_ACU(.t.)
          .GSUT_ACU.NewDocument()
          .GSUT_ACU.ChangeRow('1',1,.w_CODUTE,"CICODUTE")
          if not(.GSUT_ACU.bLoaded)
            .GSUT_ACU.SetKey(.w_CODUTE,"CICODUTE")
          endif
        endif
      endwith
      this.DoRTCalc(6,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
  return

  proc Calculate_LJSAXOSBIX()
    with this
          * --- Aggiorna da eliminazione utente
          GSUT_BEU(this;
              ,'Eliminato';
             )
    endwith
  endproc
  proc Calculate_MRVTGALGAU()
    with this
          * --- Controllo utente se mobile
          CheckUserMOBY(this;
             )
    endwith
  endproc
  proc Calculate_JOXQQTHIPZ()
    with this
          * --- Calcolo CODUTE
          CalcComboCODUTE(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.GSUT_ACU.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.visible=!this.oPgFrm.Page1.oPag.oBtn_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_15.visible=!this.oPgFrm.Page1.oPag.oBtn_1_15.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
        if lower(cEvent)==lower("Aggiorna")
          .Calculate_LJSAXOSBIX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_JOXQQTHIPZ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODUTE1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTE1)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE1 = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE1 = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE2
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_INT_IDX,3]
    i_lTable = "CONF_INT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2], .t., this.CONF_INT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where CICODUTE="+cp_ToStrODBC(this.w_CODUTE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODUTE',this.w_CODUTE2)
            select CICODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE2 = NVL(_Link_.CICODUTE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE2 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])+'\'+cp_ToStr(_Link_.CICODUTE,1)
      cp_ShowWarn(i_cKey,this.CONF_INT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE3
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTE3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTE3)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE3 = NVL(_Link_.CODE,0)
      this.w_DESUTE3 = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE3 = 0
      endif
      this.w_DESUTE3 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE4
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_INT_IDX,3]
    i_lTable = "CONF_INT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2], .t., this.CONF_INT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where CICODUTE="+cp_ToStrODBC(this.w_CODUTE4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODUTE',this.w_CODUTE4)
            select CICODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE4 = NVL(_Link_.CICODUTE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE4 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])+'\'+cp_ToStr(_Link_.CICODUTE,1)
      cp_ShowWarn(i_cKey,this.CONF_INT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_3.RadioValue()==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFIG_1_7.RadioValue()==this.w_CONFIG)
      this.oPgFrm.Page1.oPag.oCONFIG_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDELCAC_1_9.RadioValue()==this.w_FLDELCAC)
      this.oPgFrm.Page1.oPag.oFLDELCAC_1_9.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSUT_ACU.CheckForm()
      if i_bres
        i_bres=  .GSUT_ACU.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODUTE1 = this.w_CODUTE1
    this.o_CODUTE2 = this.w_CODUTE2
    this.o_CODUTE = this.w_CODUTE
    * --- GSUT_ACU : Depends On
    this.GSUT_ACU.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsut_kciPag1 as StdContainer
  Width  = 725
  height = 624
  stdWidth  = 725
  stdheight = 624
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCODUTE_1_3 as StdTableCombo with uid="VJYILSPQLN",rtseq=3,rtrep=.f.,left=113,top=4,width=485,height=21;
    , ToolTipText = "Impostazioni per utente o generali (applicate all'avvio)";
    , HelpContextID = 78663718;
    , cFormVar="w_CODUTE",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='gsut_kci.vqr',cKey='code',cValue='name',cOrderBy='',xDefault=0;
  , bGlobalFont=.t.



  add object oCONFIG_1_7 as StdDefCombo with uid="PATOWSMUPE",rtseq=7,rtrep=.f.,left=25,top=583,width=111,height=22;
    , HelpContextID = 99741734;
    , cFormVar="w_CONFIG",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.



  add object oLinkPC_1_8 as stdDynamicChildContainer with uid="CPDTQLZRPT",left=5, top=28, width=712, height=541, bOnScreen=.t.;


  func oLinkPC_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (cp_IsAdministrator() or (.w_CODUTE<>-1 and .w_CODUTE<>-2))
      endwith
    endif
  endfunc

  add object oFLDELCAC_1_9 as StdCheck with uid="NXJIZVNZSP",rtseq=8,rtrep=.f.,left=202, top=584, caption="Elimina temporaneo immagini",;
    ToolTipText = "Se attivo, svuota la cartella delle immagini temporanee presenti sul pc corrente alla chiusura della procedura",;
    HelpContextID = 35671449,;
    cFormVar="w_FLDELCAC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDELCAC_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLDELCAC_1_9.GetRadio()
    this.Parent.oContained.w_FLDELCAC = this.RadioValue()
    return .t.
  endfunc

  func oFLDELCAC_1_9.SetRadio()
    this.Parent.oContained.w_FLDELCAC=trim(this.Parent.oContained.w_FLDELCAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLDELCAC=='S',1,;
      0)
  endfunc


  add object oObj_1_11 as cp_runprogram with uid="HRGIWCSBND",left=2, top=673, width=210,height=19,;
    caption='GSUT_BEU(Setup)',;
   bGlobalFont=.t.,;
    prg="GSUT_BEU('Setup')",;
    cEvent = "Update end",;
    nPag=1;
    , ToolTipText = "Aggiorna le variabili pubbliche";
    , HelpContextID = 97842629


  add object oBtn_1_13 as StdButton with uid="QMLXLHRBFG",left=522, top=575, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per cancellare la configurazione utente";
    , HelpContextID = 253410630;
    , Caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"Elimina")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CODUTE=-1 or .w_CODUTE=-2 or .w_CODUTE=0)
     endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="CNGPCTDKCH",left=572, top=575, width=48,height=45,;
    CpPicture="SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 94280922;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        do Save_CONFINT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CODUTE<>i_CODUTE and (cp_IsAdministrator() or .w_CODUTE<>-1 and .w_CODUTE<>-2))
      endwith
    endif
  endfunc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CODUTE=i_CODUTE or .w_CODUTE=-1)
     endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="KFKRTKGBHY",left=572, top=575, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 99152666;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.w_CODUTE=i_CODUTE or .w_CODUTE=-1) and (cp_IsAdministrator() or .w_CODUTE<>-1 and .w_CODUTE<>-2))
      endwith
    endif
  endfunc

  func oBtn_1_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CODUTE<>i_CODUTE and .w_CODUTE<>-1)
     endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="KRWSHEDFOQ",left=622, top=575, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare le operazioni svolte";
    , HelpContextID = 91855802;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="RMYWHPSWON",left=671, top=575, width=48,height=45,;
    CpPicture="BMP\applica.ico", caption="", nPag=1;
    , ToolTipText = "Premere per applicare le modifiche (non tutte le modifiche saranno applicate sulle maschere gia aperte)";
    , HelpContextID = 68825350;
    , caption='\<Applica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSUT_BEU(this.Parent.oContained,"Applica")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.w_CODUTE=IIF( i_bMobileMode, -10, 1)*i_CODUTE or Empty(.w_CODUTE2) and inlist(.w_CODUTE,-1,-2)) and (cp_IsAdministrator() or not inlist(.w_CODUTE,-1,-2)))
      endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="QMSWVJVPLF",left=140, top=575, width=50,height=45,;
    CpPicture="bmp\predefinito.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per impostare la configurazione selezionata";
    , HelpContextID = 172151162;
    , Caption='\<Imposta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        GSUT_BCI(.GSUT_ACU,"Style-"+.w_CONFIG)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (cp_IsAdministrator() or .w_CODUTE<>-1 and .w_CODUTE<>-2)
      endwith
    endif
  endfunc

  add object oStr_1_10 as StdString with uid="EOVKWBUKGM",Visible=.t., Left=10, Top=4,;
    Alignment=1, Width=99, Height=18,;
    Caption="Validi per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="GWZRCAMYQQ",Visible=.t., Left=415, Top=589,;
    Alignment=0, Width=60, Height=18,;
    Caption="C.m. : 50%"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="OMPWWEBOOG",Visible=.t., Left=202, Top=605,;
    Alignment=0, Width=199, Height=18,;
    Caption="C.m. : Costo medio sulle prestazioni"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kci','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kci
Proc CheckUserMOBY(obj)
   if g_MOBY='S'
     if obj.w_CODUTE<-2 and not cp_IsAdministrator() and not obj.checkform()
       obj.w_CODUTE=i_CODUTE
     endif
   endif
Endproc

Proc Save_CONFINT(w_obj)
   w_obj.NotifyEvent('Aggiorna')
   local i_nCICODUTE
   i_nCICODUTE=w_obj.w_CODUTE
   CalcComboCODUTE(w_obj)
   w_obj.w_CODUTE=i_nCICODUTE
Endproc

Proc CalcComboCODUTE(w_obj)
   local i_oComboCODUTE
   i_oComboCODUTE=w_obj.GetCtrl("w_CODUTE")
   for i_j=alen(i_oComboCODUTE.combovalues) to 1 step -1
     adel(i_oComboCODUTE.combovalues,i_j)
     IF i_oComboCODUTE.ListCount>0 AND i_j <= i_oComboCODUTE.ListCount
     	 i_oComboCODUTE.RemoveItem(i_j)
     ENDIF
   endfor
   i_oComboCODUTE.Init()
   i_oComboCODUTE.RemoveItem(1)
   ADEL(i_oComboCODUTE.combovalues,1)
   DIMENSION i_oComboCODUTE.combovalues(ALEN(i_oComboCODUTE.combovalues)-1)
   i_oComboCODUTE.nValues = ALEN(i_oComboCODUTE.combovalues)
Endproc

Define Class StdDefCombo As StdtableCombo
  
  Proc Init()

		This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
		This.ToolTipText=cp_Translate(This.ToolTipText)

    This.CreateArray()

		If This.bSetFont
			This.SetFont()
		Endif
		This.ToolTipText=cp_Translate(This.ToolTipText)
    This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.SpecialEffect = i_nSpecialEffect
		This.BorderColor = i_nBorderColor
		Return
  Endproc
  
  Proc CreateArray()
    local i,cDefFile,cThemeFile, cVar
    m.cDefFile = cPathVfcsim+'\Themes\'
    m.cDefFile = m.cDefFile + 'Default_'+iif(IsAhr(),'AHR',iif(IsAhe(),'AHE','AETOP'))+'.xml'
    i=1
    
	  oXML = CREATEOBJECT('MSXML2.DomDocument')
	  oXML.ASYNC = .F.
	  oXML.LOAD(m.cDefFile)

	  IF Type("oXML.documentElement")='O' AND Not IsNull(oXML.documentElement)
		  oRootNode = oXML.documentElement
		  cRootTagName = oRootNode.tagName
		  oNodeList = oRootNode.getElementsByTagName("*")
		  nNumNodes = oNodeList.LENGTH
      
		  FOR nPos = 0 TO (nNumNodes-1) STEP 1
		    oNode = oNodeList.ITEM(nPos)
		    cParentName = oNode.nodeName
		    bHasChild = oNode.hasChildNodes()
		    nType = oNode.nodeType

		    IF nType = 1
		      cAttrName = oNode.getAttribute("Name")
          cAttrCondition = oNode.getAttribute("Attivazione")
          cAttrCondition = StrTran(cAttrCondition,"w_","ThisForm.w_")
		    ENDIF

		    IF bHasChild
		      IF oNode.firstChild.nodeType = 1 And (Empty(cAttrCondition) Or (Not IsNull(cAttrCondition) And Evaluate(cAttrCondition)))
            Dimension This.combovalues[m.i]
            This.combovalues[m.i] = cParentName
            cParentName = StrTran(cParentName,'_',' ')
            This.AddItem(cp_Translate(Proper(cParentName)))
            m.i=m.i+1
          Endif
        Endif
      Next
    Endif
    This.nValues=alen(This.combovalues)
    Dimension ThisForm.aConfigArray[Alen(This.combovalues)]
    ACOPY(This.combovalues, ThisForm.aConfigArray)
    cVar = This.cFormVar
    *-- Viene fatto l'assegnamento per l'import da XML
    *-- Ce n'� anche uno alla init del campo perch� viene sbiancato dal prg
    ThisForm.&cVar = ThisForm.aConfigArray[1]
  Endproc

Enddefine
* --- Fine Area Manuale
