* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bdl                                                        *
*              Aggiornamento data liquidazione                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_47]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-21                                                      *
* Last revis.: 2002-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bdl",oParentObject)
return(i_retval)

define class tgsve_bdl as StdBatch
  * --- Local variables
  w_AGEINI = space(5)
  w_AGEFIN = space(5)
  w_DATA2 = ctod("  /  /  ")
  w_DATA1 = ctod("  /  /  ")
  w_VALUTA = space(3)
  w_ROWNUM = 0
  w_SERIAL = space(10)
  w_LIQAGE = 0
  w_LIQCAP = 0
  w_Messaggio = space(10)
  w_MPLIQAGE = 0
  w_MPLIQCAP = 0
  w_ANNLIQ = space(4)
  w_MPSERIAL = space(10)
  w_CPROWNUM = 0
  * --- WorkFile variables
  MOP_DETT_idx=0
  PRO_LIQU_idx=0
  MOP_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSVE_APL riassegna data liquidazione 
    this.w_DATA2 = this.oParentObject.w_PLDATFIN
    this.w_DATA1 = this.oParentObject.w_PLDATINI
    this.w_AGEINI = this.oParentObject.w_PLCODAGE
    this.w_AGEFIN = this.oParentObject.w_PLCODAGE
    this.w_VALUTA = this.oParentObject.w_PLCODVAL
    vq_exec("query\GSVE_BDL.vqr",this,"MOVI")
    vq_exec("query\GSVE1BDL.VQR",this,"ALTMOV")
    SELECT MOVI
    if RECCOUNT("MOVI")>0
      this.w_Messaggio = "ATTENZIONE%0Se esistenti, saranno cancellate automaticamente anche le liquidazioni collegate:%0Si desidera continuare?"
      if ah_YesNo(this.w_Messaggio)
        ah_Msg("Aggiornamento data liquidazione")
        * --- Elimina Data Liquidazione dai relativi Movimenti
        select MOVI
        go top
        scan
        this.w_LIQAGE = NVL(MPLIQAGE,0)
        this.w_LIQCAP = NVL(MPLIQCAP,0)
        if this.oParentObject.w_PLNUMREG=this.w_LIQAGE OR this.oParentObject.w_PLNUMREG=this.w_LIQCAP
          this.w_SERIAL = MOVI.MPSERIAL
          this.w_ROWNUM = MOVI.CPROWNUM
          * --- Write into MOP_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MOP_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MPDATLIQ ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'MOP_DETT','MPDATLIQ');
            +",MPANNLIQ ="+cp_NullLink(cp_ToStrODBC(SPACE(4)),'MOP_DETT','MPANNLIQ');
            +",MPLIQAGE ="+cp_NullLink(cp_ToStrODBC(0),'MOP_DETT','MPLIQAGE');
            +",MPLIQCAP ="+cp_NullLink(cp_ToStrODBC(0),'MOP_DETT','MPLIQCAP');
                +i_ccchkf ;
            +" where ";
                +"MPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                   )
          else
            update (i_cTable) set;
                MPDATLIQ = cp_CharToDate("  -  -  ");
                ,MPANNLIQ = SPACE(4);
                ,MPLIQAGE = 0;
                ,MPLIQCAP = 0;
                &i_ccchkf. ;
             where;
                MPSERIAL = this.w_SERIAL;
                and CPROWNUM = this.w_ROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          select MOVI
          this.w_ANNLIQ = NVL(MPANNLIQ,SPACE(4))
          if NOT EMPTY(this.w_LIQAGE)
            * --- Delete from PRO_LIQU
            i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"PLNUMREG = "+cp_ToStrODBC(this.w_LIQAGE);
                    +" and PL__ANNO = "+cp_ToStrODBC(this.w_ANNLIQ);
                     )
            else
              delete from (i_cTable) where;
                    PLNUMREG = this.w_LIQAGE;
                    and PL__ANNO = this.w_ANNLIQ;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            select MOVI
          endif
          if NOT EMPTY(this.w_LIQCAP)
            * --- Delete from PRO_LIQU
            i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"PLNUMREG = "+cp_ToStrODBC(this.w_LIQCAP);
                    +" and PL__ANNO = "+cp_ToStrODBC(this.w_ANNLIQ);
                     )
            else
              delete from (i_cTable) where;
                    PLNUMREG = this.w_LIQCAP;
                    and PL__ANNO = this.w_ANNLIQ;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            select MOVI
          endif
          * --- cancella anche quelle collegate
          SELECT ALTMOV
          GO TOP
          SCAN FOR ((NVL(MPLIQAGE, 0)= this.w_LIQAGE and this.w_LIQAGE<>0) OR (NVL(MPLIQCAP, 0)=this.w_LIQCAP AND this.w_LIQCAP<>0)) AND ALLTRIM(NVL(MPANNLIQ,SPACE(4)))=ALLTRIM(this.w_ANNLIQ)
          this.w_MPLIQAGE = NVL(MPLIQAGE,0)
          this.w_MPLIQCAP = NVL(MPLIQCAP,0)
          this.w_MPSERIAL = NVL(MPSERIAL,SPACE(10))
          this.w_CPROWNUM = NVL(CPROWNUM,0)
          * --- Delete from PRO_LIQU
          i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PLNUMREG = "+cp_ToStrODBC(this.w_MPLIQCAP);
                  +" and PL__ANNO = "+cp_ToStrODBC(this.w_ANNLIQ);
                   )
          else
            delete from (i_cTable) where;
                  PLNUMREG = this.w_MPLIQCAP;
                  and PL__ANNO = this.w_ANNLIQ;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Write into MOP_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MOP_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MPDATLIQ ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'MOP_DETT','MPDATLIQ');
            +",MPANNLIQ ="+cp_NullLink(cp_ToStrODBC(SPACE(4)),'MOP_DETT','MPANNLIQ');
            +",MPLIQAGE ="+cp_NullLink(cp_ToStrODBC(0),'MOP_DETT','MPLIQAGE');
            +",MPLIQCAP ="+cp_NullLink(cp_ToStrODBC(0),'MOP_DETT','MPLIQCAP');
                +i_ccchkf ;
            +" where ";
                +"MPSERIAL = "+cp_ToStrODBC(this.w_MPSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                MPDATLIQ = cp_CharToDate("  -  -  ");
                ,MPANNLIQ = SPACE(4);
                ,MPLIQAGE = 0;
                ,MPLIQCAP = 0;
                &i_ccchkf. ;
             where;
                MPSERIAL = this.w_MPSERIAL;
                and CPROWNUM = this.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          select MOVI
          ENDSCAN
        endif
        endscan
      else
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_Msgformat("Eliminazione annullata")
        i_retcode = 'stop'
        return
      endif
    endif
    if Used("MOVI")
      Select MOVI
      Use
    endif
    if Used("ALTMOV")
      Select ALTMOV
      Use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MOP_DETT'
    this.cWorkTables[2]='PRO_LIQU'
    this.cWorkTables[3]='MOP_MAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
