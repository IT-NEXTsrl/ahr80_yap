* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bgf                                                        *
*              Libreria gestione file                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_308]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-12                                                      *
* Last revis.: 2011-01-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Operazione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bgf",oParentObject,m.w_Operazione)
return(i_retval)

define class tgsut_bgf as StdBatch
  * --- Local variables
  w_Operazione = space(1)
  w_NOMEFILE = space(20)
  w_TIPOFILE = space(50)
  w_posiz = 0
  w_PATH = space(250)
  w_ChiaveRicerca = space(20)
  w_CritPeri = space(2)
  w_LIPATIMG = space(254)
  w_ImgPat = space(254)
  w_TmpPat = space(520)
  w_MemFile = space(10)
  w_ChkBuffer = space(10)
  w_Volume = space(10)
  w_Ok = .f.
  w_FILEDEL = space(20)
  w_curdir = space(50)
  w_CATCH = space(250)
  w_posizpunto = 0
  w_posiz2punto = 0
  w_range = 0
  w_estensione = space(10)
  w_numfile = 0
  w_MAX = 0
  w_cont = 0
  w_indice = space(5)
  w_destinazione = space(250)
  w_sorgente = space(250)
  w_MARK_BMP = space(10)
  w_MARK_DCX = space(10)
  w_MARK_EPS = space(10)
  w_MARK_JPG = space(10)
  w_MARK_TIF = space(10)
  w_MARK_GIF = space(10)
  w_MARK = space(10)
  w_SHELLEXEC = 0
  w_KEYINDIC = space(20)
  w_PATHFILE = space(254)
  w_ORIGFILE = space(200)
  w_CLASDOCU = space(15)
  w_IDWEBFIL = space(254)
  w_IDMODALL = space(1)
  w_PADRE = .NULL.
  w_GEST = .NULL.
  w_BTNCAT = .NULL.
  w_IDWEBAPP = space(1)
  w_PROG = .NULL.
  w_cLink = space(255)
  w_cParms = space(10)
  * --- WorkFile variables
  LIB_IMMA_idx=0
  GESTFILE_idx=0
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione file associati ad articoli o documenti. Questo Batch viene richiamato dalla maschera gsut_kgf.
    * --- Operazione richiesta ('C' = Cattura, 'V' = Visualizza, 'D' = Elimina, 'M' = Modifica, 'O' = bottoncino per visualizzare)
    this.w_NOMEFILE = space(20)
    this.w_TIPOFILE = space(50)
    this.w_posiz = 1
    this.w_PATH = space(250)
    this.w_ChiaveRicerca = space(20)
    * --- Path della libreria contenente le cartelle con le immagini (In base alle aziende)
    this.w_LIPATIMG = ""
    this.w_ImgPat = ""
    this.w_TmpPat = ""
    this.w_MemFile = ""
    this.w_ChkBuffer = ""
    this.w_Volume = ""
    this.w_Ok = .T.
    * --- File da cancellare
    this.w_FILEDEL = space(20)
    * --- Directory corrente
    this.w_curdir = space(50)
    * --- File da catturare
    this.w_CATCH = space(250)
    * --- Posizione primo punto nel nome del file, partendo da dx
    this.w_posizpunto = 0
    * --- Posizione secondo punto nel nome del file, partendo da dx
    this.w_posiz2punto = 0
    * --- Numero di caratteri tra i due punti
    this.w_range = 0
    * --- Estensione del file
    this.w_estensione = space(10)
    * --- Numero dei file corrispondenti alle caratteristiche indicate
    this.w_numfile = 0
    * --- Massimo progressivo
    this.w_MAX = 0
    * --- Contatori
    this.w_cont = 1
    this.w_indice = space(5)
    * --- Nome del file in cui sar� memorizzato il file catturato o rinominato
    this.w_destinazione = space(250)
    * --- Path del file da rinominare
    this.w_sorgente = space(250)
    * --- Tipologie di file supportate da Scan & View
    * --- Codifica ASCII dei primi 4 caratteri presenti nei file
    this.w_MARK_BMP = Chr(0x42)+Chr(0x4d)+Chr(0x46)+Chr(0x16)
    this.w_MARK_DCX = Chr(0xb1)+Chr(0x68)+Chr(0xde)+Chr(0x3a)
    this.w_MARK_EPS = Chr(0x25)+Chr(0x21)+Chr(0x50)+Chr(0x53)
    this.w_MARK_JPG = Chr(0xff)+Chr(0xd8)+Chr(0xff)+Chr(0xe0)
    this.w_MARK_TIF = Chr(0x49)+Chr(0x49)+Chr(0x2a)+Chr(0x00)
    this.w_MARK_GIF = Chr(0x47)+Chr(0x49)+Chr(0x46)+Chr(0x38)
    this.w_MARK = space(10)
    this.w_SHELLEXEC = 0
    this.w_PADRE = this.oParentObject
    do case
      case Left(this.w_Operazione,1)="C"
        * --- Premuto bottone Aggiungi nella maschera di visualizzazione.
        *     Mi appoggio al bottone Cattura della Gestione Chiamante (Articoli, Documenti, Clienti...)
        *     Se al bottone Cattura si modifica la caption, questo metodo non funziona
        *     La variabile g_GSUT_KGF creata come pubblica nell' Area Manuale Header della Maschera GSUT_KGF
        *     server per poter eseguire l'aggiorna nella stessa maschera dopo che ho Aggiunto un nuovo file
        *     Utilizzata in GSUT_BSA e eliminata in GSUT_KGF alla Done
        g_GSUT_KGF = this.w_PADRE
        this.w_GEST = This.oParentObject.oParentObject.oParentObject
        * --- Se la gestione � ancora aperta
        if Type("This.w_GEST.Class")<>"U"
          if Right(this.w_Operazione,1)= "T"
            this.w_BTNCAT = this.w_GEST.GetCtrl(Ah_Msgformat("Scanner"))
          else
            this.w_BTNCAT = this.w_GEST.GetCtrl(Ah_Msgformat("Cattura"))
          endif
          if Type("this.w_BTNCAT") ="O"
            this.w_BTNCAT.Click()     
          else
            ah_ErrorMsg("� stata modificata la propriet� UID del bottone di acquisizione immagine","i","")
          endif
        else
          ah_ErrorMsg("La gestione %1 deve rimanere aperta","i","", Alltrim(this.oParentObject.w_GESTIONE) )
        endif
      case Left(this.w_Operazione,1)="V"
        * --- Visualizza Immagini
        this.w_TmpPat = alltrim(this.oParentObject.w_GFPATHFILE)
        if Not Empty(this.w_TmpPat)
          if File(this.w_TmpPat)
            this.w_SHELLEXEC = 0
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_SHELLEXEC <= 32
              * --- Determina estensione file
              this.w_estensione = lower(iif(RAT(".",this.oParentObject.w_GFNOMEFILE)>0,RIGHT(this.oParentObject.w_GFNOMEFILE,LEN(this.oParentObject.w_GFNOMEFILE)-RAT(".",this.oParentObject.w_GFNOMEFILE)+1),""))
              ah_ErrorMsg("Non � stato possibile aprire/visualizzare il file %1%0Se non � definito un programma per l'estensione %2, � necessario%0associarne uno con l'operazione di 'apri con' dalla gestione file di Windows","i","", Alltrim(this.oParentObject.w_GFNOMEFILE),upper(alltrim(this.w_estensione)) )
            endif
          else
            ah_ErrorMsg("Il file allegato non esiste","i","")
          endif
        else
          ah_ErrorMsg("Nessun file associato","i","")
        endif
      case Left(this.w_Operazione,1)="D"
        if not(Empty(this.oParentObject.w_GFNOMEFILE))
          if Upper(this.oParentObject.w_COPIACOLL) = "COPIA"
            this.w_Ok = ah_YesNo("Elimino il file %1?","", alltrim(this.oParentObject.w_GFNOMEFILE) )
          else
            this.w_Ok = ah_YesNo("Annullo collegamento al file?")
          endif
          this.w_TmpPat = alltrim(this.oParentObject.w_GFPATHFILE)
          if this.w_Ok
            if Upper(this.oParentObject.w_COPIACOLL) = "COPIA"
              * --- Se Copia file elimino il file
              *     Se collegamento elimino solo il record Da GESTFILE
              erase (this.w_TmpPat)
            endif
            * --- Read from GESTFILE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.GESTFILE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2],.t.,this.GESTFILE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "GFWEBFIL,GFWEBAPP"+;
                " from "+i_cTable+" GESTFILE where ";
                    +"GFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_GFSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                GFWEBFIL,GFWEBAPP;
                from (i_cTable) where;
                    GFSERIAL = this.oParentObject.w_GFSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_IDWEBFIL = NVL(cp_ToDate(_read_.GFWEBFIL),cp_NullValue(_read_.GFWEBFIL))
              this.w_IDWEBAPP = NVL(cp_ToDate(_read_.GFWEBAPP),cp_NullValue(_read_.GFWEBAPP))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_IDWEBAPP$"AS"
              * --- Read from CONTROPA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTROPA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "COPATHDO"+;
                  " from "+i_cTable+" CONTROPA where ";
                      +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  COPATHDO;
                  from (i_cTable) where;
                      COCODAZI = i_CODAZI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_COPATHDO = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_COPATHDO = ADDBS(this.w_COPATHDO)
              erase (alltrim(this.w_COPATHDO)+JUSTFNAME(this.w_IDWEBFIL))
            endif
            * --- Elimino l'associazione in GESTFILE
            * --- Delete from GESTFILE
            i_nConn=i_TableProp[this.GESTFILE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"GFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_GFSERIAL);
                     )
            else
              delete from (i_cTable) where;
                    GFSERIAL = this.oParentObject.w_GFSERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
        endif
        this.w_PADRE.NotifyEvent("Interroga")     
      case Left(this.w_Operazione,1)="M"
        * --- Premuto bottone Modifica dalla Visualizzazione Allegati
        g_GSUT_KGF = this.w_PADRE
        this.w_PROG = GSUT_AGF()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Richiamo il record giusto
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_GFSERIAL = this.oParentObject.w_GFSERIAL
        * --- Uso questi metodi invece della EcpSave() poich� quest'ultima
        *     � ridefinita nell'area manuale della GSUT_AGF e farebbe chiudere la maschera.
        *     Quindi recupero direttamente i metodi dalla Cp_Forms
        this.w_PROG.QueryKeySet(this.w_PROG.BuildFilter(),"")     
        this.w_PROG.LoadRecWarn()     
        this.w_PROG.ecpQuery()     
        * --- Mi metto in Modifica
        this.w_PROG.ecpEdit()     
      case Left(this.w_Operazione,1)="O"
        this.w_TmpPat = alltrim(this.oParentObject.w_GFPATHFILE)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia eseguibile associato al file
    this.w_cLink = this.w_TmpPat
    this.w_cParms = space(1)
    declare integer ShellExecute in shell32.dll integer nHwnd, string cOperation, string cFileName, string cParms, string cDir, integer nShowCmd
     declare integer FindWindow in win32api string cNull, string cWinName 
    w_hWnd = FindWindow(0,_screen.caption)
    this.w_SHELLEXEC = ShellExecute(w_hWnd,"open",alltrim(this.w_cLink),alltrim(this.w_cParms),sys(5)+curdir(),1)
  endproc


  proc Init(oParentObject,w_Operazione)
    this.w_Operazione=w_Operazione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='LIB_IMMA'
    this.cWorkTables[2]='GESTFILE'
    this.cWorkTables[3]='CONTROPA'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Operazione"
endproc
