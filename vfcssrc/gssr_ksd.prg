* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_ksd                                                        *
*              "+IIF(Upper(g_APPLICATION)='ADHOC REVOLUTION',"Storicizzazione documenti","Storicizzazione documenti/produzione")+"*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_49]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-06                                                      *
* Last revis.: 2009-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gssr_ksd
* Controllo che il sistema sia BLOCCATO
if !g_LOCKALL
  Ah_errormsg("La storicizzazione dei documenti pu� essere eseguita%0solo con il sistema in manutenzione%0Portare il sistema in manutenzione e ripetere l'operazione","STOP")
  return
endif

* --- Fine Area Manuale
return(createobject("tgssr_ksd",oParentObject))

* --- Class definition
define class tgssr_ksd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 726
  Height = 354
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-15"
  HelpContextID=99181929
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gssr_ksd"
  cComment = ""+IIF(Upper(g_APPLICATION)='ADHOC REVOLUTION',"Storicizzazione documenti","Storicizzazione documenti/produzione")+""
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CodAzi = space(5)
  w_CODESE = space(4)
  w_CHECKLIB = space(1)
  w_TRAN = space(10)
  w_Msg = space(0)
  w_xesiniese = ctod('  /  /  ')
  w_xesfinese = ctod('  /  /  ')
  w_VALNAZ = space(3)
  w_CONVEN = ctod('  /  /  ')
  w_CONACQ = ctod('  /  /  ')
  w_CONMAG = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgssr_ksdPag1","gssr_ksd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ESERCIZI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CodAzi=space(5)
      .w_CODESE=space(4)
      .w_CHECKLIB=space(1)
      .w_TRAN=space(10)
      .w_Msg=space(0)
      .w_xesiniese=ctod("  /  /  ")
      .w_xesfinese=ctod("  /  /  ")
      .w_VALNAZ=space(3)
      .w_CONVEN=ctod("  /  /  ")
      .w_CONACQ=ctod("  /  /  ")
      .w_CONMAG=ctod("  /  /  ")
        .w_CodAzi = i_codazi
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODESE))
          .link_1_2('Full')
        endif
        .w_CHECKLIB = 'S'
        .w_TRAN = 'S'
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate(ah_msgformat("La storicizzazione dei documenti%0� una operazione non reversibile da%0eseguire a seguito di%0Backup del database."),0)
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate(ah_msgformat("Disattivare questa opzione solo nel%0caso l'elaborazione presenti rischi di%0fallimento dovuti all'eccessiva mole%0della transazione."),0)
    endwith
    this.DoRTCalc(5,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CodAzi = i_codazi
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(ah_msgformat("La storicizzazione dei documenti%0� una operazione non reversibile da%0eseguire a seguito di%0Backup del database."),0)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(ah_msgformat("Disattivare questa opzione solo nel%0caso l'elaborazione presenti rischi di%0fallimento dovuti all'eccessiva mole%0della transazione."),0)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(ah_msgformat("La storicizzazione dei documenti%0� una operazione non reversibile da%0eseguire a seguito di%0Backup del database."),0)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(ah_msgformat("Disattivare questa opzione solo nel%0caso l'elaborazione presenti rischi di%0fallimento dovuti all'eccessiva mole%0della transazione."),0)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CodAzi);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ,ESCONVEN,ESCONACQ,ESCONMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CodAzi;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ,ESCONVEN,ESCONACQ,ESCONMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CodAzi<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ,ESCONVEN,ESCONACQ,ESCONMAG";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ,ESCONVEN,ESCONACQ,ESCONMAG;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ,ESCONVEN,ESCONACQ,ESCONMAG";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CodAzi);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ,ESCONVEN,ESCONACQ,ESCONMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ,ESCONVEN,ESCONACQ,ESCONMAG";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CodAzi);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CodAzi;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ,ESCONVEN,ESCONACQ,ESCONMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_xesiniese = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_xesfinese = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_CONVEN = NVL(cp_ToDate(_Link_.ESCONVEN),ctod("  /  /  "))
      this.w_CONACQ = NVL(cp_ToDate(_Link_.ESCONACQ),ctod("  /  /  "))
      this.w_CONMAG = NVL(cp_ToDate(_Link_.ESCONMAG),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_xesiniese = ctod("  /  /  ")
      this.w_xesfinese = ctod("  /  /  ")
      this.w_VALNAZ = space(3)
      this.w_CONVEN = ctod("  /  /  ")
      this.w_CONACQ = ctod("  /  /  ")
      this.w_CONMAG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_2.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_2.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCHECKLIB_1_3.RadioValue()==this.w_CHECKLIB)
      this.oPgFrm.Page1.oPag.oCHECKLIB_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRAN_1_4.RadioValue()==this.w_TRAN)
      this.oPgFrm.Page1.oPag.oTRAN_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_9.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_9.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page1.oPag.oxesiniese_1_10.value==this.w_xesiniese)
      this.oPgFrm.Page1.oPag.oxesiniese_1_10.value=this.w_xesiniese
    endif
    if not(this.oPgFrm.Page1.oPag.oxesfinese_1_11.value==this.w_xesfinese)
      this.oPgFrm.Page1.oPag.oxesfinese_1_11.value=this.w_xesfinese
    endif
    if not(this.oPgFrm.Page1.oPag.oVALNAZ_1_16.value==this.w_VALNAZ)
      this.oPgFrm.Page1.oPag.oVALNAZ_1_16.value=this.w_VALNAZ
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgssr_ksdPag1 as StdContainer
  Width  = 722
  height = 354
  stdWidth  = 722
  stdheight = 354
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESE_1_2 as StdField with uid="UZKGKBYPNV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio da storicizzare",;
    HelpContextID = 76557862,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=62, Top=116, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CodAzi", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CodAzi)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CodAzi)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCHECKLIB_1_3 as StdCheck with uid="HTCGTEHPRE",rtseq=3,rtrep=.f.,left=9, top=170, caption="Check libro giornale magazzino",;
    ToolTipText = "Se attivo la procedura verifica presenza di documenti/movimenti storicizzabili non stampati sul libro giornale di magazzino",;
    HelpContextID = 82954392,;
    cFormVar="w_CHECKLIB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHECKLIB_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCHECKLIB_1_3.GetRadio()
    this.Parent.oContained.w_CHECKLIB = this.RadioValue()
    return .t.
  endfunc

  func oCHECKLIB_1_3.SetRadio()
    this.Parent.oContained.w_CHECKLIB=trim(this.Parent.oContained.w_CHECKLIB)
    this.value = ;
      iif(this.Parent.oContained.w_CHECKLIB=='S',1,;
      0)
  endfunc

  add object oTRAN_1_4 as StdCheck with uid="OKZPTPWROC",rtseq=4,rtrep=.f.,left=9, top=194, caption="Sotto transazione",;
    ToolTipText = "Se abilitato esegue l'elaborazione sotto transazione",;
    HelpContextID = 93781450,;
    cFormVar="w_TRAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRAN_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oTRAN_1_4.GetRadio()
    this.Parent.oContained.w_TRAN = this.RadioValue()
    return .t.
  endfunc

  func oTRAN_1_4.SetRadio()
    this.Parent.oContained.w_TRAN=trim(this.Parent.oContained.w_TRAN)
    this.value = ;
      iif(this.Parent.oContained.w_TRAN=='S',1,;
      0)
  endfunc


  add object oBtn_1_6 as StdButton with uid="VFSEADJQCS",left=139, top=306, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare la storicizzazione";
    , HelpContextID = 224408598;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        do GSSR_BSD with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_CODESE ))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="URECPKNNNL",left=190, top=306, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 91864506;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMsg_1_9 as StdMemo with uid="ATBBCHAJSX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Messaggi elaborazione",;
    HelpContextID = 98729274,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=315, Width=464, Left=251, Top=28, tabstop = .f., readonly = .t.

  add object oxesiniese_1_10 as StdField with uid="XEFRKYYSTC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_xesiniese", cQueryName = "xesiniese",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Intervallo esercizio",;
    HelpContextID = 174538297,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=62, Top=145

  add object oxesfinese_1_11 as StdField with uid="DQJFHSTLSL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_xesfinese", cQueryName = "xesfinese",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Intervallo esercizio",;
    HelpContextID = 252984889,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=166, Top=145

  add object oVALNAZ_1_16 as StdField with uid="OFVQNWFYQU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_VALNAZ", cQueryName = "VALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 142188886,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=166, Top=116, InputMask=replicate('X',3)


  add object oObj_1_21 as cp_calclbl with uid="UIGSZZGHPZ",left=14, top=28, width=224,height=84,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 78815718


  add object oObj_1_22 as cp_calclbl with uid="ICPRVBKSAK",left=14, top=218, width=224,height=84,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 78815718

  add object oStr_1_5 as StdString with uid="ICYYTXIFDV",Visible=.t., Left=6, Top=120,;
    Alignment=1, Width=53, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="MNNAMJJFUG",Visible=.t., Left=251, Top=8,;
    Alignment=0, Width=63, Height=18,;
    Caption="Messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="RGZUEVVJFR",Visible=.t., Left=11, Top=6,;
    Alignment=2, Width=228, Height=19,;
    Caption="Attenzione"    , backstyle = 1, backcolor = 8454143, borderstyle = 1;
  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="OZXNAPJDAR",Visible=.t., Left=25, Top=149,;
    Alignment=1, Width=34, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="FEJUHCPVDO",Visible=.t., Left=139, Top=149,;
    Alignment=1, Width=23, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="AZNLZHHOZX",Visible=.t., Left=144, Top=120,;
    Alignment=1, Width=18, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  add object oBox_1_8 as StdBox with uid="IEHDGTHVHI",left=243, top=10, width=2,height=338
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gssr_ksd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
