* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bkm                                                        *
*              Recupera numero per menu per click2call                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-12-05                                                      *
* Last revis.: 2014-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFIELDNAM,pTABNAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bkm",oParentObject,m.pFIELDNAM,m.pTABNAM)
return(i_retval)

define class tgsut_bkm as StdBatch
  * --- Local variables
  pFIELDNAM = space(20)
  pTABNAM = space(20)
  w_RETVAL = space(50)
  w_RET_CURS = space(50)
  w_OLD_AREA = space(50)
  w_ARCHIVIO = space(30)
  w_IDXTABLE = 0
  w_CHIAVE = space(40)
  w_CAMPO = space(30)
  w_LOOP = 0
  w_INDICE = 0
  * --- WorkFile variables
  OFF_NOMI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LOOP = 0
    this.w_INDICE = 1
    =cp_ReadXdc()
    this.w_ARCHIVIO = ALLTRIM(this.pTABNAM)
    this.w_IDXTABLE = cp_OpenTable( this.w_ARCHIVIO ,.T.)
    this.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( this.w_ARCHIVIO ,1 ) )
    this.w_CHIAVE = ","+ALLTRIM(this.w_CHIAVE)+","
    do while NOT EMPTY(STREXTRACT( this.w_CHIAVE , ",",",",this.w_INDICE ))
      DIMENSION L_ArrKey(this.w_INDICE,2)
      this.w_CAMPO = STREXTRACT( this.w_CHIAVE , ",",",",this.w_INDICE )
      L_ArrKey(this.w_INDICE,1) = this.w_CAMPO
      L_Arrkey(this.w_INDICE,2)=g_oMenu.GetByIndexKeyValue(this.w_INDICE)
      this.w_INDICE = this.w_INDICE+1
    enddo
    this.w_RETVAL = ""
    this.w_RET_CURS = ""
    this.w_RET_CURS = readtable(this.w_ARCHIVIO, this.pFIELDNAM, @L_ArrKey)
    ALINES(L_ArrNumber,this.pFIELDNAM,4,",")
    if !EMPTY(this.w_RET_CURS)
      this.w_OLD_AREA = SELECT()
      SELECT (this.w_RET_CURS)
      GO TOP
      SCAN
      this.w_INDICE = 1
      do while  this.w_INDICE<=ALEN(L_ArrNumber,1)
        this.w_RETVAL = this.w_RETVAL + ( &L_ArrNumber(this.w_INDICE) )
        this.w_INDICE = this.w_INDICE+1
        this.w_RETVAL = alltrim(this.w_RETVAL)+"$"
      enddo
      SELECT (this.w_OLD_AREA)
      ENDSCAN
      USE IN SELECT(this.w_RET_CURS)
      this.w_RETVAL = left(this.w_RETVAL,len(this.w_RETVAL)-1)
    endif
    release L_ArrKey, L_ArrNumber
    i_retcode = 'stop'
    i_retval = ALLTRIM(NVL(this.w_RETVAL,""))
    return
  endproc


  proc Init(oParentObject,pFIELDNAM,pTABNAM)
    this.pFIELDNAM=pFIELDNAM
    this.pTABNAM=pTABNAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_NOMI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFIELDNAM,pTABNAM"
endproc
