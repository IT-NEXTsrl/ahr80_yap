* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bgz                                                        *
*              Eventi da gruppi di magazzini                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-30                                                      *
* Last revis.: 2012-10-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bgz",oParentObject,m.pAzione)
return(i_retval)

define class tgsar_bgz as StdBatch
  * --- Local variables
  w_GRCODMAG = space(5)
  w_NREC = 0
  w_NREC1 = .f.
  w_ANNULLA = .f.
  w_CONFKME = .f.
  w_DAIM = .f.
  w_NOBLOC = space(0)
  w_MEMO = space(0)
  w_oMess1 = .NULL.
  w_oMess2 = .NULL.
  w_oPart = .NULL.
  w_oPART1 = .NULL.
  w_PADRE = .NULL.
  w_GRGRUMAG = space(5)
  pAzione = space(10)
  * --- WorkFile variables
  GRUDMAG_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pAzione="CONTROLLI"
        this.w_oMess1=createobject("ah_message")
        this.w_oMess2=createobject("ah_message")
        this.w_PADRE = This.oParentObject
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          this.w_NREC = this.w_NREC + IIF(this.w_PADRE.w_GRMGPRIN = "S", 1, 0)
          this.w_GRCODMAG = NVL(this.w_PADRE.w_GRCODMAG, SPACE(5))
          this.w_NREC1 = .F.
          this.w_GRGRUMAG = ""
          * --- Select from GRUDMAG
          i_nConn=i_TableProp[this.GRUDMAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GRUDMAG_idx,2],.t.,this.GRUDMAG_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" GRUDMAG ";
                +" where GRCODICE<>"+cp_ToStrODBC(this.oParentObject.w_GRCODICE)+" AND GRCODMAG="+cp_ToStrODBC(this.w_GRCODMAG)+" ";
                 ,"_Curs_GRUDMAG")
          else
            select * from (i_cTable);
             where GRCODICE<>this.oParentObject.w_GRCODICE AND GRCODMAG=this.w_GRCODMAG ;
              into cursor _Curs_GRUDMAG
          endif
          if used('_Curs_GRUDMAG')
            select _Curs_GRUDMAG
            locate for 1=1
            do while not(eof())
            this.w_NREC1 = .T.
            this.w_GRGRUMAG = NVL(_Curs_GRUDMAG.GRCODICE, SPACE(5))
              select _Curs_GRUDMAG
              continue
            enddo
            use
          endif
          if this.w_NREC1
            this.w_oPART1 = this.w_oMess2.AddMsgPartNL("Il magazzino (%1) � gi� presente nel gruppo (%2)")
            this.w_oPART1.AddParam(Alltrim(this.w_GRCODMAG))     
            this.w_oPART1.AddParam(Alltrim(this.w_GRGRUMAG))     
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.RePos()     
        if this.w_NREC > 1
          this.w_oPART1 = this.w_oMess2.AddMsgPartNL("Selezionare solo un magazzino come predefinito")
        else
          if this.w_NREC = 0
            this.w_oPART1 = this.w_oMess2.AddMsgPartNL("Selezionare un magazzino come predefinito")
          endif
        endif
        this.w_MEMO = this.w_oMess2.ComposeMessage()
        if ! Empty (this.w_MEMO)
          * --- Visualizzo la maschera con all'interno tutti i messagi di errore
          this.w_ANNULLA = False
          this.w_CONFKME = False
          this.w_DAIM = False
          do GSVE_KLG with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_CONFKME = this.w_ANNULLA
        else
          if this.oParentObject.w_TESTFORM
            this.w_CONFKME = True
          else
            this.w_CONFKME = False
          endif
        endif
        this.oParentObject.w_TESTFORM = this.w_CONFKME
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='GRUDMAG'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GRUDMAG')
      use in _Curs_GRUDMAG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
