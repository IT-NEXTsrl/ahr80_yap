* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bed                                                        *
*              Elimina dic. di esenzione                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_101]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2017-12-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bed",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bed as StdBatch
  * --- Local variables
  pOper = space(1)
  w_IMPNAZ = 0
  w_MESS = space(10)
  w_APPO = space(40)
  w_RAGSOC = space(40)
  w_INDIRI = space(35)
  w_LOCALI = space(30)
  w_CODCAP = space(8)
  w_CODFIS = space(16)
  w_PROVIN = space(2)
  w_RISP = .f.
  w_RISP2 = .f.
  w_DECCOM = 0
  w_GSVE_MDV = .NULL.
  w_PARAM_FILTRO = space(1)
  w_SERIALE_DICHIARAZIONE = space(10)
  w_TIPOGEST = .NULL.
  w_TIPOMASK = .NULL.
  w_CLADOC = space(2)
  w_RECTRS = 0
  w_VALMAG = 0
  w_IMPNAZ = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Operazioni da Maschere Documento ( GSVE_KRT, GSVE_KFT, GSAC_KFT, GSOR_KFT)
    * --- Cambia: D = Dich.Intento; I = Inizio Competenza; F = Fine competenza; R = Intestaz.Ricevuta Fiscale; 'A' Cambio Agente 
    *                     C = Aggiorna Importo su commessa e livello di raggruppamento
    do case
      case this.pOper="D"
        this.w_MESS = "Confermi eliminazione riferimento dichiarazione di esenzione?"
        if ah_YesNo(this.w_MESS)
          this.w_TIPOGEST = This.oParentObject.oParentObject
          this.w_TIPOMASK = This.oParentObject
          this.w_TIPOGEST.w_MVCODIVE = Space(5)
          this.w_TIPOGEST.w_BOLIVE = Space(1)
          this.w_TIPOGEST.w_MVRIFDIC = Space(15)
          this.w_TIPOGEST.w_DICINTE_INDIRETTA = Space(15)
          this.w_TIPOGEST.w_NUMDIC = 0
          this.w_TIPOGEST.w_NUMDIC_COLL = 0
          this.w_TIPOGEST.w_DATDIC = {}
          this.w_TIPOGEST.w_DATDIC_COLL = {}
          this.w_TIPOGEST.w_ANNDIC = SPACE(4)
          this.w_TIPOGEST.w_ANNDIC_COLL = SPACE(4)
          this.w_TIPOGEST.w_SERDOC_COLL = SPACE(3)
          this.w_TIPOGEST.w_TIPDIC = SPACE(1)
          this.w_TIPOGEST.w_OPEDIC = SPACE(1)
          this.w_TIPOGEST.w_UTIDIC = 0
          this.w_TIPOGEST.w_PERIVE = 0
          this.w_TIPOGEST.w_TIPIVA = SPACE(1)
          this.w_TIPOMASK.w_MVCODIVE = Space(5)
          this.w_TIPOMASK.w_BOLIVE = Space(1)
          this.w_TIPOMASK.w_MVRIFDIC = Space(15)
          this.w_TIPOMASK.w_DICINTE_INDIRETTA = Space(15)
          this.w_TIPOMASK.w_NUMDIC = 0
          this.w_TIPOMASK.w_NUMDIC_COLL = 0
          this.w_TIPOMASK.w_DATDIC = {}
          this.w_TIPOMASK.w_DATDIC_COLL = {}
          this.w_TIPOMASK.w_ANNDIC = SPACE(4)
          this.w_TIPOMASK.w_ANNDIC_COLL = SPACE(4)
          this.w_TIPOMASK.w_SERDOC_COLL = SPACE(3)
          this.w_TIPOMASK.w_SERDOC = SPACE(3)
          this.w_TIPOMASK.w_TIPDIC = SPACE(1)
          this.w_TIPOMASK.w_OPEDIC = SPACE(1)
          this.w_TIPOMASK.w_UTIDIC = 0
          this.w_TIPOMASK.w_PERIVE = 0
          this.w_TIPOMASK.w_TIPIVA = SPACE(1)
        endif
      case this.pOper = "I"
        this.w_MESS = "Aggiorno le date inizio competenza presenti sulle righe?"
      case this.pOper="F"
        this.w_MESS = "Aggiorno le date fine competenza presenti sulle righe?"
      case this.pOper="A"
        * --- Cambio Agente (solo in GSVE_MDV, GSOR_MDV)
        if EMPTY(this.oParentObject.w_MVCODAGE)
          this.oParentObject.w_MVCODAG2 = SPACE(5)
        else
          if this.oParentObject.w_CODAG2<>this.oParentObject.w_MVCODAG2
            if ah_YesNo("Assegno il capoarea dell'agente selezionato?")
              this.oParentObject.w_MVCODAG2 = this.oParentObject.w_CODAG2
            endif
          endif
        endif
      case this.pOper="T"
        WITH this.oParentObject
        this.w_APPO = LEFT(.w_RAGSOC+SPACE(40),40)+LEFT(.w_INDIRI+SPACE(35),35)+LEFT(.w_CODCAP+ SPACE(8), 8) +LEFT(.w_LOCALI+SPACE(30), 30) +LEFT(.w_PROVIN+SPACE(2), 2) +LEFT(.w_CODFIS+SPACE(16), 16)
        this.w_RAGSOC = SUBSTR(this.w_APPO, 1, 40)
        this.w_INDIRI = SUBSTR(this.w_APPO, 41, 35)
        this.w_CODCAP = SUBSTR(this.w_APPO, 76, 8)
        this.w_LOCALI = SUBSTR(this.w_APPO, 84, 30)
        this.w_PROVIN = SUBSTR(this.w_APPO, 114, 2)
        this.w_CODFIS = SUBSTR(this.w_APPO, 116, 16)
        .oParentObject.w_MV__NOTE = this.w_APPO
        ENDWITH
      case this.pOper="M"
        * --- Controlliamo se la dichiarazione di intento ha una dichiarazione di intento
        *     collegata e questa � stata utilizzata all'interno del documento
        this.w_CLADOC = this.oParentObject.w_MVCLADOC
        this.w_PARAM_FILTRO = "C"
        this.w_SERIALE_DICHIARAZIONE = this.oParentObject.w_DIDICCOL
        * --- Select from DICDINTE8
        do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
        if used('_Curs_DICDINTE8')
          select _Curs_DICDINTE8
          locate for 1=1
          do while not(eof())
          * --- Se esiste un record nella query Dicdinte8 vuol dire che al documento
          *     sono associate due dichiarazioni di intento
          this.oParentObject.w_DICINTE_INDIRETTA = _Curs_DICDINTE8.DDSERIAL
          exit
            select _Curs_DICDINTE8
            continue
          enddo
          use
        endif
    endcase
    if this.pOper="I" OR this.pOper="F" OR this.pOper="C" OR this.pOper="T"
      this.w_GSVE_MDV = this.oParentObject.oParentObject
      this.w_RECTRS = this.w_GSVE_MDV.NumRow()
      if this.w_RECTRS>0
        this.w_RISP = .F.
        if this.pOper="I" OR this.pOper="F" 
          this.w_RISP = ah_YesNo(this.w_MESS)
        endif
        if this.oParentObject.w_MVTFRAGG<>this.oParentObject.w_LIVRAGG AND this.pOper="C"
          this.w_RISP2 = ah_YesNo("Aggiorno i livelli di raggruppamento presenti sulle righe?")
        endif
        if this.w_RISP OR this.pOper="C" OR this.pOper="T"
          this.w_GSVE_MDV.MarkPos()     
          this.w_GSVE_MDV.FirstRow()     
          do while Not this.w_GSVE_MDV.Eof_Trs()
            this.w_GSVE_MDV.SetRow()     
            * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
            *     se non cancellate
            if this.w_GSVE_MDV.FullRow()
              * --- Legge i Dati del Temporaneo
              do case
                case this.pOper="I"
                  this.w_GSVE_MDV.Set("w_MVINICOM" , this.oParentObject.w_MVTINCOM)     
                case this.pOper="F"
                  this.w_GSVE_MDV.Set("w_MVFINCOM" , this.oParentObject.w_MVTFICOM)     
                case this.pOper="C" OR this.pOper="T"
                  this.w_VALMAG = CAVALMAG( this.w_GSVE_MDV.w_MVFLSCOR, this.w_GSVE_MDV.w_MVVALRIG, this.w_GSVE_MDV.w_MVIMPSCO, this.w_GSVE_MDV.w_MVIMPACC, this.w_GSVE_MDV.w_PERIVA, this.w_GSVE_MDV.w_DECTOT, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE )
                  this.w_GSVE_MDV.Set("w_MVVALMAG" , this.w_VALMAG )     
                  this.w_IMPNAZ = CAIMPNAZ(this.oParentObject.w_MVFLVEAC, this.w_VALMAG, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_MVCODVAL, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE, this.oParentObject.w_INDIVE, Nvl(this.w_GSVE_MDV.w_PERIVA,0), Nvl(this.w_GSVE_MDV.w_INDIVA,0) )
                  this.w_GSVE_MDV.Set("w_MVIMPNAZ" , this.w_IMPNAZ )     
                  this.w_GSVE_MDV.Set("w_MVIMPCOM" , CAIMPCOM( IIF(Empty(this.w_GSVE_MDV.w_MVCODCOM),"S", iif(this.w_GSVE_MDV.w_MVFLEVAS="S" And Not Empty(this.w_GSVE_MDV.w_MVFLORCO),"S","N") ), this.oParentObject.w_MVVALNAZ, this.w_GSVE_MDV.w_COCODVAL, this.w_IMPNAZ, this.oParentObject.w_CAONAZ, IIF(EMPTY(this.oParentObject.w_MVDATDOC), this.oParentObject.w_MVDATREG, this.oParentObject.w_MVDATDOC), this.w_GSVE_MDV.w_DECCOM ) )     
                  * --- Se presente aggiorno anche il dato visualizzato sulla riga...
                  if this.w_GSVE_MDV.GetType("w_VISNAZ")<>"U"
                    this.w_GSVE_MDV.Set("w_VISNAZ" , cp_ROUND(this.w_GSVE_MDV.Get("w_MVIMPNAZ"), this.w_GSVE_MDV.w_DECTOP))     
                  endif
                  if this.w_RISP2
                    this.w_GSVE_MDV.Set("w_MVFLRAGG" , this.oParentObject.w_MVTFRAGG)     
                  endif
              endcase
            endif
            this.w_GSVE_MDV.NextRow()     
          enddo
          * --- Riposizionamento sul Transitorio senza effettuare la SaveDependsOn()
          this.w_GSVE_MDV.RePos(.T.)     
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
