* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bvc                                                        *
*              Check valutazione att.pass.                                     *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_10]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-22                                                      *
* Last revis.: 2005-03-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSerial,pGest
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bvc",oParentObject,m.pSerial,m.pGest)
return(i_retval)

define class tgsar_bvc as StdBatch
  * --- Local variables
  pSerial = space(10)
  pGest = space(1)
  w_DVSERIAL = space(10)
  w_APDESCRI = space(50)
  w_APDATREG = ctod("  /  /  ")
  w_MESS = space(254)
  * --- WorkFile variables
  VALDATPA_idx=0
  VAL_ATPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se scadenza diversa o prima nota sono legate a valutazioni
    *     attivit� / pasivit�...
    * --- Riceve come parametro il Seriale (pSerial) e il tipo ('P'=Prima Nota, 'S' scadenze diverse)
    *     
    *     Restiruisce .t. se nessun legame, .f. se legame che impedisce operazioni
    *     sulla gestione chiamante..
    if this.pGest="S"
      * --- Select from VALDATPA
      i_nConn=i_TableProp[this.VALDATPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALDATPA_idx,2],.t.,this.VALDATPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Min( DVSERIAL ) As DVSERIAL  from "+i_cTable+" VALDATPA ";
            +" where DVPASERI ="+cp_ToStrODBC(this.pSERIAL)+" And DVROWORD=-1";
             ,"_Curs_VALDATPA")
      else
        select Min( DVSERIAL ) As DVSERIAL from (i_cTable);
         where DVPASERI =this.pSERIAL And DVROWORD=-1;
          into cursor _Curs_VALDATPA
      endif
      if used('_Curs_VALDATPA')
        select _Curs_VALDATPA
        locate for 1=1
        do while not(eof())
        this.w_DVSERIAL = Nvl( _Curs_VALDATPA.DVSERIAL ,"" )
          select _Curs_VALDATPA
          continue
        enddo
        use
      endif
    else
      * --- Select from VALDATPA
      i_nConn=i_TableProp[this.VALDATPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALDATPA_idx,2],.t.,this.VALDATPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Min( DVSERIAL ) As DVSERIAL  from "+i_cTable+" VALDATPA ";
            +" where DVPASERI ="+cp_ToStrODBC(this.pSERIAL)+" And DVROWORD>0";
             ,"_Curs_VALDATPA")
      else
        select Min( DVSERIAL ) As DVSERIAL from (i_cTable);
         where DVPASERI =this.pSERIAL And DVROWORD>0;
          into cursor _Curs_VALDATPA
      endif
      if used('_Curs_VALDATPA')
        select _Curs_VALDATPA
        locate for 1=1
        do while not(eof())
        this.w_DVSERIAL = Nvl( _Curs_VALDATPA.DVSERIAL ,"" )
          select _Curs_VALDATPA
          continue
        enddo
        use
      endif
    endif
    if not Empty( this.w_DVSERIAL )
      * --- Read from VAL_ATPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VAL_ATPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAL_ATPA_idx,2],.t.,this.VAL_ATPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "APDATREG,APDESCRI"+;
          " from "+i_cTable+" VAL_ATPA where ";
              +"APSERIAL = "+cp_ToStrODBC(this.w_DVSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          APDATREG,APDESCRI;
          from (i_cTable) where;
              APSERIAL = this.w_DVSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_APDATREG = NVL(cp_ToDate(_read_.APDATREG),cp_NullValue(_read_.APDATREG))
        this.w_APDESCRI = NVL(cp_ToDate(_read_.APDESCRI),cp_NullValue(_read_.APDESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MESS = "Le partite della registrazione sono state oggetto di valutazione attivit� e passivit� in divisa%0Per cancellare o modificare, eliminare la corrispondente valutazione:%0Seriale: [%1] %2 del %3"
      ah_ErrorMsg(this.w_MESS,,"",this.w_DVSERIAL, Alltrim(this.w_APDESCRI), Dtoc(this.w_APDATREG) ) 
      i_retcode = 'stop'
      i_retval = .F.
      return
    else
      i_retcode = 'stop'
      i_retval = .T.
      return
    endif
  endproc


  proc Init(oParentObject,pSerial,pGest)
    this.pSerial=pSerial
    this.pGest=pGest
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VALDATPA'
    this.cWorkTables[2]='VAL_ATPA'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_VALDATPA')
      use in _Curs_VALDATPA
    endif
    if used('_Curs_VALDATPA')
      use in _Curs_VALDATPA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSerial,pGest"
endproc
