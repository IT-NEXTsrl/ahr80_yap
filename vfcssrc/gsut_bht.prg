* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bht                                                        *
*              Penna ottica bht-8000d denso                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_56]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-11                                                      *
* Last revis.: 2007-07-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bht",oParentObject,m.pCursore,m.pTXTFile,m.pObjMsg,m.p_ArrayFieldName,m.p_ArrayFieldPosition)
return(i_retval)

define class tgsut_bht as StdBatch
  * --- Local variables
  pCursore = space(10)
  pTXTFile = space(254)
  pObjMsg = .NULL.
  p_ArrayFieldName = space(40)
  p_ArrayFieldPosition = space(40)
  w_NCom = 0
  w_DataBit = 0
  w_BitStop = 0
  w_Vel = 0
  w_Parita = space(1)
  w_HardParam = space(254)
  W_FileOut = space(250)
  w_FileHandle = 0
  w_StrParam = space(254)
  w_StrErr = space(254)
  w_TmpParita = space(10)
  w_Err = 0
  w_NumRip = 0
  w_NFile = 0
  * --- WorkFile variables
  DIS_HARD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Driver per penna ottica BHT-8000D DENSO 1� modalit� (utilizza BHTDLL.DLL)
    *     =========================================================
    *     
    *                    ************************************************************
    *                    |   PARAMETRI DI CONFIGURAZIONE PENNA OTTICA  |
    *                    ---------------------------------------------------------------------------
    *                    |           Baud Rate:   19200                                                     |
    *                    |           Parit�:            No                                                           |
    *                    |           Bit Dati:         8                                                               |
    *                    |           Bit Stop:        1                                                               |
    *                    ************************************************************
    *     
    *     Utilizzare questo batch come driver per caricare dati dalla penna ottica
    *     tramite l'INSERIMENTO DATI (GSAR_BHT=>GSAR_MPG).
    *     
    *     La procedura riceve come parametro il nome del cursore che dovr� riempire.
    *     La struttura del cursore � fissa ed �:
    *     
    *     Create Cursor ( w_CURSORE ) ( CODICE C(20) NULL, UNIMIS C(3) NULL, QTAMOV N(12,3) NULL, PREZZO N(18,5) NULL,;
    *     LOTTO CHAR(20) NULL , CODMAG C(5) NULL,UBICAZIONE C(20) NULL , MATRICOLA C(40) NULL )
    *     
    *     Il secondo parametro � il nome del file contenuto nella penna da scaricare. In questo caso non � necessario 
    *     specificarlo nei parametri della penna poich� il file da scaricare viene selezionato dal dispositivo in questo modo:
    *     
    *       - Accedere al menu di sistema della penna attraverso la combinazione dei tasti <SF>+<PW>+<1>
    *       - Selezionare la voce '3:UPLOAD' e premere il tasto <ENT>
    *       - Selezionare la voce '1:ONE FILE' e premere il tasto <ENT>
    *       - Selezionare il file da scaricare (quello di default � DATI.DAT) e premere il tasto <ENT>
    *     
    *     Il terzo parametro rappresenta l'oggetto da utilizzare per visualizzare i messaggi (deve contenere un campo memo w_MSG).
    *     Il quarto e quinto parametro contengono le informazioni riguardanti posizione e nome campo (sono array passati come riferimento)
    *     (gia cablati all'interno del codice da passare vuoti)
    *     
    *     Il Driver scarica il contenuto della penna in un file di testo (file temporaneo nella dir. TEMP di Windows)
    *     attraverso la funzione long ExecTu3(long, String, String) interna alla dll BHTDLL.DLL.
    *     
    *     - Il primo parametro � l'Handle della finestra che chiama la dll (in questo caso non necessario, quindi 0),
    *     - Il secondo parametro � la stringa che specifica i parametri necessari al trasferimento dati,
    *     - Il terzo parametro � il nome del file in cui la penna ottica memorizza i dati per il trasferimento. 
    *     
    *     Viene poi utilizzato il Driver per file di testo (GSUT_BDT) per caricare la movimentazione.
    *     
    *     
    *     
    * --- Dichiarazione funzioni contenute nella DLL Generale Code
     
 Declare long ExecTu3 IN BHTDLL.DLL long, String, String 
 Declare long AbortTu3 IN BHTDLL.DLL
    * --- -- Parametri di configurazione per la penna
    * --- -- Fine parametri di configurazione per la penna
    this.w_NumRip = 60
    ah_ErrorMsg("Avviare in modalit� sistema la penna (<pw>+<sf>+<1>), selezionare upload,%0One file, quindi il file di dati da caricare (default: dati.Dat)","!","")
    AddMsgNL("%1", this, REPLICATE("-",this.w_NumRip) )
    AddMsgNL("Fase 1: collegamento alla penna%0%1",this, REPLICATE("-",this.w_NumRip))
    * --- Leggo i dati dall'anagrafica dispositivi
    * --- Read from DIS_HARD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_HARD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_HARD_idx,2],.t.,this.DIS_HARD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DHNUMPOR,DH__BAUD,DHBITDAT,DHPARITA,DHBITSTP,DHPROPAR"+;
        " from "+i_cTable+" DIS_HARD where ";
            +"DHCODICE = "+cp_ToStrODBC(g_CODPEN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DHNUMPOR,DH__BAUD,DHBITDAT,DHPARITA,DHBITSTP,DHPROPAR;
        from (i_cTable) where;
            DHCODICE = g_CODPEN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NCom = NVL(cp_ToDate(_read_.DHNUMPOR),cp_NullValue(_read_.DHNUMPOR))
      this.w_Vel = NVL(cp_ToDate(_read_.DH__BAUD),cp_NullValue(_read_.DH__BAUD))
      this.w_DataBit = NVL(cp_ToDate(_read_.DHBITDAT),cp_NullValue(_read_.DHBITDAT))
      this.w_TmpParita = NVL(cp_ToDate(_read_.DHPARITA),cp_NullValue(_read_.DHPARITA))
      this.w_BitStop = NVL(cp_ToDate(_read_.DHBITSTP),cp_NullValue(_read_.DHBITSTP))
      this.w_HardParam = NVL(cp_ToDate(_read_.DHPROPAR),cp_NullValue(_read_.DHPROPAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.W_FileOut = tempadhoc()+"\"+SYS(2015)
    AddMsgNL("Verifica file di esportazione...",this)
    if FILE(this.w_FileOut)
      this.w_FileWriteHandle = FOPEN(this.w_FileOut,1)
      if this.w_FileWriteHandle<0
        this.w_NFile = 1
        do while this.w_FileWriteHandle<0
          FCLOSE(this.w_FileWriteHandle) 
          if FILE(ALLTRIM(this.w_FileOut)+ALLTRIM(str(this.w_NFile)))
            this.w_FileWriteHandle = FOPEN(ALLTRIM(this.w_FileOut)+ALLTRIM(str(this.w_NFile)),1)
          else
            this.w_FileWriteHandle = FCREATE(ALLTRIM(this.w_FileOut)+ALLTRIM(str(this.w_NFile)),0)
          endif
          this.w_NFile = this.w_NFile+1
        enddo
        FCLOSE(this.w_FileWriteHandle) 
        ERASE ALLTRIM(this.w_FileOut)+ALLTRIM(Str(this.w_NFile-1))
        this.W_FileOut = ALLTRIM(this.w_FileOut)+ALLTRIM(Str(this.w_NFile-1))
      else
        FCLOSE(this.w_FileWriteHandle) 
        ERASE (this.w_FileOut)
      endif
    endif
    AddMsgNL("Impostazione parametri di comunicazione... ", this)
    * --- Porta COM collegata alla penna
    AddMsgNL("%1Porta COM...%1%1%2",this,chr(9), ALLTRIM(str(this.w_NCom)) )
    AddMsgNL("%1Trasmissione a...%1%2 BAUD",this,chr(9), ALLTRIM(str(this.w_Vel)) )
    AddMsgNL("%1Codificata in...%1%2", this, chr(9), ALLTRIM(str(this.w_Vel)) )
    AddMsgNL("%1Data Bit...%1%1%2",this,chr(9), ALLTRIM(str(this.w_DataBit)) )
    do case
      case this.w_Tmpparita="0"
        this.w_Parita = "N"
        AddMsgNL("%1Controllo parit�...%1Nessuno",this,chr(9))
      case this.w_Tmpparita="1"
        this.w_Parita = "O"
        AddMsgNL("%1Controllo parit�...%1Pari",this,chr(9))
      case this.w_Tmpparita="2"
        this.w_Parita = "E"
        AddMsgNL("%1Controllo parit�...%1Dispari",this,chr(9))
    endcase
    * --- Bit di stop (0,1,2)
    AddMsgNL("%1Stop Bit...%1%1%2",this,chr(9), ALLTRIM(str(this.w_BitStop)) )
    AddMsgNL("Fase 2: scaricamento informazioni%0%1",this, REPLICATE("-",this.w_NumRip))
    * --- Costruzione della stringa passata come 2� parametro al metodo ExecTu3
    this.w_StrParam = this.w_FileOut+" +P"+ALLTRIM(STR(this.w_NCom))+" +B"+ALLTRIM(STR(this.w_Vel))+" +P"+this.w_Parita
    this.w_StrParam = this.w_StrParam+" +D"+ALLTRIM(STR(this.w_DataBit))+" +S"+ALLTRIM(STR(this.w_BitStop))+" +R -A"
    * --- Invoco il metodo ExecTu3
    this.w_Err = ExecTu3(0, this.w_StrParam,ALLTRIM(this.w_HardParam))
    * --- Interpretazione del codice errore
    do case
      case this.w_Err=0
        this.w_StrErr = ah_Msgformat("Trasferimento dati eseguito correttamente")
      case this.w_Err=3
        this.w_StrErr = ah_Msgformat("Il numero totale di record specificati nel file � superiore a 32767")
      case this.w_Err=4
        this.w_StrErr = ah_Msgformat("La dimensione del campo � fuori dal range 1-254 caratteri")
      case this.w_Err=5
        this.w_StrErr = ah_Msgformat("Il numero dei campi � fuori dal range 1-16")
      case this.w_Err=6
        this.w_StrErr = ah_Msgformat("Il numero totale dei campi e la lunghezza totale dei campi supera 255 bytes")
      case this.w_Err=52
        this.w_StrErr = ah_Msgformat("Errore nella comunicazione: il timer di ricezione � scaduto")
      case this.w_Err=54
        this.w_StrErr = ah_Msgformat("Errore nella comunicazione: numero di NAK superiore a 10")
      case this.w_Err=72 
 
        this.w_StrErr = ah_Msgformat("Spazio disponibile su disco esaurito")
      case this.w_Err=74
        this.w_StrErr = ah_Msgformat("Il timer designato � stato utilizzato da un altra applicazione")
      case this.w_Err=75
        this.w_StrErr = ah_Msgformat("La porta di comunicazione COM %1 � utilizzata da un'altra applicazione",this.w_NCom)
      case this.w_Err=99
        this.w_StrErr = ah_Msgformat("Errore nel trasferimento dati")
    endcase
    AddMsgNL("%1%2", this, chr(9), this.w_StrErr )
     
 Dimension L_FieldName[3], L_FieldPosition [3] 
 L_FieldPosition =0 
 L_FieldName="" 
 L_FieldPosition [1]=1 
 L_FieldPosition [2]=14 
 * 
 L_FieldName[1]="CODICE" 
 L_FieldName[2]="QTAMOV" 
 
    * --- Riempio il cursore con il file TXT creato, utilzzo il DRIVER file di testo
    GSUT_BDT (This, this.pCursore , this.w_FILEOUT , this.pObjMsg, @L_FieldName, @L_FieldPosition)
    * --- Rimuovo il file di testo creato
    Erase(this.w_FILEOUT)
  endproc


  proc Init(oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition)
    this.pCursore=pCursore
    this.pTXTFile=pTXTFile
    this.pObjMsg=pObjMsg
    this.p_ArrayFieldName=p_ArrayFieldName
    this.p_ArrayFieldPosition=p_ArrayFieldPosition
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIS_HARD'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- gsut_bht
  proc AddMsg (Message)
  local oField
  if Type('pObjMsg')='O'
    This.pObjMsg.w_Msg = This.pObjMsg.w_Msg + Message
    oField = this.pObjMsg.getctrl("w_Msg")
    This.pObjMsg.SetControlsValue()
    oField.SelStart =  len(alltrim(This.pObjMsg.w_Msg))
    oField.SetFocus
  Endif
  endproc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition"
endproc
