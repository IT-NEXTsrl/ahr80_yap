* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kde                                                        *
*              Dettaglio cash flow effettivo                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-14                                                      *
* Last revis.: 2015-10-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kde",oParentObject))

* --- Class definition
define class tgste_kde as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 734
  Height = 444
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-26"
  HelpContextID=82457705
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gste_kde"
  cComment = "Dettaglio cash flow effettivo"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NUMCOR = space(15)
  w_DESCON = space(35)
  w_FINE = ctod('  /  /  ')
  w_FINE_MSK = ctod('  /  /  ')
  w_INIZIO = ctod('  /  /  ')
  w_INIZIO_MSK = ctod('  /  /  ')
  w_NONDOMIC = .F.
  w_TIPOCASHFLOW = space(1)
  w_TIPOLOGIACONTO = space(1)
  w_TIPOPAG = space(2)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_ROWNUM = 0
  w_ZOOMDDE = .NULL.
  w_ZOOMDDER = .NULL.
  w_ZOOMDDC = .NULL.
  w_ZOOMDDCE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kdePag1","gste_kde",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMDDE = this.oPgFrm.Pages(1).oPag.ZOOMDDE
    this.w_ZOOMDDER = this.oPgFrm.Pages(1).oPag.ZOOMDDER
    this.w_ZOOMDDC = this.oPgFrm.Pages(1).oPag.ZOOMDDC
    this.w_ZOOMDDCE = this.oPgFrm.Pages(1).oPag.ZOOMDDCE
    DoDefault()
    proc Destroy()
      this.w_ZOOMDDE = .NULL.
      this.w_ZOOMDDER = .NULL.
      this.w_ZOOMDDC = .NULL.
      this.w_ZOOMDDCE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NUMCOR=space(15)
      .w_DESCON=space(35)
      .w_FINE=ctod("  /  /  ")
      .w_FINE_MSK=ctod("  /  /  ")
      .w_INIZIO=ctod("  /  /  ")
      .w_INIZIO_MSK=ctod("  /  /  ")
      .w_NONDOMIC=.f.
      .w_TIPOCASHFLOW=space(1)
      .w_TIPOLOGIACONTO=space(1)
      .w_TIPOPAG=space(2)
      .w_PTSERIAL=space(10)
      .w_PTROWORD=0
      .w_ROWNUM=0
      .w_NUMCOR=oParentObject.w_NUMCOR
      .w_DESCON=oParentObject.w_DESCON
      .w_FINE=oParentObject.w_FINE
      .w_FINE_MSK=oParentObject.w_FINE_MSK
      .w_INIZIO=oParentObject.w_INIZIO
      .w_INIZIO_MSK=oParentObject.w_INIZIO_MSK
      .w_NONDOMIC=oParentObject.w_NONDOMIC
      .w_TIPOCASHFLOW=oParentObject.w_TIPOCASHFLOW
      .w_TIPOLOGIACONTO=oParentObject.w_TIPOLOGIACONTO
      .w_TIPOPAG=oParentObject.w_TIPOPAG
      .oPgFrm.Page1.oPag.ZOOMDDE.Calculate()
      .oPgFrm.Page1.oPag.ZOOMDDER.Calculate()
      .oPgFrm.Page1.oPag.ZOOMDDC.Calculate()
      .oPgFrm.Page1.oPag.ZOOMDDCE.Calculate()
          .DoRTCalc(1,10,.f.)
        .w_PTSERIAL = iif(.w_TIPOCASHFLOW='E',Nvl(.w_ZOOMDDE.getVar('PTSERIAL'),' '),Nvl(.w_ZOOMDDC.getVar('PTSERIAL'),' '))
        .w_PTROWORD = iif(.w_TIPOCASHFLOW='E',Nvl(.w_ZOOMDDE.getVar('PTROWORD'),0),Nvl(.w_ZOOMDDC.getVar('PTROWORD'),0))
        .w_ROWNUM = iif(.w_TIPOCASHFLOW='E',Nvl(.w_ZOOMDDE.getVar('CPROWNUM'),0),Nvl(.w_ZOOMDDC.getVar('CPROWNUM'),0))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_NUMCOR=.w_NUMCOR
      .oParentObject.w_DESCON=.w_DESCON
      .oParentObject.w_FINE=.w_FINE
      .oParentObject.w_FINE_MSK=.w_FINE_MSK
      .oParentObject.w_INIZIO=.w_INIZIO
      .oParentObject.w_INIZIO_MSK=.w_INIZIO_MSK
      .oParentObject.w_NONDOMIC=.w_NONDOMIC
      .oParentObject.w_TIPOCASHFLOW=.w_TIPOCASHFLOW
      .oParentObject.w_TIPOLOGIACONTO=.w_TIPOLOGIACONTO
      .oParentObject.w_TIPOPAG=.w_TIPOPAG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMDDE.Calculate()
        .oPgFrm.Page1.oPag.ZOOMDDER.Calculate()
        .oPgFrm.Page1.oPag.ZOOMDDC.Calculate()
        .oPgFrm.Page1.oPag.ZOOMDDCE.Calculate()
        .DoRTCalc(1,10,.t.)
            .w_PTSERIAL = iif(.w_TIPOCASHFLOW='E',Nvl(.w_ZOOMDDE.getVar('PTSERIAL'),' '),Nvl(.w_ZOOMDDC.getVar('PTSERIAL'),' '))
            .w_PTROWORD = iif(.w_TIPOCASHFLOW='E',Nvl(.w_ZOOMDDE.getVar('PTROWORD'),0),Nvl(.w_ZOOMDDC.getVar('PTROWORD'),0))
            .w_ROWNUM = iif(.w_TIPOCASHFLOW='E',Nvl(.w_ZOOMDDE.getVar('CPROWNUM'),0),Nvl(.w_ZOOMDDC.getVar('CPROWNUM'),0))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMDDE.Calculate()
        .oPgFrm.Page1.oPag.ZOOMDDER.Calculate()
        .oPgFrm.Page1.oPag.ZOOMDDC.Calculate()
        .oPgFrm.Page1.oPag.ZOOMDDCE.Calculate()
    endwith
  return

  proc Calculate_SNEWWVZJFG()
    with this
          * --- CREA
          GSTE_BD5(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .cComment = IIF( .w_TIPOCASHFLOW='C', AH_MSGFORMAT("Dettaglio cash flow certo"),IIF(.w_TIPOCASHFLOW = 'E' , AH_MSGFORMAT("Dettaglio cash flow effettivo"), " " ) )
          .Caption = .cComment
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_1.visible=!this.oPgFrm.Page1.oPag.oStr_1_1.mHide()
    this.oPgFrm.Page1.oPag.oNUMCOR_1_2.visible=!this.oPgFrm.Page1.oPag.oNUMCOR_1_2.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_3.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMDDE.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMDDER.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_SNEWWVZJFG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZOOMDDC.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMDDCE.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMCOR_1_2.value==this.w_NUMCOR)
      this.oPgFrm.Page1.oPag.oNUMCOR_1_2.value=this.w_NUMCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_3.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_3.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oFINE_MSK_1_6.value==this.w_FINE_MSK)
      this.oPgFrm.Page1.oPag.oFINE_MSK_1_6.value=this.w_FINE_MSK
    endif
    if not(this.oPgFrm.Page1.oPag.oINIZIO_MSK_1_8.value==this.w_INIZIO_MSK)
      this.oPgFrm.Page1.oPag.oINIZIO_MSK_1_8.value=this.w_INIZIO_MSK
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgste_kdePag1 as StdContainer
  Width  = 730
  height = 444
  stdWidth  = 730
  stdheight = 444
  resizeXpos=395
  resizeYpos=215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMCOR_1_2 as StdField with uid="AHNGSFHRKB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NUMCOR", cQueryName = "NUMCOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto selezionato per cash flow",;
    HelpContextID = 38663638,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=103, Top=6, InputMask=replicate('X',15)

  func oNUMCOR_1_2.mHide()
    with this.Parent.oContained
      return (.w_NONDOMIC)
    endwith
  endfunc

  add object oDESCON_1_3 as StdField with uid="BSBFLZNSTQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 240010550,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=223, Top=6, InputMask=replicate('X',35)

  func oDESCON_1_3.mHide()
    with this.Parent.oContained
      return (.w_NONDOMIC)
    endwith
  endfunc


  add object ZOOMDDE as cp_zoombox with uid="FMSRBJXCFE",left=4, top=39, width=715,height=208,;
    caption='ZOOMDDE',;
   bGlobalFont=.t.,;
    cTable="MOD_PAGA",cZoomFile="GSTE_KDE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",bRetriveAllRows=.f.,cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 61345942

  add object oFINE_MSK_1_6 as StdField with uid="WRBJIAIOKH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FINE_MSK", cQueryName = "FINE_MSK",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine movimenti",;
    HelpContextID = 240122273,;
   bGlobalFont=.t.,;
    Height=20, Width=76, Left=626, Top=6

  add object oINIZIO_MSK_1_8 as StdField with uid="SWINJLVOOA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_INIZIO_MSK", cQueryName = "INIZIO_MSK",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio movimenti",;
    HelpContextID = 16449789,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=523, Top=6


  add object ZOOMDDER as cp_zoombox with uid="GKXEFFZQPW",left=4, top=274, width=715,height=113,;
    caption='ZOOMDDER',;
   bGlobalFont=.t.,;
    cTable="MOD_PAGA",cZoomFile="GSTE1KDE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,bRetriveAllRows=.f.,bQueryOnDblClick=.t.,cMenuFile="",bNoZoomGridShape=.f.,cZoomOnZoom="",;
    cEvent = "Raggruppate",;
    nPag=1;
    , HelpContextID = 61346024


  add object oBtn_1_16 as StdButton with uid="GHXPFFDRPI",left=672, top=392, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Annulla";
    , HelpContextID = 75140282;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMDDC as cp_zoombox with uid="EAOIGOVWYN",left=4, top=39, width=715,height=208,;
    caption='ZOOMDDC',;
   bGlobalFont=.t.,;
    cTable="MOD_PAGA",cZoomFile="GSTE2KDE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",bRetriveAllRows=.f.,cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 61345942


  add object ZOOMDDCE as cp_zoombox with uid="YZJRCUFCDY",left=4, top=274, width=715,height=113,;
    caption='ZOOMDDCE',;
   bGlobalFont=.t.,;
    cTable="MOD_PAGA",cZoomFile="GSTE3KDE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,bRetriveAllRows=.f.,bQueryOnDblClick=.t.,cMenuFile="",bNoZoomGridShape=.f.,cZoomOnZoom="",;
    cEvent = "Raggruppate",;
    nPag=1;
    , HelpContextID = 61346011


  add object oBtn_1_24 as StdButton with uid="MSTLUBLVZR",left=621, top=393, width=48,height=45,;
    CpPicture="bmp\ORIGINE.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza l'origine della scadenza (primanota, scadenze diverse o distinte)";
    , HelpContextID = 267136282;
    , Caption='Ori\<gine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSTE_BZP(this.Parent.oContained,.w_PTSERIAL, .w_PTROWORD, .w_ROWNUM,0,0,0)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PTSERIAL))
      endwith
    endif
  endfunc

  add object oStr_1_1 as StdString with uid="OZOXSNJUMX",Visible=.t., Left=15, Top=6,;
    Alignment=1, Width=85, Height=18,;
    Caption="Conto cassa:"  ;
  , bGlobalFont=.t.

  func oStr_1_1.mHide()
    with this.Parent.oContained
      return (.w_TIPOLOGIACONTO <> 'G')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="IENRQNJDTA",Visible=.t., Left=487, Top=6,;
    Alignment=1, Width=32, Height=18,;
    Caption="dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="OOEHRSQLGY",Visible=.t., Left=602, Top=6,;
    Alignment=1, Width=22, Height=18,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="CJPUSVTCKD",Visible=.t., Left=138, Top=7,;
    Alignment=0, Width=207, Height=21,;
    Caption="Scadenze non domiciliate"    , forecolor= rgb(255,0,0);
  ;
    , FontName = "Arial", FontSize = 12, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (!.w_NONDOMIC)
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="KWKBJRPQXS",Visible=.t., Left=138, Top=251,;
    Alignment=2, Width=431, Height=21,;
    Caption="Dettaglio per tipo pagamento"    , forecolor= rgb(255,0,0);
  ;
    , FontName = "Arial", FontSize = 12, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="AGHABLKMIA",Visible=.t., Left=7, Top=6,;
    Alignment=1, Width=93, Height=18,;
    Caption="Conto banca:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_TIPOLOGIACONTO <> 'C')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kde','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
