* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bpn                                                        *
*              Menu contestuale primanota dettaglio                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_50]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-28                                                      *
* Last revis.: 2009-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFUNZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bpn",oParentObject,m.pFUNZ)
return(i_retval)

define class tgszm_bpn as StdBatch
  * --- Local variables
  pFUNZ = space(1)
  w_OBJECT = .NULL.
  w_PROG = .NULL.
  w_BOTTONE = .NULL.
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_PNSERIAL = space(10)
  w_CPROWNUM = 0
  w_CODESE = space(4)
  * --- WorkFile variables
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine men� contestuale intestatario su Manutenzione Partite
    * --- Assegno i valori alle variabili
    this.w_PNSERIAL = g_oMenu.getbyindexkeyvalue(1)
    if g_oMenu.nkeycount=2
      * --- Se esiste la seconda chiave nella gestione
      this.w_CPROWNUM = g_oMenu.getbyindexkeyvalue(2)
    else
      * --- Se esiste la variabile relativa alla seconda chiave
      this.w_CPROWNUM = iif(Type("g_oMenu.oParentobject.w_CPROWNUM")="N",g_oMenu.oParentobject.w_CPROWNUM,1)
    endif
    * --- Read from PNT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PNCODESE"+;
        " from "+i_cTable+" PNT_MAST where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PNCODESE;
        from (i_cTable) where;
            PNSERIAL = this.w_PNSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODESE = NVL(cp_ToDate(_read_.PNCODESE),cp_NullValue(_read_.PNCODESE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from PNT_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PNCODCON,PNTIPCON"+;
        " from "+i_cTable+" PNT_DETT where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PNCODCON,PNTIPCON;
        from (i_cTable) where;
            PNSERIAL = this.w_PNSERIAL;
            and CPROWNUM = this.w_CPROWNUM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANCODICE = NVL(cp_ToDate(_read_.PNCODCON),cp_NullValue(_read_.PNCODCON))
      this.w_ANTIPCON = NVL(cp_ToDate(_read_.PNTIPCON),cp_NullValue(_read_.PNTIPCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pFUNZ = "1"
        if EMPTY(this.w_ANCODICE)
          this.w_ANCODICE = iif(Type("g_oMenu.oParentobject.w_ANCODICE")="C",g_oMenu.oParentobject.w_ANCODICE,"")
        endif
        if EMPTY(this.w_ANTIPCON)
          this.w_ANTIPCON = iif(Type("g_oMenu.oParentobject.w_ANTIPCON")="C",g_oMenu.oParentobject.w_ANTIPCON,"")
        endif
        * --- Anagrafica Clienti\Conti\Fornitori
        this.w_OBJECT = iif(this.w_ANTIPCON="F",GSAR_AFR(),iif(this.w_ANTIPCON="C",GSAR_ACL(),GSAR_API()))
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_ANTIPCON = this.w_ANTIPCON
        this.w_OBJECT.w_ANCODICE = this.w_ANCODICE
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
      case this.pFUNZ = "2"
        * --- Stampa Movimenti Contabili
        this.w_OBJECT = GSCG_SPN()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        if Upper(g_APPLICATION) = "ADHOC REVOLUTION"
          if this.w_ANTIPCON $ "C-F"
            this.w_OBJECT.w_CODCLF = this.w_ANCODICE
            this.w_OBJECT.w_TIPCLF = this.w_ANTIPCON
          else
            this.w_OBJECT.w_TIPCLF = "T"
          endif
          this.w_PROG = this.w_OBJECT.GetcTRL("w_CODCLF")
          this.w_PROG.Check()     
          this.w_OBJECT.mCalc(.t.)     
        endif
      case this.pFUNZ = "3"
        * --- Bilancio Libro Inventari
        this.w_OBJECT = GSCG_SBI()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_ESE = this.w_CODESE
      case this.pFUNZ = "4"
        * --- Bilancio UE
        this.w_OBJECT = GSCG_SBG()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_ESE = this.w_CODESE
      case this.pFUNZ = "5"
        * --- Se lanciato da visualizza schede per contropartita (gscg1szm)
        if vartype (g_oMenu.oParentobject.class)="C" AND UPPER (g_oMenu.oParentobject.class)="TGSCG1KCS"
          this.w_ANTIPCON = g_oMenu.oParentobject.w_TIPINT
        endif
        do case
          case this.w_ANTIPCON="C"
            * --- Crediti Verso\Clienti
            this.w_OBJECT = GSCG_SSC()
            if !(this.w_OBJECT.bSec1)
              i_retcode = 'stop'
              return
            endif
            this.w_OBJECT.w_ESE = this.w_CODESE
          case this.w_ANTIPCON="F"
            * --- Debiti da Fornitori
            this.w_OBJECT = GSCG_SSF()
            if !(this.w_OBJECT.bSec1)
              i_retcode = 'stop'
              return
            endif
            this.w_OBJECT.w_ESE = this.w_CODESE
        endcase
    endcase
    this.w_PROG = .NULL.
    this.w_OBJECT = .NULL.
  endproc


  proc Init(oParentObject,pFUNZ)
    this.pFUNZ=pFUNZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PNT_DETT'
    this.cWorkTables[2]='PNT_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFUNZ"
endproc
