* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bie                                                        *
*              Interpretazione esito                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-09-19                                                      *
* Last revis.: 2014-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNomfile,pNODO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bie",oParentObject,m.pNomfile,m.pNODO)
return(i_retval)

define class tgsut_bie as StdBatch
  * --- Local variables
  pNomfile = space(100)
  pNODO = space(100)
  w_OXML = .NULL.
  w_onode = .NULL.
  w_RETVAL = space(0)
  w_FILE = space(254)
  w_NOMEFILE = space(254)
  w_INFILE = space(0)
  w_TIPORIS = space(2)
  w_NOMEFILE = space(100)
  w_ESITO = ctot("")
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FILE = Addbs(TempAdhoc())+Sys(2015)+".xml"
    this.w_INFILE = FILETOSTR(this.pNomfile)
    this.w_INFILE = STRTRAN(this.w_INFILE,'<!DOCTYPE EsitoAtto SYSTEM "http://schemi.processotelematico.giustizia.it/Schemi/EsitoAtto.dtd">',"")
    STRTOFILE(this.w_INFILE,this.w_FILE)
    L_OLDERR=ON("ERROR")
    L_Err=.F.
    ON ERROR L_Err=.T.
    this.w_OXML = createobject("Msxml2.DOMDocument")
    ON ERROR &L_OLDERR
    if L_Err
      this.w_RETVAL = AH_MSGFormat("Impossibile trovare componente MSXML2")
      this.w_OXML = null
      i_retcode = 'stop'
      i_retval = this.w_RETVAL
      return
    endif
    if CP_FILEEXIST(Alltrim(this.w_FILE))
      this.w_OXML.Load(Alltrim(this.w_FIle))     
      * --- NomeFile
      if type('this.w_oXML.DocumentElement.selectNodes("NomeFile").item(0)')="O"
        ON ERROR L_Err=.T.
        this.w_onode = this.w_oXML.selectNodes(this.pNODO)
        if TYPE("this.w_ONODE.item(0).text")="C"
          this.w_ESITO = this.w_ONODE.item(0).text
        endif
        Delete File (Alltrim(this.w_FILE))
        ON ERROR &L_OLDERR
      endif
    endif
    if L_Err
      i_retcode = 'stop'
      i_retval = " "
      return
    else
      i_retcode = 'stop'
      i_retval = this.w_ESITO
      return
    endif
  endproc


  proc Init(oParentObject,pNomfile,pNODO)
    this.pNomfile=pNomfile
    this.pNODO=pNODO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNomfile,pNODO"
endproc
