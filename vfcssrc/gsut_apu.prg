* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_apu                                                        *
*              Politiche di sicurezza utenti                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_58]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-09                                                      *
* Last revis.: 2013-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsut_apu
*Questa gestione la pu� usare solo l'amministratore
if not cp_IsAdministrator(.t.)
  AH_ERRORMSG("Accesso negato",'stop',"Gestione sicurezza")
  return null
endif
* --- Fine Area Manuale
return(createobject("tgsut_apu"))

* --- Class definition
define class tgsut_apu as StdForm
  Top    = 8
  Left   = 29

  * --- Standard Properties
  Width  = 506
  Height = 149+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-19"
  HelpContextID=159990633
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  POL_UTE_IDX = 0
  CPUSERS_IDX = 0
  POL_SIC_IDX = 0
  CPUSRGRP_IDX = 0
  cFile = "POL_UTE"
  cKeySelect = "PUCODUTE"
  cKeyWhere  = "PUCODUTE=this.w_PUCODUTE"
  cKeyWhereODBC = '"PUCODUTE="+cp_ToStrODBC(this.w_PUCODUTE)';

  cKeyWhereODBCqualified = '"POL_UTE.PUCODUTE="+cp_ToStrODBC(this.w_PUCODUTE)';

  cPrg = "gsut_apu"
  cComment = "Politiche di sicurezza utenti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AHE = space(10)
  w_VALMAX = 0
  w_VALAMM = 0
  w_PUCODUTE = 0
  o_PUCODUTE = 0
  w_PUMOTBLO = space(1)
  o_PUMOTBLO = space(1)
  w_PUNUMTEN = 0
  o_PUNUMTEN = 0
  w_PUFLGBLO = space(1)
  w_USERCODE = 0
  w_ADMIN = 0
  w_PUDATULT = ctod('  /  /  ')
  w_PUDATACC = ctod('  /  /  ')
  w_NOMUTE = space(20)
  w_NUMTEN = 0
  w_FLGAMM = space(1)
  w_PUDATSCA = ctod('  /  /  ')
  w_BLOCCO = space(1)
  w_PUCHGPWD = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'POL_UTE','gsut_apu')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_apuPag1","gsut_apu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Politiche")
      .Pages(1).HelpContextID = 119994286
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPUCODUTE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='POL_SIC'
    this.cWorkTables[3]='CPUSRGRP'
    this.cWorkTables[4]='POL_UTE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.POL_UTE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.POL_UTE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PUCODUTE = NVL(PUCODUTE,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from POL_UTE where PUCODUTE=KeySet.PUCODUTE
    *
    i_nConn = i_TableProp[this.POL_UTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('POL_UTE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "POL_UTE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' POL_UTE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PUCODUTE',this.w_PUCODUTE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AHE = 'AHE'
        .w_VALMAX = 0
        .w_VALAMM = 0
        .w_ADMIN = 0
        .w_NOMUTE = space(20)
        .w_FLGAMM = space(1)
        .w_BLOCCO = space(1)
          .link_1_1('Load')
        .w_PUCODUTE = NVL(PUCODUTE,0)
          .link_1_4('Load')
        .w_PUMOTBLO = NVL(PUMOTBLO,space(1))
        .w_PUNUMTEN = NVL(PUNUMTEN,0)
        .w_PUFLGBLO = NVL(PUFLGBLO,space(1))
        .w_USERCODE = .w_PUCODUTE
          .link_1_8('Load')
        .w_PUDATULT = NVL(cp_ToDate(PUDATULT),ctod("  /  /  "))
        .w_PUDATACC = NVL(cp_ToDate(PUDATACC),ctod("  /  /  "))
        .w_NUMTEN = IIF( .w_PUNUMTEN=-1 , 0 , .w_PUNUMTEN )
        .w_PUDATSCA = IIF(.w_FLGAMM='N',IIF(.w_VALAMM > 0 AND .w_ADMIN>0 ,  .w_PUDATULT + .w_VALAMM,IIF(.w_VALMAX = 0, null, .w_PUDATULT + .w_VALMAX)),IIF(.w_VALMAX = 0, null, .w_PUDATULT + .w_VALMAX))
        .w_PUCHGPWD = NVL(PUCHGPWD,space(1))
        cp_LoadRecExtFlds(this,'POL_UTE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AHE = space(10)
      .w_VALMAX = 0
      .w_VALAMM = 0
      .w_PUCODUTE = 0
      .w_PUMOTBLO = space(1)
      .w_PUNUMTEN = 0
      .w_PUFLGBLO = space(1)
      .w_USERCODE = 0
      .w_ADMIN = 0
      .w_PUDATULT = ctod("  /  /  ")
      .w_PUDATACC = ctod("  /  /  ")
      .w_NOMUTE = space(20)
      .w_NUMTEN = 0
      .w_FLGAMM = space(1)
      .w_PUDATSCA = ctod("  /  /  ")
      .w_BLOCCO = space(1)
      .w_PUCHGPWD = space(1)
      if .cFunction<>"Filter"
        .w_AHE = 'AHE'
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AHE))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,4,.f.)
          if not(empty(.w_PUCODUTE))
          .link_1_4('Full')
          endif
        .w_PUMOTBLO = 'N'
          .DoRTCalc(6,6,.f.)
        .w_PUFLGBLO = iif(.w_PUMOTBLO='N','N','S')
        .w_USERCODE = .w_PUCODUTE
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_USERCODE))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,9,.f.)
        .w_PUDATULT = i_DATSYS
          .DoRTCalc(11,12,.f.)
        .w_NUMTEN = IIF( .w_PUNUMTEN=-1 , 0 , .w_PUNUMTEN )
          .DoRTCalc(14,14,.f.)
        .w_PUDATSCA = IIF(.w_FLGAMM='N',IIF(.w_VALAMM > 0 AND .w_ADMIN>0 ,  .w_PUDATULT + .w_VALAMM,IIF(.w_VALMAX = 0, null, .w_PUDATULT + .w_VALMAX)),IIF(.w_VALMAX = 0, null, .w_PUDATULT + .w_VALMAX))
          .DoRTCalc(16,16,.f.)
        .w_PUCHGPWD = IIF(.w_PUMOTBLO='N' and .w_BLOCCO='P','S','N')
      endif
    endwith
    cp_BlankRecExtFlds(this,'POL_UTE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPUCODUTE_1_4.enabled = i_bVal
      .Page1.oPag.oPUMOTBLO_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPUCODUTE_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPUCODUTE_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'POL_UTE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.POL_UTE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCODUTE,"PUCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMOTBLO,"PUMOTBLO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUNUMTEN,"PUNUMTEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUFLGBLO,"PUFLGBLO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUDATULT,"PUDATULT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUDATACC,"PUDATACC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCHGPWD,"PUCHGPWD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.POL_UTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2])
    i_lTable = "POL_UTE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.POL_UTE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.POL_UTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.POL_UTE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into POL_UTE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'POL_UTE')
        i_extval=cp_InsertValODBCExtFlds(this,'POL_UTE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PUCODUTE,PUMOTBLO,PUNUMTEN,PUFLGBLO,PUDATULT"+;
                  ",PUDATACC,PUCHGPWD "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PUCODUTE)+;
                  ","+cp_ToStrODBC(this.w_PUMOTBLO)+;
                  ","+cp_ToStrODBC(this.w_PUNUMTEN)+;
                  ","+cp_ToStrODBC(this.w_PUFLGBLO)+;
                  ","+cp_ToStrODBC(this.w_PUDATULT)+;
                  ","+cp_ToStrODBC(this.w_PUDATACC)+;
                  ","+cp_ToStrODBC(this.w_PUCHGPWD)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'POL_UTE')
        i_extval=cp_InsertValVFPExtFlds(this,'POL_UTE')
        cp_CheckDeletedKey(i_cTable,0,'PUCODUTE',this.w_PUCODUTE)
        INSERT INTO (i_cTable);
              (PUCODUTE,PUMOTBLO,PUNUMTEN,PUFLGBLO,PUDATULT,PUDATACC,PUCHGPWD  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PUCODUTE;
                  ,this.w_PUMOTBLO;
                  ,this.w_PUNUMTEN;
                  ,this.w_PUFLGBLO;
                  ,this.w_PUDATULT;
                  ,this.w_PUDATACC;
                  ,this.w_PUCHGPWD;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.POL_UTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.POL_UTE_IDX,i_nConn)
      *
      * update POL_UTE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'POL_UTE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PUMOTBLO="+cp_ToStrODBC(this.w_PUMOTBLO)+;
             ",PUNUMTEN="+cp_ToStrODBC(this.w_PUNUMTEN)+;
             ",PUFLGBLO="+cp_ToStrODBC(this.w_PUFLGBLO)+;
             ",PUDATULT="+cp_ToStrODBC(this.w_PUDATULT)+;
             ",PUDATACC="+cp_ToStrODBC(this.w_PUDATACC)+;
             ",PUCHGPWD="+cp_ToStrODBC(this.w_PUCHGPWD)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'POL_UTE')
        i_cWhere = cp_PKFox(i_cTable  ,'PUCODUTE',this.w_PUCODUTE  )
        UPDATE (i_cTable) SET;
              PUMOTBLO=this.w_PUMOTBLO;
             ,PUNUMTEN=this.w_PUNUMTEN;
             ,PUFLGBLO=this.w_PUFLGBLO;
             ,PUDATULT=this.w_PUDATULT;
             ,PUDATACC=this.w_PUDATACC;
             ,PUCHGPWD=this.w_PUCHGPWD;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.POL_UTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.POL_UTE_IDX,i_nConn)
      *
      * delete POL_UTE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PUCODUTE',this.w_PUCODUTE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.POL_UTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,6,.t.)
        if .o_PUMOTBLO<>.w_PUMOTBLO
            .w_PUFLGBLO = iif(.w_PUMOTBLO='N','N','S')
        endif
            .w_USERCODE = .w_PUCODUTE
          .link_1_8('Full')
        .DoRTCalc(9,12,.t.)
        if .o_PUNUMTEN<>.w_PUNUMTEN
            .w_NUMTEN = IIF( .w_PUNUMTEN=-1 , 0 , .w_PUNUMTEN )
        endif
        .DoRTCalc(14,14,.t.)
        if .o_PUCODUTE<>.w_PUCODUTE
            .w_PUDATSCA = IIF(.w_FLGAMM='N',IIF(.w_VALAMM > 0 AND .w_ADMIN>0 ,  .w_PUDATULT + .w_VALAMM,IIF(.w_VALMAX = 0, null, .w_PUDATULT + .w_VALMAX)),IIF(.w_VALMAX = 0, null, .w_PUDATULT + .w_VALMAX))
        endif
        .DoRTCalc(16,16,.t.)
        if .o_PUMOTBLO<>.w_PUMOTBLO
            .w_PUCHGPWD = IIF(.w_PUMOTBLO='N' and .w_BLOCCO='P','S','N')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_TMFIJKNYUJ()
    with this
          * --- Campi iniziali
          .w_BLOCCO = .w_PUMOTBLO
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Edit Started")
          .Calculate_TMFIJKNYUJ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AHE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.POL_SIC_IDX,3]
    i_lTable = "POL_SIC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2], .t., this.POL_SIC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AHE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AHE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSSERIAL,PSVALMAX,PSVALMAA,PS__AMMI";
                   +" from "+i_cTable+" "+i_lTable+" where PSSERIAL="+cp_ToStrODBC(this.w_AHE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSSERIAL',this.w_AHE)
            select PSSERIAL,PSVALMAX,PSVALMAA,PS__AMMI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AHE = NVL(_Link_.PSSERIAL,space(10))
      this.w_VALMAX = NVL(_Link_.PSVALMAX,0)
      this.w_VALAMM = NVL(_Link_.PSVALMAA,0)
      this.w_FLGAMM = NVL(_Link_.PS__AMMI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AHE = space(10)
      endif
      this.w_VALMAX = 0
      this.w_VALAMM = 0
      this.w_FLGAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])+'\'+cp_ToStr(_Link_.PSSERIAL,1)
      cp_ShowWarn(i_cKey,this.POL_SIC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AHE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PUCODUTE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PUCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PUCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PUCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oPUCODUTE_1_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PUCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PUCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUCODUTE = NVL(_Link_.CODE,0)
      this.w_NOMUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PUCODUTE = 0
      endif
      this.w_NOMUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=USERCODE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSRGRP_IDX,3]
    i_lTable = "CPUSRGRP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSRGRP_IDX,2], .t., this.CPUSRGRP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSRGRP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_USERCODE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_USERCODE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GROUPCODE,USERCODE";
                   +" from "+i_cTable+" "+i_lTable+" where USERCODE="+cp_ToStrODBC(this.w_USERCODE);
                   +" and GROUPCODE="+cp_ToStrODBC(1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GROUPCODE',1;
                       ,'USERCODE',this.w_USERCODE)
            select GROUPCODE,USERCODE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_USERCODE = NVL(_Link_.USERCODE,0)
      this.w_ADMIN = NVL(_Link_.USERCODE,0)
    else
      if i_cCtrl<>'Load'
        this.w_USERCODE = 0
      endif
      this.w_ADMIN = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSRGRP_IDX,2])+'\'+cp_ToStr(_Link_.GROUPCODE,1)+'\'+cp_ToStr(_Link_.USERCODE,1)
      cp_ShowWarn(i_cKey,this.CPUSRGRP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_USERCODE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPUCODUTE_1_4.value==this.w_PUCODUTE)
      this.oPgFrm.Page1.oPag.oPUCODUTE_1_4.value=this.w_PUCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMOTBLO_1_5.RadioValue()==this.w_PUMOTBLO)
      this.oPgFrm.Page1.oPag.oPUMOTBLO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUDATULT_1_10.value==this.w_PUDATULT)
      this.oPgFrm.Page1.oPag.oPUDATULT_1_10.value=this.w_PUDATULT
    endif
    if not(this.oPgFrm.Page1.oPag.oPUDATACC_1_11.value==this.w_PUDATACC)
      this.oPgFrm.Page1.oPag.oPUDATACC_1_11.value=this.w_PUDATACC
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMUTE_1_17.value==this.w_NOMUTE)
      this.oPgFrm.Page1.oPag.oNOMUTE_1_17.value=this.w_NOMUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMTEN_1_21.value==this.w_NUMTEN)
      this.oPgFrm.Page1.oPag.oNUMTEN_1_21.value=this.w_NUMTEN
    endif
    if not(this.oPgFrm.Page1.oPag.oPUDATSCA_1_23.value==this.w_PUDATSCA)
      this.oPgFrm.Page1.oPag.oPUDATSCA_1_23.value=this.w_PUDATSCA
    endif
    cp_SetControlsValueExtFlds(this,'POL_UTE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PUCODUTE = this.w_PUCODUTE
    this.o_PUMOTBLO = this.w_PUMOTBLO
    this.o_PUNUMTEN = this.w_PUNUMTEN
    return

enddefine

* --- Define pages as container
define class tgsut_apuPag1 as StdContainer
  Width  = 502
  height = 149
  stdWidth  = 502
  stdheight = 149
  resizeXpos=250
  resizeYpos=67
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPUCODUTE_1_4 as StdField with uid="NXCTPUCPIJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PUCODUTE", cQueryName = "PUCODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 673595,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=42, Left=105, Top=15, cSayPict='"999"', cGetPict='"999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_PUCODUTE"

  func oPUCODUTE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUCODUTE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUCODUTE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oPUCODUTE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oPUMOTBLO_1_5 as StdCombo with uid="JJRPEJATQD",rtseq=5,rtrep=.f.,left=105,top=43,width=156,height=21;
    , ToolTipText = "Motivo blocco";
    , HelpContextID = 32839867;
    , cFormVar="w_PUMOTBLO",RowSource=""+"Nessun blocco,"+"Scaduta password,"+"Scaduto account,"+"Num. max tentativi,"+"Blocco manuale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUMOTBLO_1_5.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'A',;
    iif(this.value =4,'S',;
    iif(this.value =5,'B',;
    space(1)))))))
  endfunc
  func oPUMOTBLO_1_5.GetRadio()
    this.Parent.oContained.w_PUMOTBLO = this.RadioValue()
    return .t.
  endfunc

  func oPUMOTBLO_1_5.SetRadio()
    this.Parent.oContained.w_PUMOTBLO=trim(this.Parent.oContained.w_PUMOTBLO)
    this.value = ;
      iif(this.Parent.oContained.w_PUMOTBLO=='N',1,;
      iif(this.Parent.oContained.w_PUMOTBLO=='P',2,;
      iif(this.Parent.oContained.w_PUMOTBLO=='A',3,;
      iif(this.Parent.oContained.w_PUMOTBLO=='S',4,;
      iif(this.Parent.oContained.w_PUMOTBLO=='B',5,;
      0)))))
  endfunc

  add object oPUDATULT_1_10 as StdField with uid="EIMXIAUPXG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PUDATULT", cQueryName = "PUDATULT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultimo aggiornamento password",;
    HelpContextID = 251898038,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=417, Top=95

  add object oPUDATACC_1_11 as StdField with uid="PJPQLFERJW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PUDATACC", cQueryName = "PUDATACC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultimo accesso",;
    HelpContextID = 217863993,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=417, Top=125

  add object oNOMUTE_1_17 as StdField with uid="QBHASDPDTN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NOMUTE", cQueryName = "NOMUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 17883350,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=152, Top=15, InputMask=replicate('X',20)

  add object oNUMTEN_1_21 as StdField with uid="KAEFEGDZLR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NUMTEN", cQueryName = "NUMTEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero tentativi errati di accesso",;
    HelpContextID = 153085654,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=138, Top=125, cSayPict='"999999999"', cGetPict='"999999999"'

  add object oPUDATSCA_1_23 as StdField with uid="ZOXPBKYVMN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PUDATSCA", cQueryName = "PUDATSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza password",;
    HelpContextID = 251418423,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=138, Top=95

  add object oStr_1_12 as StdString with uid="CNFSZKPWWH",Visible=.t., Left=48, Top=17,;
    Alignment=1, Width=55, Height=18,;
    Caption="Utente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="BOIOAPRIAK",Visible=.t., Left=15, Top=95,;
    Alignment=1, Width=120, Height=18,;
    Caption="Scadenza password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="IVSTUKQMWO",Visible=.t., Left=315, Top=95,;
    Alignment=1, Width=99, Height=18,;
    Caption="Ultimo agg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ERRWBTPZPG",Visible=.t., Left=296, Top=125,;
    Alignment=1, Width=118, Height=18,;
    Caption="Ultimo accesso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="OXQBJEYBBG",Visible=.t., Left=32, Top=125,;
    Alignment=1, Width=103, Height=18,;
    Caption="Numero tentativi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="IGAYXETFGS",Visible=.t., Left=10, Top=44,;
    Alignment=1, Width=93, Height=18,;
    Caption="Bloccato per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="UFSLJZSNXJ",Visible=.t., Left=271, Top=68,;
    Alignment=0, Width=225, Height=17,;
    Caption="Informazioni sicurezza - non modificabili"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_19 as StdBox with uid="IRWQSHDQQD",left=8, top=87, width=493,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_apu','POL_UTE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PUCODUTE=POL_UTE.PUCODUTE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
