* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpc                                                        *
*              Treev.strut.piano conti                                         *
*                                                                              *
*      Author: ZUCCHETTI TAM S.r.l.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_391]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-24                                                      *
* Last revis.: 2014-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpc",oParentObject,m.pTipOp)
return(i_retval)

define class tgsar_bpc as StdBatch
  * --- Local variables
  pTipOp = space(10)
  w_MASTRO = space(15)
  w_CONTO = space(15)
  w_NUMPUNTI = 0
  w_NUMMASTR = 0
  w_CICLOVAR = 0
  w_ANTIPCON = space(1)
  w_CODICE = space(15)
  w_OBJECT = .NULL.
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore da passare alla TreeView
    *     Piano dei Conti
    do case
      case this.pTipop="Reload"
        if EMPTY(this.oParentObject.w_CURSORNA)
          this.oParentObject.w_CURSORNA = SYS(2015)
        endif
        L_NomeCursore = this.oParentObject.w_CURSORNA
         
 CREATE CURSOR &L_NomeCursore (MASTRO1 C(18), MASTRO2 C(18), MASTRO3 C(18), ; 
 MASTRO4 C(18), MASTRO5 C(18), MASTRO6 C(15), MASTRO7 C(15), ; 
 MASTRO8 C(15), MASTRO9 C(15), MASTRO10 C(15), CONTO C(15), MASTRO C(200), ; 
 DESCRI1 C(40), DESCRI2 C(40), DESCRI3 C(40), DESCRI4 C(40), ; 
 DESCRI5 C(40), DESCRI6 C(40), DESCRI7 C(40), DESCRI8 C(40), ; 
 DESCRI9 C(40), DESCRI10 C(40), DESCRI C(40),TIPCON C(1), SEQSTA1 C(3), SEQSTA2 C(3), SEQSTA3 C(3), SEQSTA4 C(3), SEQSTA5 C(3)) 
 
        * --- Reperimento dati su Voci di bilancio
        ah_Msg("Fase 1: elaborazione mastri...",.T.)
        VQ_EXEC("QUERY\GSAR_BPC.VQR",this,"CursMastro")
        b = WRCURSOR("CursMastro")
        * --- Reperimento dati su Voci di bilancio
        ah_Msg("Fase 2: elaborazione conti...",.T.)
        VQ_EXEC("QUERY\GSAR0BPC.VQR",this,"CursConto")
        b = WRCURSOR("CursConto")
        SELECT CursConto
        GO TOP
        do while Not Eof( "CursConto" )
           
 INSERT INTO &L_NomeCursore (MASTRO1,MASTRO2,MASTRO3,MASTRO4,MASTRO5, ; 
 MASTRO6,MASTRO7,MASTRO8,MASTRO9,MASTRO10,CONTO,MASTRO, ; 
 DESCRI1, DESCRI2, DESCRI3, DESCRI4, DESCRI5, DESCRI6, ; 
 DESCRI7, DESCRI8, DESCRI9, DESCRI10, DESCRI,TIPCON,SEQSTA1,SEQSTA2,SEQSTA3,SEQSTA4,SEQSTA5) ; 
 VALUES (CursConto.Anconsup,CursConto.ANCODICE,"", "", "", "", "", "", "", "", ; 
 CursConto.ANCODICE," ", CursConto.DESCRI1, CursConto.DESCRI, " ", " ", " ", " ", " ", " ", " ", " ", CursConto.DESCRI,CursConto.ANTIPCON,"","","","","")
          if Not Eof( "CursConto" )
             
 Select CursConto 
 Skip
          endif
        enddo
        if used("CursConto")
          select CursConto
          use
        endif
        ah_Msg("Fase 3: creazione struttura...",.T.)
         
 SELECT DISTINCT &L_NomeCursore..*,CursMastro.MCCONSUP, CursMastro.DESCRI1 AS DESCRIN ; 
 FROM (&L_NomeCursore Inner Join CursMastro on left(alltrim( &L_NomeCursore..MASTRO1)+space(20),20)=left(alltrim(CursMastro.MCCODICE)+space(20),20)) ; 
 INTO CURSOR CursSelez WHERE NVL(CursMastro.MCCONSUP," ") <> " " NOFILTER
        do while _tally > 0
          SELECT CursSelez
          GO TOP
          SCAN
           
 UPDATE &L_NomeCursore SET &L_NomeCursore..MASTRO9=CursSelez.MASTRO8, &L_NomeCursore..MASTRO8=CursSelez.MASTRO7, ; 
 &L_NomeCursore..MASTRO7=CursSelez.MASTRO6, &L_NomeCursore..MASTRO6=CursSelez.MASTRO5, ; 
 &L_NomeCursore..MASTRO5=CursSelez.MASTRO4, &L_NomeCursore..MASTRO4=CursSelez.MASTRO3, ; 
 &L_NomeCursore..MASTRO3=CursSelez.MASTRO2, &L_NomeCursore..MASTRO2=CursSelez.MASTRO1, ; 
 &L_NomeCursore..MASTRO1=CursSelez.MCCONSUP, &L_NomeCursore..MASTRO10=CursSelez.MASTRO9, ; 
 &L_NomeCursore..DESCRI9=CursSelez.DESCRI8, &L_NomeCursore..DESCRI8=CursSelez.DESCRI7, ; 
 &L_NomeCursore..DESCRI7=CursSelez.DESCRI6, &L_NomeCursore..DESCRI6=CursSelez.DESCRI5, ; 
 &L_NomeCursore..DESCRI5=CursSelez.DESCRI4, &L_NomeCursore..DESCRI4=CursSelez.DESCRI3, ; 
 &L_NomeCursore..DESCRI3=CursSelez.DESCRI2, &L_NomeCursore..DESCRI2=CursSelez.DESCRI1, ; 
 &L_NomeCursore..DESCRI1=CursSelez.DESCRIN, &L_NomeCursore..DESCRI10=CursSelez.DESCRI9 ; 
 WHERE CursSelez.CONTO = &L_NomeCursore..CONTO And CursSelez.Tipcon=&L_NomeCursore..TipCon
          ENDSCAN
          if used("CursSelez")
            select CursSelez
            use
          endif
           
 SELECT DISTINCT &L_NomeCursore..*,CursMastro.MCCONSUP, CursMastro.DESCRI1 AS DESCRIN ; 
 FROM (&L_NomeCursore Inner Join CursMastro on left(alltrim(&L_NomeCursore..MASTRO1)+space(20),20)=left(alltrim(CursMastro.MCCODICE)+space(20),20)) ; 
 INTO CURSOR CursSelez WHERE NVL(CursMastro.MCCONSUP," ") <> " " NOFILTER
        enddo
        if used("CursSelez")
          select CursSelez
          use
        endif
        * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
        ah_Msg("Fase 4: elaborazione livelli...",.T.)
        SELECT ( L_NomeCursore )
        GO TOP
        SCAN 
        if TIPCON<>"G"
          do case
            case NOT EMPTY(NVL(MASTRO9," "))
              REPLACE MASTRO9 WITH SPACE(15)
            case NOT EMPTY(NVL(MASTRO8," "))
              REPLACE MASTRO8 WITH SPACE(15)
            case NOT EMPTY(NVL(MASTRO7," "))
              REPLACE MASTRO7 WITH SPACE(15)
            case NOT EMPTY(NVL(MASTRO6," "))
              REPLACE MASTRO6 WITH SPACE(15)
            case NOT EMPTY(NVL(MASTRO5," "))
              REPLACE MASTRO5 WITH SPACE(18)
            case NOT EMPTY(NVL(MASTRO4," "))
              REPLACE MASTRO4 WITH SPACE(18)
            case NOT EMPTY(NVL(MASTRO3," "))
              REPLACE MASTRO3 WITH SPACE(18)
            case NOT EMPTY(NVL(MASTRO2," "))
              REPLACE MASTRO2 WITH SPACE(18)
            case NOT EMPTY(NVL(MASTRO1," "))
              REPLACE MASTRO1 WITH SPACE(18)
          endcase
        endif
        if AT(",", NVL(MASTRO5," "))<>0
          REPLACE MASTRO5 WITH STRTRAN(MASTRO5, ",", "^")
        endif
        if AT(",", NVL(MASTRO4," "))<>0
          REPLACE MASTRO4 WITH STRTRAN(MASTRO4, ",", "^")
        endif
        if AT(",", NVL(MASTRO3," "))<>0
          REPLACE MASTRO3 WITH STRTRAN(MASTRO3, ",", "^")
        endif
        if AT(",", NVL(MASTRO2," "))<>0
          REPLACE MASTRO2 WITH STRTRAN(MASTRO2, ",", "^")
        endif
        if AT(",", NVL(MASTRO1," "))<>0
          REPLACE MASTRO1 WITH STRTRAN(MASTRO1, ",", "^")
        endif
        if AT(",", NVL(CONTO," "))<>0
          REPLACE CONTO WITH STRTRAN(CONTO, ",", "^")
        endif
        if AT(",", NVL(MASTRO," "))<>0
          REPLACE MASTRO WITH STRTRAN(MASTRO, ",", "^")
        endif
        ENDSCAN
        * --- Associa ad ogni mastro il valore per la sequenza di stampa.
        * --- Vengono inseriti tanti campi (carattere di 3 e riempiti con zeri) per la valorizzazione della sequenza di stampa quanti sono i mastri
        VQ_EXEC("QUERY\GSARABPC.VQR",this,"CursStamp")
        b = WRCURSOR("CursStamp")
        SELECT (L_NomeCursore)
        GO TOP
        SCAN 
        SELECT Cursstamp
        GO TOP
        SCAN 
         
 UPDATE &L_NomeCursore SET SEQSTA1=IIF(len(ALLTRIM(STR(CursStamp.SEQSTA)))=1,"00"+ALLTRIM(STR(CursStamp.SEQSTA)), ; 
 IIF(LEN(ALLTRIM(STR(CursStamp.SEQSTA)))=2,"0"+ALLTRIM(STR(CursStamp.SEQSTA)),ALLTRIM(STR(CursStamp.SEQSTA)))); 
 WHERE &L_NomeCursore..MASTRO1=CursStamp.Mccodice
        UPDATE &L_NomeCursore SET SEQSTA2=IIF(len(ALLTRIM(STR(CursStamp.SEQSTA)))=1,"00"+ALLTRIM(STR(CursStamp.SEQSTA)), ; 
 IIF(LEN(ALLTRIM(STR(CursStamp.SEQSTA)))=2,"0"+ALLTRIM(STR(CursStamp.SEQSTA)),ALLTRIM(STR(CursStamp.SEQSTA)))); 
 WHERE &L_NomeCursore..MASTRO2=CursStamp.Mccodice
        UPDATE &L_NomeCursore SET SEQSTA3=IIF(len(ALLTRIM(STR(CursStamp.SEQSTA)))=1,"00"+ALLTRIM(STR(CursStamp.SEQSTA)), ; 
 IIF(LEN(ALLTRIM(STR(CursStamp.SEQSTA)))=2,"0"+ALLTRIM(STR(CursStamp.SEQSTA)),ALLTRIM(STR(CursStamp.SEQSTA)))); 
 WHERE &L_NomeCursore..MASTRO3=CursStamp.Mccodice
        UPDATE &L_NomeCursore SET SEQSTA4=IIF(len(ALLTRIM(STR(CursStamp.SEQSTA)))=1,"00"+ALLTRIM(STR(CursStamp.SEQSTA)), ; 
 IIF(LEN(ALLTRIM(STR(CursStamp.SEQSTA)))=2,"0"+ALLTRIM(STR(CursStamp.SEQSTA)),ALLTRIM(STR(CursStamp.SEQSTA)))); 
 WHERE &L_NomeCursore..MASTRO4=CursStamp.Mccodice
        UPDATE &L_NomeCursore SET SEQSTA5=IIF(len(ALLTRIM(STR(CursStamp.SEQSTA)))=1,"00"+ALLTRIM(STR(CursStamp.SEQSTA)), ; 
 IIF(LEN(ALLTRIM(STR(CursStamp.SEQSTA)))=2,"0"+ALLTRIM(STR(CursStamp.SEQSTA)),ALLTRIM(STR(CursStamp.SEQSTA)))); 
 WHERE &L_NomeCursore..MASTRO5=CursStamp.Mccodice
        ENDSCAN
        ENDSCAN
        * --- Modifica nomi mastri per il corretto ordinamento
        SELECT (L_NomeCursore)
        GO TOP
        SCAN 
        UPDATE &L_NomeCursore SET MASTRO1=ALLTRIM(ALLTRIM(SEQSTA1)+ALLTRIM(MASTRO1)) WHERE LEN(ALLTRIM(MASTRO1))<>0
        UPDATE &L_NomeCursore SET MASTRO2=ALLTRIM(ALLTRIM(SEQSTA2)+ALLTRIM(MASTRO2)) WHERE LEN(ALLTRIM(MASTRO2))<>0
        UPDATE &L_NomeCursore SET MASTRO3=ALLTRIM(ALLTRIM(SEQSTA3)+ALLTRIM(MASTRO3)) WHERE LEN(ALLTRIM(MASTRO3))<>0
        UPDATE &L_NomeCursore SET MASTRO4=ALLTRIM(ALLTRIM(SEQSTA4)+ALLTRIM(MASTRO4)) WHERE LEN(ALLTRIM(MASTRO4))<>0
        UPDATE &L_NomeCursore SET MASTRO5=ALLTRIM(ALLTRIM(SEQSTA5)+ALLTRIM(MASTRO5)) WHERE LEN(ALLTRIM(MASTRO5))<>0
        ENDSCAN
        SELECT (L_NomeCursore)
        GO TOP
        cp_Level1(L_NomeCursore,"MASTRO1,MASTRO2,MASTRO3,MASTRO4,MASTRO5,MASTRO6, MASTRO7,MASTRO8,MASTRO9","")
        * --- Riporta nomi mastri a valori originali
        SELECT (L_NomeCursore)
        GO TOP
        SCAN 
        UPDATE &L_NomeCursore SET MASTRO1=ALLTRIM(SUBSTR(MASTRO1,4)) WHERE ((ALLTRIM(MASTRO1) <> CONTO) AND ALLTRIM(SUBSTR(MASTRO1,1,3))=ALLTRIM(SEQSTA1))
        UPDATE &L_NomeCursore SET MASTRO2=ALLTRIM(SUBSTR(MASTRO2,4)) WHERE ((ALLTRIM(MASTRO2) <> CONTO) AND ALLTRIM(SUBSTR(MASTRO2,1,3))=ALLTRIM(SEQSTA2))
        UPDATE &L_NomeCursore SET MASTRO3=ALLTRIM(SUBSTR(MASTRO3,4)) WHERE ((ALLTRIM(MASTRO3) <> CONTO) AND ALLTRIM(SUBSTR(MASTRO3,1,3))=ALLTRIM(SEQSTA3))
        UPDATE &L_NomeCursore SET MASTRO4=ALLTRIM(SUBSTR(MASTRO4,4)) WHERE ((ALLTRIM(MASTRO4) <> CONTO) AND ALLTRIM(SUBSTR(MASTRO4,1,3))=ALLTRIM(SEQSTA4))
        UPDATE &L_NomeCursore SET MASTRO5=ALLTRIM(SUBSTR(MASTRO5,4)) WHERE ((ALLTRIM(MASTRO5) <> CONTO) AND ALLTRIM(SUBSTR(MASTRO5,1,3))=ALLTRIM(SEQSTA5))
        ENDSCAN
        SELECT (L_NomeCursore)
        GO TOP
        SCAN 
        if AT("^", NVL(MASTRO5," "))<>0
          REPLACE MASTRO5 WITH STRTRAN(MASTRO5, "^", ",")
        endif
        if AT("^", NVL(MASTRO4," "))<>0
          REPLACE MASTRO4 WITH STRTRAN(MASTRO4, "^", ",")
        endif
        if AT("^", NVL(MASTRO3," "))<>0
          REPLACE MASTRO3 WITH STRTRAN(MASTRO3, "^", ",")
        endif
        if AT("^", NVL(MASTRO2," "))<>0
          REPLACE MASTRO2 WITH STRTRAN(MASTRO2, "^", ",")
        endif
        if AT("^", NVL(MASTRO1," "))<>0
          REPLACE MASTRO1 WITH STRTRAN(MASTRO1, "^", ",")
        endif
        if AT("^", NVL(CONTO," "))<>0
          REPLACE CONTO WITH STRTRAN(CONTO, "^", ",")
        endif
        if AT("^", NVL(MASTRO," "))<>0
          REPLACE MASTRO WITH STRTRAN(MASTRO, "^", ",")
        endif
        ENDSCAN
        GO TOP
        UPDATE &L_NomeCursore SET &L_NomeCursore..MASTRO=&L_NomeCursore..MASTRO1 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI1 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+1,1)<>"."
        UPDATE &L_NomeCursore SET &L_NomeCursore..MASTRO=&L_NomeCursore..MASTRO2 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI2 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+1
        UPDATE &L_NomeCursore SET &L_NomeCursore..MASTRO=&L_NomeCursore..MASTRO3 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI3 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*2+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4+1
        UPDATE &L_NomeCursore SET &L_NomeCursore..MASTRO=&L_NomeCursore..MASTRO4 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI4 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*3+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*2+1
        UPDATE &L_NomeCursore SET &L_NomeCursore..MASTRO=&L_NomeCursore..MASTRO5 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI5 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*3+1
        UPDATE &L_NomeCursore SET &L_NomeCursore..MASTRO=&L_NomeCursore..MASTRO6 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI6 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*5+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*4+1
        UPDATE &L_NomeCursore SET &L_NomeCursore..MASTRO=&L_NomeCursore..MASTRO7 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI7 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*6+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*5+1
        UPDATE &L_NomeCursore SET &L_NomeCursore..MASTRO=&L_NomeCursore..MASTRO8 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI8 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*7+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*6+1
        UPDATE &L_NomeCursore SET &L_NomeCursore..MASTRO=&L_NomeCursore..MASTRO9 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI9 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*8+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*7+1
        if used("CursStamp")
          select CursStamp
          use
        endif
        if used("CursMastro")
          select CursMastro
          use
        endif
        this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
        this.oParentObject.w_TREEVIEW.FillTree()     
      case this.pTipop="Close"
        * --- Elimina i cursori
        if used( this.oParentObject.w_CURSORNA )
          select ( this.oParentObject.w_CURSORNA )
          use
        endif
        if used("__CURS__")
          select __CURS__
          use
        endif
      case this.pTipOp = "Click"
        this.w_MASTRO = NVL(this.oParentObject.w_TREEVIEW.GETVAR("MASTRO"), SPACE(15))
        this.w_CONTO = NVL(this.oParentObject.w_TREEVIEW.GETVAR("CONTO"), SPACE(15))
        this.w_NUMPUNTI = 1+OCCURS(".", NVL( this.oParentObject.w_TREEVIEW.GETVAR("LVLKEY"), " "))
        this.w_NUMMASTR = 0
        this.w_CICLOVAR = 1
        do while this.w_CICLOVAR<8
          this.w_NUMMASTR = this.w_NUMMASTR+IIF(EMPTY(NVL( this.oParentObject.w_TREEVIEW.GETVAR("MASTRO"+ALLTRIM(STR(this.w_CICLOVAR))), "")), 0, 1)
          this.w_CICLOVAR = this.w_CICLOVAR+1
        enddo
        * --- Verifico se il codice letto � di un conto....
        this.w_CODICE = this.w_CONTO
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANTIPCON"+;
            " from "+i_cTable+" CONTI where ";
                +"ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
                +" and ANTIPCON = "+cp_ToStrODBC("G");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANTIPCON;
            from (i_cTable) where;
                ANCODICE = this.w_CODICE;
                and ANTIPCON = "G";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANTIPCON = NVL(cp_ToDate(_read_.ANTIPCON),cp_NullValue(_read_.ANTIPCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_NUMPUNTI = this.w_NUMMASTR AND this.w_ANTIPCON="G"
          if EMPTY(this.w_CONTO)
            ah_ErrorMsg("Selezionare un codice conto",,"")
            i_retcode = 'stop'
            return
          else
            this.w_OBJECT = GSAR_API()
            if !(this.w_OBJECT.bSec1)
              i_retcode = 'stop'
              return
            endif
            this.w_OBJECT.w_ANCODICE = this.w_CONTO
            this.w_OBJECT.QueryKeySet("ANCODICE=" + cp_ToStrODBC(this.w_CONTO),"")     
          endif
        else
          if EMPTY(this.w_MASTRO)
            ah_ErrorMsg("Selezionare un codice mastro",,"")
            i_retcode = 'stop'
            return
          else
            this.w_OBJECT = GSAR_AMC()
            if !(this.w_OBJECT.bSec1)
              i_retcode = 'stop'
              return
            endif
            this.w_OBJECT.w_MCCODICE = this.w_MASTRO
            this.w_OBJECT.QueryKeySet("MCCODICE=" + cp_ToStrODBC(this.w_MASTRO),"")     
          endif
        endif
        this.w_OBJECT.LoadRecWarn()     
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipOp)
    this.pTipOp=pTipOp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOp"
endproc
