* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bpd                                                        *
*              Controlli articoli                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_51]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-27                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bpd",oParentObject,m.pEXEC)
return(i_retval)

define class tgsma_bpd as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_CODART = space(20)
  w_TOTART = 0
  w_TEST = .f.
  w_MESS = space(100)
  w_LOCODESE = space(4)
  w_LOSERIAL = space(10)
  w_CODLOT = space(20)
  w_FORMAT = space(20)
  w_READAZI = space(5)
  w_OFLFRAZ = space(1)
  w_OF2FRAZ = space(1)
  w_CFUNC = space(10)
  w_CONTA = 0
  w_OLDCOMME = space(15)
  w_OLDQTAUM1 = 0
  w_FLCASC = space(1)
  w_F2CASC = space(1)
  w_FLORD = space(1)
  w_F2ORD = space(1)
  w_FLIMP = space(1)
  w_F2IMP = space(1)
  w_FLRIS = space(1)
  w_F2RIS = space(1)
  w_OLDCODMAG = space(5)
  w_OLDCODMAT = space(5)
  w_KEYSAL = space(40)
  w_CODART = space(20)
  w_CODVAR = space(20)
  w_OLDFCM = space(1)
  w_COMMDEFA = space(15)
  w_OLDCODUBI = space(20)
  w_OLDCODLOT = space(20)
  w_PunPAD = .NULL.
  w_OLDLOTMAG = space(5)
  w_OLDLOTMAT = space(5)
  w_OLDCODART = space(20)
  w_OLDKEYSAL = space(20)
  w_OLDCODUB2 = space(20)
  w_CODCOM = space(15)
  w_RowStatus = space(1)
  * --- WorkFile variables
  SALDIART_idx=0
  LOTTIART_idx=0
  ART_ICOL_idx=0
  AZIENDA_idx=0
  MATRICOL_idx=0
  UNIMIS_idx=0
  DOC_DETT_idx=0
  MVM_DETT_idx=0
  SALDICOM_idx=0
  SALOTCOM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da Gsma_AAR Esegue controllo sulla movimentazione 
    *     dell'articolo per consentire la modifica del flag lotti
    *     pEXEC
    *     A: Controlli  Unit� di Misura e Matricole eventi w_ARUNMIS1 Changed,w_ARGESMAT Changed,w_ARUNMIS2 Changed
    *     B: Aggiornamento Lotti Documenti\Movimenti magazzino evento  Record Updated
    *     C: Controlli Lotti  evento w_ARFLLOTT Changed
    *     M: controlli Matricole all'evento w_ARGESMAT Changed
    *     D: Avvertimento modifica check disponibilit� al variare dei check disponibilit� lotti e  gestioni matricole
    *     S: Aggiornamento saldi commessa movimento di magazzino
    * --- Flag gestione lotti
    do case
      case this.pEXEC="A"
        if Not Empty(this.oParentObject.w_OLDUNI)
          * --- Read from UNIMIS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.UNIMIS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "UMFLFRAZ"+;
              " from "+i_cTable+" UNIMIS where ";
                  +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDUNI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              UMFLFRAZ;
              from (i_cTable) where;
                  UMCODICE = this.oParentObject.w_OLDUNI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OFLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(this.oParentObject.w_OLDUN2)
          * --- Read from UNIMIS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.UNIMIS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "UMFLFRAZ"+;
              " from "+i_cTable+" UNIMIS where ";
                  +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDUN2);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              UMFLFRAZ;
              from (i_cTable) where;
                  UMCODICE = this.oParentObject.w_OLDUN2;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OF2FRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.oParentObject.o_ARGESMAT = iif(Empty(this.oParentObject.o_ARGESMAT)," ",this.oParentObject.o_ARGESMAT)
        if  (this.oParentObject.w_OLDUNI <> this.oParentObject.w_ARUNMIS1) 
          * --- Consente Modifica 1 Unit� di misura solo se articolo non movimentato
          * --- Select from SALDIART
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS TOTART  from "+i_cTable+" SALDIART ";
                +" where SLCODICE="+cp_ToStrODBC(this.oParentObject.w_ARCODART)+"";
                 ,"_Curs_SALDIART")
          else
            select COUNT(*) AS TOTART from (i_cTable);
             where SLCODICE=this.oParentObject.w_ARCODART;
              into cursor _Curs_SALDIART
          endif
          if used('_Curs_SALDIART')
            select _Curs_SALDIART
            locate for 1=1
            do while not(eof())
            this.w_TOTART = NVL(TOTART,0)
              select _Curs_SALDIART
              continue
            enddo
            use
          endif
          if this.w_TOTART > 0 
            this.oParentObject.w_ARUNMIS1 = this.oParentObject.w_OLDUNI
            this.oParentObject.w_FLFRAZ = this.w_OFLFRAZ
            this.w_MESS = "Impossibile modificare prima unit� di misura, %1 � presente nell'archivio saldi articoli"
            ah_ErrorMsg(this.w_MESS,"!","",alltrim(this.oParentObject.w_ARCODART))
            i_retcode = 'stop'
            return
          endif
        endif
        if this.oParentObject.w_ARGESMAT="S"
          if this.oParentObject.w_FLFRAZ<>"S" 
            this.oParentObject.w_ARUNMIS1 = this.oParentObject.w_OLDUNI
            this.oParentObject.w_FLFRAZ = this.w_OFLFRAZ
            if this.oParentObject.w_ARGESMAT<>this.oParentObject.o_ARGESMAT
              this.oParentObject.w_ARGESMAT = " "
            endif
            this.w_MESS = "Attenzione impossibile utilizzare unit� di misura frazionabile, con articolo gestito a matricole"
            ah_ErrorMsg(this.w_MESS,"!")
            i_retcode = 'stop'
            return
          endif
          if this.oParentObject.w_F2FRAZ<>"S" AND NOT EMPTY(this.oParentObject.w_ARUNMIS2) 
            this.oParentObject.w_ARUNMIS2 = this.oParentObject.w_OLDUN2
            this.oParentObject.w_F2FRAZ = this.w_OF2FRAZ
            if this.oParentObject.w_ARGESMAT<>this.oParentObject.o_ARGESMAT
              this.oParentObject.w_ARGESMAT = " "
            endif
            this.w_MESS = "Attenzione impossibile utilizzare seconda unit� di misura frazionabile, con articolo gestito a matricole"
            ah_ErrorMsg(this.w_MESS,"!")
            i_retcode = 'stop'
            return
          endif
        endif
      case this.pEXEC="B"
        * --- Aggiornamento nei Documenti\Movimenti  codice Lotto
        this.w_CFUNC = This.oparentobject.cFunction
        if (this.oParentObject.w_ARFLLOTT<>this.oParentObject.w_OLDFLAG) AND this.oParentObject.w_ARFLLOTT $ "SC" AND (this.oParentObject.w_OLDFLAG="N" OR EMPTY(this.oParentObject.w_OLDFLAG)) AND g_PERLOT= "S" AND this.w_CFUNC <> "Load"
          this.w_TEST = .F.
          this.w_READAZI = i_CODAZI
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZMASLOT"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(this.w_READAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZMASLOT;
              from (i_cTable) where;
                  AZCODAZI = this.w_READAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FORMAT = NVL(cp_ToDate(_read_.AZMASLOT),cp_NullValue(_read_.AZMASLOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if EMPTY(this.w_FORMAT)
            this.w_CODLOT = this.oParentObject.w_ARCODART
          endif
          this.w_CODART = this.oParentObject.w_ARCODART
          do GSMD_KAL with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_TEST=.F.
            * --- Ripristino valore originario
            this.oParentObject.w_ARFLLOTT = this.oParentObject.w_OLDFLAG
            * --- Write into ART_ICOL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ARFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLDFLAG),'ART_ICOL','ARFLLOTT');
                  +i_ccchkf ;
              +" where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
                     )
            else
              update (i_cTable) set;
                  ARFLLOTT = this.oParentObject.w_OLDFLAG;
                  &i_ccchkf. ;
               where;
                  ARCODART = this.oParentObject.w_ARCODART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      case this.pEXEC="C"
        if this.oParentObject.o_ARFLLOTT $ "C-S" and this.oParentObject.w_ARFLLOTT="N"
          * --- Select from GSMA_BPD
          do vq_exec with 'GSMA_BPD',this,'_Curs_GSMA_BPD','',.f.,.t.
          if used('_Curs_GSMA_BPD')
            select _Curs_GSMA_BPD
            locate for 1=1
            do while not(eof())
            if Nvl( _Curs_GSMA_BPD.TEST, 0 )>0 
              ah_ErrorMsg("L'articolo � associato a lotti movimentati. Impossibile disattivare la gestione","!")
              this.oParentObject.w_ARFLLOTT = this.oParentObject.o_ARFLLOTT
              this.oParentObject.w_OLDFLAG = this.oParentObject.o_ARFLLOTT
              EXIT
            endif
              select _Curs_GSMA_BPD
              continue
            enddo
            use
          endif
        endif
      case this.pEXEC="D"
        if this.oParentObject.w_ARFLDISP<>this.oParentObject.o_ARFLDISP
          ah_errormsg("Attenzione, � stata modificata la valorizzazione della combo Check Disponibilit� presente nella scheda articolo")
          this.oParentObject.o_ARFLDISP = this.oParentObject.w_ARFLDISP
        endif
      case this.pEXEC="M" and g_MATR="S" And This.oParentObject.cFunction<>"Load"
        * --- Verifico al cambio del check Matricole se questo � possibile (solo in modifica)
        this.w_CONTA = 0
        if this.oParentObject.w_ARGESMAT="S"
          * --- Per poter attivare il flag matricole l'articolo non deve avere movimenti successivi la data inizio check Matricole
          * --- Select from gsmambdp
          do vq_exec with 'gsmambdp',this,'_Curs_gsmambdp','',.f.,.t.
          if used('_Curs_gsmambdp')
            select _Curs_gsmambdp
            locate for 1=1
            do while not(eof())
            this.w_CONTA = this.w_CONTA + Nvl ( _Curs_gsmambdp.CONTA , 0 )
              select _Curs_gsmambdp
              continue
            enddo
            use
          endif
          if this.w_CONTA>0
            ah_ErrorMsg("L'articolo � stato movimentato successivamente alla data di inizio controlli matricole. Impossibile attivare la gestione","!")
            this.oParentObject.w_ARGESMAT = " "
            i_retcode = 'stop'
            return
          endif
          * --- Controllo caratteristiche Unit� di misura per consentire attivazione delle matrcole
          if this.oParentObject.w_FLFRAZ<>"S" AND this.oParentObject.w_ARGESMAT="S"
            this.oParentObject.w_ARGESMAT = " "
            this.w_MESS = "Prima unit� di misura frazionabile, impossibile attivare matricole"
            ah_ErrorMsg(this.w_MESS,"!")
            i_retcode = 'stop'
            return
          endif
          if this.oParentObject.w_F2FRAZ<>"S" AND this.oParentObject.w_ARGESMAT="S" AND NOT EMPTY(this.oParentObject.w_ARUNMIS2)
            this.oParentObject.w_ARGESMAT = " "
            this.w_MESS = "Seconda unit� di misura frazionabile, impossibile attivare matricole"
            ah_ErrorMsg(this.w_MESS,"!")
            i_retcode = 'stop'
            return
          endif
        else
          * --- Per poter disattivare il check l'articolo non deve avere associata nessuna matricola
          * --- Select from MATRICOL
          i_nConn=i_TableProp[this.MATRICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2],.t.,this.MATRICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select Count(*) as Conta  from "+i_cTable+" MATRICOL ";
                +" where AMCODART= "+cp_ToStrODBC(this.oParentObject.w_ARCODART)+"";
                 ,"_Curs_MATRICOL")
          else
            select Count(*) as Conta from (i_cTable);
             where AMCODART= this.oParentObject.w_ARCODART;
              into cursor _Curs_MATRICOL
          endif
          if used('_Curs_MATRICOL')
            select _Curs_MATRICOL
            locate for 1=1
            do while not(eof())
            this.w_CONTA = Nvl ( _Curs_MATRICOL.CONTA , 0 )
              select _Curs_MATRICOL
              continue
            enddo
            use
          endif
          if this.w_CONTA>0
            ah_ErrorMsg("L'articolo ha associate delle matricole [%1]. Impossibile disattivare la gestione","!","", Alltrim(Str(this.w_CONTA)) )
            this.oParentObject.w_ARGESMAT = "S"
            i_retcode = 'stop'
            return
          endif
        endif
      case this.pEXEC="S"
        * --- Aggiornamento Saldi Commessa
        * --- Caller
        * --- Locali
        * --- Leggo il vecchio valore dal Documento
        * --- Read from MVM_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MVM_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MMCODCOM,MMQTAUM1,MMFLCASC,MMFLORDI,MMFLIMPE,MMFLRISE,MMF2CASC,MMF2ORDI,MMF2IMPE,MMF2RISE,MMCODMAG,MMCODMAT,MMCODART,MMKEYSAL,MMCODLOT,MMCODUBI,MMCODUB2,MMLOTMAG,MMLOTMAT"+;
            " from "+i_cTable+" MVM_DETT where ";
                +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MMSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MMCODCOM,MMQTAUM1,MMFLCASC,MMFLORDI,MMFLIMPE,MMFLRISE,MMF2CASC,MMF2ORDI,MMF2IMPE,MMF2RISE,MMCODMAG,MMCODMAT,MMCODART,MMKEYSAL,MMCODLOT,MMCODUBI,MMCODUB2,MMLOTMAG,MMLOTMAT;
            from (i_cTable) where;
                MMSERIAL = this.oParentObject.w_MMSERIAL;
                and CPROWNUM = this.oParentObject.w_CPROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDCOMME = NVL(cp_ToDate(_read_.MMCODCOM),cp_NullValue(_read_.MMCODCOM))
          this.w_OLDQTAUM1 = NVL(cp_ToDate(_read_.MMQTAUM1),cp_NullValue(_read_.MMQTAUM1))
          this.w_FLCASC = NVL(cp_ToDate(_read_.MMFLCASC),cp_NullValue(_read_.MMFLCASC))
          this.w_FLORD = NVL(cp_ToDate(_read_.MMFLORDI),cp_NullValue(_read_.MMFLORDI))
          this.w_FLIMP = NVL(cp_ToDate(_read_.MMFLIMPE),cp_NullValue(_read_.MMFLIMPE))
          this.w_FLRIS = NVL(cp_ToDate(_read_.MMFLRISE),cp_NullValue(_read_.MMFLRISE))
          this.w_F2CASC = NVL(cp_ToDate(_read_.MMF2CASC),cp_NullValue(_read_.MMF2CASC))
          this.w_F2ORD = NVL(cp_ToDate(_read_.MMF2ORDI),cp_NullValue(_read_.MMF2ORDI))
          this.w_F2IMP = NVL(cp_ToDate(_read_.MMF2IMPE),cp_NullValue(_read_.MMF2IMPE))
          this.w_F2RIS = NVL(cp_ToDate(_read_.MMF2RISE),cp_NullValue(_read_.MMF2RISE))
          this.w_OLDCODMAG = NVL(cp_ToDate(_read_.MMCODMAG),cp_NullValue(_read_.MMCODMAG))
          this.w_OLDCODMAT = NVL(cp_ToDate(_read_.MMCODMAT),cp_NullValue(_read_.MMCODMAT))
          this.w_OLDCODART = NVL(cp_ToDate(_read_.MMCODART),cp_NullValue(_read_.MMCODART))
          this.w_OLDKEYSAL = NVL(cp_ToDate(_read_.MMKEYSAL),cp_NullValue(_read_.MMKEYSAL))
          this.w_OLDCODLOT = NVL(cp_ToDate(_read_.MMCODLOT),cp_NullValue(_read_.MMCODLOT))
          this.w_OLDCODUBI = NVL(cp_ToDate(_read_.MMCODUBI),cp_NullValue(_read_.MMCODUBI))
          this.w_OLDCODUB2 = NVL(cp_ToDate(_read_.MMCODUB2),cp_NullValue(_read_.MMCODUB2))
          this.w_OLDLOTMAG = NVL(cp_ToDate(_read_.MMLOTMAG),cp_NullValue(_read_.MMLOTMAG))
          this.w_OLDLOTMAT = NVL(cp_ToDate(_read_.MMLOTMAT),cp_NullValue(_read_.MMLOTMAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_OLDCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_OLDCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDFCM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PunPAD = this.oParentObject
        * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
        this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
        this.oParentObject.w_MMCODCOM = NVL(this.oParentObject.w_MMCODCOM, SPACE(15))
        this.w_OLDCOMME = NVL(this.w_OLDCOMME, SPACE(15))
        if this.oParentObject.w_FLCOM1="S" or this.w_OLDFCM="S"
          if Empty(this.oParentObject.w_MMCODCOM)
            this.w_CODCOM = this.w_COMMDEFA
          else
            this.w_CODCOM = this.oParentObject.w_MMCODCOM
          endif
          if Empty(this.w_OLDCOMME)
            this.w_OLDCOMME = this.w_COMMDEFA
          endif
          this.w_FLCASC = IIF(this.w_FLCASC="+", "-", IIF(this.w_FLCASC="-", "+", " "))
          this.w_FLORD = IIF(this.w_FLORD="+", "-", IIF(this.w_FLORD="-", "+", " "))
          this.w_FLIMP = IIF(this.w_FLIMP="+", "-", IIF(this.w_FLIMP="-", "+", " "))
          this.w_FLRIS = IIF(this.w_FLRIS="+", "-", IIF(this.w_FLRIS="-", "+", " "))
          this.w_F2CASC = IIF(this.w_F2CASC="+", "-", IIF(this.w_F2CASC="-", "+", " "))
          this.w_F2ORD = IIF(this.w_F2ORD="+", "-", IIF(this.w_F2ORD="-", "+", " "))
          this.w_F2IMP = IIF(this.w_F2IMP="+", "-", IIF(this.w_F2IMP="-", "+", " "))
          this.w_F2RIS = IIF(this.w_F2RIS="+", "-", IIF(this.w_F2RIS="-", "+", " "))
          this.w_RowStatus = this.w_PunPad.RowStatus()
          if this.w_OLDFCM="S" And inlist(this.w_PunPAD.cFunction, "Query", "Edit") and this.w_RowStatus <> "A"
            * --- Storno i Saldi Commessa
            if NOT EMPTY(this.w_FLCASC+this.w_FLRIS+this.w_FLORD+this.w_FLIMP) AND NOT EMPTY(this.w_OLDCODMAG)
              * --- Magazzino principale
              * --- Try
              local bErr_0428CEF8
              bErr_0428CEF8=bTrsErr
              this.Try_0428CEF8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_0428CEF8
              * --- End
              if g_MADV="S"
                if (g_PERUBI = "S" OR g_PERLOT = "S") and !Empty(this.w_OLDLOTMAG)
                  * --- Try
                  local bErr_0428C808
                  bErr_0428C808=bTrsErr
                  this.Try_0428C808()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_0428C808
                  * --- End
                endif
              endif
            endif
            if NOT EMPTY(this.w_F2CASC+this.w_F2RIS+this.w_F2ORD+this.w_F2IMP) AND NOT EMPTY(this.w_OLDCODMAT)
              * --- Magazzino collegato
              * --- Try
              local bErr_0428E818
              bErr_0428E818=bTrsErr
              this.Try_0428E818()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_0428E818
              * --- End
              if g_MADV="S"
                if (g_PERUBI = "S" OR g_PERLOT = "S") and !Empty(this.w_OLDLOTMAT)
                  * --- Try
                  local bErr_0428E0F8
                  bErr_0428E0F8=bTrsErr
                  this.Try_0428E0F8()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_0428E0F8
                  * --- End
                endif
              endif
            endif
          endif
          if this.oParentObject.w_FLCOM1="S" And Inlist(this.w_PunPAD.cFunction, "Load", "Edit") and this.w_RowStatus $ "A-U"
            * --- Caso in cui non sono in interrogazione , quindi non st� cancellando
            * --- Aggiorno i saldi commessa con i nuovi valori.
            if NOT EMPTY(this.oParentObject.w_MMFLCASC+this.oParentObject.w_MMFLRISE+this.oParentObject.w_MMFLORDI+this.oParentObject.w_MMFLIMPE) and !Empty(this.oParentObject.w_MMCODMAG)
              * --- Magazzino principale
              * --- Try
              local bErr_04280958
              bErr_04280958=bTrsErr
              this.Try_04280958()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_04280958
              * --- End
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.oParentObject.w_MMFLCASC,'SCQTAPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.oParentObject.w_MMFLORDI,'SCQTOPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.oParentObject.w_MMFLIMPE,'SCQTIPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
                i_cOp4=cp_SetTrsOp(this.oParentObject.w_MMFLRISE,'SCQTRPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
                +",SCQTRPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MMKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MMCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                       )
              else
                update (i_cTable) set;
                    SCQTAPER = &i_cOp1.;
                    ,SCQTOPER = &i_cOp2.;
                    ,SCQTIPER = &i_cOp3.;
                    ,SCQTRPER = &i_cOp4.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.oParentObject.w_MMKEYSAL;
                    and SCCODMAG = this.oParentObject.w_MMCODMAG;
                    and SCCODCAN = this.w_CODCOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore Aggiornamento Saldi Commessa'
                return
              endif
              if g_MADV="S"
                if (g_PERUBI = "S" OR g_PERLOT = "S") and !Empty(this.oParentObject.w_MMLOTMAG)
                  * --- Try
                  local bErr_042804D8
                  bErr_042804D8=bTrsErr
                  this.Try_042804D8()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_042804D8
                  * --- End
                  * --- Write into SALOTCOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALOTCOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
                    i_cOp1=cp_SetTrsOp(this.oParentObject.w_MMFLCASC,'SMQTAPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.oParentObject.w_MMFLRISE,'SMQTRPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
                    +",SMQTRPER ="+cp_NullLink(i_cOp2,'SALOTCOM','SMQTRPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SMCODART = "+cp_ToStrODBC(this.oParentObject.w_MMCODART);
                        +" and SMCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MMLOTMAG);
                        +" and SMCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                        +" and SMCODUBI = "+cp_ToStrODBC(this.oParentObject.w_MMCODUBI);
                        +" and SMCODLOT = "+cp_ToStrODBC(this.oParentObject.w_MMCODLOT);
                           )
                  else
                    update (i_cTable) set;
                        SMQTAPER = &i_cOp1.;
                        ,SMQTRPER = &i_cOp2.;
                        &i_ccchkf. ;
                     where;
                        SMCODART = this.oParentObject.w_MMCODART;
                        and SMCODMAG = this.oParentObject.w_MMLOTMAG;
                        and SMCODCAN = this.w_CODCOM;
                        and SMCODUBI = this.oParentObject.w_MMCODUBI;
                        and SMCODLOT = this.oParentObject.w_MMCODLOT;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
            if NOT EMPTY(this.oParentObject.w_MMF2CASC+this.oParentObject.w_MMF2RISE+this.oParentObject.w_MMF2ORDI+this.oParentObject.w_MMF2IMPE) AND NOT EMPTY(this.oParentObject.w_MMCODMAT)
              * --- Magazzino collegato
              * --- Try
              local bErr_042827E8
              bErr_042827E8=bTrsErr
              this.Try_042827E8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_042827E8
              * --- End
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.oParentObject.w_MMF2CASC,'SCQTAPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.oParentObject.w_MMF2ORDI,'SCQTOPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.oParentObject.w_MMF2IMPE,'SCQTIPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
                i_cOp4=cp_SetTrsOp(this.oParentObject.w_MMF2RISE,'SCQTRPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
                +",SCQTRPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MMKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MMCODMAT);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                       )
              else
                update (i_cTable) set;
                    SCQTAPER = &i_cOp1.;
                    ,SCQTOPER = &i_cOp2.;
                    ,SCQTIPER = &i_cOp3.;
                    ,SCQTRPER = &i_cOp4.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.oParentObject.w_MMKEYSAL;
                    and SCCODMAG = this.oParentObject.w_MMCODMAT;
                    and SCCODCAN = this.w_CODCOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore Aggiornamento Saldi Commessa'
                return
              endif
              if g_MADV="S"
                if (g_PERUBI = "S" OR g_PERLOT = "S") and !Empty(this.oParentObject.w_MMLOTMAT)
                  * --- Try
                  local bErr_04281B88
                  bErr_04281B88=bTrsErr
                  this.Try_04281B88()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_04281B88
                  * --- End
                  * --- Write into SALOTCOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALOTCOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
                    i_cOp1=cp_SetTrsOp(this.oParentObject.w_MMF2CASC,'SMQTAPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.oParentObject.w_MMF2RISE,'SMQTRPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
                    +",SMQTRPER ="+cp_NullLink(i_cOp2,'SALOTCOM','SMQTRPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SMCODART = "+cp_ToStrODBC(this.oParentObject.w_MMCODART);
                        +" and SMCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MMLOTMAT);
                        +" and SMCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                        +" and SMCODUBI = "+cp_ToStrODBC(w_MMCODUB2);
                        +" and SMCODLOT = "+cp_ToStrODBC(this.oParentObject.w_MMCODLOT);
                           )
                  else
                    update (i_cTable) set;
                        SMQTAPER = &i_cOp1.;
                        ,SMQTRPER = &i_cOp2.;
                        &i_ccchkf. ;
                     where;
                        SMCODART = this.oParentObject.w_MMCODART;
                        and SMCODMAG = this.oParentObject.w_MMLOTMAT;
                        and SMCODCAN = this.w_CODCOM;
                        and SMCODUBI = w_MMCODUB2;
                        and SMCODLOT = this.oParentObject.w_MMCODLOT;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
          endif
        endif
        * --- Evito la chiamata alla mCalc comunque fatta dopo la Valid dei vari campi
        this.bUpdateParentObject=.F.
    endcase
  endproc
  proc Try_0428CEF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SCQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_FLRIS,'SCQTRPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
      +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_OLDKEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLDCODMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLDCOMME);
             )
    else
      update (i_cTable) set;
          SCQTAPER = &i_cOp1.;
          ,SCQTOPER = &i_cOp2.;
          ,SCQTIPER = &i_cOp3.;
          ,SCQTRPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_OLDKEYSAL;
          and SCCODMAG = this.w_OLDCODMAG;
          and SCCODCAN = this.w_OLDCOMME;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_0428C808()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALOTCOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SMQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLRIS,'SMQTRPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
      +",SMQTRPER ="+cp_NullLink(i_cOp2,'SALOTCOM','SMQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SMCODART = "+cp_ToStrODBC(this.w_OLDKEYSAL);
          +" and SMCODMAG = "+cp_ToStrODBC(this.w_OLDLOTMAG);
          +" and SMCODCAN = "+cp_ToStrODBC(this.w_OLDCOMME);
          +" and SMCODUBI = "+cp_ToStrODBC(this.w_OLDCODUBI);
          +" and SMCODLOT = "+cp_ToStrODBC(this.w_OLDCODLOT);
             )
    else
      update (i_cTable) set;
          SMQTAPER = &i_cOp1.;
          ,SMQTRPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SMCODART = this.w_OLDKEYSAL;
          and SMCODMAG = this.w_OLDLOTMAG;
          and SMCODCAN = this.w_OLDCOMME;
          and SMCODUBI = this.w_OLDCODUBI;
          and SMCODLOT = this.w_OLDCODLOT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0428E818()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_F2CASC,'SCQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_F2ORD,'SCQTOPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_F2IMP,'SCQTIPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_F2RIS,'SCQTRPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
      +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_OLDKEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLDCODMAT);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLDCOMME);
             )
    else
      update (i_cTable) set;
          SCQTAPER = &i_cOp1.;
          ,SCQTOPER = &i_cOp2.;
          ,SCQTIPER = &i_cOp3.;
          ,SCQTRPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_OLDKEYSAL;
          and SCCODMAG = this.w_OLDCODMAT;
          and SCCODCAN = this.w_OLDCOMME;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_0428E0F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALOTCOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_F2CASC,'SMQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_F2RIS,'SMQTRPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
      +",SMQTRPER ="+cp_NullLink(i_cOp2,'SALOTCOM','SMQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SMCODART = "+cp_ToStrODBC(this.w_OLDKEYSAL);
          +" and SMCODMAG = "+cp_ToStrODBC(this.w_OLDLOTMAT);
          +" and SMCODCAN = "+cp_ToStrODBC(this.w_OLDCOMME);
          +" and SMCODUBI = "+cp_ToStrODBC(this.w_OLDCODUB2);
          +" and SMCODLOT = "+cp_ToStrODBC(this.w_OLDCODLOT);
             )
    else
      update (i_cTable) set;
          SMQTAPER = &i_cOp1.;
          ,SMQTRPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SMCODART = this.w_OLDKEYSAL;
          and SMCODMAG = this.w_OLDLOTMAT;
          and SMCODCAN = this.w_OLDCOMME;
          and SMCODUBI = this.w_OLDCODUB2;
          and SMCODLOT = this.w_OLDCODLOT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04280958()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.oParentObject.w_MMKEYSAL,'SCCODMAG',this.oParentObject.w_MMCODMAG,'SCCODCAN',this.w_CODCOM,'SCCODART',this.oParentObject.w_MMCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.oParentObject.w_MMKEYSAL;
           ,this.oParentObject.w_MMCODMAG;
           ,this.w_CODCOM;
           ,this.oParentObject.w_MMCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_042804D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALOTCOM
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SMCODART"+",SMCODMAG"+",SMCODCAN"+",SMCODUBI"+",SMCODLOT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMCODART),'SALOTCOM','SMCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMLOTMAG),'SALOTCOM','SMCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'SALOTCOM','SMCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMCODUBI),'SALOTCOM','SMCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMCODLOT),'SALOTCOM','SMCODLOT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SMCODART',this.oParentObject.w_MMCODART,'SMCODMAG',this.oParentObject.w_MMLOTMAG,'SMCODCAN',this.w_CODCOM,'SMCODUBI',this.oParentObject.w_MMCODUBI,'SMCODLOT',this.oParentObject.w_MMCODLOT)
      insert into (i_cTable) (SMCODART,SMCODMAG,SMCODCAN,SMCODUBI,SMCODLOT &i_ccchkf. );
         values (;
           this.oParentObject.w_MMCODART;
           ,this.oParentObject.w_MMLOTMAG;
           ,this.w_CODCOM;
           ,this.oParentObject.w_MMCODUBI;
           ,this.oParentObject.w_MMCODLOT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_042827E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMCODMAT),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.oParentObject.w_MMKEYSAL,'SCCODMAG',this.oParentObject.w_MMCODMAT,'SCCODCAN',this.w_CODCOM,'SCCODART',this.oParentObject.w_MMCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.oParentObject.w_MMKEYSAL;
           ,this.oParentObject.w_MMCODMAT;
           ,this.w_CODCOM;
           ,this.oParentObject.w_MMCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04281B88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALOTCOM
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SMCODART"+",SMCODMAG"+",SMCODCAN"+",SMCODUBI"+",SMCODLOT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMCODART),'SALOTCOM','SMCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMLOTMAT),'SALOTCOM','SMCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'SALOTCOM','SMCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(w_MMCODUB2),'SALOTCOM','SMCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMCODLOT),'SALOTCOM','SMCODLOT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SMCODART',this.oParentObject.w_MMCODART,'SMCODMAG',this.oParentObject.w_MMLOTMAT,'SMCODCAN',this.w_CODCOM,'SMCODUBI',w_MMCODUB2,'SMCODLOT',this.oParentObject.w_MMCODLOT)
      insert into (i_cTable) (SMCODART,SMCODMAG,SMCODCAN,SMCODUBI,SMCODLOT &i_ccchkf. );
         values (;
           this.oParentObject.w_MMCODART;
           ,this.oParentObject.w_MMLOTMAT;
           ,this.w_CODCOM;
           ,w_MMCODUB2;
           ,this.oParentObject.w_MMCODLOT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='SALDIART'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='MATRICOL'
    this.cWorkTables[6]='UNIMIS'
    this.cWorkTables[7]='DOC_DETT'
    this.cWorkTables[8]='MVM_DETT'
    this.cWorkTables[9]='SALDICOM'
    this.cWorkTables[10]='SALOTCOM'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_SALDIART')
      use in _Curs_SALDIART
    endif
    if used('_Curs_GSMA_BPD')
      use in _Curs_GSMA_BPD
    endif
    if used('_Curs_gsmambdp')
      use in _Curs_gsmambdp
    endif
    if used('_Curs_MATRICOL')
      use in _Curs_MATRICOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
