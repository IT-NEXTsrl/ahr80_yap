* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bgs                                                        *
*              Lancia programma collegato                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_43]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2006-03-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO,pCODI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bgs",oParentObject,m.pTIPO,m.pCODI)
return(i_retval)

define class tgsar_bgs as StdBatch
  * --- Local variables
  pTIPO = space(1)
  pCODI = space(15)
  w_CONTO = .NULL.
  w_KEY = space(15)
  w_COD = space(15)
  * --- WorkFile variables
  CONTI_idx=0
  SALDICON_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Anagrafica Saldi da un Programma (da GSAR_API, GSAR_ACL, GSAR_AFR)
    * --- chiave...
    this.w_KEY = ALLTRIM(this.pCODI)
    this.w_COD = " "
    * --- Read from SALDICON
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2],.t.,this.SALDICON_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SLCODICE"+;
        " from "+i_cTable+" SALDICON where ";
            +"SLTIPCON = "+cp_ToStrODBC(this.pTIPO);
            +" and SLCODICE = "+cp_ToStrODBC(this.w_KEY);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SLCODICE;
        from (i_cTable) where;
            SLTIPCON = this.pTIPO;
            and SLCODICE = this.w_KEY;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COD = NVL(cp_ToDate(_read_.SLCODICE),cp_NullValue(_read_.SLCODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pTIPO="G"
        * --- Saldi Conti Generici
        this.w_CONTO = GSCG_ASP()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_CONTO.bSec1)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        if this.w_COD<>" "
          this.w_CONTO.ecpFilter()     
          this.w_CONTO.w_SLTIPCON = "G"
          this.w_CONTO.w_SLCODICE = this.pCODI
          this.w_CONTO.w_SLCODESE = g_CODESE
          this.w_CONTO.ecpSave()     
          this.w_CONTO.ecpQuery()     
        else
          this.w_CONTO.BlankRec()     
        endif
      case this.pTIPO="C"
        * --- Saldi Clienti
        this.w_CONTO = GSCG_ASC()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_CONTO.bSec1)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        if this.w_COD<>" "
          this.w_CONTO.ecpFilter()     
          this.w_CONTO.w_SLTIPCON = "C"
          this.w_CONTO.w_SLCODICE = this.pCODI
          this.w_CONTO.w_SLCODESE = g_CODESE
          this.w_CONTO.ecpSave()     
          this.w_CONTO.ecpQuery()     
        else
          this.w_CONTO.BlankRec()     
        endif
      case this.pTIPO="F"
        * --- Saldi Fornitori
        this.w_CONTO = GSCG_ASF()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_CONTO.bSec1)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        if this.w_COD<>" "
          this.w_CONTO.ecpFilter()     
          this.w_CONTO.w_SLTIPCON = "F"
          this.w_CONTO.w_SLCODICE = this.pCODI
          this.w_CONTO.w_SLCODESE = g_CODESE
          this.w_CONTO.ecpSave()     
          this.w_CONTO.ecpQuery()     
        else
          this.w_CONTO.BlankRec()     
        endif
    endcase
    * --- Non deve fare il refresh del chiamante
    this.bUpdateParentObject=.F.
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggio di errore
    Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
  endproc


  proc Init(oParentObject,pTIPO,pCODI)
    this.pTIPO=pTIPO
    this.pCODI=pCODI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='SALDICON'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO,pCODI"
endproc
