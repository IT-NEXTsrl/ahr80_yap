* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_atb                                                        *
*              Tributi                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_36]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2018-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_atb"))

* --- Class definition
define class tgsar_atb as StdForm
  Top    = 20
  Left   = 83

  * --- Standard Properties
  Width  = 568
  Height = 129+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-01-23"
  HelpContextID=92895081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  TRI_BUTI_IDX = 0
  COD_TRIB_IDX = 0
  CAU_INPS_IDX = 0
  COD_PREV_IDX = 0
  CAU_AEN_IDX = 0
  cFile = "TRI_BUTI"
  cKeySelect = "TRCODTRI"
  cKeyWhere  = "TRCODTRI=this.w_TRCODTRI"
  cKeyWhereODBC = '"TRCODTRI="+cp_ToStrODBC(this.w_TRCODTRI)';

  cKeyWhereODBCqualified = '"TRI_BUTI.TRCODTRI="+cp_ToStrODBC(this.w_TRCODTRI)';

  cPrg = "gsar_atb"
  cComment = "Tributi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TRCODTRI = space(5)
  w_TRFLACON = space(1)
  o_TRFLACON = space(1)
  w_TRDESTRI = space(60)
  w_TRCOTRIF = space(5)
  w_TRCODENT = space(5)
  w_TRCAUCON = space(5)
  w_TRCAUPRE = space(5)
  w_TRCAUPRE2 = space(2)
  w_TRREGAGE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TRI_BUTI','gsar_atb')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_atbPag1","gsar_atb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tributi")
      .Pages(1).HelpContextID = 164655050
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRCODTRI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='COD_TRIB'
    this.cWorkTables[2]='CAU_INPS'
    this.cWorkTables[3]='COD_PREV'
    this.cWorkTables[4]='CAU_AEN'
    this.cWorkTables[5]='TRI_BUTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TRI_BUTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TRI_BUTI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TRCODTRI = NVL(TRCODTRI,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TRI_BUTI where TRCODTRI=KeySet.TRCODTRI
    *
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TRI_BUTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TRI_BUTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TRI_BUTI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRCODTRI',this.w_TRCODTRI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TRCODTRI = NVL(TRCODTRI,space(5))
        .w_TRFLACON = NVL(TRFLACON,space(1))
        .w_TRDESTRI = NVL(TRDESTRI,space(60))
        .w_TRCOTRIF = NVL(TRCOTRIF,space(5))
          * evitabile
          *.link_1_4('Load')
        .w_TRCODENT = NVL(TRCODENT,space(5))
          * evitabile
          *.link_1_5('Load')
        .w_TRCAUCON = NVL(TRCAUCON,space(5))
          * evitabile
          *.link_1_6('Load')
        .w_TRCAUPRE = NVL(TRCAUPRE,space(5))
          * evitabile
          *.link_1_7('Load')
        .w_TRCAUPRE2 = NVL(TRCAUPRE2,space(2))
        .w_TRREGAGE = NVL(TRREGAGE,space(1))
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        cp_LoadRecExtFlds(this,'TRI_BUTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TRCODTRI = space(5)
      .w_TRFLACON = space(1)
      .w_TRDESTRI = space(60)
      .w_TRCOTRIF = space(5)
      .w_TRCODENT = space(5)
      .w_TRCAUCON = space(5)
      .w_TRCAUPRE = space(5)
      .w_TRCAUPRE2 = space(2)
      .w_TRREGAGE = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_TRFLACON = "R"
        .DoRTCalc(3,4,.f.)
          if not(empty(.w_TRCOTRIF))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_TRCODENT))
          .link_1_5('Full')
          endif
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_TRCAUCON))
          .link_1_6('Full')
          endif
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_TRCAUPRE))
          .link_1_7('Full')
          endif
          .DoRTCalc(8,8,.f.)
        .w_TRREGAGE = IIF(.w_TRFLACON='R', .w_TRREGAGE, ' ')
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TRI_BUTI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTRCODTRI_1_1.enabled = i_bVal
      .Page1.oPag.oTRFLACON_1_2.enabled = i_bVal
      .Page1.oPag.oTRDESTRI_1_3.enabled = i_bVal
      .Page1.oPag.oTRCOTRIF_1_4.enabled = i_bVal
      .Page1.oPag.oTRCODENT_1_5.enabled = i_bVal
      .Page1.oPag.oTRCAUCON_1_6.enabled = i_bVal
      .Page1.oPag.oTRCAUPRE_1_7.enabled = i_bVal
      .Page1.oPag.oTRCAUPRE2_1_8.enabled = i_bVal
      .Page1.oPag.oTRREGAGE_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTRCODTRI_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTRCODTRI_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TRI_BUTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODTRI,"TRCODTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRFLACON,"TRFLACON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRDESTRI,"TRDESTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCOTRIF,"TRCOTRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODENT,"TRCODENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCAUCON,"TRCAUCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCAUPRE,"TRCAUPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCAUPRE2,"TRCAUPRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRREGAGE,"TRREGAGE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    i_lTable = "TRI_BUTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TRI_BUTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_ST2 with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TRI_BUTI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TRI_BUTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TRI_BUTI')
        i_extval=cp_InsertValODBCExtFlds(this,'TRI_BUTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TRCODTRI,TRFLACON,TRDESTRI,TRCOTRIF,TRCODENT"+;
                  ",TRCAUCON,TRCAUPRE,TRCAUPRE2,TRREGAGE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TRCODTRI)+;
                  ","+cp_ToStrODBC(this.w_TRFLACON)+;
                  ","+cp_ToStrODBC(this.w_TRDESTRI)+;
                  ","+cp_ToStrODBCNull(this.w_TRCOTRIF)+;
                  ","+cp_ToStrODBCNull(this.w_TRCODENT)+;
                  ","+cp_ToStrODBCNull(this.w_TRCAUCON)+;
                  ","+cp_ToStrODBCNull(this.w_TRCAUPRE)+;
                  ","+cp_ToStrODBC(this.w_TRCAUPRE2)+;
                  ","+cp_ToStrODBC(this.w_TRREGAGE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TRI_BUTI')
        i_extval=cp_InsertValVFPExtFlds(this,'TRI_BUTI')
        cp_CheckDeletedKey(i_cTable,0,'TRCODTRI',this.w_TRCODTRI)
        INSERT INTO (i_cTable);
              (TRCODTRI,TRFLACON,TRDESTRI,TRCOTRIF,TRCODENT,TRCAUCON,TRCAUPRE,TRCAUPRE2,TRREGAGE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TRCODTRI;
                  ,this.w_TRFLACON;
                  ,this.w_TRDESTRI;
                  ,this.w_TRCOTRIF;
                  ,this.w_TRCODENT;
                  ,this.w_TRCAUCON;
                  ,this.w_TRCAUPRE;
                  ,this.w_TRCAUPRE2;
                  ,this.w_TRREGAGE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TRI_BUTI_IDX,i_nConn)
      *
      * update TRI_BUTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TRI_BUTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TRFLACON="+cp_ToStrODBC(this.w_TRFLACON)+;
             ",TRDESTRI="+cp_ToStrODBC(this.w_TRDESTRI)+;
             ",TRCOTRIF="+cp_ToStrODBCNull(this.w_TRCOTRIF)+;
             ",TRCODENT="+cp_ToStrODBCNull(this.w_TRCODENT)+;
             ",TRCAUCON="+cp_ToStrODBCNull(this.w_TRCAUCON)+;
             ",TRCAUPRE="+cp_ToStrODBCNull(this.w_TRCAUPRE)+;
             ",TRCAUPRE2="+cp_ToStrODBC(this.w_TRCAUPRE2)+;
             ",TRREGAGE="+cp_ToStrODBC(this.w_TRREGAGE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TRI_BUTI')
        i_cWhere = cp_PKFox(i_cTable  ,'TRCODTRI',this.w_TRCODTRI  )
        UPDATE (i_cTable) SET;
              TRFLACON=this.w_TRFLACON;
             ,TRDESTRI=this.w_TRDESTRI;
             ,TRCOTRIF=this.w_TRCOTRIF;
             ,TRCODENT=this.w_TRCODENT;
             ,TRCAUCON=this.w_TRCAUCON;
             ,TRCAUPRE=this.w_TRCAUPRE;
             ,TRCAUPRE2=this.w_TRCAUPRE2;
             ,TRREGAGE=this.w_TRREGAGE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TRI_BUTI_IDX,i_nConn)
      *
      * delete TRI_BUTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TRCODTRI',this.w_TRCODTRI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
        if .o_TRFLACON<>.w_TRFLACON
            .w_TRREGAGE = IIF(.w_TRFLACON='R', .w_TRREGAGE, ' ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTRCAUPRE_1_7.enabled = this.oPgFrm.Page1.oPag.oTRCAUPRE_1_7.mCond()
    this.oPgFrm.Page1.oPag.oTRCAUPRE2_1_8.enabled = this.oPgFrm.Page1.oPag.oTRCAUPRE2_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTRCOTRIF_1_4.visible=!this.oPgFrm.Page1.oPag.oTRCOTRIF_1_4.mHide()
    this.oPgFrm.Page1.oPag.oTRCODENT_1_5.visible=!this.oPgFrm.Page1.oPag.oTRCODENT_1_5.mHide()
    this.oPgFrm.Page1.oPag.oTRCAUCON_1_6.visible=!this.oPgFrm.Page1.oPag.oTRCAUCON_1_6.mHide()
    this.oPgFrm.Page1.oPag.oTRCAUPRE_1_7.visible=!this.oPgFrm.Page1.oPag.oTRCAUPRE_1_7.mHide()
    this.oPgFrm.Page1.oPag.oTRCAUPRE2_1_8.visible=!this.oPgFrm.Page1.oPag.oTRCAUPRE2_1_8.mHide()
    this.oPgFrm.Page1.oPag.oTRREGAGE_1_9.visible=!this.oPgFrm.Page1.oPag.oTRREGAGE_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TRCOTRIF
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCOTRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_TRCOTRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_TRCOTRIF))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCOTRIF)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCOTRIF) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oTRCOTRIF_1_4'),i_cWhere,'GSCG_ATR',"Codici tributo F24",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCOTRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_TRCOTRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_TRCOTRIF)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCOTRIF = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TRCOTRIF = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCOTRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCODENT
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_PREV_IDX,3]
    i_lTable = "COD_PREV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2], .t., this.COD_PREV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODENT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACP',True,'COD_PREV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CPCODICE like "+cp_ToStrODBC(trim(this.w_TRCODENT)+"%");

          i_ret=cp_SQL(i_nConn,"select CPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CPCODICE',trim(this.w_TRCODENT))
          select CPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODENT)==trim(_Link_.CPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODENT) and !this.bDontReportError
            deferred_cp_zoom('COD_PREV','*','CPCODICE',cp_AbsName(oSource.parent,'oTRCODENT_1_5'),i_cWhere,'GSCG_ACP',"Codici enti previdenziali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODICE',oSource.xKey(1))
            select CPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODENT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CPCODICE="+cp_ToStrODBC(this.w_TRCODENT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODICE',this.w_TRCODENT)
            select CPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODENT = NVL(_Link_.CPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODENT = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])+'\'+cp_ToStr(_Link_.CPCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_PREV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODENT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCAUCON
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_TRCAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_TRCAUCON))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCAUCON)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oTRCAUCON_1_6'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_TRCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_TRCAUCON)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCAUCON = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TRCAUCON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCAUPRE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_AEN_IDX,3]
    i_lTable = "CAU_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2], .t., this.CAU_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCAUPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MAE',True,'CAU_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AECAUSEN like "+cp_ToStrODBC(trim(this.w_TRCAUPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select AECAUSEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AECAUSEN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AECAUSEN',trim(this.w_TRCAUPRE))
          select AECAUSEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AECAUSEN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCAUPRE)==trim(_Link_.AECAUSEN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCAUPRE) and !this.bDontReportError
            deferred_cp_zoom('CAU_AEN','*','AECAUSEN',cp_AbsName(oSource.parent,'oTRCAUPRE_1_7'),i_cWhere,'GSCG_MAE',"Causali contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECAUSEN',oSource.xKey(1))
            select AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCAUPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECAUSEN";
                   +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(this.w_TRCAUPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECAUSEN',this.w_TRCAUPRE)
            select AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCAUPRE = NVL(_Link_.AECAUSEN,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TRCAUPRE = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])+'\'+cp_ToStr(_Link_.AECAUSEN,1)
      cp_ShowWarn(i_cKey,this.CAU_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCAUPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTRCODTRI_1_1.value==this.w_TRCODTRI)
      this.oPgFrm.Page1.oPag.oTRCODTRI_1_1.value=this.w_TRCODTRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTRFLACON_1_2.RadioValue()==this.w_TRFLACON)
      this.oPgFrm.Page1.oPag.oTRFLACON_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRDESTRI_1_3.value==this.w_TRDESTRI)
      this.oPgFrm.Page1.oPag.oTRDESTRI_1_3.value=this.w_TRDESTRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCOTRIF_1_4.value==this.w_TRCOTRIF)
      this.oPgFrm.Page1.oPag.oTRCOTRIF_1_4.value=this.w_TRCOTRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCODENT_1_5.value==this.w_TRCODENT)
      this.oPgFrm.Page1.oPag.oTRCODENT_1_5.value=this.w_TRCODENT
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCAUCON_1_6.value==this.w_TRCAUCON)
      this.oPgFrm.Page1.oPag.oTRCAUCON_1_6.value=this.w_TRCAUCON
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCAUPRE_1_7.value==this.w_TRCAUPRE)
      this.oPgFrm.Page1.oPag.oTRCAUPRE_1_7.value=this.w_TRCAUPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCAUPRE2_1_8.RadioValue()==this.w_TRCAUPRE2)
      this.oPgFrm.Page1.oPag.oTRCAUPRE2_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRREGAGE_1_9.RadioValue()==this.w_TRREGAGE)
      this.oPgFrm.Page1.oPag.oTRREGAGE_1_9.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'TRI_BUTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TRCODTRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCODTRI_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TRCODTRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TRFLACON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRFLACON_1_2.SetFocus()
            i_bnoObbl = !empty(.w_TRFLACON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TRCAUPRE))  and not(.w_TRFLACON<>'P')  and (NOT EMPTY(.w_TRCODENT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCAUPRE_1_7.SetFocus()
            i_bnoObbl = !empty(.w_TRCAUPRE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TRCAUPRE2))  and not(.w_TRFLACON<>'R')  and (.w_TRFLACON='R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCAUPRE2_1_8.SetFocus()
            i_bnoObbl = !empty(.w_TRCAUPRE2)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TRFLACON = this.w_TRFLACON
    return

enddefine

* --- Define pages as container
define class tgsar_atbPag1 as StdContainer
  Width  = 607
  height = 129
  stdWidth  = 607
  stdheight = 129
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRCODTRI_1_1 as StdField with uid="DHNTONNTOC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TRCODTRI", cQueryName = "TRCODTRI",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 50991231,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=123, Top=13, InputMask=replicate('X',5)


  add object oTRFLACON_1_2 as StdCombo with uid="JPBFFVJHAH",rtseq=2,rtrep=.f.,left=421,top=11,width=135,height=21;
    , HelpContextID = 237551484;
    , cFormVar="w_TRFLACON",RowSource=""+"IRPEF,"+"INPS,"+"Previdenziali", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTRFLACON_1_2.RadioValue()
    return(iif(this.value =1,"R",;
    iif(this.value =2,"I",;
    iif(this.value =3,"P",;
    "X"))))
  endfunc
  func oTRFLACON_1_2.GetRadio()
    this.Parent.oContained.w_TRFLACON = this.RadioValue()
    return .t.
  endfunc

  func oTRFLACON_1_2.SetRadio()
    this.Parent.oContained.w_TRFLACON=trim(this.Parent.oContained.w_TRFLACON)
    this.value = ;
      iif(this.Parent.oContained.w_TRFLACON=="R",1,;
      iif(this.Parent.oContained.w_TRFLACON=="I",2,;
      iif(this.Parent.oContained.w_TRFLACON=="P",3,;
      0)))
  endfunc

  add object oTRDESTRI_1_3 as StdField with uid="SBMRRFWWRU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TRDESTRI", cQueryName = "TRDESTRI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 66068607,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=123, Top=43, InputMask=replicate('X',60)

  add object oTRCOTRIF_1_4 as StdField with uid="RCKDQULESE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TRCOTRIF", cQueryName = "TRCOTRIF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo F24",;
    HelpContextID = 234221444,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=123, Top=73, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_TRCOTRIF"

  func oTRCOTRIF_1_4.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'R')
    endwith
  endfunc

  func oTRCOTRIF_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCOTRIF_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCOTRIF_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oTRCOTRIF_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo F24",'',this.parent.oContained
  endproc
  proc oTRCOTRIF_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_TRCOTRIF
     i_obj.ecpSave()
  endproc

  add object oTRCODENT_1_5 as StdField with uid="HNBRIGTPXU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TRCODENT", cQueryName = "TRCODENT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 200666998,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=123, Top=73, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_PREV", cZoomOnZoom="GSCG_ACP", oKey_1_1="CPCODICE", oKey_1_2="this.w_TRCODENT"

  func oTRCODENT_1_5.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'P')
    endwith
  endfunc

  func oTRCODENT_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODENT_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCODENT_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_PREV','*','CPCODICE',cp_AbsName(this.parent,'oTRCODENT_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACP',"Codici enti previdenziali",'',this.parent.oContained
  endproc
  proc oTRCODENT_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CPCODICE=this.parent.oContained.w_TRCODENT
     i_obj.ecpSave()
  endproc

  add object oTRCAUCON_1_6 as StdField with uid="LSFYCZJCME",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TRCAUCON", cQueryName = "TRCAUCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 217313148,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=123, Top=73, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_TRCAUCON"

  func oTRCAUCON_1_6.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'I')
    endwith
  endfunc

  func oTRCAUCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCAUCON_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCAUCON_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oTRCAUCON_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oTRCAUCON_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_TRCAUCON
     i_obj.ecpSave()
  endproc

  add object oTRCAUPRE_1_7 as StdField with uid="JOOWOMDRSR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TRCAUPRE", cQueryName = "TRCAUPRE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo altri enti",;
    HelpContextID = 790651,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=494, Top=73, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_AEN", cZoomOnZoom="GSCG_MAE", oKey_1_1="AECAUSEN", oKey_1_2="this.w_TRCAUPRE"

  func oTRCAUPRE_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TRCODENT))
    endwith
   endif
  endfunc

  func oTRCAUPRE_1_7.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'P')
    endwith
  endfunc

  func oTRCAUPRE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCAUPRE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCAUPRE_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_AEN','*','AECAUSEN',cp_AbsName(this.parent,'oTRCAUPRE_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MAE',"Causali contributo",'',this.parent.oContained
  endproc
  proc oTRCAUPRE_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MAE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AECAUSEN=this.parent.oContained.w_TRCAUPRE
     i_obj.ecpSave()
  endproc


  add object oTRCAUPRE2_1_8 as StdCombo with uid="WSDCCQWIEQ",rtseq=8,rtrep=.f.,left=123,top=101,width=76,height=21;
    , ToolTipText = "Valore predefinito causale prestazione";
    , HelpContextID = 791451;
    , cFormVar="w_TRCAUPRE2",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTRCAUPRE2_1_8.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'Z',;
    iif(this.value =33,'ZO',;
    space(2)))))))))))))))))))))))))))))))))))
  endfunc
  func oTRCAUPRE2_1_8.GetRadio()
    this.Parent.oContained.w_TRCAUPRE2 = this.RadioValue()
    return .t.
  endfunc

  func oTRCAUPRE2_1_8.SetRadio()
    this.Parent.oContained.w_TRCAUPRE2=trim(this.Parent.oContained.w_TRCAUPRE2)
    this.value = ;
      iif(this.Parent.oContained.w_TRCAUPRE2=='A',1,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='B',2,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='C',3,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='D',4,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='E',5,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='F',6,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='G',7,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='H',8,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='I',9,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='J',10,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='K',11,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='L',12,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='L1',13,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='M',14,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='M1',15,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='M2',16,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='N',17,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='O',18,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='O1',19,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='P',20,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='Q',21,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='R',22,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='S',23,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='T',24,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='U',25,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='V',26,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='V1',27,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='V2',28,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='W',29,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='X',30,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='Y',31,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='Z',32,;
      iif(this.Parent.oContained.w_TRCAUPRE2=='ZO',33,;
      0)))))))))))))))))))))))))))))))))
  endfunc

  func oTRCAUPRE2_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRFLACON='R')
    endwith
   endif
  endfunc

  func oTRCAUPRE2_1_8.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'R')
    endwith
  endfunc

  add object oTRREGAGE_1_9 as StdCheck with uid="PBEXTLPWGA",rtseq=9,rtrep=.f.,left=435, top=100, caption="Regime agevolato",;
    ToolTipText = "Se attivo, � possibile effettuare versamenti a zero",;
    HelpContextID = 3211387,;
    cFormVar="w_TRREGAGE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRREGAGE_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTRREGAGE_1_9.GetRadio()
    this.Parent.oContained.w_TRREGAGE = this.RadioValue()
    return .t.
  endfunc

  func oTRREGAGE_1_9.SetRadio()
    this.Parent.oContained.w_TRREGAGE=trim(this.Parent.oContained.w_TRREGAGE)
    this.value = ;
      iif(this.Parent.oContained.w_TRREGAGE=='S',1,;
      0)
  endfunc

  func oTRREGAGE_1_9.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'R')
    endwith
  endfunc


  add object oObj_1_18 as cp_runprogram with uid="UIJBJBUZMD",left=40, top=188, width=128,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSRI_BTR",;
    cEvent = "Delete end",;
    nPag=1;
    , HelpContextID = 85102566

  add object oStr_1_10 as StdString with uid="FENQHUADBD",Visible=.t., Left=24, Top=13,;
    Alignment=1, Width=97, Height=15,;
    Caption="Codice tributo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="EFFHGVXTBT",Visible=.t., Left=24, Top=43,;
    Alignment=1, Width=97, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GWBQEPVQMG",Visible=.t., Left=303, Top=13,;
    Alignment=1, Width=113, Height=15,;
    Caption="Tipologia tributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="FBAAOYTHTU",Visible=.t., Left=3, Top=103,;
    Alignment=1, Width=118, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'R')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="EHQTZESXAB",Visible=.t., Left=9, Top=74,;
    Alignment=1, Width=112, Height=18,;
    Caption="Codice tributo F24:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'R')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="QVBFHTLQSE",Visible=.t., Left=2, Top=74,;
    Alignment=1, Width=119, Height=18,;
    Caption="Causale contr. INPS:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'I')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="ZTLGOVYAAJ",Visible=.t., Left=51, Top=74,;
    Alignment=1, Width=70, Height=18,;
    Caption="Codice ente:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'P')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="BPPLACELHI",Visible=.t., Left=322, Top=74,;
    Alignment=1, Width=170, Height=18,;
    Caption="Causale contr. altri enti:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_TRFLACON<>'P')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_atb','TRI_BUTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRCODTRI=TRI_BUTI.TRCODTRI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
