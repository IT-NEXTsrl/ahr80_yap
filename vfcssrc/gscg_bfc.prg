* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bfc                                                        *
*              Controlla chiusura esercizi                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_34]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-25                                                      *
* Last revis.: 2001-09-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bfc",oParentObject)
return(i_retval)

define class tgscg_bfc as StdBatch
  * --- Local variables
  * --- WorkFile variables
  SALDICON_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla se l'esercizio relativo a un determinato conto � stato chiuso (Da GSCG_ASC, GSCG_ASF,GSCG_ASP)
    vq_exec("QUERY\GSCG_CCF.VQR",this,"__TMP__")
    if RECCOUNT("__TMP__")>0
      this.oParentObject.w_CONTROL = "S"
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SALDICON'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
