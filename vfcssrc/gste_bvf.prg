* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bvf                                                        *
*              Visualizza cash flow                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-04-08                                                      *
* Last revis.: 2015-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bvf",oParentObject,m.pTipo)
return(i_retval)

define class tgste_bvf as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_INIZIO_SALIVA = ctod("  /  /  ")
  w_COLONNA = 0
  w_ZOOMCONTI = space(10)
  w_ZOOMFLOW = space(10)
  w_NUMCONTI = 0
  CFMSK = .NULL.
  w_GIORNI = 0
  w_INIZIO_PER = ctod("  /  /  ")
  w_FINE_CALC = ctod("  /  /  ")
  w_PERIODI_CALC = 0
  w_PERIODI = 0
  w_NUMCOR = space(15)
  w_FLAGIO = space(1)
  w_CODESE = space(4)
  w_COUNTER = 0
  w_ESEPRE = space(4)
  w_FINPRE = ctod("  /  /  ")
  w_CODAZI = space(4)
  w_CODES1 = space(4)
  w_VALPRE = space(3)
  w_TEMP = 0
  w_POSI = 0
  w_NCONTI = 0
  w_TOTALEAPP = 0
  w_CODBAN = space(15)
  w_CAOAPE = 0
  w_DATAPE = ctod("  /  /  ")
  w_FILEFF = space(10)
  w_DATAVUOTA = ctod("  /  /  ")
  w_MPTIPPAG = space(2)
  w_TITLE = ctod("  /  /  ")
  w_GGSET = 0
  GRID = .NULL.
  w_ZOOM = .NULL.
  w_ZOOMPA = .NULL.
  w_ZOOMCB = .NULL.
  w_TipoZoom = space(1)
  w_DATE_INIZIO = ctod("  /  /  ")
  w_DATAINISCA = ctod("  /  /  ")
  w_DATAFINSCA = ctod("  /  /  ")
  w_RETCURS = space(50)
  w_GIOSMO = 0
  w_DIVIDEEFF = 0
  w_BACONCOL = space(15)
  w_NUMREC = 0
  w_TEMP_RID = 0
  w_IMPRID = 0
  w_PERRID = 0
  w_FLAGSMOB = space(1)
  w_DATASMOB = ctod("  /  /  ")
  w_IMPSMOB = 0
  w_DECTOT = 0
  w_ORDINA = space(1)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_ROWNUM = 0
  w_February = .f.
  w_DVALMAG = 0
  w_TVALMAG = 0
  w_VALIVA = 0
  w_SERIAL = space(10)
  w_VALNAZ = space(3)
  w_TOTALE = 0
  w_TIPRIG = space(1)
  w_PERIVA = 0
  w_FLOMAG = space(1)
  w_PREZZO = 0
  w_IMPEVA = 0
  w_FLEVAS = space(1)
  w_DECTOP = 0
  w_TOTRAT = 0
  w_IMPNAZ = 0
  w_IMPACC = 0
  w_QTAEV1 = 0
  w_QTAMOV = 0
  w_QTAEVA = 0
  w_VALORERIGA = 0
  w_IMPRAT = 0
  w_DATRAT = ctod("  /  /  ")
  w_NUMRAT = 0
  w_DIFFRAT = 0
  w_CFCODVOC = space(15)
  w_CFCODICE = space(15)
  w_CFCODCOM = space(15)
  w_PERCEN = 0
  w_FINE_SALIVA = ctod("  /  /  ")
  w_DATADOC = ctod("  /  /  ")
  w_INIZIO_ELAB = ctod("  /  /  ")
  * --- WorkFile variables
  ESERCIZI_idx=0
  TMPFL3_idx=0
  TMPFL1_idx=0
  AZIENDA_idx=0
  CASHFLOW_idx=0
  DOC_RATE_idx=0
  TMPCASHATT_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_DATAVUOTA = Cp_CharToDate("  -  -    ")
    do case
      case this.pTipo="Elabora"
        this.w_ZOOM = This.oParentObject.w_Zoomdetcf
        this.w_ZOOMPA = This.oParentObject.w_Zoomdetpa
        this.w_ZOOMCB = This.oParentObject.w_Zoomdetcb
        if this.oParentObject.w_TIPOVISUA="C"
          This.oParentObject.w_Zoomdetpa.Visible=.f.
          This.oParentObject.w_Zoomdetcb.Visible=.f.
          This.oParentObject.w_Zoomdetcf.Visible=.t.
        else
          This.oParentObject.w_Zoomdetpa.Visible=.t.
          This.oParentObject.w_Zoomdetcb.Visible=.t.
          This.oParentObject.w_Zoomdetcf.Visible=.f.
        endif
        ZAP IN ( this.oParentObject.w_Zoomdetcf.cCursor )
        ZAP IN ( this.oParentObject.w_Zoomdetpa.cCursor )
        ZAP IN ( this.oParentObject.w_Zoomdetcb.cCursor )
        this.CFMSK = THIS.OPARENTOBJECT
        this.w_GIORNI = IIF(this.oParentObject.w_TIPPER="G",1,IIF(this.oParentObject.w_TIPPER="S",7,IIF(this.oParentObject.w_TIPPER="Q",15,30)))
        this.w_ZOOMFLOW = iif(this.oParentObject.w_TipoVisua="C",this.oParentObject.w_zoomdetcf.cCursor,this.oParentObject.w_zoomdetpa.cCursor)
        this.w_ZOOMCONTI = this.oParentObject.w_zoomcf.cCursor
        this.w_INIZIO_PER = this.oParentObject.w_DATINI - 1
        if this.w_GIORNI<>1
          do case
            case this.w_GIORNI=7 OR this.w_GIORNI=15
              this.w_INIZIO_PER = this.oParentObject.w_DATINI - DOW(this.oParentObject.w_DATINI,2) + 1
            case this.w_GIORNI=30
              this.w_INIZIO_PER = DATE(YEAR(this.oParentObject.w_DATINI),MONTH(this.oParentObject.w_DATINI),1)
          endcase
        endif
        this.w_FINE_CALC = this.oParentObject.w_DATFIN
        this.w_PERIODI = 9999999999
        this.w_PERIODI_CALC = 0
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_PERIODI_CALC>this.w_PERIODI
          this.w_PERIODI_CALC = this.w_PERIODI
        endif
        if this.w_PERIODI_CALC<=0
          this.w_PERIODI_CALC = 1
        endif
        this.w_PERIODI = this.w_PERIODI_CALC
        * --- seleziono il cursore dello Zoom
        if this.oParentObject.w_TipoVisua="C"
          this.w_NUMCONTI = 1
          * --- Scorro il cursore per cercare i conti marcati
          SELECT ( this.w_ZOOMCONTI )
          GO TOP
          SCAN FOR XCHK=1
          * --- Contiene il nome dei conti utilizzati
          DIMENSION this.CFMSK.CODCONTI(this.w_NUMCONTI,2)
          DIMENSION this.CFMSK.TIPOCONTI(this.w_NUMCONTI)
          this.w_NUMCOR = NVL(BACODBAN, SPACE(15))
          this.w_BACONCOL = NVL(BACONCOL, SPACE(15))
          this.CFMSK.CODCONTI(this.w_NUMCONTI,1) = this.w_NUMCOR
          this.CFMSK.CODCONTI(this.w_NUMCONTI,2) = this.w_BACONCOL
          this.CFMSK.TIPOCONTI(this.w_NUMCONTI) = iif(NVL(ORDINE,1)=0,"N",iif(NVL(ORDINE,1)=1, "C","G"))
          this.w_NUMCONTI = this.w_NUMCONTI + 1
          ENDSCAN
          * --- Assegno la variabile caller CONTINUM
          DIMENSION this.CFMSK.CODCONTI(this.w_NUMCONTI,2)
          DIMENSION this.CFMSK.TIPOCONTI(this.w_NUMCONTI)
          this.w_NUMCONTI = this.w_NUMCONTI -1
          this.oParentObject.w_CONTINUM = this.w_NUMCONTI
          this.CFMSK.CODCONTI(this.w_NUMCONTI+1,1) = "TOTALI"
          this.CFMSK.CODCONTI(this.w_NUMCONTI+1,2) = " "
          this.CFMSK.TIPOCONTI(this.w_NUMCONTI+1) = " "
        else
          this.w_NUMCONTI = 1
          * --- Query che restituisce le tipologie pagamento
          vq_exec("query\GSTE6BFLV.VQR",this,"TipoPag")
          * --- Scorro il cursore per cercare i conti marcati
          SELECT TipoPag
          GO TOP
          SCAN
          * --- Contiene il nome dei conti utilizzati
          DIMENSION this.CFMSK.CODCONTI(this.w_NUMCONTI,2)
          DIMENSION this.CFMSK.TIPOCONTI(this.w_NUMCONTI)
          this.w_MPTIPPAG = NVL(MPTIPPAG, SPACE(2))
          this.CFMSK.CODCONTI(this.w_NUMCONTI,1) = this.w_MPTIPPAG
          this.CFMSK.CODCONTI(this.w_NUMCONTI,2) = " "
          this.CFMSK.TIPOCONTI(this.w_NUMCONTI) = iif(this.w_Mptippag="RD","Rimessa diretta",iif(this.w_Mptippag="BO","Bonifico",iif(this.w_Mptippag="RB","Ric.bancaria/Ri.Ba.",iif(this.w_Mptippag="RI","R.I.D.",iif(this.w_Mptippag="CA","Cambiale","M.AV.")))))
          this.w_NUMCONTI = this.w_NUMCONTI + 1
          ENDSCAN
          * --- Assegno la variabile caller CONTINUM
          DIMENSION this.CFMSK.CODCONTI(this.w_NUMCONTI,2)
          DIMENSION this.CFMSK.TIPOCONTI(this.w_NUMCONTI)
          this.w_NUMCONTI = this.w_NUMCONTI -1
          this.oParentObject.w_CONTINUM = this.w_NUMCONTI
          this.CFMSK.CODCONTI(this.w_NUMCONTI+1,1) = "TOTALI"
          this.CFMSK.CODCONTI(this.w_NUMCONTI+1,2) = " "
          this.CFMSK.TIPOCONTI(this.w_NUMCONTI+1) = " "
        endif
        if this.w_PERIODI>100
          AH_ERRORMSG("Intervallo di date troppo grande%0Ridurre l'intervallo" , 48 , "" ,)
          use in select ("TipoPag")
          i_retcode = 'stop'
          return
        endif
        * --- Contiene i periodi da elaborare
        DIMENSION this.CFMSK.PERIODO ( this.w_PERIODI )
        * --- Contiene il valore del cash flow certo, diviso per periodo
        DIMENSION this.CFMSK.CERTI ( this.w_NUMCONTI,this.w_PERIODI )
        * --- Contiene il valore del cash flow effettivo, diviso per periodo
        DIMENSION this.CFMSK.EFFETTIVI (this.w_NUMCONTI, this.w_PERIODI )
        * --- Contiene il valore del cash flow atteso, diviso per periodo
        DIMENSION this.CFMSK.ATTESI ( this.w_NUMCONTI, this.w_PERIODI )
        * --- Contiene il valore del cash flow saldo stimato iva certo, diviso per periodo
        DIMENSION this.CFMSK.SALIVAC ( this.w_NUMCONTI, this.w_PERIODI )
        * --- Contiene il valore del cash flow saldo stimato iva teorico, diviso per periodo
        DIMENSION this.CFMSK.SALIVAT ( this.w_NUMCONTI, this.w_PERIODI )
        for i=1 to this.w_PERIODI
        for k=1 to this.w_NUMCONTI
        this.CFMSK.CERTI(K,I) = 0
        this.CFMSK.EFFETTIVI(K,I) = 0
        this.CFMSK.ATTESI(K,I) = 0
        this.CFMSK.SALIVAC(K,I) = 0
        this.CFMSK.SALIVAT(K,I) = 0
        ENDFOR
        this.CFMSK.PERIODO(I) = 0
        ENDFOR
        * --- Elaborazione cash flow. Passo il cursore dello zoom in un cursore di appoggio
        Select Bacodban as Codban,Ordine as Ordine from (this.w_ZoomConti) where xchk=1 And Not Empty(Nvl(Bacodban," ")) into cursor Filbanca
        * --- Creo il cursore temporeaneo TMPFL1 per avere la struttura dei dati
        *     elaborati per il cash flow certo ed atteso
        * --- Create temporary table TMPFL1
        i_nIdx=cp_AddTableDef('TMPFL1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSTE2BFLV',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPFL1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Copio l'elenco dei conti banca selezionati nel cursore temporane TMPFL2
        CURTOTAB("Filbanca" ,"TMPFL2")
        if this.oParentObject.w_TIPCER="S"
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_TIPATT="S"
          * --- Insert into TMPFL1
          i_nConn=i_TableProp[this.TMPFL1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPFL1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSTE3BFLV",this.TMPFL1_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if this.oParentObject.w_TIPEFF="S"
          * --- Nel caso di cash flow effettivo, verifico se � stato inserito il filtro sullo
          *     "Smobilizzo scaduto"
          if Not Empty(this.oParentObject.w_SMOSCA)
            this.w_RETCURS = readtable("SMOBSCAD", "*", "", .F., .F., "SSCODICE="+cp_ToStrODBC(this.oParentObject.w_SMOSCA), .F., "CURS_SMOBSCAD")
          endif
          this.w_FILEFF = Space(10)
          this.w_DATAINISCA = this.oParentObject.w_DATINI
          if this.oParentObject.w_FLSCAD="S"
            this.oParentObject.w_DATINI = Cp_CharToDate("  -  -    ")
          endif
          vq_exec("query\GSTE7BFL.VQR",this,"SELEPART")
          if RECCOUNT("SELEPART")>0
            Cur = WrCursor("Selepart")
            GSTE_BCP(this,"A","Selepart"," ")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            Select BANNOS AS CODBAN,IIF(Nvl(SEGNO," ")="D" AND Nvl(CAOVAL,0)<>0,ABS(TOTIMP), TOTIMP*0) AS IMPDAR,; 
 IIF(Nvl(SEGNO," ")="A" AND Nvl(CAOVAL,0)<>0,ABS(TOTIMP),TOTIMP*0) AS IMPAVE,; 
 IIF(Cp_Todate(DATSCA)<=(this.w_DATAINISCA-1),"B","A") as ORDINA,; 
 Cp_Todate(DATSCA) AS DATVAL,CAOAPE,DATAPE,ORDINEZOOM AS ORDINE,; 
 "E" AS TIPOCASHFLOW,CODVAL AS PTCODVAL,NUMPAR AS PTNUMPAR,TIPCON AS PTTIPCON,; 
 CODCON AS PTCODCON,MODPAG AS PTMODPAG,; 
 ANDESCRI AS ANDESCRI,TIPPAG AS MPTIPPAG,ANCATCOM AS ANCATCOM,DECTOT AS DECTOT,PTSERIAL,PTROWORD,CPROWNUM; 
 FROM SELEPART Into Cursor CashEffettivo NoFilter
            * --- Passo il risultato del cursore CashEffettivo al cursore temporaneo TMPFL1
            Select CashEffettivo 
 Go Top 
 Scan
            this.w_PTSERIAL = CashEffettivo.PTSERIAL
            this.w_PTROWORD = CashEffettivo.PTROWORD
            this.w_ROWNUM = CashEffettivo.CPROWNUM
            this.w_FLAGSMOB = " "
            this.w_DECTOT = Nvl(CashEffettivo.Dectot,2)
            this.w_IMPSMOB = Nvl(CashEffettivo.Impdar,0)-Nvl(CashEffettivo.Impave,0)
            this.w_DATASMOB = Cp_Todate(CashEffettivo.Datval)
            this.w_ORDINA = CashEffettivo.Ordina
            if CashEffettivo.Mptippag=this.oParentObject.w_PriPagRd or CashEffettivo.Mptippag=this.oParentObject.w_PriPagRi or CashEffettivo.Mptippag=this.oParentObject.w_PriPagBo or CashEffettivo.Mptippag=this.oParentObject.w_PriPagRb or CashEffettivo.Mptippag=this.oParentObject.w_PriPagCa or CashEffettivo.Mptippag=this.oParentObject.w_PriPagMa 
              this.w_IMPRID = IIF(CashEffettivo.Ordina="A" And this.oParentObject.w_Perimp<>0,Cp_Round((CashEffettivo.Impdar+CashEffettivo.Impave)*(100-this.oParentObject.w_Perimp)/100,this.w_Dectot),Nvl(Impdar,0)-Nvl(Impave,0))
              this.w_PERRID = this.oParentObject.w_Perimp
            else
              this.w_PERRID = 0
              this.w_IMPRID = Nvl(Impdar,0)-Nvl(Impave,0)
            endif
            if CashEffettivo.Ordina="B" And Not Empty(this.oParentObject.w_Smosca)
              this.Pag9()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Insert into TMPFL1
            i_nConn=i_TableProp[this.TMPFL1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPFL1_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPFL1_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CODBAN"+",IMPDAR"+",IMPAVE"+",ORDINA"+",DATVAL"+",CAOAPE"+",DATAPE"+",ORDINE"+",TIPOCASHFLOW"+",PTCODVAL"+",PTNUMPAR"+",PTTIPCON"+",PTCODCON"+",PTMODPAG"+",ANDESCRI"+",MPTIPPAG"+",MVFLVEAC"+",MVCLADOC"+",PERCEN"+",TOTRATE"+",MVDATDOC"+",MVALFDOC"+",MVNUMDOC"+",MVTIPDOC"+",MVSERIAL"+",ANCATCOM"+",IMPRID"+",PERRID"+",FLAGSMOB"+",DATASMOB"+",IMPSMOB"+",PTSERIAL"+",PTROWORD"+",CPROWNUM"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(CashEffettivo.Codban),'TMPFL1','CODBAN');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Impdar),'TMPFL1','IMPDAR');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Impave),'TMPFL1','IMPAVE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_Ordina),'TMPFL1','ORDINA');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Datval),'TMPFL1','DATVAL');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Caoape),'TMPFL1','CAOAPE');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Datape),'TMPFL1','DATAPE');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Ordine),'TMPFL1','ORDINE');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Tipocashflow),'TMPFL1','TIPOCASHFLOW');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Ptcodval),'TMPFL1','PTCODVAL');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Ptnumpar),'TMPFL1','PTNUMPAR');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Pttipcon),'TMPFL1','PTTIPCON');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Ptcodcon),'TMPFL1','PTCODCON');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Ptmodpag),'TMPFL1','PTMODPAG');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Andescri),'TMPFL1','ANDESCRI');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Mptippag),'TMPFL1','MPTIPPAG');
              +","+cp_NullLink(cp_ToStrODBC(Space(1)),'TMPFL1','MVFLVEAC');
              +","+cp_NullLink(cp_ToStrODBC(Space(2)),'TMPFL1','MVCLADOC');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Impdar*0),'TMPFL1','PERCEN');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Impdar*0),'TMPFL1','TOTRATE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_Datavuota),'TMPFL1','MVDATDOC');
              +","+cp_NullLink(cp_ToStrODBC(Space(10)),'TMPFL1','MVALFDOC');
              +","+cp_NullLink(cp_ToStrODBC(0),'TMPFL1','MVNUMDOC');
              +","+cp_NullLink(cp_ToStrODBC(Space(5)),'TMPFL1','MVTIPDOC');
              +","+cp_NullLink(cp_ToStrODBC(Space(10)),'TMPFL1','MVSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(CashEffettivo.Ancatcom),'TMPFL1','ANCATCOM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_Imprid),'TMPFL1','IMPRID');
              +","+cp_NullLink(cp_ToStrODBC(this.w_Perrid),'TMPFL1','PERRID');
              +","+cp_NullLink(cp_ToStrODBC(this.w_Flagsmob),'TMPFL1','FLAGSMOB');
              +","+cp_NullLink(cp_ToStrODBC(this.w_Datasmob),'TMPFL1','DATASMOB');
              +","+cp_NullLink(cp_ToStrODBC(this.w_Impsmob),'TMPFL1','IMPSMOB');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'TMPFL1','PTSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'TMPFL1','PTROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'TMPFL1','CPROWNUM');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CODBAN',CashEffettivo.Codban,'IMPDAR',CashEffettivo.Impdar,'IMPAVE',CashEffettivo.Impave,'ORDINA',this.w_Ordina,'DATVAL',CashEffettivo.Datval,'CAOAPE',CashEffettivo.Caoape,'DATAPE',CashEffettivo.Datape,'ORDINE',CashEffettivo.Ordine,'TIPOCASHFLOW',CashEffettivo.Tipocashflow,'PTCODVAL',CashEffettivo.Ptcodval,'PTNUMPAR',CashEffettivo.Ptnumpar,'PTTIPCON',CashEffettivo.Pttipcon)
              insert into (i_cTable) (CODBAN,IMPDAR,IMPAVE,ORDINA,DATVAL,CAOAPE,DATAPE,ORDINE,TIPOCASHFLOW,PTCODVAL,PTNUMPAR,PTTIPCON,PTCODCON,PTMODPAG,ANDESCRI,MPTIPPAG,MVFLVEAC,MVCLADOC,PERCEN,TOTRATE,MVDATDOC,MVALFDOC,MVNUMDOC,MVTIPDOC,MVSERIAL,ANCATCOM,IMPRID,PERRID,FLAGSMOB,DATASMOB,IMPSMOB,PTSERIAL,PTROWORD,CPROWNUM &i_ccchkf. );
                 values (;
                   CashEffettivo.Codban;
                   ,CashEffettivo.Impdar;
                   ,CashEffettivo.Impave;
                   ,this.w_Ordina;
                   ,CashEffettivo.Datval;
                   ,CashEffettivo.Caoape;
                   ,CashEffettivo.Datape;
                   ,CashEffettivo.Ordine;
                   ,CashEffettivo.Tipocashflow;
                   ,CashEffettivo.Ptcodval;
                   ,CashEffettivo.Ptnumpar;
                   ,CashEffettivo.Pttipcon;
                   ,CashEffettivo.Ptcodcon;
                   ,CashEffettivo.Ptmodpag;
                   ,CashEffettivo.Andescri;
                   ,CashEffettivo.Mptippag;
                   ,Space(1);
                   ,Space(2);
                   ,CashEffettivo.Impdar*0;
                   ,CashEffettivo.Impdar*0;
                   ,this.w_Datavuota;
                   ,Space(10);
                   ,0;
                   ,Space(5);
                   ,Space(10);
                   ,CashEffettivo.Ancatcom;
                   ,this.w_Imprid;
                   ,this.w_Perrid;
                   ,this.w_Flagsmob;
                   ,this.w_Datasmob;
                   ,this.w_Impsmob;
                   ,this.w_PTSERIAL;
                   ,this.w_PTROWORD;
                   ,this.w_ROWNUM;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            Endscan
          endif
          this.oParentObject.w_DATINI = this.w_DATAINISCA
        endif
        if this.oParentObject.w_SALIVAC="S" OR this.oParentObject.w_SALIVAT="S"
          this.Pag13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_TipoVisua="C"
          * --- Elaboro il cash flow per singolo conto banca
          SELECT ( this.w_ZOOMCONTI )
          GO TOP
          * --- Scorro il cursore per cercare i conti marcati
          SCAN FOR XCHK=1
          this.w_NUMCOR = NVL(BACODBAN, SPACE(15))
          this.w_NCONTI = this.w_NCONTI+1
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ENDSCAN
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          SELECT TipoPag
          Go Top
          Scan
          this.w_MPTIPPAG = NVL(MPTIPPAG, SPACE(2))
          this.w_NCONTI = this.w_NCONTI+1
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          Endscan
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.w_TIPOPAGAMENTO = "TOTALI"
          this.w_MPTIPPAG = "TO"
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pTipo="Drop"
        * --- Drop temporary table TMPFL1
        i_nIdx=cp_GetTableDefIdx('TMPFL1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPFL1')
        endif
        this.oParentObject.w_DETTPAGAMENTI = "N"
      case this.pTipo="ZoomDett"
        this.w_MPTIPPAG = IIF(Alltrim(This.oParentObject.w_TipoPag)="TOTALI","TO",Alltrim(This.oParentObject.w_TipoPag))
        this.w_GIORNI = IIF(this.oParentObject.w_TIPPER="G",1,IIF(this.oParentObject.w_TIPPER="S",7,IIF(this.oParentObject.w_TIPPER="Q",15,30)))
        this.w_PERIODI = This.oParentObject.w_Periodi_Elaborati
        this.w_ZOOMCB = This.oParentObject.w_Zoomdetcb
        this.CFMSK = THIS.OPARENTOBJECT
        this.w_INIZIO_PER = this.oParentObject.w_DATINI - 1
        if this.w_GIORNI<>1
          do case
            case this.w_GIORNI=7 OR this.w_GIORNI=15
              this.w_INIZIO_PER = this.oParentObject.w_DATINI - DOW(this.oParentObject.w_DATINI,2) + 1
            case this.w_GIORNI=30
              this.w_INIZIO_PER = DATE(YEAR(this.oParentObject.w_DATINI),MONTH(this.oParentObject.w_DATINI),1)
          endcase
        endif
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipo="StrutturaZoom"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipo="RateDoc"
        * --- Calcolo storico cash flow per Atteso
        this.oParentObject.w_FLSEVA = iif(EMPTY(NVL(this.oParentObject.w_FLSEVA," "))," ",this.oParentObject.w_FLSEVA)
        this.oParentObject.w_AZFLSEVA = iif(EMPTY(NVL(this.oParentObject.w_AZFLSEVA," "))," ",this.oParentObject.w_AZFLSEVA)
        if this.oParentObject.w_FLSEVA<>this.oParentObject.w_AZFLSEVA
          if not ah_YesNo("Attenzione, il cambiamento del controllo evasione comporta un ricalcolo di tutto lo storico.%0Si vuole procedere?")
            i_retcode = 'stop'
            return
          endif
          this.oParentObject.w_INIZIO = i_inidat
          this.oParentObject.w_FINE = i_findat
          * --- Write into AZIENDA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AZFLSEVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FLSEVA),'AZIENDA','AZFLSEVA');
                +i_ccchkf ;
            +" where ";
                +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   )
          else
            update (i_cTable) set;
                AZFLSEVA = this.oParentObject.w_FLSEVA;
                &i_ccchkf. ;
             where;
                AZCODAZI = i_CODAZI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Try
        local bErr_043B4A40
        bErr_043B4A40=bTrsErr
        this.Try_043B4A40()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_043B4A40
        * --- End
        ah_errormsg("Elaborazione terminata")
      case this.pTipo="Disabilita"
        if (Empty(this.oParentObject.w_TIPCER) And Empty(this.oParentObject.w_TIPEFF) And Empty(this.oParentObject.w_TIPATT) And Empty(this.oParentObject.w_SALIVAC) And Empty(this.oParentObject.w_SALIVAT) ) Or (this.oParentObject.w_TIPOVISUA="P" And Empty(this.oParentObject.w_PAGRD) And Empty(this.oParentObject.w_PAGBO) And Empty(this.oParentObject.w_PAGRB) And Empty(this.oParentObject.w_PAGRI) And Empty(this.oParentObject.w_PAGCA) And Empty(this.oParentObject.w_PAGMA)) Or Not this.oParentObject.w_ABILITAZOOM
          this.oParentObject.oPgFrm.Pages(3).enabled=.f.
        else
          this.oParentObject.oPgFrm.Pages(3).enabled=.t.
        endif
      case this.pTipo="ZoomDisable"
        this.w_ZOOMCONTI = this.oParentObject.w_zoomcf.cCursor
        SELECT ( this.w_ZOOMCONTI )
        if USED(this.w_ZoomConti) And Reccount(this.w_ZoomConti)>0
          this.w_NUMREC = RECNO()
          COUNT FOR XCHK=1 TO w_COUNT
          if w_COUNT>0
            this.oParentObject.oPgFrm.Pages(3).enabled=.t.
            this.oParentObject.w_ABILITAZOOM = .T.
          else
            this.oParentObject.oPgFrm.Pages(3).enabled=.f.
            this.oParentObject.w_ABILITAZOOM = .F.
          endif
          GOTO this.w_NUMREC
        endif
      case this.pTipo="Atteso"
        if this.oParentObject.w_TIPATT="S"
          if (EMPTY(this.oParentObject.w_CFDATCRE) or this.oParentObject.w_CFDATCRE<i_DATSYS-7)
            if EMPTY(this.oParentObject.w_CFDATCRE)
              ah_ErrorMsg("Non � stata lanciata la funzione di calcolo rate documenti")
              this.oParentObject.w_TIPATT = " "
            else
              ah_ErrorMsg("La funzione di calcolo rate documenti non � aggiornata da %1 giorni.",, "" ,ALLTRIM(STR(cp_todate(i_DATSYS)-cp_todate(this.oParentObject.w_CFDATCRE))))
            endif
          endif
        endif
    endcase
  endproc
  proc Try_043B4A40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from CASHFLOW
    i_nConn=i_TableProp[this.CASHFLOW_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CASHFLOW_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CFDATRAT >= "+cp_ToStrODBC(this.oParentObject.w_INIZIO);
            +" and CFDATRAT <= "+cp_ToStrODBC(this.oParentObject.w_FINE);
             )
    else
      delete from (i_cTable) where;
            CFDATRAT >= this.oParentObject.w_INIZIO;
            and CFDATRAT <= this.oParentObject.w_FINE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Prendo gli importi dalle rate associate ai documenti
    ah_msg("Elabora cash flow atteso",.t.,.t.,)
    * --- La query GSTE1BVF individua i seriali dei documenti che contengono almeno una riga evasa, anche solo parzialmente.
    *     La query GSTE_BVF, utilizzando GSTE1BVF nella clausola NOT in, elabora i soli documenti totalmente inevasi.
    * --- Insert into CASHFLOW
    i_nConn=i_TableProp[this.CASHFLOW_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CASHFLOW_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\gste_bvf",this.CASHFLOW_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Calcolo il saldo iniziale 
    *     Ora x i documenti evasi
    * --- La query GSTE2BVF deve elaborare i documenti parzialmente evasi,
    *     La query GSTE5BVF seleziona i seriali dei documenti che hanno delle righe da evadere, 
    *     e viene utilizzata dalla query GSTE2BFV nella clausola IN per non elaborare documenti chiusi (completamente evasi).
    *     Affinch� non siano nuovamante presi i documenti gi� elaborati da GSTE_BVF ed inseriti nella tabella CASHFLOW, ovvero i documenti completamente da evadere,
    *     la query GSTE2BVF utilizza un ulteriore filtro in modo da escludere tali documenti (fa la left JOIN tra DOC_MAST e CASHFLOW (MVSERIAL=CFSERIAL) 
    *     ed applica il filtro CASHFLOW.CFSERIAL is null)
    * --- Create temporary table TMPCASHATT
    i_nIdx=cp_AddTableDef('TMPCASHATT') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_VJXZEZMTRH[1]
    indexes_VJXZEZMTRH[1]='CFSERIAL,CFNUMRAT,CFDATRAT'
    vq_exec('query\gste2bvf',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_VJXZEZMTRH,.f.)
    this.TMPCASHATT_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CASHFLOW
    i_nConn=i_TableProp[this.CASHFLOW_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CASHFLOW_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPCASHATT_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where CFIMPRAT<>0",this.CASHFLOW_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPCASHATT
    i_nIdx=cp_GetTableDefIdx('TMPCASHATT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPCASHATT')
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nel caso di visualizzazione per tipo pagamento non leggo i dati dall'archivio
    *     dei saldi
    if this.oParentObject.w_TipoVisua="C"
      * --- La procedura verifica se esiste un movimento di apertura per l'esercizio
      *     corrente
      this.w_FLAGIO = "A"
      this.w_CODESE = g_CODESE
      this.w_COUNTER = 0
      * --- Select from Gste10bfl
      do vq_exec with 'Gste10bfl',this,'_Curs_Gste10bfl','',.f.,.t.
      if used('_Curs_Gste10bfl')
        select _Curs_Gste10bfl
        locate for 1=1
        do while not(eof())
        this.w_COUNTER = Nvl ( _Curs_Gste10bfl.Conta,0)
          select _Curs_Gste10bfl
          continue
        enddo
        use
      endif
      * --- Se non esistono aperture nell'esercizio corrente controllo 
      *     se esistono chiusure relative all'esercizio precedente
      if this.w_COUNTER=0
        this.w_ESEPRE = ""
        this.w_FINPRE = g_INIESE-1
        this.w_CODAZI = i_CODAZI
        * --- Select from ESERCIZI
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ESCODESE,ESINIESE,ESFINESE,ESVALNAZ  from "+i_cTable+" ESERCIZI ";
              +" where ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+" AND ESFINESE="+cp_ToStrODBC(this.w_FINPRE)+"";
               ,"_Curs_ESERCIZI")
        else
          select ESCODESE,ESINIESE,ESFINESE,ESVALNAZ from (i_cTable);
           where ESCODAZI=this.w_CODAZI AND ESFINESE=this.w_FINPRE;
            into cursor _Curs_ESERCIZI
        endif
        if used('_Curs_ESERCIZI')
          select _Curs_ESERCIZI
          locate for 1=1
          do while not(eof())
          this.w_ESEPRE = NVL(_Curs_ESERCIZI.ESCODESE, SPACE(4))
          this.w_VALPRE = NVL(_Curs_ESERCIZI.ESVALNAZ, SPACE(3))
            select _Curs_ESERCIZI
            continue
          enddo
          use
        endif
        this.w_FLAGIO = "C"
        * --- La procedura verifica se sono presenti chiusure nell'esercizio precedente
        * --- Select from Gste10bfl
        do vq_exec with 'Gste10bfl',this,'_Curs_Gste10bfl','',.f.,.t.
        if used('_Curs_Gste10bfl')
          select _Curs_Gste10bfl
          locate for 1=1
          do while not(eof())
          this.w_COUNTER = Nvl ( _Curs_Gste10bfl.Conta,0)
            select _Curs_Gste10bfl
            continue
          enddo
          use
        endif
        this.w_CODESE = g_CODESE
        this.w_CODES1 = this.w_ESEPRE
        if this.w_COUNTER=0
          * --- Non � presente un movimento di apertura per l'esercizio corrente e nemmeno
          *     un movimento di chiusura per l'esercizio precedente
          * --- Create temporary table TMPFL3
          i_nIdx=cp_AddTableDef('TMPFL3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSTEVBFL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPFL3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Non � presente un movimento di apertura per l'esercizio corrente ma
          *     un movimento di chiusura per l'esercizio precedente
          * --- Create temporary table TMPFL3
          i_nIdx=cp_AddTableDef('TMPFL3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSTEZBFL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPFL3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
      else
        * --- Esiste un movimento di apertura nell'esercizio corrente
        this.w_CODESE = g_CODESE
        this.w_CODES1 = SPACE(4)
        * --- Create temporary table TMPFL3
        i_nIdx=cp_AddTableDef('TMPFL3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSTEWBFL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPFL3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    else
      * --- Create temporary table TMPFL3
      i_nIdx=cp_AddTableDef('TMPFL3') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTE7BFLV',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPFL3_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Insert into TMPFL1
    i_nConn=i_TableProp[this.TMPFL1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPFL1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSTE_BFLV",this.TMPFL1_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if not empty(this.w_FINE_CALC)
      if this.w_GIORNI<>1
        do case
          case this.w_GIORNI=7 OR this.w_GIORNI=15
            this.w_DATE_INIZIO = iif(this.w_INIZIO_PER=DATE(YEAR(this.w_INIZIO_PER),1,1), DATE(YEAR(this.w_INIZIO_PER)-1,12,31), this.w_INIZIO_PER)
            do while YEAR(this.w_DATE_INIZIO)<>YEAR(this.w_FINE_CALC)
              if this.w_DATE_INIZIO<DATE(YEAR(this.w_DATE_INIZIO),12,31-6)
                this.w_PERIODI_CALC = this.w_PERIODI_CALC + WEEK(DATE(YEAR(this.w_DATE_INIZIO),12,31-6),0,2) - WEEK(this.w_DATE_INIZIO,0,2) + 1
              else
                if WEEK(this.w_DATE_INIZIO,0,2)<>1
                  this.w_PERIODI_CALC = this.w_PERIODI_CALC + WEEK(DATE(YEAR(this.w_DATE_INIZIO),12,31),0,2) - WEEK(this.w_DATE_INIZIO,0,2) + 1
                endif
              endif
              this.w_DATE_INIZIO = DATE(YEAR(this.w_DATE_INIZIO)+1,1,1)
            enddo
            this.w_DATE_INIZIO = MIN(this.w_FINE_CALC,this.w_DATE_INIZIO)
            if MONTH(this.w_FINE_CALC)=12 AND WEEK(this.w_FINE_CALC,0,2)=1
              this.w_PERIODI_CALC = this.w_PERIODI_CALC + 1
              this.w_FINE_CALC = this.w_FINE_CALC - 6
              this.w_FINE_CALC = MAX(this.w_FINE_CALC,this.w_DATE_INIZIO)
            endif
            this.w_PERIODI_CALC = this.w_PERIODI_CALC + WEEK(this.w_FINE_CALC,0,2) - WEEK(this.w_DATE_INIZIO,0,2)
            if this.w_GIORNI=15
              this.w_PERIODI_CALC = INT(this.w_PERIODI_CALC / 2)
            endif
          case this.w_GIORNI=30
            this.w_PERIODI_CALC = (YEAR(this.w_FINE_CALC)-YEAR(this.w_INIZIO_PER))*12+ MONTH(this.w_FINE_CALC) - MONTH(this.w_INIZIO_PER)
        endcase
      else
        this.w_PERIODI_CALC = ( this.w_FINE_CALC - this.w_INIZIO_PER )
      endif
      * --- Se il numero di giorni nell'intervallo diviso la periodicit� non � intero
      *     Aggiungo 1 al numero di periodi
      *     NOTA: agiungo sempre uno anche per i saldi iniziali
      if this.w_GIORNI=1
        this.w_PERIODI_CALC = this.w_PERIODI_CALC+1
      else
        this.w_PERIODI_CALC = this.w_PERIODI_CALC+2
      endif
    else
      this.w_PERIODI_CALC = 2
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizzazione
    CREATE CURSOR GRAFICODETT1 ( NUMCOR C(15), BACODVAL C(3), TIPODATO C(15),TIPOCONTI C(30), TOT_000 N(18,4),BACONCOL C(15),ORDINA N(1))
    FOR J=2 TO this.w_PERIODI
    PERIODO = ALLTRIM("TOT_"+RIGHT("000"+ALLTRIM(STR(J-1)),3))
    ALTER TABLE GRAFICODETT1 ADD COLUMN &PERIODO N(18,4) NULL
    ENDFOR
    * --- Metto i vettori nel cursore
    for K = 1 to this.w_NUMCONTI
    CONTO1=""
    CONCOL1= ""
    TIPOCONTI1=""
    ORDINAZ=0
    CERT1=0
    ATTES1=0
    EFFETTIV1=0
    SALIVAC1=0
    SALIVAT1=0
    CONTO1= ALLTRIM(this.CFMSK.CODCONTI (K,1))
    CONCOL1= IIF(this.oParentObject.w_TIPOVISUA="C",ALLTRIM(this.CFMSK.CODCONTI (K,2)),SPACE(15))
    TIPOCONTI1= ALLTRIM(this.CFMSK.TIPOCONTI (K))
    ORDINAZ=IIF(this.oParentObject.w_TIPOVISUA="P",1,IIF(ALLTRIM(this.CFMSK.TIPOCONTI (K))="N",0,IIF(ALLTRIM(this.CFMSK.TIPOCONTI (K))="C",1,2)))
    INSERT INTO GRAFICODETT1 (NUMCOR, BACODVAL, TIPODATO,TIPOCONTI,BACONCOL,ORDINA) VALUES (CONTO1, g_PERVAL, Space(15),TIPOCONTI1,Space(15),ORDINAZ)
    if this.oParentObject.w_TIPCER="S" AND CONTO1 <> "Non Disponibile"
      INSERT INTO GRAFICODETT1 (NUMCOR, BACODVAL, TIPODATO,TIPOCONTI,BACONCOL,ORDINA) VALUES (CONTO1, g_PERVAL, "CERTO",TIPOCONTI1,CONCOL1,ORDINAZ)
    endif
    if this.oParentObject.w_TIPEFF = "S"
      INSERT INTO GRAFICODETT1 (NUMCOR, BACODVAL, TIPODATO,TIPOCONTI,BACONCOL,ORDINA) VALUES (CONTO1, g_PERVAL, "EFFETTIVO",TIPOCONTI1,SPACE(15),ORDINAZ)
    endif
    if this.oParentObject.w_TIPATT = "S"
      INSERT INTO GRAFICODETT1 (NUMCOR, BACODVAL, TIPODATO,TIPOCONTI,BACONCOL,ORDINA) VALUES (CONTO1, g_PERVAL, "ATTESO",TIPOCONTI1,SPACE(15),ORDINAZ)
    endif
    if this.oParentObject.w_SALIVAC = "S"
      INSERT INTO GRAFICODETT1 (NUMCOR, BACODVAL, TIPODATO) VALUES (CONTO1, VALUTA1, "SAL.IVA CERTO")
    endif
    if this.oParentObject.w_SALIVAT = "S" 
      INSERT INTO GRAFICODETT1 (NUMCOR, BACODVAL, TIPODATO) VALUES (CONTO1, VALUTA1, "SAL.IVA TEORICO")
    endif
    for i=1 to this.w_PERIODI
    PERIODO = ALLTRIM("TOT_"+RIGHT("000"+ALLTRIM(STR(I-1)),3))
    * --- Costruisco i dati da stampare / visualizzare
    CERT1=CERT1 + this.CFMSK.CERTI(K,I)
    ATTES1=ATTES1+this.CFMSK.ATTESI(K,I)
    EFFETTIV1=EFFETTIV1+this.CFMSK.EFFETTIVI(K,I)
    if this.oParentObject.w_TIPCER="S"
      UPDATE GRAFICODETT1 SET &PERIODO=cp_ROUND(CERT1,g_PERPVL) WHERE ALLTRIM(NUMCOR)==CONTO1 AND TIPODATO="CERTO"
    endif
    if this.oParentObject.w_TIPEFF = "S"
      UPDATE GRAFICODETT1 SET &PERIODO=cp_ROUND(EFFETTIV1,g_PERPVL) WHERE ALLTRIM(NUMCOR)==CONTO1 AND TIPODATO="EFFETTIVO"
    endif
    if this.oParentObject.w_TIPATT = "S"
      UPDATE GRAFICODETT1 SET &PERIODO=cp_ROUND(ATTES1,g_PERPVL) WHERE ALLTRIM(NUMCOR)==CONTO1 AND TIPODATO="ATTESO"
    endif
    UPDATE GRAFICODETT1 SET &PERIODO=cp_ROUND(CERT1+ATTES1++EFFETTIV1,g_PERPVL) WHERE ALLTRIM(NUMCOR)==CONTO1 AND TIPODATO=space(15)
    ENDFOR
    ENDFOR
    if this.oParentObject.w_TIPOVISUA $ "CP"
      ORDINAZ=IIF(this.oParentObject.w_TIPOVISUA="P",0,3)
      INSERT INTO GRAFICODETT1 (NUMCOR, BACODVAL, TIPODATO,TIPOCONTI,BACONCOL,ORDINA) VALUES (this.CFMSK.CODCONTI(this.w_NUMCONTI + 1,1),g_PERVAL, space(15),this.CFMSK.CODCONTI(this.w_NUMCONTI + 1,1),SPACE(15),ORDINAZ)
      if this.oParentObject.w_TIPCER = "S"
        INSERT INTO GRAFICODETT1 (NUMCOR, BACODVAL, TIPODATO,TIPOCONTI,BACONCOL,ORDINA) VALUES (this.CFMSK.CODCONTI(this.w_NUMCONTI + 1,1), g_PERVAL, "CERTO",this.CFMSK.CODCONTI(this.w_NUMCONTI + 1,1),SPACE(15),ORDINAZ)
      endif
      if this.oParentObject.w_TIPEFF = "S"
        INSERT INTO GRAFICODETT1 (NUMCOR, BACODVAL, TIPODATO,TIPOCONTI,BACONCOL,ORDINA) VALUES (this.CFMSK.CODCONTI(this.w_NUMCONTI + 1,1), g_PERVAL, "EFFETTIVO",this.CFMSK.CODCONTI(this.w_NUMCONTI + 1,1),SPACE(15),ORDINAZ)
      endif
      if this.oParentObject.w_TIPATT = "S"
        INSERT INTO GRAFICODETT1 (NUMCOR, BACODVAL, TIPODATO,TIPOCONTI,BACONCOL,ORDINA) VALUES (this.CFMSK.CODCONTI(this.w_NUMCONTI + 1,1),g_PERVAL, "ATTESO",this.CFMSK.CODCONTI(this.w_NUMCONTI + 1,1),SPACE(15),ORDINAZ)
      endif
      this.w_TOTALEAPP = 0
      CERT1=0
      ATTES1=0
      EFFETTIV1=0
      for i = 1 to this.w_PERIODI
      for k = 1 to this.w_NUMCONTI
      CERT1=CERT1 + this.CFMSK.CERTI(K,I)
      ATTES1=ATTES1+this.CFMSK.ATTESI(K,I)
      EFFETTIV1=EFFETTIV1+this.CFMSK.EFFETTIVI(K,I)
      this.w_TOTALEAPP = this.w_TOTALEAPP + this.CFMSK.CERTI(K,I) + this.CFMSK.ATTESI(K,I)+this.CFMSK.EFFETTIVI(K,I)
      ENDFOR
      PERIODO = ALLTRIM("TOT_"+RIGHT("000"+ALLTRIM(STR(I-1)),3))
      if this.oParentObject.w_TIPCER= "S"
        UPDATE GRAFICODETT1 SET &PERIODO=cp_ROUND(CERT1,g_PERPVL) WHERE ALLTRIM(NUMCOR) = "TOTALI" AND TIPODATO="CERTO"
      endif
      if this.oParentObject.w_TIPEFF = "S"
        UPDATE GRAFICODETT1 SET &PERIODO=cp_ROUND(EFFETTIV1,g_PERPVL) WHERE ALLTRIM(NUMCOR) = "TOTALI" AND TIPODATO="EFFETTIVO"
      endif
      if this.oParentObject.w_TIPATT = "S"
        UPDATE GRAFICODETT1 SET &PERIODO=cp_ROUND(ATTES1,g_PERPVL) WHERE ALLTRIM(NUMCOR) = "TOTALI" AND TIPODATO="ATTESO"
      endif
      UPDATE GRAFICODETT1 SET &PERIODO=cp_round(this.w_TOTALEAPP,g_PERPVL) WHERE ALLTRIM(NUMCOR) = "TOTALI" AND TIPODATO=space(15)
      ENDFOR
    endif
    SELECT * FROM GRAFICODETT1 INTO CURSOR ZOOMCURSOR ORDER BY ORDINA,NUMCOR 
    WrCursor("ZoomCursor")
    Select(this.w_ZOOMFLOW)
    Zap
    Select ZOOMCURSOR
    Scan
    if UPPER(ALLTRIM(ZOOMCURSOR.TIPODATO))="CERTO" AND NOT EMPTY(ZOOMCURSOR.BACONCOL) AND this.oParentObject.w_TIPOVISUA="C" AND 1=0
      this.w_BACONCOL = ALLTRIM(ZOOMCURSOR.BACONCOL)
      Replace Numcor With this.w_BACONCOL
    endif
    Scatter Memvar
    Select(this.w_ZOOMFLOW)
    Append blank
    Gather Memvar
    Select ZOOMCURSOR
    Endscan
    this.Pag12()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select (this.w_ZOOMFLOW)
    Go Top
    do case
      case this.oParentObject.w_TipoVisua="C"
        this.oParentObject.w_zoomdetcf.Refresh()
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TipoVisua="P"
        this.oParentObject.w_zoomdetpa.Refresh()
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TipoVisua="T"
        this.oParentObject.w_zoomdetcb.Refresh()
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Leggo i dati dal cursore temporaneo TMPFL1 che contiene il cash flow elaborato
    *     per ogni conto
    vq_exec("query\GSTE1BFLV.VQR",this,"CashFlow")
    Select CashFlow 
 Go Top 
 Scan
    this.w_FLAGSMOB = Nvl(CashFlow.FlagSmob," ")
    this.w_TEMP = iif(CashFlow.Tipocashflow="S",Nvl(Totrate,0),Nvl(Impdar,0)-Nvl(Impave,0))
    this.w_TEMP_RID = iif(this.w_FlagSmob="S",Nvl(CashFlow.ImpSmob,0),Nvl(Imprid,0))
    this.w_CAOAPE = NVL(CAOAPE,0)
    this.w_DATAPE = NVL(DATAPE,cp_CharToDate("  -  -  "))
    if CashFlow.Ptcodval<>g_Perval
      this.w_DIVIDEEFF = GETCAM(CashFlow.Ptcodval,this.oParentObject.w_Datcam,0)
      if this.w_DIVIDEEFF=0
        ah_errormsg("Impossibile determinare il cambio della valuta: %1",48,, CashFlow.Ptcodval)
        this.Pag12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      else
        this.w_TEMP = VAL2MON(this.w_TEMP,this.w_DIVIDEEFF,1,this.oParentObject.w_DATCAM,g_PERVAL,g_PERPVL)
        this.w_TEMP_RID = VAL2MON(this.w_TEMP_RID,this.w_DIVIDEEFF,1,this.oParentObject.w_DATCAM,g_PERVAL,g_PERPVL)
      endif
    endif
    if CashFlow.Ordina="A"
      this.w_FINE_CALC = iif(this.w_Flagsmob="S",Cp_Todate(CashFlow.Datasmob),Cp_Todate(CashFlow.Datval))
      if this.w_Fine_Calc<=this.oParentObject.w_Datfin
        this.w_PERIODI_CALC = 0
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_POSI = this.w_PERIODI_CALC
        * --- Sommo l'importo al periodo
        do case
          case CashFlow.Tipocashflow="C"
            this.CFMSK.CERTI(THIS.w_NCONTI,THIS.w_POSI) = this.CFMSK.CERTI(this.w_NCONTI,this.w_POSI) + this.w_TEMP
          case CashFlow.Tipocashflow="T"
            this.CFMSK.ATTESI(THIS.w_NCONTI,THIS.w_POSI) = this.CFMSK.ATTESI(this.w_NCONTI,this.w_POSI) + this.w_TEMP
          case CashFlow.Tipocashflow="E"
            this.CFMSK.EFFETTIVI(THIS.w_NCONTI,THIS.w_POSI) = this.CFMSK.EFFETTIVI(THIS.w_NCONTI,this.w_POSI)+this.w_TEMP_RID
          case CashFlow.Tipocashflow="S"
        endcase
      endif
    else
      do case
        case CashFlow.Tipocashflow="C"
          this.CFMSK.CERTI(THIS.w_NCONTI,1) = this.CFMSK.CERTI(this.w_NCONTI,1) + this.w_TEMP
        case CashFlow.Tipocashflow="E"
          this.CFMSK.EFFETTIVI(THIS.w_NCONTI,1) = this.CFMSK.EFFETTIVI(THIS.w_NCONTI,1)+this.w_TEMP_RID
        case CashFlow.Tipocashflow="S"
          if CashFlow.Mvcladoc="DT"
            this.CFMSK.SALIVAT(THIS.w_NCONTI,1) = this.CFMSK.SALIVAT(this.w_NCONTI,1) + this.w_TEMP
          else
            this.CFMSK.SALIVAC(THIS.w_NCONTI,1) = this.CFMSK.SALIVAC(this.w_NCONTI,1) + this.w_TEMP
          endif
      endcase
    endif
    Endscan
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaboro lo zoom del dettaglio conti di tesoreria per tipologia pagamento
    this.w_ZOOMFLOW = this.oParentObject.w_zoomdetcb.cCursor
    vq_exec("query\GSTE5BFLV.VQR",this,"ContiBanca")
    if Reccount("ContiBanca")>0
      this.w_NUMCONTI = 1
      * --- Scorro il cursore per cercare i conti marcati
      SELECT ContiBanca 
      GO TOP
      SCAN
      * --- Contiene il nome dei conti utilizzati
      DIMENSION this.CFMSK.CODCONTI(this.w_NUMCONTI,2)
      DIMENSION this.CFMSK.TIPOCONTI(this.w_NUMCONTI)
      this.w_NUMCOR = NVL(CODBAN, SPACE(15))
      this.CFMSK.CODCONTI(this.w_NUMCONTI,1) = this.w_NUMCOR
      this.CFMSK.CODCONTI(this.w_NUMCONTI,2) = SPACE(15)
      this.CFMSK.TIPOCONTI(this.w_NUMCONTI) = iif(NVL(ORDINE,1)=0,"N",iif(NVL(ORDINE,1)=1, "C","G"))
      this.w_NUMCONTI = this.w_NUMCONTI + 1
      ENDSCAN
      * --- Assegno la variabile caller CONTINUM
      DIMENSION this.CFMSK.CODCONTI(this.w_NUMCONTI,2)
      DIMENSION this.CFMSK.TIPOCONTI(this.w_NUMCONTI)
      this.w_NUMCONTI = this.w_NUMCONTI -1
      this.oParentObject.w_CONTINUM = this.w_NUMCONTI
      this.CFMSK.CODCONTI(this.w_NUMCONTI+1,1) = "TOTALI"
      this.CFMSK.CODCONTI(this.w_NUMCONTI+1,2) = " "
      this.CFMSK.TIPOCONTI(this.w_NUMCONTI+1) = " "
      * --- Contiene i periodi da elaborare
      DIMENSION this.CFMSK.PERIODO ( this.w_PERIODI )
      * --- Contiene il valore del cash flow certo, diviso per periodo
      DIMENSION this.CFMSK.CERTI ( this.w_NUMCONTI,this.w_PERIODI )
      * --- Contiene il valore del cash flow effettivo, diviso per periodo
      DIMENSION this.CFMSK.EFFETTIVI (this.w_NUMCONTI, this.w_PERIODI )
      * --- Contiene il valore del cash flow atteso, diviso per periodo
      DIMENSION this.CFMSK.ATTESI ( this.w_NUMCONTI, this.w_PERIODI )
      * --- Mi dice se nel periodo ho almeno un movimento
      for i=1 to this.w_PERIODI
      for k=1 to this.w_NUMCONTI
      this.CFMSK.CERTI(K,I) = 0
      this.CFMSK.EFFETTIVI(K,I) = 0
      this.CFMSK.ATTESI(K,I) = 0
      ENDFOR
      this.CFMSK.PERIODO(I) = 0
      ENDFOR
      * --- Elaboro il cash flow per singolo conto banca
      this.w_NCONTI = 0
      this.oParentObject.w_TIPOVISUA = "T"
      SELECT Contibanca
      Go Top
      * --- Scorro il cursore per cercare i conti marcati
      SCAN
      this.w_NUMCOR = NVL(CODBAN, SPACE(15))
      this.w_NCONTI = this.w_NCONTI+1
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ENDSCAN
      this.w_ZOOMFLOW = this.oParentObject.w_zoomdetcb.cCursor
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Se il parametro che lancia la routine vale 'Elabora', devo eseguire l'intestazione
      *     dello zoom
      if this.pTipo="Elabora"
        this.oParentObject.w_TIPOVISUA = "T"
        this.GRID = this.w_ZoomCB.GRD
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.GRID.Refresh()     
      else
        Select(this.w_ZOOMFLOW)
        Zap
      endif
    endif
    this.oParentObject.w_DETTPAGAMENTI = "S"
    this.oParentObject.w_TIPOVISUA = "P"
    this.oParentObject.w_PERIODI_ELABORATI = this.w_PERIODI
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_Tipovisua="T"
      this.GRID = this.w_ZoomCB.GRD
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.GRID.Refresh()     
    else
      this.w_ZOOM = This.oParentObject.w_Zoomdetcf
      this.w_ZOOMPA = This.oParentObject.w_Zoomdetpa
      this.w_ZOOMCB = This.oParentObject.w_Zoomdetcb
      if this.oParentObject.w_TIPOVISUA="C"
        this.w_ZOOMPA.VISIBLE = .F.
        this.w_ZOOMCB.VISIBLE = .F.
        this.w_ZOOM.VISIBLE = .T.
      else
        this.w_ZOOM.VISIBLE = .F.
        this.w_ZOOMPA.VISIBLE = .T.
        this.w_ZOOMCB.VISIBLE = .T.
      endif
      this.w_PERIODI = this.w_PERIODI+1
      if this.oParentObject.w_Tipovisua="C"
        this.GRID = this.w_Zoom.GRD
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.GRID.Refresh()     
      else
        this.GRID = this.w_ZoomPa.GRD
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.GRID.Refresh()     
      endif
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.GRID.Column2.width = 0
    this.GRID.Column104.width = 115
    if this.oParentObject.w_Tipovisua$"CT"
      this.GRID.Column105.width = 0
    else
      this.GRID.Column1.width = 0
      this.GRID.Column105.width = 200
    endif
    if this.w_GIORNI =1
      * --- Le prime due colonne contengono l'intestazione
      this.w_COLONNA = 3
      if this.w_PERIODI>101
        i_retcode = 'stop'
        return
      endif
      this.w_TITLE = this.oParentObject.w_DATINI - 1
      this.w_GGSET = 1
      do while this.w_COLONNA <= this.w_PERIODI + 1
        INDICE=alltrim(str(this.w_COLONNA,3,0))
        if this.w_GGSET <> 1
          this.GRID.Column&INDICE..hdr.caption = DTOC(cp_todate(this.w_TITLE))
          this.GRID.Column&INDICE..width = 90
        else
          this.GRID.Column&INDICE..hdr.caption = AH_MSGFORMAT ("Saldo al") + CHR(32) + dtoc(cp_todate(this.oParentObject.w_DATINI)-1)
          this.GRID.Column&INDICE..width = 115
        endif
        this.GRID.Column&INDICE..hdr.forecolor = RGB(0,0,0)
        this.GRID.Column&INDICE..visible = .T.
        this.GRID.Column&INDICE..inputmask = v_pv[80] 
        this.GRID.Column&INDICE..dynamicbackcolor = "iif(!EMPTY(TIPODATO), RGB(240,240,240), iif(TOT_"+ RIGHT("000"+ALLTRIM(STR(VAL(INDICE)-3)),3)+ " <0 , RGB(255,0,0), IIF(ALLTRIM(NUMCOR) = 'TOTALI', RGB(255,255,64), RGB(255,255,255))))"
        this.w_COLONNA = this.w_COLONNA + 1
        this.w_TITLE = this.w_TITLE + 1
        this.w_GGSET = this.w_GGSET + 1
      enddo
    endif
    if this.w_GIORNI=7
      * --- La prima colonna contiene l'intestazione
      this.w_COLONNA = 3
      if this.w_PERIODI>101
        i_retcode = 'stop'
        return
      endif
      this.w_GGSET = 1
      do while this.w_COLONNA <= this.w_PERIODI + 1
        INDICE=alltrim(str(this.w_COLONNA,3,0))
        if this.w_GGSET <> 1
          this.GRID.Column&INDICE..hdr.caption = STR(this.w_GGSET-1)+chr(176)+ chr(32)+ AH_MSGFORMAT ("Settimana")
        else
          this.GRID.Column&INDICE..hdr.caption = AH_MSGFORMAT ("Saldo al") + CHR(32) + dtoc(cp_todate(this.oParentObject.w_DATINI)-1)
        endif
        this.GRID.Column&INDICE..hdr.forecolor = RGB(0,0,0)
        this.GRID.Column&INDICE..visible = .T.
        this.GRID.Column&INDICE..width = 115
        this.GRID.Column&INDICE..inputmask = v_pv[80]
        this.GRID.Column&INDICE..dynamicbackcolor = "iif(!EMPTY(TIPODATO), RGB(240,240,240), iif(TOT_"+ RIGHT("000"+ALLTRIM(STR(VAL(INDICE)-3)),3)+ " <0 , RGB(255,0,0), IIF(ALLTRIM(NUMCOR) = 'TOTALI', RGB(255,255,64), RGB(255,255,255))))"
        this.w_COLONNA = this.w_COLONNA + 1
        this.w_GGSET = this.w_GGSET + 1
      enddo
    endif
    if this.w_GIORNI=15
      this.w_COLONNA = 3
      if this.w_PERIODI>101
        i_retcode = 'stop'
        return
      endif
      this.w_GGSET = 1
      do while this.w_COLONNA <= this.w_PERIODI + 1
        INDICE=alltrim(str(this.w_COLONNA,3,0))
        if this.W_ggset <> 1
          this.GRID.Column&INDICE..hdr.caption = STR(this.w_GGSET-1)+chr(176)+ chr(32)+ AH_MSGFORMAT ("Quindicina")
        else
          this.GRID.Column&INDICE..hdr.caption = AH_MSGFORMAT ("Saldo al")+chr(32) + dtoc(cp_todate(this.oParentObject.w_DATINI)-1)
        endif
        this.GRID.Column&INDICE..hdr.forecolor = RGB(0,0,0)
        this.GRID.Column&INDICE..visible = .T.
        this.GRID.Column&INDICE..width = 115
        this.GRID.Column&INDICE..inputmask = v_pv[80] 
        this.GRID.Column&INDICE..dynamicbackcolor = "iif(!EMPTY(TIPODATO), RGB(240,240,240), iif(TOT_"+ RIGHT("000"+ALLTRIM(STR(VAL(INDICE)-3)),3)+ " <0 , RGB(255,0,0), IIF(ALLTRIM(NUMCOR) = 'TOTALI', RGB(255,255,64), RGB(255,255,255))))"
        this.w_COLONNA = this.w_COLONNA + 1
        this.w_GGSET = this.w_GGSET + 1
      enddo
    endif
    if this.w_GIORNI=30
      this.w_COLONNA = 3
      if this.w_PERIODI>101
        i_retcode = 'stop'
        return
      endif
      this.w_GGSET = 1
      do while this.w_COLONNA <= this.w_PERIODI + 1
        INDICE=alltrim(str(this.w_COLONNA,3,0))
        if this.W_ggset <> 1
          this.GRID.Column&INDICE..hdr.caption = STR(this.w_GGSET-1)+chr(176)+ chr(32)+AH_MSGFORMAT ("Mese")
        else
          this.GRID.Column&INDICE..hdr.caption = AH_MSGFORMAT ("Saldo al")+chr(32) + dtoc(cp_todate(this.oParentObject.w_DATINI)-1)
        endif
        this.GRID.Column&INDICE..hdr.forecolor = RGB(0,0,0)
        this.GRID.Column&INDICE..visible = .T.
        this.GRID.Column&INDICE..width = 115
        this.GRID.Column&INDICE..inputmask = v_pv[80] 
        this.GRID.Column&INDICE..dynamicbackcolor = "iif(!EMPTY(TIPODATO), RGB(240,240,240), iif(TOT_"+ RIGHT("000"+ALLTRIM(STR(VAL(INDICE)-3)),3)+ " <0 , RGB(255,0,0), IIF(ALLTRIM(NUMCOR) = 'TOTALI', RGB(255,255,64), RGB(255,255,255))))"
        this.w_COLONNA = this.w_COLONNA + 1
        this.w_GGSET = this.w_GGSET + 1
      enddo
    endif
    * --- Nascondo le colonne in esubero
    do while this.w_COLONNA <=103
      INDICE=alltrim(str(this.w_COLONNA,3,0))
      this.GRID.Column&INDICE..visible = .F.
      this.GRID.Column&INDICE..width = 0
      this.w_COLONNA = this.w_COLONNA + 1
    enddo
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_GIOSMO = iif(this.w_DATAINISCA - this.w_DATASMOB>=999, 999, this.w_DATAINISCA - this.w_DATASMOB )
    if this.w_GIOSMO>0
      Select "CURS_SMOBSCAD"
      LOCATE FOR SSCODICE=this.oParentObject.w_SMOSCA AND this.w_GIOSMO>=SSGIOSCA AND this.w_GIOSMO<=SSGIOSCF
      if FOUND ( )
        this.w_FLAGSMOB = "S"
        this.w_PERRID = 100-Nvl(Ssimpori,0)
        this.w_IMPSMOB = Cp_Round(this.w_ImpSmob*Ssimpori/100,this.w_Dectot)
        if SSGIOSPO<>0 OR SSGIOFIS<>0
          this.w_PERIODI_CALC = 0
          this.w_FINE_CALC = this.w_DATAINISCA + SSGIOSPO - 1
          if SSGIOFIS<>0
            if SSGIOFIS=31
              this.w_FINE_CALC = GOMONTH(this.w_FINE_CALC, 1)
              this.w_FINE_CALC = DATE(YEAR(this.w_FINE_CALC), MONTH(this.w_FINE_CALC), 1)-1
            else
              if DAY(this.w_FINE_CALC)>=SSGIOFIS
                this.w_FINE_CALC = GOMONTH(this.w_FINE_CALC, 1)
              endif
              this.w_February = .f.
              if MONTH(this.w_FINE_CALC)=2 AND SSGIOFIS>=29
                this.w_February = .t.
                this.w_FINE_CALC = GOMONTH(this.w_FINE_CALC, 1)
              endif
              this.w_FINE_CALC = DATE(YEAR(this.w_FINE_CALC), MONTH(this.w_FINE_CALC), SSGIOFIS)
              if this.w_February
                this.w_FINE_CALC = GOMONTH(this.w_FINE_CALC, -1)
              endif
            endif
          endif
          this.w_ORDINA = "A"
          this.w_DATASMOB = this.w_Fine_Calc
        endif
      endif
    endif
    Select CashEffettivo
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcolo Importo per DDT e Ordini evasi
    vq_exec("QUERY\GSTE3BVF.VQR",this,"RISCORDT")
    select RISCORDT 
 INDEX ON MVSERIAL TAG RIS_MVS
    SELECT RISCORDT
    GO TOP
    SCAN
    if this.w_SERIAL <> RISCORDT.MVSERIAL
      if not empty(this.w_SERIAL)
        this.Pag11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_DVALMAG = 0
      this.w_TVALMAG = 0
      this.w_VALIVA = 0
      SELECT RISCORDT
      this.w_SERIAL = MVSERIAL
      this.w_VALNAZ = IIF(EMPTY(NVL(VALNAZ," ")), g_PERVAL, VALNAZ)
      this.w_TOTALE = IIF(RISCORDT.MVAFLOM1 $ "XI",RISCORDT.MVAIMPS1,0)+IIF(RISCORDT.MVAFLOM2 $ "XI",RISCORDT.MVAIMPS2,0)+IIF(RISCORDT.MVAFLOM3 $ "XI",RISCORDT.MVAIMPS3,0)
      this.w_TOTALE = this.w_TOTALE+IIF(RISCORDT.MVAFLOM4 $ "XI",RISCORDT.MVAIMPS4,0)+IIF(RISCORDT.MVAFLOM5 $ "XI",RISCORDT.MVAIMPS5,0)+IIF(RISCORDT.MVAFLOM6 $ "XI",RISCORDT.MVAIMPS6,0)
      this.w_TOTALE = this.w_TOTALE+IIF(RISCORDT.MVAFLOM1="X",RISCORDT.MVAIMPN1,0)+IIF(RISCORDT.MVAFLOM2="X",RISCORDT.MVAIMPN2,0)+IIF(RISCORDT.MVAFLOM3="X",RISCORDT.MVAIMPN3,0)
      this.w_TOTALE = this.w_TOTALE+IIF(RISCORDT.MVAFLOM4="X",RISCORDT.MVAIMPN4,0)+IIF(RISCORDT.MVAFLOM5="X",RISCORDT.MVAIMPN5,0)+IIF(RISCORDT.MVAFLOM6="X",RISCORDT.MVAIMPN6,0)
    endif
    this.w_FLEVAS = NVL(MVFLEVAS,"")
    this.w_TIPRIG = NVL(TIPRIG, "  ")
    this.w_PREZZO = NVL(PREZZO, 0)
    this.w_IMPNAZ = NVL(IMPNAZ, 0)
    this.w_IMPEVA = NVL(IMPEVA, 0)
    this.w_FLOMAG = NVL(FLOMAG, " ")
    this.w_PERIVA = NVL(PERIVA,0)
    this.w_DECTOP = g_PERPVL
    this.w_IMPACC = NVL(MVIMPACC, 0)
    this.w_QTAEV1 = NVL(MVQTAEV1, 0)
    this.w_QTAMOV = NVL(MVQTAMOV, 0)
    this.w_QTAEVA = NVL(MVQTAEVA, 0)
    this.w_VALIVA = 0
    this.w_VALORERIGA = IIF(this.w_FLOMAG="X",this.w_IMPNAZ,0)
    this.w_VALORERIGA = this.w_VALORERIGA+ IIF(this.w_PERIVA<>0 AND this.w_FLOMAG $ "XI",IVAROUND((this.w_IMPNAZ * this.w_PERIVA / 100), this.w_DECTOP, 1, this.w_VALNAZ),0)
    if this.w_FLEVAS<>"S" And ( (this.w_TIPRIG<>"F" And this.w_QTAMOV>this.w_QTAEVA) OR (this.w_TIPRIG="F" And Abs(this.w_PREZZO)>Abs(this.w_IMPEVA) ) ) 
      if this.w_TIPRIG="F"
        if this.w_PREZZO=0
          this.w_IMPNAZ = 0
        else
          this.w_IMPNAZ = cp_ROUND((this.w_IMPNAZ/this.w_PREZZO)*(this.w_PREZZO-this.w_IMPEVA), this.w_DECTOP)
        endif
      else
        this.w_IMPNAZ = cp_ROUND((this.w_IMPNAZ/this.w_QTAMOV)*(this.w_QTAMOV-this.w_QTAEV1), this.w_DECTOP)
      endif
      if this.w_FLOMAG $ "XI" AND this.w_PERIVA<>0
        this.w_VALIVA = IVAROUND((this.w_IMPNAZ * this.w_PERIVA / 100), this.w_DECTOP, 1, this.w_VALNAZ)
        this.w_IMPNAZ = this.w_IMPNAZ+this.w_VALIVA
      endif
      this.w_IMPNAZ = IIF(this.w_FLOMAG="X",this.w_IMPNAZ,IIF(this.w_FLOMAG="I",this.w_VALIVA,0))
    else
      this.w_IMPNAZ = 0
    endif
    this.w_DVALMAG = this.w_DVALMAG + this.w_IMPNAZ
    this.w_TVALMAG = this.w_TVALMAG + this.w_VALORERIGA
    SELECT RISCORDT
    ENDSCAN
    if not empty(this.w_SERIAL)
      this.Pag11()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    use in select ("Riscordt")
  endproc


  procedure Pag11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifico importi rate
    if this.w_TVALMAG > this.w_DVALMAG
      if this.oParentObject.w_FLSEVA="S"
        * --- Calcolo il totale rate
        * --- Select from DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select SUM(RSIMPRAT) as TOTRAT  from "+i_cTable+" DOC_RATE ";
              +" where RSSERIAL="+cp_ToStrODBC(this.w_SERIAL)+"";
               ,"_Curs_DOC_RATE")
        else
          select SUM(RSIMPRAT) as TOTRAT from (i_cTable);
           where RSSERIAL=this.w_SERIAL;
            into cursor _Curs_DOC_RATE
        endif
        if used('_Curs_DOC_RATE')
          select _Curs_DOC_RATE
          locate for 1=1
          do while not(eof())
          this.w_TOTRAT = _Curs_DOC_RATE.TOTRAT
            select _Curs_DOC_RATE
            continue
          enddo
          use
        endif
        this.w_DVALMAG = cp_ROUND( this.w_TOTRAT * this.w_DVALMAG / this.w_TVALMAG, g_PERPVL )
        this.w_TVALMAG = this.w_TOTRAT
        * --- Select from TMPCASHATT
        i_nConn=i_TableProp[this.TMPCASHATT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCASHATT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPCASHATT ";
              +" where CFSERIAL="+cp_ToStrODBC(this.w_SERIAL)+"";
              +" order by CFSERIAL, CFDATRAT";
               ,"_Curs_TMPCASHATT")
        else
          select * from (i_cTable);
           where CFSERIAL=this.w_SERIAL;
           order by CFSERIAL, CFDATRAT;
            into cursor _Curs_TMPCASHATT
        endif
        if used('_Curs_TMPCASHATT')
          select _Curs_TMPCASHATT
          locate for 1=1
          do while not(eof())
          if this.w_TVALMAG > this.w_DVALMAG
            this.w_IMPRAT = NVL(_Curs_TMPCASHATT.CFIMPRAT,0)
            this.w_DIFFRAT = MIN(this.w_IMPRAT, this.w_TVALMAG-this.w_DVALMAG)
            this.w_IMPRAT = this.w_IMPRAT - this.w_DIFFRAT
            this.w_DATRAT = _Curs_TMPCASHATT.CFDATRAT
            this.w_NUMRAT = _Curs_TMPCASHATT.CFNUMRAT
            this.w_PERCEN = cp_Round((this.w_DVALMAG*100/this.w_TOTRAT),2)
            * --- Aggiorno la percentuale di evasione sul documento
            * --- Write into TMPCASHATT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPCASHATT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPCASHATT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCASHATT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CFPERCEN ="+cp_NullLink(cp_ToStrODBC(this.w_PERCEN),'TMPCASHATT','CFPERCEN');
                  +i_ccchkf ;
              +" where ";
                  +"CFSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                     )
            else
              update (i_cTable) set;
                  CFPERCEN = this.w_PERCEN;
                  &i_ccchkf. ;
               where;
                  CFSERIAL = this.w_SERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Aggiorno l'importo della rata del documento
            * --- Write into TMPCASHATT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPCASHATT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPCASHATT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCASHATT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CFIMPRAT ="+cp_NullLink(cp_ToStrODBC(this.w_IMPRAT),'TMPCASHATT','CFIMPRAT');
                  +i_ccchkf ;
              +" where ";
                  +"CFSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                  +" and CFDATRAT = "+cp_ToStrODBC(this.w_DATRAT);
                  +" and CFNUMRAT = "+cp_ToStrODBC(this.w_NUMRAT);
                     )
            else
              update (i_cTable) set;
                  CFIMPRAT = this.w_IMPRAT;
                  &i_ccchkf. ;
               where;
                  CFSERIAL = this.w_SERIAL;
                  and CFDATRAT = this.w_DATRAT;
                  and CFNUMRAT = this.w_NUMRAT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_TVALMAG = this.w_TVALMAG - this.w_DIFFRAT
          else
            exit
          endif
            select _Curs_TMPCASHATT
            continue
          enddo
          use
        endif
      else
        * --- Write into TMPCASHATT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPCASHATT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCASHATT_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="CFSERIAL,CFNUMRAT,CFDATRAT"
          do vq_exec with 'gste6bvf',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCASHATT_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPCASHATT.CFSERIAL = _t2.CFSERIAL";
                  +" and "+"TMPCASHATT.CFNUMRAT = _t2.CFNUMRAT";
                  +" and "+"TMPCASHATT.CFDATRAT = _t2.CFDATRAT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CFIMPRAT = _t2.CFIMPRAT ";
              +",CFPERCEN = _t2.CFPERCEN";
              +i_ccchkf;
              +" from "+i_cTable+" TMPCASHATT, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPCASHATT.CFSERIAL = _t2.CFSERIAL";
                  +" and "+"TMPCASHATT.CFNUMRAT = _t2.CFNUMRAT";
                  +" and "+"TMPCASHATT.CFDATRAT = _t2.CFDATRAT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCASHATT, "+i_cQueryTable+" _t2 set ";
              +"TMPCASHATT.CFIMPRAT = _t2.CFIMPRAT ";
              +",TMPCASHATT.CFPERCEN = _t2.CFPERCEN";
              +Iif(Empty(i_ccchkf),"",",TMPCASHATT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMPCASHATT.CFSERIAL = t2.CFSERIAL";
                  +" and "+"TMPCASHATT.CFNUMRAT = t2.CFNUMRAT";
                  +" and "+"TMPCASHATT.CFDATRAT = t2.CFDATRAT";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCASHATT set (";
              +"CFIMPRAT,";
              +"CFPERCEN";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.CFIMPRAT ,";
              +"t2.CFPERCEN";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMPCASHATT.CFSERIAL = _t2.CFSERIAL";
                  +" and "+"TMPCASHATT.CFNUMRAT = _t2.CFNUMRAT";
                  +" and "+"TMPCASHATT.CFDATRAT = _t2.CFDATRAT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCASHATT set ";
              +"CFIMPRAT = _t2.CFIMPRAT ";
              +",CFPERCEN = _t2.CFPERCEN";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".CFSERIAL = "+i_cQueryTable+".CFSERIAL";
                  +" and "+i_cTable+".CFNUMRAT = "+i_cQueryTable+".CFNUMRAT";
                  +" and "+i_cTable+".CFDATRAT = "+i_cQueryTable+".CFDATRAT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CFIMPRAT = (select CFIMPRAT  from "+i_cQueryTable+" where "+i_cWhere+")";
              +",CFPERCEN = (select CFPERCEN from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  procedure Pag12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    use in select ("Filbanca")
    use in select ("Selepart")
    use in select ("CashFlow")
    use in select ("CashEffettivo")
    use in select ("Graficodett1")
    use in select ("TipoPag")
    use in select ("Contibanca")
    use in select ("Curs_Smobscad")
    use in select ("ZoomCursor")
    * --- Drop temporary table TMPFL2
    i_nIdx=cp_GetTableDefIdx('TMPFL2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPFL2')
    endif
    * --- Drop temporary table TMPFL3
    i_nIdx=cp_GetTableDefIdx('TMPFL3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPFL3')
    endif
    * --- Drop temporary table TMPCASHATT
    i_nIdx=cp_GetTableDefIdx('TMPCASHATT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPCASHATT')
    endif
  endproc


  procedure Pag13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_TIPDEN="T"
      do case
        case this.oParentObject.w_DATINI<DATE(YEAR(this.oParentObject.w_DATINI),5,16)+icase(DOW(DATE(YEAR(this.oParentObject.w_DATINI),5,16))=0, 1, DOW(DATE(YEAR(this.oParentObject.w_DATINI),5,16))=7, 2, 0)
          this.w_INIZIO_SALIVA = DATE(YEAR(this.oParentObject.w_DATINI),1,1)
        case this.oParentObject.w_DATINI<DATE(YEAR(this.oParentObject.w_DATINI),8,16)+icase(DOW(DATE(YEAR(this.oParentObject.w_DATINI),8,16))=0, 1, DOW(DATE(YEAR(this.oParentObject.w_DATINI),8,16))=7, 2, 0)
          this.w_INIZIO_SALIVA = DATE(YEAR(this.oParentObject.w_DATINI),4,1)
        case this.oParentObject.w_DATINI<DATE(YEAR(this.oParentObject.w_DATINI),11,16)+icase(DOW(DATE(YEAR(this.oParentObject.w_DATINI),11,16))=0, 1, DOW(DATE(YEAR(this.oParentObject.w_DATINI),11,16))=7, 2, 0)
          this.w_INIZIO_SALIVA = DATE(YEAR(this.oParentObject.w_DATINI),7,1)
        otherwise
          this.w_INIZIO_SALIVA = DATE(YEAR(this.oParentObject.w_DATINI),10,1)
      endcase
      do case
        case this.oParentObject.w_DATFIN>=DATE(YEAR(this.oParentObject.w_DATFIN),11,16)+icase(DOW(DATE(YEAR(this.oParentObject.w_DATFIN),11,16))=0, 1, DOW(DATE(YEAR(this.oParentObject.w_DATFIN),11,16))=7, 2, 0)
          this.w_FINE_SALIVA = DATE(YEAR(this.oParentObject.w_DATFIN),9,30)
        case this.oParentObject.w_DATFIN>=DATE(YEAR(this.oParentObject.w_DATFIN),8,16)+icase(DOW(DATE(YEAR(this.oParentObject.w_DATFIN),8,16))=0, 1, DOW(DATE(YEAR(this.oParentObject.w_DATFIN),8,16))=7, 2, 0)
          this.w_FINE_SALIVA = DATE(YEAR(this.oParentObject.w_DATFIN),6,30)
        case this.oParentObject.w_DATFIN>=DATE(YEAR(this.oParentObject.w_DATFIN),5,16)+icase(DOW(DATE(YEAR(this.oParentObject.w_DATFIN),5,16))=0, 1, DOW(DATE(YEAR(this.oParentObject.w_DATFIN),5,16))=7, 2, 0)
          this.w_FINE_SALIVA = DATE(YEAR(this.oParentObject.w_DATFIN),3,31)
        otherwise
          this.w_FINE_SALIVA = DATE(YEAR(this.oParentObject.w_DATFIN)-1,12,31)
      endcase
    else
      this.w_INIZIO_SALIVA = DATE(YEAR(this.oParentObject.w_DATINI),MONTH(this.oParentObject.w_DATINI),16)
      this.w_INIZIO_SALIVA = this.w_INIZIO_SALIVA+icase(DOW(this.w_INIZIO_SALIVA)=0, 1, DOW(this.w_INIZIO_SALIVA)=7, 2, 0)
      if DAY(this.oParentObject.w_DATINI)<=DAY(this.w_INIZIO_SALIVA)
        this.w_INIZIO_SALIVA = DATE(YEAR(GOMONTH(this.oParentObject.w_DATINI,-1)),MONTH(GOMONTH(this.oParentObject.w_DATINI,-1)),1)
      else
        this.w_INIZIO_SALIVA = DATE(YEAR(this.oParentObject.w_DATINI),MONTH(this.oParentObject.w_DATINI),1)
      endif
      this.w_FINE_SALIVA = DATE(YEAR(this.oParentObject.w_DATFIN),MONTH(this.oParentObject.w_DATFIN),16)
      this.w_FINE_SALIVA = this.w_FINE_SALIVA+icase(DOW(this.w_FINE_SALIVA)=0, 1, DOW(this.w_FINE_SALIVA)=7, 2, 0)
      if DAY(this.oParentObject.w_DATFIN)<DAY(this.w_FINE_SALIVA)
        this.w_FINE_SALIVA = DATE(YEAR(GOMONTH(this.oParentObject.w_DATFIN,-1)),MONTH(GOMONTH(this.oParentObject.w_DATFIN,-1)),1)-1
      else
        this.w_FINE_SALIVA = DATE(YEAR(this.oParentObject.w_DATFIN),MONTH(this.oParentObject.w_DATFIN),1)-1
      endif
    endif
    if this.w_INIZIO_SALIVA<=this.w_FINE_SALIVA
      this.w_INIZIO_ELAB = this.w_INIZIO_SALIVA
      Ah_msg("Elabora saldo stimato IVA")
      vq_exec("QUERY\GSTE8BFLV.VQR",this,"SaldoStimato")
      if USED("SaldoStimato")
        Select SaldoStimato
        SCAN
        this.w_DATADOC = CP_TODATE(_Curs_SaldoStimato.SIDATA)
        * --- Calcolo data versamento
        if g_TIPDEN="T"
          do case
            case MONTH(this.w_DATADOC)<=3
              this.w_DATADOC = DATE(YEAR(this.w_DATADOC),5,16)
            case MONTH(this.w_DATADOC)<=6
              this.w_DATADOC = DATE(YEAR(this.w_DATADOC),8,16)
            case MONTH(this.w_DATADOC)<=9
              this.w_DATADOC = DATE(YEAR(this.w_DATADOC),11,16)
            otherwise
              this.w_DATADOC = DATE(YEAR(this.w_DATADOC)+1,3,16)
          endcase
        else
          this.w_DATADOC = DATE(YEAR(GOMONTH(this.w_DATADOC,1)),MONTH(GOMONTH(this.w_DATADOC,1)),16)
        endif
        this.w_DATADOC = icase(DOW(this.w_DATADOC)=0, this.w_DATADOC+1, DOW(this.w_DATADOC)=7, this.w_DATADOC+2, this.w_DATADOC)
        this.w_ORDINA = iif(this.w_DATADOC < this.oParentObject.w_DATINI,"B","A")
        * --- Insert into TMPFL1
        i_nConn=i_TableProp[this.TMPFL1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPFL1_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPFL1_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CODBAN"+",IMPDAR"+",IMPAVE"+",ORDINA"+",DATVAL"+",CAOAPE"+",DATAPE"+",ORDINE"+",TIPOCASHFLOW"+",PTCODVAL"+",PTNUMPAR"+",PTTIPCON"+",PTCODCON"+",PTMODPAG"+",ANDESCRI"+",MPTIPPAG"+",MVFLVEAC"+",MVCLADOC"+",PERCEN"+",TOTRATE"+",MVDATDOC"+",MVALFDOC"+",MVNUMDOC"+",MVTIPDOC"+",MVSERIAL"+",ANCATCOM"+",IMPRID"+",PERRID"+",FLAGSMOB"+",DATASMOB"+",IMPSMOB"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(SaldoStimato.Codban),'TMPFL1','CODBAN');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Impdar),'TMPFL1','IMPDAR');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Impave),'TMPFL1','IMPAVE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_Ordina),'TMPFL1','ORDINA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_Datadoc),'TMPFL1','DATVAL');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Caoape),'TMPFL1','CAOAPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_Datadoc),'TMPFL1','DATAPE');
          +","+cp_NullLink(cp_ToStrODBC(0),'TMPFL1','ORDINE');
          +","+cp_NullLink(cp_ToStrODBC("S"),'TMPFL1','TIPOCASHFLOW');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Ptcodval),'TMPFL1','PTCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Ptnumpar),'TMPFL1','PTNUMPAR');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Pttipcon),'TMPFL1','PTTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Ptcodcon),'TMPFL1','PTCODCON');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Ptmodpag),'TMPFL1','PTMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Andescri),'TMPFL1','ANDESCRI');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Mptippag),'TMPFL1','MPTIPPAG');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Mvflveac),'TMPFL1','MVFLVEAC');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Mvcladoc),'TMPFL1','MVCLADOC');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Percen),'TMPFL1','PERCEN');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Totrate),'TMPFL1','TOTRATE');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Mvdatdoc),'TMPFL1','MVDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Mvalfdoc),'TMPFL1','MVALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Mvnumdoc),'TMPFL1','MVNUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Mvtipdoc),'TMPFL1','MVTIPDOC');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Mvserial),'TMPFL1','MVSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Ancatcom),'TMPFL1','ANCATCOM');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Imprid),'TMPFL1','IMPRID');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Perrid),'TMPFL1','PERRID');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Flagsmob),'TMPFL1','FLAGSMOB');
          +","+cp_NullLink(cp_ToStrODBC(SaldoStimato.Datasmob),'TMPFL1','DATASMOB');
          +","+cp_NullLink(cp_ToStrODBC(this.w_Impsmob),'TMPFL1','IMPSMOB');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CODBAN',SaldoStimato.Codban,'IMPDAR',SaldoStimato.Impdar,'IMPAVE',SaldoStimato.Impave,'ORDINA',this.w_Ordina,'DATVAL',this.w_Datadoc,'CAOAPE',SaldoStimato.Caoape,'DATAPE',this.w_Datadoc,'ORDINE',0,'TIPOCASHFLOW',"S",'PTCODVAL',SaldoStimato.Ptcodval,'PTNUMPAR',SaldoStimato.Ptnumpar,'PTTIPCON',SaldoStimato.Pttipcon)
          insert into (i_cTable) (CODBAN,IMPDAR,IMPAVE,ORDINA,DATVAL,CAOAPE,DATAPE,ORDINE,TIPOCASHFLOW,PTCODVAL,PTNUMPAR,PTTIPCON,PTCODCON,PTMODPAG,ANDESCRI,MPTIPPAG,MVFLVEAC,MVCLADOC,PERCEN,TOTRATE,MVDATDOC,MVALFDOC,MVNUMDOC,MVTIPDOC,MVSERIAL,ANCATCOM,IMPRID,PERRID,FLAGSMOB,DATASMOB,IMPSMOB &i_ccchkf. );
             values (;
               SaldoStimato.Codban;
               ,SaldoStimato.Impdar;
               ,SaldoStimato.Impave;
               ,this.w_Ordina;
               ,this.w_Datadoc;
               ,SaldoStimato.Caoape;
               ,this.w_Datadoc;
               ,0;
               ,"S";
               ,SaldoStimato.Ptcodval;
               ,SaldoStimato.Ptnumpar;
               ,SaldoStimato.Pttipcon;
               ,SaldoStimato.Ptcodcon;
               ,SaldoStimato.Ptmodpag;
               ,SaldoStimato.Andescri;
               ,SaldoStimato.Mptippag;
               ,SaldoStimato.Mvflveac;
               ,SaldoStimato.Mvcladoc;
               ,SaldoStimato.Percen;
               ,SaldoStimato.Totrate;
               ,SaldoStimato.Mvdatdoc;
               ,SaldoStimato.Mvalfdoc;
               ,SaldoStimato.Mvnumdoc;
               ,SaldoStimato.Mvtipdoc;
               ,SaldoStimato.Mvserial;
               ,SaldoStimato.Ancatcom;
               ,SaldoStimato.Imprid;
               ,SaldoStimato.Perrid;
               ,SaldoStimato.Flagsmob;
               ,SaldoStimato.Datasmob;
               ,this.w_Impsmob;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        ENDSCAN
      endif
      USE IN SaldoStimato
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='*TMPFL3'
    this.cWorkTables[3]='*TMPFL1'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='CASHFLOW'
    this.cWorkTables[6]='DOC_RATE'
    this.cWorkTables[7]='*TMPCASHATT'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_Gste10bfl')
      use in _Curs_Gste10bfl
    endif
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_Gste10bfl')
      use in _Curs_Gste10bfl
    endif
    if used('_Curs_DOC_RATE')
      use in _Curs_DOC_RATE
    endif
    if used('_Curs_TMPCASHATT')
      use in _Curs_TMPCASHATT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
