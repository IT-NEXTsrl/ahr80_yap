* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sda                                                        *
*              Dichiarazione annuale IVA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_105]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-20                                                      *
* Last revis.: 2007-07-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sda",oParentObject))

* --- Class definition
define class tgscg_sda as StdForm
  Top    = 27
  Left   = 69

  * --- Standard Properties
  Width  = 483
  Height = 192
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-10"
  HelpContextID=194370199
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  ATTIMAST_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gscg_sda"
  cComment = "Dichiarazione annuale IVA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_ANNO = space(4)
  o_ANNO = space(4)
  w_NUMPER = 0
  w_MTIPLIQ = space(1)
  o_MTIPLIQ = space(1)
  w_CODATT = space(5)
  w_DESATT = space(35)
  w_FLDEFI = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_IVACOF = space(16)
  w_MFLTEST = space(1)
  w_DENMAG = 0
  w_PRPARI = 0
  w_PREFIS = space(20)
  w_INTLIG = space(1)
  w_ATNUMREG = 0
  w_ATTIPREG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sdaPag1","gscg_sda",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ATTIMAST'
    this.cWorkTables[2]='AZIENDA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_BDA with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sda
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_ANNO=space(4)
      .w_NUMPER=0
      .w_MTIPLIQ=space(1)
      .w_CODATT=space(5)
      .w_DESATT=space(35)
      .w_FLDEFI=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_IVACOF=space(16)
      .w_MFLTEST=space(1)
      .w_DENMAG=0
      .w_PRPARI=0
      .w_PREFIS=space(20)
      .w_INTLIG=space(1)
      .w_ATNUMREG=0
      .w_ATTIPREG=space(1)
        .w_CODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_ANNO = ALLTRIM(STR(YEAR(i_datsys)))
        .w_NUMPER = 0
        .w_MTIPLIQ = 'S'
        .w_CODATT = IIF(.w_MTIPLIQ<>'R', g_CATAZI, SPACE(5))
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODATT))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_FLDEFI = 'N'
        .w_OBTEST = i_INIDAT
          .DoRTCalc(9,9,.f.)
        .w_MFLTEST = ' '
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
    endwith
    this.DoRTCalc(11,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_ANNO<>.w_ANNO
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.t.)
            .w_NUMPER = 0
        .DoRTCalc(4,4,.t.)
        if .o_MTIPLIQ<>.w_MTIPLIQ
            .w_CODATT = IIF(.w_MTIPLIQ<>'R', g_CATAZI, SPACE(5))
          .link_1_5('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMTIPLIQ_1_4.enabled = this.oPgFrm.Page1.oPag.oMTIPLIQ_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_5.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_5.mCond()
    this.oPgFrm.Page1.oPag.oPRPARI_1_18.enabled = this.oPgFrm.Page1.oPag.oPRPARI_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODATT_1_5.visible=!this.oPgFrm.Page1.oPag.oCODATT_1_5.mHide()
    this.oPgFrm.Page1.oPag.oDESATT_1_6.visible=!this.oPgFrm.Page1.oPag.oDESATT_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZIVACOF,AZDENMAG";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZIVACOF,AZDENMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_IVACOF = NVL(_Link_.AZIVACOF,space(16))
      this.w_DENMAG = NVL(_Link_.AZDENMAG,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_IVACOF = space(16)
      this.w_DENMAG = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODATT,ATDESATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIMAST','*','ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_5'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_DESATT = NVL(_Link_.ATDESATT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(5)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_2.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_2.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oMTIPLIQ_1_4.RadioValue()==this.w_MTIPLIQ)
      this.oPgFrm.Page1.oPag.oMTIPLIQ_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_5.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_5.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_6.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_6.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDEFI_1_7.RadioValue()==this.w_FLDEFI)
      this.oPgFrm.Page1.oPag.oFLDEFI_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMFLTEST_1_16.RadioValue()==this.w_MFLTEST)
      this.oPgFrm.Page1.oPag.oMFLTEST_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPARI_1_18.value==this.w_PRPARI)
      this.oPgFrm.Page1.oPag.oPRPARI_1_18.value=this.w_PRPARI
    endif
    if not(this.oPgFrm.Page1.oPag.oPREFIS_1_20.value==this.w_PREFIS)
      this.oPgFrm.Page1.oPag.oPREFIS_1_20.value=this.w_PREFIS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ANNO)) or not(Val(.w_ANNO)>1900))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATT))  and not(.w_MTIPLIQ='R')  and (g_ATTIVI='S' AND .w_MTIPLIQ<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATT_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CODATT)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ANNO = this.w_ANNO
    this.o_MTIPLIQ = this.w_MTIPLIQ
    return

enddefine

* --- Define pages as container
define class tgscg_sdaPag1 as StdContainer
  Width  = 479
  height = 192
  stdWidth  = 479
  stdheight = 192
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_2 as StdField with uid="FWDUQQUDDE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento della liquidazione",;
    HelpContextID = 199888134,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=106, Top=13, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Val(.w_ANNO)>1900)
    endwith
    return bRes
  endfunc


  add object oMTIPLIQ_1_4 as StdCombo with uid="VCSDZTNYRP",rtseq=4,rtrep=.f.,left=332,top=13,width=134,height=21;
    , ToolTipText = "Tipo liquidazione riepilogativa o per singola attivit�";
    , HelpContextID = 106249274;
    , cFormVar="w_MTIPLIQ",RowSource=""+"Per singola attivita,"+"Riepilogativa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMTIPLIQ_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oMTIPLIQ_1_4.GetRadio()
    this.Parent.oContained.w_MTIPLIQ = this.RadioValue()
    return .t.
  endfunc

  func oMTIPLIQ_1_4.SetRadio()
    this.Parent.oContained.w_MTIPLIQ=trim(this.Parent.oContained.w_MTIPLIQ)
    this.value = ;
      iif(this.Parent.oContained.w_MTIPLIQ=='S',1,;
      iif(this.Parent.oContained.w_MTIPLIQ=='R',2,;
      0))
  endfunc

  func oMTIPLIQ_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ATTIVI='S')
    endwith
   endif
  endfunc

  add object oCODATT_1_5 as StdField with uid="RFKKDYZVJD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 182751706,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=45, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ATTIMAST", oKey_1_1="ATCODATT", oKey_1_2="this.w_CODATT"

  func oCODATT_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ATTIVI='S' AND .w_MTIPLIQ<>'R')
    endwith
   endif
  endfunc

  func oCODATT_1_5.mHide()
    with this.Parent.oContained
      return (.w_MTIPLIQ='R')
    endwith
  endfunc

  func oCODATT_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIMAST','*','ATCODATT',cp_AbsName(this.parent,'oCODATT_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oDESATT_1_6 as StdField with uid="MIYFQISBXX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attivit�",;
    HelpContextID = 182692810,;
   bGlobalFont=.t.,;
    Height=21, Width=292, Left=174, Top=45, InputMask=replicate('X',35)

  func oDESATT_1_6.mHide()
    with this.Parent.oContained
      return (.w_MTIPLIQ='R')
    endwith
  endfunc


  add object oFLDEFI_1_7 as StdCombo with uid="VSMFUMITUA",rtseq=7,rtrep=.f.,left=106,top=77,width=122,height=21;
    , ToolTipText = "Se definitiva aggiorna il prorata ed il credito IVA annuale";
    , HelpContextID = 113284266;
    , cFormVar="w_FLDEFI",RowSource=""+"Definitiva,"+"In prova", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLDEFI_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oFLDEFI_1_7.GetRadio()
    this.Parent.oContained.w_FLDEFI = this.RadioValue()
    return .t.
  endfunc

  func oFLDEFI_1_7.SetRadio()
    this.Parent.oContained.w_FLDEFI=trim(this.Parent.oContained.w_FLDEFI)
    this.value = ;
      iif(this.Parent.oContained.w_FLDEFI=='S',1,;
      iif(this.Parent.oContained.w_FLDEFI=='N',2,;
      0))
  endfunc


  add object oBtn_1_12 as StdButton with uid="SAVQGQIKMH",left=370, top=140, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "premere per iniziare l'elaborazione";
    , HelpContextID = 194398950;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        do GSCG_BDA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="DOLPLIDGYZ",left=418, top=140, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 201687622;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMFLTEST_1_16 as StdCheck with uid="OBVCHJNZHV",rtseq=10,rtrep=.f.,left=106, top=139, caption="Stampa su modulo continuo",;
    ToolTipText = "Se attivo: stampa su modulo continuo",;
    HelpContextID = 54453702,;
    cFormVar="w_MFLTEST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMFLTEST_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMFLTEST_1_16.GetRadio()
    this.Parent.oContained.w_MFLTEST = this.RadioValue()
    return .t.
  endfunc

  func oMFLTEST_1_16.SetRadio()
    this.Parent.oContained.w_MFLTEST=trim(this.Parent.oContained.w_MFLTEST)
    this.value = ;
      iif(this.Parent.oContained.w_MFLTEST=='S',1,;
      0)
  endfunc

  add object oPRPARI_1_18 as StdField with uid="GZVSBCMEQD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PRPARI", cQueryName = "PRPARI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima pagina stampata nel corrispondente R.I.",;
    HelpContextID = 100912650,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=106, Top=107, cSayPict='"9999999"', cGetPict='"9999999"'

  func oPRPARI_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INTLIG='S')
    endwith
   endif
  endfunc

  add object oPREFIS_1_20 as StdField with uid="SJBUMXHJGU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PREFIS", cQueryName = "PREFIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso che comparir� nella stampa prima del numero di pagina",;
    HelpContextID = 210730506,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=314, Top=107, InputMask=replicate('X',20)


  add object oObj_1_23 as cp_runprogram with uid="YKIXWMKGBT",left=8, top=203, width=444,height=19,;
    caption='GSCG_BD1',;
   bGlobalFont=.t.,;
    prg="GSCG_BD1",;
    cEvent = "w_MTIPLIQ Changed,w_CODATT Changed,Blank,w_ANNO Changed",;
    nPag=1;
    , HelpContextID = 64053911

  add object oStr_1_8 as StdString with uid="NEQLFLHMCG",Visible=.t., Left=20, Top=45,;
    Alignment=1, Width=84, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_MTIPLIQ='R')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="IHIOCRVRVZ",Visible=.t., Left=20, Top=13,;
    Alignment=1, Width=84, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="LZYKPPGFJG",Visible=.t., Left=245, Top=13,;
    Alignment=1, Width=84, Height=15,;
    Caption="Liquidazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="FMIDQKWZEB",Visible=.t., Left=15, Top=77,;
    Alignment=1, Width=89, Height=15,;
    Caption="Elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="FWNKOATCCI",Visible=.t., Left=20, Top=107,;
    Alignment=1, Width=84, Height=15,;
    Caption="Ultima pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="QBIQLOVEJE",Visible=.t., Left=204, Top=109,;
    Alignment=1, Width=110, Height=15,;
    Caption="Prefisso num. pag.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sda','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
