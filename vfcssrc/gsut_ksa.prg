* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ksa                                                        *
*              Parametri accesso silente schedulatore                          *
*                                                                              *
*      Author: Pollina Fabrizio                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-10-06                                                      *
* Last revis.: 2016-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_ksa",oParentObject))

* --- Class definition
define class tgsut_ksa as StdForm
  Top    = 14
  Left   = 12

  * --- Standard Properties
  Width  = 608
  Height = 474
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-31"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  _IDX = 0
  GROUPSJ_IDX = 0
  CPUSERS_IDX = 0
  CPUSRGRP_IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  BUSIUNIT_IDX = 0
  cPrg = "gsut_ksa"
  cComment = "Parametri accesso silente schedulatore"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPO = space(1)
  w_FILECNF = space(254)
  o_FILECNF = space(254)
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_USERIN = 0
  o_USERIN = 0
  w_PWDUSER = space(50)
  o_PWDUSER = space(50)
  w_CODBUN = space(3)
  o_CODBUN = space(3)
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_PATHEXE = space(254)
  w_PARACSIL = space(0)
  w_FLASCRYP = space(1)
  o_FLASCRYP = space(1)
  w_FLWNDSTA = 0
  w_COMMENTO = space(100)
  w_ICONAME = space(50)
  w_FLHKSPEC = 0
  o_FLHKSPEC = 0
  w_FUNC__HK = space(3)
  o_FUNC__HK = space(3)
  w_CHAR__HK = space(1)
  o_CHAR__HK = space(1)
  w_SCHEDGRP = 0
  w_DESCUSER = space(20)
  w_GRPUSER = 0
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_DESBUN = space(40)
  w_AZFLBUNI = space(1)
  w_RAGAZI = space(40)
  w_ZSSERLOC = space(1)
  o_ZSSERLOC = space(1)
  w_ZMAINSER = space(254)
  w_ZSRVMNG = space(254)
  w_STARTUP = .F.
  w_ZSLAUNCH = .F.
  w_ZSCREASE = .F.
  w_ZSCREAIC = .F.
  w_ZSDEVMAP = space(254)
  o_ZSDEVMAP = space(254)
  w_TIPOCONF = space(3)
  o_TIPOCONF = space(3)
  w_FLHOTKEY = .F.
  o_FLHOTKEY = .F.
  w_FLSUBFOLDER = space(1)
  w_PATHVBS = space(254)
  w_ZSRVFLAZI = space(1)
  o_ZSRVFLAZI = space(1)
  w_ZSRVAZI = space(5)
  o_ZSRVAZI = space(5)
  w_ZSRVNAME = space(254)
  w_ZSRVINI = space(254)
  w_ZSRVMTX = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_ksaPag1","gsut_ksa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFILECNF_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='GROUPSJ'
    this.cWorkTables[2]='CPUSERS'
    this.cWorkTables[3]='CPUSRGRP'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='BUSIUNIT'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSUT1BSA(this,"CI")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPO=space(1)
      .w_FILECNF=space(254)
      .w_CODAZI=space(5)
      .w_USERIN=0
      .w_PWDUSER=space(50)
      .w_CODBUN=space(3)
      .w_CODESE=space(4)
      .w_PATHEXE=space(254)
      .w_PARACSIL=space(0)
      .w_FLASCRYP=space(1)
      .w_FLWNDSTA=0
      .w_COMMENTO=space(100)
      .w_ICONAME=space(50)
      .w_FLHKSPEC=0
      .w_FUNC__HK=space(3)
      .w_CHAR__HK=space(1)
      .w_SCHEDGRP=0
      .w_DESCUSER=space(20)
      .w_GRPUSER=0
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_DESBUN=space(40)
      .w_AZFLBUNI=space(1)
      .w_RAGAZI=space(40)
      .w_ZSSERLOC=space(1)
      .w_ZMAINSER=space(254)
      .w_ZSRVMNG=space(254)
      .w_STARTUP=.f.
      .w_ZSLAUNCH=.f.
      .w_ZSCREASE=.f.
      .w_ZSCREAIC=.f.
      .w_ZSDEVMAP=space(254)
      .w_TIPOCONF=space(3)
      .w_FLHOTKEY=.f.
      .w_FLSUBFOLDER=space(1)
      .w_PATHVBS=space(254)
      .w_ZSRVFLAZI=space(1)
      .w_ZSRVAZI=space(5)
      .w_ZSRVNAME=space(254)
      .w_ZSRVINI=space(254)
      .w_ZSRVMTX=space(254)
        .w_TIPO = This.oParentObject
        .w_FILECNF = IIF(EMPTY(NVL(i_cFileCNF," ")),'Selezionare cp3start.cnf',i_cFileCNF)
      .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .w_CODAZI = IIF(!IsAlt(), i_CODAZI, '')
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI))
          .link_1_5('Full')
        endif
        .w_USERIN = IIF(.w_TIPO<>'J', i_codute, 0)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_USERIN))
          .link_1_6('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_CODBUN = g_CODBUN
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODBUN))
          .link_1_8('Full')
        endif
        .w_CODESE = IIF(!IsAlt(), g_CODESE, '')
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODESE))
          .link_1_9('Full')
        endif
        .w_PATHEXE = SYS(5)+SYS(2003)
          .DoRTCalc(9,9,.f.)
        .w_FLASCRYP = 'N'
        .w_FLWNDSTA = 3
        .w_COMMENTO = ICASE(.w_TIPO='C', "Collegamento per telefonate in ingresso", .w_TIPO="S", "Collegamento per gestione Send to", "")
        .w_ICONAME = IIF(!InList(.w_TIPO,'J','R'), ICASE(isAhr(), 'Ad hoc Revolution', isAhe(), 'Ad hoc Enterprise', isAlt(), 'Alteregotop', 'AHPA') + " "+.w_CODAZI+iif(.w_tipo='M', " accesso mobile", ""), iCase(isAhr(),'AHR',isAhe(),'AHE',isAlt(),'ALTE','AHPA')+iif(.w_TIPO='J', ' accesso silente schedulatore', ' accesso silente esecutore processi'))
        .w_FLHKSPEC = IIF(!.w_FLHOTKEY,0, IIF(.w_FLHKSPEC=0, IIF(.w_FUNC__HK='C', 4, 0), IIF(.w_FUNC__HK='C' and .w_FLHKSPEC<4,4,.w_FLHKSPEC) )  )
        .w_FUNC__HK = 'C'
        .w_CHAR__HK = IIF(.w_FUNC__HK<>'C',SPACE(1), .w_CHAR__HK)
          .DoRTCalc(17,24,.f.)
        .w_ZSSERLOC = 'L'
        .w_ZMAINSER = IIF(.w_ZSSERLOC='L',SUBSTR(SYS(5)+SYS(2003),1,RAT('\',SYS(5)+SYS(2003),1)),ADDBS(Alltrim(.w_ZSDEVMAP))+SUBSTR(SYS(2003),2,RAT('\',SYS(2003),1)-1))+'ZSrvMng\ZMainService.exe'
        .w_ZSRVMNG = SUBSTR(SYS(5)+SYS(2003),1,RAT('\',SYS(5)+SYS(2003),1))+'ZSrvMng\ZSrvMng.exe'
        .w_STARTUP = .T.
        .w_ZSLAUNCH = .T.
        .w_ZSCREASE = .T.
        .w_ZSCREAIC = .T.
        .w_ZSDEVMAP = '\\'
        .w_TIPOCONF = 'ICO'
        .w_FLHOTKEY = .F.
        .w_FLSUBFOLDER = 'N'
        .w_PATHVBS = DESKPATH()
        .w_ZSRVFLAZI = IIF(Empty(.w_ZSRVAZI), 'N', 'S')
        .w_ZSRVAZI = IIF(.w_ZSRVFLAZI='S', i_codazi, .w_ZSRVAZI)
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_ZSRVAZI))
          .link_1_67('Full')
        endif
        .w_ZSRVNAME = iCase(.w_TIPO='J',"Zucchetti Scheduler Service ["+Tran(.w_USERIN)+"]", .w_TIPO='R' And .w_ZSRVFLAZI='S',"Zucchetti Syncronize Service ["+Alltrim(.w_ZSRVAZI)+"]", .w_TIPO='R' And .w_ZSRVFLAZI<>'S',"Zucchetti Syncronize Service", "")
        .w_ZSRVINI = ForceExt(iCase(.w_TIPO='J','scheduler_'+Tran(.w_USERIN), .w_TIPO='R','syncro'+iif(.w_ZSRVFLAZI='S', '_'+Alltrim(.w_ZSRVAZI),''),''),'ini')
        .w_ZSRVMTX = ForceExt(iCase(.w_TIPO='J','scheduler_mutex_'+Tran(.w_USERIN), .w_TIPO='R','syncro_mutex'+iif(.w_ZSRVFLAZI='S', '_'+Alltrim(.w_ZSRVAZI),''),''),'log')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .DoRTCalc(1,12,.t.)
        if .o_CODAZI<>.w_CODAZI
            .w_ICONAME = IIF(!InList(.w_TIPO,'J','R'), ICASE(isAhr(), 'Ad hoc Revolution', isAhe(), 'Ad hoc Enterprise', isAlt(), 'Alteregotop', 'AHPA') + " "+.w_CODAZI+iif(.w_tipo='M', " accesso mobile", ""), iCase(isAhr(),'AHR',isAhe(),'AHE',isAlt(),'ALTE','AHPA')+iif(.w_TIPO='J', ' accesso silente schedulatore', ' accesso silente esecutore processi'))
        endif
        if .o_CHAR__HK<>.w_CHAR__HK.or. .o_FLHOTKEY<>.w_FLHOTKEY.or. .o_FUNC__HK<>.w_FUNC__HK.or. .o_FLHKSPEC<>.w_FLHKSPEC
            .w_FLHKSPEC = IIF(!.w_FLHOTKEY,0, IIF(.w_FLHKSPEC=0, IIF(.w_FUNC__HK='C', 4, 0), IIF(.w_FUNC__HK='C' and .w_FLHKSPEC<4,4,.w_FLHKSPEC) )  )
        endif
        .DoRTCalc(15,15,.t.)
        if .o_FUNC__HK<>.w_FUNC__HK
            .w_CHAR__HK = IIF(.w_FUNC__HK<>'C',SPACE(1), .w_CHAR__HK)
        endif
        if .o_CODAZI<>.w_CODAZI.or. .o_USERIN<>.w_USERIN.or. .o_PWDUSER<>.w_PWDUSER.or. .o_CODBUN<>.w_CODBUN.or. .o_CODESE<>.w_CODESE.or. .o_FLASCRYP<>.w_FLASCRYP
          .Calculate_QTKCHLMHAO()
        endif
        .DoRTCalc(17,25,.t.)
        if .o_ZSSERLOC<>.w_ZSSERLOC.or. .o_ZSDEVMAP<>.w_ZSDEVMAP
            .w_ZMAINSER = IIF(.w_ZSSERLOC='L',SUBSTR(SYS(5)+SYS(2003),1,RAT('\',SYS(5)+SYS(2003),1)),ADDBS(Alltrim(.w_ZSDEVMAP))+SUBSTR(SYS(2003),2,RAT('\',SYS(2003),1)-1))+'ZSrvMng\ZMainService.exe'
        endif
        if .o_CODAZI<>.w_CODAZI
          .Calculate_UHMCCASSYY()
        endif
        if .o_USERIN<>.w_USERIN
          .Calculate_FUFTYFSLJL()
        endif
        .DoRTCalc(27,36,.t.)
        if .o_ZSRVAZI<>.w_ZSRVAZI
            .w_ZSRVFLAZI = IIF(Empty(.w_ZSRVAZI), 'N', 'S')
        endif
        if .o_ZSRVFLAZI<>.w_ZSRVFLAZI
            .w_ZSRVAZI = IIF(.w_ZSRVFLAZI='S', i_codazi, .w_ZSRVAZI)
          .link_1_67('Full')
        endif
        if .o_USERIN<>.w_USERIN.or. .o_ZSRVFLAZI<>.w_ZSRVFLAZI.or. .o_ZSRVAZI<>.w_ZSRVAZI
            .w_ZSRVNAME = iCase(.w_TIPO='J',"Zucchetti Scheduler Service ["+Tran(.w_USERIN)+"]", .w_TIPO='R' And .w_ZSRVFLAZI='S',"Zucchetti Syncronize Service ["+Alltrim(.w_ZSRVAZI)+"]", .w_TIPO='R' And .w_ZSRVFLAZI<>'S',"Zucchetti Syncronize Service", "")
        endif
        if .o_USERIN<>.w_USERIN.or. .o_ZSRVAZI<>.w_ZSRVAZI.or. .o_ZSRVFLAZI<>.w_ZSRVFLAZI
            .w_ZSRVINI = ForceExt(iCase(.w_TIPO='J','scheduler_'+Tran(.w_USERIN), .w_TIPO='R','syncro'+iif(.w_ZSRVFLAZI='S', '_'+Alltrim(.w_ZSRVAZI),''),''),'ini')
        endif
        if .o_USERIN<>.w_USERIN.or. .o_ZSRVAZI<>.w_ZSRVAZI.or. .o_ZSRVFLAZI<>.w_ZSRVFLAZI
            .w_ZSRVMTX = ForceExt(iCase(.w_TIPO='J','scheduler_mutex_'+Tran(.w_USERIN), .w_TIPO='R','syncro_mutex'+iif(.w_ZSRVFLAZI='S', '_'+Alltrim(.w_ZSRVAZI),''),''),'log')
        endif
        if .o_TIPOCONF<>.w_TIPOCONF
          .Calculate_MEUYBZBQDD()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
  return

  proc Calculate_WZSYORECGG()
    with this
          * --- Cambio caption
          .cComment = ICASE(.w_TIPO='S', Ah_MsgFormat("Crea collegamento invia a"), .w_TIPO='C', Ah_MsgFormat("Crea collegamento telefonate in ingresso"), .w_TIPO='M', Ah_MsgFormat("Crea collegamento accesso silente mobile"), .w_TIPO='R', Ah_MsgFormat("Configurazione esecutore processi"), This.Caption)
          .Caption = Thisform.cComment
    endwith
  endproc
  proc Calculate_JCLZCJRVPB()
    with this
          * --- Avvio maschera gsut1bsa('CM')
          gsut1bsa(this;
              ,'CM';
             )
    endwith
  endproc
  proc Calculate_QTKCHLMHAO()
    with this
          * --- Calcolo parametro accesso silente gsut1bsa ('CP')
          gsut1bsa(this;
              ,'CP';
             )
    endwith
  endproc
  proc Calculate_UHMCCASSYY()
    with this
          * --- Cambio azienda gsut1bsa ('AC')
          gsut1bsa(this;
              ,'AC';
             )
    endwith
  endproc
  proc Calculate_FUFTYFSLJL()
    with this
          * --- Cambio utente gsut1bsa ('uc')
          gsut1bsa(this;
              ,'UC';
             )
    endwith
  endproc
  proc Calculate_MEUYBZBQDD()
    with this
          * --- Controllo configurazione
          gsut1bsa(this;
              ,'CC';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODBUN_1_8.enabled = this.oPgFrm.Page1.oPag.oCODBUN_1_8.mCond()
    this.oPgFrm.Page1.oPag.oFLASCRYP_1_12.enabled = this.oPgFrm.Page1.oPag.oFLASCRYP_1_12.mCond()
    this.oPgFrm.Page1.oPag.oFLHKSPEC_1_16.enabled = this.oPgFrm.Page1.oPag.oFLHKSPEC_1_16.mCond()
    this.oPgFrm.Page1.oPag.oFUNC__HK_1_17.enabled = this.oPgFrm.Page1.oPag.oFUNC__HK_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCHAR__HK_1_18.enabled = this.oPgFrm.Page1.oPag.oCHAR__HK_1_18.mCond()
    this.oPgFrm.Page1.oPag.oZSDEVMAP_1_55.enabled = this.oPgFrm.Page1.oPag.oZSDEVMAP_1_55.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODBUN_1_8.visible=!this.oPgFrm.Page1.oPag.oCODBUN_1_8.mHide()
    this.oPgFrm.Page1.oPag.oFLWNDSTA_1_13.visible=!this.oPgFrm.Page1.oPag.oFLWNDSTA_1_13.mHide()
    this.oPgFrm.Page1.oPag.oCOMMENTO_1_14.visible=!this.oPgFrm.Page1.oPag.oCOMMENTO_1_14.mHide()
    this.oPgFrm.Page1.oPag.oICONAME_1_15.visible=!this.oPgFrm.Page1.oPag.oICONAME_1_15.mHide()
    this.oPgFrm.Page1.oPag.oFLHKSPEC_1_16.visible=!this.oPgFrm.Page1.oPag.oFLHKSPEC_1_16.mHide()
    this.oPgFrm.Page1.oPag.oFUNC__HK_1_17.visible=!this.oPgFrm.Page1.oPag.oFUNC__HK_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCHAR__HK_1_18.visible=!this.oPgFrm.Page1.oPag.oCHAR__HK_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oDESBUN_1_41.visible=!this.oPgFrm.Page1.oPag.oDESBUN_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oZSSERLOC_1_46.visible=!this.oPgFrm.Page1.oPag.oZSSERLOC_1_46.mHide()
    this.oPgFrm.Page1.oPag.oZMAINSER_1_47.visible=!this.oPgFrm.Page1.oPag.oZMAINSER_1_47.mHide()
    this.oPgFrm.Page1.oPag.oZSRVMNG_1_48.visible=!this.oPgFrm.Page1.oPag.oZSRVMNG_1_48.mHide()
    this.oPgFrm.Page1.oPag.oSTARTUP_1_49.visible=!this.oPgFrm.Page1.oPag.oSTARTUP_1_49.mHide()
    this.oPgFrm.Page1.oPag.oZSLAUNCH_1_50.visible=!this.oPgFrm.Page1.oPag.oZSLAUNCH_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oZSCREASE_1_52.visible=!this.oPgFrm.Page1.oPag.oZSCREASE_1_52.mHide()
    this.oPgFrm.Page1.oPag.oZSCREAIC_1_53.visible=!this.oPgFrm.Page1.oPag.oZSCREAIC_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oZSDEVMAP_1_55.visible=!this.oPgFrm.Page1.oPag.oZSDEVMAP_1_55.mHide()
    this.oPgFrm.Page1.oPag.oTIPOCONF_1_56.visible=!this.oPgFrm.Page1.oPag.oTIPOCONF_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oFLHOTKEY_1_59.visible=!this.oPgFrm.Page1.oPag.oFLHOTKEY_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oFLSUBFOLDER_1_61.visible=!this.oPgFrm.Page1.oPag.oFLSUBFOLDER_1_61.mHide()
    this.oPgFrm.Page1.oPag.oPATHVBS_1_62.visible=!this.oPgFrm.Page1.oPag.oPATHVBS_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oZSRVFLAZI_1_66.visible=!this.oPgFrm.Page1.oPag.oZSRVFLAZI_1_66.mHide()
    this.oPgFrm.Page1.oPag.oZSRVAZI_1_67.visible=!this.oPgFrm.Page1.oPag.oZSRVAZI_1_67.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_WZSYORECGG()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_JCLZCJRVPB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Aggiorna") or lower(cEvent)==lower("Blank")
          .Calculate_QTKCHLMHAO()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_CODAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_CODAZI))
          select AZCODAZI,AZRAGAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAZI)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAZI) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oCODAZI_1_5'),i_cWhere,'',"Azienda",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=USERIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_USERIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_USERIN);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_USERIN)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_USERIN) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oUSERIN_1_6'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_USERIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_USERIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_USERIN)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_USERIN = NVL(_Link_.CODE,0)
      this.w_DESCUSER = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_USERIN = 0
      endif
      this.w_DESCUSER = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_USERIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_CODAZI;
                     ,'BUCODICE',trim(this.w_CODBUN))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODBUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODBUN_1_8'),i_cWhere,'',"Business Unit",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_CODAZI;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_DESBUN = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(3)
      endif
      this.w_DESBUN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_9'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZSRVAZI
  func Link_1_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZSRVAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_ZSRVAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_ZSRVAZI))
          select AZCODAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZSRVAZI)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ZSRVAZI) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oZSRVAZI_1_67'),i_cWhere,'',"Azienda",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZSRVAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_ZSRVAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_ZSRVAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZSRVAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ZSRVAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZSRVAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFILECNF_1_3.value==this.w_FILECNF)
      this.oPgFrm.Page1.oPag.oFILECNF_1_3.value=this.w_FILECNF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODAZI_1_5.value==this.w_CODAZI)
      this.oPgFrm.Page1.oPag.oCODAZI_1_5.value=this.w_CODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oUSERIN_1_6.value==this.w_USERIN)
      this.oPgFrm.Page1.oPag.oUSERIN_1_6.value=this.w_USERIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPWDUSER_1_7.value==this.w_PWDUSER)
      this.oPgFrm.Page1.oPag.oPWDUSER_1_7.value=this.w_PWDUSER
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBUN_1_8.value==this.w_CODBUN)
      this.oPgFrm.Page1.oPag.oCODBUN_1_8.value=this.w_CODBUN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_9.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_9.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oPATHEXE_1_10.value==this.w_PATHEXE)
      this.oPgFrm.Page1.oPag.oPATHEXE_1_10.value=this.w_PATHEXE
    endif
    if not(this.oPgFrm.Page1.oPag.oPARACSIL_1_11.value==this.w_PARACSIL)
      this.oPgFrm.Page1.oPag.oPARACSIL_1_11.value=this.w_PARACSIL
    endif
    if not(this.oPgFrm.Page1.oPag.oFLASCRYP_1_12.RadioValue()==this.w_FLASCRYP)
      this.oPgFrm.Page1.oPag.oFLASCRYP_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLWNDSTA_1_13.RadioValue()==this.w_FLWNDSTA)
      this.oPgFrm.Page1.oPag.oFLWNDSTA_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMMENTO_1_14.value==this.w_COMMENTO)
      this.oPgFrm.Page1.oPag.oCOMMENTO_1_14.value=this.w_COMMENTO
    endif
    if not(this.oPgFrm.Page1.oPag.oICONAME_1_15.value==this.w_ICONAME)
      this.oPgFrm.Page1.oPag.oICONAME_1_15.value=this.w_ICONAME
    endif
    if not(this.oPgFrm.Page1.oPag.oFLHKSPEC_1_16.RadioValue()==this.w_FLHKSPEC)
      this.oPgFrm.Page1.oPag.oFLHKSPEC_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFUNC__HK_1_17.RadioValue()==this.w_FUNC__HK)
      this.oPgFrm.Page1.oPag.oFUNC__HK_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHAR__HK_1_18.value==this.w_CHAR__HK)
      this.oPgFrm.Page1.oPag.oCHAR__HK_1_18.value=this.w_CHAR__HK
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCUSER_1_23.value==this.w_DESCUSER)
      this.oPgFrm.Page1.oPag.oDESCUSER_1_23.value=this.w_DESCUSER
    endif
    if not(this.oPgFrm.Page1.oPag.oINIESE_1_36.value==this.w_INIESE)
      this.oPgFrm.Page1.oPag.oINIESE_1_36.value=this.w_INIESE
    endif
    if not(this.oPgFrm.Page1.oPag.oFINESE_1_37.value==this.w_FINESE)
      this.oPgFrm.Page1.oPag.oFINESE_1_37.value=this.w_FINESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBUN_1_41.value==this.w_DESBUN)
      this.oPgFrm.Page1.oPag.oDESBUN_1_41.value=this.w_DESBUN
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_44.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_44.value=this.w_RAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oZSSERLOC_1_46.RadioValue()==this.w_ZSSERLOC)
      this.oPgFrm.Page1.oPag.oZSSERLOC_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZMAINSER_1_47.value==this.w_ZMAINSER)
      this.oPgFrm.Page1.oPag.oZMAINSER_1_47.value=this.w_ZMAINSER
    endif
    if not(this.oPgFrm.Page1.oPag.oZSRVMNG_1_48.value==this.w_ZSRVMNG)
      this.oPgFrm.Page1.oPag.oZSRVMNG_1_48.value=this.w_ZSRVMNG
    endif
    if not(this.oPgFrm.Page1.oPag.oSTARTUP_1_49.RadioValue()==this.w_STARTUP)
      this.oPgFrm.Page1.oPag.oSTARTUP_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZSLAUNCH_1_50.RadioValue()==this.w_ZSLAUNCH)
      this.oPgFrm.Page1.oPag.oZSLAUNCH_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZSCREASE_1_52.RadioValue()==this.w_ZSCREASE)
      this.oPgFrm.Page1.oPag.oZSCREASE_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZSCREAIC_1_53.RadioValue()==this.w_ZSCREAIC)
      this.oPgFrm.Page1.oPag.oZSCREAIC_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZSDEVMAP_1_55.value==this.w_ZSDEVMAP)
      this.oPgFrm.Page1.oPag.oZSDEVMAP_1_55.value=this.w_ZSDEVMAP
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCONF_1_56.RadioValue()==this.w_TIPOCONF)
      this.oPgFrm.Page1.oPag.oTIPOCONF_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLHOTKEY_1_59.RadioValue()==this.w_FLHOTKEY)
      this.oPgFrm.Page1.oPag.oFLHOTKEY_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSUBFOLDER_1_61.RadioValue()==this.w_FLSUBFOLDER)
      this.oPgFrm.Page1.oPag.oFLSUBFOLDER_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATHVBS_1_62.value==this.w_PATHVBS)
      this.oPgFrm.Page1.oPag.oPATHVBS_1_62.value=this.w_PATHVBS
    endif
    if not(this.oPgFrm.Page1.oPag.oZSRVFLAZI_1_66.RadioValue()==this.w_ZSRVFLAZI)
      this.oPgFrm.Page1.oPag.oZSRVFLAZI_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZSRVAZI_1_67.value==this.w_ZSRVAZI)
      this.oPgFrm.Page1.oPag.oZSRVAZI_1_67.value=this.w_ZSRVAZI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FILECNF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFILECNF_1_3.SetFocus()
            i_bnoObbl = !empty(.w_FILECNF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_USERIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUSERIN_1_6.SetFocus()
            i_bnoObbl = !empty(.w_USERIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ICONAME))  and not(.w_TIPOCONF='INI')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oICONAME_1_15.SetFocus()
            i_bnoObbl = !empty(.w_ICONAME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_FUNC__HK='C' and .w_FLHKSPEC>=4) OR .w_FUNC__HK<>'C')  and not(.w_TIPOCONF='INI' Or !InList(.w_TIPO,'J','R'))  and (.w_FLHOTKEY)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFLHKSPEC_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Con la selezione del carattere come tasto di attivazione non � possibile impostare come tasto speciale: nessuno, CTRL, ALT o MAIUSC")
          case   not(UPPER(.w_CHAR__HK) $ 'QWERTYUIOPASDFGHJKLZXCVBNM1234567890')  and not(.w_TIPOCONF='INI' Or !InList(.w_TIPO,'J','R'))  and (.w_FLHOTKEY AND .w_FUNC__HK='C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHAR__HK_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Carattere specificato non valido. Sono validi i caratteri dalla A alla Z e da 0 a 9")
          case   (empty(.w_ZMAINSER))  and not(.w_TIPOCONF='ICO' Or .w_TIPO='C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZMAINSER_1_47.SetFocus()
            i_bnoObbl = !empty(.w_ZMAINSER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ZSRVMNG))  and not(.w_TIPOCONF='ICO' Or .w_TIPO='C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZSRVMNG_1_48.SetFocus()
            i_bnoObbl = !empty(.w_ZSRVMNG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ZSDEVMAP))  and not(.w_TIPOCONF='ICO')  and (.w_ZSSERLOC='R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZSDEVMAP_1_55.SetFocus()
            i_bnoObbl = !empty(.w_ZSDEVMAP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FILECNF = this.w_FILECNF
    this.o_CODAZI = this.w_CODAZI
    this.o_USERIN = this.w_USERIN
    this.o_PWDUSER = this.w_PWDUSER
    this.o_CODBUN = this.w_CODBUN
    this.o_CODESE = this.w_CODESE
    this.o_FLASCRYP = this.w_FLASCRYP
    this.o_FLHKSPEC = this.w_FLHKSPEC
    this.o_FUNC__HK = this.w_FUNC__HK
    this.o_CHAR__HK = this.w_CHAR__HK
    this.o_ZSSERLOC = this.w_ZSSERLOC
    this.o_ZSDEVMAP = this.w_ZSDEVMAP
    this.o_TIPOCONF = this.w_TIPOCONF
    this.o_FLHOTKEY = this.w_FLHOTKEY
    this.o_ZSRVFLAZI = this.w_ZSRVFLAZI
    this.o_ZSRVAZI = this.w_ZSRVAZI
    return

enddefine

* --- Define pages as container
define class tgsut_ksaPag1 as StdContainer
  Width  = 604
  height = 474
  stdWidth  = 604
  stdheight = 474
  resizeXpos=267
  resizeYpos=227
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFILECNF_1_3 as StdField with uid="FLYJRUHLHW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FILECNF", cQueryName = "FILECNF",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "File CNF da utilizzare per l'accesso alla procedura",;
    HelpContextID = 210815574,;
   bGlobalFont=.t.,;
    Height=21, Width=381, Left=183, Top=38, InputMask=replicate('X',254)


  add object oObj_1_4 as cp_askfile with uid="GIGRPVJRZL",left=567, top=38, width=21,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_FILECNF",cExt="CNF",;
    nPag=1;
    , ToolTipText = "Premere per selezionare un file di configurazione differente";
    , HelpContextID = 98972202

  add object oCODAZI_1_5 as StdField with uid="CGTITBXFPA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODAZI", cQueryName = "CODAZI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Azienda/Studio con cui accedere alla procedura",;
    HelpContextID = 117682138,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=183, Top=62, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_CODAZI"

  func oCODAZI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
      if .not. empty(.w_CODBUN)
        bRes2=.link_1_8('Full')
      endif
      if .not. empty(.w_CODESE)
        bRes2=.link_1_9('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODAZI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAZI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oCODAZI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Azienda",'',this.parent.oContained
  endproc

  add object oUSERIN_1_6 as StdField with uid="HQHUKQTUBM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_USERIN", cQueryName = "USERIN",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente con cui accedere alla procedura",;
    HelpContextID = 50502330,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=183, Top=86, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_USERIN"

  func oUSERIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oUSERIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUSERIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oUSERIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oPWDUSER_1_7 as StdField with uid="PRTZGXITTI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PWDUSER", cQueryName = "PWDUSER",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Password dell'utente selezionato",;
    HelpContextID = 190818058,;
   bGlobalFont=.t.,;
    Height=21, Width=404, Left=183, Top=111, InputMask=replicate('X',50), PasswordChar='*'

  add object oCODBUN_1_8 as StdField with uid="UYDRNKVSOQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODBUN", cQueryName = "CODBUN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della Business Unit con cui accedere alla procedura",;
    HelpContextID = 38973402,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=183, Top=136, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", oKey_1_1="BUCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODBUN"

  func oCODBUN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZFLBUNI $ "ET")
    endwith
   endif
  endfunc

  func oCODBUN_1_8.mHide()
    with this.Parent.oContained
      return (isAhr() or isAlt())
    endwith
  endfunc

  func oCODBUN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBUN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBUN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODBUN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Business Unit",'',this.parent.oContained
  endproc

  add object oCODESE_1_9 as StdField with uid="GQYPCJSLPK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice dell'esercizio con cui accedere alla procedura",;
    HelpContextID = 191868890,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=183, Top=162, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oPATHEXE_1_10 as StdField with uid="SMZIQXESOZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PATHEXE", cQueryName = "PATHEXE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 112476918,;
   bGlobalFont=.t.,;
    Height=21, Width=405, Left=183, Top=188, InputMask=replicate('X',254)

  add object oPARACSIL_1_11 as StdMemo with uid="UGTTREMLUM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PARACSIL", cQueryName = "PARACSIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 26026818,;
   bGlobalFont=.t.,;
    Height=47, Width=405, Left=183, Top=213

  add object oFLASCRYP_1_12 as StdCheck with uid="WZKREJUMXM",rtseq=10,rtrep=.f.,left=183, top=265, caption="Stringa di accesso criptata",;
    ToolTipText = "Se attivo cripta la stringa di connessione silente",;
    HelpContextID = 10362278,;
    cFormVar="w_FLASCRYP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLASCRYP_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLASCRYP_1_12.GetRadio()
    this.Parent.oContained.w_FLASCRYP = this.RadioValue()
    return .t.
  endfunc

  func oFLASCRYP_1_12.SetRadio()
    this.Parent.oContained.w_FLASCRYP=trim(this.Parent.oContained.w_FLASCRYP)
    this.value = ;
      iif(this.Parent.oContained.w_FLASCRYP=='S',1,;
      0)
  endfunc

  func oFLASCRYP_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_PARACSIL))
    endwith
   endif
  endfunc


  add object oFLWNDSTA_1_13 as StdCombo with uid="UREQQGEXKM",rtseq=11,rtrep=.f.,left=183,top=287,width=171,height=21;
    , ToolTipText = "Modalit� di apertura della procedura";
    , HelpContextID = 27950487;
    , cFormVar="w_FLWNDSTA",RowSource=""+"Ridotta a icona,"+"Finestra normale,"+"Ingrandita", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLWNDSTA_1_13.RadioValue()
    return(iif(this.value =1,7,;
    iif(this.value =2,1,;
    iif(this.value =3,3,;
    0))))
  endfunc
  func oFLWNDSTA_1_13.GetRadio()
    this.Parent.oContained.w_FLWNDSTA = this.RadioValue()
    return .t.
  endfunc

  func oFLWNDSTA_1_13.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLWNDSTA==7,1,;
      iif(this.Parent.oContained.w_FLWNDSTA==1,2,;
      iif(this.Parent.oContained.w_FLWNDSTA==3,3,;
      0)))
  endfunc

  func oFLWNDSTA_1_13.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI' Or !InList(.w_TIPO,'J','R'))
    endwith
  endfunc

  add object oCOMMENTO_1_14 as StdField with uid="FITZEFJAFI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COMMENTO", cQueryName = "COMMENTO",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Commento da visualizzare nell'icona di collegamento",;
    HelpContextID = 213442677,;
   bGlobalFont=.t.,;
    Height=21, Width=405, Left=183, Top=315, InputMask=replicate('X',100)

  func oCOMMENTO_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI')
    endwith
  endfunc

  add object oICONAME_1_15 as StdField with uid="MBKJOYCEPD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ICONAME", cQueryName = "ICONAME",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome dell'icona di collegamento",;
    HelpContextID = 192541830,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=183, Top=338, InputMask=replicate('X',50)

  func oICONAME_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI')
    endwith
  endfunc


  add object oFLHKSPEC_1_16 as StdCombo with uid="OYKVREBLOF",value=1,rtseq=14,rtrep=.f.,left=182,top=380,width=171,height=21;
    , ToolTipText = "Tasti speciali da utilizzare";
    , HelpContextID = 261524889;
    , cFormVar="w_FLHKSPEC",RowSource=""+"Nessuno,"+"CTRL,"+"ALT,"+"MAIUSCOLO,"+"CTRL+ALT,"+"CTRL+MAIUSCOLO,"+"ALT+MAIUSCOLO,"+"CTRL+ALT+MAIUSCOLO", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Con la selezione del carattere come tasto di attivazione non � possibile impostare come tasto speciale: nessuno, CTRL, ALT o MAIUSC";
  , bGlobalFont=.t.


  func oFLHKSPEC_1_16.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,4,;
    iif(this.value =6,5,;
    iif(this.value =7,6,;
    iif(this.value =8,7,;
    0)))))))))
  endfunc
  func oFLHKSPEC_1_16.GetRadio()
    this.Parent.oContained.w_FLHKSPEC = this.RadioValue()
    return .t.
  endfunc

  func oFLHKSPEC_1_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLHKSPEC==0,1,;
      iif(this.Parent.oContained.w_FLHKSPEC==1,2,;
      iif(this.Parent.oContained.w_FLHKSPEC==2,3,;
      iif(this.Parent.oContained.w_FLHKSPEC==3,4,;
      iif(this.Parent.oContained.w_FLHKSPEC==4,5,;
      iif(this.Parent.oContained.w_FLHKSPEC==5,6,;
      iif(this.Parent.oContained.w_FLHKSPEC==6,7,;
      iif(this.Parent.oContained.w_FLHKSPEC==7,8,;
      0))))))))
  endfunc

  func oFLHKSPEC_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLHOTKEY)
    endwith
   endif
  endfunc

  func oFLHKSPEC_1_16.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI' Or !InList(.w_TIPO,'J','R'))
    endwith
  endfunc

  func oFLHKSPEC_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_FUNC__HK='C' and .w_FLHKSPEC>=4) OR .w_FUNC__HK<>'C')
    endwith
    return bRes
  endfunc


  add object oFUNC__HK_1_17 as StdCombo with uid="AGINZJNFFF",rtseq=15,rtrep=.f.,left=371,top=380,width=122,height=21;
    , ToolTipText = "Tasto di attivazione";
    , HelpContextID = 256833185;
    , cFormVar="w_FUNC__HK",RowSource=""+"Carattere,"+"F1,"+"F2,"+"F3,"+"F4,"+"F5,"+"F6,"+"F7,"+"F8,"+"F9,"+"F10,"+"F11,"+"F12", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFUNC__HK_1_17.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F1',;
    iif(this.value =3,'F2',;
    iif(this.value =4,'F3',;
    iif(this.value =5,'F4',;
    iif(this.value =6,'F5',;
    iif(this.value =7,'F6',;
    iif(this.value =8,'F7',;
    iif(this.value =9,'F8',;
    iif(this.value =10,'F9',;
    iif(this.value =11,'F10',;
    iif(this.value =12,'F11',;
    iif(this.value =13,'F12',;
    space(3)))))))))))))))
  endfunc
  func oFUNC__HK_1_17.GetRadio()
    this.Parent.oContained.w_FUNC__HK = this.RadioValue()
    return .t.
  endfunc

  func oFUNC__HK_1_17.SetRadio()
    this.Parent.oContained.w_FUNC__HK=trim(this.Parent.oContained.w_FUNC__HK)
    this.value = ;
      iif(this.Parent.oContained.w_FUNC__HK=='C',1,;
      iif(this.Parent.oContained.w_FUNC__HK=='F1',2,;
      iif(this.Parent.oContained.w_FUNC__HK=='F2',3,;
      iif(this.Parent.oContained.w_FUNC__HK=='F3',4,;
      iif(this.Parent.oContained.w_FUNC__HK=='F4',5,;
      iif(this.Parent.oContained.w_FUNC__HK=='F5',6,;
      iif(this.Parent.oContained.w_FUNC__HK=='F6',7,;
      iif(this.Parent.oContained.w_FUNC__HK=='F7',8,;
      iif(this.Parent.oContained.w_FUNC__HK=='F8',9,;
      iif(this.Parent.oContained.w_FUNC__HK=='F9',10,;
      iif(this.Parent.oContained.w_FUNC__HK=='F10',11,;
      iif(this.Parent.oContained.w_FUNC__HK=='F11',12,;
      iif(this.Parent.oContained.w_FUNC__HK=='F12',13,;
      0)))))))))))))
  endfunc

  func oFUNC__HK_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLHOTKEY)
    endwith
   endif
  endfunc

  func oFUNC__HK_1_17.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI' Or !InList(.w_TIPO,'J','R'))
    endwith
  endfunc

  add object oCHAR__HK_1_18 as StdField with uid="GUSIWGQTDI",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CHAR__HK", cQueryName = "CHAR__HK",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    sErrorMsg = "Carattere specificato non valido. Sono validi i caratteri dalla A alla Z e da 0 a 9",;
    ToolTipText = "Carattere di attivazione",;
    HelpContextID = 257759601,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=498, Top=380, cSayPict='"N"', cGetPict='"N"', InputMask=replicate('X',1)

  func oCHAR__HK_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLHOTKEY AND .w_FUNC__HK='C')
    endwith
   endif
  endfunc

  func oCHAR__HK_1_18.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI' Or !InList(.w_TIPO,'J','R'))
    endwith
  endfunc

  func oCHAR__HK_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (UPPER(.w_CHAR__HK) $ 'QWERTYUIOPASDFGHJKLZXCVBNM1234567890')
    endwith
    return bRes
  endfunc


  add object oBtn_1_19 as StdButton with uid="MQUINSAGYU",left=500, top=425, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare la creazione dell'icona";
    , HelpContextID = 99173130;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSUT1BSA(this.Parent.oContained,"CI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_SCHEDGRP=.w_GRPUSER and NOT EMPTY(.w_FILECNF) AND NOT EMPTY(.w_ICONAME))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="QWCYUUECVM",left=551, top=425, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 99173130;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCUSER_1_23 as StdField with uid="RVJKDIEFIL",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCUSER", cQueryName = "DESCUSER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 45037192,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=250, Top=86, InputMask=replicate('X',20)

  add object oINIESE_1_36 as StdField with uid="VASHNRYVUJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_INIESE", cQueryName = "INIESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 191848570,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=312, Top=162

  add object oFINESE_1_37 as StdField with uid="HKJJBNCOLX",rtseq=21,rtrep=.f.,;
    cFormVar = "w_FINESE", cQueryName = "FINESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 191829418,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=458, Top=162

  add object oDESBUN_1_41 as StdField with uid="XOXLLGBCZL",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESBUN", cQueryName = "DESBUN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38914506,;
   bGlobalFont=.t.,;
    Height=21, Width=341, Left=247, Top=136, InputMask=replicate('X',40)

  func oDESBUN_1_41.mHide()
    with this.Parent.oContained
      return (isAhr() or isAlt())
    endwith
  endfunc

  add object oRAGAZI_1_44 as StdField with uid="COQWVBJNFO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 117673194,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=250, Top=62, InputMask=replicate('X',40)


  add object oZSSERLOC_1_46 as StdCombo with uid="OQLFIZWOGE",rtseq=25,rtrep=.f.,left=183,top=287,width=128,height=21;
    , ToolTipText = "Utilizzare il servizio di rete se l'installazione non � sulla macchina locale";
    , HelpContextID = 75414055;
    , cFormVar="w_ZSSERLOC",RowSource=""+"Servizio locale,"+"Servizio di rete", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oZSSERLOC_1_46.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oZSSERLOC_1_46.GetRadio()
    this.Parent.oContained.w_ZSSERLOC = this.RadioValue()
    return .t.
  endfunc

  func oZSSERLOC_1_46.SetRadio()
    this.Parent.oContained.w_ZSSERLOC=trim(this.Parent.oContained.w_ZSSERLOC)
    this.value = ;
      iif(this.Parent.oContained.w_ZSSERLOC=='L',1,;
      iif(this.Parent.oContained.w_ZSSERLOC=='R',2,;
      0))
  endfunc

  func oZSSERLOC_1_46.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='ICO')
    endwith
  endfunc

  add object oZMAINSER_1_47 as StdField with uid="DLMAYBLTSM",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ZMAINSER", cQueryName = "ZMAINSER",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Se l'installazione risulta mappata su un'unit� di rete, inserire un percorso di rete valido",;
    HelpContextID = 38019048,;
   bGlobalFont=.t.,;
    Height=21, Width=404, Left=183, Top=315, InputMask=replicate('X',254)

  func oZMAINSER_1_47.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='ICO' Or .w_TIPO='C')
    endwith
  endfunc

  add object oZSRVMNG_1_48 as StdField with uid="RSZSIJPDMU",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ZSRVMNG", cQueryName = "ZSRVMNG",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Programma necessario per l'avvio del servizio",;
    HelpContextID = 222442902,;
   bGlobalFont=.t.,;
    Height=21, Width=404, Left=183, Top=338, InputMask=replicate('X',254)

  func oZSRVMNG_1_48.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='ICO' Or .w_TIPO='C')
    endwith
  endfunc

  add object oSTARTUP_1_49 as StdCheck with uid="BCEJUMLKPI",rtseq=28,rtrep=.f.,left=183, top=361, caption="Esegui Zucchetti Service Manager all'avvio",;
    ToolTipText = "Permette di avviare lo Zucchetti Service Manager alla login del sistema",;
    HelpContextID = 189979098,;
    cFormVar="w_STARTUP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTARTUP_1_49.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oSTARTUP_1_49.GetRadio()
    this.Parent.oContained.w_STARTUP = this.RadioValue()
    return .t.
  endfunc

  func oSTARTUP_1_49.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_STARTUP==.T.,1,;
      0)
  endfunc

  func oSTARTUP_1_49.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='ICO')
    endwith
  endfunc

  add object oZSLAUNCH_1_50 as StdCheck with uid="VOHQVYCCTH",rtseq=29,rtrep=.f.,left=183, top=380, caption="Avvia subito Zucchetti Service Manager",;
    ToolTipText = "Al salvataggio avvia Zucchetti Service Manager",;
    HelpContextID = 229430750,;
    cFormVar="w_ZSLAUNCH", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oZSLAUNCH_1_50.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oZSLAUNCH_1_50.GetRadio()
    this.Parent.oContained.w_ZSLAUNCH = this.RadioValue()
    return .t.
  endfunc

  func oZSLAUNCH_1_50.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ZSLAUNCH==.T.,1,;
      0)
  endfunc

  func oZSLAUNCH_1_50.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='ICO')
    endwith
  endfunc

  add object oZSCREASE_1_52 as StdCheck with uid="ZCVWJCHRGH",rtseq=30,rtrep=.f.,left=375, top=400, caption="Crea il servizio",;
    ToolTipText = "La creazione e cancellazione del servizio � controllabile anche da Zucchetti Service Manager",;
    HelpContextID = 4373029,;
    cFormVar="w_ZSCREASE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oZSCREASE_1_52.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oZSCREASE_1_52.GetRadio()
    this.Parent.oContained.w_ZSCREASE = this.RadioValue()
    return .t.
  endfunc

  func oZSCREASE_1_52.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ZSCREASE==.T.,1,;
      0)
  endfunc

  func oZSCREASE_1_52.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='ICO')
    endwith
  endfunc

  add object oZSCREAIC_1_53 as StdCheck with uid="TALQKVQRCX",rtseq=31,rtrep=.f.,left=183, top=400, caption="Crea icona sul Desktop",;
    ToolTipText = "Crea l'icona di Zucchetti Service Manager sul desktop",;
    HelpContextID = 264062425,;
    cFormVar="w_ZSCREAIC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oZSCREAIC_1_53.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oZSCREAIC_1_53.GetRadio()
    this.Parent.oContained.w_ZSCREAIC = this.RadioValue()
    return .t.
  endfunc

  func oZSCREAIC_1_53.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ZSCREAIC==.T.,1,;
      0)
  endfunc

  func oZSCREAIC_1_53.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='ICO')
    endwith
  endfunc

  add object oZSDEVMAP_1_55 as StdField with uid="QJAKPIYQSG",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ZSDEVMAP", cQueryName = "ZSDEVMAP",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indicare il percorso di rete per la connessione dell'unit� [Es: \\SERVER\Programmi\]",;
    HelpContextID = 54503962,;
   bGlobalFont=.t.,;
    Height=21, Width=196, Left=392, Top=286, InputMask=replicate('X',254)

  func oZSDEVMAP_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ZSSERLOC='R')
    endwith
   endif
  endfunc

  func oZSDEVMAP_1_55.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='ICO')
    endwith
  endfunc

  add object oTIPOCONF_1_56 as StdRadio with uid="PLYFEDZEYB",rtseq=33,rtrep=.f.,left=183, top=6, width=221,height=30;
    , ToolTipText = "Scelta configurazione";
    , cFormVar="w_TIPOCONF", ButtonCount=2, bObbl=.f., nPag=1;
    , FntName="Arial", FntSize=8, FntBold=.f., FntItalic=.t., FntUnderline=.f., FntStrikeThru=.f.

    proc oTIPOCONF_1_56.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Icona accesso silente"
      this.Buttons(1).HelpContextID = 40170628
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Servizio"
      this.Buttons(2).HelpContextID = 40170628
      this.Buttons(2).Top=14
      this.SetAll("Width",219)
      this.SetAll("Height",16)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Scelta configurazione")
      StdRadio::init()
    endproc

  func oTIPOCONF_1_56.RadioValue()
    return(iif(this.value =1,'ICO',;
    iif(this.value =2,'INI',;
    space(3))))
  endfunc
  func oTIPOCONF_1_56.GetRadio()
    this.Parent.oContained.w_TIPOCONF = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCONF_1_56.SetRadio()
    this.Parent.oContained.w_TIPOCONF=trim(this.Parent.oContained.w_TIPOCONF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCONF=='ICO',1,;
      iif(this.Parent.oContained.w_TIPOCONF=='INI',2,;
      0))
  endfunc

  func oTIPOCONF_1_56.mHide()
    with this.Parent.oContained
      return (!InList(.w_TIPO,'J','R'))
    endwith
  endfunc

  add object oFLHOTKEY_1_59 as StdCheck with uid="JAYQAOCTCB",rtseq=34,rtrep=.f.,left=183, top=361, caption="Abilita tasti di scelta rapida",;
    ToolTipText = "Se attivo permette di specificare la sequenza di tasti per l'apertura della procedura",;
    HelpContextID = 178949551,;
    cFormVar="w_FLHOTKEY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLHOTKEY_1_59.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLHOTKEY_1_59.GetRadio()
    this.Parent.oContained.w_FLHOTKEY = this.RadioValue()
    return .t.
  endfunc

  func oFLHOTKEY_1_59.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLHOTKEY==.T.,1,;
      0)
  endfunc

  func oFLHOTKEY_1_59.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI' Or !InList(.w_TIPO,'J','R'))
    endwith
  endfunc

  add object oFLSUBFOLDER_1_61 as StdCheck with uid="LBJCAEJQHE",rtseq=35,rtrep=.f.,left=375, top=265, caption="Inserisci nel sottomenu",;
    ToolTipText = "Se attivo inserisce il collegamento in un sottomenu della voce invia a... ",;
    HelpContextID = 191453470,;
    cFormVar="w_FLSUBFOLDER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSUBFOLDER_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLSUBFOLDER_1_61.GetRadio()
    this.Parent.oContained.w_FLSUBFOLDER = this.RadioValue()
    return .t.
  endfunc

  func oFLSUBFOLDER_1_61.SetRadio()
    this.Parent.oContained.w_FLSUBFOLDER=trim(this.Parent.oContained.w_FLSUBFOLDER)
    this.value = ;
      iif(this.Parent.oContained.w_FLSUBFOLDER=='S',1,;
      0)
  endfunc

  func oFLSUBFOLDER_1_61.mHide()
    with this.Parent.oContained
      return (.w_TIPO <> 'S' Or OS(3)='6')
    endwith
  endfunc

  add object oPATHVBS_1_62 as StdField with uid="TXOPPKGIRA",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PATHVBS", cQueryName = "PATHVBS",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path di destinazione da utilizzare per creare un collegamento con il gestionale",;
    HelpContextID = 29639414,;
   bGlobalFont=.t.,;
    Height=21, Width=404, Left=183, Top=286, InputMask=replicate('X',254), bHasZoom = .t. 

  func oPATHVBS_1_62.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>"C")
    endwith
  endfunc

  proc oPATHVBS_1_62.mZoom
    this.parent.oContained.w_PATHVBS=cp_GetDir(SYS(5)+SYS(2003), "Selezionare il percorso in cui sar� creato il collegamento")
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oZSRVFLAZI_1_66 as StdCheck with uid="KDOADELEIJ",rtseq=37,rtrep=.f.,left=392, top=265, caption="Filtro azioni per azienda",;
    ToolTipText = "Se attivo configura il servizio per eseguire le azioni relative esclusivamente all'azienda selezionata",;
    HelpContextID = 86885760,;
    cFormVar="w_ZSRVFLAZI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oZSRVFLAZI_1_66.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oZSRVFLAZI_1_66.GetRadio()
    this.Parent.oContained.w_ZSRVFLAZI = this.RadioValue()
    return .t.
  endfunc

  func oZSRVFLAZI_1_66.SetRadio()
    this.Parent.oContained.w_ZSRVFLAZI=trim(this.Parent.oContained.w_ZSRVFLAZI)
    this.value = ;
      iif(this.Parent.oContained.w_ZSRVFLAZI=='S',1,;
      0)
  endfunc

  func oZSRVFLAZI_1_66.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'R')
    endwith
  endfunc

  add object oZSRVAZI_1_67 as StdField with uid="YNICOYMCOB",rtseq=38,rtrep=.f.,;
    cFormVar = "w_ZSRVAZI", cQueryName = "ZSRVAZI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Azienda/Studio con cui accedere alla procedura",;
    HelpContextID = 142751126,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=525, Top=263, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_ZSRVAZI"

  func oZSRVAZI_1_67.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'R' Or .w_ZSRVFLAZI<>'S')
    endwith
  endfunc

  func oZSRVAZI_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oZSRVAZI_1_67.ecpDrop(oSource)
    this.Parent.oContained.link_1_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZSRVAZI_1_67.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oZSRVAZI_1_67'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Azienda",'',this.parent.oContained
  endproc

  add object oStr_1_21 as StdString with uid="WXVTZWKJTR",Visible=.t., Left=7, Top=38,;
    Alignment=1, Width=173, Height=18,;
    Caption="File CNF da utilizzare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="JGQSWYZZTQ",Visible=.t., Left=7, Top=86,;
    Alignment=1, Width=173, Height=18,;
    Caption="Utente con cui accedere:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="YAWKVYGBMA",Visible=.t., Left=7, Top=189,;
    Alignment=1, Width=173, Height=18,;
    Caption="Percorso applicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="DXWUDIUPZK",Visible=.t., Left=7, Top=213,;
    Alignment=1, Width=173, Height=18,;
    Caption="Parametri accesso silente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="EOTDTQTNFO",Visible=.t., Left=7, Top=288,;
    Alignment=1, Width=173, Height=18,;
    Caption="Esegui:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI' Or !InList(.w_TIPO,'J','R'))
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="XEEEFDHWKK",Visible=.t., Left=9, Top=318,;
    Alignment=1, Width=173, Height=17,;
    Caption="Commento:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="FNCGDHFWPA",Visible=.t., Left=357, Top=380,;
    Alignment=0, Width=12, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI' Or !InList(.w_TIPO,'J','R'))
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="GDRPPZASPK",Visible=.t., Left=7, Top=338,;
    Alignment=1, Width=173, Height=18,;
    Caption="Nome collegamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='INI')
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="LHFJPEGOQT",Visible=.t., Left=7, Top=113,;
    Alignment=1, Width=173, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="NAJDLLLLYV",Visible=.t., Left=7, Top=164,;
    Alignment=1, Width=173, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="BZULQVRCIW",Visible=.t., Left=270, Top=164,;
    Alignment=1, Width=39, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="RMLXWHKZYW",Visible=.t., Left=411, Top=164,;
    Alignment=1, Width=44, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="RWBNLUXJDQ",Visible=.t., Left=7, Top=64,;
    Alignment=1, Width=173, Height=18,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="HCHWPJDLBW",Visible=.t., Left=7, Top=138,;
    Alignment=1, Width=173, Height=15,;
    Caption="Business Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (isAhr() or isAlt())
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="MHTWZDQIQP",Visible=.t., Left=31, Top=318,;
    Alignment=1, Width=150, Height=18,;
    Caption="Percorso ZMainService:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF = 'ICO' Or .w_TIPO='C')
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="CIGVBJJWMW",Visible=.t., Left=7, Top=338,;
    Alignment=1, Width=173, Height=18,;
    Caption="Percorso ZSrvMng:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF = 'ICO' Or .w_TIPO='C')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="BTJYICQFKT",Visible=.t., Left=311, Top=288,;
    Alignment=1, Width=78, Height=18,;
    Caption="Mappatura:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='ICO')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="WLHRSMFCAS",Visible=.t., Left=7, Top=6,;
    Alignment=1, Width=173, Height=19,;
    Caption="Configurazione:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (!InList(.w_TIPO,'J','R'))
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="KMFHZHBPEJ",Visible=.t., Left=7, Top=287,;
    Alignment=1, Width=173, Height=18,;
    Caption="Esegui come:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONF='ICO' Or !InList(.w_TIPO,'J','R'))
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="CRJDJDEUJB",Visible=.t., Left=7, Top=64,;
    Alignment=1, Width=173, Height=18,;
    Caption="Studio:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (!isAlt())
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="GGSQUXFOYY",Visible=.t., Left=7, Top=287,;
    Alignment=1, Width=173, Height=18,;
    Caption="Path destinazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>"C")
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ksa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_ksa
Function DESKPATH
LOCAL w_OBJ,w_PATH
w_OBJ=CreateObject("WScript.Shell")
w_PATH=w_OBJ.SpecialFolders("Desktop")
Return (w_PATH)
* --- Fine Area Manuale
