* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bdp                                                        *
*              Batch dettaglio provvig.                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_2]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2000-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bdp",oParentObject)
return(i_retval)

define class tgsve_bdp as StdBatch
  * --- Local variables
  w_MPNUMDOC = 0
  w_MPDATDOC = ctod("  /  /  ")
  w_SERIAL = space(10)
  w_MPCODVAL = space(3)
  w_MPALFDOC = space(10)
  w_ROWNUM = 0
  w_MPTIPCON = space(1)
  w_MPDATSCA = ctod("  /  /  ")
  w_MPCODCON = space(15)
  w_MPPERPRC = 0
  w_MPDESSUP = space(50)
  w_MPPERPRA = 0
  w_MPTOTIMP = 0
  w_MPDATMAT = ctod("  /  /  ")
  w_MPTOTAGE = 0
  w_MPDATLIQ = ctod("  /  /  ")
  w_MPTOTZON = 0
  w_MPTIPMAT = space(2)
  * --- WorkFile variables
  MOP_MAST_idx=0
  MOP_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dettaglio Provvigione (da GSVE_KPR)
    * --- Inizializza Dati Provvigioni
    this.w_MPNUMDOC = 0
    this.w_MPTIPMAT = SPACE(2)
    this.w_MPTOTAGE = 0
    this.w_MPCODCON = SPACE(15)
    this.w_MPCODVAL = SPACE(3)
    this.w_MPDESSUP = SPACE(50)
    this.w_MPTOTIMP = 0
    this.w_MPDATDOC = cp_CharToDate("  -  -  ")
    this.w_MPTIPCON = " "
    this.w_MPDATMAT = cp_CharToDate("  -  -  ")
    this.w_MPPERPRC = 0
    this.w_MPDATLIQ = cp_CharToDate("  -  -  ")
    this.w_MPPERPRA = 0
    this.w_MPDATSCA = cp_CharToDate("  -  -  ")
    this.w_MPTOTZON = 0
    this.w_MPALFDOC = Space(10)
    * --- Leggo i dati dai movimenti di provvigione
    * --- Read from MOP_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_MAST_idx,2],.t.,this.MOP_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MPNUMDOC,MPALFDOC,MPDATDOC,MPCODVAL,MPTIPCON,MPCODCON,MPDESSUP"+;
        " from "+i_cTable+" MOP_MAST where ";
            +"MPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MPSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MPNUMDOC,MPALFDOC,MPDATDOC,MPCODVAL,MPTIPCON,MPCODCON,MPDESSUP;
        from (i_cTable) where;
            MPSERIAL = this.oParentObject.w_MPSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MPNUMDOC = NVL(cp_ToDate(_read_.MPNUMDOC),cp_NullValue(_read_.MPNUMDOC))
      this.w_MPALFDOC = NVL(cp_ToDate(_read_.MPALFDOC),cp_NullValue(_read_.MPALFDOC))
      this.w_MPDATDOC = NVL(cp_ToDate(_read_.MPDATDOC),cp_NullValue(_read_.MPDATDOC))
      this.w_MPCODVAL = NVL(cp_ToDate(_read_.MPCODVAL),cp_NullValue(_read_.MPCODVAL))
      this.w_MPTIPCON = NVL(cp_ToDate(_read_.MPTIPCON),cp_NullValue(_read_.MPTIPCON))
      this.w_MPCODCON = NVL(cp_ToDate(_read_.MPCODCON),cp_NullValue(_read_.MPCODCON))
      this.w_MPDESSUP = NVL(cp_ToDate(_read_.MPDESSUP),cp_NullValue(_read_.MPDESSUP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from MOP_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOP_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2],.t.,this.MOP_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MPDATMAT,MPTOTIMP,MPTOTAGE,MPTOTZON,MPTIPMAT,MPDATLIQ,MPDATSCA,MPPERPRA,MPPERPRC"+;
        " from "+i_cTable+" MOP_DETT where ";
            +"MPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MPSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MPDATMAT,MPTOTIMP,MPTOTAGE,MPTOTZON,MPTIPMAT,MPDATLIQ,MPDATSCA,MPPERPRA,MPPERPRC;
        from (i_cTable) where;
            MPSERIAL = this.oParentObject.w_MPSERIAL;
            and CPROWNUM = this.oParentObject.w_CPROWNUM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MPDATMAT = NVL(cp_ToDate(_read_.MPDATMAT),cp_NullValue(_read_.MPDATMAT))
      this.w_MPTOTIMP = NVL(cp_ToDate(_read_.MPTOTIMP),cp_NullValue(_read_.MPTOTIMP))
      this.w_MPTOTAGE = NVL(cp_ToDate(_read_.MPTOTAGE),cp_NullValue(_read_.MPTOTAGE))
      this.w_MPTOTZON = NVL(cp_ToDate(_read_.MPTOTZON),cp_NullValue(_read_.MPTOTZON))
      this.w_MPTIPMAT = NVL(cp_ToDate(_read_.MPTIPMAT),cp_NullValue(_read_.MPTIPMAT))
      this.w_MPDATLIQ = NVL(cp_ToDate(_read_.MPDATLIQ),cp_NullValue(_read_.MPDATLIQ))
      this.w_MPDATSCA = NVL(cp_ToDate(_read_.MPDATSCA),cp_NullValue(_read_.MPDATSCA))
      this.w_MPPERPRA = NVL(cp_ToDate(_read_.MPPERPRA),cp_NullValue(_read_.MPPERPRA))
      this.w_MPPERPRC = NVL(cp_ToDate(_read_.MPPERPRC),cp_NullValue(_read_.MPPERPRC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_SERIAL = this.oParentObject.w_MPSERIAL
    this.w_ROWNUM = this.oParentObject.w_CPROWNUM
    * --- Maschera Dettaglio Provvigioni
    do GSVE_KDP with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOP_MAST'
    this.cWorkTables[2]='MOP_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
