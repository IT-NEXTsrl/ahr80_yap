* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bnt                                                        *
*              Inserisce N righe vuote in cursore nota spese                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-03-30                                                      *
* Last revis.: 2012-08-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_MVSERIAL,w_TIPOIN,w_CODESE,w_NUMINI,w_NUMFIN,w_DATAIN,w_DATAFI,w_CLIFOR,w_MVRIFFAD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bnt",oParentObject,m.w_MVSERIAL,m.w_TIPOIN,m.w_CODESE,m.w_NUMINI,m.w_NUMFIN,m.w_DATAIN,m.w_DATAFI,m.w_CLIFOR,m.w_MVRIFFAD)
return(i_retval)

define class tgsve_bnt as StdBatch
  * --- Local variables
  w_MVSERIAL = space(10)
  w_TIPOIN = space(5)
  w_CODESE = space(4)
  w_NUMINI = 0
  w_NUMFIN = 0
  w_DATAIN = ctod("  /  /  ")
  w_DATAFI = ctod("  /  /  ")
  w_CLIFOR = space(15)
  w_MVRIFFAD = space(10)
  w_CURSORNAME = space(10)
  w_CURSORNAME2 = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Determina partite aperte legate a fatture 
    *     I parametri sono i filtri da passare alla query
    this.w_CURSORNAME = Sys(2015)
    this.w_CURSORNAME2 = Sys(2015)
    * --- Al fine di ottenere n vuote righe per ciascuna pratica analizzata nella nota spese 
    *     come primo passo inseriamo nel cursore i codici pratica
    vq_exec("QUERY\GSVE_BNT.VQR", this, this.w_CURSORNAME)
    CREATE CURSOR N_Righe (N_Riga C(2))
    FOR w_Step=1 TO 15
    INSERT INTO N_Righe ( N_Riga ) VALUES ( PADL(ALLTRIM(STR(w_Step)),2,"0") )
    ENDFOR
    SELECT * FROM ( this.w_CURSORNAME ), N_Righe INTO CURSOR ( this.w_CURSORNAME2)
    CURTOTAB( this.w_CURSORNAME2, "GSVE_BNT")
    USE IN SELECT(this.w_CURSORNAME)
    USE IN SELECT(this.w_CURSORNAME2)
    USE IN SELECT("N_Righe")
  endproc


  proc Init(oParentObject,w_MVSERIAL,w_TIPOIN,w_CODESE,w_NUMINI,w_NUMFIN,w_DATAIN,w_DATAFI,w_CLIFOR,w_MVRIFFAD)
    this.w_MVSERIAL=w_MVSERIAL
    this.w_TIPOIN=w_TIPOIN
    this.w_CODESE=w_CODESE
    this.w_NUMINI=w_NUMINI
    this.w_NUMFIN=w_NUMFIN
    this.w_DATAIN=w_DATAIN
    this.w_DATAFI=w_DATAFI
    this.w_CLIFOR=w_CLIFOR
    this.w_MVRIFFAD=w_MVRIFFAD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_MVSERIAL,w_TIPOIN,w_CODESE,w_NUMINI,w_NUMFIN,w_DATAIN,w_DATAFI,w_CLIFOR,w_MVRIFFAD"
endproc
