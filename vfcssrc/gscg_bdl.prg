* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bdl                                                        *
*              Aggiornamento data applicazione                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-07                                                      *
* Last revis.: 2012-03-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bdl",oParentObject,m.pParame)
return(i_retval)

define class tgscg_bdl as StdBatch
  * --- Local variables
  pParame = space(4)
  w_ZOOM = space(10)
  w_NUMREC = 0
  w_DISERIAL = space(10)
  w_NRECORD = 0
  w_DATAPL = ctod("  /  /  ")
  w_MESS = space(250)
  * --- WorkFile variables
  DIC_INTE_idx=0
  TMPVEND1_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento data applicazione
    do case
      case LEFT(this.pParame, 3) = "SEL"
        this.w_ZOOM = this.oParentObject.w_ZoomDic_Inte
        * --- Aggiorno il cursore
        if USED(this.w_Zoom.cCursor) And RECCOUNT(this.w_Zoom.cCursor)>0
          SELECT ( this.w_ZOOM.cCursor )
          this.w_NUMREC = RECNO()
          UPDATE (this.w_ZOOM.cCursor) SET XCHK=ICASE(RIGHT(this.pPARAME,1)="S",1,RIGHT(this.pPARAME,1)="D", 0,RIGHT(this.pPARAME,1)="I",IIF(XCHK=1,0,1),XCHK)
          SELECT ( this.w_ZOOM.cCursor )
          COUNT FOR XCHK=1 TO w_COUNT
          if w_COUNT>0
            this.oParentObject.w_TESTSEL = "S"
          else
            this.oParentObject.w_TESTSEL = "D"
          endif
          GO this.w_NUMREC
        endif
      case this.pParame="AGRE"
        this.w_ZOOM = this.oParentObject.w_ZoomDic_Inte
        NC = this.w_ZOOM.cCursor
        * --- Controllo se esistono dichiarazioni di intento con data validit� che 
        *     non comprende la data prima applicazione inserita
        if Empty(this.oParentObject.w_Didatapl)
          select Diserial from (NC) where Xchk=1 And (Cp_Todate(Didatapl)<Didatini Or Cp_Todate(Didatapl)>Didatfin) into cursor ControlloDati 
        else
          select Diserial from (NC) where Xchk=1 And (this.oParentObject.w_Didatapl<Didatini Or this.oParentObject.w_Didatapl>Didatfin) into cursor ControlloDati 
        endif
        if Reccount("ControlloDati")>0
          this.w_MESS = "Attenzione: sono presenti dichiarazioni d'intento per le quali%0la data di prima applicazione non � coerente con l'intervallo di validit�.%0Confermi l'aggiornamento?"
          use in select ("Controllodati")
          if Ah_YesNo(this.w_Mess)
            * --- Proseguo con l'elaborazione
          else
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Try
        local bErr_0361DC88
        bErr_0361DC88=bTrsErr
        this.Try_0361DC88()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_errormsg("Errore durante aggiornamento. Operazione abbandonata")
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_0361DC88
        * --- End
        AH_ERRORMSG("Aggiornamento completato",48)
        this.oParentObject.w_DIDATAPL = Cp_CharToDate("  -  -    ")
        if this.oParentObject.w_TIPOPE="S"
          This.oParentObject.NotifyEvent("Ricerca")
        else
          This.oParentObject.oParentObject.oParentObject.NotifyEvent("Esegui")
        endif
      case this.pParame="AGGP"
        this.w_ZOOM = this.oParentObject.w_ZoomDoc
        NC = this.w_ZOOM.cCursor
        * --- Create temporary table TMPVEND1
        i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPVEND1_proto';
              )
        this.TMPVEND1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        CURTOTAB(NC, "TMPVEND1")
        * --- Select from TMPVEND1
        i_nConn=i_TableProp[this.TMPVEND1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2],.t.,this.TMPVEND1_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select count(*) AS NRECORD  from "+i_cTable+" TMPVEND1 ";
               ,"_Curs_TMPVEND1")
        else
          select count(*) AS NRECORD from (i_cTable);
            into cursor _Curs_TMPVEND1
        endif
        if used('_Curs_TMPVEND1')
          select _Curs_TMPVEND1
          locate for 1=1
          do while not(eof())
          this.w_NRECORD = NRECORD
            select _Curs_TMPVEND1
            continue
          enddo
          use
        endif
        if this.w_NRECORD>0
          Ah_ErrorMsg("Sono visualizzate le dichiarazioni di intento ricevute,%0di tipologia 'No applicazione IVA' con data%0prima applicazione non valorizzata.")
          do Gscg1Kda with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          Ah_ErrorMsg("Non sono presenti dichiarazioni di intento da aggiornare.")
        endif
      case this.pParame="VISU"
        this.oParentObject.w_ZoomDic_Inte.cCpQueryName = "QUERY\GSCG2KDA"
        This.OparentObject.NotifyEvent( "Visualizza")
        * --- Drop temporary table TMPVEND1
        i_nIdx=cp_GetTableDefIdx('TMPVEND1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND1')
        endif
    endcase
  endproc
  proc Try_0361DC88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT &NC
    GO TOP
    * --- Cicla sui record Selezionati
    SCAN FOR XCHK<>0
    this.w_DISERIAL = DISERIAL
    if Not Empty(this.oParentObject.w_DIDATAPL)
      * --- Write into DIC_INTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DIDATAPL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DIDATAPL),'DIC_INTE','DIDATAPL');
            +i_ccchkf ;
        +" where ";
            +"DISERIAL = "+cp_ToStrODBC(this.w_DISERIAL);
               )
      else
        update (i_cTable) set;
            DIDATAPL = this.oParentObject.w_DIDATAPL;
            &i_ccchkf. ;
         where;
            DISERIAL = this.w_DISERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      this.w_DATAPL = Cp_Todate(DIDATAPL)
      * --- Write into DIC_INTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DIDATAPL ="+cp_NullLink(cp_ToStrODBC(this.w_DATAPL),'DIC_INTE','DIDATAPL');
            +i_ccchkf ;
        +" where ";
            +"DISERIAL = "+cp_ToStrODBC(this.w_DISERIAL);
               )
      else
        update (i_cTable) set;
            DIDATAPL = this.w_DATAPL;
            &i_ccchkf. ;
         where;
            DISERIAL = this.w_DISERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DIC_INTE'
    this.cWorkTables[2]='*TMPVEND1'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_TMPVEND1')
      use in _Curs_TMPVEND1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
