* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bc7                                                        *
*              Selezione/deselezione cont.distinte                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_19]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-29                                                      *
* Last revis.: 2003-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bc7",oParentObject,m.pExec)
return(i_retval)

define class tgscg_bc7 as StdBatch
  * --- Local variables
  pExec = space(1)
  w_NUMERO = 0
  w_Padre = .NULL.
  w_GPRIFDIS = space(10)
  w_RIFDIS = space(10)
  w_SCAFIN = ctod("  /  /  ")
  w_SCAINI = ctod("  /  /  ")
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_TIPDIS = space(2)
  w_CATPAG = space(2)
  w_DATDIS = ctod("  /  /  ")
  w_DINUMERO = 0
  w_CODVAL = space(5)
  w_TIPSBF = space(1)
  w_CHK = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per Seleziona/deseleziona (Lanciato dalla Contabilizzazione Distinte)
    NC = this.oParentObject.w_ZOOMCALC.cCursor
    do case
      case this.pExec="S"
        if this.oParentObject.w_SELEZI="S"
          UPDATE &NC SET XCHK = 1
          this.oParentObject.w_FLSELE = 1
        else
          UPDATE &NC SET XCHK = 0
          this.oParentObject.w_FLSELE = 0
        endif
      case this.pExec="D"
        this.w_Padre = This.oparentObject
        this.w_Padre.Notifyevent("Carica")     
        UPDATE &NC SET XCHK = 1
        this.oParentObject.w_FLSELE = 1
      case this.pExec="C"
        * --- Per CompatibilitÓ maschera GSTE_KDS
        this.w_SCAFIN = this.oParentObject.w_DATFIN
        do GSTE_KDS with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_NUMERO = this.w_DINUMERO
        this.oParentObject.w_NUMDIS = this.w_RIFDIS
        if Not Empty(this.oParentObject.w_NUMERO)
          this.w_Padre = This.oparentObject
          this.w_Padre.Notifyevent("Carica")     
          this.w_PADRE.oPgFrm.ActivePage = 2
        endif
      case this.pExec="G" or this.pExec="A" 
        * --- Seleziona\Deseleziona Gruppo Effetti
        this.w_CHK = IIF(this.pEXEC="G",1,0)
        SELECT (NC) 
 PUNT=Recno() 
 UPDATE &NC SET XCHK = this.w_CHK WHERE PTNUMEFF=this.oParentObject.w_NUMEFF 
 Go PUNT
    endcase
  endproc


  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
