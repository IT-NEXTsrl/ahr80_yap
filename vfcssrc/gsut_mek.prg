* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mek                                                        *
*              Elaborazioni KPI                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-07-03                                                      *
* Last revis.: 2015-03-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsut_mek
If Vartype(i_designfile)=='U'
  Public i_designfile
Endif
i_designfile = ''
* --- Fine Area Manuale
return(createobject("tgsut_mek"))

* --- Class definition
define class tgsut_mek as StdTrsForm
  Top    = 10
  Left   = 22

  * --- Standard Properties
  Width  = 777
  Height = 377+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-23"
  HelpContextID=204913815
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  ELABKPIM_IDX = 0
  ELABKPID_IDX = 0
  cpusers_IDX = 0
  cpgroups_IDX = 0
  cFile = "ELABKPIM"
  cFileDetail = "ELABKPID"
  cKeySelect = "EKCODICE"
  cKeyWhere  = "EKCODICE=this.w_EKCODICE"
  cKeyDetail  = "EKCODICE=this.w_EKCODICE and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"EKCODICE="+cp_ToStrODBC(this.w_EKCODICE)';

  cKeyDetailWhereODBC = '"EKCODICE="+cp_ToStrODBC(this.w_EKCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"ELABKPID.EKCODICE="+cp_ToStrODBC(this.w_EKCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ELABKPID.CPROWORD '
  cPrg = "gsut_mek"
  cComment = "Elaborazioni KPI"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_EKCODICE = space(10)
  w_EKDESCRI = space(100)
  w_EKTIPOUG = space(1)
  o_EKTIPOUG = space(1)
  w_EKUTEGRP = 0
  w_EKUTEGRP = 0
  w_DESUTE = space(20)
  w_DESGRP = space(20)
  w_EKVISIBI = space(1)
  w_EKCNDATT = space(250)
  w_EKCHKATT = space(1)
  w_EK_QUERY = space(250)
  o_EK_QUERY = space(250)
  w_EKTIPRES = space(1)
  o_EKTIPRES = space(1)
  w_EKTIPSIN = space(3)
  o_EKTIPSIN = space(3)
  w_EKBENCHM = space(3)
  o_EKBENCHM = space(3)
  w_EKBENFIX = 0
  w_EKBENCOD = space(10)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_EK_PARAM = space(20)
  w_EKVALORE = space(250)
  w_DESELA = space(100)
  w_EKFRQEXE = 0
  w_EKWRTLOG = space(1)
  w_EKNUMRES = 0
  w_LNKSIN = space(3)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ELABKPIM','gsut_mek')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_mekPag1","gsut_mek",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Elaborazione KPI")
      .Pages(1).HelpContextID = 218393703
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oEKCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='cpgroups'
    this.cWorkTables[3]='ELABKPIM'
    this.cWorkTables[4]='ELABKPID'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ELABKPIM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ELABKPIM_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_EKCODICE = NVL(EKCODICE,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_18_joined
    link_1_18_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from ELABKPIM where EKCODICE=KeySet.EKCODICE
    *
    i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2],this.bLoadRecFilter,this.ELABKPIM_IDX,"gsut_mek")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ELABKPIM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ELABKPIM.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"ELABKPID.","ELABKPIM.")
      i_cTable = i_cTable+' ELABKPIM '
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'EKCODICE',this.w_EKCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DESUTE = space(20)
        .w_DESGRP = space(20)
        .w_DESELA = space(100)
        .w_LNKSIN = space(3)
        .w_EKCODICE = NVL(EKCODICE,space(10))
        .w_EKDESCRI = NVL(EKDESCRI,space(100))
        .w_EKTIPOUG = NVL(EKTIPOUG,space(1))
        .w_EKUTEGRP = NVL(EKUTEGRP,0)
          .link_1_4('Load')
        .w_EKUTEGRP = NVL(EKUTEGRP,0)
          .link_1_5('Load')
        .w_EKVISIBI = NVL(EKVISIBI,space(1))
        .w_EKCNDATT = NVL(EKCNDATT,space(250))
        .w_EKCHKATT = NVL(EKCHKATT,space(1))
        .w_EK_QUERY = NVL(EK_QUERY,space(250))
        .w_EKTIPRES = NVL(EKTIPRES,space(1))
        .w_EKTIPSIN = NVL(EKTIPSIN,space(3))
        .w_EKBENCHM = NVL(EKBENCHM,space(3))
        .w_EKBENFIX = NVL(EKBENFIX,0)
        .w_EKBENCOD = NVL(EKBENCOD,space(10))
          if link_1_18_joined
            this.w_EKBENCOD = NVL(EKCODICE118,NVL(this.w_EKBENCOD,space(10)))
            this.w_DESELA = NVL(EKDESCRI118,space(100))
            this.w_LNKSIN = NVL(EKTIPSIN118,space(3))
          else
          .link_1_18('Load')
          endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_EKFRQEXE = NVL(EKFRQEXE,0)
        .w_EKWRTLOG = NVL(EKWRTLOG,space(1))
        .w_EKNUMRES = NVL(EKNUMRES,0)
        cp_LoadRecExtFlds(this,'ELABKPIM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from ELABKPID where EKCODICE=KeySet.EKCODICE
      *                            and CPROWNUM=KeySet.CPROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.ELABKPID_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELABKPID_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('ELABKPID')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "ELABKPID.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" ELABKPID"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'EKCODICE',this.w_EKCODICE  )
        select * from (i_cTable) ELABKPID where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_EK_PARAM = NVL(EK_PARAM,space(20))
          .w_EKVALORE = NVL(EKVALORE,space(250))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_40.enabled = .oPgFrm.Page1.oPag.oBtn_1_40.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsut_mek
    i_designfile = Alltrim(This.w_EK_QUERY)
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_mek
    * --- Per caricamento rapido: eseguo i metodi usati nella BlankRec senza per�
    * --- sbiancare le variabili
    IF this.cFunction='Load' AND NOT EMPTY(this.w_EKCODICE)
       * --- Valorizzo i dati da non riportare sulla nuova elaborazione
       this.w_EKCODICE=Space(10)
       this.w_EKTIPOUG='U'
       this.w_EKUTEGRP=0
       this.w_DESUTE=Space(20)
       this.w_DESGRP=Space(20)
       
       * --- Dettaglio parametri
       this.bLoaded=.f.
       this.bUpdated=.t.
       set delete off
       update (this.cTrsName) set i_Srv='A' where 1=1
       set delete on
       this.FirstRow()
       this.SetRow()
    
       * --- Fine
       this.SaveDependsOn()
       this.SetControlsValue()
       this.TrsFromWork()
       this.mHideControls()
       this.ChildrenChangeRow()
    	 this.NotifyEvent('Blank')
       this.mCalc(.t.)
       return
    Endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_EKCODICE=space(10)
      .w_EKDESCRI=space(100)
      .w_EKTIPOUG=space(1)
      .w_EKUTEGRP=0
      .w_EKUTEGRP=0
      .w_DESUTE=space(20)
      .w_DESGRP=space(20)
      .w_EKVISIBI=space(1)
      .w_EKCNDATT=space(250)
      .w_EKCHKATT=space(1)
      .w_EK_QUERY=space(250)
      .w_EKTIPRES=space(1)
      .w_EKTIPSIN=space(3)
      .w_EKBENCHM=space(3)
      .w_EKBENFIX=0
      .w_EKBENCOD=space(10)
      .w_CPROWNUM=0
      .w_CPROWORD=10
      .w_EK_PARAM=space(20)
      .w_EKVALORE=space(250)
      .w_DESELA=space(100)
      .w_EKFRQEXE=0
      .w_EKWRTLOG=space(1)
      .w_EKNUMRES=0
      .w_LNKSIN=space(3)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_EKTIPOUG = 'U'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_EKUTEGRP))
         .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_EKUTEGRP))
         .link_1_5('Full')
        endif
        .DoRTCalc(6,7,.f.)
        .w_EKVISIBI = 'G'
        .DoRTCalc(9,9,.f.)
        .w_EKCHKATT = 'S'
        .DoRTCalc(11,11,.f.)
        .w_EKTIPRES = 'C'
        .w_EKTIPSIN = 'NUL'
        .w_EKBENCHM = 'NUL'
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_EKBENCOD))
         .link_1_18('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(17,21,.f.)
        .w_EKFRQEXE = 60
        .w_EKWRTLOG = 'N'
        .w_EKNUMRES = 1
      endif
    endwith
    cp_BlankRecExtFlds(this,'ELABKPIM')
    this.DoRTCalc(25,25,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oEKCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oEKDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oEKTIPOUG_1_3.enabled = i_bVal
      .Page1.oPag.oEKUTEGRP_1_4.enabled = i_bVal
      .Page1.oPag.oEKUTEGRP_1_5.enabled = i_bVal
      .Page1.oPag.oEKVISIBI_1_8.enabled = i_bVal
      .Page1.oPag.oEKCNDATT_1_9.enabled = i_bVal
      .Page1.oPag.oEKCHKATT_1_10.enabled = i_bVal
      .Page1.oPag.oEK_QUERY_1_11.enabled = i_bVal
      .Page1.oPag.oEKTIPRES_1_12.enabled = i_bVal
      .Page1.oPag.oEKTIPSIN_1_13.enabled = i_bVal
      .Page1.oPag.oEKBENCHM_1_14.enabled = i_bVal
      .Page1.oPag.oEKBENFIX_1_17.enabled = i_bVal
      .Page1.oPag.oEKBENCOD_1_18.enabled = i_bVal
      .Page1.oPag.oEKFRQEXE_1_33.enabled = i_bVal
      .Page1.oPag.oEKWRTLOG_1_35.enabled = i_bVal
      .Page1.oPag.oEKNUMRES_1_36.enabled = i_bVal
      .Page1.oPag.oBtn_1_40.enabled = .Page1.oPag.oBtn_1_40.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oEKCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oEKCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ELABKPIM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKCODICE,"EKCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKDESCRI,"EKDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKTIPOUG,"EKTIPOUG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKUTEGRP,"EKUTEGRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKUTEGRP,"EKUTEGRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKVISIBI,"EKVISIBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKCNDATT,"EKCNDATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKCHKATT,"EKCHKATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EK_QUERY,"EK_QUERY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKTIPRES,"EKTIPRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKTIPSIN,"EKTIPSIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKBENCHM,"EKBENCHM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKBENFIX,"EKBENFIX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKBENCOD,"EKBENCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKFRQEXE,"EKFRQEXE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKWRTLOG,"EKWRTLOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EKNUMRES,"EKNUMRES",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
    i_lTable = "ELABKPIM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ELABKPIM_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_EK_PARAM C(20);
      ,t_EKVALORE C(250);
      ,CPROWNUM N(10);
      ,t_CPROWNUM N(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mekbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oEK_PARAM_2_3.controlsource=this.cTrsName+'.t_EK_PARAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oEKVALORE_2_4.controlsource=this.cTrsName+'.t_EKVALORE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(59)
    this.AddVLine(267)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ELABKPIM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ELABKPIM')
        i_extval=cp_InsertValODBCExtFlds(this,'ELABKPIM')
        local i_cFld
        i_cFld=" "+;
                  "(EKCODICE,EKDESCRI,EKTIPOUG,EKUTEGRP,EKVISIBI"+;
                  ",EKCNDATT,EKCHKATT,EK_QUERY,EKTIPRES,EKTIPSIN"+;
                  ",EKBENCHM,EKBENFIX,EKBENCOD,EKFRQEXE,EKWRTLOG"+;
                  ",EKNUMRES"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_EKCODICE)+;
                    ","+cp_ToStrODBC(this.w_EKDESCRI)+;
                    ","+cp_ToStrODBC(this.w_EKTIPOUG)+;
                    ","+cp_ToStrODBCNull(this.w_EKUTEGRP)+;
                    ","+cp_ToStrODBC(this.w_EKVISIBI)+;
                    ","+cp_ToStrODBC(this.w_EKCNDATT)+;
                    ","+cp_ToStrODBC(this.w_EKCHKATT)+;
                    ","+cp_ToStrODBC(this.w_EK_QUERY)+;
                    ","+cp_ToStrODBC(this.w_EKTIPRES)+;
                    ","+cp_ToStrODBC(this.w_EKTIPSIN)+;
                    ","+cp_ToStrODBC(this.w_EKBENCHM)+;
                    ","+cp_ToStrODBC(this.w_EKBENFIX)+;
                    ","+cp_ToStrODBCNull(this.w_EKBENCOD)+;
                    ","+cp_ToStrODBC(this.w_EKFRQEXE)+;
                    ","+cp_ToStrODBC(this.w_EKWRTLOG)+;
                    ","+cp_ToStrODBC(this.w_EKNUMRES)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ELABKPIM')
        i_extval=cp_InsertValVFPExtFlds(this,'ELABKPIM')
        cp_CheckDeletedKey(i_cTable,0,'EKCODICE',this.w_EKCODICE)
        INSERT INTO (i_cTable);
              (EKCODICE,EKDESCRI,EKTIPOUG,EKUTEGRP,EKVISIBI,EKCNDATT,EKCHKATT,EK_QUERY,EKTIPRES,EKTIPSIN,EKBENCHM,EKBENFIX,EKBENCOD,EKFRQEXE,EKWRTLOG,EKNUMRES &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_EKCODICE;
                  ,this.w_EKDESCRI;
                  ,this.w_EKTIPOUG;
                  ,this.w_EKUTEGRP;
                  ,this.w_EKVISIBI;
                  ,this.w_EKCNDATT;
                  ,this.w_EKCHKATT;
                  ,this.w_EK_QUERY;
                  ,this.w_EKTIPRES;
                  ,this.w_EKTIPSIN;
                  ,this.w_EKBENCHM;
                  ,this.w_EKBENFIX;
                  ,this.w_EKBENCOD;
                  ,this.w_EKFRQEXE;
                  ,this.w_EKWRTLOG;
                  ,this.w_EKNUMRES;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ELABKPID_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELABKPID_IDX,2])
      *
      * insert into ELABKPID
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(EKCODICE,CPROWORD,EK_PARAM,EKVALORE,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_EKCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_EK_PARAM)+","+cp_ToStrODBC(this.w_EKVALORE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'EKCODICE',this.w_EKCODICE,'CPROWNUM',this.w_CPROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_EKCODICE,this.w_CPROWORD,this.w_EK_PARAM,this.w_EKVALORE,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update ELABKPIM
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'ELABKPIM')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " EKDESCRI="+cp_ToStrODBC(this.w_EKDESCRI)+;
             ",EKTIPOUG="+cp_ToStrODBC(this.w_EKTIPOUG)+;
             ",EKUTEGRP="+cp_ToStrODBCNull(this.w_EKUTEGRP)+;
             ",EKVISIBI="+cp_ToStrODBC(this.w_EKVISIBI)+;
             ",EKCNDATT="+cp_ToStrODBC(this.w_EKCNDATT)+;
             ",EKCHKATT="+cp_ToStrODBC(this.w_EKCHKATT)+;
             ",EK_QUERY="+cp_ToStrODBC(this.w_EK_QUERY)+;
             ",EKTIPRES="+cp_ToStrODBC(this.w_EKTIPRES)+;
             ",EKTIPSIN="+cp_ToStrODBC(this.w_EKTIPSIN)+;
             ",EKBENCHM="+cp_ToStrODBC(this.w_EKBENCHM)+;
             ",EKBENFIX="+cp_ToStrODBC(this.w_EKBENFIX)+;
             ",EKBENCOD="+cp_ToStrODBCNull(this.w_EKBENCOD)+;
             ",EKFRQEXE="+cp_ToStrODBC(this.w_EKFRQEXE)+;
             ",EKWRTLOG="+cp_ToStrODBC(this.w_EKWRTLOG)+;
             ",EKNUMRES="+cp_ToStrODBC(this.w_EKNUMRES)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'ELABKPIM')
          i_cWhere = cp_PKFox(i_cTable  ,'EKCODICE',this.w_EKCODICE  )
          UPDATE (i_cTable) SET;
              EKDESCRI=this.w_EKDESCRI;
             ,EKTIPOUG=this.w_EKTIPOUG;
             ,EKUTEGRP=this.w_EKUTEGRP;
             ,EKVISIBI=this.w_EKVISIBI;
             ,EKCNDATT=this.w_EKCNDATT;
             ,EKCHKATT=this.w_EKCHKATT;
             ,EK_QUERY=this.w_EK_QUERY;
             ,EKTIPRES=this.w_EKTIPRES;
             ,EKTIPSIN=this.w_EKTIPSIN;
             ,EKBENCHM=this.w_EKBENCHM;
             ,EKBENFIX=this.w_EKBENFIX;
             ,EKBENCOD=this.w_EKBENCOD;
             ,EKFRQEXE=this.w_EKFRQEXE;
             ,EKWRTLOG=this.w_EKWRTLOG;
             ,EKNUMRES=this.w_EKNUMRES;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD) Or Empty(t_EK_PARAM) Or Empty(t_EKVALORE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.ELABKPID_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.ELABKPID_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from ELABKPID
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ELABKPID
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",EK_PARAM="+cp_ToStrODBC(this.w_EK_PARAM)+;
                     ",EKVALORE="+cp_ToStrODBC(this.w_EKVALORE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,EK_PARAM=this.w_EK_PARAM;
                     ,EKVALORE=this.w_EKVALORE;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD) Or Empty(t_EK_PARAM) Or Empty(t_EKVALORE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.ELABKPID_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ELABKPID_IDX,2])
        *
        * delete ELABKPID
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
        *
        * delete ELABKPIM
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD) Or Empty(t_EK_PARAM) Or Empty(t_EKVALORE))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsut_mek
    i_designfile = ''
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_EKTIPOUG<>.w_EKTIPOUG
          .link_1_4('Full')
        endif
        if .o_EKTIPOUG<>.w_EKTIPOUG
          .link_1_5('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        if .o_EKBENCHM<>.w_EKBENCHM
          .Calculate_KDLVXHIWWC()
        endif
        if .o_EKTIPOUG<>.w_EKTIPOUG
          .Calculate_ZAWKZPVBRA()
        endif
        if .o_EKTIPRES<>.w_EKTIPRES.or. .o_EKTIPSIN<>.w_EKTIPSIN.or. .o_EKBENCHM<>.w_EKBENCHM
          .Calculate_WXMMGWYHDK()
        endif
        if .o_EK_QUERY<>.w_EK_QUERY
          .Calculate_BHQMTFZOLL()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWNUM with this.w_CPROWNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_WYSZCOLKLR()
    with this
          * --- Cancellazione dei risultati e dei log
          gsut_bek(this;
              ,'DEL';
             )
    endwith
  endproc
  proc Calculate_KDLVXHIWWC()
    with this
          * --- Gestione valori EKBENFIX,  EKBENCOD
      if .w_EKBENCHM<>'FIX'
          .w_EKBENFIX = 0
      endif
      if .w_EKBENCHM<>'LSI' and .w_EKBENCHM<>'LRE'
          .w_EKBENCOD = Space(10)
      endif
      if .w_EKBENCHM<>'LSI' and .w_EKBENCHM<>'LRE'
          .w_DESELA = ''
      endif
    endwith
  endproc
  proc Calculate_ZAWKZPVBRA()
    with this
          * --- Gestione valori filtro
          .w_EKUTEGRP = 0
          .w_EKUTEGRP = 0
          .w_DESUTE = ''
          .w_DESGRP = ''
    endwith
  endproc
  proc Calculate_WXMMGWYHDK()
    with this
          * --- Controllo tipi risultato-sintetico-benchmark
          gsut_bek(this;
              ,'CHKT';
             )
    endwith
  endproc
  proc Calculate_BHQMTFZOLL()
    with this
          * --- Inserimento / modifica query
          gsut_bek(this;
              ,'VQR';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oEKUTEGRP_1_4.enabled = this.oPgFrm.Page1.oPag.oEKUTEGRP_1_4.mCond()
    this.oPgFrm.Page1.oPag.oEKUTEGRP_1_5.enabled = this.oPgFrm.Page1.oPag.oEKUTEGRP_1_5.mCond()
    this.oPgFrm.Page1.oPag.oEKCNDATT_1_9.enabled = this.oPgFrm.Page1.oPag.oEKCNDATT_1_9.mCond()
    this.oPgFrm.Page1.oPag.oEKTIPSIN_1_13.enabled = this.oPgFrm.Page1.oPag.oEKTIPSIN_1_13.mCond()
    this.oPgFrm.Page1.oPag.oEKBENFIX_1_17.enabled = this.oPgFrm.Page1.oPag.oEKBENFIX_1_17.mCond()
    this.oPgFrm.Page1.oPag.oEKBENCOD_1_18.enabled = this.oPgFrm.Page1.oPag.oEKBENCOD_1_18.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oEKUTEGRP_1_4.visible=!this.oPgFrm.Page1.oPag.oEKUTEGRP_1_4.mHide()
    this.oPgFrm.Page1.oPag.oEKUTEGRP_1_5.visible=!this.oPgFrm.Page1.oPag.oEKUTEGRP_1_5.mHide()
    this.oPgFrm.Page1.oPag.oDESUTE_1_6.visible=!this.oPgFrm.Page1.oPag.oDESUTE_1_6.mHide()
    this.oPgFrm.Page1.oPag.oDESGRP_1_7.visible=!this.oPgFrm.Page1.oPag.oDESGRP_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oEKBENFIX_1_17.visible=!this.oPgFrm.Page1.oPag.oEKBENFIX_1_17.mHide()
    this.oPgFrm.Page1.oPag.oEKBENCOD_1_18.visible=!this.oPgFrm.Page1.oPag.oEKBENCOD_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDESELA_1_29.visible=!this.oPgFrm.Page1.oPag.oDESELA_1_29.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Delete start")
          .Calculate_WYSZCOLKLR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=EKUTEGRP
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EKUTEGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_EKUTEGRP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_EKUTEGRP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_EKUTEGRP) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oEKUTEGRP_1_4'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EKUTEGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_EKUTEGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_EKUTEGRP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EKUTEGRP = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_EKUTEGRP = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EKUTEGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EKUTEGRP
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EKUTEGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_EKUTEGRP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_EKUTEGRP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_EKUTEGRP) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','code',cp_AbsName(oSource.parent,'oEKUTEGRP_1_5'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EKUTEGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_EKUTEGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_EKUTEGRP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EKUTEGRP = NVL(_Link_.code,0)
      this.w_DESGRP = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_EKUTEGRP = 0
      endif
      this.w_DESGRP = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EKUTEGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EKBENCOD
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
    i_lTable = "ELABKPIM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2], .t., this.ELABKPIM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EKBENCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ELABKPIM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EKCODICE like "+cp_ToStrODBC(trim(this.w_EKBENCOD)+"%");

          i_ret=cp_SQL(i_nConn,"select EKCODICE,EKDESCRI,EKTIPSIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EKCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EKCODICE',trim(this.w_EKBENCOD))
          select EKCODICE,EKDESCRI,EKTIPSIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EKCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EKBENCOD)==trim(_Link_.EKCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EKBENCOD) and !this.bDontReportError
            deferred_cp_zoom('ELABKPIM','*','EKCODICE',cp_AbsName(oSource.parent,'oEKBENCOD_1_18'),i_cWhere,'',"Elaborazioni",'GSUT_MEK.ELABKPIM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EKCODICE,EKDESCRI,EKTIPSIN";
                     +" from "+i_cTable+" "+i_lTable+" where EKCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EKCODICE',oSource.xKey(1))
            select EKCODICE,EKDESCRI,EKTIPSIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EKBENCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EKCODICE,EKDESCRI,EKTIPSIN";
                   +" from "+i_cTable+" "+i_lTable+" where EKCODICE="+cp_ToStrODBC(this.w_EKBENCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EKCODICE',this.w_EKBENCOD)
            select EKCODICE,EKDESCRI,EKTIPSIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EKBENCOD = NVL(_Link_.EKCODICE,space(10))
      this.w_DESELA = NVL(_Link_.EKDESCRI,space(100))
      this.w_LNKSIN = NVL(_Link_.EKTIPSIN,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_EKBENCOD = space(10)
      endif
      this.w_DESELA = space(100)
      this.w_LNKSIN = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LNKSIN<>'NUL'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Elaborazione inesistente o senza risultato sintetico")
        endif
        this.w_EKBENCOD = space(10)
        this.w_DESELA = space(100)
        this.w_LNKSIN = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])+'\'+cp_ToStr(_Link_.EKCODICE,1)
      cp_ShowWarn(i_cKey,this.ELABKPIM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EKBENCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ELABKPIM_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.EKCODICE as EKCODICE118"+ ",link_1_18.EKDESCRI as EKDESCRI118"+ ",link_1_18.EKTIPSIN as EKTIPSIN118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on ELABKPIM.EKBENCOD=link_1_18.EKCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and ELABKPIM.EKBENCOD=link_1_18.EKCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oEKCODICE_1_1.value==this.w_EKCODICE)
      this.oPgFrm.Page1.oPag.oEKCODICE_1_1.value=this.w_EKCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oEKDESCRI_1_2.value==this.w_EKDESCRI)
      this.oPgFrm.Page1.oPag.oEKDESCRI_1_2.value=this.w_EKDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oEKTIPOUG_1_3.RadioValue()==this.w_EKTIPOUG)
      this.oPgFrm.Page1.oPag.oEKTIPOUG_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEKUTEGRP_1_4.value==this.w_EKUTEGRP)
      this.oPgFrm.Page1.oPag.oEKUTEGRP_1_4.value=this.w_EKUTEGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oEKUTEGRP_1_5.value==this.w_EKUTEGRP)
      this.oPgFrm.Page1.oPag.oEKUTEGRP_1_5.value=this.w_EKUTEGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_6.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_6.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRP_1_7.value==this.w_DESGRP)
      this.oPgFrm.Page1.oPag.oDESGRP_1_7.value=this.w_DESGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oEKVISIBI_1_8.RadioValue()==this.w_EKVISIBI)
      this.oPgFrm.Page1.oPag.oEKVISIBI_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEKCNDATT_1_9.value==this.w_EKCNDATT)
      this.oPgFrm.Page1.oPag.oEKCNDATT_1_9.value=this.w_EKCNDATT
    endif
    if not(this.oPgFrm.Page1.oPag.oEKCHKATT_1_10.RadioValue()==this.w_EKCHKATT)
      this.oPgFrm.Page1.oPag.oEKCHKATT_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEK_QUERY_1_11.value==this.w_EK_QUERY)
      this.oPgFrm.Page1.oPag.oEK_QUERY_1_11.value=this.w_EK_QUERY
    endif
    if not(this.oPgFrm.Page1.oPag.oEKTIPRES_1_12.RadioValue()==this.w_EKTIPRES)
      this.oPgFrm.Page1.oPag.oEKTIPRES_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEKTIPSIN_1_13.RadioValue()==this.w_EKTIPSIN)
      this.oPgFrm.Page1.oPag.oEKTIPSIN_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEKBENCHM_1_14.RadioValue()==this.w_EKBENCHM)
      this.oPgFrm.Page1.oPag.oEKBENCHM_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEKBENFIX_1_17.value==this.w_EKBENFIX)
      this.oPgFrm.Page1.oPag.oEKBENFIX_1_17.value=this.w_EKBENFIX
    endif
    if not(this.oPgFrm.Page1.oPag.oEKBENCOD_1_18.value==this.w_EKBENCOD)
      this.oPgFrm.Page1.oPag.oEKBENCOD_1_18.value=this.w_EKBENCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESELA_1_29.value==this.w_DESELA)
      this.oPgFrm.Page1.oPag.oDESELA_1_29.value=this.w_DESELA
    endif
    if not(this.oPgFrm.Page1.oPag.oEKFRQEXE_1_33.RadioValue()==this.w_EKFRQEXE)
      this.oPgFrm.Page1.oPag.oEKFRQEXE_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEKWRTLOG_1_35.RadioValue()==this.w_EKWRTLOG)
      this.oPgFrm.Page1.oPag.oEKWRTLOG_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEKNUMRES_1_36.value==this.w_EKNUMRES)
      this.oPgFrm.Page1.oPag.oEKNUMRES_1_36.value=this.w_EKNUMRES
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEK_PARAM_2_3.value==this.w_EK_PARAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEK_PARAM_2_3.value=this.w_EK_PARAM
      replace t_EK_PARAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEK_PARAM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEKVALORE_2_4.value==this.w_EKVALORE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEKVALORE_2_4.value=this.w_EKVALORE
      replace t_EKVALORE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEKVALORE_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'ELABKPIM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(!Empty(.w_EK_QUERY))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Selezionare una query di estrazione dati"))
          case   (empty(.w_EKCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oEKCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_EKCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EK_QUERY) or not(File(.w_EK_QUERY) And Lower(JustExt(.w_EK_QUERY))=='vqr'))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oEK_QUERY_1_11.SetFocus()
            i_bnoObbl = !empty(.w_EK_QUERY)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("File %w_EK_QUERY% inesistente")
          case   (empty(.w_EKTIPRES))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oEKTIPRES_1_12.SetFocus()
            i_bnoObbl = !empty(.w_EKTIPRES)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LNKSIN<>'NUL')  and not(.w_EKBENCHM<>'LSI' and .w_EKBENCHM<>'LRE')  and (.w_EKBENCHM=='LSI' or .w_EKBENCHM=='LRE')  and not(empty(.w_EKBENCOD))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oEKBENCOD_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Elaborazione inesistente o senza risultato sintetico")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD) Or Empty(.w_EK_PARAM) Or Empty(.w_EKVALORE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_EKTIPOUG = this.w_EKTIPOUG
    this.o_EK_QUERY = this.w_EK_QUERY
    this.o_EKTIPRES = this.w_EKTIPRES
    this.o_EKTIPSIN = this.w_EKTIPSIN
    this.o_EKBENCHM = this.w_EKBENCHM
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD) Or Empty(t_EK_PARAM) Or Empty(t_EKVALORE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_EK_PARAM=space(20)
      .w_EKVALORE=space(250)
    endwith
    this.DoRTCalc(1,25,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWNUM = t_CPROWNUM
    this.w_CPROWORD = t_CPROWORD
    this.w_EK_PARAM = t_EK_PARAM
    this.w_EKVALORE = t_EKVALORE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_CPROWORD with this.w_CPROWORD
    replace t_EK_PARAM with this.w_EK_PARAM
    replace t_EKVALORE with this.w_EKVALORE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_mekPag1 as StdContainer
  Width  = 773
  height = 377
  stdWidth  = 773
  stdheight = 377
  resizeXpos=404
  resizeYpos=297
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oEKCODICE_1_1 as StdField with uid="VVVIAHIEYR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_EKCODICE", cQueryName = "EKCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice della elaborazione",;
    HelpContextID = 104186741,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=116, Top=7, InputMask=replicate('X',10)

  add object oEKDESCRI_1_2 as StdField with uid="BWHBLJUFEI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_EKDESCRI", cQueryName = "EKDESCRI",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della elaborazione",;
    HelpContextID = 189772657,;
   bGlobalFont=.t.,;
    Height=21, Width=582, Left=116, Top=31, InputMask=replicate('X',100)


  add object oEKTIPOUG_1_3 as StdCombo with uid="YRTVEUPFXX",rtseq=3,rtrep=.f.,left=116,top=56,width=88,height=21;
    , ToolTipText = "Filtro utente/gruppo per cui saranno visualizzabili i risultati dell'elaborazione";
    , HelpContextID = 259699571;
    , cFormVar="w_EKTIPOUG",RowSource=""+"Utente,"+"Gruppo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEKTIPOUG_1_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..EKTIPOUG,&i_cF..t_EKTIPOUG),this.value)
    return(iif(xVal =1,'U',;
    iif(xVal =2,'G',;
    space(1))))
  endfunc
  func oEKTIPOUG_1_3.GetRadio()
    this.Parent.oContained.w_EKTIPOUG = this.RadioValue()
    return .t.
  endfunc

  func oEKTIPOUG_1_3.ToRadio()
    this.Parent.oContained.w_EKTIPOUG=trim(this.Parent.oContained.w_EKTIPOUG)
    return(;
      iif(this.Parent.oContained.w_EKTIPOUG=='U',1,;
      iif(this.Parent.oContained.w_EKTIPOUG=='G',2,;
      0)))
  endfunc

  func oEKTIPOUG_1_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oEKUTEGRP_1_4 as StdField with uid="DDTMZEANNS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_EKUTEGRP", cQueryName = "EKUTEGRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente per cui filtrare",;
    HelpContextID = 136291178,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=205, Top=56, cSayPict='"99999"', cGetPict='"99999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_EKUTEGRP"

  func oEKUTEGRP_1_4.mCond()
    with this.Parent.oContained
      return (.w_EKTIPOUG=='U')
    endwith
  endfunc

  func oEKUTEGRP_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EKTIPOUG<>'U')
    endwith
    endif
  endfunc

  func oEKUTEGRP_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oEKUTEGRP_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEKUTEGRP_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oEKUTEGRP_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oEKUTEGRP_1_5 as StdField with uid="LRPWKNYNAH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_EKUTEGRP", cQueryName = "EKUTEGRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo per cui filtrare",;
    HelpContextID = 136291178,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=205, Top=56, cSayPict='"99999"', cGetPict='"99999"', bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="code", oKey_1_2="this.w_EKUTEGRP"

  func oEKUTEGRP_1_5.mCond()
    with this.Parent.oContained
      return (.w_EKTIPOUG=='G')
    endwith
  endfunc

  func oEKUTEGRP_1_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EKTIPOUG<>'G')
    endwith
    endif
  endfunc

  func oEKUTEGRP_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oEKUTEGRP_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEKUTEGRP_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpgroups','*','code',cp_AbsName(this.parent,'oEKUTEGRP_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESUTE_1_6 as StdField with uid="VZMFNEGDRR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 114374198,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=254, Top=56, InputMask=replicate('X',20)

  func oDESUTE_1_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EKTIPOUG<>'U')
    endwith
    endif
  endfunc

  add object oDESGRP_1_7 as StdField with uid="LGZSCFZHTQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESGRP", cQueryName = "DESGRP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 27473462,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=254, Top=56, InputMask=replicate('X',20)

  func oDESGRP_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EKTIPOUG<>'G')
    endwith
    endif
  endfunc


  add object oEKVISIBI_1_8 as StdCombo with uid="VHWPBOQVLV",rtseq=8,rtrep=.f.,left=494,top=56,width=203,height=21;
    , ToolTipText = "Identifica le gestioni su cui visualizzare i risultati dell'elaborazioni";
    , HelpContextID = 88773489;
    , cFormVar="w_EKVISIBI",RowSource=""+"su Gadget,"+"su gestione Situazione clienti,"+"su gestione Situazione fornitori,"+"su gestione Situazione prodotti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEKVISIBI_1_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..EKVISIBI,&i_cF..t_EKVISIBI),this.value)
    return(iif(xVal =1,'G',;
    iif(xVal =2,'C',;
    iif(xVal =3,'F',;
    iif(xVal =4,'P',;
    space(1))))))
  endfunc
  func oEKVISIBI_1_8.GetRadio()
    this.Parent.oContained.w_EKVISIBI = this.RadioValue()
    return .t.
  endfunc

  func oEKVISIBI_1_8.ToRadio()
    this.Parent.oContained.w_EKVISIBI=trim(this.Parent.oContained.w_EKVISIBI)
    return(;
      iif(this.Parent.oContained.w_EKVISIBI=='G',1,;
      iif(this.Parent.oContained.w_EKVISIBI=='C',2,;
      iif(this.Parent.oContained.w_EKVISIBI=='F',3,;
      iif(this.Parent.oContained.w_EKVISIBI=='P',4,;
      0)))))
  endfunc

  func oEKVISIBI_1_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oEKCNDATT_1_9 as StdField with uid="FRRLYPUXLM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_EKCNDATT", cQueryName = "EKCNDATT",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Condizione di attivazione",;
    HelpContextID = 238469990,;
   bGlobalFont=.t.,;
    Height=21, Width=582, Left=116, Top=81, InputMask=replicate('X',250)

  func oEKCNDATT_1_9.mCond()
    with this.Parent.oContained
      return (.w_EKCHKATT=='S')
    endwith
  endfunc

  add object oEKCHKATT_1_10 as StdCheck with uid="LRKKACUOCT",rtseq=10,rtrep=.f.,left=701, top=84, caption="Abilitato",;
    ToolTipText = "Abilita l'esecuzione dell'elaborazione",;
    HelpContextID = 231523174,;
    cFormVar="w_EKCHKATT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEKCHKATT_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..EKCHKATT,&i_cF..t_EKCHKATT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oEKCHKATT_1_10.GetRadio()
    this.Parent.oContained.w_EKCHKATT = this.RadioValue()
    return .t.
  endfunc

  func oEKCHKATT_1_10.ToRadio()
    this.Parent.oContained.w_EKCHKATT=trim(this.Parent.oContained.w_EKCHKATT)
    return(;
      iif(this.Parent.oContained.w_EKCHKATT=='S',1,;
      0))
  endfunc

  func oEKCHKATT_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oEK_QUERY_1_11 as StdField with uid="TJMQSUQKFN",rtseq=11,rtrep=.f.,;
    cFormVar = "w_EK_QUERY", cQueryName = "EK_QUERY",;
    bObbl = .t. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    sErrorMsg = "File %w_EK_QUERY% inesistente",;
    HelpContextID = 153224033,;
   bGlobalFont=.t.,;
    Height=21, Width=582, Left=116, Top=105, InputMask=replicate('X',250), bHasZoom = .t. 

  func oEK_QUERY_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (File(.w_EK_QUERY) And Lower(JustExt(.w_EK_QUERY))=='vqr')
    endwith
    return bRes
  endfunc

  proc oEK_QUERY_1_11.mZoom
    This.Parent.oContained.w_EK_QUERY=GetFile("vqr","Nome query","",0,"Query di estrazione dati")
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oEKTIPRES_1_12 as StdCombo with uid="HCGQRILQQS",rtseq=12,rtrep=.f.,left=116,top=130,width=138,height=21;
    , ToolTipText = "Tipo dei dati presenti nella colonna risultato";
    , HelpContextID = 209367911;
    , cFormVar="w_EKTIPRES",RowSource=""+"Conteggio,"+"Monetario,"+"Percentuale,"+"Testo", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oEKTIPRES_1_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..EKTIPRES,&i_cF..t_EKTIPRES),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'M',;
    iif(xVal =3,'P',;
    iif(xVal =4,'T',;
    space(1))))))
  endfunc
  func oEKTIPRES_1_12.GetRadio()
    this.Parent.oContained.w_EKTIPRES = this.RadioValue()
    return .t.
  endfunc

  func oEKTIPRES_1_12.ToRadio()
    this.Parent.oContained.w_EKTIPRES=trim(this.Parent.oContained.w_EKTIPRES)
    return(;
      iif(this.Parent.oContained.w_EKTIPRES=='C',1,;
      iif(this.Parent.oContained.w_EKTIPRES=='M',2,;
      iif(this.Parent.oContained.w_EKTIPRES=='P',3,;
      iif(this.Parent.oContained.w_EKTIPRES=='T',4,;
      0)))))
  endfunc

  func oEKTIPRES_1_12.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oEKTIPSIN_1_13 as StdCombo with uid="IVYQNHFWBL",rtseq=13,rtrep=.f.,left=116,top=154,width=138,height=21;
    , ToolTipText = "Funzione per il calcolo del risultato sintetico da mostrare";
    , HelpContextID = 75844756;
    , cFormVar="w_EKTIPSIN",RowSource=""+"Nessun risultato,"+"Primo record,"+"Massimo,"+"Minimo,"+"Media,"+"Mediana,"+"Conteggio,"+"Somma", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEKTIPSIN_1_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..EKTIPSIN,&i_cF..t_EKTIPSIN),this.value)
    return(iif(xVal =1,'NUL',;
    iif(xVal =2,'FST',;
    iif(xVal =3,'MAX',;
    iif(xVal =4,'MIN',;
    iif(xVal =5,'AVG',;
    iif(xVal =6,'MED',;
    iif(xVal =7,'CNT',;
    iif(xVal =8,'SUM',;
    space(3))))))))))
  endfunc
  func oEKTIPSIN_1_13.GetRadio()
    this.Parent.oContained.w_EKTIPSIN = this.RadioValue()
    return .t.
  endfunc

  func oEKTIPSIN_1_13.ToRadio()
    this.Parent.oContained.w_EKTIPSIN=trim(this.Parent.oContained.w_EKTIPSIN)
    return(;
      iif(this.Parent.oContained.w_EKTIPSIN=='NUL',1,;
      iif(this.Parent.oContained.w_EKTIPSIN=='FST',2,;
      iif(this.Parent.oContained.w_EKTIPSIN=='MAX',3,;
      iif(this.Parent.oContained.w_EKTIPSIN=='MIN',4,;
      iif(this.Parent.oContained.w_EKTIPSIN=='AVG',5,;
      iif(this.Parent.oContained.w_EKTIPSIN=='MED',6,;
      iif(this.Parent.oContained.w_EKTIPSIN=='CNT',7,;
      iif(this.Parent.oContained.w_EKTIPSIN=='SUM',8,;
      0)))))))))
  endfunc

  func oEKTIPSIN_1_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oEKTIPSIN_1_13.mCond()
    with this.Parent.oContained
      return (.w_EKTIPRES<>'T')
    endwith
  endfunc


  add object oEKBENCHM_1_14 as StdCombo with uid="RYVMJERQSQ",rtseq=14,rtrep=.f.,left=116,top=178,width=138,height=21;
    , ToolTipText = "Tipo benchmark";
    , HelpContextID = 195023725;
    , cFormVar="w_EKBENCHM",RowSource=""+"Nessuno,"+"Massimo,"+"Minimo,"+"Media,"+"Mediana,"+"Conteggio,"+"Somma,"+"Valore fisso,"+"Link al risultato sintetico,"+"Link al set di risultati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEKBENCHM_1_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..EKBENCHM,&i_cF..t_EKBENCHM),this.value)
    return(iif(xVal =1,'NUL',;
    iif(xVal =2,'MAX',;
    iif(xVal =3,'MIN',;
    iif(xVal =4,'AVG',;
    iif(xVal =5,'MED',;
    iif(xVal =6,'CNT',;
    iif(xVal =7,'SUM',;
    iif(xVal =8,'FIX',;
    iif(xVal =9,'LSI',;
    iif(xVal =10,'LRE',;
    space(3))))))))))))
  endfunc
  func oEKBENCHM_1_14.GetRadio()
    this.Parent.oContained.w_EKBENCHM = this.RadioValue()
    return .t.
  endfunc

  func oEKBENCHM_1_14.ToRadio()
    this.Parent.oContained.w_EKBENCHM=trim(this.Parent.oContained.w_EKBENCHM)
    return(;
      iif(this.Parent.oContained.w_EKBENCHM=='NUL',1,;
      iif(this.Parent.oContained.w_EKBENCHM=='MAX',2,;
      iif(this.Parent.oContained.w_EKBENCHM=='MIN',3,;
      iif(this.Parent.oContained.w_EKBENCHM=='AVG',4,;
      iif(this.Parent.oContained.w_EKBENCHM=='MED',5,;
      iif(this.Parent.oContained.w_EKBENCHM=='CNT',6,;
      iif(this.Parent.oContained.w_EKBENCHM=='SUM',7,;
      iif(this.Parent.oContained.w_EKBENCHM=='FIX',8,;
      iif(this.Parent.oContained.w_EKBENCHM=='LSI',9,;
      iif(this.Parent.oContained.w_EKBENCHM=='LRE',10,;
      0)))))))))))
  endfunc

  func oEKBENCHM_1_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oEKBENFIX_1_17 as StdField with uid="MUKDUHGIQA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_EKBENFIX", cQueryName = "EKBENFIX",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore fisso da utilizzare come benchmark",;
    HelpContextID = 144692066,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=296, Top=178, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oEKBENFIX_1_17.mCond()
    with this.Parent.oContained
      return (.w_EKBENCHM=='FIX')
    endwith
  endfunc

  func oEKBENFIX_1_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EKBENCHM<>'FIX')
    endwith
    endif
  endfunc

  add object oEKBENCOD_1_18 as StdField with uid="HBQOPHGREV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_EKBENCOD", cQueryName = "EKBENCOD",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Elaborazione inesistente o senza risultato sintetico",;
    ToolTipText = "Codice dell'elaborazione da cui recuperare i valori da usare come benchmark",;
    HelpContextID = 73411722,;
   bGlobalFont=.t.,;
    Height=21, Width=98, Left=296, Top=178, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ELABKPIM", oKey_1_1="EKCODICE", oKey_1_2="this.w_EKBENCOD"

  func oEKBENCOD_1_18.mCond()
    with this.Parent.oContained
      return (.w_EKBENCHM=='LSI' or .w_EKBENCHM=='LRE')
    endwith
  endfunc

  func oEKBENCOD_1_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EKBENCHM<>'LSI' and .w_EKBENCHM<>'LRE')
    endwith
    endif
  endfunc

  func oEKBENCOD_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oEKBENCOD_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEKBENCOD_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ELABKPIM','*','EKCODICE',cp_AbsName(this.parent,'oEKBENCOD_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elaborazioni",'GSUT_MEK.ELABKPIM_VZM',this.parent.oContained
  endproc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=204, width=757,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="CPROWORD",Label1="Riga",Field2="EK_PARAM",Label2="Parametro",Field3="EKVALORE",Label3="Valore",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 15866246

  add object oDESELA_1_29 as StdField with uid="YLAQGZVNTQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESELA", cQueryName = "DESELA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione elaborazione KPI",;
    HelpContextID = 37828150,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=396, Top=178, InputMask=replicate('X',100)

  func oDESELA_1_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EKBENCHM<>'LSI' and .w_EKBENCHM<>'LRE')
    endwith
    endif
  endfunc


  add object oEKFRQEXE_1_33 as StdCombo with uid="XQECVHXLNN",rtseq=22,rtrep=.f.,left=578,top=130,width=120,height=21;
    , ToolTipText = "Frequenza di esecuzione dell'elaborazione";
    , HelpContextID = 157455221;
    , cFormVar="w_EKFRQEXE",RowSource=""+"1 min,"+"5 min,"+"10 min,"+"30 min,"+"1 ora,"+"2 ore,"+"4 ore,"+"8 ore,"+"12 ore,"+"1 giorno,"+"2 giorni,"+"3 giorni,"+"5 giorni,"+"7 giorni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEKFRQEXE_1_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..EKFRQEXE,&i_cF..t_EKFRQEXE),this.value)
    return(iif(xVal =1,60,;
    iif(xVal =2,300,;
    iif(xVal =3,600,;
    iif(xVal =4,1800,;
    iif(xVal =5,3600,;
    iif(xVal =6,7200,;
    iif(xVal =7,14400,;
    iif(xVal =8,28800,;
    iif(xVal =9,43200,;
    iif(xVal =10,86400,;
    iif(xVal =11,172800,;
    iif(xVal =12,259200,;
    iif(xVal =13,432000,;
    iif(xVal =14,604800,;
    0)))))))))))))))
  endfunc
  func oEKFRQEXE_1_33.GetRadio()
    this.Parent.oContained.w_EKFRQEXE = this.RadioValue()
    return .t.
  endfunc

  func oEKFRQEXE_1_33.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_EKFRQEXE==60,1,;
      iif(this.Parent.oContained.w_EKFRQEXE==300,2,;
      iif(this.Parent.oContained.w_EKFRQEXE==600,3,;
      iif(this.Parent.oContained.w_EKFRQEXE==1800,4,;
      iif(this.Parent.oContained.w_EKFRQEXE==3600,5,;
      iif(this.Parent.oContained.w_EKFRQEXE==7200,6,;
      iif(this.Parent.oContained.w_EKFRQEXE==14400,7,;
      iif(this.Parent.oContained.w_EKFRQEXE==28800,8,;
      iif(this.Parent.oContained.w_EKFRQEXE==43200,9,;
      iif(this.Parent.oContained.w_EKFRQEXE==86400,10,;
      iif(this.Parent.oContained.w_EKFRQEXE==172800,11,;
      iif(this.Parent.oContained.w_EKFRQEXE==259200,12,;
      iif(this.Parent.oContained.w_EKFRQEXE==432000,13,;
      iif(this.Parent.oContained.w_EKFRQEXE==604800,14,;
      0)))))))))))))))
  endfunc

  func oEKFRQEXE_1_33.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oEKWRTLOG_1_35 as StdCheck with uid="VALIKNZBSJ",rtseq=23,rtrep=.f.,left=701, top=132, caption="Attiva log",;
    ToolTipText = "Se attivo registra il log dell'elaborazione",;
    HelpContextID = 231636109,;
    cFormVar="w_EKWRTLOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEKWRTLOG_1_35.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..EKWRTLOG,&i_cF..t_EKWRTLOG),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oEKWRTLOG_1_35.GetRadio()
    this.Parent.oContained.w_EKWRTLOG = this.RadioValue()
    return .t.
  endfunc

  func oEKWRTLOG_1_35.ToRadio()
    this.Parent.oContained.w_EKWRTLOG=trim(this.Parent.oContained.w_EKWRTLOG)
    return(;
      iif(this.Parent.oContained.w_EKWRTLOG=='S',1,;
      0))
  endfunc

  func oEKWRTLOG_1_35.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oEKNUMRES_1_36 as StdField with uid="MORWQXMAOL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_EKNUMRES", cQueryName = "EKNUMRES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero massimo di risultati da mantenere su db (se 0 li mantiene tutti)",;
    HelpContextID = 211751783,;
   bGlobalFont=.t.,;
    Height=20, Width=120, Left=578, Top=154, cSayPict='"99999"', cGetPict='"99999"'


  add object oBtn_1_40 as StdButton with uid="KPAVOWNDAA",left=700, top=104, width=25,height=25,;
    CpPicture="exe\bmp\apertura.ico", caption="", nPag=1;
    , ToolTipText = "Apre la query selezionata";
    , HelpContextID = 212291846;
    , ImgSize=16;
  , bGlobalFont=.t.

    proc oBtn_1_40.Click()
      with this.Parent.oContained
        vq_build()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_15 as StdString with uid="BEOKTLUWKA",Visible=.t., Left=249, Top=180,;
    Alignment=1, Width=47, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_EKBENCHM<>'LSI' and .w_EKBENCHM<>'LRE')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="EJSOCZQWBP",Visible=.t., Left=249, Top=180,;
    Alignment=1, Width=47, Height=18,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_EKBENCHM<>'FIX')
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="SGGDIXLTLJ",Visible=.t., Left=59, Top=10,;
    Alignment=1, Width=54, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="IRBYWULEKV",Visible=.t., Left=32, Top=35,;
    Alignment=1, Width=81, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="IMFYJXBSMC",Visible=.t., Left=43, Top=62,;
    Alignment=1, Width=70, Height=18,;
    Caption="Tipo filtro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="YWQDIHUXQW",Visible=.t., Left=39, Top=84,;
    Alignment=1, Width=74, Height=18,;
    Caption="Attivazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="UGZXNMLFLU",Visible=.t., Left=70, Top=106,;
    Alignment=1, Width=43, Height=18,;
    Caption="Query:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="KENSXCDQJA",Visible=.t., Left=34, Top=133,;
    Alignment=1, Width=79, Height=18,;
    Caption="Tipo risultato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="VOHMIRUYUR",Visible=.t., Left=6, Top=155,;
    Alignment=1, Width=107, Height=18,;
    Caption="Risultato sintetico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="LNPFFLQWEP",Visible=.t., Left=17, Top=179,;
    Alignment=1, Width=96, Height=18,;
    Caption="Tipo benchmark:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="BSSSNCGXLO",Visible=.t., Left=404, Top=62,;
    Alignment=1, Width=89, Height=18,;
    Caption="Visualizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="XBOCETCAFE",Visible=.t., Left=473, Top=133,;
    Alignment=1, Width=103, Height=18,;
    Caption="Freq. esecuzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="VTEZWOLIBY",Visible=.t., Left=477, Top=155,;
    Alignment=1, Width=100, Height=18,;
    Caption="Numero risultati:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=223,;
    width=753+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=224,width=752+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_mekBodyRow as CPBodyRowCnt
  Width=743
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_2 as StdTrsField with uid="LJGYUEPTSR",rtseq=18,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 251973270,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oEK_PARAM_2_3 as StdTrsField with uid="IGCBIILLQG",rtseq=19,rtrep=.t.,;
    cFormVar="w_EK_PARAM",value=space(20),;
    HelpContextID = 224592749,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=205, Left=49, Top=0, InputMask=replicate('X',20)

  add object oEKVALORE_2_4 as StdTrsField with uid="UVKUMFOWQZ",rtseq=20,rtrep=.t.,;
    cFormVar="w_EKVALORE",value=space(250),;
    HelpContextID = 264409973,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=481, Left=257, Top=0, InputMask=replicate('X',250)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_2.When()
    return(.t.)
  proc oCPROWORD_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mek','ELABKPIM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".EKCODICE=ELABKPIM.EKCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
