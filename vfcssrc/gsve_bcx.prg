* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bcx                                                        *
*              Conferma documenti                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_371]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-08                                                      *
* Last revis.: 2008-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bcx",oParentObject)
return(i_retval)

define class tgsve_bcx as StdBatch
  * --- Local variables
  w_oERRORLOG = .NULL.
  w_OSERIAL = space(10)
  w_BANNOS = space(15)
  w_BANAPP = space(10)
  w_FLSALD = space(1)
  w_ACCSUC = 0
  w_CONCOR = space(25)
  w_PTNUMCOR = space(25)
  w_PTDESRIG = space(50)
  w_SCCODICE = space(10)
  w_SCCODSEC = 0
  w_CPROWNUM = 0
  w_ROWNUM = 0
  w_SCDESCRI = space(35)
  w_SCTIPCLF = space(1)
  w_SCTIPSCA = space(1)
  w_SCCODCLF = space(15)
  w_SCDATDOC = ctod("  /  /  ")
  w_SCNUMDOC = 0
  w_SCALFDOC = space(10)
  w_SCCODVAL = space(3)
  w_SCCAOVAL = 0
  w_SCVALNAZ = space(3)
  w_SCIMPSCA = 0
  w_SCCODPAG = space(5)
  w_SCDATREG = ctod("  /  /  ")
  w_APPO = space(10)
  w_APPO1 = 0
  w_APPO2 = 0
  w_SEZCLF = space(1)
  w_APPSEZ = space(1)
  w_TIPDOC = space(2)
  w_CODESE = space(4)
  w_DECTOT = 0
  w_OKDOC = 0
  w_NUDOC = 0
  w_DATBLO = ctod("  /  /  ")
  w_FLERR = .f.
  w_FLRICE = space(1)
  w_ACCPRE = 0
  w_ACCONT = 0
  w_PARFOR = 0
  w_RESTO = 0
  w_ABDIFF = 0
  w_ROWINI = 0
  w_TROPAR = .f.
  w_TOTPAR = 0
  w_VALRIT = 0
  w_VALENA = 0
  w_MESS = space(90)
  w_MESS1 = space(10)
  w_TROV = .f.
  w_CODAZI = space(5)
  w_PTNUMPAR = space(31)
  w_PTDATSCA = ctod("  /  /  ")
  w_PTFLSOSP = space(1)
  w_PTMODPAG = space(10)
  w_PTTOTIMP = 0
  w_PTBANAPP = space(10)
  w_PTBANNOS = space(15)
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  PAR_TITE_idx=0
  AZIENDA_idx=0
  SCA_VARI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conferma Documenti (da GSVE_KCD) 
    * --- Parametri dalla Maschera
    if this.oParentObject.w_FLVEAC="V" AND EMPTY(this.oParentObject.w_CONCLI) AND NOT IsAlt()
      ah_ErrorMsg("Contropartita crediti v.clienti non definita")
      i_retcode = 'stop'
      return
    endif
    * --- Carica i Documenti da Contabilizzare
    ah_Msg("Ricerca documenti da confermare...")
    vq_exec("query\GSVE_QCX.VQR",this,"CONTADOC")
    if USED("CONTADOC")
      * --- Inizio Aggiornamento vero e proprio
      this.w_NUDOC = 0
      this.w_OKDOC = 0
      if RECCOUNT("CONTADOC") > 0
        ah_Msg("Inizio fase di generazione...")
        * --- Fase di Blocco
        this.w_CODAZI = i_CODAZI
        * --- Try
        local bErr_03CCB7A0
        bErr_03CCB7A0=bTrsErr
        this.Try_03CCB7A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg(i_errmsg,,"")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_03CCB7A0
        * --- End
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Rimuovo i blocchi
        * --- Try
        local bErr_03ADB6D8
        bErr_03ADB6D8=bTrsErr
        this.Try_03ADB6D8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Impossibile rimuovere i blocchi. Rimuovere semaforo dai dati azienda")
        endif
        bTrsErr=bTrsErr or bErr_03ADB6D8
        * --- End
        this.w_APPO = "Operazione completata%0N. %1 documenti confermati%0su %2 documenti da confermare"
        ah_ErrorMsg(this.w_APPO,,"",ALLTRIM(STR(this.w_OKDOC)), ALLTRIM(STR(this.w_NUDOC)) )
        * --- LOG Errori
        if this.w_NUDOC>0 AND this.w_OKDOC<>this.w_NUDOC
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
        endif
      else
        this.w_APPO = "Per l'intervallo selezionato non esistono documenti da confermare"
        ah_ErrorMsg(this.w_APPO)
      endif
      * --- Chiudo la Maschera
      This.bUpdateParentObject=.f.
      This.oParentObject.bUpdated=.f.
      This.oParentObject.ecpQuit()
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_03CCB7A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_DATBLO)
      * --- Inserisce data di <Blocco> 
      * --- Write into AZIENDA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'AZIENDA','AZDATBLO');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               )
      else
        update (i_cTable) set;
            AZDATBLO = this.oParentObject.w_DATFIN;
            &i_ccchkf. ;
         where;
            AZCODAZI = this.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- UN altro utente ha impostato il blocco - controllo concorrenza
      this.w_MESS = "Elaborazione bloccata - verificare semaforo in dati azienda -. Impossibile contabilizzare"
      * --- Raise
      i_Error=this.w_MESS
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03ADB6D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera i Documenti da Contabilizzare
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Testa il Cambio di Documento
    this.w_OSERIAL = REPL("#", 10)
    SELECT CONTADOC
    GO TOP
    SCAN FOR NOT EMPTY(NVL(MVSERIAL," "))
    * --- Testa Cambio Documento
    if this.w_OSERIAL<>MVSERIAL
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT CONTADOC
      * --- Inizializza i dati di Testata della Nuova Registrazione 
      this.w_SCDESCRI = SPACE(35)
      this.w_SCNUMDOC = NVL(MVNUMDOC, 0)
      this.w_SCALFDOC = NVL(MVALFDOC, Space(10))
      this.w_SCDATDOC = CP_TODATE(MVDATDOC)
      this.w_SCDATREG = CP_TODATE(MVDATREG)
      this.w_SCVALNAZ = NVL(MVVALNAZ, g_PERVAL)
      this.w_SCCODVAL = NVL(MVCODVAL, g_PERVAL)
      this.w_SCCAOVAL = NVL(MVCAOVAL, 1)
      this.w_SCCODPAG = NVL(MVCODPAG, SPACE(5))
      this.w_CODESE = NVL(MVCODESE, SPACE(4))
      this.w_TIPDOC = NVL(MVCLADOC, "  ")
      this.w_FLRICE = IIF(this.w_TIPDOC="FA" AND NVL(TDFLRICE," ")="S", "S", " ")
      if this.w_TIPDOC="RF"
        this.w_SCTIPCLF = "G"
        this.w_SCCODCLF = this.oParentObject.w_CONCLI
      else
        this.w_SCTIPCLF = NVL(MVTIPCON, " ")
        this.w_SCCODCLF = NVL(MVCODCON, SPACE(15))
      endif
      this.w_VALRIT = NVL(MVTOTRIT, 0)
      this.w_VALENA = NVL(MVTOTENA, 0)
      this.w_BANNOS = NVL(MVCODBA2, SPACE(15))
      this.w_BANAPP = NVL(MVCODBAN, SPACE(10))
      this.w_PTBANNOS = this.w_BANNOS
      this.w_PTBANAPP = this.w_BANAPP
      this.w_DECTOT = NVL(VADECTOT, 0)
      this.w_CONCOR = NVL(MVNUMCOR, SPACE(25))
      this.w_FLSALD = NVL(MVFLSALD, " ")
      this.w_ACCONT = NVL(MVACCONT, 0)
      this.w_ACCPRE = NVL(MVACCPRE, 0)
      this.w_ACCSUC = NVL(MVACCSUC,0)
      this.w_SCIMPSCA = 0
      FOR L_i = 1 TO 6
      y = ALLTRIM(STR(L_i))
      this.w_APPO = NVL(MVAFLOM&y," ")
      this.w_APPO1 = NVL(MVAIMPN&y, 0)
      this.w_APPO2 = NVL(MVAIMPS&y, 0)
      * --- Totale Documento (in Valuta)  (Le righe Sconto Merce non vengono memorizzate)
      this.w_SCIMPSCA = this.w_SCIMPSCA + (IIF(this.w_APPO $ "IE", 0, this.w_APPO1) + IIF(this.w_APPO = "E", 0, this.w_APPO2))
      ENDFOR
      * --- Sezione Cli/For (Dare/Avere)
      if this.oParentObject.w_FLVEAC="V"
        this.w_SEZCLF = IIF(this.w_TIPDOC = "NC", "A", "D")
      else
        this.w_SEZCLF = IIF(this.w_TIPDOC = "NC", "D", "A")
      endif
    endif
    SELECT CONTADOC
    this.w_TROV = .T.
    this.w_OSERIAL = MVSERIAL
    ENDSCAN
    * --- Testa l'Ultima Uscita
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Cursori di Appoggio
    if USED("CONTADOC")
      SELECT CONTADOC
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non e' il Primo Ingresso, Scrive la Nuova Registrazione Contabile
    if this.w_OSERIAL <> REPL("#", 10) AND this.w_TROV=.T.
      * --- Inizializza Messaggistica di Errore
      this.w_FLERR = .F.
      * --- Calcola SERIAL
      this.w_SCCODICE = SPACE(10)
      this.w_SCCODSEC = -1
      this.w_SCTIPSCA = IIF(this.w_SEZCLF="D", "C", "F")
      this.w_PARFOR = 0
      this.w_NUDOC = this.w_NUDOC + 1
      * --- Try
      local bErr_03D51230
      bErr_03D51230=bTrsErr
      this.Try_03D51230()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if this.w_FLERR=.F.
          * --- Errore non Compreso nei Controlli
          this.w_oERRORLOG.AddMsgLog("Verificare doc. n. %1 del %2", ALLTRIM(STR(this.w_SCNUMDOC,15))+IIF(EMPTY(this.w_SCALFDOC),"","/"+Alltrim(this.w_SCALFDOC)) , DTOC(this.w_SCDATDOC) )     
          this.w_oERRORLOG.AddMsgLogNoTranslate(space(50))     
        endif
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_03D51230
      * --- End
    endif
    * --- Reinizializza la Registrazione
    this.w_TROV = .F.
  endproc
  proc Try_03D51230()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.SCA_VARI_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SESCA", "i_codazi,w_SCCODICE")
    if EMPTY(this.w_SCCODCLF) OR EMPTY (this.w_SCTIPCLF)
      this.w_oERRORLOG.AddMsgLog("Verificare doc. n. %1 del %2", ALLTRIM(STR(this.w_SCNUMDOC,15))+IIF(EMPTY(this.w_SCALFDOC),"","/"+Alltrim(this.w_SCALFDOC)) , DTOC(this.w_SCDATDOC) )     
      if this.oParentObject.w_FLVEAC="V"
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(10), "Cliente non definito")     
      else
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(10), "Fornitore non definito")     
      endif
      this.w_oERRORLOG.AddMsgLogNoTranslate(space(50))     
      * --- Abbandona la Registrazione
      this.w_FLERR = .T.
      * --- Raise
      i_Error="Errore"
      return
    else
      ah_Msg("Scrivo reg. n.: %1 del %2",.T.,.F.,.F., this.w_SCCODICE, DTOC(this.w_SCDATDOC) )
      * --- Scrive la Testata
      if this.w_TIPDOC="RF"
        if this.w_ACCSUC<>0
          this.w_SCIMPSCA = IIF(this.w_FLSALD="S", this.w_ACCSUC, this.w_SCIMPSCA - this.w_ACCONT)
        else
          this.w_SCIMPSCA = IIF(this.w_FLSALD="S", 0, this.w_SCIMPSCA - this.w_ACCONT)
        endif
      endif
      * --- Insert into SCA_VARI
      i_nConn=i_TableProp[this.SCA_VARI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SCA_VARI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"SCCODICE"+",SCCODSEC"+",SCDESCRI"+",SCTIPSCA"+",SCTIPCLF"+",SCCODCLF"+",SCNUMDOC"+",SCALFDOC"+",SCDATDOC"+",SCVALNAZ"+",SCCODVAL"+",SCCAOVAL"+",SCIMPSCA"+",SCFLIMPG"+",SCCODPAG"+",SCDATVAL"+",SCCODBAN"+",SCBANNOS"+",SCRIFDCO"+",SCDATREG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_SCCODICE),'SCA_VARI','SCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODSEC),'SCA_VARI','SCCODSEC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCDESCRI),'SCA_VARI','SCDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCTIPSCA),'SCA_VARI','SCTIPSCA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCTIPCLF),'SCA_VARI','SCTIPCLF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODCLF),'SCA_VARI','SCCODCLF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCNUMDOC),'SCA_VARI','SCNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCALFDOC),'SCA_VARI','SCALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'SCA_VARI','SCDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCVALNAZ),'SCA_VARI','SCVALNAZ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODVAL),'SCA_VARI','SCCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'SCA_VARI','SCCAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCIMPSCA),'SCA_VARI','SCIMPSCA');
        +","+cp_NullLink(cp_ToStrODBC("G"),'SCA_VARI','SCFLIMPG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODPAG),'SCA_VARI','SCCODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'SCA_VARI','SCDATVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'SCA_VARI','SCCODBAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'SCA_VARI','SCBANNOS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'SCA_VARI','SCRIFDCO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATREG),'SCA_VARI','SCDATREG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_SCCODICE,'SCCODSEC',this.w_SCCODSEC,'SCDESCRI',this.w_SCDESCRI,'SCTIPSCA',this.w_SCTIPSCA,'SCTIPCLF',this.w_SCTIPCLF,'SCCODCLF',this.w_SCCODCLF,'SCNUMDOC',this.w_SCNUMDOC,'SCALFDOC',this.w_SCALFDOC,'SCDATDOC',this.w_SCDATDOC,'SCVALNAZ',this.w_SCVALNAZ,'SCCODVAL',this.w_SCCODVAL,'SCCAOVAL',this.w_SCCAOVAL)
        insert into (i_cTable) (SCCODICE,SCCODSEC,SCDESCRI,SCTIPSCA,SCTIPCLF,SCCODCLF,SCNUMDOC,SCALFDOC,SCDATDOC,SCVALNAZ,SCCODVAL,SCCAOVAL,SCIMPSCA,SCFLIMPG,SCCODPAG,SCDATVAL,SCCODBAN,SCBANNOS,SCRIFDCO,SCDATREG &i_ccchkf. );
           values (;
             this.w_SCCODICE;
             ,this.w_SCCODSEC;
             ,this.w_SCDESCRI;
             ,this.w_SCTIPSCA;
             ,this.w_SCTIPCLF;
             ,this.w_SCCODCLF;
             ,this.w_SCNUMDOC;
             ,this.w_SCALFDOC;
             ,this.w_SCDATDOC;
             ,this.w_SCVALNAZ;
             ,this.w_SCCODVAL;
             ,this.w_SCCAOVAL;
             ,this.w_SCIMPSCA;
             ,"G";
             ,this.w_SCCODPAG;
             ,this.w_SCDATDOC;
             ,this.w_PTBANAPP;
             ,this.w_PTBANNOS;
             ,this.w_OSERIAL;
             ,this.w_SCDATREG;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Scrive il Dettaglio: Riga Cliente/Fornitore
      * --- Importi Clienti c.Vendite e Abbuono
      this.w_APPSEZ = this.w_SEZCLF
      this.w_CPROWNUM = 0
      if this.w_FLRICE<>"S"
        * --- Aggiorna le Partite (Tranne Fattura C.Corrispettivi)
        this.w_PTNUMPAR = CANUMPAR("N", this.w_CODESE, this.w_SCNUMDOC, this.w_SCALFDOC)
        this.w_PTDATSCA = this.w_SCDATDOC
        this.w_PTFLSOSP = " "
        this.w_PTMODPAG = this.oParentObject.w_TIPPAB
        if this.w_ACCONT<>0 AND this.w_TIPDOC<>"RF"
          * --- Inverte la sezione se Negativo..
          this.w_APPSEZ = IIF(this.w_ACCONT<0, IIF(this.w_SEZCLF="D", "A", "D"), this.w_SEZCLF)
          this.w_PTTOTIMP = ABS(this.w_ACCONT)
          this.w_CPROWNUM = this.w_CPROWNUM + 1
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTTIPCON"+",PTNUMDOC"+",PTDATAPE"+",PTDATDOC"+",PTROWORD"+",PTSERIAL"+",PTCODCON"+",PTALFDOC"+",PTTOTIMP"+",PTNUMPAR"+",PTMODPAG"+",PTFLSOSP"+",PTDATSCA"+",PTBANNOS"+",PTBANAPP"+",PTIMPDOC"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",CPROWNUM"+",PT_SEGNO"+",PTTOTABB"+",PTFLCRSA"+",PTNUMDIS"+",PTFLRAGG"+",PTFLINDI"+",PTFLIMPE"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_SCTIPCLF),'PAR_TITE','PTTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCNUMDOC),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'PAR_TITE','PTDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODSEC),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODICE),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODCLF),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCALFDOC),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCIMPSCA),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODVAL),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
            +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
            +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTTIPCON',this.w_SCTIPCLF,'PTNUMDOC',this.w_SCNUMDOC,'PTDATAPE',this.w_SCDATDOC,'PTDATDOC',this.w_SCDATDOC,'PTROWORD',this.w_SCCODSEC,'PTSERIAL',this.w_SCCODICE,'PTCODCON',this.w_SCCODCLF,'PTALFDOC',this.w_SCALFDOC,'PTTOTIMP',this.w_PTTOTIMP,'PTNUMPAR',this.w_PTNUMPAR,'PTMODPAG',this.w_PTMODPAG,'PTFLSOSP',this.w_PTFLSOSP)
            insert into (i_cTable) (PTTIPCON,PTNUMDOC,PTDATAPE,PTDATDOC,PTROWORD,PTSERIAL,PTCODCON,PTALFDOC,PTTOTIMP,PTNUMPAR,PTMODPAG,PTFLSOSP,PTDATSCA,PTBANNOS,PTBANAPP,PTIMPDOC,PTCODVAL,PTCAOVAL,PTCAOAPE,CPROWNUM,PT_SEGNO,PTTOTABB,PTFLCRSA,PTNUMDIS,PTFLRAGG,PTFLINDI,PTFLIMPE &i_ccchkf. );
               values (;
                 this.w_SCTIPCLF;
                 ,this.w_SCNUMDOC;
                 ,this.w_SCDATDOC;
                 ,this.w_SCDATDOC;
                 ,this.w_SCCODSEC;
                 ,this.w_SCCODICE;
                 ,this.w_SCCODCLF;
                 ,this.w_SCALFDOC;
                 ,this.w_PTTOTIMP;
                 ,this.w_PTNUMPAR;
                 ,this.w_PTMODPAG;
                 ,this.w_PTFLSOSP;
                 ,this.w_PTDATSCA;
                 ,this.w_PTBANNOS;
                 ,this.w_PTBANAPP;
                 ,this.w_SCIMPSCA;
                 ,this.w_SCCODVAL;
                 ,this.w_SCCAOVAL;
                 ,this.w_SCCAOVAL;
                 ,this.w_CPROWNUM;
                 ,this.w_APPSEZ;
                 ,0;
                 ,"C";
                 ," ";
                 ," ";
                 ," ";
                 ,"  ";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if this.w_ACCPRE<>0
          * --- Inverte la sezione se Negativo..
          this.w_APPSEZ = IIF(this.w_ACCPRE<0, IIF(this.w_SEZCLF="D", "A", "D"), this.w_SEZCLF)
          this.w_PTTOTIMP = ABS(this.w_ACCPRE)
          this.w_CPROWNUM = this.w_CPROWNUM + 1
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTFLIMPE"+",PTFLRAGG"+",PTNUMDIS"+",PTFLINDI"+",PTFLCRSA"+",PTTOTABB"+",PT_SEGNO"+",CPROWNUM"+",PTIMPDOC"+",PTBANAPP"+",PTBANNOS"+",PTDATSCA"+",PTFLSOSP"+",PTMODPAG"+",PTNUMPAR"+",PTTOTIMP"+",PTALFDOC"+",PTCAOVAL"+",PTCAOAPE"+",PTCODCON"+",PTSERIAL"+",PTROWORD"+",PTCODVAL"+",PTDATDOC"+",PTDATAPE"+",PTNUMDOC"+",PTTIPCON"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
            +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
            +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCIMPSCA),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCALFDOC),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODCLF),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODICE),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODSEC),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODVAL),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'PAR_TITE','PTDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCNUMDOC),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCTIPCLF),'PAR_TITE','PTTIPCON');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTFLIMPE',"  ",'PTFLRAGG'," ",'PTNUMDIS'," ",'PTFLINDI'," ",'PTFLCRSA',"C",'PTTOTABB',0,'PT_SEGNO',this.w_APPSEZ,'CPROWNUM',this.w_CPROWNUM,'PTIMPDOC',this.w_SCIMPSCA,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS,'PTDATSCA',this.w_PTDATSCA)
            insert into (i_cTable) (PTFLIMPE,PTFLRAGG,PTNUMDIS,PTFLINDI,PTFLCRSA,PTTOTABB,PT_SEGNO,CPROWNUM,PTIMPDOC,PTBANAPP,PTBANNOS,PTDATSCA,PTFLSOSP,PTMODPAG,PTNUMPAR,PTTOTIMP,PTALFDOC,PTCAOVAL,PTCAOAPE,PTCODCON,PTSERIAL,PTROWORD,PTCODVAL,PTDATDOC,PTDATAPE,PTNUMDOC,PTTIPCON &i_ccchkf. );
               values (;
                 "  ";
                 ," ";
                 ," ";
                 ," ";
                 ,"C";
                 ,0;
                 ,this.w_APPSEZ;
                 ,this.w_CPROWNUM;
                 ,this.w_SCIMPSCA;
                 ,this.w_PTBANAPP;
                 ,this.w_PTBANNOS;
                 ,this.w_PTDATSCA;
                 ,this.w_PTFLSOSP;
                 ,this.w_PTMODPAG;
                 ,this.w_PTNUMPAR;
                 ,this.w_PTTOTIMP;
                 ,this.w_SCALFDOC;
                 ,this.w_SCCAOVAL;
                 ,this.w_SCCAOVAL;
                 ,this.w_SCCODCLF;
                 ,this.w_SCCODICE;
                 ,this.w_SCCODSEC;
                 ,this.w_SCCODVAL;
                 ,this.w_SCDATDOC;
                 ,this.w_SCDATDOC;
                 ,this.w_SCNUMDOC;
                 ,this.w_SCTIPCLF;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if this.w_ACCSUC<>0
          * --- Inverte la sezione se Negativo..
          this.w_APPSEZ = this.w_SEZCLF
          this.w_PTTOTIMP = ABS(this.w_ACCSUC)
          this.w_CPROWNUM = this.w_CPROWNUM + 1
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTFLIMPE"+",PTFLRAGG"+",PTNUMDIS"+",PTFLINDI"+",PTFLCRSA"+",PTTOTABB"+",PT_SEGNO"+",CPROWNUM"+",PTIMPDOC"+",PTBANAPP"+",PTBANNOS"+",PTDATSCA"+",PTFLSOSP"+",PTMODPAG"+",PTNUMPAR"+",PTTOTIMP"+",PTALFDOC"+",PTCAOVAL"+",PTCAOAPE"+",PTCODCON"+",PTSERIAL"+",PTROWORD"+",PTCODVAL"+",PTDATDOC"+",PTDATAPE"+",PTNUMDOC"+",PTTIPCON"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
            +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
            +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCIMPSCA),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCALFDOC),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODCLF),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODICE),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODSEC),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODVAL),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'PAR_TITE','PTDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCNUMDOC),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCTIPCLF),'PAR_TITE','PTTIPCON');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTFLIMPE',"  ",'PTFLRAGG'," ",'PTNUMDIS'," ",'PTFLINDI'," ",'PTFLCRSA',"C",'PTTOTABB',0,'PT_SEGNO',this.w_APPSEZ,'CPROWNUM',this.w_CPROWNUM,'PTIMPDOC',this.w_SCIMPSCA,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS,'PTDATSCA',this.w_PTDATSCA)
            insert into (i_cTable) (PTFLIMPE,PTFLRAGG,PTNUMDIS,PTFLINDI,PTFLCRSA,PTTOTABB,PT_SEGNO,CPROWNUM,PTIMPDOC,PTBANAPP,PTBANNOS,PTDATSCA,PTFLSOSP,PTMODPAG,PTNUMPAR,PTTOTIMP,PTALFDOC,PTCAOVAL,PTCAOAPE,PTCODCON,PTSERIAL,PTROWORD,PTCODVAL,PTDATDOC,PTDATAPE,PTNUMDOC,PTTIPCON &i_ccchkf. );
               values (;
                 "  ";
                 ," ";
                 ," ";
                 ," ";
                 ,"C";
                 ,0;
                 ,this.w_APPSEZ;
                 ,this.w_CPROWNUM;
                 ,this.w_SCIMPSCA;
                 ,this.w_PTBANAPP;
                 ,this.w_PTBANNOS;
                 ,this.w_PTDATSCA;
                 ,this.w_PTFLSOSP;
                 ,this.w_PTMODPAG;
                 ,this.w_PTNUMPAR;
                 ,this.w_PTTOTIMP;
                 ,this.w_SCALFDOC;
                 ,this.w_SCCAOVAL;
                 ,this.w_SCCAOVAL;
                 ,this.w_SCCODCLF;
                 ,this.w_SCCODICE;
                 ,this.w_SCCODSEC;
                 ,this.w_SCCODVAL;
                 ,this.w_SCDATDOC;
                 ,this.w_SCDATDOC;
                 ,this.w_SCNUMDOC;
                 ,this.w_SCTIPCLF;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        * --- Cicla Sulle rate Documento
        * --- Select from DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2],.t.,this.DOC_RATE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_RATE ";
              +" where RSSERIAL="+cp_ToStrODBC(this.w_OSERIAL)+"";
              +" order by RSDATRAT";
               ,"_Curs_DOC_RATE")
        else
          select * from (i_cTable);
           where RSSERIAL=this.w_OSERIAL;
           order by RSDATRAT;
            into cursor _Curs_DOC_RATE
        endif
        if used('_Curs_DOC_RATE')
          select _Curs_DOC_RATE
          locate for 1=1
          do while not(eof())
          this.w_PTDATSCA = _Curs_DOC_RATE.RSDATRAT
          this.w_PTFLSOSP = NVL(_Curs_DOC_RATE.RSFLSOSP, " ")
          this.w_PTMODPAG = NVL(_Curs_DOC_RATE.RSMODPAG, SPACE(10))
          * --- Inverte la sezione se Negativo..
          this.w_APPSEZ = IIF(NVL(_Curs_DOC_RATE.RSIMPRAT,0)<0, IIF(this.w_SEZCLF="D", "A", "D"), this.w_SEZCLF)
          this.w_PTTOTIMP = ABS(NVL(_Curs_DOC_RATE.RSIMPRAT, 0))
          * --- Totale Partite per Eventuale Abbinamento
          this.w_PARFOR = this.w_PARFOR + this.w_PTTOTIMP
          this.w_CPROWNUM = this.w_CPROWNUM + 1
          this.w_PTBANNOS = IIF(NOT EMPTY(NVL(_Curs_DOC_RATE.RSBANNOS, SPACE(15))),_Curs_DOC_RATE.RSBANNOS,this.w_BANNOS)
          this.w_PTBANAPP = IIF(NOT EMPTY(NVL(_Curs_DOC_RATE.RSBANAPP, SPACE(10))),_Curs_DOC_RATE.RSBANAPP,this.w_BANAPP)
          this.w_PTNUMCOR = IIF(NOT EMPTY(NVL(_Curs_DOC_RATE.RSCONCOR, SPACE(25))),_Curs_DOC_RATE.RSCONCOR,IIF(this.w_PTBANAPP=this.w_BANAPP,this.w_CONCOR,Space(25)))
          this.w_PTDESRIG = NVL(_Curs_DOC_RATE.RSDESRIG, "")
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTFLIMPE"+",PTFLINDI"+",PTNUMDIS"+",PTFLRAGG"+",PTTOTABB"+",PT_SEGNO"+",CPROWNUM"+",PTFLCRSA"+",PTBANAPP"+",PTBANNOS"+",PTDATSCA"+",PTFLSOSP"+",PTMODPAG"+",PTNUMPAR"+",PTTOTIMP"+",PTALFDOC"+",PTCAOVAL"+",PTCAOAPE"+",PTCODCON"+",PTSERIAL"+",PTROWORD"+",PTCODVAL"+",PTDATDOC"+",PTDATAPE"+",PTIMPDOC"+",PTNUMDOC"+",PTTIPCON"+",PTDESRIG"+",PTNUMCOR"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
            +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCALFDOC),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODCLF),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODICE),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODSEC),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODVAL),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'PAR_TITE','PTDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCIMPSCA),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCNUMDOC),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SCTIPCLF),'PAR_TITE','PTTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTFLIMPE',"  ",'PTFLINDI'," ",'PTNUMDIS'," ",'PTFLRAGG'," ",'PTTOTABB',0,'PT_SEGNO',this.w_APPSEZ,'CPROWNUM',this.w_CPROWNUM,'PTFLCRSA',"C",'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS,'PTDATSCA',this.w_PTDATSCA,'PTFLSOSP',this.w_PTFLSOSP)
            insert into (i_cTable) (PTFLIMPE,PTFLINDI,PTNUMDIS,PTFLRAGG,PTTOTABB,PT_SEGNO,CPROWNUM,PTFLCRSA,PTBANAPP,PTBANNOS,PTDATSCA,PTFLSOSP,PTMODPAG,PTNUMPAR,PTTOTIMP,PTALFDOC,PTCAOVAL,PTCAOAPE,PTCODCON,PTSERIAL,PTROWORD,PTCODVAL,PTDATDOC,PTDATAPE,PTIMPDOC,PTNUMDOC,PTTIPCON,PTDESRIG,PTNUMCOR &i_ccchkf. );
               values (;
                 "  ";
                 ," ";
                 ," ";
                 ," ";
                 ,0;
                 ,this.w_APPSEZ;
                 ,this.w_CPROWNUM;
                 ,"C";
                 ,this.w_PTBANAPP;
                 ,this.w_PTBANNOS;
                 ,this.w_PTDATSCA;
                 ,this.w_PTFLSOSP;
                 ,this.w_PTMODPAG;
                 ,this.w_PTNUMPAR;
                 ,this.w_PTTOTIMP;
                 ,this.w_SCALFDOC;
                 ,this.w_SCCAOVAL;
                 ,this.w_SCCAOVAL;
                 ,this.w_SCCODCLF;
                 ,this.w_SCCODICE;
                 ,this.w_SCCODSEC;
                 ,this.w_SCCODVAL;
                 ,this.w_SCDATDOC;
                 ,this.w_SCDATDOC;
                 ,this.w_SCIMPSCA;
                 ,this.w_SCNUMDOC;
                 ,this.w_SCTIPCLF;
                 ,this.w_PTDESRIG;
                 ,this.w_PTNUMCOR;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
            select _Curs_DOC_RATE
            continue
          enddo
          use
        endif
        * --- Aggiorna Erario C/Ritenute e/o Enasarco
        if this.w_VALRIT<>0 OR this.w_VALENA<>0
          * --- Aggiorna Partite a Saldo
          this.w_ABDIFF = this.w_VALRIT+this.w_VALENA
          this.w_TOTPAR = 0
          this.w_RESTO = this.w_ABDIFF
          this.w_ROWINI = 0
          this.w_TROPAR = .F.
          * --- Select from PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
                +" where PTSERIAL="+cp_ToStrODBC(this.w_SCCODICE)+" AND PTROWORD="+cp_ToStrODBC(this.w_SCCODSEC)+" AND CPROWNUM<50 AND PTTOTIMP<>0";
                +" order by CPROWNUM";
                 ,"_Curs_PAR_TITE")
          else
            select * from (i_cTable);
             where PTSERIAL=this.w_SCCODICE AND PTROWORD=this.w_SCCODSEC AND CPROWNUM<50 AND PTTOTIMP<>0;
             order by CPROWNUM;
              into cursor _Curs_PAR_TITE
          endif
          if used('_Curs_PAR_TITE')
            select _Curs_PAR_TITE
            locate for 1=1
            do while not(eof())
            * --- Cicla sulle Partite da Abbinare
            this.w_APPO1 = CPROWNUM
            if this.w_ACCONT=0 OR this.w_APPO1>1
              * --- Se c'e' un Acconto, scarta la prima riga Partite (Acconto)
              this.w_PTDATSCA = PTDATSCA
              this.w_PTNUMPAR = PTNUMPAR
              this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
              this.w_PTTOTIMP = NVL(PTTOTIMP, 0)
              this.w_PTMODPAG = PTMODPAG
              this.w_PTBANAPP = PTBANAPP
              * --- Puntatore alla Prima Riga buona delle Partite di origine (per eventuale resto)
              this.w_ROWINI = IIF(this.w_ROWINI=0, this.w_APPO1, this.w_ROWINI)
              * --- 'Spalma' sulle Partite di Apertura Altrimenti Salda per l'intero Importo
              this.w_PTTOTIMP = cp_ROUND((this.w_ABDIFF * this.w_PTTOTIMP) / this.w_PARFOR, this.w_DECTOT)
              this.w_RESTO = this.w_RESTO - this.w_PTTOTIMP
              this.w_CPROWNUM = this.w_CPROWNUM + 1
              * --- Insert into PAR_TITE
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PTFLIMPE"+",PTFLSOSP"+",PTFLINDI"+",PTFLRAGG"+",PTFLCRSA"+",PTTOTABB"+",PTNUMDIS"+",PT_SEGNO"+",CPROWNUM"+",PTIMPDOC"+",PTBANAPP"+",PTBANNOS"+",PTDATSCA"+",PTMODPAG"+",PTNUMPAR"+",PTTOTIMP"+",PTALFDOC"+",PTCAOVAL"+",PTCAOAPE"+",PTCODCON"+",PTSERIAL"+",PTROWORD"+",PTCODVAL"+",PTDATDOC"+",PTDATAPE"+",PTNUMDOC"+",PTTIPCON"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
                +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCIMPSCA),'PAR_TITE','PTIMPDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCALFDOC),'PAR_TITE','PTALFDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'PAR_TITE','PTCAOVAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCCAOVAL),'PAR_TITE','PTCAOAPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODCLF),'PAR_TITE','PTCODCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODICE),'PAR_TITE','PTSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODSEC),'PAR_TITE','PTROWORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODVAL),'PAR_TITE','PTCODVAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'PAR_TITE','PTDATDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCDATDOC),'PAR_TITE','PTDATAPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCNUMDOC),'PAR_TITE','PTNUMDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCTIPCLF),'PAR_TITE','PTTIPCON');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PTFLIMPE',"  ",'PTFLSOSP'," ",'PTFLINDI'," ",'PTFLRAGG'," ",'PTFLCRSA',"S",'PTTOTABB',0,'PTNUMDIS',SPACE(10),'PT_SEGNO',this.w_APPSEZ,'CPROWNUM',this.w_CPROWNUM,'PTIMPDOC',this.w_SCIMPSCA,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS)
                insert into (i_cTable) (PTFLIMPE,PTFLSOSP,PTFLINDI,PTFLRAGG,PTFLCRSA,PTTOTABB,PTNUMDIS,PT_SEGNO,CPROWNUM,PTIMPDOC,PTBANAPP,PTBANNOS,PTDATSCA,PTMODPAG,PTNUMPAR,PTTOTIMP,PTALFDOC,PTCAOVAL,PTCAOAPE,PTCODCON,PTSERIAL,PTROWORD,PTCODVAL,PTDATDOC,PTDATAPE,PTNUMDOC,PTTIPCON &i_ccchkf. );
                   values (;
                     "  ";
                     ," ";
                     ," ";
                     ," ";
                     ,"S";
                     ,0;
                     ,SPACE(10);
                     ,this.w_APPSEZ;
                     ,this.w_CPROWNUM;
                     ,this.w_SCIMPSCA;
                     ,this.w_PTBANAPP;
                     ,this.w_PTBANNOS;
                     ,this.w_PTDATSCA;
                     ,this.w_PTMODPAG;
                     ,this.w_PTNUMPAR;
                     ,this.w_PTTOTIMP;
                     ,this.w_SCALFDOC;
                     ,this.w_SCCAOVAL;
                     ,this.w_SCCAOVAL;
                     ,this.w_SCCODCLF;
                     ,this.w_SCCODICE;
                     ,this.w_SCCODSEC;
                     ,this.w_SCCODVAL;
                     ,this.w_SCDATDOC;
                     ,this.w_SCDATDOC;
                     ,this.w_SCNUMDOC;
                     ,this.w_SCTIPCLF;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              * --- Partita di Apertura
              * --- Write into PAR_TITE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PTTOTIMP =PTTOTIMP+ "+cp_ToStrODBC(this.w_PTTOTIMP);
                    +i_ccchkf ;
                +" where ";
                    +"PTSERIAL = "+cp_ToStrODBC(this.w_SCCODICE);
                    +" and PTROWORD = "+cp_ToStrODBC(this.w_SCCODSEC);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_APPO1);
                       )
              else
                update (i_cTable) set;
                    PTTOTIMP = PTTOTIMP + this.w_PTTOTIMP;
                    &i_ccchkf. ;
                 where;
                    PTSERIAL = this.w_SCCODICE;
                    and PTROWORD = this.w_SCCODSEC;
                    and CPROWNUM = this.w_APPO1;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.w_TROPAR = .T.
            endif
              select _Curs_PAR_TITE
              continue
            enddo
            use
          endif
          if this.w_RESTO<>0 AND this.w_TROPAR=.T.
            * --- Mette il resto sulla Prima Rata
            * --- Write into PAR_TITE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PTTOTIMP =PTTOTIMP+ "+cp_ToStrODBC(this.w_RESTO);
                  +i_ccchkf ;
              +" where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.w_SCCODICE);
                  +" and PTROWORD = "+cp_ToStrODBC(this.w_SCCODSEC);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  PTTOTIMP = PTTOTIMP + this.w_RESTO;
                  &i_ccchkf. ;
               where;
                  PTSERIAL = this.w_SCCODICE;
                  and PTROWORD = this.w_SCCODSEC;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Partita di Apertura
            * --- Write into PAR_TITE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PTTOTIMP =PTTOTIMP+ "+cp_ToStrODBC(this.w_RESTO);
                  +i_ccchkf ;
              +" where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.w_SCCODICE);
                  +" and PTROWORD = "+cp_ToStrODBC(this.w_SCCODSEC);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWINI);
                     )
            else
              update (i_cTable) set;
                  PTTOTIMP = PTTOTIMP + this.w_RESTO;
                  &i_ccchkf. ;
               where;
                  PTSERIAL = this.w_SCCODICE;
                  and PTROWORD = this.w_SCCODSEC;
                  and CPROWNUM = this.w_ROWINI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
      * --- Scrive Flag Confermato sul Documento Origine
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVRIFDCO ="+cp_NullLink(cp_ToStrODBC(this.w_SCCODICE),'DOC_MAST','MVRIFDCO');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
               )
      else
        update (i_cTable) set;
            MVRIFDCO = this.w_SCCODICE;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_OSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
      this.w_OKDOC = this.w_OKDOC + 1
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Locali
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_RATE'
    this.cWorkTables[3]='PAR_TITE'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='SCA_VARI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_DOC_RATE')
      use in _Curs_DOC_RATE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
