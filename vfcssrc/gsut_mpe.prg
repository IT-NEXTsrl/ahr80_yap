* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mpe                                                        *
*              Permessi classi documentali                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-26                                                      *
* Last revis.: 2011-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_mpe")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_mpe")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_mpe")
  return

* --- Class definition
define class tgsut_mpe as StdPCForm
  Width  = 726
  Height = 422
  Top    = 7
  Left   = 9
  cComment = "Permessi classi documentali"
  cPrg = "gsut_mpe"
  HelpContextID=147407721
  add object cnt as tcgsut_mpe
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_mpe as PCContext
  w_PECODCLA = space(10)
  w_CPROWORD = 0
  w_PETIPFIL = space(1)
  w_PECODUTE = 0
  w_PECODGRU = 0
  w_PECHKARC = space(1)
  w_DESUTE = space(20)
  w_DESGRU = space(20)
  w_DESUTEGRU = space(30)
  w_PECHKVIS = space(1)
  w_PECHKELI = space(1)
  proc Save(i_oFrom)
    this.w_PECODCLA = i_oFrom.w_PECODCLA
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_PETIPFIL = i_oFrom.w_PETIPFIL
    this.w_PECODUTE = i_oFrom.w_PECODUTE
    this.w_PECODGRU = i_oFrom.w_PECODGRU
    this.w_PECHKARC = i_oFrom.w_PECHKARC
    this.w_DESUTE = i_oFrom.w_DESUTE
    this.w_DESGRU = i_oFrom.w_DESGRU
    this.w_DESUTEGRU = i_oFrom.w_DESUTEGRU
    this.w_PECHKVIS = i_oFrom.w_PECHKVIS
    this.w_PECHKELI = i_oFrom.w_PECHKELI
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PECODCLA = this.w_PECODCLA
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_PETIPFIL = this.w_PETIPFIL
    i_oTo.w_PECODUTE = this.w_PECODUTE
    i_oTo.w_PECODGRU = this.w_PECODGRU
    i_oTo.w_PECHKARC = this.w_PECHKARC
    i_oTo.w_DESUTE = this.w_DESUTE
    i_oTo.w_DESGRU = this.w_DESGRU
    i_oTo.w_DESUTEGRU = this.w_DESUTEGRU
    i_oTo.w_PECHKVIS = this.w_PECHKVIS
    i_oTo.w_PECHKELI = this.w_PECHKELI
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsut_mpe as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 726
  Height = 422
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-08-26"
  HelpContextID=147407721
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PRO_PERM_IDX = 0
  PRODCLAS_IDX = 0
  PROMCLAS_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  cFile = "PRO_PERM"
  cKeySelect = "PECODCLA"
  cKeyWhere  = "PECODCLA=this.w_PECODCLA"
  cKeyDetail  = "PECODCLA=this.w_PECODCLA"
  cKeyWhereODBC = '"PECODCLA="+cp_ToStrODBC(this.w_PECODCLA)';

  cKeyDetailWhereODBC = '"PECODCLA="+cp_ToStrODBC(this.w_PECODCLA)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PRO_PERM.PECODCLA="+cp_ToStrODBC(this.w_PECODCLA)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PRO_PERM.CPROWORD '
  cPrg = "gsut_mpe"
  cComment = "Permessi classi documentali"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PECODCLA = space(10)
  w_CPROWORD = 0
  w_PETIPFIL = space(1)
  o_PETIPFIL = space(1)
  w_PECODUTE = 0
  w_PECODGRU = 0
  w_PECHKARC = space(1)
  w_DESUTE = space(20)
  w_DESGRU = space(20)
  w_DESUTEGRU = space(30)
  w_PECHKVIS = space(1)
  w_PECHKELI = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_mpePag1","gsut_mpe",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Permessi")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='PRODCLAS'
    this.cWorkTables[2]='PROMCLAS'
    this.cWorkTables[3]='CPUSERS'
    this.cWorkTables[4]='CPGROUPS'
    this.cWorkTables[5]='PRO_PERM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRO_PERM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRO_PERM_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_mpe'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PRO_PERM where PECODCLA=KeySet.PECODCLA
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PRO_PERM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_PERM_IDX,2],this.bLoadRecFilter,this.PRO_PERM_IDX,"gsut_mpe")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRO_PERM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRO_PERM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRO_PERM '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PECODCLA',this.w_PECODCLA  )
      select * from (i_cTable) PRO_PERM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PECODCLA = NVL(PECODCLA,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PRO_PERM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESUTE = space(20)
          .w_DESGRU = space(20)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PETIPFIL = NVL(PETIPFIL,space(1))
          .w_PECODUTE = NVL(PECODUTE,0)
          .link_2_3('Load')
          .w_PECODGRU = NVL(PECODGRU,0)
          .link_2_4('Load')
          .w_PECHKARC = NVL(PECHKARC,space(1))
        .w_DESUTEGRU = IIF(.w_PETIPFIL='U',.w_DESUTE,.w_DESGRU)
          .w_PECHKVIS = NVL(PECHKVIS,space(1))
          .w_PECHKELI = NVL(PECHKELI,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PECODCLA=space(10)
      .w_CPROWORD=10
      .w_PETIPFIL=space(1)
      .w_PECODUTE=0
      .w_PECODGRU=0
      .w_PECHKARC=space(1)
      .w_DESUTE=space(20)
      .w_DESGRU=space(20)
      .w_DESUTEGRU=space(30)
      .w_PECHKVIS=space(1)
      .w_PECHKELI=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_PECODUTE = IIF(.w_PETIPFIL='U',.w_PECODUTE, 0)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PECODUTE))
         .link_2_3('Full')
        endif
        .w_PECODGRU = IIF(.w_PETIPFIL='G',.w_PECODGRU, 0)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PECODGRU))
         .link_2_4('Full')
        endif
        .w_PECHKARC = 'N'
        .DoRTCalc(7,8,.f.)
        .w_DESUTEGRU = IIF(.w_PETIPFIL='U',.w_DESUTE,.w_DESGRU)
        .DoRTCalc(10,10,.f.)
        .w_PECHKELI = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRO_PERM')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PRO_PERM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRO_PERM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PECODCLA,"PECODCLA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_PETIPFIL N(3);
      ,t_PECODUTE N(4);
      ,t_PECODGRU N(4);
      ,t_PECHKARC N(3);
      ,t_DESUTEGRU C(30);
      ,t_PECHKVIS N(3);
      ,t_PECHKELI N(3);
      ,CPROWNUM N(10);
      ,t_DESUTE C(20);
      ,t_DESGRU C(20);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mpebodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.controlsource=this.cTrsName+'.t_PETIPFIL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECODUTE_2_3.controlsource=this.cTrsName+'.t_PECODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECODGRU_2_4.controlsource=this.cTrsName+'.t_PECODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_5.controlsource=this.cTrsName+'.t_PECHKARC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_8.controlsource=this.cTrsName+'.t_DESUTEGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKVIS_2_9.controlsource=this.cTrsName+'.t_PECHKVIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKELI_2_10.controlsource=this.cTrsName+'.t_PECHKELI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(55)
    this.AddVLine(145)
    this.AddVLine(204)
    this.AddVLine(261)
    this.AddVLine(515)
    this.AddVLine(576)
    this.AddVLine(646)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRO_PERM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_PERM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRO_PERM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_PERM_IDX,2])
      *
      * insert into PRO_PERM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRO_PERM')
        i_extval=cp_InsertValODBCExtFlds(this,'PRO_PERM')
        i_cFldBody=" "+;
                  "(PECODCLA,CPROWORD,PETIPFIL,PECODUTE,PECODGRU"+;
                  ",PECHKARC,PECHKVIS,PECHKELI,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PECODCLA)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_PETIPFIL)+","+cp_ToStrODBCNull(this.w_PECODUTE)+","+cp_ToStrODBCNull(this.w_PECODGRU)+;
             ","+cp_ToStrODBC(this.w_PECHKARC)+","+cp_ToStrODBC(this.w_PECHKVIS)+","+cp_ToStrODBC(this.w_PECHKELI)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRO_PERM')
        i_extval=cp_InsertValVFPExtFlds(this,'PRO_PERM')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PECODCLA',this.w_PECODCLA)
        INSERT INTO (i_cTable) (;
                   PECODCLA;
                  ,CPROWORD;
                  ,PETIPFIL;
                  ,PECODUTE;
                  ,PECODGRU;
                  ,PECHKARC;
                  ,PECHKVIS;
                  ,PECHKELI;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PECODCLA;
                  ,this.w_CPROWORD;
                  ,this.w_PETIPFIL;
                  ,this.w_PECODUTE;
                  ,this.w_PECODGRU;
                  ,this.w_PECHKARC;
                  ,this.w_PECHKVIS;
                  ,this.w_PECHKELI;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PRO_PERM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_PERM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) and not empty(t_PETIPFIL)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_PERM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_PERM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and not empty(t_PETIPFIL)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRO_PERM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_PERM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PETIPFIL="+cp_ToStrODBC(this.w_PETIPFIL)+;
                     ",PECODUTE="+cp_ToStrODBCNull(this.w_PECODUTE)+;
                     ",PECODGRU="+cp_ToStrODBCNull(this.w_PECODGRU)+;
                     ",PECHKARC="+cp_ToStrODBC(this.w_PECHKARC)+;
                     ",PECHKVIS="+cp_ToStrODBC(this.w_PECHKVIS)+;
                     ",PECHKELI="+cp_ToStrODBC(this.w_PECHKELI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_PERM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,PETIPFIL=this.w_PETIPFIL;
                     ,PECODUTE=this.w_PECODUTE;
                     ,PECODGRU=this.w_PECODGRU;
                     ,PECHKARC=this.w_PECHKARC;
                     ,PECHKVIS=this.w_PECHKVIS;
                     ,PECHKELI=this.w_PECHKELI;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRO_PERM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_PERM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and not empty(t_PETIPFIL)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PRO_PERM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and not empty(t_PETIPFIL)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRO_PERM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_PERM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_PETIPFIL<>.w_PETIPFIL
          .w_PECODUTE = IIF(.w_PETIPFIL='U',.w_PECODUTE, 0)
          .link_2_3('Full')
        endif
        if .o_PETIPFIL<>.w_PETIPFIL
          .w_PECODGRU = IIF(.w_PETIPFIL='G',.w_PECODGRU, 0)
          .link_2_4('Full')
        endif
        .DoRTCalc(6,8,.t.)
          .w_DESUTEGRU = IIF(.w_PETIPFIL='U',.w_DESUTE,.w_DESGRU)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESUTE with this.w_DESUTE
      replace t_DESGRU with this.w_DESGRU
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECODUTE_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECODUTE_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECODGRU_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECODGRU_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKVIS_2_9.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKVIS_2_9.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKELI_2_10.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKELI_2_10.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PECODUTE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PECODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PECODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PECODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PECODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oPECODUTE_2_3'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PECODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PECODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PECODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PECODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PECODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PECODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PECODGRU
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PECODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PECODGRU);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PECODGRU)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PECODGRU) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oPECODGRU_2_4'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PECODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PECODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PECODGRU)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PECODGRU = NVL(_Link_.CODE,0)
      this.w_DESGRU = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PECODGRU = 0
      endif
      this.w_DESGRU = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PECODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.RadioValue()==this.w_PETIPFIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.SetRadio()
      replace t_PETIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODUTE_2_3.value==this.w_PECODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODUTE_2_3.value=this.w_PECODUTE
      replace t_PECODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODUTE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODGRU_2_4.value==this.w_PECODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODGRU_2_4.value=this.w_PECODGRU
      replace t_PECODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODGRU_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_5.RadioValue()==this.w_PECHKARC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_5.SetRadio()
      replace t_PECHKARC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_8.value==this.w_DESUTEGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_8.value=this.w_DESUTEGRU
      replace t_DESUTEGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKVIS_2_9.RadioValue()==this.w_PECHKVIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKVIS_2_9.SetRadio()
      replace t_PECHKVIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKVIS_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKELI_2_10.RadioValue()==this.w_PECHKELI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKELI_2_10.SetRadio()
      replace t_PECHKELI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKELI_2_10.value
    endif
    cp_SetControlsValueExtFlds(this,'PRO_PERM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD)) and not empty(.w_PETIPFIL)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PETIPFIL = this.w_PETIPFIL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and not empty(t_PETIPFIL))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_PETIPFIL=space(1)
      .w_PECODUTE=0
      .w_PECODGRU=0
      .w_PECHKARC=space(1)
      .w_DESUTE=space(20)
      .w_DESGRU=space(20)
      .w_DESUTEGRU=space(30)
      .w_PECHKVIS=space(1)
      .w_PECHKELI=space(1)
      .DoRTCalc(1,3,.f.)
        .w_PECODUTE = IIF(.w_PETIPFIL='U',.w_PECODUTE, 0)
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_PECODUTE))
        .link_2_3('Full')
      endif
        .w_PECODGRU = IIF(.w_PETIPFIL='G',.w_PECODGRU, 0)
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_PECODGRU))
        .link_2_4('Full')
      endif
        .w_PECHKARC = 'N'
      .DoRTCalc(7,8,.f.)
        .w_DESUTEGRU = IIF(.w_PETIPFIL='U',.w_DESUTE,.w_DESGRU)
      .DoRTCalc(10,10,.f.)
        .w_PECHKELI = 'N'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_PETIPFIL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.RadioValue(.t.)
    this.w_PECODUTE = t_PECODUTE
    this.w_PECODGRU = t_PECODGRU
    this.w_PECHKARC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_5.RadioValue(.t.)
    this.w_DESUTE = t_DESUTE
    this.w_DESGRU = t_DESGRU
    this.w_DESUTEGRU = t_DESUTEGRU
    this.w_PECHKVIS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKVIS_2_9.RadioValue(.t.)
    this.w_PECHKELI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKELI_2_10.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PETIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.ToRadio()
    replace t_PECODUTE with this.w_PECODUTE
    replace t_PECODGRU with this.w_PECODGRU
    replace t_PECHKARC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_5.ToRadio()
    replace t_DESUTE with this.w_DESUTE
    replace t_DESGRU with this.w_DESGRU
    replace t_DESUTEGRU with this.w_DESUTEGRU
    replace t_PECHKVIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKVIS_2_9.ToRadio()
    replace t_PECHKELI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKELI_2_10.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_mpePag1 as StdContainer
  Width  = 722
  height = 422
  stdWidth  = 722
  stdheight = 422
  resizeXpos=369
  resizeYpos=167
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=2, width=705,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Seq.",Field2="PETIPFIL",Label2="Soggetto",Field3="PECODUTE",Label3="Utente",Field4="PECODGRU",Label4="Gruppo",Field5="DESUTEGRU",Label5="Descrizione",Field6="PECHKARC",Label6="Archivia",Field7="PECHKVIS",Label7="iif(g_DMIP<>'S', Ah_MsgFormat('Visualizza'),'')",Field8="PECHKELI",Label8="iif(g_DMIP<>'S', Ah_MsgFormat('Elimina'),'')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168683130

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=20,;
    width=701+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=21,width=700+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CPUSERS|CPGROUPS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CPUSERS'
        oDropInto=this.oBodyCol.oRow.oPECODUTE_2_3
      case cFile='CPGROUPS'
        oDropInto=this.oBodyCol.oRow.oPECODGRU_2_4
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_mpeBodyRow as CPBodyRowCnt
  Width=691
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="VMCUIGLUFG",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza",;
    HelpContextID = 201011562,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oPETIPFIL_2_2 as StdTrsCombo with uid="FXEZSUYKVH",rtrep=.t.,;
    cFormVar="w_PETIPFIL", RowSource=""+"Utente,"+"Gruppo" , ;
    HelpContextID = 226146494,;
    Height=22, Width=83, Left=48, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPETIPFIL_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PETIPFIL,&i_cF..t_PETIPFIL),this.value)
    return(iif(xVal =1,'U',;
    iif(xVal =2,'G',;
    space(1))))
  endfunc
  func oPETIPFIL_2_2.GetRadio()
    this.Parent.oContained.w_PETIPFIL = this.RadioValue()
    return .t.
  endfunc

  func oPETIPFIL_2_2.ToRadio()
    this.Parent.oContained.w_PETIPFIL=trim(this.Parent.oContained.w_PETIPFIL)
    return(;
      iif(this.Parent.oContained.w_PETIPFIL=='U',1,;
      iif(this.Parent.oContained.w_PETIPFIL=='G',2,;
      0)))
  endfunc

  func oPETIPFIL_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPECODUTE_2_3 as StdTrsField with uid="HZGGWJFOBA",rtseq=4,rtrep=.t.,;
    cFormVar="w_PECODUTE",value=0,;
    HelpContextID = 13252411,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=136, Top=0, cSayPict=["@Z 9999"], cGetPict=["@Z 9999"], bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_PECODUTE"

  func oPECODUTE_2_3.mCond()
    with this.Parent.oContained
      return (.w_PETIPFIL='U')
    endwith
  endfunc

  func oPECODUTE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPECODUTE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPECODUTE_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oPECODUTE_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oPECODGRU_2_4 as StdTrsField with uid="FXJSDZUIVQ",rtseq=5,rtrep=.t.,;
    cFormVar="w_PECODGRU",value=0,;
    HelpContextID = 46806859,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=193, Top=0, cSayPict=["@Z 9999"], cGetPict=["@Z 9999"], bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_PECODGRU"

  func oPECODGRU_2_4.mCond()
    with this.Parent.oContained
      return (.w_PETIPFIL='G')
    endwith
  endfunc

  func oPECODGRU_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPECODGRU_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPECODGRU_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oPECODGRU_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oPECHKARC_2_5 as StdTrsCheck with uid="DEHICGNVBC",rtrep=.t.,;
    cFormVar="w_PECHKARC",  caption="",;
    HelpContextID = 221460281,;
    Left=511, Top=0, Width=45,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKARC_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKARC,&i_cF..t_PECHKARC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKARC_2_5.GetRadio()
    this.Parent.oContained.w_PECHKARC = this.RadioValue()
    return .t.
  endfunc

  func oPECHKARC_2_5.ToRadio()
    this.Parent.oContained.w_PECHKARC=trim(this.Parent.oContained.w_PECHKARC)
    return(;
      iif(this.Parent.oContained.w_PECHKARC=='S',1,;
      0))
  endfunc

  func oPECHKARC_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDESUTEGRU_2_8 as StdTrsField with uid="OORAQWARDS",rtseq=9,rtrep=.t.,;
    cFormVar="w_DESUTEGRU",value=space(30),enabled=.f.,;
    HelpContextID = 237945896,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=245, Left=251, Top=0, InputMask=replicate('X',30)

  add object oPECHKVIS_2_9 as StdTrsCheck with uid="PINGTTOWGH",rtrep=.t.,;
    cFormVar="w_PECHKVIS",  caption="",;
    HelpContextID = 231524535,;
    Left=573, Top=0, Width=55,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKVIS_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKVIS,&i_cF..t_PECHKVIS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKVIS_2_9.GetRadio()
    this.Parent.oContained.w_PECHKVIS = this.RadioValue()
    return .t.
  endfunc

  func oPECHKVIS_2_9.ToRadio()
    this.Parent.oContained.w_PECHKVIS=trim(this.Parent.oContained.w_PECHKVIS)
    return(;
      iif(this.Parent.oContained.w_PECHKVIS=='S',1,;
      0))
  endfunc

  func oPECHKVIS_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKVIS_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DMIP='S')
    endwith
    endif
  endfunc

  add object oPECHKELI_2_10 as StdTrsCheck with uid="NGSJSBFLQV",rtrep=.t.,;
    cFormVar="w_PECHKELI",  caption="",;
    HelpContextID = 248301761,;
    Left=641, Top=0, Width=45,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKELI_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKELI,&i_cF..t_PECHKELI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKELI_2_10.GetRadio()
    this.Parent.oContained.w_PECHKELI = this.RadioValue()
    return .t.
  endfunc

  func oPECHKELI_2_10.ToRadio()
    this.Parent.oContained.w_PECHKELI=trim(this.Parent.oContained.w_PECHKELI)
    return(;
      iif(this.Parent.oContained.w_PECHKELI=='S',1,;
      0))
  endfunc

  func oPECHKELI_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKELI_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DMIP='S')
    endwith
    endif
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mpe','PRO_PERM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PECODCLA=PRO_PERM.PECODCLA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
