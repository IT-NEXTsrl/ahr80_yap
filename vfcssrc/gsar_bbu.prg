* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bbu                                                        *
*              Controllo vincolo integrita con strutt budget                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-15                                                      *
* Last revis.: 2008-02-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam,w_CODICE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bbu",oParentObject,m.pParam,m.w_CODICE)
return(i_retval)

define class tgsar_bbu as StdBatch
  * --- Local variables
  pParam = space(10)
  w_CODICE = space(40)
  w_ANCODICE = space(15)
  w_ANTIPCON = space(15)
  w_OK = .f.
  * --- WorkFile variables
  PAR_RIOR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if TYPE ("g_BUVE")="C" AND g_BUVE="S"
      GSBV1BBU(this,this.pParam,this.w_CODICE)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Esegue controlli in Cancellazione. Se il fornitore � usato come fornitore abituale di qualche articolo allora se ne impedisce la cancellazione (controllo eseguito solo per la cancellazione dei fornitori)
    if UPPER (this.oparentobject.class) = "TGSAR_AFR" AND g_APPLICATION = "ad hoc ENTERPRISE"
      this.w_ANCODICE = this.oparentobject.w_ANCODICE
      this.w_ANTIPCON = this.oparentobject.w_ANTIPCON
      this.w_OK = .T.
      * --- Select from PAR_RIOR
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PRCODFOR,PRTIPCON  from "+i_cTable+" PAR_RIOR ";
            +" where PRCODFOR="+cp_ToStrODBC(this.w_ANCODICE)+" AND PRTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)+"";
             ,"_Curs_PAR_RIOR")
      else
        select PRCODFOR,PRTIPCON from (i_cTable);
         where PRCODFOR=this.w_ANCODICE AND PRTIPCON=this.w_ANTIPCON;
          into cursor _Curs_PAR_RIOR
      endif
      if used('_Curs_PAR_RIOR')
        select _Curs_PAR_RIOR
        locate for 1=1
        do while not(eof())
        this.w_OK = .F.
        Exit
          select _Curs_PAR_RIOR
          continue
        enddo
        use
      endif
      if Not this.w_OK
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_MsgFormat("Attenzione, fornitore utilizzato come fornitore abituale negli articoli, impossibile cancellare")
      endif
    endif
  endproc


  proc Init(oParentObject,pParam,w_CODICE)
    this.pParam=pParam
    this.w_CODICE=w_CODICE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_RIOR'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PAR_RIOR')
      use in _Curs_PAR_RIOR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam,w_CODICE"
endproc
