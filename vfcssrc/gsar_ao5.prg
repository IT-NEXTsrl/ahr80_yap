* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ao5                                                        *
*              Parametri differenze e abbuoni                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_96]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2013-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_ao5"))

* --- Class definition
define class tgsar_ao5 as StdForm
  Top    = 38
  Left   = 17

  * --- Standard Properties
  Width  = 635
  Height = 282+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-16"
  HelpContextID=176781161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  CONTROPA_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  VOCIIVA_IDX = 0
  PAG_AMEN_IDX = 0
  AZIENDA_IDX = 0
  KEY_ARTI_IDX = 0
  cFile = "CONTROPA"
  cKeySelect = "COCODAZI"
  cKeyWhere  = "COCODAZI=this.w_COCODAZI"
  cKeyWhereODBC = '"COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cKeyWhereODBCqualified = '"CONTROPA.COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cPrg = "gsar_ao5"
  cComment = "Parametri differenze e abbuoni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_COTIPCON = space(1)
  w_PERIVB = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TIPART = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_COSAABBA = space(15)
  w_DESABA = space(40)
  w_COSAABBP = space(15)
  w_DESABP = space(40)
  w_COSADIFC = space(15)
  w_DESDIP = space(40)
  w_COSADIFN = space(15)
  w_DESDIN = space(40)
  w_CODIFCON = space(15)
  w_DESDIF = space(40)
  w_COSAPAGA = space(5)
  w_DESPAG = space(30)
  w_COIFCIVA = space(5)
  w_COCAUNOP = space(5)
  w_PERIVA = 0
  w_DESIVA = space(35)
  w_DESNOP = space(35)
  w_FLPART = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsar_ao5
  * --- Disabilita il Caricamento e la Cancellazione sulla Toolbar
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b3.enabled=.f.
          oCpToolBar.b5.enabled=.f.
  endproc
  * ---- Disattiva i metodi Load e Delete (posso solo variare)
  proc ecpLoad()
      * ----
  endproc
  proc ecpDelete()
      * ----
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CONTROPA','gsar_ao5')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ao5Pag1","gsar_ao5",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Vendite")
      .Pages(1).HelpContextID = 260975786
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsar_ao5
    * --- nMaxFieldsJoin di default vale 200
    this.parent.nMaxFieldsJoin = iif(lower(i_ServerConn[1,6])="oracle", 150, this.parent.nMaxFieldsJoin)
    * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='CONTROPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTROPA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_COCODAZI = NVL(COCODAZI,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gsar_ao5
    this.w_cocodazi=i_codazi
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTROPA where COCODAZI=KeySet.COCODAZI
    *
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTROPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTROPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTROPA '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_PERIVB = 0
        .w_TIPART = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESABA = space(40)
        .w_DESABP = space(40)
        .w_DESDIP = space(40)
        .w_DESDIN = space(40)
        .w_DESDIF = space(40)
        .w_DESPAG = space(30)
        .w_PERIVA = 0
        .w_DESIVA = space(35)
        .w_DESNOP = space(35)
        .w_FLPART = space(1)
        .w_COCODAZI = NVL(COCODAZI,space(5))
          if link_1_1_joined
            this.w_COCODAZI = NVL(AZCODAZI101,NVL(this.w_COCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_COTIPCON = NVL(COTIPCON,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .w_COSAABBA = NVL(COSAABBA,space(15))
          if link_1_13_joined
            this.w_COSAABBA = NVL(ANCODICE113,NVL(this.w_COSAABBA,space(15)))
            this.w_DESABA = NVL(ANDESCRI113,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO113),ctod("  /  /  "))
          else
          .link_1_13('Load')
          endif
        .w_COSAABBP = NVL(COSAABBP,space(15))
          if link_1_15_joined
            this.w_COSAABBP = NVL(ANCODICE115,NVL(this.w_COSAABBP,space(15)))
            this.w_DESABP = NVL(ANDESCRI115,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO115),ctod("  /  /  "))
          else
          .link_1_15('Load')
          endif
        .w_COSADIFC = NVL(COSADIFC,space(15))
          if link_1_17_joined
            this.w_COSADIFC = NVL(ANCODICE117,NVL(this.w_COSADIFC,space(15)))
            this.w_DESDIP = NVL(ANDESCRI117,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO117),ctod("  /  /  "))
          else
          .link_1_17('Load')
          endif
        .w_COSADIFN = NVL(COSADIFN,space(15))
          if link_1_19_joined
            this.w_COSADIFN = NVL(ANCODICE119,NVL(this.w_COSADIFN,space(15)))
            this.w_DESDIN = NVL(ANDESCRI119,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO119),ctod("  /  /  "))
          else
          .link_1_19('Load')
          endif
        .w_CODIFCON = NVL(CODIFCON,space(15))
          if link_1_21_joined
            this.w_CODIFCON = NVL(ANCODICE121,NVL(this.w_CODIFCON,space(15)))
            this.w_DESDIF = NVL(ANDESCRI121,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO121),ctod("  /  /  "))
          else
          .link_1_21('Load')
          endif
        .w_COSAPAGA = NVL(COSAPAGA,space(5))
          if link_1_23_joined
            this.w_COSAPAGA = NVL(PACODICE123,NVL(this.w_COSAPAGA,space(5)))
            this.w_DESPAG = NVL(PADESCRI123,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(PADTOBSO123),ctod("  /  /  "))
          else
          .link_1_23('Load')
          endif
        .w_COIFCIVA = NVL(COIFCIVA,space(5))
          if link_1_25_joined
            this.w_COIFCIVA = NVL(IVCODIVA125,NVL(this.w_COIFCIVA,space(5)))
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO125),ctod("  /  /  "))
            this.w_PERIVA = NVL(IVPERIVA125,0)
            this.w_DESIVA = NVL(IVDESIVA125,space(35))
          else
          .link_1_25('Load')
          endif
        .w_COCAUNOP = NVL(COCAUNOP,space(5))
          if link_1_26_joined
            this.w_COCAUNOP = NVL(CCCODICE126,NVL(this.w_COCAUNOP,space(5)))
            this.w_DESNOP = NVL(CCDESCRI126,space(35))
            this.w_FLPART = NVL(CCFLPART126,space(1))
          else
          .link_1_26('Load')
          endif
        cp_LoadRecExtFlds(this,'CONTROPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_ao5
    if this.w_cocodazi<>i_codazi and not empty(this.w_cocodazi)
     this.blankrec()
     this.w_cocodazi=""
     ah_ErrorMsg("Manutenzione consentita alla sola azienda corrente (%1)",,'',i_CODAZI)
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COCODAZI = space(5)
      .w_RAGAZI = space(40)
      .w_COTIPCON = space(1)
      .w_PERIVB = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_TIPART = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_COSAABBA = space(15)
      .w_DESABA = space(40)
      .w_COSAABBP = space(15)
      .w_DESABP = space(40)
      .w_COSADIFC = space(15)
      .w_DESDIP = space(40)
      .w_COSADIFN = space(15)
      .w_DESDIN = space(40)
      .w_CODIFCON = space(15)
      .w_DESDIF = space(40)
      .w_COSAPAGA = space(5)
      .w_DESPAG = space(30)
      .w_COIFCIVA = space(5)
      .w_COCAUNOP = space(5)
      .w_PERIVA = 0
      .w_DESIVA = space(35)
      .w_DESNOP = space(35)
      .w_FLPART = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_COCODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_COTIPCON = 'G'
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
          .DoRTCalc(4,9,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(11,12,.f.)
          if not(empty(.w_COSAABBA))
          .link_1_13('Full')
          endif
        .DoRTCalc(13,14,.f.)
          if not(empty(.w_COSAABBP))
          .link_1_15('Full')
          endif
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_COSADIFC))
          .link_1_17('Full')
          endif
        .DoRTCalc(17,18,.f.)
          if not(empty(.w_COSADIFN))
          .link_1_19('Full')
          endif
        .DoRTCalc(19,20,.f.)
          if not(empty(.w_CODIFCON))
          .link_1_21('Full')
          endif
        .DoRTCalc(21,22,.f.)
          if not(empty(.w_COSAPAGA))
          .link_1_23('Full')
          endif
        .DoRTCalc(23,24,.f.)
          if not(empty(.w_COIFCIVA))
          .link_1_25('Full')
          endif
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_COCAUNOP))
          .link_1_26('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTROPA')
    this.DoRTCalc(26,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_ao5
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCOSAABBA_1_13.enabled = i_bVal
      .Page1.oPag.oCOSAABBP_1_15.enabled = i_bVal
      .Page1.oPag.oCOSADIFC_1_17.enabled = i_bVal
      .Page1.oPag.oCOSADIFN_1_19.enabled = i_bVal
      .Page1.oPag.oCODIFCON_1_21.enabled = i_bVal
      .Page1.oPag.oCOSAPAGA_1_23.enabled = i_bVal
      .Page1.oPag.oCOIFCIVA_1_25.enabled = i_bVal
      .Page1.oPag.oCOCAUNOP_1_26.enabled = i_bVal
      .Page1.oPag.oBtn_1_27.enabled = i_bVal
      .Page1.oPag.oBtn_1_29.enabled = .Page1.oPag.oBtn_1_29.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'CONTROPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODAZI,"COCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCON,"COTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSAABBA,"COSAABBA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSAABBP,"COSAABBP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSADIFC,"COSADIFC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSADIFN,"COSADIFN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODIFCON,"CODIFCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSAPAGA,"COSAPAGA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COIFCIVA,"COIFCIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUNOP,"COCAUNOP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    i_lTable = "CONTROPA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTROPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTROPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(COCODAZI,COTIPCON,UTCC,UTCV,UTDC"+;
                  ",UTDV,COSAABBA,COSAABBP,COSADIFC,COSADIFN"+;
                  ",CODIFCON,COSAPAGA,COIFCIVA,COCAUNOP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_COCODAZI)+;
                  ","+cp_ToStrODBC(this.w_COTIPCON)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_COSAABBA)+;
                  ","+cp_ToStrODBCNull(this.w_COSAABBP)+;
                  ","+cp_ToStrODBCNull(this.w_COSADIFC)+;
                  ","+cp_ToStrODBCNull(this.w_COSADIFN)+;
                  ","+cp_ToStrODBCNull(this.w_CODIFCON)+;
                  ","+cp_ToStrODBCNull(this.w_COSAPAGA)+;
                  ","+cp_ToStrODBCNull(this.w_COIFCIVA)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUNOP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTROPA')
        cp_CheckDeletedKey(i_cTable,0,'COCODAZI',this.w_COCODAZI)
        INSERT INTO (i_cTable);
              (COCODAZI,COTIPCON,UTCC,UTCV,UTDC,UTDV,COSAABBA,COSAABBP,COSADIFC,COSADIFN,CODIFCON,COSAPAGA,COIFCIVA,COCAUNOP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_COCODAZI;
                  ,this.w_COTIPCON;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_COSAABBA;
                  ,this.w_COSAABBP;
                  ,this.w_COSADIFC;
                  ,this.w_COSADIFN;
                  ,this.w_CODIFCON;
                  ,this.w_COSAPAGA;
                  ,this.w_COIFCIVA;
                  ,this.w_COCAUNOP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTROPA_IDX,i_nConn)
      *
      * update CONTROPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTROPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",COSAABBA="+cp_ToStrODBCNull(this.w_COSAABBA)+;
             ",COSAABBP="+cp_ToStrODBCNull(this.w_COSAABBP)+;
             ",COSADIFC="+cp_ToStrODBCNull(this.w_COSADIFC)+;
             ",COSADIFN="+cp_ToStrODBCNull(this.w_COSADIFN)+;
             ",CODIFCON="+cp_ToStrODBCNull(this.w_CODIFCON)+;
             ",COSAPAGA="+cp_ToStrODBCNull(this.w_COSAPAGA)+;
             ",COIFCIVA="+cp_ToStrODBCNull(this.w_COIFCIVA)+;
             ",COCAUNOP="+cp_ToStrODBCNull(this.w_COCAUNOP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTROPA')
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        UPDATE (i_cTable) SET;
              COTIPCON=this.w_COTIPCON;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,COSAABBA=this.w_COSAABBA;
             ,COSAABBP=this.w_COSAABBP;
             ,COSADIFC=this.w_COSADIFC;
             ,COSADIFN=this.w_COSADIFN;
             ,CODIFCON=this.w_CODIFCON;
             ,COSAPAGA=this.w_COSAPAGA;
             ,COIFCIVA=this.w_COIFCIVA;
             ,COCAUNOP=this.w_COCAUNOP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTROPA_IDX,i_nConn)
      *
      * delete CONTROPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
            .w_COTIPCON = 'G'
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CONTROPA.COCODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CONTROPA.COCODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COSAABBA
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COSAABBA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COSAABBA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COSAABBA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COSAABBA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COSAABBA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COSAABBA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COSAABBA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOSAABBA_1_13'),i_cWhere,'GSAR_BZC',"Conti abbuoni attivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COSAABBA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COSAABBA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COSAABBA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COSAABBA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESABA = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COSAABBA = space(15)
      endif
      this.w_DESABA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COSAABBA = space(15)
        this.w_DESABA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COSAABBA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.ANCODICE as ANCODICE113"+ ",link_1_13.ANDESCRI as ANDESCRI113"+ ",link_1_13.ANDTOBSO as ANDTOBSO113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on CONTROPA.COSAABBA=link_1_13.ANCODICE"+" and CONTROPA.COTIPCON=link_1_13.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and CONTROPA.COSAABBA=link_1_13.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_13.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COSAABBP
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COSAABBP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COSAABBP)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COSAABBP))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COSAABBP)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COSAABBP)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COSAABBP)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COSAABBP) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOSAABBP_1_15'),i_cWhere,'GSAR_BZC',"Conti abbuoni passivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COSAABBP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COSAABBP);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COSAABBP)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COSAABBP = NVL(_Link_.ANCODICE,space(15))
      this.w_DESABP = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COSAABBP = space(15)
      endif
      this.w_DESABP = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COSAABBP = space(15)
        this.w_DESABP = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COSAABBP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.ANCODICE as ANCODICE115"+ ",link_1_15.ANDESCRI as ANDESCRI115"+ ",link_1_15.ANDTOBSO as ANDTOBSO115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on CONTROPA.COSAABBP=link_1_15.ANCODICE"+" and CONTROPA.COTIPCON=link_1_15.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and CONTROPA.COSAABBP=link_1_15.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_15.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COSADIFC
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COSADIFC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COSADIFC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COSADIFC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COSADIFC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COSADIFC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COSADIFC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COSADIFC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOSADIFC_1_17'),i_cWhere,'GSAR_BZC',"Conti diff.cambi attive",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COSADIFC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COSADIFC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COSADIFC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COSADIFC = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDIP = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COSADIFC = space(15)
      endif
      this.w_DESDIP = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COSADIFC = space(15)
        this.w_DESDIP = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COSADIFC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.ANCODICE as ANCODICE117"+ ",link_1_17.ANDESCRI as ANDESCRI117"+ ",link_1_17.ANDTOBSO as ANDTOBSO117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on CONTROPA.COSADIFC=link_1_17.ANCODICE"+" and CONTROPA.COTIPCON=link_1_17.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and CONTROPA.COSADIFC=link_1_17.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_17.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COSADIFN
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COSADIFN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COSADIFN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COSADIFN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COSADIFN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COSADIFN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COSADIFN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COSADIFN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOSADIFN_1_19'),i_cWhere,'GSAR_BZC',"Conti diff.cambi passive",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COSADIFN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COSADIFN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COSADIFN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COSADIFN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COSADIFN = space(15)
      endif
      this.w_DESDIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COSADIFN = space(15)
        this.w_DESDIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COSADIFN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.ANCODICE as ANCODICE119"+ ",link_1_19.ANDESCRI as ANDESCRI119"+ ",link_1_19.ANDTOBSO as ANDTOBSO119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on CONTROPA.COSADIFN=link_1_19.ANCODICE"+" and CONTROPA.COTIPCON=link_1_19.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and CONTROPA.COSADIFN=link_1_19.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_19.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODIFCON
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIFCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODIFCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_CODIFCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIFCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODIFCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODIFCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODIFCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODIFCON_1_21'),i_cWhere,'GSAR_BZC',"Conti diff. di conversione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIFCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODIFCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_CODIFCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIFCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDIF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODIFCON = space(15)
      endif
      this.w_DESDIF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CODIFCON = space(15)
        this.w_DESDIF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIFCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.ANCODICE as ANCODICE121"+ ",link_1_21.ANDESCRI as ANDESCRI121"+ ",link_1_21.ANDTOBSO as ANDTOBSO121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on CONTROPA.CODIFCON=link_1_21.ANCODICE"+" and CONTROPA.COTIPCON=link_1_21.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and CONTROPA.CODIFCON=link_1_21.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_21.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COSAPAGA
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COSAPAGA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_COSAPAGA)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_COSAPAGA))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COSAPAGA)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_COSAPAGA)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_COSAPAGA))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_COSAPAGA)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COSAPAGA) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oCOSAPAGA_1_23'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COSAPAGA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_COSAPAGA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_COSAPAGA)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COSAPAGA = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COSAPAGA = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_COSAPAGA) OR LEFT(CHTIPPAG(.w_COSAPAGA, 'RD'),2)='RD') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo pagamento inesistente, non rimessa diretta oppure obsoleto")
        endif
        this.w_COSAPAGA = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COSAPAGA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.PACODICE as PACODICE123"+ ","+cp_TransLinkFldName('link_1_23.PADESCRI')+" as PADESCRI123"+ ",link_1_23.PADTOBSO as PADTOBSO123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on CONTROPA.COSAPAGA=link_1_23.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and CONTROPA.COSAPAGA=link_1_23.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COIFCIVA
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COIFCIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_COIFCIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO,IVPERIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_COIFCIVA))
          select IVCODIVA,IVDTOBSO,IVPERIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COIFCIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COIFCIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCOIFCIVA_1_25'),i_cWhere,'GSAR_AIV',"Voci IVA",'GSAR_AO5.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO,IVPERIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDTOBSO,IVPERIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COIFCIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO,IVPERIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_COIFCIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_COIFCIVA)
            select IVCODIVA,IVDTOBSO,IVPERIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COIFCIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COIFCIVA = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PERIVA = 0
      this.w_DESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PERIVA=0 AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto o incongruente o inesistente")
        endif
        this.w_COIFCIVA = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PERIVA = 0
        this.w_DESIVA = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COIFCIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.IVCODIVA as IVCODIVA125"+ ",link_1_25.IVDTOBSO as IVDTOBSO125"+ ",link_1_25.IVPERIVA as IVPERIVA125"+ ",link_1_25.IVDESIVA as IVDESIVA125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on CONTROPA.COIFCIVA=link_1_25.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and CONTROPA.COIFCIVA=link_1_25.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUNOP
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUNOP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUNOP)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUNOP))
          select CCCODICE,CCDESCRI,CCFLPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUNOP)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCAUNOP) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUNOP_1_26'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCGNPAR.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPART";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUNOP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUNOP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUNOP)
            select CCCODICE,CCDESCRI,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUNOP = NVL(_Link_.CCCODICE,space(5))
      this.w_DESNOP = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLPART = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUNOP = space(5)
      endif
      this.w_DESNOP = space(35)
      this.w_FLPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLPART='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, causale contabile incongruente o gestita a partite")
        endif
        this.w_COCAUNOP = space(5)
        this.w_DESNOP = space(35)
        this.w_FLPART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUNOP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.CCCODICE as CCCODICE126"+ ",link_1_26.CCDESCRI as CCDESCRI126"+ ",link_1_26.CCFLPART as CCFLPART126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on CONTROPA.COCAUNOP=link_1_26.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUNOP=link_1_26.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOSAABBA_1_13.value==this.w_COSAABBA)
      this.oPgFrm.Page1.oPag.oCOSAABBA_1_13.value=this.w_COSAABBA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESABA_1_14.value==this.w_DESABA)
      this.oPgFrm.Page1.oPag.oDESABA_1_14.value=this.w_DESABA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSAABBP_1_15.value==this.w_COSAABBP)
      this.oPgFrm.Page1.oPag.oCOSAABBP_1_15.value=this.w_COSAABBP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESABP_1_16.value==this.w_DESABP)
      this.oPgFrm.Page1.oPag.oDESABP_1_16.value=this.w_DESABP
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSADIFC_1_17.value==this.w_COSADIFC)
      this.oPgFrm.Page1.oPag.oCOSADIFC_1_17.value=this.w_COSADIFC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDIP_1_18.value==this.w_DESDIP)
      this.oPgFrm.Page1.oPag.oDESDIP_1_18.value=this.w_DESDIP
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSADIFN_1_19.value==this.w_COSADIFN)
      this.oPgFrm.Page1.oPag.oCOSADIFN_1_19.value=this.w_COSADIFN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDIN_1_20.value==this.w_DESDIN)
      this.oPgFrm.Page1.oPag.oDESDIN_1_20.value=this.w_DESDIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIFCON_1_21.value==this.w_CODIFCON)
      this.oPgFrm.Page1.oPag.oCODIFCON_1_21.value=this.w_CODIFCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDIF_1_22.value==this.w_DESDIF)
      this.oPgFrm.Page1.oPag.oDESDIF_1_22.value=this.w_DESDIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSAPAGA_1_23.value==this.w_COSAPAGA)
      this.oPgFrm.Page1.oPag.oCOSAPAGA_1_23.value=this.w_COSAPAGA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_24.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_24.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCOIFCIVA_1_25.value==this.w_COIFCIVA)
      this.oPgFrm.Page1.oPag.oCOIFCIVA_1_25.value=this.w_COIFCIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAUNOP_1_26.value==this.w_COCAUNOP)
      this.oPgFrm.Page1.oPag.oCOCAUNOP_1_26.value=this.w_COCAUNOP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_36.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_36.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOP_1_38.value==this.w_DESNOP)
      this.oPgFrm.Page1.oPag.oDESNOP_1_38.value=this.w_DESNOP
    endif
    cp_SetControlsValueExtFlds(this,'CONTROPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COSAABBA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOSAABBA_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COSAABBP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOSAABBP_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COSADIFC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOSADIFC_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COSADIFN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOSADIFN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODIFCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODIFCON_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   ((empty(.w_COSAPAGA)) or not((EMPTY(.w_COSAPAGA) OR LEFT(CHTIPPAG(.w_COSAPAGA, 'RD'),2)='RD') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOSAPAGA_1_23.SetFocus()
            i_bnoObbl = !empty(.w_COSAPAGA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo pagamento inesistente, non rimessa diretta oppure obsoleto")
          case   not(.w_PERIVA=0 AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_COIFCIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOIFCIVA_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto o incongruente o inesistente")
          case   not(.w_FLPART='N')  and not(empty(.w_COCAUNOP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAUNOP_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, causale contabile incongruente o gestita a partite")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ao5Pag1 as StdContainer
  Width  = 631
  height = 283
  stdWidth  = 631
  stdheight = 283
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_9 as cp_runprogram with uid="KBKOJQLSZS",left=337, top=305, width=128,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BAM("modifica")',;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 1216486

  add object oCOSAABBA_1_13 as StdField with uid="NDEFJCNGNR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COSAABBA", cQueryName = "COSAABBA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione abbuoni attivi",;
    HelpContextID = 197987431,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=198, Top=14, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COSAABBA"

  func oCOSAABBA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOSAABBA_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOSAABBA_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOSAABBA_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti abbuoni attivi",'',this.parent.oContained
  endproc
  proc oCOSAABBA_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COSAABBA
     i_obj.ecpSave()
  endproc

  add object oDESABA_1_14 as StdField with uid="BYZFRIASPB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESABA", cQueryName = "DESABA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 182256182,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=337, Top=14, InputMask=replicate('X',40)

  add object oCOSAABBP_1_15 as StdField with uid="JADONRFSIB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_COSAABBP", cQueryName = "COSAABBP",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione abbuoni passivi",;
    HelpContextID = 197987446,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=198, Top=42, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COSAABBP"

  func oCOSAABBP_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOSAABBP_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOSAABBP_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOSAABBP_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti abbuoni passivi",'',this.parent.oContained
  endproc
  proc oCOSAABBP_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COSAABBP
     i_obj.ecpSave()
  endproc

  add object oDESABP_1_16 as StdField with uid="SCTMFLYKTP",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESABP", cQueryName = "DESABP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 165478966,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=337, Top=42, InputMask=replicate('X',40)

  add object oCOSADIFC_1_17 as StdField with uid="SWLNVRPYOE",rtseq=16,rtrep=.f.,;
    cFormVar = "w_COSADIFC", cQueryName = "COSADIFC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione differenza cambi attiva",;
    HelpContextID = 50138217,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=198, Top=70, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COSADIFC"

  func oCOSADIFC_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOSADIFC_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOSADIFC_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOSADIFC_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti diff.cambi attive",'',this.parent.oContained
  endproc
  proc oCOSADIFC_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COSADIFC
     i_obj.ecpSave()
  endproc

  add object oDESDIP_1_18 as StdField with uid="USNSESLGIJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESDIP", cQueryName = "DESDIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 173015606,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=337, Top=70, InputMask=replicate('X',40)

  add object oCOSADIFN_1_19 as StdField with uid="MPONVEFMRS",rtseq=18,rtrep=.f.,;
    cFormVar = "w_COSADIFN", cQueryName = "COSADIFN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione differenza cambi passiva",;
    HelpContextID = 50138228,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=198, Top=98, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COSADIFN"

  func oCOSADIFN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOSADIFN_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOSADIFN_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOSADIFN_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti diff.cambi passive",'',this.parent.oContained
  endproc
  proc oCOSADIFN_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COSADIFN
     i_obj.ecpSave()
  endproc

  add object oDESDIN_1_20 as StdField with uid="EERSHBUCYA",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESDIN", cQueryName = "DESDIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 139461174,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=337, Top=98, InputMask=replicate('X',40)

  add object oCODIFCON_1_21 as StdField with uid="UMOKRJIVNU",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODIFCON", cQueryName = "CODIFCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione differenze di conversione cambi",;
    HelpContextID = 47965068,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=198, Top=126, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODIFCON"

  func oCODIFCON_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIFCON_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIFCON_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODIFCON_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti diff. di conversione",'',this.parent.oContained
  endproc
  proc oCODIFCON_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODIFCON
     i_obj.ecpSave()
  endproc

  add object oDESDIF_1_22 as StdField with uid="MNLLYHJYPE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESDIF", cQueryName = "DESDIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 5243446,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=337, Top=126, InputMask=replicate('X',40)

  add object oCOSAPAGA_1_23 as StdField with uid="IQZIKKZIDK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_COSAPAGA", cQueryName = "COSAPAGA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo pagamento inesistente, non rimessa diretta oppure obsoleto",;
    ToolTipText = "Codice pagamento di default per gestione partite su abbuoni e/o anticipi",;
    HelpContextID = 196938855,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=198, Top=154, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_COSAPAGA"

  func oCOSAPAGA_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOSAPAGA_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOSAPAGA_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oCOSAPAGA_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oCOSAPAGA_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_COSAPAGA
     i_obj.ecpSave()
  endproc

  add object oDESPAG_1_24 as StdField with uid="SYPNFKZLTI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 14418486,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=337, Top=154, InputMask=replicate('X',30)

  add object oCOIFCIVA_1_25 as StdField with uid="LNBTCCJTOO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_COIFCIVA", cQueryName = "COIFCIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto o incongruente o inesistente",;
    ToolTipText = "Codice fuori campo IVA utilizzato per le differenze di conversione",;
    HelpContextID = 49376359,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=198, Top=183, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_COIFCIVA"

  func oCOIFCIVA_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOIFCIVA_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOIFCIVA_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCOIFCIVA_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'GSAR_AO5.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oCOIFCIVA_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_COIFCIVA
     i_obj.ecpSave()
  endproc

  add object oCOCAUNOP_1_26 as StdField with uid="BFDRVQGIUQ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_COCAUNOP", cQueryName = "COCAUNOP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, causale contabile incongruente o gestita a partite",;
    ToolTipText = "Causale contabile di incasso per righe contabili non gestite a partite",;
    HelpContextID = 151784566,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=198, Top=213, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUNOP"

  func oCOCAUNOP_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUNOP_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUNOP_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUNOP_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCGNPAR.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUNOP_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUNOP
     i_obj.ecpSave()
  endproc


  add object oBtn_1_27 as StdButton with uid="ZOMKMPWSMV",left=527, top=238, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 176752410;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="ESFCKVQXET",left=579, top=238, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per abbandonare";
    , HelpContextID = 169463738;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESIVA_1_36 as StdField with uid="QHXEVPLHUZ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 203751990,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=337, Top=182, InputMask=replicate('X',35)

  add object oDESNOP_1_38 as StdField with uid="NLGGHSWCMM",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESNOP", cQueryName = "DESNOP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 179962422,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=338, Top=213, InputMask=replicate('X',35)

  add object oStr_1_28 as StdString with uid="UDEJRKDPXZ",Visible=.t., Left=15, Top=14,;
    Alignment=1, Width=181, Height=15,;
    Caption="Abbuoni attivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="LRQNHPXGTU",Visible=.t., Left=15, Top=42,;
    Alignment=1, Width=181, Height=15,;
    Caption="Abbuoni passivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="KRNYUJBWNI",Visible=.t., Left=15, Top=70,;
    Alignment=1, Width=181, Height=15,;
    Caption="Diff. cambi attiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="DDCBPBWKER",Visible=.t., Left=15, Top=98,;
    Alignment=1, Width=181, Height=15,;
    Caption="Diff. cambi passiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="AIMPZTWLNF",Visible=.t., Left=2, Top=154,;
    Alignment=1, Width=194, Height=15,;
    Caption="Pagamento per abbuoni/anticipi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YCTKUOBNAR",Visible=.t., Left=15, Top=126,;
    Alignment=1, Width=181, Height=15,;
    Caption="Differenze di conversione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="EWWQVDTDGZ",Visible=.t., Left=15, Top=182,;
    Alignment=1, Width=181, Height=15,;
    Caption="Cod.IVA diff. di conversione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="JVGUKOXVPJ",Visible=.t., Left=11, Top=212,;
    Alignment=1, Width=185, Height=18,;
    Caption="Causale non gestita a partite:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ao5','CONTROPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODAZI=CONTROPA.COCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
