* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kax                                                        *
*              Dati FAX                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_138]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-22                                                      *
* Last revis.: 2014-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kax",oParentObject))

* --- Class definition
define class tgsut_kax as StdForm
  Top    = 12
  Left   = 12

  * --- Standard Properties
  Width  = 405
  Height = 342
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-19"
  HelpContextID=132727657
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  cPrg = "gsut_kax"
  cComment = "Dati FAX"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CLIFORDES = space(1)
  w_CODDES = space(15)
  w_CFDESCRI = space(40)
  w_INVIAA = space(40)
  w_RIFERI = space(40)
  w_FAXA = space(18)
  w_FAXA = space(18)
  w_SOGFAX = space(0)
  w_OGGFAX = space(0)
  w_FRONTE = space(1)
  w_STOPFAX = .F.
  w_MITTEN = space(40)
  w_UTEAZI = space(40)
  w_FAXMIT = space(18)
  w_TELMIT = space(18)
  w_MAIMIT = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kaxPag1","gsut_kax",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oINVIAA_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CLIFORDES=space(1)
      .w_CODDES=space(15)
      .w_CFDESCRI=space(40)
      .w_INVIAA=space(40)
      .w_RIFERI=space(40)
      .w_FAXA=space(18)
      .w_FAXA=space(18)
      .w_SOGFAX=space(0)
      .w_OGGFAX=space(0)
      .w_FRONTE=space(1)
      .w_STOPFAX=.f.
      .w_MITTEN=space(40)
      .w_UTEAZI=space(40)
      .w_FAXMIT=space(18)
      .w_TELMIT=space(18)
      .w_MAIMIT=space(50)
      .w_INVIAA=oParentObject.w_INVIAA
      .w_RIFERI=oParentObject.w_RIFERI
      .w_FAXA=oParentObject.w_FAXA
      .w_FAXA=oParentObject.w_FAXA
      .w_SOGFAX=oParentObject.w_SOGFAX
      .w_OGGFAX=oParentObject.w_OGGFAX
      .w_FRONTE=oParentObject.w_FRONTE
      .w_STOPFAX=oParentObject.w_STOPFAX
      .w_MITTEN=oParentObject.w_MITTEN
      .w_UTEAZI=oParentObject.w_UTEAZI
      .w_FAXMIT=oParentObject.w_FAXMIT
      .w_TELMIT=oParentObject.w_TELMIT
      .w_MAIMIT=oParentObject.w_MAIMIT
        .w_CLIFORDES = iif(type('i_CLIFORDES')='C', i_CLIFORDES, '')
        .w_CODDES = iif(type('i_CODDES')='C', i_CODDES, '')
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODDES))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_INVIAA = iif(not empty(.w_CFDESCRI), .w_CFDESCRI, .w_INVIAA)
          .DoRTCalc(5,6,.f.)
        .w_FAXA = i_FAXNO
    endwith
    this.DoRTCalc(8,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_kax
    this.w_STOPFAX=.f.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_INVIAA=.w_INVIAA
      .oParentObject.w_RIFERI=.w_RIFERI
      .oParentObject.w_FAXA=.w_FAXA
      .oParentObject.w_FAXA=.w_FAXA
      .oParentObject.w_SOGFAX=.w_SOGFAX
      .oParentObject.w_OGGFAX=.w_OGGFAX
      .oParentObject.w_FRONTE=.w_FRONTE
      .oParentObject.w_STOPFAX=.w_STOPFAX
      .oParentObject.w_MITTEN=.w_MITTEN
      .oParentObject.w_UTEAZI=.w_UTEAZI
      .oParentObject.w_FAXMIT=.w_FAXMIT
      .oParentObject.w_TELMIT=.w_TELMIT
      .oParentObject.w_MAIMIT=.w_MAIMIT
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFAXA_1_6.visible=!this.oPgFrm.Page1.oPag.oFAXA_1_6.mHide()
    this.oPgFrm.Page1.oPag.oFAXA_1_7.visible=!this.oPgFrm.Page1.oPag.oFAXA_1_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODDES
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODDES);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFORDES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CLIFORDES;
                       ,'ANCODICE',this.w_CODDES)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODDES = NVL(_Link_.ANCODICE,space(15))
      this.w_CFDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODDES = space(15)
      endif
      this.w_CFDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oINVIAA_1_4.value==this.w_INVIAA)
      this.oPgFrm.Page1.oPag.oINVIAA_1_4.value=this.w_INVIAA
    endif
    if not(this.oPgFrm.Page1.oPag.oRIFERI_1_5.value==this.w_RIFERI)
      this.oPgFrm.Page1.oPag.oRIFERI_1_5.value=this.w_RIFERI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAXA_1_6.value==this.w_FAXA)
      this.oPgFrm.Page1.oPag.oFAXA_1_6.value=this.w_FAXA
    endif
    if not(this.oPgFrm.Page1.oPag.oFAXA_1_7.value==this.w_FAXA)
      this.oPgFrm.Page1.oPag.oFAXA_1_7.value=this.w_FAXA
    endif
    if not(this.oPgFrm.Page1.oPag.oFRONTE_1_10.RadioValue()==this.w_FRONTE)
      this.oPgFrm.Page1.oPag.oFRONTE_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMITTEN_1_19.value==this.w_MITTEN)
      this.oPgFrm.Page1.oPag.oMITTEN_1_19.value=this.w_MITTEN
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEAZI_1_20.value==this.w_UTEAZI)
      this.oPgFrm.Page1.oPag.oUTEAZI_1_20.value=this.w_UTEAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAXMIT_1_21.value==this.w_FAXMIT)
      this.oPgFrm.Page1.oPag.oFAXMIT_1_21.value=this.w_FAXMIT
    endif
    if not(this.oPgFrm.Page1.oPag.oTELMIT_1_22.value==this.w_TELMIT)
      this.oPgFrm.Page1.oPag.oTELMIT_1_22.value=this.w_TELMIT
    endif
    if not(this.oPgFrm.Page1.oPag.oMAIMIT_1_23.value==this.w_MAIMIT)
      this.oPgFrm.Page1.oPag.oMAIMIT_1_23.value=this.w_MAIMIT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsut_kax
      i_FAXNO = .w_FAXA
      i_DEST = .w_INVIAA
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kaxPag1 as StdContainer
  Width  = 401
  height = 342
  stdWidth  = 401
  stdheight = 342
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINVIAA_1_4 as StdField with uid="XLENAJHTKT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_INVIAA", cQueryName = "INVIAA",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome/ragione sociale del destinatario",;
    HelpContextID = 225800070,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=91, Top=37, InputMask=replicate('X',40)

  add object oRIFERI_1_5 as StdField with uid="IRRWEQQLEM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RIFERI", cQueryName = "RIFERI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale riferimento",;
    HelpContextID = 109079318,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=91, Top=65, InputMask=replicate('X',40)

  add object oFAXA_1_6 as StdField with uid="PVLEGTSHCZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FAXA", cQueryName = "FAXA",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di FAX",;
    HelpContextID = 128089514,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=91, Top=93, cSayPict="REPL('9',18)", cGetPict="REPL('9',18)", InputMask=replicate('X',18), bHasZoom = .t. 

  func oFAXA_1_6.mHide()
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE")
    endwith
  endfunc

  proc oFAXA_1_6.mZoom
      with this.Parent.oContained
        GSAR_BRM(this.Parent.oContained,"FAX")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFAXA_1_7 as StdField with uid="DGJHKGEKTC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FAXA", cQueryName = "FAXA",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di FAX",;
    HelpContextID = 128089514,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=91, Top=93, InputMask=replicate('X',18)

  func oFAXA_1_7.mHide()
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oFRONTE_1_10 as StdCheck with uid="ZUCGDKSCJP",rtseq=10,rtrep=.f.,left=93, top=295, caption="Rimuovi frontespizio",;
    HelpContextID = 44696406,;
    cFormVar="w_FRONTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFRONTE_1_10.RadioValue()
    return(iif(this.value =1,'N',;
    'S'))
  endfunc
  func oFRONTE_1_10.GetRadio()
    this.Parent.oContained.w_FRONTE = this.RadioValue()
    return .t.
  endfunc

  func oFRONTE_1_10.SetRadio()
    this.Parent.oContained.w_FRONTE=trim(this.Parent.oContained.w_FRONTE)
    this.value = ;
      iif(this.Parent.oContained.w_FRONTE=='N',1,;
      0)
  endfunc


  add object oBtn_1_16 as StdButton with uid="WHSENRFQRK",left=286, top=293, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 132698906;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="SJNEQIMFBD",left=338, top=293, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 125410234;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMITTEN_1_19 as StdField with uid="LEUSMNRYEW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MITTEN", cQueryName = "MITTEN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome / ragione sociale del mittente",;
    HelpContextID = 180374214,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=91, Top=155, InputMask=replicate('X',40)

  add object oUTEAZI_1_20 as StdField with uid="YUJURLZYNT",rtseq=13,rtrep=.f.,;
    cFormVar = "w_UTEAZI", cQueryName = "UTEAZI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome utente",;
    HelpContextID = 117204550,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=91, Top=183, InputMask=replicate('X',40)

  add object oFAXMIT_1_21 as StdField with uid="EWERSRKLRE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FAXMIT", cQueryName = "FAXMIT",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 16351830,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=91, Top=211, InputMask=replicate('X',18)

  add object oTELMIT_1_22 as StdField with uid="EXUNQKRLPR",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TELMIT", cQueryName = "TELMIT",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 16303926,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=91, Top=239, InputMask=replicate('X',18)

  add object oMAIMIT_1_23 as StdField with uid="AKFQWUFIET",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MAIMIT", cQueryName = "MAIMIT",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 16290502,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=91, Top=267, InputMask=replicate('X',50)

  add object oStr_1_12 as StdString with uid="AQEOULSGKR",Visible=.t., Left=10, Top=37,;
    Alignment=1, Width=80, Height=15,;
    Caption="Destinatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="RMWPCXGRGP",Visible=.t., Left=10, Top=65,;
    Alignment=1, Width=80, Height=15,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="PMKKKSKPOB",Visible=.t., Left=10, Top=93,;
    Alignment=1, Width=80, Height=15,;
    Caption="Numero FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="IUQTIVCDHB",Visible=.t., Left=5, Top=8,;
    Alignment=0, Width=390, Height=15,;
    Caption="Destinatario"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="DGEQCCARAK",Visible=.t., Left=8, Top=155,;
    Alignment=1, Width=82, Height=15,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="UJVTSAOOND",Visible=.t., Left=8, Top=183,;
    Alignment=1, Width=82, Height=15,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ILYDOWTGZM",Visible=.t., Left=8, Top=211,;
    Alignment=1, Width=82, Height=15,;
    Caption="Numero FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WWJVKCBTMD",Visible=.t., Left=8, Top=239,;
    Alignment=1, Width=82, Height=15,;
    Caption="Numero tel.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="UNTPIOXJOT",Visible=.t., Left=8, Top=268,;
    Alignment=1, Width=82, Height=15,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ADDFBAUDHJ",Visible=.t., Left=5, Top=128,;
    Alignment=0, Width=390, Height=18,;
    Caption="Mittente"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_11 as StdBox with uid="YCOEKCKDVY",left=5, top=24, width=392,height=1

  add object oBox_1_29 as StdBox with uid="KHSIVBMEEI",left=5, top=147, width=392,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kax','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
