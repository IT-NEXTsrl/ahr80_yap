* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bfd                                                        *
*              Filtri data registraz                                           *
*                                                                              *
*      Author: Zucchetti TAM S.p.a.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_46]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-11                                                      *
* Last revis.: 2010-03-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bfd",oParentObject)
return(i_retval)

define class tgsar_bfd as StdBatch
  * --- Local variables
  * --- WorkFile variables
  FILDTREG_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza varaibili pubbliche per gestione filtri su elenchi
    * --- Assegnazione e salvataggio FILTRI DATA REGISTRAZIONE
     
 STORE i_inidat TO g_DTINPN, g_DTINDC, g_DTINMM, ; 
 g_DTINTE, g_DTINCE, g_DTINRT 
 STORE i_findat TO g_DTFIPN, g_DTFIDC, g_DTFIMM, ; 
 g_DTFITE, g_DTFICE, g_DTFIRT,g_DTULTCOS
    * --- --Importazione documenti
    g_DTINIM = 18250
    g_DTFIIM = 0
    * --- --Fatturazione differita
    g_DTINFA = 50
    g_DTFIFA = 0
    if this.oParentObject.w_FLNOFLPN="S"
      g_DTINPN = i_DATSYS - this.oParentObject.w_FLDATPNP
      g_DTFIPN = i_DATSYS + this.oParentObject.w_FLDATPNS
    endif
    if this.oParentObject.w_FLNOFLDC="S"
      g_DTINDC = i_DATSYS - this.oParentObject.w_FLDATDCP
      g_DTFIDC = i_DATSYS + this.oParentObject.w_FLDATDCS
    endif
    if this.oParentObject.w_FLNOFLMM="S"
      g_DTINMM = i_DATSYS - this.oParentObject.w_FLDATMMP
      g_DTFIMM = i_DATSYS + this.oParentObject.w_FLDATMMS
    endif
    if this.oParentObject.w_FLNOFLTE="S"
      g_DTINTE = i_DATSYS - this.oParentObject.w_FLDATTEP
      g_DTFITE = i_DATSYS + this.oParentObject.w_FLDATTES
    endif
    if this.oParentObject.w_FLNOFLCE="S"
      g_DTINCE = i_DATSYS - this.oParentObject.w_FLDATCEP
      g_DTFICE = i_DATSYS + this.oParentObject.w_FLDATCES
    endif
    if this.oParentObject.w_FLNOFLRT="S"
      g_DTINRT = i_DATSYS - this.oParentObject.w_FLDATRTP
      g_DTFIRT = i_DATSYS + this.oParentObject.w_FLDATRTS
    endif
    if this.oParentObject.w_FLNOFLIM="S"
      * --- In questo caso sono solo i giorni di filtro poich� vengono sommati/sottratti
      *     direttamente nelle maschere di import GSVE_KIM e GSPS_KIM
      g_DTINIM = this.oParentObject.w_FLDATIMP
      g_DTFIIM = this.oParentObject.w_FLDATIMS
    endif
    if this.oParentObject.w_FLNOFLFA="S"
      * --- DA FATTURAZIONE DIFFERITA GSVE_AFD
      g_DTINFA = this.oParentObject.w_FLDATIFA
      g_DTFIFA = this.oParentObject.w_FLDATFFA
    endif
    * --- Data da cui partire per determinare ultimo prezzo costo
    g_DTULTCOS=IIF(empty(this.oParentObject.w_FLDTUCOS),g_DTULTCOS,this.oParentObject.w_FLDTUCOS)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='FILDTREG'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
