* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aos                                                        *
*              Operazioni superiori a 3000 euro                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-04-08                                                      *
* Last revis.: 2011-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_aos")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_aos")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_aos")
  return

* --- Class definition
define class tgscg_aos as StdPCForm
  Width  = 693
  Height = 390
  Top    = 10
  Left   = 10
  cComment = "Operazioni superiori a 3000 euro"
  cPrg = "gscg_aos"
  HelpContextID=176825705
  add object cnt as tcgscg_aos
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_aos as PCContext
  w_OSSERIAL = space(10)
  w_PNTIPDOC = space(1)
  w_PNTIPREG = space(1)
  w_ANOPETRE = space(1)
  w_PNCODVAL = space(3)
  w_OBTEST = space(8)
  w_ANFLSOAL = space(1)
  w_OSTIPOPE = space(1)
  w_OSRIFCON = space(30)
  w_OSTIPCLF = space(1)
  w_OSCODICE = space(15)
  w_OSTIPFAT = space(1)
  w_OSANNRET = 0
  w_OSIMPFRA = space(1)
  w_OSRIFFAT = space(10)
  w_OSFLGEXT = space(1)
  w_OSFLGDVE = space(1)
  w_OSGENERA = space(1)
  w_OSMODUTE = space(1)
  w_NUMREG = 0
  w_DATREG = space(8)
  w_COMPET = space(4)
  w_OSUTEINS = 0
  w_OSDATINS = space(8)
  w_OSUTEVAR = 0
  w_OSDATVAR = space(8)
  w_RIFFAT = space(10)
  w_SERIALE_OPERAZIONE = space(10)
  w_OSRIFACC = space(10)
  w_RIFACC = space(10)
  w_NUMREG_A = 0
  w_DATREG_A = space(8)
  w_COMPET_A = space(4)
  w_SERIALE_ACCONTO = space(10)
  w_ANDESCRI = space(40)
  w_INTEFFET = space(1)
  w_ANDTOBSO = space(8)
  w_DERIFPNT = space(10)
  w_DESERIAL = space(10)
  w_DE__AREA = space(1)
  w_CPROWNUM = 0
  w_OBJECT = space(1)
  w_TRASH = space(1)
  w_GEST = space(1)
  proc Save(oFrom)
    this.w_OSSERIAL = oFrom.w_OSSERIAL
    this.w_PNTIPDOC = oFrom.w_PNTIPDOC
    this.w_PNTIPREG = oFrom.w_PNTIPREG
    this.w_ANOPETRE = oFrom.w_ANOPETRE
    this.w_PNCODVAL = oFrom.w_PNCODVAL
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_ANFLSOAL = oFrom.w_ANFLSOAL
    this.w_OSTIPOPE = oFrom.w_OSTIPOPE
    this.w_OSRIFCON = oFrom.w_OSRIFCON
    this.w_OSTIPCLF = oFrom.w_OSTIPCLF
    this.w_OSCODICE = oFrom.w_OSCODICE
    this.w_OSTIPFAT = oFrom.w_OSTIPFAT
    this.w_OSANNRET = oFrom.w_OSANNRET
    this.w_OSIMPFRA = oFrom.w_OSIMPFRA
    this.w_OSRIFFAT = oFrom.w_OSRIFFAT
    this.w_OSFLGEXT = oFrom.w_OSFLGEXT
    this.w_OSFLGDVE = oFrom.w_OSFLGDVE
    this.w_OSGENERA = oFrom.w_OSGENERA
    this.w_OSMODUTE = oFrom.w_OSMODUTE
    this.w_NUMREG = oFrom.w_NUMREG
    this.w_DATREG = oFrom.w_DATREG
    this.w_COMPET = oFrom.w_COMPET
    this.w_OSUTEINS = oFrom.w_OSUTEINS
    this.w_OSDATINS = oFrom.w_OSDATINS
    this.w_OSUTEVAR = oFrom.w_OSUTEVAR
    this.w_OSDATVAR = oFrom.w_OSDATVAR
    this.w_RIFFAT = oFrom.w_RIFFAT
    this.w_SERIALE_OPERAZIONE = oFrom.w_SERIALE_OPERAZIONE
    this.w_OSRIFACC = oFrom.w_OSRIFACC
    this.w_RIFACC = oFrom.w_RIFACC
    this.w_NUMREG_A = oFrom.w_NUMREG_A
    this.w_DATREG_A = oFrom.w_DATREG_A
    this.w_COMPET_A = oFrom.w_COMPET_A
    this.w_SERIALE_ACCONTO = oFrom.w_SERIALE_ACCONTO
    this.w_ANDESCRI = oFrom.w_ANDESCRI
    this.w_INTEFFET = oFrom.w_INTEFFET
    this.w_ANDTOBSO = oFrom.w_ANDTOBSO
    this.w_DERIFPNT = oFrom.w_DERIFPNT
    this.w_DESERIAL = oFrom.w_DESERIAL
    this.w_DE__AREA = oFrom.w_DE__AREA
    this.w_CPROWNUM = oFrom.w_CPROWNUM
    this.w_OBJECT = oFrom.w_OBJECT
    this.w_TRASH = oFrom.w_TRASH
    this.w_GEST = oFrom.w_GEST
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_OSSERIAL = this.w_OSSERIAL
    oTo.w_PNTIPDOC = this.w_PNTIPDOC
    oTo.w_PNTIPREG = this.w_PNTIPREG
    oTo.w_ANOPETRE = this.w_ANOPETRE
    oTo.w_PNCODVAL = this.w_PNCODVAL
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_ANFLSOAL = this.w_ANFLSOAL
    oTo.w_OSTIPOPE = this.w_OSTIPOPE
    oTo.w_OSRIFCON = this.w_OSRIFCON
    oTo.w_OSTIPCLF = this.w_OSTIPCLF
    oTo.w_OSCODICE = this.w_OSCODICE
    oTo.w_OSTIPFAT = this.w_OSTIPFAT
    oTo.w_OSANNRET = this.w_OSANNRET
    oTo.w_OSIMPFRA = this.w_OSIMPFRA
    oTo.w_OSRIFFAT = this.w_OSRIFFAT
    oTo.w_OSFLGEXT = this.w_OSFLGEXT
    oTo.w_OSFLGDVE = this.w_OSFLGDVE
    oTo.w_OSGENERA = this.w_OSGENERA
    oTo.w_OSMODUTE = this.w_OSMODUTE
    oTo.w_NUMREG = this.w_NUMREG
    oTo.w_DATREG = this.w_DATREG
    oTo.w_COMPET = this.w_COMPET
    oTo.w_OSUTEINS = this.w_OSUTEINS
    oTo.w_OSDATINS = this.w_OSDATINS
    oTo.w_OSUTEVAR = this.w_OSUTEVAR
    oTo.w_OSDATVAR = this.w_OSDATVAR
    oTo.w_RIFFAT = this.w_RIFFAT
    oTo.w_SERIALE_OPERAZIONE = this.w_SERIALE_OPERAZIONE
    oTo.w_OSRIFACC = this.w_OSRIFACC
    oTo.w_RIFACC = this.w_RIFACC
    oTo.w_NUMREG_A = this.w_NUMREG_A
    oTo.w_DATREG_A = this.w_DATREG_A
    oTo.w_COMPET_A = this.w_COMPET_A
    oTo.w_SERIALE_ACCONTO = this.w_SERIALE_ACCONTO
    oTo.w_ANDESCRI = this.w_ANDESCRI
    oTo.w_INTEFFET = this.w_INTEFFET
    oTo.w_ANDTOBSO = this.w_ANDTOBSO
    oTo.w_DERIFPNT = this.w_DERIFPNT
    oTo.w_DESERIAL = this.w_DESERIAL
    oTo.w_DE__AREA = this.w_DE__AREA
    oTo.w_CPROWNUM = this.w_CPROWNUM
    oTo.w_OBJECT = this.w_OBJECT
    oTo.w_TRASH = this.w_TRASH
    oTo.w_GEST = this.w_GEST
    PCContext::Load(oTo)
enddefine

define class tcgscg_aos as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 693
  Height = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-12-12"
  HelpContextID=176825705
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=44

  * --- Constant Properties
  OPERSUPE_IDX = 0
  PNT_MAST_IDX = 0
  CONTI_IDX = 0
  cFile = "OPERSUPE"
  cKeySelect = "OSSERIAL"
  cKeyWhere  = "OSSERIAL=this.w_OSSERIAL"
  cKeyWhereODBC = '"OSSERIAL="+cp_ToStrODBC(this.w_OSSERIAL)';

  cKeyWhereODBCqualified = '"OPERSUPE.OSSERIAL="+cp_ToStrODBC(this.w_OSSERIAL)';

  cPrg = "gscg_aos"
  cComment = "Operazioni superiori a 3000 euro"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_OSSERIAL = space(10)
  w_PNTIPDOC = space(1)
  w_PNTIPREG = space(1)
  w_ANOPETRE = space(1)
  w_PNCODVAL = space(3)
  w_OBTEST = ctod('  /  /  ')
  w_ANFLSOAL = space(1)
  w_OSTIPOPE = space(1)
  o_OSTIPOPE = space(1)
  w_OSRIFCON = space(30)
  w_OSTIPCLF = space(1)
  w_OSCODICE = space(15)
  w_OSTIPFAT = space(1)
  o_OSTIPFAT = space(1)
  w_OSANNRET = 0
  w_OSIMPFRA = space(1)
  w_OSRIFFAT = space(10)
  w_OSFLGEXT = space(1)
  o_OSFLGEXT = space(1)
  w_OSFLGDVE = space(1)
  w_OSGENERA = space(1)
  w_OSMODUTE = space(1)
  w_NUMREG = 0
  w_DATREG = ctod('  /  /  ')
  w_COMPET = space(4)
  w_OSUTEINS = 0
  w_OSDATINS = ctod('  /  /  ')
  w_OSUTEVAR = 0
  w_OSDATVAR = ctod('  /  /  ')
  w_RIFFAT = space(10)
  w_SERIALE_OPERAZIONE = space(10)
  w_OSRIFACC = space(10)
  w_RIFACC = space(10)
  w_NUMREG_A = 0
  w_DATREG_A = ctod('  /  /  ')
  w_COMPET_A = space(4)
  w_SERIALE_ACCONTO = space(10)
  w_ANDESCRI = space(40)
  w_INTEFFET = space(1)
  w_ANDTOBSO = ctod('  /  /  ')
  w_DERIFPNT = space(10)
  w_DESERIAL = space(10)
  w_DE__AREA = space(1)
  w_CPROWNUM = 0
  w_OBJECT = .F.
  w_TRASH = .F.
  w_GEST = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_aosPag1","gscg_aos",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 118378250
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOSTIPOPE_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='OPERSUPE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.OPERSUPE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.OPERSUPE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_aos'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_52_joined
    link_1_52_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from OPERSUPE where OSSERIAL=KeySet.OSSERIAL
    *
    i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('OPERSUPE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "OPERSUPE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' OPERSUPE '
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_52_joined=this.AddJoinedLink_1_52(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'OSSERIAL',this.w_OSSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PNTIPDOC = THIS.oParentObject.w_PNTIPDOC
        .w_PNTIPREG = THIS.oParentObject.w_PNTIPREG
        .w_ANOPETRE = THIS.oParentObject.w_ANOPETRE
        .w_PNCODVAL = THIS.oParentObject.w_PNCODVAL
        .w_ANFLSOAL = THIS.oParentObject.w_ANFLSOAL
        .w_NUMREG = 0
        .w_DATREG = ctod("  /  /  ")
        .w_COMPET = space(4)
        .w_RIFFAT = This.oParentObject.w_PNSERIAL
        .w_SERIALE_OPERAZIONE = This.oParentObject.w_PNSERIAL
        .w_RIFACC = This.oParentObject.w_PNSERIAL
        .w_NUMREG_A = 0
        .w_DATREG_A = ctod("  /  /  ")
        .w_COMPET_A = space(4)
        .w_SERIALE_ACCONTO = This.oParentObject.w_PNSERIAL
        .w_ANDESCRI = space(40)
        .w_INTEFFET = space(1)
        .w_ANDTOBSO = ctod("  /  /  ")
        .w_DERIFPNT = .w_OSSERIAL
        .w_DESERIAL = space(10)
        .w_DE__AREA = space(1)
        .w_CPROWNUM = 0
        .w_OBJECT = .f.
        .w_TRASH = .f.
        .w_GEST = .f.
        .w_OSSERIAL = NVL(OSSERIAL,space(10))
        .w_OBTEST = This.oParentObject .w_OBTEST
        .w_OSTIPOPE = NVL(OSTIPOPE,space(1))
        .w_OSRIFCON = NVL(OSRIFCON,space(30))
        .w_OSTIPCLF = NVL(OSTIPCLF,space(1))
        .w_OSCODICE = NVL(OSCODICE,space(15))
          if link_1_13_joined
            this.w_OSCODICE = NVL(ANCODICE113,NVL(this.w_OSCODICE,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI113,space(40))
            this.w_INTEFFET = NVL(ANOPETRE113,space(1))
            this.w_ANDTOBSO = NVL(cp_ToDate(ANDTOBSO113),ctod("  /  /  "))
          else
          .link_1_13('Load')
          endif
        .w_OSTIPFAT = NVL(OSTIPFAT,space(1))
        .w_OSANNRET = NVL(OSANNRET,0)
        .w_OSIMPFRA = NVL(OSIMPFRA,space(1))
        .w_OSRIFFAT = NVL(OSRIFFAT,space(10))
          if link_1_19_joined
            this.w_OSRIFFAT = NVL(PNSERIAL119,NVL(this.w_OSRIFFAT,space(10)))
            this.w_NUMREG = NVL(PNNUMRER119,0)
            this.w_DATREG = NVL(cp_ToDate(PNDATREG119),ctod("  /  /  "))
            this.w_COMPET = NVL(PNCOMPET119,space(4))
          else
          .link_1_19('Load')
          endif
        .w_OSFLGEXT = NVL(OSFLGEXT,space(1))
        .w_OSFLGDVE = NVL(OSFLGDVE,space(1))
        .w_OSGENERA = NVL(OSGENERA,space(1))
        .w_OSMODUTE = NVL(OSMODUTE,space(1))
        .w_OSUTEINS = NVL(OSUTEINS,0)
        .w_OSDATINS = NVL(cp_ToDate(OSDATINS),ctod("  /  /  "))
        .w_OSUTEVAR = NVL(OSUTEVAR,0)
        .w_OSDATVAR = NVL(cp_ToDate(OSDATVAR),ctod("  /  /  "))
          .link_1_48('Load')
          .link_1_50('Load')
        .w_OSRIFACC = NVL(OSRIFACC,space(10))
          if link_1_52_joined
            this.w_OSRIFACC = NVL(PNSERIAL152,NVL(this.w_OSRIFACC,space(10)))
            this.w_NUMREG_A = NVL(PNNUMRER152,0)
            this.w_DATREG_A = NVL(cp_ToDate(PNDATREG152),ctod("  /  /  "))
            this.w_COMPET_A = NVL(PNCOMPET152,space(4))
          else
          .link_1_52('Load')
          endif
          .link_1_54('Load')
          .link_1_62('Load')
        cp_LoadRecExtFlds(this,'OPERSUPE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_OSSERIAL = space(10)
      .w_PNTIPDOC = space(1)
      .w_PNTIPREG = space(1)
      .w_ANOPETRE = space(1)
      .w_PNCODVAL = space(3)
      .w_OBTEST = ctod("  /  /  ")
      .w_ANFLSOAL = space(1)
      .w_OSTIPOPE = space(1)
      .w_OSRIFCON = space(30)
      .w_OSTIPCLF = space(1)
      .w_OSCODICE = space(15)
      .w_OSTIPFAT = space(1)
      .w_OSANNRET = 0
      .w_OSIMPFRA = space(1)
      .w_OSRIFFAT = space(10)
      .w_OSFLGEXT = space(1)
      .w_OSFLGDVE = space(1)
      .w_OSGENERA = space(1)
      .w_OSMODUTE = space(1)
      .w_NUMREG = 0
      .w_DATREG = ctod("  /  /  ")
      .w_COMPET = space(4)
      .w_OSUTEINS = 0
      .w_OSDATINS = ctod("  /  /  ")
      .w_OSUTEVAR = 0
      .w_OSDATVAR = ctod("  /  /  ")
      .w_RIFFAT = space(10)
      .w_SERIALE_OPERAZIONE = space(10)
      .w_OSRIFACC = space(10)
      .w_RIFACC = space(10)
      .w_NUMREG_A = 0
      .w_DATREG_A = ctod("  /  /  ")
      .w_COMPET_A = space(4)
      .w_SERIALE_ACCONTO = space(10)
      .w_ANDESCRI = space(40)
      .w_INTEFFET = space(1)
      .w_ANDTOBSO = ctod("  /  /  ")
      .w_DERIFPNT = space(10)
      .w_DESERIAL = space(10)
      .w_DE__AREA = space(1)
      .w_CPROWNUM = 0
      .w_OBJECT = .f.
      .w_TRASH = .f.
      .w_GEST = .f.
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_PNTIPDOC = THIS.oParentObject.w_PNTIPDOC
        .w_PNTIPREG = THIS.oParentObject.w_PNTIPREG
        .w_ANOPETRE = THIS.oParentObject.w_ANOPETRE
        .w_PNCODVAL = THIS.oParentObject.w_PNCODVAL
        .w_OBTEST = This.oParentObject .w_OBTEST
        .w_ANFLSOAL = THIS.oParentObject.w_ANFLSOAL
        .w_OSTIPOPE = iif(.w_ANOPETRE="E", "N", .w_ANOPETRE)
        .w_OSRIFCON = SPACE(30)
        .w_OSTIPCLF = This.oParentObject .w_PNTIPCLF
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_OSCODICE))
          .link_1_13('Full')
          endif
        .w_OSTIPFAT = ICASE(.w_PNTIPDOC $ 'FA/FC/FE',  'S', .w_PNTIPDOC $ 'NC/NE/NU', 'N', .w_PNTIPREG $ 'C/E', 'C',  'S')
        .w_OSANNRET = IIF(.w_OSTIPFAT='N',Year(THIS.oParentObject.w_PNDATREG),0)
        .w_OSIMPFRA = IIF(.w_OSTIPFAT='S','S','N')
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_OSRIFFAT))
          .link_1_19('Full')
          endif
        .w_OSFLGEXT = iif(.w_ANOPETRE="E", "F", "N")
        .w_OSFLGDVE = 'N'
        .w_OSGENERA = 'N'
        .w_OSMODUTE = 'S'
          .DoRTCalc(20,22,.f.)
        .w_OSUTEINS = i_CODUTE
        .w_OSDATINS = i_DATSYS
        .w_OSUTEVAR = i_CODUTE
        .w_OSDATVAR = i_DATSYS
        .w_RIFFAT = This.oParentObject.w_PNSERIAL
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_RIFFAT))
          .link_1_48('Full')
          endif
        .w_SERIALE_OPERAZIONE = This.oParentObject.w_PNSERIAL
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_SERIALE_OPERAZIONE))
          .link_1_50('Full')
          endif
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_OSRIFACC))
          .link_1_52('Full')
          endif
        .w_RIFACC = This.oParentObject.w_PNSERIAL
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_RIFACC))
          .link_1_54('Full')
          endif
          .DoRTCalc(31,33,.f.)
        .w_SERIALE_ACCONTO = This.oParentObject.w_PNSERIAL
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_SERIALE_ACCONTO))
          .link_1_62('Full')
          endif
          .DoRTCalc(35,37,.f.)
        .w_DERIFPNT = .w_OSSERIAL
      endif
    endwith
    cp_BlankRecExtFlds(this,'OPERSUPE')
    this.DoRTCalc(39,44,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_aos
    if this.cFunction="Load" or (this.cFunction="Edit" and empty(this.oParentObject.w_OSTIPORE))
       this.bupdated=.t.
    endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oOSTIPOPE_1_8.enabled = i_bVal
      .Page1.oPag.oOSRIFCON_1_10.enabled = i_bVal
      .Page1.oPag.oOSTIPCLF_1_12.enabled = i_bVal
      .Page1.oPag.oOSCODICE_1_13.enabled = i_bVal
      .Page1.oPag.oOSTIPFAT_1_14.enabled = i_bVal
      .Page1.oPag.oOSANNRET_1_15.enabled = i_bVal
      .Page1.oPag.oOSIMPFRA_1_16.enabled = i_bVal
      .Page1.oPag.oOSFLGEXT_1_21.enabled = i_bVal
      .Page1.oPag.oOSFLGDVE_1_22.enabled = i_bVal
      .Page1.oPag.oBtn_1_17.enabled = i_bVal
      .Page1.oPag.oBtn_1_45.enabled = i_bVal
      .Page1.oPag.oBtn_1_51.enabled = i_bVal
      .Page1.oPag.oBtn_1_55.enabled = i_bVal
      .Page1.oPag.oBtn_1_74.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'OPERSUPE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gscg_aos
    this.mEnableControls()
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSSERIAL,"OSSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSTIPOPE,"OSTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSRIFCON,"OSRIFCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSTIPCLF,"OSTIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSCODICE,"OSCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSTIPFAT,"OSTIPFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSANNRET,"OSANNRET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSIMPFRA,"OSIMPFRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSRIFFAT,"OSRIFFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSFLGEXT,"OSFLGEXT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSFLGDVE,"OSFLGDVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSGENERA,"OSGENERA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSMODUTE,"OSMODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSUTEINS,"OSUTEINS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSDATINS,"OSDATINS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSUTEVAR,"OSUTEVAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSDATVAR,"OSDATVAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OSRIFACC,"OSRIFACC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.OPERSUPE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into OPERSUPE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'OPERSUPE')
        i_extval=cp_InsertValODBCExtFlds(this,'OPERSUPE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(OSSERIAL,OSTIPOPE,OSRIFCON,OSTIPCLF,OSCODICE"+;
                  ",OSTIPFAT,OSANNRET,OSIMPFRA,OSRIFFAT,OSFLGEXT"+;
                  ",OSFLGDVE,OSGENERA,OSMODUTE,OSUTEINS,OSDATINS"+;
                  ",OSUTEVAR,OSDATVAR,OSRIFACC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_OSSERIAL)+;
                  ","+cp_ToStrODBC(this.w_OSTIPOPE)+;
                  ","+cp_ToStrODBC(this.w_OSRIFCON)+;
                  ","+cp_ToStrODBC(this.w_OSTIPCLF)+;
                  ","+cp_ToStrODBCNull(this.w_OSCODICE)+;
                  ","+cp_ToStrODBC(this.w_OSTIPFAT)+;
                  ","+cp_ToStrODBC(this.w_OSANNRET)+;
                  ","+cp_ToStrODBC(this.w_OSIMPFRA)+;
                  ","+cp_ToStrODBCNull(this.w_OSRIFFAT)+;
                  ","+cp_ToStrODBC(this.w_OSFLGEXT)+;
                  ","+cp_ToStrODBC(this.w_OSFLGDVE)+;
                  ","+cp_ToStrODBC(this.w_OSGENERA)+;
                  ","+cp_ToStrODBC(this.w_OSMODUTE)+;
                  ","+cp_ToStrODBC(this.w_OSUTEINS)+;
                  ","+cp_ToStrODBC(this.w_OSDATINS)+;
                  ","+cp_ToStrODBC(this.w_OSUTEVAR)+;
                  ","+cp_ToStrODBC(this.w_OSDATVAR)+;
                  ","+cp_ToStrODBCNull(this.w_OSRIFACC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'OPERSUPE')
        i_extval=cp_InsertValVFPExtFlds(this,'OPERSUPE')
        cp_CheckDeletedKey(i_cTable,0,'OSSERIAL',this.w_OSSERIAL)
        INSERT INTO (i_cTable);
              (OSSERIAL,OSTIPOPE,OSRIFCON,OSTIPCLF,OSCODICE,OSTIPFAT,OSANNRET,OSIMPFRA,OSRIFFAT,OSFLGEXT,OSFLGDVE,OSGENERA,OSMODUTE,OSUTEINS,OSDATINS,OSUTEVAR,OSDATVAR,OSRIFACC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_OSSERIAL;
                  ,this.w_OSTIPOPE;
                  ,this.w_OSRIFCON;
                  ,this.w_OSTIPCLF;
                  ,this.w_OSCODICE;
                  ,this.w_OSTIPFAT;
                  ,this.w_OSANNRET;
                  ,this.w_OSIMPFRA;
                  ,this.w_OSRIFFAT;
                  ,this.w_OSFLGEXT;
                  ,this.w_OSFLGDVE;
                  ,this.w_OSGENERA;
                  ,this.w_OSMODUTE;
                  ,this.w_OSUTEINS;
                  ,this.w_OSDATINS;
                  ,this.w_OSUTEVAR;
                  ,this.w_OSDATVAR;
                  ,this.w_OSRIFACC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.OPERSUPE_IDX,i_nConn)
      *
      * update OPERSUPE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'OPERSUPE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " OSTIPOPE="+cp_ToStrODBC(this.w_OSTIPOPE)+;
             ",OSRIFCON="+cp_ToStrODBC(this.w_OSRIFCON)+;
             ",OSTIPCLF="+cp_ToStrODBC(this.w_OSTIPCLF)+;
             ",OSCODICE="+cp_ToStrODBCNull(this.w_OSCODICE)+;
             ",OSTIPFAT="+cp_ToStrODBC(this.w_OSTIPFAT)+;
             ",OSANNRET="+cp_ToStrODBC(this.w_OSANNRET)+;
             ",OSIMPFRA="+cp_ToStrODBC(this.w_OSIMPFRA)+;
             ",OSRIFFAT="+cp_ToStrODBCNull(this.w_OSRIFFAT)+;
             ",OSFLGEXT="+cp_ToStrODBC(this.w_OSFLGEXT)+;
             ",OSFLGDVE="+cp_ToStrODBC(this.w_OSFLGDVE)+;
             ",OSGENERA="+cp_ToStrODBC(this.w_OSGENERA)+;
             ",OSMODUTE="+cp_ToStrODBC(this.w_OSMODUTE)+;
             ",OSUTEINS="+cp_ToStrODBC(this.w_OSUTEINS)+;
             ",OSDATINS="+cp_ToStrODBC(this.w_OSDATINS)+;
             ",OSUTEVAR="+cp_ToStrODBC(this.w_OSUTEVAR)+;
             ",OSDATVAR="+cp_ToStrODBC(this.w_OSDATVAR)+;
             ",OSRIFACC="+cp_ToStrODBCNull(this.w_OSRIFACC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'OPERSUPE')
        i_cWhere = cp_PKFox(i_cTable  ,'OSSERIAL',this.w_OSSERIAL  )
        UPDATE (i_cTable) SET;
              OSTIPOPE=this.w_OSTIPOPE;
             ,OSRIFCON=this.w_OSRIFCON;
             ,OSTIPCLF=this.w_OSTIPCLF;
             ,OSCODICE=this.w_OSCODICE;
             ,OSTIPFAT=this.w_OSTIPFAT;
             ,OSANNRET=this.w_OSANNRET;
             ,OSIMPFRA=this.w_OSIMPFRA;
             ,OSRIFFAT=this.w_OSRIFFAT;
             ,OSFLGEXT=this.w_OSFLGEXT;
             ,OSFLGDVE=this.w_OSFLGDVE;
             ,OSGENERA=this.w_OSGENERA;
             ,OSMODUTE=this.w_OSMODUTE;
             ,OSUTEINS=this.w_OSUTEINS;
             ,OSDATINS=this.w_OSDATINS;
             ,OSUTEVAR=this.w_OSUTEVAR;
             ,OSDATVAR=this.w_OSDATVAR;
             ,OSRIFACC=this.w_OSRIFACC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.OPERSUPE_IDX,i_nConn)
      *
      * delete OPERSUPE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'OSSERIAL',this.w_OSSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_OBTEST = This.oParentObject .w_OBTEST
        .DoRTCalc(7,8,.t.)
        if .o_OSTIPOPE<>.w_OSTIPOPE
            .w_OSRIFCON = SPACE(30)
        endif
        .DoRTCalc(10,12,.t.)
        if .o_OSTIPFAT<>.w_OSTIPFAT
            .w_OSANNRET = IIF(.w_OSTIPFAT='N',Year(THIS.oParentObject.w_PNDATREG),0)
        endif
        if .o_OSTIPFAT<>.w_OSTIPFAT
            .w_OSIMPFRA = IIF(.w_OSTIPFAT='S','S','N')
        endif
          .link_1_19('Full')
        if .o_OSTIPFAT<>.w_OSTIPFAT
          .Calculate_TWBXQIWBWB()
        endif
        .DoRTCalc(16,24,.t.)
            .w_OSUTEVAR = i_CODUTE
            .w_OSDATVAR = i_DATSYS
        if .o_OSFLGEXT<>.w_OSFLGEXT
          .Calculate_IOVRWRIZOU()
        endif
          .link_1_48('Full')
          .link_1_50('Full')
          .link_1_52('Full')
          .link_1_54('Full')
        .DoRTCalc(31,33,.t.)
          .link_1_62('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(35,44,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_TWBXQIWBWB()
    with this
          * --- Azzeramento operazioni da rettificare
          .w_OSRIFFAT = SPACE(10)
          .w_OSRIFACC = SPACE(10)
          .w_NUMREG = 0
          .w_NUMREG_A = 0
          .w_DATREG = CTOD('  -  -    ')
          .w_DATREG_A = CTOD('  -  -    ')
          .w_COMPET = SPACE(4)
          .w_COMPET_A = SPACE(4)
    endwith
  endproc
  proc Calculate_JYFFPJFMPO()
    with this
          * --- DEFAULT
          .w_PNTIPDOC = THIS.oParentObject.w_PNTIPDOC
          .w_PNTIPREG = THIS.oParentObject.w_PNTIPREG
          .w_ANOPETRE = THIS.oParentObject.w_ANOPETRE
          .w_OSTIPOPE = iif(.w_ANOPETRE="E", "N", .w_ANOPETRE)
          .w_OSTIPFAT = ICASE(.w_PNTIPDOC $ 'FA/FC/FE',  'S', .w_PNTIPDOC $ 'NC/NE/NU', 'N', .w_PNTIPREG $ 'C/E', 'C',  'S')
          .w_OSIMPFRA = IIF(.w_OSTIPFAT='S','S','N')
          .w_OSFLGEXT = iif(.w_ANOPETRE="E", "F", "N")
          .w_OSTIPCLF = THIS.oParentObject.w_PNTIPCLF
          .w_OSCODICE = SPACE(15)
          .w_ANDESCRI = SPACE(40)
          .w_OSANNRET = IIF(.w_OSTIPFAT='N',Year(This.oParentObject.w_PNDATREG),0)
    endwith
  endproc
  proc Calculate_IOVRWRIZOU()
    with this
          * --- Tipo Estrazione
          .w_OSTIPOPE = iif(.w_OSFLGEXT="F", "N", .w_OSTIPOPE)
          .w_OSRIFCON = iif(.w_OSFLGEXT="F", Space(30), .w_OSRIFCON)
          .w_OSRIFFAT = iif(.w_OSFLGEXT="F", Space(10), .w_OSRIFFAT)
          .w_OSRIFACC = iif(.w_OSFLGEXT="F", Space(10), .w_OSRIFACC)
          .w_NUMREG = iif(.w_OSFLGEXT="F",0,.w_NUMREG)
          .w_NUMREG_A = iif(.w_OSFLGEXT="F",0,.w_NUMREG)
          .w_DATREG = iif(.w_OSFLGEXT="F",CTOD('  -  -    '),.w_DATREG)
          .w_DATREG_A = iif(.w_OSFLGEXT="F",CTOD('  -  -    '),.w_DATREG)
          .w_COMPET = iif(.w_OSFLGEXT="F",Space(4),.w_COMPET)
          .w_COMPET_A = iif(.w_OSFLGEXT="F",Space(4),.w_COMPET)
          .w_OSTIPFAT = IIF(.w_OSFLGEXT="F",ICASE(.w_PNTIPDOC $ 'FA/FC/FE',  'S', .w_PNTIPDOC $ 'NC/NE/NU', 'N', .w_PNTIPREG $ 'C/E', 'C',  'S'),.w_OSTIPFAT)
          .w_OSIMPFRA = IIF(.w_OSFLGEXT="F",IIF(.w_OSTIPFAT='S','S','N'),.w_OSIMPFRA)
          .w_OSTIPCLF = IIF(.w_OSFLGEXT="F",This.oParentObject.w_PNTIPCLF,.w_OSTIPCLF)
          .w_OSCODICE = IIF(.w_OSFLGEXT="F",space(15),.w_OSCODICE)
          .w_ANDESCRI = IIF(.w_OSFLGEXT="F",space(40),.w_ANDESCRI)
          .w_OSANNRET = IIF(.w_OSFLGEXT='F' AND .w_OSTIPFAT<>'N' ,0, IIF (EMPTY (.w_OSANNRET) , Year(THIS.oParentObject.w_PNDATREG) , .w_OSANNRET ) )
    endwith
  endproc
  proc Calculate_OEBUCQVPFH()
    with this
          * --- Apertura dati estratti
          .w_OBJECT = Gsai_Ade()
          .w_TRASH = .w_Object.EcpFilter()
          .w_Object.w_Deserial = .w_Deserial
          .w_TRASH = .w_Object.EcpSave()
          .w_Object.opGFRM.ActivePage = Icase(.w_De__Area='1',2,.w_De__Area='2',3,.w_De__Area='4',4,1)
          .w_GEST = Icase(.w_De__Area='1',.w_Object.Gsai_Mde,.w_De__Area='2',.w_Object.Gsai2Mde,.w_De__Area='4',.w_Object.Gsai4Mde,.f.)
          .w_TRASH = IIF(Empty(.w_De__Area),-1,.w_Gest.Search('Cprownum='+Alltrim(Str(.w_Cprownum,6,0))))
          .w_TRASH = IIF(.w_Trash<0,.f.,.w_Gest.SetRow(.w_Trash))
          .w_TRASH = IIF(.w_Trash,.w_Gest.Refresh(),.f.)
    endwith
  endproc
  proc Calculate_FQIAHJIXUO()
    with this
          * --- Lettura tabella "Dati estratti"
          GSAI_BDT(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOSTIPOPE_1_8.enabled = this.oPgFrm.Page1.oPag.oOSTIPOPE_1_8.mCond()
    this.oPgFrm.Page1.oPag.oOSRIFCON_1_10.enabled = this.oPgFrm.Page1.oPag.oOSRIFCON_1_10.mCond()
    this.oPgFrm.Page1.oPag.oOSTIPCLF_1_12.enabled = this.oPgFrm.Page1.oPag.oOSTIPCLF_1_12.mCond()
    this.oPgFrm.Page1.oPag.oOSCODICE_1_13.enabled = this.oPgFrm.Page1.oPag.oOSCODICE_1_13.mCond()
    this.oPgFrm.Page1.oPag.oOSTIPFAT_1_14.enabled = this.oPgFrm.Page1.oPag.oOSTIPFAT_1_14.mCond()
    this.oPgFrm.Page1.oPag.oOSANNRET_1_15.enabled = this.oPgFrm.Page1.oPag.oOSANNRET_1_15.mCond()
    this.oPgFrm.Page1.oPag.oOSIMPFRA_1_16.enabled = this.oPgFrm.Page1.oPag.oOSIMPFRA_1_16.mCond()
    this.oPgFrm.Page1.oPag.oOSFLGEXT_1_21.enabled = this.oPgFrm.Page1.oPag.oOSFLGEXT_1_21.mCond()
    this.oPgFrm.Page1.oPag.oOSFLGDVE_1_22.enabled = this.oPgFrm.Page1.oPag.oOSFLGDVE_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_74.enabled = this.oPgFrm.Page1.oPag.oBtn_1_74.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oOSANNRET_1_15.visible=!this.oPgFrm.Page1.oPag.oOSANNRET_1_15.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_17.visible=!this.oPgFrm.Page1.oPag.oBtn_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oNUMREG_1_27.visible=!this.oPgFrm.Page1.oPag.oNUMREG_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDATREG_1_31.visible=!this.oPgFrm.Page1.oPag.oDATREG_1_31.mHide()
    this.oPgFrm.Page1.oPag.oCOMPET_1_32.visible=!this.oPgFrm.Page1.oPag.oCOMPET_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_51.visible=!this.oPgFrm.Page1.oPag.oBtn_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_55.visible=!this.oPgFrm.Page1.oPag.oBtn_1_55.mHide()
    this.oPgFrm.Page1.oPag.oNUMREG_A_1_56.visible=!this.oPgFrm.Page1.oPag.oNUMREG_A_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oDATREG_A_1_60.visible=!this.oPgFrm.Page1.oPag.oDATREG_A_1_60.mHide()
    this.oPgFrm.Page1.oPag.oCOMPET_A_1_61.visible=!this.oPgFrm.Page1.oPag.oCOMPET_A_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_74.visible=!this.oPgFrm.Page1.oPag.oBtn_1_74.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Default")
          .Calculate_JYFFPJFMPO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Dati_estratti")
          .Calculate_OEBUCQVPFH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_FQIAHJIXUO()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=OSCODICE
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OSCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_OSCODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_OSTIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_OSTIPCLF;
                     ,'ANCODICE',trim(this.w_OSCODICE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OSCODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_OSCODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_OSTIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_OSCODICE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_OSTIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OSCODICE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oOSCODICE_1_13'),i_cWhere,'',"ELENCO CONTI",'Gscg_Aos.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_OSTIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice conto selezionato ha tipologia operazioni IVA ad escluso oppure � obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_OSTIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OSCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_OSCODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_OSTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_OSTIPCLF;
                       ,'ANCODICE',this.w_OSCODICE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OSCODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_INTEFFET = NVL(_Link_.ANOPETRE,space(1))
      this.w_ANDTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_OSCODICE = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_INTEFFET = space(1)
      this.w_ANDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_INTEFFET $ 'PCN' And (.w_ANDTOBSO>.w_OBTEST OR EMPTY(.w_ANDTOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice conto selezionato ha tipologia operazioni IVA ad escluso oppure � obsoleto")
        endif
        this.w_OSCODICE = space(15)
        this.w_ANDESCRI = space(40)
        this.w_INTEFFET = space(1)
        this.w_ANDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OSCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.ANCODICE as ANCODICE113"+ ",link_1_13.ANDESCRI as ANDESCRI113"+ ",link_1_13.ANOPETRE as ANOPETRE113"+ ",link_1_13.ANDTOBSO as ANDTOBSO113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on OPERSUPE.OSCODICE=link_1_13.ANCODICE"+" and OPERSUPE.OSTIPCLF=link_1_13.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and OPERSUPE.OSCODICE=link_1_13.ANCODICE(+)"'+'+" and OPERSUPE.OSTIPCLF=link_1_13.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OSRIFFAT
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_lTable = "PNT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2], .t., this.PNT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OSRIFFAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OSRIFFAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PNSERIAL,PNNUMRER,PNDATREG,PNCOMPET";
                   +" from "+i_cTable+" "+i_lTable+" where PNSERIAL="+cp_ToStrODBC(this.w_OSRIFFAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PNSERIAL',this.w_OSRIFFAT)
            select PNSERIAL,PNNUMRER,PNDATREG,PNCOMPET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OSRIFFAT = NVL(_Link_.PNSERIAL,space(10))
      this.w_NUMREG = NVL(_Link_.PNNUMRER,0)
      this.w_DATREG = NVL(cp_ToDate(_Link_.PNDATREG),ctod("  /  /  "))
      this.w_COMPET = NVL(_Link_.PNCOMPET,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_OSRIFFAT = space(10)
      endif
      this.w_NUMREG = 0
      this.w_DATREG = ctod("  /  /  ")
      this.w_COMPET = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.PNSERIAL,1)
      cp_ShowWarn(i_cKey,this.PNT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OSRIFFAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PNT_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.PNSERIAL as PNSERIAL119"+ ",link_1_19.PNNUMRER as PNNUMRER119"+ ",link_1_19.PNDATREG as PNDATREG119"+ ",link_1_19.PNCOMPET as PNCOMPET119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on OPERSUPE.OSRIFFAT=link_1_19.PNSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and OPERSUPE.OSRIFFAT=link_1_19.PNSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RIFFAT
  func Link_1_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
    i_lTable = "OPERSUPE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2], .t., this.OPERSUPE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIFFAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIFFAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OSRIFFAT";
                   +" from "+i_cTable+" "+i_lTable+" where OSRIFFAT="+cp_ToStrODBC(this.w_RIFFAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OSRIFFAT',this.w_RIFFAT)
            select OSRIFFAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIFFAT = NVL(_Link_.OSRIFFAT,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_RIFFAT = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])+'\'+cp_ToStr(_Link_.OSRIFFAT,1)
      cp_ShowWarn(i_cKey,this.OPERSUPE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIFFAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SERIALE_OPERAZIONE
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
    i_lTable = "OPERSUPE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2], .t., this.OPERSUPE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERIALE_OPERAZIONE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERIALE_OPERAZIONE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OSRIFFAT";
                   +" from "+i_cTable+" "+i_lTable+" where OSRIFFAT="+cp_ToStrODBC(this.w_SERIALE_OPERAZIONE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OSRIFFAT',this.w_SERIALE_OPERAZIONE)
            select OSRIFFAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERIALE_OPERAZIONE = NVL(_Link_.OSRIFFAT,space(10))
      this.w_RIFFAT = NVL(_Link_.OSRIFFAT,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SERIALE_OPERAZIONE = space(10)
      endif
      this.w_RIFFAT = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])+'\'+cp_ToStr(_Link_.OSRIFFAT,1)
      cp_ShowWarn(i_cKey,this.OPERSUPE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERIALE_OPERAZIONE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OSRIFACC
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_lTable = "PNT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2], .t., this.PNT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OSRIFACC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OSRIFACC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PNSERIAL,PNNUMRER,PNDATREG,PNCOMPET";
                   +" from "+i_cTable+" "+i_lTable+" where PNSERIAL="+cp_ToStrODBC(this.w_OSRIFACC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PNSERIAL',this.w_OSRIFACC)
            select PNSERIAL,PNNUMRER,PNDATREG,PNCOMPET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OSRIFACC = NVL(_Link_.PNSERIAL,space(10))
      this.w_NUMREG_A = NVL(_Link_.PNNUMRER,0)
      this.w_DATREG_A = NVL(cp_ToDate(_Link_.PNDATREG),ctod("  /  /  "))
      this.w_COMPET_A = NVL(_Link_.PNCOMPET,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_OSRIFACC = space(10)
      endif
      this.w_NUMREG_A = 0
      this.w_DATREG_A = ctod("  /  /  ")
      this.w_COMPET_A = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.PNSERIAL,1)
      cp_ShowWarn(i_cKey,this.PNT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OSRIFACC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_52(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PNT_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_52.PNSERIAL as PNSERIAL152"+ ",link_1_52.PNNUMRER as PNNUMRER152"+ ",link_1_52.PNDATREG as PNDATREG152"+ ",link_1_52.PNCOMPET as PNCOMPET152"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_52 on OPERSUPE.OSRIFACC=link_1_52.PNSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_52"
          i_cKey=i_cKey+'+" and OPERSUPE.OSRIFACC=link_1_52.PNSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RIFACC
  func Link_1_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
    i_lTable = "OPERSUPE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2], .t., this.OPERSUPE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIFACC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIFACC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OSRIFACC";
                   +" from "+i_cTable+" "+i_lTable+" where OSRIFACC="+cp_ToStrODBC(this.w_RIFACC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OSRIFACC',this.w_RIFACC)
            select OSRIFACC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIFACC = NVL(_Link_.OSRIFACC,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_RIFACC = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])+'\'+cp_ToStr(_Link_.OSRIFACC,1)
      cp_ShowWarn(i_cKey,this.OPERSUPE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIFACC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SERIALE_ACCONTO
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
    i_lTable = "OPERSUPE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2], .t., this.OPERSUPE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERIALE_ACCONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERIALE_ACCONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OSRIFACC";
                   +" from "+i_cTable+" "+i_lTable+" where OSRIFACC="+cp_ToStrODBC(this.w_SERIALE_ACCONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OSRIFACC',this.w_SERIALE_ACCONTO)
            select OSRIFACC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERIALE_ACCONTO = NVL(_Link_.OSRIFACC,space(10))
      this.w_RIFACC = NVL(_Link_.OSRIFACC,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SERIALE_ACCONTO = space(10)
      endif
      this.w_RIFACC = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])+'\'+cp_ToStr(_Link_.OSRIFACC,1)
      cp_ShowWarn(i_cKey,this.OPERSUPE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERIALE_ACCONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oOSTIPOPE_1_8.RadioValue()==this.w_OSTIPOPE)
      this.oPgFrm.Page1.oPag.oOSTIPOPE_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOSRIFCON_1_10.value==this.w_OSRIFCON)
      this.oPgFrm.Page1.oPag.oOSRIFCON_1_10.value=this.w_OSRIFCON
    endif
    if not(this.oPgFrm.Page1.oPag.oOSTIPCLF_1_12.RadioValue()==this.w_OSTIPCLF)
      this.oPgFrm.Page1.oPag.oOSTIPCLF_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOSCODICE_1_13.value==this.w_OSCODICE)
      this.oPgFrm.Page1.oPag.oOSCODICE_1_13.value=this.w_OSCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oOSTIPFAT_1_14.RadioValue()==this.w_OSTIPFAT)
      this.oPgFrm.Page1.oPag.oOSTIPFAT_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOSANNRET_1_15.value==this.w_OSANNRET)
      this.oPgFrm.Page1.oPag.oOSANNRET_1_15.value=this.w_OSANNRET
    endif
    if not(this.oPgFrm.Page1.oPag.oOSIMPFRA_1_16.RadioValue()==this.w_OSIMPFRA)
      this.oPgFrm.Page1.oPag.oOSIMPFRA_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOSFLGEXT_1_21.RadioValue()==this.w_OSFLGEXT)
      this.oPgFrm.Page1.oPag.oOSFLGEXT_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOSFLGDVE_1_22.RadioValue()==this.w_OSFLGDVE)
      this.oPgFrm.Page1.oPag.oOSFLGDVE_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOSGENERA_1_23.RadioValue()==this.w_OSGENERA)
      this.oPgFrm.Page1.oPag.oOSGENERA_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOSMODUTE_1_24.RadioValue()==this.w_OSMODUTE)
      this.oPgFrm.Page1.oPag.oOSMODUTE_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREG_1_27.value==this.w_NUMREG)
      this.oPgFrm.Page1.oPag.oNUMREG_1_27.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_31.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_31.value=this.w_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPET_1_32.value==this.w_COMPET)
      this.oPgFrm.Page1.oPag.oCOMPET_1_32.value=this.w_COMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oOSUTEINS_1_39.value==this.w_OSUTEINS)
      this.oPgFrm.Page1.oPag.oOSUTEINS_1_39.value=this.w_OSUTEINS
    endif
    if not(this.oPgFrm.Page1.oPag.oOSDATINS_1_40.value==this.w_OSDATINS)
      this.oPgFrm.Page1.oPag.oOSDATINS_1_40.value=this.w_OSDATINS
    endif
    if not(this.oPgFrm.Page1.oPag.oOSUTEVAR_1_41.value==this.w_OSUTEVAR)
      this.oPgFrm.Page1.oPag.oOSUTEVAR_1_41.value=this.w_OSUTEVAR
    endif
    if not(this.oPgFrm.Page1.oPag.oOSDATVAR_1_42.value==this.w_OSDATVAR)
      this.oPgFrm.Page1.oPag.oOSDATVAR_1_42.value=this.w_OSDATVAR
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREG_A_1_56.value==this.w_NUMREG_A)
      this.oPgFrm.Page1.oPag.oNUMREG_A_1_56.value=this.w_NUMREG_A
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_A_1_60.value==this.w_DATREG_A)
      this.oPgFrm.Page1.oPag.oDATREG_A_1_60.value=this.w_DATREG_A
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPET_A_1_61.value==this.w_COMPET_A)
      this.oPgFrm.Page1.oPag.oCOMPET_A_1_61.value=this.w_COMPET_A
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_66.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_66.value=this.w_ANDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'OPERSUPE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_INTEFFET $ 'PCN' And (.w_ANDTOBSO>.w_OBTEST OR EMPTY(.w_ANDTOBSO)))  and (.w_ANFLSOAL='S' And .w_OSFLGEXT $ 'NI' and INLIST(.oParentObject.cFunction, 'Edit', 'Load'))  and not(empty(.w_OSCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOSCODICE_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice conto selezionato ha tipologia operazioni IVA ad escluso oppure � obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_OSTIPOPE = this.w_OSTIPOPE
    this.o_OSTIPFAT = this.w_OSTIPFAT
    this.o_OSFLGEXT = this.w_OSFLGEXT
    return

enddefine

* --- Define pages as container
define class tgscg_aosPag1 as StdContainer
  Width  = 689
  height = 390
  stdWidth  = 689
  stdheight = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oOSTIPOPE_1_8 as StdCombo with uid="QLYDJFUDAC",rtseq=8,rtrep=.f.,left=143,top=18,width=146,height=21;
    , ToolTipText = "Tipologia operazione";
    , HelpContextID = 104565973;
    , cFormVar="w_OSTIPOPE",RowSource=""+"Nessuno,"+"Corrispettivi periodici,"+"Contratti collegati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOSTIPOPE_1_8.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oOSTIPOPE_1_8.GetRadio()
    this.Parent.oContained.w_OSTIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oOSTIPOPE_1_8.SetRadio()
    this.Parent.oContained.w_OSTIPOPE=trim(this.Parent.oContained.w_OSTIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_OSTIPOPE=='N',1,;
      iif(this.Parent.oContained.w_OSTIPOPE=='P',2,;
      iif(this.Parent.oContained.w_OSTIPOPE=='C',3,;
      0)))
  endfunc

  func oOSTIPOPE_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSFLGEXT $ 'NI' and INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
    endwith
   endif
  endfunc

  add object oOSRIFCON_1_10 as StdField with uid="STNYSWHIZQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_OSRIFCON", cQueryName = "OSRIFCON",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Codice riferimento contratto",;
    HelpContextID = 47951052,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=426, Top=18, InputMask=replicate('X',30)

  func oOSRIFCON_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSTIPOPE<>'N' and INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
    endwith
   endif
  endfunc


  add object oOSTIPCLF_1_12 as StdCombo with uid="NQTUBCSYDW",rtseq=10,rtrep=.f.,left=38,top=90,width=91,height=21;
    , ToolTipText = "Tipologia conto";
    , HelpContextID = 37457108;
    , cFormVar="w_OSTIPCLF",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOSTIPCLF_1_12.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oOSTIPCLF_1_12.GetRadio()
    this.Parent.oContained.w_OSTIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oOSTIPCLF_1_12.SetRadio()
    this.Parent.oContained.w_OSTIPCLF=trim(this.Parent.oContained.w_OSTIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_OSTIPCLF=='C',1,;
      iif(this.Parent.oContained.w_OSTIPCLF=='F',2,;
      0))
  endfunc

  func oOSTIPCLF_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLSOAL='S' And .w_OSFLGEXT $ 'NI' and INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
    endwith
   endif
  endfunc

  func oOSTIPCLF_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_OSCODICE)
        bRes2=.link_1_13('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oOSCODICE_1_13 as StdField with uid="SARDUQNBLY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_OSCODICE", cQueryName = "OSCODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice conto selezionato ha tipologia operazioni IVA ad escluso oppure � obsoleto",;
    ToolTipText = "Codice conto",;
    HelpContextID = 50946859,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=143, Top=91, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_OSTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_OSCODICE"

  func oOSCODICE_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLSOAL='S' And .w_OSFLGEXT $ 'NI' and INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
    endwith
   endif
  endfunc

  func oOSCODICE_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oOSCODICE_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOSCODICE_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_OSTIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_OSTIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oOSCODICE_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO CONTI",'Gscg_Aos.CONTI_VZM',this.parent.oContained
  endproc


  add object oOSTIPFAT_1_14 as StdCombo with uid="CCIQUBHPSC",rtseq=12,rtrep=.f.,left=143,top=136,width=146,height=21;
    , ToolTipText = "Tipologia fattura";
    , HelpContextID = 12874554;
    , cFormVar="w_OSTIPFAT",RowSource=""+"Saldo,"+"Acconto,"+"Nota rettifica,"+"Corrispettivi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOSTIPFAT_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'A',;
    iif(this.value =3,'N',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oOSTIPFAT_1_14.GetRadio()
    this.Parent.oContained.w_OSTIPFAT = this.RadioValue()
    return .t.
  endfunc

  func oOSTIPFAT_1_14.SetRadio()
    this.Parent.oContained.w_OSTIPFAT=trim(this.Parent.oContained.w_OSTIPFAT)
    this.value = ;
      iif(this.Parent.oContained.w_OSTIPFAT=='S',1,;
      iif(this.Parent.oContained.w_OSTIPFAT=='A',2,;
      iif(this.Parent.oContained.w_OSTIPFAT=='N',3,;
      iif(this.Parent.oContained.w_OSTIPFAT=='C',4,;
      0))))
  endfunc

  func oOSTIPFAT_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSFLGEXT $ 'NI' and INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
    endwith
   endif
  endfunc

  add object oOSANNRET_1_15 as StdField with uid="EHEGHVDLSQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_OSANNRET", cQueryName = "OSANNRET",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno riferimento nota di rettifica",;
    HelpContextID = 212353850,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=433, Top=137, cSayPict="'9999'", cGetPict="'9999'"

  func oOSANNRET_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSTIPFAT='N' And .w_OSFLGEXT $ 'NI' and INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
    endwith
   endif
  endfunc

  func oOSANNRET_1_15.mHide()
    with this.Parent.oContained
      return (.w_OSTIPFAT<>'N')
    endwith
  endfunc

  add object oOSIMPFRA_1_16 as StdCheck with uid="QMPHMCSNEL",rtseq=14,rtrep=.f.,left=489, top=140, caption="Importo non frazionato",;
    ToolTipText = "Importo non frazionato",;
    HelpContextID = 13091623,;
    cFormVar="w_OSIMPFRA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOSIMPFRA_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOSIMPFRA_1_16.GetRadio()
    this.Parent.oContained.w_OSIMPFRA = this.RadioValue()
    return .t.
  endfunc

  func oOSIMPFRA_1_16.SetRadio()
    this.Parent.oContained.w_OSIMPFRA=trim(this.Parent.oContained.w_OSIMPFRA)
    this.value = ;
      iif(this.Parent.oContained.w_OSIMPFRA=='S',1,;
      0)
  endfunc

  func oOSIMPFRA_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSFLGEXT $ 'NI' and INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
    endwith
   endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="PGQDXZTSOD",left=470, top=220, width=20,height=23,;
    caption="...", nPag=1;
    , HelpContextID = 176624682;
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSCG_BOS(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_OSTIPFAT<>'N' Or .w_OSFLGEXT='F' Or Not Empty(.w_RIFFAT)  )
     endwith
    endif
  endfunc


  add object oOSFLGEXT_1_21 as StdCombo with uid="EKYDBOIZYU",rtseq=16,rtrep=.f.,left=143,top=266,width=145,height=21;
    , ToolTipText = "Tipo estrazione";
    , HelpContextID = 255234874;
    , cFormVar="w_OSFLGEXT",RowSource=""+"Inclusione forzata,"+"Esclusione forzata,"+"Nessuna forzatura", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOSFLGEXT_1_21.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oOSFLGEXT_1_21.GetRadio()
    this.Parent.oContained.w_OSFLGEXT = this.RadioValue()
    return .t.
  endfunc

  func oOSFLGEXT_1_21.SetRadio()
    this.Parent.oContained.w_OSFLGEXT=trim(this.Parent.oContained.w_OSFLGEXT)
    this.value = ;
      iif(this.Parent.oContained.w_OSFLGEXT=='I',1,;
      iif(this.Parent.oContained.w_OSFLGEXT=='F',2,;
      iif(this.Parent.oContained.w_OSFLGEXT=='N',3,;
      0)))
  endfunc

  func oOSFLGEXT_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
    endwith
   endif
  endfunc

  add object oOSFLGDVE_1_22 as StdCheck with uid="DTCCVDXNJU",rtseq=17,rtrep=.f.,left=143, top=309, caption="Dati da verificare",;
    ToolTipText = "Se attivo, identifica le registrazioni che l'utente vuole ricontrollare",;
    HelpContextID = 238457643,;
    cFormVar="w_OSFLGDVE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOSFLGDVE_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOSFLGDVE_1_22.GetRadio()
    this.Parent.oContained.w_OSFLGDVE = this.RadioValue()
    return .t.
  endfunc

  func oOSFLGDVE_1_22.SetRadio()
    this.Parent.oContained.w_OSFLGDVE=trim(this.Parent.oContained.w_OSFLGDVE)
    this.value = ;
      iif(this.Parent.oContained.w_OSFLGDVE=='S',1,;
      0)
  endfunc

  func oOSFLGDVE_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
    endwith
   endif
  endfunc

  add object oOSGENERA_1_23 as StdCheck with uid="RDUCRIQLGB",rtseq=18,rtrep=.f.,left=272, top=309, caption="Dati valorizzati da procedura", enabled=.f.,;
    ToolTipText = "Se attivo, identifica le registrazioni valorizzate dalla funzionalit� generazione dati",;
    HelpContextID = 262120231,;
    cFormVar="w_OSGENERA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOSGENERA_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOSGENERA_1_23.GetRadio()
    this.Parent.oContained.w_OSGENERA = this.RadioValue()
    return .t.
  endfunc

  func oOSGENERA_1_23.SetRadio()
    this.Parent.oContained.w_OSGENERA=trim(this.Parent.oContained.w_OSGENERA)
    this.value = ;
      iif(this.Parent.oContained.w_OSGENERA=='S',1,;
      0)
  endfunc

  add object oOSMODUTE_1_24 as StdCheck with uid="PGPYWVTVTL",rtseq=19,rtrep=.f.,left=468, top=309, caption="Dati inseriti/modificati dall'utente", enabled=.f.,;
    ToolTipText = "Se attivo, identifica le registrazioni caricate/variate dall'utente",;
    HelpContextID = 252314411,;
    cFormVar="w_OSMODUTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOSMODUTE_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOSMODUTE_1_24.GetRadio()
    this.Parent.oContained.w_OSMODUTE = this.RadioValue()
    return .t.
  endfunc

  func oOSMODUTE_1_24.SetRadio()
    this.Parent.oContained.w_OSMODUTE=trim(this.Parent.oContained.w_OSMODUTE)
    this.value = ;
      iif(this.Parent.oContained.w_OSMODUTE=='S',1,;
      0)
  endfunc

  add object oNUMREG_1_27 as StdField with uid="MAQPKSXXXG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 249756458,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=143, Top=222

  func oNUMREG_1_27.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFFAT)  Or .w_OSTIPFAT<>'N')
    endwith
  endfunc

  add object oDATREG_1_31 as StdField with uid="WIDSNVMGAF",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 249733066,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=271, Top=222

  func oDATREG_1_31.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFFAT)  Or .w_OSTIPFAT<>'N')
    endwith
  endfunc

  add object oCOMPET_1_32 as StdField with uid="CBCZXQFTLI",rtseq=22,rtrep=.f.,;
    cFormVar = "w_COMPET", cQueryName = "COMPET",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 31785434,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=424, Top=222, InputMask=replicate('X',4)

  func oCOMPET_1_32.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFFAT)  Or .w_OSTIPFAT<>'N')
    endwith
  endfunc

  add object oOSUTEINS_1_39 as StdField with uid="NBHJNMHAGI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_OSUTEINS", cQueryName = "OSUTEINS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente cbe ha inserito l'operazione",;
    HelpContextID = 216038599,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=156, Top=364, cSayPict='"9999"', cGetPict='"9999"'

  add object oOSDATINS_1_40 as StdField with uid="NRMNWNCNZZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_OSDATINS", cQueryName = "OSDATINS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inserimento operazione",;
    HelpContextID = 201624775,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=233, Top=364

  add object oOSUTEVAR_1_41 as StdField with uid="BQNRKEGOCN",rtseq=25,rtrep=.f.,;
    cFormVar = "w_OSUTEVAR", cQueryName = "OSUTEVAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha variato l'operazione",;
    HelpContextID = 2065208,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=471, Top=364, cSayPict='"9999"', cGetPict='"9999"'

  add object oOSDATVAR_1_42 as StdField with uid="GHQLZPVBDL",rtseq=26,rtrep=.f.,;
    cFormVar = "w_OSDATVAR", cQueryName = "OSDATVAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data variazione operazione",;
    HelpContextID = 16479032,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=553, Top=364


  add object oBtn_1_45 as StdButton with uid="HEBHBZFGEA",left=583, top=68, width=48,height=45,;
    CpPicture="BMP\PREDEFINITO.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per ripristinare i valori di default";
    , HelpContextID = 153986102;
    , caption='\<Default',TabStop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      with this.Parent.oContained
        .NotifyEvent("Default")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
      endwith
    endif
  endfunc


  add object oBtn_1_51 as StdButton with uid="GXLWSCCHJL",left=660, top=17, width=20,height=23,;
    caption="...", nPag=1;
    , HelpContextID = 176624682;
  , bGlobalFont=.t.

    proc oBtn_1_51.Click()
      with this.Parent.oContained
        GSCG_BOS(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_51.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((.w_OSTIPOPE='N' Or .w_OSFLGEXT='F') and INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
     endwith
    endif
  endfunc


  add object oBtn_1_55 as StdButton with uid="ZYIZXAWHHK",left=470, top=220, width=20,height=23,;
    caption="...", nPag=1;
    , HelpContextID = 176624682;
  , bGlobalFont=.t.

    proc oBtn_1_55.Click()
      with this.Parent.oContained
        GSCG_BOS(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_55.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (INLIST(.oParentObject.cFunction, 'Edit', 'Load'))
      endwith
    endif
  endfunc

  func oBtn_1_55.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_OSTIPFAT<>'A' Or .w_OSFLGEXT='F' Or Not Empty(.w_RIFACC)  )
     endwith
    endif
  endfunc

  add object oNUMREG_A_1_56 as StdField with uid="PYWUYAZZTE",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NUMREG_A", cQueryName = "NUMREG_A",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 249756393,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=143, Top=222

  func oNUMREG_A_1_56.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFACC)  Or .w_OSTIPFAT<>'A')
    endwith
  endfunc

  add object oDATREG_A_1_60 as StdField with uid="TKNNNIXTVE",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DATREG_A", cQueryName = "DATREG_A",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 249733001,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=271, Top=222

  func oDATREG_A_1_60.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFACC)  Or .w_OSTIPFAT<>'A')
    endwith
  endfunc

  add object oCOMPET_A_1_61 as StdField with uid="FBVQTWDHKS",rtseq=33,rtrep=.f.,;
    cFormVar = "w_COMPET_A", cQueryName = "COMPET_A",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 31785369,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=424, Top=222, InputMask=replicate('X',4)

  func oCOMPET_A_1_61.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFACC)  Or .w_OSTIPFAT<>'A')
    endwith
  endfunc

  add object oANDESCRI_1_66 as StdField with uid="LPRNMNZRFH",rtseq=35,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 233794895,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=286, Top=91, InputMask=replicate('X',40)


  add object oBtn_1_74 as StdButton with uid="CNBADBTKAQ",left=634, top=68, width=48,height=45,;
    CpPicture="bmp\Scadenze.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare dettaglio dati estratti";
    , HelpContextID = 66201802;
    , caption='\<Dat. Estr.',TabStop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_74.Click()
      with this.Parent.oContained
        .NotifyEvent("Dati_estratti")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_74.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_Deserial))
      endwith
    endif
  endfunc

  func oBtn_1_74.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_Deserial))
     endwith
    endif
  endfunc

  add object oStr_1_9 as StdString with uid="QSJGPSVWHQ",Visible=.t., Left=23, Top=19,;
    Alignment=1, Width=115, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="IADJXZHMAT",Visible=.t., Left=294, Top=19,;
    Alignment=1, Width=128, Height=18,;
    Caption="Riferimento contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="RRMOGBQALP",Visible=.t., Left=61, Top=137,;
    Alignment=1, Width=77, Height=18,;
    Caption="Tipo fattura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="JSTCDZCTXV",Visible=.t., Left=7, Top=194,;
    Alignment=0, Width=224, Height=18,;
    Caption="Riferimento documenti da rettificare"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFFAT) Or .w_OSTIPFAT<>'N')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="IUINKHGSME",Visible=.t., Left=5, Top=367,;
    Alignment=1, Width=143, Height=18,;
    Caption="Operazione inserita da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="VUECMDZEAY",Visible=.t., Left=202, Top=367,;
    Alignment=1, Width=25, Height=18,;
    Caption="Il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="HEKCZVEJSG",Visible=.t., Left=96, Top=223,;
    Alignment=0, Width=42, Height=18,;
    Caption="Reg. n.:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFFAT)  Or .w_OSTIPFAT<>'N')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="SSLXWJNINW",Visible=.t., Left=223, Top=224,;
    Alignment=1, Width=42, Height=18,;
    Caption="Del.:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFFAT)  Or .w_OSTIPFAT<>'N')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="OEQLEDAHYI",Visible=.t., Left=352, Top=224,;
    Alignment=1, Width=67, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFFAT)  Or .w_OSTIPFAT<>'N')
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="XPSOGFFEIM",Visible=.t., Left=34, Top=266,;
    Alignment=1, Width=104, Height=18,;
    Caption="Tipo estrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="WWZTWGXRBV",Visible=.t., Left=323, Top=367,;
    Alignment=1, Width=143, Height=18,;
    Caption="Operazione variata da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="OZNIURLTHW",Visible=.t., Left=524, Top=368,;
    Alignment=1, Width=25, Height=18,;
    Caption="Il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="XUJMBGHDSC",Visible=.t., Left=-1, Top=608,;
    Alignment=0, Width=440, Height=19,;
    Caption="Attenzione: ridefinita classe  tgscg_aos nell'A.M. function_procedure  "  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="HRABIUKYNV",Visible=.t., Left=7, Top=336,;
    Alignment=0, Width=151, Height=18,;
    Caption="Informazioni utente"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="KKUXAXYOYN",Visible=.t., Left=307, Top=194,;
    Alignment=0, Width=228, Height=19,;
    Caption="Operazione gi� rettificata"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFFAT))
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="HOTZFIONUL",Visible=.t., Left=7, Top=194,;
    Alignment=0, Width=224, Height=18,;
    Caption="Riferimento documenti di saldo"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFACC) Or .w_OSTIPFAT<>'A')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="TBPBLLAVTA",Visible=.t., Left=96, Top=223,;
    Alignment=0, Width=42, Height=18,;
    Caption="Reg. n.:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFACC)  Or .w_OSTIPFAT<>'A')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="KNXZWYQOJC",Visible=.t., Left=223, Top=224,;
    Alignment=1, Width=42, Height=18,;
    Caption="Del.:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFACC)  Or .w_OSTIPFAT<>'A')
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="OWLHZMSIHM",Visible=.t., Left=352, Top=224,;
    Alignment=1, Width=67, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_RIFACC)  Or .w_OSTIPFAT<>'A')
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="ZPSGXFYWWI",Visible=.t., Left=307, Top=172,;
    Alignment=0, Width=228, Height=19,;
    Caption="Operazione con acconti associati"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFACC))
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="KUOUGZFQMM",Visible=.t., Left=7, Top=65,;
    Alignment=0, Width=105, Height=18,;
    Caption="Intestatario effettivo"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="HSEXNKPRHD",Visible=.t., Left=293, Top=139,;
    Alignment=1, Width=137, Height=18,;
    Caption="Anno di riferimento:"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (.w_OSTIPFAT<>'N')
    endwith
  endfunc

  add object oBox_1_33 as StdBox with uid="MACGYOPNSI",left=1, top=215, width=614,height=2

  add object oBox_1_44 as StdBox with uid="OVSIVOWUYA",left=0, top=351, width=685,height=2

  add object oBox_1_65 as StdBox with uid="OAOUCCZNNA",left=-2, top=81, width=578,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aos','OPERSUPE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".OSSERIAL=OPERSUPE.OSSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_aos
 define class tgscg_aos as StdPCForm
  Width  = 693
  Height = 390
  Top    = 10
  Left   = 10
  cComment = "Operazioni superiori a 3000 euro"
  HelpContextID=176825705
  add object cnt as tcgscg_aos

  
  
  PROCEDURE EcpQuit()
    DoDefault()
    if not this.cnt.bUpdated and (this.cFunction="Load" or (this.cFunction="Edit" and empty(this.cnt.oParentObject.w_OSTIPORE)))
      ah_errormsg("Attenzione: non � stato salvato nessun dato nella gestione '%1'",,,this.cComment)
    endif
  ENDPROC
enddefine
* --- Fine Area Manuale
