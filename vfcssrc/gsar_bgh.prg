* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bgh                                                        *
*              Gestione path                                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_76]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-24                                                      *
* Last revis.: 2012-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bgh",oParentObject,m.pParam)
return(i_retval)

define class tgsar_bgh as StdBatch
  * --- Local variables
  pParam = space(4)
  w_CPATH = space(254)
  * --- WorkFile variables
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegnazione e salvataggio DATI PATH
    do case
      case this.pParam="INIT"
        this.oParentObject.w_COCODAZI = i_CODAZI
        * --- Try
        local bErr_03892E40
        bErr_03892E40=bTrsErr
        this.Try_03892E40()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03892E40
        * --- End
      case this.pParam="OK"
        this.w_CPATH = ""
        if !this.oParentObject.w_HIDE_CP AND !empty(this.oParentObject.w_PATHXML) and !directory( alltrim(this.oParentObject.w_PATHXML) )
          this.w_CPATH = "file xml"
        endif
        if !empty(this.oParentObject.w_PATHDOC) and !directory( alltrim(this.oParentObject.w_PATHDOC) ) and (g_IZCP="S" OR g_CPIN="S")
          this.w_CPATH = iif(empty(this.w_CPATH),"",+alltrim(this.w_CPATH)+", ")+"generazione documenti"
        endif
        if !empty(this.w_CPATH)
          ah_errormsg("La cartella impostata per il path %1 non esiste",,,ah_msgformat(alltrim(this.w_CPATH)))
          i_retcode = 'stop'
          return
        endif
        * --- Write into CONTROPA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTROPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTROPA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COPATHSJ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PATHSJ),'CONTROPA','COPATHSJ');
          +",COPATHCP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PATHXML),'CONTROPA','COPATHCP');
          +",COPATHDO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PATHDOC),'CONTROPA','COPATHDO');
          +",COCHKAZI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ADDAZI),'CONTROPA','COCHKAZI');
              +i_ccchkf ;
          +" where ";
              +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 )
        else
          update (i_cTable) set;
              COPATHSJ = this.oParentObject.w_PATHSJ;
              ,COPATHCP = this.oParentObject.w_PATHXML;
              ,COPATHDO = this.oParentObject.w_PATHDOC;
              ,COCHKAZI = this.oParentObject.w_ADDAZI;
              &i_ccchkf. ;
           where;
              COCODAZI = i_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.oParentObject.ecpQuit
    endcase
  endproc
  proc Try_03892E40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONTROPA
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"COCODAZI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(i_CODAZI),'CONTROPA','COCODAZI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'COCODAZI',i_CODAZI)
      insert into (i_cTable) (COCODAZI &i_ccchkf. );
         values (;
           i_CODAZI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTROPA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
