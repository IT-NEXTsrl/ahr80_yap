* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ada                                                        *
*              Dati aggiuntivi docum./p.nota                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-02-08                                                      *
* Last revis.: 2012-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_ada")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_ada")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_ada")
  return

* --- Class definition
define class tgsar_ada as StdPCForm
  Width  = 567
  Height = 481
  Top    = 6
  Left   = 10
  cComment = "Dati aggiuntivi docum./p.nota"
  cPrg = "gsar_ada"
  HelpContextID=175540375
  add object cnt as tcgsar_ada
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_ada as PCContext
  w_DASERIAL = space(10)
  w_DACAM_01 = space(30)
  w_DAORD_01 = space(1)
  w_DAPRI_01 = 0
  w_DACAM_02 = space(30)
  w_DAORD_02 = space(1)
  w_DAPRI_02 = 0
  w_DACAM_03 = space(30)
  w_DAORD_03 = space(1)
  w_DAPRI_03 = 0
  w_DACAM_04 = space(30)
  w_DAORD_04 = space(1)
  w_DAPRI_04 = 0
  w_DACAM_05 = space(30)
  w_DAORD_05 = space(1)
  w_DAPRI_05 = 0
  w_DACAM_06 = space(30)
  w_DAORD_06 = space(1)
  w_DAPRI_06 = 0
  proc Save(oFrom)
    this.w_DASERIAL = oFrom.w_DASERIAL
    this.w_DACAM_01 = oFrom.w_DACAM_01
    this.w_DAORD_01 = oFrom.w_DAORD_01
    this.w_DAPRI_01 = oFrom.w_DAPRI_01
    this.w_DACAM_02 = oFrom.w_DACAM_02
    this.w_DAORD_02 = oFrom.w_DAORD_02
    this.w_DAPRI_02 = oFrom.w_DAPRI_02
    this.w_DACAM_03 = oFrom.w_DACAM_03
    this.w_DAORD_03 = oFrom.w_DAORD_03
    this.w_DAPRI_03 = oFrom.w_DAPRI_03
    this.w_DACAM_04 = oFrom.w_DACAM_04
    this.w_DAORD_04 = oFrom.w_DAORD_04
    this.w_DAPRI_04 = oFrom.w_DAPRI_04
    this.w_DACAM_05 = oFrom.w_DACAM_05
    this.w_DAORD_05 = oFrom.w_DAORD_05
    this.w_DAPRI_05 = oFrom.w_DAPRI_05
    this.w_DACAM_06 = oFrom.w_DACAM_06
    this.w_DAORD_06 = oFrom.w_DAORD_06
    this.w_DAPRI_06 = oFrom.w_DAPRI_06
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_DASERIAL = this.w_DASERIAL
    oTo.w_DACAM_01 = this.w_DACAM_01
    oTo.w_DAORD_01 = this.w_DAORD_01
    oTo.w_DAPRI_01 = this.w_DAPRI_01
    oTo.w_DACAM_02 = this.w_DACAM_02
    oTo.w_DAORD_02 = this.w_DAORD_02
    oTo.w_DAPRI_02 = this.w_DAPRI_02
    oTo.w_DACAM_03 = this.w_DACAM_03
    oTo.w_DAORD_03 = this.w_DAORD_03
    oTo.w_DAPRI_03 = this.w_DAPRI_03
    oTo.w_DACAM_04 = this.w_DACAM_04
    oTo.w_DAORD_04 = this.w_DAORD_04
    oTo.w_DAPRI_04 = this.w_DAPRI_04
    oTo.w_DACAM_05 = this.w_DACAM_05
    oTo.w_DAORD_05 = this.w_DAORD_05
    oTo.w_DAPRI_05 = this.w_DAPRI_05
    oTo.w_DACAM_06 = this.w_DACAM_06
    oTo.w_DAORD_06 = this.w_DAORD_06
    oTo.w_DAPRI_06 = this.w_DAPRI_06
    PCContext::Load(oTo)
enddefine

define class tcgsar_ada as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 567
  Height = 481
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-16"
  HelpContextID=175540375
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  DATI_AGG_IDX = 0
  cFile = "DATI_AGG"
  cKeySelect = "DASERIAL"
  cKeyWhere  = "DASERIAL=this.w_DASERIAL"
  cKeyWhereODBC = '"DASERIAL="+cp_ToStrODBC(this.w_DASERIAL)';

  cKeyWhereODBCqualified = '"DATI_AGG.DASERIAL="+cp_ToStrODBC(this.w_DASERIAL)';

  cPrg = "gsar_ada"
  cComment = "Dati aggiuntivi docum./p.nota"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DASERIAL = space(10)
  w_DACAM_01 = space(30)
  w_DAORD_01 = space(1)
  w_DAPRI_01 = 0
  w_DACAM_02 = space(30)
  w_DAORD_02 = space(1)
  w_DAPRI_02 = 0
  w_DACAM_03 = space(30)
  w_DAORD_03 = space(1)
  w_DAPRI_03 = 0
  w_DACAM_04 = space(30)
  w_DAORD_04 = space(1)
  w_DAPRI_04 = 0
  w_DACAM_05 = space(30)
  w_DAORD_05 = space(1)
  w_DAPRI_05 = 0
  w_DACAM_06 = space(30)
  w_DAORD_06 = space(1)
  w_DAPRI_06 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_adaPag1","gsar_ada",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati aggiuntivi documenti/primanota")
      .Pages(1).HelpContextID = 189657327
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDACAM_01_1_26
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DATI_AGG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATI_AGG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATI_AGG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_ada'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DATI_AGG where DASERIAL=KeySet.DASERIAL
    *
    i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATI_AGG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATI_AGG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATI_AGG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DASERIAL',this.w_DASERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DASERIAL = NVL(DASERIAL,space(10))
        .w_DACAM_01 = NVL(DACAM_01,space(30))
        .w_DAORD_01 = NVL(DAORD_01,space(1))
        .w_DAPRI_01 = NVL(DAPRI_01,0)
        .w_DACAM_02 = NVL(DACAM_02,space(30))
        .w_DAORD_02 = NVL(DAORD_02,space(1))
        .w_DAPRI_02 = NVL(DAPRI_02,0)
        .w_DACAM_03 = NVL(DACAM_03,space(30))
        .w_DAORD_03 = NVL(DAORD_03,space(1))
        .w_DAPRI_03 = NVL(DAPRI_03,0)
        .w_DACAM_04 = NVL(DACAM_04,space(30))
        .w_DAORD_04 = NVL(DAORD_04,space(1))
        .w_DAPRI_04 = NVL(DAPRI_04,0)
        .w_DACAM_05 = NVL(DACAM_05,space(30))
        .w_DAORD_05 = NVL(DAORD_05,space(1))
        .w_DAPRI_05 = NVL(DAPRI_05,0)
        .w_DACAM_06 = NVL(DACAM_06,space(30))
        .w_DAORD_06 = NVL(DAORD_06,space(1))
        .w_DAPRI_06 = NVL(DAPRI_06,0)
        cp_LoadRecExtFlds(this,'DATI_AGG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_DASERIAL = space(10)
      .w_DACAM_01 = space(30)
      .w_DAORD_01 = space(1)
      .w_DAPRI_01 = 0
      .w_DACAM_02 = space(30)
      .w_DAORD_02 = space(1)
      .w_DAPRI_02 = 0
      .w_DACAM_03 = space(30)
      .w_DAORD_03 = space(1)
      .w_DAPRI_03 = 0
      .w_DACAM_04 = space(30)
      .w_DAORD_04 = space(1)
      .w_DAPRI_04 = 0
      .w_DACAM_05 = space(30)
      .w_DAORD_05 = space(1)
      .w_DAPRI_05 = 0
      .w_DACAM_06 = space(30)
      .w_DAORD_06 = space(1)
      .w_DAPRI_06 = 0
      if .cFunction<>"Filter"
        .w_DASERIAL = '0000000001'
          .DoRTCalc(2,2,.f.)
        .w_DAORD_01 = 'I'
        .w_DAPRI_01 = 1
          .DoRTCalc(5,5,.f.)
        .w_DAORD_02 = 'I'
        .w_DAPRI_02 = 2
          .DoRTCalc(8,8,.f.)
        .w_DAORD_03 = 'I'
        .w_DAPRI_03 = 3
          .DoRTCalc(11,11,.f.)
        .w_DAORD_04 = 'I'
        .w_DAPRI_04 = 4
          .DoRTCalc(14,14,.f.)
        .w_DAORD_05 = 'I'
        .w_DAPRI_05 = 5
          .DoRTCalc(17,17,.f.)
        .w_DAORD_06 = 'I'
        .w_DAPRI_06 = 6
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATI_AGG')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDACAM_01_1_26.enabled = i_bVal
      .Page1.oPag.oDAORD_01_1_27.enabled = i_bVal
      .Page1.oPag.oDAPRI_01_1_28.enabled = i_bVal
      .Page1.oPag.oDACAM_02_1_29.enabled = i_bVal
      .Page1.oPag.oDAORD_02_1_30.enabled = i_bVal
      .Page1.oPag.oDAPRI_02_1_31.enabled = i_bVal
      .Page1.oPag.oDACAM_03_1_32.enabled = i_bVal
      .Page1.oPag.oDAORD_03_1_33.enabled = i_bVal
      .Page1.oPag.oDAPRI_03_1_34.enabled = i_bVal
      .Page1.oPag.oDACAM_04_1_35.enabled = i_bVal
      .Page1.oPag.oDAORD_04_1_36.enabled = i_bVal
      .Page1.oPag.oDAPRI_04_1_37.enabled = i_bVal
      .Page1.oPag.oDACAM_05_1_38.enabled = i_bVal
      .Page1.oPag.oDAORD_05_1_39.enabled = i_bVal
      .Page1.oPag.oDAPRI_05_1_40.enabled = i_bVal
      .Page1.oPag.oDACAM_06_1_41.enabled = i_bVal
      .Page1.oPag.oDAORD_06_1_42.enabled = i_bVal
      .Page1.oPag.oDAPRI_06_1_43.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DATI_AGG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DASERIAL,"DASERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DACAM_01,"DACAM_01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAORD_01,"DAORD_01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAPRI_01,"DAPRI_01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DACAM_02,"DACAM_02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAORD_02,"DAORD_02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAPRI_02,"DAPRI_02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DACAM_03,"DACAM_03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAORD_03,"DAORD_03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAPRI_03,"DAPRI_03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DACAM_04,"DACAM_04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAORD_04,"DAORD_04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAPRI_04,"DAPRI_04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DACAM_05,"DACAM_05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAORD_05,"DAORD_05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAPRI_05,"DAPRI_05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DACAM_06,"DACAM_06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAORD_06,"DAORD_06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAPRI_06,"DAPRI_06",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DATI_AGG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DATI_AGG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATI_AGG')
        i_extval=cp_InsertValODBCExtFlds(this,'DATI_AGG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DASERIAL,DACAM_01,DAORD_01,DAPRI_01,DACAM_02"+;
                  ",DAORD_02,DAPRI_02,DACAM_03,DAORD_03,DAPRI_03"+;
                  ",DACAM_04,DAORD_04,DAPRI_04,DACAM_05,DAORD_05"+;
                  ",DAPRI_05,DACAM_06,DAORD_06,DAPRI_06 "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DASERIAL)+;
                  ","+cp_ToStrODBC(this.w_DACAM_01)+;
                  ","+cp_ToStrODBC(this.w_DAORD_01)+;
                  ","+cp_ToStrODBC(this.w_DAPRI_01)+;
                  ","+cp_ToStrODBC(this.w_DACAM_02)+;
                  ","+cp_ToStrODBC(this.w_DAORD_02)+;
                  ","+cp_ToStrODBC(this.w_DAPRI_02)+;
                  ","+cp_ToStrODBC(this.w_DACAM_03)+;
                  ","+cp_ToStrODBC(this.w_DAORD_03)+;
                  ","+cp_ToStrODBC(this.w_DAPRI_03)+;
                  ","+cp_ToStrODBC(this.w_DACAM_04)+;
                  ","+cp_ToStrODBC(this.w_DAORD_04)+;
                  ","+cp_ToStrODBC(this.w_DAPRI_04)+;
                  ","+cp_ToStrODBC(this.w_DACAM_05)+;
                  ","+cp_ToStrODBC(this.w_DAORD_05)+;
                  ","+cp_ToStrODBC(this.w_DAPRI_05)+;
                  ","+cp_ToStrODBC(this.w_DACAM_06)+;
                  ","+cp_ToStrODBC(this.w_DAORD_06)+;
                  ","+cp_ToStrODBC(this.w_DAPRI_06)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATI_AGG')
        i_extval=cp_InsertValVFPExtFlds(this,'DATI_AGG')
        cp_CheckDeletedKey(i_cTable,0,'DASERIAL',this.w_DASERIAL)
        INSERT INTO (i_cTable);
              (DASERIAL,DACAM_01,DAORD_01,DAPRI_01,DACAM_02,DAORD_02,DAPRI_02,DACAM_03,DAORD_03,DAPRI_03,DACAM_04,DAORD_04,DAPRI_04,DACAM_05,DAORD_05,DAPRI_05,DACAM_06,DAORD_06,DAPRI_06  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DASERIAL;
                  ,this.w_DACAM_01;
                  ,this.w_DAORD_01;
                  ,this.w_DAPRI_01;
                  ,this.w_DACAM_02;
                  ,this.w_DAORD_02;
                  ,this.w_DAPRI_02;
                  ,this.w_DACAM_03;
                  ,this.w_DAORD_03;
                  ,this.w_DAPRI_03;
                  ,this.w_DACAM_04;
                  ,this.w_DAORD_04;
                  ,this.w_DAPRI_04;
                  ,this.w_DACAM_05;
                  ,this.w_DAORD_05;
                  ,this.w_DAPRI_05;
                  ,this.w_DACAM_06;
                  ,this.w_DAORD_06;
                  ,this.w_DAPRI_06;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DATI_AGG_IDX,i_nConn)
      *
      * update DATI_AGG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DATI_AGG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DACAM_01="+cp_ToStrODBC(this.w_DACAM_01)+;
             ",DAORD_01="+cp_ToStrODBC(this.w_DAORD_01)+;
             ",DAPRI_01="+cp_ToStrODBC(this.w_DAPRI_01)+;
             ",DACAM_02="+cp_ToStrODBC(this.w_DACAM_02)+;
             ",DAORD_02="+cp_ToStrODBC(this.w_DAORD_02)+;
             ",DAPRI_02="+cp_ToStrODBC(this.w_DAPRI_02)+;
             ",DACAM_03="+cp_ToStrODBC(this.w_DACAM_03)+;
             ",DAORD_03="+cp_ToStrODBC(this.w_DAORD_03)+;
             ",DAPRI_03="+cp_ToStrODBC(this.w_DAPRI_03)+;
             ",DACAM_04="+cp_ToStrODBC(this.w_DACAM_04)+;
             ",DAORD_04="+cp_ToStrODBC(this.w_DAORD_04)+;
             ",DAPRI_04="+cp_ToStrODBC(this.w_DAPRI_04)+;
             ",DACAM_05="+cp_ToStrODBC(this.w_DACAM_05)+;
             ",DAORD_05="+cp_ToStrODBC(this.w_DAORD_05)+;
             ",DAPRI_05="+cp_ToStrODBC(this.w_DAPRI_05)+;
             ",DACAM_06="+cp_ToStrODBC(this.w_DACAM_06)+;
             ",DAORD_06="+cp_ToStrODBC(this.w_DAORD_06)+;
             ",DAPRI_06="+cp_ToStrODBC(this.w_DAPRI_06)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DATI_AGG')
        i_cWhere = cp_PKFox(i_cTable  ,'DASERIAL',this.w_DASERIAL  )
        UPDATE (i_cTable) SET;
              DACAM_01=this.w_DACAM_01;
             ,DAORD_01=this.w_DAORD_01;
             ,DAPRI_01=this.w_DAPRI_01;
             ,DACAM_02=this.w_DACAM_02;
             ,DAORD_02=this.w_DAORD_02;
             ,DAPRI_02=this.w_DAPRI_02;
             ,DACAM_03=this.w_DACAM_03;
             ,DAORD_03=this.w_DAORD_03;
             ,DAPRI_03=this.w_DAPRI_03;
             ,DACAM_04=this.w_DACAM_04;
             ,DAORD_04=this.w_DAORD_04;
             ,DAPRI_04=this.w_DAPRI_04;
             ,DACAM_05=this.w_DACAM_05;
             ,DAORD_05=this.w_DAORD_05;
             ,DAPRI_05=this.w_DAPRI_05;
             ,DACAM_06=this.w_DACAM_06;
             ,DAORD_06=this.w_DAORD_06;
             ,DAPRI_06=this.w_DAPRI_06;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DATI_AGG_IDX,i_nConn)
      *
      * delete DATI_AGG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DASERIAL',this.w_DASERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDACAM_01_1_26.value==this.w_DACAM_01)
      this.oPgFrm.Page1.oPag.oDACAM_01_1_26.value=this.w_DACAM_01
    endif
    if not(this.oPgFrm.Page1.oPag.oDAORD_01_1_27.RadioValue()==this.w_DAORD_01)
      this.oPgFrm.Page1.oPag.oDAORD_01_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPRI_01_1_28.RadioValue()==this.w_DAPRI_01)
      this.oPgFrm.Page1.oPag.oDAPRI_01_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDACAM_02_1_29.value==this.w_DACAM_02)
      this.oPgFrm.Page1.oPag.oDACAM_02_1_29.value=this.w_DACAM_02
    endif
    if not(this.oPgFrm.Page1.oPag.oDAORD_02_1_30.RadioValue()==this.w_DAORD_02)
      this.oPgFrm.Page1.oPag.oDAORD_02_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPRI_02_1_31.RadioValue()==this.w_DAPRI_02)
      this.oPgFrm.Page1.oPag.oDAPRI_02_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDACAM_03_1_32.value==this.w_DACAM_03)
      this.oPgFrm.Page1.oPag.oDACAM_03_1_32.value=this.w_DACAM_03
    endif
    if not(this.oPgFrm.Page1.oPag.oDAORD_03_1_33.RadioValue()==this.w_DAORD_03)
      this.oPgFrm.Page1.oPag.oDAORD_03_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPRI_03_1_34.RadioValue()==this.w_DAPRI_03)
      this.oPgFrm.Page1.oPag.oDAPRI_03_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDACAM_04_1_35.value==this.w_DACAM_04)
      this.oPgFrm.Page1.oPag.oDACAM_04_1_35.value=this.w_DACAM_04
    endif
    if not(this.oPgFrm.Page1.oPag.oDAORD_04_1_36.RadioValue()==this.w_DAORD_04)
      this.oPgFrm.Page1.oPag.oDAORD_04_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPRI_04_1_37.RadioValue()==this.w_DAPRI_04)
      this.oPgFrm.Page1.oPag.oDAPRI_04_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDACAM_05_1_38.value==this.w_DACAM_05)
      this.oPgFrm.Page1.oPag.oDACAM_05_1_38.value=this.w_DACAM_05
    endif
    if not(this.oPgFrm.Page1.oPag.oDAORD_05_1_39.RadioValue()==this.w_DAORD_05)
      this.oPgFrm.Page1.oPag.oDAORD_05_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPRI_05_1_40.RadioValue()==this.w_DAPRI_05)
      this.oPgFrm.Page1.oPag.oDAPRI_05_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDACAM_06_1_41.value==this.w_DACAM_06)
      this.oPgFrm.Page1.oPag.oDACAM_06_1_41.value=this.w_DACAM_06
    endif
    if not(this.oPgFrm.Page1.oPag.oDAORD_06_1_42.RadioValue()==this.w_DAORD_06)
      this.oPgFrm.Page1.oPag.oDAORD_06_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPRI_06_1_43.RadioValue()==this.w_DAPRI_06)
      this.oPgFrm.Page1.oPag.oDAPRI_06_1_43.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'DATI_AGG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsar_ada
      If i_bRes
        local L_sToCheck, L_Idx, L_Macro
        L_sToCheck = ""
        For L_Idx=1 to 6
          L_Macro = "this.w_DAPRI_"+RIGHT("00"+ALLTRIM(STR(L_Idx)),2)
          L_sToCheck = L_sToCheck + ALLTRIM(STR( &L_Macro ))
        EndFor
        For L_Idx=1 to 6
          i_bRes = i_bRes And OCCURS(ALLTRIM(STR(L_Idx)), L_sToCheck) = 1
        EndFor
        If !i_bRes
          ah_ErrorMsg("Verificare priorit� campi. Priorit� duplicate o assenti")
        endif
        release L_sToCheck, L_Idx, L_Macro
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_adaPag1 as StdContainer
  Width  = 563
  height = 481
  stdWidth  = 563
  stdheight = 481
  resizeXpos=329
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDACAM_01_1_26 as StdField with uid="PZCWESSIER",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DACAM_01", cQueryName = "DACAM_01",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione campo che accoglie dati aggiuntivi",;
    HelpContextID = 24379801,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=153, Top=30, InputMask=replicate('X',30)


  add object oDAORD_01_1_27 as StdCombo with uid="NWWELMWVBJ",rtseq=3,rtrep=.f.,left=153,top=56,width=223,height=22;
    , ToolTipText = "Comportamento del campo durante gli ordinamenti. Il campo pu� essere ignorato, precedere o seguire gli ordinamenti standard";
    , HelpContextID = 32653721;
    , cFormVar="w_DAORD_01",RowSource=""+"Ignora,"+"Precede ascendente,"+"Precede discendente,"+"Segue ascendente,"+"Segue discendente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAORD_01_1_27.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'D',;
    iif(this.value =4,'S',;
    iif(this.value =5,'F',;
    space(1)))))))
  endfunc
  func oDAORD_01_1_27.GetRadio()
    this.Parent.oContained.w_DAORD_01 = this.RadioValue()
    return .t.
  endfunc

  func oDAORD_01_1_27.SetRadio()
    this.Parent.oContained.w_DAORD_01=trim(this.Parent.oContained.w_DAORD_01)
    this.value = ;
      iif(this.Parent.oContained.w_DAORD_01=='I',1,;
      iif(this.Parent.oContained.w_DAORD_01=='P',2,;
      iif(this.Parent.oContained.w_DAORD_01=='D',3,;
      iif(this.Parent.oContained.w_DAORD_01=='S',4,;
      iif(this.Parent.oContained.w_DAORD_01=='F',5,;
      0)))))
  endfunc


  add object oDAPRI_01_1_28 as StdCombo with uid="RMZOIIAZHN",rtseq=4,rtrep=.f.,left=465,top=56,width=73,height=22;
    , ToolTipText = "Imposta la priorit� del campo durante gli ordinamenti (1=massima, 6=minima)";
    , HelpContextID = 27406745;
    , cFormVar="w_DAPRI_01",RowSource=""+"1,"+"2,"+"3,"+"4,"+"5,"+"6", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAPRI_01_1_28.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    0)))))))
  endfunc
  func oDAPRI_01_1_28.GetRadio()
    this.Parent.oContained.w_DAPRI_01 = this.RadioValue()
    return .t.
  endfunc

  func oDAPRI_01_1_28.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DAPRI_01==1,1,;
      iif(this.Parent.oContained.w_DAPRI_01==2,2,;
      iif(this.Parent.oContained.w_DAPRI_01==3,3,;
      iif(this.Parent.oContained.w_DAPRI_01==4,4,;
      iif(this.Parent.oContained.w_DAPRI_01==5,5,;
      iif(this.Parent.oContained.w_DAPRI_01==6,6,;
      0))))))
  endfunc

  add object oDACAM_02_1_29 as StdField with uid="KJKGQTBXRB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DACAM_02", cQueryName = "DACAM_02",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione campo che accoglie dati aggiuntivi",;
    HelpContextID = 24379800,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=153, Top=104, InputMask=replicate('X',30)


  add object oDAORD_02_1_30 as StdCombo with uid="QDFMVPKGKG",rtseq=6,rtrep=.f.,left=153,top=131,width=223,height=22;
    , ToolTipText = "Comportamento del campo durante gli ordinamenti. Il campo pu� essere ignorato, precedere o seguire gli ordinamenti standard";
    , HelpContextID = 32653720;
    , cFormVar="w_DAORD_02",RowSource=""+"Ignora,"+"Precede ascendente,"+"Precede discendente,"+"Segue ascendente,"+"Segue discendente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAORD_02_1_30.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'D',;
    iif(this.value =4,'S',;
    iif(this.value =5,'F',;
    space(1)))))))
  endfunc
  func oDAORD_02_1_30.GetRadio()
    this.Parent.oContained.w_DAORD_02 = this.RadioValue()
    return .t.
  endfunc

  func oDAORD_02_1_30.SetRadio()
    this.Parent.oContained.w_DAORD_02=trim(this.Parent.oContained.w_DAORD_02)
    this.value = ;
      iif(this.Parent.oContained.w_DAORD_02=='I',1,;
      iif(this.Parent.oContained.w_DAORD_02=='P',2,;
      iif(this.Parent.oContained.w_DAORD_02=='D',3,;
      iif(this.Parent.oContained.w_DAORD_02=='S',4,;
      iif(this.Parent.oContained.w_DAORD_02=='F',5,;
      0)))))
  endfunc


  add object oDAPRI_02_1_31 as StdCombo with uid="FSXXYJWGAB",rtseq=7,rtrep=.f.,left=465,top=131,width=73,height=22;
    , ToolTipText = "Imposta la priorit� del campo durante gli ordinamenti (1=massima, 6=minima)";
    , HelpContextID = 27406744;
    , cFormVar="w_DAPRI_02",RowSource=""+"1,"+"2,"+"3,"+"4,"+"5,"+"6", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAPRI_02_1_31.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    0)))))))
  endfunc
  func oDAPRI_02_1_31.GetRadio()
    this.Parent.oContained.w_DAPRI_02 = this.RadioValue()
    return .t.
  endfunc

  func oDAPRI_02_1_31.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DAPRI_02==1,1,;
      iif(this.Parent.oContained.w_DAPRI_02==2,2,;
      iif(this.Parent.oContained.w_DAPRI_02==3,3,;
      iif(this.Parent.oContained.w_DAPRI_02==4,4,;
      iif(this.Parent.oContained.w_DAPRI_02==5,5,;
      iif(this.Parent.oContained.w_DAPRI_02==6,6,;
      0))))))
  endfunc

  add object oDACAM_03_1_32 as StdField with uid="XNJRIFMYSX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DACAM_03", cQueryName = "DACAM_03",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione campo che accoglie dati aggiuntivi",;
    HelpContextID = 24379799,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=153, Top=187, InputMask=replicate('X',30)


  add object oDAORD_03_1_33 as StdCombo with uid="OIREZNWWQV",rtseq=9,rtrep=.f.,left=153,top=220,width=223,height=22;
    , ToolTipText = "Comportamento del campo durante gli ordinamenti. Il campo pu� essere ignorato, precedere o seguire gli ordinamenti standard";
    , HelpContextID = 32653719;
    , cFormVar="w_DAORD_03",RowSource=""+"Ignora,"+"Precede ascendente,"+"Precede discendente,"+"Segue ascendente,"+"Segue discendente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAORD_03_1_33.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'D',;
    iif(this.value =4,'S',;
    iif(this.value =5,'F',;
    space(1)))))))
  endfunc
  func oDAORD_03_1_33.GetRadio()
    this.Parent.oContained.w_DAORD_03 = this.RadioValue()
    return .t.
  endfunc

  func oDAORD_03_1_33.SetRadio()
    this.Parent.oContained.w_DAORD_03=trim(this.Parent.oContained.w_DAORD_03)
    this.value = ;
      iif(this.Parent.oContained.w_DAORD_03=='I',1,;
      iif(this.Parent.oContained.w_DAORD_03=='P',2,;
      iif(this.Parent.oContained.w_DAORD_03=='D',3,;
      iif(this.Parent.oContained.w_DAORD_03=='S',4,;
      iif(this.Parent.oContained.w_DAORD_03=='F',5,;
      0)))))
  endfunc


  add object oDAPRI_03_1_34 as StdCombo with uid="ASWFQHJGSJ",rtseq=10,rtrep=.f.,left=465,top=220,width=73,height=22;
    , ToolTipText = "Imposta la priorit� del campo durante gli ordinamenti (1=massima, 6=minima)";
    , HelpContextID = 27406743;
    , cFormVar="w_DAPRI_03",RowSource=""+"1,"+"2,"+"3,"+"4,"+"5,"+"6", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAPRI_03_1_34.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    0)))))))
  endfunc
  func oDAPRI_03_1_34.GetRadio()
    this.Parent.oContained.w_DAPRI_03 = this.RadioValue()
    return .t.
  endfunc

  func oDAPRI_03_1_34.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DAPRI_03==1,1,;
      iif(this.Parent.oContained.w_DAPRI_03==2,2,;
      iif(this.Parent.oContained.w_DAPRI_03==3,3,;
      iif(this.Parent.oContained.w_DAPRI_03==4,4,;
      iif(this.Parent.oContained.w_DAPRI_03==5,5,;
      iif(this.Parent.oContained.w_DAPRI_03==6,6,;
      0))))))
  endfunc

  add object oDACAM_04_1_35 as StdField with uid="KRCKVHAHZS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DACAM_04", cQueryName = "DACAM_04",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione campo che accoglie dati aggiuntivi",;
    HelpContextID = 24379798,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=153, Top=269, InputMask=replicate('X',30)


  add object oDAORD_04_1_36 as StdCombo with uid="XNEOPDBPRY",rtseq=12,rtrep=.f.,left=153,top=294,width=223,height=22;
    , ToolTipText = "Comportamento del campo durante gli ordinamenti. Il campo pu� essere ignorato, precedere o seguire gli ordinamenti standard";
    , HelpContextID = 32653718;
    , cFormVar="w_DAORD_04",RowSource=""+"Ignora,"+"Precede ascendente,"+"Precede discendente,"+"Segue ascendente,"+"Segue discendente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAORD_04_1_36.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'D',;
    iif(this.value =4,'S',;
    iif(this.value =5,'F',;
    space(1)))))))
  endfunc
  func oDAORD_04_1_36.GetRadio()
    this.Parent.oContained.w_DAORD_04 = this.RadioValue()
    return .t.
  endfunc

  func oDAORD_04_1_36.SetRadio()
    this.Parent.oContained.w_DAORD_04=trim(this.Parent.oContained.w_DAORD_04)
    this.value = ;
      iif(this.Parent.oContained.w_DAORD_04=='I',1,;
      iif(this.Parent.oContained.w_DAORD_04=='P',2,;
      iif(this.Parent.oContained.w_DAORD_04=='D',3,;
      iif(this.Parent.oContained.w_DAORD_04=='S',4,;
      iif(this.Parent.oContained.w_DAORD_04=='F',5,;
      0)))))
  endfunc


  add object oDAPRI_04_1_37 as StdCombo with uid="LJCAGEFQLV",rtseq=13,rtrep=.f.,left=465,top=294,width=73,height=22;
    , ToolTipText = "Imposta la priorit� del campo durante gli ordinamenti (1=massima, 6=minima)";
    , HelpContextID = 27406742;
    , cFormVar="w_DAPRI_04",RowSource=""+"1,"+"2,"+"3,"+"4,"+"5,"+"6", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAPRI_04_1_37.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    0)))))))
  endfunc
  func oDAPRI_04_1_37.GetRadio()
    this.Parent.oContained.w_DAPRI_04 = this.RadioValue()
    return .t.
  endfunc

  func oDAPRI_04_1_37.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DAPRI_04==1,1,;
      iif(this.Parent.oContained.w_DAPRI_04==2,2,;
      iif(this.Parent.oContained.w_DAPRI_04==3,3,;
      iif(this.Parent.oContained.w_DAPRI_04==4,4,;
      iif(this.Parent.oContained.w_DAPRI_04==5,5,;
      iif(this.Parent.oContained.w_DAPRI_04==6,6,;
      0))))))
  endfunc

  add object oDACAM_05_1_38 as StdField with uid="UBQJKITPFU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DACAM_05", cQueryName = "DACAM_05",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione campo che accoglie dati aggiuntivi di tipo data",;
    HelpContextID = 24379797,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=153, Top=346, InputMask=replicate('X',30)


  add object oDAORD_05_1_39 as StdCombo with uid="KVXHQSCIWA",rtseq=15,rtrep=.f.,left=153,top=371,width=223,height=22;
    , ToolTipText = "Comportamento del campo durante gli ordinamenti. Il campo pu� essere ignorato, precedere o seguire gli ordinamenti standard";
    , HelpContextID = 32653717;
    , cFormVar="w_DAORD_05",RowSource=""+"Ignora,"+"Precede ascendente,"+"Precede discendente,"+"Segue ascendente,"+"Segue discendente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAORD_05_1_39.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'D',;
    iif(this.value =4,'S',;
    iif(this.value =5,'F',;
    space(1)))))))
  endfunc
  func oDAORD_05_1_39.GetRadio()
    this.Parent.oContained.w_DAORD_05 = this.RadioValue()
    return .t.
  endfunc

  func oDAORD_05_1_39.SetRadio()
    this.Parent.oContained.w_DAORD_05=trim(this.Parent.oContained.w_DAORD_05)
    this.value = ;
      iif(this.Parent.oContained.w_DAORD_05=='I',1,;
      iif(this.Parent.oContained.w_DAORD_05=='P',2,;
      iif(this.Parent.oContained.w_DAORD_05=='D',3,;
      iif(this.Parent.oContained.w_DAORD_05=='S',4,;
      iif(this.Parent.oContained.w_DAORD_05=='F',5,;
      0)))))
  endfunc


  add object oDAPRI_05_1_40 as StdCombo with uid="HWCHJPJKQV",rtseq=16,rtrep=.f.,left=465,top=371,width=73,height=22;
    , ToolTipText = "Imposta la priorit� del campo durante gli ordinamenti (1=massima, 6=minima)";
    , HelpContextID = 27406741;
    , cFormVar="w_DAPRI_05",RowSource=""+"1,"+"2,"+"3,"+"4,"+"5,"+"6", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAPRI_05_1_40.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    0)))))))
  endfunc
  func oDAPRI_05_1_40.GetRadio()
    this.Parent.oContained.w_DAPRI_05 = this.RadioValue()
    return .t.
  endfunc

  func oDAPRI_05_1_40.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DAPRI_05==1,1,;
      iif(this.Parent.oContained.w_DAPRI_05==2,2,;
      iif(this.Parent.oContained.w_DAPRI_05==3,3,;
      iif(this.Parent.oContained.w_DAPRI_05==4,4,;
      iif(this.Parent.oContained.w_DAPRI_05==5,5,;
      iif(this.Parent.oContained.w_DAPRI_05==6,6,;
      0))))))
  endfunc

  add object oDACAM_06_1_41 as StdField with uid="YPWYOZBJVO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DACAM_06", cQueryName = "DACAM_06",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione campo che accoglie dati aggiuntivi di tipo data",;
    HelpContextID = 24379796,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=153, Top=424, InputMask=replicate('X',30)


  add object oDAORD_06_1_42 as StdCombo with uid="LTOSHNYHHS",rtseq=18,rtrep=.f.,left=153,top=449,width=223,height=22;
    , ToolTipText = "Comportamento del campo durante gli ordinamenti. Il campo pu� essere ignorato, precedere o seguire gli ordinamenti standard";
    , HelpContextID = 32653716;
    , cFormVar="w_DAORD_06",RowSource=""+"Ignora,"+"Precede ascendente,"+"Precede discendente,"+"Segue ascendente,"+"Segue discendente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAORD_06_1_42.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'D',;
    iif(this.value =4,'S',;
    iif(this.value =5,'F',;
    space(1)))))))
  endfunc
  func oDAORD_06_1_42.GetRadio()
    this.Parent.oContained.w_DAORD_06 = this.RadioValue()
    return .t.
  endfunc

  func oDAORD_06_1_42.SetRadio()
    this.Parent.oContained.w_DAORD_06=trim(this.Parent.oContained.w_DAORD_06)
    this.value = ;
      iif(this.Parent.oContained.w_DAORD_06=='I',1,;
      iif(this.Parent.oContained.w_DAORD_06=='P',2,;
      iif(this.Parent.oContained.w_DAORD_06=='D',3,;
      iif(this.Parent.oContained.w_DAORD_06=='S',4,;
      iif(this.Parent.oContained.w_DAORD_06=='F',5,;
      0)))))
  endfunc


  add object oDAPRI_06_1_43 as StdCombo with uid="FIJMPGDWCN",rtseq=19,rtrep=.f.,left=465,top=449,width=73,height=22;
    , ToolTipText = "Imposta la priorit� del campo durante gli ordinamenti (1=massima, 6=minima)";
    , HelpContextID = 27406740;
    , cFormVar="w_DAPRI_06",RowSource=""+"1,"+"2,"+"3,"+"4,"+"5,"+"6", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDAPRI_06_1_43.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    0)))))))
  endfunc
  func oDAPRI_06_1_43.GetRadio()
    this.Parent.oContained.w_DAPRI_06 = this.RadioValue()
    return .t.
  endfunc

  func oDAPRI_06_1_43.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DAPRI_06==1,1,;
      iif(this.Parent.oContained.w_DAPRI_06==2,2,;
      iif(this.Parent.oContained.w_DAPRI_06==3,3,;
      iif(this.Parent.oContained.w_DAPRI_06==4,4,;
      iif(this.Parent.oContained.w_DAPRI_06==5,5,;
      iif(this.Parent.oContained.w_DAPRI_06==6,6,;
      0))))))
  endfunc

  add object oStr_1_2 as StdString with uid="DZLRHGYOZE",Visible=.t., Left=10, Top=9,;
    Alignment=0, Width=288, Height=18,;
    Caption="Campo a disposizione 1"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="YYLFWNZSED",Visible=.t., Left=10, Top=32,;
    Alignment=1, Width=141, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="UMZOFMRXVW",Visible=.t., Left=10, Top=82,;
    Alignment=0, Width=288, Height=18,;
    Caption="Campo a disposizione 2"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="BBARLJSSFQ",Visible=.t., Left=10, Top=164,;
    Alignment=0, Width=288, Height=18,;
    Caption="Campo a disposizione 3"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="JNIIOHFKHT",Visible=.t., Left=10, Top=245,;
    Alignment=0, Width=288, Height=18,;
    Caption="Campo a disposizione 4"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RMGYWBZWMQ",Visible=.t., Left=10, Top=321,;
    Alignment=0, Width=288, Height=18,;
    Caption="Campo a disposizione 5"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="HUGISJRMKT",Visible=.t., Left=10, Top=398,;
    Alignment=0, Width=288, Height=18,;
    Caption="Campo a disposizione 6"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XYDJTCPJHL",Visible=.t., Left=380, Top=58,;
    Alignment=1, Width=81, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="YMCDIMMPTC",Visible=.t., Left=31, Top=58,;
    Alignment=1, Width=120, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="CXFSOPXYIP",Visible=.t., Left=10, Top=105,;
    Alignment=1, Width=141, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="HKUMHZAAIM",Visible=.t., Left=380, Top=133,;
    Alignment=1, Width=81, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="SKVDXHHWHF",Visible=.t., Left=31, Top=133,;
    Alignment=1, Width=120, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="YQUNEZJSJS",Visible=.t., Left=10, Top=189,;
    Alignment=1, Width=141, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="IQGWQPNLUZ",Visible=.t., Left=380, Top=222,;
    Alignment=1, Width=81, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="SQCRPRLCSF",Visible=.t., Left=31, Top=222,;
    Alignment=1, Width=120, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="EMGTFBKPWE",Visible=.t., Left=10, Top=270,;
    Alignment=1, Width=141, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="QWDCSOAUAF",Visible=.t., Left=380, Top=295,;
    Alignment=1, Width=81, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="DMWDPLWQWT",Visible=.t., Left=31, Top=295,;
    Alignment=1, Width=120, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="TTEUSABVXO",Visible=.t., Left=10, Top=348,;
    Alignment=1, Width=141, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="NXHWXIMSJR",Visible=.t., Left=380, Top=373,;
    Alignment=1, Width=81, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="QPWFOKUJIL",Visible=.t., Left=31, Top=373,;
    Alignment=1, Width=120, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="YXCPVUJOUE",Visible=.t., Left=10, Top=425,;
    Alignment=1, Width=141, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="SEPRUTMZSN",Visible=.t., Left=380, Top=450,;
    Alignment=1, Width=81, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="JSMDKBEVIU",Visible=.t., Left=31, Top=450,;
    Alignment=1, Width=120, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ada','DATI_AGG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DASERIAL=DATI_AGG.DASERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
