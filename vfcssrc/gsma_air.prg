* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_air                                                        *
*              Determinazione domanda media                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-27                                                      *
* Last revis.: 2014-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_air"))

* --- Class definition
define class tgsma_air as StdForm
  Top    = 10
  Left   = 4

  * --- Standard Properties
  Width  = 798
  Height = 346+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-05"
  HelpContextID=9075561
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=42

  * --- Constant Properties
  IRDOMMED_IDX = 0
  ART_ICOL_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  CAU_DOMA_IDX = 0
  cFile = "IRDOMMED"
  cKeySelect = "IRNUMDOM,IRCODESE"
  cKeyWhere  = "IRNUMDOM=this.w_IRNUMDOM and IRCODESE=this.w_IRCODESE"
  cKeyWhereODBC = '"IRNUMDOM="+cp_ToStrODBC(this.w_IRNUMDOM)';
      +'+" and IRCODESE="+cp_ToStrODBC(this.w_IRCODESE)';

  cKeyWhereODBCqualified = '"IRDOMMED.IRNUMDOM="+cp_ToStrODBC(this.w_IRNUMDOM)';
      +'+" and IRDOMMED.IRCODESE="+cp_ToStrODBC(this.w_IRCODESE)';

  cPrg = "gsma_air"
  cComment = "Determinazione domanda media"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZIENDA = space(5)
  w_IRNUMDOM = space(6)
  w_IRDESMED = space(40)
  w_IRCODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  o_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  o_FINESE = ctod('  /  /  ')
  w_IRDATINI = ctod('  /  /  ')
  w_IRDATFIN = ctod('  /  /  ')
  w_IRCODINI = space(41)
  o_IRCODINI = space(41)
  w_DESINI = space(41)
  w_IRCODFIN = space(41)
  w_DESFIN = space(41)
  w_IRFAMINI = space(5)
  o_IRFAMINI = space(5)
  w_DESFAMAI = space(40)
  w_IRFAMIFN = space(5)
  w_DESFAMAF = space(40)
  w_IRGRUINI = space(5)
  o_IRGRUINI = space(5)
  w_DESGRUI = space(40)
  w_IRGRUFIN = space(5)
  w_DESGRUF = space(40)
  w_IRCATINI = space(5)
  o_IRCATINI = space(5)
  w_DESCATI = space(40)
  w_IRCATFIN = space(5)
  w_DESCATF = space(40)
  w_IRTIPGES = space(1)
  w_IRMAGINI = space(5)
  o_IRMAGINI = space(5)
  w_DESMAGI = space(40)
  w_IRMAGFIN = space(5)
  w_DESMAGF = space(40)
  w_IRFAPINI = space(5)
  w_IRFAPFIN = space(5)
  w_IRPIAINI = space(5)
  o_IRPIAINI = space(5)
  w_IRPIAFIN = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_DECQT = 0
  w_IRCODGRU = space(5)
  w_DESGRU = space(40)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_IRNUMDOM = this.W_IRNUMDOM
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'IRDOMMED','gsma_air')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_airPag1","gsma_air",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Calcolo domanda")
      .Pages(1).HelpContextID = 236449670
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIRNUMDOM_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='GRUMERC'
    this.cWorkTables[6]='CATEGOMO'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='CAM_AGAZ'
    this.cWorkTables[9]='CAU_DOMA'
    this.cWorkTables[10]='IRDOMMED'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(10))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.IRDOMMED_IDX,5],7]
    this.nPostItConn=i_TableProp[this.IRDOMMED_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_IRNUMDOM = NVL(IRNUMDOM,space(6))
      .w_IRCODESE = NVL(IRCODESE,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    local link_1_28_joined
    link_1_28_joined=.f.
    local link_1_31_joined
    link_1_31_joined=.f.
    local link_1_34_joined
    link_1_34_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    local link_1_41_joined
    link_1_41_joined=.f.
    local link_1_44_joined
    link_1_44_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from IRDOMMED where IRNUMDOM=KeySet.IRNUMDOM
    *                            and IRCODESE=KeySet.IRCODESE
    *
    i_nConn = i_TableProp[this.IRDOMMED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IRDOMMED_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('IRDOMMED')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "IRDOMMED.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' IRDOMMED '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_28_joined=this.AddJoinedLink_1_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_31_joined=this.AddJoinedLink_1_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_34_joined=this.AddJoinedLink_1_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_41_joined=this.AddJoinedLink_1_41(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_44_joined=this.AddJoinedLink_1_44(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IRNUMDOM',this.w_IRNUMDOM  ,'IRCODESE',this.w_IRCODESE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZIENDA = i_CODAZI
        .w_INIESE = ctod("  /  /  ")
        .w_FINESE = ctod("  /  /  ")
        .w_DESINI = space(41)
        .w_DESFIN = space(41)
        .w_DESFAMAI = space(40)
        .w_DESFAMAF = space(40)
        .w_DESGRUI = space(40)
        .w_DESGRUF = space(40)
        .w_DESCATI = space(40)
        .w_DESCATF = space(40)
        .w_DESMAGI = space(40)
        .w_DESMAGF = space(40)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DECQT = 0
        .w_DESGRU = space(40)
          .link_1_1('Load')
        .w_IRNUMDOM = NVL(IRNUMDOM,space(6))
        .op_IRNUMDOM = .w_IRNUMDOM
        .w_IRDESMED = NVL(IRDESMED,space(40))
        .w_IRCODESE = NVL(IRCODESE,space(4))
          .link_1_4('Load')
        .w_IRDATINI = NVL(cp_ToDate(IRDATINI),ctod("  /  /  "))
        .w_IRDATFIN = NVL(cp_ToDate(IRDATFIN),ctod("  /  /  "))
        .w_IRCODINI = NVL(IRCODINI,space(41))
          if link_1_16_joined
            this.w_IRCODINI = NVL(ARCODART116,NVL(this.w_IRCODINI,space(41)))
            this.w_DESINI = NVL(ARDESART116,space(41))
          else
          .link_1_16('Load')
          endif
        .w_IRCODFIN = NVL(IRCODFIN,space(41))
          if link_1_19_joined
            this.w_IRCODFIN = NVL(ARCODART119,NVL(this.w_IRCODFIN,space(41)))
            this.w_DESFIN = NVL(ARDESART119,space(41))
          else
          .link_1_19('Load')
          endif
        .w_IRFAMINI = NVL(IRFAMINI,space(5))
          if link_1_22_joined
            this.w_IRFAMINI = NVL(FACODICE122,NVL(this.w_IRFAMINI,space(5)))
            this.w_DESFAMAI = NVL(FADESCRI122,space(40))
          else
          .link_1_22('Load')
          endif
        .w_IRFAMIFN = NVL(IRFAMIFN,space(5))
          if link_1_26_joined
            this.w_IRFAMIFN = NVL(FACODICE126,NVL(this.w_IRFAMIFN,space(5)))
            this.w_DESFAMAF = NVL(FADESCRI126,space(40))
          else
          .link_1_26('Load')
          endif
        .w_IRGRUINI = NVL(IRGRUINI,space(5))
          if link_1_28_joined
            this.w_IRGRUINI = NVL(GMCODICE128,NVL(this.w_IRGRUINI,space(5)))
            this.w_DESGRUI = NVL(GMDESCRI128,space(40))
          else
          .link_1_28('Load')
          endif
        .w_IRGRUFIN = NVL(IRGRUFIN,space(5))
          if link_1_31_joined
            this.w_IRGRUFIN = NVL(GMCODICE131,NVL(this.w_IRGRUFIN,space(5)))
            this.w_DESGRUF = NVL(GMDESCRI131,space(40))
          else
          .link_1_31('Load')
          endif
        .w_IRCATINI = NVL(IRCATINI,space(5))
          if link_1_34_joined
            this.w_IRCATINI = NVL(OMCODICE134,NVL(this.w_IRCATINI,space(5)))
            this.w_DESCATI = NVL(OMDESCRI134,space(40))
          else
          .link_1_34('Load')
          endif
        .w_IRCATFIN = NVL(IRCATFIN,space(5))
          if link_1_38_joined
            this.w_IRCATFIN = NVL(OMCODICE138,NVL(this.w_IRCATFIN,space(5)))
            this.w_DESCATF = NVL(OMDESCRI138,space(40))
          else
          .link_1_38('Load')
          endif
        .w_IRTIPGES = NVL(IRTIPGES,space(1))
        .w_IRMAGINI = NVL(IRMAGINI,space(5))
          if link_1_41_joined
            this.w_IRMAGINI = NVL(MGCODMAG141,NVL(this.w_IRMAGINI,space(5)))
            this.w_DESMAGI = NVL(MGDESMAG141,space(40))
          else
          .link_1_41('Load')
          endif
        .w_IRMAGFIN = NVL(IRMAGFIN,space(5))
          if link_1_44_joined
            this.w_IRMAGFIN = NVL(MGCODMAG144,NVL(this.w_IRMAGFIN,space(5)))
            this.w_DESMAGF = NVL(MGDESMAG144,space(40))
          else
          .link_1_44('Load')
          endif
        .w_IRFAPINI = NVL(IRFAPINI,space(5))
        .w_IRFAPFIN = NVL(IRFAPFIN,space(5))
        .w_IRPIAINI = NVL(IRPIAINI,space(5))
        .w_IRPIAFIN = NVL(IRPIAFIN,space(5))
        .w_OBTEST = .w_IRDATINI
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .w_IRCODGRU = NVL(IRCODGRU,space(5))
          .link_1_62('Load')
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'IRDOMMED')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA = space(5)
      .w_IRNUMDOM = space(6)
      .w_IRDESMED = space(40)
      .w_IRCODESE = space(4)
      .w_INIESE = ctod("  /  /  ")
      .w_FINESE = ctod("  /  /  ")
      .w_IRDATINI = ctod("  /  /  ")
      .w_IRDATFIN = ctod("  /  /  ")
      .w_IRCODINI = space(41)
      .w_DESINI = space(41)
      .w_IRCODFIN = space(41)
      .w_DESFIN = space(41)
      .w_IRFAMINI = space(5)
      .w_DESFAMAI = space(40)
      .w_IRFAMIFN = space(5)
      .w_DESFAMAF = space(40)
      .w_IRGRUINI = space(5)
      .w_DESGRUI = space(40)
      .w_IRGRUFIN = space(5)
      .w_DESGRUF = space(40)
      .w_IRCATINI = space(5)
      .w_DESCATI = space(40)
      .w_IRCATFIN = space(5)
      .w_DESCATF = space(40)
      .w_IRTIPGES = space(1)
      .w_IRMAGINI = space(5)
      .w_DESMAGI = space(40)
      .w_IRMAGFIN = space(5)
      .w_DESMAGF = space(40)
      .w_IRFAPINI = space(5)
      .w_IRFAPFIN = space(5)
      .w_IRPIAINI = space(5)
      .w_IRPIAFIN = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_DECQT = 0
      .w_IRCODGRU = space(5)
      .w_DESGRU = space(40)
      if .cFunction<>"Filter"
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AZIENDA))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_IRCODESE = g_CODESE
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_IRCODESE))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,6,.f.)
        .w_IRDATINI = .w_INIESE
        .w_IRDATFIN = iif(.w_IRCODESE=g_CODESE,I_DATSYS,.w_FINESE)
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_IRCODINI))
          .link_1_16('Full')
          endif
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_IRCODFIN))
          .link_1_19('Full')
          endif
        .DoRTCalc(12,13,.f.)
          if not(empty(.w_IRFAMINI))
          .link_1_22('Full')
          endif
        .DoRTCalc(14,15,.f.)
          if not(empty(.w_IRFAMIFN))
          .link_1_26('Full')
          endif
        .DoRTCalc(16,17,.f.)
          if not(empty(.w_IRGRUINI))
          .link_1_28('Full')
          endif
        .DoRTCalc(18,19,.f.)
          if not(empty(.w_IRGRUFIN))
          .link_1_31('Full')
          endif
        .DoRTCalc(20,21,.f.)
          if not(empty(.w_IRCATINI))
          .link_1_34('Full')
          endif
        .DoRTCalc(22,23,.f.)
          if not(empty(.w_IRCATFIN))
          .link_1_38('Full')
          endif
          .DoRTCalc(24,24,.f.)
        .w_IRTIPGES = 'T'
        .DoRTCalc(26,26,.f.)
          if not(empty(.w_IRMAGINI))
          .link_1_41('Full')
          endif
        .DoRTCalc(27,28,.f.)
          if not(empty(.w_IRMAGFIN))
          .link_1_44('Full')
          endif
          .DoRTCalc(29,29,.f.)
        .w_IRFAPINI = space(5)
        .w_IRFAPFIN = space(5)
        .w_IRPIAINI = space(5)
        .w_IRPIAFIN = space(5)
        .w_OBTEST = .w_IRDATINI
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .DoRTCalc(35,41,.f.)
          if not(empty(.w_IRCODGRU))
          .link_1_62('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'IRDOMMED')
    this.DoRTCalc(42,42,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.IRDOMMED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IRDOMMED_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SERIDIR","i_codazi,w_IRNUMDOM")
      .op_codazi = .w_codazi
      .op_IRNUMDOM = .w_IRNUMDOM
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oIRNUMDOM_1_2.enabled = i_bVal
      .Page1.oPag.oIRDESMED_1_3.enabled = i_bVal
      .Page1.oPag.oIRCODESE_1_4.enabled = i_bVal
      .Page1.oPag.oIRDATINI_1_7.enabled = i_bVal
      .Page1.oPag.oIRDATFIN_1_8.enabled = i_bVal
      .Page1.oPag.oIRCODINI_1_16.enabled = i_bVal
      .Page1.oPag.oIRCODFIN_1_19.enabled = i_bVal
      .Page1.oPag.oIRFAMINI_1_22.enabled = i_bVal
      .Page1.oPag.oIRFAMIFN_1_26.enabled = i_bVal
      .Page1.oPag.oIRGRUINI_1_28.enabled = i_bVal
      .Page1.oPag.oIRGRUFIN_1_31.enabled = i_bVal
      .Page1.oPag.oIRCATINI_1_34.enabled = i_bVal
      .Page1.oPag.oIRCATFIN_1_38.enabled = i_bVal
      .Page1.oPag.oIRTIPGES_1_40.enabled = i_bVal
      .Page1.oPag.oIRMAGINI_1_41.enabled = i_bVal
      .Page1.oPag.oIRMAGFIN_1_44.enabled = i_bVal
      .Page1.oPag.oIRCODGRU_1_62.enabled = i_bVal
      .Page1.oPag.oBtn_1_64.enabled = i_bVal
      .Page1.oPag.oObj_1_60.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oIRNUMDOM_1_2.enabled = .f.
        .Page1.oPag.oIRCODESE_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oIRNUMDOM_1_2.enabled = .t.
        .Page1.oPag.oIRCODESE_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'IRDOMMED',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.IRDOMMED_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRNUMDOM,"IRNUMDOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRDESMED,"IRDESMED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRCODESE,"IRCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRDATINI,"IRDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRDATFIN,"IRDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRCODINI,"IRCODINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRCODFIN,"IRCODFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRFAMINI,"IRFAMINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRFAMIFN,"IRFAMIFN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRGRUINI,"IRGRUINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRGRUFIN,"IRGRUFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRCATINI,"IRCATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRCATFIN,"IRCATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRTIPGES,"IRTIPGES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRMAGINI,"IRMAGINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRMAGFIN,"IRMAGFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRFAPINI,"IRFAPINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRFAPFIN,"IRFAPFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRPIAINI,"IRPIAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRPIAFIN,"IRPIAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IRCODGRU,"IRCODGRU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.IRDOMMED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IRDOMMED_IDX,2])
    i_lTable = "IRDOMMED"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.IRDOMMED_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.IRDOMMED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IRDOMMED_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.IRDOMMED_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SERIDIR","i_codazi,w_IRNUMDOM")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into IRDOMMED
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'IRDOMMED')
        i_extval=cp_InsertValODBCExtFlds(this,'IRDOMMED')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(IRNUMDOM,IRDESMED,IRCODESE,IRDATINI,IRDATFIN"+;
                  ",IRCODINI,IRCODFIN,IRFAMINI,IRFAMIFN,IRGRUINI"+;
                  ",IRGRUFIN,IRCATINI,IRCATFIN,IRTIPGES,IRMAGINI"+;
                  ",IRMAGFIN,IRFAPINI,IRFAPFIN,IRPIAINI,IRPIAFIN"+;
                  ",UTCC,UTCV,UTDC,UTDV,IRCODGRU "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_IRNUMDOM)+;
                  ","+cp_ToStrODBC(this.w_IRDESMED)+;
                  ","+cp_ToStrODBCNull(this.w_IRCODESE)+;
                  ","+cp_ToStrODBC(this.w_IRDATINI)+;
                  ","+cp_ToStrODBC(this.w_IRDATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_IRCODINI)+;
                  ","+cp_ToStrODBCNull(this.w_IRCODFIN)+;
                  ","+cp_ToStrODBCNull(this.w_IRFAMINI)+;
                  ","+cp_ToStrODBCNull(this.w_IRFAMIFN)+;
                  ","+cp_ToStrODBCNull(this.w_IRGRUINI)+;
                  ","+cp_ToStrODBCNull(this.w_IRGRUFIN)+;
                  ","+cp_ToStrODBCNull(this.w_IRCATINI)+;
                  ","+cp_ToStrODBCNull(this.w_IRCATFIN)+;
                  ","+cp_ToStrODBC(this.w_IRTIPGES)+;
                  ","+cp_ToStrODBCNull(this.w_IRMAGINI)+;
                  ","+cp_ToStrODBCNull(this.w_IRMAGFIN)+;
                  ","+cp_ToStrODBC(this.w_IRFAPINI)+;
                  ","+cp_ToStrODBC(this.w_IRFAPFIN)+;
                  ","+cp_ToStrODBC(this.w_IRPIAINI)+;
                  ","+cp_ToStrODBC(this.w_IRPIAFIN)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_IRCODGRU)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'IRDOMMED')
        i_extval=cp_InsertValVFPExtFlds(this,'IRDOMMED')
        cp_CheckDeletedKey(i_cTable,0,'IRNUMDOM',this.w_IRNUMDOM,'IRCODESE',this.w_IRCODESE)
        INSERT INTO (i_cTable);
              (IRNUMDOM,IRDESMED,IRCODESE,IRDATINI,IRDATFIN,IRCODINI,IRCODFIN,IRFAMINI,IRFAMIFN,IRGRUINI,IRGRUFIN,IRCATINI,IRCATFIN,IRTIPGES,IRMAGINI,IRMAGFIN,IRFAPINI,IRFAPFIN,IRPIAINI,IRPIAFIN,UTCC,UTCV,UTDC,UTDV,IRCODGRU  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_IRNUMDOM;
                  ,this.w_IRDESMED;
                  ,this.w_IRCODESE;
                  ,this.w_IRDATINI;
                  ,this.w_IRDATFIN;
                  ,this.w_IRCODINI;
                  ,this.w_IRCODFIN;
                  ,this.w_IRFAMINI;
                  ,this.w_IRFAMIFN;
                  ,this.w_IRGRUINI;
                  ,this.w_IRGRUFIN;
                  ,this.w_IRCATINI;
                  ,this.w_IRCATFIN;
                  ,this.w_IRTIPGES;
                  ,this.w_IRMAGINI;
                  ,this.w_IRMAGFIN;
                  ,this.w_IRFAPINI;
                  ,this.w_IRFAPFIN;
                  ,this.w_IRPIAINI;
                  ,this.w_IRPIAFIN;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_IRCODGRU;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- gsma_air
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.IRDOMMED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IRDOMMED_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.IRDOMMED_IDX,i_nConn)
      *
      * update IRDOMMED
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'IRDOMMED')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " IRDESMED="+cp_ToStrODBC(this.w_IRDESMED)+;
             ",IRDATINI="+cp_ToStrODBC(this.w_IRDATINI)+;
             ",IRDATFIN="+cp_ToStrODBC(this.w_IRDATFIN)+;
             ",IRCODINI="+cp_ToStrODBCNull(this.w_IRCODINI)+;
             ",IRCODFIN="+cp_ToStrODBCNull(this.w_IRCODFIN)+;
             ",IRFAMINI="+cp_ToStrODBCNull(this.w_IRFAMINI)+;
             ",IRFAMIFN="+cp_ToStrODBCNull(this.w_IRFAMIFN)+;
             ",IRGRUINI="+cp_ToStrODBCNull(this.w_IRGRUINI)+;
             ",IRGRUFIN="+cp_ToStrODBCNull(this.w_IRGRUFIN)+;
             ",IRCATINI="+cp_ToStrODBCNull(this.w_IRCATINI)+;
             ",IRCATFIN="+cp_ToStrODBCNull(this.w_IRCATFIN)+;
             ",IRTIPGES="+cp_ToStrODBC(this.w_IRTIPGES)+;
             ",IRMAGINI="+cp_ToStrODBCNull(this.w_IRMAGINI)+;
             ",IRMAGFIN="+cp_ToStrODBCNull(this.w_IRMAGFIN)+;
             ",IRFAPINI="+cp_ToStrODBC(this.w_IRFAPINI)+;
             ",IRFAPFIN="+cp_ToStrODBC(this.w_IRFAPFIN)+;
             ",IRPIAINI="+cp_ToStrODBC(this.w_IRPIAINI)+;
             ",IRPIAFIN="+cp_ToStrODBC(this.w_IRPIAFIN)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",IRCODGRU="+cp_ToStrODBCNull(this.w_IRCODGRU)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'IRDOMMED')
        i_cWhere = cp_PKFox(i_cTable  ,'IRNUMDOM',this.w_IRNUMDOM  ,'IRCODESE',this.w_IRCODESE  )
        UPDATE (i_cTable) SET;
              IRDESMED=this.w_IRDESMED;
             ,IRDATINI=this.w_IRDATINI;
             ,IRDATFIN=this.w_IRDATFIN;
             ,IRCODINI=this.w_IRCODINI;
             ,IRCODFIN=this.w_IRCODFIN;
             ,IRFAMINI=this.w_IRFAMINI;
             ,IRFAMIFN=this.w_IRFAMIFN;
             ,IRGRUINI=this.w_IRGRUINI;
             ,IRGRUFIN=this.w_IRGRUFIN;
             ,IRCATINI=this.w_IRCATINI;
             ,IRCATFIN=this.w_IRCATFIN;
             ,IRTIPGES=this.w_IRTIPGES;
             ,IRMAGINI=this.w_IRMAGINI;
             ,IRMAGFIN=this.w_IRMAGFIN;
             ,IRFAPINI=this.w_IRFAPINI;
             ,IRFAPFIN=this.w_IRFAPFIN;
             ,IRPIAINI=this.w_IRPIAINI;
             ,IRPIAFIN=this.w_IRPIAFIN;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,IRCODGRU=this.w_IRCODGRU;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.IRDOMMED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IRDOMMED_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.IRDOMMED_IDX,i_nConn)
      *
      * delete IRDOMMED
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'IRNUMDOM',this.w_IRNUMDOM  ,'IRCODESE',this.w_IRCODESE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.IRDOMMED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IRDOMMED_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,6,.t.)
        if .o_INIESE<>.w_INIESE
            .w_IRDATINI = .w_INIESE
        endif
        if .o_FINESE<>.w_FINESE
            .w_IRDATFIN = iif(.w_IRCODESE=g_CODESE,I_DATSYS,.w_FINESE)
        endif
        .DoRTCalc(9,10,.t.)
        if .o_IRCODINI<>.w_IRCODINI
          .link_1_19('Full')
        endif
        .DoRTCalc(12,14,.t.)
        if .o_IRFAMINI<>.w_IRFAMINI
          .link_1_26('Full')
        endif
        .DoRTCalc(16,18,.t.)
        if .o_IRGRUINI<>.w_IRGRUINI
          .link_1_31('Full')
        endif
        .DoRTCalc(20,22,.t.)
        if .o_IRCATINI<>.w_IRCATINI
          .link_1_38('Full')
        endif
        .DoRTCalc(24,27,.t.)
        if .o_IRMAGINI<>.w_IRMAGINI
          .link_1_44('Full')
        endif
        .DoRTCalc(29,33,.t.)
            .w_OBTEST = .w_IRDATINI
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .DoRTCalc(35,40,.t.)
        if .o_IRCATINI<>.w_IRCATINI
          .link_1_62('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SERIDIR","i_codazi,w_IRNUMDOM")
          .op_IRNUMDOM = .w_IRNUMDOM
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(42,42,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIRDESMED_1_3.enabled = this.oPgFrm.Page1.oPag.oIRDESMED_1_3.mCond()
    this.oPgFrm.Page1.oPag.oIRCODESE_1_4.enabled = this.oPgFrm.Page1.oPag.oIRCODESE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oIRDATINI_1_7.enabled = this.oPgFrm.Page1.oPag.oIRDATINI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oIRDATFIN_1_8.enabled = this.oPgFrm.Page1.oPag.oIRDATFIN_1_8.mCond()
    this.oPgFrm.Page1.oPag.oIRCODINI_1_16.enabled = this.oPgFrm.Page1.oPag.oIRCODINI_1_16.mCond()
    this.oPgFrm.Page1.oPag.oIRCODFIN_1_19.enabled = this.oPgFrm.Page1.oPag.oIRCODFIN_1_19.mCond()
    this.oPgFrm.Page1.oPag.oIRFAMINI_1_22.enabled = this.oPgFrm.Page1.oPag.oIRFAMINI_1_22.mCond()
    this.oPgFrm.Page1.oPag.oIRFAMIFN_1_26.enabled = this.oPgFrm.Page1.oPag.oIRFAMIFN_1_26.mCond()
    this.oPgFrm.Page1.oPag.oIRGRUINI_1_28.enabled = this.oPgFrm.Page1.oPag.oIRGRUINI_1_28.mCond()
    this.oPgFrm.Page1.oPag.oIRGRUFIN_1_31.enabled = this.oPgFrm.Page1.oPag.oIRGRUFIN_1_31.mCond()
    this.oPgFrm.Page1.oPag.oIRCATINI_1_34.enabled = this.oPgFrm.Page1.oPag.oIRCATINI_1_34.mCond()
    this.oPgFrm.Page1.oPag.oIRCATFIN_1_38.enabled = this.oPgFrm.Page1.oPag.oIRCATFIN_1_38.mCond()
    this.oPgFrm.Page1.oPag.oIRMAGINI_1_41.enabled = this.oPgFrm.Page1.oPag.oIRMAGINI_1_41.mCond()
    this.oPgFrm.Page1.oPag.oIRMAGFIN_1_44.enabled = this.oPgFrm.Page1.oPag.oIRMAGFIN_1_44.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPERPQT";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZPERPQT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_DECQT = NVL(_Link_.AZPERPQT,0)
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_DECQT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IRCODESE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_IRCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_IRCODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oIRCODESE_1_4'),i_cWhere,'GSAR_MES',"Elenco esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_IRCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_IRCODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_IRCODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IRCODINI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_IRCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_IRCODINI))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRCODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRCODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oIRCODINI_1_16'),i_cWhere,'GSMA_AAR',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_IRCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_IRCODINI)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRCODINI = NVL(_Link_.ARCODART,space(41))
      this.w_DESINI = NVL(_Link_.ARDESART,space(41))
    else
      if i_cCtrl<>'Load'
        this.w_IRCODINI = space(41)
      endif
      this.w_DESINI = space(41)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IRCODINI<=.w_IRCODFIN or empty(.w_IRCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_IRCODINI = space(41)
        this.w_DESINI = space(41)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.ARCODART as ARCODART116"+ ",link_1_16.ARDESART as ARDESART116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on IRDOMMED.IRCODINI=link_1_16.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and IRDOMMED.IRCODINI=link_1_16.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IRCODFIN
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_IRCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_IRCODFIN))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRCODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRCODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oIRCODFIN_1_19'),i_cWhere,'GSMA_AAR',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_IRCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_IRCODFIN)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRCODFIN = NVL(_Link_.ARCODART,space(41))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(41))
    else
      if i_cCtrl<>'Load'
        this.w_IRCODFIN = space(41)
      endif
      this.w_DESFIN = space(41)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IRCODINI<=.w_IRCODFIN or empty(.w_IRCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_IRCODFIN = space(41)
        this.w_DESFIN = space(41)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.ARCODART as ARCODART119"+ ",link_1_19.ARDESART as ARDESART119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on IRDOMMED.IRCODFIN=link_1_19.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and IRDOMMED.IRCODFIN=link_1_19.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IRFAMINI
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRFAMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_IRFAMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_IRFAMINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRFAMINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRFAMINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oIRFAMINI_1_22'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRFAMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_IRFAMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_IRFAMINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRFAMINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IRFAMINI = space(5)
      endif
      this.w_DESFAMAI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IRFAMINI <= .w_IRFAMIFN OR EMPTY(.w_IRFAMIFN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_IRFAMINI = space(5)
        this.w_DESFAMAI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRFAMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.FACODICE as FACODICE122"+ ","+cp_TransLinkFldName('link_1_22.FADESCRI')+" as FADESCRI122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on IRDOMMED.IRFAMINI=link_1_22.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and IRDOMMED.IRFAMINI=link_1_22.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IRFAMIFN
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRFAMIFN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_IRFAMIFN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_IRFAMIFN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRFAMIFN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRFAMIFN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oIRFAMIFN_1_26'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRFAMIFN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_IRFAMIFN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_IRFAMIFN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRFAMIFN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IRFAMIFN = space(5)
      endif
      this.w_DESFAMAF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IRFAMINI <= .w_IRFAMIFN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_IRFAMIFN = space(5)
        this.w_DESFAMAF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRFAMIFN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.FACODICE as FACODICE126"+ ","+cp_TransLinkFldName('link_1_26.FADESCRI')+" as FADESCRI126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on IRDOMMED.IRFAMIFN=link_1_26.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and IRDOMMED.IRFAMIFN=link_1_26.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IRGRUINI
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRGRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_IRGRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_IRGRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRGRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRGRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oIRGRUINI_1_28'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRGRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_IRGRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_IRGRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRGRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IRGRUINI = space(5)
      endif
      this.w_DESGRUI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IRGRUINI <= .w_IRGRUFIN OR EMPTY(.w_IRGRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_IRGRUINI = space(5)
        this.w_DESGRUI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRGRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_28.GMCODICE as GMCODICE128"+ ","+cp_TransLinkFldName('link_1_28.GMDESCRI')+" as GMDESCRI128"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_28 on IRDOMMED.IRGRUINI=link_1_28.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_28"
          i_cKey=i_cKey+'+" and IRDOMMED.IRGRUINI=link_1_28.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IRGRUFIN
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRGRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_IRGRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_IRGRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRGRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRGRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oIRGRUFIN_1_31'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRGRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_IRGRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_IRGRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRGRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IRGRUFIN = space(5)
      endif
      this.w_DESGRUF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IRGRUINI <= .w_IRGRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_IRGRUFIN = space(5)
        this.w_DESGRUF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRGRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_31.GMCODICE as GMCODICE131"+ ","+cp_TransLinkFldName('link_1_31.GMDESCRI')+" as GMDESCRI131"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_31 on IRDOMMED.IRGRUFIN=link_1_31.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_31"
          i_cKey=i_cKey+'+" and IRDOMMED.IRGRUFIN=link_1_31.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IRCATINI
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRCATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_IRCATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_IRCATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRCATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRCATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oIRCATINI_1_34'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRCATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_IRCATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_IRCATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRCATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IRCATINI = space(5)
      endif
      this.w_DESCATI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IRCATINI<= .w_IRCATFIN OR EMPTY(.w_IRCATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_IRCATINI = space(5)
        this.w_DESCATI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRCATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_34.OMCODICE as OMCODICE134"+ ","+cp_TransLinkFldName('link_1_34.OMDESCRI')+" as OMDESCRI134"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_34 on IRDOMMED.IRCATINI=link_1_34.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_34"
          i_cKey=i_cKey+'+" and IRDOMMED.IRCATINI=link_1_34.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IRCATFIN
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRCATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_IRCATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_IRCATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRCATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRCATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oIRCATFIN_1_38'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRCATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_IRCATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_IRCATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRCATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IRCATFIN = space(5)
      endif
      this.w_DESCATF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IRCATINI<= .w_IRCATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_IRCATFIN = space(5)
        this.w_DESCATF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRCATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.OMCODICE as OMCODICE138"+ ","+cp_TransLinkFldName('link_1_38.OMDESCRI')+" as OMDESCRI138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on IRDOMMED.IRCATFIN=link_1_38.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and IRDOMMED.IRCATFIN=link_1_38.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IRMAGINI
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRMAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_IRMAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_IRMAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRMAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRMAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oIRMAGINI_1_41'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRMAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_IRMAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_IRMAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRMAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IRMAGINI = space(5)
      endif
      this.w_DESMAGI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IRMAGINI <= .w_IRMAGFIN OR EMPTY(.w_IRMAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_IRMAGINI = space(5)
        this.w_DESMAGI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRMAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_41(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_41.MGCODMAG as MGCODMAG141"+ ",link_1_41.MGDESMAG as MGDESMAG141"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_41 on IRDOMMED.IRMAGINI=link_1_41.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_41"
          i_cKey=i_cKey+'+" and IRDOMMED.IRMAGINI=link_1_41.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IRMAGFIN
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRMAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_IRMAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_IRMAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRMAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRMAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oIRMAGFIN_1_44'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRMAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_IRMAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_IRMAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRMAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IRMAGFIN = space(5)
      endif
      this.w_DESMAGF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IRMAGINI <= .w_IRMAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_IRMAGFIN = space(5)
        this.w_DESMAGF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRMAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_44(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_44.MGCODMAG as MGCODMAG144"+ ",link_1_44.MGDESMAG as MGDESMAG144"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_44 on IRDOMMED.IRMAGFIN=link_1_44.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_44"
          i_cKey=i_cKey+'+" and IRDOMMED.IRMAGFIN=link_1_44.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IRCODGRU
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_DOMA_IDX,3]
    i_lTable = "CAU_DOMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_DOMA_IDX,2], .t., this.CAU_DOMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_DOMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IRCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_MCD',True,'CAU_DOMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCAUGRU like "+cp_ToStrODBC(trim(this.w_IRCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCAUGRU,CSCAUDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCAUGRU","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCAUGRU',trim(this.w_IRCODGRU))
          select CSCAUGRU,CSCAUDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCAUGRU into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IRCODGRU)==trim(_Link_.CSCAUGRU) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IRCODGRU) and !this.bDontReportError
            deferred_cp_zoom('CAU_DOMA','*','CSCAUGRU',cp_AbsName(oSource.parent,'oIRCODGRU_1_62'),i_cWhere,'GSMA_MCD',"Gruppo causali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCAUGRU,CSCAUDES";
                     +" from "+i_cTable+" "+i_lTable+" where CSCAUGRU="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCAUGRU',oSource.xKey(1))
            select CSCAUGRU,CSCAUDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IRCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCAUGRU,CSCAUDES";
                   +" from "+i_cTable+" "+i_lTable+" where CSCAUGRU="+cp_ToStrODBC(this.w_IRCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCAUGRU',this.w_IRCODGRU)
            select CSCAUGRU,CSCAUDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IRCODGRU = NVL(_Link_.CSCAUGRU,space(5))
      this.w_DESGRU = NVL(_Link_.CSCAUDES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IRCODGRU = space(5)
      endif
      this.w_DESGRU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_DOMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCAUGRU,1)
      cp_ShowWarn(i_cKey,this.CAU_DOMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IRCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIRNUMDOM_1_2.value==this.w_IRNUMDOM)
      this.oPgFrm.Page1.oPag.oIRNUMDOM_1_2.value=this.w_IRNUMDOM
    endif
    if not(this.oPgFrm.Page1.oPag.oIRDESMED_1_3.value==this.w_IRDESMED)
      this.oPgFrm.Page1.oPag.oIRDESMED_1_3.value=this.w_IRDESMED
    endif
    if not(this.oPgFrm.Page1.oPag.oIRCODESE_1_4.value==this.w_IRCODESE)
      this.oPgFrm.Page1.oPag.oIRCODESE_1_4.value=this.w_IRCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oIRDATINI_1_7.value==this.w_IRDATINI)
      this.oPgFrm.Page1.oPag.oIRDATINI_1_7.value=this.w_IRDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oIRDATFIN_1_8.value==this.w_IRDATFIN)
      this.oPgFrm.Page1.oPag.oIRDATFIN_1_8.value=this.w_IRDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oIRCODINI_1_16.value==this.w_IRCODINI)
      this.oPgFrm.Page1.oPag.oIRCODINI_1_16.value=this.w_IRCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_17.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_17.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oIRCODFIN_1_19.value==this.w_IRCODFIN)
      this.oPgFrm.Page1.oPag.oIRCODFIN_1_19.value=this.w_IRCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_20.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_20.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oIRFAMINI_1_22.value==this.w_IRFAMINI)
      this.oPgFrm.Page1.oPag.oIRFAMINI_1_22.value=this.w_IRFAMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_24.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_24.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oIRFAMIFN_1_26.value==this.w_IRFAMIFN)
      this.oPgFrm.Page1.oPag.oIRFAMIFN_1_26.value=this.w_IRFAMIFN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_27.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_27.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oIRGRUINI_1_28.value==this.w_IRGRUINI)
      this.oPgFrm.Page1.oPag.oIRGRUINI_1_28.value=this.w_IRGRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_30.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_30.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oIRGRUFIN_1_31.value==this.w_IRGRUFIN)
      this.oPgFrm.Page1.oPag.oIRGRUFIN_1_31.value=this.w_IRGRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_33.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_33.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oIRCATINI_1_34.value==this.w_IRCATINI)
      this.oPgFrm.Page1.oPag.oIRCATINI_1_34.value=this.w_IRCATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_37.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_37.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oIRCATFIN_1_38.value==this.w_IRCATFIN)
      this.oPgFrm.Page1.oPag.oIRCATFIN_1_38.value=this.w_IRCATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_39.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_39.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oIRTIPGES_1_40.RadioValue()==this.w_IRTIPGES)
      this.oPgFrm.Page1.oPag.oIRTIPGES_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIRMAGINI_1_41.value==this.w_IRMAGINI)
      this.oPgFrm.Page1.oPag.oIRMAGINI_1_41.value=this.w_IRMAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_43.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_43.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oIRMAGFIN_1_44.value==this.w_IRMAGFIN)
      this.oPgFrm.Page1.oPag.oIRMAGFIN_1_44.value=this.w_IRMAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_46.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_46.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oIRCODGRU_1_62.value==this.w_IRCODGRU)
      this.oPgFrm.Page1.oPag.oIRCODGRU_1_62.value=this.w_IRCODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_63.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_63.value=this.w_DESGRU
    endif
    cp_SetControlsValueExtFlds(this,'IRDOMMED')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_IRDESMED))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRDESMED_1_3.SetFocus()
            i_bnoObbl = !empty(.w_IRDESMED)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_IRCODESE))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRCODESE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_IRCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_IRDATINI)) or not(.w_IRDATINI <= .w_IrDATFIN))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRDATINI_1_7.SetFocus()
            i_bnoObbl = !empty(.w_IRDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore della data finale")
          case   ((empty(.w_IRDATFIN)) or not(.w_IRDATINI <= .w_IRDATFIN))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRDATFIN_1_8.SetFocus()
            i_bnoObbl = !empty(.w_IRDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore della data finale")
          case   not(.w_IRCODINI<=.w_IRCODFIN or empty(.w_IRCODFIN))  and (.cFunction<>'Edit')  and not(empty(.w_IRCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRCODINI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_IRCODINI<=.w_IRCODFIN or empty(.w_IRCODINI))  and (.cFunction<>'Edit')  and not(empty(.w_IRCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRCODFIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_IRFAMINI <= .w_IRFAMIFN OR EMPTY(.w_IRFAMIFN))  and (.cFunction<>'Edit')  and not(empty(.w_IRFAMINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRFAMINI_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_IRFAMINI <= .w_IRFAMIFN)  and (.cFunction<>'Edit')  and not(empty(.w_IRFAMIFN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRFAMIFN_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_IRGRUINI <= .w_IRGRUFIN OR EMPTY(.w_IRGRUFIN))  and (.cFunction<>'Edit')  and not(empty(.w_IRGRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRGRUINI_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_IRGRUINI <= .w_IRGRUFIN)  and (.cFunction<>'Edit')  and not(empty(.w_IRGRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRGRUFIN_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_IRCATINI<= .w_IRCATFIN OR EMPTY(.w_IRCATFIN))  and (.cFunction<>'Edit')  and not(empty(.w_IRCATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRCATINI_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_IRCATINI<= .w_IRCATFIN)  and (.cFunction<>'Edit')  and not(empty(.w_IRCATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRCATFIN_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_IRMAGINI <= .w_IRMAGFIN OR EMPTY(.w_IRMAGFIN))  and (.cFunction<>'Edit')  and not(empty(.w_IRMAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRMAGINI_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_IRMAGINI <= .w_IRMAGFIN)  and (.cFunction<>'Edit')  and not(empty(.w_IRMAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIRMAGFIN_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_INIESE = this.w_INIESE
    this.o_FINESE = this.w_FINESE
    this.o_IRCODINI = this.w_IRCODINI
    this.o_IRFAMINI = this.w_IRFAMINI
    this.o_IRGRUINI = this.w_IRGRUINI
    this.o_IRCATINI = this.w_IRCATINI
    this.o_IRMAGINI = this.w_IRMAGINI
    this.o_IRPIAINI = this.w_IRPIAINI
    return

enddefine

* --- Define pages as container
define class tgsma_airPag1 as StdContainer
  Width  = 794
  height = 347
  stdWidth  = 794
  stdheight = 347
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIRNUMDOM_1_2 as StdField with uid="ORBYCBCFTR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IRNUMDOM", cQueryName = "IRNUMDOM",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo della domanda media",;
    HelpContextID = 123749421,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=84, Top=13, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',6)

  add object oIRDESMED_1_3 as StdField with uid="LWAOZJSLAU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IRDESMED", cQueryName = "IRDESMED",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione associata alla domanda media",;
    HelpContextID = 32447434,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=84, Top=44, InputMask=replicate('X',40)

  func oIRDESMED_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  add object oIRCODESE_1_4 as StdField with uid="IRQQXYKEGI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IRCODESE", cQueryName = "IRNUMDOM,IRCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio",;
    HelpContextID = 151587787,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=444, Top=44, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_MES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_IRCODESE"

  func oIRCODESE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRCODESE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRCODESE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRCODESE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oIRCODESE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MES',"Elenco esercizi",'',this.parent.oContained
  endproc
  proc oIRCODESE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_IRCODESE
     i_obj.ecpSave()
  endproc

  add object oIRDATINI_1_7 as StdField with uid="UGUPPCTRZA",rtseq=7,rtrep=.f.,;
    cFormVar = "w_IRDATINI", cQueryName = "IRDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore della data finale",;
    ToolTipText = "Data a partire dalla quale sono stati considerati i movimenti",;
    HelpContextID = 33874993,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=571, Top=44

  func oIRDATINI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRDATINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IRDATINI <= .w_IrDATFIN)
    endwith
    return bRes
  endfunc

  add object oIRDATFIN_1_8 as StdField with uid="AUHPGQBXOC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IRDATFIN", cQueryName = "IRDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore della data finale",;
    ToolTipText = "Data fino alla quale sono stati considerati i movimenti",;
    HelpContextID = 184228820,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=712, Top=44

  func oIRDATFIN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRDATFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IRDATINI <= .w_IRDATFIN)
    endwith
    return bRes
  endfunc

  add object oIRCODINI_1_16 as StdField with uid="KZLVNVNYZD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_IRCODINI", cQueryName = "IRCODINI",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo iniziale",;
    HelpContextID = 49738801,;
   bGlobalFont=.t.,;
    Height=21, Width=314, Left=110, Top=109, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_IRCODINI"

  func oIRCODINI_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRCODINI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRCODINI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRCODINI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oIRCODINI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Articoli",'',this.parent.oContained
  endproc
  proc oIRCODINI_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_IRCODINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_17 as StdField with uid="GNKUAPTQYP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice articolo iniziale",;
    HelpContextID = 39584202,;
   bGlobalFont=.t.,;
    Height=21, Width=358, Left=426, Top=109, InputMask=replicate('X',41)

  add object oIRCODFIN_1_19 as StdField with uid="TJOPTWSAJR",rtseq=11,rtrep=.f.,;
    cFormVar = "w_IRCODFIN", cQueryName = "IRCODFIN",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo finale",;
    HelpContextID = 168365012,;
   bGlobalFont=.t.,;
    Height=21, Width=314, Left=110, Top=133, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_IRCODFIN"

  proc oIRCODFIN_1_19.mDefault
    with this.Parent.oContained
      if empty(.w_IRCODFIN)
        .w_IRCODFIN = .w_IRCODINI
      endif
    endwith
  endproc

  func oIRCODFIN_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRCODFIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRCODFIN_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRCODFIN_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oIRCODFIN_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Articoli",'',this.parent.oContained
  endproc
  proc oIRCODFIN_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_IRCODFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_20 as StdField with uid="KMZPQMWUNH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice articolo finale",;
    HelpContextID = 229573066,;
   bGlobalFont=.t.,;
    Height=21, Width=358, Left=426, Top=133, InputMask=replicate('X',41)

  add object oIRFAMINI_1_22 as StdField with uid="WSAXRUIGRH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_IRFAMINI", cQueryName = "IRFAMINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice famiglia iniziale",;
    HelpContextID = 41206833,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_IRFAMINI"

  func oIRFAMINI_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRFAMINI_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRFAMINI_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRFAMINI_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oIRFAMINI_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oDESFAMAI_1_24 as StdField with uid="OTJVWRHRIN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice famiglia iniziale",;
    HelpContextID = 254738817,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=178, Top=157, InputMask=replicate('X',40)

  add object oIRFAMIFN_1_26 as StdField with uid="WRJMQQUSTN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_IRFAMIFN", cQueryName = "IRFAMIFN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice famiglia finale",;
    HelpContextID = 227228628,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=500, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_IRFAMIFN"

  proc oIRFAMIFN_1_26.mDefault
    with this.Parent.oContained
      if empty(.w_IRFAMIFN)
        .w_IRFAMIFN = .w_IRFAMINI
      endif
    endwith
  endproc

  func oIRFAMIFN_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRFAMIFN_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRFAMIFN_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRFAMIFN_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oIRFAMIFN_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oDESFAMAF_1_27 as StdField with uid="RDKTLNSCJY",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice famiglia finale",;
    HelpContextID = 254738820,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=566, Top=157, InputMask=replicate('X',40)

  add object oIRGRUINI_1_28 as StdField with uid="OGEWDVKEIX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_IRGRUINI", cQueryName = "IRGRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Guppo merceologico iniziale",;
    HelpContextID = 31700017,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=182, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_IRGRUINI"

  func oIRGRUINI_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRGRUINI_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRGRUINI_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRGRUINI_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oIRGRUINI_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oDESGRUI_1_30 as StdField with uid="ETRKIIXCVX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione guppo merceologico iniziale",;
    HelpContextID = 165805622,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=178, Top=182, InputMask=replicate('X',40)

  add object oIRGRUFIN_1_31 as StdField with uid="BNAMYZKGAK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_IRGRUFIN", cQueryName = "IRGRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Gruppo merceologico finale",;
    HelpContextID = 186403796,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=500, Top=182, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_IRGRUFIN"

  proc oIRGRUFIN_1_31.mDefault
    with this.Parent.oContained
      if empty(.w_IRGRUFIN)
        .w_IRGRUFIN = .w_IRGRUINI
      endif
    endwith
  endproc

  func oIRGRUFIN_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRGRUFIN_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRGRUFIN_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRGRUFIN_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oIRGRUFIN_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oDESGRUF_1_33 as StdField with uid="CRACSOXCVU",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descriizione gruppo merceologico finale",;
    HelpContextID = 165805622,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=566, Top=182, InputMask=replicate('X',40)

  add object oIRCATINI_1_34 as StdField with uid="DYZGBECYIH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_IRCATINI", cQueryName = "IRCATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice famiglia produzione iniziale",;
    HelpContextID = 33879089,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=206, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_IRCATINI"

  func oIRCATINI_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRCATINI_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRCATINI_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRCATINI_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oIRCATINI_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oDESCATI_1_37 as StdField with uid="LCTXWVEWBP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice famiglia iniziale",;
    HelpContextID = 130940470,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=178, Top=206, InputMask=replicate('X',40)

  add object oIRCATFIN_1_38 as StdField with uid="QMZPEBAUQL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_IRCATFIN", cQueryName = "IRCATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice famiglia produzione finale",;
    HelpContextID = 184224724,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=500, Top=206, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_IRCATFIN"

  proc oIRCATFIN_1_38.mDefault
    with this.Parent.oContained
      if empty(.w_IRCATFIN)
        .w_IRCATFIN = .w_IRCATINI
      endif
    endwith
  endproc

  func oIRCATFIN_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRCATFIN_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRCATFIN_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRCATFIN_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oIRCATFIN_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oDESCATF_1_39 as StdField with uid="UEANTVTCCJ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 130940470,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=566, Top=206, InputMask=replicate('X',40)


  add object oIRTIPGES_1_40 as StdCombo with uid="NMKUCZBOSF",rtseq=25,rtrep=.f.,left=500,top=231,width=152,height=21;
    , HelpContextID = 197401561;
    , cFormVar="w_IRTIPGES",RowSource=""+"a fabbisogno,"+"a scorta,"+"a consumo,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIRTIPGES_1_40.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oIRTIPGES_1_40.GetRadio()
    this.Parent.oContained.w_IRTIPGES = this.RadioValue()
    return .t.
  endfunc

  func oIRTIPGES_1_40.SetRadio()
    this.Parent.oContained.w_IRTIPGES=trim(this.Parent.oContained.w_IRTIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_IRTIPGES=='F',1,;
      iif(this.Parent.oContained.w_IRTIPGES=='S',2,;
      iif(this.Parent.oContained.w_IRTIPGES=='C',3,;
      iif(this.Parent.oContained.w_IRTIPGES=='T',4,;
      0))))
  endfunc

  add object oIRMAGINI_1_41 as StdField with uid="YEXKWZAGMG",rtseq=26,rtrep=.f.,;
    cFormVar = "w_IRMAGINI", cQueryName = "IRMAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice magazzino iniziale",;
    HelpContextID = 47469617,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=276, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_IRMAGINI"

  func oIRMAGINI_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRMAGINI_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRMAGINI_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRMAGINI_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oIRMAGINI_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oIRMAGINI_1_41.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_IRMAGINI
     i_obj.ecpSave()
  endproc

  add object oDESMAGI_1_43 as StdField with uid="SMQGACSEAJ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice magazzino iniziale",;
    HelpContextID = 181927478,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=178, Top=276, InputMask=replicate('X',40)

  add object oIRMAGFIN_1_44 as StdField with uid="NJMQBQTCMI",rtseq=28,rtrep=.f.,;
    cFormVar = "w_IRMAGFIN", cQueryName = "IRMAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice magazzino finale",;
    HelpContextID = 170634196,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=301, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_IRMAGFIN"

  proc oIRMAGFIN_1_44.mDefault
    with this.Parent.oContained
      if empty(.w_IRMAGFIN)
        .w_IRMAGFIN = .w_IRMAGINI
      endif
    endwith
  endproc

  func oIRMAGFIN_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oIRMAGFIN_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRMAGFIN_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRMAGFIN_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oIRMAGFIN_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oIRMAGFIN_1_44.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_IRMAGFIN
     i_obj.ecpSave()
  endproc

  add object oDESMAGF_1_46 as StdField with uid="GYTUONEXTT",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice magazzino finale",;
    HelpContextID = 181927478,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=178, Top=301, InputMask=replicate('X',40)


  add object oObj_1_60 as cp_runprogram with uid="HXEGIAJMSL",left=11, top=391, width=238,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMA_BIR",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , HelpContextID = 168922086

  add object oIRCODGRU_1_62 as StdField with uid="MDUTKRZRDI",rtseq=41,rtrep=.f.,;
    cFormVar = "w_IRCODGRU", cQueryName = "IRCODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo causali",;
    HelpContextID = 83293221,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=500, Top=277, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_DOMA", cZoomOnZoom="GSMA_MCD", oKey_1_1="CSCAUGRU", oKey_1_2="this.w_IRCODGRU"

  func oIRCODGRU_1_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_62('Part',this)
    endwith
    return bRes
  endfunc

  proc oIRCODGRU_1_62.ecpDrop(oSource)
    this.Parent.oContained.link_1_62('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIRCODGRU_1_62.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_DOMA','*','CSCAUGRU',cp_AbsName(this.parent,'oIRCODGRU_1_62'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_MCD',"Gruppo causali",'',this.parent.oContained
  endproc
  proc oIRCODGRU_1_62.mZoomOnZoom
    local i_obj
    i_obj=GSMA_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCAUGRU=this.parent.oContained.w_IRCODGRU
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_63 as StdField with uid="IDDOEMRCCX",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 102629834,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=566, Top=277, InputMask=replicate('X',40)


  add object oBtn_1_64 as StdButton with uid="PYPQPVDIGP",left=734, top=302, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare l'elaborazione";
    , HelpContextID = 130885497;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_64.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_9 as StdString with uid="JIPVTCWIHF",Visible=.t., Left=378, Top=44,;
    Alignment=1, Width=60, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="DYSQTGTYCH",Visible=.t., Left=0, Top=44,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="FEFKBRVOVA",Visible=.t., Left=500, Top=44,;
    Alignment=1, Width=66, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="FDJFOQRIXG",Visible=.t., Left=646, Top=44,;
    Alignment=1, Width=63, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="GWFLRRWJTG",Visible=.t., Left=3, Top=17,;
    Alignment=1, Width=79, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="SRRCMUTRGQ",Visible=.t., Left=14, Top=71,;
    Alignment=0, Width=85, Height=18,;
    Caption="Codice articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="IYPAPGBIBM",Visible=.t., Left=11, Top=109,;
    Alignment=1, Width=97, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="URWPAUFIUW",Visible=.t., Left=11, Top=133,;
    Alignment=1, Width=97, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ABDRUJYZBN",Visible=.t., Left=11, Top=157,;
    Alignment=1, Width=97, Height=18,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="BUWXVLDVUG",Visible=.t., Left=402, Top=157,;
    Alignment=1, Width=95, Height=18,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="HSXSQARAJB",Visible=.t., Left=11, Top=182,;
    Alignment=1, Width=97, Height=18,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="JFUFHMCPMW",Visible=.t., Left=402, Top=182,;
    Alignment=1, Width=95, Height=18,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="WSEIFWOPOD",Visible=.t., Left=11, Top=206,;
    Alignment=1, Width=97, Height=18,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ARLORWNBII",Visible=.t., Left=402, Top=206,;
    Alignment=1, Width=95, Height=18,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="LQUUQVVYSR",Visible=.t., Left=9, Top=276,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="FMWOCAGXHM",Visible=.t., Left=9, Top=301,;
    Alignment=1, Width=99, Height=18,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="DBOUELFUDI",Visible=.t., Left=14, Top=245,;
    Alignment=0, Width=115, Height=18,;
    Caption="Altre selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="XJBFXSXTLW",Visible=.t., Left=402, Top=277,;
    Alignment=1, Width=95, Height=18,;
    Caption="Gr.causali.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="UWCKJHVKFY",Visible=.t., Left=391, Top=233,;
    Alignment=1, Width=106, Height=18,;
    Caption="Tipo gestione:"  ;
  , bGlobalFont=.t.

  add object oBox_1_15 as StdBox with uid="XKDZJNUGTB",left=5, top=86, width=783,height=3

  add object oBox_1_48 as StdBox with uid="UAYFMKHOEI",left=5, top=260, width=769,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_air','IRDOMMED','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IRNUMDOM=IRDOMMED.IRNUMDOM";
  +" and "+i_cAliasName2+".IRCODESE=IRDOMMED.IRCODESE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
