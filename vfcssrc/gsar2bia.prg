* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar2bia                                                        *
*              FILTRO RISORSE                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-11                                                      *
* Last revis.: 2011-01-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_DPTIPRIS,w_CODPART,w_GRUPART,w_FLESGR,w_CURPART
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar2bia",oParentObject,m.w_DPTIPRIS,m.w_CODPART,m.w_GRUPART,m.w_FLESGR,m.w_CURPART)
return(i_retval)

define class tgsar2bia as StdBatch
  * --- Local variables
  w_DPTIPRIS = space(1)
  w_CODPART = space(5)
  w_GRUPART = space(5)
  w_FLESGR = space(1)
  w_CURPART = space(10)
  w_PAVISTRC = space(1)
  w_PAFLVISI = space(1)
  w_PACODAZI = space(5)
  w_FLTPART = space(5)
  w_FLTRIS = space(5)
  * --- WorkFile variables
  GSAR2BIA_idx=0
  PAR_AGEN_idx=0
  DIPENDEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PACODAZI = i_CODAZI
    this.w_CODPART = IIF( VARTYPE(this.w_CODPART)<>"C", "", this.w_CODPART )
    this.w_GRUPART = IIF( VARTYPE(this.w_GRUPART)<>"C", "", this.w_GRUPART )
    this.w_FLESGR = IIF( VARTYPE(this.w_FLESGR)<>"C", "N", this.w_FLESGR )
    this.w_CURPART = IIF( VARTYPE(this.w_CURPART)<>"C", space(1), this.w_CURPART)
    * --- Read from PAR_AGEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAVISTRC,PAFLVISI"+;
        " from "+i_cTable+" PAR_AGEN where ";
            +"PACODAZI = "+cp_ToStrODBC(this.w_PACODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAVISTRC,PAFLVISI;
        from (i_cTable) where;
            PACODAZI = this.w_PACODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PAVISTRC = NVL(cp_ToDate(_read_.PAVISTRC),cp_NullValue(_read_.PAVISTRC))
      this.w_PAFLVISI = NVL(cp_ToDate(_read_.PAFLVISI),cp_NullValue(_read_.PAFLVISI))
      use
      if i_Rows=0
        this.w_PAVISTRC = " "
        this.w_PAFLVISI = "L"
      endif
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_DPTIPRIS = IIF(VARTYPE(this.w_DPTIPRIS)<>"C" , "T", this.w_DPTIPRIS)
    this.w_DPTIPRIS = IIF(EMPTY(this.w_DPTIPRIS), "T", this.w_DPTIPRIS)
    * --- Delete from GSAR2BIA
    i_nConn=i_TableProp[this.GSAR2BIA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GSAR2BIA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DPCODUTE = "+cp_ToStrODBC(i_CODUTE);
             )
    else
      delete from (i_cTable) where;
            DPCODUTE = i_CODUTE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if this.w_PAFLVISI<>"L"
      this.w_DPTIPRIS = "T"
      this.w_FLTPART = readdipend( i_CodUte, "C")
      if EMPTY(this.w_FLTPART)
        this.w_FLTPART = "#@ZA�"
        this.w_FLTRIS = "#@ZA�"
      else
        * --- Insert into GSAR2BIA
        i_nConn=i_TableProp[this.GSAR2BIA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GSAR2BIA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\gsar1msr",this.GSAR2BIA_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    else
      * --- Insert into GSAR2BIA
      i_nConn=i_TableProp[this.GSAR2BIA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GSAR2BIA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\gsar2msr",this.GSAR2BIA_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endproc


  proc Init(oParentObject,w_DPTIPRIS,w_CODPART,w_GRUPART,w_FLESGR,w_CURPART)
    this.w_DPTIPRIS=w_DPTIPRIS
    this.w_CODPART=w_CODPART
    this.w_GRUPART=w_GRUPART
    this.w_FLESGR=w_FLESGR
    this.w_CURPART=w_CURPART
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='GSAR2BIA'
    this.cWorkTables[2]='PAR_AGEN'
    this.cWorkTables[3]='DIPENDEN'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_DPTIPRIS,w_CODPART,w_GRUPART,w_FLESGR,w_CURPART"
endproc
