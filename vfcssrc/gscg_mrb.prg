* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mrb                                                        *
*              Voci di raccordo sd�-Basilea2                                   *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_98]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-16                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gscg_mrb
PARAMETERS pTipGes
* --- Fine Area Manuale
return(createobject("tgscg_mrb"))

* --- Class definition
define class tgscg_mrb as StdTrsForm
  Top    = 3
  Left   = 9

  * --- Standard Properties
  Width  = 787
  Height = 370+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=113911145
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  RACCBILA_IDX = 0
  RACDBILA_IDX = 0
  CONTI_IDX = 0
  MASTRI_IDX = 0
  VOCIRICL_IDX = 0
  TIR_DETT_IDX = 0
  cFile = "RACCBILA"
  cFileDetail = "RACDBILA"
  cKeySelect = "VBGRUPPO,VBMASTRO,VB_CONTO,VB__VOCE,VBNATURA"
  cKeyWhere  = "VBGRUPPO=this.w_VBGRUPPO and VBMASTRO=this.w_VBMASTRO and VB_CONTO=this.w_VB_CONTO and VB__VOCE=this.w_VB__VOCE and VBNATURA=this.w_VBNATURA"
  cKeyDetail  = "VBGRUPPO=this.w_VBGRUPPO and VBMASTRO=this.w_VBMASTRO and VB_CONTO=this.w_VB_CONTO and VB__VOCE=this.w_VB__VOCE and VBNATURA=this.w_VBNATURA"
  cKeyWhereODBC = '"VBGRUPPO="+cp_ToStrODBC(this.w_VBGRUPPO)';
      +'+" and VBMASTRO="+cp_ToStrODBC(this.w_VBMASTRO)';
      +'+" and VB_CONTO="+cp_ToStrODBC(this.w_VB_CONTO)';
      +'+" and VB__VOCE="+cp_ToStrODBC(this.w_VB__VOCE)';
      +'+" and VBNATURA="+cp_ToStrODBC(this.w_VBNATURA)';

  cKeyDetailWhereODBC = '"VBGRUPPO="+cp_ToStrODBC(this.w_VBGRUPPO)';
      +'+" and VBMASTRO="+cp_ToStrODBC(this.w_VBMASTRO)';
      +'+" and VB_CONTO="+cp_ToStrODBC(this.w_VB_CONTO)';
      +'+" and VB__VOCE="+cp_ToStrODBC(this.w_VB__VOCE)';
      +'+" and VBNATURA="+cp_ToStrODBC(this.w_VBNATURA)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"RACDBILA.VBGRUPPO="+cp_ToStrODBC(this.w_VBGRUPPO)';
      +'+" and RACDBILA.VBMASTRO="+cp_ToStrODBC(this.w_VBMASTRO)';
      +'+" and RACDBILA.VB_CONTO="+cp_ToStrODBC(this.w_VB_CONTO)';
      +'+" and RACDBILA.VB__VOCE="+cp_ToStrODBC(this.w_VB__VOCE)';
      +'+" and RACDBILA.VBNATURA="+cp_ToStrODBC(this.w_VBNATURA)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'RACDBILA.CPROWORD'
  cPrg = "gscg_mrb"
  cComment = "Voci di raccordo sd�-Basilea2"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPGES = space(1)
  w_VBGRUPPO = space(15)
  w_VBMASTRO = space(15)
  w_VB_CONTO = space(15)
  w_VB__VOCE = space(20)
  w_CODICE = space(65)
  w_VBESPDET = space(1)
  o_VBESPDET = space(1)
  w_VBNATURA = space(1)
  w_VBDESCRI = space(70)
  w_VBPARTEC = space(1)
  w_VB___NOTE = space(0)
  w_CPROWORD = 0
  w_VB__TIPO = space(1)
  o_VB__TIPO = space(1)
  w_VBCODCON = space(15)
  w_DESCRICON = space(40)
  o_DESCRICON = space(40)
  w_VBCODMAS = space(15)
  w_VBCODVOC = space(15)
  w_VBROWORD = 0
  w_DESVOCE = space(50)
  o_DESVOCE = space(50)
  w_VBFORMUL = space(30)
  w_VB_SEGNO = 0
  w_VBCONDIZ = space(1)
  w_DES_PIEDE = space(50)
  w_DESCRIMAS = space(40)
  o_DESCRIMAS = space(40)
  w_OBTEST = space(10)
  w_VBTIPVOC = space(1)
  w_LIVELLO = 0
  w_TIPO = space(1)
  w_VBTIPESP = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_mrb
  * ---- Parametro Gestione
   * ---- B: Basilea2
   * ---- M: Fisco azienda
  pTipGes=' '
  * ---- Verifica Formale Formula
  * ---- Costruisce una stringa concatendando uno
  * ---- con il parametro e svolge una macro
  * ---- verifica che il tipo � numerico
  * ---- restituisce una stringa con il msg di errore o tutto Ok..
  Function Is_Correct(Formula)
  Local Old_Err,cTmp,b_err,nTest,cMsg, nUno,param2
  cMsg='Formula [%1] corretta'
  Old_Err=On('Error')
  b_err=.f.
  On Error b_err=.t.
  * --- la formula � ottenuta concatenando importo e formula
  * --- la testo con una variabile posta ad uno
  nUno=1
  cTmp='nUno '+Formula
  nTest=&cTmp
  If b_err
   cMsg='Formula [%1] scorretta %2'
   param2=Message()
  Else
  *--- anteponendo 1 non � possibile che il risultato, se corretto
  *--- sia diverso da N, lascio ugualmente..
   If Vartype(nTest)<>'N'
     cMsg='Formula [%1] scorretta non di tipo numerico [%2]'
     param2=Vartype(nTest)
   endif
  Endif
  
  * Ripristino precedente gestione errori
  On Error &Old_Err
  ah_ErrorMsg(cMsg,,'', alltrim(Formula),param2)
  EndFunc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RACCBILA','gscg_mrb')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mrbPag1","gscg_mrb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Voce")
      .Pages(1).HelpContextID = 106856618
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVB__VOCE_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gscg_mrb
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       IF Type('pTipGes')='U' Or EMPTY(pTipGes)
          .pTipges = 'B'
       ELSE
          .pTipges = pTipges
       ENDIF
    
       DO CASE
             CASE .pTipGes = 'B'
                   .cComment = Ah_Msgformat('Voci di raccordo sd�-Basilea2')
                   .cAutoZoom = 'GSCG_MRB'
             CASE .pTipGes = 'M'
                   .cComment = Ah_Msgformat('Voci di raccordo fisco azienda')
                   .cAutoZoom = 'GSCGMMRB'
             CASE .pTipGes = 'O'
                   .cComment = Ah_Msgformat('Voci di raccordo Bilancio e Oltre')
                   .cAutoZoom = 'GSCGOMRB'
       ENDCASE
    ENDWITH
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='VOCIRICL'
    this.cWorkTables[4]='TIR_DETT'
    this.cWorkTables[5]='RACCBILA'
    this.cWorkTables[6]='RACDBILA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RACCBILA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RACCBILA_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_VBGRUPPO = NVL(VBGRUPPO,space(15))
      .w_VBMASTRO = NVL(VBMASTRO,space(15))
      .w_VB_CONTO = NVL(VB_CONTO,space(15))
      .w_VB__VOCE = NVL(VB__VOCE,space(20))
      .w_VBNATURA = NVL(VBNATURA,space(1))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from RACCBILA where VBGRUPPO=KeySet.VBGRUPPO
    *                            and VBMASTRO=KeySet.VBMASTRO
    *                            and VB_CONTO=KeySet.VB_CONTO
    *                            and VB__VOCE=KeySet.VB__VOCE
    *                            and VBNATURA=KeySet.VBNATURA
    *
    i_nConn = i_TableProp[this.RACCBILA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RACCBILA_IDX,2],this.bLoadRecFilter,this.RACCBILA_IDX,"gscg_mrb")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RACCBILA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RACCBILA.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"RACDBILA.","RACCBILA.")
      i_cTable = i_cTable+' RACCBILA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'VBGRUPPO',this.w_VBGRUPPO  ,'VBMASTRO',this.w_VBMASTRO  ,'VB_CONTO',this.w_VB_CONTO  ,'VB__VOCE',this.w_VB__VOCE  ,'VBNATURA',this.w_VBNATURA  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_OBTEST = i_DATSYS
        .w_TIPGES = this.pTipGes
        .w_VBGRUPPO = NVL(VBGRUPPO,space(15))
        .w_VBMASTRO = NVL(VBMASTRO,space(15))
        .w_VB_CONTO = NVL(VB_CONTO,space(15))
        .w_VB__VOCE = NVL(VB__VOCE,space(20))
        .w_CODICE = Alltrim(.w_VBGRUPPO) +'.' +Alltrim(.w_VBMASTRO) +'.' + Alltrim(.w_VB_CONTO) +'.' + Alltrim(.w_VB__VOCE)
        .w_VBESPDET = NVL(VBESPDET,space(1))
        .w_VBNATURA = NVL(VBNATURA,space(1))
        .w_VBDESCRI = NVL(VBDESCRI,space(70))
        .w_VBPARTEC = NVL(VBPARTEC,space(1))
        .w_VB___NOTE = NVL(VB___NOTE,space(0))
        .w_VBTIPESP = NVL(VBTIPESP,space(1))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'RACCBILA')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from RACDBILA where VBGRUPPO=KeySet.VBGRUPPO
      *                            and VBMASTRO=KeySet.VBMASTRO
      *                            and VB_CONTO=KeySet.VB_CONTO
      *                            and VB__VOCE=KeySet.VB__VOCE
      *                            and VBNATURA=KeySet.VBNATURA
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.RACDBILA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RACDBILA_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('RACDBILA')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "RACDBILA.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" RACDBILA"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'VBGRUPPO',this.w_VBGRUPPO  ,'VBMASTRO',this.w_VBMASTRO  ,'VB_CONTO',this.w_VB_CONTO  ,'VB__VOCE',this.w_VB__VOCE  ,'VBNATURA',this.w_VBNATURA  )
        select * from (i_cTable) RACDBILA where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCRICON = space(40)
          .w_DESVOCE = space(50)
          .w_DESCRIMAS = space(40)
          .w_LIVELLO = 0
        .w_TIPO = ''
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_VB__TIPO = NVL(VB__TIPO,space(1))
          .w_VBCODCON = NVL(VBCODCON,space(15))
          if link_2_3_joined
            this.w_VBCODCON = NVL(ANCODICE203,NVL(this.w_VBCODCON,space(15)))
            this.w_DESCRICON = NVL(ANDESCRI203,space(40))
          else
          .link_2_3('Load')
          endif
          .w_VBCODMAS = NVL(VBCODMAS,space(15))
          if link_2_5_joined
            this.w_VBCODMAS = NVL(MCCODICE205,NVL(this.w_VBCODMAS,space(15)))
            this.w_DESCRIMAS = NVL(MCDESCRI205,space(40))
            this.w_LIVELLO = NVL(MCNUMLIV205,0)
          else
          .link_2_5('Load')
          endif
          .w_VBCODVOC = NVL(VBCODVOC,space(15))
          .w_VBROWORD = NVL(VBROWORD,0)
          if link_2_7_joined
            this.w_VBROWORD = NVL(CPROWNUM207,NVL(this.w_VBROWORD,0))
            this.w_DESVOCE = NVL(TRDESDET207,space(50))
          else
          .link_2_7('Load')
          endif
          .w_VBFORMUL = NVL(VBFORMUL,space(30))
          .w_VB_SEGNO = NVL(VB_SEGNO,0)
          .w_VBCONDIZ = NVL(VBCONDIZ,space(1))
        .w_DES_PIEDE = iif( .w_VB__TIPO='V' , .w_DESVOCE , iif( .w_VB__TIPO='M' , .w_DESCRIMAS ,  .w_DESCRICON ) )
          .w_VBTIPVOC = NVL(VBTIPVOC,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_TIPGES = this.pTipGes
        .w_CODICE = Alltrim(.w_VBGRUPPO) +'.' +Alltrim(.w_VBMASTRO) +'.' + Alltrim(.w_VB_CONTO) +'.' + Alltrim(.w_VB__VOCE)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_17.enabled = .oPgFrm.Page1.oPag.oBtn_1_17.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_19.enabled = .oPgFrm.Page1.oPag.oBtn_1_19.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TIPGES=space(1)
      .w_VBGRUPPO=space(15)
      .w_VBMASTRO=space(15)
      .w_VB_CONTO=space(15)
      .w_VB__VOCE=space(20)
      .w_CODICE=space(65)
      .w_VBESPDET=space(1)
      .w_VBNATURA=space(1)
      .w_VBDESCRI=space(70)
      .w_VBPARTEC=space(1)
      .w_VB___NOTE=space(0)
      .w_CPROWORD=10
      .w_VB__TIPO=space(1)
      .w_VBCODCON=space(15)
      .w_DESCRICON=space(40)
      .w_VBCODMAS=space(15)
      .w_VBCODVOC=space(15)
      .w_VBROWORD=0
      .w_DESVOCE=space(50)
      .w_VBFORMUL=space(30)
      .w_VB_SEGNO=0
      .w_VBCONDIZ=space(1)
      .w_DES_PIEDE=space(50)
      .w_DESCRIMAS=space(40)
      .w_OBTEST=space(10)
      .w_VBTIPVOC=space(1)
      .w_LIVELLO=0
      .w_TIPO=space(1)
      .w_VBTIPESP=space(1)
      if .cFunction<>"Filter"
        .w_TIPGES = this.pTipGes
        .DoRTCalc(2,5,.f.)
        .w_CODICE = Alltrim(.w_VBGRUPPO) +'.' +Alltrim(.w_VBMASTRO) +'.' + Alltrim(.w_VB_CONTO) +'.' + Alltrim(.w_VB__VOCE)
        .w_VBESPDET = 'S'
        .w_VBNATURA = 'A'
        .DoRTCalc(9,9,.f.)
        .w_VBPARTEC = 'S'
        .DoRTCalc(11,14,.f.)
        if not(empty(.w_VBCODCON))
         .link_2_3('Full')
        endif
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_VBCODMAS))
         .link_2_5('Full')
        endif
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_VBROWORD))
         .link_2_7('Full')
        endif
        .DoRTCalc(19,19,.f.)
        .w_VBFORMUL = Space(30)
        .w_VB_SEGNO = 1
        .w_VBCONDIZ = 'N'
        .w_DES_PIEDE = iif( .w_VB__TIPO='V' , .w_DESVOCE , iif( .w_VB__TIPO='M' , .w_DESCRIMAS ,  .w_DESCRICON ) )
        .DoRTCalc(24,24,.f.)
        .w_OBTEST = i_DATSYS
        .w_VBTIPVOC = IIF( g_APPLICATION <> "ADHOC REVOLUTION", 'M' , ' ' )
        .DoRTCalc(27,27,.f.)
        .w_TIPO = ''
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'RACCBILA')
    this.DoRTCalc(29,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oVB__VOCE_1_5.enabled = !i_bVal
      .Page1.oPag.oVBNATURA_1_9.enabled = !i_bVal
      .Page1.oPag.oVBESPDET_1_8.enabled = i_bVal
      .Page1.oPag.oVBPARTEC_1_13.enabled = i_bVal
      .Page1.oPag.oVB___NOTE_1_14.enabled = i_bVal
      .Page1.oPag.oBtn_1_17.enabled = .Page1.oPag.oBtn_1_17.mCond()
      .Page1.oPag.oBtn_1_19.enabled = .Page1.oPag.oBtn_1_19.mCond()
      .Page1.oPag.oObj_1_22.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'RACCBILA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gscg_mrb
    * --- Disabilito anche la combo natura...
    Local L_Ogg
    L_Ogg=this.GetCtrl( 'w_VBNATURA' )
    L_Ogg.Enabled=.f.
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RACCBILA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VBGRUPPO,"VBGRUPPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VBMASTRO,"VBMASTRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VB_CONTO,"VB_CONTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VB__VOCE,"VB__VOCE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VBESPDET,"VBESPDET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VBNATURA,"VBNATURA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VBDESCRI,"VBDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VBPARTEC,"VBPARTEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VB___NOTE,"VB___NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VBTIPESP,"VBTIPESP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RACCBILA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RACCBILA_IDX,2])
    i_lTable = "RACCBILA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RACCBILA_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        GSAR_BF2(this,this.w_TIPGES,"GSCG_KSV")
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_VB__TIPO N(3);
      ,t_VBCODCON C(15);
      ,t_VBCODMAS C(15);
      ,t_DESVOCE C(50);
      ,t_VBFORMUL C(30);
      ,t_VB_SEGNO N(3);
      ,t_VBCONDIZ N(3);
      ,t_DES_PIEDE C(50);
      ,CPROWNUM N(10);
      ,t_DESCRICON C(40);
      ,t_VBCODVOC C(15);
      ,t_VBROWORD N(5);
      ,t_DESCRIMAS C(40);
      ,t_VBTIPVOC C(1);
      ,t_LIVELLO N(1);
      ,t_TIPO C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mrbbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVB__TIPO_2_2.controlsource=this.cTrsName+'.t_VB__TIPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVBCODCON_2_3.controlsource=this.cTrsName+'.t_VBCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVBCODMAS_2_5.controlsource=this.cTrsName+'.t_VBCODMAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESVOCE_2_8.controlsource=this.cTrsName+'.t_DESVOCE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVBFORMUL_2_10.controlsource=this.cTrsName+'.t_VBFORMUL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVB_SEGNO_2_11.controlsource=this.cTrsName+'.t_VB_SEGNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVBCONDIZ_2_12.controlsource=this.cTrsName+'.t_VBCONDIZ'
    this.oPgFRm.Page1.oPag.oDES_PIEDE_2_13.controlsource=this.cTrsName+'.t_DES_PIEDE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(40)
    this.AddVLine(123)
    this.AddVLine(245)
    this.AddVLine(366)
    this.AddVLine(527)
    this.AddVLine(646)
    this.AddVLine(687)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RACCBILA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RACCBILA_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RACCBILA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RACCBILA')
        i_extval=cp_InsertValODBCExtFlds(this,'RACCBILA')
        local i_cFld
        i_cFld=" "+;
                  "(VBGRUPPO,VBMASTRO,VB_CONTO,VB__VOCE,VBESPDET"+;
                  ",VBNATURA,VBDESCRI,VBPARTEC,VB___NOTE,VBTIPESP"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_VBGRUPPO)+;
                    ","+cp_ToStrODBC(this.w_VBMASTRO)+;
                    ","+cp_ToStrODBC(this.w_VB_CONTO)+;
                    ","+cp_ToStrODBC(this.w_VB__VOCE)+;
                    ","+cp_ToStrODBC(this.w_VBESPDET)+;
                    ","+cp_ToStrODBC(this.w_VBNATURA)+;
                    ","+cp_ToStrODBC(this.w_VBDESCRI)+;
                    ","+cp_ToStrODBC(this.w_VBPARTEC)+;
                    ","+cp_ToStrODBC(this.w_VB___NOTE)+;
                    ","+cp_ToStrODBC(this.w_VBTIPESP)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RACCBILA')
        i_extval=cp_InsertValVFPExtFlds(this,'RACCBILA')
        cp_CheckDeletedKey(i_cTable,0,'VBGRUPPO',this.w_VBGRUPPO,'VBMASTRO',this.w_VBMASTRO,'VB_CONTO',this.w_VB_CONTO,'VB__VOCE',this.w_VB__VOCE,'VBNATURA',this.w_VBNATURA)
        INSERT INTO (i_cTable);
              (VBGRUPPO,VBMASTRO,VB_CONTO,VB__VOCE,VBESPDET,VBNATURA,VBDESCRI,VBPARTEC,VB___NOTE,VBTIPESP &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_VBGRUPPO;
                  ,this.w_VBMASTRO;
                  ,this.w_VB_CONTO;
                  ,this.w_VB__VOCE;
                  ,this.w_VBESPDET;
                  ,this.w_VBNATURA;
                  ,this.w_VBDESCRI;
                  ,this.w_VBPARTEC;
                  ,this.w_VB___NOTE;
                  ,this.w_VBTIPESP;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RACDBILA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RACDBILA_IDX,2])
      *
      * insert into RACDBILA
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(VBGRUPPO,VBMASTRO,VB_CONTO,VB__VOCE,VBNATURA"+;
                  ",CPROWORD,VB__TIPO,VBCODCON,VBCODMAS,VBCODVOC"+;
                  ",VBROWORD,VBFORMUL,VB_SEGNO,VBCONDIZ,VBTIPVOC,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_VBGRUPPO)+","+cp_ToStrODBC(this.w_VBMASTRO)+","+cp_ToStrODBC(this.w_VB_CONTO)+","+cp_ToStrODBC(this.w_VB__VOCE)+","+cp_ToStrODBC(this.w_VBNATURA)+;
             ","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_VB__TIPO)+","+cp_ToStrODBCNull(this.w_VBCODCON)+","+cp_ToStrODBCNull(this.w_VBCODMAS)+","+cp_ToStrODBC(this.w_VBCODVOC)+;
             ","+cp_ToStrODBCNull(this.w_VBROWORD)+","+cp_ToStrODBC(this.w_VBFORMUL)+","+cp_ToStrODBC(this.w_VB_SEGNO)+","+cp_ToStrODBC(this.w_VBCONDIZ)+","+cp_ToStrODBC(this.w_VBTIPVOC)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'VBGRUPPO',this.w_VBGRUPPO,'VBMASTRO',this.w_VBMASTRO,'VB_CONTO',this.w_VB_CONTO,'VB__VOCE',this.w_VB__VOCE,'VBNATURA',this.w_VBNATURA)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_VBGRUPPO,this.w_VBMASTRO,this.w_VB_CONTO,this.w_VB__VOCE,this.w_VBNATURA"+;
                ",this.w_CPROWORD,this.w_VB__TIPO,this.w_VBCODCON,this.w_VBCODMAS,this.w_VBCODVOC"+;
                ",this.w_VBROWORD,this.w_VBFORMUL,this.w_VB_SEGNO,this.w_VBCONDIZ,this.w_VBTIPVOC,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.RACCBILA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RACCBILA_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update RACCBILA
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'RACCBILA')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " VBESPDET="+cp_ToStrODBC(this.w_VBESPDET)+;
             ",VBDESCRI="+cp_ToStrODBC(this.w_VBDESCRI)+;
             ",VBPARTEC="+cp_ToStrODBC(this.w_VBPARTEC)+;
             ",VB___NOTE="+cp_ToStrODBC(this.w_VB___NOTE)+;
             ",VBTIPESP="+cp_ToStrODBC(this.w_VBTIPESP)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'RACCBILA')
          i_cWhere = cp_PKFox(i_cTable  ,'VBGRUPPO',this.w_VBGRUPPO  ,'VBMASTRO',this.w_VBMASTRO  ,'VB_CONTO',this.w_VB_CONTO  ,'VB__VOCE',this.w_VB__VOCE  ,'VBNATURA',this.w_VBNATURA  )
          UPDATE (i_cTable) SET;
              VBESPDET=this.w_VBESPDET;
             ,VBDESCRI=this.w_VBDESCRI;
             ,VBPARTEC=this.w_VBPARTEC;
             ,VB___NOTE=this.w_VB___NOTE;
             ,VBTIPESP=this.w_VBTIPESP;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) And Not Empty( Nvl( t_VBCODCON , '' ) + Nvl( t_VBCODMAS , '' ) +  t_DESVOCE )) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.RACDBILA_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.RACDBILA_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from RACDBILA
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update RACDBILA
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",VB__TIPO="+cp_ToStrODBC(this.w_VB__TIPO)+;
                     ",VBCODCON="+cp_ToStrODBCNull(this.w_VBCODCON)+;
                     ",VBCODMAS="+cp_ToStrODBCNull(this.w_VBCODMAS)+;
                     ",VBCODVOC="+cp_ToStrODBC(this.w_VBCODVOC)+;
                     ",VBROWORD="+cp_ToStrODBCNull(this.w_VBROWORD)+;
                     ",VBFORMUL="+cp_ToStrODBC(this.w_VBFORMUL)+;
                     ",VB_SEGNO="+cp_ToStrODBC(this.w_VB_SEGNO)+;
                     ",VBCONDIZ="+cp_ToStrODBC(this.w_VBCONDIZ)+;
                     ",VBTIPVOC="+cp_ToStrODBC(this.w_VBTIPVOC)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,VB__TIPO=this.w_VB__TIPO;
                     ,VBCODCON=this.w_VBCODCON;
                     ,VBCODMAS=this.w_VBCODMAS;
                     ,VBCODVOC=this.w_VBCODVOC;
                     ,VBROWORD=this.w_VBROWORD;
                     ,VBFORMUL=this.w_VBFORMUL;
                     ,VB_SEGNO=this.w_VB_SEGNO;
                     ,VBCONDIZ=this.w_VBCONDIZ;
                     ,VBTIPVOC=this.w_VBTIPVOC;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) And Not Empty( Nvl( t_VBCODCON , '' ) + Nvl( t_VBCODMAS , '' ) +  t_DESVOCE )) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.RACDBILA_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.RACDBILA_IDX,2])
        *
        * delete RACDBILA
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.RACCBILA_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.RACCBILA_IDX,2])
        *
        * delete RACCBILA
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) And Not Empty( Nvl( t_VBCODCON , '' ) + Nvl( t_VBCODMAS , '' ) +  t_DESVOCE )) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RACCBILA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RACCBILA_IDX,2])
    if i_bUpd
      with this
          .w_TIPGES = this.pTipGes
        .DoRTCalc(2,5,.t.)
          .w_CODICE = Alltrim(.w_VBGRUPPO) +'.' +Alltrim(.w_VBMASTRO) +'.' + Alltrim(.w_VB_CONTO) +'.' + Alltrim(.w_VB__VOCE)
        .DoRTCalc(7,17,.t.)
          .link_2_7('Full')
        .DoRTCalc(19,19,.t.)
        if .o_VBESPDET<>.w_VBESPDET
          .w_VBFORMUL = Space(30)
        endif
        if .o_VBESPDET<>.w_VBESPDET
          .w_VB_SEGNO = 1
        endif
        .DoRTCalc(22,22,.t.)
        if .o_VB__TIPO<>.w_VB__TIPO.or. .o_DESVOCE<>.w_DESVOCE.or. .o_DESCRIMAS<>.w_DESCRIMAS.or. .o_DESCRICON<>.w_DESCRICON
          .w_DES_PIEDE = iif( .w_VB__TIPO='V' , .w_DESVOCE , iif( .w_VB__TIPO='M' , .w_DESCRIMAS ,  .w_DESCRICON ) )
        endif
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(24,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESCRICON with this.w_DESCRICON
      replace t_VBCODVOC with this.w_VBCODVOC
      replace t_VBROWORD with this.w_VBROWORD
      replace t_DESCRIMAS with this.w_DESCRIMAS
      replace t_VBTIPVOC with this.w_VBTIPVOC
      replace t_LIVELLO with this.w_LIVELLO
      replace t_TIPO with this.w_TIPO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_MMXTXUVHKC()
    with this
          * --- Azzerro tutto al cambio tipo
          .w_VBCODMAS = Space(15)
          .w_VBROWORD = 0
          .w_VBCONDIZ = 'N'
          .w_DESVOCE = ''
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVBCODCON_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVBCODCON_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVBCODMAS_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVBCODMAS_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVBFORMUL_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVBFORMUL_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVB_SEGNO_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVB_SEGNO_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVBCONDIZ_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVBCONDIZ_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oBtn_2_9.enabled =this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oBtn_2_9.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oVB__VOCE_1_5.visible=!this.oPgFrm.Page1.oPag.oVB__VOCE_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCODICE_1_7.visible=!this.oPgFrm.Page1.oPag.oCODICE_1_7.mHide()
    this.oPgFrm.Page1.oPag.oVBESPDET_1_8.visible=!this.oPgFrm.Page1.oPag.oVBESPDET_1_8.mHide()
    this.oPgFrm.Page1.oPag.oVBNATURA_1_9.visible=!this.oPgFrm.Page1.oPag.oVBNATURA_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_17.visible=!this.oPgFrm.Page1.oPag.oBtn_1_17.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_19.visible=!this.oPgFrm.Page1.oPag.oBtn_1_19.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_VB__TIPO Changed")
          .Calculate_MMXTXUVHKC()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=VBCODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VBCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_VBCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_VB__TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_VB__TIPO;
                     ,'ANCODICE',trim(this.w_VBCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VBCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_VBCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_VB__TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_VBCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_VB__TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VBCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oVBCODCON_2_3'),i_cWhere,'GSAR_BZC',"Elenco conti",'GSCG_MRB.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_VB__TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_VB__TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VBCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_VBCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_VB__TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_VB__TIPO;
                       ,'ANCODICE',this.w_VBCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VBCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRICON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_VBCODCON = space(15)
      endif
      this.w_DESCRICON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VBCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ANCODICE as ANCODICE203"+ ",link_2_3.ANDESCRI as ANDESCRI203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on RACDBILA.VBCODCON=link_2_3.ANCODICE"+" and RACDBILA.VB__TIPO=link_2_3.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and RACDBILA.VBCODCON=link_2_3.ANCODICE(+)"'+'+" and RACDBILA.VB__TIPO=link_2_3.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VBCODMAS
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VBCODMAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_VBCODMAS)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_VBCODMAS))
          select MCCODICE,MCDESCRI,MCNUMLIV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VBCODMAS)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_VBCODMAS)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_VBCODMAS)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VBCODMAS) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oVBCODMAS_2_5'),i_cWhere,'GSAR_AMC',"Elenco mastri",'GSAR_SSC.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VBCODMAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_VBCODMAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_VBCODMAS)
            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VBCODMAS = NVL(_Link_.MCCODICE,space(15))
      this.w_DESCRIMAS = NVL(_Link_.MCDESCRI,space(40))
      this.w_LIVELLO = NVL(_Link_.MCNUMLIV,0)
    else
      if i_cCtrl<>'Load'
        this.w_VBCODMAS = space(15)
      endif
      this.w_DESCRIMAS = space(40)
      this.w_LIVELLO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LIVELLO<=1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Mastro inesistente o non di livello 1")
        endif
        this.w_VBCODMAS = space(15)
        this.w_DESCRIMAS = space(40)
        this.w_LIVELLO = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VBCODMAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.MCCODICE as MCCODICE205"+ ",link_2_5.MCDESCRI as MCDESCRI205"+ ",link_2_5.MCNUMLIV as MCNUMLIV205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on RACDBILA.VBCODMAS=link_2_5.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and RACDBILA.VBCODMAS=link_2_5.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VBROWORD
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIR_DETT_IDX,3]
    i_lTable = "TIR_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIR_DETT_IDX,2], .t., this.TIR_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIR_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VBROWORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VBROWORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,CPROWNUM,TRDESDET";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_VBROWORD);
                   +" and TRCODICE="+cp_ToStrODBC(this.w_VBCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_VBCODVOC;
                       ,'CPROWNUM',this.w_VBROWORD)
            select TRCODICE,CPROWNUM,TRDESDET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VBROWORD = NVL(_Link_.CPROWNUM,0)
      this.w_DESVOCE = NVL(_Link_.TRDESDET,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_VBROWORD = 0
      endif
      this.w_DESVOCE = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIR_DETT_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.TIR_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VBROWORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIR_DETT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIR_DETT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.CPROWNUM as CPROWNUM207"+ ",link_2_7.TRDESDET as TRDESDET207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on RACDBILA.VBROWORD=link_2_7.CPROWNUM"+" and RACDBILA.VBCODVOC=link_2_7.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and RACDBILA.VBROWORD=link_2_7.CPROWNUM(+)"'+'+" and RACDBILA.VBCODVOC=link_2_7.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oVB__VOCE_1_5.value==this.w_VB__VOCE)
      this.oPgFrm.Page1.oPag.oVB__VOCE_1_5.value=this.w_VB__VOCE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_7.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_7.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oVBESPDET_1_8.RadioValue()==this.w_VBESPDET)
      this.oPgFrm.Page1.oPag.oVBESPDET_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVBNATURA_1_9.RadioValue()==this.w_VBNATURA)
      this.oPgFrm.Page1.oPag.oVBNATURA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVBDESCRI_1_11.value==this.w_VBDESCRI)
      this.oPgFrm.Page1.oPag.oVBDESCRI_1_11.value=this.w_VBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oVBPARTEC_1_13.RadioValue()==this.w_VBPARTEC)
      this.oPgFrm.Page1.oPag.oVBPARTEC_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVB___NOTE_1_14.value==this.w_VB___NOTE)
      this.oPgFrm.Page1.oPag.oVB___NOTE_1_14.value=this.w_VB___NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDES_PIEDE_2_13.value==this.w_DES_PIEDE)
      this.oPgFrm.Page1.oPag.oDES_PIEDE_2_13.value=this.w_DES_PIEDE
      replace t_DES_PIEDE with this.oPgFrm.Page1.oPag.oDES_PIEDE_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVB__TIPO_2_2.RadioValue()==this.w_VB__TIPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVB__TIPO_2_2.SetRadio()
      replace t_VB__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVB__TIPO_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCODCON_2_3.value==this.w_VBCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCODCON_2_3.value=this.w_VBCODCON
      replace t_VBCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCODCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCODMAS_2_5.value==this.w_VBCODMAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCODMAS_2_5.value=this.w_VBCODMAS
      replace t_VBCODMAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCODMAS_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESVOCE_2_8.value==this.w_DESVOCE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESVOCE_2_8.value=this.w_DESVOCE
      replace t_DESVOCE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESVOCE_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBFORMUL_2_10.value==this.w_VBFORMUL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBFORMUL_2_10.value=this.w_VBFORMUL
      replace t_VBFORMUL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBFORMUL_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVB_SEGNO_2_11.RadioValue()==this.w_VB_SEGNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVB_SEGNO_2_11.SetRadio()
      replace t_VB_SEGNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVB_SEGNO_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCONDIZ_2_12.RadioValue()==this.w_VBCONDIZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCONDIZ_2_12.SetRadio()
      replace t_VBCONDIZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCONDIZ_2_12.value
    endif
    cp_SetControlsValueExtFlds(this,'RACCBILA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_LIVELLO<=1) and (.w_VB__TIPO='M') and not(empty(.w_VBCODMAS)) and (not(Empty(.w_CPROWORD)) And Not Empty( Nvl( .w_VBCODCON , '' ) + Nvl( .w_VBCODMAS , '' ) +  .w_DESVOCE ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCODMAS_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Mastro inesistente o non di livello 1")
      endcase
      if not(Empty(.w_CPROWORD)) And Not Empty( Nvl( .w_VBCODCON , '' ) + Nvl( .w_VBCODMAS , '' ) +  .w_DESVOCE )
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VBESPDET = this.w_VBESPDET
    this.o_VB__TIPO = this.w_VB__TIPO
    this.o_DESCRICON = this.w_DESCRICON
    this.o_DESVOCE = this.w_DESVOCE
    this.o_DESCRIMAS = this.w_DESCRIMAS
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) And Not Empty( Nvl( t_VBCODCON , '' ) + Nvl( t_VBCODMAS , '' ) +  t_DESVOCE ))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_VB__TIPO=space(1)
      .w_VBCODCON=space(15)
      .w_DESCRICON=space(40)
      .w_VBCODMAS=space(15)
      .w_VBCODVOC=space(15)
      .w_VBROWORD=0
      .w_DESVOCE=space(50)
      .w_VBFORMUL=space(30)
      .w_VB_SEGNO=0
      .w_VBCONDIZ=space(1)
      .w_DES_PIEDE=space(50)
      .w_DESCRIMAS=space(40)
      .w_VBTIPVOC=space(1)
      .w_LIVELLO=0
      .w_TIPO=space(1)
      .DoRTCalc(1,14,.f.)
      if not(empty(.w_VBCODCON))
        .link_2_3('Full')
      endif
      .DoRTCalc(15,16,.f.)
      if not(empty(.w_VBCODMAS))
        .link_2_5('Full')
      endif
      .DoRTCalc(17,18,.f.)
      if not(empty(.w_VBROWORD))
        .link_2_7('Full')
      endif
      .DoRTCalc(19,19,.f.)
        .w_VBFORMUL = Space(30)
        .w_VB_SEGNO = 1
        .w_VBCONDIZ = 'N'
        .w_DES_PIEDE = iif( .w_VB__TIPO='V' , .w_DESVOCE , iif( .w_VB__TIPO='M' , .w_DESCRIMAS ,  .w_DESCRICON ) )
      .DoRTCalc(24,25,.f.)
        .w_VBTIPVOC = IIF( g_APPLICATION <> "ADHOC REVOLUTION", 'M' , ' ' )
      .DoRTCalc(27,27,.f.)
        .w_TIPO = ''
    endwith
    this.DoRTCalc(29,29,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_VB__TIPO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVB__TIPO_2_2.RadioValue(.t.)
    this.w_VBCODCON = t_VBCODCON
    this.w_DESCRICON = t_DESCRICON
    this.w_VBCODMAS = t_VBCODMAS
    this.w_VBCODVOC = t_VBCODVOC
    this.w_VBROWORD = t_VBROWORD
    this.w_DESVOCE = t_DESVOCE
    this.w_VBFORMUL = t_VBFORMUL
    this.w_VB_SEGNO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVB_SEGNO_2_11.RadioValue(.t.)
    this.w_VBCONDIZ = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCONDIZ_2_12.RadioValue(.t.)
    this.w_DES_PIEDE = t_DES_PIEDE
    this.w_DESCRIMAS = t_DESCRIMAS
    this.w_VBTIPVOC = t_VBTIPVOC
    this.w_LIVELLO = t_LIVELLO
    this.w_TIPO = t_TIPO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_VB__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVB__TIPO_2_2.ToRadio()
    replace t_VBCODCON with this.w_VBCODCON
    replace t_DESCRICON with this.w_DESCRICON
    replace t_VBCODMAS with this.w_VBCODMAS
    replace t_VBCODVOC with this.w_VBCODVOC
    replace t_VBROWORD with this.w_VBROWORD
    replace t_DESVOCE with this.w_DESVOCE
    replace t_VBFORMUL with this.w_VBFORMUL
    replace t_VB_SEGNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVB_SEGNO_2_11.ToRadio()
    replace t_VBCONDIZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVBCONDIZ_2_12.ToRadio()
    replace t_DES_PIEDE with this.w_DES_PIEDE
    replace t_DESCRIMAS with this.w_DESCRIMAS
    replace t_VBTIPVOC with this.w_VBTIPVOC
    replace t_LIVELLO with this.w_LIVELLO
    replace t_TIPO with this.w_TIPO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
  func CanAdd()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile aggiungere una voce raccordo manualmente, utilizzare carica salva dati"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgscg_mrbPag1 as StdContainer
  Width  = 783
  height = 373
  stdWidth  = 783
  stdheight = 373
  resizeXpos=415
  resizeYpos=307
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVB__VOCE_1_5 as StdField with uid="RYSVTHBFMS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_VB__VOCE", cQueryName = "VBGRUPPO,VBMASTRO,VB_CONTO,VB__VOCE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 234558107,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=65, Top=14, InputMask=replicate('X',20)

  func oVB__VOCE_1_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES<>'O')
    endwith
    endif
  endfunc

  add object oCODICE_1_7 as StdField with uid="XVKGKTINNP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(65), bMultilanguage =  .f.,;
    ToolTipText = "Codice voce di raccordo (dato dalla composizione dei primi quattro campi chiave)",;
    HelpContextID = 223121882,;
   bGlobalFont=.t.,;
    Height=21, Width=468, Left=65, Top=14, InputMask=replicate('X',65)

  func oCODICE_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES='O')
    endwith
    endif
  endfunc

  add object oVBESPDET_1_8 as StdCheck with uid="DQBUUFGELP",rtseq=7,rtrep=.f.,left=356, top=13, caption="Esportazione dettagliata",;
    HelpContextID = 42824362,;
    cFormVar="w_VBESPDET", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVBESPDET_1_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VBESPDET,&i_cF..t_VBESPDET),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oVBESPDET_1_8.GetRadio()
    this.Parent.oContained.w_VBESPDET = this.RadioValue()
    return .t.
  endfunc

  func oVBESPDET_1_8.ToRadio()
    this.Parent.oContained.w_VBESPDET=trim(this.Parent.oContained.w_VBESPDET)
    return(;
      iif(this.Parent.oContained.w_VBESPDET=='S',1,;
      0))
  endfunc

  func oVBESPDET_1_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oVBESPDET_1_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES<>'O')
    endwith
    endif
  endfunc


  add object oVBNATURA_1_9 as StdCombo with uid="VKALGMWZXQ",rtseq=8,rtrep=.f.,left=645,top=14,width=131,height=21, enabled=.f.;
    , ToolTipText = "Natura voce";
    , HelpContextID = 62653079;
    , cFormVar="w_VBNATURA",RowSource=""+"Attiva,"+"Passiva,"+"Costo,"+"Ricavo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVBNATURA_1_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VBNATURA,&i_cF..t_VBNATURA),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'P',;
    iif(xVal =3,'C',;
    iif(xVal =4,'R',;
    space(1))))))
  endfunc
  func oVBNATURA_1_9.GetRadio()
    this.Parent.oContained.w_VBNATURA = this.RadioValue()
    return .t.
  endfunc

  func oVBNATURA_1_9.ToRadio()
    this.Parent.oContained.w_VBNATURA=trim(this.Parent.oContained.w_VBNATURA)
    return(;
      iif(this.Parent.oContained.w_VBNATURA=='A',1,;
      iif(this.Parent.oContained.w_VBNATURA=='P',2,;
      iif(this.Parent.oContained.w_VBNATURA=='C',3,;
      iif(this.Parent.oContained.w_VBNATURA=='R',4,;
      0)))))
  endfunc

  func oVBNATURA_1_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oVBNATURA_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES='M')
    endwith
    endif
  endfunc

  add object oVBDESCRI_1_11 as StdField with uid="DNBZGGIOYX",rtseq=9,rtrep=.f.,;
    cFormVar = "w_VBDESCRI", cQueryName = "VBDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(70), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione voce di raccordo",;
    HelpContextID = 28271263,;
   bGlobalFont=.t.,;
    Height=21, Width=572, Left=65, Top=43, InputMask=replicate('X',70)


  add object oVBPARTEC_1_13 as StdCombo with uid="OWZWVHDXKO",rtseq=10,rtrep=.f.,left=716,top=43,width=60,height=21;
    , ToolTipText = "Partecipa o meno alla generazione file xml";
    , HelpContextID = 43786905;
    , cFormVar="w_VBPARTEC",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVBPARTEC_1_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VBPARTEC,&i_cF..t_VBPARTEC),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oVBPARTEC_1_13.GetRadio()
    this.Parent.oContained.w_VBPARTEC = this.RadioValue()
    return .t.
  endfunc

  func oVBPARTEC_1_13.ToRadio()
    this.Parent.oContained.w_VBPARTEC=trim(this.Parent.oContained.w_VBPARTEC)
    return(;
      iif(this.Parent.oContained.w_VBPARTEC=='S',1,;
      iif(this.Parent.oContained.w_VBPARTEC=='N',2,;
      0)))
  endfunc

  func oVBPARTEC_1_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oVB___NOTE_1_14 as StdMemo with uid="IGAPFOGSUK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_VB___NOTE", cQueryName = "VB___NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione supplementare",;
    HelpContextID = 41216262,;
   bGlobalFont=.t.,;
    Height=39, Width=711, Left=65, Top=70


  add object oBtn_1_17 as StdButton with uid="CCSELXVSBA",left=709, top=328, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere il bottone per avviare la verifica formale della formula";
    , HelpContextID = 151414199;
    , TabStop=.f.,Caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        Cp_ErrorMsg (  .Is_Correct( .w_VBFORMUL ) )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    with this.Parent.oContained
      return (Not Empty( .w_VBFORMUL ))
    endwith
  endfunc

  func oBtn_1_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="BTGHEPOPZV",left=709, top=328, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere il bottone per avviare la verifica formale della formula";
    , HelpContextID = 151414199;
    , tabstop=.f., caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        .Is_Correct( .w_VBFORMUL )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    with this.Parent.oContained
      return (Not Empty( .w_VBFORMUL ))
    endwith
  endfunc

  func oBtn_1_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
    endwith
   endif
  endfunc


  add object oObj_1_22 as cp_runprogram with uid="HRBMELEKFQ",left=174, top=384, width=237,height=24,;
    caption='GSCG_BEK',;
   bGlobalFont=.t.,;
    prg="gscg_bek",;
    cEvent = "w_VBESPDET Changed",;
    nPag=1;
    , ToolTipText = "Controlli esportazione dettagliata";
    , HelpContextID = 24208049


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=114, width=770,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Riga",Field2="VB__TIPO",Label2="Tipo",Field3="VBCODCON",Label3="Conto",Field4="VBCODMAS",Label4="Mastro",Field5="DESVOCE",Label5="Voce",Field6="VBFORMUL",Label6="Formula",Field7="VB_SEGNO",Label7="+/-",Field8="VBCONDIZ",Label8="Vinc.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202179706

  add object oStr_1_6 as StdString with uid="LNPLCUKOFU",Visible=.t., Left=2, Top=14,;
    Alignment=1, Width=59, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="ARLCESKQJK",Visible=.t., Left=597, Top=14,;
    Alignment=1, Width=40, Height=18,;
    Caption="Natura:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPGES='M')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="IORUFGHEKX",Visible=.t., Left=648, Top=43,;
    Alignment=1, Width=61, Height=18,;
    Caption="Partecipa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="PBQGZPHANJ",Visible=.t., Left=32, Top=338,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KSNPIZGNTK",Visible=.t., Left=2, Top=70,;
    Alignment=1, Width=59, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=133,;
    width=766+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=134,width=765+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONTI|MASTRI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDES_PIEDE_2_13.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oVBCODCON_2_3
      case cFile='MASTRI'
        oDropInto=this.oBodyCol.oRow.oVBCODMAS_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oDES_PIEDE_2_13 as StdTrsField with uid="UZAAIRAOVQ",rtseq=23,rtrep=.t.,;
    cFormVar="w_DES_PIEDE",value=space(50),enabled=.f.,;
    HelpContextID = 127555786,;
    cTotal="", bFixedPos=.t., cQueryName = "DES_PIEDE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Descrizione conto / mastro / voce",;
   bGlobalFont=.t.,;
    Height=21, Width=414, Left=124, Top=336, InputMask=replicate('X',50)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mrbBodyRow as CPBodyRowCnt
  Width=756
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="OTXLPZYMIZ",rtseq=12,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero riga e sequenza di calcolo",;
    HelpContextID = 234508138,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=35, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oVB__TIPO_2_2 as StdTrsCombo with uid="KAARNJVPER",rtrep=.t.,;
    cFormVar="w_VB__TIPO", RowSource=""+"Conto,"+"Mastro,"+"Voce" , ;
    ToolTipText = "Tipo",;
    HelpContextID = 136637787,;
    Height=21, Width=77, Left=39, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oVB__TIPO_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VB__TIPO,&i_cF..t_VB__TIPO),this.value)
    return(iif(xVal =1,'G',;
    iif(xVal =2,'M',;
    iif(xVal =3,'V',;
    space(1)))))
  endfunc
  func oVB__TIPO_2_2.GetRadio()
    this.Parent.oContained.w_VB__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oVB__TIPO_2_2.ToRadio()
    this.Parent.oContained.w_VB__TIPO=trim(this.Parent.oContained.w_VB__TIPO)
    return(;
      iif(this.Parent.oContained.w_VB__TIPO=='G',1,;
      iif(this.Parent.oContained.w_VB__TIPO=='M',2,;
      iif(this.Parent.oContained.w_VB__TIPO=='V',3,;
      0))))
  endfunc

  func oVB__TIPO_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oVB__TIPO_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_VBCODCON)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oVBCODCON_2_3 as StdTrsField with uid="KOMWJBSDLU",rtseq=14,rtrep=.t.,;
    cFormVar="w_VBCODCON",value=space(15),;
    ToolTipText = "Codice conto",;
    HelpContextID = 255241564,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=122, Top=0, cSayPict=[p_CON], cGetPict=[p_CON], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_VB__TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_VBCODCON"

  func oVBCODCON_2_3.mCond()
    with this.Parent.oContained
      return (.w_VB__TIPO='G')
    endwith
  endfunc

  func oVBCODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oVBCODCON_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oVBCODCON_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_VB__TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_VB__TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oVBCODCON_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco conti",'GSCG_MRB.CONTI_VZM',this.parent.oContained
  endproc
  proc oVBCODCON_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_VB__TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_VBCODCON
    i_obj.ecpSave()
  endproc

  add object oVBCODMAS_2_5 as StdTrsField with uid="WGSQMTPEYT",rtseq=16,rtrep=.t.,;
    cFormVar="w_VBCODMAS",value=space(15),;
    ToolTipText = "Codice mastro",;
    HelpContextID = 87469399,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Mastro inesistente o non di livello 1",;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=243, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_VBCODMAS"

  func oVBCODMAS_2_5.mCond()
    with this.Parent.oContained
      return (.w_VB__TIPO='M')
    endwith
  endfunc

  func oVBCODMAS_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oVBCODMAS_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oVBCODMAS_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oVBCODMAS_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Elenco mastri",'GSAR_SSC.MASTRI_VZM',this.parent.oContained
  endproc
  proc oVBCODMAS_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_VBCODMAS
    i_obj.ecpSave()
  endproc

  add object oDESVOCE_2_8 as StdTrsField with uid="WEFPNTRRNZ",rtseq=19,rtrep=.t.,;
    cFormVar="w_DESVOCE",value=space(50),enabled=.f.,;
    ToolTipText = "Descriz. dettaglio",;
    HelpContextID = 25252918,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=147, Left=363, Top=0, InputMask=replicate('X',50)

  add object oBtn_2_9 as StdButton with uid="XWCQXJGXVX",width=18,height=18,;
   left=512, top=2,;
    caption="...", nPag=2;
    , ToolTipText = "Selezionare voce di bilancio";
    , HelpContextID = 113710122;
  , bGlobalFont=.t.

    proc oBtn_2_9.Click()
      do GSCG_KRB with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_9.mCond()
    with this.Parent.oContained
      return (.w_VB__TIPO='V')
    endwith
  endfunc

  add object oVBFORMUL_2_10 as StdTrsField with uid="PZIFOXNTRV",rtseq=20,rtrep=.t.,;
    cFormVar="w_VBFORMUL",value=space(30),;
    ToolTipText = "Eventuale ulteriore formula di elaborazione della riga",;
    HelpContextID = 195658402,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=103, Left=536, Top=0, InputMask=replicate('X',30)

  func oVBFORMUL_2_10.mCond()
    with this.Parent.oContained
      return ((.w_VBESPDET='N' AND .w_TIPGES='O') or .w_TIPGES<>'O')
    endwith
  endfunc

  add object oVB_SEGNO_2_11 as StdTrsCombo with uid="OFEGIHHIOM",rtrep=.t.,;
    cFormVar="w_VB_SEGNO", RowSource=""+"+,"+"-" , ;
    ToolTipText = "Segno da applicare alla riga",;
    HelpContextID = 186707291,;
    Height=21, Width=36, Left=645, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oVB_SEGNO_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VB_SEGNO,&i_cF..t_VB_SEGNO),this.value)
    return(iif(xVal =1,1,;
    iif(xVal =2,-1,;
    0)))
  endfunc
  func oVB_SEGNO_2_11.GetRadio()
    this.Parent.oContained.w_VB_SEGNO = this.RadioValue()
    return .t.
  endfunc

  func oVB_SEGNO_2_11.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_VB_SEGNO==1,1,;
      iif(this.Parent.oContained.w_VB_SEGNO==-1,2,;
      0)))
  endfunc

  func oVB_SEGNO_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oVB_SEGNO_2_11.mCond()
    with this.Parent.oContained
      return (((.w_VBESPDET='N' AND .w_TIPGES='O') or .w_TIPGES<>'O'))
    endwith
  endfunc

  add object oVBCONDIZ_2_12 as StdTrsCombo with uid="JXCVKLHYPC",rtrep=.t.,;
    cFormVar="w_VBCONDIZ", RowSource=""+"No,"+"Dare,"+"Avere" , ;
    ToolTipText = "Vincolo da applicare per riportare o meno la riga nel totale ('D', 'A')",;
    HelpContextID = 40456880,;
    Height=21, Width=65, Left=686, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oVBCONDIZ_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VBCONDIZ,&i_cF..t_VBCONDIZ),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    iif(xVal =3,'A',;
    space(1)))))
  endfunc
  func oVBCONDIZ_2_12.GetRadio()
    this.Parent.oContained.w_VBCONDIZ = this.RadioValue()
    return .t.
  endfunc

  func oVBCONDIZ_2_12.ToRadio()
    this.Parent.oContained.w_VBCONDIZ=trim(this.Parent.oContained.w_VBCONDIZ)
    return(;
      iif(this.Parent.oContained.w_VBCONDIZ=='N',1,;
      iif(this.Parent.oContained.w_VBCONDIZ=='D',2,;
      iif(this.Parent.oContained.w_VBCONDIZ=='A',3,;
      0))))
  endfunc

  func oVBCONDIZ_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oVBCONDIZ_2_12.mCond()
    with this.Parent.oContained
      return (((.w_VBESPDET='N' AND .w_TIPGES='O') or .w_TIPGES<>'O') and .w_VB__TIPO<>'V')
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mrb','RACCBILA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".VBGRUPPO=RACCBILA.VBGRUPPO";
  +" and "+i_cAliasName2+".VBMASTRO=RACCBILA.VBMASTRO";
  +" and "+i_cAliasName2+".VB_CONTO=RACCBILA.VB_CONTO";
  +" and "+i_cAliasName2+".VB__VOCE=RACCBILA.VB__VOCE";
  +" and "+i_cAliasName2+".VBNATURA=RACCBILA.VBNATURA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
