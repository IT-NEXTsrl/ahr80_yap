* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bzm                                                        *
*              M.M./doc. da schede magazzino                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_26]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2013-09-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pNUMRIF,pCODODL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bzm",oParentObject,m.pSERIAL,m.pNUMRIF,m.pCODODL)
return(i_retval)

define class tgsar_bzm as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  pNUMRIF = 0
  pCODODL = space(15)
  w_SERIAL = space(10)
  w_CODODL = space(15)
  w_SERDOC = space(10)
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_TIPDOC = space(5)
  w_PROG = .NULL.
  w_ORDAPE = space(1)
  w_PADRE = .NULL.
  * --- WorkFile variables
  DOC_MAST_idx=0
  MVMSMAST_idx=0
  MVM_MAST_idx=0
  DOCSMAST_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia Movimenti Mag. o Documenti da Visualizzazione Schede Magazzino (da GSMA_SZM)
    * --- anche dalla Disponibilit� nel Tempo (da GSMA_SZD)
    *     Lanciato da GSVE_MFD (Dettaglio fatture differite)
    * --- Questo oggetto sar� definito come Prima Nota o Documento
    do case
      case this.pNUMRIF=-10
        * --- Movimenti Magazzino
        * --- Read from MVM_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MVM_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2],.t.,this.MVM_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MMCODESE"+;
            " from "+i_cTable+" MVM_MAST where ";
                +"MMSERIAL = "+cp_ToStrODBC(this.pSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MMCODESE;
            from (i_cTable) where;
                MMSERIAL = this.pSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_COMPET = NVL(cp_ToDate(_read_.MMCODESE),cp_NullValue(_read_.MMCODESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(w_COMPET)
          this.w_PROG = GSMA_MVM()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PROG.bSec1)
            Ah_ErrorMsg("Impossibile aprire il movimento di magazzino!",48,"")
            i_retcode = 'stop'
            return
          endif
          this.w_PROG.w_MMSERIAL = this.pSERIAL
          this.w_PROG.QueryKeySet("MMSERIAL='"+this.pSERIAL+ "'","")     
          * --- carico il record
          this.w_PROG.LoadRecWarn()     
        else
          * --- Read from MVMSMAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MVMSMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVMSMAST_idx,2],.t.,this.MVMSMAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MMCODESE"+;
              " from "+i_cTable+" MVMSMAST where ";
                  +"MMSERIAL = "+cp_ToStrODBC(this.pSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MMCODESE;
              from (i_cTable) where;
                  MMSERIAL = this.pSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_COMPET = NVL(cp_ToDate(_read_.MMCODESE),cp_NullValue(_read_.MMCODESE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(w_COMPET)
            ah_ErrorMsg("Movimento di magazzino storicizzato","!","")
          else
            ah_ErrorMsg("Movimento di magazzino inesistente","!","")
          endif
        endif
      case this.pNUMRIF=-20 OR this.pNUMRIF=-70
        * --- Documenti
        this.w_SERIAL = this.pSERIAL
        this.w_SERDOC = SPACE(10)
        this.w_FLVEAC = "V"
        this.w_CLADOC = "  "
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVSERIAL,MVFLVEAC,MVCLADOC,MVTIPDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVSERIAL,MVFLVEAC,MVCLADOC,MVTIPDOC;
            from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERDOC = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
          this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
          this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
          this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_SERDOC) OR i_Rows=0
          * --- Read from DOCSMAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOCSMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOCSMAST_idx,2],.t.,this.DOCSMAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVSERIAL,MVFLVEAC,MVCLADOC"+;
              " from "+i_cTable+" DOCSMAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVSERIAL,MVFLVEAC,MVCLADOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_SERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERDOC = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
            this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
            this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if EMPTY(this.w_SERDOC) OR i_Rows=0
            ah_ErrorMsg("Documento inesistente",,"")
          else
            ah_ErrorMsg("Documento storicizzato",,"")
          endif
        else
          if this.w_CLADOC="OR"
            * --- Read from TIP_DOCU
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TDORDAPE"+;
                " from "+i_cTable+" TIP_DOCU where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TDORDAPE;
                from (i_cTable) where;
                    TDTIPDOC = this.w_TIPDOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ORDAPE = NVL(cp_ToDate(_read_.TDORDAPE),cp_NullValue(_read_.TDORDAPE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_ORDAPE="S"
              this.w_PROG = GSOR_MDV("VS")
            else
              this.w_PROG = GSOR_MDV(this.w_FLVEAC)
            endif
          else
            if this.w_FLVEAC="V"
              this.w_PROG = GSVE_MDV(this.w_CLADOC)
            else
              if g_ACQU<>"S" AND NOT this.w_CLADOC $ "DI-DT"
                ah_ErrorMsg("Categoria documento consentita solo dal modulo ciclo acquisti","!","")
                i_retcode = 'stop'
                return
              else
                this.w_PROG = GSAC_MDV(this.w_CLADOC)
              endif
            endif
          endif
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PROG.bSec1)
            Ah_ErrorMsg("Impossibile aprire la funzionalit� richiesta!",48,"")
            i_retcode = 'stop'
            return
          endif
          this.w_PROG.w_MVSERIAL = this.w_SERDOC
          this.w_PROG.QueryKeySet("MVSERIAL='"+this.w_SERDOC+ "'","")     
          * --- carico il record
          this.w_PROG.LoadRecWarn()     
          this.w_PROG.Refresh()     
        endif
        this.w_PADRE = This.oParentObject
        if UPPER(this.w_PADRE.Class)="TCGSVE_MFD"
          if this.w_PADRE.cFunction="Edit"
            * --- Se questo batch � lanciato da Dettaglio fatture DIfferite in Modifica valorizzo la variabile
            *     w_Det_Diff con il seriale del documento selezionato
            this.w_PROG.w_Det_Diff = this.pSerial
          endif
        endif
      case this.pNUMRIF=-30
        * --- POS
        this.w_PROG = GSPS_MVD()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          Ah_ErrorMsg("Impossibile aprire la funzionalit� richiesta!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_MDSERIAL = this.pSERIAL
        this.w_PROG.QueryKeySet("MDSERIAL='"+this.pSERIAL+ "'","")     
        * --- carico il record
        this.w_PROG.LoadRecWarn()     
      case (this.pNUMRIF=-40 OR this.pNUMRIF=-50) AND g_PROD="S"
        * --- ODL/OCL
        this.w_CODODL = this.pCODODL
        GSCO_BOR(this,this.w_CODODL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pNUMRIF=-80
         
 =Opengest("O","GSAG_APR","PRSERIAL",this.pSERIAL)
    endcase
    i_retcode = 'stop'
    i_retval = this.w_PROG
    return
  endproc


  proc Init(oParentObject,pSERIAL,pNUMRIF,pCODODL)
    this.pSERIAL=pSERIAL
    this.pNUMRIF=pNUMRIF
    this.pCODODL=pCODODL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='MVMSMAST'
    this.cWorkTables[3]='MVM_MAST'
    this.cWorkTables[4]='DOCSMAST'
    this.cWorkTables[5]='TIP_DOCU'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pNUMRIF,pCODODL"
endproc
