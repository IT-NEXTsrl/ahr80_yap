* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bem                                                        *
*              Cancella registrazioni contabili                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-02                                                      *
* Last revis.: 2012-06-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bem",oParentObject,m.pParam)
return(i_retval)

define class tgscg_bem as StdBatch
  * --- Local variables
  pParam = space(1)
  w_PADRE = .NULL.
  w_ZOOM = .NULL.
  w_NUMREC = 0
  w_COUNT = 0
  w_LSERIAL = space(10)
  w_NUMINT = 0
  w_PNSERIAL = space(10)
  w_LOGERR = .f.
  w_MESS = space(100)
  w_oERRORLOG = .NULL.
  w_LOGERR = .f.
  w_STATOPN = space(1)
  w_STAMPATO = space(1)
  w_MESS = space(100)
  w_oERRORLOG = .NULL.
  * --- WorkFile variables
  PNT_MAST_idx=0
  CAU_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestisce Eventi della maschera GSCG_KES
    this.w_PADRE = this.oParentObject
    this.w_ZOOM = this.oParentObject.W_ZOOMPN
    do case
      case this.pParam$"SDV"
        if USED(this.w_Zoom.cCursor) And RECCOUNT(this.w_Zoom.cCursor)>0
          if isahe() or isapa()
            SELECT ( this.w_ZOOM.cCursor )
            this.w_NUMREC = RECNO()
            UPDATE (this.w_ZOOM.cCursor) SET XCHK=ICASE(this.pPARAM="S",1,this.pPARAM="D", 0,this.pPARAM="V",IIF(XCHK=1,0,1),XCHK)
            SELECT ( this.w_ZOOM.cCursor )
            COUNT FOR XCHK=1 TO this.w_COUNT
            if this.w_COUNT>0
              this.oParentObject.w_TESTSEL = "S"
            else
              this.oParentObject.w_TESTSEL = "D"
            endif
            GO this.w_NUMREC
          else
            * --- Seleziona/Deseleziona tutto
            ND = this.w_ZOOM.cCursor
            UPDATE (ND) SET XCHK=IIF(this.oParentObject.w_SELEZI="S",1,0)
            this.oParentObject.w_TESTSEL = this.oParentObject.w_SELEZI
            SELECT (ND)
            GOTO 1
          endif
        endif
      case this.pParam="E"
        * --- Lo schedulatore � attivo
        this.w_PADRE.oPgFrm.ActivePage = 2
        this.oParentObject.w_MSG = ""
        AddMsgNL("Inizio elaborazione %1",this,Time())
        cp_SetGlobalVar("g_SCHEDULER","S")
        this.w_NUMINT = iif(isahe() or isapa(),6,15)
        ND = this.w_ZOOM.cCursor
        SELECT PNSERIAL,PNNUMDOC,PNALFDOC,PNDATDOC,PNCODCAU,PNNUMRER,PNDATREG FROM (ND) WHERE xchk=1 ORDER BY PNTIPREG, PNALFPRO, PNNUMPRO DESC, PNALFDOC, PNNUMDOC DESC INTO CURSOR AZKO
        SELECT AZKO
        GO TOP
        SCAN
        cp_SetGlobalVar("g_MSG"," ")
        this.w_LSERIAL = AZKO.PNSERIAL
        GSCG_BZP(this,"ELA",this.w_LSERIAL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Read from PNT_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" PNT_MAST where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_LSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                PNSERIAL = this.w_LSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        cp_SetGlobalVar("g_SCHEDULER","N")
        this.w_MESS = Alltrim(Str(Nvl(PNNUMRER,0))) + " del " + DTOC(cp_todate(PNDATREG))
        if i_ROWS <> 0
           
 AddMsgNL("Cancellazione registrazione N� %5 con causale %4  num. doc. %1%2 del %3 non avvenuta" ,this,Alltrim(Str(Nvl(PNNUMDOC,0),this.w_NUMINT)),iif(empty(Nvl(PNALFDOC," ")),"","/"+Alltrim(Nvl(PNALFDOC," "))),DTOC(cp_todate(PNDATDOC)),Alltrim(PNCODCAU),Alltrim(this.w_MESS))
           
 AddMsg("%1",this,cp_GetGlobalVar("g_MSG"))
          this.w_LOGERR = .t.
        else
           
 AddMsgNL("Cancellazione registrazione N� %5 con  causale %4  num. doc. %1%2 del %3 avvenuta" ,this,Alltrim(Str(Nvl(PNNUMDOC,0),this.w_NUMINT)),iif(empty(Nvl(PNALFDOC," ")),"","/"+Alltrim(Nvl(PNALFDOC," "))),DTOC(cp_todate(PNDATDOC)),Alltrim(PNCODCAU),Alltrim(this.w_MESS))
        endif
        cp_SetGlobalVar("g_SCHEDULER","S")
        ENDSCAN
        cp_SetGlobalVar("g_SCHEDULER","N")
        AddMsgNL("Fine elaborazione %1",this,Time())
        Use in AZKO
        this.w_PADRE.NotifyEvent("Ricerca")     
        if this.w_LOGERR
          this.w_oERRORLOG=createobject("AH_ErrorLog")
          this.w_oERRORLOG.AddMsgLogNoTranslate(this.oParentObject.w_msg)     
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
        endif
        this.w_PADRE = .Null.
      case this.pParam="U"
        SELECT ( this.w_ZOOM.cCursor )
        if USED(this.w_Zoom.cCursor) And RECCOUNT(this.w_Zoom.cCursor)>0
          this.w_NUMREC = RECNO()
          COUNT FOR XCHK=1 TO this.w_COUNT
          if this.w_COUNT>0
            this.oParentObject.w_TESTSEL = "S"
          else
            this.oParentObject.w_TESTSEL = "D"
          endif
          GOTO this.w_NUMREC
        endif
      case this.pParam="I"
        this.w_ZOOM.ccpQueryName = "query\gscg1kes"
        this.w_PADRE.NotifyEvent("Ricerca")     
      case this.pParam="A"
        this.w_LSERIAL = this.oParentObject.w_SERIALE
      case this.pParam="O"
        * --- Lo schedulatore � attivo
        this.w_PADRE.oPgFrm.ActivePage = 2
        this.oParentObject.w_MSG = ""
        AddMsgNL("Inizio elaborazione %1",this,Time())
        if isahr()
          * --- w_STAMPATO vale 'S' se almeno uno dei registri Iva presente nel castelletto Iva risulta avere, nelle attivit�, una data di stampa superiore alla data di registrazione
          cp_SetGlobalVar("g_SCHEDULER","S")
          this.w_NUMINT = 15
          ND = this.w_ZOOM.cCursor
          SELECT PNSERIAL,PNNUMDOC,PNALFDOC,PNDATDOC,PNCODCAU,PNNUMRER,PNDATREG FROM (ND) WHERE xchk=1 ORDER BY PNTIPREG, PNALFPRO, PNNUMPRO DESC, PNALFDOC, PNNUMDOC DESC INTO CURSOR AZKO
          SELECT AZKO
          GO TOP
          SCAN
          cp_SetGlobalVar("g_MSG"," ")
          this.w_STATOPN = "S"
          this.w_STAMPATO = "S"
          this.w_LSERIAL = AZKO.PNSERIAL
          * --- Verifico se per la registrazione corrente � possibile modificare lo stato..
          *     - Controllo se sono gi� stati stampati i registri IVA
          this.w_PNSERIAL = this.w_LSERIAL
          VQ_EXEC("QUERY\GSCG_SRI.VQR",this,"REGSTAMP")
          if used("REGSTAMP")
            this.w_STAMPATO = IIF(""=ALLTRIM(NVL(STAMPATO,"")),"N",STAMPATO)
          endif
          SELECT AZKO
          if this.w_STAMPATO<>"S"
            GSCG_BOP(this,this.w_LSERIAL,"GSCG_MPN()","w_PNFLPROV","PNSERIAL",1)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Read from PNT_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PNFLPROV"+;
                " from "+i_cTable+" PNT_MAST where ";
                    +"PNSERIAL = "+cp_ToStrODBC(this.w_LSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PNFLPROV;
                from (i_cTable) where;
                    PNSERIAL = this.w_LSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_STATOPN = NVL(cp_ToDate(_read_.PNFLPROV),cp_NullValue(_read_.PNFLPROV))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          * --- Chiudo il cursore
          use in select("REGSTAMP")
          cp_SetGlobalVar("g_SCHEDULER","N")
          this.w_MESS = Alltrim(Str(Nvl(PNNUMRER,0))) + " del " + DTOC(cp_todate(PNDATREG))
          if this.w_STATOPN <> "N"
            AddMsgNL("Aggiornamento registrazione N� %5 con causale %4  num. doc. %1%2 del %3 non avvenuto" ,this,Alltrim(Str(Nvl(PNNUMDOC,0),this.w_NUMINT)),iif(empty(Nvl(PNALFDOC," ")),"","/"+Alltrim(Nvl(PNALFDOC," "))),DTOC(cp_todate(PNDATDOC)),Alltrim(PNCODCAU),Alltrim(this.w_MESS))
            if this.w_STAMPATO="S"
              AddMsgNL("Documento con data registrazione antecedente alla data di stampa registri IVA." ,this)
            endif
            AddMsg("%1",this,cp_GetGlobalVar("g_MSG"))
            this.w_LOGERR = .t.
          else
            AddMsgNL("Aggiornamento registrazione N� %5 con  causale %4  num. doc. %1%2 del %3 avvenuto" ,this,Alltrim(Str(Nvl(PNNUMDOC,0),this.w_NUMINT)),iif(empty(Nvl(PNALFDOC," ")),"","/"+Alltrim(Nvl(PNALFDOC," "))),DTOC(cp_todate(PNDATDOC)),Alltrim(PNCODCAU),Alltrim(this.w_MESS))
          endif
          cp_SetGlobalVar("g_SCHEDULER","S")
          ENDSCAN
          cp_SetGlobalVar("g_SCHEDULER","N")
          Use in AZKO
        endif
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        if !this.w_LOGERR AND isahe()
          do GSCG_BMP WITH This.oparentobject,"ESE",this.w_oERRORLOG,this.oParentObject.W_ZOOMPN.cCursor
        endif
        AddMsgNL("Fine elaborazione %1",this,Time())
        if this.w_LOGERR
          this.w_oERRORLOG.AddMsgLogNoTranslate(this.oParentObject.w_msg)     
        endif
        this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
        this.w_PADRE.NotifyEvent("Ricerca")     
        this.w_PADRE = .Null.
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='CAU_CONT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
