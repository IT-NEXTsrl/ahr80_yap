* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kdr                                                        *
*              Dettaglio dati rilevati                                         *
*                                                                              *
*      Author: Zucchetti Tam S.p.A                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_60]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-30                                                      *
* Last revis.: 2007-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kdr",oParentObject))

* --- Class definition
define class tgsma_kdr as StdForm
  Top    = 5
  Left   = 10

  * --- Standard Properties
  Width  = 721
  Height = 366
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-13"
  HelpContextID=82475881
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  MAGAZZIN_IDX = 0
  CODIRILE_IDX = 0
  cPrg = "gsma_kdr"
  cComment = "Dettaglio dati rilevati"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SERIALE = space(10)
  w_KEYSAL = space(40)
  w_KEYULO = space(40)
  w_CODMAT = space(40)
  w_CODMAG = space(5)
  w_CODRIL = space(10)
  w_DESMAG = space(30)
  w_DATARIL = ctod('  /  /  ')
  w_DESCRI = space(40)
  w_ZoomDet = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kdrPag1","gsma_kdr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDet = this.oPgFrm.Pages(1).oPag.ZoomDet
    DoDefault()
    proc Destroy()
      this.w_ZoomDet = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='CODIRILE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SERIALE=space(10)
      .w_KEYSAL=space(40)
      .w_KEYULO=space(40)
      .w_CODMAT=space(40)
      .w_CODMAG=space(5)
      .w_CODRIL=space(10)
      .w_DESMAG=space(30)
      .w_DATARIL=ctod("  /  /  ")
      .w_DESCRI=space(40)
      .w_KEYSAL=oParentObject.w_KEYSAL
      .w_KEYULO=oParentObject.w_KEYULO
      .w_CODMAT=oParentObject.w_CODMAT
      .w_CODMAG=oParentObject.w_CODMAG
      .w_CODRIL=oParentObject.w_CODRIL
      .w_DESMAG=oParentObject.w_DESMAG
      .w_DATARIL=oParentObject.w_DATARIL
      .oPgFrm.Page1.oPag.ZoomDet.Calculate()
        .w_SERIALE = .w_ZoomDet.GetVar('DRSERIAL')
        .DoRTCalc(2,5,.f.)
        if not(empty(.w_CODMAG))
          .link_1_8('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODRIL))
          .link_1_9('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
          .DoRTCalc(7,8,.f.)
        .w_DESCRI = .w_ZoomDet.GETVAR('ARDESART')
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_KEYSAL=.w_KEYSAL
      .oParentObject.w_KEYULO=.w_KEYULO
      .oParentObject.w_CODMAT=.w_CODMAT
      .oParentObject.w_CODMAG=.w_CODMAG
      .oParentObject.w_CODRIL=.w_CODRIL
      .oParentObject.w_DESMAG=.w_DESMAG
      .oParentObject.w_DATARIL=.w_DATARIL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomDet.Calculate()
            .w_SERIALE = .w_ZoomDet.GetVar('DRSERIAL')
        .DoRTCalc(2,4,.t.)
          .link_1_8('Full')
          .link_1_9('Full')
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .DoRTCalc(7,8,.t.)
            .w_DESCRI = .w_ZoomDet.GETVAR('ARDESART')
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDet.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomDet.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRIL
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CODIRILE_IDX,3]
    i_lTable = "CODIRILE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2], .t., this.CODIRILE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDATRIL";
                   +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(this.w_CODRIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',this.w_CODRIL)
            select RICODICE,RIDATRIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRIL = NVL(_Link_.RICODICE,space(10))
      this.w_DATARIL = NVL(cp_ToDate(_Link_.RIDATRIL),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODRIL = space(10)
      endif
      this.w_DATARIL = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])+'\'+cp_ToStr(_Link_.RICODICE,1)
      cp_ShowWarn(i_cKey,this.CODIRILE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_8.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_8.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODRIL_1_9.value==this.w_CODRIL)
      this.oPgFrm.Page1.oPag.oCODRIL_1_9.value=this.w_CODRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_11.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_11.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATARIL_1_13.value==this.w_DATARIL)
      this.oPgFrm.Page1.oPag.oDATARIL_1_13.value=this.w_DATARIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_17.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_17.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_kdrPag1 as StdContainer
  Width  = 717
  height = 366
  stdWidth  = 717
  stdheight = 366
  resizeXpos=372
  resizeYpos=214
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomDet as cp_zoombox with uid="YNJUPZWIWW",left=-4, top=47, width=719,height=265,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="RILEVAZI",cZoomFile="GSMA_KDR",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryonLoad=.f.,bQueryOnDblClick=.f.,bRetriveAllRows=.f.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 95521766


  add object oBtn_1_2 as StdButton with uid="XQUXQVUQUA",left=626, top=317, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75158458;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="SPNYJFPGNH",left=7, top=317, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la gestione";
    , HelpContextID = 123654497;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        GSMA_BRL(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_SERIALE))
      endwith
    endif
  endfunc

  add object oCODMAG_1_8 as StdField with uid="LHCUUSMSYR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del magazzino",;
    HelpContextID = 159967194,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=87, Top=10, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODRIL_1_9 as StdField with uid="YZIRCZFHSG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODRIL", cQueryName = "CODRIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice rilevazione",;
    HelpContextID = 67364826,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=472, Top=8, InputMask=replicate('X',10), cLinkFile="CODIRILE", oKey_1_1="RICODICE", oKey_1_2="this.w_CODRIL"

  func oCODRIL_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESMAG_1_11 as StdField with uid="WPSJYHROON",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 159908298,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=141, Top=10, InputMask=replicate('X',30)

  add object oDATARIL_1_13 as StdField with uid="DMKXFPEGVR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATARIL", cQueryName = "DATARIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 109311434,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=598, Top=8


  add object oObj_1_15 as cp_runprogram with uid="LUZDKCVJLL",left=426, top=366, width=131,height=26,;
    caption='GSMA_BRL(Z)',;
   bGlobalFont=.t.,;
    prg="GSMA_BRL('Z')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 212952782

  add object oDESCRI_1_17 as StdField with uid="GZRIYMYRMG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "descrizione articolo",;
    HelpContextID = 109183434,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=168, Top=324, InputMask=replicate('X',40)


  add object oObj_1_18 as cp_runprogram with uid="OVFLUWNXVW",left=564, top=367, width=153,height=26,;
    caption='GSMA_BRL(R)',;
   bGlobalFont=.t.,;
    prg="GSMA_BRL('R')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 212954830

  add object oStr_1_10 as StdString with uid="CTDDYNKFKJ",Visible=.t., Left=11, Top=14,;
    Alignment=1, Width=69, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="FKVZBWRPGJ",Visible=.t., Left=399, Top=12,;
    Alignment=1, Width=66, Height=18,;
    Caption="Rilevazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="PZQADHNMZX",Visible=.t., Left=562, Top=11,;
    Alignment=1, Width=32, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="OSTEXODWMB",Visible=.t., Left=93, Top=324,;
    Alignment=1, Width=71, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kdr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
