* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpi                                                        *
*              Stampa annuale plafond                                          *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_136]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-16                                                      *
* Last revis.: 2008-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpi",oParentObject,m.pEXEC)
return(i_retval)

define class tgscg_bpi as StdBatch
  * --- Local variables
  w_ANNO = space(4)
  w_VENATT = space(5)
  w_OLNUMPER = 0
  w_PERIODO = 0
  w_nPER = 0
  w_nALI = 0
  w_IMPMA = 0
  w_VENTIMPO = 0
  w_VENTIL = 0
  w_TOTPER = 0
  w_TOTIMPO = 0
  w_TOTIVA = 0
  w_VENTIVA = 0
  w_NUMALIQU = 0
  w_INDARR = 0
  w_ALIIVA = 0
  w_TOTVEN = 0
  w_PRIMO = .f.
  w_IMPONIPER = 0
  w_IMPOSTAPER = 0
  w_VALUTACAL = space(3)
  w_DECIMALI = 0
  w_DECIMALIV = 0
  w_CAMBIO = 0
  w_FINESER = ctod("  /  /  ")
  w_PROSEGUI = .f.
  w_CODAZI = space(5)
  w_PLAINI = 0
  pEXEC = space(1)
  w_INIANNO = ctod("  /  /  ")
  w_LCODATT = space(8)
  w_LDESATT = space(35)
  * --- WorkFile variables
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  MOD_COAN_idx=0
  SEDIAZIE_idx=0
  TITOLARI_idx=0
  ESERCIZI_idx=0
  VALUTE_idx=0
  ATT_ALTE_idx=0
  DAT_IVAN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PROSEGUI = .T.
    this.w_ANNO = this.oParentObject.w_ANNOEL
    this.w_CODAZI = i_CODAZI
    * --- Leggo Plafond iniziale
    * --- Read from DAT_IVAN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IAPLAINI"+;
        " from "+i_cTable+" DAT_IVAN where ";
            +"IACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
            +" and IA__ANNO = "+cp_ToStrODBC(this.w_ANNO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IAPLAINI;
        from (i_cTable) where;
            IACODAZI = this.w_CODAZI;
            and IA__ANNO = this.w_ANNO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PLAINI = NVL(cp_ToDate(_read_.IAPLAINI),cp_NullValue(_read_.IAPLAINI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    CREATE CURSOR XSTAMPA (PERIODO N(6), UINTRA N(18,4), UIMPORT N(18,4), VA1 N(18,4), ESP1 N(18,4), VA2 N(18,4), ESP2 N(18,4))
    this.w_PERIODO = 1
    do while this.w_PERIODO <13
       
 SELECT XSTAMPA 
 APPEND BLANK 
 REPLACE PERIODO WITH this.w_PERIODO 
 REPLACE UINTRA WITH 0*1.9999 
 REPLACE UIMPORT WITH 0*1.9999 
 REPLACE VA1 WITH 0*1.9999 
 REPLACE ESP1 WITH 0*1.9999 
 REPLACE VA2 WITH 0*1.9999 
 REPLACE ESP2 WITH 0*1.9999
      this.w_PERIODO = this.w_PERIODO + 1
    enddo
    * --- Verifico la presenza di ventilazione
    * --- Select from ATTIDETT
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ATCODATT  from "+i_cTable+" ATTIDETT ";
          +" where ATTIPREG = 'E'";
           ,"_Curs_ATTIDETT")
    else
      select ATCODATT from (i_cTable);
       where ATTIPREG = "E";
        into cursor _Curs_ATTIDETT
    endif
    if used('_Curs_ATTIDETT')
      select _Curs_ATTIDETT
      locate for 1=1
      do while not(eof())
      this.w_VENATT = _Curs_ATTIDETT.ATCODATT
        select _Curs_ATTIDETT
        continue
      enddo
      use
    endif
    do while VAL(this.w_ANNO) > VAL(this.oParentObject.w_ANNOEL) - 2
      this.w_TOTPER = 0
      this.w_TOTVEN = 0
      * --- Determino la valuta
      this.w_FINESER = cp_CharToDate("31-12-"+this.w_ANNO)
      * --- Select from ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ESVALNAZ  from "+i_cTable+" ESERCIZI ";
            +" where "+cp_ToStrODBC(this.w_CODAZI)+"=ESCODAZI AND "+cp_ToStrODBC(this.w_FINESER)+">=ESINIESE AND "+cp_ToStrODBC(this.w_FINESER)+"<=ESFINESE";
             ,"_Curs_ESERCIZI")
      else
        select ESVALNAZ from (i_cTable);
         where this.w_CODAZI=ESCODAZI AND this.w_FINESER>=ESINIESE AND this.w_FINESER<=ESFINESE;
          into cursor _Curs_ESERCIZI
      endif
      if used('_Curs_ESERCIZI')
        select _Curs_ESERCIZI
        locate for 1=1
        do while not(eof())
        this.w_VALUTACAL = _Curs_ESERCIZI.ESVALNAZ
        * --- Determino i decimali per la ventilazione
        *     Per la traduzione prendo sempre due decimali
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_VALUTACAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_VALUTACAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECIMALIV = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_CAMBIO = GETCAM(this.w_VALUTACAL, I_DATSYS)
        this.w_DECIMALI = 2
        if NVL(this.w_CAMBIO,0) = 0
          if VAL(this.w_ANNO) = VAL(this.oParentObject.w_ANNOEL) 
            ah_ErrorMsg("Impossibile determinare il cambio della valuta per il %1","!","",this.w_ANNO)
            if USED("XSTAMPA")
              SELECT XSTAMPA 
 USE
            endif
            i_retcode = 'stop'
            return
          else
            * --- Non ho l'esercizio precedente
            this.w_PROSEGUI = .F.
          endif
        endif
          select _Curs_ESERCIZI
          continue
        enddo
        use
      endif
      if this.w_PROSEGUI
        * --- Calcolo Volume di affari per periodo
        VQ_EXEC("QUERY\GSCG_BPI.VQR",this,"VOLAFFARI")
        * --- Controllo se ho registri in ventilazione
        if !EMPTY(NVL(this.w_VENATT,space(5)))
          * --- Creo un corsore con i periodi e gli importi della ventilazione
          CREATE CURSOR SOLOVENT (PERIODO N(6), IMPONIBILE N(18,4))
          VQ_EXEC("QUERY\GSCG1BPI.VQR",this,"MONTEACQ")
          VQ_EXEC("QUERY\GSCG2BPI.VQR",this,"VENTILA")
          if USED("MONTEACQ") AND USED("VENTILA")
            * --- Creao un cursore con i periodi e le aliquote del Monte Acquisti.
            *     E' necessario che lo crei nuovo perch� i periodi devono contenere la somma dei precedenti
            *     ed il periodo deve essere replicato anche se non ho monte acquisti ma solo ventilazione
            Select Distinct TRCODIVA From MONTEACQ into cursor APPO ORDER BY TRCODIVA
            this.w_NUMALIQU = RECCOUNT("APPO")
            if this.w_NUMALIQU > 0
              CREATE CURSOR COMPLETO (TRNUMPER N(2), TRCODIVA N(2))
              SELECT APPO
              DIMENSION MONTEACQ(this.w_NUMALIQU)
              MONTEACQ = 0
              this.w_nPER = 1
              do while this.w_nPER <=12
                SELECT APPO
                GO TOP
                SCAN
                SELECT COMPLETO
                APPEND BLANK
                REPLACE TRNUMPER WITH this.w_nPER, TRCODIVA WITH APPO.TRCODIVA
                SELECT APPO
                ENDSCAN
                this.w_nPER = this.w_nPER + 1
              enddo
               
 SELECT COMPLETO.TRNUMPER, COMPLETO.TRCODIVA, Nvl(MONTEACQ.IMPONIBILE,0*1.9999) AS IMPONIBILE, ; 
 VENTILA.TRIMPVEN AS TRIMPVEN FROM ; 
 ((COMPLETO LEFT JOIN MONTEACQ ON COMPLETO.TRNUMPER = MONTEACQ.TRNUMPER AND COMPLETO.TRCODIVA = MONTEACQ.TRCODIVA) ; 
 LEFT JOIN VENTILA ON COMPLETO.TRNUMPER = VENTILA.TRNUMPER) ; 
 INTO CURSOR ELABORA
              Select VENTILA
              Use
              Select MONTEACQ
              Use
              Select COMPLETO
              Use
            endif
            if USED("APPO")
              Select APPO 
 USE
              Use
            endif
            if USED("VENTILA")
              Select VENTILA 
 USE
            endif
            if USED("COMPLETO")
              Select COMPLETO 
 USE
            endif
            if USED("MONTEACQ")
              Select MONTEACQ 
 USE
            endif
          endif
          if USED("ELABORA")
            * --- Prendo i totali del perodo
            *     Eseguo due scan: sono solo 12 periodi quindi non c'� problemi di performance
            DIMENSION aTOTPER(12)
            aTOTPER = 0
            this.w_OLNUMPER = 0
            Select ELABORA
            Go Top
            SCAN
            if this.w_OLNUMPER <> TRNUMPER 
              this.w_OLNUMPER = TRNUMPER
              if TRNUMPER <> 1
                aTOTPER(TRNUMPER) = aTOTPER(TRNUMPER-1) + aTOTPER(TRNUMPER)
              endif
            endif
            aTOTPER(TRNUMPER) = aTOTPER(TRNUMPER) + IMPONIBILE
            ENDSCAN
            Select ELABORA
            Go Top
            this.w_PRIMO = .T.
            SCAN
            * --- Quando cambia il periodo assegno al cursore SOLOVENT i valori
            *     Lo faccio sempre tranne al primo passaggio
            if TRNUMPER<>this.w_OLNUMPER
              if !this.w_PRIMO
                 
 SELECT SOLOVENT 
 APPEND BLANK 
 REPLACE PERIODO WITH this.w_OLNUMPER 
 REPLACE IMPONIBILE WITH this.w_IMPONIPER 
 SELECT ELABORA
              endif
              this.w_OLNUMPER = TRNUMPER
              this.w_VENTIL = nvl(TRIMPVEN,0)
              this.w_TOTVEN = this.w_TOTVEN + this.w_VENTIL
              this.w_IMPONIPER = 0
              this.w_IMPOSTAPER = 0
              this.w_INDARR = 1
            endif
            this.w_PRIMO = .F.
            this.w_ALIIVA = TRCODIVA
            MONTEACQ(this.w_INDARR) = MONTEACQ(this.w_INDARR) + IMPONIBILE
            if aTOTPER(TRNUMPER)=0
              this.w_VENTIMPO = IVAROUND(IVAROUND(this.w_VENTIL,this.w_DECIMALI,0,this.w_VALUTACAL)/(1+(this.w_ALIIVA/100)),this.w_DECIMALI,0,this.w_VALUTACAL)
              this.w_VENTIVA = IVAROUND(IVAROUND(this.w_VENTIL,this.w_DECIMALI,0,this.w_VALUTACAL)*this.w_ALIIVA/(100+this.w_ALIIVA),this.w_DECIMALI,0,this.w_VALUTACAL)
            else
              this.w_VENTIMPO = IVAROUND(IVAROUND(this.w_VENTIL*(MONTEACQ(this.w_INDARR)/aTOTPER(TRNUMPER)),this.w_DECIMALI,0,this.w_VALUTACAL)/(1+(this.w_ALIIVA/100)),this.w_DECIMALI,0,this.w_VALUTACAL)
              this.w_VENTIVA = IVAROUND(IVAROUND(this.w_VENTIL*(MONTEACQ(this.w_INDARR)/aTOTPER(TRNUMPER)),this.w_DECIMALI,0,this.w_VALUTACAL)*this.w_ALIIVA/(100+this.w_ALIIVA),this.w_DECIMALI,0,this.w_VALUTACAL)
            endif
            this.w_IMPONIPER = this.w_IMPONIPER + this.w_VENTIMPO
            this.w_IMPOSTAPER = this.w_IMPOSTAPER + this.w_VENTIVA
            this.w_TOTIMPO = this.w_TOTIMPO + this.w_VENTIMPO
            this.w_TOTIVA = this.w_TOTIVA + this.w_VENTIVA
            this.w_INDARR = this.w_INDARR+1
            ENDSCAN
            SELECT ELABORA
            if RECCOUNT() > 0
               
 SELECT SOLOVENT 
 APPEND BLANK 
 REPLACE PERIODO WITH this.w_OLNUMPER 
 REPLACE IMPONIBILE WITH this.w_IMPONIPER
            endif
            SELECT ELABORA
            Select ELABORA
            Use
          endif
        endif
        if USED("SOLOVENT")
          SELECT IIF(NVL(VOLAFFARI.TRNUMPER,99) = 99, SOLOVENT.PERIODO,VOLAFFARI.TRNUMPER) AS PERIODO, ; 
 NVL(VOLAFFARI.VA, 0) + NVL(SOLOVENT.IMPONIBILE, 0*1.9999) AS IMPONIBILE FROM ; 
 VOLAFFARI FULL JOIN SOLOVENT ON VOLAFFARI.TRNUMPER = SOLOVENT.PERIODO ORDER BY 1 INTO CURSOR TOTVA
          SELECT SOLOVENT 
 USE
        else
          if USED("VOLAFFARI")
            SELECT VOLAFFARI.TRNUMPER AS PERIODO, Nvl(VOLAFFARI.VA,0) AS IMPONIBILE FROM VOLAFFARI ORDER BY 1 INTO CURSOR TOTVA
          endif
        endif
        if USED("VOLAFFARI")
          SELECT VOLAFFARI 
 USE
        endif
        VQ_EXEC("QUERY\GSCG3BPI.VQR",this,"ESPORTAZIONI")
        VQ_EXEC("QUERY\GSCG4BPI.VQR",this,"UTILIZZI")
        if this.oParentObject.w_ANNOEL = this.w_ANNO
           
 SELECT XSTAMPA.PERIODO AS PERIODO, XSTAMPA.UINTRA + NVL(UTILIZZI.INTRA, 0) AS UINTRA, XSTAMPA.UIMPORT + NVL(UTILIZZI.IMPORT, 0) AS UIMPORT, ; 
 XSTAMPA.VA1 + NVL(TOTVA.IMPONIBILE,0) AS VA1, XSTAMPA.ESP1 + NVL(ESPORTAZIONI.ESP,0) AS ESP1, ; 
 XSTAMPA.VA2, XSTAMPA.ESP2 FROM ; 
 ((XSTAMPA LEFT JOIN UTILIZZI ON XSTAMPA.PERIODO = UTILIZZI. TRNUMPER) LEFT JOIN ESPORTAZIONI ON XSTAMPA.PERIODO = ESPORTAZIONI.TRNUMPER) ; 
 LEFT JOIN TOTVA ON XSTAMPA.PERIODO = TOTVA.PERIODO ; 
 ORDER BY 1 INTO CURSOR PRIMOPER NOFILTER
        else
           
 SELECT XSTAMPA.PERIODO AS PERIODO, XSTAMPA.UINTRA , XSTAMPA.UIMPORT, ; 
 XSTAMPA.VA1, XSTAMPA.ESP1, ; 
 XSTAMPA.VA2 + NVL(TOTVA.IMPONIBILE,0) AS VA2, XSTAMPA.ESP2 + NVL(ESPORTAZIONI.ESP,0) AS ESP2 FROM ; 
 ((XSTAMPA LEFT JOIN UTILIZZI ON XSTAMPA.PERIODO = UTILIZZI. TRNUMPER) LEFT JOIN ESPORTAZIONI ON XSTAMPA.PERIODO = ESPORTAZIONI.TRNUMPER) ; 
 LEFT JOIN TOTVA ON XSTAMPA.PERIODO = TOTVA.PERIODO ; 
 ORDER BY 1 INTO CURSOR SECONDOPER NOFILTER
        endif
      endif
      this.w_ANNO = ALLTRIM(STR(VAL(this.w_ANNO)-1))
      if USED("ESPORTAZIONI")
        SELECT ESPORTAZIONI 
 USE
      endif
      if USED("UTILIZZI")
        SELECT UTILIZZI 
 USE
      endif
      if USED("TOTVA")
        SELECT TOTVA 
 USE
      endif
      RELEASE MONTEACQ
    enddo
    if USED("SECONDOPER") AND Reccount("SECONDOPER")<>0
      * --- Verifico presenza secondo periodo
       
 SELECT PRIMOPER.PERIODO AS PERIODO, PRIMOPER.UINTRA AS UINTRA, PRIMOPER.UIMPORT AS UIMPORT, ; 
 PRIMOPER.VA1 AS VA1, PRIMOPER.ESP1 AS ESP1, SECONDOPER.VA2 AS VA2, SECONDOPER.ESP2 AS ESP2,this.w_PLAINI AS PLAINI ; 
 FROM PRIMOPER INNER JOIN SECONDOPER ON PRIMOPER.PERIODO = SECONDOPER.PERIODO ; 
 ORDER BY 1 INTO CURSOR __TMP__ 
 SELECT SECONDOPER 
 USE
    else
       
 SELECT PRIMOPER.PERIODO AS PERIODO, PRIMOPER.UINTRA AS UINTRA, PRIMOPER.UIMPORT AS UIMPORT, ; 
 PRIMOPER.VA1 AS VA1, PRIMOPER.ESP1 AS ESP1, PRIMOPER.VA1*0 AS VA2, PRIMOPER.ESP1*0 AS ESP2, this.w_PLAINI AS PLAINI; 
 FROM PRIMOPER ORDER BY 1 INTO CURSOR __TMP__
    endif
     
 SELECT XSTAMPA 
 USE 
 SELECT PRIMOPER 
 USE
    SELECT __TMP__
    * --- Applico Codice Attivit� alternativo
    *     Esiste procedura di conversione che crea record di default cod codice attivit�
    *     pari al codice attivit� padre
    this.w_INIANNO = cp_CharToDate("01-01"+"-"+ALLTRIM(this.oParentObject.w_ANNOEL))
    this.w_LCODATT = this.oParentObject.w_CODATT
    this.w_LDESATT = this.oParentObject.w_DESATT
    * --- Select from ATT_ALTE
    i_nConn=i_TableProp[this.ATT_ALTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_ALTE_idx,2],.t.,this.ATT_ALTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CAATTIVI,CADESCRI  from "+i_cTable+" ATT_ALTE ";
          +" where CADATATT<="+cp_ToStrODBC(this.w_INIANNO)+" AND CACODATT="+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
          +" order by CADATATT Desc";
           ,"_Curs_ATT_ALTE")
    else
      select CAATTIVI,CADESCRI from (i_cTable);
       where CADATATT<=this.w_INIANNO AND CACODATT=this.oParentObject.w_CODATT;
       order by CADATATT Desc;
        into cursor _Curs_ATT_ALTE
    endif
    if used('_Curs_ATT_ALTE')
      select _Curs_ATT_ALTE
      locate for 1=1
      do while not(eof())
      this.w_LCODATT = Nvl(_Curs_ATT_ALTE.CAATTIVI,Space(8))
      this.w_LDESATT = Nvl(_Curs_ATT_ALTE.CADESCRI,Space(35))
      Exit
        select _Curs_ATT_ALTE
        continue
      enddo
      use
    endif
     
 L_PRIMOANNO = this.oParentObject.w_ANNOEL 
 L_ANNOPREC = ALLTRIM(STR(VAL(this.oParentObject.w_ANNOEL)-1)) 
 L_ATTIVITA = this.w_LCODATT 
 L_DESATTI = this.w_LDESATT 
 L_SIMBOLO = this.oParentObject.w_SIMBOLO
    if this.pEXEC="S"
      CP_CHPRN("QUERY\GSCG_SPA.FRX","",this.oParentObject)
    else
      CURTOTAB("__TMP__", "GSCG1BPI")
    endif
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='ATTIMAST'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='MOD_COAN'
    this.cWorkTables[5]='SEDIAZIE'
    this.cWorkTables[6]='TITOLARI'
    this.cWorkTables[7]='ESERCIZI'
    this.cWorkTables[8]='VALUTE'
    this.cWorkTables[9]='ATT_ALTE'
    this.cWorkTables[10]='DAT_IVAN'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_ATT_ALTE')
      use in _Curs_ATT_ALTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
