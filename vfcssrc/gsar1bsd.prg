* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bsd                                                        *
*              Query dinamica                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-15                                                      *
* Last revis.: 2014-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_QRYDINAMICA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bsd",oParentObject,m.w_QRYDINAMICA)
return(i_retval)

define class tgsar1bsd as StdBatch
  * --- Local variables
  w_QRYDINAMICA = space(254)
  * --- WorkFile variables
  GSAR1BSD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea tabella temporanea con la query dell'output utente
    this.oParentObject=this.oParentObject.oParentObject
    if vartype(i_oCacheFile)="O"
      i_oCacheFile.RemoveFile(this.w_QRYDINAMICA)
    endif
    * --- Create temporary table GSAR1BSD
    i_nIdx=cp_AddTableDef('GSAR1BSD') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec(''+This.w_QRYDINAMICA+'',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.GSAR1BSD_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
  endproc


  proc Init(oParentObject,w_QRYDINAMICA)
    this.w_QRYDINAMICA=w_QRYDINAMICA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*GSAR1BSD'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_QRYDINAMICA"
endproc
