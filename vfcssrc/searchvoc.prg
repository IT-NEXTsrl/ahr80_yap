* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: searchvoc                                                       *
*              Determina voci di analitica da applicare                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-12-19                                                      *
* Last revis.: 2011-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPVOC,pCODART,pTIPCON,pCODCON,pTIPDOC,pCODNOM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tsearchvoc",oParentObject,m.pTIPVOC,m.pCODART,m.pTIPCON,m.pCODCON,m.pTIPDOC,m.pCODNOM)
return(i_retval)

define class tsearchvoc as StdBatch
  * --- Local variables
  pTIPVOC = space(1)
  pCODART = space(20)
  pTIPCON = space(1)
  pCODCON = space(15)
  pTIPDOC = space(5)
  pCODNOM = space(15)
  w_CODVOR = space(15)
  w_CODVOC = space(15)
  w_NOCATCON = space(5)
  w_ANCATCON = space(5)
  w_CATCON = space(5)
  w_NOCACLFO = space(5)
  w_INCACLFO = space(5)
  w_CODCAA = space(5)
  w_RETVOC = space(15)
  w_ANCACLFO = space(1)
  w_CENTROCR = space(15)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CONTI_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Determina Voce di costao\ricavo dato Codice articolo\servizio
    *     Tipo voce Costo C \  Ricavo R \T restituisce il centro costo-ricavo associato all'articolo
    * --- Codice articolo
    * --- Tipo cliente/fornitore
    * --- Codice cliente/fornitore
    * --- Causale documento
    * --- Codice nominativo
    * --- Centro di costo e ricavo
    if IsAhe()
      * --- Nel caso di tipo='T' ci limitiamo a leggere il centro di costo
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCODCAA,ARCENCOS"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.pCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCODCAA,ARCENCOS;
          from (i_cTable) where;
              ARCODART = this.pCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCAA = NVL(cp_ToDate(_read_.ARCODCAA),cp_NullValue(_read_.ARCODCAA))
        this.w_CENTROCR = NVL(cp_ToDate(_read_.ARCENCOS),cp_NullValue(_read_.ARCENCOS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.pTIPVOC<>"T"
        if Not Empty(this.pCODNOM)
          * --- Read from OFF_NOMI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NOCATCON,NOCACLFO"+;
              " from "+i_cTable+" OFF_NOMI where ";
                  +"NOCODICE = "+cp_ToStrODBC(this.pCODNOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NOCATCON,NOCACLFO;
              from (i_cTable) where;
                  NOCODICE = this.pCODNOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NOCATCON = NVL(cp_ToDate(_read_.NOCATCON),cp_NullValue(_read_.NOCATCON))
            this.w_NOCACLFO = NVL(cp_ToDate(_read_.NOCACLFO),cp_NullValue(_read_.NOCACLFO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(this.pCODCON) AND Not Empty(this.pTIPCON)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCATCON,ANCACLFO"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.pTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.pCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCATCON,ANCACLFO;
              from (i_cTable) where;
                  ANTIPCON = this.pTIPCON;
                  and ANCODICE = this.pCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ANCATCON = NVL(cp_ToDate(_read_.ANCATCON),cp_NullValue(_read_.ANCATCON))
            this.w_INCACLFO = NVL(cp_ToDate(_read_.ANCACLFO),cp_NullValue(_read_.ANCACLFO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_ANCACLFO = iif(empty(this.w_NOCACLFO), this.w_INCACLFO, this.w_NOCACLFO)
        this.w_CATCON = iif(EMPTY(this.w_NOCATCON), this.w_ANCATCON, this.w_NOCATCON)
         
 Linkcaan(This,this.w_CODCAA,this.w_CATCON,this.w_ANCACLFO,this.pTIPDOC,"w_CODVOC","w_CODVOR")
        if (this.pTIPVOC="R" and Empty(this.w_CODVOR)) or (this.pTIPVOC="C" and Empty(this.w_CODVOC))
          * --- Select from vocaanarti
          do vq_exec with 'vocaanarti',this,'_Curs_vocaanarti','',.f.,.t.
          if used('_Curs_vocaanarti')
            select _Curs_vocaanarti
            locate for 1=1
            do while not(eof())
            this.w_CODVOC = NVL(_Curs_vocaanarti.CAVOCCOS,"")
            this.w_CODVOR = NVL(_Curs_vocaanarti.CAVOCRIC,"")
              select _Curs_vocaanarti
              continue
            enddo
            use
          endif
        endif
      endif
    else
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARVOCCEN,ARVOCRIC"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.pCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARVOCCEN,ARVOCRIC;
          from (i_cTable) where;
              ARCODART = this.pCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODVOC = NVL(cp_ToDate(_read_.ARVOCCEN),cp_NullValue(_read_.ARVOCCEN))
        this.w_CODVOR = NVL(cp_ToDate(_read_.ARVOCRIC),cp_NullValue(_read_.ARVOCRIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_RETVOC = ICASE(this.pTIPVOC="R",this.w_CODVOR,this.pTIPVOC="T",this.w_CENTROCR,this.w_CODVOC)
    i_retcode = 'stop'
    i_retval = this.w_RETVOC
    return
  endproc


  proc Init(oParentObject,pTIPVOC,pCODART,pTIPCON,pCODCON,pTIPDOC,pCODNOM)
    this.pTIPVOC=pTIPVOC
    this.pCODART=pCODART
    this.pTIPCON=pTIPCON
    this.pCODCON=pCODCON
    this.pTIPDOC=pTIPDOC
    this.pCODNOM=pCODNOM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='OFF_NOMI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_vocaanarti')
      use in _Curs_vocaanarti
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPVOC,pCODART,pTIPCON,pCODCON,pTIPDOC,pCODNOM"
endproc
