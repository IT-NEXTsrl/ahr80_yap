* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kbl                                                        *
*              Sblocco amministratore                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_32]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-10                                                      *
* Last revis.: 2008-09-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kbl",oParentObject))

* --- Class definition
define class tgsut_kbl as StdForm
  Top    = 24
  Left   = 38

  * --- Standard Properties
  Width  = 280
  Height = 226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-02"
  HelpContextID=152485015
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  POL_UTE_IDX = 0
  cPrg = "gsut_kbl"
  cComment = "Sblocco amministratore"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODUTE = 0
  w_PWD_1 = space(20)
  w_UTE = 0
  o_UTE = 0
  w_UTEDES = space(20)
  w_MAC = space(20)
  w_LOGIN = space(20)
  w_OK = .F.
  * --- Area Manuale = Declare Variables
  * --- gsut_kbl
  AutoCenter=.t.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kblPag1","gsut_kbl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPWD_1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='POL_UTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODUTE=0
      .w_PWD_1=space(20)
      .w_UTE=0
      .w_UTEDES=space(20)
      .w_MAC=space(20)
      .w_LOGIN=space(20)
      .w_OK=.f.
      .w_CODUTE=oParentObject.w_CODUTE
      .w_PWD_1=oParentObject.w_PWD_1
      .w_OK=oParentObject.w_OK
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODUTE))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_UTE = .w_CODUTE
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_UTE))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_4.Calculate('*')
          .DoRTCalc(4,6,.f.)
        .w_OK = .t.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CODUTE=.w_CODUTE
      .oParentObject.w_PWD_1=.w_PWD_1
      .oParentObject.w_OK=.w_OK
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
        if .o_UTE<>.w_UTE
            .w_UTE = .w_CODUTE
          .link_1_3('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate('*')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate('*')
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODUTE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.CODE,0)
      this.w_UTEDES = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_UTEDES = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.POL_UTE_IDX,3]
    i_lTable = "POL_UTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2], .t., this.POL_UTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PUCODUTE,PU_LOGIN,PU___MAC";
                   +" from "+i_cTable+" "+i_lTable+" where PUCODUTE="+cp_ToStrODBC(this.w_UTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PUCODUTE',this.w_UTE)
            select PUCODUTE,PU_LOGIN,PU___MAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTE = NVL(_Link_.PUCODUTE,0)
      this.w_LOGIN = NVL(_Link_.PU_LOGIN,space(20))
      this.w_MAC = NVL(_Link_.PU___MAC,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTE = 0
      endif
      this.w_LOGIN = space(20)
      this.w_MAC = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2])+'\'+cp_ToStr(_Link_.PUCODUTE,1)
      cp_ShowWarn(i_cKey,this.POL_UTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_1.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_1.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPWD_1_1_2.value==this.w_PWD_1)
      this.oPgFrm.Page1.oPag.oPWD_1_1_2.value=this.w_PWD_1
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEDES_1_6.value==this.w_UTEDES)
      this.oPgFrm.Page1.oPag.oUTEDES_1_6.value=this.w_UTEDES
    endif
    if not(this.oPgFrm.Page1.oPag.oMAC_1_8.value==this.w_MAC)
      this.oPgFrm.Page1.oPag.oMAC_1_8.value=this.w_MAC
    endif
    if not(this.oPgFrm.Page1.oPag.oLOGIN_1_9.value==this.w_LOGIN)
      this.oPgFrm.Page1.oPag.oLOGIN_1_9.value=this.w_LOGIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_UTE = this.w_UTE
    return

enddefine

* --- Define pages as container
define class tgsut_kblPag1 as StdContainer
  Width  = 276
  height = 226
  stdWidth  = 276
  stdheight = 226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODUTE_1_1 as StdField with uid="JANIWGXEUW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente amministratore",;
    HelpContextID = 61886502,;
   bGlobalFont=.t.,;
    Height=21, Width=35, Left=79, Top=68, cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oPWD_1_1_2 as StdField with uid="LGEUYVGYPM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PWD_1", cQueryName = "PWD_1",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Inserire password",;
    HelpContextID = 210393334,;
   bGlobalFont=.t.,;
    Height=21, Width=190, Left=79, Top=151, InputMask=replicate('X',20)


  add object oObj_1_4 as cp_setobjprop with uid="AUWPJKIPCT",left=0, top=246, width=229,height=21,;
    caption='Pwd',;
   bGlobalFont=.t.,;
    cObj="w_pwd_1",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 152926454

  add object oUTEDES_1_6 as StdField with uid="HSYANUWWJS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_UTEDES", cQueryName = "UTEDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 256940474,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=116, Top=68, InputMask=replicate('X',20)

  add object oMAC_1_8 as StdField with uid="WATIHUQWOR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MAC", cQueryName = "MAC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Computer name dell'ultimo accesso",;
    HelpContextID = 152777414,;
   bGlobalFont=.t.,;
    Height=21, Width=190, Left=79, Top=115, InputMask=replicate('X',20)

  add object oLOGIN_1_9 as StdField with uid="JNFENZCFYD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LOGIN", cQueryName = "LOGIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Windows user name dell'ultimo accesso",;
    HelpContextID = 239370422,;
   bGlobalFont=.t.,;
    Height=21, Width=190, Left=79, Top=92, InputMask=replicate('X',20)


  add object oBtn_1_15 as StdButton with uid="ZMIZZNTUDJ",left=170, top=178, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 207640086;
    , caption='\<OK';
  , bGlobalFont=.t.

    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="JWIBHYOYEX",left=221, top=178, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 159802438;
    , Caption='\<Esci';
  , bGlobalFont=.t.

    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="CCYWRIANEH",Visible=.t., Left=5, Top=151,;
    Alignment=1, Width=70, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="LKPAETVCHM",Visible=.t., Left=6, Top=68,;
    Alignment=1, Width=69, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="MKNFZHRQOS",Visible=.t., Left=24, Top=24,;
    Alignment=0, Width=244, Height=18,;
    Caption="L'utente amministratore � stato bloccato"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="FCDWIAAXQY",Visible=.t., Left=24, Top=41,;
    Alignment=0, Width=244, Height=18,;
    Caption="Inserire la password per sbloccare"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="YIPXZUEWWM",Visible=.t., Left=24, Top=7,;
    Alignment=0, Width=80, Height=18,;
    Caption="ATTENZIONE"    , forecolor=RGB(255,0,0);
  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="NLNKVDWKHJ",Visible=.t., Left=6, Top=92,;
    Alignment=1, Width=69, Height=17,;
    Caption="Login:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="CWVIHEPKSJ",Visible=.t., Left=6, Top=115,;
    Alignment=1, Width=69, Height=17,;
    Caption="Macchina:"  ;
  , bGlobalFont=.t.

  add object oBox_1_18 as StdBox with uid="FSKIIDSIOP",left=1, top=141, width=270,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kbl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
