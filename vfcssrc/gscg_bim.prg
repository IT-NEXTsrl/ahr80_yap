* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bim                                                        *
*              Controlli F24                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_77]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-20                                                      *
* Last revis.: 2000-08-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPROV,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bim",oParentObject,m.pPROV,m.pTIPO)
return(i_retval)

define class tgscg_bim as StdBatch
  * --- Local variables
  pPROV = space(1)
  pTIPO = space(3)
  w_INIPER = ctod("  /  /  ")
  w_FINEPER = ctod("  /  /  ")
  w_OBJFIGLIO = .NULL.
  w_VALUTA = space(3)
  w_MESS = space(50)
  w_PUNPAD = .NULL.
  w_DATARIF = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_CODESE = space(4)
  * --- WorkFile variables
  ESERCIZI_idx=0
  MOD_PAG_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla se gli estremi dell'esercizio concidono con l'inizio e la fine dell'anno solare
    * --- Controlla Data Presentazione Modello
    * --- Aggiorna la valuta della sezione erario (da GSCG_AMF)
    * --- Puntatore al Padre (GSCG_AMF, GSCG_ANF)
    this.w_PUNPAD = this.oParentObject
    do case
      case this.pTIPO="ANN"
        do case
          case this.pPROV="N"
            if this.w_PUNPAD.GSCG_AVF.w_VFDTPRES < cp_CharToDate("01-01-2002")
              ah_ErrorMsg("La data di presentazione del modello deve essere successiva al 01-01-2002.%0I dati inseriti non potranno essere salvati",,"")
            endif
          case this.pPROV="M"
            if this.w_PUNPAD.GSCG_AVF.w_VFDTPRES >= cp_CharToDate("01-01-2002")
              ah_ErrorMsg("La data di presentazione del modello deve essere antecedente al 01-01-2002.%0I dati inseriti non potranno essere salvati",,"")
            endif
        endcase
        * --- FLAG Anno di imposta non coincidente con anno solare
        this.w_CODAZI = i_CODAZI
        this.w_DATARIF = cp_CharToDate("01-"+this.oParentObject.w_MFMESRIF+"-"+this.oParentObject.w_MFANNRIF)
        this.w_CODESE = CALCESER(this.w_DATARIF,g_CODESE)
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESINIESE,ESFINESE"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.w_CODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESINIESE,ESFINESE;
            from (i_cTable) where;
                ESCODAZI = this.w_CODAZI;
                and ESCODESE = this.w_CODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INIPER = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
          this.w_FINEPER = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if right(dtoc(this.w_INIPER,1),4)<>"0101" or right(dtoc(this.w_FINEPER,1),4)<>"1231"
          this.oParentObject.w_APPCOIN = "N"
        else
          this.oParentObject.w_APPCOIN = " "
        endif
      case this.pTIPO="VAL"
        this.w_OBJFIGLIO = this.w_PUNPAD.GSCG_AEF
        this.w_OBJFIGLIO.w_VALUTA = this.oParentObject.w_MFVALUTA
        this.w_OBJFIGLIO.SetControlsValue()     
        this.w_OBJFIGLIO.mHideControls()     
        ah_ErrorMsg("Gli importi non verranno automaticamente riconvertiti",,"")
    endcase
  endproc


  proc Init(oParentObject,pPROV,pTIPO)
    this.pPROV=pPROV
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='MOD_PAG'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPROV,pTIPO"
endproc
