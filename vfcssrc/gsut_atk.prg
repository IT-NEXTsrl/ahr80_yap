* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_atk                                                        *
*              Timer Elaborazioni KPI                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-09-02                                                      *
* Last revis.: 2014-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_atk")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_atk")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_atk")
  return

* --- Class definition
define class tgsut_atk as StdPCForm
  Width  = 258
  Height = 37
  Top    = 10
  Left   = 10
  cComment = "Timer Elaborazioni KPI"
  cPrg = "gsut_atk"
  HelpContextID=175553687
  add object cnt as tcgsut_atk
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_atk as PCContext
  w_TKCODUTE = 0
  w_TK_AVVIO = space(1)
  w_TKMINUTI = 0
  proc Save(oFrom)
    this.w_TKCODUTE = oFrom.w_TKCODUTE
    this.w_TK_AVVIO = oFrom.w_TK_AVVIO
    this.w_TKMINUTI = oFrom.w_TKMINUTI
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_TKCODUTE = this.w_TKCODUTE
    oTo.w_TK_AVVIO = this.w_TK_AVVIO
    oTo.w_TKMINUTI = this.w_TKMINUTI
    PCContext::Load(oTo)
enddefine

define class tcgsut_atk as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 258
  Height = 37
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-13"
  HelpContextID=175553687
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  TMRELKPI_IDX = 0
  cFile = "TMRELKPI"
  cKeySelect = "TKCODUTE"
  cKeyWhere  = "TKCODUTE=this.w_TKCODUTE"
  cKeyWhereODBC = '"TKCODUTE="+cp_ToStrODBC(this.w_TKCODUTE)';

  cKeyWhereODBCqualified = '"TMRELKPI.TKCODUTE="+cp_ToStrODBC(this.w_TKCODUTE)';

  cPrg = "gsut_atk"
  cComment = "Timer Elaborazioni KPI"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TKCODUTE = 0
  w_TK_AVVIO = space(1)
  w_TKMINUTI = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_atkPag1","gsut_atk",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Timer")
      .Pages(1).HelpContextID = 33749814
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTK_AVVIO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TMRELKPI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TMRELKPI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TMRELKPI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_atk'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TMRELKPI where TKCODUTE=KeySet.TKCODUTE
    *
    i_nConn = i_TableProp[this.TMRELKPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TMRELKPI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TMRELKPI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TMRELKPI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TMRELKPI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TKCODUTE',this.w_TKCODUTE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TKCODUTE = NVL(TKCODUTE,0)
        .w_TK_AVVIO = NVL(TK_AVVIO,space(1))
        .w_TKMINUTI = NVL(TKMINUTI,0)
        cp_LoadRecExtFlds(this,'TMRELKPI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_TKCODUTE = 0
      .w_TK_AVVIO = space(1)
      .w_TKMINUTI = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_TK_AVVIO = 'N'
        .w_TKMINUTI = 60
      endif
    endwith
    cp_BlankRecExtFlds(this,'TMRELKPI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oTK_AVVIO_1_2.enabled = i_bVal
      .Page1.oPag.oTKMINUTI_1_3.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'TMRELKPI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TMRELKPI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TKCODUTE,"TKCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TK_AVVIO,"TK_AVVIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TKMINUTI,"TKMINUTI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TMRELKPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TMRELKPI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TMRELKPI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TMRELKPI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TMRELKPI')
        i_extval=cp_InsertValODBCExtFlds(this,'TMRELKPI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TKCODUTE,TK_AVVIO,TKMINUTI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TKCODUTE)+;
                  ","+cp_ToStrODBC(this.w_TK_AVVIO)+;
                  ","+cp_ToStrODBC(this.w_TKMINUTI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TMRELKPI')
        i_extval=cp_InsertValVFPExtFlds(this,'TMRELKPI')
        cp_CheckDeletedKey(i_cTable,0,'TKCODUTE',this.w_TKCODUTE)
        INSERT INTO (i_cTable);
              (TKCODUTE,TK_AVVIO,TKMINUTI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TKCODUTE;
                  ,this.w_TK_AVVIO;
                  ,this.w_TKMINUTI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TMRELKPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TMRELKPI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TMRELKPI_IDX,i_nConn)
      *
      * update TMRELKPI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TMRELKPI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TK_AVVIO="+cp_ToStrODBC(this.w_TK_AVVIO)+;
             ",TKMINUTI="+cp_ToStrODBC(this.w_TKMINUTI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TMRELKPI')
        i_cWhere = cp_PKFox(i_cTable  ,'TKCODUTE',this.w_TKCODUTE  )
        UPDATE (i_cTable) SET;
              TK_AVVIO=this.w_TK_AVVIO;
             ,TKMINUTI=this.w_TKMINUTI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TMRELKPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TMRELKPI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TMRELKPI_IDX,i_nConn)
      *
      * delete TMRELKPI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TKCODUTE',this.w_TKCODUTE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TMRELKPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TMRELKPI_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTK_AVVIO_1_2.RadioValue()==this.w_TK_AVVIO)
      this.oPgFrm.Page1.oPag.oTK_AVVIO_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTKMINUTI_1_3.RadioValue()==this.w_TKMINUTI)
      this.oPgFrm.Page1.oPag.oTKMINUTI_1_3.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'TMRELKPI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_atkPag1 as StdContainer
  Width  = 254
  height = 37
  stdWidth  = 254
  stdheight = 37
  resizeXpos=122
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTK_AVVIO_1_2 as StdCheck with uid="TOTKTNQADY",rtseq=2,rtrep=.f.,left=154, top=8, caption="Attiva all'avvio",;
    ToolTipText = "il timer verr� attivato automaticamente quando si accede al programma (impostazione valida solo con thread secondario attivo)",;
    HelpContextID = 102628741,;
    cFormVar="w_TK_AVVIO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTK_AVVIO_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTK_AVVIO_1_2.GetRadio()
    this.Parent.oContained.w_TK_AVVIO = this.RadioValue()
    return .t.
  endfunc

  func oTK_AVVIO_1_2.SetRadio()
    this.Parent.oContained.w_TK_AVVIO=trim(this.Parent.oContained.w_TK_AVVIO)
    this.value = ;
      iif(this.Parent.oContained.w_TK_AVVIO=='S',1,;
      0)
  endfunc


  add object oTKMINUTI_1_3 as StdCombo with uid="VORESSSGWB",rtseq=3,rtrep=.f.,left=69,top=6,width=83,height=20;
    , ToolTipText = "Intervallo di esecuzione in minuti";
    , HelpContextID = 190521985;
    , cFormVar="w_TKMINUTI",RowSource=""+"1 min,"+"5 min,"+"10 min,"+"15 min,"+"30 min,"+"45 min,"+"1 ora,"+"1 ora e mezza,"+"2 ore,"+"3 ore,"+"4 ore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTKMINUTI_1_3.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,5,;
    iif(this.value =3,10,;
    iif(this.value =4,15,;
    iif(this.value =5,30,;
    iif(this.value =6,45,;
    iif(this.value =7,60,;
    iif(this.value =8,90,;
    iif(this.value =9,120,;
    iif(this.value =10,180,;
    iif(this.value =11,240,;
    0))))))))))))
  endfunc
  func oTKMINUTI_1_3.GetRadio()
    this.Parent.oContained.w_TKMINUTI = this.RadioValue()
    return .t.
  endfunc

  func oTKMINUTI_1_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TKMINUTI==1,1,;
      iif(this.Parent.oContained.w_TKMINUTI==5,2,;
      iif(this.Parent.oContained.w_TKMINUTI==10,3,;
      iif(this.Parent.oContained.w_TKMINUTI==15,4,;
      iif(this.Parent.oContained.w_TKMINUTI==30,5,;
      iif(this.Parent.oContained.w_TKMINUTI==45,6,;
      iif(this.Parent.oContained.w_TKMINUTI==60,7,;
      iif(this.Parent.oContained.w_TKMINUTI==90,8,;
      iif(this.Parent.oContained.w_TKMINUTI==120,9,;
      iif(this.Parent.oContained.w_TKMINUTI==180,10,;
      iif(this.Parent.oContained.w_TKMINUTI==240,11,;
      0)))))))))))
  endfunc

  add object oStr_1_4 as StdString with uid="RQCGIZOLTO",Visible=.t., Left=13, Top=9,;
    Alignment=1, Width=55, Height=18,;
    Caption="Intervallo"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_atk','TMRELKPI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TKCODUTE=TMRELKPI.TKCODUTE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
