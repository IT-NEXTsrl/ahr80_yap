* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bmp                                                        *
*              Calcolo provvigioni maturate                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_450]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2000-06-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bmp",oParentObject)
return(i_retval)

define class tgsve_bmp as StdBatch
  * --- Local variables
  w_OKDOC = 0
  w_UNDODOC = 0
  w_DISDOC = 0
  w_NUDOC = 0
  w_MESS = space(200)
  w_DATADIS = ctod("  /  /  ")
  w_READAZI = space(5)
  w_GIORNI = 0
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_STAMPA = space(1)
  w_DATREG = ctod("  /  /  ")
  w_ESCI = .f.
  w_LOOP = 0
  w_DATSCA = ctod("  /  /  ")
  w_NUMPAR = space(14)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODVAL = space(3)
  w_DATSCA1 = ctod("  /  /  ")
  w_NUMPAR1 = space(14)
  w_TIPCON1 = space(1)
  w_CODCON1 = space(15)
  w_CODVAL1 = space(3)
  w_SERIAL = space(10)
  w_TROUBLEDOC = 0
  w_DIVI = space(200)
  w_TIPO = 0
  * --- WorkFile variables
  MOP_DETT_idx=0
  PAR_TITE_idx=0
  AZIENDA_idx=0
  PNT_MAST_idx=0
  TMP_PRODIN_idx=0
  TMP_PRCHIUSE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Provvigioni Maturate alla data incasso (da GSVE_KMP)
    * --- =========================================================
    *     Schema di esecuzione
    *     =========================================================
    *     Determinazione due temporanei il primo
    *     TMP_PRODIN contiene i movimenti e relative partite da considerare in base ai filtri 
    *     impostati sulla maschera
    *     
    *     TMP_PRCHIUSE � costruito a partire da TMP_PRODIN considerando le sole partite aperte. 
    *     Questo contiene durante l'elebaorazione la situazione delle partite chiuse con relativi 
    *     riferimenti ai movimenti e la data (DATREG) da utilizzare per aggiornarli.
    *     
    *     Una volta determinati occorre verificare se le partite chiuse in TMP_PRCHIUSE siano 
    *     chiuse con un incasso o tramite raggruppamento. 
    *     
    *     Se chiuse per raggruppamento la procedura avvia una ricerca per determinare la partita 
    *     risultante, una volta determinata i riferimenti (DATASCA,TIPCON,CODCON,NUMPAR,CODVAL) 
    *     della partita in origine sono sostituiti con la partita raggruppata se questa � chiusa 
    *     altrimenti la partita viene marcata (DAESAM=0 e DATREG =Null) come aperta e da ignorare per le 
    *     successive elaborazioni. Se la raggruppata � chiusa ad essa viene ripetuto il 
    *     procedimento.
    *     
    *     La procedura si limita a 50 cicli, superati tali cicli la procedura verifica se ci siano
    *     ancora partite chiuse che sono state raggruppate, se si chiede all'utente se si desidera
    *     stamparle (per eventuali problemi sul flag PTFLRAGG, scadenze diverse con pi� partite con
    *     tale flag attivo, stesso PTSERIAL).
    *     
    *     Una volta determinato l'insieme delle partite chiuse la procedura determina il max della
    *     data registrazione a loro associata (Scadenze Diverse e Prima Nota e basta!).
    *     
    *     Occorre inoltre verificare se la partita chiusa appartiene ad un distinta, se si allora 
    *     occorre accertarsi che la data scadenza+giorni di tolleranza non sia superiore alla data 
    *     finale di filtro (w_datfin). Se questo accade la partita � cancellate altrimenti, se la 
    *     partita � in distinta ed il filtro sulla data � Ok viene impostato DATREG con la data 
    *     scadenza + giorni di tolleranza.
    *     
    *     A questo punto in TMP_PRCHIUSE ho l'insieme delle righe da aggiornare con relativa data 
    *     registrazione (tutte le chiuse). L'algoritmo prosegue sbiancando le data di maturazione 
    *     piene dei movimenti in TMP_PRODIN e dopo va a scrivere DATREG in TMP_PRCHIUSE all'interno 
    *     della data maturazione dei movimenti provvigioni.
    *     
    * --- istanzio oggetto per mess. incrementali
    this.w_oMESS=createobject("ah_message")
    if this.oParentObject.w_DATFIN>i_DATSYS
      if NOT ah_YesNo("Attenzione: si stanno maturando anche provvigioni con data incasso superiore alla data corrente, proseguire comunque?")
        i_retcode = 'stop'
        return
      endif
    endif
    ah_Msg("Ricerca provvigioni da maturare...")
    * --- Creo il temporaneo contenente le partite associate a movimenti di provvigione
    *     con righe a data incasso e data liquidazione vuota...
    *     
    *     Utilizzato nelle query successiva per determinare le partite da considerare
    *     
    *     Due indici buoni per entrambi i temporanei
    * --- Create temporary table TMP_PRODIN
    i_nIdx=cp_AddTableDef('TMP_PRODIN') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('gsvebbmp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_PRODIN_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Conto le righe di movimenti di provvigione da considerare
    this.w_NUDOC = 0
    * --- Select from TMP_PRODIN
    i_nConn=i_TableProp[this.TMP_PRODIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRODIN_idx,2],.t.,this.TMP_PRODIN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" TMP_PRODIN ";
           ,"_Curs_TMP_PRODIN")
    else
      select Count(*) As Conta from (i_cTable);
        into cursor _Curs_TMP_PRODIN
    endif
    if used('_Curs_TMP_PRODIN')
      select _Curs_TMP_PRODIN
      locate for 1=1
      do while not(eof())
      this.w_NUDOC = Nvl ( _Curs_TMP_PRODIN.CONTA , 0 )
        select _Curs_TMP_PRODIN
        continue
      enddo
      use
    endif
    if this.w_NUDOC=0
      * --- Nessun movimento provvigione nell'intervallo esco...
      this.w_oMESS.AddMsgPart("Per l'intervallo selezionato non esistono provvigioni da maturare")     
      * --- Drop temporary table TMP_PRODIN
      i_nIdx=cp_GetTableDefIdx('TMP_PRODIN')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PRODIN')
      endif
    else
      * --- Contiene le partite chiuse + riferimenti movimenti 
      ah_Msg("Ricerca partite chiuse collegate...")
      * --- Create temporary table TMP_PRCHIUSE
      i_nIdx=cp_AddTableDef('TMP_PRCHIUSE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('gsveabmp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_PRCHIUSE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Verifico se tra le partite chiuse ve ne sono alcune chiuse tramite raggruppamento
      * --- Ciclo sulle partite chiuse, da queste determino eventuali partite risultati
      *     da loro raggruppamenti e ne verifico lo stato.
      *     
      *     Se la partita raggruppata � aperta elimino la partita originaria se � chiusa 
      *     aggiorno i riferimenti della partita con quella raggruppata.
      *     
      *     Al termine di ogni giro ri eseguo la query ripetendo per un massimo di 
      *     50 giri
      this.w_ESCI = .T.
      this.w_LOOP = 50
      this.w_STAMPA = ""
      do while this.w_ESCI
        this.w_ESCI = .F.
        ah_Msg("Determinazione raggruppamenti...%1",.T.,.F.,.F., Alltrim(Str(this.w_LOOP)) )
        * --- Select from gsvefbmp
        do vq_exec with 'gsvefbmp',this,'_Curs_gsvefbmp','',.f.,.t.
        if used('_Curs_gsvefbmp')
          select _Curs_gsvefbmp
          locate for 1=1
          do while not(eof())
          * --- Valuto se la partita raggruppata � aperta o chiusa
          * --- Riferimenti partita di partenza
          this.w_DATSCA = _Curs_gsvefbmp.DATSCA
          this.w_NUMPAR = _Curs_gsvefbmp.NUMPAR
          this.w_TIPCON = _Curs_gsvefbmp.TIPCON
          this.w_CODCON = _Curs_gsvefbmp.CODCON
          this.w_CODVAL = _Curs_gsvefbmp.CODVAL
          * --- Riferimenti partita risultante (da raggruppamento)
          this.w_DATSCA1 = _Curs_gsvefbmp.DATSCA1
          this.w_NUMPAR1 = _Curs_gsvefbmp.NUMPAR1
          this.w_TIPCON1 = _Curs_gsvefbmp.TIPCON1
          this.w_CODCON1 = _Curs_gsvefbmp.CODCON1
          this.w_CODVAL1 = _Curs_gsvefbmp.CODVAL1
          * --- Data registrazione associata alla partita raggruppata
          this.w_DATREG = _Curs_gsvefbmp.DATREG
          this.w_ESCI = this.w_LOOP>0
          * --- Mentre esamino i raggruppamenti mi memorizzo il seriale della
          *     scadenza diversa che determina il raggruppamento.
          *     
          *     Questa informazioni mi occorre per evitare loop del tipo
          *     A+B => C (la raggruppata ha gli stessi estremi di partita gia esistente
          *     che raggruppo  cos� C+D =>A, dove A ha gli stessi estermi di una
          *     partita esistente che raggruppo.. C+A=>A
          *     se costruisco tre scadenze diverse in questo modo passa da C ad A.
          *     Cerco sempre un raggruppamento con seriale maggiore del'ultimo trovato...
          *     
          this.w_SERIAL = _Curs_gsvefbmp.SCCODICE 
          ah_Msg("Verifica stato partita %1 del %2 intestatario %3",.T.,.F.,.F.,this.w_NUMPAR, Dtoc(this.w_DATSCA), this.w_CODCON )
          vq_exec("GSVEGBMP.VQR",this,"_SALDO_")
           
 Select _Saldo_ 
 Go Top
          if Nvl ( _SALDO_.SALDO , 0 )=0
            * --- Se partita chiusa modifico i riferimenti partita e la data registrazione
            * --- Write into TMP_PRCHIUSE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMP_PRCHIUSE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRCHIUSE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PRCHIUSE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DATSCA ="+cp_NullLink(cp_ToStrODBC(this.w_DATSCA1),'TMP_PRCHIUSE','DATSCA');
              +",NUMPAR ="+cp_NullLink(cp_ToStrODBC(this.w_NUMPAR1),'TMP_PRCHIUSE','NUMPAR');
              +",TIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_TIPCON1),'TMP_PRCHIUSE','TIPCON');
              +",CODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CODCON1),'TMP_PRCHIUSE','CODCON');
              +",CODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_CODVAL1),'TMP_PRCHIUSE','CODVAL');
              +",DATREG ="+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'TMP_PRCHIUSE','DATREG');
              +",SERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMP_PRCHIUSE','SERIAL');
                  +i_ccchkf ;
              +" where ";
                  +"DATSCA = "+cp_ToStrODBC(this.w_DATSCA);
                  +" and NUMPAR = "+cp_ToStrODBC(this.w_NUMPAR);
                  +" and TIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and CODCON = "+cp_ToStrODBC(this.w_CODCON);
                  +" and CODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                     )
            else
              update (i_cTable) set;
                  DATSCA = this.w_DATSCA1;
                  ,NUMPAR = this.w_NUMPAR1;
                  ,TIPCON = this.w_TIPCON1;
                  ,CODCON = this.w_CODCON1;
                  ,CODVAL = this.w_CODVAL1;
                  ,DATREG = this.w_DATREG;
                  ,SERIAL = this.w_SERIAL;
                  &i_ccchkf. ;
               where;
                  DATSCA = this.w_DATSCA;
                  and NUMPAR = this.w_NUMPAR;
                  and TIPCON = this.w_TIPCON;
                  and CODCON = this.w_CODCON;
                  and CODVAL = this.w_CODVAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Se partita aperta la cancello logicamente dall'insieme (DAESAM=0) ed imposto DATREG
            *     a vuoto la data registrazione (filtro sulla query di scrittura movimenti provvigione)
            * --- Write into TMP_PRCHIUSE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMP_PRCHIUSE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRCHIUSE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PRCHIUSE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DATREG ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /    ")),'TMP_PRCHIUSE','DATREG');
              +",DAESAM ="+cp_NullLink(cp_ToStrODBC(0),'TMP_PRCHIUSE','DAESAM');
                  +i_ccchkf ;
              +" where ";
                  +"DATSCA = "+cp_ToStrODBC(this.w_DATSCA);
                  +" and NUMPAR = "+cp_ToStrODBC(this.w_NUMPAR);
                  +" and TIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and CODCON = "+cp_ToStrODBC(this.w_CODCON);
                  +" and CODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                     )
            else
              update (i_cTable) set;
                  DATREG = cp_CharToDate("  /  /    ");
                  ,DAESAM = 0;
                  &i_ccchkf. ;
               where;
                  DATSCA = this.w_DATSCA;
                  and NUMPAR = this.w_NUMPAR;
                  and TIPCON = this.w_TIPCON;
                  and CODCON = this.w_CODCON;
                  and CODVAL = this.w_CODVAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
           
 Select _Saldo_ 
 Use
            select _Curs_gsvefbmp
            continue
          enddo
          use
        endif
        this.w_LOOP = this.w_LOOP - 1
      enddo
      * --- Se esco al termine dei 50 giri significa che esistono partite alle quali non riesco
      *     ad attribuire il raggruppamento
      if this.w_LOOP<=0 
        if ah_YesNo("Per alcune partite chiuse non � possibile determinare la data incasso%0Si desidera stamparle?")
          * --- Parametro x la query, in modo che mi distingua le scadenze anche in base 
          *     al movimento provvigione al quale sono legate
          this.w_STAMPA = "S"
          vq_exec("GSVEFBMP.VQR",this,"__TMP__")
          CP_CHPRN( "QUERY\GSVE_BMP.FRX", " ", this )
          if Used("__Tmp__")
             
 Select __tmp__ 
 Use
          endif
          * --- Sbianco la data registrazione delle partite in questione
        endif
        ah_Msg("Svuota data registrazione partite stampate...")
        * --- Per i movimenti di questo tipo la data registrazione � sbiancata...
        * --- Write into TMP_PRCHIUSE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMP_PRCHIUSE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRCHIUSE_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="DATSCA,NUMPAR,TIPCON,CODCON,CODVAL"
          do vq_exec with 'gsvepbmp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PRCHIUSE_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMP_PRCHIUSE.DATSCA = _t2.DATSCA";
                  +" and "+"TMP_PRCHIUSE.NUMPAR = _t2.NUMPAR";
                  +" and "+"TMP_PRCHIUSE.TIPCON = _t2.TIPCON";
                  +" and "+"TMP_PRCHIUSE.CODCON = _t2.CODCON";
                  +" and "+"TMP_PRCHIUSE.CODVAL = _t2.CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DATREG ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /    ")),'TMP_PRCHIUSE','DATREG');
              +i_ccchkf;
              +" from "+i_cTable+" TMP_PRCHIUSE, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMP_PRCHIUSE.DATSCA = _t2.DATSCA";
                  +" and "+"TMP_PRCHIUSE.NUMPAR = _t2.NUMPAR";
                  +" and "+"TMP_PRCHIUSE.TIPCON = _t2.TIPCON";
                  +" and "+"TMP_PRCHIUSE.CODCON = _t2.CODCON";
                  +" and "+"TMP_PRCHIUSE.CODVAL = _t2.CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PRCHIUSE, "+i_cQueryTable+" _t2 set ";
          +"TMP_PRCHIUSE.DATREG ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /    ")),'TMP_PRCHIUSE','DATREG');
              +Iif(Empty(i_ccchkf),"",",TMP_PRCHIUSE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMP_PRCHIUSE.DATSCA = t2.DATSCA";
                  +" and "+"TMP_PRCHIUSE.NUMPAR = t2.NUMPAR";
                  +" and "+"TMP_PRCHIUSE.TIPCON = t2.TIPCON";
                  +" and "+"TMP_PRCHIUSE.CODCON = t2.CODCON";
                  +" and "+"TMP_PRCHIUSE.CODVAL = t2.CODVAL";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PRCHIUSE set (";
              +"DATREG";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /    ")),'TMP_PRCHIUSE','DATREG')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMP_PRCHIUSE.DATSCA = _t2.DATSCA";
                  +" and "+"TMP_PRCHIUSE.NUMPAR = _t2.NUMPAR";
                  +" and "+"TMP_PRCHIUSE.TIPCON = _t2.TIPCON";
                  +" and "+"TMP_PRCHIUSE.CODCON = _t2.CODCON";
                  +" and "+"TMP_PRCHIUSE.CODVAL = _t2.CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PRCHIUSE set ";
          +"DATREG ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /    ")),'TMP_PRCHIUSE','DATREG');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
                  +" and "+i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
                  +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                  +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                  +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DATREG ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /    ")),'TMP_PRCHIUSE','DATREG');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_TROUBLEDOC = i_ROWS
      endif
      * --- Determino la data registrazione... (considero le sole partite di saldo)
      *     Debbo svolgere questo calcolo prima dell'identificazione di eventuali partite
      *     appartenenti a distinte e dopo la determinazione di eventuali raggrupamenti
      ah_Msg("Determinazione data registrazione...")
      * --- Write into TMP_PRCHIUSE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMP_PRCHIUSE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRCHIUSE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MPSERIAL,CPROWNUM"
        do vq_exec with 'gsveebmp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PRCHIUSE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMP_PRCHIUSE.MPSERIAL = _t2.MPSERIAL";
                +" and "+"TMP_PRCHIUSE.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DATREG = _t2.DATREG";
            +i_ccchkf;
            +" from "+i_cTable+" TMP_PRCHIUSE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMP_PRCHIUSE.MPSERIAL = _t2.MPSERIAL";
                +" and "+"TMP_PRCHIUSE.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PRCHIUSE, "+i_cQueryTable+" _t2 set ";
            +"TMP_PRCHIUSE.DATREG = _t2.DATREG";
            +Iif(Empty(i_ccchkf),"",",TMP_PRCHIUSE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMP_PRCHIUSE.MPSERIAL = t2.MPSERIAL";
                +" and "+"TMP_PRCHIUSE.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PRCHIUSE set (";
            +"DATREG";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DATREG";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMP_PRCHIUSE.MPSERIAL = _t2.MPSERIAL";
                +" and "+"TMP_PRCHIUSE.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PRCHIUSE set ";
            +"DATREG = _t2.DATREG";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MPSERIAL = "+i_cQueryTable+".MPSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DATREG = (select DATREG from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Elaborazione Distinte
      *     ======================================================
      *     Recupero le partite legate a distinte, ho due casi
      *     a) La data scadenza + giorni di tolleranza � maggiore della data di fine (w_DATFIN)
      *     b) La data scadenza + giorni di tolleranza � minore uguale della data di fine (w_DATFIN)
      *     
      *     Nel caso a) occore sbiancare il movimento provvigione per cui cancello
      *     la partita dalle chiuse.
      *     
      *     Nel caso b) occorre aggiornare la data maturazione con la data scadenza
      *     pi� i giorni di tolleranza
      *     
      *     Se partita sia in distinta che in isoluto contabilizzato la escludo da questo giro e
      *     la tratto come se fosse chiusa normalmente
      ah_Msg("Determinazione partite in distinte...")
      this.w_READAZI = i_CODAZI
      * --- Lettura giorni rischio
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZGIORIS"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_READAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZGIORIS;
          from (i_cTable) where;
              AZCODAZI = this.w_READAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GIORNI = NVL(cp_ToDate(_read_.AZGIORIS),cp_NullValue(_read_.AZGIORIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DATADIS = this.oParentObject.w_DATFIN - this.w_GIORNI
      * --- Elimino le partite che hanno sia  la data scadenza + giorni di tolleranza superiore alla data finale
      *     e che siano in distinta non ancora contabilizzata / Contabilizzazione indiretta�
      *      
      *     La query esclude eventuali distinte alle quali � succeduto un insoluto.
      *     Per farlo verifica che il Max(Seriale) di prima nota della contabilizzazione 
      *     Distinta sia maggiore nel max(seriale) di prima nota relativo alla contabilizzazione
      *     insoluto
      * --- Delete from TMP_PRCHIUSE
      i_nConn=i_TableProp[this.TMP_PRCHIUSE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRCHIUSE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
              +" and "+i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
              +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
              +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
              +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
      
        do vq_exec with 'gsveibmp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      this.w_DISDOC = i_Rows
      if this.w_GIORNI<>0
        * --- Aggiorno la data registrazione con la data scadenza pi� giorni di
        *     tolleranza sulle partite chiuse
        *     
        *     A T T E N Z I O N E 
        *     La query � utilizzata nella WRITE nel ramo Else nel caso di giorni
        *     trolleranza a 0!
        * --- Select from gsvelbmp
        do vq_exec with 'gsvelbmp',this,'_Curs_gsvelbmp','',.f.,.t.
        if used('_Curs_gsvelbmp')
          select _Curs_gsvelbmp
          locate for 1=1
          do while not(eof())
          this.w_DATREG = cp_todate( _Curs_gsvelbmp.DATSCA ) + this.w_GIORNI
          this.w_DATSCA = cp_todate( _Curs_gsvelbmp.DATSCA ) 
          this.w_NUMPAR = _Curs_gsvelbmp.NUMPAR
          this.w_TIPCON = _Curs_gsvelbmp.TIPCON
          this.w_CODCON = _Curs_gsvelbmp.CODCON
          this.w_CODVAL = _Curs_gsvelbmp.CODVAL
          ah_Msg("Partita %1 del %2 intestatario %3 in distinta...",.T.,.F.,.F.,this.w_NUMPAR, Dtoc(this.w_DATSCA), this.w_CODCON)
          * --- Write into TMP_PRCHIUSE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMP_PRCHIUSE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRCHIUSE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PRCHIUSE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DATREG ="+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'TMP_PRCHIUSE','DATREG');
                +i_ccchkf ;
            +" where ";
                +"DATSCA = "+cp_ToStrODBC(this.w_DATSCA);
                +" and NUMPAR = "+cp_ToStrODBC(this.w_NUMPAR);
                +" and TIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                +" and CODCON = "+cp_ToStrODBC(this.w_CODCON);
                +" and CODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                   )
          else
            update (i_cTable) set;
                DATREG = this.w_DATREG;
                &i_ccchkf. ;
             where;
                DATSCA = this.w_DATSCA;
                and NUMPAR = this.w_NUMPAR;
                and TIPCON = this.w_TIPCON;
                and CODCON = this.w_CODCON;
                and CODVAL = this.w_CODVAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
            select _Curs_gsvelbmp
            continue
          enddo
          use
        endif
      else
        * --- UPDATE TMP_PRCHIUSE SET DATREG=DATSCA WHERE <PARTITA> IN DISTINTA...
        * --- Write into TMP_PRCHIUSE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMP_PRCHIUSE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRCHIUSE_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="DATSCA,NUMPAR,TIPCON,CODCON,CODVAL"
          do vq_exec with 'gsvelbmp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PRCHIUSE_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMP_PRCHIUSE.DATSCA = _t2.DATSCA";
                  +" and "+"TMP_PRCHIUSE.NUMPAR = _t2.NUMPAR";
                  +" and "+"TMP_PRCHIUSE.TIPCON = _t2.TIPCON";
                  +" and "+"TMP_PRCHIUSE.CODCON = _t2.CODCON";
                  +" and "+"TMP_PRCHIUSE.CODVAL = _t2.CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DATREG = _t2.DATSCA";
              +i_ccchkf;
              +" from "+i_cTable+" TMP_PRCHIUSE, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMP_PRCHIUSE.DATSCA = _t2.DATSCA";
                  +" and "+"TMP_PRCHIUSE.NUMPAR = _t2.NUMPAR";
                  +" and "+"TMP_PRCHIUSE.TIPCON = _t2.TIPCON";
                  +" and "+"TMP_PRCHIUSE.CODCON = _t2.CODCON";
                  +" and "+"TMP_PRCHIUSE.CODVAL = _t2.CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PRCHIUSE, "+i_cQueryTable+" _t2 set ";
              +"TMP_PRCHIUSE.DATREG = _t2.DATSCA";
              +Iif(Empty(i_ccchkf),"",",TMP_PRCHIUSE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMP_PRCHIUSE.DATSCA = t2.DATSCA";
                  +" and "+"TMP_PRCHIUSE.NUMPAR = t2.NUMPAR";
                  +" and "+"TMP_PRCHIUSE.TIPCON = t2.TIPCON";
                  +" and "+"TMP_PRCHIUSE.CODCON = t2.CODCON";
                  +" and "+"TMP_PRCHIUSE.CODVAL = t2.CODVAL";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PRCHIUSE set (";
              +"DATREG";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.DATSCA";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMP_PRCHIUSE.DATSCA = _t2.DATSCA";
                  +" and "+"TMP_PRCHIUSE.NUMPAR = _t2.NUMPAR";
                  +" and "+"TMP_PRCHIUSE.TIPCON = _t2.TIPCON";
                  +" and "+"TMP_PRCHIUSE.CODCON = _t2.CODCON";
                  +" and "+"TMP_PRCHIUSE.CODVAL = _t2.CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PRCHIUSE set ";
              +"DATREG = _t2.DATSCA";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
                  +" and "+i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
                  +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                  +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                  +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DATREG = (select DATSCA from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Try
      local bErr_0355A218
      bErr_0355A218=bTrsErr
      this.Try_0355A218()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Impossibile aggiornare data maturazione%0Errore: %1",,"", Message()+" "+Message(1) )
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_0355A218
      * --- End
      this.w_OKDOC = 0
      this.w_oMESS.AddMsgPartNL("Operazione completata")     
      this.w_DIVI = Repl( "-" ,100 )
      this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
      this.w_oPART.addParam(this.w_DIVI)     
      if this.w_TROUBLEDOC>0
        this.w_oPART = this.w_oMESS.addmsgpartNL("Provvigioni per le quali � impossibile determinare la data incasso: %1%0%2")
        this.w_oPART.addParam(ALLTRIM(STR(this.w_TROUBLEDOC)))     
        this.w_oPART.addParam(this.w_DIVI)     
      endif
      * --- Recupero il numero di movimenti con effettivo cambio di data maturazione
      * --- Select from gsvenbmp
      do vq_exec with 'gsvenbmp',this,'_Curs_gsvenbmp','',.f.,.t.
      if used('_Curs_gsvenbmp')
        select _Curs_gsvenbmp
        locate for 1=1
        do while not(eof())
        this.w_TIPO = Nvl( _Curs_gsvenbmp.TIPO ,0 )
        this.w_OKDOC = Nvl( _Curs_gsvenbmp.CONTA ,0 )
        do case
          case this.w_TIPO=1
            this.w_oPART = this.w_oMESS.addmsgpartNL("Provvigioni alle quali � stata riempita la data maturazione: %1")
            this.w_oPART.addParam(ALLTRIM(STR(this.w_OKDOC)))     
          case this.w_TIPO=2
            this.w_oPART = this.w_oMESS.addmsgpartNL("Provvigioni alle quali � stata svuotata la data maturazione: %1")
            this.w_oPART.addParam(ALLTRIM(STR(this.w_OKDOC)))     
          case this.w_TIPO=3
            this.w_oPART = this.w_oMESS.addmsgpartNL("Provvigioni alle quali � stata modificata la data maturazione: %1")
            this.w_oPART.addParam(ALLTRIM(STR(this.w_OKDOC)))     
        endcase
          select _Curs_gsvenbmp
          continue
        enddo
        use
      endif
      * --- Rimuovo i temporanei lato server
      * --- Drop temporary table TMP_PRODIN
      i_nIdx=cp_GetTableDefIdx('TMP_PRODIN')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PRODIN')
      endif
      * --- Drop temporary table TMP_PRCHIUSE
      i_nIdx=cp_GetTableDefIdx('TMP_PRCHIUSE')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PRCHIUSE')
      endif
      if this.w_DISDOC>0
        this.w_oPART = this.w_oMESS.addmsgpartNL("Provvigioni non maturate perch� legate a distinta o a cont.indiretta effetti: %1")
        this.w_oPART.addParam(ALLTRIM(STR(this.w_DISDOC)))     
        this.w_oPART = this.w_oMESS.addmsgpartNL("%1[Verificare la data fine selezione: data scadenza + giorni di tolleranza]")
        this.w_oPART.addParam(Space(10))     
      endif
      this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
      this.w_oPART.addParam(this.w_DIVI)     
      this.w_oPART = this.w_oMESS.addmsgpartNL("Su %1 provvigioni esaminate")
      this.w_oPART.addParam(ALLTRIM(STR(this.w_NUDOC)))     
    endif
    this.w_oMESS.ah_ErrorMsg()     
  endproc
  proc Try_0355A218()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Vado nei movimenti provvigioni presi in esame e sbianco la data registrazione
    ah_Msg("Sbianco la data maturazione...")
    * --- Utilizzo una Visual Query per filtro DATMAT Not Is Null piuttosto che una
    *     frase SQL (utilizza temporaneo TMP_PRODIN)
    * --- Write into MOP_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOP_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MPSERIAL,CPROWNUM"
      do vq_exec with 'gsveobmp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MOP_DETT.MPSERIAL = _t2.MPSERIAL";
              +" and "+"MOP_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MPDATMAT ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /   ")),'MOP_DETT','MPDATMAT');
          +i_ccchkf;
          +" from "+i_cTable+" MOP_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MOP_DETT.MPSERIAL = _t2.MPSERIAL";
              +" and "+"MOP_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOP_DETT, "+i_cQueryTable+" _t2 set ";
      +"MOP_DETT.MPDATMAT ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /   ")),'MOP_DETT','MPDATMAT');
          +Iif(Empty(i_ccchkf),"",",MOP_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MOP_DETT.MPSERIAL = t2.MPSERIAL";
              +" and "+"MOP_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOP_DETT set (";
          +"MPDATMAT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /   ")),'MOP_DETT','MPDATMAT')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MOP_DETT.MPSERIAL = _t2.MPSERIAL";
              +" and "+"MOP_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOP_DETT set ";
      +"MPDATMAT ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /   ")),'MOP_DETT','MPDATMAT');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MPSERIAL = "+i_cQueryTable+".MPSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MPDATMAT ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  /  /   ")),'MOP_DETT','MPDATMAT');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Vado a scrivere nei movimenti provvigioni la data registrazione
    *     risultante
    ah_Msg("Scrivo nuova data maturazione...")
    * --- Utilizzo una Visual Query per filtro DATREG Not Is Null
    * --- Write into MOP_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOP_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MPSERIAL,CPROWNUM"
      do vq_exec with 'gsvembmp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MOP_DETT.MPSERIAL = _t2.MPSERIAL";
              +" and "+"MOP_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MPDATMAT = _t2.DATREG";
          +i_ccchkf;
          +" from "+i_cTable+" MOP_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MOP_DETT.MPSERIAL = _t2.MPSERIAL";
              +" and "+"MOP_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOP_DETT, "+i_cQueryTable+" _t2 set ";
          +"MOP_DETT.MPDATMAT = _t2.DATREG";
          +Iif(Empty(i_ccchkf),"",",MOP_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MOP_DETT.MPSERIAL = t2.MPSERIAL";
              +" and "+"MOP_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOP_DETT set (";
          +"MPDATMAT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.DATREG";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MOP_DETT.MPSERIAL = _t2.MPSERIAL";
              +" and "+"MOP_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOP_DETT set ";
          +"MPDATMAT = _t2.DATREG";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MPSERIAL = "+i_cQueryTable+".MPSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MPDATMAT = (select DATREG from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='MOP_DETT'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='PNT_MAST'
    this.cWorkTables[5]='*TMP_PRODIN'
    this.cWorkTables[6]='*TMP_PRCHIUSE'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_TMP_PRODIN')
      use in _Curs_TMP_PRODIN
    endif
    if used('_Curs_gsvefbmp')
      use in _Curs_gsvefbmp
    endif
    if used('_Curs_gsvelbmp')
      use in _Curs_gsvelbmp
    endif
    if used('_Curs_gsvenbmp')
      use in _Curs_gsvenbmp
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
