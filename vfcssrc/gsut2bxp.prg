* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut2bxp                                                        *
*              Verifica tracciato selezionato                                  *
*                                                                              *
*      Author: Pollina Fabrizio                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-20                                                      *
* Last revis.: 2005-07-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut2bxp",oParentObject)
return(i_retval)

define class tgsut2bxp as StdBatch
  * --- Local variables
  w_CTRL = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case "%LOCATION%" $ UPPER(this.oParentObject.w_ZCPPFOLDER) AND EMPTY(g_ZCPLOCATION)
        Ah_Errormsg("Il codice folder selezionato contiene il parametro %LOCATION%%0Impossibile utilizzarlo con questo tipo d'invio")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case "%YEAR%" $ UPPER(this.oParentObject.w_ZCPPFOLDER) AND EMPTY(g_ZCPYEAR)
        Ah_Errormsg("Il codice folder selezionato contiene il parametro %YEAR%%0Impossibile utilizzarlo con questo tipo d'invio")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case "%MONTH%" $ UPPER(this.oParentObject.w_ZCPPFOLDER) AND EMPTY(g_ZCPMONTH)
        Ah_Errormsg("Il codice folder selezionato contiene il parametro %MONTH%%0Impossibile utilizzarlo con questo tipo d'invio")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case "%DOC_TYPE%" $ UPPER(this.oParentObject.w_ZCPPFOLDER) AND EMPTY(g_ZCPDOCTYPE)
        Ah_Errormsg("Il codice folder selezionato contiene il parametro %DOC_TYPE%%0Impossibile utilizzarlo con questo tipo d'invio")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      otherwise
        * --- Ricalcolo la stringa corretta
        this.oParentObject.w_ZCPPFOLDER = STRTRAN(this.oParentObject.w_ZCPPFOLDER, "%LOCATION%", g_ZCPLOCATION )
        this.oParentObject.w_ZCPPFOLDER = STRTRAN(this.oParentObject.w_ZCPPFOLDER, "%YEAR%", g_ZCPYEAR )
        this.oParentObject.w_ZCPPFOLDER = STRTRAN(this.oParentObject.w_ZCPPFOLDER, "%MONTH%", g_ZCPMONTH )
        this.oParentObject.w_ZCPPFOLDER = STRTRAN(this.oParentObject.w_ZCPPFOLDER, "%DOC_TYPE%", g_ZCPDOCTYPE )
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CTRL = this.oParentObject.GetCtrl("w_ZCPCFOLDER")
    this.w_CTRL.SetFocus()     
    this.w_CTRL = .NULL.
    this.oParentObject.w_ZCPCFOLDER = 0
    this.oParentObject.w_ZCPDFOLDER = SPACE(50)
    this.oParentObject.w_ZCPFDESCRI = SPACE(10)
    this.oParentObject.w_ZCPPFOLDER = SPACE(254)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
