* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bdl                                                        *
*              Driver per terminali datalogic (rettifiche)                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_86]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-13                                                      *
* Last revis.: 2014-05-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bdl",oParentObject,m.pCursore,m.pTXTFile,m.pObjMsg,m.p_ArrayFieldName,m.p_ArrayFieldPosition)
return(i_retval)

define class tgsut_bdl as StdBatch
  * --- Local variables
  pCursore = space(10)
  pTXTFile = space(254)
  pObjMsg = .NULL.
  p_ArrayFieldName = space(40)
  p_ArrayFieldPosition = space(40)
  w_ErrIns = .f.
  W_numrip = 0
  w_TEMPCURS = space(10)
  w_TEMPFILE = space(254)
  w_StrDaIns = space(0)
  w_oWSH = .NULL.
  w_FileToRead = space(254)
  w_OutFile = space(254)
  w_MESS = space(250)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Driver per datalogic.
    *     ========================================================
    *     Utilizzare questo batch come driver per caricare dati da foglio elettronico
    *     tramite l'INSERIMENTO DATI (GSAR_BPG=>GSAR_MPG).
    * --- La procedura riceve come parametro il nome del cursore che dovr� riempire.
    *     La struttura del cursore � fissa ed �:
    *     
    *     Create Cursor ( w_CURSORE ) ( CODICE C(20) NULL, UNIMIS C(3) NULL, QTAMOV N(12,3) NULL, PREZZO N(18,5) NULL,;
    *     LOTTO CHAR(20) NULL , CODMAG C(5) NULL,UBICAZIONE C(20) NULL , MATRICOLA C(40) NULL,SERRIF C(10) NULL )
    *     
    *     All'interno del batch vi � una chiamata per aprire una maschera di selezione 
    *     per caricare un file (*.csv) se questo non � stato precedentemente settato
    *     Se il secondo parametro � gi� stato valorizzato questa non apparir�.
    *     Il secondo parametro rappresenta appunto il file csv (con path relativo o assoluto)
    *     dal quale la procedura prelever� i dati. (Da specificare, se fisso, nell'anagrafica
    *     dispositivi nella casella parametro)
    *     
    *     Il terzo parametro rappresenta l'oggetto da utilizzare per visualizzare i messaggi (deve contenere un campo memo w_MSG).
    *     
    *     Il quarto e quinto parametro contengono le informazioni riguardanti posizione e nome campo (sono array passati come riferimento)
    *     In questo driver  non sono utilizzati, MA E' NECESSARIO CHE SIANO PRESENTI PER EVITARE SPIACEVOLI MALFUNZIONAMENTI!
    *     
    * --- Messaggistica ed errori
    * --- E' necessario inizializzare le variabili per evitare
    *     che la insert nel cursore pCursore dia errore.
     
 L_CODICE=" " 
 L_UNIMIS=" " 
 L_QTAMOV=0 
 L_PREZZO=0 
 L_LOTTO=" " 
 L_CODMAG=" " 
 L_UBICAZIONE=" " 
 L_MATRICOLA=" " 
 L_SERRIF=" "
    * --- Inizio Elaborazione...
    this.W_numrip = 60
    * --- Verifico la presenza dell'eseguibile Rapicopy per eseguire la copia del file
    if EMPTY (this.pTXTFile)
      AH_ERRORMSG("Il file %1 non esiste",48,"",this.pTXTFile)
      i_retcode = 'stop'
      return
    endif
    if !cp_fileexist("Rapicopy.exe")
      AH_ERRORMSG("RapiCopy.exe non trovato.%0Impossibile proseguire",48)
      i_retcode = 'stop'
      return
    else
      L_OldErr = ON("ERROR")
      L_Err = .F.
      ON ERROR L_Err=.T.
      this.w_oWSH = CREATEOBJECT("WScript.Shell")
      this.w_FileToRead = "\BACKUP\Pal\Data\OutFile\"+ALLTRIM(this.pTXTFile)
      this.w_OutFile = ADDBS(ALLTRIM(G_tempadhoc))+SYS(2015)+".csv"
      if !L_Err
        * --- Creazione oggetto scripting host ok, proseguo con l'esecuzione di 
        *     rapicopy per ottenere il file e la risposta dell'elaborazione
        this.w_oWSH = CREATEOBJECT("WScript.Shell")
        AddMsgNL("File selezionato %1",this.pObjMsg,ALLTRIM(this.pTXTFile) )
        * --- && ###start remove from setup###
        if g_ADHOCONE
          this.w_ErrIns = this.w_oWSH.Run('..\..\exe\RapiCopy -r "' + this.w_FileToRead + '" "' + this.w_OutFile + '" ',0,.T.) = 1
        else
          * --- && ###end remove from setup###
          this.w_ErrIns = this.w_oWSH.Run('RapiCopy -r "' + this.w_FileToRead + '" "' + this.w_OutFile + '" ',0,.T.) = 1
          * --- && ###start remove from setup###
        endif
        * --- && ###end remove from setup###
        if this.w_ErrIns
          * --- File non trovato, provo nell'altra posizione conosciuta
          this.w_FileToRead = "\Program Files\Datalogic\Pal\Data\OutFile\"+ALLTRIM(this.pTXTFile)
          * --- && ###start remove from setup###
          if g_ADHOCONE
            this.w_ErrIns = this.w_oWSH.Run('..\..\exe\RapiCopy -r "' + this.w_FileToRead + '" "' + this.w_OutFile + '" ',0,.T.) = 1
          else
            * --- && ###end remove from setup###
            this.w_ErrIns = this.w_oWSH.Run('RapiCopy -r "' + this.w_FileToRead + '" "' + this.w_OutFile + '" ',0,.T.) = 1
            * --- && ###start remove from setup###
          endif
          * --- && ###end remove from setup###
        endif
        this.w_oWSH = .NULL.
      else
        * --- Creazione oggetto scripting host fallita, proseguo con l'esecuzione di 
        *     rapicopy con il comando standard fox e verifico la dimensione del file
        RUN 'RapiCopy -r "' + this.w_FileToRead + '" "' + this.w_OutFile + '" '
        this.w_ErrIns = !cp_FileExist(this.w_OutFile) Or LEN(FILETOSTR(this.w_OutFile))=0
        if this.w_ErrIns
          * --- File non trovato, provo nell'altra posizione conosciuta
          this.w_FileToRead = "\Program Files\Datalogic\Pal\Data\OutFile\"+ALLTRIM(this.pTXTFile)
          RUN 'RapiCopy -r "' + this.w_FileToRead + '" "' + this.w_OutFile + '" '
        endif
      endif
      * --- Verifico se il file � presente nella destinazione
      *     Eseguo qui il controllo in modo da verificare la corretta
      *     esecuzione della copia anche per il caso dell'utilizzo di
      *     windows scripting host e non solo per il caso del comando run
      this.w_ErrIns = !cp_FileExist(this.w_OutFile) Or LEN(FILETOSTR(this.w_OutFile))=0
      if this.w_ErrIns
        AH_ERRORMSG("Il file %1 non esiste nel terminale o non � stato possibile leggerlo",48,"",this.pTXTFile)
      endif
      ON ERROR &L_OldErr
    endif
    this.w_TEMPCURS = SYS(2015)
    if !this.w_ErrIns
      AddMsgNL("File selezionato %1",this.pObjMsg,ALLTRIM(this.w_OutFile) )
      AddMsgNL("%1",this.pObjMsg,REPLICATE(chr(45),this.w_NumRip))
      AddMsgNL("Fase 1: apertura file...%0%1",this.pObjMsg,REPLICATE(chr(45),this.w_NumRip))
      AddMsgNL("%1",this.pObjMsg,REPLICATE(chr(45),this.w_NumRip))
      this.w_TEMPFILE = ADDBS(ALLTRIM(G_tempadhoc))+SYS(2015)+".csv"
      * --- Leggo il file e metto una prima riga fittizia altrimenti foxpro non importa i dati
      STRTOFILE( REPLICATE("=", 80)+CHR(13)+CHR(10)+FILETOSTR(this.w_OutFile) , this.w_TEMPFILE )
      CREATE CURSOR (this.w_TEMPCURS) (Documento N(6), Addetto C(5), Magazzino C(5), Ubicazione C(20), Matricola C(40), Articolo C(41), Lotto C(20), UNIMIS C(3), QTAMOV N(16,3))
      SELECT ( this.w_TEMPCURS)
      L_oldDecSep= SET("POINT") 
 SET POINT TO . 
 APPEND FROM (this.w_TEMPFILE) TYPE DELIMITED 
 SET POINT TO (L_oldDecSep)
      DELETE FILE (this.w_TEMPFILE)
      DELETE FILE (this.w_OutFile)
      AddMsgNL("Fase 2: verifica file...%0%1",this.pObjMsg,REPLICATE(chr(45),this.w_NumRip))
    endif
    if USED(this.w_TEMPCURS) AND reccount ( this.w_TEMPCURS ) > 0
      AddMsgNL("%1",this.pObjMsg,REPLICATE(chr(45),this.w_NumRip))
      AddMsgNL("Fase 3: inserimento informazioni...%0%1",this.pObjMsg,REPLICATE(chr(45),this.w_NumRip))
      SELECT ( this.w_TEMPCURS)
      GO TOP
      SCAN
      L_CODICE = NVL( Articolo," ")
      L_UNIMIS = NVL(UNIMIS," ")
      L_QTAMOV = NVL(QTAMOV,0)
      L_LOTTO = NVL(LOTTO," ")
      L_CODMAG = NVL(MAGAZZINO," ")
      L_UBICAZIONE = NVL(UBICAZIONE," ")
      L_MATRICOLA = NVL(MATRICOLA," ")
      if type("PREZZO")<>"U" and type("PREZZO")="N"
        L_PREZZO=Nvl(PREZZO,0)
      endif
      if type("SERRIF")<>"U" and type("SERRIF")$"MC"
        L_SERRIF=left(Nvl(SERRIF," "),10)
      endif
      if Type("pObjMsg")="O"
        this.w_MESS = Ah_MsgFormat("Inserimento record")
        if AT(this.w_MESS,this.pObjMsg.w_Msg)>0
          this.w_StrDaIns = LEFT(this.pObjMsg.w_Msg,AT(this.w_MESS,this.pObjMsg.w_Msg)-1)
          this.pObjMsg.w_MSG=""
        else
          this.w_StrDaIns = ""
        endif
      endif
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Inserimento record")
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("%0Codice ricerca: %1",L_CODICE)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("%1u.mis.: %2%0", chr(45), L_UNIMIS)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Quantit�: %1", str(L_QTAMOV,12,7) )
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("%1prezzo: %2%0", chr(45), str(L_PREZZO,20,4) )
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Lotto: %1%0",L_LOTTO)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Magazzino: %1%0", L_CODMAG)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Ubicazione: %1%0", L_UBICAZIONE)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Matricola: %1%0", L_MATRICOLA)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Rif origine: %1%0", L_SERRIF)
      AddMsgNL("%1", this.oparentobject, this.w_StrDaIns)
      L_OldErr = ON("ERROR")
      this.w_ErrIns = .F.
      ON ERROR this.w_ErrIns=.T.
       
 INSERT INTO ( this.pCursore ) (CODICE,UNIMIS,QTAMOV,PREZZO,LOTTO,CODMAG,UBICAZIONE,MATRICOLA,SERRIF) ; 
 VALUES (L_CODICE,L_UNIMIS,L_QTAMOV,L_PREZZO,L_LOTTO,L_CODMAG,L_UBICAZIONE,L_MATRICOLA,L_SERRIF)
      if this.w_ErrIns
        AH_ERRORMSG("Errore inserimento dati cursore %1",48,"",MESSAGE())
      endif
      ON ERROR &L_OldErr
      ENDSCAN
    else
      AddMsgNL("%1Riscontrati errori nel file, procedura interrotta%1",this.pObjMsg, replicate(chr(42),3))
      AH_ERRORMSG("Il file selezionato non contiene dati.",48)
    endif
    USE IN SELECT (this.w_TEMPCURS )
  endproc


  proc Init(oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition)
    this.pCursore=pCursore
    this.pTXTFile=pTXTFile
    this.pObjMsg=pObjMsg
    this.p_ArrayFieldName=p_ArrayFieldName
    this.p_ArrayFieldPosition=p_ArrayFieldPosition
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition"
endproc
