* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_sli                                                        *
*              Confronto listini                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_85]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-17                                                      *
* Last revis.: 2011-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_sli",oParentObject))

* --- Class definition
define class tgsma_sli as StdForm
  Top    = 1
  Left   = 16

  * --- Standard Properties
  Width  = 669
  Height = 405
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-09"
  HelpContextID=60130455
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=42

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  CONTI_IDX = 0
  LISTINI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  TRADARTI_IDX = 0
  LINGUE_IDX = 0
  cPrg = "gsma_sli"
  cComment = "Confronto listini"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_tipo = space(10)
  o_tipo = space(10)
  w_codlis1 = space(5)
  w_desl1 = space(40)
  w_valu1 = space(3)
  w_codlis2 = space(5)
  w_desl2 = space(40)
  w_valu2 = space(3)
  w_codlis3 = space(5)
  w_desl3 = space(40)
  w_valu3 = space(3)
  w_DATA1 = ctod('  /  /  ')
  o_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_bcodi = space(20)
  w_desi = space(40)
  w_ecodi = space(20)
  w_desi1 = space(40)
  w_grumer1 = space(5)
  w_desg1 = space(30)
  w_grumer2 = space(5)
  w_desg2 = space(30)
  w_catomo = space(5)
  w_desCAT = space(30)
  w_produ1 = space(15)
  w_desF = space(40)
  w_produ2 = space(15)
  w_desF2 = space(40)
  w_TIPCONTO = space(1)
  w_CODAZ1 = space(10)
  w_TIPO1 = space(1)
  w_TIPO2 = space(1)
  w_TIPO3 = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ONUME = 0
  w_datin1 = ctod('  /  /  ')
  w_datin2 = ctod('  /  /  ')
  w_datin3 = ctod('  /  /  ')
  w_OBS1 = ctod('  /  /  ')
  w_OBS2 = ctod('  /  /  ')
  w_OBS3 = ctod('  /  /  ')
  w_NOUGUALI = space(1)
  w_TIPUMI = 0
  * --- Area Manuale = Declare Variables
  * --- gsma_sli
  * Lancia la stampa
  proc Confronta()
  * --- Assegnazione valori di default
      if empty(this.w_codlis2)
        this.w_codlis2 = this.w_codlis1
      endif
      if empty(this.w_codlis3)
        this.w_codlis3 = this.w_codlis1
      endif
      if empty(this.w_data2)
        this.w_data2 = g_finese
      endif
      if empty(this.w_data1)
        this.w_data1 = g_iniese
      endif
  * --- Definizione variabili utilizzate nei report
      L_d1=this.w_data1
      L_c1=this.w_codlis1
      L_d2=this.w_data2
      L_c2=this.w_codlis2
      L_val1=this.w_valu1
      L_c3=this.w_codlis3
      L_val2=this.w_valu2
      L_TIPO=this.w_TIPO
      L_val3=this.w_valu3
      L_NOUGUALI=this.w_NOUGUALI
  * --- Lancio report
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
  * --- Ripristina il valore delle variabili per la maschera
      if this.w_codlis3=this.w_codlis1
        this.w_codlis3 = space(5)
      endif
      if this.w_codlis2=this.w_codlis1
        this.w_codlis2 = space(5)
      endif
  * --- Chiudo il cursore __tmp__
      if (used("__TMP__"))
        select __TMP__
        use
      endif
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_sliPag1","gsma_sli",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.otipo_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='GRUMERC'
    this.cWorkTables[6]='CATEGOMO'
    this.cWorkTables[7]='TRADARTI'
    this.cWorkTables[8]='LINGUE'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Confronta
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsma_sli
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_tipo=space(10)
      .w_codlis1=space(5)
      .w_desl1=space(40)
      .w_valu1=space(3)
      .w_codlis2=space(5)
      .w_desl2=space(40)
      .w_valu2=space(3)
      .w_codlis3=space(5)
      .w_desl3=space(40)
      .w_valu3=space(3)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_bcodi=space(20)
      .w_desi=space(40)
      .w_ecodi=space(20)
      .w_desi1=space(40)
      .w_grumer1=space(5)
      .w_desg1=space(30)
      .w_grumer2=space(5)
      .w_desg2=space(30)
      .w_catomo=space(5)
      .w_desCAT=space(30)
      .w_produ1=space(15)
      .w_desF=space(40)
      .w_produ2=space(15)
      .w_desF2=space(40)
      .w_TIPCONTO=space(1)
      .w_CODAZ1=space(10)
      .w_TIPO1=space(1)
      .w_TIPO2=space(1)
      .w_TIPO3=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ONUME=0
      .w_datin1=ctod("  /  /  ")
      .w_datin2=ctod("  /  /  ")
      .w_datin3=ctod("  /  /  ")
      .w_OBS1=ctod("  /  /  ")
      .w_OBS2=ctod("  /  /  ")
      .w_OBS3=ctod("  /  /  ")
      .w_NOUGUALI=space(1)
      .w_TIPUMI=0
        .w_tipo = 'N'
        .w_codlis1 = space(5)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_codlis1))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_codlis2 = space(5)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_codlis2))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,7,.f.)
        .w_codlis3 = space(5)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_codlis3))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,10,.f.)
        .w_DATA1 = g_iniese
        .w_DATA2 = g_finese
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_bcodi))
          .link_1_13('Full')
        endif
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_ecodi))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,17,.f.)
        if not(empty(.w_grumer1))
          .link_1_17('Full')
        endif
        .DoRTCalc(18,19,.f.)
        if not(empty(.w_grumer2))
          .link_1_19('Full')
        endif
        .DoRTCalc(20,21,.f.)
        if not(empty(.w_catomo))
          .link_1_21('Full')
        endif
        .DoRTCalc(22,23,.f.)
        if not(empty(.w_produ1))
          .link_1_23('Full')
        endif
        .DoRTCalc(24,25,.f.)
        if not(empty(.w_produ2))
          .link_1_25('Full')
        endif
          .DoRTCalc(26,26,.f.)
        .w_TIPCONTO = 'F'
        .w_CODAZ1 = I_CODAZI
      .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
          .DoRTCalc(29,31,.f.)
        .w_OBTEST = IIF(EMPTY(.w_DATA1),i_INIDAT,.w_DATA1)
          .DoRTCalc(33,41,.f.)
        .w_TIPUMI = 0
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- gsma_sli
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_tipo<>.w_tipo.or. .o_DATA1<>.w_DATA1
            .w_codlis1 = space(5)
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.t.)
        if .o_tipo<>.w_tipo.or. .o_DATA1<>.w_DATA1
            .w_codlis2 = space(5)
          .link_1_5('Full')
        endif
        .DoRTCalc(6,7,.t.)
        if .o_tipo<>.w_tipo.or. .o_DATA1<>.w_DATA1
            .w_codlis3 = space(5)
          .link_1_8('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .DoRTCalc(9,31,.t.)
        if .o_DATA1<>.w_DATA1
            .w_OBTEST = IIF(EMPTY(.w_DATA1),i_INIDAT,.w_DATA1)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(33,42,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=codlis1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codlis1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_codlis1)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_codlis1))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_codlis1)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_codlis1)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_codlis1)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_codlis1) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'ocodlis1_1_2'),i_cWhere,'GSAR_ALI',"Listini",'GSAR_SLI.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codlis1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_codlis1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_codlis1)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codlis1 = NVL(_Link_.LSCODLIS,space(5))
      this.w_desl1 = NVL(_Link_.LSDESLIS,space(40))
      this.w_valu1 = NVL(_Link_.LSVALLIS,space(3))
      this.w_tipo1 = NVL(_Link_.LSIVALIS,space(1))
      this.w_datin1 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_OBS1 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_codlis1 = space(5)
      endif
      this.w_desl1 = space(40)
      this.w_valu1 = space(3)
      this.w_tipo1 = space(1)
      this.w_datin1 = ctod("  /  /  ")
      this.w_OBS1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKLIST(.w_codlis1,.w_codlis2,.w_codlis3,.w_tipo,.w_tipo1) .and. .w_Tipo1=.w_Tipo and  (EMPTY(.w_datin1) or .w_datin1<=.w_data1 or .w_datin1<=.w_data2) AND (EMPTY(.w_OBS1) OR .w_OBS1>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino gi� selezionato o tipo non corretto o non valido")
        endif
        this.w_codlis1 = space(5)
        this.w_desl1 = space(40)
        this.w_valu1 = space(3)
        this.w_tipo1 = space(1)
        this.w_datin1 = ctod("  /  /  ")
        this.w_OBS1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codlis1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=codlis2
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codlis2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_codlis2)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_codlis2))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_codlis2)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_codlis2)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_codlis2)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_codlis2) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'ocodlis2_1_5'),i_cWhere,'GSAR_ALI',"Listini",'GSAR_SLI.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codlis2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_codlis2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_codlis2)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codlis2 = NVL(_Link_.LSCODLIS,space(5))
      this.w_desl2 = NVL(_Link_.LSDESLIS,space(40))
      this.w_valu2 = NVL(_Link_.LSVALLIS,space(3))
      this.w_tipo2 = NVL(_Link_.LSIVALIS,space(1))
      this.w_datin2 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_OBS2 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_codlis2 = space(5)
      endif
      this.w_desl2 = space(40)
      this.w_valu2 = space(3)
      this.w_tipo2 = space(1)
      this.w_datin2 = ctod("  /  /  ")
      this.w_OBS2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKLIST(.w_codlis1,.w_codlis2,.w_codlis3,.w_tipo,.w_tipo2) .and. (.w_Tipo2=.w_Tipo or EMPTY(.w_TIPO2)) and (EMPTY(.w_datin2) or .w_datin2<=.w_data1 or .w_datin2<=.w_data2) AND (EMPTY(.w_OBS2) OR .w_OBS2>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino gi� selezionato o tipo non corretto o non valido")
        endif
        this.w_codlis2 = space(5)
        this.w_desl2 = space(40)
        this.w_valu2 = space(3)
        this.w_tipo2 = space(1)
        this.w_datin2 = ctod("  /  /  ")
        this.w_OBS2 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codlis2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=codlis3
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codlis3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_codlis3)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_codlis3))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_codlis3)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_codlis3)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_codlis3)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_codlis3) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'ocodlis3_1_8'),i_cWhere,'GSAR_ALI',"Listini",'GSAR_SLI.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codlis3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_codlis3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_codlis3)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codlis3 = NVL(_Link_.LSCODLIS,space(5))
      this.w_desl3 = NVL(_Link_.LSDESLIS,space(40))
      this.w_valu3 = NVL(_Link_.LSVALLIS,space(3))
      this.w_tipo3 = NVL(_Link_.LSIVALIS,space(1))
      this.w_datin3 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_OBS3 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_codlis3 = space(5)
      endif
      this.w_desl3 = space(40)
      this.w_valu3 = space(3)
      this.w_tipo3 = space(1)
      this.w_datin3 = ctod("  /  /  ")
      this.w_OBS3 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKLIST(.w_codlis1,.w_codlis2,.w_codlis3,.w_tipo,.w_tipo3) .and.  (.w_Tipo3=.w_Tipo or EMPTY(.w_TIPO3)) and (EMPTY(.w_datin3) or .w_datin3<=.w_data1 or .w_datin3<= .w_data2) AND (EMPTY(.w_OBS3) OR .w_OBS3>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino gi� selezionato o tipo non corretto o non valido")
        endif
        this.w_codlis3 = space(5)
        this.w_desl3 = space(40)
        this.w_valu3 = space(3)
        this.w_tipo3 = space(1)
        this.w_datin3 = ctod("  /  /  ")
        this.w_OBS3 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codlis3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=bcodi
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_bcodi) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_bcodi)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_bcodi))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_bcodi)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_bcodi)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_bcodi)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_bcodi) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'obcodi_1_13'),i_cWhere,'GSMA_BZA',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_bcodi)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_bcodi);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_bcodi)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_bcodi = NVL(_Link_.ARCODART,space(20))
      this.w_desi = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_bcodi = space(20)
      endif
      this.w_desi = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_ECODI)) OR  (upper(.w_BCODI)<=upper(.w_ECODI))) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_bcodi = space(20)
        this.w_desi = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_bcodi Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ecodi
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ecodi) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ecodi)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ecodi))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ecodi)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_ecodi)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_ecodi)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ecodi) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oecodi_1_15'),i_cWhere,'GSMA_BZA',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ecodi)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ecodi);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ecodi)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ecodi = NVL(_Link_.ARCODART,space(20))
      this.w_desi1 = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ecodi = space(20)
      endif
      this.w_desi1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((upper(.w_ECODI)>= upper(.w_BCODI)) or (empty(.w_Bcodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_ecodi = space(20)
        this.w_desi1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ecodi Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=grumer1
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_grumer1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_grumer1)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_grumer1))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_grumer1)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_grumer1) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'ogrumer1_1_17'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_grumer1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_grumer1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_grumer1)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_grumer1 = NVL(_Link_.GMCODICE,space(5))
      this.W_desg1 = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_grumer1 = space(5)
      endif
      this.W_desg1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_grumer2)) OR  (.w_grumer1<=.w_grumer2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_grumer1 = space(5)
        this.W_desg1 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_grumer1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=grumer2
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_grumer2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_grumer2)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_grumer2))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_grumer2)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_grumer2) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'ogrumer2_1_19'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_grumer2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_grumer2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_grumer2)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_grumer2 = NVL(_Link_.GMCODICE,space(5))
      this.W_desg2 = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_grumer2 = space(5)
      endif
      this.W_desg2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_grumer2>=.w_grumer1) or (empty(.w_grumer1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_grumer2 = space(5)
        this.W_desg2 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_grumer2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=catomo
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_catomo) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_catomo)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_catomo))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_catomo)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_catomo) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'ocatomo_1_21'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_catomo)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_catomo);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_catomo)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_catomo = NVL(_Link_.OMCODICE,space(5))
      this.w_descat = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_catomo = space(5)
      endif
      this.w_descat = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_catomo Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=produ1
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_produ1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_produ1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TipConto;
                     ,'ANCODICE',trim(this.w_produ1))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_produ1)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_produ1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_produ1)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TipConto);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_produ1) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oprodu1_1_23'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipConto<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_produ1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_produ1);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TipConto;
                       ,'ANCODICE',this.w_produ1)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_produ1 = NVL(_Link_.ANCODICE,space(15))
      this.W_DESF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_produ1 = space(15)
      endif
      this.W_DESF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_produ2)) OR  (.w_produ1<=.w_produ2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_produ1 = space(15)
        this.W_DESF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_produ1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=produ2
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_produ2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_produ2)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TipConto;
                     ,'ANCODICE',trim(this.w_produ2))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_produ2)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_produ2)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_produ2)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TipConto);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_produ2) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oprodu2_1_25'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipConto<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_produ2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_produ2);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TipConto;
                       ,'ANCODICE',this.w_produ2)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_produ2 = NVL(_Link_.ANCODICE,space(15))
      this.W_DESF2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_produ2 = space(15)
      endif
      this.W_DESF2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_produ1)) OR  (.w_produ1<=.w_produ2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_produ2 = space(15)
        this.W_DESF2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_produ2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.otipo_1_1.RadioValue()==this.w_tipo)
      this.oPgFrm.Page1.oPag.otipo_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ocodlis1_1_2.value==this.w_codlis1)
      this.oPgFrm.Page1.oPag.ocodlis1_1_2.value=this.w_codlis1
    endif
    if not(this.oPgFrm.Page1.oPag.odesl1_1_3.value==this.w_desl1)
      this.oPgFrm.Page1.oPag.odesl1_1_3.value=this.w_desl1
    endif
    if not(this.oPgFrm.Page1.oPag.ovalu1_1_4.value==this.w_valu1)
      this.oPgFrm.Page1.oPag.ovalu1_1_4.value=this.w_valu1
    endif
    if not(this.oPgFrm.Page1.oPag.ocodlis2_1_5.value==this.w_codlis2)
      this.oPgFrm.Page1.oPag.ocodlis2_1_5.value=this.w_codlis2
    endif
    if not(this.oPgFrm.Page1.oPag.odesl2_1_6.value==this.w_desl2)
      this.oPgFrm.Page1.oPag.odesl2_1_6.value=this.w_desl2
    endif
    if not(this.oPgFrm.Page1.oPag.ovalu2_1_7.value==this.w_valu2)
      this.oPgFrm.Page1.oPag.ovalu2_1_7.value=this.w_valu2
    endif
    if not(this.oPgFrm.Page1.oPag.ocodlis3_1_8.value==this.w_codlis3)
      this.oPgFrm.Page1.oPag.ocodlis3_1_8.value=this.w_codlis3
    endif
    if not(this.oPgFrm.Page1.oPag.odesl3_1_9.value==this.w_desl3)
      this.oPgFrm.Page1.oPag.odesl3_1_9.value=this.w_desl3
    endif
    if not(this.oPgFrm.Page1.oPag.ovalu3_1_10.value==this.w_valu3)
      this.oPgFrm.Page1.oPag.ovalu3_1_10.value=this.w_valu3
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_11.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_11.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_12.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_12.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.obcodi_1_13.value==this.w_bcodi)
      this.oPgFrm.Page1.oPag.obcodi_1_13.value=this.w_bcodi
    endif
    if not(this.oPgFrm.Page1.oPag.odesi_1_14.value==this.w_desi)
      this.oPgFrm.Page1.oPag.odesi_1_14.value=this.w_desi
    endif
    if not(this.oPgFrm.Page1.oPag.oecodi_1_15.value==this.w_ecodi)
      this.oPgFrm.Page1.oPag.oecodi_1_15.value=this.w_ecodi
    endif
    if not(this.oPgFrm.Page1.oPag.odesi1_1_16.value==this.w_desi1)
      this.oPgFrm.Page1.oPag.odesi1_1_16.value=this.w_desi1
    endif
    if not(this.oPgFrm.Page1.oPag.ogrumer1_1_17.value==this.w_grumer1)
      this.oPgFrm.Page1.oPag.ogrumer1_1_17.value=this.w_grumer1
    endif
    if not(this.oPgFrm.Page1.oPag.odesg1_1_18.value==this.w_desg1)
      this.oPgFrm.Page1.oPag.odesg1_1_18.value=this.w_desg1
    endif
    if not(this.oPgFrm.Page1.oPag.ogrumer2_1_19.value==this.w_grumer2)
      this.oPgFrm.Page1.oPag.ogrumer2_1_19.value=this.w_grumer2
    endif
    if not(this.oPgFrm.Page1.oPag.odesg2_1_20.value==this.w_desg2)
      this.oPgFrm.Page1.oPag.odesg2_1_20.value=this.w_desg2
    endif
    if not(this.oPgFrm.Page1.oPag.ocatomo_1_21.value==this.w_catomo)
      this.oPgFrm.Page1.oPag.ocatomo_1_21.value=this.w_catomo
    endif
    if not(this.oPgFrm.Page1.oPag.odesCAT_1_22.value==this.w_desCAT)
      this.oPgFrm.Page1.oPag.odesCAT_1_22.value=this.w_desCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oprodu1_1_23.value==this.w_produ1)
      this.oPgFrm.Page1.oPag.oprodu1_1_23.value=this.w_produ1
    endif
    if not(this.oPgFrm.Page1.oPag.odesF_1_24.value==this.w_desF)
      this.oPgFrm.Page1.oPag.odesF_1_24.value=this.w_desF
    endif
    if not(this.oPgFrm.Page1.oPag.oprodu2_1_25.value==this.w_produ2)
      this.oPgFrm.Page1.oPag.oprodu2_1_25.value=this.w_produ2
    endif
    if not(this.oPgFrm.Page1.oPag.odesF2_1_26.value==this.w_desF2)
      this.oPgFrm.Page1.oPag.odesF2_1_26.value=this.w_desF2
    endif
    if not(this.oPgFrm.Page1.oPag.oNOUGUALI_1_62.RadioValue()==this.w_NOUGUALI)
      this.oPgFrm.Page1.oPag.oNOUGUALI_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPUMI_1_63.RadioValue()==this.w_TIPUMI)
      this.oPgFrm.Page1.oPag.oTIPUMI_1_63.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_codlis1)) or not(CHKLIST(.w_codlis1,.w_codlis2,.w_codlis3,.w_tipo,.w_tipo1) .and. .w_Tipo1=.w_Tipo and  (EMPTY(.w_datin1) or .w_datin1<=.w_data1 or .w_datin1<=.w_data2) AND (EMPTY(.w_OBS1) OR .w_OBS1>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ocodlis1_1_2.SetFocus()
            i_bnoObbl = !empty(.w_codlis1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino gi� selezionato o tipo non corretto o non valido")
          case   not(CHKLIST(.w_codlis1,.w_codlis2,.w_codlis3,.w_tipo,.w_tipo2) .and. (.w_Tipo2=.w_Tipo or EMPTY(.w_TIPO2)) and (EMPTY(.w_datin2) or .w_datin2<=.w_data1 or .w_datin2<=.w_data2) AND (EMPTY(.w_OBS2) OR .w_OBS2>.w_OBTEST))  and not(empty(.w_codlis2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ocodlis2_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino gi� selezionato o tipo non corretto o non valido")
          case   not(CHKLIST(.w_codlis1,.w_codlis2,.w_codlis3,.w_tipo,.w_tipo3) .and.  (.w_Tipo3=.w_Tipo or EMPTY(.w_TIPO3)) and (EMPTY(.w_datin3) or .w_datin3<=.w_data1 or .w_datin3<= .w_data2) AND (EMPTY(.w_OBS3) OR .w_OBS3>.w_OBTEST))  and not(empty(.w_codlis3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ocodlis3_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino gi� selezionato o tipo non corretto o non valido")
          case   not(.w_data2>=.w_data1 or empty(.w_data2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date scorretto")
          case   not(.w_data2>=.w_data1 or empty(.w_data1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date scorretto")
          case   not(((empty(.w_ECODI)) OR  (upper(.w_BCODI)<=upper(.w_ECODI))) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_bcodi))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.obcodi_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((upper(.w_ECODI)>= upper(.w_BCODI)) or (empty(.w_Bcodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_ecodi))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oecodi_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not((empty(.w_grumer2)) OR  (.w_grumer1<=.w_grumer2))  and not(empty(.w_grumer1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ogrumer1_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not((.w_grumer2>=.w_grumer1) or (empty(.w_grumer1)))  and not(empty(.w_grumer2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ogrumer2_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not(((empty(.w_produ2)) OR  (.w_produ1<=.w_produ2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_produ1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oprodu1_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not(((empty(.w_produ1)) OR  (.w_produ1<=.w_produ2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_produ2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oprodu2_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_tipo = this.w_tipo
    this.o_DATA1 = this.w_DATA1
    return

enddefine

* --- Define pages as container
define class tgsma_sliPag1 as StdContainer
  Width  = 665
  height = 405
  stdWidth  = 665
  stdheight = 405
  resizeXpos=309
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object otipo_1_1 as StdCombo with uid="FMESHNSWIR",rtseq=1,rtrep=.f.,left=104,top=30,width=161,height=21;
    , ToolTipText = "Seleziona il tipo di stampa";
    , HelpContextID = 67892534;
    , cFormVar="w_tipo",RowSource=""+"IVA inclusa,"+"Netto IVA", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func otipo_1_1.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'N',;
    space(10))))
  endfunc
  func otipo_1_1.GetRadio()
    this.Parent.oContained.w_tipo = this.RadioValue()
    return .t.
  endfunc

  func otipo_1_1.SetRadio()
    this.Parent.oContained.w_tipo=trim(this.Parent.oContained.w_tipo)
    this.value = ;
      iif(this.Parent.oContained.w_tipo=='L',1,;
      iif(this.Parent.oContained.w_tipo=='N',2,;
      0))
  endfunc

  add object ocodlis1_1_2 as StdField with uid="UUKSEPDVNP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_codlis1", cQueryName = "codlis1",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino gi� selezionato o tipo non corretto o non valido",;
    ToolTipText = "Seleziona il listino",;
    HelpContextID = 40355290,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=104, Top=59, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_codlis1"

  func ocodlis1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc ocodlis1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocodlis1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'ocodlis1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSAR_SLI.LISTINI_VZM',this.parent.oContained
  endproc
  proc ocodlis1_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_codlis1
     i_obj.ecpSave()
  endproc

  add object odesl1_1_3 as StdField with uid="FKDHWASRGD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_desl1", cQueryName = "desl1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 119087158,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=164, Top=59, InputMask=replicate('X',40)

  add object ovalu1_1_4 as StdField with uid="QJMCNGNJAL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_valu1", cQueryName = "valu1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 119647574,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=459, Top=59, InputMask=replicate('X',3)

  add object ocodlis2_1_5 as StdField with uid="UAPZGPVLVW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_codlis2", cQueryName = "codlis2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino gi� selezionato o tipo non corretto o non valido",;
    ToolTipText = "Seleziona il listino",;
    HelpContextID = 40355290,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=104, Top=83, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_codlis2"

  func ocodlis2_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc ocodlis2_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocodlis2_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'ocodlis2_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSAR_SLI.LISTINI_VZM',this.parent.oContained
  endproc
  proc ocodlis2_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_codlis2
     i_obj.ecpSave()
  endproc

  add object odesl2_1_6 as StdField with uid="KCEULWISVW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_desl2", cQueryName = "desl2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 120135734,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=164, Top=83, InputMask=replicate('X',40)

  add object ovalu2_1_7 as StdField with uid="TTPNSUQOVJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_valu2", cQueryName = "valu2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 120696150,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=459, Top=83, InputMask=replicate('X',3)

  add object ocodlis3_1_8 as StdField with uid="VFATAWXMCC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_codlis3", cQueryName = "codlis3",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino gi� selezionato o tipo non corretto o non valido",;
    ToolTipText = "Seleziona il listino",;
    HelpContextID = 40355290,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=104, Top=107, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_codlis3"

  func ocodlis3_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc ocodlis3_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocodlis3_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'ocodlis3_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSAR_SLI.LISTINI_VZM',this.parent.oContained
  endproc
  proc ocodlis3_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_codlis3
     i_obj.ecpSave()
  endproc

  add object odesl3_1_9 as StdField with uid="ISEJOZEXQI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_desl3", cQueryName = "desl3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 121184310,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=164, Top=107, InputMask=replicate('X',40)

  add object ovalu3_1_10 as StdField with uid="BTVIRAYVNI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_valu3", cQueryName = "valu3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 121744726,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=459, Top=107, InputMask=replicate('X',3)

  add object oDATA1_1_11 as StdField with uid="PUVOIXFVIA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date scorretto",;
    ToolTipText = "Data di inizio confronto listini",;
    HelpContextID = 116132406,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=315, Top=30

  func oDATA1_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_data2>=.w_data1 or empty(.w_data2))
    endwith
    return bRes
  endfunc

  add object oDATA2_1_12 as StdField with uid="MRPSTXGBIL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date scorretto",;
    ToolTipText = "Data di fine confronto listini",;
    HelpContextID = 117180982,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=424, Top=30

  func oDATA2_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_data2>=.w_data1 or empty(.w_data1))
    endwith
    return bRes
  endfunc

  add object obcodi_1_13 as StdField with uid="QDLSYDMRWS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_bcodi", cQueryName = "bcodi",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice primo articolo da stampare",;
    HelpContextID = 177266198,;
   bGlobalFont=.t.,;
    Height=21, Width=198, Left=104, Top=173, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_bcodi"

  func obcodi_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc obcodi_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc obcodi_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'obcodi_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'',this.parent.oContained
  endproc
  proc obcodi_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_bcodi
     i_obj.ecpSave()
  endproc

  add object odesi_1_14 as StdField with uid="KFLOYZVEFP",rtseq=14,rtrep=.f.,;
    cFormVar = "w_desi", cQueryName = "desi",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 67510326,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=302, Top=173, InputMask=replicate('X',40)

  add object oecodi_1_15 as StdField with uid="WFNFKQEZFS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ecodi", cQueryName = "ecodi",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Codice ultimo articolo da stampare",;
    HelpContextID = 177266246,;
   bGlobalFont=.t.,;
    Height=21, Width=198, Left=104, Top=195, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ecodi"

  func oecodi_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oecodi_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oecodi_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oecodi_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'',this.parent.oContained
  endproc
  proc oecodi_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ecodi
     i_obj.ecpSave()
  endproc

  add object odesi1_1_16 as StdField with uid="KIFHHOVYSZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_desi1", cQueryName = "desi1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 118890550,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=302, Top=195, InputMask=replicate('X',40)

  add object ogrumer1_1_17 as StdField with uid="PDCZRZBJCN",rtseq=17,rtrep=.f.,;
    cFormVar = "w_grumer1", cQueryName = "grumer1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    ToolTipText = "Filtro sul gruppo merceologico",;
    HelpContextID = 61190810,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=104, Top=225, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_grumer1"

  func ogrumer1_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc ogrumer1_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ogrumer1_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'ogrumer1_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc ogrumer1_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_grumer1
     i_obj.ecpSave()
  endproc

  add object odesg1_1_18 as StdField with uid="CLTAFFWTRW",rtseq=18,rtrep=.f.,;
    cFormVar = "w_desg1", cQueryName = "desg1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 118759478,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=163, Top=225, InputMask=replicate('X',30)

  add object ogrumer2_1_19 as StdField with uid="XGJPWJDLPQ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_grumer2", cQueryName = "grumer2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    ToolTipText = "Filtro sul gruppo merceologico",;
    HelpContextID = 61190810,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=104, Top=247, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_grumer2"

  func ogrumer2_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc ogrumer2_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ogrumer2_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'ogrumer2_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc ogrumer2_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_grumer2
     i_obj.ecpSave()
  endproc

  add object odesg2_1_20 as StdField with uid="KQENDFRIHA",rtseq=20,rtrep=.f.,;
    cFormVar = "w_desg2", cQueryName = "desg2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 119808054,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=163, Top=247, InputMask=replicate('X',30)

  add object ocatomo_1_21 as StdField with uid="XIAMTFFEIY",rtseq=21,rtrep=.f.,;
    cFormVar = "w_catomo", cQueryName = "catomo",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Stampa solo gli articoli con questa categoria omogenea",;
    HelpContextID = 165424166,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=104, Top=276, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_catomo"

  func ocatomo_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc ocatomo_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocatomo_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'ocatomo_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc ocatomo_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_catomo
     i_obj.ecpSave()
  endproc

  add object odesCAT_1_22 as StdField with uid="KZTJFXDJKA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_desCAT", cQueryName = "desCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 200286262,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=163, Top=276, InputMask=replicate('X',30)

  add object oprodu1_1_23 as StdField with uid="GHWDJSULDI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_produ1", cQueryName = "produ1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Filtro sui fornitori",;
    HelpContextID = 206630390,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=104, Top=305, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TipConto", oKey_2_1="ANCODICE", oKey_2_2="this.w_produ1"

  func oprodu1_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oprodu1_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oprodu1_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TipConto)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TipConto)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oprodu1_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oprodu1_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TipConto
     i_obj.w_ANCODICE=this.parent.oContained.w_produ1
     i_obj.ecpSave()
  endproc

  add object odesF_1_24 as StdField with uid="CVYMOJNUUB",rtseq=24,rtrep=.f.,;
    cFormVar = "w_desF", cQueryName = "desF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 65216566,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=247, Top=305, InputMask=replicate('X',40)

  add object oprodu2_1_25 as StdField with uid="GXCSWPKBZV",rtseq=25,rtrep=.f.,;
    cFormVar = "w_produ2", cQueryName = "produ2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Filtro sui fornitori",;
    HelpContextID = 223407606,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=104, Top=330, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TipConto", oKey_2_1="ANCODICE", oKey_2_2="this.w_produ2"

  func oprodu2_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oprodu2_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oprodu2_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TipConto)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TipConto)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oprodu2_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oprodu2_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TipConto
     i_obj.w_ANCODICE=this.parent.oContained.w_produ2
     i_obj.ecpSave()
  endproc

  add object odesF2_1_26 as StdField with uid="YFACPYGEVP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_desF2", cQueryName = "desF2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 117645366,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=247, Top=330, InputMask=replicate('X',40)


  add object oObj_1_36 as cp_outputCombo with uid="ZLERXIVPWT",left=104, top=357, width=385,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 238128102


  add object oBtn_1_37 as StdButton with uid="DHTFNYVPUS",left=555, top=356, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 201920038;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        this.Parent.oContained.Confronta()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_codlis1))
      endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="ITLYMOVYBM",left=608, top=356, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 67447878;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNOUGUALI_1_62 as StdCheck with uid="PQDFUGQPCV",rtseq=41,rtrep=.f.,left=529, top=60, caption="Escludi uguali",;
    ToolTipText = "Se attivo, esclude i listini uguali",;
    HelpContextID = 171059487,;
    cFormVar="w_NOUGUALI", bObbl = .f. , nPag = 1;
    , tabstop=.F.;
   , bGlobalFont=.t.


  func oNOUGUALI_1_62.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oNOUGUALI_1_62.GetRadio()
    this.Parent.oContained.w_NOUGUALI = this.RadioValue()
    return .t.
  endfunc

  func oNOUGUALI_1_62.SetRadio()
    this.Parent.oContained.w_NOUGUALI=trim(this.Parent.oContained.w_NOUGUALI)
    this.value = ;
      iif(this.Parent.oContained.w_NOUGUALI=='S',1,;
      0)
  endfunc


  add object oTIPUMI_1_63 as StdCombo with uid="ZLVISPSMQV",value=3,rtseq=42,rtrep=.f.,left=529,top=30,width=99,height=21;
    , ToolTipText = "Cerca il listino nell'unit� di misura indicata (prima, seconda o tutte)";
    , HelpContextID = 29348662;
    , cFormVar="w_TIPUMI",RowSource=""+"Nella 1a U.m.,"+"Nella 2a U.m.,"+"In tutte le U.m.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPUMI_1_63.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,0,;
    0))))
  endfunc
  func oTIPUMI_1_63.GetRadio()
    this.Parent.oContained.w_TIPUMI = this.RadioValue()
    return .t.
  endfunc

  func oTIPUMI_1_63.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TIPUMI==1,1,;
      iif(this.Parent.oContained.w_TIPUMI==2,2,;
      iif(this.Parent.oContained.w_TIPUMI==0,3,;
      0)))
  endfunc

  add object oStr_1_28 as StdString with uid="ELBEDMIPUM",Visible=.t., Left=14, Top=30,;
    Alignment=1, Width=88, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="IPBIKYSDGR",Visible=.t., Left=14, Top=61,;
    Alignment=1, Width=88, Height=18,;
    Caption="1� Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="FCBCOTAEID",Visible=.t., Left=14, Top=85,;
    Alignment=1, Width=88, Height=18,;
    Caption="2� Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="ZLTBDPNUOR",Visible=.t., Left=14, Top=109,;
    Alignment=1, Width=88, Height=18,;
    Caption="3� Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="INOYYFKNXB",Visible=.t., Left=266, Top=30,;
    Alignment=1, Width=48, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="RPXTEZVCDR",Visible=.t., Left=27, Top=12,;
    Alignment=0, Width=626, Height=15,;
    Caption="Selezione listini"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="XVALGGNYWC",Visible=.t., Left=395, Top=30,;
    Alignment=1, Width=28, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="UQHACOXVSZ",Visible=.t., Left=11, Top=277,;
    Alignment=1, Width=92, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="MXMLRHGTRI",Visible=.t., Left=11, Top=225,;
    Alignment=1, Width=92, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="GWRIPHUDOV",Visible=.t., Left=8, Top=247,;
    Alignment=1, Width=95, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="RPCQOQLJIE",Visible=.t., Left=11, Top=305,;
    Alignment=1, Width=92, Height=15,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="IGVCDJICJQ",Visible=.t., Left=11, Top=330,;
    Alignment=1, Width=92, Height=15,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="YZJNUHBNEG",Visible=.t., Left=11, Top=173,;
    Alignment=1, Width=92, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="FNNBREZJTI",Visible=.t., Left=11, Top=195,;
    Alignment=1, Width=92, Height=15,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="MCJATRDWEZ",Visible=.t., Left=27, Top=154,;
    Alignment=0, Width=624, Height=15,;
    Caption="Selezione articoli"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="YSWFRLLLVT",Visible=.t., Left=11, Top=357,;
    Alignment=1, Width=92, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_47 as StdBox with uid="SVIZKBUGZH",left=24, top=168, width=630,height=1

  add object oBox_1_55 as StdBox with uid="FGNHIOGGAW",left=24, top=26, width=630,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_sli','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
