* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kvs                                                        *
*              Visualizza struttura SEPA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-30                                                      *
* Last revis.: 2015-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kvs",oParentObject))

* --- Class definition
define class tgste_kvs as StdForm
  Top    = 6
  Left   = 19

  * --- Standard Properties
  Width  = 668
  Height = 573
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-28"
  HelpContextID=48903273
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  VAELEMEN_IDX = 0
  VASTRUTT_IDX = 0
  cPrg = "gste_kvs"
  cComment = "Visualizza struttura SEPA"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODSTR = space(10)
  o_CODSTR = space(10)
  w_FLESCL = space(1)
  w_LROOT = space(20)
  w_TIPFIL = space(1)
  w_TIPVIS = space(1)
  o_TIPVIS = space(1)
  w_ROOT = space(20)
  o_ROOT = space(20)
  w_CURSORNA = space(10)
  w_CODICE = space(20)
  w_DESCRI = space(50)
  w_DESSTR = space(30)
  w_DESELE = space(50)
  w_FL__ELAB = .F.
  w_STFILXSD = space(254)
  w_STCRISTR = space(100)
  w_TREEVIEW = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kvsPag1","gste_kvs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODSTR_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TREEVIEW = this.oPgFrm.Pages(1).oPag.TREEVIEW
    DoDefault()
    proc Destroy()
      this.w_TREEVIEW = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VAELEMEN'
    this.cWorkTables[2]='VASTRUTT'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_kvs
    * --- Propone l'ultima commessa Inserita
    p_OLDSTR=space(10)
    if p_OLDSTR<>this.w_CODSTR
       p_OLDSTR=this.w_CODSTR
    endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODSTR=space(10)
      .w_FLESCL=space(1)
      .w_LROOT=space(20)
      .w_TIPFIL=space(1)
      .w_TIPVIS=space(1)
      .w_ROOT=space(20)
      .w_CURSORNA=space(10)
      .w_CODICE=space(20)
      .w_DESCRI=space(50)
      .w_DESSTR=space(30)
      .w_DESELE=space(50)
      .w_FL__ELAB=.f.
      .w_STFILXSD=space(254)
      .w_STCRISTR=space(100)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODSTR))
          .link_1_1('Full')
        endif
        .w_FLESCL = 'N'
          .DoRTCalc(3,4,.f.)
        .w_TIPVIS = 'N'
        .w_ROOT = iif(.w_TIPVIS='S',Space(20),.w_LROOT)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ROOT))
          .link_1_6('Full')
        endif
      .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
          .DoRTCalc(7,7,.f.)
        .w_CODICE = Nvl( .w_TREEVIEW.GETVAR('ELCODICE') , Space(20))
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODICE))
          .link_1_10('Full')
        endif
          .DoRTCalc(9,11,.f.)
        .w_FL__ELAB = .F.
    endwith
    this.DoRTCalc(13,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_TIPVIS<>.w_TIPVIS.or. .o_CODSTR<>.w_CODSTR
            .w_ROOT = iif(.w_TIPVIS='S',Space(20),.w_LROOT)
          .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
        .DoRTCalc(7,7,.t.)
            .w_CODICE = Nvl( .w_TREEVIEW.GETVAR('ELCODICE') , Space(20))
          .link_1_10('Full')
        if .o_CODSTR<>.w_CODSTR.or. .o_ROOT<>.w_ROOT
          .Calculate_SBRKGDBRNL()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
    endwith
  return

  proc Calculate_VFXKQMRIKX()
    with this
          * --- gste_bsv(close) - done
          gste_bsv(this;
              ,'Close';
             )
    endwith
  endproc
  proc Calculate_IUBWKGLBYK()
    with this
          * --- gste_bsv(reload) - reload
          gste_bsv(this;
              ,'Reload';
             )
    endwith
  endproc
  proc Calculate_SBRKGDBRNL()
    with this
          * --- Reimposta flag elaborazione
          .w_FL__ELAB = .F.
    endwith
  endproc
  proc Calculate_GWYMIJCOPS()
    with this
          * --- Esplodi
          .w_ROOT = .w_CODICE
          .link_1_6('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oROOT_1_6.enabled = this.oPgFrm.Page1.oPag.oROOT_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oROOT_1_6.visible=!this.oPgFrm.Page1.oPag.oROOT_1_6.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_13.visible=!this.oPgFrm.Page1.oPag.oBtn_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDESELE_1_18.visible=!this.oPgFrm.Page1.oPag.oDESELE_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.TREEVIEW.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_VFXKQMRIKX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Reload")
          .Calculate_IUBWKGLBYK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Esplodi")
          .Calculate_GWYMIJCOPS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gste_kvs
    IF cevent='w_FLESCL Changed'
       This.w_ROOT=IIF(This.w_TIPVIS='N',This.w_LROOT,' ')
       This.NotifyEvent("Reload")
    Endif
    if cevent='Esplodi'
       This.w_TIPVIS='N'
       This.o_TIPVIS='N'
       This.NotifyEvent("Reload")
    endif
    if cevent='Implodi'
       This.w_ROOT=''
       This.w_TIPVIS='S'
       This.o_TIPVIS='S'
       This.NotifyEvent("Reload")
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODSTR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_CODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STELROOT,STTIPFIL,STFILXSD,STCRISTR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_CODSTR))
          select STCODICE,STDESCRI,STELROOT,STTIPFIL,STFILXSD,STCRISTR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" STDESCRI like "+cp_ToStrODBC(trim(this.w_CODSTR)+"%");

            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STELROOT,STTIPFIL,STFILXSD,STCRISTR";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" STDESCRI like "+cp_ToStr(trim(this.w_CODSTR)+"%");

            select STCODICE,STDESCRI,STELROOT,STTIPFIL,STFILXSD,STCRISTR;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oCODSTR_1_1'),i_cWhere,'',"Strutture",'GSTE_KVS.VASTRUTT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STELROOT,STTIPFIL,STFILXSD,STCRISTR";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI,STELROOT,STTIPFIL,STFILXSD,STCRISTR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STELROOT,STTIPFIL,STFILXSD,STCRISTR";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_CODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_CODSTR)
            select STCODICE,STDESCRI,STELROOT,STTIPFIL,STFILXSD,STCRISTR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSTR = NVL(_Link_.STCODICE,space(10))
      this.w_DESSTR = NVL(_Link_.STDESCRI,space(30))
      this.w_LROOT = NVL(_Link_.STELROOT,space(20))
      this.w_TIPFIL = NVL(_Link_.STTIPFIL,space(1))
      this.w_STFILXSD = NVL(_Link_.STFILXSD,space(254))
      this.w_STCRISTR = NVL(_Link_.STCRISTR,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_CODSTR = space(10)
      endif
      this.w_DESSTR = space(30)
      this.w_LROOT = space(20)
      this.w_TIPFIL = space(1)
      this.w_STFILXSD = space(254)
      this.w_STCRISTR = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=cifracnf(Alltrim(.w_STCRISTR),'D')=Alltrim(.w_CODSTR)+juststem(.w_STFILXSD)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Struttura non ufficiale")
        endif
        this.w_CODSTR = space(10)
        this.w_DESSTR = space(30)
        this.w_LROOT = space(20)
        this.w_TIPFIL = space(1)
        this.w_STFILXSD = space(254)
        this.w_STCRISTR = space(100)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROOT
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gste_AEL',True,'VAELEMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_ROOT)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_CODSTR);

          i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODSTR,ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODSTR',this.w_CODSTR;
                     ,'ELCODICE',trim(this.w_ROOT))
          select ELCODSTR,ELCODICE,ELDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODSTR,ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROOT)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROOT) and !this.bDontReportError
            deferred_cp_zoom('VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(oSource.parent,'oROOT_1_6'),i_cWhere,'gste_AEL',"Elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODSTR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ELCODSTR="+cp_ToStrODBC(this.w_CODSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',oSource.xKey(1);
                       ,'ELCODICE',oSource.xKey(2))
            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_ROOT);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_CODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_CODSTR;
                       ,'ELCODICE',this.w_ROOT)
            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROOT = NVL(_Link_.ELCODICE,space(20))
      this.w_DESELE = NVL(_Link_.ELDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ROOT = space(20)
      endif
      this.w_DESELE = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_CODICE);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_CODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_CODSTR;
                       ,'ELCODICE',this.w_CODICE)
            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.ELCODICE,space(20))
      this.w_DESCRI = NVL(_Link_.ELDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(20)
      endif
      this.w_DESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODSTR_1_1.value==this.w_CODSTR)
      this.oPgFrm.Page1.oPag.oCODSTR_1_1.value=this.w_CODSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oFLESCL_1_2.RadioValue()==this.w_FLESCL)
      this.oPgFrm.Page1.oPag.oFLESCL_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oROOT_1_6.value==this.w_ROOT)
      this.oPgFrm.Page1.oPag.oROOT_1_6.value=this.w_ROOT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_10.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_10.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_14.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_14.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSTR_1_17.value==this.w_DESSTR)
      this.oPgFrm.Page1.oPag.oDESSTR_1_17.value=this.w_DESSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESELE_1_18.value==this.w_DESELE)
      this.oPgFrm.Page1.oPag.oDESELE_1_18.value=this.w_DESELE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(cifracnf(Alltrim(.w_STCRISTR),'D')=Alltrim(.w_CODSTR)+juststem(.w_STFILXSD))  and not(empty(.w_CODSTR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODSTR_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Struttura non ufficiale")
          case   (empty(.w_ROOT))  and not(.w_TIPVIS='S')  and (!EMPTY(NVL(.w_CODSTR, ' ')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oROOT_1_6.SetFocus()
            i_bnoObbl = !empty(.w_ROOT)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODSTR = this.w_CODSTR
    this.o_TIPVIS = this.w_TIPVIS
    this.o_ROOT = this.w_ROOT
    return

enddefine

* --- Define pages as container
define class tgste_kvsPag1 as StdContainer
  Width  = 664
  height = 573
  stdWidth  = 664
  stdheight = 573
  resizeXpos=358
  resizeYpos=424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODSTR_1_1 as StdField with uid="JDOQEOTSKB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODSTR", cQueryName = "CODSTR",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Struttura non ufficiale",;
    ToolTipText = "Codice struttura",;
    HelpContextID = 78470950,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=95, Top=9, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", oKey_1_1="STCODICE", oKey_1_2="this.w_CODSTR"

  func oCODSTR_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_ROOT)
        bRes2=.link_1_6('Full')
      endif
      if .not. empty(.w_CODICE)
        bRes2=.link_1_10('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODSTR_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSTR_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oCODSTR_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture",'GSTE_KVS.VASTRUTT_VZM',this.parent.oContained
  endproc

  add object oFLESCL_1_2 as StdCheck with uid="ASIOAVNZPO",rtseq=2,rtrep=.f.,left=351, top=8, caption="Escludi elementi non presenti nel file",;
    ToolTipText = "Se attivo visualizza solo elementi gestiti nella generazione file XML",;
    HelpContextID = 40014762,;
    cFormVar="w_FLESCL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLESCL_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLESCL_1_2.GetRadio()
    this.Parent.oContained.w_FLESCL = this.RadioValue()
    return .t.
  endfunc

  func oFLESCL_1_2.SetRadio()
    this.Parent.oContained.w_FLESCL=trim(this.Parent.oContained.w_FLESCL)
    this.value = ;
      iif(this.Parent.oContained.w_FLESCL=='S',1,;
      0)
  endfunc

  add object oROOT_1_6 as StdField with uid="DOKYHDKEJA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ROOT", cQueryName = "ROOT",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 43053034,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=95, Top=57, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAELEMEN", cZoomOnZoom="gste_AEL", oKey_1_1="ELCODSTR", oKey_1_2="this.w_CODSTR", oKey_2_1="ELCODICE", oKey_2_2="this.w_ROOT"

  func oROOT_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(NVL(.w_CODSTR, ' ')))
    endwith
   endif
  endfunc

  func oROOT_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPVIS='S')
    endwith
  endfunc

  func oROOT_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oROOT_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROOT_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAELEMEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_CODSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStr(this.Parent.oContained.w_CODSTR)
    endif
    do cp_zoom with 'VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(this.parent,'oROOT_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'gste_AEL',"Elementi",'',this.parent.oContained
  endproc
  proc oROOT_1_6.mZoomOnZoom
    local i_obj
    i_obj=gste_AEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ELCODSTR=w_CODSTR
     i_obj.w_ELCODICE=this.parent.oContained.w_ROOT
     i_obj.ecpSave()
  endproc


  add object TREEVIEW as cp_Treeview with uid="FVJYSBPBBD",left=42, top=85, width=555,height=449,;
    caption='Object',;
   bGlobalFont=.t.,;
    cCursor="cur_str",cShowFields="STFILLER+STDESCRI",cNodeShowField="ELCODICE",cLeafShowField="ELCODICE",cNodeBmp="ROOT.BMP,CONTO.BMP,LEAF.BMP,NEW1.BMP",cLeafBmp="",nIndent=20,cLvlSep="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 129094374


  add object oBtn_1_9 as StdButton with uid="PPWFAOCXOY",left=603, top=85, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per riempire la tree view";
    , HelpContextID = 128018966;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        .NotifyEvent("Reload")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_CODSTR) and (Not Empty(.w_ROOT) or .w_TIPVIS='S'))
      endwith
    endif
  endfunc

  add object oCODICE_1_10 as StdField with uid="CQUQMQURRM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 158114010,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=42, Top=538, InputMask=replicate('X',20), cLinkFile="VAELEMEN", cZoomOnZoom="gste_AEL", oKey_1_1="ELCODSTR", oKey_1_2="this.w_CODSTR", oKey_2_1="ELCODICE", oKey_2_2="this.w_CODICE"

  func oCODICE_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oBtn_1_11 as StdButton with uid="ZVAMGQYVJH",left=603, top=185, width=48,height=45,;
    CpPicture="BMP\IMPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per chiudere la struttura";
    , HelpContextID = 126272122;
    , Caption='\<Implodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(NVL(.w_CODICE,'')) And .w_FL__ELAB)
      endwith
    endif
  endfunc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPVIS='S')
     endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="EDOPRFXLCY",left=603, top=235, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la gestione dell'elemento";
    , HelpContextID = 31034549;
    , Caption='\<Elemento';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        gste_bsv(this.Parent.oContained,"Elemento")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty ( NVL ( .w_CODICE,'' ) ) And .w_FL__ELAB)
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="XWAVWHNZRN",left=603, top=135, width=48,height=45,;
    CpPicture="BMP\ESPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per espandere la struttura";
    , HelpContextID = 126270650;
    , Caption='\<Esplodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (NVL(.w_CODICE,'')) And .w_FL__ELAB)
      endwith
    endif
  endfunc

  func oBtn_1_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPVIS='S')
     endwith
    endif
  endfunc

  add object oDESCRI_1_14 as StdField with uid="MCZVUXCZID",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 75610826,;
   bGlobalFont=.t.,;
    Height=21, Width=394, Left=201, Top=538, InputMask=replicate('X',50)


  add object oBtn_1_15 as StdButton with uid="KOTWJAFPMC",left=603, top=488, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 5381638;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESSTR_1_17 as StdField with uid="ODLHLTRROK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESSTR", cQueryName = "DESSTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 78529846,;
   bGlobalFont=.t.,;
    Height=21, Width=500, Left=95, Top=32, InputMask=replicate('X',30)

  add object oDESELE_1_18 as StdField with uid="ZKSQQKSTBL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESELE", cQueryName = "DESELE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 148880074,;
   bGlobalFont=.t.,;
    Height=21, Width=344, Left=251, Top=57, InputMask=replicate('X',50)

  func oDESELE_1_18.mHide()
    with this.Parent.oContained
      return (.w_TIPVIS='S')
    endwith
  endfunc


  add object oBtn_1_24 as StdButton with uid="FIETXPBADH",left=603, top=135, width=48,height=45,;
    CpPicture="BMP\VISUALIZZA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il dettaglio del nodo selezionato";
    , HelpContextID = 90080113;
    , Caption='\<Dettaglio';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      this.parent.oContained.NotifyEvent("Esplodi")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (NVL(.w_CODICE,'')))
      endwith
    endif
  endfunc

  func oBtn_1_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPVIS='N')
     endwith
    endif
  endfunc

  add object oStr_1_16 as StdString with uid="RVYSWZJLYJ",Visible=.t., Left=10, Top=11,;
    Alignment=1, Width=83, Height=18,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="WRZDNROCOK",Visible=.t., Left=10, Top=59,;
    Alignment=1, Width=83, Height=18,;
    Caption="Radice:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_TIPVIS='S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kvs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
