* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bcz                                                        *
*              Esegue il link automatico                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-25                                                      *
* Last revis.: 2009-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pForm,pOBJECT,pARCHIVIO,pFIELD,pZOOMFILE,pZOOMONZOOM,pFiltro
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bcz",oParentObject,m.pForm,m.pOBJECT,m.pARCHIVIO,m.pFIELD,m.pZOOMFILE,m.pZOOMONZOOM,m.pFiltro)
return(i_retval)

define class tgsar1bcz as StdBatch
  * --- Local variables
  pForm = .NULL.
  pOBJECT = .NULL.
  pARCHIVIO = space(20)
  pFIELD = space(20)
  pZOOMFILE = space(254)
  pZOOMONZOOM = space(20)
  pFiltro = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue il link automatico
    * --- pFORM- Parametro del form
    *     pOBJECT- Parametro del control sul campo in cui faccio lo zoom
    *     pARCHIVIO- nome dell'archivio collegato
    *     pFIELD - nome del campo archivio collegato
    *     pZOOMFILE - zoom da selezionare
    *     pZOOMONZOOM - zoom on zoom collegato allo zoom
    *     pFILTRO - filtro dello zoom
    if vartype( this.pOBJECT )="O"
      this.pARCHIVIO = alltrim( this.pARCHIVIO )
      this.pFIELD = ALLTRIM( this.pFIELD )
      this.pZOOMFILE = Alltrim( this.pZOOMFILE )
      this.pZOOMFILE = this.pZOOMFILE+"."+ this.pARCHIVIO+"_VZM" 
      this.pZOOMONZOOM = Alltrim( this.pZOOMONZOOM )
      * --- Applico il filtro passatomi come parametro se definito e pieno.
      *     DA INSERIRE CODICE PER GESTIRE FILTRO LIKE SE TIPO 
      *     OGGETTO CARATTERE E PRESENTE UN DATO SULL'OGGETTO
      *     (PARAMETRICO SE F9 NON SE USCITA DA CONTROL SI)
      this.pFiltro = iif( vartype(this.pFiltro)="C" And not EMPTY(this.pFiltro) , this.pFiltro, "1=1" )
      CP_ZOOM( this.pARCHIVIO, "*", this.pFIELD, cp_AbsName(this.pOBJECT.parent,this.pOBJECT.nAME) ,this.pFiltro,this.pZOOMONZOOM,"Valore attributo",this.pZOOMFILE, this.pOBJECT.parent.oContained )
    endif
  endproc


  proc Init(oParentObject,pForm,pOBJECT,pARCHIVIO,pFIELD,pZOOMFILE,pZOOMONZOOM,pFiltro)
    this.pForm=pForm
    this.pOBJECT=pOBJECT
    this.pARCHIVIO=pARCHIVIO
    this.pFIELD=pFIELD
    this.pZOOMFILE=pZOOMFILE
    this.pZOOMONZOOM=pZOOMONZOOM
    this.pFiltro=pFiltro
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pForm,pOBJECT,pARCHIVIO,pFIELD,pZOOMFILE,pZOOMONZOOM,pFiltro"
endproc
