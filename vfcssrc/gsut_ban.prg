* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ban                                                        *
*              Salvataggio anteprima solo testo                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_14]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-30                                                      *
* Last revis.: 2004-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPath,pReport
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_ban",oParentObject,m.pPath,m.pReport)
return(i_retval)

define class tgsut_ban as StdBatch
  * --- Local variables
  pPath = space(50)
  pReport = space(20)
  w_FILENAME = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato da maschera GSUT_KAS per salvataggio file .PRN
    *     creato dalla stampa solo testo in file di testo
    *     pPath -> Path assoluto file Prn
    *     pReport -> nome del File Report
    * --- Apro la maschera di Save As di Window
    this.w_FILENAME = Putfile(ah_Msgformat("File di testo"), this.pReport,"txt")
    * --- Se non premo Annulla, copio il file Prn sul file scelto dall'utente
    if Not Empty(this.w_FILENAME)
      COPY FILE ( this.pPath ) TO ( this.w_FILENAME )
    endif
  endproc


  proc Init(oParentObject,pPath,pReport)
    this.pPath=pPath
    this.pReport=pReport
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPath,pReport"
endproc
