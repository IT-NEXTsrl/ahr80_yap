* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bmg                                                        *
*              Manutenzione attributi clienti/fornitori                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-18                                                      *
* Last revis.: 2013-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper,pFILTER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bmg",oParentObject,m.pOper,m.pFILTER)
return(i_retval)

define class tgsar_bmg as StdBatch
  * --- Local variables
  pOper = space(5)
  pFILTER = space(254)
  w_GSAR_MAC = .NULL.
  w_GSAR_MDA = .NULL.
  w_PUNPAD = .NULL.
  w_NFLTATTR = 0
  w_APPOGUID = space(15)
  w_CONTANUM = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_ASTIPNUM = 0
  w_ASTIPDAT = ctod("  /  /  ")
  w_ASTIPCAR = space(20)
  w_INPUTA = space(1)
  w_APPTIPCLF = space(1)
  w_APPCODCON = space(15)
  w_OKINSATT = .f.
  w_CACODGRU = space(10)
  w_CACODFAM = space(10)
  w_CAVALATT = space(20)
  w_CONFE = .f.
  w_AUTOMA = space(1)
  w_FILTER = space(254)
  w_MSK = .NULL.
  w_FRINPUTA = space(1)
  w_CONTAX = 0
  * --- WorkFile variables
  ASS_ATTR_idx=0
  FAM_ATTR_idx=0
  CONTI_idx=0
  FAM_ATTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pFILTER � un filtro da applicare al risultato dello zoom
    this.w_PUNPAD = this.oParentObject
    if UPPER(this.w_PUNPAD.Class)=="TCGSAR_MAC"
      this.w_GSAR_MAC = this.w_PUNPAD
    else
      this.w_GSAR_MAC = IIF(TYPE("this.w_PUNPAD.GSAR_MAC")<>"U", this.w_PUNPAD.GSAR_MAC, .NULL. )
    endif
    this.w_GSAR_MDA = IIF(TYPE("this.w_PUNPAD.GSAR_MDA")<>"U", this.w_PUNPAD.GSAR_MDA, .NULL. )
    if TYPE("this.oParentObject.w_ZCONTI")<>"U"
      if lower(this.w_PUNPAD.class)="tgsar_kas"
        this.oParentObject.w_ZCONTI = this.w_PUNPAD.w_ZATTRIB
      else
        this.oParentObject.w_ZCONTI = this.w_PUNPAD.w_ZCONTI
      endif
    endif
    do case
      case this.pOper=="ZOOM"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_PUNPAD.oPgfrm.ActivePage = 2
      case this.pOper=="SELEZ" or this.pOper=="DESEL" or this.pOper=="INVSE"
        if USED(this.oParentObject.w_ZCONTI.cCursor)
          Select (this.oParentObject.w_ZCONTI.cCursor)
          if RECCOUNT()>0
            this.w_NFLTATTR = RECNO()
            UPDATE (this.oParentObject.w_ZCONTI.cCursor) SET XCHK=ICASE(this.pOper=="SELEZ",1,this.pOper=="DESEL",0,IIF(XCHK=1,0,1))
            Select (this.oParentObject.w_ZCONTI.cCursor)
            GO MIN( this.w_NFLTATTR, RECNO() )
          endif
        endif
      case this.pOper=="VISCL" or this.pOper=="STACL"
        * --- Verifico che almeno un attributo sia stato impostato
        select COUNT(*) AS CONTA from (this.w_GSAR_MAC.cTrsName) where Not Empty (t_CAVALATT) into Cursor "Flt_Attr"
        if USED("Flt_attr")
          if NVL(Flt_Attr.Conta, 0) >0
            USE IN "Flt_Attr"
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.pOper=="VISCL"
              do GSAR_KVI with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              vq_exec("query\GSAR_BMG",this,"Att_Obbl")
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if ah_YesNo(IIF(this.oParentObject.w_ANTIPCON="C", "Si desidera stampare anche il dettaglio attributi associati al cliente?", "Si desidera stampare anche il dettaglio attributi associati al fornitore?"))
                CP_CHPRN("QUERY\GSARDKMA.FRX", "", this)
              else
                CP_CHPRN("QUERY\GSAR_KMA.FRX", "", this)
              endif
            endif
          else
            Ah_ErrorMsg("Occorre impostare almeno un attributo",48)
          endif
        else
          Ah_ErrorMsg("Occorre impostare almeno un attributo",48)
        endif
      case this.pOper=="INVCL"
        * --- Imposto titolo maschera
        this.w_PUNPAD.Caption = IIF(this.w_PUNPAD.oParentObject.oParentObject.w_ANTIPCON="C", ah_MsgFormat("VISUALIZZA CLIENTI"), ah_MsgFormat("VISUALIZZA FORNITORI"))
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper=="PRINT" Or this.pOper=="PRIAT"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Preparo il cursore per la stampa
        SELECT * FROM "OutCurs" Order by antipcon, ancodice, stato INTO CURSOR "__Tmp__"
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        CP_CHPRN("QUERY\GSAREKMA.FRX", "", this)
        if this.pOper=="PRINT"
          this.oParentObject.w_MAFLCTRL = .T.
        endif
      case this.pOper=="SALVA"
        if this.oParentObject.w_MAFLCTRL or ah_yesno("Non � stata eseguita la stampa di verifica, proseguire comunque?")
          * --- Preparo il cursore con i soli clienti selezionati
          * --- Se esiste, il cursore � stato creato da GSAR_BPM
          if NOT USED("_Selez_") OR TYPE("_Selez_.guid")<>"C"
            SELECT antipcon, ancodice, guid FROM (this.oParentObject.w_ZCONTI.cCursor) WHERE XCHK=1 INTO CURSOR "_Selez_"
            * --- Memorizzo gli attributi aggiunti ed eliminati
            select t_CACODGRU,t_CACODFAM,t_CAVALATT from (this.w_GSAR_MDA.cTrsName) where Not Empty (t_CAVALATT) and t_CAFLELIM=1 into Cursor "Del_Attr"
            select t_CACODGRU,t_CACODFAM,t_CAVALATT from (this.w_GSAR_MDA.cTrsName) where Not Empty (t_CAVALATT) and t_CAFLINSE=1 into Cursor "Add_Attr"
            * --- Elimino dal cursore gli articoli che non possono essere aggiornati per 
            *     mancanza di uno o pi� attributi obbligatori
            this.Pag6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            =WRCURSOR("_Selez_")
            DELETE FROM "_Selez_" WHERE antipcon+ancodice IN (SELECT DISTINCT antipcon+ancodice FROM "OutCurs" WHERE Stato="T")
            SELECT "_Selez_"
            GO TOP
            PACK
          endif
          SELECT "_Selez_"
          GO TOP
          if RECCOUNT()>0
            * --- begin transaction
            cp_BeginTrs()
            SCAN
            this.w_APPTIPCLF = _Selez_.antipcon
            this.w_APPCODCON = _Selez_.ancodice
            this.w_APPOGUID = _Selez_.Guid
            if !EMPTY(NVL(this.w_APPOGUID, " "))
              SELECT "Del_Attr"
              GO TOP
              SCAN
              * --- Try
              local bErr_039EBBA0
              bErr_039EBBA0=bTrsErr
              this.Try_039EBBA0()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                this.Pag4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                Ah_ErrorMsg("Errore durante eliminazione attributi %1 %2",48, "", IIF(this.w_APPTIPCLF="C", ah_msgformat("cliente"), ah_msgformat("fornitore")), ALLTRIM(this.w_APPCODCON))
                i_retcode = 'stop'
                return
              endif
              bTrsErr=bTrsErr or bErr_039EBBA0
              * --- End
              SELECT "Del_Attr"
              ENDSCAN
            else
              * --- Nessun attributo associato calcolo il nuovo gestguid
              this.w_APPOGUID = SUBSTR(DTOC(DATE(),1),4)+SUBSTR(SYS(2015),2)
              * --- Try
              local bErr_03A0A1E0
              bErr_03A0A1E0=bTrsErr
              this.Try_03A0A1E0()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                this.Pag4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                Ah_ErrorMsg("Errore aggiornamento %1 %2",48, "", IIF(this.w_APPTIPCLF="C", ah_msgformat("cliente"), ah_msgformat("fornitore")), ALLTRIM(this.w_APPCODCON))
                i_retcode = 'stop'
                return
              endif
              bTrsErr=bTrsErr or bErr_03A0A1E0
              * --- End
            endif
            * --- Calcolo ultimo cprownum
            this.w_CPROWNUM = 0
            * --- Select from ASS_ATTR
            i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2],.t.,this.ASS_ATTR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CONTA  from "+i_cTable+" ASS_ATTR ";
                  +" where GUID="+cp_ToStrODBC(this.w_APPOGUID)+"";
                   ,"_Curs_ASS_ATTR")
            else
              select MAX(CPROWNUM) AS CONTA from (i_cTable);
               where GUID=this.w_APPOGUID;
                into cursor _Curs_ASS_ATTR
            endif
            if used('_Curs_ASS_ATTR')
              select _Curs_ASS_ATTR
              locate for 1=1
              do while not(eof())
              this.w_CPROWNUM = NVL(_Curs_ASS_ATTR.CONTA , 0 )
                select _Curs_ASS_ATTR
                continue
              enddo
              use
            endif
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            SELECT "Add_Attr"
            GO TOP
            SCAN
            this.w_ASTIPNUM = 0
            this.w_ASTIPDAT = .NULL.
            this.w_ASTIPCAR = space(20)
            * --- Read from FAM_ATTR
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.FAM_ATTR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.FAM_ATTR_idx,2],.t.,this.FAM_ATTR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "FRINPUTA"+;
                " from "+i_cTable+" FAM_ATTR where ";
                    +"FRCODICE = "+cp_ToStrODBC(Add_Attr.t_CACODFAM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                FRINPUTA;
                from (i_cTable) where;
                    FRCODICE = Add_Attr.t_CACODFAM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_INPUTA = NVL(cp_ToDate(_read_.FRINPUTA),cp_NullValue(_read_.FRINPUTA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            do case
              case this.w_INPUTA="C"
                this.w_ASTIPCAR = Add_Attr.t_CAVALATT
              case this.w_INPUTA="D"
                this.w_ASTIPDAT = cp_CharToDate(Add_Attr.t_CAVALATT)
              case this.w_INPUTA="N"
                this.w_ASTIPNUM = VAL(ALLTRIM(NVL(Add_Attr.t_CAVALATT, 0)))
            endcase
            SELECT "Add_Attr"
            this.w_CPROWORD = this.w_CPROWNUM * 10
            this.w_CACODGRU = Add_Attr.t_CACODGRU
            this.w_CACODFAM = Add_Attr.t_CACODFAM
            this.w_CAVALATT = Add_Attr.t_CAVALATT
            this.w_OKINSATT = .T.
            * --- Try
            local bErr_038936B0
            bErr_038936B0=bTrsErr
            this.Try_038936B0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              this.Pag4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              Ah_ErrorMsg("Errore durante inserimento attributi %1 %2",48, "", IIF(this.w_APPTIPCLF="C", ah_msgformat("cliente"), ah_msgformat("fornitore")), ALLTRIM(this.w_APPCODCON))
              i_retcode = 'stop'
              return
            endif
            bTrsErr=bTrsErr or bErr_038936B0
            * --- End
            SELECT "Add_Attr"
            ENDSCAN
            SELECT "_Selez_"
            ENDSCAN
            * --- commit
            cp_EndTrs(.t.)
            Ah_ErrorMsg("Aggiornamento completato con successo. L'elenco dei dettagli non � stato aggiornato, se necessario operare aggiornamento manuale",64)
            if this.oParentObject.w_FLDELATTR
              this.w_GSAR_MDA.FirstRow()     
              if this.w_GSAR_MDA.Numrow()>0
                do while NOT this.w_GSAR_MDA.Eof_Trs()
                  this.w_GSAR_MDA.FirstRow()     
                  this.w_GSAR_MDA.DeleteRow(.t.)     
                enddo
              endif
              * --- Cancello tutte le righe del transitorio
              *     la deleterow con parametro true non esegue la initrow nel caso in cui 
              *     non ho pi� righe nel temporaneo poich� la eseguo direttamente 
              *     alla fine del ciclo While
              this.w_GSAR_MDA.InitRow()     
              this.w_GSAR_MDA.Refresh()     
            endif
          else
            Ah_ErrorMsg("Nessun cliente/fornitore selezionato o aggiornabile",48)
          endif
          this.oParentObject.w_MAFLCTRL = .F.
        endif
      case this.pOper=="INIT"
        if TYPE("this.w_PUNPAD.oParentObject")="O" and (lower(this.w_PUNPAD.oParentObject.class)=="tgsar_acl" or lower(this.w_PUNPAD.oParentObject.class)=="tgsar_afr")
          this.oParentObject.w_ANCODINI = this.w_PUNPAD.oParentObject.w_ANCODICE
          this.oParentObject.w_ANDESINI = this.w_PUNPAD.oParentObject.w_ANDESCRI
          this.oParentObject.w_ANCODFIN = this.oParentObject.w_ANCODINI
          this.oParentObject.w_ANDESFIN = this.oParentObject.w_ANDESINI
          this.w_PUNPAD.NotifyEvent("Esegui")     
        endif
      case this.pOper=="ZAINS"
        * --- Apro la maskera selezione multipla attributi
        this.w_AUTOMA = IIF (vartype (this.oparentobject.w_AUTOMA) ="C", this.oparentobject.w_AUTOMA , "S")
        this.w_AUTOMA = IIF (EMPTY (this.w_AUTOMA),"S",this.w_AUTOMA)
        * --- pFILTER � un filtro da applicare al risultato dello zoom
        this.w_FILTER = ""
        if NOT EMPTY( this.pFILTER )
          this.w_FILTER = this.pFILTER
        endif
        this.w_MSK = GSAR_KAS(This)
        if TYPE("g_SCHEDULER")="C" and g_SCHEDULER="S"
          this.w_MSK.ecpsave()     
          g_SCHEDULER = "N"
        endif
        this.w_MSK = .NULL.
        this.w_PUNPAD.MarkPos()     
        if this.w_CONFE
          if USED("SaveAttr")
            SELECT "SaveAttr"
            GO TOP
            SCAN
            if RECNO()>1
              this.w_PUNPAD.AddRow()     
            endif
            do case
              case UPPER( this.w_PUNPAD.CLASS ) = "TCGSAR_MRA" OR UPPER( this.w_PUNPAD.CLASS ) = "TCGSAR_MR2"
                * --- Nel caso la chiamata sia eseguita da GSAR_MRA i nomi dei campi sono diversi
                if EMPTY(NVL(this.w_PUNPAD.w_ASCODATT , " "))
                  this.w_PUNPAD.w_MODATTR = SaveAttr.MACODICE
                  this.w_PUNPAD.w_ASCODATT = SaveAttr.CACODGRU
                endif
                if EMPTY(NVL(this.w_PUNPAD.w_ASCODFAM , " "))
                  this.w_PUNPAD.w_ASCODFAM = SaveAttr.CACODFAM
                  * --- Read from FAM_ATTR
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.FAM_ATTR_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.FAM_ATTR_idx,2],.t.,this.FAM_ATTR_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "FRDIMENS,FRNUMDEC,FRINPUTA"+;
                      " from "+i_cTable+" FAM_ATTR where ";
                          +"FRCODICE = "+cp_ToStrODBC(this.w_PUNPAD.w_ASCODFAM);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      FRDIMENS,FRNUMDEC,FRINPUTA;
                      from (i_cTable) where;
                          FRCODICE = this.w_PUNPAD.w_ASCODFAM;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_PUNPAD.w_FRDIMENS = NVL(cp_ToDate(_read_.FRDIMENS),cp_NullValue(_read_.FRDIMENS))
                    this.w_PUNPAD.w_FRNUMDEC = NVL(cp_ToDate(_read_.FRNUMDEC),cp_NullValue(_read_.FRNUMDEC))
                    this.w_PUNPAD.w_INPUTA = NVL(cp_ToDate(_read_.FRINPUTA),cp_NullValue(_read_.FRINPUTA))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_PUNPAD.w_CODFAM = this.w_PUNPAD.w_ASCODFAM
                  * --- Read from FAM_ATTR
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.FAM_ATTR_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.FAM_ATTR_idx,2],.t.,this.FAM_ATTR_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "FRCODICE,FRTIPOLO,FRNUMDEC,FRDIMENS,FR_TABLE,FR_CAMPO,FR__ZOOM,FRZOOMOZ,FRCODFAM,FRCAMCOL"+;
                      " from "+i_cTable+" FAM_ATTR where ";
                          +"FRCODICE = "+cp_ToStrODBC(this.w_PUNPAD.w_CODFAM);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      FRCODICE,FRTIPOLO,FRNUMDEC,FRDIMENS,FR_TABLE,FR_CAMPO,FR__ZOOM,FRZOOMOZ,FRCODFAM,FRCAMCOL;
                      from (i_cTable) where;
                          FRCODICE = this.w_PUNPAD.w_CODFAM;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_PUNPAD.w_CODFAM = NVL(cp_ToDate(_read_.FRCODICE),cp_NullValue(_read_.FRCODICE))
                    this.w_PUNPAD.w_TIPO = NVL(cp_ToDate(_read_.FRTIPOLO),cp_NullValue(_read_.FRTIPOLO))
                    this.w_PUNPAD.w_DEC = NVL(cp_ToDate(_read_.FRNUMDEC),cp_NullValue(_read_.FRNUMDEC))
                    this.w_PUNPAD.w_DIME = NVL(cp_ToDate(_read_.FRDIMENS),cp_NullValue(_read_.FRDIMENS))
                    this.w_PUNPAD.w_TABLE = NVL(cp_ToDate(_read_.FR_TABLE),cp_NullValue(_read_.FR_TABLE))
                    this.w_PUNPAD.w_CAMPO = NVL(cp_ToDate(_read_.FR_CAMPO),cp_NullValue(_read_.FR_CAMPO))
                    this.w_PUNPAD.w_ZOOM = NVL(cp_ToDate(_read_.FR__ZOOM),cp_NullValue(_read_.FR__ZOOM))
                    this.w_PUNPAD.w_ZOOMOZ = NVL(cp_ToDate(_read_.FRZOOMOZ),cp_NullValue(_read_.FRZOOMOZ))
                    this.w_PUNPAD.w_ATTRIBUTO = NVL(cp_ToDate(_read_.FRCODFAM),cp_NullValue(_read_.FRCODFAM))
                    this.w_PUNPAD.w_ATTCOL = NVL(cp_ToDate(_read_.FRCAMCOL),cp_NullValue(_read_.FRCAMCOL))
                    this.w_PUNPAD.w_FR_TABLE = NVL(cp_ToDate(_read_.FR_TABLE),cp_NullValue(_read_.FR_TABLE))
                    this.w_PUNPAD.w_FR__ZOOM = NVL(cp_ToDate(_read_.FR__ZOOM),cp_NullValue(_read_.FR__ZOOM))
                    this.w_PUNPAD.w_FR_CAMPO = NVL(cp_ToDate(_read_.FR_CAMPO),cp_NullValue(_read_.FR_CAMPO))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                * --- Read from FAM_ATTR
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.FAM_ATTR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.FAM_ATTR_idx,2],.t.,this.FAM_ATTR_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "FRINPUTA"+;
                    " from "+i_cTable+" FAM_ATTR where ";
                        +"FRCODICE = "+cp_ToStrODBC(this.w_PUNPAD.w_ASCODFAM);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    FRINPUTA;
                    from (i_cTable) where;
                        FRCODICE = this.w_PUNPAD.w_ASCODFAM;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_FRINPUTA = NVL(cp_ToDate(_read_.FRINPUTA),cp_NullValue(_read_.FRINPUTA))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                do case
                  case this.w_FRINPUTA = "C"
                    this.w_PUNPAD.w_ASTIPCAR = SaveAttr.CAVALATT
                    this.w_PUNPAD.w_ASTIPNUM = 0
                    this.w_PUNPAD.w_ASTIPDAT = cp_chartodate ("  -  -  ")
                    this.w_PUNPAD.w_ASVALATT = ALLTRIM(this.w_PUNPAD.w_ASTIPCAR)
                  case this.w_FRINPUTA = "N"
                    this.w_PUNPAD.w_ASTIPCAR = ""
                    if VARTYPE( SaveAttr.CAVALATT ) = "N"
                      this.w_PUNPAD.w_ASTIPNUM = SaveAttr.CAVALATT
                    else
                      this.w_PUNPAD.w_ASTIPNUM = VAL( SaveAttr.CAVALATT )
                    endif
                    this.w_PUNPAD.w_ASTIPDAT = cp_chartodate ("  -  -  ")
                    this.w_PUNPAD.w_ASVALATT = ALLTRIM(STR(this.w_PUNPAD.w_ASTIPNUM, this.w_PUNPAD.w_FRDIMENS, this.w_PUNPAD.w_FRNUMDEC))
                  case this.w_FRINPUTA = "D"
                    this.w_PUNPAD.w_ASTIPCAR = ""
                    this.w_PUNPAD.w_ASTIPNUM = 0
                    if VARTYPE( SaveAttr.CAVALATT ) $ "D-T"
                      this.w_PUNPAD.w_ASTIPDAT = DATE( YEAR(SaveAttr.CAVALATT), MONTH(SaveAttr.CAVALATT), DAY(SaveAttr.CAVALATT) )
                    else
                      this.w_PUNPAD.w_ASTIPDAT = CTOD(SaveAttr.CAVALATT)
                    endif
                    this.w_PUNPAD.w_ASVALATT = DTOC(this.w_PUNPAD.w_ASTIPDAT)
                endcase
              otherwise
                if EMPTY(NVL(this.w_PUNPAD.w_CACODGRU , " "))
                  this.w_PUNPAD.w_CACODMOD = SaveAttr.MACODICE
                  if UPPER(this.w_PUNPAD.Class)<>"TCGSCC_MAA" AND UPPER(this.w_PUNPAD.Class)<>"TCGSCC_MDL" AND UPPER(this.w_PUNPAD.Class)<>"TCGSCC_MTL"
                    SETVALUELINKED( "D", this.w_PUNPAD, "w_CACODGRU",SaveAttr.CACODGRU)
                  endif
                  this.w_PUNPAD.w_CACODGRU = SaveAttr.CACODGRU
                endif
                if EMPTY(NVL(this.w_PUNPAD.w_CACODFAM , " "))
                  this.w_PUNPAD.w_CACODFAM = SaveAttr.CACODFAM
                endif
                do case
                  case TYPE("SaveAttr.CAVALATT") = "C"
                    this.w_PUNPAD.w_CAVALATT = SaveAttr.CAVALATT
                  case TYPE("SaveAttr.CAVALATT") $ "D-T"
                    this.w_PUNPAD.w_CAVALATT = DTOC( SaveAttr.CAVALATT )
                  case TYPE("SaveAttr.CAVALATT") = "N"
                    this.w_PUNPAD.w_CAVALATT = alltrim(STR(SaveAttr.CAVALATT, this.w_PUNPAD.w_FRDIMENS, this.w_PUNPAD.w_FRNUMDEC ))
                endcase
            endcase
            this.w_PUNPAD.SaveDependsOn()     
            this.w_PUNPAD.NotifyEvent("w_CAVALATT Changed")     
            this.w_PUNPAD.SaveRow()     
            ENDSCAN
            USE IN "SaveAttr"
          endif
        else
          if this.w_PUNPAD.w_TIPO="A"
            * --- Se fisso da archivio il dato viene cancellato
            if VARTYPE( this.w_PUNPAD.w_ASTIPCAR ) <> "U"
              this.w_PUNPAD.w_ASTIPCAR = ""
            endif
            if VARTYPE( this.w_PUNPAD.w_ASTIPNUM ) <> "U"
              this.w_PUNPAD.w_ASTIPNUM = 0
            endif
            if VARTYPE( this.w_PUNPAD.w_ASTIPDAT ) <> "U"
              this.w_PUNPAD.w_ASTIPDAT = cp_chartodate ("  -  -  ")
            endif
            this.w_PUNPAD.SaveDependsOn()     
            this.w_PUNPAD.NotifyEvent("w_CAVALATT Changed")     
            this.w_PUNPAD.SaveRow()     
          endif
          if this.w_PUNPAD.w_TIPO="B"
            * --- Se misto da archivio il dato viene copiato in w_ASVALATT
            if this.w_PUNPAD.w_INPUTA="C"
              if VARTYPE( this.w_PUNPAD.w_ASTIPCAR ) <> "U"
                this.w_PUNPAD.w_ASVALATT = LEFT( this.w_PUNPAD.w_ASTIPCAR,this.w_PUNPAD.w_FRDIMENS )
              endif
            endif
            if this.w_PUNPAD.w_INPUTA="N"
              if VARTYPE( this.w_PUNPAD.w_ASTIPNUM ) <> "U"
                this.w_PUNPAD.w_ASVALATT = ALLTRIM(STR(this.w_PUNPAD.w_ASTIPNUM, this.w_PUNPAD.w_FRDIMENS, this.w_PUNPAD.w_FRNUMDEC))
              endif
            endif
            if this.w_PUNPAD.w_INPUTA="D"
              if VARTYPE( this.w_PUNPAD.w_ASTIPDAT ) <> "U"
                this.w_PUNPAD.w_ASVALATT = DTOC( this.w_PUNPAD.w_ASTIPDAT )
              endif
            endif
            this.w_PUNPAD.SaveDependsOn()     
            this.w_PUNPAD.SaveRow()     
          endif
        endif
        * --- Riposizionamento in riga da dove lo zoom � partito
        this.w_PUNPAD.RePos()     
      case this.pOper=="SAVAT"
        SELECT * FROM (this.oParentObject.w_ZCONTI.cCursor) WHERE XCHK=1 INTO CURSOR "SaveAttr"
        if TYPE("SAVEATTR.CAVALATT") = "U"
          * --- Se � andato in esecuzione lo zoom specificato nella famiglia, la struttura dello zoom � diversa, per cui occorre modificare SAVEATTR
          L_COMMAND = "SELECT "
          L_COMMAND = L_COMMAND + CP_TOSTR(this.w_PUNPAD.w_CACODGRU) + " AS CACODGRU, "
          L_COMMAND = L_COMMAND + CP_TOSTR( this.w_PUNPAD.w_CACODFAM ) + " AS CACODFAM, "
          L_COMMAND = L_COMMAND + CP_TOSTR( this.w_PUNPAD.w_CACODMOD ) + " AS MACODICE, "
          L_COMMAND = L_COMMAND + this.w_PUNPAD.w_FR_CAMPO + " AS CAVALATT "
          L_COMMAND = L_COMMAND + "FROM SaveAttr INTO CURSOR SaveAttr"
          &L_COMMAND
        endif
      case this.pOper=="SELEC"
        Select (this.oParentObject.w_ZCONTI.cCursor)
        this.w_NFLTATTR = RECNO()
        COUNT FOR XCHK= 1 To this.w_CONTAX
        if this.w_CONTAX=0
          * --- Se non ci sono altre righe selezionate salvo l'attributo altrimenti viene attivato il flag e basta
          GO this.w_NFLTATTR
          REPLACE XCHK With 1
          this.w_PUNPAD.Ecpsave()     
        else
          GO this.w_NFLTATTR
          REPLACE XCHK With IIF(XCHK=1,0,1)
        endif
    endcase
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_039EBBA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from ASS_ATTR
    i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"GUID = "+cp_ToStrODBC(this.w_APPOGUID);
            +" and ASMODFAM = "+cp_ToStrODBC(this.oParentObject.w_SECODMOD);
            +" and ASCODATT = "+cp_ToStrODBC(Del_Attr.t_CACODGRU);
            +" and ASCODFAM = "+cp_ToStrODBC(Del_Attr.t_CACODFAM);
            +" and ASVALATT = "+cp_ToStrODBC(Del_Attr.t_CAVALATT);
             )
    else
      delete from (i_cTable) where;
            GUID = this.w_APPOGUID;
            and ASMODFAM = this.oParentObject.w_SECODMOD;
            and ASCODATT = Del_Attr.t_CACODGRU;
            and ASCODFAM = Del_Attr.t_CACODFAM;
            and ASVALATT = Del_Attr.t_CAVALATT;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_03A0A1E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GESTGUID ="+cp_NullLink(cp_ToStrODBC(this.w_APPOGUID),'CONTI','GESTGUID');
          +i_ccchkf ;
      +" where ";
          +"ANTIPCON = "+cp_ToStrODBC(this.w_APPTIPCLF);
          +" and ANCODICE = "+cp_ToStrODBC(this.w_APPCODCON);
             )
    else
      update (i_cTable) set;
          GESTGUID = this.w_APPOGUID;
          &i_ccchkf. ;
       where;
          ANTIPCON = this.w_APPTIPCLF;
          and ANCODICE = this.w_APPCODCON;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_038936B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from ASS_ATTR
    i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2],.t.,this.ASS_ATTR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" ASS_ATTR ";
          +" where GUID="+cp_ToStrODBC(this.w_APPOGUID)+" And ASMODFAM="+cp_ToStrODBC(this.oParentObject.w_SECODMOD)+" And ASCODATT="+cp_ToStrODBC(this.w_CACODGRU)+" And ASCODFAM="+cp_ToStrODBC(this.w_CACODFAM)+" And ASVALATT="+cp_ToStrODBC(this.w_CAVALATT)+"";
           ,"_Curs_ASS_ATTR")
    else
      select COUNT(*) AS CONTA from (i_cTable);
       where GUID=this.w_APPOGUID And ASMODFAM=this.oParentObject.w_SECODMOD And ASCODATT=this.w_CACODGRU And ASCODFAM=this.w_CACODFAM And ASVALATT=this.w_CAVALATT;
        into cursor _Curs_ASS_ATTR
    endif
    if used('_Curs_ASS_ATTR')
      select _Curs_ASS_ATTR
      locate for 1=1
      do while not(eof())
      this.w_OKINSATT = NVL(_Curs_ASS_ATTR.CONTA, 0)=0
        select _Curs_ASS_ATTR
        continue
      enddo
      use
    endif
    if this.w_OKINSATT
      * --- Read from FAM_ATTR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.FAM_ATTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FAM_ATTR_idx,2],.t.,this.FAM_ATTR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "FRMULTIP"+;
          " from "+i_cTable+" FAM_ATTR where ";
              +"FRCODICE = "+cp_ToStrODBC(this.w_CACODFAM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          FRMULTIP;
          from (i_cTable) where;
              FRCODICE = this.w_CACODFAM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_FRMULTIP = NVL(cp_ToDate(_read_.FRMULTIP),cp_NullValue(_read_.FRMULTIP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if w_FRMULTIP="S"
        * --- Select from ASS_ATTR
        i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2],.t.,this.ASS_ATTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" ASS_ATTR ";
              +" where GUID="+cp_ToStrODBC(this.w_APPOGUID)+" And ASMODFAM="+cp_ToStrODBC(this.oParentObject.w_SECODMOD)+" And ASCODATT="+cp_ToStrODBC(this.w_CACODGRU)+" And ASCODFAM="+cp_ToStrODBC(this.w_CACODFAM)+"";
               ,"_Curs_ASS_ATTR")
        else
          select COUNT(*) AS CONTA from (i_cTable);
           where GUID=this.w_APPOGUID And ASMODFAM=this.oParentObject.w_SECODMOD And ASCODATT=this.w_CACODGRU And ASCODFAM=this.w_CACODFAM;
            into cursor _Curs_ASS_ATTR
        endif
        if used('_Curs_ASS_ATTR')
          select _Curs_ASS_ATTR
          locate for 1=1
          do while not(eof())
          this.w_OKINSATT = NVL(_Curs_ASS_ATTR.CONTA, 0)=0
            select _Curs_ASS_ATTR
            continue
          enddo
          use
        endif
      endif
      if this.w_OKINSATT
        SELECT "Add_Attr"
        * --- Insert into ASS_ATTR
        i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ASS_ATTR_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"GUID"+",CPROWNUM"+",CPROWORD"+",ASMODFAM"+",ASCODATT"+",ASCODFAM"+",ASTIPNUM"+",ASTIPDAT"+",ASTIPCAR"+",ASVALATT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_APPOGUID),'ASS_ATTR','GUID');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'ASS_ATTR','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'ASS_ATTR','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SECODMOD),'ASS_ATTR','ASMODFAM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CACODGRU),'ASS_ATTR','ASCODATT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CACODFAM),'ASS_ATTR','ASCODFAM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ASTIPNUM),'ASS_ATTR','ASTIPNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ASTIPDAT),'ASS_ATTR','ASTIPDAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ASTIPCAR),'ASS_ATTR','ASTIPCAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAVALATT),'ASS_ATTR','ASVALATT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'GUID',this.w_APPOGUID,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'ASMODFAM',this.oParentObject.w_SECODMOD,'ASCODATT',this.w_CACODGRU,'ASCODFAM',this.w_CACODFAM,'ASTIPNUM',this.w_ASTIPNUM,'ASTIPDAT',this.w_ASTIPDAT,'ASTIPCAR',this.w_ASTIPCAR,'ASVALATT',this.w_CAVALATT)
          insert into (i_cTable) (GUID,CPROWNUM,CPROWORD,ASMODFAM,ASCODATT,ASCODFAM,ASTIPNUM,ASTIPDAT,ASTIPCAR,ASVALATT &i_ccchkf. );
             values (;
               this.w_APPOGUID;
               ,this.w_CPROWNUM;
               ,this.w_CPROWORD;
               ,this.oParentObject.w_SECODMOD;
               ,this.w_CACODGRU;
               ,this.w_CACODFAM;
               ,this.w_ASTIPNUM;
               ,this.w_ASTIPDAT;
               ,this.w_ASTIPCAR;
               ,this.w_CAVALATT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_CPROWNUM = this.w_CPROWNUM + 1
      endif
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguo la query per estrazione dei dati in base ai filtri dichiarati in prima pagina
    *     ad esclusione degli attributi che verranno analizzati in seguito
    vq_exec("query\GSAR_KMA",this,"_Conti_")
    * --- Memorizzo cursore per eventuale stampa
    SELECT * FROM "_Conti_" INTO CURSOR "__Tmp__"
    * --- Memorizzo gli attributi impostati come filtro
    select t_CACODMOD,t_CACODGRU,t_CACODFAM,t_CAVALATT from (this.w_GSAR_MAC.cTrsName); 
 where Not Empty (t_CAVALATT) into Cursor "Flt_Attr"
    this.w_NFLTATTR = RECCOUNT("Flt_Attr")
    if this.w_NFLTATTR>0
      if this.oParentObject.w_TIPSELE = "T"
        * --- Conto i record (attributi) per ogni intestatario
        *     (serivir� pi� avanti per scartare intestatari non validi se attivo in maschera il flag Selezione Puntuale)
        Select GUID, count(*) as NUM from "_Conti_" where ASMODFAM is not null group by GUID into cursor "_NAttrib_"
      endif
      * --- La join consente gi� di scartare tutti intestatari che non hanno neppure un attributo in comune
      Select _Conti_.* from "_Conti_" inner join "Flt_Attr" on; 
 ASMODFAM=t_CACODMOD and ASCODATT=t_CACODGRU and ASCODFAM=t_CACODFAM and ASVALATT=t_CAVALATT; 
 Into Cursor "_Conti_"
      SELECT "_Conti_"
      =wrcursor("_Conti_")
      * --- 'AT = Almeno uno / Tutti validi
      if this.oParentObject.w_TIPSELE $ "AT"
        if this.oParentObject.w_TIPSELE = "T"
          * --- Dal risultato della join conto per ogni dettaglio i record (attributri) rimasti.
          *     Se NON coincidono col totale di attributi di selezione l'intestatario NON � valido,
          *     � presente cio� almeno un attributo di selezione non soddisfatto.
          *     Se coincidono occorre controllare il flag PUNTUALE nella maschera:
          *     se ATTIVO confronto il totale dei record (attributi) di CONTAJ (risulatto della join)
          *     con il totale dei record di _NAttrib_ (totale di attributi per Intestatario), se coincide il dettaglio viene ACCETTATO
          *     se NON coincide il dettaglio viene SCARTATO.
          *     Se il flag puntuale � disattivo il dettaglio � ACCETTATO
          Select GUID, count(*) as NUM from "_Conti_" where ASMODFAM is not null group by GUID into cursor "ContaJ"
          Select "ContaJ"
          GO TOP
          SCAN
          this.w_APPOGUID = ContaJ.GUID
          if this.w_NFLTATTR>ContaJ.Num
            * --- Se sono presenti piu attributi di selezione rspetto a quelli associati all'intestatario scarto il dettaglio in questione
            DELETE FROM "_Conti_" WHERE GUID=this.w_APPOGUID
            Select "_Conti_"
            GO TOP
          else
            if this.oParentObject.w_PUNTUALE="S"
              Select "_NAttrib_"
              Locate for GUID=this.w_APPOGUID
              this.w_CONTANUM = _NAttrib_.NUM 
              Select "ContaJ"
              if this.w_CONTANUM > ContaJ.NUM
                * --- Se sono presenti piu attributi di selezione rspetto a quelli associati all'intestatario scarto il dettaglio in questione
                DELETE FROM "_Conti_" WHERE GUID=this.w_APPOGUID
                Select "_Conti_"
                GO TOP
              endif
            endif
          endif
          Select "ContaJ"
          ENDSCAN
        else
          * --- La join precedente basta per selezionare 'almeno uno'
        endif
      else
        * --- Almeno uno per gruppo o per famiglia
        if this.oParentObject.w_TIPSELE="G"
          Select distinct ANTIPCON, ANCODICE, GUID, ASMODFAM, ASCODATT From "_Conti_" into cursor "_Gruppo_"
          Select Count(*) as CONTA, ANTIPCON, ANCODICE, GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
          * --- Count per GRUPPO
          select t_CACODMOD,t_CACODGRU from (this.w_GSAR_MAC.cTrsName); 
 where Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU into Cursor "Grp_Attr"
        else
          Select distinct ANTIPCON, ANCODICE, GUID, ASMODFAM, ASCODATT, ASCODFAM From "_Conti_" into cursor "_Gruppo_"
          Select Count(*) as CONTA, ANTIPCON, ANCODICE, GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
          * --- Count per GRUPPO e FAMIGLIA
          select t_CACODMOD,t_CACODGRU, t_CACODFAM from (this.w_GSAR_MAC.cTrsName); 
 where Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU,t_CACODFAM into Cursor "Grp_Attr"
        endif
        this.w_NFLTATTR = RECCOUNT("Grp_Attr")
        if this.w_NFLTATTR > 0
          * --- Sono eliminati tutti i record che non soddisfano
          Delete from _Conti_ where ANTIPCON+ANCODICE+GUID not in; 
 (Select ANTIPCON+ANCODICE+GUID from _Gruppo_ where CONTA= this.w_NFLTATTR)
        endif
      endif
    endif
    * --- Preparo cursore per eventuale stampa
    Select __Tmp__.* From "__Tmp__" inner join "_Conti_" on __Tmp__.ANTIPCON=_Conti_.ANTIPCON and __Tmp__.ANCODICE=_Conti_.ANCODICE INTO CURSOR "__Tmp__"
    Select DISTINCT * From "__Tmp__" INTO CURSOR "__Tmp__"
    Select "_Conti_"
    GO TOP
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Svuoto lo zoom
    Select (this.oParentObject.w_ZCONTI.cCursor)
    ZAP
    SELECT ANTIPCON, ANCODICE, MAX(ANDESCRI) AS ANDESCRI, MAX(ANPARIVA) AS ANPARIVA, MAX(GUID) AS GUID, 0 AS XCHK FROM "_Conti_" WHERE NOT DELETED() GROUP BY ANTIPCON, ANCODICE INTO CURSOR "__Tmp__"
    Select "__Tmp__"
    SCAN
    Scatter Memvar
    Select (this.oParentObject.w_ZCONTI.cCursor)
    Append blank
    Gather memvar
    ENDSCAN
    SELECT "__Tmp__"
    USE
    * --- Refresh della griglia
    Select (this.oParentObject.w_ZCONTI.cCursor)
    GO TOP
    this.oParentObject.w_ZCONTI.Grd.Refresh()     
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED("_Conti_")
      USE IN "_Conti_"
    endif
    if USED("Flt_Attr")
      USE IN "Flt_Attr"
    endif
    if USED("Grp_Attr")
      USE IN "Grp_Attr"
    endif
    if USED("_Gruppo_")
      USE IN "_Gruppo_"
    endif
    if USED("_NAttrib_")
      USE IN "_NAttrib_"
    endif
    if USED("ContaJ")
      USE IN "ContaJ"
    endif
    if USED("__Tmp__")
      USE IN "__Tmp__"
    endif
    if USED("_Selez_")
      USE IN "_Selez_"
    endif
    if USED("_SelezChk_")
      USE IN "_SelezChk_"
    endif
    if USED("Del_Attr")
      USE IN "Del_Attr"
    endif
    if USED("Add_Attr")
      USE IN "Add_Attr"
    endif
    if USED("_Diff_")
      USE IN "_Diff_"
    endif
    if USED("_SolCli_")
      USE IN "_SolCli_"
    endif
    if USED("Att_Obbl")
      USE IN "Att_Obbl"
    endif
    if USED("OutCurs")
      USE IN "OutCurs"
    endif
    if USED("Chk_obbl")
      USE IN "Chk_obbl"
    endif
    if USED("NoMoreAtt")
      USE IN "NoMoreAtt"
    endif
    if USED("_ChkAttr_")
      USE IN "_ChkAttr_"
    endif
    * --- Rimuove variabili globali definite per report
    release L_ANTIPCON, L_ANCODINI, L_ANCODFIN, L_ANDESINI, L_ANDESFIN, L_MCCODICE, L_MCDESCRI, L_NACODNAZ, L_CCCODICE, L_ZOCODZON
    release L_LUCODICE, L_CTCODICE, L_CSCODICE, L_GPCODICE, L_VACODVAL, L_AGCODAG1, L_AGCODAG2, L_PACODICE, L_BANUMCOR, L_PUNTUALE, L_TYPESTMP
    release L_GRUATTRIB, L_FAMATTRIB, L_VALATTRIB
    release L_GRUATTOBB, L_FAMATTOBB
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    public L_ANTIPCON, L_ANCODINI, L_ANCODFIN, L_ANDESINI, L_ANDESFIN, L_MCCODICE, L_MCDESCRI, L_NACODNAZ, L_CCCODICE, L_ZOCODZON
    public L_LUCODICE, L_CTCODICE, L_CSCODICE, L_GPCODICE, L_VACODVAL, L_AGCODAG1, L_AGCODAG2, L_PACODICE, L_BANUMCOR, L_PUNTUALE, L_TYPESTMP
    public L_GRUATTRIB, L_FAMATTRIB, L_VALATTRIB, L_FROMANALIS
    public L_GRUATTOBB, L_FAMATTOBB
    if UPPER(this.w_PUNPAD.Class)=="TCGSAR_MAC"
      L_FROMANALIS = .T.
      L_ANDESINI = ""
      L_ANDESFIN = ""
      L_MCDESCRI = ""
    else
      L_ANDESINI=this.oParentObject.w_ANDESINI
      L_ANDESFIN=this.oParentObject.w_ANDESFIN
      L_MCDESCRI=this.oParentObject.w_MCDESCRI
    endif
    L_ANTIPCON=this.oParentObject.w_ANTIPCON
    L_ANCODINI=this.oParentObject.w_ANCODINI
    L_ANCODFIN=this.oParentObject.w_ANCODFIN
    L_MCCODICE=this.oParentObject.w_MCCODICE
    L_NACODNAZ=this.oParentObject.w_NACODNAZ
    L_CCCODICE=this.oParentObject.w_CCCODICE
    L_ZOCODZON=this.oParentObject.w_ZOCODZON
    L_LUCODICE=this.oParentObject.w_LUCODICE
    L_CTCODICE=this.oParentObject.w_CTCODICE
    L_CSCODICE=this.oParentObject.w_CSCODICE
    L_GPCODICE=this.oParentObject.w_GPCODICE
    L_VACODVAL=this.oParentObject.w_VACODVAL
    L_AGCODAG1=this.oParentObject.w_AGCODAG1
    L_AGCODAG2=this.oParentObject.w_AGCODAG2
    L_PACODICE=this.oParentObject.w_PACODICE
    L_BANUMCOR=this.oParentObject.w_BANUMCOR
    L_PUNTUALE=this.oParentObject.w_PUNTUALE
    L_TYPESTMP = IIF(this.pOper="PRIAT", "A", "M")
    L_GRUATTRIB=""
    L_FAMATTRIB=""
    L_VALATTRIB=""
    SELECT "Flt_Attr"
    GO TOP
    SCAN
    if !EMPTY(L_VALATTRIB)
      L_GRUATTRIB=L_GRUATTRIB+CHR(10)
      L_FAMATTRIB=L_FAMATTRIB+CHR(10)
      L_VALATTRIB=L_VALATTRIB+CHR(10)
    endif
    L_GRUATTRIB=L_GRUATTRIB+LEFT(ALLTRIM(t_CACODGRU)+SPACE(10),10)
    L_FAMATTRIB=L_FAMATTRIB+LEFT(ALLTRIM(t_CACODFAM)+SPACE(10),10)
    L_VALATTRIB=L_VALATTRIB+LEFT(ALLTRIM(t_CAVALATT)+SPACE(20),20)
    ENDSCAN
    L_GRUATTOBB=""
    L_FAMATTOBB=""
    SELECT "Att_Obbl"
    GO TOP
    SCAN
    if !EMPTY(L_GRUATTOBB)
      L_GRUATTOBB=L_GRUATTOBB+CHR(10)
      L_FAMATTOBB=L_FAMATTOBB+CHR(10)
    endif
    L_GRUATTOBB=L_GRUATTOBB+LEFT(ALLTRIM(grcodice)+SPACE(10),10)
    L_FAMATTOBB=L_FAMATTOBB+LEFT(ALLTRIM(grfamatt)+SPACE(10),10)
    ENDSCAN
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Preparo il cursore con i soli clienti selezionati
    vq_exec("query\GSAR_KMA",this,"_Conti_")
    SELECT antipcon, ancodice FROM (this.oParentObject.w_ZCONTI.cCursor) WHERE XCHK=1 INTO CURSOR "_SelezChk_"
    if this.pOper=="PRIAT"
      vq_exec("query\GSAR_BMG",this,"Att_Obbl")
      SELECT _Conti_.*, "A" as Stato FROM _Conti_ INNER JOIN _SelezChk_ on _Conti_.antipcon=_SelezChk_.antipcon and _Conti_.ancodice=_SelezChk_.ancodice INTO CURSOR "OutCurs"
      select t_CACODMOD,t_CACODGRU,t_CACODFAM,t_CAVALATT from (this.w_GSAR_MAC.cTrsName); 
 where Not Empty (t_CAVALATT) into Cursor "Flt_Attr"
    else
      SELECT _Conti_.*, "P" as Stato FROM _Conti_ INNER JOIN _SelezChk_ on _Conti_.antipcon=_SelezChk_.antipcon and _Conti_.ancodice=_SelezChk_.ancodice INTO CURSOR "_Conti_"
      select t_CACODMOD,t_CACODGRU,t_CACODFAM,t_CAVALATT from (this.w_GSAR_MAC.cTrsName); 
 where Not Empty (t_CAVALATT) into Cursor "Flt_Attr"
      * --- Memorizzo gli attributi aggiunti ed eliminati
      select t_CACODGRU,t_CACODFAM,t_CAVALATT from (this.w_GSAR_MDA.cTrsName) where Not Empty (t_CAVALATT) and t_CAFLELIM=1 into Cursor "Del_Attr"
      select t_CACODGRU,t_CACODFAM,t_CAVALATT from (this.w_GSAR_MDA.cTrsName) where Not Empty (t_CAVALATT) and t_CAFLINSE=1 into Cursor "Add_Attr"
      * --- Copio il cursore per creare lo stato dopo l'aggiornamento
      Select * From "_Conti_" INTO CURSOR "_Diff_"
      =wrcursor("_Diff_")
      Select "_Diff_"
      GO TOP
      * --- Aggiorno lo stato di riga
      UPDATE "_Diff_" SET STATO="S" WHERE 1=1
      * --- Preparo il cursore con i soli clienti per inserimento attributi
      Select * from "_Diff_" GROUP BY antipcon, ancodice INTO CURSOR "_SolCli_"
      SELECT "_Diff_"
      GO TOP
      * --- Elimino dal cursore gli attributi eliminati
      DELETE FROM _Diff_ WHERE ALLTRIM(ASCODATT)+ALLTRIM(ASCODFAM)+ALLTRIM(ASVALATT) IN ; 
 (SELECT ALLTRIM(t_CACODGRU)+ALLTRIM(t_CACODFAM)+ALLTRIM(t_CAVALATT) AS ATTRIBUTO FROM "Del_Attr")
      SELECT "_SolCli_"
      GO TOP
      SCAN
      this.w_APPOGUID = _SolCli_.Guid
      SELECT "Add_Attr"
      GO TOP
      SCAN
      this.w_OKINSATT = .T.
      this.w_CACODGRU = Add_Attr.t_CACODGRU
      this.w_CACODFAM = Add_Attr.t_CACODFAM
      this.w_CAVALATT = Add_Attr.t_CAVALATT
      * --- Verifico se l'attributo pu� essere inserito
      SELECT COUNT(*) AS CONTA FROM "_Diff_" WHERE _Diff_.GUID=this.w_APPOGUID And _Diff_.ASMODFAM=this.oParentObject.w_SECODMOD And _Diff_.ASCODATT=this.w_CACODGRU And _Diff_.ASCODFAM=this.w_CACODFAM And _Diff_.ASVALATT=this.w_CAVALATT INTO CURSOR "_ChkAttr_"
      if USED("_ChkAttr_")
        SELECT "_ChkAttr_"
        GO TOP
        this.w_OKINSATT = NVL(_ChkAttr_.CONTA, 0)=0
        USE IN "_ChkAttr_"
      endif
      if this.w_OKINSATT
        * --- Read from FAM_ATTR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.FAM_ATTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FAM_ATTR_idx,2],.t.,this.FAM_ATTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "FRMULTIP"+;
            " from "+i_cTable+" FAM_ATTR where ";
                +"FRCODICE = "+cp_ToStrODBC(this.w_CACODFAM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            FRMULTIP;
            from (i_cTable) where;
                FRCODICE = this.w_CACODFAM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_FRMULTIP = NVL(cp_ToDate(_read_.FRMULTIP),cp_NullValue(_read_.FRMULTIP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if w_FRMULTIP="S"
          SELECT COUNT(*) AS CONTA FROM "_Diff_" WHERE _Diff_.GUID=this.w_APPOGUID And _Diff_.ASMODFAM=this.oParentObject.w_SECODMOD And _Diff_.ASCODATT=this.w_CACODGRU And _Diff_.ASCODFAM=this.w_CACODFAM INTO CURSOR "_ChkAttr_"
          if USED("_ChkAttr_")
            SELECT "_ChkAttr_"
            GO TOP
            this.w_OKINSATT = NVL(_ChkAttr_.CONTA, 0)=0
            USE IN "_ChkAttr_"
          endif
          if this.w_OKINSATT
            * --- Verifico che lo stesso attributo non sia stato inserito due volte nel cursore
            SELECT COUNT(*) AS CONTA FROM "_DIFF_" WHERE GUID=this.w_APPOGUID And ASMODFAM=this.oParentObject.w_SECODMOD And ASCODATT=this.w_CACODGRU And ASCODFAM=this.w_CACODFAM and stato="S" INTO CURSOR "ContaAttr"
            if USED("ContaAttr")
              SELECT "ContaAttr"
              GO TOP
              this.w_OKINSATT = NVL(CONTA, 0)=0
              USE
            endif
          endif
        endif
        if this.w_OKINSATT
          INSERT INTO "_Diff_" (antipcon, ancodice, andescri, anpariva, guid, asmodfam, ascodatt, ascodfam, asvalatt, stato) VALUES ; 
 (_SolCli_.antipcon, _SolCli_.ancodice, _SolCli_.andescri, _SolCli_.anpariva, _SolCli_.guid, this.oParentObject.w_SECODMOD, Add_Attr.t_cacodgru, Add_Attr.t_cacodfam, Add_Attr.t_cavalatt, "S")
        else
          INSERT INTO "_Diff_" (antipcon, ancodice, andescri, anpariva, guid, asmodfam, ascodatt, ascodfam, asvalatt, stato) VALUES ; 
 (_SolCli_.antipcon, _SolCli_.ancodice, _SolCli_.andescri, _SolCli_.anpariva, _SolCli_.guid, this.oParentObject.w_SECODMOD, Add_Attr.t_cacodgru, Add_Attr.t_cacodfam, Add_Attr.t_cavalatt, "U")
        endif
      else
        INSERT INTO "_Diff_" (antipcon, ancodice, andescri, anpariva, guid, asmodfam, ascodatt, ascodfam, asvalatt, stato) VALUES ; 
 (_SolCli_.antipcon, _SolCli_.ancodice, _SolCli_.andescri, _SolCli_.anpariva, _SolCli_.guid, this.oParentObject.w_SECODMOD, Add_Attr.t_cacodgru, Add_Attr.t_cacodfam, Add_Attr.t_cavalatt, "Z")
      endif
      ENDSCAN
      SELECT "_SolCli_"
      ENDSCAN
      * --- Unisco i due cursori per le verifiche finali
      SELECT * FROM "_Conti_" UNION ALL SELECT * FROM "_Diff_" Order by antipcon, ancodice, stato INTO CURSOR "OutCurs"
      =wrcursor("OutCurs")
      * --- Verifico la presenza di tutti gli attributi obbligatori
      vq_exec("query\GSAR_BMG",this,"Att_Obbl")
      SELECT "_SolCli_"
      GO TOP
      SCAN
      SELECT "Att_Obbl"
      this.w_OKINSATT = .T.
      GO TOP
      SCAN
      SELECT COUNT(*) AS CONTA FROM OutCurs WHERE OutCurs.antipcon=_SolCli_.antipcon AND OutCurs.ancodice=_SolCli_.Ancodice AND OutCurs.ascodatt=Att_Obbl.grcodice Into Cursor "Chk_Obbl"
      if USED("Chk_Obbl")
        SELECT "Chk_Obbl"
        GO TOP
        if Chk_Obbl.Conta=0
          this.w_OKINSATT = .T.
        else
          USE IN "Chk_Obbl"
          SELECT COUNT(*) AS CONTA FROM OutCurs WHERE OutCurs.antipcon=_SolCli_.antipcon AND OutCurs.ancodice=_SolCli_.Ancodice AND OutCurs.ascodatt=Att_Obbl.grcodice AND OutCurs.ascodfam=Att_obbl.grfamatt Into Cursor "Chk_Obbl"
          if USED("Chk_Obbl")
            SELECT "Chk_Obbl"
            GO TOP
            this.w_OKINSATT = Chk_Obbl.Conta>0
          else
            this.w_OKINSATT = .F.
          endif
        endif
      else
        this.w_OKINSATT = .F.
      endif
      if Not(this.w_OKINSATT)
        exit
      endif
      SELECT "Att_Obbl"
      ENDSCAN
      if Not(this.w_OKINSATT)
        * --- Attributi obbligatori non impostati
        UPDATE "OutCurs" SET Stato="T" WHERE OutCurs.antipcon=_SolCli_.antipcon AND OutCurs.ancodice=_SolCli_.ancodice AND OutCurs.Stato="S"
      endif
      SELECT "_SolCli_"
      ENDSCAN
      * --- Inserisco un record vuoto di tipo situazione successiva per tutti i clienti che
      *     al termine dell'elaborazione non avranno pi� attributi impostati
      SELECT * FROM outcurs WHERE ALLTRIM(antipcon)+ALLTRIM(ancodice) NOT in (SELECT ALLTRIM(antipcon)+ALLTRIM(ancodice) AS Codice FROM Outcurs WHERE Stato="S" GROUP BY antipcon, ancodice) GROUP BY antipcon, ancodice INTO CURSOR "NoMoreAtt"
      if USED("NoMoreAtt")
        =WRCURSOR("NoMoreAtt")
        SELECT "NoMoreAtt"
        GO TOP
        UPDATE "NoMoreAtt" SET STATO="S", ASMODFAM="", ASCODATT="", ASCODFAM="", ASTIPNUM=0, ASTIPDAT=CTOD("  -  -    "), ASTIPCAR="", ASVALATT=""
        GO TOP
        INSERT INTO "OutCurs" SELECT * FROM "NoMoreAtt"
      endif
    endif
  endproc


  proc Init(oParentObject,pOper,pFILTER)
    this.pOper=pOper
    this.pFILTER=pFILTER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ASS_ATTR'
    this.cWorkTables[2]='FAM_ATTR'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='FAM_ATTR'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_ASS_ATTR')
      use in _Curs_ASS_ATTR
    endif
    if used('_Curs_ASS_ATTR')
      use in _Curs_ASS_ATTR
    endif
    if used('_Curs_ASS_ATTR')
      use in _Curs_ASS_ATTR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper,pFILTER"
endproc
