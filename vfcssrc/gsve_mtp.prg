* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_mtp                                                        *
*              Tabella provvigioni                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_35]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2013-01-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsve_mtp")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsve_mtp")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsve_mtp")
  return

* --- Class definition
define class tgsve_mtp as StdPCForm
  Width  = 467
  Height = 154
  Top    = 33
  Left   = 146
  cComment = "Tabella provvigioni"
  cPrg = "gsve_mtp"
  HelpContextID=188075415
  add object cnt as tcgsve_mtp
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsve_mtp as PCContext
  w_TPCATAGE = space(5)
  w_TPCHIAVE = space(10)
  w_TPPERSCO = 0
  w_TPPERAGE = 0
  w_TPPERZON = 0
  w_TPPROVVI = space(1)
  w_PROVVI = space(1)
  proc Save(i_oFrom)
    this.w_TPCATAGE = i_oFrom.w_TPCATAGE
    this.w_TPCHIAVE = i_oFrom.w_TPCHIAVE
    this.w_TPPERSCO = i_oFrom.w_TPPERSCO
    this.w_TPPERAGE = i_oFrom.w_TPPERAGE
    this.w_TPPERZON = i_oFrom.w_TPPERZON
    this.w_TPPROVVI = i_oFrom.w_TPPROVVI
    this.w_PROVVI = i_oFrom.w_PROVVI
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TPCATAGE = this.w_TPCATAGE
    i_oTo.w_TPCHIAVE = this.w_TPCHIAVE
    i_oTo.w_TPPERSCO = this.w_TPPERSCO
    i_oTo.w_TPPERAGE = this.w_TPPERAGE
    i_oTo.w_TPPERZON = this.w_TPPERZON
    i_oTo.w_TPPROVVI = this.w_TPPROVVI
    i_oTo.w_PROVVI = this.w_PROVVI
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsve_mtp as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 467
  Height = 154
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-01-28"
  HelpContextID=188075415
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  TAB_PROV_IDX = 0
  cFile = "TAB_PROV"
  cKeySelect = "TPCATAGE,TPCHIAVE"
  cKeyWhere  = "TPCATAGE=this.w_TPCATAGE and TPCHIAVE=this.w_TPCHIAVE"
  cKeyDetail  = "TPCATAGE=this.w_TPCATAGE and TPCHIAVE=this.w_TPCHIAVE"
  cKeyWhereODBC = '"TPCATAGE="+cp_ToStrODBC(this.w_TPCATAGE)';
      +'+" and TPCHIAVE="+cp_ToStrODBC(this.w_TPCHIAVE)';

  cKeyDetailWhereODBC = '"TPCATAGE="+cp_ToStrODBC(this.w_TPCATAGE)';
      +'+" and TPCHIAVE="+cp_ToStrODBC(this.w_TPCHIAVE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"TAB_PROV.TPCATAGE="+cp_ToStrODBC(this.w_TPCATAGE)';
      +'+" and TAB_PROV.TPCHIAVE="+cp_ToStrODBC(this.w_TPCHIAVE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'TAB_PROV.CPROWNUM '
  cPrg = "gsve_mtp"
  cComment = "Tabella provvigioni"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TPCATAGE = space(5)
  w_TPCHIAVE = space(10)
  w_TPPERSCO = 0
  w_TPPERAGE = 0
  w_TPPERZON = 0
  w_TPPROVVI = space(1)
  o_TPPROVVI = space(1)
  w_PROVVI = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_mtpPag1","gsve_mtp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TAB_PROV'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAB_PROV_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAB_PROV_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsve_mtp'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from TAB_PROV where TPCATAGE=KeySet.TPCATAGE
    *                            and TPCHIAVE=KeySet.TPCHIAVE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsve_mtp
      * --- Setta Ordine per Sconto
      i_cOrder = 'order by TPPERSCO '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.TAB_PROV_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_PROV_IDX,2],this.bLoadRecFilter,this.TAB_PROV_IDX,"gsve_mtp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAB_PROV')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAB_PROV.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAB_PROV '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TPCATAGE',this.w_TPCATAGE  ,'TPCHIAVE',this.w_TPCHIAVE  )
      select * from (i_cTable) TAB_PROV where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TPCATAGE = NVL(TPCATAGE,space(5))
        .w_TPCHIAVE = NVL(TPCHIAVE,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TAB_PROV')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_TPPERSCO = NVL(TPPERSCO,0)
          .w_TPPERAGE = NVL(TPPERAGE,0)
          .w_TPPERZON = NVL(TPPERZON,0)
          .w_TPPROVVI = NVL(TPPROVVI,space(1))
        .w_PROVVI = .w_TPPROVVI
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TPCATAGE=space(5)
      .w_TPCHIAVE=space(10)
      .w_TPPERSCO=0
      .w_TPPERAGE=0
      .w_TPPERZON=0
      .w_TPPROVVI=space(1)
      .w_PROVVI=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_TPPERAGE = IIF(.w_TPPROVVI='A',.w_TPPERAGE,0)
        .w_TPPERZON = IIF(.w_TPPROVVI='A',.w_TPPERZON,0)
        .w_TPPROVVI = 'A'
        .w_PROVVI = .w_TPPROVVI
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAB_PROV')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'TAB_PROV',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAB_PROV_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPCATAGE,"TPCATAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPCHIAVE,"TPCHIAVE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TPPERSCO N(5,2);
      ,t_TPPERAGE N(5,2);
      ,t_TPPERZON N(5,2);
      ,t_TPPROVVI N(3);
      ,CPROWNUM N(10);
      ,t_PROVVI C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsve_mtpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERSCO_2_1.controlsource=this.cTrsName+'.t_TPPERSCO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERAGE_2_2.controlsource=this.cTrsName+'.t_TPPERAGE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERZON_2_3.controlsource=this.cTrsName+'.t_TPPERZON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTPPROVVI_2_4.controlsource=this.cTrsName+'.t_TPPROVVI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(101)
    this.AddVLine(200)
    this.AddVLine(342)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERSCO_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_PROV_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_PROV_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_PROV_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_PROV_IDX,2])
      *
      * insert into TAB_PROV
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAB_PROV')
        i_extval=cp_InsertValODBCExtFlds(this,'TAB_PROV')
        i_cFldBody=" "+;
                  "(TPCATAGE,TPCHIAVE,TPPERSCO,TPPERAGE,TPPERZON"+;
                  ",TPPROVVI,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TPCATAGE)+","+cp_ToStrODBC(this.w_TPCHIAVE)+","+cp_ToStrODBC(this.w_TPPERSCO)+","+cp_ToStrODBC(this.w_TPPERAGE)+","+cp_ToStrODBC(this.w_TPPERZON)+;
             ","+cp_ToStrODBC(this.w_TPPROVVI)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAB_PROV')
        i_extval=cp_InsertValVFPExtFlds(this,'TAB_PROV')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'TPCATAGE',this.w_TPCATAGE,'TPCHIAVE',this.w_TPCHIAVE)
        INSERT INTO (i_cTable) (;
                   TPCATAGE;
                  ,TPCHIAVE;
                  ,TPPERSCO;
                  ,TPPERAGE;
                  ,TPPERZON;
                  ,TPPROVVI;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_TPCATAGE;
                  ,this.w_TPCHIAVE;
                  ,this.w_TPPERSCO;
                  ,this.w_TPPERAGE;
                  ,this.w_TPPERZON;
                  ,this.w_TPPROVVI;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.TAB_PROV_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_PROV_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for ((t_TPPERAGE<>0 AND t_PROVVI='A') or ( t_PROVVI <>'A')) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_PROV')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_PROV')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for ((t_TPPERAGE<>0 AND t_PROVVI='A') or ( t_PROVVI <>'A')) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TAB_PROV
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_PROV')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TPPERSCO="+cp_ToStrODBC(this.w_TPPERSCO)+;
                     ",TPPERAGE="+cp_ToStrODBC(this.w_TPPERAGE)+;
                     ",TPPERZON="+cp_ToStrODBC(this.w_TPPERZON)+;
                     ",TPPROVVI="+cp_ToStrODBC(this.w_TPPROVVI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_PROV')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TPPERSCO=this.w_TPPERSCO;
                     ,TPPERAGE=this.w_TPPERAGE;
                     ,TPPERZON=this.w_TPPERZON;
                     ,TPPROVVI=this.w_TPPROVVI;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAB_PROV_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_PROV_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for ((t_TPPERAGE<>0 AND t_PROVVI='A') or ( t_PROVVI <>'A')) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete TAB_PROV
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for ((t_TPPERAGE<>0 AND t_PROVVI='A') or ( t_PROVVI <>'A')) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAB_PROV_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_PROV_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_TPPROVVI<>.w_TPPROVVI
          .w_TPPERAGE = IIF(.w_TPPROVVI='A',.w_TPPERAGE,0)
        endif
        if .o_TPPROVVI<>.w_TPPROVVI
          .w_TPPERZON = IIF(.w_TPPROVVI='A',.w_TPPERZON,0)
        endif
        .DoRTCalc(6,6,.t.)
        if .o_TPPROVVI<>.w_TPPROVVI
          .w_PROVVI = .w_TPPROVVI
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PROVVI with this.w_PROVVI
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTPPERAGE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTPPERAGE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTPPERZON_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTPPERZON_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERSCO_2_1.value==this.w_TPPERSCO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERSCO_2_1.value=this.w_TPPERSCO
      replace t_TPPERSCO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERSCO_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERAGE_2_2.value==this.w_TPPERAGE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERAGE_2_2.value=this.w_TPPERAGE
      replace t_TPPERAGE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERAGE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERZON_2_3.value==this.w_TPPERZON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERZON_2_3.value=this.w_TPPERZON
      replace t_TPPERZON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPERZON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPROVVI_2_4.RadioValue()==this.w_TPPROVVI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPROVVI_2_4.SetRadio()
      replace t_TPPROVVI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPROVVI_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'TAB_PROV')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not(NOT EMPTY(.w_TPPERAGE) or (.w_PROVVI<>'A' AND .w_TPPERSCO<>0))
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("% Provvigione agente mancante")
      endcase
      if (.w_TPPERAGE<>0 AND .w_PROVVI='A') or ( .w_PROVVI <>'A')
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TPPROVVI = this.w_TPPROVVI
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=((t_TPPERAGE<>0 AND t_PROVVI='A') or ( t_PROVVI <>'A'))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TPPERSCO=0
      .w_TPPERAGE=0
      .w_TPPERZON=0
      .w_TPPROVVI=space(1)
      .w_PROVVI=space(1)
      .DoRTCalc(1,3,.f.)
        .w_TPPERAGE = IIF(.w_TPPROVVI='A',.w_TPPERAGE,0)
        .w_TPPERZON = IIF(.w_TPPROVVI='A',.w_TPPERZON,0)
        .w_TPPROVVI = 'A'
        .w_PROVVI = .w_TPPROVVI
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TPPERSCO = t_TPPERSCO
    this.w_TPPERAGE = t_TPPERAGE
    this.w_TPPERZON = t_TPPERZON
    this.w_TPPROVVI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPROVVI_2_4.RadioValue(.t.)
    this.w_PROVVI = t_PROVVI
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TPPERSCO with this.w_TPPERSCO
    replace t_TPPERAGE with this.w_TPPERAGE
    replace t_TPPERZON with this.w_TPPERZON
    replace t_TPPROVVI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTPPROVVI_2_4.ToRadio()
    replace t_PROVVI with this.w_PROVVI
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsve_mtpPag1 as StdContainer
  Width  = 463
  height = 154
  stdWidth  = 463
  stdheight = 154
  resizeYpos=130
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=0, width=449,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="TPPERSCO",Label1="Sconto fino al",Field2="TPPERAGE",Label2="% Provv.agente",Field3="TPPERZON",Label3="% Provv.capo area",Field4="TPPROVVI",Label4="Provvigione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235730810

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=20,;
    width=447+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=21,width=446+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsve_mtpBodyRow as CPBodyRowCnt
  Width=437
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTPPERSCO_2_1 as StdTrsField with uid="NIUBJUHANK",rtseq=3,rtrep=.t.,;
    cFormVar="w_TPPERSCO",value=0,;
    ToolTipText = "% Massima di sconto sul documento per la quale sono previste le provvigioni",;
    HelpContextID = 60826501,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=89, Left=-2, Top=0, cSayPict=["99.99"], cGetPict=["99.99"]

  add object oTPPERAGE_2_2 as StdTrsField with uid="NTDEJLRZFR",rtseq=4,rtrep=.t.,;
    cFormVar="w_TPPERAGE",value=0,;
    ToolTipText = "% Provvigioni agente",;
    HelpContextID = 27272059,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=93, Left=91, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oTPPERAGE_2_2.mCond()
    with this.Parent.oContained
      return (.w_TPPROVVI='A')
    endwith
  endfunc

  add object oTPPERZON_2_3 as StdTrsField with uid="ZXJRSZTEKG",rtseq=5,rtrep=.t.,;
    cFormVar="w_TPPERZON",value=0,;
    ToolTipText = "% Provvigioni capo area",;
    HelpContextID = 90168444,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=192, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oTPPERZON_2_3.mCond()
    with this.Parent.oContained
      return (.w_TPPROVVI='A')
    endwith
  endfunc

  add object oTPPROVVI_2_4 as StdTrsCombo with uid="MLCGEYYKWV",rtrep=.t.,;
    cFormVar="w_TPPROVVI", RowSource=""+"Applicata,"+"Non applicata,"+"Esclusa" , ;
    ToolTipText = "Tipo provvigione: applicata, non applicata, esclusa",;
    HelpContextID = 108864383,;
    Height=21, Width=99, Left=333, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTPPROVVI_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TPPROVVI,&i_cF..t_TPPROVVI),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'N',;
    iif(xVal =3,'E',;
    'A'))))
  endfunc
  func oTPPROVVI_2_4.GetRadio()
    this.Parent.oContained.w_TPPROVVI = this.RadioValue()
    return .t.
  endfunc

  func oTPPROVVI_2_4.ToRadio()
    this.Parent.oContained.w_TPPROVVI=trim(this.Parent.oContained.w_TPPROVVI)
    return(;
      iif(this.Parent.oContained.w_TPPROVVI=='A',1,;
      iif(this.Parent.oContained.w_TPPROVVI=='N',2,;
      iif(this.Parent.oContained.w_TPPROVVI=='E',3,;
      0))))
  endfunc

  func oTPPROVVI_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oTPPERSCO_2_1.When()
    return(.t.)
  proc oTPPERSCO_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTPPERSCO_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_mtp','TAB_PROV','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TPCATAGE=TAB_PROV.TPCATAGE";
  +" and "+i_cAliasName2+".TPCHIAVE=TAB_PROV.TPCHIAVE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
