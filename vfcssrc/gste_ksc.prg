* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_ksc                                                        *
*              Stampa estratto conto clienti/fornitori                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_88]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-04                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_ksc",oParentObject))

* --- Class definition
define class tgste_ksc as StdForm
  Top    = 21
  Left   = 46

  * --- Standard Properties
  Width  = 567
  Height = 359+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-14"
  HelpContextID=99234921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  LINGUE_IDX = 0
  CATECOMM_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  cPrg = "gste_ksc"
  cComment = "Stampa estratto conto clienti/fornitori"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATSTA = ctod('  /  /  ')
  o_DATSTA = ctod('  /  /  ')
  w_FLANSCA = space(1)
  o_FLANSCA = space(1)
  w_CODLIN = space(3)
  o_CODLIN = space(3)
  w_DESLIN = space(30)
  w_ONUME = 0
  o_ONUME = 0
  w_TIPCLF = space(1)
  o_TIPCLF = space(1)
  w_SEDE = space(2)
  w_CODINI = space(15)
  o_CODINI = space(15)
  w_DESCRI1 = space(40)
  w_CODLIN1 = space(3)
  w_CODFIN = space(15)
  o_CODFIN = space(15)
  w_DESCRI2 = space(40)
  w_CODLIN2 = space(3)
  w_FLSALD = space(1)
  o_FLSALD = space(1)
  w_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_FLPART = space(1)
  o_FLPART = space(1)
  w_FLSOSP = space(1)
  w_FLORIG = space(1)
  w_DATPAR = ctod('  /  /  ')
  w_PARNEG = space(1)
  w_VALCLI = space(1)
  w_TIPRAG = space(1)
  w_TIPORD = space(1)
  w_FLPART1 = space(1)
  w_AZIENDA = space(4)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = space(10)
  w_CATCOM = space(3)
  w_DESCOM = space(35)
  w_CODZON = space(3)
  w_DESZON = space(35)
  w_CODAGE = space(5)
  w_ESPORTAL = space(1)
  o_ESPORTAL = space(1)
  w_FLGSOS = space(1)
  w_FLGNSO = space(1)
  w_DESAGE = space(35)
  w_AGEOBSO = ctod('  /  /  ')
  w_TIPORD = space(1)
  w_EFFETTI = space(1)
  w_TIPORD = space(1)
  w_EMAIL = space(254)
  w_EMPEC = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kscPag1","gste_ksc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgste_kscPag2","gste_ksc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATSTA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='LINGUE'
    this.cWorkTables[3]='CATECOMM'
    this.cWorkTables[4]='ZONE'
    this.cWorkTables[5]='AGENTI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSTE_BEC with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_ksc
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATSTA=ctod("  /  /  ")
      .w_FLANSCA=space(1)
      .w_CODLIN=space(3)
      .w_DESLIN=space(30)
      .w_ONUME=0
      .w_TIPCLF=space(1)
      .w_SEDE=space(2)
      .w_CODINI=space(15)
      .w_DESCRI1=space(40)
      .w_CODLIN1=space(3)
      .w_CODFIN=space(15)
      .w_DESCRI2=space(40)
      .w_CODLIN2=space(3)
      .w_FLSALD=space(1)
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_FLPART=space(1)
      .w_FLSOSP=space(1)
      .w_FLORIG=space(1)
      .w_DATPAR=ctod("  /  /  ")
      .w_PARNEG=space(1)
      .w_VALCLI=space(1)
      .w_TIPRAG=space(1)
      .w_TIPORD=space(1)
      .w_FLPART1=space(1)
      .w_AZIENDA=space(4)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=space(10)
      .w_CATCOM=space(3)
      .w_DESCOM=space(35)
      .w_CODZON=space(3)
      .w_DESZON=space(35)
      .w_CODAGE=space(5)
      .w_ESPORTAL=space(1)
      .w_FLGSOS=space(1)
      .w_FLGNSO=space(1)
      .w_DESAGE=space(35)
      .w_AGEOBSO=ctod("  /  /  ")
      .w_TIPORD=space(1)
      .w_EFFETTI=space(1)
      .w_TIPORD=space(1)
      .w_EMAIL=space(254)
      .w_EMPEC=space(254)
        .w_DATSTA = i_DATSYS
        .w_FLANSCA = ' '
        .w_CODLIN = g_CODLIN
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODLIN))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,5,.f.)
        .w_TIPCLF = 'C'
        .w_SEDE = 'NO'
        .w_CODINI = SPACE(15)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODINI))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,10,.f.)
        .w_CODFIN = space(15)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODFIN))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,13,.f.)
        .w_FLSALD = 'R'
          .DoRTCalc(15,15,.f.)
        .w_SCAFIN = i_datsys+365
        .w_FLPART = 'T'
        .w_FLSOSP = 'T'
        .w_FLORIG = IIF(.w_ONUME=6,' ',.w_FLORIG)
        .w_DATPAR = cp_CharToDate('  -  -  ')
        .w_PARNEG = ' '
        .w_VALCLI = 'N'
        .w_TIPRAG = IIF(.w_ONUME=6,'S','T')
        .w_TIPORD = 'P'
        .w_FLPART1 = IIF(.w_FLPART='T', ' ', .w_FLPART)
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .w_AZIENDA = i_codazi
        .w_OBTEST = .w_DATSTA
        .DoRTCalc(28,29,.f.)
        if not(empty(.w_CATCOM))
          .link_2_1('Full')
        endif
        .DoRTCalc(30,31,.f.)
        if not(empty(.w_CODZON))
          .link_2_4('Full')
        endif
          .DoRTCalc(32,32,.f.)
        .w_CODAGE = SPACE(5)
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_CODAGE))
          .link_2_6('Full')
        endif
        .w_ESPORTAL = ' '
        .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
        .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
          .DoRTCalc(37,38,.f.)
        .w_TIPORD = IIF(.w_ONUME=5,'A','P')
        .w_EFFETTI = 'A'
        .w_TIPORD = IIF(.w_ONUME>100,'P',.w_TIPORD)
        .w_EMAIL = IIF((ALLTRIM(.w_CODINI)<>ALLTRIM(.w_CODFIN)) OR (EMPTY(NVL(.w_CODINI,'')) AND EMPTY(NVL(.w_CODFIN,''))),'',.w_EMAIL)
        .w_EMPEC = IIF((ALLTRIM(.w_CODINI)<>ALLTRIM(.w_CODFIN)) OR (EMPTY(NVL(.w_CODINI,'')) AND EMPTY(NVL(.w_CODFIN,''))),'',.w_EMPEC)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_ONUME<>.w_ONUME
            .w_SEDE = 'NO'
        endif
        if .o_CODLIN<>.w_CODLIN
            .w_CODINI = SPACE(15)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,10,.t.)
        if .o_CODLIN<>.w_CODLIN
            .w_CODFIN = space(15)
          .link_1_11('Full')
        endif
        .DoRTCalc(12,18,.t.)
        if .o_ONUME<>.w_ONUME
            .w_FLORIG = IIF(.w_ONUME=6,' ',.w_FLORIG)
        endif
        if .o_FLSALD<>.w_FLSALD
            .w_DATPAR = cp_CharToDate('  -  -  ')
        endif
        if .o_FLPART<>.w_FLPART.or. .o_TIPCLF<>.w_TIPCLF.or. .o_FLSALD<>.w_FLSALD
            .w_PARNEG = ' '
        endif
        .DoRTCalc(22,22,.t.)
        if .o_ONUME<>.w_ONUME
            .w_TIPRAG = IIF(.w_ONUME=6,'S','T')
        endif
        if .o_ONUME<>.w_ONUME
            .w_TIPORD = 'P'
        endif
        if .o_FLPART<>.w_FLPART
            .w_FLPART1 = IIF(.w_FLPART='T', ' ', .w_FLPART)
        endif
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .DoRTCalc(26,26,.t.)
        if .o_DATSTA<>.w_DATSTA
            .w_OBTEST = .w_DATSTA
        endif
        .DoRTCalc(28,32,.t.)
        if .o_TIPCLF<>.w_TIPCLF
            .w_CODAGE = SPACE(5)
          .link_2_6('Full')
        endif
            .w_ESPORTAL = ' '
            .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
            .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
        .DoRTCalc(37,38,.t.)
        if .o_ONUME<>.w_ONUME
            .w_TIPORD = IIF(.w_ONUME=5,'A','P')
        endif
        if .o_ESPORTAL<>.w_ESPORTAL.or. .o_FLANSCA<>.w_FLANSCA
            .w_EFFETTI = 'A'
        endif
        if .o_ONUME<>.w_ONUME
            .w_TIPORD = IIF(.w_ONUME>100,'P',.w_TIPORD)
        endif
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN
            .w_EMAIL = IIF((ALLTRIM(.w_CODINI)<>ALLTRIM(.w_CODFIN)) OR (EMPTY(NVL(.w_CODINI,'')) AND EMPTY(NVL(.w_CODFIN,''))),'',.w_EMAIL)
        endif
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN
            .w_EMPEC = IIF((ALLTRIM(.w_CODINI)<>ALLTRIM(.w_CODFIN)) OR (EMPTY(NVL(.w_CODINI,'')) AND EMPTY(NVL(.w_CODFIN,''))),'',.w_EMPEC)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
    endwith
  return

  proc Calculate_TEOKRRWHOY()
    with this
          * --- init
          Inizializza(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSEDE_1_7.enabled = this.oPgFrm.Page1.oPag.oSEDE_1_7.mCond()
    this.oPgFrm.Page1.oPag.oFLSALD_1_14.enabled = this.oPgFrm.Page1.oPag.oFLSALD_1_14.mCond()
    this.oPgFrm.Page1.oPag.oFLPART_1_17.enabled = this.oPgFrm.Page1.oPag.oFLPART_1_17.mCond()
    this.oPgFrm.Page1.oPag.oDATPAR_1_20.enabled = this.oPgFrm.Page1.oPag.oDATPAR_1_20.mCond()
    this.oPgFrm.Page1.oPag.oPARNEG_1_21.enabled = this.oPgFrm.Page1.oPag.oPARNEG_1_21.mCond()
    this.oPgFrm.Page1.oPag.oVALCLI_1_22.enabled = this.oPgFrm.Page1.oPag.oVALCLI_1_22.mCond()
    this.oPgFrm.Page1.oPag.oTIPRAG_1_23.enabled = this.oPgFrm.Page1.oPag.oTIPRAG_1_23.mCond()
    this.oPgFrm.Page1.oPag.oTIPORD_1_24.enabled = this.oPgFrm.Page1.oPag.oTIPORD_1_24.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE_2_6.enabled = this.oPgFrm.Page2.oPag.oCODAGE_2_6.mCond()
    this.oPgFrm.Page1.oPag.oEFFETTI_1_54.enabled = this.oPgFrm.Page1.oPag.oEFFETTI_1_54.mCond()
    this.oPgFrm.Page1.oPag.oTIPORD_1_57.enabled = this.oPgFrm.Page1.oPag.oTIPORD_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLPART_1_17.visible=!this.oPgFrm.Page1.oPag.oFLPART_1_17.mHide()
    this.oPgFrm.Page1.oPag.oFLORIG_1_19.visible=!this.oPgFrm.Page1.oPag.oFLORIG_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDATPAR_1_20.visible=!this.oPgFrm.Page1.oPag.oDATPAR_1_20.mHide()
    this.oPgFrm.Page1.oPag.oPARNEG_1_21.visible=!this.oPgFrm.Page1.oPag.oPARNEG_1_21.mHide()
    this.oPgFrm.Page1.oPag.oTIPORD_1_24.visible=!this.oPgFrm.Page1.oPag.oTIPORD_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oTIPORD_1_53.visible=!this.oPgFrm.Page1.oPag.oTIPORD_1_53.mHide()
    this.oPgFrm.Page1.oPag.oTIPORD_1_57.visible=!this.oPgFrm.Page1.oPag.oTIPORD_1_57.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_TEOKRRWHOY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODLIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_CODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_CODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oCODLIN_1_3'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_CODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_CODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLIN = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIN = space(3)
      endif
      this.w_DESLIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO,AN_EMAIL,AN_EMPEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLF;
                     ,'ANCODICE',trim(this.w_CODINI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO,AN_EMAIL,AN_EMPEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO,AN_EMAIL,AN_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO,AN_EMAIL,AN_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODINI_1_8'),i_cWhere,'GSAR_BZC',"Clienti / fornitori",'GSTE_KSC.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO,AN_EMAIL,AN_EMPEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO,AN_EMAIL,AN_EMPEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure lingua incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO,AN_EMAIL,AN_EMPEC";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO,AN_EMAIL,AN_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO,AN_EMAIL,AN_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLF;
                       ,'ANCODICE',this.w_CODINI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO,AN_EMAIL,AN_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_CODLIN1 = NVL(_Link_.ANCODLIN,space(3))
      this.w_DATOBSO = NVL(_Link_.ANDTOBSO,space(10))
      this.w_EMAIL = NVL(_Link_.AN_EMAIL,space(254))
      this.w_EMPEC = NVL(_Link_.AN_EMPEC,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(15)
      endif
      this.w_DESCRI1 = space(40)
      this.w_CODLIN1 = space(3)
      this.w_DATOBSO = space(10)
      this.w_EMAIL = space(254)
      this.w_EMPEC = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODLIN=.w_CODLIN1 OR EMPTY(.w_CODLIN1)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure lingua incongruente")
        endif
        this.w_CODINI = space(15)
        this.w_DESCRI1 = space(40)
        this.w_CODLIN1 = space(3)
        this.w_DATOBSO = space(10)
        this.w_EMAIL = space(254)
        this.w_EMPEC = space(254)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLF;
                     ,'ANCODICE',trim(this.w_CODFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_11'),i_cWhere,'GSAR_BZC',"Clienti / fornitori",'GSTE_KSC.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure lingua incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLF;
                       ,'ANCODICE',this.w_CODFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODLIN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_CODLIN2 = NVL(_Link_.ANCODLIN,space(3))
      this.w_DATOBSO = NVL(_Link_.ANDTOBSO,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DESCRI2 = space(40)
      this.w_CODLIN2 = space(3)
      this.w_DATOBSO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODLIN=.w_CODLIN2  OR EMPTY(.w_CODLIN2)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure lingua incongruente")
        endif
        this.w_CODFIN = space(15)
        this.w_DESCRI2 = space(40)
        this.w_CODLIN2 = space(3)
        this.w_DATOBSO = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCOM
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOM_2_1'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCOM = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOM = space(3)
      endif
      this.w_DESCOM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODZON
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_CODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_CODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oCODZON_2_4'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_CODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_CODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODZON = space(3)
      endif
      this.w_DESZON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE_2_6'),i_cWhere,'GSAR_AGE',"Elenco agenti",'AGE1ZOOM.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_AGEOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_AGEOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_1.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_1.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLANSCA_1_2.RadioValue()==this.w_FLANSCA)
      this.oPgFrm.Page1.oPag.oFLANSCA_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLIN_1_3.value==this.w_CODLIN)
      this.oPgFrm.Page1.oPag.oCODLIN_1_3.value=this.w_CODLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIN_1_4.value==this.w_DESLIN)
      this.oPgFrm.Page1.oPag.oDESLIN_1_4.value=this.w_DESLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCLF_1_6.RadioValue()==this.w_TIPCLF)
      this.oPgFrm.Page1.oPag.oTIPCLF_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEDE_1_7.RadioValue()==this.w_SEDE)
      this.oPgFrm.Page1.oPag.oSEDE_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_8.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_8.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_9.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_9.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_11.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_11.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI2_1_12.value==this.w_DESCRI2)
      this.oPgFrm.Page1.oPag.oDESCRI2_1_12.value=this.w_DESCRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSALD_1_14.RadioValue()==this.w_FLSALD)
      this.oPgFrm.Page1.oPag.oFLSALD_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_15.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_15.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_16.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_16.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPART_1_17.RadioValue()==this.w_FLPART)
      this.oPgFrm.Page1.oPag.oFLPART_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSOSP_1_18.RadioValue()==this.w_FLSOSP)
      this.oPgFrm.Page1.oPag.oFLSOSP_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLORIG_1_19.RadioValue()==this.w_FLORIG)
      this.oPgFrm.Page1.oPag.oFLORIG_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATPAR_1_20.value==this.w_DATPAR)
      this.oPgFrm.Page1.oPag.oDATPAR_1_20.value=this.w_DATPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oPARNEG_1_21.RadioValue()==this.w_PARNEG)
      this.oPgFrm.Page1.oPag.oPARNEG_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALCLI_1_22.RadioValue()==this.w_VALCLI)
      this.oPgFrm.Page1.oPag.oVALCLI_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRAG_1_23.RadioValue()==this.w_TIPRAG)
      this.oPgFrm.Page1.oPag.oTIPRAG_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPORD_1_24.RadioValue()==this.w_TIPORD)
      this.oPgFrm.Page1.oPag.oTIPORD_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCATCOM_2_1.value==this.w_CATCOM)
      this.oPgFrm.Page2.oPag.oCATCOM_2_1.value=this.w_CATCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_2.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_2.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODZON_2_4.value==this.w_CODZON)
      this.oPgFrm.Page2.oPag.oCODZON_2_4.value=this.w_CODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZON_2_5.value==this.w_DESZON)
      this.oPgFrm.Page2.oPag.oDESZON_2_5.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE_2_6.value==this.w_CODAGE)
      this.oPgFrm.Page2.oPag.oCODAGE_2_6.value=this.w_CODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_8.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_8.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPORD_1_53.RadioValue()==this.w_TIPORD)
      this.oPgFrm.Page1.oPag.oTIPORD_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEFFETTI_1_54.RadioValue()==this.w_EFFETTI)
      this.oPgFrm.Page1.oPag.oEFFETTI_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPORD_1_57.RadioValue()==this.w_TIPORD)
      this.oPgFrm.Page1.oPag.oTIPORD_1_57.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TIPCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPCLF_1_6.SetFocus()
            i_bnoObbl = !empty(.w_TIPCLF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CODLIN=.w_CODLIN1 OR EMPTY(.w_CODLIN1)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente, obsoleto oppure lingua incongruente")
          case   not((.w_CODLIN=.w_CODLIN2  OR EMPTY(.w_CODLIN2)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente, obsoleto oppure lingua incongruente")
          case   not(EMPTY(.w_SCAFIN) OR (.w_SCAINI<=.w_SCAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAINI_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale superiore a quella finale")
          case   not(EMPTY(.w_SCAFIN) OR (.w_SCAINI<=.w_SCAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAFIN_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale superiore a quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATSTA = this.w_DATSTA
    this.o_FLANSCA = this.w_FLANSCA
    this.o_CODLIN = this.w_CODLIN
    this.o_ONUME = this.w_ONUME
    this.o_TIPCLF = this.w_TIPCLF
    this.o_CODINI = this.w_CODINI
    this.o_CODFIN = this.w_CODFIN
    this.o_FLSALD = this.w_FLSALD
    this.o_FLPART = this.w_FLPART
    this.o_ESPORTAL = this.w_ESPORTAL
    return

enddefine

* --- Define pages as container
define class tgste_kscPag1 as StdContainer
  Width  = 563
  height = 359
  stdWidth  = 563
  stdheight = 359
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATSTA_1_1 as StdField with uid="EOMPPAYZYO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento della stampa (per calcolo del cambio giornaliero)",;
    HelpContextID = 257011402,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=101, Top=9, TabStop=.f.

  add object oFLANSCA_1_2 as StdCheck with uid="WMCZRHHEPW",rtseq=2,rtrep=.f.,left=195, top=9, caption="Analisi scaduto per partita",;
    ToolTipText = "Se attivo lancia la maschera analisi scaduto per partita",;
    HelpContextID = 43527254,;
    cFormVar="w_FLANSCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLANSCA_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLANSCA_1_2.GetRadio()
    this.Parent.oContained.w_FLANSCA = this.RadioValue()
    return .t.
  endfunc

  func oFLANSCA_1_2.SetRadio()
    this.Parent.oContained.w_FLANSCA=trim(this.Parent.oContained.w_FLANSCA)
    this.value = ;
      iif(this.Parent.oContained.w_FLANSCA=='S',1,;
      0)
  endfunc

  add object oCODLIN_1_3 as StdField with uid="FLNMUVPEWM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODLIN", cQueryName = "CODLIN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice lingua di stampa",;
    HelpContextID = 50962650,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=101, Top=38, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_CODLIN"

  func oCODLIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oCODLIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oCODLIN_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_CODLIN
     i_obj.ecpSave()
  endproc

  add object oDESLIN_1_4 as StdField with uid="OHUBQCDLGJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESLIN", cQueryName = "DESLIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 50903754,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=146, Top=38, InputMask=replicate('X',30)


  add object oTIPCLF_1_6 as StdCombo with uid="SPINXHDHOQ",rtseq=6,rtrep=.f.,left=101,top=67,width=91,height=21;
    , ToolTipText = "Tipo: cliente o fornitore";
    , HelpContextID = 182576586;
    , cFormVar="w_TIPCLF",RowSource=""+"Cliente,"+"Fornitore", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCLF_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPCLF_1_6.GetRadio()
    this.Parent.oContained.w_TIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLF_1_6.SetRadio()
    this.Parent.oContained.w_TIPCLF=trim(this.Parent.oContained.w_TIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLF=='C',1,;
      iif(this.Parent.oContained.w_TIPCLF=='F',2,;
      0))
  endfunc

  func oTIPCLF_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODINI)
        bRes2=.link_1_8('Full')
      endif
      if .not. empty(.w_CODFIN)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oSEDE_1_7 as StdCombo with uid="FHLEJLBNZH",rtseq=7,rtrep=.f.,left=438,top=67,width=96,height=21;
    , ToolTipText = "Sede da utilizzare per stampa indirizzo";
    , HelpContextID = 94415322;
    , cFormVar="w_SEDE",RowSource=""+"Principale,"+"Pagamento,"+"Fatturazione,"+"Consegna,"+"Residenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSEDE_1_7.RadioValue()
    return(iif(this.value =1,'NO',;
    iif(this.value =2,'PA',;
    iif(this.value =3,'FA',;
    iif(this.value =4,'CO',;
    iif(this.value =5,'RE',;
    space(2)))))))
  endfunc
  func oSEDE_1_7.GetRadio()
    this.Parent.oContained.w_SEDE = this.RadioValue()
    return .t.
  endfunc

  func oSEDE_1_7.SetRadio()
    this.Parent.oContained.w_SEDE=trim(this.Parent.oContained.w_SEDE)
    this.value = ;
      iif(this.Parent.oContained.w_SEDE=='NO',1,;
      iif(this.Parent.oContained.w_SEDE=='PA',2,;
      iif(this.Parent.oContained.w_SEDE=='FA',3,;
      iif(this.Parent.oContained.w_SEDE=='CO',4,;
      iif(this.Parent.oContained.w_SEDE=='RE',5,;
      0)))))
  endfunc

  func oSEDE_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLANSCA<>'S' AND .w_ONUME<>5)
    endwith
   endif
  endfunc

  add object oCODINI_1_8 as StdField with uid="TPNOXTFDUA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente, obsoleto oppure lingua incongruente",;
    ToolTipText = "Codice cliente o fornitore di inizio selezione (spazio=no selezione iniziale)",;
    HelpContextID = 129802458,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=101, Top=95, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODINI"

  func oCODINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODINI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti / fornitori",'GSTE_KSC.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oDESCRI1_1_9 as StdField with uid="SLXLJCZILR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 142492982,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=239, Top=95, InputMask=replicate('X',40)

  add object oCODFIN_1_11 as StdField with uid="RQWNUFXJVT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente, obsoleto oppure lingua incongruente",;
    ToolTipText = "Codice cliente o fornitore di fine selezione (spazio=no selezione finale)",;
    HelpContextID = 51355866,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=101, Top=124, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFIN"

  func oCODFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFIN_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti / fornitori",'GSTE_KSC.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDESCRI2_1_12 as StdField with uid="AZDHJXTVYP",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCRI2", cQueryName = "DESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 142492982,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=239, Top=124, InputMask=replicate('X',40)


  add object oFLSALD_1_14 as StdCombo with uid="RZKVWQRZLP",rtseq=14,rtrep=.f.,left=101,top=155,width=119,height=21;
    , ToolTipText = "Stampe le scadenze aperte, non ancora saldate o tutte";
    , HelpContextID = 216249258;
    , cFormVar="w_FLSALD",RowSource=""+"Tutte,"+"Non saldate,"+"Solo parte aperta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSALD_1_14.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oFLSALD_1_14.GetRadio()
    this.Parent.oContained.w_FLSALD = this.RadioValue()
    return .t.
  endfunc

  func oFLSALD_1_14.SetRadio()
    this.Parent.oContained.w_FLSALD=trim(this.Parent.oContained.w_FLSALD)
    this.value = ;
      iif(this.Parent.oContained.w_FLSALD=='T',1,;
      iif(this.Parent.oContained.w_FLSALD=='N',2,;
      iif(this.Parent.oContained.w_FLSALD=='R',3,;
      0)))
  endfunc

  func oFLSALD_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLANSCA<>'S')
    endwith
   endif
  endfunc

  add object oSCAINI_1_15 as StdField with uid="BJEOQWKBZF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale superiore a quella finale",;
    ToolTipText = "Data inizio filtro sulle scadenze",;
    HelpContextID = 129817562,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=343, Top=155

  func oSCAINI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_SCAFIN) OR (.w_SCAINI<=.w_SCAFIN))
    endwith
    return bRes
  endfunc

  add object oSCAFIN_1_16 as StdField with uid="WBWGJNTNGZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale superiore a quella finale",;
    ToolTipText = "Data fine filtro sulle scadenze",;
    HelpContextID = 51370970,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=476, Top=155

  func oSCAFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_SCAFIN) OR (.w_SCAINI<=.w_SCAFIN))
    endwith
    return bRes
  endfunc


  add object oFLPART_1_17 as StdCombo with uid="PRTIZEDRCX",rtseq=17,rtrep=.f.,left=101,top=182,width=119,height=21;
    , HelpContextID = 58465366;
    , cFormVar="w_FLPART",RowSource=""+"Tutte,"+"Saldo,"+"Creazione,"+"Acconto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPART_1_17.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oFLPART_1_17.GetRadio()
    this.Parent.oContained.w_FLPART = this.RadioValue()
    return .t.
  endfunc

  func oFLPART_1_17.SetRadio()
    this.Parent.oContained.w_FLPART=trim(this.Parent.oContained.w_FLPART)
    this.value = ;
      iif(this.Parent.oContained.w_FLPART=='T',1,;
      iif(this.Parent.oContained.w_FLPART=='S',2,;
      iif(this.Parent.oContained.w_FLPART=='C',3,;
      iif(this.Parent.oContained.w_FLPART=='A',4,;
      0))))
  endfunc

  func oFLPART_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLANSCA<>'S')
    endwith
   endif
  endfunc

  func oFLPART_1_17.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R')
    endwith
  endfunc


  add object oFLSOSP_1_18 as StdCombo with uid="JPWRXLSRGK",rtseq=18,rtrep=.f.,left=343,top=182,width=92,height=21;
    , ToolTipText = "Ricerca le scadenze per il flag sospeso";
    , HelpContextID = 6665130;
    , cFormVar="w_FLSOSP",RowSource=""+"Tutte,"+"Non sospese,"+"Sospese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSOSP_1_18.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oFLSOSP_1_18.GetRadio()
    this.Parent.oContained.w_FLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oFLSOSP_1_18.SetRadio()
    this.Parent.oContained.w_FLSOSP=trim(this.Parent.oContained.w_FLSOSP)
    this.value = ;
      iif(this.Parent.oContained.w_FLSOSP=='T',1,;
      iif(this.Parent.oContained.w_FLSOSP=='N',2,;
      iif(this.Parent.oContained.w_FLSOSP=='S',3,;
      0)))
  endfunc

  add object oFLORIG_1_19 as StdCheck with uid="UBXKXPPUUY",rtseq=19,rtrep=.f.,left=452, top=182, caption="Dettaglio partite",;
    ToolTipText = "Se attivo: per le partite raggruppate vengono stampate le partite di origine.",;
    HelpContextID = 167965610,;
    cFormVar="w_FLORIG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLORIG_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLORIG_1_19.GetRadio()
    this.Parent.oContained.w_FLORIG = this.RadioValue()
    return .t.
  endfunc

  func oFLORIG_1_19.SetRadio()
    this.Parent.oContained.w_FLORIG=trim(this.Parent.oContained.w_FLORIG)
    this.value = ;
      iif(this.Parent.oContained.w_FLORIG=='S',1,;
      0)
  endfunc

  func oFLORIG_1_19.mHide()
    with this.Parent.oContained
      return (.w_ONUME=6)
    endwith
  endfunc

  add object oDATPAR_1_20 as StdField with uid="TJMGRRCJPO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DATPAR", cQueryName = "DATPAR",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 8081718,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=144, Top=209

  func oDATPAR_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLANSCA<>'S')
    endwith
   endif
  endfunc

  func oDATPAR_1_20.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R')
    endwith
  endfunc

  add object oPARNEG_1_21 as StdCheck with uid="TTTGCOEIZB",rtseq=21,rtrep=.f.,left=322, top=234, caption="Esclude partite negative",;
    ToolTipText = "Se attivo esclude le partite dei clienti/fornitori con saldo negativo (debiti/crediti netti)",;
    HelpContextID = 172412426,;
    cFormVar="w_PARNEG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPARNEG_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPARNEG_1_21.GetRadio()
    this.Parent.oContained.w_PARNEG = this.RadioValue()
    return .t.
  endfunc

  func oPARNEG_1_21.SetRadio()
    this.Parent.oContained.w_PARNEG=trim(this.Parent.oContained.w_PARNEG)
    this.value = ;
      iif(this.Parent.oContained.w_PARNEG=='S',1,;
      0)
  endfunc

  func oPARNEG_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLPART $ 'C-T')
    endwith
   endif
  endfunc

  func oPARNEG_1_21.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R')
    endwith
  endfunc


  add object oVALCLI_1_22 as StdCombo with uid="KQYUQVWASJ",rtseq=22,rtrep=.f.,left=28,top=285,width=101,height=21;
    , ToolTipText = "Stampa importi nella valuta di conto o del cliente/fornitore";
    , HelpContextID = 132263338;
    , cFormVar="w_VALCLI",RowSource=""+"di conto,"+"del cli/for", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVALCLI_1_22.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    '')))
  endfunc
  func oVALCLI_1_22.GetRadio()
    this.Parent.oContained.w_VALCLI = this.RadioValue()
    return .t.
  endfunc

  func oVALCLI_1_22.SetRadio()
    this.Parent.oContained.w_VALCLI=trim(this.Parent.oContained.w_VALCLI)
    this.value = ;
      iif(this.Parent.oContained.w_VALCLI=='N',1,;
      iif(this.Parent.oContained.w_VALCLI=='S',2,;
      0))
  endfunc

  func oVALCLI_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLANSCA<>'S')
    endwith
   endif
  endfunc


  add object oTIPRAG_1_23 as StdCombo with uid="XETEECWKZQ",rtseq=23,rtrep=.f.,left=182,top=285,width=111,height=21;
    , ToolTipText = "Raggruppa le righe per data scadenza, per partita o nessun raggruppamento";
    , HelpContextID = 176350666;
    , cFormVar="w_TIPRAG",RowSource=""+"No,"+"Per partita,"+"Per scadenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPRAG_1_23.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'P',;
    iif(this.value =3,'S',;
    ''))))
  endfunc
  func oTIPRAG_1_23.GetRadio()
    this.Parent.oContained.w_TIPRAG = this.RadioValue()
    return .t.
  endfunc

  func oTIPRAG_1_23.SetRadio()
    this.Parent.oContained.w_TIPRAG=trim(this.Parent.oContained.w_TIPRAG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRAG=='T',1,;
      iif(this.Parent.oContained.w_TIPRAG=='P',2,;
      iif(this.Parent.oContained.w_TIPRAG=='S',3,;
      0)))
  endfunc

  func oTIPRAG_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLANSCA<>'S' AND .w_ONUME<>6 and .w_ONUME<>5)
    endwith
   endif
  endfunc


  add object oTIPORD_1_24 as StdCombo with uid="FOQSONXXOM",rtseq=24,rtrep=.f.,left=345,top=285,width=110,height=21;
    , ToolTipText = "Stampa ordinata per partita e scadenza o per sola data scadenza";
    , HelpContextID = 209053130;
    , cFormVar="w_TIPORD",RowSource=""+"Partita/scadenza,"+"Scadenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPORD_1_24.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPORD_1_24.GetRadio()
    this.Parent.oContained.w_TIPORD = this.RadioValue()
    return .t.
  endfunc

  func oTIPORD_1_24.SetRadio()
    this.Parent.oContained.w_TIPORD=trim(this.Parent.oContained.w_TIPORD)
    this.value = ;
      iif(this.Parent.oContained.w_TIPORD=='P',1,;
      iif(this.Parent.oContained.w_TIPORD=='S',2,;
      0))
  endfunc

  func oTIPORD_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPRAG<>'S' AND .w_FLANSCA<>'S' and .w_ONUME<>5)
    endwith
   endif
  endfunc

  func oTIPORD_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPRAG='S' or .w_ONUME=5)
    endwith
  endfunc


  add object oBtn_1_26 as StdButton with uid="BJLYJVMVHQ",left=460, top=311, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 42554662;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        do GSTE_BEC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY( .w_OREP ))
      endwith
    endif
  endfunc


  add object oObj_1_27 as cp_outputCombo with uid="ZLERXIVPWT",left=101, top=319, width=319,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 78762726


  add object oBtn_1_28 as StdButton with uid="MYAOMVHQFA",left=510, top=311, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 91917498;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTIPORD_1_53 as StdCombo with uid="HOFVYDRSEJ",rtseq=39,rtrep=.f.,left=345,top=285,width=110,height=21, enabled=.f.;
    , ToolTipText = "Stampa ordinata per partita e scadenza o per sola data scadenza";
    , HelpContextID = 209053130;
    , cFormVar="w_TIPORD",RowSource=""+"Agente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPORD_1_53.RadioValue()
    return(iif(this.value =1,'A',;
    space(1)))
  endfunc
  func oTIPORD_1_53.GetRadio()
    this.Parent.oContained.w_TIPORD = this.RadioValue()
    return .t.
  endfunc

  func oTIPORD_1_53.SetRadio()
    this.Parent.oContained.w_TIPORD=trim(this.Parent.oContained.w_TIPORD)
    this.value = ;
      iif(this.Parent.oContained.w_TIPORD=='A',1,;
      0)
  endfunc

  func oTIPORD_1_53.mHide()
    with this.Parent.oContained
      return (.w_ONUME<>5)
    endwith
  endfunc


  add object oEFFETTI_1_54 as StdCombo with uid="ISVVHCTWOC",rtseq=40,rtrep=.f.,left=343,top=209,width=119,height=21;
    , ToolTipText = "Partite chiuse: le partite risultano chiuse a prescindere dalla data di scadenza.Partite aperte: le partite risultano aperte fino alla data di scadenza pi� eventuali giorni di tolleranza";
    , HelpContextID = 207653306;
    , cFormVar="w_EFFETTI",RowSource=""+"Partite aperte,"+"Partite chiuse", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEFFETTI_1_54.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oEFFETTI_1_54.GetRadio()
    this.Parent.oContained.w_EFFETTI = this.RadioValue()
    return .t.
  endfunc

  func oEFFETTI_1_54.SetRadio()
    this.Parent.oContained.w_EFFETTI=trim(this.Parent.oContained.w_EFFETTI)
    this.value = ;
      iif(this.Parent.oContained.w_EFFETTI=='A',1,;
      iif(this.Parent.oContained.w_EFFETTI=='C',2,;
      0))
  endfunc

  func oEFFETTI_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLANSCA<>'S')
    endwith
   endif
  endfunc


  add object oTIPORD_1_57 as StdCombo with uid="JGXPSJUAGD",rtseq=41,rtrep=.f.,left=345,top=285,width=110,height=21;
    , ToolTipText = "Stampa ordinata per partita e scadenza o per sola data scadenza";
    , HelpContextID = 209053130;
    , cFormVar="w_TIPORD",RowSource=""+"Partita/scadenza,"+"Scadenza,"+"Agente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPORD_1_57.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTIPORD_1_57.GetRadio()
    this.Parent.oContained.w_TIPORD = this.RadioValue()
    return .t.
  endfunc

  func oTIPORD_1_57.SetRadio()
    this.Parent.oContained.w_TIPORD=trim(this.Parent.oContained.w_TIPORD)
    this.value = ;
      iif(this.Parent.oContained.w_TIPORD=='P',1,;
      iif(this.Parent.oContained.w_TIPORD=='S',2,;
      iif(this.Parent.oContained.w_TIPORD=='A',3,;
      0)))
  endfunc

  func oTIPORD_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME>100)
    endwith
   endif
  endfunc

  func oTIPORD_1_57.mHide()
    with this.Parent.oContained
      return (.w_ONUME<=100)
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="HNSXRXLWWS",Visible=.t., Left=2, Top=38,;
    Alignment=1, Width=97, Height=15,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ERSQUEIBHU",Visible=.t., Left=2, Top=9,;
    Alignment=1, Width=97, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="NZTZXWHAVR",Visible=.t., Left=65, Top=95,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="ZCHUGDBCPO",Visible=.t., Left=70, Top=124,;
    Alignment=1, Width=29, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="QAPIOOJGUR",Visible=.t., Left=248, Top=155,;
    Alignment=1, Width=94, Height=15,;
    Caption="Scadenze dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="REDGXTMYLQ",Visible=.t., Left=448, Top=155,;
    Alignment=1, Width=26, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="LXYZRMJLIY",Visible=.t., Left=10, Top=319,;
    Alignment=1, Width=89, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="LJHALBQRQJ",Visible=.t., Left=52, Top=66,;
    Alignment=1, Width=47, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="MEZMXDBSKQ",Visible=.t., Left=28, Top=247,;
    Alignment=0, Width=127, Height=15,;
    Caption="Opzioni di stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="BSBENGJWGK",Visible=.t., Left=28, Top=268,;
    Alignment=0, Width=99, Height=15,;
    Caption="Valuta"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="PYWRZEHQCS",Visible=.t., Left=345, Top=268,;
    Alignment=0, Width=108, Height=15,;
    Caption="Ordinamento"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.w_TIPRAG='S')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="YFNBWBWLUD",Visible=.t., Left=180, Top=268,;
    Alignment=0, Width=113, Height=15,;
    Caption="Raggruppamento"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="DHNSFNFDJQ",Visible=.t., Left=2, Top=155,;
    Alignment=1, Width=97, Height=15,;
    Caption="Test saldate:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="XYHGQZYBQY",Visible=.t., Left=274, Top=66,;
    Alignment=1, Width=161, Height=18,;
    Caption="Indirizzo da stampare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="HBZSJYGTCN",Visible=.t., Left=2, Top=182,;
    Alignment=1, Width=97, Height=18,;
    Caption="Tipo partite:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="FHZQXJLHND",Visible=.t., Left=4, Top=209,;
    Alignment=1, Width=138, Height=18,;
    Caption="Partite aperte alla data:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R')
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="ZDCZOSBMTF",Visible=.t., Left=223, Top=182,;
    Alignment=1, Width=119, Height=15,;
    Caption="Test sospese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="WXJNHBXQBU",Visible=.t., Left=279, Top=209,;
    Alignment=1, Width=63, Height=18,;
    Caption="Effetti:"  ;
  , bGlobalFont=.t.

  add object oBox_1_40 as StdBox with uid="ZNNRWVRZFL",left=28, top=264, width=492,height=1
enddefine
define class tgste_kscPag2 as StdContainer
  Width  = 563
  height = 359
  stdWidth  = 563
  stdheight = 359
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCATCOM_2_1 as StdField with uid="FACBKISQQY",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CATCOM", cQueryName = "CATCOM",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale del cliente/fornitore",;
    HelpContextID = 61976282,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=122, Top=20, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOM"

  func oCATCOM_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOM_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOM_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOM_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATCOM_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATCOM
     i_obj.ecpSave()
  endproc

  add object oDESCOM_2_2 as StdField with uid="ZOYBTWMEFD",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 61979338,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=176, Top=20, InputMask=replicate('X',35)

  add object oCODZON_2_4 as StdField with uid="ZPVRJRMDOT",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CODZON", cQueryName = "CODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice zona del cliente/fornitore",;
    HelpContextID = 43753690,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=122, Top=51, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_CODZON"

  func oCODZON_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODZON_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODZON_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oCODZON_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oCODZON_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_CODZON
     i_obj.ecpSave()
  endproc

  add object oDESZON_2_5 as StdField with uid="FOZQEQLSQT",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 43694794,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=175, Top=51, InputMask=replicate('X',35)

  add object oCODAGE_2_6 as StdField with uid="ANBLNSHDPI",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CODAGE", cQueryName = "CODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente di selezione",;
    HelpContextID = 204775642,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=122, Top=82, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE"

  func oCODAGE_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLF='C')
    endwith
   endif
  endfunc

  func oCODAGE_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Elenco agenti",'AGE1ZOOM.AGENTI_VZM',this.parent.oContained
  endproc
  proc oCODAGE_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CODAGE
     i_obj.ecpSave()
  endproc

  add object oDESAGE_2_8 as StdField with uid="ABLOIHLJTT",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 204716746,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=191, Top=82, InputMask=replicate('X',35)

  add object oStr_2_3 as StdString with uid="MFLAEXCKCK",Visible=.t., Left=17, Top=20,;
    Alignment=1, Width=103, Height=18,;
    Caption="Cat. commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="KLEFCKFEHE",Visible=.t., Left=33, Top=51,;
    Alignment=1, Width=87, Height=18,;
    Caption="Codice zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="OMMPRNQWNZ",Visible=.t., Left=26, Top=82,;
    Alignment=1, Width=94, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_ksc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gste_ksc
Proc Inizializza()
  parameters padre  
  padre.mcalc(.t.)
endproc  
* --- Fine Area Manuale
