* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bis                                                        *
*              Sincronizzazione indici documenti pratiche                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-28                                                      *
* Last revis.: 2014-07-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bis",oParentObject,m.pOper)
return(i_retval)

define class tgsut_bis as StdBatch
  * --- Local variables
  pOper = space(1)
  w_TIPOPATH = space(1)
  w_CURSOR = space(15)
  w_FILENAME = space(250)
  w_INDICE = space(20)
  w_LSCODCAN = space(15)
  w_CNPUBWEB = space(1)
  w_AR_TABLE = space(15)
  w_MskMESS = .NULL.
  w_LICLADOC = space(100)
  w_MODALLEG = space(1)
  w_TIPOARCH = space(1)
  w_CLAALLE = space(5)
  w_TIPPATH = space(1)
  w_TIPOALLE = space(5)
  w_TIPCLA = space(1)
  oWord = .NULL.
  w_PATH = space(250)
  w_EXT = space(250)
  w_CODCAN = space(20)
  w_LOG = space(0)
  w_LSSTATUS = space(1)
  w_PATH = space(250)
  w_PATHPRA = space(250)
  w_CNCODCAN = space(15)
  w_DESCRI = space(40)
  w_CNDESCAN = space(100)
  w_IDSERIAL = space(20)
  w_OK = .f.
  w_PATHTIPCLAS = space(15)
  w_TIPOALL = space(5)
  w_CLASSEALL = space(5)
  w_AROGGETT = space(50)
  oDoc = .NULL.
  w_CONFARC = space(1)
  * --- WorkFile variables
  PROMCLAS_idx=0
  LOGSININ_idx=0
  CLA_ALLE_idx=0
  TIP_ALLE_idx=0
  PROMINDI_idx=0
  PRODINDI_idx=0
  CAN_TIER_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSUT_KIS e GSUT1KIS
    * --- Sincronizzazione indici documenti pratiche
    do case
      case this.pOper = "R"
        this.w_CURSOR = this.oParentObject.w_ZOOMPATH.cCursor
        this.w_TIPOPATH = This.oParentObject.w_TIPOPATH
        SELECT(this.w_CURSOR)
        ZAP
        do case
          case this.w_TIPOPATH="P"
            VQ_EXEC("QUERY\GSUT_KIS", This, "_TMPPATHDEF_")
            if USED("_TMPPATHDEF_")
              * --- Inserisco il risultato del cursore Visu nello zoom
              SELECT("_TMPPATHDEF_")
              GO TOP
              SCAN
              SCATTER MEMVAR
              SELECT(this.w_CURSOR)
              APPEND BLANK
              GATHER MEMVAR
              SELECT("_TMPPATHDEF_")
              ENDSCAN
              USE IN SELECT("_TMPPATHDEF_")
              SELECT(this.w_CURSOR)
            endif
          case this.w_TIPOPATH="C"
            VQ_EXEC("QUERY\GSUT1KIS", This, "_TMPPATHPRAT_")
            if USED("_TMPPATHPRAT_")
              * --- Inserisco il risultato del cursore Visu nello zoom
              SELECT("_TMPPATHPRAT_")
              GO TOP
              SCAN
              SCATTER MEMVAR
              SELECT(this.w_CURSOR)
              APPEND BLANK
              GATHER MEMVAR
              SELECT("_TMPPATHPRAT_")
              ENDSCAN
              USE IN SELECT("_TMPPATHPRAT_")
              SELECT(this.w_CURSOR)
            endif
        endcase
        * --- Seleziono tutti i percorsi
        UPDATE (this.w_CURSOR) SET XCHK = 1
        GO TOP
        this.oParentObject.w_ZOOMPATH.Refresh()     
      case this.pOper = "S"
        * --- Sincronizza
        this.w_CURSOR = this.oParentObject.w_ZOOMPATH.cCursor
        this.w_TIPOPATH = This.oParentObject.w_TIPOPATH
        this.oParentObject.w_SHOWLOG = .T.
        this.w_AR_TABLE = "CAN_TIER"
        this.w_MskMESS = This.oParentObject
        this.w_MskMESS.w_MSG = ""
        this.w_MskMESS.oPgFrm.ActivePage = 2
        * --- Legge, se c'�, la classe di archiviazione standard
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDRIFTAB = "+cp_ToStrODBC(this.w_AR_TABLE);
                +" and CDRIFDEF = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL;
            from (i_cTable) where;
                CDRIFTAB = this.w_AR_TABLE;
                and CDRIFDEF = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
          this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
          this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
          this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
          this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
          this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
          this.w_TIPOALLE = NVL(cp_ToDate(_read_.CDTIPALL),cp_NullValue(_read_.CDTIPALL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if empty(this.w_LICLADOC)
          * --- Verifica se c'� almeno una classe
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDRIFTAB = "+cp_ToStrODBC(this.w_AR_TABLE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL;
              from (i_cTable) where;
                  CDRIFTAB = this.w_AR_TABLE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
            this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
            this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
            this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
            this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
            this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
            this.w_TIPOALLE = NVL(cp_ToDate(_read_.CDTIPALL),cp_NullValue(_read_.CDTIPALL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows=0
            addmsgnl("Classe documentale non definita", this.w_MskMESS)
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Verifica se l'utente pu� archiviare ....
        if SUBSTR( CHKPECLA( this.w_LICLADOC, i_CODUTE, this.w_AR_TABLE), 2, 1 ) <> "S"
          addmsgnl("Non si possiedono i diritti per archiviare documenti con la classe documentale %1", this.w_MskMESS, ALLTRIM(this.w_LICLADOC))
          i_retcode = 'stop'
          return
        endif
        addmsgnl("Lettura informazioni classe documentale %1", this.w_MskMESS, this.w_LICLADOC)
        * --- Creo oggetto word per eventuale titolo per descrizione
        if this.oParentObject.w_TIPODESC = "T"
           
 Local L_oldErr, L_err 
 L_err = .f. 
 L_oldErr = On("Error") 
 On Error L_err = .t.
          this.oWord = CREATEOBJECT("word.application")
           
 On Error &L_oldErr
          if L_err
            addmsgnl("Microsoft Office Word non trovato, sar� utilizzato il nome file per la descrizione dell'indice", this.w_MskMESS)
            this.oWord = .NULL.
          endif
        endif
        * --- Elenco percorsi pratiche
        this.w_TIPOPATH = "C"
        VQ_EXEC("QUERY\GSUT1KIS", This, "_TMPPATHPRAT_")
        SELECT * FROM (this.w_CURSOR) WHERE XCHK = 1 AND NOT DELETED() INTO CURSOR _TMPPATH_
        if RECCOUNT() > 0
          SELECT("_TMPPATH_") 
          GO TOP
          SCAN
          this.w_PATH = ALLTRIM(_TMPPATH_.SISRPATH)
          this.w_EXT = ALLTRIM(_TMPPATH_.SISR_EXT)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT("_TMPPATH_") 
          ENDSCAN
          if this.oParentObject.w_FILEEXIST="S"
            addmsgnl("Verifica esistenza documenti archiviati", this.w_MskMESS)
            * --- Select from PROMINDI
            i_nConn=i_TableProp[this.PROMINDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select IDORIFIL,IDNOMFIL,IDPATALL,IDSERIAL,IDMODALL  from "+i_cTable+" PROMINDI ";
                  +" where IDFLHIDE<>'S'";
                  +" order by IDSERIAL";
                   ,"_Curs_PROMINDI")
            else
              select IDORIFIL,IDNOMFIL,IDPATALL,IDSERIAL,IDMODALL from (i_cTable);
               where IDFLHIDE<>"S";
               order by IDSERIAL;
                into cursor _Curs_PROMINDI
            endif
            if used('_Curs_PROMINDI')
              select _Curs_PROMINDI
              locate for 1=1
              do while not(eof())
              if _Curs_PROMINDI.IDMODALL = "F"
                this.w_FILENAME = ALLTRIM(ADDBS(_Curs_PROMINDI.IDPATALL)+_Curs_PROMINDI.IDNOMFIL)
              else
                this.w_FILENAME = UPPER(_Curs_PROMINDI.IDORIFIL)
              endif
              if !cp_FileExist(this.w_FILENAME)
                addmsgnl("Il documento archiviato %1 non � pi� presente", this.w_MskMESS, this.w_FILENAME)
                * --- Read from PRODINDI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PRODINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "IDVALATT"+;
                    " from "+i_cTable+" PRODINDI where ";
                        +"IDSERIAL = "+cp_ToStrODBC(_Curs_PROMINDI.IDSERIAL);
                        +" and IDCODATT = "+cp_ToStrODBC("CODICE");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    IDVALATT;
                    from (i_cTable) where;
                        IDSERIAL = _Curs_PROMINDI.IDSERIAL;
                        and IDCODATT = "CODICE";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CODCAN = NVL(cp_ToDate(_read_.IDVALATT),cp_NullValue(_read_.IDVALATT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Try
                local bErr_039D3BC0
                bErr_039D3BC0=bTrsErr
                this.Try_039D3BC0()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_039D3BC0
                * --- End
              endif
                select _Curs_PROMINDI
                continue
              enddo
              use
            endif
          endif
          addmsgnl("Sincronizzazione terminata", this.w_MskMESS)
        else
          addmsgnl("Occorre selezionare almeno un percorso", this.w_MskMESS)
        endif
        USE IN SELECT("_TMPPATHPRAT_")
        USE IN SELECT("_TMPPATH_")
        * --- Chiudo eventuale oggetto Word
        if !ISNULL(this.oWord)
          this.oWord.Quit()     
          this.oWord = .NULL.
        endif
        if Type("This.oparentobject.oparentobject")="O"
           
 This.oparentobject.oparentobject.Notifyevent("Ricerca") 
 This.oparentobject.Ecpquit()
        endif
      case this.pOper = "C"
        if Ah_YesNo("Gli indici selezionati saranno confermati.%0Si desidera continuare?")
          SELECT * FROM (this.oParentObject.w_ZOOM.cCursor) WHERE XCHK=1 INTO CURSOR _TMPSEL_
          SCAN
          this.w_FILENAME = _TMPSEL_.LSFILENM
          this.w_INDICE = _TMPSEL_.LDSER_ID
          this.w_LSCODCAN = _TMPSEL_.LSCODCAN
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNPUBWEB"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.w_LSCODCAN);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNPUBWEB;
              from (i_cTable) where;
                  CNCODCAN = this.w_LSCODCAN;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CNPUBWEB = NVL(cp_ToDate(_read_.CNPUBWEB),cp_NullValue(_read_.CNPUBWEB))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Write into LOGSININ
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LOGSININ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOGSININ_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGSININ_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LSSTATUS ="+cp_NullLink(cp_ToStrODBC("C"),'LOGSININ','LSSTATUS');
                +i_ccchkf ;
            +" where ";
                +"LSFILENM = "+cp_ToStrODBC(this.w_FILENAME);
                   )
          else
            update (i_cTable) set;
                LSSTATUS = "C";
                &i_ccchkf. ;
             where;
                LSFILENM = this.w_FILENAME;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Try
          local bErr_035B4818
          bErr_035B4818=bTrsErr
          this.Try_035B4818()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            Ah_ErrorMsg("Impossibile confermare l'indice documentale %1", this.w_INDICE)
          endif
          bTrsErr=bTrsErr or bErr_035B4818
          * --- End
          ENDSCAN
          USE IN SELECT("_TMPSEL_")
        endif
        This.oParentObject.NotifyEvent("Ricerca")
      case this.pOper = "I"
        if Ah_YesNo("I documenti selezionati saranno ignorati dalla prossima sincronizzazione.%0L'eventuale indice documentale associato sar� cancellato.%0Si desidera continuare?")
          SELECT * FROM (this.oParentObject.w_ZOOM.cCursor) WHERE XCHK=1 INTO CURSOR _TMPSEL_
          SCAN
          this.w_FILENAME = _TMPSEL_.LSFILENM
          this.w_INDICE = _TMPSEL_.LDSER_ID
          * --- Write into LOGSININ
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LOGSININ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOGSININ_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGSININ_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LSSTATUS ="+cp_NullLink(cp_ToStrODBC("S"),'LOGSININ','LSSTATUS');
            +",LDSER_ID ="+cp_NullLink(cp_ToStrODBC(""),'LOGSININ','LDSER_ID');
            +",LSCODCAN ="+cp_NullLink(cp_ToStrODBC(""),'LOGSININ','LSCODCAN');
                +i_ccchkf ;
            +" where ";
                +"LSFILENM = "+cp_ToStrODBC(this.w_FILENAME);
                   )
          else
            update (i_cTable) set;
                LSSTATUS = "S";
                ,LDSER_ID = "";
                ,LSCODCAN = "";
                &i_ccchkf. ;
             where;
                LSFILENM = this.w_FILENAME;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Try
          local bErr_03976F80
          bErr_03976F80=bTrsErr
          this.Try_03976F80()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03976F80
          * --- End
          ENDSCAN
          USE IN SELECT("_TMPSEL_")
        endif
        This.oParentObject.NotifyEvent("Ricerca")
      case this.pOper = "E"
        if Ah_YesNo("Le voci selezionate saranno eliminate dal log di sincronizzazione.%0L'eventuale indice documentale associato sar� cancellato.%0Si desidera continuare?")
          SELECT * FROM (this.oParentObject.w_ZOOM.cCursor) WHERE XCHK=1 INTO CURSOR _TMPSEL_
          SCAN
          this.w_FILENAME = _TMPSEL_.LSFILENM
          this.w_INDICE = _TMPSEL_.LDSER_ID
          * --- Delete from LOGSININ
          i_nConn=i_TableProp[this.LOGSININ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOGSININ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"LSFILENM = "+cp_ToStrODBC(this.w_FILENAME);
                   )
          else
            delete from (i_cTable) where;
                  LSFILENM = this.w_FILENAME;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Try
          local bErr_03D05858
          bErr_03D05858=bTrsErr
          this.Try_03D05858()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03D05858
          * --- End
          ENDSCAN
          USE IN SELECT("_TMPSEL_")
        endif
        This.oParentObject.NotifyEvent("Ricerca")
    endcase
  endproc
  proc Try_039D3BC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into LOGSININ
    i_nConn=i_TableProp[this.LOGSININ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGSININ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGSININ_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LSFILENM"+",LDSER_ID"+",LSCODUTE"+",LSDATSIN"+",LSSTATUS"+",LSCODCAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_FILENAME),'LOGSININ','LSFILENM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_PROMINDI.IDSERIAL),'LOGSININ','LDSER_ID');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOGSININ','LSCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(DATE()),'LOGSININ','LSDATSIN');
      +","+cp_NullLink(cp_ToStrODBC("F"),'LOGSININ','LSSTATUS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCAN),'LOGSININ','LSCODCAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LSFILENM',this.w_FILENAME,'LDSER_ID',_Curs_PROMINDI.IDSERIAL,'LSCODUTE',i_CODUTE,'LSDATSIN',DATE(),'LSSTATUS',"F",'LSCODCAN',this.w_CODCAN)
      insert into (i_cTable) (LSFILENM,LDSER_ID,LSCODUTE,LSDATSIN,LSSTATUS,LSCODCAN &i_ccchkf. );
         values (;
           this.w_FILENAME;
           ,_Curs_PROMINDI.IDSERIAL;
           ,i_CODUTE;
           ,DATE();
           ,"F";
           ,this.w_CODCAN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_035B4818()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PROMINDI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IDFLHIDE ="+cp_NullLink(cp_ToStrODBC("N"),'PROMINDI','IDFLHIDE');
      +",IDPUBWEB ="+cp_NullLink(cp_ToStrODBC(this.w_CNPUBWEB),'PROMINDI','IDPUBWEB');
          +i_ccchkf ;
      +" where ";
          +"IDSERIAL = "+cp_ToStrODBC(this.w_INDICE);
             )
    else
      update (i_cTable) set;
          IDFLHIDE = "N";
          ,IDPUBWEB = this.w_CNPUBWEB;
          &i_ccchkf. ;
       where;
          IDSERIAL = this.w_INDICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03976F80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from PRODINDI
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.w_INDICE);
             )
    else
      delete from (i_cTable) where;
            IDSERIAL = this.w_INDICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PROMINDI
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.w_INDICE);
             )
    else
      delete from (i_cTable) where;
            IDSERIAL = this.w_INDICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_03D05858()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from PRODINDI
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.w_INDICE);
             )
    else
      delete from (i_cTable) where;
            IDSERIAL = this.w_INDICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PROMINDI
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.w_INDICE);
             )
    else
      delete from (i_cTable) where;
            IDSERIAL = this.w_INDICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sincronizzo la cartella
    * --- Recupero l'elenco dei file
    addmsgnl("Sincronizzazione cartella %1%0", this.w_MskMESS, this.w_PATH)
    This.ListDirToCur(this.w_PATH, ALLTRIM(this.w_EXT), "_TMPFILEDIR_")
    * --- Elenco file con pratica
    SELECT A.FILENAME, B.CNCODCAN, B.SISRPATH AS PATHPRA FROM _TMPFILEDIR_ A; 
 INNER JOIN _TMPPATHPRAT_ B ON A.FILENAME LIKE ADDBS(B.SISRPATH)+"%" INTO CURSOR _FILETOINDEX_
    * --- Elenco file senza pratica
    SELECT * FROM _TMPFILEDIR_ WHERE FILENAME NOT IN (SELECT FILENAME FROM _FILETOINDEX_) INTO CURSOR _FILENOPRAT_
    * --- Inserisco nel log i file che nn posso archiviare per mancaza della pratica
    this.w_LOG = ""
    SELECT _FILENOPRAT_
    SCAN
    this.w_FILENAME = ALLTRIM(_FILENOPRAT_.FILENAME)
    this.w_LOG = this.w_LOG + this.w_FILENAME + CHR(13)+CHR(10)
    * --- Il file pu� essere gi� presente
    * --- Try
    local bErr_03971F70
    bErr_03971F70=bTrsErr
    this.Try_03971F70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into LOGSININ
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.LOGSININ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOGSININ_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGSININ_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"LDSER_ID ="+cp_NullLink(cp_ToStrODBC(""),'LOGSININ','LDSER_ID');
        +",LSCODCAN ="+cp_NullLink(cp_ToStrODBC(""),'LOGSININ','LSCODCAN');
        +",LSCODUTE ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOGSININ','LSCODUTE');
        +",LSDATSIN ="+cp_NullLink(cp_ToStrODBC(DATE()),'LOGSININ','LSDATSIN');
            +i_ccchkf ;
        +" where ";
            +"LSFILENM = "+cp_ToStrODBC(this.w_FILENAME);
               )
      else
        update (i_cTable) set;
            LDSER_ID = "";
            ,LSCODCAN = "";
            ,LSCODUTE = i_CODUTE;
            ,LSDATSIN = DATE();
            &i_ccchkf. ;
         where;
            LSFILENM = this.w_FILENAME;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_03971F70
    * --- End
    SELECT _FILENOPRAT_
    ENDSCAN
    if Not Empty(this.w_LOG)
      addmsgnl("Impossibile associare questi documenti ad una pratica:", this.w_MskMESS)
      addmsgnl(this.w_LOG, this.w_MskMESS)
    endif
    USE IN SELECT("_FILENOPRAT_")
    * --- Elimino file da ignorare
    this.w_LSSTATUS = "S"
    VQ_EXEC("QUERY\GSUT2KIS", This, "_TMPTOSKIP_")
    SELECT * FROM _FILETOINDEX_ A WHERE UPPER(A.FILENAME) NOT IN (SELECT UPPER(B.LSFILENM) AS FILENAME FROM _TMPTOSKIP_ B) INTO CURSOR _TMPFILENOSKIP_
    SELECT("_TMPTOSKIP_")
    this.w_LOG = ""
    SCAN
    this.w_LOG = this.w_LOG + _TMPTOSKIP_.LSFILENM
    ENDSCAN
    if Not Empty(this.w_LOG)
      addmsgnl("I seguenti documenti saranno ignorati:", this.w_MskMESS)
      addmsgnl(this.w_LOG, this.w_MskMESS)
    endif
    USE IN SELECT("_TMPTOSKIP_")
    * --- Creo gli indici
    SELECT _TMPFILENOSKIP_
    SCAN
    this.w_FILENAME = UPPER(_TMPFILENOSKIP_.FILENAME)
    this.w_PATH = JUSTPATH(this.w_FILENAME)
    this.w_PATHPRA = ADDBS(_TMPFILENOSKIP_.PATHPRA)
    this.w_CNCODCAN = ALLTRIM(_TMPFILENOSKIP_.CNCODCAN)
    * --- Read from CAN_TIER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CNDESCAN"+;
        " from "+i_cTable+" CAN_TIER where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.w_CNCODCAN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CNDESCAN;
        from (i_cTable) where;
            CNCODCAN = this.w_CNCODCAN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CNDESCAN = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo se il file � gi� stato indicizzato
    this.w_OK = .F.
    * --- Select from PROMINDI
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select IDSERIAL  from "+i_cTable+" PROMINDI ";
          +" where UPPER(IDORIFIL) = "+cp_ToStrODBC(this.w_FILENAME)+"";
           ,"_Curs_PROMINDI")
    else
      select IDSERIAL from (i_cTable);
       where UPPER(IDORIFIL) = this.w_FILENAME;
        into cursor _Curs_PROMINDI
    endif
    if used('_Curs_PROMINDI')
      select _Curs_PROMINDI
      locate for 1=1
      if not(eof())
      do while not(eof())
      this.w_IDSERIAL = _Curs_PROMINDI.IDSERIAL
        select _Curs_PROMINDI
        continue
      enddo
      else
        * --- Select from QUERY\GSUT3KIS
        do vq_exec with 'QUERY\GSUT3KIS',this,'_Curs_QUERY_GSUT3KIS','',.f.,.t.
        if used('_Curs_QUERY_GSUT3KIS')
          select _Curs_QUERY_GSUT3KIS
          locate for 1=1
          if not(eof())
          do while not(eof())
          this.w_IDSERIAL = IDSERIAL
            select _Curs_QUERY_GSUT3KIS
            continue
          enddo
          else
            this.w_OK = .T.
            select _Curs_QUERY_GSUT3KIS
          endif
          use
        endif
        select _Curs_PROMINDI
      endif
      use
    endif
    if this.w_OK
      * --- Controllo eventuale tipologia/classe allegato
      this.w_PATHTIPCLAS = ADDBS(STRTRAN(ADDBS(this.w_PATH), this.w_PATHPRA, ""))
      this.w_TIPOALL = ""
      this.w_CLASSEALL = ""
      if ! EMPTY(this.w_PATHTIPCLAS)
        * --- Ho una possibile tipo/classe documento
        this.w_TIPOALL = LEFT(this.w_PATHTIPCLAS, AT("\", this.w_PATHTIPCLAS)-1)
        this.w_DESCRI = ""
        * --- Ricerca per descrizione
        * --- Read from TIP_ALLE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_ALLE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_ALLE_idx,2],.t.,this.TIP_ALLE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TACODICE"+;
            " from "+i_cTable+" TIP_ALLE where ";
                +"TADESCRI = "+cp_ToStrODBC(this.w_TIPOALL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TACODICE;
            from (i_cTable) where;
                TADESCRI = this.w_TIPOALL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESCRI = NVL(cp_ToDate(_read_.TACODICE),cp_NullValue(_read_.TACODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Eventualmente ricerco il codice
        if Empty(this.w_DESCRI)
          this.w_TIPOALL = Alltrim(this.w_TIPOALL)+"\"
          * --- Read from TIP_ALLE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_ALLE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_ALLE_idx,2],.t.,this.TIP_ALLE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TACODICE"+;
              " from "+i_cTable+" TIP_ALLE where ";
                  +"TAPATTIP = "+cp_ToStrODBC(this.w_TIPOALL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TACODICE;
              from (i_cTable) where;
                  TAPATTIP = this.w_TIPOALL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESCRI = NVL(cp_ToDate(_read_.TACODICE),cp_NullValue(_read_.TACODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(this.w_DESCRI)
          * --- Cerco l'eventuale classe
          this.w_TIPOALL = this.w_DESCRI
          this.w_CLASSEALL = SUBSTR(this.w_PATHTIPCLAS, AT("\", this.w_PATHTIPCLAS)+1,AT("\", this.w_PATHTIPCLAS,2)-AT("\", this.w_PATHTIPCLAS)-1)
          this.w_DESCRI = ""
          * --- Read from CLA_ALLE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2],.t.,this.CLA_ALLE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TACODCLA"+;
              " from "+i_cTable+" CLA_ALLE where ";
                  +"TACODICE = "+cp_ToStrODBC(this.w_TIPOALL);
                  +" and TACLADES = "+cp_ToStrODBC(this.w_CLASSEALL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TACODCLA;
              from (i_cTable) where;
                  TACODICE = this.w_TIPOALL;
                  and TACLADES = this.w_CLASSEALL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESCRI = NVL(cp_ToDate(_read_.TACODCLA),cp_NullValue(_read_.TACODCLA))
            use
            if i_Rows=0
              this.w_CLASSEALL = Alltrim(this.w_CLASSEALL) + "\"
              * --- Read from CLA_ALLE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2],.t.,this.CLA_ALLE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TACODCLA"+;
                  " from "+i_cTable+" CLA_ALLE where ";
                      +"TACODICE = "+cp_ToStrODBC(this.w_TIPOALL);
                      +" and TAPATCLA = "+cp_ToStrODBC(this.w_CLASSEALL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TACODCLA;
                  from (i_cTable) where;
                      TACODICE = this.w_TIPOALL;
                      and TAPATCLA = this.w_CLASSEALL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DESCRI = NVL(cp_ToDate(_read_.TACODCLA),cp_NullValue(_read_.TACODCLA))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CLASSEALL = this.w_DESCRI
        else
          this.w_TIPOALL = ""
        endif
      endif
      do case
        case this.oParentObject.w_TIPODESC = "N"
          this.w_AROGGETT = JUSTFNAME(this.w_FILENAME)
        case this.oParentObject.w_TIPODESC = "T"
          if UPPER(JUSTEXT(this.w_FILENAME)) = "DOC" AND !ISNULL(this.oWord)
            this.oDoc = this.oWord.Documents.Open(this.w_FILENAME,.t.,.t.,.f.)
            this.w_AROGGETT = this.oDoc.BuiltInDocumentProperties.Item("Title").Value
            if Empty(this.w_AROGGETT)
              this.w_AROGGETT = JUSTFNAME(this.w_FILENAME)
            endif
            this.oDoc.Close(.F.)     
            this.oDoc = .NULL.
          else
            this.w_AROGGETT = JUSTFNAME(this.w_FILENAME)
          endif
        case this.oParentObject.w_TIPODESC = "M"
          this.w_AROGGETT = this.oParentObject.w_DESINDICE
      endcase
      this.w_CONFARC = this.oParentObject.w_CONFARCH
      private L_SerIndex
      L_SerIndex=""
      Private L_ArrParam
      dimension L_ArrParam(30)
      L_ArrParam(1)="AR"
      L_ArrParam(2)=this.w_FILENAME
      L_ArrParam(3)=this.w_LICLADOC
      L_ArrParam(4)=i_CODUTE
      L_ArrParam(5)=this
      L_ArrParam(6)=this.w_AR_TABLE
      L_ArrParam(7)=this.w_CNCODCAN
      L_ArrParam(8)=ALLTRIM(this.w_AROGGETT)
      L_ArrParam(9)=""
      L_ArrParam(10)=.T.
      L_ArrParam(11)=.T.
      L_ArrParam(12)=" "
      L_ArrParam(13)=" "
      L_ArrParam(14)=this
      L_ArrParam(15)=this.w_TIPOALL
      L_ArrParam(16)=this.w_CLASSEALL
      L_ArrParam(17)=" "
      L_ArrParam(18)=" "
      L_ArrParam(19)="C"
      L_ArrParam(22)=""
      GSUT_BBA(this,@L_ArrParam, @L_SerIndex)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      L_SerIndex=LEFT(L_SerIndex, AT("*", L_SerIndex)-1)
      if this.w_CONFARC="S"
        addmsgnl("Creato indice confermato %1", this.w_MskMESS, L_SerIndex)
      else
        addmsgnl("Creato indice provvisorio %1", this.w_MskMESS, L_SerIndex)
      endif
      * --- Inserisco nella tabella di log l nuovo indice
      * --- Try
      local bErr_03A4C0E0
      bErr_03A4C0E0=bTrsErr
      this.Try_03A4C0E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into LOGSININ
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.LOGSININ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOGSININ_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGSININ_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LSCODCAN ="+cp_NullLink(cp_ToStrODBC(this.w_CNCODCAN),'LOGSININ','LSCODCAN');
          +",LDSER_ID ="+cp_NullLink(cp_ToStrODBC(L_SERINDEX),'LOGSININ','LDSER_ID');
          +",LSCODUTE ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOGSININ','LSCODUTE');
          +",LSDATSIN ="+cp_NullLink(cp_ToStrODBC(DATE()),'LOGSININ','LSDATSIN');
          +",LSSTATUS ="+cp_NullLink(cp_ToStrODBC(IIF(this.w_CONFARC="S", "C", "P")),'LOGSININ','LSSTATUS');
              +i_ccchkf ;
          +" where ";
              +"LSFILENM = "+cp_ToStrODBC(this.w_FILENAME);
                 )
        else
          update (i_cTable) set;
              LSCODCAN = this.w_CNCODCAN;
              ,LDSER_ID = L_SERINDEX;
              ,LSCODUTE = i_CODUTE;
              ,LSDATSIN = DATE();
              ,LSSTATUS = IIF(this.w_CONFARC="S", "C", "P");
              &i_ccchkf. ;
           where;
              LSFILENM = this.w_FILENAME;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_03A4C0E0
      * --- End
    endif
    SELECT _TMPFILENOSKIP_
    ENDSCAN
    USE IN SELECT("_TMPFILENOSKIP_")
    USE IN SELECT("_FILETOINDEX_")
    USE IN SELECT("_TMPFILEDIR_")
  endproc
  proc Try_03971F70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into LOGSININ
    i_nConn=i_TableProp[this.LOGSININ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGSININ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGSININ_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LSFILENM"+",LSSTATUS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_FILENAME),'LOGSININ','LSFILENM');
      +","+cp_NullLink(cp_ToStrODBC("I"),'LOGSININ','LSSTATUS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LSFILENM',this.w_FILENAME,'LSSTATUS',"I")
      insert into (i_cTable) (LSFILENM,LSSTATUS &i_ccchkf. );
         values (;
           this.w_FILENAME;
           ,"I";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03A4C0E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into LOGSININ
    i_nConn=i_TableProp[this.LOGSININ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGSININ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGSININ_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LSFILENM"+",LDSER_ID"+",LSCODCAN"+",LSCODUTE"+",LSDATSIN"+",LSSTATUS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_FILENAME),'LOGSININ','LSFILENM');
      +","+cp_NullLink(cp_ToStrODBC(L_SerIndex),'LOGSININ','LDSER_ID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CNCODCAN),'LOGSININ','LSCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOGSININ','LSCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(DATE()),'LOGSININ','LSDATSIN');
      +","+cp_NullLink(cp_ToStrODBC(IIF(this.w_CONFARC="S", "C", "P")),'LOGSININ','LSSTATUS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LSFILENM',this.w_FILENAME,'LDSER_ID',L_SerIndex,'LSCODCAN',this.w_CNCODCAN,'LSCODUTE',i_CODUTE,'LSDATSIN',DATE(),'LSSTATUS',IIF(this.w_CONFARC="S", "C", "P"))
      insert into (i_cTable) (LSFILENM,LDSER_ID,LSCODCAN,LSCODUTE,LSDATSIN,LSSTATUS &i_ccchkf. );
         values (;
           this.w_FILENAME;
           ,L_SerIndex;
           ,this.w_CNCODCAN;
           ,i_CODUTE;
           ,DATE();
           ,IIF(this.w_CONFARC="S", "C", "P");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='LOGSININ'
    this.cWorkTables[3]='CLA_ALLE'
    this.cWorkTables[4]='TIP_ALLE'
    this.cWorkTables[5]='PROMINDI'
    this.cWorkTables[6]='PRODINDI'
    this.cWorkTables[7]='CAN_TIER'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_PROMINDI')
      use in _Curs_PROMINDI
    endif
    if used('_Curs_PROMINDI')
      use in _Curs_PROMINDI
    endif
    if used('_Curs_QUERY_GSUT3KIS')
      use in _Curs_QUERY_GSUT3KIS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsut_bis
  Proc ListDirToCur
    PARAMETERS cPath, cExt, cCursor
    LOCAL l_path
    l_path = ADDBS(cPath)
    LOCAL l_numExt, l_NumFile, l_Count, l_NumDir, l_i 
    CREATE CURSOR (cCursor) (FileName C(250),FileAtt C(5))
    If Directory(l_path)
      l_NumFile = ADIR(aFile, l_path+'*.*','AHRSD')
      FOR l_i = 1 TO l_NumFile
        IF aFile[l_i,1]<>'.' AND aFile[l_i,1]<>'..'
          aFile(l_i,1) = l_path + aFile(l_i,1)
        ENDIF
      NEXT
      l_Count = 1
      DO WHILE l_Count <= ALEN(aFile,1)
        IF !('D'$aFile[l_Count,5]) AND (EMPTY(cExt) OR UPPER(JUSTEXT(aFile[l_Count,1]))$UPPER(cExt))
          INSERT INTO (cCursor) (FileName, FileAtt) VALUES (aFile[l_Count,1], aFile[l_Count,5])
        ENDIF
        IF aFile[l_Count,1]<>'.' AND aFile[l_Count,1]<>'..' AND 'D'$aFile[l_Count,5]
            l_path = ADDBS(aFile[l_Count,1])
          l_NumDir = ADIR(aFileDir, l_path+'*.*','AHRSD')
          DIMENSION aFile[l_NumDir+l_NumFile, 5]
          FOR l_i = 1 TO l_NumDir
              IF aFileDir[l_i,1]<>'.' AND aFileDir[l_i,1]<>'..'
                aFile(l_NumFile+l_i,1) = l_path + aFileDir[l_i,1]
                aFile(l_NumFile+l_i,5) = aFileDir[l_i,5]				 	
              ELSE
                aFile(l_NumFile+l_i,1) = aFileDir[l_i,1]
                aFile(l_NumFile+l_i,5) = aFileDir[l_i,5]
              ENDIF 
          NEXT
          l_NumFile = l_NumDir + l_NumFile
        ENDIF
      l_Count = l_Count + 1
      ENDDO
      Release aFile
      Release aFileDir
    EndIf
  EndProc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
