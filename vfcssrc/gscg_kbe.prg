* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kbe                                                        *
*              Generazione file per bilancio esterno                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-03-05                                                      *
* Last revis.: 2014-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kbe",oParentObject))

* --- Class definition
define class tgscg_kbe as StdForm
  Top    = 19
  Left   = 19

  * --- Standard Properties
  Width  = 697
  Height = 184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-01-24"
  HelpContextID=116008297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=63

  * --- Constant Properties
  _IDX = 0
  TIR_MAST_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  BUSIUNIT_IDX = 0
  AZIENDA_IDX = 0
  SB_MAST_IDX = 0
  VOCIRICL_IDX = 0
  CONTROPA_IDX = 0
  CONF_INT_IDX = 0
  cPrg = "gscg_kbe"
  cComment = "Generazione file per bilancio esterno"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_codaz1 = space(5)
  o_codaz1 = space(5)
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_MASABI = space(1)
  w_AZESSTCO = space(4)
  w_FINSTO = ctod('  /  /  ')
  w_UTENTE = 0
  w_gestbu = space(1)
  w_ESE = space(4)
  o_ESE = space(4)
  w_ESSALDI = space(1)
  o_ESSALDI = space(1)
  w_VALAPP = space(3)
  o_VALAPP = space(3)
  w_totdat = space(1)
  o_totdat = space(1)
  w_data1 = ctod('  /  /  ')
  o_data1 = ctod('  /  /  ')
  w_data2 = ctod('  /  /  ')
  w_ESPREC = space(1)
  o_ESPREC = space(1)
  w_monete = space(1)
  o_monete = space(1)
  w_divisa = space(3)
  o_divisa = space(3)
  w_DATEMU = ctod('  /  /  ')
  w_simval = space(5)
  w_SBDESCRI = space(35)
  w_PROVVI = space(1)
  o_PROVVI = space(1)
  w_CODBUN = space(3)
  w_SUPERBU = space(15)
  w_PATH = space(150)
  o_PATH = space(150)
  w_PROGRE = space(10)
  w_SETMRK = space(1)
  o_SETMRK = space(1)
  w_FILE = space(150)
  w_ANONIMO = space(1)
  w_descri = space(30)
  w_decimi = 0
  w_BUDESCRI = space(35)
  w_CAMVAL = 0
  w_EXTRAEUR = 0
  o_EXTRAEUR = 0
  w_DATINIESE = ctod('  /  /  ')
  w_DATFINESE = ctod('  /  /  ')
  w_TIPVOC = space(10)
  w_DECNAZ = 0
  w_CAONAZ = 0
  w_esepre = space(4)
  w_PRVALNAZ = space(3)
  w_PRCAOVAL = 0
  w_TIPSTA = space(1)
  w_PRDECTOT = 0
  w_PRINIESE = ctod('  /  /  ')
  w_AZ_INDWE = space(100)
  w_CODIDE = space(5)
  w_CODIDA = space(4)
  w_PROATT = space(3)
  w_PATHF = space(200)
  o_PATHF = space(200)
  w_CONSOLIDATO = space(1)
  w_PATHB = space(200)
  o_PATHB = space(200)
  w_TIPFILE = space(4)
  w_LPATH = .F.
  w_attivita = space(1)
  w_passivi = space(1)
  w_Costi = space(1)
  w_Ricavi = space(1)
  w_Ordine = space(1)
  w_transi = space(1)
  w_PATHO = space(200)
  o_PATHO = space(200)
  w_CODITTA = space(5)
  w_INFRAAN = space(1)
  w_CONFRONTO = space(1)
  w_DETCLFR = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kbePag1","gscg_kbe",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESE_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='TIR_MAST'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='BUSIUNIT'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='SB_MAST'
    this.cWorkTables[7]='VOCIRICL'
    this.cWorkTables[8]='CONTROPA'
    this.cWorkTables[9]='CONF_INT'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_kbe
    this.bUpdated=.t.
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_codaz1=space(5)
      .w_CODAZI=space(5)
      .w_MASABI=space(1)
      .w_AZESSTCO=space(4)
      .w_FINSTO=ctod("  /  /  ")
      .w_UTENTE=0
      .w_gestbu=space(1)
      .w_ESE=space(4)
      .w_ESSALDI=space(1)
      .w_VALAPP=space(3)
      .w_totdat=space(1)
      .w_data1=ctod("  /  /  ")
      .w_data2=ctod("  /  /  ")
      .w_ESPREC=space(1)
      .w_monete=space(1)
      .w_divisa=space(3)
      .w_DATEMU=ctod("  /  /  ")
      .w_simval=space(5)
      .w_SBDESCRI=space(35)
      .w_PROVVI=space(1)
      .w_CODBUN=space(3)
      .w_SUPERBU=space(15)
      .w_PATH=space(150)
      .w_PROGRE=space(10)
      .w_SETMRK=space(1)
      .w_FILE=space(150)
      .w_ANONIMO=space(1)
      .w_descri=space(30)
      .w_decimi=0
      .w_BUDESCRI=space(35)
      .w_CAMVAL=0
      .w_EXTRAEUR=0
      .w_DATINIESE=ctod("  /  /  ")
      .w_DATFINESE=ctod("  /  /  ")
      .w_TIPVOC=space(10)
      .w_DECNAZ=0
      .w_CAONAZ=0
      .w_esepre=space(4)
      .w_PRVALNAZ=space(3)
      .w_PRCAOVAL=0
      .w_TIPSTA=space(1)
      .w_PRDECTOT=0
      .w_PRINIESE=ctod("  /  /  ")
      .w_AZ_INDWE=space(100)
      .w_CODIDE=space(5)
      .w_CODIDA=space(4)
      .w_PROATT=space(3)
      .w_PATHF=space(200)
      .w_CONSOLIDATO=space(1)
      .w_PATHB=space(200)
      .w_TIPFILE=space(4)
      .w_LPATH=.f.
      .w_attivita=space(1)
      .w_passivi=space(1)
      .w_Costi=space(1)
      .w_Ricavi=space(1)
      .w_Ordine=space(1)
      .w_transi=space(1)
      .w_PATHO=space(200)
      .w_CODITTA=space(5)
      .w_INFRAAN=space(1)
      .w_CONFRONTO=space(1)
      .w_DETCLFR=space(1)
        .w_codaz1 = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_codaz1))
          .link_1_1('Full')
        endif
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZI))
          .link_1_2('Full')
        endif
        .w_MASABI = ' '
        .w_AZESSTCO = IIF(g_APPLICATION="ADHOC REVOLUTION",'    ',LOOKTAB("AZIENDA","AZESSTCO","AZCODAZI",.w_CODAZ1))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_AZESSTCO))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_UTENTE = g_CODUTE
          .DoRTCalc(7,7,.f.)
        .w_ESE = g_codese
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_ESE))
          .link_1_8('Full')
        endif
        .w_ESSALDI = IIF(.w_MASABI='S',' ','S')
        .w_VALAPP = g_PERVAL
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_VALAPP))
          .link_1_10('Full')
        endif
        .w_totdat = IIF(.w_ESSALDI='S','T',IIF(EMPTY(NVL(.w_totdat,' ')),'T',.w_totdat))
        .w_data1 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATINIESE))
        .w_data2 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATFINESE))
        .w_ESPREC = IIF(.w_TOTDAT='T','S',IIF(.w_DATA1=.w_DATINIESE,'S',' '))
        .w_monete = 'c'
        .w_divisa = .w_VALAPP
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_divisa))
          .link_1_16('Full')
        endif
          .DoRTCalc(17,19,.f.)
        .w_PROVVI = IIF(.w_ESSALDI='S','N',IIF(.w_TOTDAT='T','N',.w_PROVVI))
        .w_CODBUN = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'   ',IIF(EMPTY(NVL(.w_CODBUN,'   ')),'   ',.w_CODBUN))
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_CODBUN))
          .link_1_21('Full')
        endif
        .w_SUPERBU = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'',IIF(NOT EMPTY(.w_CODBUN),'',IIF(EMPTY(NVL(.w_SUPERBU,'')),'',.w_SUPERBU)))
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_SUPERBU))
          .link_1_22('Full')
        endif
        .w_PATH = .w_PATHF
        .w_PROGRE = Right('00'+Alltrim(Str(Hour( Datetime()))),2) +Right('00'+Alltrim(Str(Minute(Datetime()))),2)+Right('00'+Alltrim(Str(Sec(Datetime()))),2)
          .DoRTCalc(25,25,.f.)
        .w_FILE = ALLTRIM('BLIM'+SUBSTR(.w_CODITTA,1,4)+'.txt')
          .DoRTCalc(27,30,.f.)
        .w_CAMVAL = iif(.w_EXTRAEUR=0,GETCAM(.w_DIVISA, .w_DATFINESE, -1), .w_EXTRAEUR)
          .DoRTCalc(32,34,.f.)
        .w_TIPVOC = 'M'
          .DoRTCalc(36,37,.f.)
        .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_esepre))
          .link_1_46('Full')
        endif
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_PRVALNAZ))
          .link_1_47('Full')
        endif
          .DoRTCalc(40,40,.f.)
        .w_TIPSTA = 'T'
          .DoRTCalc(42,43,.f.)
        .w_AZ_INDWE = 'http://www.supermercato.it/Category.pasp?txtCatalog=MainCatalog&txtCategory=mondo_imprese&Restart=1'
          .DoRTCalc(45,48,.f.)
        .w_CONSOLIDATO = 'N'
        .w_PATHB = SYS(5)
        .w_TIPFILE = '.TXT'
          .DoRTCalc(52,52,.f.)
        .w_attivita = 'A'
        .w_passivi = 'P'
        .w_Costi = 'C'
        .w_Ricavi = 'R'
          .DoRTCalc(57,60,.f.)
        .w_INFRAAN = ' '
        .w_CONFRONTO = ' '
        .w_DETCLFR = ' '
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_CODAZ1<>.w_CODAZ1
          .link_1_1('Full')
        endif
        if .o_CODAZI<>.w_CODAZI
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,9,.t.)
        if .o_ESE<>.w_ESE
          .link_1_10('Full')
        endif
        if .o_ESSALDI<>.w_ESSALDI
            .w_totdat = IIF(.w_ESSALDI='S','T',IIF(EMPTY(NVL(.w_totdat,' ')),'T',.w_totdat))
        endif
        if .o_ESE<>.w_ESE.or. .o_TOTDAT<>.w_TOTDAT
            .w_data1 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATINIESE))
        endif
        if .o_ESE<>.w_ESE.or. .o_TOTDAT<>.w_TOTDAT
            .w_data2 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATFINESE))
        endif
        if .o_data1<>.w_data1.or. .o_totdat<>.w_totdat.or. .o_PROVVI<>.w_PROVVI
            .w_ESPREC = IIF(.w_TOTDAT='T','S',IIF(.w_DATA1=.w_DATINIESE,'S',' '))
        endif
        .DoRTCalc(15,15,.t.)
        if .o_monete<>.w_monete.or. .o_VALAPP<>.w_VALAPP
            .w_divisa = .w_VALAPP
          .link_1_16('Full')
        endif
        .DoRTCalc(17,19,.t.)
        if .o_ESSALDI<>.w_ESSALDI
            .w_PROVVI = IIF(.w_ESSALDI='S','N',IIF(.w_TOTDAT='T','N',.w_PROVVI))
        endif
        if .o_totdat<>.w_totdat.or. .o_ESPREC<>.w_ESPREC.or. .o_data1<>.w_data1.or. .o_ESSALDI<>.w_ESSALDI
            .w_CODBUN = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'   ',IIF(EMPTY(NVL(.w_CODBUN,'   ')),'   ',.w_CODBUN))
          .link_1_21('Full')
        endif
        if .o_data1<>.w_data1.or. .o_ESPREC<>.w_ESPREC.or. .o_totdat<>.w_totdat.or. .o_ESSALDI<>.w_ESSALDI
            .w_SUPERBU = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'',IIF(NOT EMPTY(.w_CODBUN),'',IIF(EMPTY(NVL(.w_SUPERBU,'')),'',.w_SUPERBU)))
          .link_1_22('Full')
        endif
        .DoRTCalc(23,23,.t.)
            .w_PROGRE = Right('00'+Alltrim(Str(Hour( Datetime()))),2) +Right('00'+Alltrim(Str(Minute(Datetime()))),2)+Right('00'+Alltrim(Str(Sec(Datetime()))),2)
        .DoRTCalc(25,30,.t.)
        if .o_EXTRAEUR<>.w_EXTRAEUR.or. .o_divisa<>.w_divisa
            .w_CAMVAL = iif(.w_EXTRAEUR=0,GETCAM(.w_DIVISA, .w_DATFINESE, -1), .w_EXTRAEUR)
        endif
        .DoRTCalc(32,37,.t.)
        if .o_ese<>.w_ese
            .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
          .link_1_46('Full')
        endif
          .link_1_47('Full')
        .DoRTCalc(40,43,.t.)
            .w_AZ_INDWE = 'http://www.supermercato.it/Category.pasp?txtCatalog=MainCatalog&txtCategory=mondo_imprese&Restart=1'
        .DoRTCalc(45,50,.t.)
            .w_TIPFILE = '.TXT'
        if .o_PATH<>.w_PATH
          .Calculate_MMWNFHGZRT()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(52,63,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_MMWNFHGZRT()
    with this
          * --- Aggiunge \ al path e controllo esistenza
          .w_PATH = IIF(right(alltrim(.w_PATH),1)='\' or empty(.w_PATH),.w_PATH,alltrim(.w_PATH)+iif(len(alltrim(.w_PATH))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_PATH,'F')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.ototdat_1_11.enabled = this.oPgFrm.Page1.oPag.ototdat_1_11.mCond()
    this.oPgFrm.Page1.oPag.odata1_1_12.enabled = this.oPgFrm.Page1.oPag.odata1_1_12.mCond()
    this.oPgFrm.Page1.oPag.odata2_1_13.enabled = this.oPgFrm.Page1.oPag.odata2_1_13.mCond()
    this.oPgFrm.Page1.oPag.oESPREC_1_14.enabled = this.oPgFrm.Page1.oPag.oESPREC_1_14.mCond()
    this.oPgFrm.Page1.oPag.oPROVVI_1_20.enabled = this.oPgFrm.Page1.oPag.oPROVVI_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oESSALDI_1_9.visible=!this.oPgFrm.Page1.oPag.oESSALDI_1_9.mHide()
    this.oPgFrm.Page1.oPag.oESPREC_1_14.visible=!this.oPgFrm.Page1.oPag.oESPREC_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=codaz1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codaz1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codaz1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_codaz1)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codaz1 = NVL(_Link_.AZCODAZI,space(5))
      this.w_gestbu = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_codaz1 = space(5)
      endif
      this.w_gestbu = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codaz1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COPATHB2,COPATHBO,COPATHFA,COCODIDE,COCODIDA,COPROATT";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COPATHB2,COPATHBO,COPATHFA,COCODIDE,COCODIDA,COPROATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_PATHB = NVL(_Link_.COPATHB2,space(200))
      this.w_PATHO = NVL(_Link_.COPATHBO,space(200))
      this.w_PATHF = NVL(_Link_.COPATHFA,space(200))
      this.w_CODIDE = NVL(_Link_.COCODIDE,space(5))
      this.w_CODIDA = NVL(_Link_.COCODIDA,space(4))
      this.w_PROATT = NVL(_Link_.COPROATT,space(3))
      this.w_CODITTA = NVL(_Link_.COCODIDE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PATHB = space(200)
      this.w_PATHO = space(200)
      this.w_PATHF = space(200)
      this.w_CODIDE = space(5)
      this.w_CODIDA = space(4)
      this.w_PROATT = space(3)
      this.w_CODITTA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZESSTCO
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZESSTCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZESSTCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_AZESSTCO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZ1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZ1;
                       ,'ESCODESE',this.w_AZESSTCO)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZESSTCO = NVL(_Link_.ESCODESE,space(4))
      this.w_FINSTO = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZESSTCO = space(4)
      endif
      this.w_FINSTO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZESSTCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_codaz1;
                     ,'ESCODESE',trim(this.w_ESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESE_1_8'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_codaz1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio storicizzato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
      this.w_DATINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_DATFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALAPP = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
      this.w_DATINIESE = ctod("  /  /  ")
      this.w_DATFINESE = ctod("  /  /  ")
      this.w_VALAPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATINIESE>.w_FINSTO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio storicizzato")
        endif
        this.w_ESE = space(4)
        this.w_DATINIESE = ctod("  /  /  ")
        this.w_DATFINESE = ctod("  /  /  ")
        this.w_VALAPP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALAPP
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALAPP)
            select VACODVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALAPP = NVL(_Link_.VACODVAL,space(3))
      this.w_DECNAZ = NVL(_Link_.VADECTOT,0)
      this.w_CAONAZ = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALAPP = space(3)
      endif
      this.w_DECNAZ = 0
      this.w_CAONAZ = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=divisa
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_divisa) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_divisa)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_divisa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_divisa)
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_divisa = NVL(_Link_.VACODVAL,space(3))
      this.w_descri = NVL(_Link_.VADESVAL,space(30))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
      this.w_EXTRAEUR = NVL(_Link_.VACAOVAL,0)
      this.w_simval = NVL(_Link_.VASIMVAL,space(5))
      this.w_DATEMU = NVL(cp_ToDate(_Link_.VADATEUR),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_divisa = space(3)
      endif
      this.w_descri = space(30)
      this.w_decimi = 0
      this.w_EXTRAEUR = 0
      this.w_simval = space(5)
      this.w_DATEMU = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_divisa Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_codaz1;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_BUDESCRI = NVL(_Link_.BUDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(3)
      endif
      this.w_BUDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SUPERBU
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SB_MAST_IDX,3]
    i_lTable = "SB_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2], .t., this.SB_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SUPERBU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SUPERBU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(this.w_SUPERBU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',this.w_SUPERBU)
            select SBCODICE,SBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SUPERBU = NVL(_Link_.SBCODICE,space(15))
      this.w_SBDESCRI = NVL(_Link_.SBDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SUPERBU = space(15)
      endif
      this.w_SBDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])+'\'+cp_ToStr(_Link_.SBCODICE,1)
      cp_ShowWarn(i_cKey,this.SB_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SUPERBU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=esepre
  func Link_1_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_esepre) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_esepre)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_esepre);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_esepre)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_esepre = NVL(_Link_.ESCODESE,space(4))
      this.w_PRVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_PRINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_esepre = space(4)
      endif
      this.w_PRVALNAZ = space(3)
      this.w_PRINIESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_esepre Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRVALNAZ
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PRVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PRVALNAZ)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_PRCAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_PRDECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRVALNAZ = space(3)
      endif
      this.w_PRCAOVAL = 0
      this.w_PRDECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESE_1_8.value==this.w_ESE)
      this.oPgFrm.Page1.oPag.oESE_1_8.value=this.w_ESE
    endif
    if not(this.oPgFrm.Page1.oPag.oESSALDI_1_9.RadioValue()==this.w_ESSALDI)
      this.oPgFrm.Page1.oPag.oESSALDI_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ototdat_1_11.RadioValue()==this.w_totdat)
      this.oPgFrm.Page1.oPag.ototdat_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.odata1_1_12.value==this.w_data1)
      this.oPgFrm.Page1.oPag.odata1_1_12.value=this.w_data1
    endif
    if not(this.oPgFrm.Page1.oPag.odata2_1_13.value==this.w_data2)
      this.oPgFrm.Page1.oPag.odata2_1_13.value=this.w_data2
    endif
    if not(this.oPgFrm.Page1.oPag.oESPREC_1_14.RadioValue()==this.w_ESPREC)
      this.oPgFrm.Page1.oPag.oESPREC_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVVI_1_20.RadioValue()==this.w_PROVVI)
      this.oPgFrm.Page1.oPag.oPROVVI_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_23.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_23.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oFILE_1_28.value==this.w_FILE)
      this.oPgFrm.Page1.oPag.oFILE_1_28.value=this.w_FILE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ESE)) or not(.w_DATINIESE>.w_FINSTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oESE_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ESE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio storicizzato")
          case   not((empty(.w_data2) or .w_data2>=.w_data1) AND .w_data1>.w_FINSTO)  and (.w_totdat='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata1_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo date non valido o data inferiore a quella di storicizzazione")
          case   not(empty(.w_data1) or .w_data2>=.w_data1)  and (.w_totdat='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata2_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date non valido")
          case   (empty(.w_divisa))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odivisa_1_16.SetFocus()
            i_bnoObbl = !empty(.w_divisa)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMVAL_1_39.SetFocus()
            i_bnoObbl = !empty(.w_CAMVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_esepre))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oesepre_1_46.SetFocus()
            i_bnoObbl = !empty(.w_esepre)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_kbe
        If (  Empty( this.w_PATH ))
        return .F.
        ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_codaz1 = this.w_codaz1
    this.o_CODAZI = this.w_CODAZI
    this.o_ESE = this.w_ESE
    this.o_ESSALDI = this.w_ESSALDI
    this.o_VALAPP = this.w_VALAPP
    this.o_totdat = this.w_totdat
    this.o_data1 = this.w_data1
    this.o_ESPREC = this.w_ESPREC
    this.o_monete = this.w_monete
    this.o_divisa = this.w_divisa
    this.o_PROVVI = this.w_PROVVI
    this.o_PATH = this.w_PATH
    this.o_SETMRK = this.w_SETMRK
    this.o_EXTRAEUR = this.w_EXTRAEUR
    this.o_PATHF = this.w_PATHF
    this.o_PATHB = this.w_PATHB
    this.o_PATHO = this.w_PATHO
    return

enddefine

* --- Define pages as container
define class tgscg_kbePag1 as StdContainer
  Width  = 693
  height = 184
  stdWidth  = 693
  stdheight = 184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESE_1_8 as StdField with uid="HDHRXGXWEA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ESE", cQueryName = "ESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio storicizzato",;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 115703226,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=118, Top=8, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_codaz1", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESE"

  func oESE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oESE_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESE_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_codaz1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_codaz1)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESE_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oESSALDI_1_9 as StdCheck with uid="LHILIIGAEZ",rtseq=9,rtrep=.f.,left=189, top=8, caption="Lettura saldi eser. attuale",;
    ToolTipText = "I dati estratti verranno presi dall'archivio saldi",;
    HelpContextID = 233020858,;
    cFormVar="w_ESSALDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESSALDI_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oESSALDI_1_9.GetRadio()
    this.Parent.oContained.w_ESSALDI = this.RadioValue()
    return .t.
  endfunc

  func oESSALDI_1_9.SetRadio()
    this.Parent.oContained.w_ESSALDI=trim(this.Parent.oContained.w_ESSALDI)
    this.value = ;
      iif(this.Parent.oContained.w_ESSALDI=='S',1,;
      0)
  endfunc

  func oESSALDI_1_9.mHide()
    with this.Parent.oContained
      return (.w_MASABI='S')
    endwith
  endfunc


  add object ototdat_1_11 as StdCombo with uid="BRHXJMVGFR",rtseq=11,rtrep=.f.,left=118,top=36,width=136,height=21;
    , ToolTipText = "Tipo bilancio selezionato";
    , HelpContextID = 59871542;
    , cFormVar="w_totdat",RowSource=""+"Totale esercizio,"+"Da data a data", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func ototdat_1_11.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func ototdat_1_11.GetRadio()
    this.Parent.oContained.w_totdat = this.RadioValue()
    return .t.
  endfunc

  func ototdat_1_11.SetRadio()
    this.Parent.oContained.w_totdat=trim(this.Parent.oContained.w_totdat)
    this.value = ;
      iif(this.Parent.oContained.w_totdat=='T',1,;
      iif(this.Parent.oContained.w_totdat=='D',2,;
      0))
  endfunc

  func ototdat_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  add object odata1_1_12 as StdField with uid="LRAOKJALSD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_data1", cQueryName = "data1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo date non valido o data inferiore a quella di storicizzazione",;
    ToolTipText = "Data di registrazione di inizio stampa",;
    HelpContextID = 57769418,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=345, Top=36

  func odata1_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_totdat='D')
    endwith
   endif
  endfunc

  func odata1_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_data2) or .w_data2>=.w_data1) AND .w_data1>.w_FINSTO)
    endwith
    return bRes
  endfunc

  add object odata2_1_13 as StdField with uid="VDVPBDFQIF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_data2", cQueryName = "data2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date non valido",;
    ToolTipText = "Data di registrazione di fine stampa",;
    HelpContextID = 56720842,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=448, Top=36

  func odata2_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_totdat='D')
    endwith
   endif
  endfunc

  func odata2_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data1) or .w_data2>=.w_data1)
    endwith
    return bRes
  endfunc

  add object oESPREC_1_14 as StdCheck with uid="NULXFFIFIL",rtseq=14,rtrep=.f.,left=528, top=36, caption="Saldi es. precedente",;
    ToolTipText = "Considera i saldi dell'esercizio precedente",;
    HelpContextID = 12399174,;
    cFormVar="w_ESPREC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESPREC_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESPREC_1_14.GetRadio()
    this.Parent.oContained.w_ESPREC = this.RadioValue()
    return .t.
  endfunc

  func oESPREC_1_14.SetRadio()
    this.Parent.oContained.w_ESPREC=trim(this.Parent.oContained.w_ESPREC)
    this.value = ;
      iif(this.Parent.oContained.w_ESPREC=='S',1,;
      0)
  endfunc

  func oESPREC_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TOTDAT='D' AND .w_DATA1=.w_DATINIESE)
    endwith
   endif
  endfunc

  func oESPREC_1_14.mHide()
    with this.Parent.oContained
      return (.w_PROVVI='S')
    endwith
  endfunc


  add object oPROVVI_1_20 as StdCombo with uid="WZWYZYTQAG",value=3,rtseq=20,rtrep=.f.,left=118,top=64,width=136,height=21;
    , ToolTipText = "Selezione dello stato dei movimenti da stampare";
    , HelpContextID = 131146230;
    , cFormVar="w_PROVVI",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVVI_1_20.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oPROVVI_1_20.GetRadio()
    this.Parent.oContained.w_PROVVI = this.RadioValue()
    return .t.
  endfunc

  func oPROVVI_1_20.SetRadio()
    this.Parent.oContained.w_PROVVI=trim(this.Parent.oContained.w_PROVVI)
    this.value = ;
      iif(this.Parent.oContained.w_PROVVI=='N',1,;
      iif(this.Parent.oContained.w_PROVVI=='S',2,;
      iif(this.Parent.oContained.w_PROVVI=='',3,;
      0)))
  endfunc

  func oPROVVI_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  add object oPATH_1_23 as StdField with uid="HBNCUVPVRI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il path file (impostabile di default in gestione path)",;
    HelpContextID = 110927626,;
   bGlobalFont=.t.,;
    Height=21, Width=521, Left=118, Top=91, InputMask=replicate('X',150)


  add object oBtn_1_24 as StdButton with uid="PSACVHWWYM",left=649, top=91, width=22,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per scegliere destinazione file";
    , HelpContextID = 115807274;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        .w_PATH=left(cp_getdir(IIF(EMPTY(.w_PATH),sys(5)+sys(2003),.w_PATH),"Percorso di destinazione")+space(40),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFILE_1_28 as StdField with uid="KDKEYWXRRW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_FILE", cQueryName = "FILE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Indicare il nome del file da generare",;
    HelpContextID = 111155114,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=118, Top=121, InputMask=replicate('X',150)


  add object oBtn_1_32 as StdButton with uid="NDASJAFEBO",left=573, top=131, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare il file";
    , HelpContextID = 243373078;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        do GSCG_BKE with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_34 as StdButton with uid="NLMNHXMBPQ",left=628, top=131, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 243373078;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_26 as StdString with uid="BPUEMPCWXC",Visible=.t., Left=425, Top=36,;
    Alignment=1, Width=20, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="SSOHJFPBMQ",Visible=.t., Left=21, Top=8,;
    Alignment=1, Width=95, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="PUKCKPISOE",Visible=.t., Left=21, Top=36,;
    Alignment=1, Width=95, Height=15,;
    Caption="Tipo bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ADKVKJVYMA",Visible=.t., Left=260, Top=36,;
    Alignment=1, Width=80, Height=15,;
    Caption="Periodo dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="TSHJITFASN",Visible=.t., Left=21, Top=64,;
    Alignment=1, Width=95, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="EHJMGLHYKT",Visible=.t., Left=6, Top=92,;
    Alignment=1, Width=110, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="WXQDWIQOER",Visible=.t., Left=6, Top=122,;
    Alignment=1, Width=110, Height=18,;
    Caption="File txt:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kbe','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
