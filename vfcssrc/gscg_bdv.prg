* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bdv                                                        *
*              Gestione maschera IVA per.                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_290]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2015-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bdv",oParentObject,m.pTipo)
return(i_retval)

define class tgscg_bdv as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_OGGETTO = .NULL.
  w_FLPROR = space(1)
  w_PERPRO = 0
  w_IVACRE = 0
  w_KEYATT = space(5)
  w_TOTIVD = 0
  w_ANNO = space(4)
  w_NUMPER = 0
  w_AZIENDA = space(5)
  w_INDAZI = space(25)
  w_LOCAZI = space(35)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_PIVAZI = space(12)
  w_COFAZI = space(16)
  w_PRPARI = 0
  w_INTLIG = space(1)
  w_PREFIS = space(20)
  w_PAGINE = 0
  w_DESBAN = space(35)
  w_IMPVAL = space(3)
  w_DESABI = space(80)
  w_DESFIL = space(40)
  w_INDIRI = space(50)
  w_CAP = space(5)
  w_PRIREG = space(1)
  w_PRINUM = 0
  w_DESCRI = space(65)
  w_PRPARI = 0
  w_INTLIG = space(1)
  w_PREFIS = space(20)
  w_PAGINE = 0
  w_CODATTS = space(8)
  w_DATINI = ctod("  /  /  ")
  * --- WorkFile variables
  ATTIMAST_idx=0
  PRO_RATA_idx=0
  VOCIIVA_idx=0
  AZIENDA_idx=0
  ATT_ALTE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Maschera Iva Periodica (DA GSCG_SIM (Parametro: D, L), GSCG_SDP (Parametro S), GSCG_SD2 (Parametro L) e GSCG_BDI (Parametro B))
    * --- ATTENZIONE: Le maschere GSCG_SD2 e GSCG_SIM vengono a loro volta lanciate da GSCG_BDI
    WITH this.oParentObject
    do case
      case this.pTipo="B"
        this.w_PAGINE = .w_PAGINE
        this.w_PRPARI = .w_PRPARI
        this.w_INTLIG = .w_INTLIG
        this.w_PREFIS = .w_PREFIS
        this.w_DESBAN = SPACE(35)
        this.w_IMPVAL = IIF(.w_VPCODVAL="S", g_CODEUR, g_CODLIR)
        this.w_DESABI = .w_DESABI
        this.w_DESFIL = .w_DESFIL
        this.w_INDIRI = .w_INDIRI
        this.w_CAP = .w_CAP
        this.w_PRIREG = .w_PRIREG
        this.w_PRINUM = .w_PRINUM
      case this.pTipo $ "DL"
        this.w_PAGINE = .oParentObject.w_PAGINE
        this.w_PRPARI = .oParentObject.w_PRPARI
        this.w_INTLIG = .oParentObject.w_INTLIG
        this.w_PREFIS = .oParentObject.w_PREFIS
        this.w_DESBAN = .w_DESBAN
        this.w_IMPVAL = .w_IMPVAL
        this.w_DESABI = .w_DESABI
        this.w_DESFIL = .w_DESFIL
        this.w_INDIRI = .w_INDIRI
        this.w_CAP = .w_CAP
        this.w_PRIREG = .oParentObject.w_PRIREG
        this.w_PRINUM = .oParentObject.w_PRINUM
      case this.pTipo="_BEFOREVP11"
        * --- Cambio sfondo al credito utilizzato
        this.w_OGGETTO = This.oParentObject.GETCTRL("w_CRERES")
        this.w_OGGETTO.DisabledBackColor = 11454589
        this.w_OGGETTO.DisabledForeColor = 16777215
        this.w_OGGETTO.Refresh()     
        i_retcode = 'stop'
        return
      case this.pTipo="_AFTERVP11"
        * --- Rimetto lo sfondo al credito utilizzato
        this.w_OGGETTO = This.oParentObject.GETCTRL("w_CRERES")
        this.w_OGGETTO.DisabledBackColor = 16777215
        this.w_OGGETTO.DisabledForeColor = 0
        this.w_OGGETTO.Refresh()     
        i_retcode = 'stop'
        return
      otherwise
        this.w_PAGINE = 0
        this.w_PRPARI = 0
        this.w_INTLIG = " "
        this.w_PREFIS = SPACE(20)
        this.w_DESBAN = .w_DESBAN
        this.w_IMPVAL = .w_IMPVAL
        this.w_DESABI = .w_DESABI
        this.w_DESFIL = .w_DESFIL
        this.w_INDIRI = .w_INDIRI
        this.w_CAP = .w_CAP
        this.w_PRIREG = " "
        this.w_PRINUM = 0
    endcase
    ENDWITH
    this.w_AZIENDA = i_CODAZI
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = this.w_AZIENDA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
      this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
      this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
      this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
      this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.pTipo="S" AND g_ATTIVI="S" AND this.oParentObject.w_TIPLIQ<>"R"
      ah_ErrorMsg("Impossibile stampare la dichiarazione di una singola attivit�",,"")
      i_retcode = 'stop'
      return
    endif
    if this.pTipo $ "DS"
      if this.oParentObject.w_VPIMPVER>0 AND(EMPTY(this.oParentObject.w_VPDATVER) OR EMPTY(this.oParentObject.w_VPCODAZI) OR EMPTY(this.oParentObject.w_VPCODCAB))
        ah_ErrorMsg("Estremi del versamento incompleti",,"")
      endif
      if EMPTY(this.oParentObject.w_VPCODFIS)
        ah_ErrorMsg("Codice fiscale del dichiarante non inserito",,"")
      endif
    endif
    * --- Lancio il report di stampa ( Modulo Continuo /Grafico )
    * --- L_Stampato viene messo a 1 dai Report
    L_STAMPATO=0
    * --- Passo le variabili presenti sulla maschera ai vari report di stampa.
    L_DICHGRU1=this.oParentObject.w_TIPDIC
    L_VPIMPOR1=this.oParentObject.w_VPIMPOR1
    L_VPIMPOR2=this.oParentObject.w_VPIMPOR2
    L_VPCESINT= this.oParentObject.w_VPCESINT
    L_VPACQINT= this.oParentObject.w_VPACQINT
    L_VPIMPON3= this.oParentObject.w_VPIMPON3
    L_VPIMPOS3= this.oParentObject.w_VPIMPOS3
    L_VPIMPOR5= this.oParentObject.w_VPIMPOR5
    L_VPIMPOR6C= this.oParentObject.w_VPIMPOR6C
    L_VPIMPOR6= this.oParentObject.w_VPIMPOR6
    L_VPIMPOR7= this.oParentObject.w_VPIMPOR7
    L_VPIMPOR8= this.oParentObject.w_VPIMPOR8
    L_VPIMPOR9= this.oParentObject.w_VPIMPOR9
    L_VPIMPO10=this.oParentObject.w_VPIMPO10
    L_VPIMPO11=this.oParentObject.w_VPIMPO11
    L_VPIMPO12=this.oParentObject.w_VPIMPO12
    L_VPIMPO13=this.oParentObject.w_VPIMPO13
    L_VPIMPO14=this.oParentObject.w_VPIMPO14
    L_VPIMPO15= this.oParentObject.w_VPIMPO15
    L_VPIMPO16= this.oParentObject.w_VPIMPO16
    L_VPOPNOIM=this.oParentObject.w_VPOPNOIM
    L_VPPAR74C4=this.oParentObject.w_VPAR74C4
    L_VPVARIMP=this.oParentObject.w_VPVARIMP
    L_VPVERNEF=this.oParentObject.w_VPVERNEF
    L_VPCODFIS=this.oParentObject.w_VPCODFIS
    L_VPOPMEDI=this.oParentObject.w_VPOPMEDI
    L_VPCODCAR=this.oParentObject.w_VPCODCAR
    L_VPCRERIM=this.oParentObject.w_VPCRERIM
    L_VPCREUTI= this.oParentObject.w_VPCREUTI
    L_VPCORTER=this.oParentObject.w_VPCORTER
    L_VPACQBEA=this.oParentObject.w_VPACQBEA
    * --- w_IMPVAL: Codice Valuta Dichiarazione calcolato sulla maschera in base a VPCODVAL 
    L_IMPVAL=this.w_IMPVAL
    L_VPEURO19=this.oParentObject.w_VPEURO19
    L_VPEURO17=this.oParentObject.w_VPVEREUR
    L_VPIMPVER=this.oParentObject.w_VPIMPVER
    L_TIPODIC=this.oParentObject.w_VPPERIOD
    L_ABI=this.oParentObject.w_VPCODAZI
    L_CAB=this.oParentObject.w_VPCODCAB
    L_ANNO=this.oParentObject.w_VP__ANNO
    L_DATVERS=this.oParentObject.w_VPDATVER
    L_CONCES=this.oParentObject.w_CONCES
    L_DESBAN=this.w_DESBAN
    L_TIPLIQ=this.oParentObject.w_TIPLIQ
    L_DATPRE=this.oParentObject.w_VPDATPRE
    L_DESABI=this.w_DESABI
    L_DESFIL=this.w_DESFIL
    L_INDIRI=this.w_INDIRI
    L_CAP=this.w_CAP
    * --- PER NUMERAZIONE PAGINE IN TESTATA
    L_PAGINE=0
    L_PRPARI=this.w_PRPARI
    L_INTLIG=this.w_INTLIG
    L_PREFIS=this.w_PREFIS
    L_INDAZI=this.w_INDAZI
    L_LOCAZI=this.w_LOCAZI
    L_CAPAZI=this.w_CAPAZI
    L_PROAZI=this.w_PROAZI
    L_COFAZI=this.w_COFAZI
    L_PIVAZI=this.w_PIVAZI
    L_ATNUMREG= this.w_PRINUM
    L_ATTIPREG= this.w_PRIREG
    this.w_PAGINE = 0
    * --- Ricalcolo l'attivit� da stampare
    SELECT distinct CODATT as CODATTP, MAX(DESCRI) as DESCRIS, MAX(left(CODATT+space(8),8)) as CODATTS FROM LIQIVA INTO CURSOR LIQIVA_CODATT group by CODATTP 
 wrLC=wrcursor("LIQIVA_CODATT")
    if this.pTipo="B"
      this.w_DATINI = this.oParentObject.w_DATINI
    else
      this.w_DATINI = this.oParentObject.oParentObject.w_DATINI
    endif
    Select LIQIVA_CODATT 
 SCAN FOR NOT EMPTY(CODATTP)
    this.w_CODATTS = LIQIVA_CODATT.CODATTP
    * --- Select from ATT_ALTE
    i_nConn=i_TableProp[this.ATT_ALTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_ALTE_idx,2],.t.,this.ATT_ALTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CAATTIVI,CADESCRI  from "+i_cTable+" ATT_ALTE ";
          +" where CADATATT<="+cp_ToStrODBC(this.w_DATINI)+" AND CACODATT="+cp_ToStrODBC(this.w_CODATTS)+"";
          +" order by CADATATT Desc";
           ,"_Curs_ATT_ALTE")
    else
      select CAATTIVI,CADESCRI from (i_cTable);
       where CADATATT<=this.w_DATINI AND CACODATT=this.w_CODATTS;
       order by CADATATT Desc;
        into cursor _Curs_ATT_ALTE
    endif
    if used('_Curs_ATT_ALTE')
      select _Curs_ATT_ALTE
      locate for 1=1
      do while not(eof())
      Select LIQIVA_CODATT 
 replace CODATTS with Nvl(_Curs_ATT_ALTE.CAATTIVI,Space(8)) 
 replace DESCRIS with Nvl(_Curs_ATT_ALTE.CADESCRI,Space(35)) 
 Select _Curs_ATT_ALTE
      Exit
        select _Curs_ATT_ALTE
        continue
      enddo
      use
    endif
    Select LIQIVA_CODATT
    ENDSCAN
    Select * From LIQIVA left join LIQIVA_CODATT on LIQIVA.CODATT=LIQIVA_CODATT.CODATTP Into Cursor LIQIVA NoFilter
    if Used("LIQIVA_CODATT")
      Select LIQIVA_CODATT 
 USE
    endif
    * --- Report di Stampa
    do case
      case this.pTipo $ "BL"
        * --- Stampa Liquidazione Periodica
        * --- Check per stampare o meno informazioni relative al versamento
        L_VersaSn = IIF( this.oParentObject.w_VPIMPVER>0 , "S" , "N" )
        if this.oParentObject.w_TIPLIQ="R"
          * --- Stampa Riepilogativa (Prima Solo Testo seconda Grafica)
          * --- Ordine = 1 le normali registrazioni Iva.
          Select CODATT, DESCRI, ;
          SUM(IIF(TIPREG="A" AND (FLAGRI<>"P" OR TIPAGR<>"A"), TOTIVD, 0)+; 
 IIF(TIPREG$ "V-C" AND TOTIMP<>0 AND FLAGRI="P" AND PERCOM>0, cp_ROUND(((TOTIMP * PERCOM) / 100), IIF(g_PERVAL=g_CODLIR, 0, 2)), 0)) ; 
 AS IVACRE, ;
          SUM(cp_ROUND(IIF(TIPREG<>"A" , TOTIVD,0),IIF(g_PERVAL=g_CODLIR, 0, 2))) AS IVADEB, 1 AS ORDINE, CODATTS, DESCRIS ;
           From LIQIVA GROUP BY CODATT, DESCRI Into Cursor LIQIVA ORDER BY CODATT NoFilter
          SELECT LIQIVA
          GO TOP
          WRCURSOR("LIQIVA")
          SELECT LIQIVA
          GO TOP
          SCAN FOR NOT EMPTY(NVL(CODATT," "))
          this.w_KEYATT = CODATT
          this.w_ANNO = this.oParentObject.w_VP__ANNO
          this.w_NUMPER = this.oParentObject.w_VPPERIOD
          this.w_IVACRE = IVACRE
          this.w_TOTIVD = 0
          VQ_EXEC("QUERY\GSCG_BLP.VQR",this,"SALDIIVA")
          SELECT SALDIIVA
          GO TOP
          SCAN FOR NVL(TIPREG," ") $ "VACE" AND NVL(NUMREG, 0)<>0 AND NOT EMPTY(NVL(CODIVA, " "))
          if TIPREG<>"A" AND ( (TIPREG="V" AND NVL(SPLPAY,"N")<>"S") OR TIPREG$"CE")
            this.w_TOTIVD = this.w_TOTIVD+ ((NVL(IVASTA,0)+NVL(IVASEG,0)+NVL(IVAIND,0))-(NVL(IVAPRE,0)+NVL(IVAFAD,0)))
          endif
          SELECT SALDIIVA
          ENDSCAN
          SELECT LIQIVA
          REPLACE IVADEB WITH this.w_TOTIVD
          * --- Read from ATTIMAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ATTIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ATPERPRO"+;
              " from "+i_cTable+" ATTIMAST where ";
                  +"ATCODATT = "+cp_ToStrODBC(this.w_KEYATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ATPERPRO;
              from (i_cTable) where;
                  ATCODATT = this.w_KEYATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLPROR = NVL(cp_ToDate(_read_.ATPERPRO),cp_NullValue(_read_.ATPERPRO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_FLPROR="S"
            * --- Read from PRO_RATA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PRO_RATA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRO_RATA_idx,2],.t.,this.PRO_RATA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AIPERPRO"+;
                " from "+i_cTable+" PRO_RATA where ";
                    +"AICODATT = "+cp_ToStrODBC(this.w_KEYATT);
                    +" and AI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_VP__ANNO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AIPERPRO;
                from (i_cTable) where;
                    AICODATT = this.w_KEYATT;
                    and AI__ANNO = this.oParentObject.w_VP__ANNO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PERPRO = NVL(cp_ToDate(_read_.AIPERPRO),cp_NullValue(_read_.AIPERPRO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_PERPRO<>100
              this.w_IVACRE = cp_ROUND((this.w_IVACRE * (this.w_PERPRO)) / 100, g_PERPVL)
              SELECT LIQIVA
              REPLACE IVACRE WITH this.w_IVACRE
            endif
          endif
          SELECT LIQIVA
          ENDSCAN
          Select *, ORDINE AS ORDINE1, 9999999999999.9999-9999999999999.9999 AS TOTIMP,SPACE(35) AS D1,; 
 SPACE(35) AS D2, SPACE(35) AS D3, SPACE(35) AS D4 From LIQIVA Into Cursor __Tmp__ NoFilter
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_VPSTAREG="V"
            if this.oParentObject.w_FLTEST="S"
              Cp_ChPrn("Query\LIQIVAR1.FRX", " ", this)
              this.w_PAGINE = L_PAGINE+this.w_PRPARI
            else
              Cp_ChPrn("Query\LIQIVAR.FRX", " ", this)
              this.w_PAGINE = L_PAGINE+this.w_PRPARI
            endif
          else
            if this.oParentObject.w_FLTEST="S"
              Cp_ChPrn("Query\LIQIVAR1O.FRX", " ", this)
              this.w_PAGINE = L_PAGINE+this.w_PRPARI
            else
              Cp_ChPrn("Query\LIQIVARO.FRX", " ", this)
              this.w_PAGINE = L_PAGINE+this.w_PRPARI
            endif
          endif
          WITH this.oParentObject
          do case
            case this.pTipo="B"
              .w_PAGINE=THIS.w_PAGINE
            case this.pTipo $ "DL"
              .oParentObject.w_PAGINE=THIS.w_PAGINE
            otherwise
          endcase
          ENDWITH
        else
          * --- Inserisco riga vuota con ORDINE = 2 per problemi di stampa. Raggruppamento su Ordine in pagina nuova stampa i totali.
          *     ORDINE = 1 le normali registraizoni Iva.
          *     '{' in TIPREG per avere questa riga in fondo al cursore poich� TIPREG pu� assumere 'Z'
          SELECT * , 1 AS ORDINE FROM LIQIVA INTO CURSOR LIQIVA
          Select *, ORDINE AS ORDINE1, SPACE(35) AS D1, SPACE(35) AS D2, SPACE(35) AS D3, SPACE(35) AS D4 ; 
 From LIQIVA Into Cursor __Tmp__ NoFilter
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Stampa Normale (Prima Solo Testo seconda Grafica)
          if this.oParentObject.w_VPSTAREG="V"
            if this.oParentObject.w_FLTEST="S"
              Cp_ChPrn("QUERY\LIQIVA1.FRX", " ", this)
              this.w_PAGINE = L_PAGINE+this.w_PRPARI
            else
              Cp_ChPrn("QUERY\LIQIVA.FRX", " ", this)
              this.w_PAGINE = L_PAGINE+this.w_PRPARI
            endif
          else
            if this.oParentObject.w_FLTEST="S"
              Cp_ChPrn("QUERY\LIQIVA1O.FRX", " ", this)
              this.w_PAGINE = L_PAGINE+this.w_PRPARI
            else
              Cp_ChPrn("QUERY\LIQIVAO.FRX", " ", this)
              this.w_PAGINE = L_PAGINE+this.w_PRPARI
            endif
          endif
          WITH this.oParentObject
          do case
            case this.pTipo="B"
              .w_PAGINE=this.w_PAGINE
            case this.pTipo $ "DL"
              .oParentObject.w_PAGINE=this.w_PAGINE
            otherwise
              this.w_PAGINE = this.w_PAGINE
          endcase
          ENDWITH
        endif
      case this.pTipo="D" OR this.pTipo="S"
        * --- Stampa Dichiarazione Periodica
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Frontespizio
    * --- Preparo il cursore __TMP__ per stapare su PDF.I dati saranno elaborati nella CP_FFDF
    CREATE CURSOR __TMP__ ;
    (RAGAZI C(40), COFAZI C(16), PIVAZI C(12), ANNO C(4), MESE C(2), TRIMES C(1), ;
    CODVAL C(1), VARIMP C(1), CORTER C(1), DICGRU C(1), DICSOC C(1), ;
    IMPOR1 C(22), CESINT C(22), IMPOR2 C(22), ACQINT C(22), ;
    IMPON3 C(22), IMPOS3 C(22), IMPOR5 C(22), IMPOR6 C(22), ;
    IMPO7D C(22), IMPO7C C(22), IMPO8D C(22), IMPO8C C(22), IMPO9D C(22), IMPO9C C(22), IMP10D C(22), IMP10C C(22), ;
    IMPO11 C(22), IMP12D C(22), IMP12C C(22), IMPO13 C(22), IMPO14 C(22), IMPO15 C(22), IMPO16 C(22), ;
    IMPVER C(22), VEREUR C(1), DATVER C(8), CODAZI C(5), CODCAB C(5), VERNEF C(1), AR74C5 C(1), AR74C4 C(1), ;
    OPMEDI C(1), OPNOIM C(1), ACQBEA C(1), CRERIM C(22), EURO19 C(1), CREUTI C(22), CODCAR C(1), CODFIS C(16))
    * --- Definisco Array bidimensionale da passare alla funzione CP_FFDF per la creazione del file FDF
    DECLARE ARRFDF (8,2)
    * --- Riempio l'Array ARRFDF con i nomi dei campi che nel Modello PDF sono rappresentati come N, N>1, celle distinte.
    ARRFDF (1,1) = "COFAZI"
    ARRFDF (1,2) = 16
    ARRFDF (2,1) = "PIVAZI"
    ARRFDF (2,2) = 12
    ARRFDF (3,1) = "ANNO"
    ARRFDF (3,2) = 4
    ARRFDF (4,1) = "MESE"
    ARRFDF (4,2) = 2
    ARRFDF (5,1) = "DATVER"
    ARRFDF (5,2) = 8
    ARRFDF (6,1) = "CODAZI"
    ARRFDF (6,2) = 5
    ARRFDF (7,1) = "CODCAB"
    ARRFDF (7,2) = 5
    ARRFDF (8,1) = "CODFIS"
    ARRFDF (8,2) = 16
    * --- Memorizzo i dati nel cursore __TMP__
    INSERT INTO __TMP__ VALUES ;
    (g_RAGAZI, this.w_COFAZI, this.w_PIVAZI, this.oParentObject.w_VP__ANNO, IIF(g_TIPDEN="M", RIGHT("00"+ALLTRIM(STR(this.oParentObject.w_VPPERIOD)),2), "  "), ;
    IIF(g_TIPDEN<>"M", ALLTRIM(STR(this.oParentObject.w_VPPERIOD)), " "), ;
    IIF(this.w_IMPVAL=g_CODEUR, "X", " "), IIF(this.oParentObject.w_VPVARIMP="S", "X", " "), IIF(this.oParentObject.w_VPCORTER="S", "X", " "), ;
    IIF(this.oParentObject.w_TIPDIC="G", "X", " "), IIF(this.oParentObject.w_TIPDIC="S", "X", " "), ;
    TRAN(this.oParentObject.w_VPIMPOR1, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPCESINT, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPOR2, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPACQINT, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPON3, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPOS3, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPOR5, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPOR6, "@Z "+v_PV[40]), ;
    TRAN(IIF(this.oParentObject.w_VPIMPOR7>0, this.oParentObject.w_VPIMPOR7, 0), "@Z "+v_PV[40]), ;
    TRAN(IIF(this.oParentObject.w_VPIMPOR7<0, ABS(this.oParentObject.w_VPIMPOR7), 0), "@Z "+v_PV[40]), ;
    TRAN(IIF(this.oParentObject.w_VPIMPOR8>0, this.oParentObject.w_VPIMPOR8, 0), "@Z "+v_PV[40]), ;
    TRAN(IIF(this.oParentObject.w_VPIMPOR8<0, ABS(this.oParentObject.w_VPIMPOR8), 0), "@Z "+v_PV[40]), ;
    TRAN(IIF(this.oParentObject.w_VPIMPOR9>0, this.oParentObject.w_VPIMPOR9, 0), "@Z "+v_PV[40]), ;
    TRAN(IIF(this.oParentObject.w_VPIMPOR9<0, ABS(this.oParentObject.w_VPIMPOR9), 0), "@Z "+v_PV[40]), ;
    TRAN(IIF(this.oParentObject.w_VPIMPO10>0, this.oParentObject.w_VPIMPO10, 0), "@Z "+v_PV[40]), ;
    TRAN(IIF(this.oParentObject.w_VPIMPO10<0, ABS(this.oParentObject.w_VPIMPO10), 0), "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPO11, "@Z "+v_PV[40]), ;
    TRAN(IIF(this.oParentObject.w_VPIMPO12>0, this.oParentObject.w_VPIMPO12, 0), "@Z "+v_PV[40]), ;
    TRAN(IIF(this.oParentObject.w_VPIMPO12<0, ABS(this.oParentObject.w_VPIMPO12), 0), "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPO13, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPO14, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPO15, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPO16, "@Z "+v_PV[40]), ;
    TRAN(this.oParentObject.w_VPIMPVER, "@Z "+v_PV[40]), ;
    IIF(this.oParentObject.w_VPVEREUR="S" AND this.oParentObject.w_VPIMPVER>0, "X", " "), ;
    IIF(this.oParentObject.w_VPIMPVER>0, DTOS(this.oParentObject.w_VPDATVER), SPACE(8)), ;
    IIF(this.oParentObject.w_VPIMPVER>0, this.oParentObject.w_VPCODAZI, SPACE(5)), ;
    IIF(this.oParentObject.w_VPIMPVER>0, this.oParentObject.w_VPCODCAB, SPACE(5)), ;
    IIF(this.oParentObject.w_VPVERNEF="S", "X", " "), IIF(this.oParentObject.w_VPAR74C5="S", "X", " "), IIF(this.oParentObject.w_VPAR74C4="S", "X", " "), ;
    IIF(this.oParentObject.w_VPOPMEDI="S", "X", " "), IIF(this.oParentObject.w_VPOPNOIM="S", "X", " "), IIF(this.oParentObject.w_VPACQBEA="S", "X", " "), ;
    TRAN(this.oParentObject.w_VPCRERIM, "@Z "+v_PV[40]), ;
    IIF(this.oParentObject.w_VPEURO19="S" AND this.oParentObject.w_VPCRERIM>0, "X", " "), ;
    TRAN(this.oParentObject.w_VPCREUTI, "@Z "+v_PV[40]), ;
    this.oParentObject.w_VPCODCAR, this.oParentObject.w_VPCODFIS)
    if this.oParentObject.w_FLTEST="S"
      * --- Stampa in formato solo Testo
      Cp_Chprn("QUERY\DICHIVA1.FRX", " ", this)
    else
      * --- Elabora al stampa in formato Grafico PDF
      if this.oParentObject.w_VISPDF<>"S"
        ah_Msg("Stampa grafica in corso; attendere...",.T.)
      endif
      * --- Funzione di creazione dell' FDF
      result1=cp_ffdf(" ","DICHIVA.PDF",this," ",@ARRFDF)
      * --- Funzione di visualizzazione su PDF
      result2=STAPDF(tempadhoc()+"\DICHIVA1.FDF", IIF(this.oParentObject.w_VISPDF="S", "OPEN", "PRINT"), " ")
      WAIT CLEAR
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiungo in append al cursore __tmp__ i records per il riepilogo...
     
 SELECT __tmp__ 
 WRCURSOR("__tmp__") 
    APPEND BLANK
    REPLACE ORDINE1 WITH 2, DESCRI WITH Ah_MsgFormat("Liquidazione %1",L_PERIODO)
    APPEND BLANK
    REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Totale IVA a debito"), TOTIMP WITH L_VPIMPOR5
    APPEND BLANK
    if L_FLPROR="S"
      REPLACE ORDINE1 WITH 3,; 
 DESCRI WITH Ah_MsgFormat("Totale IVA a credito prorata di detraibilit� (%1%)",ALLTRIM(str(L_PERPRO,6,IIF(L_PERPRO=INT(L_PERPRO),0,2)))),; 
 TOTIMP WITH L_VPIMPOR6
    else
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Totale IVA a credito"), TOTIMP WITH L_VPIMPOR6
    endif
    APPEND BLANK
    if L_VPIMPOR8<0
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Variazione d'imposta a credito"),; 
 TOTIMP WITH ABS(L_VPIMPOR8)
    else
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Variazione d'imposta a debito"), TOTIMP WITH ABS(L_VPIMPOR8)
    endif
    APPEND BLANK
    if L_VPIMPOR9>=0
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("IVA non versata Periodi precedenti"),; 
 TOTIMP WITH ABS(L_VPIMPOR9)
    else
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("IVA in eccesso"), TOTIMP WITH ABS(L_VPIMPOR9)
    endif
    APPEND BLANK
    if L_VPIMPO10>=0
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Debito art. 27 Dpr. 633"),; 
 TOTIMP WITH ABS(L_VPIMPO10)
    else
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Credito IVA precedente"), TOTIMP WITH ABS(L_VPIMPO10)
    endif
    APPEND BLANK
    REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Credito IVA compensabile"), TOTIMP WITH L_VPIMPO11
    APPEND BLANK
    if L_VPIMPO12<0
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("IVA credito del periodo"),; 
 TOTIMP WITH ABS(L_VPIMPO12)
    else
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Iva dovuta del periodo"), TOTIMP WITH ABS(L_VPIMPO12)
    endif
    APPEND BLANK
    REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Crediti speciali"), TOTIMP WITH L_VPIMPO13
    APPEND BLANK
    REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Acconto IVA gi� versato"), TOTIMP WITH L_VPIMPO15
    APPEND BLANK
    if L_VPIMPO12-(L_VPIMPO13+L_VPIMPO15)>=0
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Totale IVA a debito"),; 
 TOTIMP WITH ABS(L_VPIMPO12-(L_VPIMPO13+L_VPIMPO15))
    else
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Totale IVA a credito"), TOTIMP WITH ABS(L_VPIMPO12-(L_VPIMPO13+L_VPIMPO15))
    endif
    if g_TIPDEN<>"M"
      APPEND BLANK
      REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Interessi trimestrali"), TOTIMP WITH L_VPIMPO14
    endif
    if L_DICHGRU1="S"
      this.w_DESCRI = Ah_MsgFormat("Il saldo � trasferito alla societ� controllante")
    else
      if L_VPIMPVER=0
        if (g_TIPDEN="T" AND this.oParentObject.w_VPPERIOD=4) OR (g_TIPDEN="M" AND this.oParentObject.w_VPPERIOD=12)
          this.w_DESCRI = ""
        else
          if (L_VPIMPO12 - (L_VPIMPO13 + L_VPIMPO15) >= 0) AND L_VPIMPO12<>0
            this.w_DESCRI = ah_MsgFormat("Imp. da trasferire a periodo succ.")
          else
            this.w_DESCRI = ""
          endif
        endif
      else
        this.w_DESCRI = Ah_MsgFormat("Importo da versare")
      endif
    endif
    if NOT EMPTY(this.w_DESCRI)
      APPEND BLANK
      REPLACE ORDINE1 WITH 3, DESCRI WITH this.w_DESCRI, TOTIMP WITH L_VPIMPO16
    endif
    APPEND BLANK
    REPLACE ORDINE1 WITH 4, DESCRI WITH Ah_MsgFormat("Credito chiesto a rimborso"), TOTIMP WITH L_VPCRERIM
    APPEND BLANK
    REPLACE ORDINE1 WITH 4, DESCRI WITH Ah_MsgFormat("Credito da utilizzare per comp. mod. F24"), TOTIMP WITH L_VPCREUTI
    APPEND BLANK
    REPLACE ORDINE1 WITH 5, D1 WITH Ah_MsgFormat("Descrizione ABI della banca :"),; 
 D2 WITH IIF(TYPE("L_DESABI")<>"U",L_DESABI," "), D3 WITH Ah_MsgFormat("Cod. ABI:"),; 
 D4 WITH IIF(TYPE("L_ABI")<>"U",L_ABI," ")
    APPEND BLANK
    REPLACE ORDINE1 WITH 5, D1 WITH Ah_MsgFormat("Agenzia :"),; 
 D2 WITH IIF(TYPE("L_DESFIL")<>"U",L_DESFIL," "), D3 WITH Ah_MsgFormat("Cod. CAB:"),; 
 D4 WITH IIF(TYPE("L_CAB")<>"U",L_CAB," ")
    APPEND BLANK
    REPLACE ORDINE1 WITH 5, D1 WITH Ah_MsgFormat("Indirizzo :"),; 
 D2 WITH IIF(TYPE("L_INDIRI")<>"U",L_INDIRI," "), D3 WITH Ah_MsgFormat("CAP:"),; 
 D4 WITH IIF(TYPE("L_CAP")<>"U",L_CAP," ")
    if L_VPIMPVER>0
      APPEND BLANK
      REPLACE ORDINE1 WITH 6, D1 WITH Ah_MsgFormat("Data versamento :"),; 
 D2 WITH IIF(TYPE("L_DATVERS")<>"U",DTOC(L_DATVERS)," "), D3 WITH Ah_MsgFormat("Codice concessione:"),; 
 D4 WITH IIF(TYPE("L_CONCES")<>"U",L_CONCES," ")
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ATTIMAST'
    this.cWorkTables[2]='PRO_RATA'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='ATT_ALTE'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_ATT_ALTE')
      use in _Curs_ATT_ALTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
