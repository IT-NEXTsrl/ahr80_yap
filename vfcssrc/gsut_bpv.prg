* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bpv                                                        *
*              Blocca / sblocca utenti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-15                                                      *
* Last revis.: 2013-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bpv",oParentObject,m.pOper)
return(i_retval)

define class tgsut_bpv as StdBatch
  * --- Local variables
  pOper = space(10)
  w_CODUTE = 0
  w_DATA = ctod("  /  /  ")
  w_PROG = .NULL.
  w_PADRE = .NULL.
  w_DATSCA = ctod("  /  /  ")
  w_ZCODUTE = 0
  w_ZCODUTE1 = 0
  w_ISADMIN = .f.
  * --- WorkFile variables
  POL_UTE_idx=0
  TMPSALDI_idx=0
  CPUSRGRP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Blocca/Sblocca Utenti (lanciato da GSUT_KPV)
    this.w_DATA = Date()
    do case
      case this.pOper = "BLOCCA" Or this.pOper = "SBLOCCA"
        * --- Blocco gli utenti selezionati
         
 Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Calculate CNT() For XCHK=1 To w_count
        if w_count=0
          AH_ERRORMSG("Selezionare almeno una riga",48)
        else
           
 Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Go Top
          Scan For Xchk=1
          this.w_CODUTE = PUCODUTE
          if this.pOper = "BLOCCA"
            * --- Write into POL_UTE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.POL_UTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PUFLGBLO ="+cp_NullLink(cp_ToStrODBC("S"),'POL_UTE','PUFLGBLO');
              +",PUMOTBLO ="+cp_NullLink(cp_ToStrODBC("B"),'POL_UTE','PUMOTBLO');
                  +i_ccchkf ;
              +" where ";
                  +"PUCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                     )
            else
              update (i_cTable) set;
                  PUFLGBLO = "S";
                  ,PUMOTBLO = "B";
                  &i_ccchkf. ;
               where;
                  PUCODUTE = this.w_CODUTE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            if TRIM(PUMOTBLO) = "Blocco Manuale"
              * --- Write into POL_UTE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.POL_UTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PUFLGBLO ="+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUFLGBLO');
                +",PUMOTBLO ="+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUMOTBLO');
                +",PU_LOGIN ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'POL_UTE','PU_LOGIN');
                +",PU___MAC ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'POL_UTE','PU___MAC');
                    +i_ccchkf ;
                +" where ";
                    +"PUCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                       )
              else
                update (i_cTable) set;
                    PUFLGBLO = "N";
                    ,PUMOTBLO = "N";
                    ,PU_LOGIN = SPACE(20);
                    ,PU___MAC = SPACE(20);
                    &i_ccchkf. ;
                 where;
                    PUCODUTE = this.w_CODUTE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              if TRIM(PUMOTBLO) = "Scaduta Password"
                * --- Write into POL_UTE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.POL_UTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PUFLGBLO ="+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUFLGBLO');
                  +",PUMOTBLO ="+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUMOTBLO');
                  +",PUDATULT ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'POL_UTE','PUDATULT');
                  +",PUDATACC ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'POL_UTE','PUDATACC');
                  +",PU_LOGIN ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'POL_UTE','PU_LOGIN');
                  +",PU___MAC ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'POL_UTE','PU___MAC');
                  +",PUCHGPWD ="+cp_NullLink(cp_ToStrODBC("S"),'POL_UTE','PUCHGPWD');
                      +i_ccchkf ;
                  +" where ";
                      +"PUCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                         )
                else
                  update (i_cTable) set;
                      PUFLGBLO = "N";
                      ,PUMOTBLO = "N";
                      ,PUDATULT = this.w_DATA;
                      ,PUDATACC = this.w_DATA;
                      ,PU_LOGIN = SPACE(20);
                      ,PU___MAC = SPACE(20);
                      ,PUCHGPWD = "S";
                      &i_ccchkf. ;
                   where;
                      PUCODUTE = this.w_CODUTE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              else
                * --- Write into POL_UTE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.POL_UTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PUFLGBLO ="+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUFLGBLO');
                  +",PUMOTBLO ="+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUMOTBLO');
                  +",PUDATULT ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'POL_UTE','PUDATULT');
                  +",PUDATACC ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'POL_UTE','PUDATACC');
                  +",PU_LOGIN ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'POL_UTE','PU_LOGIN');
                  +",PU___MAC ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'POL_UTE','PU___MAC');
                      +i_ccchkf ;
                  +" where ";
                      +"PUCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                         )
                else
                  update (i_cTable) set;
                      PUFLGBLO = "N";
                      ,PUMOTBLO = "N";
                      ,PUDATULT = this.w_DATA;
                      ,PUDATACC = this.w_DATA;
                      ,PU_LOGIN = SPACE(20);
                      ,PU___MAC = SPACE(20);
                      &i_ccchkf. ;
                   where;
                      PUCODUTE = this.w_CODUTE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
          endif
          Select ( this.oParentObject.w_ZOOM.cCursor ) 
          EndScan
          * --- Refresh dello zoom
          this.oParentObject.notifyEvent("Zoom")
        endif
      case this.pOper = "SELEZ"
        * --- Seleziono/Deseleziono utenti zoom
        UPDATE ( this.oParentObject.w_ZOOM.cCursor ) SET XCHK = IIF(this.oParentObject.w_SELEZ="S", 1, 0)
      case this.pOper = "DETTAGLI"
        if Nvl( this.oParentObject.w_ZOOM.GetVar("PUCODUTE") , 0 )<>0
          * --- Lancia da men� gestione politiche utenti
          this.w_PROG = GSUT_APU()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PROG.bSec1)
            i_retcode = 'stop'
            return
          endif
          * --- inizializzo la chiave Primaria
          this.w_PROG.ecpFilter()     
          this.w_PROG.w_PUCODUTE = Nvl( this.oParentObject.w_ZOOM.GetVar("PUCODUTE") , 0 )
          this.w_PROG.ecpSave()     
          this.w_PROG.ecpQuery()     
        endif
      case this.pOper = "ZOOM"
        this.w_ISADMIN = .T.
        this.w_PADRE = This.oParentObject
        if this.oParentObject.w_VALMAX<>0
          * --- Causa problema calcolo data scadenze come somma di  data ultima
          *     modifica password + giorni scadenza.
          * --- Create temporary table TMPSALDI
          i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSUT_KPV',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPSALDI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Select from TMPSALDI
          i_nConn=i_TableProp[this.TMPSALDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSALDI_idx,2],.t.,this.TMPSALDI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select PUCODUTE, PUDATULT   from "+i_cTable+" TMPSALDI ";
                 ,"_Curs_TMPSALDI")
          else
            select PUCODUTE, PUDATULT  from (i_cTable);
              into cursor _Curs_TMPSALDI
          endif
          if used('_Curs_TMPSALDI')
            select _Curs_TMPSALDI
            locate for 1=1
            do while not(eof())
            * --- Se data modifica pwd vuota non aggiungo nulla alla data scadenza
            *     Rimane vuota...
            this.w_ISADMIN = .T.
            if Not Empty (Nvl( _Curs_TMPSALDI.PUDATULT ,cp_CharToDate("  /  /    ") ))
              this.w_ZCODUTE = _Curs_TMPSALDI.PUCODUTE
              this.w_ZCODUTE1 = this.w_ZCODUTE
              if this.oParentObject.w_FLGAM="N"
                * --- Read from CPUSRGRP
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CPUSRGRP_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CPUSRGRP_idx,2],.t.,this.CPUSRGRP_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "usercode"+;
                    " from "+i_cTable+" CPUSRGRP where ";
                        +"groupcode = "+cp_ToStrODBC(1);
                        +" and usercode = "+cp_ToStrODBC(this.w_ZCODUTE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    usercode;
                    from (i_cTable) where;
                        groupcode = 1;
                        and usercode = this.w_ZCODUTE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_ZCODUTE1 = NVL(cp_ToDate(_read_.usercode),cp_NullValue(_read_.usercode))
                  use
                  if i_Rows=0
                    this.w_ISADMIN = .F.
                  endif
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_ISADMIN
                  this.w_DATSCA = Cp_Todate(_Curs_TMPSALDI.PUDATULT)+ this.oParentObject.w_VALAM
                else
                  this.w_DATSCA = Cp_Todate(_Curs_TMPSALDI.PUDATULT)+ this.oParentObject.w_VALMAX
                endif
              else
                this.w_DATSCA = Cp_Todate(_Curs_TMPSALDI.PUDATULT)+ this.oParentObject.w_VALMAX
              endif
              * --- Write into TMPSALDI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPSALDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPSALDI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPSALDI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PUDATSCA ="+cp_NullLink(cp_ToStrODBC(this.w_DATSCA),'TMPSALDI','PUDATSCA');
                    +i_ccchkf ;
                +" where ";
                    +"PUCODUTE = "+cp_ToStrODBC(this.w_ZCODUTE);
                       )
              else
                update (i_cTable) set;
                    PUDATSCA = this.w_DATSCA;
                    &i_ccchkf. ;
                 where;
                    PUCODUTE = this.w_ZCODUTE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
              select _Curs_TMPSALDI
              continue
            enddo
            use
          endif
          this.oParentObject.w_ZOOM.cCpQueryName = "QUERY\GSUT1KPV"
        else
          this.oParentObject.w_ZOOM.cCpQueryName = "QUERY\GSUT_KPV"
        endif
        this.w_PADRE.notifyEvent("Aggiorna")     
      case this.pOper = "DONE"
        if this.oParentObject.w_VALMAX<>0
          * --- Drop temporary table TMPSALDI
          i_nIdx=cp_GetTableDefIdx('TMPSALDI')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPSALDI')
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='POL_UTE'
    this.cWorkTables[2]='*TMPSALDI'
    this.cWorkTables[3]='CPUSRGRP'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_TMPSALDI')
      use in _Curs_TMPSALDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
