* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_ksk                                                        *
*              Sposta confezioni                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_48]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-23                                                      *
* Last revis.: 2010-01-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_ksk",oParentObject))

* --- Class definition
define class tgsma_ksk as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 805
  Height = 333
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-01-19"
  HelpContextID=169182359
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  TIP_COLL_IDX = 0
  cPrg = "gsma_ksk"
  cComment = "Sposta confezioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NUMCOL1 = 0
  w_CODCOL1 = space(5)
  w_DESCOL1 = space(35)
  w_NUMCOL2 = 0
  o_NUMCOL2 = 0
  w_CODCOL2 = space(5)
  w_DESCOL2 = space(35)
  w_ADDCOL = space(1)
  w_SELEZI = space(1)
  w_VOLCOL2 = 0
  w_UMVCOL2 = space(3)
  w_TARCOL2 = 0
  w_COLMIX2 = space(1)
  w_FLESIS = space(10)
  w_TESTERR = .F.
  w_ZoomConf = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kskPag1","gsma_ksk",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNUMCOL2_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomConf = this.oPgFrm.Pages(1).oPag.ZoomConf
    DoDefault()
    proc Destroy()
      this.w_ZoomConf = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TIP_COLL'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NUMCOL1=0
      .w_CODCOL1=space(5)
      .w_DESCOL1=space(35)
      .w_NUMCOL2=0
      .w_CODCOL2=space(5)
      .w_DESCOL2=space(35)
      .w_ADDCOL=space(1)
      .w_SELEZI=space(1)
      .w_VOLCOL2=0
      .w_UMVCOL2=space(3)
      .w_TARCOL2=0
      .w_COLMIX2=space(1)
      .w_FLESIS=space(10)
      .w_TESTERR=.f.
        .w_NUMCOL1 = this.oParentObject.oParentObject .w_CPROWORD
        .w_CODCOL1 = this.oParentObject.oParentObject .w_PLTIPCOL
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCOL1))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_CODCOL2 = IIF(.w_NUMCOL2=0, SPACE(5), .w_CODCOL2)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODCOL2))
          .link_1_7('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_ADDCOL = ' '
      .oPgFrm.Page1.oPag.ZoomConf.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
          .DoRTCalc(9,12,.f.)
        .w_FLESIS = ' '
        .w_TESTERR = .T.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_NUMCOL1 = this.oParentObject.oParentObject .w_CPROWORD
            .w_CODCOL1 = this.oParentObject.oParentObject .w_PLTIPCOL
          .link_1_3('Full')
        .DoRTCalc(3,4,.t.)
        if .o_NUMCOL2<>.w_NUMCOL2
            .w_CODCOL2 = IIF(.w_NUMCOL2=0, SPACE(5), .w_CODCOL2)
          .link_1_7('Full')
        endif
        .oPgFrm.Page1.oPag.ZoomConf.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomConf.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCOL2_1_7.enabled = this.oPgFrm.Page1.oPag.oCODCOL2_1_7.mCond()
    this.oPgFrm.Page1.oPag.oSELEZI_1_16.enabled_(this.oPgFrm.Page1.oPag.oSELEZI_1_16.mCond())
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomConf.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCOL1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCOL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCOL1)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOL1 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOL1 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOL1 = space(5)
      endif
      this.w_DESCOL1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOL2
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CODCOL2)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA,TCFL_MIX";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CODCOL2))
          select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA,TCFL_MIX;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOL2)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOL2) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oCODCOL2_1_7'),i_cWhere,'GSAR_MTO',"Tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA,TCFL_MIX";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA,TCFL_MIX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA,TCFL_MIX";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCOL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCOL2)
            select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA,TCFL_MIX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOL2 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOL2 = NVL(_Link_.TCDESCRI,space(35))
      this.w_VOLCOL2 = NVL(_Link_.TCVOLUME,0)
      this.w_UMVCOL2 = NVL(_Link_.TCUMVOLU,space(3))
      this.w_TARCOL2 = NVL(_Link_.TC__TARA,0)
      this.w_COLMIX2 = NVL(_Link_.TCFL_MIX,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOL2 = space(5)
      endif
      this.w_DESCOL2 = space(35)
      this.w_VOLCOL2 = 0
      this.w_UMVCOL2 = space(3)
      this.w_TARCOL2 = 0
      this.w_COLMIX2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMCOL1_1_1.value==this.w_NUMCOL1)
      this.oPgFrm.Page1.oPag.oNUMCOL1_1_1.value=this.w_NUMCOL1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOL1_1_3.value==this.w_CODCOL1)
      this.oPgFrm.Page1.oPag.oCODCOL1_1_3.value=this.w_CODCOL1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOL1_1_4.value==this.w_DESCOL1)
      this.oPgFrm.Page1.oPag.oDESCOL1_1_4.value=this.w_DESCOL1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMCOL2_1_6.value==this.w_NUMCOL2)
      this.oPgFrm.Page1.oPag.oNUMCOL2_1_6.value=this.w_NUMCOL2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOL2_1_7.value==this.w_CODCOL2)
      this.oPgFrm.Page1.oPag.oCODCOL2_1_7.value=this.w_CODCOL2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOL2_1_8.value==this.w_DESCOL2)
      this.oPgFrm.Page1.oPag.oDESCOL2_1_8.value=this.w_DESCOL2
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_16.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_16.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_NUMCOL2)) or not(.w_NUMCOL2<>.w_NUMCOL1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMCOL2_1_6.SetFocus()
            i_bnoObbl = !empty(.w_NUMCOL2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Collo destinazione inesistente o uguale al collo di origine")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsma_ksk
      * --- Controlli Finali
      if i_bRes = .t.
        this.w_TESTERR=.T.
        this.NotifyEvent('ControlliFinali')
        return (this.w_TESTERR)
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NUMCOL2 = this.w_NUMCOL2
    return

enddefine

* --- Define pages as container
define class tgsma_kskPag1 as StdContainer
  Width  = 801
  height = 333
  stdWidth  = 801
  stdheight = 333
  resizeXpos=640
  resizeYpos=224
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMCOL1_1_1 as StdField with uid="SCQTYYPCJG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NUMCOL1", cQueryName = "NUMCOL1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "N.progressivo di riga del codice collo di origine",;
    HelpContextID = 78795050,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=96, Top=9

  add object oCODCOL1_1_3 as StdField with uid="ZWCMYELRJJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCOL1", cQueryName = "CODCOL1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice collo di origine",;
    HelpContextID = 78833626,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=168, Top=9, InputMask=replicate('X',5), cLinkFile="TIP_COLL", oKey_1_1="TCCODICE", oKey_1_2="this.w_CODCOL1"

  func oCODCOL1_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCOL1_1_4 as StdField with uid="OHJSYVVTFX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCOL1", cQueryName = "DESCOL1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 78774730,;
   bGlobalFont=.t.,;
    Height=21, Width=349, Left=227, Top=9, InputMask=replicate('X',35)

  add object oNUMCOL2_1_6 as StdField with uid="MTECRHWBZZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NUMCOL2", cQueryName = "NUMCOL2",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Collo destinazione inesistente o uguale al collo di origine",;
    ToolTipText = "N.progressivo di riga del codice collo di destinazione",;
    HelpContextID = 78795050,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=96, Top=38, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. 

  func oNUMCOL2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMCOL2<>.w_NUMCOL1)
    endwith
    return bRes
  endfunc

  proc oNUMCOL2_1_6.mZoom
      with this.Parent.oContained
        GSMA_BSK(this.Parent.oContained,"ZOOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCODCOL2_1_7 as StdField with uid="LDDXFGGZML",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODCOL2", cQueryName = "CODCOL2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Collo di destinazione inesistente",;
    ToolTipText = "Codice collo di destinazione",;
    HelpContextID = 78833626,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=168, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_CODCOL2"

  func oCODCOL2_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ADDCOL='S')
    endwith
   endif
  endfunc

  func oCODCOL2_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOL2_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOL2_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oCODCOL2_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tipologie colli",'',this.parent.oContained
  endproc
  proc oCODCOL2_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CODCOL2
     i_obj.ecpSave()
  endproc

  add object oDESCOL2_1_8 as StdField with uid="OAKVRCOGYX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCOL2", cQueryName = "DESCOL2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione colli di destinazione",;
    HelpContextID = 78774730,;
   bGlobalFont=.t.,;
    Height=21, Width=349, Left=227, Top=38, InputMask=replicate('X',35)


  add object ZoomConf as cp_szoombox with uid="GOPJREOSJR",left=4, top=79, width=802,height=204,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSMA_KSK",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,cTable="CON_COLL",cZoomOnZoom="",cMenuFile="",;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 189690906


  add object oBtn_1_11 as StdButton with uid="AKOERXBELZ",left=692, top=286, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 169211110;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="VAYMUUHAYT",left=745, top=286, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176499782;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_13 as cp_runprogram with uid="PWUVEHENLH",left=218, top=378, width=132,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMA_BSK('BLANK')",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 189690906


  add object oObj_1_14 as cp_runprogram with uid="YDBEEGVSKP",left=19, top=377, width=194,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMA_BSK('NUMCOL')",;
    cEvent = "w_NUMCOL2 Changed",;
    nPag=1;
    , HelpContextID = 189690906


  add object oObj_1_15 as cp_runprogram with uid="YHDJKTNFOV",left=21, top=402, width=194,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMA_BSK('AGGIORNA')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 189690906

  add object oSELEZI_1_16 as StdRadio with uid="VNICEXLLTR",rtseq=8,rtrep=.f.,left=13, top=287, width=241,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le confezioni del collo";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_16.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 150966054
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 150966054
      this.Buttons(2).Top=15
      this.SetAll("Width",239)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le confezioni del collo")
      StdRadio::init()
    endproc

  func oSELEZI_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_16.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_16.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOL2<>0 AND NOT EMPTY(.w_CODCOL2))
    endwith
   endif
  endfunc


  add object oObj_1_17 as cp_runprogram with uid="FHFZMDWAGY",left=362, top=378, width=183,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMA_BSK('SELEZ')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 189690906


  add object oObj_1_18 as cp_runprogram with uid="OOKNFUPYOH",left=219, top=401, width=257,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMA_BSK('UNCHK')",;
    cEvent = "ZOOMCONF row unchecked",;
    nPag=1;
    , HelpContextID = 189690906


  add object oObj_1_19 as cp_runprogram with uid="SNFOGKYDRY",left=218, top=422, width=265,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMA_BSK('CHECK')",;
    cEvent = "ZOOMCONF row checked",;
    nPag=1;
    , HelpContextID = 189690906

  add object oStr_1_2 as StdString with uid="THJKEHPEAV",Visible=.t., Left=11, Top=9,;
    Alignment=1, Width=80, Height=18,;
    Caption="Numero collo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="SHASVNEMVZ",Visible=.t., Left=12, Top=38,;
    Alignment=1, Width=80, Height=18,;
    Caption="Sposta in:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_ksk','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
