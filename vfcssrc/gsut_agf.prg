* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_agf                                                        *
*              Gestione file                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_96]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-04                                                      *
* Last revis.: 2008-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_agf"))

* --- Class definition
define class tgsut_agf as StdForm
  Top    = 10
  Left   = 52

  * --- Standard Properties
  Width  = 668
  Height = 404+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-16"
  HelpContextID=42550121
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=54

  * --- Constant Properties
  GESTFILE_IDX = 0
  TIP_ALLE_IDX = 0
  CLA_ALLE_IDX = 0
  CONTI_IDX = 0
  DOC_DETT_IDX = 0
  PNT_DETT_IDX = 0
  ART_ICOL_IDX = 0
  DOC_MAST_IDX = 0
  PNT_MAST_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  cFile = "GESTFILE"
  cKeySelect = "GFSERIAL"
  cKeyWhere  = "GFSERIAL=this.w_GFSERIAL"
  cKeyWhereODBC = '"GFSERIAL="+cp_ToStrODBC(this.w_GFSERIAL)';

  cKeyWhereODBCqualified = '"GESTFILE.GFSERIAL="+cp_ToStrODBC(this.w_GFSERIAL)';

  cPrg = "gsut_agf"
  cComment = "Gestione file"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GFSERIAL = space(10)
  w_GFDATREG = ctod('  /  /  ')
  w_GFTIPALL = space(5)
  o_GFTIPALL = space(5)
  w_GFCLAALL = space(5)
  o_GFCLAALL = space(5)
  w_GFDESCRI = space(40)
  w_GFMODALL = space(1)
  o_GFMODALL = space(1)
  w_GF__NOTE = space(0)
  w_GFDATINI = ctod('  /  /  ')
  w_GFDATOBS = ctod('  /  /  ')
  w_GFFLPRIN = space(1)
  w_GFPUBWEB = space(1)
  w_GF__PATH = space(250)
  w_DESCLA = space(40)
  w_DESTIP = space(40)
  w_GFCODPAD = space(1)
  w_GFTIPCON = space(1)
  w_GFCODCON = space(15)
  w_GFSERDOC = space(10)
  w_GFSERPNT = space(10)
  w_GFCODART = space(20)
  w_DESCON = space(40)
  w_TIPDOC = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod('  /  /  ')
  w_CODCAU = space(5)
  w_NUMRER = 0
  w_CODUTE = 0
  w_DATREG = ctod('  /  /  ')
  w_CODAZI = space(5)
  w_NOMFIL = space(254)
  w_POSIZ = 0
  w_NFILONLY = space(254)
  w_GF__FILE = space(254)
  w_FILE = space(20)
  w_COLLEG = space(254)
  w_COPIA = space(254)
  w_estensione = space(5)
  w_numfile = 0
  w_FL_TIP = space(1)
  w_FL_CLA = space(1)
  w_PERIODO = space(200)
  w_CARTLIB = space(200)
  w_FLUNIC = space(1)
  w_GFCODCAT = space(5)
  w_GFCODMAR = space(5)
  w_GFCODFAM = space(5)
  w_GFCODGRU = space(5)
  w_DESART = space(40)
  w_DESCAT = space(40)
  w_DESMAR = space(40)
  w_DESFAM = space(40)
  w_DESMER = space(40)
  w_SCANNER = .F.

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_GFSERIAL = this.W_GFSERIAL
  * --- Area Manuale = Declare Variables
  * --- gsut_agf
  proc ecpQuit()
      local i_oldfunc
      i_oldfunc=this.cFunction
      this.bUpdated=.t.
      this.NotifyEvent('Edit Aborted')
      * --- si sposta senza attivare i controlli
      this.cFunction='Filter'
      local i_err
      i_err=on('error')
      on error =.t.
      this.activecontrol.parent.oContained.bDontReportError=.t.
      on error &i_err
      this.__dummy__.enabled=.t.
      this.__dummy__.Setfocus()
      * ---
      do case
        case i_oldfunc="Query" Or i_oldfunc="Load" Or i_oldfunc="Edit"
          this.NotifyEvent("Done")
          this.Hide()
          this.Release()
        otherwise
          this.ecpQuery()
      endcase
      * -- Per cancellare il file nella Temp nel caso di acquisizione da Scanner
      If this.w_SCANNER
         Erase(this.w_COLLEG)
      Endif
      return
  endproc
  
  proc ecpSave()
     DoDefault()
     * -- Se c'� stato un errore non chiudo la maschera.
     * -- In GSUT_BSA controllo ad esempio se ci sono altri allegati principali
     * -- e in tal caso notifico errore nella transazione. Non devo chiudere la gestione
     If Not bTrsErr
       this.ecpQuit()
     Endif
  endproc
  
  proc QueryUnload()
     this.ecpQuit()
  Endproc
  
  proc ecpEdit()
        * ----
        DoDefault()
        this.oPgFrm.Pages(this.oPgFrm.PageCount).caption=''
        this.oPgFrm.Pages(this.oPgFrm.PageCount).enabled=.f.
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GESTFILE','gsut_agf')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_agfPag1","gsut_agf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Allegato")
      .Pages(1).HelpContextID = 89320821
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGFSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='TIP_ALLE'
    this.cWorkTables[2]='CLA_ALLE'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='DOC_DETT'
    this.cWorkTables[5]='PNT_DETT'
    this.cWorkTables[6]='ART_ICOL'
    this.cWorkTables[7]='DOC_MAST'
    this.cWorkTables[8]='PNT_MAST'
    this.cWorkTables[9]='CATEGOMO'
    this.cWorkTables[10]='MARCHI'
    this.cWorkTables[11]='FAM_ARTI'
    this.cWorkTables[12]='GRUMERC'
    this.cWorkTables[13]='GESTFILE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(13))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GESTFILE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GESTFILE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GFSERIAL = NVL(GFSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_1_32_joined
    link_1_32_joined=.f.
    local link_1_33_joined
    link_1_33_joined=.f.
    local link_1_34_joined
    link_1_34_joined=.f.
    local link_1_70_joined
    link_1_70_joined=.f.
    local link_1_71_joined
    link_1_71_joined=.f.
    local link_1_72_joined
    link_1_72_joined=.f.
    local link_1_73_joined
    link_1_73_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GESTFILE where GFSERIAL=KeySet.GFSERIAL
    *
    i_nConn = i_TableProp[this.GESTFILE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESTFILE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GESTFILE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GESTFILE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GESTFILE '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_32_joined=this.AddJoinedLink_1_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_33_joined=this.AddJoinedLink_1_33(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_34_joined=this.AddJoinedLink_1_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_70_joined=this.AddJoinedLink_1_70(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_71_joined=this.AddJoinedLink_1_71(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_72_joined=this.AddJoinedLink_1_72(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_73_joined=this.AddJoinedLink_1_73(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GFSERIAL',this.w_GFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCLA = space(40)
        .w_DESTIP = space(40)
        .w_DESCON = space(40)
        .w_TIPDOC = space(5)
        .w_NUMDOC = 0
        .w_ALFDOC = space(10)
        .w_DATDOC = ctod("  /  /  ")
        .w_CODCAU = space(5)
        .w_NUMRER = 0
        .w_CODUTE = 0
        .w_DATREG = ctod("  /  /  ")
        .w_NOMFIL = space(254)
        .w_FILE = space(20)
        .w_COLLEG = space(254)
        .w_COPIA = space(254)
        .w_estensione = space(5)
        .w_numfile = 0
        .w_FL_TIP = space(1)
        .w_FL_CLA = space(1)
        .w_PERIODO = space(200)
        .w_CARTLIB = space(200)
        .w_FLUNIC = space(1)
        .w_DESART = space(40)
        .w_DESCAT = space(40)
        .w_DESMAR = space(40)
        .w_DESFAM = space(40)
        .w_DESMER = space(40)
        .w_SCANNER = .f.
        .w_GFSERIAL = NVL(GFSERIAL,space(10))
        .op_GFSERIAL = .w_GFSERIAL
        .w_GFDATREG = NVL(cp_ToDate(GFDATREG),ctod("  /  /  "))
        .w_GFTIPALL = NVL(GFTIPALL,space(5))
          if link_1_3_joined
            this.w_GFTIPALL = NVL(TACODICE103,NVL(this.w_GFTIPALL,space(5)))
            this.w_DESTIP = NVL(TADESCRI103,space(40))
          else
          .link_1_3('Load')
          endif
        .w_GFCLAALL = NVL(GFCLAALL,space(5))
          if link_1_5_joined
            this.w_GFCLAALL = NVL(TACODCLA105,NVL(this.w_GFCLAALL,space(5)))
            this.w_DESCLA = NVL(TACLADES105,space(40))
            this.w_FLUNIC = NVL(TAFLUNIC105,space(1))
            this.w_GFPUBWEB = NVL(TDPUBWEB105,space(1))
          else
          .link_1_5('Load')
          endif
        .w_GFDESCRI = NVL(GFDESCRI,space(40))
        .w_GFMODALL = NVL(GFMODALL,space(1))
        .w_GF__NOTE = NVL(GF__NOTE,space(0))
        .w_GFDATINI = NVL(cp_ToDate(GFDATINI),ctod("  /  /  "))
        .w_GFDATOBS = NVL(cp_ToDate(GFDATOBS),ctod("  /  /  "))
        .w_GFFLPRIN = NVL(GFFLPRIN,space(1))
        .w_GFPUBWEB = NVL(GFPUBWEB,space(1))
        .w_GF__PATH = NVL(GF__PATH,space(250))
        .w_GFCODPAD = NVL(GFCODPAD,space(1))
        .w_GFTIPCON = NVL(GFTIPCON,space(1))
        .w_GFCODCON = NVL(GFCODCON,space(15))
          if link_1_30_joined
            this.w_GFCODCON = NVL(ANCODICE130,NVL(this.w_GFCODCON,space(15)))
            this.w_DESCON = NVL(ANDESCRI130,space(40))
          else
          .link_1_30('Load')
          endif
        .w_GFSERDOC = NVL(GFSERDOC,space(10))
          if link_1_32_joined
            this.w_GFSERDOC = NVL(MVSERIAL132,NVL(this.w_GFSERDOC,space(10)))
            this.w_TIPDOC = NVL(MVTIPDOC132,space(5))
            this.w_NUMDOC = NVL(MVNUMDOC132,0)
            this.w_ALFDOC = NVL(MVALFDOC132,space(10))
            this.w_DATDOC = NVL(cp_ToDate(MVDATDOC132),ctod("  /  /  "))
          else
          .link_1_32('Load')
          endif
        .w_GFSERPNT = NVL(GFSERPNT,space(10))
          if link_1_33_joined
            this.w_GFSERPNT = NVL(PNSERIAL133,NVL(this.w_GFSERPNT,space(10)))
            this.w_CODCAU = NVL(PNCODCAU133,space(5))
            this.w_NUMRER = NVL(PNNUMRER133,0)
            this.w_CODUTE = NVL(PNCODUTE133,0)
            this.w_DATREG = NVL(cp_ToDate(PNDATREG133),ctod("  /  /  "))
          else
          .link_1_33('Load')
          endif
        .w_GFCODART = NVL(GFCODART,space(20))
          if link_1_34_joined
            this.w_GFCODART = NVL(ARCODART134,NVL(this.w_GFCODART,space(20)))
            this.w_DESART = NVL(ARDESART134,space(40))
          else
          .link_1_34('Load')
          endif
        .w_CODAZI = i_CODAZI
        .w_POSIZ = rat('\',.w_NOMFIL)
        .w_NFILONLY = substr(.w_NOMFIL,.w_POSIZ+1,50)
        .w_GF__FILE = NVL(GF__FILE,space(254))
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .w_GFCODCAT = NVL(GFCODCAT,space(5))
          if link_1_70_joined
            this.w_GFCODCAT = NVL(OMCODICE170,NVL(this.w_GFCODCAT,space(5)))
            this.w_DESCAT = NVL(OMDESCRI170,space(40))
          else
          .link_1_70('Load')
          endif
        .w_GFCODMAR = NVL(GFCODMAR,space(5))
          if link_1_71_joined
            this.w_GFCODMAR = NVL(MACODICE171,NVL(this.w_GFCODMAR,space(5)))
            this.w_DESMAR = NVL(MADESCRI171,space(40))
          else
          .link_1_71('Load')
          endif
        .w_GFCODFAM = NVL(GFCODFAM,space(5))
          if link_1_72_joined
            this.w_GFCODFAM = NVL(FACODICE172,NVL(this.w_GFCODFAM,space(5)))
            this.w_DESFAM = NVL(FADESCRI172,space(40))
          else
          .link_1_72('Load')
          endif
        .w_GFCODGRU = NVL(GFCODGRU,space(5))
          if link_1_73_joined
            this.w_GFCODGRU = NVL(GMCODICE173,NVL(this.w_GFCODGRU,space(5)))
            this.w_DESMER = NVL(GMDESCRI173,space(40))
          else
          .link_1_73('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'GESTFILE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GFSERIAL = space(10)
      .w_GFDATREG = ctod("  /  /  ")
      .w_GFTIPALL = space(5)
      .w_GFCLAALL = space(5)
      .w_GFDESCRI = space(40)
      .w_GFMODALL = space(1)
      .w_GF__NOTE = space(0)
      .w_GFDATINI = ctod("  /  /  ")
      .w_GFDATOBS = ctod("  /  /  ")
      .w_GFFLPRIN = space(1)
      .w_GFPUBWEB = space(1)
      .w_GF__PATH = space(250)
      .w_DESCLA = space(40)
      .w_DESTIP = space(40)
      .w_GFCODPAD = space(1)
      .w_GFTIPCON = space(1)
      .w_GFCODCON = space(15)
      .w_GFSERDOC = space(10)
      .w_GFSERPNT = space(10)
      .w_GFCODART = space(20)
      .w_DESCON = space(40)
      .w_TIPDOC = space(5)
      .w_NUMDOC = 0
      .w_ALFDOC = space(10)
      .w_DATDOC = ctod("  /  /  ")
      .w_CODCAU = space(5)
      .w_NUMRER = 0
      .w_CODUTE = 0
      .w_DATREG = ctod("  /  /  ")
      .w_CODAZI = space(5)
      .w_NOMFIL = space(254)
      .w_POSIZ = 0
      .w_NFILONLY = space(254)
      .w_GF__FILE = space(254)
      .w_FILE = space(20)
      .w_COLLEG = space(254)
      .w_COPIA = space(254)
      .w_estensione = space(5)
      .w_numfile = 0
      .w_FL_TIP = space(1)
      .w_FL_CLA = space(1)
      .w_PERIODO = space(200)
      .w_CARTLIB = space(200)
      .w_FLUNIC = space(1)
      .w_GFCODCAT = space(5)
      .w_GFCODMAR = space(5)
      .w_GFCODFAM = space(5)
      .w_GFCODGRU = space(5)
      .w_DESART = space(40)
      .w_DESCAT = space(40)
      .w_DESMAR = space(40)
      .w_DESFAM = space(40)
      .w_DESMER = space(40)
      .w_SCANNER = .f.
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
          if not(empty(.w_GFTIPALL))
          .link_1_3('Full')
          endif
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_GFCLAALL))
          .link_1_5('Full')
          endif
          .DoRTCalc(5,5,.f.)
        .w_GFMODALL = 'F'
          .DoRTCalc(7,9,.f.)
        .w_GFFLPRIN = ' '
          .DoRTCalc(11,11,.f.)
        .w_GF__PATH = IIF(.w_GFMODALL='F', Alltrim(.w_CARTLIB)+(IIF(.w_FL_TIP='S',Alltrim(.w_GFTIPALL)+'\',''))+(IIF(.w_FL_CLA='S',Alltrim(.w_GFCLAALL)+'\',''))+Alltrim(.w_PERIODO)+'\'+Alltrim(.w_FILE) + Alltrim(.w_ESTENSIONE), .w_COLLEG)
        .DoRTCalc(13,17,.f.)
          if not(empty(.w_GFCODCON))
          .link_1_30('Full')
          endif
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_GFSERDOC))
          .link_1_32('Full')
          endif
        .DoRTCalc(19,19,.f.)
          if not(empty(.w_GFSERPNT))
          .link_1_33('Full')
          endif
        .DoRTCalc(20,20,.f.)
          if not(empty(.w_GFCODART))
          .link_1_34('Full')
          endif
          .DoRTCalc(21,29,.f.)
        .w_CODAZI = i_CODAZI
          .DoRTCalc(31,31,.f.)
        .w_POSIZ = rat('\',.w_NOMFIL)
        .w_NFILONLY = substr(.w_NOMFIL,.w_POSIZ+1,50)
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .DoRTCalc(34,45,.f.)
          if not(empty(.w_GFCODCAT))
          .link_1_70('Full')
          endif
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_GFCODMAR))
          .link_1_71('Full')
          endif
        .DoRTCalc(47,47,.f.)
          if not(empty(.w_GFCODFAM))
          .link_1_72('Full')
          endif
        .DoRTCalc(48,48,.f.)
          if not(empty(.w_GFCODGRU))
          .link_1_73('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'GESTFILE')
    this.DoRTCalc(49,54,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GESTFILE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESTFILE_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"GESTSER","i_codazi,w_GFSERIAL")
      .op_codazi = .w_codazi
      .op_GFSERIAL = .w_GFSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGFSERIAL_1_1.enabled = !i_bVal
      .Page1.oPag.oGFDATREG_1_2.enabled = i_bVal
      .Page1.oPag.oGFTIPALL_1_3.enabled = i_bVal
      .Page1.oPag.oGFCLAALL_1_5.enabled = i_bVal
      .Page1.oPag.oGFDESCRI_1_6.enabled = i_bVal
      .Page1.oPag.oGFMODALL_1_7.enabled = i_bVal
      .Page1.oPag.oGF__NOTE_1_8.enabled = i_bVal
      .Page1.oPag.oGFDATINI_1_9.enabled = i_bVal
      .Page1.oPag.oGFDATOBS_1_10.enabled = i_bVal
      .Page1.oPag.oGFFLPRIN_1_20.enabled = i_bVal
      .Page1.oPag.oGFPUBWEB_1_21.enabled = i_bVal
      .Page1.oPag.oGF__PATH_1_22.enabled = i_bVal
      .Page1.oPag.oGFCODCON_1_30.enabled = i_bVal
      .Page1.oPag.oGFSERDOC_1_32.enabled = i_bVal
      .Page1.oPag.oGFSERPNT_1_33.enabled = i_bVal
      .Page1.oPag.oGFCODART_1_34.enabled = i_bVal
      .Page1.oPag.oGFCODCAT_1_70.enabled = i_bVal
      .Page1.oPag.oGFCODMAR_1_71.enabled = i_bVal
      .Page1.oPag.oGFCODFAM_1_72.enabled = i_bVal
      .Page1.oPag.oGFCODGRU_1_73.enabled = i_bVal
      .Page1.oPag.oBtn_1_11.enabled = .Page1.oPag.oBtn_1_11.mCond()
      .Page1.oPag.oBtn_1_12.enabled = .Page1.oPag.oBtn_1_12.mCond()
      .Page1.oPag.oObj_1_66.enabled = i_bVal
      .Page1.oPag.oObj_1_86.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oGFDATREG_1_2.enabled = .t.
        .Page1.oPag.oGFTIPALL_1_3.enabled = .t.
        .Page1.oPag.oGFCLAALL_1_5.enabled = .t.
        .Page1.oPag.oGFDESCRI_1_6.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'GESTFILE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GESTFILE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFSERIAL,"GFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFDATREG,"GFDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFTIPALL,"GFTIPALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFCLAALL,"GFCLAALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFDESCRI,"GFDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFMODALL,"GFMODALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GF__NOTE,"GF__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFDATINI,"GFDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFDATOBS,"GFDATOBS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFFLPRIN,"GFFLPRIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFPUBWEB,"GFPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GF__PATH,"GF__PATH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFCODPAD,"GFCODPAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFTIPCON,"GFTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFCODCON,"GFCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFSERDOC,"GFSERDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFSERPNT,"GFSERPNT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFCODART,"GFCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GF__FILE,"GF__FILE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFCODCAT,"GFCODCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFCODMAR,"GFCODMAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFCODFAM,"GFCODFAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GFCODGRU,"GFCODGRU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GESTFILE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESTFILE_IDX,2])
    i_lTable = "GESTFILE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GESTFILE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GESTFILE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GESTFILE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GESTFILE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"GESTSER","i_codazi,w_GFSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GESTFILE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GESTFILE')
        i_extval=cp_InsertValODBCExtFlds(this,'GESTFILE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GFSERIAL,GFDATREG,GFTIPALL,GFCLAALL,GFDESCRI"+;
                  ",GFMODALL,GF__NOTE,GFDATINI,GFDATOBS,GFFLPRIN"+;
                  ",GFPUBWEB,GF__PATH,GFCODPAD,GFTIPCON,GFCODCON"+;
                  ",GFSERDOC,GFSERPNT,GFCODART,GF__FILE,GFCODCAT"+;
                  ",GFCODMAR,GFCODFAM,GFCODGRU "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GFSERIAL)+;
                  ","+cp_ToStrODBC(this.w_GFDATREG)+;
                  ","+cp_ToStrODBCNull(this.w_GFTIPALL)+;
                  ","+cp_ToStrODBCNull(this.w_GFCLAALL)+;
                  ","+cp_ToStrODBC(this.w_GFDESCRI)+;
                  ","+cp_ToStrODBC(this.w_GFMODALL)+;
                  ","+cp_ToStrODBC(this.w_GF__NOTE)+;
                  ","+cp_ToStrODBC(this.w_GFDATINI)+;
                  ","+cp_ToStrODBC(this.w_GFDATOBS)+;
                  ","+cp_ToStrODBC(this.w_GFFLPRIN)+;
                  ","+cp_ToStrODBC(this.w_GFPUBWEB)+;
                  ","+cp_ToStrODBC(this.w_GF__PATH)+;
                  ","+cp_ToStrODBC(this.w_GFCODPAD)+;
                  ","+cp_ToStrODBC(this.w_GFTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_GFCODCON)+;
                  ","+cp_ToStrODBCNull(this.w_GFSERDOC)+;
                  ","+cp_ToStrODBCNull(this.w_GFSERPNT)+;
                  ","+cp_ToStrODBCNull(this.w_GFCODART)+;
                  ","+cp_ToStrODBC(this.w_GF__FILE)+;
                  ","+cp_ToStrODBCNull(this.w_GFCODCAT)+;
                  ","+cp_ToStrODBCNull(this.w_GFCODMAR)+;
                  ","+cp_ToStrODBCNull(this.w_GFCODFAM)+;
                  ","+cp_ToStrODBCNull(this.w_GFCODGRU)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GESTFILE')
        i_extval=cp_InsertValVFPExtFlds(this,'GESTFILE')
        cp_CheckDeletedKey(i_cTable,0,'GFSERIAL',this.w_GFSERIAL)
        INSERT INTO (i_cTable);
              (GFSERIAL,GFDATREG,GFTIPALL,GFCLAALL,GFDESCRI,GFMODALL,GF__NOTE,GFDATINI,GFDATOBS,GFFLPRIN,GFPUBWEB,GF__PATH,GFCODPAD,GFTIPCON,GFCODCON,GFSERDOC,GFSERPNT,GFCODART,GF__FILE,GFCODCAT,GFCODMAR,GFCODFAM,GFCODGRU  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GFSERIAL;
                  ,this.w_GFDATREG;
                  ,this.w_GFTIPALL;
                  ,this.w_GFCLAALL;
                  ,this.w_GFDESCRI;
                  ,this.w_GFMODALL;
                  ,this.w_GF__NOTE;
                  ,this.w_GFDATINI;
                  ,this.w_GFDATOBS;
                  ,this.w_GFFLPRIN;
                  ,this.w_GFPUBWEB;
                  ,this.w_GF__PATH;
                  ,this.w_GFCODPAD;
                  ,this.w_GFTIPCON;
                  ,this.w_GFCODCON;
                  ,this.w_GFSERDOC;
                  ,this.w_GFSERPNT;
                  ,this.w_GFCODART;
                  ,this.w_GF__FILE;
                  ,this.w_GFCODCAT;
                  ,this.w_GFCODMAR;
                  ,this.w_GFCODFAM;
                  ,this.w_GFCODGRU;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GESTFILE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GESTFILE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GESTFILE_IDX,i_nConn)
      *
      * update GESTFILE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GESTFILE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GFDATREG="+cp_ToStrODBC(this.w_GFDATREG)+;
             ",GFTIPALL="+cp_ToStrODBCNull(this.w_GFTIPALL)+;
             ",GFCLAALL="+cp_ToStrODBCNull(this.w_GFCLAALL)+;
             ",GFDESCRI="+cp_ToStrODBC(this.w_GFDESCRI)+;
             ",GFMODALL="+cp_ToStrODBC(this.w_GFMODALL)+;
             ",GF__NOTE="+cp_ToStrODBC(this.w_GF__NOTE)+;
             ",GFDATINI="+cp_ToStrODBC(this.w_GFDATINI)+;
             ",GFDATOBS="+cp_ToStrODBC(this.w_GFDATOBS)+;
             ",GFFLPRIN="+cp_ToStrODBC(this.w_GFFLPRIN)+;
             ",GFPUBWEB="+cp_ToStrODBC(this.w_GFPUBWEB)+;
             ",GF__PATH="+cp_ToStrODBC(this.w_GF__PATH)+;
             ",GFCODPAD="+cp_ToStrODBC(this.w_GFCODPAD)+;
             ",GFTIPCON="+cp_ToStrODBC(this.w_GFTIPCON)+;
             ",GFCODCON="+cp_ToStrODBCNull(this.w_GFCODCON)+;
             ",GFSERDOC="+cp_ToStrODBCNull(this.w_GFSERDOC)+;
             ",GFSERPNT="+cp_ToStrODBCNull(this.w_GFSERPNT)+;
             ",GFCODART="+cp_ToStrODBCNull(this.w_GFCODART)+;
             ",GF__FILE="+cp_ToStrODBC(this.w_GF__FILE)+;
             ",GFCODCAT="+cp_ToStrODBCNull(this.w_GFCODCAT)+;
             ",GFCODMAR="+cp_ToStrODBCNull(this.w_GFCODMAR)+;
             ",GFCODFAM="+cp_ToStrODBCNull(this.w_GFCODFAM)+;
             ",GFCODGRU="+cp_ToStrODBCNull(this.w_GFCODGRU)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GESTFILE')
        i_cWhere = cp_PKFox(i_cTable  ,'GFSERIAL',this.w_GFSERIAL  )
        UPDATE (i_cTable) SET;
              GFDATREG=this.w_GFDATREG;
             ,GFTIPALL=this.w_GFTIPALL;
             ,GFCLAALL=this.w_GFCLAALL;
             ,GFDESCRI=this.w_GFDESCRI;
             ,GFMODALL=this.w_GFMODALL;
             ,GF__NOTE=this.w_GF__NOTE;
             ,GFDATINI=this.w_GFDATINI;
             ,GFDATOBS=this.w_GFDATOBS;
             ,GFFLPRIN=this.w_GFFLPRIN;
             ,GFPUBWEB=this.w_GFPUBWEB;
             ,GF__PATH=this.w_GF__PATH;
             ,GFCODPAD=this.w_GFCODPAD;
             ,GFTIPCON=this.w_GFTIPCON;
             ,GFCODCON=this.w_GFCODCON;
             ,GFSERDOC=this.w_GFSERDOC;
             ,GFSERPNT=this.w_GFSERPNT;
             ,GFCODART=this.w_GFCODART;
             ,GF__FILE=this.w_GF__FILE;
             ,GFCODCAT=this.w_GFCODCAT;
             ,GFCODMAR=this.w_GFCODMAR;
             ,GFCODFAM=this.w_GFCODFAM;
             ,GFCODGRU=this.w_GFCODGRU;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsut_agf
    This.NotifyEvent('Salva')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GESTFILE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GESTFILE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GESTFILE_IDX,i_nConn)
      *
      * delete GESTFILE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GFSERIAL',this.w_GFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GESTFILE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESTFILE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,11,.t.)
        if .o_GFMODALL<>.w_GFMODALL.or. .o_GFTIPALL<>.w_GFTIPALL.or. .o_GFCLAALL<>.w_GFCLAALL
            .w_GF__PATH = IIF(.w_GFMODALL='F', Alltrim(.w_CARTLIB)+(IIF(.w_FL_TIP='S',Alltrim(.w_GFTIPALL)+'\',''))+(IIF(.w_FL_CLA='S',Alltrim(.w_GFCLAALL)+'\',''))+Alltrim(.w_PERIODO)+'\'+Alltrim(.w_FILE) + Alltrim(.w_ESTENSIONE), .w_COLLEG)
        endif
        .DoRTCalc(13,29,.t.)
            .w_CODAZI = i_CODAZI
        .DoRTCalc(31,31,.t.)
            .w_POSIZ = rat('\',.w_NOMFIL)
            .w_NFILONLY = substr(.w_NOMFIL,.w_POSIZ+1,50)
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"GESTSER","i_codazi,w_GFSERIAL")
          .op_GFSERIAL = .w_GFSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(34,54,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGFDATREG_1_2.enabled = this.oPgFrm.Page1.oPag.oGFDATREG_1_2.mCond()
    this.oPgFrm.Page1.oPag.oGFTIPALL_1_3.enabled = this.oPgFrm.Page1.oPag.oGFTIPALL_1_3.mCond()
    this.oPgFrm.Page1.oPag.oGFCLAALL_1_5.enabled = this.oPgFrm.Page1.oPag.oGFCLAALL_1_5.mCond()
    this.oPgFrm.Page1.oPag.oGFMODALL_1_7.enabled = this.oPgFrm.Page1.oPag.oGFMODALL_1_7.mCond()
    this.oPgFrm.Page1.oPag.oGFPUBWEB_1_21.enabled = this.oPgFrm.Page1.oPag.oGFPUBWEB_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oGFCODCON_1_30.visible=!this.oPgFrm.Page1.oPag.oGFCODCON_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oGFSERDOC_1_32.visible=!this.oPgFrm.Page1.oPag.oGFSERDOC_1_32.mHide()
    this.oPgFrm.Page1.oPag.oGFSERPNT_1_33.visible=!this.oPgFrm.Page1.oPag.oGFSERPNT_1_33.mHide()
    this.oPgFrm.Page1.oPag.oGFCODART_1_34.visible=!this.oPgFrm.Page1.oPag.oGFCODART_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_39.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_39.mHide()
    this.oPgFrm.Page1.oPag.oTIPDOC_1_40.visible=!this.oPgFrm.Page1.oPag.oTIPDOC_1_40.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOC_1_41.visible=!this.oPgFrm.Page1.oPag.oNUMDOC_1_41.mHide()
    this.oPgFrm.Page1.oPag.oALFDOC_1_42.visible=!this.oPgFrm.Page1.oPag.oALFDOC_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDATDOC_1_43.visible=!this.oPgFrm.Page1.oPag.oDATDOC_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oCODCAU_1_47.visible=!this.oPgFrm.Page1.oPag.oCODCAU_1_47.mHide()
    this.oPgFrm.Page1.oPag.oNUMRER_1_48.visible=!this.oPgFrm.Page1.oPag.oNUMRER_1_48.mHide()
    this.oPgFrm.Page1.oPag.oCODUTE_1_49.visible=!this.oPgFrm.Page1.oPag.oCODUTE_1_49.mHide()
    this.oPgFrm.Page1.oPag.oDATREG_1_50.visible=!this.oPgFrm.Page1.oPag.oDATREG_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oGF__FILE_1_57.visible=!this.oPgFrm.Page1.oPag.oGF__FILE_1_57.mHide()
    this.oPgFrm.Page1.oPag.oCOLLEG_1_60.visible=!this.oPgFrm.Page1.oPag.oCOLLEG_1_60.mHide()
    this.oPgFrm.Page1.oPag.oGFCODCAT_1_70.visible=!this.oPgFrm.Page1.oPag.oGFCODCAT_1_70.mHide()
    this.oPgFrm.Page1.oPag.oGFCODMAR_1_71.visible=!this.oPgFrm.Page1.oPag.oGFCODMAR_1_71.mHide()
    this.oPgFrm.Page1.oPag.oGFCODFAM_1_72.visible=!this.oPgFrm.Page1.oPag.oGFCODFAM_1_72.mHide()
    this.oPgFrm.Page1.oPag.oGFCODGRU_1_73.visible=!this.oPgFrm.Page1.oPag.oGFCODGRU_1_73.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_74.visible=!this.oPgFrm.Page1.oPag.oStr_1_74.mHide()
    this.oPgFrm.Page1.oPag.oDESART_1_75.visible=!this.oPgFrm.Page1.oPag.oDESART_1_75.mHide()
    this.oPgFrm.Page1.oPag.oDESCAT_1_76.visible=!this.oPgFrm.Page1.oPag.oDESCAT_1_76.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    this.oPgFrm.Page1.oPag.oDESMAR_1_78.visible=!this.oPgFrm.Page1.oPag.oDESMAR_1_78.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_79.visible=!this.oPgFrm.Page1.oPag.oStr_1_79.mHide()
    this.oPgFrm.Page1.oPag.oDESFAM_1_80.visible=!this.oPgFrm.Page1.oPag.oDESFAM_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_81.visible=!this.oPgFrm.Page1.oPag.oStr_1_81.mHide()
    this.oPgFrm.Page1.oPag.oDESMER_1_82.visible=!this.oPgFrm.Page1.oPag.oDESMER_1_82.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut_agf
    *-- GSUT_KGF: variabile pubblica creata nella maschera GSUT_KGF.
    *-- Se si preme il bottone Aggiungi viene valorizzata con l'Oggetto
    *-- maschera in modo che si possa prendere il controllo del Bottone Aggiorna
    * -- Alla chiusura della gestione, se questa � lanciata dalla maschera GSUT_KGF
    * -- viene aggiornato lo zoom. Solo se ho premuto salva.
    
    If cEvent='Done'
       LOCAL BtnAgg
       if Type("g_GSUT_KGF")="O"
            BtnAgg = g_GSUT_KGF.GetCtrl(Cp_Translate("A\<ggiorna"))
            if Type("BtnAgg")="O"
              * --- Dopo aver inserito un nuovo file, aggiorno lo Zoom
              BtnAgg.Click()
            endif
       endif
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_86.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GFTIPALL
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFTIPALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'TIP_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODICE like "+cp_ToStrODBC(trim(this.w_GFTIPALL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',trim(this.w_GFTIPALL))
          select TACODICE,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GFTIPALL)==trim(_Link_.TACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GFTIPALL) and !this.bDontReportError
            deferred_cp_zoom('TIP_ALLE','*','TACODICE',cp_AbsName(oSource.parent,'oGFTIPALL_1_3'),i_cWhere,'GSUT_MTA',"Tipologie allegati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1))
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFTIPALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_GFTIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_GFTIPALL)
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFTIPALL = NVL(_Link_.TACODICE,space(5))
      this.w_DESTIP = NVL(_Link_.TADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GFTIPALL = space(5)
      endif
      this.w_DESTIP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFTIPALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_ALLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.TACODICE as TACODICE103"+ ",link_1_3.TADESCRI as TADESCRI103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on GESTFILE.GFTIPALL=link_1_3.TACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and GESTFILE.GFTIPALL=link_1_3.TACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GFCLAALL
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFCLAALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLA_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODCLA like "+cp_ToStrODBC(trim(this.w_GFCLAALL)+"%");
                   +" and TACODICE="+cp_ToStrODBC(this.w_GFTIPALL);

          i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE,TACODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',this.w_GFTIPALL;
                     ,'TACODCLA',trim(this.w_GFCLAALL))
          select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE,TACODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GFCLAALL)==trim(_Link_.TACODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GFCLAALL) and !this.bDontReportError
            deferred_cp_zoom('CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(oSource.parent,'oGFCLAALL_1_5'),i_cWhere,'',"Classi allegati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GFTIPALL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB";
                     +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TACODICE="+cp_ToStrODBC(this.w_GFTIPALL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1);
                       ,'TACODCLA',oSource.xKey(2))
            select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFCLAALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_GFCLAALL);
                   +" and TACODICE="+cp_ToStrODBC(this.w_GFTIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_GFTIPALL;
                       ,'TACODCLA',this.w_GFCLAALL)
            select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFCLAALL = NVL(_Link_.TACODCLA,space(5))
      this.w_DESCLA = NVL(_Link_.TACLADES,space(40))
      this.w_FLUNIC = NVL(_Link_.TAFLUNIC,space(1))
      this.w_GFPUBWEB = NVL(_Link_.TDPUBWEB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GFCLAALL = space(5)
      endif
      this.w_DESCLA = space(40)
      this.w_FLUNIC = space(1)
      this.w_GFPUBWEB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFCLAALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLA_ALLE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.TACODCLA as TACODCLA105"+ ",link_1_5.TACLADES as TACLADES105"+ ",link_1_5.TAFLUNIC as TAFLUNIC105"+ ",link_1_5.TDPUBWEB as TDPUBWEB105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on GESTFILE.GFCLAALL=link_1_5.TACODCLA"+" and GESTFILE.GFTIPALL=link_1_5.TACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and GESTFILE.GFCLAALL=link_1_5.TACODCLA(+)"'+'+" and GESTFILE.GFTIPALL=link_1_5.TACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GFCODCON
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_GFCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GFTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_GFTIPCON;
                     ,'ANCODICE',trim(this.w_GFCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GFCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GFCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oGFCODCON_1_30'),i_cWhere,'GSAR_BZC',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GFTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_GFTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_GFCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GFTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_GFTIPCON;
                       ,'ANCODICE',this.w_GFCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GFCODCON = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.ANCODICE as ANCODICE130"+ ",link_1_30.ANDESCRI as ANDESCRI130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on GESTFILE.GFCODCON=link_1_30.ANCODICE"+" and GESTFILE.GFTIPCON=link_1_30.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and GESTFILE.GFCODCON=link_1_30.ANCODICE(+)"'+'+" and GESTFILE.GFTIPCON=link_1_30.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GFSERDOC
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFSERDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_MDV',True,'DOC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MVSERIAL like "+cp_ToStrODBC(trim(this.w_GFSERDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MVSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MVSERIAL',trim(this.w_GFSERDOC))
          select MVSERIAL,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MVSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GFSERDOC)==trim(_Link_.MVSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GFSERDOC) and !this.bDontReportError
            deferred_cp_zoom('DOC_MAST','*','MVSERIAL',cp_AbsName(oSource.parent,'oGFSERDOC_1_32'),i_cWhere,'GSVE_MDV',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',oSource.xKey(1))
            select MVSERIAL,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFSERDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_GFSERDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_GFSERDOC)
            select MVSERIAL,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFSERDOC = NVL(_Link_.MVSERIAL,space(10))
      this.w_TIPDOC = NVL(_Link_.MVTIPDOC,space(5))
      this.w_NUMDOC = NVL(_Link_.MVNUMDOC,0)
      this.w_ALFDOC = NVL(_Link_.MVALFDOC,space(10))
      this.w_DATDOC = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GFSERDOC = space(10)
      endif
      this.w_TIPDOC = space(5)
      this.w_NUMDOC = 0
      this.w_ALFDOC = space(10)
      this.w_DATDOC = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFSERDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DOC_MAST_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_32.MVSERIAL as MVSERIAL132"+ ",link_1_32.MVTIPDOC as MVTIPDOC132"+ ",link_1_32.MVNUMDOC as MVNUMDOC132"+ ",link_1_32.MVALFDOC as MVALFDOC132"+ ",link_1_32.MVDATDOC as MVDATDOC132"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_32 on GESTFILE.GFSERDOC=link_1_32.MVSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_32"
          i_cKey=i_cKey+'+" and GESTFILE.GFSERDOC=link_1_32.MVSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GFSERPNT
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_lTable = "PNT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2], .t., this.PNT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFSERPNT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MPN',True,'PNT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PNSERIAL like "+cp_ToStrODBC(trim(this.w_GFSERPNT)+"%");

          i_ret=cp_SQL(i_nConn,"select PNSERIAL,PNCODCAU,PNNUMRER,PNCODUTE,PNDATREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PNSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PNSERIAL',trim(this.w_GFSERPNT))
          select PNSERIAL,PNCODCAU,PNNUMRER,PNCODUTE,PNDATREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PNSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GFSERPNT)==trim(_Link_.PNSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GFSERPNT) and !this.bDontReportError
            deferred_cp_zoom('PNT_MAST','*','PNSERIAL',cp_AbsName(oSource.parent,'oGFSERPNT_1_33'),i_cWhere,'GSCG_MPN',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PNSERIAL,PNCODCAU,PNNUMRER,PNCODUTE,PNDATREG";
                     +" from "+i_cTable+" "+i_lTable+" where PNSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PNSERIAL',oSource.xKey(1))
            select PNSERIAL,PNCODCAU,PNNUMRER,PNCODUTE,PNDATREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFSERPNT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PNSERIAL,PNCODCAU,PNNUMRER,PNCODUTE,PNDATREG";
                   +" from "+i_cTable+" "+i_lTable+" where PNSERIAL="+cp_ToStrODBC(this.w_GFSERPNT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PNSERIAL',this.w_GFSERPNT)
            select PNSERIAL,PNCODCAU,PNNUMRER,PNCODUTE,PNDATREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFSERPNT = NVL(_Link_.PNSERIAL,space(10))
      this.w_CODCAU = NVL(_Link_.PNCODCAU,space(5))
      this.w_NUMRER = NVL(_Link_.PNNUMRER,0)
      this.w_CODUTE = NVL(_Link_.PNCODUTE,0)
      this.w_DATREG = NVL(cp_ToDate(_Link_.PNDATREG),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GFSERPNT = space(10)
      endif
      this.w_CODCAU = space(5)
      this.w_NUMRER = 0
      this.w_CODUTE = 0
      this.w_DATREG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.PNSERIAL,1)
      cp_ShowWarn(i_cKey,this.PNT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFSERPNT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_33(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PNT_MAST_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_33.PNSERIAL as PNSERIAL133"+ ",link_1_33.PNCODCAU as PNCODCAU133"+ ",link_1_33.PNNUMRER as PNNUMRER133"+ ",link_1_33.PNCODUTE as PNCODUTE133"+ ",link_1_33.PNDATREG as PNDATREG133"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_33 on GESTFILE.GFSERPNT=link_1_33.PNSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_33"
          i_cKey=i_cKey+'+" and GESTFILE.GFSERPNT=link_1_33.PNSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GFCODART
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_GFCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_GFCODART))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GFCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GFCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oGFCODART_1_34'),i_cWhere,'GSMA_BZA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_GFCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_GFCODART)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GFCODART = space(20)
      endif
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_34.ARCODART as ARCODART134"+ ",link_1_34.ARDESART as ARDESART134"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_34 on GESTFILE.GFCODART=link_1_34.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_34"
          i_cKey=i_cKey+'+" and GESTFILE.GFCODART=link_1_34.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GFCODCAT
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFCODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_GFCODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_GFCODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GFCODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GFCODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oGFCODCAT_1_70'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFCODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_GFCODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_GFCODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFCODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GFCODCAT = space(5)
      endif
      this.w_DESCAT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFCODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_70(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_70.OMCODICE as OMCODICE170"+ ","+cp_TransLinkFldName('link_1_70.OMDESCRI')+" as OMDESCRI170"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_70 on GESTFILE.GFCODCAT=link_1_70.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_70"
          i_cKey=i_cKey+'+" and GESTFILE.GFCODCAT=link_1_70.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GFCODMAR
  func Link_1_71(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFCODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_GFCODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_GFCODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GFCODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GFCODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oGFCODMAR_1_71'),i_cWhere,'GSAR_AMH',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFCODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_GFCODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_GFCODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFCODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GFCODMAR = space(5)
      endif
      this.w_DESMAR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFCODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_71(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MARCHI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_71.MACODICE as MACODICE171"+ ",link_1_71.MADESCRI as MADESCRI171"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_71 on GESTFILE.GFCODMAR=link_1_71.MACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_71"
          i_cKey=i_cKey+'+" and GESTFILE.GFCODMAR=link_1_71.MACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GFCODFAM
  func Link_1_72(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFCODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_GFCODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_GFCODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GFCODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GFCODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oGFCODFAM_1_72'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFCODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_GFCODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_GFCODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFCODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(_Link_.FADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GFCODFAM = space(5)
      endif
      this.w_DESFAM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFCODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_72(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_72.FACODICE as FACODICE172"+ ",link_1_72.FADESCRI as FADESCRI172"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_72 on GESTFILE.GFCODFAM=link_1_72.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_72"
          i_cKey=i_cKey+'+" and GESTFILE.GFCODFAM=link_1_72.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GFCODGRU
  func Link_1_73(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GFCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GFCODGRU))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GFCODGRU)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GFCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGFCODGRU_1_73'),i_cWhere,'GSAR_AGM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GFCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GFCODGRU)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFCODGRU = NVL(_Link_.GMCODICE,space(5))
      this.w_DESMER = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GFCODGRU = space(5)
      endif
      this.w_DESMER = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_73(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_73.GMCODICE as GMCODICE173"+ ","+cp_TransLinkFldName('link_1_73.GMDESCRI')+" as GMDESCRI173"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_73 on GESTFILE.GFCODGRU=link_1_73.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_73"
          i_cKey=i_cKey+'+" and GESTFILE.GFCODGRU=link_1_73.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGFSERIAL_1_1.value==this.w_GFSERIAL)
      this.oPgFrm.Page1.oPag.oGFSERIAL_1_1.value=this.w_GFSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oGFDATREG_1_2.value==this.w_GFDATREG)
      this.oPgFrm.Page1.oPag.oGFDATREG_1_2.value=this.w_GFDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oGFTIPALL_1_3.value==this.w_GFTIPALL)
      this.oPgFrm.Page1.oPag.oGFTIPALL_1_3.value=this.w_GFTIPALL
    endif
    if not(this.oPgFrm.Page1.oPag.oGFCLAALL_1_5.value==this.w_GFCLAALL)
      this.oPgFrm.Page1.oPag.oGFCLAALL_1_5.value=this.w_GFCLAALL
    endif
    if not(this.oPgFrm.Page1.oPag.oGFDESCRI_1_6.value==this.w_GFDESCRI)
      this.oPgFrm.Page1.oPag.oGFDESCRI_1_6.value=this.w_GFDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oGFMODALL_1_7.RadioValue()==this.w_GFMODALL)
      this.oPgFrm.Page1.oPag.oGFMODALL_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGF__NOTE_1_8.value==this.w_GF__NOTE)
      this.oPgFrm.Page1.oPag.oGF__NOTE_1_8.value=this.w_GF__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oGFDATINI_1_9.value==this.w_GFDATINI)
      this.oPgFrm.Page1.oPag.oGFDATINI_1_9.value=this.w_GFDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGFDATOBS_1_10.value==this.w_GFDATOBS)
      this.oPgFrm.Page1.oPag.oGFDATOBS_1_10.value=this.w_GFDATOBS
    endif
    if not(this.oPgFrm.Page1.oPag.oGFFLPRIN_1_20.RadioValue()==this.w_GFFLPRIN)
      this.oPgFrm.Page1.oPag.oGFFLPRIN_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGFPUBWEB_1_21.RadioValue()==this.w_GFPUBWEB)
      this.oPgFrm.Page1.oPag.oGFPUBWEB_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGF__PATH_1_22.value==this.w_GF__PATH)
      this.oPgFrm.Page1.oPag.oGF__PATH_1_22.value=this.w_GF__PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_23.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_23.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIP_1_24.value==this.w_DESTIP)
      this.oPgFrm.Page1.oPag.oDESTIP_1_24.value=this.w_DESTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oGFCODCON_1_30.value==this.w_GFCODCON)
      this.oPgFrm.Page1.oPag.oGFCODCON_1_30.value=this.w_GFCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oGFSERDOC_1_32.value==this.w_GFSERDOC)
      this.oPgFrm.Page1.oPag.oGFSERDOC_1_32.value=this.w_GFSERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGFSERPNT_1_33.value==this.w_GFSERPNT)
      this.oPgFrm.Page1.oPag.oGFSERPNT_1_33.value=this.w_GFSERPNT
    endif
    if not(this.oPgFrm.Page1.oPag.oGFCODART_1_34.value==this.w_GFCODART)
      this.oPgFrm.Page1.oPag.oGFCODART_1_34.value=this.w_GFCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_39.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_39.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_40.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_40.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_41.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_41.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_42.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_42.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC_1_43.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oDATDOC_1_43.value=this.w_DATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_47.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_47.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMRER_1_48.value==this.w_NUMRER)
      this.oPgFrm.Page1.oPag.oNUMRER_1_48.value=this.w_NUMRER
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_49.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_49.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_50.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_50.value=this.w_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oGF__FILE_1_57.value==this.w_GF__FILE)
      this.oPgFrm.Page1.oPag.oGF__FILE_1_57.value=this.w_GF__FILE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLLEG_1_60.value==this.w_COLLEG)
      this.oPgFrm.Page1.oPag.oCOLLEG_1_60.value=this.w_COLLEG
    endif
    if not(this.oPgFrm.Page1.oPag.oGFCODCAT_1_70.value==this.w_GFCODCAT)
      this.oPgFrm.Page1.oPag.oGFCODCAT_1_70.value=this.w_GFCODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oGFCODMAR_1_71.value==this.w_GFCODMAR)
      this.oPgFrm.Page1.oPag.oGFCODMAR_1_71.value=this.w_GFCODMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oGFCODFAM_1_72.value==this.w_GFCODFAM)
      this.oPgFrm.Page1.oPag.oGFCODFAM_1_72.value=this.w_GFCODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oGFCODGRU_1_73.value==this.w_GFCODGRU)
      this.oPgFrm.Page1.oPag.oGFCODGRU_1_73.value=this.w_GFCODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_75.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_75.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_76.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_76.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAR_1_78.value==this.w_DESMAR)
      this.oPgFrm.Page1.oPag.oDESMAR_1_78.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_80.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_80.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMER_1_82.value==this.w_DESMER)
      this.oPgFrm.Page1.oPag.oDESMER_1_82.value=this.w_DESMER
    endif
    cp_SetControlsValueExtFlds(this,'GESTFILE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_GFDATREG))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGFDATREG_1_2.SetFocus()
            i_bnoObbl = !empty(.w_GFDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GFTIPALL))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGFTIPALL_1_3.SetFocus()
            i_bnoObbl = !empty(.w_GFTIPALL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GFCLAALL))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGFCLAALL_1_5.SetFocus()
            i_bnoObbl = !empty(.w_GFCLAALL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GFTIPALL = this.w_GFTIPALL
    this.o_GFCLAALL = this.w_GFCLAALL
    this.o_GFMODALL = this.w_GFMODALL
    return

enddefine

* --- Define pages as container
define class tgsut_agfPag1 as StdContainer
  Width  = 664
  height = 404
  stdWidth  = 664
  stdheight = 404
  resizeXpos=506
  resizeYpos=266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGFSERIAL_1_1 as StdField with uid="SQSZINHBJM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_GFSERIAL", cQueryName = "GFSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 199309234,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=104, Left=127, Top=15, InputMask=replicate('X',10)

  add object oGFDATREG_1_2 as StdField with uid="OHNVZELQFJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GFDATREG", cQueryName = "GFDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 83642285,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=419, Top=15, tabstop=.f.

  func oGFDATREG_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oGFTIPALL_1_3 as StdField with uid="WLCCECOMII",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GFTIPALL", cQueryName = "GFTIPALL",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 205174862,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=127, Top=49, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_GFTIPALL"

  func oGFTIPALL_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oGFTIPALL_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_GFCLAALL)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oGFTIPALL_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGFTIPALL_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_ALLE','*','TACODICE',cp_AbsName(this.parent,'oGFTIPALL_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Tipologie allegati",'',this.parent.oContained
  endproc
  proc oGFTIPALL_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODICE=this.parent.oContained.w_GFTIPALL
     i_obj.ecpSave()
  endproc

  add object oGFCLAALL_1_5 as StdField with uid="MLXTNQABFW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GFCLAALL", cQueryName = "GFCLAALL",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 220776526,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=127, Top=77, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_ALLE", oKey_1_1="TACODICE", oKey_1_2="this.w_GFTIPALL", oKey_2_1="TACODCLA", oKey_2_2="this.w_GFCLAALL"

  func oGFCLAALL_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oGFCLAALL_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oGFCLAALL_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGFCLAALL_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLA_ALLE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStrODBC(this.Parent.oContained.w_GFTIPALL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStr(this.Parent.oContained.w_GFTIPALL)
    endif
    do cp_zoom with 'CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(this.parent,'oGFCLAALL_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Classi allegati",'',this.parent.oContained
  endproc

  add object oGFDESCRI_1_6 as StdField with uid="CFNVEHLOJB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_GFDESCRI", cQueryName = "GFDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 99633071,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=127, Top=106, InputMask=replicate('X',40)


  add object oGFMODALL_1_7 as StdCombo with uid="GSGZIYBJWZ",rtseq=6,rtrep=.f.,left=127,top=137,width=105,height=21;
    , ToolTipText = "Modalit� di associazione file (collegamento o copia file)";
    , HelpContextID = 217393230;
    , cFormVar="w_GFMODALL",RowSource=""+"Collegamento,"+"Copia file", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGFMODALL_1_7.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oGFMODALL_1_7.GetRadio()
    this.Parent.oContained.w_GFMODALL = this.RadioValue()
    return .t.
  endfunc

  func oGFMODALL_1_7.SetRadio()
    this.Parent.oContained.w_GFMODALL=trim(this.Parent.oContained.w_GFMODALL)
    this.value = ;
      iif(this.Parent.oContained.w_GFMODALL=='L',1,;
      iif(this.Parent.oContained.w_GFMODALL=='F',2,;
      0))
  endfunc

  func oGFMODALL_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' and Not(.w_SCANNER))
    endwith
   endif
  endfunc

  add object oGF__NOTE_1_8 as StdMemo with uid="XSHRFYEJWJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_GF__NOTE", cQueryName = "GF__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 29095851,;
   bGlobalFont=.t.,;
    Height=91, Width=535, Left=127, Top=198

  add object oGFDATINI_1_9 as StdField with uid="FVBYDDGLRD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_GFDATINI", cQueryName = "GFDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 67352657,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=127, Top=298

  add object oGFDATOBS_1_10 as StdField with uid="ZVRLTIOODE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_GFDATOBS", cQueryName = "GFDATOBS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 33310649,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=419, Top=298


  add object oBtn_1_11 as StdButton with uid="QOUYREPXDQ",left=561, top=357, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 42529562;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="ZOPXDCHXAD",left=612, top=357, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 11734790;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oGFFLPRIN_1_20 as StdCheck with uid="VCOCSKJKVH",rtseq=10,rtrep=.f.,left=517, top=15, caption="Allegato principale",;
    ToolTipText = "Se attivo indica che il file allegato � quello principale per questo record",;
    HelpContextID = 188258380,;
    cFormVar="w_GFFLPRIN", bObbl = .f. , nPag = 1;
    , Tabstop=.F.;
   , bGlobalFont=.t.


  func oGFFLPRIN_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oGFFLPRIN_1_20.GetRadio()
    this.Parent.oContained.w_GFFLPRIN = this.RadioValue()
    return .t.
  endfunc

  func oGFFLPRIN_1_20.SetRadio()
    this.Parent.oContained.w_GFFLPRIN=trim(this.Parent.oContained.w_GFFLPRIN)
    this.value = ;
      iif(this.Parent.oContained.w_GFFLPRIN=='S',1,;
      0)
  endfunc

  add object oGFPUBWEB_1_21 as StdCheck with uid="JUQERQYDAX",rtseq=11,rtrep=.f.,left=517, top=37, caption="Pubblica su web",;
    ToolTipText = "Se attivo: l'allegato sar� pubblicato sul web",;
    HelpContextID = 150013864,;
    cFormVar="w_GFPUBWEB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGFPUBWEB_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGFPUBWEB_1_21.GetRadio()
    this.Parent.oContained.w_GFPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oGFPUBWEB_1_21.SetRadio()
    this.Parent.oContained.w_GFPUBWEB=trim(this.Parent.oContained.w_GFPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_GFPUBWEB=='S',1,;
      0)
  endfunc

  func oGFPUBWEB_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP $ 'SA')
    endwith
   endif
  endfunc

  add object oGF__PATH_1_22 as StdField with uid="COQYXCSXDG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_GF__PATH", cQueryName = "GF__PATH",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "File che verr� effettivamente allegato",;
    HelpContextID = 64747438,;
   bGlobalFont=.t.,;
    Height=21, Width=535, Left=127, Top=167, InputMask=replicate('X',250), ReadOnly=.T.

  add object oDESCLA_1_23 as StdField with uid="XRIGRCTLQA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 58668598,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=188, Top=77, InputMask=replicate('X',40)

  add object oDESTIP_1_24 as StdField with uid="OZCDVQRSTY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESTIP", cQueryName = "DESTIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 39859766,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=188, Top=49, InputMask=replicate('X',40)

  add object oGFCODCON_1_30 as StdField with uid="DTPXSCTAKV",rtseq=17,rtrep=.f.,;
    cFormVar = "w_GFCODCON", cQueryName = "GFCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 84555700,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=127, Top=331, InputMask=replicate('X',15), ReadOnly =.t., cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_GFTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_GFCODCON"

  func oGFCODCON_1_30.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='C'))
    endwith
  endfunc

  func oGFCODCON_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oGFCODCON_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oGFSERDOC_1_32 as StdField with uid="ZYHPAXSOYO",rtseq=18,rtrep=.f.,;
    cFormVar = "w_GFSERDOC", cQueryName = "GFSERDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 115423145,;
   bGlobalFont=.t.,;
    Height=21, Width=80, Left=562, Top=63, InputMask=replicate('X',10), cLinkFile="DOC_MAST", cZoomOnZoom="GSVE_MDV", oKey_1_1="MVSERIAL", oKey_1_2="this.w_GFSERDOC"

  func oGFSERDOC_1_32.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oGFSERDOC_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oGFSERDOC_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oGFSERPNT_1_33 as StdField with uid="HXITIWYLAA",rtseq=19,rtrep=.f.,;
    cFormVar = "w_GFSERPNT", cQueryName = "GFSERPNT",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 48314298,;
   bGlobalFont=.t.,;
    Height=21, Width=80, Left=562, Top=84, InputMask=replicate('X',10), cLinkFile="PNT_MAST", cZoomOnZoom="GSCG_MPN", oKey_1_1="PNSERIAL", oKey_1_2="this.w_GFSERPNT"

  func oGFSERPNT_1_33.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oGFSERPNT_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oGFSERPNT_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oGFCODART_1_34 as StdField with uid="FQEFQYVGHM",rtseq=20,rtrep=.f.,;
    cFormVar = "w_GFCODART", cQueryName = "GFCODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 51001274,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=127, Top=331, InputMask=replicate('X',20), ReadOnly =.t., cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_GFCODART"

  func oGFCODART_1_34.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='A'))
    endwith
  endfunc

  func oGFCODART_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oGFCODART_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oDESCON_1_39 as StdField with uid="NUDBNCFWKR",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 11482678,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=266, Top=331, InputMask=replicate('X',40)

  func oDESCON_1_39.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='C'))
    endwith
  endfunc

  add object oTIPDOC_1_40 as StdField with uid="GRFQUFQTSM",rtseq=22,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 95423286,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=127, Top=331, InputMask=replicate('X',5)

  func oTIPDOC_1_40.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='D'))
    endwith
  endfunc

  add object oNUMDOC_1_41 as StdField with uid="VKFNINIIHX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 95413974,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=219, Top=331

  func oNUMDOC_1_41.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='D'))
    endwith
  endfunc

  add object oALFDOC_1_42 as StdField with uid="TZUSSXNBQB",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 95382790,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=350, Top=331, InputMask=replicate('X',10)

  func oALFDOC_1_42.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='D'))
    endwith
  endfunc

  add object oDATDOC_1_43 as StdField with uid="OITHRBYDYW",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DATDOC", cQueryName = "DATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 95437366,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=467, Top=331

  func oDATDOC_1_43.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='D'))
    endwith
  endfunc

  add object oCODCAU_1_47 as StdField with uid="ENLOCARJHD",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 114184230,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=127, Top=331, InputMask=replicate('X',5)

  func oCODCAU_1_47.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='P'))
    endwith
  endfunc

  add object oNUMRER_1_48 as StdField with uid="QARTOMVNSW",rtseq=27,rtrep=.f.,;
    cFormVar = "w_NUMRER", cQueryName = "NUMRER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 69068502,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=219, Top=331

  func oNUMRER_1_48.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='P'))
    endwith
  endfunc

  add object oCODUTE_1_49 as StdField with uid="POMOJWMAOQ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 135286822,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=308, Top=331

  func oCODUTE_1_49.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='P'))
    endwith
  endfunc

  add object oDATREG_1_50 as StdField with uid="TSAYZPYPLK",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 152977974,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=381, Top=331

  func oDATREG_1_50.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='P'))
    endwith
  endfunc

  add object oGF__FILE_1_57 as StdField with uid="NQMADMTLWI",rtseq=34,rtrep=.f.,;
    cFormVar = "w_GF__FILE", cQueryName = "GF__FILE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 79956053,;
   bGlobalFont=.t.,;
    Height=21, Width=339, Left=324, Top=137, InputMask=replicate('X',254)

  func oGF__FILE_1_57.mHide()
    with this.Parent.oContained
      return (.w_GFMODALL = 'L')
    endwith
  endfunc

  add object oCOLLEG_1_60 as StdField with uid="XPPWIEELRW",rtseq=36,rtrep=.f.,;
    cFormVar = "w_COLLEG", cQueryName = "COLLEG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path del file nel caso di collegamento",;
    HelpContextID = 152555558,;
   bGlobalFont=.t.,;
    Height=21, Width=339, Left=324, Top=137, InputMask=replicate('X',254)

  func oCOLLEG_1_60.mHide()
    with this.Parent.oContained
      return (.w_GFMODALL = 'F')
    endwith
  endfunc


  add object oObj_1_66 as cp_runprogram with uid="VCJYLCOSGL",left=19, top=462, width=127,height=32,;
    caption='GSUT_BSA',;
   bGlobalFont=.t.,;
    prg="GSUT_BSA",;
    cEvent = "Salva",;
    nPag=1;
    , HelpContextID = 96494759

  add object oGFCODCAT_1_70 as StdField with uid="LEJVRGYTUZ",rtseq=45,rtrep=.f.,;
    cFormVar = "w_GFCODCAT", cQueryName = "GFCODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea cui � associato l'allegato",;
    HelpContextID = 84555706,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=127, Top=331, InputMask=replicate('X',5), ReadOnly =.t., cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_GFCODCAT"

  func oGFCODCAT_1_70.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='O'))
    endwith
  endfunc

  func oGFCODCAT_1_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_70('Part',this)
    endwith
    return bRes
  endfunc

  proc oGFCODCAT_1_70.ecpDrop(oSource)
    this.Parent.oContained.link_1_70('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oGFCODMAR_1_71 as StdField with uid="KRGSWOCIHI",rtseq=46,rtrep=.f.,;
    cFormVar = "w_GFCODMAR", cQueryName = "GFCODMAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 252327864,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=127, Top=331, InputMask=replicate('X',5), ReadOnly =.t., cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_GFCODMAR"

  func oGFCODMAR_1_71.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='M'))
    endwith
  endfunc

  func oGFCODMAR_1_71.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_71('Part',this)
    endwith
    return bRes
  endfunc

  proc oGFCODMAR_1_71.ecpDrop(oSource)
    this.Parent.oContained.link_1_71('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oGFCODFAM_1_72 as StdField with uid="CKOWFFKYOK",rtseq=47,rtrep=.f.,;
    cFormVar = "w_GFCODFAM", cQueryName = "GFCODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 134887347,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=127, Top=331, InputMask=replicate('X',5), ReadOnly =.t., cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_GFCODFAM"

  func oGFCODFAM_1_72.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='F'))
    endwith
  endfunc

  func oGFCODFAM_1_72.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_72('Part',this)
    endwith
    return bRes
  endfunc

  proc oGFCODFAM_1_72.ecpDrop(oSource)
    this.Parent.oContained.link_1_72('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oGFCODGRU_1_73 as StdField with uid="NTEFJLFFME",rtseq=48,rtrep=.f.,;
    cFormVar = "w_GFCODGRU", cQueryName = "GFCODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 151664571,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=127, Top=331, InputMask=replicate('X',5), ReadOnly =.t., cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GFCODGRU"

  func oGFCODGRU_1_73.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='G'))
    endwith
  endfunc

  func oGFCODGRU_1_73.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_73('Part',this)
    endwith
    return bRes
  endfunc

  proc oGFCODGRU_1_73.ecpDrop(oSource)
    this.Parent.oContained.link_1_73('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oDESART_1_75 as StdField with uid="STAEESGBLW",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 115160630,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=289, Top=331, InputMask=replicate('X',40)

  func oDESART_1_75.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='A'))
    endwith
  endfunc

  add object oDESCAT_1_76 as StdField with uid="PYUJFSIAID",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 97465910,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=192, Top=331, InputMask=replicate('X',40)

  func oDESCAT_1_76.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='O'))
    endwith
  endfunc

  add object oDESMAR_1_78 as StdField with uid="MSPLOFIBJI",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 64566838,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=192, Top=331, InputMask=replicate('X',40)

  func oDESMAR_1_78.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='M'))
    endwith
  endfunc

  add object oDESFAM_1_80 as StdField with uid="LKNNPPMBCE",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 248657462,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=192, Top=331, InputMask=replicate('X',40)

  func oDESFAM_1_80.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='F'))
    endwith
  endfunc

  add object oDESMER_1_82 as StdField with uid="YLFNEBYCJG",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESMER", cQueryName = "DESMER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 68761142,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=192, Top=331, InputMask=replicate('X',40)

  func oDESMER_1_82.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='G'))
    endwith
  endfunc


  add object oObj_1_86 as cp_runprogram with uid="OAFXBYMJSJ",left=19, top=493, width=219,height=25,;
    caption='GSUT_BCT(C)',;
   bGlobalFont=.t.,;
    prg="GSUT_BCT(w_GFTIPALL,'C')",;
    cEvent = "w_GFTIPALL Changed",;
    nPag=1;
    , HelpContextID = 96680506

  add object oStr_1_4 as StdString with uid="GAVLEXKJYZ",Visible=.t., Left=1, Top=16,;
    Alignment=1, Width=123, Height=18,;
    Caption="Progressivo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="BHHNPWQQMO",Visible=.t., Left=1, Top=50,;
    Alignment=1, Width=123, Height=18,;
    Caption="Tipologia allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="HLRLYOJRKF",Visible=.t., Left=1, Top=77,;
    Alignment=1, Width=123, Height=18,;
    Caption="Classe allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="SZBZMHCERR",Visible=.t., Left=1, Top=107,;
    Alignment=1, Width=123, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="YANQLIEYPJ",Visible=.t., Left=1, Top=198,;
    Alignment=1, Width=123, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="AGTDZMXVBW",Visible=.t., Left=363, Top=16,;
    Alignment=1, Width=54, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="REZMURAKNJ",Visible=.t., Left=1, Top=299,;
    Alignment=1, Width=123, Height=18,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="PUKWJKCKUC",Visible=.t., Left=293, Top=299,;
    Alignment=1, Width=123, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="CVJPJHTAMG",Visible=.t., Left=1, Top=137,;
    Alignment=1, Width=123, Height=18,;
    Caption="Modalit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="VXUZMZETDA",Visible=.t., Left=232, Top=137,;
    Alignment=1, Width=90, Height=18,;
    Caption="Percorso file:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_GFMODALL<>'L')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="FUWLVHRXUY",Visible=.t., Left=233, Top=137,;
    Alignment=1, Width=89, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_GFMODALL='L')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="FAKLLHUZZA",Visible=.t., Left=1, Top=331,;
    Alignment=1, Width=123, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='C' And .w_GFTIPCON='C'))
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="UWMCLBNFRK",Visible=.t., Left=1, Top=331,;
    Alignment=1, Width=123, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='C' And .w_GFTIPCON='F'))
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="NWXDBRZXBZ",Visible=.t., Left=1, Top=331,;
    Alignment=1, Width=123, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='A'))
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="BJEXMMEUPG",Visible=.t., Left=1, Top=331,;
    Alignment=1, Width=123, Height=18,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='D'))
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="GFCDFMKHOA",Visible=.t., Left=1, Top=331,;
    Alignment=1, Width=123, Height=18,;
    Caption="Reg. primanota:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='P'))
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="CNRBPZJPGI",Visible=.t., Left=192, Top=331,;
    Alignment=1, Width=26, Height=18,;
    Caption="N.:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='D' OR .w_GFCODPAD='P'))
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="KMEQZFBLQW",Visible=.t., Left=339, Top=331,;
    Alignment=2, Width=9, Height=17,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='D'))
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="VYOEXURYSZ",Visible=.t., Left=435, Top=331,;
    Alignment=1, Width=30, Height=18,;
    Caption="Del.:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='D'))
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="LENUSIXYOR",Visible=.t., Left=-1, Top=423,;
    Alignment=0, Width=740, Height=18,;
    Caption="Le variabili gfserdoc e gfserpnt devono essere in edit ma con condizione di hide sempre vera per getctrl in gsut_bsw"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="OVHRECMRON",Visible=.t., Left=1, Top=443,;
    Alignment=0, Width=517, Height=18,;
    Caption="e devono rimanere sulla maschera per ridimensionamento maschera"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="RLVGLJFVOY",Visible=.t., Left=1, Top=169,;
    Alignment=1, Width=123, Height=18,;
    Caption="File allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="PRERYVAEJK",Visible=.t., Left=1, Top=331,;
    Alignment=1, Width=123, Height=18,;
    Caption="Cat. omogenea:"  ;
  , bGlobalFont=.t.

  func oStr_1_74.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='O'))
    endwith
  endfunc

  add object oStr_1_77 as StdString with uid="PFMYHZVUTH",Visible=.t., Left=1, Top=331,;
    Alignment=1, Width=123, Height=18,;
    Caption="Codice marchio:"  ;
  , bGlobalFont=.t.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='M'))
    endwith
  endfunc

  add object oStr_1_79 as StdString with uid="TIYBWCGBOX",Visible=.t., Left=1, Top=331,;
    Alignment=1, Width=123, Height=18,;
    Caption="Famiglia articolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_79.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='F'))
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="VZJCDJRVLS",Visible=.t., Left=1, Top=331,;
    Alignment=1, Width=123, Height=18,;
    Caption="Gruppo merc.:"  ;
  , bGlobalFont=.t.

  func oStr_1_81.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='G'))
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="KGLMSFBSWD",Visible=.t., Left=297, Top=331,;
    Alignment=2, Width=9, Height=17,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='P'))
    endwith
  endfunc

  add object oStr_1_85 as StdString with uid="ASZBGOKRXC",Visible=.t., Left=349, Top=331,;
    Alignment=1, Width=30, Height=18,;
    Caption="Del.:"  ;
  , bGlobalFont=.t.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return (Not(.w_GFCODPAD='P'))
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_agf','GESTFILE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GFSERIAL=GESTFILE.GFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
