* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_brb                                                        *
*              Imposta C/C sulle righe del dettaglio paramento                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-10-25                                                      *
* Last revis.: 2011-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_brb",oParentObject)
return(i_retval)

define class tgsve_brb as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_GSVE_MRT = .NULL.
  w_CODBAN = space(10)
  w_NUMCOR = space(25)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Imposta C/C sulle righe del dettaglio paramento (da GSVE_MDV)
    this.w_PADRE = This.oParentObject
    this.w_GSVE_MRT = this.w_PADRE.GSVE_MRT
    this.w_CODBAN = this.w_PADRE.o_MVCODBAN
    this.w_NUMCOR = this.w_PADRE.o_MVNUMCOR
    if this.oParentObject.w_MVFLSCAF="S"
      * --- Scadenze Confermate
      if (empty(this.oParentObject.w_MVCODBAN) or empty(this.oParentObject.w_MVNUMCOR)) and not empty(this.w_NUMCOR)
        this.w_GSVE_MRT.MarkPos()     
        this.w_GSVE_MRT.FirstRow()     
        do while not this.w_GSVE_MRT.Eof_Trs()
          this.w_GSVE_MRT.SetRow()     
          if this.w_GSVE_MRT.CheckCC()
            this.w_GSVE_MRT.w_RSBANAPP = this.w_CODBAN
            this.w_GSVE_MRT.w_DESBAN1 = this.w_PADRE.w_DESBAN
            this.w_GSVE_MRT.w_RSCONCOR = this.w_NUMCOR
          endif
          this.w_GSVE_MRT.SaveRow()     
          this.w_GSVE_MRT.NextRow()     
        enddo
        this.w_GSVE_MRT.RePos()     
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
