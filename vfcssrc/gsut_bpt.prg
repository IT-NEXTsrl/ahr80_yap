* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bpt                                                        *
*              Creazione postin con allegata gestione                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-08                                                      *
* Last revis.: 2010-05-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR,w_NOMGEST
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bpt",oParentObject,m.w_CURSOR,m.w_NOMGEST)
return(i_retval)

define class tgsut_bpt as StdBatch
  * --- Local variables
  w_CURSOR = space(15)
  w_NOMGEST = space(100)
  w_POSTIN = .NULL.
  w_OBJGEST = .NULL.
  w_OLDSCHED = space(1)
  w_NUMKEY = 0
  w_NUMFIELD = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione postin con allegata gestione
    if (VARTYPE(g_DisableNewPostIt)="L" And g_DisableNewPostIt) Or VARTYPE(g_DisableNewPostIt)="U"
      * --- Creo postin
      this.w_POSTIN=createobject("postit","new",i_ServerConn[1,2])
      * --- Apertura gestione nascosta e valorizzazione chiavi
      if Vartype(this.w_NOMGEST)="C"
        Local L_MACRO
        if At("(", this.w_NOMGEST)<>0
          L_MACRO = Alltrim(this.w_NOMGEST)
        else
          L_MACRO = Alltrim(this.w_NOMGEST) + "()"
        endif
        this.w_OLDSCHED = IIF(Vartype(g_SCHEDULER)="C", g_SCHEDULER, "N")
        g_SCHEDULER="S"
        this.w_OBJGEST = &L_MACRO
        if !(this.w_OBJGEST.bSec1)
          this.w_OBJGEST.ecpQuit()     
          this.w_OBJGEST = .NULL.
          this.w_POSTIN = .NULL.
          g_SCHEDULER= this.w_OLDSCHED
          i_retcode = 'stop'
          i_retval = .NULL.
          return
        endif
        * --- Nascondo la gestione
        this.w_OBJGEST.Left = _screen.Width + 100
        this.w_NUMFIELD = AFIELDS(L_AFIELDS, this.w_CURSOR)
        if USED(this.w_CURSOR)
          SELECT(this.w_CURSOR)
          LOCATE FOR 1=1
          do while Not(EOF())
            * --- Seleziono il record nella gestione
            this.w_OBJGEST.EcpFilter()     
            this.w_NUMKEY = this.w_NUMFIELD
            do while this.w_NUMKEY>0
               
 L_MACRO = "This.w_OBJGEST.w_" + L_AFIELDS[this.w_NUMKEY,1] + "='" + TRANSFORM(&L_AFIELDS[this.w_NUMKEY,1])+"'" 
 &L_MACRO
              this.w_NUMKEY = this.w_NUMKEY - 1
            enddo
            this.w_OBJGEST.EcpSave()     
            * --- Valorizzo le chiavi per poter far inserire la descrizione della gestione pi� la chiave
            this.w_OBJGEST.oPGFRM.Page1.oPag.cFile = this.w_OBJGEST.cFile
             
 SELECT(this.w_OBJGEST.cKeySet) 
 SCATTER TO this.w_OBJGEST.oPGFRM.Page1.oPag.xKey
            * --- Aggiungo la gestione al postit
            this.w_POSTIN.OPSTCNT.POSTITEDT.DragDrop(this.w_OBJGEST.oPGFRM.Page1.oPag, 0, 0)     
            SELECT(this.w_CURSOR)
            CONTINUE
          enddo
        endif
        this.w_OBJGEST.ecpQuit()     
        this.w_OBJGEST = .NULL.
        g_SCHEDULER= this.w_OLDSCHED
      endif
      i_retcode = 'stop'
      i_retval = this.w_POSTIN
      return
    endif
  endproc


  proc Init(oParentObject,w_CURSOR,w_NOMGEST)
    this.w_CURSOR=w_CURSOR
    this.w_NOMGEST=w_NOMGEST
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR,w_NOMGEST"
endproc
