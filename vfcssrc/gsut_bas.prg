* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bas                                                        *
*              Gestione libreria immagini                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2000-01-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Chiave,w_Programma,w_Operazione
* --- Area Manuale = Header
* --- gsut_bas
private w_STRCOM,w_STRRUN
w_STRCOM = ""
w_STRRUN = ""
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bas",oParentObject,m.w_Chiave,m.w_Programma,m.w_Operazione)
return(i_retval)

define class tgsut_bas as StdBatch
  * --- Local variables
  w_Chiave = space(10)
  w_Programma = space(10)
  w_Operazione = space(10)
  w_Volume = space(10)
  w_GenPat = space(10)
  w_ImgPat = space(10)
  w_LibPat = space(10)
  w_LibExe = space(10)
  w_Librer = space(10)
  w_Immagi = space(10)
  w_ChkBuffer = space(10)
  w_MemFile = space(10)
  w_CritPeri = space(1)
  w_OkScan = .f.
  w_OkElim = .f.
  w_OKSCA2 = .f.
  w_OkDDE = .f.
  w_RunDDE = .f.
  w_TmpPat = space(10)
  w_Archivio = space(2)
  w_Periodo = space(6)
  w_TESTOPT = .f.
  w_TIPIMM = space(10)
  w_INDEX = 0
  w_AGAIN = .f.
  w_GO = .f.
  w_SELIMM = space(10)
  * --- WorkFile variables
  LIB_IMMA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GESTIONE LIBRERIA Immagini
    * --- Parametri
    * --- Chiave primaria del record selezionato sul padre
    * --- Nome del programma chiamante
    * --- Operazione richiesta ( C=Cattura e V=Visualizza )
    * --- Variabili locali relative alla configurazione della libreria
    this.w_Volume = ""
    * --- Nome volume
    this.w_GenPat = sys(5)+sys(2003)+"\IMMAGINI\"
    * --- Path cartella immagini
    this.w_ImgPat = this.w_GenPat+alltrim(i_codazi)+"\"
    * --- Path immagini
    this.w_LibPat = "C:\SCANVIEW\"
    * --- Path eseguibile da lanciare
    this.w_LibExe = "SCANVIEW"
    * --- Eseguibile da lanciare senza path
    this.w_Librer = trim(this.w_LibPat)+trim(this.w_LibExe)+".EXE"
    * --- Eseguibile da lanciare con path
    this.w_Immagi = ""
    * --- Nome immagine
    this.w_ChkBuffer = ""
    * --- Handle del file di test esistenza directory
    this.w_MemFile = ""
    * --- Nome file mem per test esistenza directory
    this.w_CritPeri = space(1)
    * --- Criterio di archiviazione per periodo (N,M,T,Q,S,Q)
    * --- Variabili Locali
    this.w_OkScan = .T.
    this.w_OkElim = .T.
    this.w_OKSCA2 = ""
    this.w_OkDDE = .f.
    this.w_RunDDE = .f.
    this.w_TmpPat = ""
    this.w_Archivio = space(2)
    this.w_Periodo = space(6)
    * --- Chiamata da 'Libreria Immagini' per caricamento tipo file su registro di configurazione
    do case
      case this.w_Chiave="CONFIG"
        if !empty(.w_LIPATEXE).and.!empty(.w_LINOMEXE).and.file(alltrim(.w_LIPATEXE)+alltrim(.w_LINOMEXE)+".EXE")
          w_STRCOM = this.w_Programma
          w_STRRUN = "RUN /N "+w_STRCOM+" 4"
          &w_STRRUN
        else
          ah_msg("Configurazione non accessibile. Verificare impostazioni caricate in libreria immagini" ,.f.)
        endif
        i_retcode = 'stop'
        return
    endcase
    * --- Lettura impostazioni da archivio "Librerie Immagini"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Composizione nome immagine
    do case
      case this.w_Programma = "GSMA_AAR" OR this.w_Programma = "GSMA_AAS" OR (g_APPLICATION = "ad hoc ENTERPRISE" AND this.w_Programma = "GSMA_AAC" )
        * --- Caso in cui il codice sia relativo a un articolo o a un servizio
        this.w_Immagi = trim(this.w_ImgPat)+"A"+alltrim(this.w_Chiave)
      case this.w_Programma = "GSVE_MDV"
        * --- Caso in cui il codice � relativo a un documento
        this.w_Immagi = trim(this.w_ImgPat)+"D"+alltrim(this.w_Chiave)
      case this.w_Programma = "GSCG_MPN"
        * --- Caso in cui il codice � relativo a un documento di primanota
        this.w_Immagi = trim(this.w_ImgPat)+"P"+alltrim(this.w_Chiave)
      case .t.
        this.w_Immagi = trim(this.w_ImgPat)+"X"+alltrim(this.w_Chiave)
    endcase
    * --- Esecuzione operazione richiesta
    do case
      case this.w_Operazione="C"
        * --- Variabile per gestione funzioni OK/F10 e ANNULLA/ESC
        this.w_TESTOPT = .f.
        * --- Selezione tipo acquisizione
        do GSUT_KSW with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_TESTOPT
          * --- Cattura Immagini
          if file( this.w_Librer )
            * --- Verifica Esistenza Immagine
            this.w_OkScan = .T.
            if file( this.w_Immagi+".001" )
              this.w_OkScan = AH_YESNO("Immagine gi� memorizzata. Proseguo?")
            endif
            * --- Verifica Esistenza Directory e Creazione
            if this.w_OkScan=.T. .and. (.not. file( this.w_ImgPat+"ImgCheck.MEM" ))
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if w_TIPACQ="E"
              this.Pag4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Lancia la libreria
              if this.w_OkScan=.T. .and. right(this.w_Immagi,8)#"00000000"
                ah_msg("Caricamento scan & view...")
                if empty(this.w_Volume)
                  w_TmpCmd = this.w_Librer+" "+this.w_Immagi+" 2"
                  RUN /N &w_TmpCmd
                else
                  w_TmpCmd = this.w_Librer+" "+this.w_Immagi+" 2 "+this.w_Volume
                  RUN /N &w_TmpCmd
                endif
              endif
            endif
          else
            AH_ERRORMSG("Libreria scan & view non trovata",48)
          endif
        endif
      case this.w_Operazione="V"
        * --- Visualizza Immagini
        if file( this.w_Librer )
          if file( this.w_Immagi+".001" )
            ah_msg("Caricamento immagine...")
            this.w_OkDDE = .T.
            if empty(this.w_Volume)
              if this.w_OkDDE
                this.w_RunDDE = DDE_Run(this.w_Librer,"SCANVIEW","DDEServer_ScanView",this.w_Immagi+" 1")
              else
                w_TmpCmd = this.w_Librer+" "+this.w_Immagi+" 1"
                RUN /N &w_TmpCmd
              endif
            else
              if this.w_OkDDE
                this.w_RunDDE = DDE_Run(this.w_Librer,"SCANVIEW","DDEServer_ScanView",this.w_Immagi+" 1 "+this.w_Volume)
              else
                w_TmpCmd = this.w_Librer+" "+this.w_Immagi+" 1 "+this.w_Volume
                RUN /N &w_TmpCmd
              endif
            endif
          else
            AH_ERRORMSG("Immagine non memorizzata",48)
          endif
        else
          AH_ERRORMSG("Libreria scan & view non trovata",48)
        endif
      case this.w_Operazione="R"
        * --- Rimuove Scan & View dalla memoria
        ah_msg("Rilascio memoria libreria scan & view...")
        w_TmpCmd = this.w_Librer+" 0"
        RUN /N &w_TmpCmd
      case this.w_Operazione="B"
        * --- Converte i TIF in BMP
        if file( this.w_Librer )
          this.w_OkScan = this.w_Immagi+".001"
          this.w_OKSCA2 = this.w_Immagi+".BMP"
          if file( this.w_OkScan )
            w_TmpCmd = this.w_Librer+" "+this.w_OkScan+" 3 "+this.w_OkSca2
            RUN /N &w_TmpCmd
            w_TmpCmd = this.w_Librer+" 0"
            RUN /N &w_TmpCmd
          endif
        endif
      case left(this.w_Operazione,3)="D"
        * --- Elimina le immagini dal disco
        if file( this.w_Immagi+".001" )
          this.w_OkElim = .T.
          this.w_OkElim = AH_YESNO("Elimino immagini %1.*?","",this.w_Immagi)
          if this.w_OkElim=.T.
            this.w_OkElim = this.w_Immagi+".*"
            erase ( this.w_OkElim )
          endif
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione directory immagini (LA GENERALE)
    if .not. file( this.w_GenPat+"ImgCheck.MEM" )
      this.w_OkScan = AH_YESNO("Percorso %1 inesistente. Creo?","",trim(this.w_GenPat))
      if this.w_OkScan=.T.
        this.w_TmpPat = left(this.w_GenPat,len(this.w_GenPat)-1)
        md ( this.w_TmpPat )
        this.w_MemFile = this.w_GenPat+"ImgCheck"
        this.w_ChkBuffer = fcreate( this.w_GenPat+"ImgCheck.MEM" )
        if .not. this.w_ChkBuffer<0
          this.w_ChkBuffer = fclose( this.w_ChkBuffer )
          save all like w_PatIm* to ( this.w_MemFile )
        else
          this.w_OkScan = .F.
          AH_ERRORMSG("Impossibile creare la directory",48)
        endif
      endif
    endif
    * --- Creazione sottodirectory immagini (QUELLE PER AZIENDA E TIPO)
    if this.w_OkScan=.T.
      this.w_OkScan = AH_YESNO("Percorso %1 inesistente. Creo?","",trim(this.w_ImgPat))
      if this.w_OkScan=.T.
        this.w_TmpPat = left(this.w_ImgPat,len(this.w_ImgPat)-1)
        md ( this.w_TmpPat )
        this.w_MemFile = this.w_ImgPat+"ImgCheck"
        this.w_ChkBuffer = fcreate( this.w_ImgPat+"ImgCheck.MEM" )
        if .not. this.w_ChkBuffer<0
          this.w_ChkBuffer = fclose( this.w_ChkBuffer )
          save all like w_PatIm* to ( this.w_MemFile )
        else
          this.w_OkScan = .F.
          AH_ERRORMSG("Impossibile creare la directory",48)
        endif
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura archivio "Librerie Immagini"
    * --- Riconoscimento archivio collegato
    this.w_Archivio = space(2)
    this.w_Periodo = date()
    do case
      case this.w_Programma = "GSMA_AAR"
        this.w_Archivio = "AR"
        this.w_Periodo = this.oParentObject.w_UTDC
      case this.w_Programma = "GSVE_MDV"
        this.w_Archivio = "DO"
        this.w_Periodo = this.oParentObject.w_MVDATDOC
      case this.w_Programma = "GSCG_MPN"
        this.w_Archivio = "PN"
        this.w_Periodo = this.oParentObject.w_PNDATREG
    endcase
    * --- Lettura impostazioni
    this.w_CritPeri = space(1)
    * --- Read from LIB_IMMA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LIB_IMMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIB_IMMA_idx,2],.t.,this.LIB_IMMA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LIDATRIF"+;
        " from "+i_cTable+" LIB_IMMA where ";
            +"LICODICE = "+cp_ToStrODBC("SCANVIEW");
            +" and LITIPARC = "+cp_ToStrODBC(this.w_Archivio);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LIDATRIF;
        from (i_cTable) where;
            LICODICE = "SCANVIEW";
            and LITIPARC = this.w_Archivio;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CritPeri = NVL(cp_ToDate(_read_.LIDATRIF),cp_NullValue(_read_.LIDATRIF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_CritPeri<>space(1)
      * --- Le impostazioni vengono lette solo se ha trovato il record desiderato. Serve per non perdere i valori di default.
      * --- Read from LIB_IMMA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LIB_IMMA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIB_IMMA_idx,2],.t.,this.LIB_IMMA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LINOMEXE,LIPATEXE,LIPATIMG,LIVOLUME"+;
          " from "+i_cTable+" LIB_IMMA where ";
              +"LICODICE = "+cp_ToStrODBC("SCANVIEW");
              +" and LITIPARC = "+cp_ToStrODBC(this.w_Archivio);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LINOMEXE,LIPATEXE,LIPATIMG,LIVOLUME;
          from (i_cTable) where;
              LICODICE = "SCANVIEW";
              and LITIPARC = this.w_Archivio;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LibExe = NVL(cp_ToDate(_read_.LINOMEXE),cp_NullValue(_read_.LINOMEXE))
        this.w_LibPat = NVL(cp_ToDate(_read_.LIPATEXE),cp_NullValue(_read_.LIPATEXE))
        this.w_GenPat = NVL(cp_ToDate(_read_.LIPATIMG),cp_NullValue(_read_.LIPATIMG))
        this.w_Volume = NVL(cp_ToDate(_read_.LIVOLUME),cp_NullValue(_read_.LIVOLUME))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Aggiustamento valori letti
    this.w_LibExe = upper(alltrim(this.w_LibExe))
    this.w_LibPat = alltrim(this.w_LibPat) + iif( right( alltrim( this.w_LibPat ) ,1) <>"\" , "\" , "" )
    this.w_Librer = trim(this.w_LibPat) + trim(this.w_LibExe) + iif( right( this.w_LibExe,4 )<>".EXE", ".EXE", "")
    this.w_GenPat = alltrim(this.w_GenPat) + iif( right( alltrim( this.w_GenPat ) ,1) <>"\" , "\" , "" )
    * --- Calcolo nome directory memorizzazione immagini
    this.w_ImgPat = this.w_GenPat+alltrim(i_codazi)
    do case
      case this.w_CritPeri = "M"
        * --- Raggruppamento per mesi
        this.w_ImgPat = this.w_ImgPat + right("00"+alltrim(str(month(this.w_Periodo),2,0)), 2) + str(year(this.w_Periodo),4,0)
      case this.w_CritPeri = "T"
        * --- Raggruppamento per trimestri
        do case
          case month(this.w_Periodo) > 0 .and. month(this.w_Periodo) < 4
            this.w_ImgPat = this.w_ImgPat + "T1" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 3 .and. month(this.w_Periodo) < 7
            this.w_ImgPat = this.w_ImgPat + "T2" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 6 .and. month(this.w_Periodo) < 10
            this.w_ImgPat = this.w_ImgPat + "T3" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 9
            this.w_ImgPat = this.w_ImgPat + "T4" + str(year(this.w_Periodo),4,0)
        endcase
      case this.w_CritPeri = "Q"
        * --- Raggruppamento per quadrimestri
        do case
          case month(this.w_Periodo) > 0 .and. month(this.w_Periodo) < 5
            this.w_ImgPat = this.w_ImgPat + "Q1" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 4 .and. month(this.w_Periodo) < 9
            this.w_ImgPat = this.w_ImgPat + "Q2" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 8
            this.w_ImgPat = this.w_ImgPat + "Q3" + str(year(this.w_Periodo),4,0)
        endcase
      case this.w_CritPeri = "S"
        * --- Raggruppamento per semestri
        this.w_ImgPat = this.w_ImgPat + iif( month(this.w_Periodo) >6 , "S2", "S1") + str(year(this.w_Periodo),4,0)
      case this.w_CritPeri = "A"
        * --- Raggruppamento per anni
        this.w_ImgPat = this.w_ImgPat + str(year(this.w_Periodo),4,0)
    endcase
    this.w_ImgPat = this.w_ImgPat+"\"
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OkScan=.T. .and. right(this.w_Immagi,8)#"00000000"
      * --- Lettura impostazioni Scan & View su registro di configurazione
      this.w_TIPIMM = ReadReg()
      if this.w_TIPIMM="Fail"
        ah_msg("Impossibile recuperare la configurazione di scan & view",.f.)
      else
        * --- Apro Explorer per selezionare le immagini da associare
        this.w_INDEX = 1
        this.w_AGAIN = .t.
        do while this.w_AGAIN
          this.w_GO = .t.
          this.w_SELIMM = GETFILE(this.w_TIPIMM,ah_Msgformat("Immagine"),ah_Msgformat("Seleziona"),0, ah_Msgformat("Immagine %1",RIGHT("000"+ALLTRIM(STR(this.w_index,3,0)),3)) )
          if !empty(this.w_SELIMM)
            if File(this.w_SELIMM)
              if File((this.w_IMMAGI+"."+RIGHT("000"+ALLTRIM(STR(this.w_index,3,0)),3)))
                if !ah_yesno ("Immagine %1 esistente. Sovrascrivo?","",this.w_IMMAGI+chr(46)+RIGHT("000"+ALLTRIM(STR(this.w_index,3,0)),3))
                  this.w_GO = .f.
                endif
              endif
              if this.w_GO
                COPY FILE (this.w_SELIMM) to (this.w_IMMAGI+"."+RIGHT("000"+ALLTRIM(STR(this.w_index,3,0)),3))
                ah_msg( "Immagine %1 copiata in %2",.f.,.f.,.f.,this.w_SELIMM,this.w_IMMAGI+chr(46)+RIGHT("000"+ALLTRIM(STR(this.w_index,3,0)),3))
              endif
            else
              ah_msg("File %1 inesistente" ,.f.,.f.,.f.,this.w_SELIMM)
            endif
          endif
          if file((this.w_IMMAGI+"."+RIGHT("000"+ALLTRIM(STR(this.w_index,3,0)),3)))
            this.w_INDEX = this.w_INDEX+1
            if !ah_yesno("Proseguire con la selezione della pagina %1?","",RIGHT("000"+ALLTRIM(STR(this.w_index,3,0)),3))
              this.w_AGAIN = .f.
            endif
          else
            this.w_AGAIN = .f.
          endif
        enddo
      endif
    endif
  endproc


  proc Init(oParentObject,w_Chiave,w_Programma,w_Operazione)
    this.w_Chiave=w_Chiave
    this.w_Programma=w_Programma
    this.w_Operazione=w_Operazione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LIB_IMMA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Chiave,w_Programma,w_Operazione"
endproc
