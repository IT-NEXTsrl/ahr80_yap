* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bzm                                                        *
*              Doc. da schede documenti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2000-04-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pPARAME
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bzm",oParentObject,m.pSERIAL,m.pPARAME)
return(i_retval)

define class tgsve_bzm as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  pPARAME = space(3)
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia i Documenti da Visualizzazione Documenti (da GSVE_SZM e GSVE_MDV)
    *     Modifica Spese Trasporto Intra (GSAR_KMD)
    * --- Questo oggetto sar� definito come Documento
    if this.pPARAME="FIDO"
      * --- Gestione Fido
      this.w_PROG = GSVE_ACF()
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_PROG.bSec1)
        Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
        i_retcode = 'stop'
        return
      endif
      this.w_PROG.EcpFIlter()     
      this.w_PROG.w_FICODCLI = this.pSERIAL
      this.w_PROG.EcpSave()     
    else
      gsar_bzm(this,this.pSERIAL,-20)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  proc Init(oParentObject,pSERIAL,pPARAME)
    this.pSERIAL=pSERIAL
    this.pPARAME=pPARAME
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pPARAME"
endproc
