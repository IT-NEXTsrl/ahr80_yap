* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bwr                                                        *
*              Avvio report wizard                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-31                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCURCAMPI,pPATHREP,pCURPARAM,pNOPARPAG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bwr",oParentObject,m.pCURCAMPI,m.pPATHREP,m.pCURPARAM,m.pNOPARPAG)
return(i_retval)

define class tgsut_bwr as StdBatch
  * --- Local variables
  pCURCAMPI = space(10)
  pPATHREP = space(254)
  pCURPARAM = space(40)
  pNOPARPAG = .f.
  w_MASK = .NULL.
  w_CURCAMPI = space(10)
  w_PATHREPORT = space(254)
  w_CURPARAM = space(10)
  w_NOPARPAG = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PATHREPORT = iif(!empty(this.pPATHREP),this.pPATHREP,addbs(SYS(5)+SYS(2003))+"RepWiz.frx")
    if !USED(this.pCURCAMPI)
      i_retcode = 'stop'
      i_retval = " "
      return
    endif
    this.w_CURCAMPI = this.pCURCAMPI
    this.w_CURPARAM = this.pCURPARAM
    this.w_NOPARPAG = IIF(VARTYPE(this.pNOPARPAG)="U", .F., this.pNOPARPAG )
    this.w_MASK = GSUT_KRW(this)
    this.w_MASK = .null.
    i_retcode = 'stop'
    i_retval = this.w_PATHREPORT
    return
  endproc


  proc Init(oParentObject,pCURCAMPI,pPATHREP,pCURPARAM,pNOPARPAG)
    this.pCURCAMPI=pCURCAMPI
    this.pPATHREP=pPATHREP
    this.pCURPARAM=pCURPARAM
    this.pNOPARPAG=pNOPARPAG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCURCAMPI,pPATHREP,pCURPARAM,pNOPARPAG"
endproc
