* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bco                                                        *
*              Bilancio per commessa                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_26]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-22                                                      *
* Last revis.: 2000-06-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bco",oParentObject)
return(i_retval)

define class tgsca_bco as StdBatch
  * --- Local variables
  w_CAMBIO = 0
  w_SIMBOLO = space(5)
  * --- WorkFile variables
  VALUTE_idx=0
  RIPATMP1_idx=0
  TMP_ANA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Bilancio per Commessa (da GSCA_STC)
    *     GSCA_STC.VQR => Manuali Originari
    *     GSCA1STC.VQR => Primanota Originari
    *     GSCA2STC.VQR => Documenti Originari
    *     GSCA4STC.VQR => Manuali Ripartiti
    *     GSCA5STC.VQR => Primanota Ripartiti
    *     GSCA6STC.VQR => Documenti Ripartiti
    if empty( this.oParentObject.w_DATA1 ) OR empty( this.oParentObject.w_DATA2 )
      ah_ErrorMsg("Intervallo di date non valido",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Creazione Tabella Temporanea
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca_stc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Stampo sempre nella valuta di conto !
    * --- Questo cambio � utilizzato per eventuali cambi - se gli importi tutti nella valuta di conto non faccio cambi
    * --- Calcolo il cambio con la valuta di conto - leggo anche la valuta
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAMBIO = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Calcolo gli importi in base al periodo se attivo il check
    if this.oParentObject.w_COMPET ="S"
      * --- Per calcolare il RATEO moltipiclo l'importo per il numero di giorni della competenza interni al periodo
      * --- il risultato lo divido l'importo per il numero di giorni della competenza
      * --- Create temporary table TMP_ANA
      i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gsca3stc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_ANA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMP_ANA
      i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.RIPATMP1_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.TMP_ANA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Esecuzione Query Output Utente (su Tabella Temporanea)
    vq_exec(alltrim(this.oParentObject.w_OQRY),this,"__tmp__")
    * --- Variabili utilizzate nel Report
    L_TIPO=this.oParentObject.w_TIPO
    L_CONTO=this.oParentObject.w_CONTO
    L_CAMBIO=this.w_CAMBIO
    L_CONTO1=this.oParentObject.w_CONTO1
    L_PROVE=this.oParentObject.w_PROVE
    L_SIMBOLO=this.w_SIMBOLO
    L_DATA1=this.oParentObject.w_DATA1
    L_COMPET=this.oParentObject.w_COMPET
    L_DATA2=this.oParentObject.w_DATA2
    L_SUCOMMES=this.oParentObject.w_SUCOMMES
    * --- Lancio Stampa Output Utente
    CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP), " ", this )
    * --- Eliminazione Tabelle Temporanee
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    * --- Drop temporary table TMP_ANA
    i_nIdx=cp_GetTableDefIdx('TMP_ANA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ANA')
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='*RIPATMP1'
    this.cWorkTables[3]='*TMP_ANA'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
