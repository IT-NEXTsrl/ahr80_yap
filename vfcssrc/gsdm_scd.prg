* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_scd                                                        *
*              Stampa classi documentali                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-03                                                      *
* Last revis.: 2013-01-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_scd",oParentObject))

* --- Class definition
define class tgsdm_scd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 600
  Height = 231
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-01-21"
  HelpContextID=90817641
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  PROMDOCU_IDX = 0
  PRODCLAS_IDX = 0
  PROMCLAS_IDX = 0
  cPrg = "gsdm_scd"
  cComment = "Stampa classi documentali"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPARCH = space(1)
  o_TIPARCH = space(1)
  w_TABARCRA = space(20)
  w_TABRIFER = space(15)
  w_CLASDOCIN = space(15)
  w_DESCLASIN = space(50)
  w_CLASDOCFI = space(15)
  w_DESCLASFI = space(50)
  w_CLATIPARC = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_scdPag1","gsdm_scd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPARCH_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='PROMDOCU'
    this.cWorkTables[2]='PRODCLAS'
    this.cWorkTables[3]='PROMCLAS'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPARCH=space(1)
      .w_TABARCRA=space(20)
      .w_TABRIFER=space(15)
      .w_CLASDOCIN=space(15)
      .w_DESCLASIN=space(50)
      .w_CLASDOCFI=space(15)
      .w_DESCLASFI=space(50)
      .w_CLATIPARC=space(1)
        .w_TIPARCH = ' '
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_TABARCRA))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TABRIFER))
          .link_1_3('Full')
        endif
        .w_CLASDOCIN = SPACE(15)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CLASDOCIN))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_CLASDOCFI = SPACE(15)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CLASDOCFI))
          .link_1_6('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
    this.DoRTCalc(7,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_TIPARCH<>.w_TIPARCH
            .w_CLASDOCIN = SPACE(15)
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_TIPARCH<>.w_TIPARCH
            .w_CLASDOCFI = SPACE(15)
          .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPARCH_1_1.visible=!this.oPgFrm.Page1.oPag.oTIPARCH_1_1.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TABARCRA
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TABARCRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDRIFTAB like "+cp_ToStrODBC(trim(this.w_TABARCRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CDRIFTAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDRIFTAB","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDRIFTAB',trim(this.w_TABARCRA))
          select CDRIFTAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDRIFTAB into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TABARCRA)==trim(_Link_.CDRIFTAB) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TABARCRA) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDRIFTAB',cp_AbsName(oSource.parent,'oTABARCRA_1_2'),i_cWhere,'',"Tabelle archivizione rapida",'GSDM_TAR.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDRIFTAB";
                     +" from "+i_cTable+" "+i_lTable+" where CDRIFTAB="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDRIFTAB',oSource.xKey(1))
            select CDRIFTAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TABARCRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDRIFTAB";
                   +" from "+i_cTable+" "+i_lTable+" where CDRIFTAB="+cp_ToStrODBC(this.w_TABARCRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDRIFTAB',this.w_TABARCRA)
            select CDRIFTAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TABARCRA = NVL(_Link_.CDRIFTAB,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_TABARCRA = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDRIFTAB,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TABARCRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TABRIFER
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRODCLAS_IDX,3]
    i_lTable = "PRODCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRODCLAS_IDX,2], .t., this.PRODCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRODCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TABRIFER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRODCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDTABKEY like "+cp_ToStrODBC(trim(this.w_TABRIFER)+"%");

          i_ret=cp_SQL(i_nConn,"select CDTABKEY";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDTABKEY","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDTABKEY',trim(this.w_TABRIFER))
          select CDTABKEY;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDTABKEY into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TABRIFER)==trim(_Link_.CDTABKEY) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TABRIFER) and !this.bDontReportError
            deferred_cp_zoom('PRODCLAS','*','CDTABKEY',cp_AbsName(oSource.parent,'oTABRIFER_1_3'),i_cWhere,'',"Tabelle di riferimento per la ricerca",'GSDM_TRR.PRODCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDTABKEY";
                     +" from "+i_cTable+" "+i_lTable+" where CDTABKEY="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDTABKEY',oSource.xKey(1))
            select CDTABKEY;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TABRIFER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDTABKEY";
                   +" from "+i_cTable+" "+i_lTable+" where CDTABKEY="+cp_ToStrODBC(this.w_TABRIFER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDTABKEY',this.w_TABRIFER)
            select CDTABKEY;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TABRIFER = NVL(_Link_.CDTABKEY,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_TABRIFER = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRODCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDTABKEY,1)
      cp_ShowWarn(i_cKey,this.PRODCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TABRIFER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLASDOCIN
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASDOCIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_CLASDOCIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_CLASDOCIN))
          select CDCODCLA,CDDESCLA,CDTIPARC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASDOCIN)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLASDOCIN) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oCLASDOCIN_1_4'),i_cWhere,'',"Classi documentali",'GSDM1TAR.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDTIPARC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASDOCIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_CLASDOCIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_CLASDOCIN)
            select CDCODCLA,CDDESCLA,CDTIPARC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASDOCIN = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLASIN = NVL(_Link_.CDDESCLA,space(50))
      this.w_CLATIPARC = NVL(_Link_.CDTIPARC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLASDOCIN = space(15)
      endif
      this.w_DESCLASIN = space(50)
      this.w_CLATIPARC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CLASDOCIN<=.w_CLASDOCFI) OR EMPTY(.w_CLASDOCFI)) AND ((NOT EMPTY(.w_TIPARCH) AND (.w_CLATIPARC=.w_TIPARCH)) OR EMPTY(.w_TIPARCH))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice della classe documentale iniziale � maggiore di quello finale o con differente tipo di archiviazione oppure inesistente")
        endif
        this.w_CLASDOCIN = space(15)
        this.w_DESCLASIN = space(50)
        this.w_CLATIPARC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASDOCIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLASDOCFI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASDOCFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_CLASDOCFI)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_CLASDOCFI))
          select CDCODCLA,CDDESCLA,CDTIPARC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASDOCFI)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLASDOCFI) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oCLASDOCFI_1_6'),i_cWhere,'',"Classi documentali",'GSDM1TAR.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDTIPARC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASDOCFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_CLASDOCFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_CLASDOCFI)
            select CDCODCLA,CDDESCLA,CDTIPARC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASDOCFI = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLASFI = NVL(_Link_.CDDESCLA,space(50))
      this.w_CLATIPARC = NVL(_Link_.CDTIPARC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLASDOCFI = space(15)
      endif
      this.w_DESCLASFI = space(50)
      this.w_CLATIPARC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CLASDOCIN<=.w_CLASDOCFI) OR EMPTY(.w_CLASDOCIN)) AND ((NOT EMPTY(.w_TIPARCH) AND (.w_CLATIPARC=.w_TIPARCH)) OR EMPTY(.w_TIPARCH))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice della classe documentale finale � minore di quello iniziale o con differente tipo di archiviazione oppure inesistente")
        endif
        this.w_CLASDOCFI = space(15)
        this.w_DESCLASFI = space(50)
        this.w_CLATIPARC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASDOCFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPARCH_1_1.RadioValue()==this.w_TIPARCH)
      this.oPgFrm.Page1.oPag.oTIPARCH_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTABARCRA_1_2.value==this.w_TABARCRA)
      this.oPgFrm.Page1.oPag.oTABARCRA_1_2.value=this.w_TABARCRA
    endif
    if not(this.oPgFrm.Page1.oPag.oTABRIFER_1_3.value==this.w_TABRIFER)
      this.oPgFrm.Page1.oPag.oTABRIFER_1_3.value=this.w_TABRIFER
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASDOCIN_1_4.value==this.w_CLASDOCIN)
      this.oPgFrm.Page1.oPag.oCLASDOCIN_1_4.value=this.w_CLASDOCIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLASIN_1_5.value==this.w_DESCLASIN)
      this.oPgFrm.Page1.oPag.oDESCLASIN_1_5.value=this.w_DESCLASIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASDOCFI_1_6.value==this.w_CLASDOCFI)
      this.oPgFrm.Page1.oPag.oCLASDOCFI_1_6.value=this.w_CLASDOCFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLASFI_1_7.value==this.w_DESCLASFI)
      this.oPgFrm.Page1.oPag.oDESCLASFI_1_7.value=this.w_DESCLASFI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((.w_CLASDOCIN<=.w_CLASDOCFI) OR EMPTY(.w_CLASDOCFI)) AND ((NOT EMPTY(.w_TIPARCH) AND (.w_CLATIPARC=.w_TIPARCH)) OR EMPTY(.w_TIPARCH)))  and not(empty(.w_CLASDOCIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLASDOCIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice della classe documentale iniziale � maggiore di quello finale o con differente tipo di archiviazione oppure inesistente")
          case   not(((.w_CLASDOCIN<=.w_CLASDOCFI) OR EMPTY(.w_CLASDOCIN)) AND ((NOT EMPTY(.w_TIPARCH) AND (.w_CLATIPARC=.w_TIPARCH)) OR EMPTY(.w_TIPARCH)))  and not(empty(.w_CLASDOCFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLASDOCFI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice della classe documentale finale � minore di quello iniziale o con differente tipo di archiviazione oppure inesistente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPARCH = this.w_TIPARCH
    return

enddefine

* --- Define pages as container
define class tgsdm_scdPag1 as StdContainer
  Width  = 596
  height = 231
  stdWidth  = 596
  stdheight = 231
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPARCH_1_1 as StdCombo with uid="VEDPDIIJND",value=3,rtseq=1,rtrep=.f.,left=150,top=8,width=115,height=21;
    , ToolTipText = "Tipo archiviazione: manuale o automatica";
    , HelpContextID = 50104886;
    , cFormVar="w_TIPARCH",RowSource=""+"Manuale,"+"Automatica,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPARCH_1_1.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'A',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTIPARCH_1_1.GetRadio()
    this.Parent.oContained.w_TIPARCH = this.RadioValue()
    return .t.
  endfunc

  func oTIPARCH_1_1.SetRadio()
    this.Parent.oContained.w_TIPARCH=trim(this.Parent.oContained.w_TIPARCH)
    this.value = ;
      iif(this.Parent.oContained.w_TIPARCH=='M',1,;
      iif(this.Parent.oContained.w_TIPARCH=='A',2,;
      iif(this.Parent.oContained.w_TIPARCH=='',3,;
      0)))
  endfunc

  func oTIPARCH_1_1.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oTABARCRA_1_2 as StdField with uid="ENCITGCUYJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TABARCRA", cQueryName = "TABARCRA",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tabella di riferimento per classe (x archiviazione rapida)",;
    HelpContextID = 50045559,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=150, Top=33, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="PROMCLAS", oKey_1_1="CDRIFTAB", oKey_1_2="this.w_TABARCRA"

  func oTABARCRA_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oTABARCRA_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTABARCRA_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDRIFTAB',cp_AbsName(this.parent,'oTABARCRA_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tabelle archivizione rapida",'GSDM_TAR.PROMCLAS_VZM',this.parent.oContained
  endproc

  add object oTABRIFER_1_3 as StdField with uid="VJIHWWJMHQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TABRIFER", cQueryName = "TABRIFER",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Tabella di riferimento per la ricerca rapida",;
    HelpContextID = 92054152,;
   bGlobalFont=.t.,;
    Height=21, Width=182, Left=150, Top=59, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PRODCLAS", oKey_1_1="CDTABKEY", oKey_1_2="this.w_TABRIFER"

  func oTABRIFER_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTABRIFER_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTABRIFER_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRODCLAS','*','CDTABKEY',cp_AbsName(this.parent,'oTABRIFER_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tabelle di riferimento per la ricerca",'GSDM_TRR.PRODCLAS_VZM',this.parent.oContained
  endproc

  add object oCLASDOCIN_1_4 as StdField with uid="DIUBWGSIYU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CLASDOCIN", cQueryName = "CLASDOCIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice della classe documentale iniziale � maggiore di quello finale o con differente tipo di archiviazione oppure inesistente",;
    ToolTipText = "Codice classe documentale iniziale",;
    HelpContextID = 237871439,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=150, Top=85, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", oKey_1_1="CDCODCLA", oKey_1_2="this.w_CLASDOCIN"

  func oCLASDOCIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASDOCIN_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASDOCIN_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oCLASDOCIN_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Classi documentali",'GSDM1TAR.PROMCLAS_VZM',this.parent.oContained
  endproc

  add object oDESCLASIN_1_5 as StdField with uid="DKHYENYRVI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCLASIN", cQueryName = "DESCLASIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della classe documentale iniziale",;
    HelpContextID = 10402399,;
   bGlobalFont=.t.,;
    Height=21, Width=301, Left=285, Top=85, InputMask=replicate('X',50)

  add object oCLASDOCFI_1_6 as StdField with uid="RTRWRAAHEK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CLASDOCFI", cQueryName = "CLASDOCFI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice della classe documentale finale � minore di quello iniziale o con differente tipo di archiviazione oppure inesistente",;
    ToolTipText = "Codice classe documentale finale",;
    HelpContextID = 237871356,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=150, Top=111, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", oKey_1_1="CDCODCLA", oKey_1_2="this.w_CLASDOCFI"

  func oCLASDOCFI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASDOCFI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASDOCFI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oCLASDOCFI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Classi documentali",'GSDM1TAR.PROMCLAS_VZM',this.parent.oContained
  endproc

  add object oDESCLASFI_1_7 as StdField with uid="ISLGRVXUPX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCLASFI", cQueryName = "DESCLASFI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della classe documentale finale",;
    HelpContextID = 10402316,;
   bGlobalFont=.t.,;
    Height=21, Width=301, Left=285, Top=111, InputMask=replicate('X',50)


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=150, top=147, width=434,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87180006


  add object oBtn_1_9 as StdButton with uid="WDHBUTUXIR",left=485, top=180, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 50971942;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)) and (not empty(.w_OREP)))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="FGOJEBDDCO",left=538, top=180, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 83500218;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_11 as StdString with uid="ZTAJHPVLMP",Visible=.t., Left=24, Top=148,;
    Alignment=1, Width=120, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="QEJMRMWTBF",Visible=.t., Left=9, Top=10,;
    Alignment=1, Width=135, Height=18,;
    Caption="Tipo archiviazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="KRORNXLYHH",Visible=.t., Left=11, Top=34,;
    Alignment=1, Width=133, Height=18,;
    Caption="Tabella archiviazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="ADWDFYNKZJ",Visible=.t., Left=10, Top=60,;
    Alignment=1, Width=134, Height=18,;
    Caption="Tabella di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="WYCOWMGDHS",Visible=.t., Left=62, Top=88,;
    Alignment=1, Width=82, Height=18,;
    Caption="Da classe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="KUKGGJDHIB",Visible=.t., Left=59, Top=114,;
    Alignment=1, Width=84, Height=18,;
    Caption="A classe:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_scd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
