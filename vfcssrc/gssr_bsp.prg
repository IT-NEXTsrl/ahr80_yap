* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_bsp                                                        *
*              Storicizzazione primanota                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_462]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-06                                                      *
* Last revis.: 2001-06-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgssr_bsp",oParentObject)
return(i_retval)

define class tgssr_bsp as StdBatch
  * --- Local variables
  w_POSIZIONE = 0
  w_STAMPART = .f.
  w_NUMPART = 0
  w_nFllibg = 0
  w_nFlregi = 0
  w_nFlprov = 0
  w_nMov = 0
  w_EsePre = space(4)
  w_BlockError = space(0)
  w_Continue = .f.
  w_CodEse = space(4)
  w_CODSCA = space(10)
  w_CODPAR = space(10)
  w_RIGA = 0
  w_RowNum = 0
  w_RowNotStor = 0
  w_AttPas = space(1)
  w_MessError = space(100)
  w_MESS = space(250)
  w_CODAZI = space(5)
  w_STALIG = ctod("  /  /  ")
  w_DATAUT = ctod("  /  /  ")
  w_DATBLO2 = ctod("  /  /  ")
  w_OK = .f.
  w_rows = 0
  w_CONTACNT = 0
  w_LSSERIAL = space(10)
  w_LG_ESITO = space(1)
  w_LOGMSG = space(0)
  * --- WorkFile variables
  MOVISCOST_idx=0
  PNTSIVA_idx=0
  TMP_idx=0
  PARSTITE_idx=0
  PNTSMAST_idx=0
  SCASVARI_idx=0
  PNTSDETT_idx=0
  TMPTMOVICOST_idx=0
  CON_INDI_idx=0
  AZIENDA_idx=0
  DIS_TINT_idx=0
  MOVICOST_idx=0
  TMPTPAR_TITE_idx=0
  PAR_TITE_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  TMPTPNT_DETT_idx=0
  TMPTPNT_MAST_idx=0
  PNT_IVA_idx=0
  TMPTPNT_IVA_idx=0
  SALDICON_idx=0
  SCA_VARI_idx=0
  TMPTSCA_VARI_idx=0
  ZOOMPART_idx=0
  LOG_STOR_idx=0
  TMP_SALLOUB_idx=0
  ASSESTAM_idx=0
  TMPTCCM_DETT_idx=0
  TMPTPNT_CESP_idx=0
  ATTIDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Storico di Primanota
    this.w_STAMPART = .F.
    this.w_CodEse = this.oParentObject.w_CodEse
    this.w_Continue = .t.
    this.oParentObject.w_Msg = ""
    if !ah_YesNo("ATTENZIONE%0Verr� effettuata la storicizzazione di primanota%0%0Confermi l'elaborazione?")
      i_retcode = 'stop'
      return
    endif
    AddMsgNL("Elaborazione iniziata alle: %1",this.oparentobject,Time() )
    AddMsgNL("Fase 1: controlli",this.oparentobject)
    AddMsgNL("%1- Controllo movimenti da stampare sul libro giornale",this.oparentobject,space(5) )
    if g_APPLICATION <> "ADHOC REVOLUTION" 
      * --- Select from gssr_q08
      do vq_exec with 'gssr_q08',this,'_Curs_gssr_q08','',.f.,.t.
      if used('_Curs_gssr_q08')
        select _Curs_gssr_q08
        locate for 1=1
        do while not(eof())
        if _Curs_gssr_q08.CONTA > 0
          this.w_BlockError = nvl(this.w_BlockError,"")+ah_Msgformat("- Esistono movimenti da stampare sul libro giornale o sul registro IVA%0")
          this.w_Continue = .f.
        endif
          select _Curs_gssr_q08
          continue
        enddo
        use
      endif
    else
      AddMsgNL("%1- Controllo data consolidamento",this.oparentobject,space(5) )
      * --- Eseguo controllo date Consolidamento
      this.w_MESS = CHKCONS("PC",this.oParentObject.w_xesfinese,"S","N")
      if Not Empty(this.w_MESS)
        this.w_BlockError = nvl(this.w_BlockError,"")+this.w_MESS+chr(13)
        this.w_Continue = .f.
      endif
      this.w_CODAZI = I_CODAZI
      AddMsgNL("%1- Controllo movimenti da stampare sul libro giornale",this.oparentobject,space(5) )
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZSTALIG,AZDATAUT"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZSTALIG,AZDATAUT;
          from (i_cTable) where;
              AZCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
        this.w_DATAUT = NVL(cp_ToDate(_read_.AZDATAUT),cp_NullValue(_read_.AZDATAUT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.oParentObject.w_xesfinese>this.w_STALIG
        this.w_BlockError = nvl(this.w_BlockError,"")+ ah_Msgformat("- Esistono movimenti da stampare sul libro giornale%0")
        this.w_Continue = .f.
      endif
      AddMsgNL("%1- Controllo movimenti da stampare su registri IVA",this.oparentobject, space(5) )
      * --- Select from ATTIDETT
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ATCODATT,ATDATSTA  from "+i_cTable+" ATTIDETT ";
             ,"_Curs_ATTIDETT")
      else
        select ATCODATT,ATDATSTA from (i_cTable);
          into cursor _Curs_ATTIDETT
      endif
      if used('_Curs_ATTIDETT')
        select _Curs_ATTIDETT
        locate for 1=1
        do while not(eof())
        this.w_DATBLO2 = CP_TODATE(_Curs_ATTIDETT.ATDATSTA)
        if  (EMPTY(this.w_DATBLO2) OR this.w_DATBLO2<this.oParentObject.w_xesfinese)
          this.w_BlockError = nvl(this.w_BlockError,"")+ah_Msgformat("- Non ancora eseguita la stampa definitiva%0di tutti i registri associati all'attivit� %1%0", Alltrim(_Curs_ATTIDETT.ATCODATT))
          this.w_Continue = .f.
          Exit
        endif
          select _Curs_ATTIDETT
          continue
        enddo
        use
      endif
      AddMsgNL("%1- Controllo giroconti IVA autotrasportatori",this.oparentobject, space(5) )
      * --- Select from gssr_q31
      do vq_exec with 'gssr_q31',this,'_Curs_gssr_q31','',.f.,.t.
      if used('_Curs_gssr_q31')
        select _Curs_gssr_q31
        locate for 1=1
        do while not(eof())
        if this.w_DATAUT < cp_CharToDate("31-03-"+ALLTRIM(STR(VAl(this.w_CODESE)+1)))
          this.w_BlockError = nvl(this.w_BlockError,"")+ah_Msgformat("- Non ancora eseguito giroconto IVA autotrasportatori%0")
          this.w_Continue = .f.
          Exit
        endif
          select _Curs_gssr_q31
          continue
        enddo
        use
      endif
    endif
    * --- Controllo Movimenti Provvisori
    AddMsgNL("%1- Controllo esistenza movimenti provvisori",this.oparentobject, space(5) )
    * --- Select from gssr_q21
    do vq_exec with 'gssr_q21',this,'_Curs_gssr_q21','',.f.,.t.
    if used('_Curs_gssr_q21')
      select _Curs_gssr_q21
      locate for 1=1
      do while not(eof())
      if _Curs_gssr_q21.PROVVIS > 0
        this.w_BlockError = nvl(this.w_BlockError,"")+ah_Msgformat("- Esistono movimenti di primanota provvisori%0")
        this.w_Continue = .f.
      endif
        select _Curs_gssr_q21
        continue
      enddo
      use
    endif
    * --- Controllo apertura esercizio successivo a quello selezionato
    *     (Contollo solo conti generici)
    AddMsgNL("%1- Controllo apertura esercizio %1",this.oparentobject, space(5), this.oParentObject.w_esesucc)
    * --- Select from gssr_q09
    do vq_exec with 'gssr_q09',this,'_Curs_gssr_q09','',.f.,.t.
    if used('_Curs_gssr_q09')
      select _Curs_gssr_q09
      locate for 1=1
      do while not(eof())
      if _Curs_gssr_q09.APERTURA = 0
        this.w_BlockError = nvl(this.w_BlockError,"")+ah_Msgformat("- Manca apertura nell'esercizio %1%0",this.oParentObject.w_esesucc )
        this.w_Continue = .f.
      endif
        select _Curs_gssr_q09
        continue
      enddo
      use
    endif
    this.w_OK = .T.
    if this.w_Continue
      AddMsgNL("Fase 2: creazione tabelle temporanee%0",this.oparentobject)
      AddMsgNL("%1- Testata primanota",this.oparentobject, space(5) )
      i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","PNSERIAL"," ")
      * --- Con la query in union GSSR_Q22 prendo anche tutti i movimenti di
      *     primanota non ancora storicizzati degli esercizi precedenti
      * --- Create temporary table TMPTPNT_MAST
      i_nIdx=cp_AddTableDef('TMPTPNT_MAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gssr_q01',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPTPNT_MAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      AddMsgNL("%1- Dettaglio primanota",this.oparentobject, space(5) )
      i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","PNSERIAL,CPROWNUM"," ")
      * --- Create temporary table TMPTPNT_DETT
      i_nIdx=cp_AddTableDef('TMPTPNT_DETT') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gssr_q02',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPTPNT_DETT_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      AddMsgNL("%1- Dati IVA",this.oparentobject, space(5) )
      i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","IVSERIAL,CPROWNUM"," ")
      * --- Create temporary table TMPTPNT_IVA
      i_nIdx=cp_AddTableDef('TMPTPNT_IVA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gssr_q03',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPTPNT_IVA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      AddMsgNL("%1- Dati analitica",this.oparentobject, space(5) )
      i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","MRSERIAL,MRROWORD,CPROWNUM"," ")
      * --- Create temporary table TMPTMOVICOST
      i_nIdx=cp_AddTableDef('TMPTMOVICOST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gssr_q04',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPTMOVICOST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      AddMsgNL("%1- Scadenze varie",this.oparentobject, space(5) )
      i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","SCCODICE,SCCODSEC"," ")
      * --- Create temporary table TMPTSCA_VARI
      i_nIdx=cp_AddTableDef('TMPTSCA_VARI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gssr_q06',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPTSCA_VARI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      AddMsgNL("%1- Partite",this.oparentobject, space(5) )
      if g_APPLICATION <> "ADHOC REVOLUTION" 
        i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","PTSERIAL,PTORIGSA,PTROWORD,CPROWNUM"," ")
      else
        i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","PTSERIAL,PTROWORD,CPROWNUM"," ")
      endif
      i_IndexOracle2 = IIF(upper(CP_DBTYPE)="ORACLE","PTSERIAL,PTROWORD"," ")
      * --- Create temporary table TMPTPAR_TITE
      i_nIdx=cp_AddTableDef('TMPTPAR_TITE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gssr_q05',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPTPAR_TITE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Create temporary table ZOOMPART
      i_nIdx=cp_AddTableDef('ZOOMPART') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gssr_q17',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.ZOOMPART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      if  g_APPLICATION = "ADHOC REVOLUTION" 
        i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","ACSERIAL,ACMOVCES,ACTIPASS"," ")
        * --- Create temporary table TMPTPNT_CESP
        i_nIdx=cp_AddTableDef('TMPTPNT_CESP') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gssr_q29',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPTPNT_CESP_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        AddMsgNL("%1- Movimenti cespiti",this.oparentobject, space(5) )
      endif
      if g_APPLICATION = "ADHOC REVOLUTION" 
        AddMsgNL("%1- Movimenti di conto corrente",this.oparentobject, space(5) )
        i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","CCSERIAL,CCROWRIF,CPROWNUM"," ")
        * --- Create temporary table TMPTCCM_DETT
        i_nIdx=cp_AddTableDef('TMPTCCM_DETT') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gssr_q30',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPTCCM_DETT_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      AddMsgNL("Fase 3: controllo movimenti",this.oparentobject)
      * --- Creo tabella temporanea con le partite aperte dei movimenti storicizzati
      this.w_POSIZIONE = LEN(this.oParentObject.w_MSG)
      AddMsgNL("%1- Livello 1 - verifica partite aperte",this.oparentobject, space(5) )
      i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","PTSERIAL,PTROWORD"," ")
      * --- Select from ZOOMPART
      i_nConn=i_TableProp[this.ZOOMPART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2],.t.,this.ZOOMPART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select count(*) AS NUMPART  from "+i_cTable+" ZOOMPART ";
             ,"_Curs_ZOOMPART")
      else
        select count(*) AS NUMPART from (i_cTable);
          into cursor _Curs_ZOOMPART
      endif
      if used('_Curs_ZOOMPART')
        select _Curs_ZOOMPART
        locate for 1=1
        do while not(eof())
        this.w_NUMPART = NVL(NUMPART,0)
          select _Curs_ZOOMPART
          continue
        enddo
        use
      endif
      if this.w_NUMPART > 0
        this.w_STAMPART = .T.
        if Not ah_YesNo("Esistono partite aperte non storicizzabili; si vuole comunque procedere con la storicizzazione?")
          this.w_OK = .F.
        endif
        * --- Cancello tutte le scadenze che risulterebbero aperte
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Elimino le partite che restano aperte perch� legate a movimenti che contengono partite aperte
        *     le uniche partite che resteranno da storicizzare sono quelle che sono chiuse e il cui movimento
        *     di chiusura ha partite totalemtne chiuse
        * --- Cancello dal temporaneo delle scadenze varie i movimenti riferiti a partite aperte
        AddMsgNL("%1- Eliminazione movimenti legati a partite aperte",this.oparentobject, space(5) )
        * --- Delete from TMPTSCA_VARI
        i_nConn=i_TableProp[this.TMPTSCA_VARI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPTSCA_VARI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='SCCODICE,SCCODSEC'
          cp_CreateTempTable(i_nConn,i_cTempTable,"PTSERIAL AS SCCODICE, PTROWORD AS SCCODSEC "," from "+i_cQueryTable+" where PTROWORD=-1",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
                +" and "+i_cTable+".SCCODSEC = "+i_cQueryTable+".SCCODSEC";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
                +" and "+i_cTable+".SCCODSEC = "+i_cQueryTable+".SCCODSEC";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Cancello dal temporaneo dei dati di primanota i movimenti riferiti a partite aperte
        * --- Delete from TMPTPNT_IVA
        i_nConn=i_TableProp[this.TMPTPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPTPNT_IVA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='IVSERIAL'
          cp_CreateTempTable(i_nConn,i_cTempTable,"distinct PTSERIAL AS IVSERIAL "," from "+i_cQueryTable+" where PTROWORD>0",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".IVSERIAL = "+i_cQueryTable+".IVSERIAL";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".IVSERIAL = "+i_cQueryTable+".IVSERIAL";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        if  g_APPLICATION = "ADHOC REVOLUTION" 
          AddMsgNL("%1- Eliminazione movimenti legati al dettaglio cespiti",this.oparentobject, space(5) )
          GSSR_BCE(this,"DELE")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if  g_APPLICATION = "ADHOC REVOLUTION" 
          AddMsgNL("%1- Eliminazione movimenti legati al dettaglio conti correnti",this.oparentobject, space(5) )
          GSSR_BCC(this,"DELE")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Delete from TMPTMOVICOST
        i_nConn=i_TableProp[this.TMPTMOVICOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPTMOVICOST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='MRSERIAL'
          cp_CreateTempTable(i_nConn,i_cTempTable,"distinct PTSERIAL AS MRSERIAL "," from "+i_cQueryTable+" where PTROWORD>0",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from TMPTPNT_DETT
        i_nConn=i_TableProp[this.TMPTPNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPTPNT_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='PNSERIAL'
          cp_CreateTempTable(i_nConn,i_cTempTable,"distinct PTSERIAL AS PNSERIAL "," from "+i_cQueryTable+" where PTROWORD>0",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from TMPTPNT_MAST
        i_nConn=i_TableProp[this.TMPTPNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPTPNT_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='PNSERIAL'
          cp_CreateTempTable(i_nConn,i_cTempTable,"distinct PTSERIAL AS PNSERIAL "," from "+i_cQueryTable+" where PTROWORD>0",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      AddMsgNL("Fase 4: creo riferimenti saldi",this.oparentobject)
      * --- Create temporary table TMP_SALLOUB
      i_nIdx=cp_AddTableDef('TMP_SALLOUB') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('gssr_q07',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_SALLOUB_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      if this.w_OK
        * --- Try
        local bErr_0514BD48
        bErr_0514BD48=bTrsErr
        this.Try_0514BD48()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          this.w_BlockError = nvl(this.w_BlockError,"")+message()+chr(13)
        endif
        bTrsErr=bTrsErr or bErr_0514BD48
        * --- End
      endif
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if used("__TMP__")
       
 Select __TMP__ 
 Use
    endif
    * --- Drop temporary table TMPTPNT_MAST
    i_nIdx=cp_GetTableDefIdx('TMPTPNT_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTPNT_MAST')
    endif
    * --- Drop temporary table TMPTPNT_DETT
    i_nIdx=cp_GetTableDefIdx('TMPTPNT_DETT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTPNT_DETT')
    endif
    * --- Drop temporary table TMPTPNT_IVA
    i_nIdx=cp_GetTableDefIdx('TMPTPNT_IVA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTPNT_IVA')
    endif
    * --- Drop temporary table TMPTPNT_CESP
    i_nIdx=cp_GetTableDefIdx('TMPTPNT_CESP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTPNT_CESP')
    endif
    * --- Drop temporary table TMPTMOVICOST
    i_nIdx=cp_GetTableDefIdx('TMPTMOVICOST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTMOVICOST')
    endif
    * --- Drop temporary table TMPTPAR_TITE
    i_nIdx=cp_GetTableDefIdx('TMPTPAR_TITE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTPAR_TITE')
    endif
    * --- Drop temporary table TMP_SALLOUB
    i_nIdx=cp_GetTableDefIdx('TMP_SALLOUB')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_SALLOUB')
    endif
    * --- Drop temporary table ZOOMPART
    i_nIdx=cp_GetTableDefIdx('ZOOMPART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('ZOOMPART')
    endif
    * --- Drop temporary table TMPTSCA_VARI
    i_nIdx=cp_GetTableDefIdx('TMPTSCA_VARI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTSCA_VARI')
    endif
    * --- Drop temporary table TMPTCCM_DETT
    i_nIdx=cp_GetTableDefIdx('TMPTCCM_DETT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTCCM_DETT')
    endif
  endproc
  proc Try_0514BD48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    AddMsgNL("Fase 5: storicizzazione movimenti%0%1- Primanota (master)",this.oparentobject,space(5))
    * --- Insert into PNTSMAST
    i_nConn=i_TableProp[this.PNTSMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNTSMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPTPNT_MAST_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PNTSMAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if i_Rows=1
      AddMsgNL("%1- Primanota (master): %2 riga storicizzata",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    else
      AddMsgNL("%1- Primanota (master): %2 righe storicizzate",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    endif
    AddMsgNL("%1- Primanota (detail)",this.oparentobject, space(5) )
    * --- Insert into PNTSDETT
    i_nConn=i_TableProp[this.PNTSDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNTSDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPTPNT_DETT_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PNTSDETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if i_Rows=1
      AddMsgNL("%1- Primanota (detail): %2 riga storicizzata",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    else
      AddMsgNL("%1- Primanota (detail): %2 righe storicizzate",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    endif
    AddMsgNL("%1- Registrazioni IVA",this.oparentobject, space(5) )
    * --- Insert into PNTSIVA
    i_nConn=i_TableProp[this.PNTSIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNTSIVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPTPNT_IVA_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PNTSIVA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if i_Rows=1
      AddMsgNL("%1- Registrazioni IVA: %2 riga storicizzata",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    else
      AddMsgNL("%1- Registrazioni IVA: %2 righe storicizzate",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    endif
    if  g_APPLICATION = "ADHOC REVOLUTION" 
      AddMsgNL("%1- Cespiti",this.oparentobject, space(5) )
      GSSR_BCE(this,"INSE")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if i_Rows=1
        AddMsgNL("%1- Cespiti: %2 riga storicizzata",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
      else
        AddMsgNL("%1- Cespiti: %2 righe storicizzate",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
      endif
    endif
    if  g_APPLICATION = "ADHOC REVOLUTION" 
      AddMsgNL("%1- Conti correnti",this.oparentobject, space(5) )
      GSSR_BCC(this,"INSE")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if i_Rows=1
        AddMsgNL("%1- Conti correnti: %2 riga storicizzata",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
      else
        AddMsgNL("%1- Conti correnti: %2 righe storicizzate",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
      endif
    endif
    AddMsgNL("%1- Analitica",this.oparentobject, space(5) )
    * --- Insert into MOVISCOST
    i_nConn=i_TableProp[this.MOVISCOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVISCOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPTMOVICOST_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MOVISCOST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if i_Rows=1
      AddMsgNL("%1- Analitica: %2 riga storicizzata",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    else
      AddMsgNL("%1- Analitica: %2 righe storicizzate",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    endif
    AddMsgNL("%1- Scadenze diverse chiuse",this.oparentobject, space(5) )
    * --- Insert into SCASVARI
    i_nConn=i_TableProp[this.SCASVARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCASVARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPTSCA_VARI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SCASVARI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if i_Rows=1
      AddMsgNL("%1- Scadenze diverse: %2 riga storicizzata",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    else
      AddMsgNL("%1- Scadenze diverse: %2 righe storicizzate",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    endif
    AddMsgNL("%1- Partite/Scadenze chiuse",this.oparentobject, space(5) )
    * --- Insert into PARSTITE
    i_nConn=i_TableProp[this.PARSTITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARSTITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PARSTITE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if i_Rows=1
      AddMsgNL("%1- Partite: %2 riga storicizzata",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    else
      AddMsgNL("%1- Partite: %2 righe storicizzate",this.oparentobject,space(7), ALLTRIM(STR(i_Rows)) )
    endif
    AddMsgNL("Fase 6: cancellazione dati in linea",this.oparentobject)
    AddMsgNL("%1- Cancellazione dati primanota",this.oparentobject, space(5) )
    * --- Delete from PNT_IVA
    i_nConn=i_TableProp[this.PNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPTPNT_IVA_idx,2])
      i_cWhere=i_cTable+".IVSERIAL = "+i_cQueryTable+".IVSERIAL";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".IVSERIAL = "+i_cQueryTable+".IVSERIAL";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if  g_APPLICATION = "ADHOC REVOLUTION" 
      GSSR_BCE(this,"DELM")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if  g_APPLICATION = "ADHOC REVOLUTION" 
      GSSR_BCC(this,"DELM")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Delete from PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPTPNT_DETT_idx,2])
      i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPTMOVICOST_idx,2])
      i_cWhere=i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
            +" and "+i_cTable+".MRROWORD = "+i_cQueryTable+".MRROWORD";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
            +" and "+i_cTable+".MRROWORD = "+i_cQueryTable+".MRROWORD";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPTPNT_MAST_idx,2])
      i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Select from PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select count(*) AS RowNotStor  from "+i_cTable+" PNT_MAST ";
          +" where PNCOMPET="+cp_ToStrODBC(this.w_CODESE)+"";
           ,"_Curs_PNT_MAST")
    else
      select count(*) AS RowNotStor from (i_cTable);
       where PNCOMPET=this.w_CODESE;
        into cursor _Curs_PNT_MAST
    endif
    if used('_Curs_PNT_MAST')
      select _Curs_PNT_MAST
      locate for 1=1
      do while not(eof())
      this.w_RowNotStor = NVL(RowNotStor,0)
        select _Curs_PNT_MAST
        continue
      enddo
      use
    endif
    do case
      case this.w_RowNotStor=0
        AddMsgNL("%1- Tutte le registrazioni di primanota sono state storicizzate",this.oparentobject,space(7) )
      case this.w_RowNotStor=1
        AddMsgNL("%1- 1 registrazione di primanota ancora da storicizzare",this.oparentobject,space(7) )
      case this.w_RowNotStor>1
        AddMsgNL("%1- %2 registrazioni di primanota ancora da storicizzare",this.oparentobject,space(7), ALLTRIM(STR(this.w_RowNotStor)) )
    endcase
    AddMsgNL("%1- Cancellazione dati scadenze diverse",this.oparentobject, space(5) )
    * --- Delete from SCA_VARI
    i_nConn=i_TableProp[this.SCA_VARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPTSCA_VARI_idx,2])
      i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
            +" and "+i_cTable+".SCCODSEC = "+i_cQueryTable+".SCCODSEC";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
            +" and "+i_cTable+".SCCODSEC = "+i_cQueryTable+".SCCODSEC";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Select from SCA_VARI
    i_nConn=i_TableProp[this.SCA_VARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2],.t.,this.SCA_VARI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select count(*) AS RowNotStor  from "+i_cTable+" SCA_VARI ";
          +" where SCDATDOC<="+cp_ToStrODBC(this.oParentObject.w_xesfinese)+"";
           ,"_Curs_SCA_VARI")
    else
      select count(*) AS RowNotStor from (i_cTable);
       where SCDATDOC<=this.oParentObject.w_xesfinese;
        into cursor _Curs_SCA_VARI
    endif
    if used('_Curs_SCA_VARI')
      select _Curs_SCA_VARI
      locate for 1=1
      do while not(eof())
      this.w_RowNotStor = NVL(RowNotStor,0)
        select _Curs_SCA_VARI
        continue
      enddo
      use
    endif
    do case
      case this.w_RowNotStor=0
        AddMsgNL("%1- Tutte le scadenze diverse sono state storicizzate",this.oparentobject,space(7) )
      case this.w_RowNotStor=1
        AddMsgNL("%1- 1 scadenza diversa ancora da storicizzare",this.oparentobject,space(7) )
      case this.w_RowNotStor>1
        AddMsgNL("%1- %2 scadenze diverse ancora da storicizzare",this.oparentobject,space(7), ALLTRIM(STR(this.w_RowNotStor)) )
    endcase
    if g_APPLICATION <> "ADHOC REVOLUTION" 
      * --- Delete from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".PTORIGSA = "+i_cQueryTable+".PTORIGSA";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".PTORIGSA = "+i_cQueryTable+".PTORIGSA";
              +")")
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Delete from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +")")
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Elimino tutte le partite collegate a movimenti storicizzati
    AddMsgNL("%1- Eliminazione dati collegati: partite",this.oparentobject, space(5) )
    if g_APPLICATION <> "ADHOC REVOLUTION"
      * --- Delete from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
        i_cTempTable=cp_GetTempTableName(i_nConn)
        i_aIndex[1]='PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PTMODPAG'
        cp_CreateTempTable(i_nConn,i_cTempTable,"distinct PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PTMODPAG "," from "+i_cQueryTable+" where 1=1",.f.,@i_aIndex)
        i_cQueryTable=i_cTempTable
        i_cWhere=i_cTable+".PTNUMPAR = "+i_cQueryTable+".PTNUMPAR";
              +" and "+i_cTable+".PTDATSCA = "+i_cQueryTable+".PTDATSCA";
              +" and "+i_cTable+".PTTIPCON = "+i_cQueryTable+".PTTIPCON";
              +" and "+i_cTable+".PTCODCON = "+i_cQueryTable+".PTCODCON";
              +" and "+i_cTable+".PTCODVAL = "+i_cQueryTable+".PTCODVAL";
              +" and "+i_cTable+".PTMODPAG = "+i_cQueryTable+".PTMODPAG";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".PTNUMPAR = "+i_cQueryTable+".PTNUMPAR";
              +" and "+i_cTable+".PTDATSCA = "+i_cQueryTable+".PTDATSCA";
              +" and "+i_cTable+".PTTIPCON = "+i_cQueryTable+".PTTIPCON";
              +" and "+i_cTable+".PTCODCON = "+i_cQueryTable+".PTCODCON";
              +" and "+i_cTable+".PTCODVAL = "+i_cQueryTable+".PTCODVAL";
              +" and "+i_cTable+".PTMODPAG = "+i_cQueryTable+".PTMODPAG";
              +")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Delete from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
        i_cTempTable=cp_GetTempTableName(i_nConn)
        i_aIndex[1]='PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL'
        cp_CreateTempTable(i_nConn,i_cTempTable,"distinct PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL "," from "+i_cQueryTable+" where 1=1",.f.,@i_aIndex)
        i_cQueryTable=i_cTempTable
        i_cWhere=i_cTable+".PTNUMPAR = "+i_cQueryTable+".PTNUMPAR";
              +" and "+i_cTable+".PTDATSCA = "+i_cQueryTable+".PTDATSCA";
              +" and "+i_cTable+".PTTIPCON = "+i_cQueryTable+".PTTIPCON";
              +" and "+i_cTable+".PTCODCON = "+i_cQueryTable+".PTCODCON";
              +" and "+i_cTable+".PTCODVAL = "+i_cQueryTable+".PTCODVAL";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".PTNUMPAR = "+i_cQueryTable+".PTNUMPAR";
              +" and "+i_cTable+".PTDATSCA = "+i_cQueryTable+".PTDATSCA";
              +" and "+i_cTable+".PTTIPCON = "+i_cQueryTable+".PTTIPCON";
              +" and "+i_cTable+".PTCODCON = "+i_cQueryTable+".PTCODCON";
              +" and "+i_cTable+".PTCODVAL = "+i_cQueryTable+".PTCODVAL";
              +")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Movimenti di Tesoreria
    *     Non li elimino ma li lascio sempre visualizzabili. 
    *     Nell'anagrafica se non esite pi� il movimento di primanota legato al 
    *     movimento di tesoreria metto il messaggio di storicizzato
    * --- Distinte
    AddMsgNL("%1- Eliminazione dati collegati: distinte",this.oparentobject, space(5) )
    * --- Create temporary table TMP
    i_nIdx=cp_AddTableDef('TMP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gssr_q20',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if g_APPLICATION <> "ADHOC REVOLUTION" 
      GSSR_BGE("CCM_MAST","WRIT")
      * --- Delete from DIS_TINT
      i_nConn=i_TableProp[this.DIS_TINT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMP_idx,2])
        i_cWhere=i_cTable+".DINUMDIS = "+i_cQueryTable+".DINUMDIS";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".DINUMDIS = "+i_cQueryTable+".DINUMDIS";
              +")")
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Delete from DIS_TINT
      i_nConn=i_TableProp[this.DIS_TINT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMP_idx,2])
        i_cWhere=i_cTable+".DINUMDIS = "+i_cQueryTable+".DINUMDIS";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".DINUMDIS = "+i_cQueryTable+".DINUMDIS";
              +")")
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Drop temporary table TMP
    i_nIdx=cp_GetTableDefIdx('TMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP')
    endif
    * --- Contabilizzazione Indiretta
    AddMsgNL("%1- Eliminazione dati collegati: contabilizzazione indiretta",this.oparentobject, space(5) )
    * --- Delete from CON_INDI
    i_nConn=i_TableProp[this.CON_INDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_INDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".CISERIAL = "+i_cQueryTable+".CISERIAL";
    
      do vq_exec with 'query\gssr_q19',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Non  elimino i dati del Contenzioso
    * --- Scritture di Assestamento
    if g_APPLICATION <> "ADHOC REVOLUTION"
      AddMsgNL("%1- Eliminazione dati collegati: scritture di assestamento",this.oparentobject, space(5) )
      * --- Delete from ASSESTAM
      i_nConn=i_TableProp[this.ASSESTAM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ASSESTAM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".ASSERIAL = "+i_cQueryTable+".ASSERIAL";
      
        do vq_exec with 'query\gssr_q18',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZESSTCO ="+cp_NullLink(cp_ToStrODBC(this.w_CodEse),'AZIENDA','AZESSTCO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             )
    else
      update (i_cTable) set;
          AZESSTCO = this.w_CodEse;
          &i_ccchkf. ;
       where;
          AZCODAZI = i_codazi;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_CONTACNT = 1
    if g_APPLICATION = "ADHOC REVOLUTION" 
      * --- Aggiorno Saldi Finali Iniziali Fuori Linea prima dell'aggiornamento dei
      *     saldi ovvero prima dell'azzeramento dei saldi finali dovuto alla eliminazione
      *     dei movimenti di Apertura e Chiusura
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SLTIPCON,SLCODICE,SLCODESE"
        do vq_exec with 'GSSRSQ28',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
                +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
                +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLDARINF = _t2.SLDARINF";
            +",SLAVEINF = _t2.SLAVEINF";
            +",SLDARFIF = _t2.SLDARFIF";
            +",SLAVEFIF = _t2.SLAVEFIF";
            +i_ccchkf;
            +" from "+i_cTable+" SALDICON, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
                +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
                +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON, "+i_cQueryTable+" _t2 set ";
            +"SALDICON.SLDARINF = _t2.SLDARINF";
            +",SALDICON.SLAVEINF = _t2.SLAVEINF";
            +",SALDICON.SLDARFIF = _t2.SLDARFIF";
            +",SALDICON.SLAVEFIF = _t2.SLAVEFIF";
            +Iif(Empty(i_ccchkf),"",",SALDICON.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SALDICON.SLTIPCON = t2.SLTIPCON";
                +" and "+"SALDICON.SLCODICE = t2.SLCODICE";
                +" and "+"SALDICON.SLCODESE = t2.SLCODESE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON set (";
            +"SLDARINF,";
            +"SLAVEINF,";
            +"SLDARFIF,";
            +"SLAVEFIF";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.SLDARINF,";
            +"t2.SLAVEINF,";
            +"t2.SLDARFIF,";
            +"t2.SLAVEFIF";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
                +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
                +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON set ";
            +"SLDARINF = _t2.SLDARINF";
            +",SLAVEINF = _t2.SLAVEINF";
            +",SLDARFIF = _t2.SLDARFIF";
            +",SLAVEFIF = _t2.SLAVEFIF";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SLTIPCON = "+i_cQueryTable+".SLTIPCON";
                +" and "+i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
                +" and "+i_cTable+".SLCODESE = "+i_cQueryTable+".SLCODESE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLDARINF = (select SLDARINF from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SLAVEINF = (select SLAVEINF from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SLDARFIF = (select SLDARFIF from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SLAVEFIF = (select SLAVEFIF from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Scrittura saldi finali\iniziali fuori linea'
        return
      endif
    endif
    * --- Select from TMPTPNT_MAST
    i_nConn=i_TableProp[this.TMPTPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPTPNT_MAST_idx,2],.t.,this.TMPTPNT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select distinct PNCOMPET  from "+i_cTable+" TMPTPNT_MAST ";
          +" order by PNCOMPET";
           ,"_Curs_TMPTPNT_MAST")
    else
      select distinct PNCOMPET from (i_cTable);
       order by PNCOMPET;
        into cursor _Curs_TMPTPNT_MAST
    endif
    if used('_Curs_TMPTPNT_MAST')
      select _Curs_TMPTPNT_MAST
      locate for 1=1
      do while not(eof())
      if this.w_CONTACNT=1
        this.w_POSIZIONE = LEN( this.oParentObject.w_MSG) 
        AddMsgNL("Fase 7: ricostruzione saldi esercizio %1",this.oparentobject, ALLTRIM(STR(this.w_CONTACNT)) )
      else
        this.oParentObject.w_MSG = Left( this.oParentObject.w_MSG , this.w_POSIZIONE)
        AddMsgNL("Fase 7: ricostruzione saldi esercizio %1",this.oparentobject, ALLTRIM(STR(this.w_CONTACNT)) )
      endif
      this.w_CodEse = NVL(PNCOMPET,"    ")
      do GSCG_BRS with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CONTACNT = this.w_CONTACNT + 1
        select _Curs_TMPTPNT_MAST
        continue
      enddo
      use
    endif
    this.w_CodEse = this.oParentObject.w_CODESE
    AddMsgNL("Fase 8: aggiornamento saldi conti fuori linea",this.oparentobject)
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLTIPCON,SLCODICE,SLCODESE"
      do vq_exec with 'gssr_q28',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
              +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARPRO = _t2.SLDARPRO";
          +",SLAVEPRO = _t2.SLAVEPRO";
          +i_ccchkf;
          +" from "+i_cTable+" SALDICON, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
              +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON, "+i_cQueryTable+" _t2 set ";
          +"SALDICON.SLDARPRO = _t2.SLDARPRO";
          +",SALDICON.SLAVEPRO = _t2.SLAVEPRO";
          +Iif(Empty(i_ccchkf),"",",SALDICON.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDICON.SLTIPCON = t2.SLTIPCON";
              +" and "+"SALDICON.SLCODICE = t2.SLCODICE";
              +" and "+"SALDICON.SLCODESE = t2.SLCODESE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON set (";
          +"SLDARPRO,";
          +"SLAVEPRO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.SLDARPRO,";
          +"t2.SLAVEPRO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
              +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON set ";
          +"SLDARPRO = _t2.SLDARPRO";
          +",SLAVEPRO = _t2.SLAVEPRO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLTIPCON = "+i_cQueryTable+".SLTIPCON";
              +" and "+i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODESE = "+i_cQueryTable+".SLCODESE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARPRO = (select SLDARPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLAVEPRO = (select SLAVEPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    AddMsgNL("%1",This.oparentobject, repl("-",62) )
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OK
      if NOT EMPTY(NVL(this.w_BlockError,""))
        AddMsgNL("STORICIZZAZIONE NON EFFETTUATA. ELENCO ANOMALIE:%0%1", this.oparentobject, this.w_BlockError)
      else
        AddMsgNL("STORICIZZAZIONE TERMINATA CON SUCCESSO",this.oparentobject)
      endif
      AddMsg("%0Elaborazione terminata alle: %1", this.oparentobject, Time() )
      * --- Try
      local bErr_04B0FCD8
      bErr_04B0FCD8=bTrsErr
      this.Try_04B0FCD8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_04B0FCD8
      * --- End
    else
      AddMsg("%0Elaborazione interrotta",this.oparentobject )
    endif
    if this.w_STAMPART AND ah_YesNo("Si vuole stampare il dettaglio delle registrazioni di primanota%0che non � possibile storicizzare perch� legate a partite aperte?")
      VQ_EXEC("QUERY\GSSR_Q26.VQR",this,"__TMP__")
      CP_CHPRN("QUERY\GSSR_Q26.FRX", " ", this)
    endif
  endproc
  proc Try_04B0FCD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Creo il log dell'esecuzione dell'analisi
    this.w_LOGMSG = this.oParentObject.w_MSG
    this.w_LG_ESITO = IIF(NOT EMPTY(NVL(this.w_BlockError,"")),"N","S")
    AddMsg("%0Inserimento dati elaborazione nel log",this.oparentobject )
    this.w_LSSERIAL = Space(10)
    * --- Inserisco nel Log - Altra transazione per avere il tempo di termine 
    *     (compreso il Commit)
    i_Conn=i_TableProp[this.LOG_STOR_IDX, 3]
    cp_NextTableProg(this, i_Conn, "STLOG", "i_codazi,w_LSSERIAL")
    * --- Insert into LOG_STOR
    i_nConn=i_TableProp[this.LOG_STOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOG_STOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LSSERIAL"+",LSCODESE"+",LG__TIPO"+",LG___OPE"+",LGCODUTE"+",LG__DATA"+",LG__NOTE"+",LG_ESITO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOG_STOR','LSSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODESE),'LOG_STOR','LSCODESE');
      +","+cp_NullLink(cp_ToStrODBC("C"),'LOG_STOR','LG__TIPO');
      +","+cp_NullLink(cp_ToStrODBC("S"),'LOG_STOR','LG___OPE');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOG_STOR','LGCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'LOG_STOR','LG__DATA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOGMSG),'LOG_STOR','LG__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LG_ESITO),'LOG_STOR','LG_ESITO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LSCODESE',this.w_CODESE,'LG__TIPO',"C",'LG___OPE',"S",'LGCODUTE',i_CODUTE,'LG__DATA',i_DATSYS,'LG__NOTE',this.w_LOGMSG,'LG_ESITO',this.w_LG_ESITO)
      insert into (i_cTable) (LSSERIAL,LSCODESE,LG__TIPO,LG___OPE,LGCODUTE,LG__DATA,LG__NOTE,LG_ESITO &i_ccchkf. );
         values (;
           this.w_LSSERIAL;
           ,this.w_CODESE;
           ,"C";
           ,"S";
           ,i_CODUTE;
           ,i_DATSYS;
           ,this.w_LOGMSG;
           ,this.w_LG_ESITO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento log'
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ciclo sulle partite aperte per controllare fino a quando ne trovo aperte
    *     Le partite aperte potrebbero aprire delle altre partite chiuse ma 
    *     appartenenti a delle registrazioni in cui ci sono anche partite aperte
    *     per cui per queste non � possible storicizzare
    this.w_CONTACNT = 1
    do while this.w_NUMPART > 0
      * --- prima cancello quelle della primanota
      i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","PTSERIAL,PTROWORD"," ")
      * --- Delete from TMPTPAR_TITE
      i_nConn=i_TableProp[this.TMPTPAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
      
        do vq_exec with 'query\GSSR_Q25',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- poi cancello quelle dalle scadenze diverse
      i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","PTSERIAL,PTROWORD"," ")
      * --- Delete from TMPTPAR_TITE
      i_nConn=i_TableProp[this.TMPTPAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
        i_cTempTable=cp_GetTempTableName(i_nConn)
        i_aIndex[1]='PTSERIAL,PTROWORD'
        cp_CreateTempTable(i_nConn,i_cTempTable,"distinct PTSERIAL, PTROWORD "," from "+i_cQueryTable+" where PTROWORD=-1",.f.,@i_aIndex)
        i_cQueryTable=i_cTempTable
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Calcolo partite chiuse collegate a stessi movimenti di partite aperte
      *     perch� non devo storicizzare neanche queste
      this.w_CONTACNT = this.w_CONTACNT + 1
      this.oParentObject.w_MSG = Left( this.oParentObject.w_MSG , this.w_POSIZIONE)
      AddMsgNL("%1- Livello %2 - verifica partite aperte",this.oparentobject, space(5), ALLTRIM(STR(this.w_CONTACNT)) )
      * --- Inserisco tutte le nuove partite aperte
      * --- Insert into ZOOMPART
      i_nConn=i_TableProp[this.ZOOMPART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\gssr_q24",this.ZOOMPART_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_NUMPART = i_Rows
    enddo
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,30)]
    this.cWorkTables[1]='MOVISCOST'
    this.cWorkTables[2]='PNTSIVA'
    this.cWorkTables[3]='*TMP'
    this.cWorkTables[4]='PARSTITE'
    this.cWorkTables[5]='PNTSMAST'
    this.cWorkTables[6]='SCASVARI'
    this.cWorkTables[7]='PNTSDETT'
    this.cWorkTables[8]='*TMPTMOVICOST'
    this.cWorkTables[9]='CON_INDI'
    this.cWorkTables[10]='AZIENDA'
    this.cWorkTables[11]='DIS_TINT'
    this.cWorkTables[12]='MOVICOST'
    this.cWorkTables[13]='*TMPTPAR_TITE'
    this.cWorkTables[14]='PAR_TITE'
    this.cWorkTables[15]='PNT_DETT'
    this.cWorkTables[16]='PNT_MAST'
    this.cWorkTables[17]='*TMPTPNT_DETT'
    this.cWorkTables[18]='*TMPTPNT_MAST'
    this.cWorkTables[19]='PNT_IVA'
    this.cWorkTables[20]='*TMPTPNT_IVA'
    this.cWorkTables[21]='SALDICON'
    this.cWorkTables[22]='SCA_VARI'
    this.cWorkTables[23]='*TMPTSCA_VARI'
    this.cWorkTables[24]='*ZOOMPART'
    this.cWorkTables[25]='LOG_STOR'
    this.cWorkTables[26]='*TMP_SALLOUB'
    this.cWorkTables[27]='ASSESTAM'
    this.cWorkTables[28]='*TMPTCCM_DETT'
    this.cWorkTables[29]='*TMPTPNT_CESP'
    this.cWorkTables[30]='ATTIDETT'
    return(this.OpenAllTables(30))

  proc CloseCursors()
    if used('_Curs_gssr_q08')
      use in _Curs_gssr_q08
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_gssr_q31')
      use in _Curs_gssr_q31
    endif
    if used('_Curs_gssr_q21')
      use in _Curs_gssr_q21
    endif
    if used('_Curs_gssr_q09')
      use in _Curs_gssr_q09
    endif
    if used('_Curs_ZOOMPART')
      use in _Curs_ZOOMPART
    endif
    if used('_Curs_PNT_MAST')
      use in _Curs_PNT_MAST
    endif
    if used('_Curs_SCA_VARI')
      use in _Curs_SCA_VARI
    endif
    if used('_Curs_TMPTPNT_MAST')
      use in _Curs_TMPTPNT_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
