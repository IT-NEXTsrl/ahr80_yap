* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_apr                                                        *
*              Province                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2009-08-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_apr"))

* --- Class definition
define class tgsar_apr as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 409
  Height = 99+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-08-04"
  HelpContextID=160003945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  ANAG_PRO_IDX = 0
  PROVANAG_IDX = 0
  cFile = "ANAG_PRO"
  cKeySelect = "PRCODPRO"
  cKeyWhere  = "PRCODPRO=this.w_PRCODPRO"
  cKeyWhereODBC = '"PRCODPRO="+cp_ToStrODBC(this.w_PRCODPRO)';

  cKeyWhereODBCqualified = '"ANAG_PRO.PRCODPRO="+cp_ToStrODBC(this.w_PRCODPRO)';

  cPrg = "gsar_apr"
  cComment = "Province"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRCODPRO = space(2)
  w_PRDESPRO = space(30)
  w_PRCODANA = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ANAG_PRO','gsar_apr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_aprPag1","gsar_apr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Province")
      .Pages(1).HelpContextID = 193196123
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPRCODPRO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='PROVANAG'
    this.cWorkTables[2]='ANAG_PRO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANAG_PRO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANAG_PRO_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PRCODPRO = NVL(PRCODPRO,space(2))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANAG_PRO where PRCODPRO=KeySet.PRCODPRO
    *
    i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANAG_PRO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANAG_PRO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANAG_PRO '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRCODPRO',this.w_PRCODPRO  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PRCODPRO = NVL(PRCODPRO,space(2))
        .w_PRDESPRO = NVL(PRDESPRO,space(30))
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .w_PRCODANA = NVL(PRCODANA,space(2))
          * evitabile
          *.link_1_6('Load')
        cp_LoadRecExtFlds(this,'ANAG_PRO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PRCODPRO = space(2)
      .w_PRDESPRO = space(30)
      .w_PRCODANA = space(2)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(1,3,.f.)
          if not(empty(.w_PRCODANA))
          .link_1_6('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANAG_PRO')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPRCODPRO_1_1.enabled = i_bVal
      .Page1.oPag.oPRDESPRO_1_3.enabled = i_bVal
      .Page1.oPag.oPRCODANA_1_6.enabled = i_bVal
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPRCODPRO_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPRCODPRO_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ANAG_PRO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODPRO,"PRCODPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDESPRO,"PRDESPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODANA,"PRCODANA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
    i_lTable = "ANAG_PRO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ANAG_PRO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANAG_PRO_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANAG_PRO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANAG_PRO')
        i_extval=cp_InsertValODBCExtFlds(this,'ANAG_PRO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PRCODPRO,PRDESPRO,PRCODANA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PRCODPRO)+;
                  ","+cp_ToStrODBC(this.w_PRDESPRO)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODANA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANAG_PRO')
        i_extval=cp_InsertValVFPExtFlds(this,'ANAG_PRO')
        cp_CheckDeletedKey(i_cTable,0,'PRCODPRO',this.w_PRCODPRO)
        INSERT INTO (i_cTable);
              (PRCODPRO,PRDESPRO,PRCODANA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PRCODPRO;
                  ,this.w_PRDESPRO;
                  ,this.w_PRCODANA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANAG_PRO_IDX,i_nConn)
      *
      * update ANAG_PRO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANAG_PRO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PRDESPRO="+cp_ToStrODBC(this.w_PRDESPRO)+;
             ",PRCODANA="+cp_ToStrODBCNull(this.w_PRCODANA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANAG_PRO')
        i_cWhere = cp_PKFox(i_cTable  ,'PRCODPRO',this.w_PRCODPRO  )
        UPDATE (i_cTable) SET;
              PRDESPRO=this.w_PRDESPRO;
             ,PRCODANA=this.w_PRCODANA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANAG_PRO_IDX,i_nConn)
      *
      * delete ANAG_PRO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PRCODPRO',this.w_PRCODPRO  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPRCODANA_1_6.visible=!this.oPgFrm.Page1.oPag.oPRCODANA_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PRCODANA
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROVANAG_IDX,3]
    i_lTable = "PROVANAG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROVANAG_IDX,2], .t., this.PROVANAG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROVANAG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODANA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_apv',True,'PROVANAG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PVCODPRO like "+cp_ToStrODBC(trim(this.w_PRCODANA)+"%");

          i_ret=cp_SQL(i_nConn,"select PVCODPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PVCODPRO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PVCODPRO',trim(this.w_PRCODANA))
          select PVCODPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PVCODPRO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODANA)==trim(_Link_.PVCODPRO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCODANA) and !this.bDontReportError
            deferred_cp_zoom('PROVANAG','*','PVCODPRO',cp_AbsName(oSource.parent,'oPRCODANA_1_6'),i_cWhere,'gsar_apv',"PROVINCE ANAGRAFE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PVCODPRO";
                     +" from "+i_cTable+" "+i_lTable+" where PVCODPRO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PVCODPRO',oSource.xKey(1))
            select PVCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODANA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PVCODPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PVCODPRO="+cp_ToStrODBC(this.w_PRCODANA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PVCODPRO',this.w_PRCODANA)
            select PVCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODANA = NVL(_Link_.PVCODPRO,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODANA = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROVANAG_IDX,2])+'\'+cp_ToStr(_Link_.PVCODPRO,1)
      cp_ShowWarn(i_cKey,this.PROVANAG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODANA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPRCODPRO_1_1.value==this.w_PRCODPRO)
      this.oPgFrm.Page1.oPag.oPRCODPRO_1_1.value=this.w_PRCODPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDESPRO_1_3.value==this.w_PRDESPRO)
      this.oPgFrm.Page1.oPag.oPRDESPRO_1_3.value=this.w_PRDESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODANA_1_6.value==this.w_PRCODANA)
      this.oPgFrm.Page1.oPag.oPRCODANA_1_6.value=this.w_PRCODANA
    endif
    cp_SetControlsValueExtFlds(this,'ANAG_PRO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PRCODPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCODPRO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_PRCODPRO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_aprPag1 as StdContainer
  Width  = 405
  height = 99
  stdWidth  = 405
  stdheight = 99
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRCODPRO_1_1 as StdField with uid="BAAEFUYZOS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PRCODPRO", cQueryName = "PRCODPRO",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice provincia",;
    HelpContextID = 185208901,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=176, Top=25, InputMask=replicate('X',2)

  add object oPRDESPRO_1_3 as StdField with uid="ZDFGZQHHLT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PRDESPRO", cQueryName = "PRDESPRO",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione provincia",;
    HelpContextID = 200286277,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=176, Top=48, InputMask=replicate('X',30)


  add object oObj_1_5 as cp_runprogram with uid="HLKYUVNYXQ",left=0, top=178, width=216,height=19,;
    caption='GSAR_BBU(CLI,CHER)',;
   bGlobalFont=.t.,;
    prg='GSAR_BBU("PROVINCE",w_PRCODPRO)',;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 188013408

  add object oPRCODANA_1_6 as StdField with uid="KSHKTYORHU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PRCODANA", cQueryName = "PRCODANA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice provincia relativo all'anagrafe",;
    HelpContextID = 66449353,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=176, Top=71, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="PROVANAG", cZoomOnZoom="gsar_apv", oKey_1_1="PVCODPRO", oKey_1_2="this.w_PRCODANA"

  func oPRCODANA_1_6.mHide()
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
  endfunc

  func oPRCODANA_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODANA_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODANA_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROVANAG','*','PVCODPRO',cp_AbsName(this.parent,'oPRCODANA_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_apv',"PROVINCE ANAGRAFE",'',this.parent.oContained
  endproc
  proc oPRCODANA_1_6.mZoomOnZoom
    local i_obj
    i_obj=gsar_apv()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PVCODPRO=this.parent.oContained.w_PRCODANA
     i_obj.ecpSave()
  endproc

  add object oStr_1_2 as StdString with uid="RNTAPWJBKQ",Visible=.t., Left=54, Top=29,;
    Alignment=1, Width=121, Height=18,;
    Caption="Codice provincia:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="SLTTMJDDTV",Visible=.t., Left=54, Top=52,;
    Alignment=1, Width=121, Height=18,;
    Caption="Descrizione provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QQTKXGHRBT",Visible=.t., Left=3, Top=75,;
    Alignment=1, Width=172, Height=18,;
    Caption="Codice provincia anagrafe:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (g_ANACOM<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_apr','ANAG_PRO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRCODPRO=ANAG_PRO.PRCODPRO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
