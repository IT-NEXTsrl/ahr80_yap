* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bam                                                        *
*              Calcolo magazzino preferenziale                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_166]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2000-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPRG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bam",oParentObject,m.pPRG)
return(i_retval)

define class tgsve_bam as StdBatch
  * --- Local variables
  pPRG = space(1)
  w_DTOBS2 = ctod("  /  /  ")
  w_DTOBS3 = ctod("  /  /  ")
  w_DTOBS4 = ctod("  /  /  ")
  w_DTOBS5 = ctod("  /  /  ")
  w_CODMAG = space(5)
  w_MAGCAU = space(5)
  w_MATCAU = space(5)
  w_MAGAZI = space(5)
  w_CAUMAG = space(5)
  w_FLMOV = space(1)
  w_TESTMAG = .f.
  * --- WorkFile variables
  PAR_RIMA_idx=0
  ART_ICOL_idx=0
  TIP_DOCU_idx=0
  CAM_AGAZ_idx=0
  MAGAZZIN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo magazzino preferenziale presente sull'anagrafica articoli Lanciato da GSVE_MDV E GSMA_MVM
    if this.pPRG="M"
    else
    endif
    this.w_CODMAG = SPACE(5)
    this.w_MAGCAU = SPACE(5)
    this.w_MATCAU = SPACE(5)
    this.w_MAGAZI = g_MAGAZI
    if this.pPRG="M"
      * --- Movimenti di Magazzino
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARMAGPRE"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MMCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARMAGPRE;
          from (i_cTable) where;
              ARCODART = this.oParentObject.w_MMCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODMAG = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(ALLTRIM(this.w_CODMAG)) 
 
        this.oParentObject.w_MMCODMAG = this.w_CODMAG
      else
        this.oParentObject.w_MMCODMAG = this.oParentObject.w_OMAG
      endif
    else
      * --- Documenti
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARMAGPRE"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARMAGPRE;
          from (i_cTable) where;
              ARCODART = this.oParentObject.w_MVCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODMAG = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDCAUMAG,TDCODMAG,TDCODMAT"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDCAUMAG,TDCODMAG,TDCODMAT;
          from (i_cTable) where;
              TDTIPDOC = this.oParentObject.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAUMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
        this.w_MAGCAU = NVL(cp_ToDate(_read_.TDCODMAG),cp_NullValue(_read_.TDCODMAG))
        this.w_MATCAU = NVL(cp_ToDate(_read_.TDCODMAT),cp_NullValue(_read_.TDCODMAT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLAVAL"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLAVAL;
          from (i_cTable) where;
              CMCODICE = this.w_CAUMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLMOV = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGDTOBSO"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_MATCAU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGDTOBSO;
          from (i_cTable) where;
              MGCODMAG = this.w_MATCAU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DTOBS5 = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.oParentObject.w_MVTIPRIG="R"
        if EMPTY (this.w_MATCAU) OR (NOT EMPTY(this.w_DTOBS5) AND this.w_DTOBS5<=i_DATSYS)
          this.oParentObject.w_MVCODMAT = SPACE(5)
        else
          this.oParentObject.w_MVCODMAT = this.oParentObject.w_OMAT
        endif
      endif
      if NOT EMPTY(ALLTRIM(this.w_CODMAG)) 
 
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGDTOBSO"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGDTOBSO;
            from (i_cTable) where;
                MGCODMAG = this.w_CODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DTOBS2 = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Controllo che sia valorizzato e non obsoleto il magazzino dell'articolo
      this.w_TESTMAG = IIF((this.oParentObject.w_MVTIPRIG="R" AND this.w_FLMOV<>"N" AND (EMPTY(this.w_DTOBS2) OR this.w_DTOBS2>i_DATSYS) AND NOT EMPTY(this.w_CODMAG)), .T., .F.)
      if this.w_TESTMAG=.T.
        this.oParentObject.w_MVCODMAG = this.w_CODMAG
      else
        if this.oParentObject.w_MVTIPRIG="R" AND this.w_FLMOV<>"N"
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDTOBSO"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDTOBSO;
              from (i_cTable) where;
                  MGCODMAG = this.w_CODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DTOBS2 = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDTOBSO"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGCAU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDTOBSO;
              from (i_cTable) where;
                  MGCODMAG = this.w_MAGCAU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DTOBS4 = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDTOBSO"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDTOBSO;
              from (i_cTable) where;
                  MGCODMAG = this.w_MAGAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DTOBS3 = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Se il magazzino dell'articolo � vuoto o obsoleto Controllo quello delle Causali Documenti
          if EMPTY (this.w_CODMAG) OR (NOT EMPTY(this.w_DTOBS2) AND this.w_DTOBS2<=i_DATSYS)
            if EMPTY(this.w_MAGCAU) OR (NOT EMPTY(this.w_DTOBS4) AND this.w_DTOBS4<=i_DATSYS)
              * --- Se il magazzino dell'articolo � vuoto o obsoleto Controllo quello delle Causali Documenti
              if EMPTY(g_MAGAZI) OR (NOT EMPTY(this.w_DTOBS3) AND this.w_DTOBS3<=i_DATSYS)
                this.oParentObject.w_MVCODMAG = SPACE(5)
              else
                this.oParentObject.w_MVCODMAG = g_MAGAZI
              endif
            else
              this.oParentObject.w_MVCODMAG = this.w_MAGCAU
            endif
          else
            this.oParentObject.w_MVCODMAG = this.w_CODMAG
          endif
        else
          this.oParentObject.w_MVCODMAG = SPACE(5)
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pPRG)
    this.pPRG=pPRG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PAR_RIMA'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='CAM_AGAZ'
    this.cWorkTables[5]='MAGAZZIN'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPRG"
endproc
