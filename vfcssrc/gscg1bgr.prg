* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg1bgr                                                        *
*              Apertura primanota                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-12                                                      *
* Last revis.: 2012-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg1bgr",oParentObject)
return(i_retval)

define class tgscg1bgr as StdBatch
  * --- Local variables
  w_PNOTA = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invocato dalla maschera "GSCG_AGP", quando si doppio click su una riga del visual zoom
    *     in essa contenuto
    this.w_PNOTA = GSCG_MPN()
    if ! EMPTY(NVL(this.oParentObject.w_ASRIFEPN,""))
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_PNOTA.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_PNOTA.EcpFilter()     
      * --- Inizializzo la chiave
      this.w_PNOTA.w_PNSERIAL = this.oParentObject.w_ASRIFEPN
      * --- Carica il record
      this.w_PNOTA.EcpSave()     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
