* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bia                                                        *
*              Inserimento attributo                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-22                                                      *
* Last revis.: 2011-06-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam,pParam2
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bia",oParentObject,m.pParam,m.pParam2)
return(i_retval)

define class tgsar_bia as StdBatch
  * --- Local variables
  pParam = space(5)
  pParam2 = space(5)
  w_ASVALATT = space(10)
  w_MASSIMO = 0
  w_MASSIMOR = 0
  w_CODFAM = space(10)
  w_NOVAL = .f.
  w_FATIPCAR = space(10)
  w_FATIPDAT = ctod("  /  /  ")
  w_FATIPNUM = 0
  w_CAR = 0
  w_MESS = space(100)
  w_FILTRO = space(254)
  w_FILTRO2 = space(254)
  w_CARAT = space(20)
  w_VALFIS = space(20)
  w_COND = .f.
  * --- WorkFile variables
  FAZDATTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Inserimento di un valore non presente in archivio
    * --- --DA gsar_mra
    * --- "Quando GSAR_BIA � in esecuzione vale "S"
    *     Utilizzato da GSAR_BIA per non andare in esecuzione due volte 
    *     contemporaneamente (la valid di w_ASTIPCAR, quando si chiama
    *     lo zoom GSAR_BIA tramite doppio click, alla valid esegue una 
    *     notifyevent che attiva nuovamente GSAR_BIA)
    * --- SE CHIAMATO CON DOPPIO CLICK (DBLCLICK) apre lo zoom per tipologia:
    *     libero
    *     misto da elenco
    *     misto da archivio
    if this.oParentObject.w_GSAR_BIA_FLAG = "N"
      this.oParentObject.w_GSAR_BIA_FLAG = "S"
    else
      i_retcode = 'stop'
      return
    endif
    if VARTYPE(this.pPARAM) = "O"
      do case
        case VARTYPE(this.pPARAM.VALUE) = "C"
          this.pPARAM.VALUE = ""
        case VARTYPE(this.pPARAM.VALUE) = "N"
          this.pPARAM.VALUE = 0
        case VARTYPE(this.pPARAM.VALUE) = "D"
          this.pPARAM.VALUE = CP_CHARTODATE( "  -  -  " )
      endcase
    endif
    this.w_CODFAM = this.oParentObject.w_ASCODFAM
    this.oParentObject.w_TABLE = alltrim(this.oParentObject.w_TABLE)
    this.oParentObject.w_ATTCOL = alltrim(this.oParentObject.w_ATTCOL)
    this.oParentObject.w_CAMPO = alltrim(this.oParentObject.w_CAMPO)
    if NOT EMPTY(this.oParentObject.w_TABLE) AND ( INLIST( this.oParentObject.w_TIPO, "F", "A" ) OR this.pPARAM2="DBLCLICK" AND INLIST( this.oParentObject.w_TIPO, "M", "B" ) )
      if vartype( this.pParam) ="C"
        this.pParam = this.oparentobject.getctrl( ALLTRIM(STRTRAN(UPPER(this.oparentobject.currentevent),"CHANGED","")) )
        * --- Aggiungiamo il filtro like se campo char e pieno
        if NOT EMPTY(this.oParentObject.w_ATTCOL)
          this.w_FILTRO = "("+this.oParentObject.w_ATTCOL+" "+"LIKE "+CP_TOSTRODBC(ALLTRIM(this.pParam.value+"%"))+")"
        endif
      endif
      * --- Se gestito attributo fisso di link lo vado a ricercare..
      if NOT EMPTY(this.oParentObject.w_ATTRIBUTO) AND NOT EMPTY(this.oParentObject.w_ATTCOL)
         
 Local L_Mess 
 L_Mess=""
        * --- Per passaggio per riferimento non si pu� utilizzare una var. locale (@this. va in errore)
        this.w_VALFIS = Getattributo(this.oparentobject,this.oParentObject.w_ATTRIBUTO,this.oParentObject.w_INPUTA,@L_MESS)
        this.w_MESS = L_MESS
        if Not Empty( this.w_MESS)
          * --- Non l'ho trovato o ne ho trovate troppe occorrenze (pi� di una)
          AH_ERRORMSG(this.w_MESS)
          this.oParentObject.w_ASCODFAM = ""
          this.bUpdateParentObject=.f.
          this.oParentObject.w_GSAR_BIA_FLAG = "N"
          i_retcode = 'stop'
          return
        endif
      endif
      if VARTYPE(this.pPARAM.VALUE) $ "C-N-D"
        if NOT EMPTY( this.pPARAM.VALUE )
          do case
            case VARTYPE(this.pPARAM.VALUE) = "C"
              if NOT EMPTY(this.oParentObject.w_CAMPO)
                this.w_FILTRO2 = "("+this.oParentObject.w_CAMPO+" LIKE "+CP_TOSTRODBC(ALLTRIM(this.pParam.value)+"%")+")"
              else
                this.w_FILTRO2 = "( 1 = 1 )"
              endif
              if NOT EMPTY(this.w_VALFIS)
                this.w_FILTRO = "("+this.oParentObject.w_ATTCOL+" LIKE "+CP_TOSTRODBC(ALLTRIM( this.w_VALFIS )+"%")+")"
              else
                this.w_FILTRO = "( 1 = 1 )"
              endif
              this.w_FILTRO = this.w_FILTRO + " AND " + this.w_FILTRO2
            case VARTYPE(this.pPARAM.VALUE) = "N"
              this.w_FILTRO = "("+this.oParentObject.w_CAMPO+" = "+CP_TOSTRODBC( this.pParam.value )+")"
            case VARTYPE(this.pPARAM.VALUE) = "D"
              this.w_FILTRO = "("+this.oParentObject.w_CAMPO+" = "+CP_TOSTR( this.pParam.value )+")"
          endcase
        else
          if NOT EMPTY(this.w_VALFIS)
            this.w_FILTRO = "("+this.oParentObject.w_ATTCOL+" LIKE "+CP_TOSTRODBC(ALLTRIM( this.w_VALFIS )+"%")+")"
          else
            this.w_FILTRO = "( 1 = 1 )"
          endif
        endif
      else
        this.w_FILTRO = "( 1 = 1 )"
      endif
      DO GSAR_BMG WITH THIS.OPARENTOBJECT, "ZAINS", this.w_FILTRO
      this.oParentObject.w_GSAR_BIA_FLAG = "N"
      i_retcode = 'stop'
      return
    else
      if vartype( this.pParam) ="C"
        this.pParam = this.oparentobject.getctrl( ALLTRIM(STRTRAN(UPPER(this.oparentobject.currentevent),"CHANGED","")) )
      endif
      if VARTYPE(this.pPARAM.VALUE) $ "C-N-D"
        if NOT EMPTY( this.pPARAM.VALUE )
          do case
            case VARTYPE(this.pPARAM.VALUE) = "C"
              * --- Si aggiunge il filtro like se il campo � di tipo carattere e pieno
              this.w_FILTRO = "(CAVALATT LIKE "+CP_TOSTRODBC(ALLTRIM(this.pParam.value)+"%")+")"
            case VARTYPE(this.pPARAM.VALUE) = "N"
              this.w_FILTRO = "(CAVALATT = "+CP_TOSTRODBC( ALLTRIM( STR( this.pParam.value, 20, 3 ) ) )+")"
            case VARTYPE(this.pPARAM.VALUE) = "D"
              this.w_FILTRO = "(CAVALATT = "+CP_TOSTR( DTOC( this.pParam.value ) )+")"
          endcase
        else
          this.w_FILTRO = "( 1 = 1 )"
        endif
      else
        this.w_FILTRO = "( 1 = 1 )"
      endif
      do case
        case this.oParentObject.w_TIPO="F"
          * --- --Campo Fisso Occorre Abilitare lo Zoom
          DO GSAR_BMG WITH THIS.OPARENTOBJECT, "ZAINS", this.w_FILTRO
        case this.oParentObject.w_TIPO="M"
          * --- --si controlla se il valore esite nella tabella per quell'attributo
          do case
            case this.oParentObject.w_INPUTA="C" 
              if !EMPTY(this.oParentObject.w_ASTIPCAR)
                * --- Read from FAZDATTR
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.FAZDATTR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.FAZDATTR_idx,2],.t.,this.FAZDATTR_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CPROWNUM"+;
                    " from "+i_cTable+" FAZDATTR where ";
                        +"FACODICE = "+cp_ToStrODBC(this.oParentObject.w_ASCODFAM);
                        +" and FATIPCAR = "+cp_ToStrODBC(this.oParentObject.w_ASTIPCAR);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CPROWNUM;
                    from (i_cTable) where;
                        FACODICE = this.oParentObject.w_ASCODFAM;
                        and FATIPCAR = this.oParentObject.w_ASTIPCAR;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CAR = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_ASVALATT = LEFT(this.oParentObject.w_ASTIPCAR,this.oParentObject.w_DIME)
                this.w_COND = not empty(this.w_ASVALATT)
              else
                DO GSAR_BMG WITH THIS.OPARENTOBJECT, "ZAINS", this.w_FILTRO
                this.oParentObject.w_GSAR_BIA_FLAG = "N"
                i_retcode = 'stop'
                return
              endif
            case this.oParentObject.w_INPUTA="N" 
              if this.oParentObject.w_ASTIPNUM>0
                * --- Read from FAZDATTR
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.FAZDATTR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.FAZDATTR_idx,2],.t.,this.FAZDATTR_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CPROWNUM"+;
                    " from "+i_cTable+" FAZDATTR where ";
                        +"FACODICE = "+cp_ToStrODBC(this.oParentObject.w_ASCODFAM);
                        +" and FATIPNUM = "+cp_ToStrODBC(this.oParentObject.w_ASTIPNUM);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CPROWNUM;
                    from (i_cTable) where;
                        FACODICE = this.oParentObject.w_ASCODFAM;
                        and FATIPNUM = this.oParentObject.w_ASTIPNUM;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CAR = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_ASVALATT = ALLTRIM(STR(this.oParentObject.w_ASTIPNUM,this.oParentObject.w_DIME,this.oParentObject.w_DEC))
                this.w_COND = VAL (NVL(this.w_ASVALATT,0))<>0
              else
                DO GSAR_BMG WITH THIS.OPARENTOBJECT, "ZAINS", this.w_FILTRO
                this.oParentObject.w_GSAR_BIA_FLAG = "N"
                i_retcode = 'stop'
                return
              endif
            case this.oParentObject.w_INPUTA="D" 
              if !EMPTY(this.oParentObject.w_ASTIPDAT)
                * --- Read from FAZDATTR
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.FAZDATTR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.FAZDATTR_idx,2],.t.,this.FAZDATTR_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CPROWNUM"+;
                    " from "+i_cTable+" FAZDATTR where ";
                        +"FACODICE = "+cp_ToStrODBC(this.oParentObject.w_ASCODFAM);
                        +" and FATIPDAT = "+cp_ToStrODBC(this.oParentObject.w_ASTIPDAT);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CPROWNUM;
                    from (i_cTable) where;
                        FACODICE = this.oParentObject.w_ASCODFAM;
                        and FATIPDAT = this.oParentObject.w_ASTIPDAT;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CAR = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_ASVALATT = DTOC(this.oParentObject.w_ASTIPDAT)
                this.w_COND = not empty(this.w_ASVALATT)
              else
                DO GSAR_BMG WITH THIS.OPARENTOBJECT, "ZAINS", this.w_FILTRO
                this.oParentObject.w_GSAR_BIA_FLAG = "N"
                i_retcode = 'stop'
                return
              endif
          endcase
          this.w_NOVAL = i_Rows=0
          * --- --si controlla se il valore esite nella tabella per quell'attributo
          if this.w_noval and this.w_COND
            * --- --Si inserisce il valore solo se l'attributo � di tipo MIsto
            if ah_YesNo("Il valore inserito non � presente, si vuole caricare come nuovo valore?")
              * --- Select from FAZDATTR
              i_nConn=i_TableProp[this.FAZDATTR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.FAZDATTR_idx,2],.t.,this.FAZDATTR_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MAX (CPROWNUM) AS MASSIMO, MAX(CPROWORD) AS MASSIMOR  from "+i_cTable+" FAZDATTR ";
                    +" where FACODICE="+cp_ToStrODBC(this.oParentObject.w_ASCODFAM)+"";
                     ,"_Curs_FAZDATTR")
              else
                select MAX (CPROWNUM) AS MASSIMO, MAX(CPROWORD) AS MASSIMOR from (i_cTable);
                 where FACODICE=this.oParentObject.w_ASCODFAM;
                  into cursor _Curs_FAZDATTR
              endif
              if used('_Curs_FAZDATTR')
                select _Curs_FAZDATTR
                locate for 1=1
                do while not(eof())
                this.w_MASSIMO = MASSIMO
                this.w_MASSIMOR = MASSIMOR
                  select _Curs_FAZDATTR
                  continue
                enddo
                use
              endif
              do case
                case this.oParentObject.w_INPUTA="C"
                  this.oParentObject.w_ASTIPCAR = LEFT(this.oParentObject.w_ASTIPCAR,this.oParentObject.w_FRDIMENS)
                  this.w_CARAT = LEFT(this.oParentObject.w_ASTIPCAR,this.oParentObject.w_FRDIMENS)
                case this.oParentObject.w_INPUTA="N"
                  this.oParentObject.w_ASTIPNUM = this.oParentObject.w_ASTIPNUM
                case this.oParentObject.w_INPUTA="D"
                  this.oParentObject.w_ASTIPDAT = this.oParentObject.w_ASTIPDAT
              endcase
              * --- Insert into FAZDATTR
              i_nConn=i_TableProp[this.FAZDATTR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.FAZDATTR_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FAZDATTR_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"FACODICE"+",CPROWNUM"+",CPROWORD"+",FAVALORE"+",FATIPCAR"+",FATIPDAT"+",FATIPNUM"+",FAINIVAL"+",FAFINVAL"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASCODFAM),'FAZDATTR','FACODICE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MASSIMO+1),'FAZDATTR','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MASSIMOR+10),'FAZDATTR','CPROWORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ASVALATT),'FAZDATTR','FAVALORE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CARAT),'FAZDATTR','FATIPCAR');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASTIPDAT),'FAZDATTR','FATIPDAT');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ASTIPNUM),'FAZDATTR','FATIPNUM');
                +","+cp_NullLink(cp_ToStrODBC(I_DATSYS),'FAZDATTR','FAINIVAL');
                +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("31-12-2099")),'FAZDATTR','FAFINVAL');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'FACODICE',this.oParentObject.w_ASCODFAM,'CPROWNUM',this.w_MASSIMO+1,'CPROWORD',this.w_MASSIMOR+10,'FAVALORE',this.w_ASVALATT,'FATIPCAR',this.w_CARAT,'FATIPDAT',this.oParentObject.w_ASTIPDAT,'FATIPNUM',this.oParentObject.w_ASTIPNUM,'FAINIVAL',I_DATSYS,'FAFINVAL',cp_CharToDate("31-12-2099"))
                insert into (i_cTable) (FACODICE,CPROWNUM,CPROWORD,FAVALORE,FATIPCAR,FATIPDAT,FATIPNUM,FAINIVAL,FAFINVAL &i_ccchkf. );
                   values (;
                     this.oParentObject.w_ASCODFAM;
                     ,this.w_MASSIMO+1;
                     ,this.w_MASSIMOR+10;
                     ,this.w_ASVALATT;
                     ,this.w_CARAT;
                     ,this.oParentObject.w_ASTIPDAT;
                     ,this.oParentObject.w_ASTIPNUM;
                     ,I_DATSYS;
                     ,cp_CharToDate("31-12-2099");
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          else
            this.w_CODFAM = this.oParentObject.w_ASCODFAM
            DO GSAR_BMG WITH THIS.OPARENTOBJECT, "ZAINS", this.w_FILTRO
          endif
      endcase
      * --- Metto il flag a 'U' se sono in modifica per salvare nel database la riga
      This.oParentObject.SetupdateRow()
    endif
    * --- "Quando GSAR_BIA � in esecuzione vale "S"
    *     Utilizzato da GSAR_BIA per non andare in esecuzione due volte 
    *     contemporaneamente (la valid di w_ASTIPCAR, quando si chiama
    *     lo zoom GSAR_BIA tramite doppio click, alla valid esegue una 
    *     notifyevent che attiva nuovamente GSAR_BIA)
    this.oParentObject.w_GSAR_BIA_FLAG = "N"
  endproc


  proc Init(oParentObject,pParam,pParam2)
    this.pParam=pParam
    this.pParam2=pParam2
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='FAZDATTR'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_FAZDATTR')
      use in _Curs_FAZDATTR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam,pParam2"
endproc
