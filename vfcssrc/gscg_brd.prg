* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_brd                                                        *
*              Controllo stampa registri IVA                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-07                                                      *
* Last revis.: 2004-05-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_brd",oParentObject)
return(i_retval)

define class tgscg_brd as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciata dalla Primanota (GSCG_MPN) per controllare se i registri iva presenti nella registrazione sono gia stati stampati
    * --- w_STAMPATO vale 'S' se almeno uno dei registri Iva presente nel castelletto Iva risulta avere, nelle attivit�, una data di stampa superiore alla data di registrazione
    * --- w_STAMPRCV vale 'S' se almeno uno dei registri Iva di tipo Corrispettivi in Ventilazione risulta avere, una data di stampa superiore alla data di registrazione
    VQ_EXEC("QUERY\GSCG_SRI.VQR",this,"REGSTAMP")
    if used("REGSTAMP")
      this.oParentObject.w_STAMPATO = IIF(""=ALLTRIM(NVL(STAMPATO,"")),"N",STAMPATO)
    endif
    VQ_EXEC("QUERY\GSCG1SRI.VQR",this,"REGSTRCV")
    if used("REGSTRCV")
      this.oParentObject.w_STAMPRCV = IIF(""=ALLTRIM(NVL(STAMPRCV,"")),"N",STAMPRCV)
    endif
    * --- Elimina i Cursori
    if USED("REGSTAMP")
      SELECT REGSTAMP
      USE
    endif
    if USED("REGSTRCV")
      SELECT REGSTRCV
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
