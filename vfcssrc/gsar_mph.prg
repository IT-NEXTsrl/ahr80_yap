* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mph                                                        *
*              Associazione path gruppi/persone                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-24                                                      *
* Last revis.: 2012-02-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mph")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mph")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mph")
  return

* --- Class definition
define class tgsar_mph as StdPCForm
  Width  = 606
  Height = 207
  Top    = 10
  Left   = 10
  cComment = "Associazione path gruppi/persone"
  cPrg = "gsar_mph"
  HelpContextID=147421033
  add object cnt as tcgsar_mph
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mph as PCContext
  w_APCODGRU = space(5)
  w_APPERSON = space(5)
  w_CPROWORD = 0
  w_APTIPEVE = space(10)
  w_AP__PATH = space(254)
  w_DESEVE = space(40)
  w_PATH = space(254)
  w_DRIVER = space(10)
  proc Save(i_oFrom)
    this.w_APCODGRU = i_oFrom.w_APCODGRU
    this.w_APPERSON = i_oFrom.w_APPERSON
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_APTIPEVE = i_oFrom.w_APTIPEVE
    this.w_AP__PATH = i_oFrom.w_AP__PATH
    this.w_DESEVE = i_oFrom.w_DESEVE
    this.w_PATH = i_oFrom.w_PATH
    this.w_DRIVER = i_oFrom.w_DRIVER
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_APCODGRU = this.w_APCODGRU
    i_oTo.w_APPERSON = this.w_APPERSON
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_APTIPEVE = this.w_APTIPEVE
    i_oTo.w_AP__PATH = this.w_AP__PATH
    i_oTo.w_DESEVE = this.w_DESEVE
    i_oTo.w_PATH = this.w_PATH
    i_oTo.w_DRIVER = this.w_DRIVER
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mph as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 606
  Height = 207
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-02-01"
  HelpContextID=147421033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ASS_PAGP_IDX = 0
  cFile = "ASS_PAGP"
  cKeySelect = "APCODGRU,APPERSON"
  cKeyWhere  = "APCODGRU=this.w_APCODGRU and APPERSON=this.w_APPERSON"
  cKeyDetail  = "APCODGRU=this.w_APCODGRU and APPERSON=this.w_APPERSON"
  cKeyWhereODBC = '"APCODGRU="+cp_ToStrODBC(this.w_APCODGRU)';
      +'+" and APPERSON="+cp_ToStrODBC(this.w_APPERSON)';

  cKeyDetailWhereODBC = '"APCODGRU="+cp_ToStrODBC(this.w_APCODGRU)';
      +'+" and APPERSON="+cp_ToStrODBC(this.w_APPERSON)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ASS_PAGP.APCODGRU="+cp_ToStrODBC(this.w_APCODGRU)';
      +'+" and ASS_PAGP.APPERSON="+cp_ToStrODBC(this.w_APPERSON)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ASS_PAGP.CPROWORD '
  cPrg = "gsar_mph"
  cComment = "Associazione path gruppi/persone"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_APCODGRU = space(5)
  w_APPERSON = space(5)
  w_CPROWORD = 0
  w_APTIPEVE = space(10)
  w_AP__PATH = space(254)
  w_DESEVE = space(40)
  w_PATH = space(254)
  w_DRIVER = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_mph
  * tabella aggiunta attraverso l'area manuale open work tables
  * per condizionare utilizzo in funzione del modulo agenda
  TIPEVENT_IDX = 0
  PORTI_IDX = 0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mphPag1","gsar_mph",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ASS_PAGP'
    * --- Area Manuale = Open Work Table
    * --- gsar_mph
    LOCAL nlen
    nlen=alen(this.cWorkTables)
    dimension this.cWorkTables(nlen + 1)
    IF g_AGFA<>'S'
        this.cWorkTables[nlen + 1]='PORTI'
      else
        this.cWorkTables[nlen + 1]='TIPEVENT'
    ENDIF
     return(this.OpenAllTables(nlen + 1))
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ASS_PAGP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ASS_PAGP_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mph'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ASS_PAGP where APCODGRU=KeySet.APCODGRU
    *                            and APPERSON=KeySet.APPERSON
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ASS_PAGP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASS_PAGP_IDX,2],this.bLoadRecFilter,this.ASS_PAGP_IDX,"gsar_mph")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ASS_PAGP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ASS_PAGP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ASS_PAGP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'APCODGRU',this.w_APCODGRU  ,'APPERSON',this.w_APPERSON  )
      select * from (i_cTable) ASS_PAGP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_APCODGRU = NVL(APCODGRU,space(5))
        .w_APPERSON = NVL(APPERSON,space(5))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ASS_PAGP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESEVE = space(40)
          .w_PATH = space(254)
          .w_DRIVER = space(10)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_APTIPEVE = NVL(APTIPEVE,space(10))
          .link_2_2('Load')
          .w_AP__PATH = NVL(AP__PATH,space(254))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_APCODGRU=space(5)
      .w_APPERSON=space(5)
      .w_CPROWORD=10
      .w_APTIPEVE=space(10)
      .w_AP__PATH=space(254)
      .w_DESEVE=space(40)
      .w_PATH=space(254)
      .w_DRIVER=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_APTIPEVE))
         .link_2_2('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ASS_PAGP')
    this.DoRTCalc(5,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ASS_PAGP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ASS_PAGP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APCODGRU,"APCODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APPERSON,"APPERSON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_APTIPEVE C(10);
      ,t_AP__PATH C(254);
      ,t_DESEVE C(40);
      ,CPROWNUM N(10);
      ,t_PATH C(254);
      ,t_DRIVER C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mphbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPEVE_2_2.controlsource=this.cTrsName+'.t_APTIPEVE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAP__PATH_2_3.controlsource=this.cTrsName+'.t_AP__PATH'
    this.oPgFRm.Page1.oPag.oDESEVE_2_4.controlsource=this.cTrsName+'.t_DESEVE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(57)
    this.AddVLine(142)
    this.AddVLine(559)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ASS_PAGP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASS_PAGP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ASS_PAGP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASS_PAGP_IDX,2])
      *
      * insert into ASS_PAGP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ASS_PAGP')
        i_extval=cp_InsertValODBCExtFlds(this,'ASS_PAGP')
        i_cFldBody=" "+;
                  "(APCODGRU,APPERSON,CPROWORD,APTIPEVE,AP__PATH,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_APCODGRU)+","+cp_ToStrODBC(this.w_APPERSON)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_APTIPEVE)+","+cp_ToStrODBC(this.w_AP__PATH)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ASS_PAGP')
        i_extval=cp_InsertValVFPExtFlds(this,'ASS_PAGP')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'APCODGRU',this.w_APCODGRU,'APPERSON',this.w_APPERSON)
        INSERT INTO (i_cTable) (;
                   APCODGRU;
                  ,APPERSON;
                  ,CPROWORD;
                  ,APTIPEVE;
                  ,AP__PATH;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_APCODGRU;
                  ,this.w_APPERSON;
                  ,this.w_CPROWORD;
                  ,this.w_APTIPEVE;
                  ,this.w_AP__PATH;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ASS_PAGP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASS_PAGP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_AP__PATH)) and not(Empty(t_APTIPEVE))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ASS_PAGP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ASS_PAGP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_AP__PATH)) and not(Empty(t_APTIPEVE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ASS_PAGP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ASS_PAGP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",APTIPEVE="+cp_ToStrODBCNull(this.w_APTIPEVE)+;
                     ",AP__PATH="+cp_ToStrODBC(this.w_AP__PATH)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ASS_PAGP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,APTIPEVE=this.w_APTIPEVE;
                     ,AP__PATH=this.w_AP__PATH;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ASS_PAGP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASS_PAGP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_AP__PATH)) and not(Empty(t_APTIPEVE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ASS_PAGP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_AP__PATH)) and not(Empty(t_APTIPEVE))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ASS_PAGP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASS_PAGP_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PATH with this.w_PATH
      replace t_DRIVER with this.w_DRIVER
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_mph
    if Cevent='Aggiorna'
       This.w_PATH=left(cp_getdir(IIF(EMPTY(this.w_AP__PATH),sys(5)+sys(2003),this.w_AP__PATH),"Percorso tipo evento")+space(254),254)
       This.w_AP__PATH=iif(Empty(this.w_PATH),this.w_AP__PATH,this.w_PATH)
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=APTIPEVE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APTIPEVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSFA_ATE',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_APTIPEVE)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_APTIPEVE))
          select TETIPEVE,TEDESCRI,TEDRIVER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_APTIPEVE)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStrODBC(trim(this.w_APTIPEVE)+"%");

            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStr(trim(this.w_APTIPEVE)+"%");

            select TETIPEVE,TEDESCRI,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_APTIPEVE) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oAPTIPEVE_2_2'),i_cWhere,'GSFA_ATE',"Tipi evento",'GSFADKTE.TIPEVENT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APTIPEVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_APTIPEVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_APTIPEVE)
            select TETIPEVE,TEDESCRI,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APTIPEVE = NVL(_Link_.TETIPEVE,space(10))
      this.w_DESEVE = NVL(_Link_.TEDESCRI,space(40))
      this.w_DRIVER = NVL(_Link_.TEDRIVER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_APTIPEVE = space(10)
      endif
      this.w_DESEVE = space(40)
      this.w_DRIVER = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Not Empty(.w_DRIVER)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, tipo evento senza codice driver")
        endif
        this.w_APTIPEVE = space(10)
        this.w_DESEVE = space(40)
        this.w_DRIVER = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APTIPEVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESEVE_2_4.value==this.w_DESEVE)
      this.oPgFrm.Page1.oPag.oDESEVE_2_4.value=this.w_DESEVE
      replace t_DESEVE with this.oPgFrm.Page1.oPag.oDESEVE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPEVE_2_2.value==this.w_APTIPEVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPEVE_2_2.value=this.w_APTIPEVE
      replace t_APTIPEVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPEVE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAP__PATH_2_3.value==this.w_AP__PATH)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAP__PATH_2_3.value=this.w_AP__PATH
      replace t_AP__PATH with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAP__PATH_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'ASS_PAGP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(Not Empty(.w_DRIVER)) and not(empty(.w_APTIPEVE)) and (not(Empty(.w_AP__PATH)) and not(Empty(.w_APTIPEVE)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPEVE_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Attenzione, tipo evento senza codice driver")
      endcase
      if not(Empty(.w_AP__PATH)) and not(Empty(.w_APTIPEVE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_AP__PATH)) and not(Empty(t_APTIPEVE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_APTIPEVE=space(10)
      .w_AP__PATH=space(254)
      .w_DESEVE=space(40)
      .w_PATH=space(254)
      .w_DRIVER=space(10)
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_APTIPEVE))
        .link_2_2('Full')
      endif
    endwith
    this.DoRTCalc(5,8,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_APTIPEVE = t_APTIPEVE
    this.w_AP__PATH = t_AP__PATH
    this.w_DESEVE = t_DESEVE
    this.w_PATH = t_PATH
    this.w_DRIVER = t_DRIVER
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_APTIPEVE with this.w_APTIPEVE
    replace t_AP__PATH with this.w_AP__PATH
    replace t_DESEVE with this.w_DESEVE
    replace t_PATH with this.w_PATH
    replace t_DRIVER with this.w_DRIVER
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mphPag1 as StdContainer
  Width  = 602
  height = 207
  stdWidth  = 602
  stdheight = 207
  resizeXpos=288
  resizeYpos=92
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=21, width=566,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="CPROWORD",Label1="Riga",Field2="APTIPEVE",Label2="Tipo",Field3="AP__PATH",Label3="Path ",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 99765638

  add object oStr_1_3 as StdString with uid="CKXWBYNIPH",Visible=.t., Left=563, Top=25,;
    Alignment=0, Width=18, Height=18,;
    Caption="P"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=41,;
    width=585+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=42,width=584+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TIPEVENT|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESEVE_2_4.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TIPEVENT'
        oDropInto=this.oBodyCol.oRow.oAPTIPEVE_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oDESEVE_2_4 as StdTrsField with uid="OGYHCVNDKO",rtseq=6,rtrep=.t.,;
    cFormVar="w_DESEVE",value=space(40),enabled=.f.,;
    HelpContextID = 31523382,;
    cTotal="", bFixedPos=.t., cQueryName = "DESEVE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=528, Left=56, Top=179, InputMask=replicate('X',40)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mphBodyRow as CPBodyRowCnt
  Width=575
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="VUPUIIGFSH",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Riga",;
    HelpContextID = 200998250,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0

  add object oAPTIPEVE_2_2 as StdTrsField with uid="KWRCUSDLLA",rtseq=4,rtrep=.t.,;
    cFormVar="w_APTIPEVE",value=space(10),;
    ToolTipText = "Tipo evento",;
    HelpContextID = 242934453,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, tipo evento senza codice driver",;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=49, Top=1, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", cZoomOnZoom="GSFA_ATE", oKey_1_1="TETIPEVE", oKey_1_2="this.w_APTIPEVE"

  func oAPTIPEVE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oAPTIPEVE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAPTIPEVE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oAPTIPEVE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSFA_ATE',"Tipi evento",'GSFADKTE.TIPEVENT_VZM',this.parent.oContained
  endproc
  proc oAPTIPEVE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSFA_ATE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TETIPEVE=this.parent.oContained.w_APTIPEVE
    i_obj.ecpSave()
  endproc

  add object oAP__PATH_2_3 as StdTrsField with uid="AVVGXNUTYN",rtseq=5,rtrep=.t.,;
    cFormVar="w_AP__PATH",value=space(254),;
    HelpContextID = 228314446,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=415, Left=132, Top=1, InputMask=replicate('X',254)

  add object oBtn_2_7 as StdButton with uid="TWIMIXDREE",width=19,height=19,;
   left=551, top=0,;
    caption="...", nPag=2;
    , HelpContextID = 147220010;
  , bGlobalFont=.t.

    proc oBtn_2_7.Click()
      this.parent.oContained.NotifyEvent("Aggiorna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mph','ASS_PAGP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".APCODGRU=ASS_PAGP.APCODGRU";
  +" and "+i_cAliasName2+".APPERSON=ASS_PAGP.APPERSON";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
