* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mel                                                        *
*              Dati comunicazione elenchi clienti/fornitori                    *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-25                                                      *
* Last revis.: 2011-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_mel"))

* --- Class definition
define class tgscg_mel as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 863
  Height = 423+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-09-23"
  HelpContextID=204855959
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  DAELCLFO_IDX = 0
  DAELCLFD_IDX = 0
  CONTI_IDX = 0
  cFile = "DAELCLFO"
  cFileDetail = "DAELCLFD"
  cKeySelect = "DESERIAL"
  cKeyWhere  = "DESERIAL=this.w_DESERIAL"
  cKeyDetail  = "DESERIAL=this.w_DESERIAL"
  cKeyWhereODBC = '"DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';

  cKeyDetailWhereODBC = '"DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"DAELCLFD.DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DAELCLFD.CPROWNUM '
  cPrg = "gscg_mel"
  cComment = "Dati comunicazione elenchi clienti/fornitori"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DESERIAL = space(10)
  w_DE__ANNO = space(4)
  w_DETIPSOG = space(1)
  o_DETIPSOG = space(1)
  w_DEPARIVA = space(12)
  w_DECODFIS = space(16)
  w_DEIMPIMP = 0
  o_DEIMPIMP = 0
  w_XIMIMP = 0
  w_DEIMPAFF = 0
  w_DEIMPNOI = 0
  w_DEIMPESE = 0
  w_DEIMPNIN = 0
  w_DEIMPLOR = 0
  w_DERIFPNT = space(10)
  w_DECODCON = space(15)
  o_DECODCON = space(15)
  w_DEDESCON = space(50)
  w_DECONPLU = space(1)
  w_DEESCGEN = space(1)
  w_DETIPCON = space(1)
  o_DETIPCON = space(1)
  w_DESCLF = space(40)
  w_OBTEST = space(10)
  w_DTOBSO = ctod('  /  /  ')
  w_TTIMPIMP = 0
  w_TTIMPAFF = 0
  w_TTIMPNOI = 0
  w_TTIMPESE = 0
  w_TTIMPNIN = 0
  w_TTIMPLOR = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_DESERIAL = this.W_DESERIAL
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DAELCLFO','gscg_mel')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_melPag1","gscg_mel",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati comunicazione elenchi clienti/fornitori")
      .Pages(1).HelpContextID = 130590340
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DAELCLFO'
    this.cWorkTables[3]='DAELCLFD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DAELCLFO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DAELCLFO_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_DESERIAL = NVL(DESERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from DAELCLFO where DESERIAL=KeySet.DESERIAL
    *
    i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2],this.bLoadRecFilter,this.DAELCLFO_IDX,"gscg_mel")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DAELCLFO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DAELCLFO.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"DAELCLFD.","DAELCLFO.")
      i_cTable = i_cTable+' DAELCLFO '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DESCLF = space(40)
        .w_OBTEST = date(iif(not empty(.w_DE__ANNO), val(.w_DE__ANNO), year(i_datsys)-1),1,1)
        .w_DTOBSO = ctod("  /  /  ")
        .w_TTIMPIMP = 0
        .w_TTIMPAFF = 0
        .w_TTIMPNOI = 0
        .w_TTIMPESE = 0
        .w_TTIMPNIN = 0
        .w_TTIMPLOR = 0
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_DESERIAL = NVL(DESERIAL,space(10))
        .op_DESERIAL = .w_DESERIAL
        .w_DE__ANNO = NVL(DE__ANNO,space(4))
        .w_DETIPSOG = NVL(DETIPSOG,space(1))
        .w_DEPARIVA = NVL(DEPARIVA,space(12))
        .w_DECODFIS = NVL(DECODFIS,space(16))
        .w_DECODCON = NVL(DECODCON,space(15))
          if link_1_11_joined
            this.w_DECODCON = NVL(ANCODICE111,NVL(this.w_DECODCON,space(15)))
            this.w_DESCLF = NVL(ANDESCRI111,space(40))
            this.w_DTOBSO = NVL(cp_ToDate(ANDTOBSO111),ctod("  /  /  "))
          else
          .link_1_11('Load')
          endif
        .w_DEDESCON = NVL(DEDESCON,space(50))
        .w_DECONPLU = NVL(DECONPLU,space(1))
        .w_DEESCGEN = NVL(DEESCGEN,space(1))
        .w_DETIPCON = NVL(DETIPCON,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DAELCLFO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from DAELCLFD where DESERIAL=KeySet.DESERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.DAELCLFD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFD_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('DAELCLFD')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "DAELCLFD.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" DAELCLFD"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
        select * from (i_cTable) DAELCLFD where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TTIMPIMP = 0
      this.w_TTIMPAFF = 0
      this.w_TTIMPNOI = 0
      this.w_TTIMPESE = 0
      this.w_TTIMPNIN = 0
      this.w_TTIMPLOR = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_DEIMPIMP = NVL(DEIMPIMP,0)
        .w_XIMIMP = .w_DEIMPIMP
          .w_DEIMPAFF = NVL(DEIMPAFF,0)
          .w_DEIMPNOI = NVL(DEIMPNOI,0)
          .w_DEIMPESE = NVL(DEIMPESE,0)
          .w_DEIMPNIN = NVL(DEIMPNIN,0)
          .w_DEIMPLOR = NVL(DEIMPLOR,0)
          .w_DERIFPNT = NVL(DERIFPNT,space(10))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TTIMPIMP = .w_TTIMPIMP+.w_XIMIMP
          .w_TTIMPAFF = .w_TTIMPAFF+.w_DEIMPAFF
          .w_TTIMPNOI = .w_TTIMPNOI+.w_DEIMPNOI
          .w_TTIMPESE = .w_TTIMPESE+.w_DEIMPESE
          .w_TTIMPNIN = .w_TTIMPNIN+.w_DEIMPNIN
          .w_TTIMPLOR = .w_TTIMPLOR+.w_DEIMPLOR
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_9.enabled = .oPgFrm.Page1.oPag.oBtn_2_9.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DESERIAL=space(10)
      .w_DE__ANNO=space(4)
      .w_DETIPSOG=space(1)
      .w_DEPARIVA=space(12)
      .w_DECODFIS=space(16)
      .w_DEIMPIMP=0
      .w_XIMIMP=0
      .w_DEIMPAFF=0
      .w_DEIMPNOI=0
      .w_DEIMPESE=0
      .w_DEIMPNIN=0
      .w_DEIMPLOR=0
      .w_DERIFPNT=space(10)
      .w_DECODCON=space(15)
      .w_DEDESCON=space(50)
      .w_DECONPLU=space(1)
      .w_DEESCGEN=space(1)
      .w_DETIPCON=space(1)
      .w_DESCLF=space(40)
      .w_OBTEST=space(10)
      .w_DTOBSO=ctod("  /  /  ")
      .w_TTIMPIMP=0
      .w_TTIMPAFF=0
      .w_TTIMPNOI=0
      .w_TTIMPESE=0
      .w_TTIMPNIN=0
      .w_TTIMPLOR=0
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,1,.f.)
        .w_DE__ANNO = ALLTRIM(STR(YEAR(i_DATSYS)-1))
        .w_DETIPSOG = 'C'
        .DoRTCalc(4,6,.f.)
        .w_XIMIMP = .w_DEIMPIMP
        .DoRTCalc(8,14,.f.)
        if not(empty(.w_DECODCON))
         .link_1_11('Full')
        endif
        .w_DEDESCON = .w_DESCLF
        .DoRTCalc(16,17,.f.)
        .w_DETIPCON = 'C'
        .DoRTCalc(19,19,.f.)
        .w_OBTEST = date(iif(not empty(.w_DE__ANNO), val(.w_DE__ANNO), year(i_datsys)-1),1,1)
      endif
    endwith
    cp_BlankRecExtFlds(this,'DAELCLFO')
    this.DoRTCalc(21,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_9.enabled = this.oPgFrm.Page1.oPag.oBtn_2_9.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDE__ANNO_1_3.enabled = i_bVal
      .Page1.oPag.oDETIPSOG_1_5.enabled = i_bVal
      .Page1.oPag.oDEPARIVA_1_7.enabled = i_bVal
      .Page1.oPag.oDECODFIS_1_9.enabled = i_bVal
      .Page1.oPag.oDEDESCON_1_12.enabled = i_bVal
      .Page1.oPag.oDEESCGEN_1_15.enabled = i_bVal
      .Page1.oPag.oBtn_2_9.enabled = .Page1.oPag.oBtn_2_9.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DAELCLFO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEECF","i_CODAZI,w_DESERIAL")
      .op_CODAZI = .w_CODAZI
      .op_DESERIAL = .w_DESERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DESERIAL,"DESERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DE__ANNO,"DE__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DETIPSOG,"DETIPSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEPARIVA,"DEPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECODFIS,"DECODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECODCON,"DECODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEDESCON,"DEDESCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECONPLU,"DECONPLU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEESCGEN,"DEESCGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DETIPCON,"DETIPCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
    i_lTable = "DAELCLFO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DAELCLFO_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DEIMPIMP N(18,4);
      ,t_DEIMPAFF N(18,4);
      ,t_DEIMPNOI N(18,4);
      ,t_DEIMPESE N(18,4);
      ,t_DEIMPNIN N(18,4);
      ,t_DEIMPLOR N(18,4);
      ,CPROWNUM N(10);
      ,t_XIMIMP N(18,4);
      ,t_DERIFPNT C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_melbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPIMP_2_1.controlsource=this.cTrsName+'.t_DEIMPIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPAFF_2_3.controlsource=this.cTrsName+'.t_DEIMPAFF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPNOI_2_4.controlsource=this.cTrsName+'.t_DEIMPNOI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPESE_2_5.controlsource=this.cTrsName+'.t_DEIMPESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPNIN_2_6.controlsource=this.cTrsName+'.t_DEIMPNIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPLOR_2_7.controlsource=this.cTrsName+'.t_DEIMPLOR'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(139)
    this.AddVLine(279)
    this.AddVLine(420)
    this.AddVLine(561)
    this.AddVLine(701)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPIMP_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEECF","i_CODAZI,w_DESERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DAELCLFO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DAELCLFO')
        i_extval=cp_InsertValODBCExtFlds(this,'DAELCLFO')
        local i_cFld
        i_cFld=" "+;
                  "(DESERIAL,DE__ANNO,DETIPSOG,DEPARIVA,DECODFIS"+;
                  ",DECODCON,DEDESCON,DECONPLU,DEESCGEN,DETIPCON"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_DESERIAL)+;
                    ","+cp_ToStrODBC(this.w_DE__ANNO)+;
                    ","+cp_ToStrODBC(this.w_DETIPSOG)+;
                    ","+cp_ToStrODBC(this.w_DEPARIVA)+;
                    ","+cp_ToStrODBC(this.w_DECODFIS)+;
                    ","+cp_ToStrODBCNull(this.w_DECODCON)+;
                    ","+cp_ToStrODBC(this.w_DEDESCON)+;
                    ","+cp_ToStrODBC(this.w_DECONPLU)+;
                    ","+cp_ToStrODBC(this.w_DEESCGEN)+;
                    ","+cp_ToStrODBC(this.w_DETIPCON)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DAELCLFO')
        i_extval=cp_InsertValVFPExtFlds(this,'DAELCLFO')
        cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_DESERIAL)
        INSERT INTO (i_cTable);
              (DESERIAL,DE__ANNO,DETIPSOG,DEPARIVA,DECODFIS,DECODCON,DEDESCON,DECONPLU,DEESCGEN,DETIPCON &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_DESERIAL;
                  ,this.w_DE__ANNO;
                  ,this.w_DETIPSOG;
                  ,this.w_DEPARIVA;
                  ,this.w_DECODFIS;
                  ,this.w_DECODCON;
                  ,this.w_DEDESCON;
                  ,this.w_DECONPLU;
                  ,this.w_DEESCGEN;
                  ,this.w_DETIPCON;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DAELCLFD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFD_IDX,2])
      *
      * insert into DAELCLFD
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(DESERIAL,DEIMPIMP,DEIMPAFF,DEIMPNOI,DEIMPESE"+;
                  ",DEIMPNIN,DEIMPLOR,DERIFPNT,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DESERIAL)+","+cp_ToStrODBC(this.w_DEIMPIMP)+","+cp_ToStrODBC(this.w_DEIMPAFF)+","+cp_ToStrODBC(this.w_DEIMPNOI)+","+cp_ToStrODBC(this.w_DEIMPESE)+;
             ","+cp_ToStrODBC(this.w_DEIMPNIN)+","+cp_ToStrODBC(this.w_DEIMPLOR)+","+cp_ToStrODBC(this.w_DERIFPNT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DESERIAL',this.w_DESERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_DESERIAL,this.w_DEIMPIMP,this.w_DEIMPAFF,this.w_DEIMPNOI,this.w_DEIMPESE"+;
                ",this.w_DEIMPNIN,this.w_DEIMPLOR,this.w_DERIFPNT,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update DAELCLFO
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'DAELCLFO')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " DE__ANNO="+cp_ToStrODBC(this.w_DE__ANNO)+;
             ",DETIPSOG="+cp_ToStrODBC(this.w_DETIPSOG)+;
             ",DEPARIVA="+cp_ToStrODBC(this.w_DEPARIVA)+;
             ",DECODFIS="+cp_ToStrODBC(this.w_DECODFIS)+;
             ",DECODCON="+cp_ToStrODBCNull(this.w_DECODCON)+;
             ",DEDESCON="+cp_ToStrODBC(this.w_DEDESCON)+;
             ",DECONPLU="+cp_ToStrODBC(this.w_DECONPLU)+;
             ",DEESCGEN="+cp_ToStrODBC(this.w_DEESCGEN)+;
             ",DETIPCON="+cp_ToStrODBC(this.w_DETIPCON)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'DAELCLFO')
          i_cWhere = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
          UPDATE (i_cTable) SET;
              DE__ANNO=this.w_DE__ANNO;
             ,DETIPSOG=this.w_DETIPSOG;
             ,DEPARIVA=this.w_DEPARIVA;
             ,DECODFIS=this.w_DECODFIS;
             ,DECODCON=this.w_DECODCON;
             ,DEDESCON=this.w_DEDESCON;
             ,DECONPLU=this.w_DECONPLU;
             ,DEESCGEN=this.w_DEESCGEN;
             ,DETIPCON=this.w_DETIPCON;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DEIMPIMP)) or not(Empty(t_DEIMPAFF)) or not(Empty(t_DEIMPNOI)) or not(Empty(t_DEIMPESE)) or not(Empty(t_DEIMPNIN)) or not(Empty(t_DEIMPLOR))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.DAELCLFD_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFD_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from DAELCLFD
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DAELCLFD
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DEIMPIMP="+cp_ToStrODBC(this.w_DEIMPIMP)+;
                     ",DEIMPAFF="+cp_ToStrODBC(this.w_DEIMPAFF)+;
                     ",DEIMPNOI="+cp_ToStrODBC(this.w_DEIMPNOI)+;
                     ",DEIMPESE="+cp_ToStrODBC(this.w_DEIMPESE)+;
                     ",DEIMPNIN="+cp_ToStrODBC(this.w_DEIMPNIN)+;
                     ",DEIMPLOR="+cp_ToStrODBC(this.w_DEIMPLOR)+;
                     ",DERIFPNT="+cp_ToStrODBC(this.w_DERIFPNT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DEIMPIMP=this.w_DEIMPIMP;
                     ,DEIMPAFF=this.w_DEIMPAFF;
                     ,DEIMPNOI=this.w_DEIMPNOI;
                     ,DEIMPESE=this.w_DEIMPESE;
                     ,DEIMPNIN=this.w_DEIMPNIN;
                     ,DEIMPLOR=this.w_DEIMPLOR;
                     ,DERIFPNT=this.w_DERIFPNT;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DEIMPIMP)) or not(Empty(t_DEIMPAFF)) or not(Empty(t_DEIMPNOI)) or not(Empty(t_DEIMPESE)) or not(Empty(t_DEIMPNIN)) or not(Empty(t_DEIMPLOR))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.DAELCLFD_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFD_IDX,2])
        *
        * delete DAELCLFD
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
        *
        * delete DAELCLFO
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DEIMPIMP)) or not(Empty(t_DEIMPAFF)) or not(Empty(t_DEIMPNOI)) or not(Empty(t_DEIMPESE)) or not(Empty(t_DEIMPNIN)) or not(Empty(t_DEIMPLOR))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,6,.t.)
        if .o_DEIMPIMP<>.w_DEIMPIMP
          .w_TTIMPIMP = .w_TTIMPIMP-.w_ximimp
          .w_XIMIMP = .w_DEIMPIMP
          .w_TTIMPIMP = .w_TTIMPIMP+.w_ximimp
        endif
        .DoRTCalc(8,13,.t.)
          .link_1_11('Full')
        if .o_DECODCON<>.w_DECODCON
          .w_DEDESCON = .w_DESCLF
        endif
        if .o_DETIPCON<>.w_DETIPCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_DECODCON<>.w_DECODCON
          .Calculate_YKYNWABSSS()
        endif
        if .o_DEIMPIMP<>.w_DEIMPIMP
          .Calculate_VHFVLPDISV()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEECF","i_CODAZI,w_DESERIAL")
          .op_DESERIAL = .w_DESERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(16,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_XIMIMP with this.w_XIMIMP
      replace t_DERIFPNT with this.w_DERIFPNT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_YKYNWABSSS()
    with this
          * --- Variazione identificativo su movimento generato
          GSCG_BEL(this;
             )
    endwith
  endproc
  proc Calculate_VHFVLPDISV()
    with this
          * --- Verifica variazione imponibile
          CheckDEIMPIMP(this;
             )
    endwith
  endproc
  proc Calculate_ROSCJVPQMD()
    with this
          * --- Controllo flag codifica plurima
          GSCG_BEX(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEIMPAFF_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEIMPAFF_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEIMPNOI_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEIMPNOI_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEIMPESE_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEIMPESE_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEIMPNIN_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEIMPNIN_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEIMPLOR_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEIMPLOR_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_9.enabled =this.oPgFrm.Page1.oPag.oBtn_2_9.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Record Deleted") or lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_ROSCJVPQMD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DECODCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DECODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DETIPCON;
                       ,'ANCODICE',this.w_DECODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DECODCON = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DECODCON = space(15)
        this.w_DESCLF = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.ANCODICE as ANCODICE111"+ ",link_1_11.ANDESCRI as ANDESCRI111"+ ",link_1_11.ANDTOBSO as ANDTOBSO111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on DAELCLFO.DECODCON=link_1_11.ANCODICE"+" and DAELCLFO.DETIPCON=link_1_11.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and DAELCLFO.DECODCON=link_1_11.ANCODICE(+)"'+'+" and DAELCLFO.DETIPCON=link_1_11.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDE__ANNO_1_3.value==this.w_DE__ANNO)
      this.oPgFrm.Page1.oPag.oDE__ANNO_1_3.value=this.w_DE__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDETIPSOG_1_5.RadioValue()==this.w_DETIPSOG)
      this.oPgFrm.Page1.oPag.oDETIPSOG_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEPARIVA_1_7.value==this.w_DEPARIVA)
      this.oPgFrm.Page1.oPag.oDEPARIVA_1_7.value=this.w_DEPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDECODFIS_1_9.value==this.w_DECODFIS)
      this.oPgFrm.Page1.oPag.oDECODFIS_1_9.value=this.w_DECODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDECODCON_1_11.value==this.w_DECODCON)
      this.oPgFrm.Page1.oPag.oDECODCON_1_11.value=this.w_DECODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDESCON_1_12.value==this.w_DEDESCON)
      this.oPgFrm.Page1.oPag.oDEDESCON_1_12.value=this.w_DEDESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDECONPLU_1_14.RadioValue()==this.w_DECONPLU)
      this.oPgFrm.Page1.oPag.oDECONPLU_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEESCGEN_1_15.RadioValue()==this.w_DEESCGEN)
      this.oPgFrm.Page1.oPag.oDEESCGEN_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDETIPCON_1_16.RadioValue()==this.w_DETIPCON)
      this.oPgFrm.Page1.oPag.oDETIPCON_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_17.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_17.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oTTIMPIMP_3_1.value==this.w_TTIMPIMP)
      this.oPgFrm.Page1.oPag.oTTIMPIMP_3_1.value=this.w_TTIMPIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oTTIMPAFF_3_2.value==this.w_TTIMPAFF)
      this.oPgFrm.Page1.oPag.oTTIMPAFF_3_2.value=this.w_TTIMPAFF
    endif
    if not(this.oPgFrm.Page1.oPag.oTTIMPNOI_3_3.value==this.w_TTIMPNOI)
      this.oPgFrm.Page1.oPag.oTTIMPNOI_3_3.value=this.w_TTIMPNOI
    endif
    if not(this.oPgFrm.Page1.oPag.oTTIMPESE_3_4.value==this.w_TTIMPESE)
      this.oPgFrm.Page1.oPag.oTTIMPESE_3_4.value=this.w_TTIMPESE
    endif
    if not(this.oPgFrm.Page1.oPag.oTTIMPNIN_3_5.value==this.w_TTIMPNIN)
      this.oPgFrm.Page1.oPag.oTTIMPNIN_3_5.value=this.w_TTIMPNIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTTIMPLOR_3_6.value==this.w_TTIMPLOR)
      this.oPgFrm.Page1.oPag.oTTIMPLOR_3_6.value=this.w_TTIMPLOR
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPIMP_2_1.value==this.w_DEIMPIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPIMP_2_1.value=this.w_DEIMPIMP
      replace t_DEIMPIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPIMP_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPAFF_2_3.value==this.w_DEIMPAFF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPAFF_2_3.value=this.w_DEIMPAFF
      replace t_DEIMPAFF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPAFF_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPNOI_2_4.value==this.w_DEIMPNOI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPNOI_2_4.value=this.w_DEIMPNOI
      replace t_DEIMPNOI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPNOI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPESE_2_5.value==this.w_DEIMPESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPESE_2_5.value=this.w_DEIMPESE
      replace t_DEIMPESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPESE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPNIN_2_6.value==this.w_DEIMPNIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPNIN_2_6.value=this.w_DEIMPNIN
      replace t_DEIMPNIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPNIN_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPLOR_2_7.value==this.w_DEIMPLOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPLOR_2_7.value=this.w_DEIMPLOR
      replace t_DEIMPLOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPLOR_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'DAELCLFO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(Val(.w_DE__ANNO)>2005 and Val(.w_DE__ANNO)<2100)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDE__ANNO_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_DEPARIVA, "PI", .w_DETIPCON, .w_DECODCON, SPACE(3)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDEPARIVA_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_DECODFIS, 'CF', .w_DETIPCON, .w_DECODCON))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDECODFIS_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO))  and not(empty(.w_DECODCON))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDECODCON_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (not(Empty(t_DEIMPIMP)) or not(Empty(t_DEIMPAFF)) or not(Empty(t_DEIMPNOI)) or not(Empty(t_DEIMPESE)) or not(Empty(t_DEIMPNIN)) or not(Empty(t_DEIMPLOR)));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_DEIMPIMP)) or not(Empty(.w_DEIMPAFF)) or not(Empty(.w_DEIMPNOI)) or not(Empty(.w_DEIMPESE)) or not(Empty(.w_DEIMPNIN)) or not(Empty(.w_DEIMPLOR))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DETIPSOG = this.w_DETIPSOG
    this.o_DEIMPIMP = this.w_DEIMPIMP
    this.o_DECODCON = this.w_DECODCON
    this.o_DETIPCON = this.w_DETIPCON
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DEIMPIMP)) or not(Empty(t_DEIMPAFF)) or not(Empty(t_DEIMPNOI)) or not(Empty(t_DEIMPESE)) or not(Empty(t_DEIMPNIN)) or not(Empty(t_DEIMPLOR)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DEIMPIMP=0
      .w_XIMIMP=0
      .w_DEIMPAFF=0
      .w_DEIMPNOI=0
      .w_DEIMPESE=0
      .w_DEIMPNIN=0
      .w_DEIMPLOR=0
      .w_DERIFPNT=space(10)
      .DoRTCalc(1,6,.f.)
        .w_XIMIMP = .w_DEIMPIMP
    endwith
    this.DoRTCalc(8,27,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DEIMPIMP = t_DEIMPIMP
    this.w_XIMIMP = t_XIMIMP
    this.w_DEIMPAFF = t_DEIMPAFF
    this.w_DEIMPNOI = t_DEIMPNOI
    this.w_DEIMPESE = t_DEIMPESE
    this.w_DEIMPNIN = t_DEIMPNIN
    this.w_DEIMPLOR = t_DEIMPLOR
    this.w_DERIFPNT = t_DERIFPNT
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DEIMPIMP with this.w_DEIMPIMP
    replace t_XIMIMP with this.w_XIMIMP
    replace t_DEIMPAFF with this.w_DEIMPAFF
    replace t_DEIMPNOI with this.w_DEIMPNOI
    replace t_DEIMPESE with this.w_DEIMPESE
    replace t_DEIMPNIN with this.w_DEIMPNIN
    replace t_DEIMPLOR with this.w_DEIMPLOR
    replace t_DERIFPNT with this.w_DERIFPNT
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TTIMPIMP = .w_TTIMPIMP-.w_ximimp
        .w_TTIMPAFF = .w_TTIMPAFF-.w_deimpaff
        .w_TTIMPNOI = .w_TTIMPNOI-.w_deimpnoi
        .w_TTIMPESE = .w_TTIMPESE-.w_deimpese
        .w_TTIMPNIN = .w_TTIMPNIN-.w_deimpnin
        .w_TTIMPLOR = .w_TTIMPLOR-.w_deimplor
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgscg_melPag1 as StdContainer
  Width  = 859
  height = 423
  stdWidth  = 859
  stdheight = 423
  resizeYpos=258
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=1, top=136, width=856,height=52,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="DEIMPIMP",Label1="Operazioni imponibili",Field2="DEIMPAFF",Label2="Imposta afferente",Field3="DEIMPNOI",Label3="Non imponibili",Field4="DEIMPESE",Label4="Operazioni esenti",Field5="DEIMPNIN",Label5="Impon. IVA non esposta",Field6="DEIMPLOR",Label6="Compr. impos. afferente",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 15924102

  add object oDE__ANNO_1_3 as StdField with uid="EOBASEXJQB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DE__ANNO", cQueryName = "DE__ANNO",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno riferimento",;
    HelpContextID = 246092933,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=144, Top=11, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oDE__ANNO_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Val(.w_DE__ANNO)>2005 and Val(.w_DE__ANNO)<2100)
    endwith
    return bRes
  endfunc


  add object oDETIPSOG_1_5 as StdCombo with uid="NATXNJYNXE",rtseq=3,rtrep=.f.,left=418,top=11,width=132,height=21;
    , ToolTipText = "Tipo soggetto (cliente / fornitore)";
    , HelpContextID = 75785341;
    , cFormVar="w_DETIPSOG",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDETIPSOG_1_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DETIPSOG,&i_cF..t_DETIPSOG),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'F',;
    space(1))))
  endfunc
  func oDETIPSOG_1_5.GetRadio()
    this.Parent.oContained.w_DETIPSOG = this.RadioValue()
    return .t.
  endfunc

  func oDETIPSOG_1_5.ToRadio()
    this.Parent.oContained.w_DETIPSOG=trim(this.Parent.oContained.w_DETIPSOG)
    return(;
      iif(this.Parent.oContained.w_DETIPSOG=='C',1,;
      iif(this.Parent.oContained.w_DETIPSOG=='F',2,;
      0)))
  endfunc

  func oDETIPSOG_1_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDEPARIVA_1_7 as StdField with uid="QWMAPNDTIE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DEPARIVA", cQueryName = "DEPARIVA",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 90430345,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=144, Top=37, cSayPict='REPL("!",12)', cGetPict='REPL("!",11)', InputMask=replicate('X',12)

  func oDEPARIVA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_DEPARIVA, "PI", .w_DETIPCON, .w_DECODCON, SPACE(3)))
    endwith
    return bRes
  endfunc

  add object oDECODFIS_1_9 as StdField with uid="LACTEVFCYP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DECODFIS", cQueryName = "DECODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 113857673,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=418, Top=37, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16)

  func oDECODFIS_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_DECODFIS, 'CF', .w_DETIPCON, .w_DECODCON))
    endwith
    return bRes
  endfunc

  add object oDECODCON_1_11 as StdField with uid="ZRBHLQSDAV",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DECODCON", cQueryName = "DECODCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Cliente/fornitore",;
    HelpContextID = 63526020,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=418, Top=64, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DETIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DECODCON"

  func oDECODCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDEDESCON_1_12 as StdField with uid="EMEROTUJFT",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DEDESCON", cQueryName = "DEDESCON",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 78603396,;
   bGlobalFont=.t.,;
    Height=21, Width=432, Left=418, Top=91, InputMask=replicate('X',50)

  add object oDECONPLU_1_14 as StdCheck with uid="KKQHKBNIKY",rtseq=16,rtrep=.f.,left=145, top=112, caption="Codifica non univoca", enabled=.f.,;
    ToolTipText = "Flag codifica non univoca",;
    HelpContextID = 23680139,;
    cFormVar="w_DECONPLU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDECONPLU_1_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DECONPLU,&i_cF..t_DECONPLU),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oDECONPLU_1_14.GetRadio()
    this.Parent.oContained.w_DECONPLU = this.RadioValue()
    return .t.
  endfunc

  func oDECONPLU_1_14.ToRadio()
    this.Parent.oContained.w_DECONPLU=trim(this.Parent.oContained.w_DECONPLU)
    return(;
      iif(this.Parent.oContained.w_DECONPLU=='S',1,;
      0))
  endfunc

  func oDECONPLU_1_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDEESCGEN_1_15 as StdCheck with uid="OICEACOGWI",rtseq=17,rtrep=.f.,left=419, top=114, caption="Escludi da generazione",;
    ToolTipText = "Flag escludi da generazione",;
    HelpContextID = 138578812,;
    cFormVar="w_DEESCGEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDEESCGEN_1_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DEESCGEN,&i_cF..t_DEESCGEN),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oDEESCGEN_1_15.GetRadio()
    this.Parent.oContained.w_DEESCGEN = this.RadioValue()
    return .t.
  endfunc

  func oDEESCGEN_1_15.ToRadio()
    this.Parent.oContained.w_DEESCGEN=trim(this.Parent.oContained.w_DEESCGEN)
    return(;
      iif(this.Parent.oContained.w_DEESCGEN=='S',1,;
      0))
  endfunc

  func oDEESCGEN_1_15.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oDETIPCON_1_16 as StdCombo with uid="NFSQNKXKGF",rtseq=18,rtrep=.f.,left=144,top=64,width=102,height=21, enabled=.f.;
    , ToolTipText = "Tipo conto (cliente / fornitore)";
    , HelpContextID = 75785348;
    , cFormVar="w_DETIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDETIPCON_1_16.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DETIPCON,&i_cF..t_DETIPCON),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'F',;
    space(1))))
  endfunc
  func oDETIPCON_1_16.GetRadio()
    this.Parent.oContained.w_DETIPCON = this.RadioValue()
    return .t.
  endfunc

  func oDETIPCON_1_16.ToRadio()
    this.Parent.oContained.w_DETIPCON=trim(this.Parent.oContained.w_DETIPCON)
    return(;
      iif(this.Parent.oContained.w_DETIPCON=='C',1,;
      iif(this.Parent.oContained.w_DETIPCON=='F',2,;
      0)))
  endfunc

  func oDETIPCON_1_16.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDETIPCON_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DECODCON)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDESCLF_1_17 as StdField with uid="ZYVLYCLGUL",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 121525302,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=557, Top=64, InputMask=replicate('X',40)

  add object oStr_1_4 as StdString with uid="MCSMSCPQDR",Visible=.t., Left=13, Top=11,;
    Alignment=1, Width=129, Height=18,;
    Caption="Anno riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="HCCHFZGUGX",Visible=.t., Left=320, Top=11,;
    Alignment=1, Width=97, Height=18,;
    Caption="Tipo soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="SHTZUVQHGU",Visible=.t., Left=13, Top=37,;
    Alignment=1, Width=129, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="EXDPEBZJES",Visible=.t., Left=320, Top=37,;
    Alignment=1, Width=97, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="CJRCENSCGA",Visible=.t., Left=320, Top=64,;
    Alignment=1, Width=97, Height=18,;
    Caption="Codice conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="RZPRQKZMCU",Visible=.t., Left=320, Top=91,;
    Alignment=1, Width=97, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="GUSHSKXZIW",Visible=.t., Left=13, Top=64,;
    Alignment=1, Width=129, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-9,top=187,;
    width=852+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-8,top=188,width=851+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_9 as StdButton with uid="XEDXHWOFBI",width=48,height=45,;
   left=5, top=380,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza la registrazione di Prima Nota";
    , HelpContextID = 143258122;
    , Caption='Re\<g.Cont';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      with this.Parent.oContained
        GSCG_BZX(this.Parent.oContained,.w_DERIFPNT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_9.mCond()
    with this.Parent.oContained
      return (g_COGE='S' AND NOT EMPTY(.w_DERIFPNT))
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTTIMPIMP_3_1 as StdField with uid="UEWTIKTUXG",rtseq=22,rtrep=.f.,;
    cFormVar="w_TTIMPIMP",value=0,enabled=.f.,;
    ToolTipText = "Importo operazioni imponibili",;
    HelpContextID = 176669830,;
    cQueryName = "TTIMPIMP",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=1, Top=162, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oTTIMPAFF_3_2 as StdField with uid="BGCQWLCBVD",rtseq=23,rtrep=.f.,;
    cFormVar="w_TTIMPAFF",value=0,enabled=.f.,;
    ToolTipText = "Importo imposta afferente",;
    HelpContextID = 225983364,;
    cQueryName = "TTIMPAFF",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=141, Top=162, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oTTIMPNOI_3_3 as StdField with uid="DOBZBYYEMJ",rtseq=24,rtrep=.f.,;
    cFormVar="w_TTIMPNOI",value=0,enabled=.f.,;
    ToolTipText = "Importo operazioni non imponibili",;
    HelpContextID = 260555903,;
    cQueryName = "TTIMPNOI",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=281, Top=162, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oTTIMPESE_3_4 as StdField with uid="BNLDIDPIVB",rtseq=25,rtrep=.f.,;
    cFormVar="w_TTIMPESE",value=0,enabled=.f.,;
    ToolTipText = "Importo operazioni esenti",;
    HelpContextID = 158874501,;
    cQueryName = "TTIMPESE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=422, Top=162, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oTTIMPNIN_3_5 as StdField with uid="UCLRHAGIKG",rtseq=26,rtrep=.f.,;
    cFormVar="w_TTIMPNIN",value=0,enabled=.f.,;
    ToolTipText = "Importo operazioni imponibile IVA non esposta",;
    HelpContextID = 260555908,;
    cQueryName = "TTIMPNIN",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=563, Top=162, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oTTIMPLOR_3_6 as StdField with uid="LOFIMMPFBA",rtseq=27,rtrep=.f.,;
    cFormVar="w_TTIMPLOR",value=0,enabled=.f.,;
    ToolTipText = "Imposta operazioni imponibili comprensive imposta afferente",;
    HelpContextID = 227001480,;
    cQueryName = "TTIMPLOR",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=703, Top=162, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]
enddefine

* --- Defining Body row
define class tgscg_melBodyRow as CPBodyRowCnt
  Width=842
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDEIMPIMP_2_1 as StdTrsField with uid="CGNTGLCHIT",rtseq=6,rtrep=.t.,;
    cFormVar="w_DEIMPIMP",value=0,;
    ToolTipText = "Importo operazioni imponibili",;
    HelpContextID = 176665734,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=137, Left=-2, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDEIMPAFF_2_3 as StdTrsField with uid="HBKNTIJHEF",rtseq=8,rtrep=.t.,;
    cFormVar="w_DEIMPAFF",value=0,;
    ToolTipText = "Importo imposta afferente",;
    HelpContextID = 225987460,;
    cTotal = "this.Parent.oContained.w_ttimpaff", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=137, Left=138, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDEIMPAFF_2_3.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT))
    endwith
  endfunc

  add object oDEIMPNOI_2_4 as StdTrsField with uid="ARZHHDJUJH",rtseq=9,rtrep=.t.,;
    cFormVar="w_DEIMPNOI",value=0,;
    ToolTipText = "Importo operazioni non imponibili",;
    HelpContextID = 260551807,;
    cTotal = "this.Parent.oContained.w_ttimpnoi", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=137, Left=278, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDEIMPNOI_2_4.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT))
    endwith
  endfunc

  add object oDEIMPESE_2_5 as StdTrsField with uid="AXPNZVOHWO",rtseq=10,rtrep=.t.,;
    cFormVar="w_DEIMPESE",value=0,;
    ToolTipText = "Importo operazioni esenti",;
    HelpContextID = 158878597,;
    cTotal = "this.Parent.oContained.w_ttimpese", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=137, Left=419, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDEIMPESE_2_5.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT))
    endwith
  endfunc

  add object oDEIMPNIN_2_6 as StdTrsField with uid="TCZAJCFAQD",rtseq=11,rtrep=.t.,;
    cFormVar="w_DEIMPNIN",value=0,;
    ToolTipText = "Importo operazioni imponibile IVA non esposta",;
    HelpContextID = 260551812,;
    cTotal = "this.Parent.oContained.w_ttimpnin", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=137, Left=560, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDEIMPNIN_2_6.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT))
    endwith
  endfunc

  add object oDEIMPLOR_2_7 as StdTrsField with uid="BGDIWMFODR",rtseq=12,rtrep=.t.,;
    cFormVar="w_DEIMPLOR",value=0,;
    ToolTipText = "Imposta operazioni imponibili comprensive imposta afferente",;
    HelpContextID = 226997384,;
    cTotal = "this.Parent.oContained.w_ttimplor", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=137, Left=700, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDEIMPLOR_2_7.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT) and .w_DETIPSOG='F')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDEIMPIMP_2_1.When()
    return(.t.)
  proc oDEIMPIMP_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDEIMPIMP_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mel','DAELCLFO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DESERIAL=DAELCLFO.DESERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_mel
proc CheckDEIMPIMP(obj)
if not empty(obj.w_DERIFPNT)
  cp_ErrorMsg("Impossibile variare importo per righe generate")
  obj.w_TTIMPIMP=obj.w_TTIMPIMP-obj.w_DEIMPIMP+obj.o_DEIMPIMP
  obj.w_DEIMPIMP=obj.o_DEIMPIMP
  obj.w_XIMIMP=obj.w_DEIMPIMP
endif
endproc
* --- Fine Area Manuale
