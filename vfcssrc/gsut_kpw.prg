* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kpw                                                        *
*              Configurazione servizi WE                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-10                                                      *
* Last revis.: 2008-09-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kpw",oParentObject))

* --- Class definition
define class tgsut_kpw as StdForm
  Top    = 21
  Left   = 12

  * --- Standard Properties
  Width  = 566
  Height = 379
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-02"
  HelpContextID=149504873
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kpw"
  cComment = "Configurazione servizi WE"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AZ_INDWE = space(75)
  w_AZ_DOMWE = space(35)
  w_AZ_LOGWE = space(35)
  w_AZ_PASWE = space(35)
  w_AZ_EMLDE = space(75)
  w_AZNOMDSC = space(35)
  w_AZ_EMLSM = space(75)
  w_AZCDESWE = space(1)
  w_AZWEENAB = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kpwPag1","gsut_kpw",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAZ_INDWE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZ_INDWE=space(75)
      .w_AZ_DOMWE=space(35)
      .w_AZ_LOGWE=space(35)
      .w_AZ_PASWE=space(35)
      .w_AZ_EMLDE=space(75)
      .w_AZNOMDSC=space(35)
      .w_AZ_EMLSM=space(75)
      .w_AZCDESWE=space(1)
      .w_AZWEENAB=space(1)
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
    this.DoRTCalc(1,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAZ_INDWE_1_1.enabled = this.oPgFrm.Page1.oPag.oAZ_INDWE_1_1.mCond()
    this.oPgFrm.Page1.oPag.oAZ_DOMWE_1_4.enabled = this.oPgFrm.Page1.oPag.oAZ_DOMWE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oAZ_LOGWE_1_6.enabled = this.oPgFrm.Page1.oPag.oAZ_LOGWE_1_6.mCond()
    this.oPgFrm.Page1.oPag.oAZ_PASWE_1_8.enabled = this.oPgFrm.Page1.oPag.oAZ_PASWE_1_8.mCond()
    this.oPgFrm.Page1.oPag.oAZ_EMLDE_1_9.enabled = this.oPgFrm.Page1.oPag.oAZ_EMLDE_1_9.mCond()
    this.oPgFrm.Page1.oPag.oAZNOMDSC_1_10.enabled = this.oPgFrm.Page1.oPag.oAZNOMDSC_1_10.mCond()
    this.oPgFrm.Page1.oPag.oAZ_EMLSM_1_12.enabled = this.oPgFrm.Page1.oPag.oAZ_EMLSM_1_12.mCond()
    this.oPgFrm.Page1.oPag.oAZCDESWE_1_22.enabled = this.oPgFrm.Page1.oPag.oAZCDESWE_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAZ_INDWE_1_1.value==this.w_AZ_INDWE)
      this.oPgFrm.Page1.oPag.oAZ_INDWE_1_1.value=this.w_AZ_INDWE
    endif
    if not(this.oPgFrm.Page1.oPag.oAZ_DOMWE_1_4.value==this.w_AZ_DOMWE)
      this.oPgFrm.Page1.oPag.oAZ_DOMWE_1_4.value=this.w_AZ_DOMWE
    endif
    if not(this.oPgFrm.Page1.oPag.oAZ_LOGWE_1_6.value==this.w_AZ_LOGWE)
      this.oPgFrm.Page1.oPag.oAZ_LOGWE_1_6.value=this.w_AZ_LOGWE
    endif
    if not(this.oPgFrm.Page1.oPag.oAZ_PASWE_1_8.value==this.w_AZ_PASWE)
      this.oPgFrm.Page1.oPag.oAZ_PASWE_1_8.value=this.w_AZ_PASWE
    endif
    if not(this.oPgFrm.Page1.oPag.oAZ_EMLDE_1_9.value==this.w_AZ_EMLDE)
      this.oPgFrm.Page1.oPag.oAZ_EMLDE_1_9.value=this.w_AZ_EMLDE
    endif
    if not(this.oPgFrm.Page1.oPag.oAZNOMDSC_1_10.value==this.w_AZNOMDSC)
      this.oPgFrm.Page1.oPag.oAZNOMDSC_1_10.value=this.w_AZNOMDSC
    endif
    if not(this.oPgFrm.Page1.oPag.oAZ_EMLSM_1_12.value==this.w_AZ_EMLSM)
      this.oPgFrm.Page1.oPag.oAZ_EMLSM_1_12.value=this.w_AZ_EMLSM
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCDESWE_1_22.RadioValue()==this.w_AZCDESWE)
      this.oPgFrm.Page1.oPag.oAZCDESWE_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZWEENAB_1_23.RadioValue()==this.w_AZWEENAB)
      this.oPgFrm.Page1.oPag.oAZWEENAB_1_23.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_AZ_DOMWE))  and (.w_AZWEENAB='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZ_DOMWE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_AZ_DOMWE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AZ_EMLDE))  and (.w_AZWEENAB='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZ_EMLDE_1_9.SetFocus()
            i_bnoObbl = !empty(.w_AZ_EMLDE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AZNOMDSC))  and (not EMPTY(.w_AZ_EMLDE) and .w_AZWEENAB='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZNOMDSC_1_10.SetFocus()
            i_bnoObbl = !empty(.w_AZNOMDSC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AZ_EMLSM))  and (.w_AZWEENAB='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZ_EMLSM_1_12.SetFocus()
            i_bnoObbl = !empty(.w_AZ_EMLSM)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kpwPag1 as StdContainer
  Width  = 562
  height = 379
  stdWidth  = 562
  stdheight = 379
  resizeXpos=394
  resizeYpos=162
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZ_INDWE_1_1 as StdField with uid="QLYHWXZKHV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_AZ_INDWE", cQueryName = "AZ_INDWE",;
    bObbl = .f. , nPag = 1, value=space(75), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo Internet del gestore dei servizi WWP",;
    HelpContextID = 263845045,;
   bGlobalFont=.t.,;
    Height=21, Width=481, Left=16, Top=54, InputMask=replicate('X',75)

  func oAZ_INDWE_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZWEENAB='S')
    endwith
   endif
  endfunc

  add object oAZ_DOMWE_1_4 as StdField with uid="ZHUTPNMAIL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AZ_DOMWE", cQueryName = "AZ_DOMWE",;
    bObbl = .t. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Nome del dominio",;
    HelpContextID = 112129205,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=232, Top=82, InputMask=replicate('X',35)

  func oAZ_DOMWE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZWEENAB='S')
    endwith
   endif
  endfunc

  add object oAZ_LOGWE_1_6 as StdField with uid="ZLFLCUALZA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_AZ_LOGWE", cQueryName = "AZ_LOGWE",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Login di amministrazione",;
    HelpContextID = 212268213,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=232, Top=107, InputMask=replicate('X',35)

  func oAZ_LOGWE_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZWEENAB='S')
    endwith
   endif
  endfunc

  add object oAZ_PASWE_1_8 as StdField with uid="LLZJXSMSKH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AZ_PASWE", cQueryName = "AZ_PASWE",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Password di amministrazione",;
    HelpContextID = 25359541,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=232, Top=133, InputMask=replicate('X',35)

  func oAZ_PASWE_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZWEENAB='S')
    endwith
   endif
  endfunc

  add object oAZ_EMLDE_1_9 as StdField with uid="OATPUMGADE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_AZ_EMLDE", cQueryName = "AZ_EMLDE",;
    bObbl = .t. , nPag = 1, value=space(75), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo E-mail per gestione servizi WWP tramite descrittore",;
    HelpContextID = 137497419,;
   bGlobalFont=.t.,;
    Height=21, Width=538, Left=16, Top=232, InputMask=replicate('X',75)

  func oAZ_EMLDE_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZWEENAB='S')
    endwith
   endif
  endfunc

  add object oAZNOMDSC_1_10 as StdField with uid="PIHJBHJWCJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AZNOMDSC", cQueryName = "AZNOMDSC",;
    bObbl = .t. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Nome file descrittore",;
    HelpContextID = 3865417,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=232, Top=256, InputMask=replicate('X',35)

  func oAZNOMDSC_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not EMPTY(.w_AZ_EMLDE) and .w_AZWEENAB='S')
    endwith
   endif
  endfunc

  add object oAZ_EMLSM_1_12 as StdField with uid="SBLCKZSRFW",rtseq=7,rtrep=.f.,;
    cFormVar = "w_AZ_EMLSM", cQueryName = "AZ_EMLSM",;
    bObbl = .t. , nPag = 1, value=space(75), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo E-mail per gestione servizi WWP tramite Simple-Mail",;
    HelpContextID = 137497427,;
   bGlobalFont=.t.,;
    Height=21, Width=538, Left=16, Top=305, InputMask=replicate('X',75)

  func oAZ_EMLSM_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZWEENAB='S')
    endwith
   endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="AQKGCSTQHP",left=445, top=331, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 209876502;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="APTRHGBNFI",left=504, top=331, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 209876502;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="OKIAZKUOOC",left=504, top=50, width=48,height=45,;
    CpPicture="BMP\WE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al sito dei servizi";
    , HelpContextID = 209876502;
    , TABSTOP=.F., caption='\<Ind. WEB';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSUT_BPW(this.Parent.oContained,"EXECUTE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_AZWEENAB='S')
      endwith
    endif
  endfunc


  add object oObj_1_19 as cp_runprogram with uid="YTNBKVLIBV",left=2, top=419, width=173,height=19,;
    caption='GSUT_BPW(LOAD)',;
   bGlobalFont=.t.,;
    prg="GSUT_BPW('LOAD')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 216441661


  add object oObj_1_20 as cp_runprogram with uid="MXWUWLOTPD",left=2, top=397, width=222,height=19,;
    caption='GSUT_BPW(SAVE)',;
   bGlobalFont=.t.,;
    prg="GSUT_BPW('SAVE')",;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 218810941

  add object oAZCDESWE_1_22 as StdCheck with uid="CEFGCSRIHJ",rtseq=8,rtrep=.f.,left=14, top=187, caption="Richiede conferma destinatario",;
    ToolTipText = "Se attivo: richiede conferma selezione destinatario (anche quando � noto)",;
    HelpContextID = 22066357,;
    cFormVar="w_AZCDESWE", bObbl = .f. , nPag = 1;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oAZCDESWE_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZCDESWE_1_22.GetRadio()
    this.Parent.oContained.w_AZCDESWE = this.RadioValue()
    return .t.
  endfunc

  func oAZCDESWE_1_22.SetRadio()
    this.Parent.oContained.w_AZCDESWE=trim(this.Parent.oContained.w_AZCDESWE)
    this.value = ;
      iif(this.Parent.oContained.w_AZCDESWE=='S',1,;
      0)
  endfunc

  func oAZCDESWE_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZWEENAB='S')
    endwith
   endif
  endfunc

  add object oAZWEENAB_1_23 as StdCheck with uid="KKFNDYJTCC",rtseq=9,rtrep=.f.,left=212, top=6, caption="Abilita servizi WE",;
    ToolTipText = "Se attivo: abilita servizi WE",;
    HelpContextID = 162630472,;
    cFormVar="w_AZWEENAB", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oAZWEENAB_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZWEENAB_1_23.GetRadio()
    this.Parent.oContained.w_AZWEENAB = this.RadioValue()
    return .t.
  endfunc

  func oAZWEENAB_1_23.SetRadio()
    this.Parent.oContained.w_AZWEENAB=trim(this.Parent.oContained.w_AZWEENAB)
    this.value = ;
      iif(this.Parent.oContained.w_AZWEENAB=='S',1,;
      0)
  endfunc

  add object oStr_1_2 as StdString with uid="EVKZSKGXUV",Visible=.t., Left=16, Top=33,;
    Alignment=0, Width=385, Height=18,;
    Caption="Indirizzo Internet del gestore dei servizi WWP"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="YCSSJMFANO",Visible=.t., Left=95, Top=84,;
    Alignment=1, Width=133, Height=18,;
    Caption="Nome del dominio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="SJXKIICFQF",Visible=.t., Left=60, Top=109,;
    Alignment=1, Width=168, Height=18,;
    Caption="Login di amministrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="HTCOKEXHKR",Visible=.t., Left=30, Top=135,;
    Alignment=1, Width=198, Height=18,;
    Caption="Password di amministrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="SFBJVTRUXE",Visible=.t., Left=16, Top=211,;
    Alignment=0, Width=385, Height=18,;
    Caption="Indirizzo E-mail per gestione servizi tramite descrittore"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="XCAKZYRTTV",Visible=.t., Left=16, Top=284,;
    Alignment=0, Width=385, Height=18,;
    Caption="Indirizzo E-mail per gestione servizi tramite simple mail"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="TLFJMMJVBC",Visible=.t., Left=14, Top=167,;
    Alignment=0, Width=357, Height=18,;
    Caption="Servizi netfolder"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="UAWMPHHMGG",Visible=.t., Left=50, Top=259,;
    Alignment=1, Width=178, Height=18,;
    Caption="Nome file descrittore:"  ;
  , bGlobalFont=.t.

  add object oBox_1_14 as StdBox with uid="WSRVMABDMB",left=12, top=183, width=546,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kpw','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
