* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_afr                                                        *
*              Fornitori                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_430]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2018-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_afr"))

* --- Class definition
define class tgsar_afr as StdForm
  Top    = 0
  Left   = 26

  * --- Standard Properties
  Width  = 739
  Height = 560+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-27"
  HelpContextID=59340649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=238

  * --- Constant Properties
  CONTI_IDX = 0
  BAN_CHE_IDX = 0
  CACOCLFO_IDX = 0
  CAT_SCMA_IDX = 0
  CATECOMM_IDX = 0
  COC_MAST_IDX = 0
  DES_DIVE_IDX = 0
  LINGUE_IDX = 0
  LISTINI_IDX = 0
  MASTRI_IDX = 0
  NAZIONI_IDX = 0
  PAG_AMEN_IDX = 0
  TRI_BUTI_IDX = 0
  VALUTE_IDX = 0
  VOCIIVA_IDX = 0
  ZONE_IDX = 0
  STU_PIAC_IDX = 0
  STUMPIAC_IDX = 0
  MAGAZZIN_IDX = 0
  METCALSP_IDX = 0
  COD_CATA_IDX = 0
  TIPCODIV_IDX = 0
  COD_AREO_IDX = 0
  CON_CONS_IDX = 0
  GRP_DEFA_IDX = 0
  VASTRUTT_IDX = 0
  GRU_INTE_IDX = 0
  PAR_OFFE_IDX = 0
  REG_PROV_IDX = 0
  NUMAUT_M_IDX = 0
  DORATING_IDX = 0
  VOC_FINZ_IDX = 0
  ENTI_COM_IDX = 0
  cFile = "CONTI"
  cKeySelect = "ANTIPCON,ANCODICE"
  cQueryFilter="ANTIPCON='F'"
  cKeyWhere  = "ANTIPCON=this.w_ANTIPCON and ANCODICE=this.w_ANCODICE"
  cKeyWhereODBC = '"ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)';
      +'+" and ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';

  cKeyWhereODBCqualified = '"CONTI.ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)';
      +'+" and CONTI.ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';

  cPrg = "gsar_afr"
  cComment = "Fornitori"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AFR'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AUTOAZI = space(5)
  w_AUTO = space(1)
  o_AUTO = space(1)
  w_ANTIPCON = space(1)
  o_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  o_ANCODICE = space(15)
  w_ANDESCRI = space(60)
  w_ANTIPCLF = space(1)
  w_ANDESCR2 = space(60)
  w_VALATT = space(20)
  w_TABKEY = space(8)
  w_DELIMM = .F.
  w_ANINDIRI = space(35)
  w_ANINDIR2 = space(35)
  w_AN___CAP = space(8)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_ANNAZION = space(3)
  w_DESNAZ = space(35)
  w_ANCODFIS = space(16)
  o_ANCODFIS = space(16)
  w_ANPARIVA = space(12)
  w_ANTELEFO = space(18)
  w_ANTELFAX = space(18)
  w_ANNUMCEL = space(18)
  w_ANINDWEB = space(254)
  w_AN_EMAIL = space(254)
  w_AN_EMPEC = space(254)
  w_ANPERFIS = space(1)
  o_ANPERFIS = space(1)
  w_AN_SESSO = space(1)
  w_ANCOGNOM = space(30)
  w_AN__NOME = space(30)
  w_ANLOCNAS = space(30)
  w_ANPRONAS = space(2)
  w_ANDATNAS = ctod('  /  /  ')
  w_ANNUMCAR = space(18)
  w_CODISO = space(3)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_ANCHKSTA = space(1)
  w_ANCHKMAI = space(1)
  o_ANCHKMAI = space(1)
  w_ANCHKPEC = space(1)
  w_ANCHKFAX = space(1)
  w_ANCHKCPZ = space(1)
  o_ANCHKCPZ = space(1)
  w_ANCHKWWP = space(1)
  w_ANCHKPTL = space(1)
  w_ANCHKFIR = space(1)
  w_ANCODEST = space(7)
  w_ANCODCLA = space(5)
  w_ANCODPEC = space(0)
  w_ANDTOBSO = ctod('  /  /  ')
  w_ANDTINVA = ctod('  /  /  ')
  w_ANTIPSOT = space(1)
  w_ERR = space(10)
  w_CODCOM = space(4)
  w_TIPRIF = space(1)
  w_CODI = space(15)
  w_DES1 = space(40)
  w_CODAZI1 = space(5)
  w_ANCONSUP = space(15)
  w_DESSUP = space(40)
  w_CODSTU = space(3)
  o_CODSTU = space(3)
  w_ANCONRIF = space(15)
  w_DESORI = space(40)
  w_ANCATCON = space(5)
  w_DESCON = space(35)
  w_ANCODIVA = space(5)
  w_DESIVA = space(35)
  w_ANTIPOPE = space(10)
  w_TIDESCRI = space(30)
  w_ANCONCAU = space(15)
  w_ANOPETRE = space(1)
  w_ANTIPPRE = space(1)
  w_PERIVA = 0
  w_DATOBSO = ctod('  /  /  ')
  w_FLIRPE = space(1)
  w_MINCON = space(6)
  w_ANPARTSN = space(1)
  w_ANFLAACC = space(1)
  w_MAXCON = space(6)
  w_AFFLINTR = space(1)
  o_AFFLINTR = space(1)
  w_ANFLBLLS = space(1)
  o_ANFLBLLS = space(1)
  w_ANFLSOAL = space(1)
  o_ANFLSOAL = space(1)
  w_ANFLBODO = space(1)
  w_ANIVASOS = space(1)
  w_ANCODSTU = space(10)
  o_ANCODSTU = space(10)
  w_ANCODSOG = space(8)
  w_ANCODCAT = space(4)
  w_NULIV = 0
  w_TIPMAS = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_ANTIPRIF = space(1)
  w_FLACON = space(1)
  w_FLACO2 = space(1)
  w_PROSTU = space(8)
  o_PROSTU = space(8)
  w_ANFLSGRE = space(1)
  o_ANFLSGRE = space(1)
  w_ANCATOPE = space(2)
  w_TIDTOBSO = ctod('  /  /  ')
  w_MASTRO = space(15)
  w_CATEGORIA = space(5)
  w_DATOBSORIF = ctod('  /  /  ')
  w_DATOBSOIVA = ctod('  /  /  ')
  w_FLSCM = space(1)
  w_ANCODISO = space(3)
  w_IVALIS = space(1)
  w_VALLIS = space(3)
  w_ANCODCUC = space(2)
  w_DTOBSO = ctod('  /  /  ')
  w_MAGWIP = space(1)
  w_DATOBSOZON = ctod('  /  /  ')
  w_TIPCC = space(1)
  w_ANNUMCOR = space(25)
  o_ANNUMCOR = space(25)
  w_ANCINABI = space(1)
  o_ANCINABI = space(1)
  w_AN__BBAN = space(30)
  o_AN__BBAN = space(30)
  w_AN__IBAN = space(35)
  w_TIPSOT = space(1)
  w_CONSBF = space(1)
  w_CODPAG = space(5)
  w_DATOBSOPAG = ctod('  /  /  ')
  w_CONTA = 0
  o_CONTA = 0
  w_RIFDIC = space(10)
  w_ANNDIC = space(4)
  w_DATDIC = ctod('  /  /  ')
  w_NUMDIC = 0
  w_ANDESCR3 = space(60)
  w_ANDESCR4 = space(60)
  w_ANINDIR3 = space(35)
  w_ANINDIR4 = space(35)
  w_ANLOCAL2 = space(30)
  w_DES1 = space(40)
  w_CODI = space(15)
  w_ANCODGRU = space(10)
  w_DESGRU = space(35)
  w_CODI = space(15)
  w_DES1 = space(40)
  w_CODI = space(15)
  w_DES1 = space(40)
  w_CODI = space(15)
  w_DES1 = space(40)
  w_ANCODPAG = space(5)
  w_DESPAG = space(30)
  w_ANGIOFIS = 0
  w_AN1MESCL = 0
  o_AN1MESCL = 0
  w_DESMES1 = space(12)
  w_ANGIOSC1 = 0
  w_AN2MESCL = 0
  o_AN2MESCL = 0
  w_DESMES2 = space(12)
  w_ANGIOSC2 = 0
  w_ANCODBAN = space(10)
  w_DESBAN = space(42)
  w_ANCODBA2 = space(15)
  w_ANFLINCA = space(1)
  w_ANSPEINC = 0
  w_ANFLESIM = space(1)
  o_ANFLESIM = space(1)
  w_ANSAGINT = 0
  o_ANSAGINT = 0
  w_ANSPRINT = space(1)
  w_ANFLACBD = space(1)
  w_ANFLRAGG = space(1)
  w_ANFLGAVV = space(1)
  w_ANDATAVV = ctod('  /  /  ')
  w_DESBA2 = space(42)
  w_CODI = space(15)
  w_DES1 = space(40)
  w_ANCATCOM = space(3)
  w_ANGRPDEF = space(5)
  w_DESCAC = space(35)
  w_ANCATSCM = space(5)
  w_DESSCM = space(35)
  w_ANCODLIN = space(3)
  w_DESLIN = space(30)
  w_ANCODVAL = space(3)
  w_DESVAL = space(35)
  w_ANNUMLIS = space(5)
  w_DESLIS = space(40)
  w_AN1SCONT = 0
  o_AN1SCONT = 0
  w_AN2SCONT = 0
  w_ANCODZON = space(3)
  w_ANCONCON = space(1)
  w_DESZON = space(35)
  w_ANMAGTER = space(5)
  w_ANCODESC = space(5)
  w_ANCODPOR = space(10)
  w_DESMAG = space(30)
  w_ANMCALSI = space(5)
  w_ANMCALST = space(5)
  w_MSDESIMB = space(40)
  w_ANSCORPO = space(1)
  w_ANFLCODI = space(1)
  w_ANFLIMBA = space(1)
  w_ANFLGCPZ = space(1)
  w_MSDESTRA = space(40)
  w_GDDESCRI = space(40)
  w_ANFLGCAU = space(1)
  w_DESCRI = space(40)
  w_ANFLAPCA = space(1)
  w_DESCAU = space(40)
  w_ECTERRIT = space(30)
  w_DESCREG = space(30)
  w_DESIRPEF = space(35)
  w_DESTR2 = space(35)
  w_ANRITENU = space(1)
  o_ANRITENU = space(1)
  w_ANCODIRP = space(5)
  o_ANCODIRP = space(5)
  w_ANCODTR2 = space(5)
  w_ANPEINPS = 0
  w_ANRIINPS = 0
  w_ANCOINPS = 0
  w_ANCASPRO = 0
  w_ANCODATT = 0
  w_ANCODASS = space(3)
  w_ANCOIMPS = space(20)
  w_CODI = space(15)
  w_DES1 = space(40)
  w_AN__NOTE = space(0)
  w_CODI = space(15)
  w_DES1 = space(40)
  w_ANRATING = space(2)
  w_ANGIORIT = 0
  w_ANVOCFIN = space(6)
  w_DFDESCRI = space(60)
  w_ANESCDOF = space(1)
  w_ANDESPAR = space(1)
  w_RADESCRI = space(50)
  w_CODI = space(15)
  w_ANFLGEST = space(1)
  o_ANFLGEST = space(1)
  w_ANDESCRI = space(40)
  w_ANFLSGRE = space(1)
  w_ANDESCR2 = space(40)
  w_ANCOFISC = space(25)
  w_ANSCHUMA = space(1)
  w_AN_EREDE = space(1)
  w_ANCODSNS = space(1)
  w_ANCODREG = space(2)
  w_ANCODCOM = space(4)
  w_DANOM = .F.
  w_ANCAURIT = space(2)
  w_CAUPRE2 = space(2)
  w_ANEVEECC = space(1)
  w_ANCATPAR = space(2)
  w_ANFLESIG = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_ANCODICE = this.W_ANCODICE

  * --- Children pointers
  GSAR_MSA = .NULL.
  GSAR_MSE = .NULL.
  GSAR_MDD = .NULL.
  GSAR_MCO = .NULL.
  GSAR_MCC = .NULL.
  GSAR_MIN = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_afr
  w_PaginaNote = 0
  w_BckpForClr = 0
  p_aut=0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=11, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CONTI','gsar_afr')
    stdPageFrame::Init()
    *set procedure to GSAR_MSA additive
    with this
      .Pages(1).addobject("oPag","tgsar_afrPag1","gsar_afr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Anagrafici")
      .Pages(1).HelpContextID = 84179615
      .Pages(2).addobject("oPag","tgsar_afrPag2","gsar_afr",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Contabili")
      .Pages(2).HelpContextID = 184425694
      .Pages(3).addobject("oPag","tgsar_afrPag3","gsar_afr",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dati percipiente")
      .Pages(3).HelpContextID = 61316669
      .Pages(4).addobject("oPag","tgsar_afrPag4","gsar_afr",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Acquisti")
      .Pages(4).HelpContextID = 109248623
      .Pages(5).addobject("oPag","tgsar_afrPag5","gsar_afr",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Pagamenti")
      .Pages(5).HelpContextID = 122788358
      .Pages(6).addobject("oPag","tgsar_afrPag6","gsar_afr",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Riferimenti")
      .Pages(6).HelpContextID = 49716645
      .Pages(7).addobject("oPag","tgsar_afrPag7","gsar_afr",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Sedi")
      .Pages(7).HelpContextID = 52022490
      .Pages(8).addobject("oPag","tgsar_afrPag8","gsar_afr",8)
      .Pages(8).oPag.Visible=.t.
      .Pages(8).Caption=cp_Translate("EDI")
      .Pages(8).HelpContextID = 59023034
      .Pages(9).addobject("oPag","tgsar_afrPag9","gsar_afr",9)
      .Pages(9).oPag.Visible=.t.
      .Pages(9).Caption=cp_Translate("Dati DocFinance")
      .Pages(9).HelpContextID = 221215737
      .Pages(10).addobject("oPag","tgsar_afrPag10","gsar_afr",10)
      .Pages(10).oPag.Visible=.t.
      .Pages(10).Caption=cp_Translate("Note")
      .Pages(10).HelpContextID = 52216618
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANCODICE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAR_MSA
    * --- Area Manuale = Init Page Frame
    * --- gsar_afr
    This.Pages(8).Enabled = (g_VEFA='S')
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[33]
    this.cWorkTables[1]='BAN_CHE'
    this.cWorkTables[2]='CACOCLFO'
    this.cWorkTables[3]='CAT_SCMA'
    this.cWorkTables[4]='CATECOMM'
    this.cWorkTables[5]='COC_MAST'
    this.cWorkTables[6]='DES_DIVE'
    this.cWorkTables[7]='LINGUE'
    this.cWorkTables[8]='LISTINI'
    this.cWorkTables[9]='MASTRI'
    this.cWorkTables[10]='NAZIONI'
    this.cWorkTables[11]='PAG_AMEN'
    this.cWorkTables[12]='TRI_BUTI'
    this.cWorkTables[13]='VALUTE'
    this.cWorkTables[14]='VOCIIVA'
    this.cWorkTables[15]='ZONE'
    this.cWorkTables[16]='STU_PIAC'
    this.cWorkTables[17]='STUMPIAC'
    this.cWorkTables[18]='MAGAZZIN'
    this.cWorkTables[19]='METCALSP'
    this.cWorkTables[20]='COD_CATA'
    this.cWorkTables[21]='TIPCODIV'
    this.cWorkTables[22]='COD_AREO'
    this.cWorkTables[23]='CON_CONS'
    this.cWorkTables[24]='GRP_DEFA'
    this.cWorkTables[25]='VASTRUTT'
    this.cWorkTables[26]='GRU_INTE'
    this.cWorkTables[27]='PAR_OFFE'
    this.cWorkTables[28]='REG_PROV'
    this.cWorkTables[29]='NUMAUT_M'
    this.cWorkTables[30]='DORATING'
    this.cWorkTables[31]='VOC_FINZ'
    this.cWorkTables[32]='ENTI_COM'
    this.cWorkTables[33]='CONTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(33))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTI_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MSA = CREATEOBJECT('stdLazyChild',this,'GSAR_MSA')
    this.GSAR_MSE = CREATEOBJECT('stdDynamicChild',this,'GSAR_MSE',this.oPgFrm.Page8.oPag.oLinkPC_8_6)
    this.GSAR_MSE.createrealchild()
    this.GSAR_MDD = CREATEOBJECT('stdDynamicChild',this,'GSAR_MDD',this.oPgFrm.Page7.oPag.oLinkPC_7_4)
    this.GSAR_MDD.createrealchild()
    this.GSAR_MCO = CREATEOBJECT('stdDynamicChild',this,'GSAR_MCO',this.oPgFrm.Page6.oPag.oLinkPC_6_4)
    this.GSAR_MCO.createrealchild()
    this.GSAR_MCC = CREATEOBJECT('stdDynamicChild',this,'GSAR_MCC',this.oPgFrm.Page5.oPag.oLinkPC_5_20)
    this.GSAR_MCC.createrealchild()
    this.GSAR_MIN = CREATEOBJECT('stdLazyChild',this,'GSAR_MIN')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MSA)
      this.GSAR_MSA.DestroyChildrenChain()
      this.GSAR_MSA=.NULL.
    endif
    if !ISNULL(this.GSAR_MSE)
      this.GSAR_MSE.DestroyChildrenChain()
      this.GSAR_MSE=.NULL.
    endif
    this.oPgFrm.Page8.oPag.RemoveObject('oLinkPC_8_6')
    if !ISNULL(this.GSAR_MDD)
      this.GSAR_MDD.DestroyChildrenChain()
      this.GSAR_MDD=.NULL.
    endif
    this.oPgFrm.Page7.oPag.RemoveObject('oLinkPC_7_4')
    if !ISNULL(this.GSAR_MCO)
      this.GSAR_MCO.DestroyChildrenChain()
      this.GSAR_MCO=.NULL.
    endif
    this.oPgFrm.Page6.oPag.RemoveObject('oLinkPC_6_4')
    if !ISNULL(this.GSAR_MCC)
      this.GSAR_MCC.DestroyChildrenChain()
      this.GSAR_MCC=.NULL.
    endif
    this.oPgFrm.Page5.oPag.RemoveObject('oLinkPC_5_20')
    if !ISNULL(this.GSAR_MIN)
      this.GSAR_MIN.DestroyChildrenChain()
      this.GSAR_MIN=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MSA.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MSE.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MDD.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MCO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MCC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MIN.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MSA.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MSE.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MDD.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MCO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MCC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MIN.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MSA.NewDocument()
    this.GSAR_MSE.NewDocument()
    this.GSAR_MDD.NewDocument()
    this.GSAR_MCO.NewDocument()
    this.GSAR_MCC.NewDocument()
    this.GSAR_MIN.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MSA.SetKey(;
            .w_ANCODICE,"SSCODICE";
            ,.w_ANTIPCON,"SSTIPCON";
            )
      this.GSAR_MSE.SetKey(;
            .w_ANTIPCON,"SITIPCON";
            ,.w_ANCODICE,"SICODCON";
            )
      this.GSAR_MDD.SetKey(;
            .w_ANTIPCON,"DDTIPCON";
            ,.w_ANCODICE,"DDCODICE";
            )
      this.GSAR_MCO.SetKey(;
            .w_ANTIPCON,"COTIPCON";
            ,.w_ANCODICE,"COCODCON";
            )
      this.GSAR_MCC.SetKey(;
            .w_ANTIPCON,"CCTIPCON";
            ,.w_ANCODICE,"CCCODCON";
            )
      this.GSAR_MIN.SetKey(;
            .w_ANTIPCON,"MCTIPINT";
            ,.w_ANCODICE,"MCCODINT";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MSA.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANCODICE,"SSCODICE";
             ,.w_ANTIPCON,"SSTIPCON";
             )
      .GSAR_MSE.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"SITIPCON";
             ,.w_ANCODICE,"SICODCON";
             )
      .GSAR_MDD.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"DDTIPCON";
             ,.w_ANCODICE,"DDCODICE";
             )
      .GSAR_MCO.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"COTIPCON";
             ,.w_ANCODICE,"COCODCON";
             )
      .WriteTo_GSAR_MCO()
      .GSAR_MCC.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"CCTIPCON";
             ,.w_ANCODICE,"CCCODCON";
             )
      .GSAR_MIN.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"MCTIPINT";
             ,.w_ANCODICE,"MCCODINT";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MSE)
        i_f=.GSAR_MSE.BuildFilter()
        if !(i_f==.GSAR_MSE.cQueryFilter)
          i_fnidx=.GSAR_MSE.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MSE.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MSE.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MSE.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MSE.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MDD)
        i_f=.GSAR_MDD.BuildFilter()
        if !(i_f==.GSAR_MDD.cQueryFilter)
          i_fnidx=.GSAR_MDD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MDD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MDD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MDD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MDD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MCO)
        i_f=.GSAR_MCO.BuildFilter()
        if !(i_f==.GSAR_MCO.cQueryFilter)
          i_fnidx=.GSAR_MCO.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MCO.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MCO.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MCO.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MCO.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MCC)
        i_f=.GSAR_MCC.BuildFilter()
        if !(i_f==.GSAR_MCC.cQueryFilter)
          i_fnidx=.GSAR_MCC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MCC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MCC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MCC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MCC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSAR_MCO()
  if at('gsar_mco',lower(this.GSAR_MCO.class))<>0
    if this.GSAR_MCO.w_VRCODCON<>this.w_ANCODICE or this.GSAR_MCO.w_VRTIPCON<>this.w_ANTIPCON
      this.GSAR_MCO.w_VRCODCON = this.w_ANCODICE
      this.GSAR_MCO.w_VRTIPCON = this.w_ANTIPCON
      this.GSAR_MCO.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ANTIPCON = NVL(ANTIPCON,space(1))
      .w_ANCODICE = NVL(ANCODICE,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_10_joined
    link_2_10_joined=.f.
    local link_2_12_joined
    link_2_12_joined=.f.
    local link_8_4_joined
    link_8_4_joined=.f.
    local link_5_4_joined
    link_5_4_joined=.f.
    local link_5_15_joined
    link_5_15_joined=.f.
    local link_5_17_joined
    link_5_17_joined=.f.
    local link_4_12_joined
    link_4_12_joined=.f.
    local link_4_13_joined
    link_4_13_joined=.f.
    local link_4_15_joined
    link_4_15_joined=.f.
    local link_4_17_joined
    link_4_17_joined=.f.
    local link_4_19_joined
    link_4_19_joined=.f.
    local link_4_21_joined
    link_4_21_joined=.f.
    local link_4_25_joined
    link_4_25_joined=.f.
    local link_4_38_joined
    link_4_38_joined=.f.
    local link_4_40_joined
    link_4_40_joined=.f.
    local link_4_44_joined
    link_4_44_joined=.f.
    local link_4_45_joined
    link_4_45_joined=.f.
    local link_3_32_joined
    link_3_32_joined=.f.
    local link_3_33_joined
    link_3_33_joined=.f.
    local link_9_7_joined
    link_9_7_joined=.f.
    local link_9_9_joined
    link_9_9_joined=.f.
    local link_3_53_joined
    link_3_53_joined=.f.
    local link_3_54_joined
    link_3_54_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTI where ANTIPCON=KeySet.ANTIPCON
    *                            and ANCODICE=KeySet.ANCODICE
    *
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTI '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_12_joined=this.AddJoinedLink_2_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_8_4_joined=this.AddJoinedLink_8_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_4_joined=this.AddJoinedLink_5_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_15_joined=this.AddJoinedLink_5_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_17_joined=this.AddJoinedLink_5_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_12_joined=this.AddJoinedLink_4_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_13_joined=this.AddJoinedLink_4_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_15_joined=this.AddJoinedLink_4_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_17_joined=this.AddJoinedLink_4_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_19_joined=this.AddJoinedLink_4_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_21_joined=this.AddJoinedLink_4_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_25_joined=this.AddJoinedLink_4_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_38_joined=this.AddJoinedLink_4_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_40_joined=this.AddJoinedLink_4_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_44_joined=this.AddJoinedLink_4_44(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_45_joined=this.AddJoinedLink_4_45(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_32_joined=this.AddJoinedLink_3_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_33_joined=this.AddJoinedLink_3_33(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_9_7_joined=this.AddJoinedLink_9_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_9_9_joined=this.AddJoinedLink_9_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_53_joined=this.AddJoinedLink_3_53(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_54_joined=this.AddJoinedLink_3_54(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ANTIPCON',this.w_ANTIPCON  ,'ANCODICE',this.w_ANCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AUTOAZI = i_CODAZI
        .w_AUTO = ' '
        .w_VALATT = "F"+.w_ANCODICE
        .w_TABKEY = 'CONTI'
        .w_DELIMM = .f.
        .w_DESNAZ = space(35)
        .w_CODISO = space(3)
        .w_ERR = space(10)
        .w_CODCOM = space(4)
        .w_CODAZI1 = i_codazi
        .w_DESSUP = space(40)
        .w_CODSTU = space(3)
        .w_DESORI = space(40)
        .w_DESCON = space(35)
        .w_DESIVA = space(35)
        .w_TIDESCRI = space(30)
        .w_PERIVA = 0
        .w_DATOBSO = ctod("  /  /  ")
        .w_FLIRPE = space(1)
        .w_MINCON = space(6)
        .w_MAXCON = space(6)
        .w_NULIV = 0
        .w_TIPMAS = space(1)
        .w_FLACON = space(1)
        .w_FLACO2 = space(1)
        .w_PROSTU = space(8)
        .w_TIDTOBSO = ctod("  /  /  ")
        .w_MASTRO = space(15)
        .w_CATEGORIA = space(5)
        .w_DATOBSORIF = ctod("  /  /  ")
        .w_DATOBSOIVA = ctod("  /  /  ")
        .w_FLSCM = space(1)
        .w_ANCODISO = g_ISONAZ
        .w_IVALIS = space(1)
        .w_VALLIS = space(3)
        .w_DTOBSO = ctod("  /  /  ")
        .w_MAGWIP = space(1)
        .w_DATOBSOZON = ctod("  /  /  ")
        .w_TIPSOT = space(1)
        .w_CONSBF = space(1)
        .w_CODPAG = space(5)
        .w_DATOBSOPAG = ctod("  /  /  ")
        .w_CONTA = .w_CONTA
        .w_DESGRU = space(35)
        .w_DESPAG = space(30)
        .w_DESBAN = space(42)
        .w_DESBA2 = space(42)
        .w_DESCAC = space(35)
        .w_DESSCM = space(35)
        .w_DESLIN = space(30)
        .w_DESVAL = space(35)
        .w_DESLIS = space(40)
        .w_DESZON = space(35)
        .w_DESMAG = space(30)
        .w_MSDESIMB = space(40)
        .w_MSDESTRA = space(40)
        .w_GDDESCRI = space(40)
        .w_DESCRI = space(40)
        .w_DESCAU = space(40)
        .w_ECTERRIT = space(30)
        .w_DESCREG = space(30)
        .w_DESIRPEF = space(35)
        .w_DESTR2 = space(35)
        .w_DFDESCRI = space(60)
        .w_RADESCRI = space(50)
        .w_DANOM = .f.
        .w_CAUPRE2 = space(2)
          .link_1_1('Load')
        .w_ANTIPCON = NVL(ANTIPCON,space(1))
        .w_ANCODICE = NVL(ANCODICE,space(15))
        .op_ANCODICE = .w_ANCODICE
        .w_ANDESCRI = NVL(ANDESCRI,space(60))
        .w_ANTIPCLF = NVL(ANTIPCLF,space(1))
        .w_ANDESCR2 = NVL(ANDESCR2,space(60))
        .w_ANINDIRI = NVL(ANINDIRI,space(35))
        .w_ANINDIR2 = NVL(ANINDIR2,space(35))
        .w_AN___CAP = NVL(AN___CAP,space(8))
        .w_ANLOCALI = NVL(ANLOCALI,space(30))
        .w_ANPROVIN = NVL(ANPROVIN,space(2))
        .w_ANNAZION = NVL(ANNAZION,space(3))
          if link_1_16_joined
            this.w_ANNAZION = NVL(NACODNAZ116,NVL(this.w_ANNAZION,space(3)))
            this.w_DESNAZ = NVL(NADESNAZ116,space(35))
            this.w_CODISO = NVL(NACODISO116,space(3))
          else
          .link_1_16('Load')
          endif
        .w_ANCODFIS = NVL(ANCODFIS,space(16))
        .w_ANPARIVA = NVL(ANPARIVA,space(12))
        .w_ANTELEFO = NVL(ANTELEFO,space(18))
        .w_ANTELFAX = NVL(ANTELFAX,space(18))
        .w_ANNUMCEL = NVL(ANNUMCEL,space(18))
        .w_ANINDWEB = NVL(ANINDWEB,space(254))
        .w_AN_EMAIL = NVL(AN_EMAIL,space(254))
        .w_AN_EMPEC = NVL(AN_EMPEC,space(254))
        .w_ANPERFIS = NVL(ANPERFIS,space(1))
        .w_AN_SESSO = NVL(AN_SESSO,space(1))
        .w_ANCOGNOM = NVL(ANCOGNOM,space(30))
        .w_AN__NOME = NVL(AN__NOME,space(30))
        .w_ANLOCNAS = NVL(ANLOCNAS,space(30))
        .w_ANPRONAS = NVL(ANPRONAS,space(2))
        .w_ANDATNAS = NVL(cp_ToDate(ANDATNAS),ctod("  /  /  "))
        .w_ANNUMCAR = NVL(ANNUMCAR,space(18))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_ANCHKSTA = NVL(ANCHKSTA,space(1))
        .w_ANCHKMAI = NVL(ANCHKMAI,space(1))
        .w_ANCHKPEC = NVL(ANCHKPEC,space(1))
        .w_ANCHKFAX = NVL(ANCHKFAX,space(1))
        .w_ANCHKCPZ = NVL(ANCHKCPZ,space(1))
        .w_ANCHKWWP = NVL(ANCHKWWP,space(1))
        .w_ANCHKPTL = NVL(ANCHKPTL,space(1))
        .w_ANCHKFIR = NVL(ANCHKFIR,space(1))
        .w_ANCODEST = NVL(ANCODEST,space(7))
        .w_ANCODCLA = NVL(ANCODCLA,space(5))
        .w_ANCODPEC = NVL(ANCODPEC,space(0))
        .w_ANDTOBSO = NVL(cp_ToDate(ANDTOBSO),ctod("  /  /  "))
        .w_ANDTINVA = NVL(cp_ToDate(ANDTINVA),ctod("  /  /  "))
        .w_ANTIPSOT = NVL(ANTIPSOT,space(1))
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .w_TIPRIF = 'C'
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
          .link_2_4('Load')
        .w_ANCONSUP = NVL(ANCONSUP,space(15))
          if link_2_5_joined
            this.w_ANCONSUP = NVL(MCCODICE205,NVL(this.w_ANCONSUP,space(15)))
            this.w_DESSUP = NVL(MCDESCRI205,space(40))
            this.w_NULIV = NVL(MCNUMLIV205,0)
            this.w_TIPMAS = NVL(MCTIPMAS205,space(1))
            this.w_CODSTU = NVL(MCCODSTU205,space(3))
            this.w_PROSTU = NVL(MCPROSTU205,space(8))
          else
          .link_2_5('Load')
          endif
        .w_ANCONRIF = NVL(ANCONRIF,space(15))
          .link_2_8('Load')
        .w_ANCATCON = NVL(ANCATCON,space(5))
          if link_2_10_joined
            this.w_ANCATCON = NVL(C2CODICE210,NVL(this.w_ANCATCON,space(5)))
            this.w_DESCON = NVL(C2DESCRI210,space(35))
          else
          .link_2_10('Load')
          endif
        .w_ANCODIVA = NVL(ANCODIVA,space(5))
          if link_2_12_joined
            this.w_ANCODIVA = NVL(IVCODIVA212,NVL(this.w_ANCODIVA,space(5)))
            this.w_DESIVA = NVL(IVDESIVA212,space(35))
            this.w_PERIVA = NVL(IVPERIVA212,0)
            this.w_DATOBSOIVA = NVL(cp_ToDate(IVDTOBSO212),ctod("  /  /  "))
          else
          .link_2_12('Load')
          endif
        .w_ANTIPOPE = NVL(ANTIPOPE,space(10))
          .link_2_14('Load')
        .w_ANCONCAU = NVL(ANCONCAU,space(15))
          .link_2_16('Load')
        .w_ANOPETRE = NVL(ANOPETRE,space(1))
        .w_ANTIPPRE = NVL(ANTIPPRE,space(1))
        .w_ANPARTSN = NVL(ANPARTSN,space(1))
        .w_ANFLAACC = NVL(ANFLAACC,space(1))
        .w_AFFLINTR = NVL(AFFLINTR,space(1))
        .w_ANFLBLLS = NVL(ANFLBLLS,space(1))
        .w_ANFLSOAL = NVL(ANFLSOAL,space(1))
        .w_ANFLBODO = NVL(ANFLBODO,space(1))
        .w_ANIVASOS = NVL(ANIVASOS,space(1))
        .w_ANCODSTU = NVL(ANCODSTU,space(10))
        .w_ANCODSOG = NVL(ANCODSOG,space(8))
        .w_ANCODCAT = NVL(ANCODCAT,space(4))
          * evitabile
          *.link_2_34('Load')
        .w_OBTEST = i_datsys
        .w_ANTIPRIF = NVL(ANTIPRIF,space(1))
        .w_ANFLSGRE = NVL(ANFLSGRE,space(1))
        .w_ANCATOPE = NVL(ANCATOPE,space(2))
        .w_ANCODCUC = NVL(ANCODCUC,space(2))
        .w_TIPCC = 'G'
        .w_ANNUMCOR = NVL(ANNUMCOR,space(25))
        .w_ANCINABI = NVL(ANCINABI,space(1))
        .w_AN__BBAN = NVL(AN__BBAN,space(30))
        .w_AN__IBAN = NVL(AN__IBAN,space(35))
        .w_RIFDIC = ''
        .w_ANNDIC = ''
        .w_DATDIC = ''
        .w_NUMDIC = ''
        .w_ANDESCR3 = NVL(ANDESCR3,space(60))
        .w_ANDESCR4 = NVL(ANDESCR4,space(60))
        .w_ANINDIR3 = NVL(ANINDIR3,space(35))
        .w_ANINDIR4 = NVL(ANINDIR4,space(35))
        .w_ANLOCAL2 = NVL(ANLOCAL2,space(30))
        .w_DES1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_ANCODGRU = NVL(ANCODGRU,space(10))
          if link_8_4_joined
            this.w_ANCODGRU = NVL(GRCODICE804,NVL(this.w_ANCODGRU,space(10)))
            this.w_DESGRU = NVL(GRDESCRI804,space(35))
          else
          .link_8_4('Load')
          endif
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .w_ANCODPAG = NVL(ANCODPAG,space(5))
          if link_5_4_joined
            this.w_ANCODPAG = NVL(PACODICE504,NVL(this.w_ANCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI504,space(30))
            this.w_DATOBSOPAG = NVL(cp_ToDate(PADTOBSO504),ctod("  /  /  "))
          else
          .link_5_4('Load')
          endif
        .w_ANGIOFIS = NVL(ANGIOFIS,0)
        .w_AN1MESCL = NVL(AN1MESCL,0)
        .w_DESMES1 = LEFT(IIF(.w_AN1MESCL>0 AND .w_AN1MESCL<13, g_MESE[.w_AN1MESCL], "")+SPACE(12),12)
        .w_ANGIOSC1 = NVL(ANGIOSC1,0)
        .w_AN2MESCL = NVL(AN2MESCL,0)
        .w_DESMES2 = LEFT(IIF(.w_AN2MESCL>0 AND .w_AN2MESCL<13, g_MESE[.w_AN2MESCL], "")+SPACE(12),12)
        .w_ANGIOSC2 = NVL(ANGIOSC2,0)
        .w_ANCODBAN = NVL(ANCODBAN,space(10))
          if link_5_15_joined
            this.w_ANCODBAN = NVL(BACODBAN515,NVL(this.w_ANCODBAN,space(10)))
            this.w_DESBAN = NVL(BADESBAN515,space(42))
          else
          .link_5_15('Load')
          endif
        .w_ANCODBA2 = NVL(ANCODBA2,space(15))
          if link_5_17_joined
            this.w_ANCODBA2 = NVL(BACODBAN517,NVL(this.w_ANCODBA2,space(15)))
            this.w_DESBA2 = NVL(BADESCRI517,space(42))
            this.w_DATOBSO = NVL(cp_ToDate(BADTOBSO517),ctod("  /  /  "))
            this.w_CONSBF = NVL(BACONSBF517,space(1))
          else
          .link_5_17('Load')
          endif
        .w_ANFLINCA = NVL(ANFLINCA,space(1))
        .w_ANSPEINC = NVL(ANSPEINC,0)
        .w_ANFLESIM = NVL(ANFLESIM,space(1))
        .w_ANSAGINT = NVL(ANSAGINT,0)
        .w_ANSPRINT = NVL(ANSPRINT,space(1))
        .w_ANFLACBD = NVL(ANFLACBD,space(1))
        .w_ANFLRAGG = NVL(ANFLRAGG,space(1))
        .w_ANFLGAVV = NVL(ANFLGAVV,space(1))
        .w_ANDATAVV = NVL(cp_ToDate(ANDATAVV),ctod("  /  /  "))
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .w_ANCATCOM = NVL(ANCATCOM,space(3))
          if link_4_12_joined
            this.w_ANCATCOM = NVL(CTCODICE412,NVL(this.w_ANCATCOM,space(3)))
            this.w_DESCAC = NVL(CTDESCRI412,space(35))
          else
          .link_4_12('Load')
          endif
        .w_ANGRPDEF = NVL(ANGRPDEF,space(5))
          if link_4_13_joined
            this.w_ANGRPDEF = NVL(GDCODICE413,NVL(this.w_ANGRPDEF,space(5)))
            this.w_GDDESCRI = NVL(GDDESCRI413,space(40))
          else
          .link_4_13('Load')
          endif
        .w_ANCATSCM = NVL(ANCATSCM,space(5))
          if link_4_15_joined
            this.w_ANCATSCM = NVL(CSCODICE415,NVL(this.w_ANCATSCM,space(5)))
            this.w_DESSCM = NVL(CSDESCRI415,space(35))
            this.w_FLSCM = NVL(CSTIPCAT415,space(1))
          else
          .link_4_15('Load')
          endif
        .w_ANCODLIN = NVL(ANCODLIN,space(3))
          if link_4_17_joined
            this.w_ANCODLIN = NVL(LUCODICE417,NVL(this.w_ANCODLIN,space(3)))
            this.w_DESLIN = NVL(LUDESCRI417,space(30))
          else
          .link_4_17('Load')
          endif
        .w_ANCODVAL = NVL(ANCODVAL,space(3))
          if link_4_19_joined
            this.w_ANCODVAL = NVL(VACODVAL419,NVL(this.w_ANCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL419,space(35))
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO419),ctod("  /  /  "))
          else
          .link_4_19('Load')
          endif
        .w_ANNUMLIS = NVL(ANNUMLIS,space(5))
          if link_4_21_joined
            this.w_ANNUMLIS = NVL(LSCODLIS421,NVL(this.w_ANNUMLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS421,space(40))
            this.w_VALLIS = NVL(LSVALLIS421,space(3))
            this.w_IVALIS = NVL(LSIVALIS421,space(1))
          else
          .link_4_21('Load')
          endif
        .w_AN1SCONT = NVL(AN1SCONT,0)
        .w_AN2SCONT = NVL(AN2SCONT,0)
        .w_ANCODZON = NVL(ANCODZON,space(3))
          if link_4_25_joined
            this.w_ANCODZON = NVL(ZOCODZON425,NVL(this.w_ANCODZON,space(3)))
            this.w_DESZON = NVL(ZODESZON425,space(35))
            this.w_DATOBSOZON = NVL(cp_ToDate(ZODTOBSO425),ctod("  /  /  "))
          else
          .link_4_25('Load')
          endif
        .w_ANCONCON = NVL(ANCONCON,space(1))
          * evitabile
          *.link_4_26('Load')
        .w_ANMAGTER = NVL(ANMAGTER,space(5))
          if link_4_38_joined
            this.w_ANMAGTER = NVL(MGCODMAG438,NVL(this.w_ANMAGTER,space(5)))
            this.w_DESMAG = NVL(MGDESMAG438,space(30))
            this.w_MAGWIP = NVL(MGTIPMAG438,space(1))
          else
          .link_4_38('Load')
          endif
        .w_ANCODESC = NVL(ANCODESC,space(5))
        .w_ANCODPOR = NVL(ANCODPOR,space(10))
          if link_4_40_joined
            this.w_ANCODPOR = NVL(PPCODICE440,NVL(this.w_ANCODPOR,space(10)))
            this.w_DESCRI = NVL(PPDESCRI440,space(40))
          else
          .link_4_40('Load')
          endif
        .w_ANMCALSI = NVL(ANMCALSI,space(5))
          if link_4_44_joined
            this.w_ANMCALSI = NVL(MSCODICE444,NVL(this.w_ANMCALSI,space(5)))
            this.w_MSDESIMB = NVL(MSDESCRI444,space(40))
          else
          .link_4_44('Load')
          endif
        .w_ANMCALST = NVL(ANMCALST,space(5))
          if link_4_45_joined
            this.w_ANMCALST = NVL(MSCODICE445,NVL(this.w_ANMCALST,space(5)))
            this.w_MSDESTRA = NVL(MSDESCRI445,space(40))
          else
          .link_4_45('Load')
          endif
        .w_ANSCORPO = NVL(ANSCORPO,space(1))
        .w_ANFLCODI = NVL(ANFLCODI,space(1))
        .w_ANFLIMBA = NVL(ANFLIMBA,space(1))
        .w_ANFLGCPZ = NVL(ANFLGCPZ,space(1))
        .w_ANFLGCAU = NVL(ANFLGCAU,space(1))
        .w_ANFLAPCA = NVL(ANFLAPCA,space(1))
        .w_ANRITENU = NVL(ANRITENU,space(1))
        .w_ANCODIRP = NVL(ANCODIRP,space(5))
          if link_3_32_joined
            this.w_ANCODIRP = NVL(TRCODTRI332,NVL(this.w_ANCODIRP,space(5)))
            this.w_DESIRPEF = NVL(TRDESTRI332,space(35))
            this.w_FLIRPE = NVL(TRFLACON332,space(1))
            this.w_CAUPRE2 = NVL(TRCAUPRE2332,space(2))
          else
          .link_3_32('Load')
          endif
        .w_ANCODTR2 = NVL(ANCODTR2,space(5))
          if link_3_33_joined
            this.w_ANCODTR2 = NVL(TRCODTRI333,NVL(this.w_ANCODTR2,space(5)))
            this.w_DESTR2 = NVL(TRDESTRI333,space(35))
            this.w_FLACO2 = NVL(TRFLACON333,space(1))
          else
          .link_3_33('Load')
          endif
        .w_ANPEINPS = NVL(ANPEINPS,0)
        .w_ANRIINPS = NVL(ANRIINPS,0)
        .w_ANCOINPS = NVL(ANCOINPS,0)
        .w_ANCASPRO = NVL(ANCASPRO,0)
        .w_ANCODATT = NVL(ANCODATT,0)
        .w_ANCODASS = NVL(ANCODASS,space(3))
        .w_ANCOIMPS = NVL(ANCOIMPS,space(20))
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .w_AN__NOTE = NVL(AN__NOTE,space(0))
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .w_ANRATING = NVL(ANRATING,space(2))
          if link_9_7_joined
            this.w_ANRATING = NVL(RACODICE907,NVL(this.w_ANRATING,space(2)))
            this.w_RADESCRI = NVL(RADESCRI907,space(50))
          else
          .link_9_7('Load')
          endif
        .w_ANGIORIT = NVL(ANGIORIT,0)
        .w_ANVOCFIN = NVL(ANVOCFIN,space(6))
          if link_9_9_joined
            this.w_ANVOCFIN = NVL(DFVOCFIN909,NVL(this.w_ANVOCFIN,space(6)))
            this.w_DFDESCRI = NVL(DFDESCRI909,space(60))
          else
          .link_9_9('Load')
          endif
        .w_ANESCDOF = NVL(ANESCDOF,space(1))
        .w_ANDESPAR = NVL(ANDESPAR,space(1))
        .w_CODI = .w_ANCODICE
        .w_ANFLGEST = NVL(ANFLGEST,space(1))
        .w_ANDESCRI = NVL(ANDESCRI,space(40))
        .w_ANFLSGRE = NVL(ANFLSGRE,space(1))
        .w_ANDESCR2 = NVL(ANDESCR2,space(40))
        .w_ANCOFISC = NVL(ANCOFISC,space(25))
        .w_ANSCHUMA = NVL(ANSCHUMA,space(1))
        .w_AN_EREDE = NVL(AN_EREDE,space(1))
        .w_ANCODSNS = NVL(ANCODSNS,space(1))
        .w_ANCODREG = NVL(ANCODREG,space(2))
          if link_3_53_joined
            this.w_ANCODREG = NVL(RPCODICE353,NVL(this.w_ANCODREG,space(2)))
            this.w_DESCREG = NVL(RPDESCRI353,space(30))
          else
          .link_3_53('Load')
          endif
        .w_ANCODCOM = NVL(ANCODCOM,space(4))
          if link_3_54_joined
            this.w_ANCODCOM = NVL(ECCODICE354,NVL(this.w_ANCODCOM,space(4)))
            this.w_ECTERRIT = NVL(ECTERRIT354,space(30))
          else
          .link_3_54('Load')
          endif
        .w_ANCAURIT = NVL(ANCAURIT,space(2))
        .w_ANEVEECC = NVL(ANEVEECC,space(1))
        .w_ANCATPAR = NVL(ANCATPAR,space(2))
        .w_ANFLESIG = NVL(ANFLESIG,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CONTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_78.enabled = this.oPgFrm.Page1.oPag.oBtn_1_78.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_80.enabled = this.oPgFrm.Page1.oPag.oBtn_1_80.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_82.enabled = this.oPgFrm.Page1.oPag.oBtn_1_82.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_88.enabled = this.oPgFrm.Page1.oPag.oBtn_1_88.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_89.enabled = this.oPgFrm.Page1.oPag.oBtn_1_89.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_51.enabled = this.oPgFrm.Page2.oPag.oBtn_2_51.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_52.enabled = this.oPgFrm.Page2.oPag.oBtn_2_52.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_53.enabled = this.oPgFrm.Page2.oPag.oBtn_2_53.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_108.enabled = this.oPgFrm.Page1.oPag.oBtn_1_108.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_11.enabled = this.oPgFrm.Page5.oPag.oBtn_5_11.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_12.enabled = this.oPgFrm.Page5.oPag.oBtn_5_12.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_54.enabled = this.oPgFrm.Page4.oPag.oBtn_4_54.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_55.enabled = this.oPgFrm.Page4.oPag.oBtn_4_55.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_56.enabled = this.oPgFrm.Page4.oPag.oBtn_4_56.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_57.enabled = this.oPgFrm.Page4.oPag.oBtn_4_57.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_112.enabled = this.oPgFrm.Page1.oPag.oBtn_1_112.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_afr
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_ANCONCON")
    CTRL_CONCON.Popola()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AUTOAZI = space(5)
      .w_AUTO = space(1)
      .w_ANTIPCON = space(1)
      .w_ANCODICE = space(15)
      .w_ANDESCRI = space(60)
      .w_ANTIPCLF = space(1)
      .w_ANDESCR2 = space(60)
      .w_VALATT = space(20)
      .w_TABKEY = space(8)
      .w_DELIMM = .f.
      .w_ANINDIRI = space(35)
      .w_ANINDIR2 = space(35)
      .w_AN___CAP = space(8)
      .w_ANLOCALI = space(30)
      .w_ANPROVIN = space(2)
      .w_ANNAZION = space(3)
      .w_DESNAZ = space(35)
      .w_ANCODFIS = space(16)
      .w_ANPARIVA = space(12)
      .w_ANTELEFO = space(18)
      .w_ANTELFAX = space(18)
      .w_ANNUMCEL = space(18)
      .w_ANINDWEB = space(254)
      .w_AN_EMAIL = space(254)
      .w_AN_EMPEC = space(254)
      .w_ANPERFIS = space(1)
      .w_AN_SESSO = space(1)
      .w_ANCOGNOM = space(30)
      .w_AN__NOME = space(30)
      .w_ANLOCNAS = space(30)
      .w_ANPRONAS = space(2)
      .w_ANDATNAS = ctod("  /  /  ")
      .w_ANNUMCAR = space(18)
      .w_CODISO = space(3)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_ANCHKSTA = space(1)
      .w_ANCHKMAI = space(1)
      .w_ANCHKPEC = space(1)
      .w_ANCHKFAX = space(1)
      .w_ANCHKCPZ = space(1)
      .w_ANCHKWWP = space(1)
      .w_ANCHKPTL = space(1)
      .w_ANCHKFIR = space(1)
      .w_ANCODEST = space(7)
      .w_ANCODCLA = space(5)
      .w_ANCODPEC = space(0)
      .w_ANDTOBSO = ctod("  /  /  ")
      .w_ANDTINVA = ctod("  /  /  ")
      .w_ANTIPSOT = space(1)
      .w_ERR = space(10)
      .w_CODCOM = space(4)
      .w_TIPRIF = space(1)
      .w_CODI = space(15)
      .w_DES1 = space(40)
      .w_CODAZI1 = space(5)
      .w_ANCONSUP = space(15)
      .w_DESSUP = space(40)
      .w_CODSTU = space(3)
      .w_ANCONRIF = space(15)
      .w_DESORI = space(40)
      .w_ANCATCON = space(5)
      .w_DESCON = space(35)
      .w_ANCODIVA = space(5)
      .w_DESIVA = space(35)
      .w_ANTIPOPE = space(10)
      .w_TIDESCRI = space(30)
      .w_ANCONCAU = space(15)
      .w_ANOPETRE = space(1)
      .w_ANTIPPRE = space(1)
      .w_PERIVA = 0
      .w_DATOBSO = ctod("  /  /  ")
      .w_FLIRPE = space(1)
      .w_MINCON = space(6)
      .w_ANPARTSN = space(1)
      .w_ANFLAACC = space(1)
      .w_MAXCON = space(6)
      .w_AFFLINTR = space(1)
      .w_ANFLBLLS = space(1)
      .w_ANFLSOAL = space(1)
      .w_ANFLBODO = space(1)
      .w_ANIVASOS = space(1)
      .w_ANCODSTU = space(10)
      .w_ANCODSOG = space(8)
      .w_ANCODCAT = space(4)
      .w_NULIV = 0
      .w_TIPMAS = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_ANTIPRIF = space(1)
      .w_FLACON = space(1)
      .w_FLACO2 = space(1)
      .w_PROSTU = space(8)
      .w_ANFLSGRE = space(1)
      .w_ANCATOPE = space(2)
      .w_TIDTOBSO = ctod("  /  /  ")
      .w_MASTRO = space(15)
      .w_CATEGORIA = space(5)
      .w_DATOBSORIF = ctod("  /  /  ")
      .w_DATOBSOIVA = ctod("  /  /  ")
      .w_FLSCM = space(1)
      .w_ANCODISO = space(3)
      .w_IVALIS = space(1)
      .w_VALLIS = space(3)
      .w_ANCODCUC = space(2)
      .w_DTOBSO = ctod("  /  /  ")
      .w_MAGWIP = space(1)
      .w_DATOBSOZON = ctod("  /  /  ")
      .w_TIPCC = space(1)
      .w_ANNUMCOR = space(25)
      .w_ANCINABI = space(1)
      .w_AN__BBAN = space(30)
      .w_AN__IBAN = space(35)
      .w_TIPSOT = space(1)
      .w_CONSBF = space(1)
      .w_CODPAG = space(5)
      .w_DATOBSOPAG = ctod("  /  /  ")
      .w_CONTA = 0
      .w_RIFDIC = space(10)
      .w_ANNDIC = space(4)
      .w_DATDIC = ctod("  /  /  ")
      .w_NUMDIC = 0
      .w_ANDESCR3 = space(60)
      .w_ANDESCR4 = space(60)
      .w_ANINDIR3 = space(35)
      .w_ANINDIR4 = space(35)
      .w_ANLOCAL2 = space(30)
      .w_DES1 = space(40)
      .w_CODI = space(15)
      .w_ANCODGRU = space(10)
      .w_DESGRU = space(35)
      .w_CODI = space(15)
      .w_DES1 = space(40)
      .w_CODI = space(15)
      .w_DES1 = space(40)
      .w_CODI = space(15)
      .w_DES1 = space(40)
      .w_ANCODPAG = space(5)
      .w_DESPAG = space(30)
      .w_ANGIOFIS = 0
      .w_AN1MESCL = 0
      .w_DESMES1 = space(12)
      .w_ANGIOSC1 = 0
      .w_AN2MESCL = 0
      .w_DESMES2 = space(12)
      .w_ANGIOSC2 = 0
      .w_ANCODBAN = space(10)
      .w_DESBAN = space(42)
      .w_ANCODBA2 = space(15)
      .w_ANFLINCA = space(1)
      .w_ANSPEINC = 0
      .w_ANFLESIM = space(1)
      .w_ANSAGINT = 0
      .w_ANSPRINT = space(1)
      .w_ANFLACBD = space(1)
      .w_ANFLRAGG = space(1)
      .w_ANFLGAVV = space(1)
      .w_ANDATAVV = ctod("  /  /  ")
      .w_DESBA2 = space(42)
      .w_CODI = space(15)
      .w_DES1 = space(40)
      .w_ANCATCOM = space(3)
      .w_ANGRPDEF = space(5)
      .w_DESCAC = space(35)
      .w_ANCATSCM = space(5)
      .w_DESSCM = space(35)
      .w_ANCODLIN = space(3)
      .w_DESLIN = space(30)
      .w_ANCODVAL = space(3)
      .w_DESVAL = space(35)
      .w_ANNUMLIS = space(5)
      .w_DESLIS = space(40)
      .w_AN1SCONT = 0
      .w_AN2SCONT = 0
      .w_ANCODZON = space(3)
      .w_ANCONCON = space(1)
      .w_DESZON = space(35)
      .w_ANMAGTER = space(5)
      .w_ANCODESC = space(5)
      .w_ANCODPOR = space(10)
      .w_DESMAG = space(30)
      .w_ANMCALSI = space(5)
      .w_ANMCALST = space(5)
      .w_MSDESIMB = space(40)
      .w_ANSCORPO = space(1)
      .w_ANFLCODI = space(1)
      .w_ANFLIMBA = space(1)
      .w_ANFLGCPZ = space(1)
      .w_MSDESTRA = space(40)
      .w_GDDESCRI = space(40)
      .w_ANFLGCAU = space(1)
      .w_DESCRI = space(40)
      .w_ANFLAPCA = space(1)
      .w_DESCAU = space(40)
      .w_ECTERRIT = space(30)
      .w_DESCREG = space(30)
      .w_DESIRPEF = space(35)
      .w_DESTR2 = space(35)
      .w_ANRITENU = space(1)
      .w_ANCODIRP = space(5)
      .w_ANCODTR2 = space(5)
      .w_ANPEINPS = 0
      .w_ANRIINPS = 0
      .w_ANCOINPS = 0
      .w_ANCASPRO = 0
      .w_ANCODATT = 0
      .w_ANCODASS = space(3)
      .w_ANCOIMPS = space(20)
      .w_CODI = space(15)
      .w_DES1 = space(40)
      .w_AN__NOTE = space(0)
      .w_CODI = space(15)
      .w_DES1 = space(40)
      .w_ANRATING = space(2)
      .w_ANGIORIT = 0
      .w_ANVOCFIN = space(6)
      .w_DFDESCRI = space(60)
      .w_ANESCDOF = space(1)
      .w_ANDESPAR = space(1)
      .w_RADESCRI = space(50)
      .w_CODI = space(15)
      .w_ANFLGEST = space(1)
      .w_ANDESCRI = space(40)
      .w_ANFLSGRE = space(1)
      .w_ANDESCR2 = space(40)
      .w_ANCOFISC = space(25)
      .w_ANSCHUMA = space(1)
      .w_AN_EREDE = space(1)
      .w_ANCODSNS = space(1)
      .w_ANCODREG = space(2)
      .w_ANCODCOM = space(4)
      .w_DANOM = .f.
      .w_ANCAURIT = space(2)
      .w_CAUPRE2 = space(2)
      .w_ANEVEECC = space(1)
      .w_ANCATPAR = space(2)
      .w_ANFLESIG = space(1)
      if .cFunction<>"Filter"
        .w_AUTOAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AUTOAZI))
          .link_1_1('Full')
          endif
        .w_AUTO = ' '
        .w_ANTIPCON = 'F'
        .w_ANCODICE = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_ANCODICE)
          .DoRTCalc(5,5,.f.)
        .w_ANTIPCLF = 'G'
          .DoRTCalc(7,7,.f.)
        .w_VALATT = "F"+.w_ANCODICE
        .w_TABKEY = 'CONTI'
        .w_DELIMM = .f.
        .DoRTCalc(11,16,.f.)
          if not(empty(.w_ANNAZION))
          .link_1_16('Full')
          endif
          .DoRTCalc(17,25,.f.)
        .w_ANPERFIS = 'N'
        .w_AN_SESSO = 'M'
          .DoRTCalc(28,45,.f.)
        .w_ANCHKFIR = iif(.w_ANCHKMAI='S' or .w_ANCHKCPZ = 'S',.w_ANCHKFIR,'N')
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
          .DoRTCalc(47,54,.f.)
        .w_TIPRIF = 'C'
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .w_CODAZI1 = i_codazi
        .DoRTCalc(58,58,.f.)
          if not(empty(.w_CODAZI1))
          .link_2_4('Full')
          endif
        .w_ANCONSUP = iif(.cFunction='Query',Space(15),.w_MASTRO)
        .DoRTCalc(59,59,.f.)
          if not(empty(.w_ANCONSUP))
          .link_2_5('Full')
          endif
        .DoRTCalc(60,62,.f.)
          if not(empty(.w_ANCONRIF))
          .link_2_8('Full')
          endif
          .DoRTCalc(63,63,.f.)
        .w_ANCATCON = iif(.cFunction='Query',Space(5),.w_CATEGORIA)
        .DoRTCalc(64,64,.f.)
          if not(empty(.w_ANCATCON))
          .link_2_10('Full')
          endif
        .DoRTCalc(65,66,.f.)
          if not(empty(.w_ANCODIVA))
          .link_2_12('Full')
          endif
        .DoRTCalc(67,68,.f.)
          if not(empty(.w_ANTIPOPE))
          .link_2_14('Full')
          endif
        .DoRTCalc(69,70,.f.)
          if not(empty(.w_ANCONCAU))
          .link_2_16('Full')
          endif
        .w_ANOPETRE = 'N'
        .w_ANTIPPRE = 'N'
          .DoRTCalc(73,76,.f.)
        .w_ANPARTSN = g_PERPAR
          .DoRTCalc(78,80,.f.)
        .w_ANFLBLLS = 'N'
        .w_ANFLSOAL = 'N'
        .w_ANFLBODO = iif(.w_AFFLINTR='S' or .w_ANFLSOAL='S' or g_TRAEXP = 'N',' ',.w_ANFLBODO)
        .w_ANIVASOS = 'N'
        .w_ANCODSTU = IIF(g_TRAEXP<>'G',SPACE(9),.w_ANCODSTU)
        .DoRTCalc(86,87,.f.)
          if not(empty(.w_ANCODCAT))
          .link_2_34('Full')
          endif
          .DoRTCalc(88,89,.f.)
        .w_OBTEST = i_datsys
        .w_ANTIPRIF = IIF(EMPTY(.w_ANCONRIF), ' ', 'C')
          .DoRTCalc(92,94,.f.)
        .w_ANFLSGRE = 'N'
        .w_ANCATOPE = 'CF'
          .DoRTCalc(97,102,.f.)
        .w_ANCODISO = g_ISONAZ
          .DoRTCalc(104,109,.f.)
        .w_TIPCC = 'G'
          .DoRTCalc(111,118,.f.)
        .w_CONTA = .w_CONTA
        .w_RIFDIC = ''
        .w_ANNDIC = ''
        .w_DATDIC = ''
        .w_NUMDIC = ''
          .DoRTCalc(124,128,.f.)
        .w_DES1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .DoRTCalc(131,131,.f.)
          if not(empty(.w_ANCODGRU))
          .link_8_4('Full')
          endif
          .DoRTCalc(132,132,.f.)
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .w_ANCODPAG = iif(.cFunction='Query',Space(5),.w_CODPAG)
        .DoRTCalc(139,139,.f.)
          if not(empty(.w_ANCODPAG))
          .link_5_4('Full')
          endif
          .DoRTCalc(140,142,.f.)
        .w_DESMES1 = LEFT(IIF(.w_AN1MESCL>0 AND .w_AN1MESCL<13, g_MESE[.w_AN1MESCL], "")+SPACE(12),12)
        .w_ANGIOSC1 = 0
        .w_AN2MESCL = 0
        .w_DESMES2 = LEFT(IIF(.w_AN2MESCL>0 AND .w_AN2MESCL<13, g_MESE[.w_AN2MESCL], "")+SPACE(12),12)
        .w_ANGIOSC2 = 0
        .DoRTCalc(148,148,.f.)
          if not(empty(.w_ANCODBAN))
          .link_5_15('Full')
          endif
        .DoRTCalc(149,150,.f.)
          if not(empty(.w_ANCODBA2))
          .link_5_17('Full')
          endif
          .DoRTCalc(151,153,.f.)
        .w_ANSAGINT = IIF(.w_ANFLESIM='S', 0, .w_ANSAGINT)
        .w_ANSPRINT = IIF(.w_ANFLESIM='S' OR .w_ANSAGINT=0,'N',.w_ANSPRINT)
          .DoRTCalc(156,157,.f.)
        .w_ANFLGAVV = 'N'
          .DoRTCalc(159,160,.f.)
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .DoRTCalc(163,163,.f.)
          if not(empty(.w_ANCATCOM))
          .link_4_12('Full')
          endif
        .DoRTCalc(164,164,.f.)
          if not(empty(.w_ANGRPDEF))
          .link_4_13('Full')
          endif
        .DoRTCalc(165,166,.f.)
          if not(empty(.w_ANCATSCM))
          .link_4_15('Full')
          endif
          .DoRTCalc(167,167,.f.)
        .w_ANCODLIN = g_CODLIN
        .DoRTCalc(168,168,.f.)
          if not(empty(.w_ANCODLIN))
          .link_4_17('Full')
          endif
        .DoRTCalc(169,170,.f.)
          if not(empty(.w_ANCODVAL))
          .link_4_19('Full')
          endif
        .DoRTCalc(171,172,.f.)
          if not(empty(.w_ANNUMLIS))
          .link_4_21('Full')
          endif
          .DoRTCalc(173,174,.f.)
        .w_AN2SCONT = 0
        .DoRTCalc(176,176,.f.)
          if not(empty(.w_ANCODZON))
          .link_4_25('Full')
          endif
        .w_ANCONCON = IIF(g_ISONAZ='ITA','E','1')
        .DoRTCalc(177,177,.f.)
          if not(empty(.w_ANCONCON))
          .link_4_26('Full')
          endif
        .DoRTCalc(178,179,.f.)
          if not(empty(.w_ANMAGTER))
          .link_4_38('Full')
          endif
        .DoRTCalc(180,181,.f.)
          if not(empty(.w_ANCODPOR))
          .link_4_40('Full')
          endif
        .DoRTCalc(182,183,.f.)
          if not(empty(.w_ANMCALSI))
          .link_4_44('Full')
          endif
        .DoRTCalc(184,184,.f.)
          if not(empty(.w_ANMCALST))
          .link_4_45('Full')
          endif
          .DoRTCalc(185,185,.f.)
        .w_ANSCORPO = "N"
          .DoRTCalc(187,187,.f.)
        .w_ANFLIMBA = 'N'
        .w_ANFLGCPZ = iif(g_REVI='S', 'S', 'N')
          .DoRTCalc(190,191,.f.)
        .w_ANFLGCAU = 'N'
          .DoRTCalc(193,199,.f.)
        .w_ANRITENU = 'N'
        .w_ANCODIRP = IIF(.w_ANRITENU$'CS',.w_ANCODIRP,space(5))
        .DoRTCalc(201,201,.f.)
          if not(empty(.w_ANCODIRP))
          .link_3_32('Full')
          endif
        .w_ANCODTR2 = SPACE(5)
        .DoRTCalc(202,202,.f.)
          if not(empty(.w_ANCODTR2))
          .link_3_33('Full')
          endif
        .w_ANPEINPS = IIF(.w_ANRITENU='C',.w_ANPEINPS,0)
        .w_ANRIINPS = IIF(.w_ANRITENU='C',.w_ANRIINPS,0)
        .w_ANCOINPS = IIF(.w_ANRITENU='C',.w_ANCOINPS,0)
          .DoRTCalc(206,206,.f.)
        .w_ANCODATT = IIF(.w_ANRITENU='C',1,0)
        .w_ANCODASS = IIF(.w_ANRITENU='C',.w_ANCODASS,SPACE(3))
        .w_ANCOIMPS = IIF(.w_ANRITENU='C',.w_ANCOIMPS,SPACE(20))
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
          .DoRTCalc(212,212,.f.)
        .w_CODI = .w_ANCODICE
        .w_DES1 = .w_ANDESCRI
        .DoRTCalc(215,215,.f.)
          if not(empty(.w_ANRATING))
          .link_9_7('Full')
          endif
        .DoRTCalc(216,217,.f.)
          if not(empty(.w_ANVOCFIN))
          .link_9_9('Full')
          endif
          .DoRTCalc(218,218,.f.)
        .w_ANESCDOF = 'N'
        .w_ANDESPAR = 'N'
          .DoRTCalc(221,221,.f.)
        .w_CODI = .w_ANCODICE
        .w_ANFLGEST = iif(.w_ANRITENU<>'N',.w_ANFLGEST,'N')
          .DoRTCalc(224,224,.f.)
        .w_ANFLSGRE = 'N'
          .DoRTCalc(226,226,.f.)
        .w_ANCOFISC = iif(.w_ANFLGEST='S' OR .w_ANFLBLLS='S',.w_ANCOFISC,SPACE(25))
        .w_ANSCHUMA = 'N'
        .w_AN_EREDE = 'N'
        .w_ANCODSNS = ' '
        .w_ANCODREG = iif(.w_ANFLGEST='S','  ',.w_ANCODREG)
        .DoRTCalc(231,231,.f.)
          if not(empty(.w_ANCODREG))
          .link_3_53('Full')
          endif
        .w_ANCODCOM = iif(.w_ANFLGEST='S',Space(4),.w_ANCODCOM)
        .DoRTCalc(232,232,.f.)
          if not(empty(.w_ANCODCOM))
          .link_3_54('Full')
          endif
          .DoRTCalc(233,233,.f.)
        .w_ANCAURIT = .w_CAUPRE2
          .DoRTCalc(235,235,.f.)
        .w_ANEVEECC = '0'
        .w_ANCATPAR = '  '
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTI')
    this.DoRTCalc(238,238,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_78.enabled = this.oPgFrm.Page1.oPag.oBtn_1_78.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_80.enabled = this.oPgFrm.Page1.oPag.oBtn_1_80.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_82.enabled = this.oPgFrm.Page1.oPag.oBtn_1_82.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_88.enabled = this.oPgFrm.Page1.oPag.oBtn_1_88.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_89.enabled = this.oPgFrm.Page1.oPag.oBtn_1_89.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_51.enabled = this.oPgFrm.Page2.oPag.oBtn_2_51.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_52.enabled = this.oPgFrm.Page2.oPag.oBtn_2_52.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_53.enabled = this.oPgFrm.Page2.oPag.oBtn_2_53.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_108.enabled = this.oPgFrm.Page1.oPag.oBtn_1_108.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_11.enabled = this.oPgFrm.Page5.oPag.oBtn_5_11.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_12.enabled = this.oPgFrm.Page5.oPag.oBtn_5_12.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_54.enabled = this.oPgFrm.Page4.oPag.oBtn_4_54.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_55.enabled = this.oPgFrm.Page4.oPag.oBtn_4_55.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_56.enabled = this.oPgFrm.Page4.oPag.oBtn_4_56.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_57.enabled = this.oPgFrm.Page4.oPag.oBtn_4_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_112.enabled = this.oPgFrm.Page1.oPag.oBtn_1_112.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_afr
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_ANCONCON")
    CTRL_CONCON.Popola()
    if UPPER(this.cfunction)='LOAD'
      this.notifyEvent('CaricaDati')
    endif
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    with this	
      if g_CFNUME='S' AND .p_AUT=0
        cp_AskTableProg(this,i_nConn,"PRNUFO","i_codazi,w_ANCODICE")
      endif
      .op_codazi = .w_codazi
      .op_ANCODICE = .w_ANCODICE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oANCODICE_1_4.enabled = i_bVal
      .Page1.oPag.oANDESCRI_1_5.enabled = i_bVal
      .Page1.oPag.oANTIPCLF_1_6.enabled = i_bVal
      .Page1.oPag.oANDESCR2_1_7.enabled = i_bVal
      .Page1.oPag.oANINDIRI_1_11.enabled = i_bVal
      .Page1.oPag.oANINDIR2_1_12.enabled = i_bVal
      .Page1.oPag.oAN___CAP_1_13.enabled = i_bVal
      .Page1.oPag.oANLOCALI_1_14.enabled = i_bVal
      .Page1.oPag.oANPROVIN_1_15.enabled = i_bVal
      .Page1.oPag.oANNAZION_1_16.enabled = i_bVal
      .Page1.oPag.oANCODFIS_1_18.enabled = i_bVal
      .Page1.oPag.oANPARIVA_1_19.enabled = i_bVal
      .Page1.oPag.oANTELEFO_1_20.enabled = i_bVal
      .Page1.oPag.oANTELFAX_1_21.enabled = i_bVal
      .Page1.oPag.oANNUMCEL_1_22.enabled = i_bVal
      .Page1.oPag.oANINDWEB_1_23.enabled = i_bVal
      .Page1.oPag.oAN_EMAIL_1_24.enabled = i_bVal
      .Page1.oPag.oAN_EMPEC_1_25.enabled = i_bVal
      .Page1.oPag.oANPERFIS_1_26.enabled = i_bVal
      .Page1.oPag.oAN_SESSO_1_27.enabled = i_bVal
      .Page1.oPag.oANCOGNOM_1_28.enabled = i_bVal
      .Page1.oPag.oAN__NOME_1_29.enabled = i_bVal
      .Page1.oPag.oANLOCNAS_1_30.enabled = i_bVal
      .Page1.oPag.oANPRONAS_1_31.enabled = i_bVal
      .Page1.oPag.oANDATNAS_1_32.enabled = i_bVal
      .Page1.oPag.oANNUMCAR_1_33.enabled = i_bVal
      .Page1.oPag.oANCHKSTA_1_57.enabled = i_bVal
      .Page1.oPag.oANCHKMAI_1_58.enabled = i_bVal
      .Page1.oPag.oANCHKPEC_1_59.enabled = i_bVal
      .Page1.oPag.oANCHKFAX_1_60.enabled = i_bVal
      .Page1.oPag.oANCHKCPZ_1_61.enabled = i_bVal
      .Page1.oPag.oANCHKWWP_1_62.enabled = i_bVal
      .Page1.oPag.oANCHKPTL_1_63.enabled = i_bVal
      .Page1.oPag.oANCHKFIR_1_64.enabled = i_bVal
      .Page1.oPag.oANCODEST_1_65.enabled = i_bVal
      .Page1.oPag.oANCODCLA_1_66.enabled = i_bVal
      .Page1.oPag.oANCODPEC_1_67.enabled = i_bVal
      .Page1.oPag.oANDTOBSO_1_68.enabled = i_bVal
      .Page1.oPag.oANDTINVA_1_73.enabled = i_bVal
      .Page2.oPag.oANCONSUP_2_5.enabled = i_bVal
      .Page2.oPag.oANCONRIF_2_8.enabled = i_bVal
      .Page2.oPag.oANCATCON_2_10.enabled = i_bVal
      .Page2.oPag.oANCODIVA_2_12.enabled = i_bVal
      .Page2.oPag.oANTIPOPE_2_14.enabled = i_bVal
      .Page2.oPag.oANCONCAU_2_16.enabled = i_bVal
      .Page2.oPag.oANOPETRE_2_17.enabled = i_bVal
      .Page2.oPag.oANTIPPRE_2_18.enabled = i_bVal
      .Page2.oPag.oANPARTSN_2_23.enabled = i_bVal
      .Page2.oPag.oANFLAACC_2_24.enabled = i_bVal
      .Page2.oPag.oAFFLINTR_2_26.enabled = i_bVal
      .Page2.oPag.oANFLBLLS_2_27.enabled = i_bVal
      .Page2.oPag.oANFLSOAL_2_28.enabled = i_bVal
      .Page2.oPag.oANFLBODO_2_29.enabled = i_bVal
      .Page2.oPag.oANIVASOS_2_30.enabled = i_bVal
      .Page2.oPag.oANCODSTU_2_31.enabled = i_bVal
      .Page2.oPag.oANCODSOG_2_33.enabled = i_bVal
      .Page2.oPag.oANCODCAT_2_34.enabled = i_bVal
      .Page8.oPag.oANCODGRU_8_4.enabled = i_bVal
      .Page5.oPag.oANCODPAG_5_4.enabled = i_bVal
      .Page5.oPag.oANGIOFIS_5_6.enabled = i_bVal
      .Page5.oPag.oAN1MESCL_5_7.enabled = i_bVal
      .Page5.oPag.oANGIOSC1_5_9.enabled = i_bVal
      .Page5.oPag.oAN2MESCL_5_10.enabled = i_bVal
      .Page5.oPag.oANGIOSC2_5_14.enabled = i_bVal
      .Page5.oPag.oANCODBAN_5_15.enabled = i_bVal
      .Page5.oPag.oANCODBA2_5_17.enabled = i_bVal
      .Page5.oPag.oANFLINCA_5_18.enabled = i_bVal
      .Page5.oPag.oANSPEINC_5_19.enabled = i_bVal
      .Page5.oPag.oANFLESIM_5_21.enabled = i_bVal
      .Page5.oPag.oANSAGINT_5_22.enabled = i_bVal
      .Page5.oPag.oANSPRINT_5_23.enabled = i_bVal
      .Page5.oPag.oANFLACBD_5_24.enabled = i_bVal
      .Page5.oPag.oANFLRAGG_5_25.enabled = i_bVal
      .Page5.oPag.oANFLGAVV_5_26.enabled = i_bVal
      .Page5.oPag.oANDATAVV_5_27.enabled = i_bVal
      .Page4.oPag.oANCATCOM_4_12.enabled = i_bVal
      .Page4.oPag.oANGRPDEF_4_13.enabled = i_bVal
      .Page4.oPag.oANCATSCM_4_15.enabled = i_bVal
      .Page4.oPag.oANCODLIN_4_17.enabled = i_bVal
      .Page4.oPag.oANCODVAL_4_19.enabled = i_bVal
      .Page4.oPag.oANNUMLIS_4_21.enabled = i_bVal
      .Page4.oPag.oAN1SCONT_4_23.enabled = i_bVal
      .Page4.oPag.oAN2SCONT_4_24.enabled = i_bVal
      .Page4.oPag.oANCODZON_4_25.enabled = i_bVal
      .Page4.oPag.oANCONCON_4_26.enabled = i_bVal
      .Page4.oPag.oANMAGTER_4_38.enabled = i_bVal
      .Page4.oPag.oANCODESC_4_39.enabled = i_bVal
      .Page4.oPag.oANCODPOR_4_40.enabled = i_bVal
      .Page4.oPag.oANMCALSI_4_44.enabled = i_bVal
      .Page4.oPag.oANMCALST_4_45.enabled = i_bVal
      .Page4.oPag.oANSCORPO_4_47.enabled = i_bVal
      .Page4.oPag.oANFLCODI_4_49.enabled = i_bVal
      .Page4.oPag.oANFLIMBA_4_50.enabled = i_bVal
      .Page4.oPag.oANFLGCPZ_4_51.enabled = i_bVal
      .Page4.oPag.oANFLGCAU_4_65.enabled = i_bVal
      .Page4.oPag.oANFLAPCA_4_70.enabled = i_bVal
      .Page3.oPag.oANRITENU_3_31.enabled = i_bVal
      .Page3.oPag.oANCODIRP_3_32.enabled = i_bVal
      .Page3.oPag.oANCODTR2_3_33.enabled = i_bVal
      .Page3.oPag.oANPEINPS_3_34.enabled = i_bVal
      .Page3.oPag.oANRIINPS_3_35.enabled = i_bVal
      .Page3.oPag.oANCOINPS_3_36.enabled = i_bVal
      .Page3.oPag.oANCASPRO_3_37.enabled = i_bVal
      .Page3.oPag.oANCODATT_3_38.enabled = i_bVal
      .Page3.oPag.oANCODASS_3_39.enabled = i_bVal
      .Page3.oPag.oANCOIMPS_3_40.enabled = i_bVal
      .Page10.oPag.oAN__NOTE_10_4.enabled = i_bVal
      .Page9.oPag.oANRATING_9_7.enabled = i_bVal
      .Page9.oPag.oANGIORIT_9_8.enabled = i_bVal
      .Page9.oPag.oANVOCFIN_9_9.enabled = i_bVal
      .Page9.oPag.oANESCDOF_9_11.enabled = i_bVal
      .Page9.oPag.oANDESPAR_9_12.enabled = i_bVal
      .Page3.oPag.oANFLGEST_3_42.enabled = i_bVal
      .Page3.oPag.oANFLSGRE_3_45.enabled = i_bVal
      .Page3.oPag.oANCOFISC_3_47.enabled = i_bVal
      .Page3.oPag.oANSCHUMA_3_48.enabled = i_bVal
      .Page3.oPag.oAN_EREDE_3_50.enabled = i_bVal
      .Page3.oPag.oANCODSNS_3_51.enabled = i_bVal
      .Page3.oPag.oANCODREG_3_53.enabled = i_bVal
      .Page3.oPag.oANCODCOM_3_54.enabled = i_bVal
      .Page3.oPag.oANCAURIT_3_55.enabled = i_bVal
      .Page3.oPag.oANEVEECC_3_56.enabled = i_bVal
      .Page3.oPag.oANCATPAR_3_57.enabled = i_bVal
      .Page1.oPag.oBtn_1_78.enabled = .Page1.oPag.oBtn_1_78.mCond()
      .Page1.oPag.oBtn_1_79.enabled = .Page1.oPag.oBtn_1_79.mCond()
      .Page1.oPag.oBtn_1_80.enabled = .Page1.oPag.oBtn_1_80.mCond()
      .Page1.oPag.oBtn_1_82.enabled = .Page1.oPag.oBtn_1_82.mCond()
      .Page1.oPag.oBtn_1_88.enabled = .Page1.oPag.oBtn_1_88.mCond()
      .Page1.oPag.oBtn_1_89.enabled = .Page1.oPag.oBtn_1_89.mCond()
      .Page1.oPag.oBtn_1_94.enabled = i_bVal
      .Page2.oPag.oBtn_2_32.enabled = i_bVal
      .Page2.oPag.oBtn_2_51.enabled = .Page2.oPag.oBtn_2_51.mCond()
      .Page2.oPag.oBtn_2_52.enabled = .Page2.oPag.oBtn_2_52.mCond()
      .Page2.oPag.oBtn_2_53.enabled = .Page2.oPag.oBtn_2_53.mCond()
      .Page1.oPag.oBtn_1_108.enabled = .Page1.oPag.oBtn_1_108.mCond()
      .Page5.oPag.oBtn_5_11.enabled = .Page5.oPag.oBtn_5_11.mCond()
      .Page5.oPag.oBtn_5_12.enabled = .Page5.oPag.oBtn_5_12.mCond()
      .Page4.oPag.oBtn_4_54.enabled = .Page4.oPag.oBtn_4_54.mCond()
      .Page4.oPag.oBtn_4_55.enabled = .Page4.oPag.oBtn_4_55.mCond()
      .Page4.oPag.oBtn_4_56.enabled = .Page4.oPag.oBtn_4_56.mCond()
      .Page4.oPag.oBtn_4_57.enabled = .Page4.oPag.oBtn_4_57.mCond()
      .Page1.oPag.oBtn_1_112.enabled = .Page1.oPag.oBtn_1_112.mCond()
      .Page2.oPag.oBtn_2_79.enabled = i_bVal
      .Page1.oPag.oObj_1_75.enabled = i_bVal
      .Page1.oPag.oObj_1_76.enabled = i_bVal
      .Page1.oPag.oObj_1_77.enabled = i_bVal
      .Page1.oPag.oObj_1_86.enabled = i_bVal
      .Page1.oPag.oObj_1_87.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oANCODICE_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oANCODICE_1_4.enabled = .t.
        .Page1.oPag.oANDESCRI_1_5.enabled = .t.
        .Page1.oPag.oANPARIVA_1_19.enabled = .t.
      endif
    endwith
    this.GSAR_MSA.SetStatus(i_cOp)
    this.GSAR_MSE.SetStatus(i_cOp)
    this.GSAR_MDD.SetStatus(i_cOp)
    this.GSAR_MCO.SetStatus(i_cOp)
    this.GSAR_MCC.SetStatus(i_cOp)
    this.GSAR_MIN.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CONTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MSA.SetChildrenStatus(i_cOp)
  *  this.GSAR_MSE.SetChildrenStatus(i_cOp)
  *  this.GSAR_MDD.SetChildrenStatus(i_cOp)
  *  this.GSAR_MCO.SetChildrenStatus(i_cOp)
  *  this.GSAR_MCC.SetChildrenStatus(i_cOp)
  *  this.GSAR_MIN.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPCON,"ANTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODICE,"ANCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCRI,"ANDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPCLF,"ANTIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCR2,"ANDESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANINDIRI,"ANINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANINDIR2,"ANINDIR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN___CAP,"AN___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANLOCALI,"ANLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPROVIN,"ANPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNAZION,"ANNAZION",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODFIS,"ANCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPARIVA,"ANPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTELEFO,"ANTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTELFAX,"ANTELFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNUMCEL,"ANNUMCEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANINDWEB,"ANINDWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN_EMAIL,"AN_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN_EMPEC,"AN_EMPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPERFIS,"ANPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN_SESSO,"AN_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCOGNOM,"ANCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN__NOME,"AN__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANLOCNAS,"ANLOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPRONAS,"ANPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDATNAS,"ANDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNUMCAR,"ANNUMCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKSTA,"ANCHKSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKMAI,"ANCHKMAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKPEC,"ANCHKPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKFAX,"ANCHKFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKCPZ,"ANCHKCPZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKWWP,"ANCHKWWP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKPTL,"ANCHKPTL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKFIR,"ANCHKFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODEST,"ANCODEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODCLA,"ANCODCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODPEC,"ANCODPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDTOBSO,"ANDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDTINVA,"ANDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPSOT,"ANTIPSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCONSUP,"ANCONSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCONRIF,"ANCONRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCATCON,"ANCATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODIVA,"ANCODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPOPE,"ANTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCONCAU,"ANCONCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANOPETRE,"ANOPETRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPPRE,"ANTIPPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPARTSN,"ANPARTSN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLAACC,"ANFLAACC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AFFLINTR,"AFFLINTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLBLLS,"ANFLBLLS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLSOAL,"ANFLSOAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLBODO,"ANFLBODO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANIVASOS,"ANIVASOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODSTU,"ANCODSTU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODSOG,"ANCODSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODCAT,"ANCODCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPRIF,"ANTIPRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLSGRE,"ANFLSGRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCATOPE,"ANCATOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODCUC,"ANCODCUC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNUMCOR,"ANNUMCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCINABI,"ANCINABI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN__BBAN,"AN__BBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN__IBAN,"AN__IBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCR3,"ANDESCR3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCR4,"ANDESCR4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANINDIR3,"ANINDIR3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANINDIR4,"ANINDIR4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANLOCAL2,"ANLOCAL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODGRU,"ANCODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODPAG,"ANCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGIOFIS,"ANGIOFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN1MESCL,"AN1MESCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGIOSC1,"ANGIOSC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN2MESCL,"AN2MESCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGIOSC2,"ANGIOSC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODBAN,"ANCODBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODBA2,"ANCODBA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLINCA,"ANFLINCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANSPEINC,"ANSPEINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLESIM,"ANFLESIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANSAGINT,"ANSAGINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANSPRINT,"ANSPRINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLACBD,"ANFLACBD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLRAGG,"ANFLRAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLGAVV,"ANFLGAVV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDATAVV,"ANDATAVV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCATCOM,"ANCATCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGRPDEF,"ANGRPDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCATSCM,"ANCATSCM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODLIN,"ANCODLIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODVAL,"ANCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNUMLIS,"ANNUMLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN1SCONT,"AN1SCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN2SCONT,"AN2SCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODZON,"ANCODZON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCONCON,"ANCONCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMAGTER,"ANMAGTER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODESC,"ANCODESC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODPOR,"ANCODPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMCALSI,"ANMCALSI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMCALST,"ANMCALST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANSCORPO,"ANSCORPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLCODI,"ANFLCODI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLIMBA,"ANFLIMBA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLGCPZ,"ANFLGCPZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLGCAU,"ANFLGCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLAPCA,"ANFLAPCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANRITENU,"ANRITENU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODIRP,"ANCODIRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODTR2,"ANCODTR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPEINPS,"ANPEINPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANRIINPS,"ANRIINPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCOINPS,"ANCOINPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCASPRO,"ANCASPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODATT,"ANCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODASS,"ANCODASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCOIMPS,"ANCOIMPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN__NOTE,"AN__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANRATING,"ANRATING",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGIORIT,"ANGIORIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANVOCFIN,"ANVOCFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANESCDOF,"ANESCDOF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESPAR,"ANDESPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLGEST,"ANFLGEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCRI,"ANDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLSGRE,"ANFLSGRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCR2,"ANDESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCOFISC,"ANCOFISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANSCHUMA,"ANSCHUMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN_EREDE,"AN_EREDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODSNS,"ANCODSNS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODREG,"ANCODREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODCOM,"ANCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCAURIT,"ANCAURIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANEVEECC,"ANEVEECC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCATPAR,"ANCATPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLESIG,"ANFLESIG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsar_afr
    * --- aggiunge alla Chiave ulteriore filtro su Tipo Conto
    IF NOT EMPTY(i_cWhere)
       IF AT('ANTIPSOT', UPPER(i_cWhere))=0
          i_cWhere=i_cWhere+" and ANTIPCON='F'"
       ENDIF
    ENDIF
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    i_lTable = "CONTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CONTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        PrintConto(this,"F")
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gsar_afr
    ** Previene eventuale inserimento di Codici non numerici dal calcolo Autonumber
    local p_POS, p_COD
    p_COD = ALLTRIM(this.w_ANCODICE)
    FOR p_POS=1 TO LEN(p_COD)
         this.p_AUT = IIF(SUBSTR(p_COD, p_POS, 1)$'0123456789', this.p_AUT, 1)
    ENDFOR
    
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTI_IDX,i_nConn)
      with this
        if g_CFNUME='S' AND .p_AUT=0
          cp_NextTableProg(this,i_nConn,"PRNUFO","i_codazi,w_ANCODICE")
        endif
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTI')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ANTIPCON,ANCODICE,ANDESCRI,ANTIPCLF,ANDESCR2"+;
                  ",ANINDIRI,ANINDIR2,AN___CAP,ANLOCALI,ANPROVIN"+;
                  ",ANNAZION,ANCODFIS,ANPARIVA,ANTELEFO,ANTELFAX"+;
                  ",ANNUMCEL,ANINDWEB,AN_EMAIL,AN_EMPEC,ANPERFIS"+;
                  ",AN_SESSO,ANCOGNOM,AN__NOME,ANLOCNAS,ANPRONAS"+;
                  ",ANDATNAS,ANNUMCAR,UTCC,UTCV,UTDC"+;
                  ",UTDV,ANCHKSTA,ANCHKMAI,ANCHKPEC,ANCHKFAX"+;
                  ",ANCHKCPZ,ANCHKWWP,ANCHKPTL,ANCHKFIR,ANCODEST"+;
                  ",ANCODCLA,ANCODPEC,ANDTOBSO,ANDTINVA,ANTIPSOT"+;
                  ",ANCONSUP,ANCONRIF,ANCATCON,ANCODIVA,ANTIPOPE"+;
                  ",ANCONCAU,ANOPETRE,ANTIPPRE,ANPARTSN,ANFLAACC"+;
                  ",AFFLINTR,ANFLBLLS,ANFLSOAL,ANFLBODO,ANIVASOS"+;
                  ",ANCODSTU,ANCODSOG,ANCODCAT,ANTIPRIF,ANFLSGRE"+;
                  ",ANCATOPE,ANCODCUC,ANNUMCOR,ANCINABI,AN__BBAN"+;
                  ",AN__IBAN,ANDESCR3,ANDESCR4,ANINDIR3,ANINDIR4"+;
                  ",ANLOCAL2,ANCODGRU,ANCODPAG,ANGIOFIS,AN1MESCL"+;
                  ",ANGIOSC1,AN2MESCL,ANGIOSC2,ANCODBAN,ANCODBA2"+;
                  ",ANFLINCA,ANSPEINC,ANFLESIM,ANSAGINT,ANSPRINT"+;
                  ",ANFLACBD,ANFLRAGG,ANFLGAVV,ANDATAVV,ANCATCOM"+;
                  ",ANGRPDEF,ANCATSCM,ANCODLIN,ANCODVAL,ANNUMLIS"+;
                  ",AN1SCONT,AN2SCONT,ANCODZON,ANCONCON,ANMAGTER"+;
                  ",ANCODESC,ANCODPOR,ANMCALSI,ANMCALST,ANSCORPO"+;
                  ",ANFLCODI,ANFLIMBA,ANFLGCPZ,ANFLGCAU,ANFLAPCA"+;
                  ",ANRITENU,ANCODIRP,ANCODTR2,ANPEINPS,ANRIINPS"+;
                  ",ANCOINPS,ANCASPRO,ANCODATT,ANCODASS,ANCOIMPS"+;
                  ",AN__NOTE,ANRATING,ANGIORIT,ANVOCFIN,ANESCDOF"+;
                  ",ANDESPAR,ANFLGEST,ANCOFISC,ANSCHUMA,AN_EREDE"+;
                  ",ANCODSNS,ANCODREG,ANCODCOM,ANCAURIT,ANEVEECC"+;
                  ",ANCATPAR,ANFLESIG "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ANTIPCON)+;
                  ","+cp_ToStrODBC(this.w_ANCODICE)+;
                  ","+cp_ToStrODBC(this.w_ANDESCRI)+;
                  ","+cp_ToStrODBC(this.w_ANTIPCLF)+;
                  ","+cp_ToStrODBC(this.w_ANDESCR2)+;
                  ","+cp_ToStrODBC(this.w_ANINDIRI)+;
                  ","+cp_ToStrODBC(this.w_ANINDIR2)+;
                  ","+cp_ToStrODBC(this.w_AN___CAP)+;
                  ","+cp_ToStrODBC(this.w_ANLOCALI)+;
                  ","+cp_ToStrODBC(this.w_ANPROVIN)+;
                  ","+cp_ToStrODBCNull(this.w_ANNAZION)+;
                  ","+cp_ToStrODBC(this.w_ANCODFIS)+;
                  ","+cp_ToStrODBC(this.w_ANPARIVA)+;
                  ","+cp_ToStrODBC(this.w_ANTELEFO)+;
                  ","+cp_ToStrODBC(this.w_ANTELFAX)+;
                  ","+cp_ToStrODBC(this.w_ANNUMCEL)+;
                  ","+cp_ToStrODBC(this.w_ANINDWEB)+;
                  ","+cp_ToStrODBC(this.w_AN_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_AN_EMPEC)+;
                  ","+cp_ToStrODBC(this.w_ANPERFIS)+;
                  ","+cp_ToStrODBC(this.w_AN_SESSO)+;
                  ","+cp_ToStrODBC(this.w_ANCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_AN__NOME)+;
                  ","+cp_ToStrODBC(this.w_ANLOCNAS)+;
                  ","+cp_ToStrODBC(this.w_ANPRONAS)+;
                  ","+cp_ToStrODBC(this.w_ANDATNAS)+;
                  ","+cp_ToStrODBC(this.w_ANNUMCAR)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_ANCHKSTA)+;
                  ","+cp_ToStrODBC(this.w_ANCHKMAI)+;
                  ","+cp_ToStrODBC(this.w_ANCHKPEC)+;
                  ","+cp_ToStrODBC(this.w_ANCHKFAX)+;
                  ","+cp_ToStrODBC(this.w_ANCHKCPZ)+;
                  ","+cp_ToStrODBC(this.w_ANCHKWWP)+;
                  ","+cp_ToStrODBC(this.w_ANCHKPTL)+;
                  ","+cp_ToStrODBC(this.w_ANCHKFIR)+;
                  ","+cp_ToStrODBC(this.w_ANCODEST)+;
                  ","+cp_ToStrODBC(this.w_ANCODCLA)+;
                  ","+cp_ToStrODBC(this.w_ANCODPEC)+;
                  ","+cp_ToStrODBC(this.w_ANDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_ANDTINVA)+;
                  ","+cp_ToStrODBC(this.w_ANTIPSOT)+;
                  ","+cp_ToStrODBCNull(this.w_ANCONSUP)+;
                  ","+cp_ToStrODBCNull(this.w_ANCONRIF)+;
                  ","+cp_ToStrODBCNull(this.w_ANCATCON)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODIVA)+;
                  ","+cp_ToStrODBCNull(this.w_ANTIPOPE)+;
                  ","+cp_ToStrODBCNull(this.w_ANCONCAU)+;
                  ","+cp_ToStrODBC(this.w_ANOPETRE)+;
                  ","+cp_ToStrODBC(this.w_ANTIPPRE)+;
                  ","+cp_ToStrODBC(this.w_ANPARTSN)+;
                  ","+cp_ToStrODBC(this.w_ANFLAACC)+;
                  ","+cp_ToStrODBC(this.w_AFFLINTR)+;
                  ","+cp_ToStrODBC(this.w_ANFLBLLS)+;
                  ","+cp_ToStrODBC(this.w_ANFLSOAL)+;
                  ","+cp_ToStrODBC(this.w_ANFLBODO)+;
                  ","+cp_ToStrODBC(this.w_ANIVASOS)+;
                  ","+cp_ToStrODBC(this.w_ANCODSTU)+;
                  ","+cp_ToStrODBC(this.w_ANCODSOG)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODCAT)+;
                  ","+cp_ToStrODBC(this.w_ANTIPRIF)+;
                  ","+cp_ToStrODBC(this.w_ANFLSGRE)+;
                  ","+cp_ToStrODBC(this.w_ANCATOPE)+;
                  ","+cp_ToStrODBC(this.w_ANCODCUC)+;
                  ","+cp_ToStrODBC(this.w_ANNUMCOR)+;
                  ","+cp_ToStrODBC(this.w_ANCINABI)+;
                  ","+cp_ToStrODBC(this.w_AN__BBAN)+;
                  ","+cp_ToStrODBC(this.w_AN__IBAN)+;
                  ","+cp_ToStrODBC(this.w_ANDESCR3)+;
                  ","+cp_ToStrODBC(this.w_ANDESCR4)+;
                  ","+cp_ToStrODBC(this.w_ANINDIR3)+;
                  ","+cp_ToStrODBC(this.w_ANINDIR4)+;
                  ","+cp_ToStrODBC(this.w_ANLOCAL2)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODGRU)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODPAG)+;
                  ","+cp_ToStrODBC(this.w_ANGIOFIS)+;
                  ","+cp_ToStrODBC(this.w_AN1MESCL)+;
                  ","+cp_ToStrODBC(this.w_ANGIOSC1)+;
                  ","+cp_ToStrODBC(this.w_AN2MESCL)+;
                  ","+cp_ToStrODBC(this.w_ANGIOSC2)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODBAN)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODBA2)+;
                  ","+cp_ToStrODBC(this.w_ANFLINCA)+;
                  ","+cp_ToStrODBC(this.w_ANSPEINC)+;
                  ","+cp_ToStrODBC(this.w_ANFLESIM)+;
                  ","+cp_ToStrODBC(this.w_ANSAGINT)+;
                  ","+cp_ToStrODBC(this.w_ANSPRINT)+;
                  ","+cp_ToStrODBC(this.w_ANFLACBD)+;
                  ","+cp_ToStrODBC(this.w_ANFLRAGG)+;
                  ","+cp_ToStrODBC(this.w_ANFLGAVV)+;
                  ","+cp_ToStrODBC(this.w_ANDATAVV)+;
                  ","+cp_ToStrODBCNull(this.w_ANCATCOM)+;
                  ","+cp_ToStrODBCNull(this.w_ANGRPDEF)+;
                  ","+cp_ToStrODBCNull(this.w_ANCATSCM)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODLIN)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODVAL)+;
                  ","+cp_ToStrODBCNull(this.w_ANNUMLIS)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_AN1SCONT)+;
                  ","+cp_ToStrODBC(this.w_AN2SCONT)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODZON)+;
                  ","+cp_ToStrODBCNull(this.w_ANCONCON)+;
                  ","+cp_ToStrODBCNull(this.w_ANMAGTER)+;
                  ","+cp_ToStrODBC(this.w_ANCODESC)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODPOR)+;
                  ","+cp_ToStrODBCNull(this.w_ANMCALSI)+;
                  ","+cp_ToStrODBCNull(this.w_ANMCALST)+;
                  ","+cp_ToStrODBC(this.w_ANSCORPO)+;
                  ","+cp_ToStrODBC(this.w_ANFLCODI)+;
                  ","+cp_ToStrODBC(this.w_ANFLIMBA)+;
                  ","+cp_ToStrODBC(this.w_ANFLGCPZ)+;
                  ","+cp_ToStrODBC(this.w_ANFLGCAU)+;
                  ","+cp_ToStrODBC(this.w_ANFLAPCA)+;
                  ","+cp_ToStrODBC(this.w_ANRITENU)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODIRP)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODTR2)+;
                  ","+cp_ToStrODBC(this.w_ANPEINPS)+;
                  ","+cp_ToStrODBC(this.w_ANRIINPS)+;
                  ","+cp_ToStrODBC(this.w_ANCOINPS)+;
                  ","+cp_ToStrODBC(this.w_ANCASPRO)+;
                  ","+cp_ToStrODBC(this.w_ANCODATT)+;
                  ","+cp_ToStrODBC(this.w_ANCODASS)+;
                  ","+cp_ToStrODBC(this.w_ANCOIMPS)+;
                  ","+cp_ToStrODBC(this.w_AN__NOTE)+;
                  ","+cp_ToStrODBCNull(this.w_ANRATING)+;
                  ","+cp_ToStrODBC(this.w_ANGIORIT)+;
                  ","+cp_ToStrODBCNull(this.w_ANVOCFIN)+;
                  ","+cp_ToStrODBC(this.w_ANESCDOF)+;
                  ","+cp_ToStrODBC(this.w_ANDESPAR)+;
                  ","+cp_ToStrODBC(this.w_ANFLGEST)+;
                  ","+cp_ToStrODBC(this.w_ANCOFISC)+;
                  ","+cp_ToStrODBC(this.w_ANSCHUMA)+;
                  ","+cp_ToStrODBC(this.w_AN_EREDE)+;
                  ","+cp_ToStrODBC(this.w_ANCODSNS)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODREG)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODCOM)+;
                  ","+cp_ToStrODBC(this.w_ANCAURIT)+;
                  ","+cp_ToStrODBC(this.w_ANEVEECC)+;
                  ","+cp_ToStrODBC(this.w_ANCATPAR)+;
                  ","+cp_ToStrODBC(this.w_ANFLESIG)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTI')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTI')
        cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',this.w_ANTIPCON,'ANCODICE',this.w_ANCODICE)
        INSERT INTO (i_cTable);
              (ANTIPCON,ANCODICE,ANDESCRI,ANTIPCLF,ANDESCR2,ANINDIRI,ANINDIR2,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION,ANCODFIS,ANPARIVA,ANTELEFO,ANTELFAX,ANNUMCEL,ANINDWEB,AN_EMAIL,AN_EMPEC,ANPERFIS,AN_SESSO,ANCOGNOM,AN__NOME,ANLOCNAS,ANPRONAS,ANDATNAS,ANNUMCAR,UTCC,UTCV,UTDC,UTDV,ANCHKSTA,ANCHKMAI,ANCHKPEC,ANCHKFAX,ANCHKCPZ,ANCHKWWP,ANCHKPTL,ANCHKFIR,ANCODEST,ANCODCLA,ANCODPEC,ANDTOBSO,ANDTINVA,ANTIPSOT,ANCONSUP,ANCONRIF,ANCATCON,ANCODIVA,ANTIPOPE,ANCONCAU,ANOPETRE,ANTIPPRE,ANPARTSN,ANFLAACC,AFFLINTR,ANFLBLLS,ANFLSOAL,ANFLBODO,ANIVASOS,ANCODSTU,ANCODSOG,ANCODCAT,ANTIPRIF,ANFLSGRE,ANCATOPE,ANCODCUC,ANNUMCOR,ANCINABI,AN__BBAN,AN__IBAN,ANDESCR3,ANDESCR4,ANINDIR3,ANINDIR4,ANLOCAL2,ANCODGRU,ANCODPAG,ANGIOFIS,AN1MESCL,ANGIOSC1,AN2MESCL,ANGIOSC2,ANCODBAN,ANCODBA2,ANFLINCA,ANSPEINC,ANFLESIM,ANSAGINT,ANSPRINT,ANFLACBD,ANFLRAGG,ANFLGAVV,ANDATAVV,ANCATCOM,ANGRPDEF,ANCATSCM,ANCODLIN,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCODZON,ANCONCON,ANMAGTER,ANCODESC,ANCODPOR,ANMCALSI,ANMCALST,ANSCORPO,ANFLCODI,ANFLIMBA,ANFLGCPZ,ANFLGCAU,ANFLAPCA,ANRITENU,ANCODIRP,ANCODTR2,ANPEINPS,ANRIINPS,ANCOINPS,ANCASPRO,ANCODATT,ANCODASS,ANCOIMPS,AN__NOTE,ANRATING,ANGIORIT,ANVOCFIN,ANESCDOF,ANDESPAR,ANFLGEST,ANCOFISC,ANSCHUMA,AN_EREDE,ANCODSNS,ANCODREG,ANCODCOM,ANCAURIT,ANEVEECC,ANCATPAR,ANFLESIG  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ANTIPCON;
                  ,this.w_ANCODICE;
                  ,this.w_ANDESCRI;
                  ,this.w_ANTIPCLF;
                  ,this.w_ANDESCR2;
                  ,this.w_ANINDIRI;
                  ,this.w_ANINDIR2;
                  ,this.w_AN___CAP;
                  ,this.w_ANLOCALI;
                  ,this.w_ANPROVIN;
                  ,this.w_ANNAZION;
                  ,this.w_ANCODFIS;
                  ,this.w_ANPARIVA;
                  ,this.w_ANTELEFO;
                  ,this.w_ANTELFAX;
                  ,this.w_ANNUMCEL;
                  ,this.w_ANINDWEB;
                  ,this.w_AN_EMAIL;
                  ,this.w_AN_EMPEC;
                  ,this.w_ANPERFIS;
                  ,this.w_AN_SESSO;
                  ,this.w_ANCOGNOM;
                  ,this.w_AN__NOME;
                  ,this.w_ANLOCNAS;
                  ,this.w_ANPRONAS;
                  ,this.w_ANDATNAS;
                  ,this.w_ANNUMCAR;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_ANCHKSTA;
                  ,this.w_ANCHKMAI;
                  ,this.w_ANCHKPEC;
                  ,this.w_ANCHKFAX;
                  ,this.w_ANCHKCPZ;
                  ,this.w_ANCHKWWP;
                  ,this.w_ANCHKPTL;
                  ,this.w_ANCHKFIR;
                  ,this.w_ANCODEST;
                  ,this.w_ANCODCLA;
                  ,this.w_ANCODPEC;
                  ,this.w_ANDTOBSO;
                  ,this.w_ANDTINVA;
                  ,this.w_ANTIPSOT;
                  ,this.w_ANCONSUP;
                  ,this.w_ANCONRIF;
                  ,this.w_ANCATCON;
                  ,this.w_ANCODIVA;
                  ,this.w_ANTIPOPE;
                  ,this.w_ANCONCAU;
                  ,this.w_ANOPETRE;
                  ,this.w_ANTIPPRE;
                  ,this.w_ANPARTSN;
                  ,this.w_ANFLAACC;
                  ,this.w_AFFLINTR;
                  ,this.w_ANFLBLLS;
                  ,this.w_ANFLSOAL;
                  ,this.w_ANFLBODO;
                  ,this.w_ANIVASOS;
                  ,this.w_ANCODSTU;
                  ,this.w_ANCODSOG;
                  ,this.w_ANCODCAT;
                  ,this.w_ANTIPRIF;
                  ,this.w_ANFLSGRE;
                  ,this.w_ANCATOPE;
                  ,this.w_ANCODCUC;
                  ,this.w_ANNUMCOR;
                  ,this.w_ANCINABI;
                  ,this.w_AN__BBAN;
                  ,this.w_AN__IBAN;
                  ,this.w_ANDESCR3;
                  ,this.w_ANDESCR4;
                  ,this.w_ANINDIR3;
                  ,this.w_ANINDIR4;
                  ,this.w_ANLOCAL2;
                  ,this.w_ANCODGRU;
                  ,this.w_ANCODPAG;
                  ,this.w_ANGIOFIS;
                  ,this.w_AN1MESCL;
                  ,this.w_ANGIOSC1;
                  ,this.w_AN2MESCL;
                  ,this.w_ANGIOSC2;
                  ,this.w_ANCODBAN;
                  ,this.w_ANCODBA2;
                  ,this.w_ANFLINCA;
                  ,this.w_ANSPEINC;
                  ,this.w_ANFLESIM;
                  ,this.w_ANSAGINT;
                  ,this.w_ANSPRINT;
                  ,this.w_ANFLACBD;
                  ,this.w_ANFLRAGG;
                  ,this.w_ANFLGAVV;
                  ,this.w_ANDATAVV;
                  ,this.w_ANCATCOM;
                  ,this.w_ANGRPDEF;
                  ,this.w_ANCATSCM;
                  ,this.w_ANCODLIN;
                  ,this.w_ANCODVAL;
                  ,this.w_ANNUMLIS;
                  ,this.w_AN1SCONT;
                  ,this.w_AN2SCONT;
                  ,this.w_ANCODZON;
                  ,this.w_ANCONCON;
                  ,this.w_ANMAGTER;
                  ,this.w_ANCODESC;
                  ,this.w_ANCODPOR;
                  ,this.w_ANMCALSI;
                  ,this.w_ANMCALST;
                  ,this.w_ANSCORPO;
                  ,this.w_ANFLCODI;
                  ,this.w_ANFLIMBA;
                  ,this.w_ANFLGCPZ;
                  ,this.w_ANFLGCAU;
                  ,this.w_ANFLAPCA;
                  ,this.w_ANRITENU;
                  ,this.w_ANCODIRP;
                  ,this.w_ANCODTR2;
                  ,this.w_ANPEINPS;
                  ,this.w_ANRIINPS;
                  ,this.w_ANCOINPS;
                  ,this.w_ANCASPRO;
                  ,this.w_ANCODATT;
                  ,this.w_ANCODASS;
                  ,this.w_ANCOIMPS;
                  ,this.w_AN__NOTE;
                  ,this.w_ANRATING;
                  ,this.w_ANGIORIT;
                  ,this.w_ANVOCFIN;
                  ,this.w_ANESCDOF;
                  ,this.w_ANDESPAR;
                  ,this.w_ANFLGEST;
                  ,this.w_ANCOFISC;
                  ,this.w_ANSCHUMA;
                  ,this.w_AN_EREDE;
                  ,this.w_ANCODSNS;
                  ,this.w_ANCODREG;
                  ,this.w_ANCODCOM;
                  ,this.w_ANCAURIT;
                  ,this.w_ANEVEECC;
                  ,this.w_ANCATPAR;
                  ,this.w_ANFLESIG;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- gsar_afr
    **Controllo che altri utenti non abbiano lo stesso codice Studio 
    w_MESS=''
    IF NOT CHK_STUDIO(this.w_ANTIPCON,this.w_ANCODICE,this.w_ANCODSTU,.T.) 
    local objcod
    this.getctrl ("w_ANCODICE")
    objcod.value=this.w_ANCODICE
    objcod.valid()
    this.mcalc(.T.)
    bTrsErr=.T.
    i_TrsMsg=w_MESS
    ENDIF
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTI_IDX,i_nConn)
      *
      * update CONTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ANDESCRI="+cp_ToStrODBC(this.w_ANDESCRI)+;
             ",ANTIPCLF="+cp_ToStrODBC(this.w_ANTIPCLF)+;
             ",ANDESCR2="+cp_ToStrODBC(this.w_ANDESCR2)+;
             ",ANINDIRI="+cp_ToStrODBC(this.w_ANINDIRI)+;
             ",ANINDIR2="+cp_ToStrODBC(this.w_ANINDIR2)+;
             ",AN___CAP="+cp_ToStrODBC(this.w_AN___CAP)+;
             ",ANLOCALI="+cp_ToStrODBC(this.w_ANLOCALI)+;
             ",ANPROVIN="+cp_ToStrODBC(this.w_ANPROVIN)+;
             ",ANNAZION="+cp_ToStrODBCNull(this.w_ANNAZION)+;
             ",ANCODFIS="+cp_ToStrODBC(this.w_ANCODFIS)+;
             ",ANPARIVA="+cp_ToStrODBC(this.w_ANPARIVA)+;
             ",ANTELEFO="+cp_ToStrODBC(this.w_ANTELEFO)+;
             ",ANTELFAX="+cp_ToStrODBC(this.w_ANTELFAX)+;
             ",ANNUMCEL="+cp_ToStrODBC(this.w_ANNUMCEL)+;
             ",ANINDWEB="+cp_ToStrODBC(this.w_ANINDWEB)+;
             ",AN_EMAIL="+cp_ToStrODBC(this.w_AN_EMAIL)+;
             ",AN_EMPEC="+cp_ToStrODBC(this.w_AN_EMPEC)+;
             ",ANPERFIS="+cp_ToStrODBC(this.w_ANPERFIS)+;
             ",AN_SESSO="+cp_ToStrODBC(this.w_AN_SESSO)+;
             ",ANCOGNOM="+cp_ToStrODBC(this.w_ANCOGNOM)+;
             ",AN__NOME="+cp_ToStrODBC(this.w_AN__NOME)+;
             ",ANLOCNAS="+cp_ToStrODBC(this.w_ANLOCNAS)+;
             ",ANPRONAS="+cp_ToStrODBC(this.w_ANPRONAS)+;
             ",ANDATNAS="+cp_ToStrODBC(this.w_ANDATNAS)+;
             ",ANNUMCAR="+cp_ToStrODBC(this.w_ANNUMCAR)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",ANCHKSTA="+cp_ToStrODBC(this.w_ANCHKSTA)+;
             ",ANCHKMAI="+cp_ToStrODBC(this.w_ANCHKMAI)+;
             ",ANCHKPEC="+cp_ToStrODBC(this.w_ANCHKPEC)+;
             ",ANCHKFAX="+cp_ToStrODBC(this.w_ANCHKFAX)+;
             ",ANCHKCPZ="+cp_ToStrODBC(this.w_ANCHKCPZ)+;
             ",ANCHKWWP="+cp_ToStrODBC(this.w_ANCHKWWP)+;
             ",ANCHKPTL="+cp_ToStrODBC(this.w_ANCHKPTL)+;
             ",ANCHKFIR="+cp_ToStrODBC(this.w_ANCHKFIR)+;
             ",ANCODEST="+cp_ToStrODBC(this.w_ANCODEST)+;
             ",ANCODCLA="+cp_ToStrODBC(this.w_ANCODCLA)+;
             ",ANCODPEC="+cp_ToStrODBC(this.w_ANCODPEC)+;
             ",ANDTOBSO="+cp_ToStrODBC(this.w_ANDTOBSO)+;
             ",ANDTINVA="+cp_ToStrODBC(this.w_ANDTINVA)+;
             ",ANTIPSOT="+cp_ToStrODBC(this.w_ANTIPSOT)+;
             ",ANCONSUP="+cp_ToStrODBCNull(this.w_ANCONSUP)+;
             ",ANCONRIF="+cp_ToStrODBCNull(this.w_ANCONRIF)+;
             ",ANCATCON="+cp_ToStrODBCNull(this.w_ANCATCON)+;
             ",ANCODIVA="+cp_ToStrODBCNull(this.w_ANCODIVA)+;
             ",ANTIPOPE="+cp_ToStrODBCNull(this.w_ANTIPOPE)+;
             ",ANCONCAU="+cp_ToStrODBCNull(this.w_ANCONCAU)+;
             ",ANOPETRE="+cp_ToStrODBC(this.w_ANOPETRE)+;
             ",ANTIPPRE="+cp_ToStrODBC(this.w_ANTIPPRE)+;
             ",ANPARTSN="+cp_ToStrODBC(this.w_ANPARTSN)+;
             ",ANFLAACC="+cp_ToStrODBC(this.w_ANFLAACC)+;
             ",AFFLINTR="+cp_ToStrODBC(this.w_AFFLINTR)+;
             ",ANFLBLLS="+cp_ToStrODBC(this.w_ANFLBLLS)+;
             ",ANFLSOAL="+cp_ToStrODBC(this.w_ANFLSOAL)+;
             ",ANFLBODO="+cp_ToStrODBC(this.w_ANFLBODO)+;
             ",ANIVASOS="+cp_ToStrODBC(this.w_ANIVASOS)+;
             ",ANCODSTU="+cp_ToStrODBC(this.w_ANCODSTU)+;
             ",ANCODSOG="+cp_ToStrODBC(this.w_ANCODSOG)+;
             ",ANCODCAT="+cp_ToStrODBCNull(this.w_ANCODCAT)+;
             ",ANTIPRIF="+cp_ToStrODBC(this.w_ANTIPRIF)+;
             ",ANFLSGRE="+cp_ToStrODBC(this.w_ANFLSGRE)+;
             ",ANCATOPE="+cp_ToStrODBC(this.w_ANCATOPE)+;
             ",ANCODCUC="+cp_ToStrODBC(this.w_ANCODCUC)+;
             ",ANNUMCOR="+cp_ToStrODBC(this.w_ANNUMCOR)+;
             ",ANCINABI="+cp_ToStrODBC(this.w_ANCINABI)+;
             ",AN__BBAN="+cp_ToStrODBC(this.w_AN__BBAN)+;
             ",AN__IBAN="+cp_ToStrODBC(this.w_AN__IBAN)+;
             ",ANDESCR3="+cp_ToStrODBC(this.w_ANDESCR3)+;
             ",ANDESCR4="+cp_ToStrODBC(this.w_ANDESCR4)+;
             ",ANINDIR3="+cp_ToStrODBC(this.w_ANINDIR3)+;
             ",ANINDIR4="+cp_ToStrODBC(this.w_ANINDIR4)+;
             ",ANLOCAL2="+cp_ToStrODBC(this.w_ANLOCAL2)+;
             ",ANCODGRU="+cp_ToStrODBCNull(this.w_ANCODGRU)+;
             ",ANCODPAG="+cp_ToStrODBCNull(this.w_ANCODPAG)+;
             ",ANGIOFIS="+cp_ToStrODBC(this.w_ANGIOFIS)+;
             ",AN1MESCL="+cp_ToStrODBC(this.w_AN1MESCL)+;
             ",ANGIOSC1="+cp_ToStrODBC(this.w_ANGIOSC1)+;
             ",AN2MESCL="+cp_ToStrODBC(this.w_AN2MESCL)+;
             ",ANGIOSC2="+cp_ToStrODBC(this.w_ANGIOSC2)+;
             ",ANCODBAN="+cp_ToStrODBCNull(this.w_ANCODBAN)+;
             ",ANCODBA2="+cp_ToStrODBCNull(this.w_ANCODBA2)+;
             ",ANFLINCA="+cp_ToStrODBC(this.w_ANFLINCA)+;
             ",ANSPEINC="+cp_ToStrODBC(this.w_ANSPEINC)+;
             ",ANFLESIM="+cp_ToStrODBC(this.w_ANFLESIM)+;
             ",ANSAGINT="+cp_ToStrODBC(this.w_ANSAGINT)+;
             ",ANSPRINT="+cp_ToStrODBC(this.w_ANSPRINT)+;
             ",ANFLACBD="+cp_ToStrODBC(this.w_ANFLACBD)+;
             ",ANFLRAGG="+cp_ToStrODBC(this.w_ANFLRAGG)+;
             ",ANFLGAVV="+cp_ToStrODBC(this.w_ANFLGAVV)+;
             ",ANDATAVV="+cp_ToStrODBC(this.w_ANDATAVV)+;
             ",ANCATCOM="+cp_ToStrODBCNull(this.w_ANCATCOM)+;
             ",ANGRPDEF="+cp_ToStrODBCNull(this.w_ANGRPDEF)+;
             ",ANCATSCM="+cp_ToStrODBCNull(this.w_ANCATSCM)+;
             ",ANCODLIN="+cp_ToStrODBCNull(this.w_ANCODLIN)+;
             ",ANCODVAL="+cp_ToStrODBCNull(this.w_ANCODVAL)+;
             ",ANNUMLIS="+cp_ToStrODBCNull(this.w_ANNUMLIS)+;
             ",AN1SCONT="+cp_ToStrODBC(this.w_AN1SCONT)+;
             ",AN2SCONT="+cp_ToStrODBC(this.w_AN2SCONT)+;
             ""
             i_nnn=i_nnn+;
             ",ANCODZON="+cp_ToStrODBCNull(this.w_ANCODZON)+;
             ",ANCONCON="+cp_ToStrODBCNull(this.w_ANCONCON)+;
             ",ANMAGTER="+cp_ToStrODBCNull(this.w_ANMAGTER)+;
             ",ANCODESC="+cp_ToStrODBC(this.w_ANCODESC)+;
             ",ANCODPOR="+cp_ToStrODBCNull(this.w_ANCODPOR)+;
             ",ANMCALSI="+cp_ToStrODBCNull(this.w_ANMCALSI)+;
             ",ANMCALST="+cp_ToStrODBCNull(this.w_ANMCALST)+;
             ",ANSCORPO="+cp_ToStrODBC(this.w_ANSCORPO)+;
             ",ANFLCODI="+cp_ToStrODBC(this.w_ANFLCODI)+;
             ",ANFLIMBA="+cp_ToStrODBC(this.w_ANFLIMBA)+;
             ",ANFLGCPZ="+cp_ToStrODBC(this.w_ANFLGCPZ)+;
             ",ANFLGCAU="+cp_ToStrODBC(this.w_ANFLGCAU)+;
             ",ANFLAPCA="+cp_ToStrODBC(this.w_ANFLAPCA)+;
             ",ANRITENU="+cp_ToStrODBC(this.w_ANRITENU)+;
             ",ANCODIRP="+cp_ToStrODBCNull(this.w_ANCODIRP)+;
             ",ANCODTR2="+cp_ToStrODBCNull(this.w_ANCODTR2)+;
             ",ANPEINPS="+cp_ToStrODBC(this.w_ANPEINPS)+;
             ",ANRIINPS="+cp_ToStrODBC(this.w_ANRIINPS)+;
             ",ANCOINPS="+cp_ToStrODBC(this.w_ANCOINPS)+;
             ",ANCASPRO="+cp_ToStrODBC(this.w_ANCASPRO)+;
             ",ANCODATT="+cp_ToStrODBC(this.w_ANCODATT)+;
             ",ANCODASS="+cp_ToStrODBC(this.w_ANCODASS)+;
             ",ANCOIMPS="+cp_ToStrODBC(this.w_ANCOIMPS)+;
             ",AN__NOTE="+cp_ToStrODBC(this.w_AN__NOTE)+;
             ",ANRATING="+cp_ToStrODBCNull(this.w_ANRATING)+;
             ",ANGIORIT="+cp_ToStrODBC(this.w_ANGIORIT)+;
             ",ANVOCFIN="+cp_ToStrODBCNull(this.w_ANVOCFIN)+;
             ",ANESCDOF="+cp_ToStrODBC(this.w_ANESCDOF)+;
             ",ANDESPAR="+cp_ToStrODBC(this.w_ANDESPAR)+;
             ",ANFLGEST="+cp_ToStrODBC(this.w_ANFLGEST)+;
             ",ANCOFISC="+cp_ToStrODBC(this.w_ANCOFISC)+;
             ",ANSCHUMA="+cp_ToStrODBC(this.w_ANSCHUMA)+;
             ",AN_EREDE="+cp_ToStrODBC(this.w_AN_EREDE)+;
             ",ANCODSNS="+cp_ToStrODBC(this.w_ANCODSNS)+;
             ",ANCODREG="+cp_ToStrODBCNull(this.w_ANCODREG)+;
             ",ANCODCOM="+cp_ToStrODBCNull(this.w_ANCODCOM)+;
             ",ANCAURIT="+cp_ToStrODBC(this.w_ANCAURIT)+;
             ",ANEVEECC="+cp_ToStrODBC(this.w_ANEVEECC)+;
             ",ANCATPAR="+cp_ToStrODBC(this.w_ANCATPAR)+;
             ",ANFLESIG="+cp_ToStrODBC(this.w_ANFLESIG)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTI')
        i_cWhere = cp_PKFox(i_cTable  ,'ANTIPCON',this.w_ANTIPCON  ,'ANCODICE',this.w_ANCODICE  )
        UPDATE (i_cTable) SET;
              ANDESCRI=this.w_ANDESCRI;
             ,ANTIPCLF=this.w_ANTIPCLF;
             ,ANDESCR2=this.w_ANDESCR2;
             ,ANINDIRI=this.w_ANINDIRI;
             ,ANINDIR2=this.w_ANINDIR2;
             ,AN___CAP=this.w_AN___CAP;
             ,ANLOCALI=this.w_ANLOCALI;
             ,ANPROVIN=this.w_ANPROVIN;
             ,ANNAZION=this.w_ANNAZION;
             ,ANCODFIS=this.w_ANCODFIS;
             ,ANPARIVA=this.w_ANPARIVA;
             ,ANTELEFO=this.w_ANTELEFO;
             ,ANTELFAX=this.w_ANTELFAX;
             ,ANNUMCEL=this.w_ANNUMCEL;
             ,ANINDWEB=this.w_ANINDWEB;
             ,AN_EMAIL=this.w_AN_EMAIL;
             ,AN_EMPEC=this.w_AN_EMPEC;
             ,ANPERFIS=this.w_ANPERFIS;
             ,AN_SESSO=this.w_AN_SESSO;
             ,ANCOGNOM=this.w_ANCOGNOM;
             ,AN__NOME=this.w_AN__NOME;
             ,ANLOCNAS=this.w_ANLOCNAS;
             ,ANPRONAS=this.w_ANPRONAS;
             ,ANDATNAS=this.w_ANDATNAS;
             ,ANNUMCAR=this.w_ANNUMCAR;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,ANCHKSTA=this.w_ANCHKSTA;
             ,ANCHKMAI=this.w_ANCHKMAI;
             ,ANCHKPEC=this.w_ANCHKPEC;
             ,ANCHKFAX=this.w_ANCHKFAX;
             ,ANCHKCPZ=this.w_ANCHKCPZ;
             ,ANCHKWWP=this.w_ANCHKWWP;
             ,ANCHKPTL=this.w_ANCHKPTL;
             ,ANCHKFIR=this.w_ANCHKFIR;
             ,ANCODEST=this.w_ANCODEST;
             ,ANCODCLA=this.w_ANCODCLA;
             ,ANCODPEC=this.w_ANCODPEC;
             ,ANDTOBSO=this.w_ANDTOBSO;
             ,ANDTINVA=this.w_ANDTINVA;
             ,ANTIPSOT=this.w_ANTIPSOT;
             ,ANCONSUP=this.w_ANCONSUP;
             ,ANCONRIF=this.w_ANCONRIF;
             ,ANCATCON=this.w_ANCATCON;
             ,ANCODIVA=this.w_ANCODIVA;
             ,ANTIPOPE=this.w_ANTIPOPE;
             ,ANCONCAU=this.w_ANCONCAU;
             ,ANOPETRE=this.w_ANOPETRE;
             ,ANTIPPRE=this.w_ANTIPPRE;
             ,ANPARTSN=this.w_ANPARTSN;
             ,ANFLAACC=this.w_ANFLAACC;
             ,AFFLINTR=this.w_AFFLINTR;
             ,ANFLBLLS=this.w_ANFLBLLS;
             ,ANFLSOAL=this.w_ANFLSOAL;
             ,ANFLBODO=this.w_ANFLBODO;
             ,ANIVASOS=this.w_ANIVASOS;
             ,ANCODSTU=this.w_ANCODSTU;
             ,ANCODSOG=this.w_ANCODSOG;
             ,ANCODCAT=this.w_ANCODCAT;
             ,ANTIPRIF=this.w_ANTIPRIF;
             ,ANFLSGRE=this.w_ANFLSGRE;
             ,ANCATOPE=this.w_ANCATOPE;
             ,ANCODCUC=this.w_ANCODCUC;
             ,ANNUMCOR=this.w_ANNUMCOR;
             ,ANCINABI=this.w_ANCINABI;
             ,AN__BBAN=this.w_AN__BBAN;
             ,AN__IBAN=this.w_AN__IBAN;
             ,ANDESCR3=this.w_ANDESCR3;
             ,ANDESCR4=this.w_ANDESCR4;
             ,ANINDIR3=this.w_ANINDIR3;
             ,ANINDIR4=this.w_ANINDIR4;
             ,ANLOCAL2=this.w_ANLOCAL2;
             ,ANCODGRU=this.w_ANCODGRU;
             ,ANCODPAG=this.w_ANCODPAG;
             ,ANGIOFIS=this.w_ANGIOFIS;
             ,AN1MESCL=this.w_AN1MESCL;
             ,ANGIOSC1=this.w_ANGIOSC1;
             ,AN2MESCL=this.w_AN2MESCL;
             ,ANGIOSC2=this.w_ANGIOSC2;
             ,ANCODBAN=this.w_ANCODBAN;
             ,ANCODBA2=this.w_ANCODBA2;
             ,ANFLINCA=this.w_ANFLINCA;
             ,ANSPEINC=this.w_ANSPEINC;
             ,ANFLESIM=this.w_ANFLESIM;
             ,ANSAGINT=this.w_ANSAGINT;
             ,ANSPRINT=this.w_ANSPRINT;
             ,ANFLACBD=this.w_ANFLACBD;
             ,ANFLRAGG=this.w_ANFLRAGG;
             ,ANFLGAVV=this.w_ANFLGAVV;
             ,ANDATAVV=this.w_ANDATAVV;
             ,ANCATCOM=this.w_ANCATCOM;
             ,ANGRPDEF=this.w_ANGRPDEF;
             ,ANCATSCM=this.w_ANCATSCM;
             ,ANCODLIN=this.w_ANCODLIN;
             ,ANCODVAL=this.w_ANCODVAL;
             ,ANNUMLIS=this.w_ANNUMLIS;
             ,AN1SCONT=this.w_AN1SCONT;
             ,AN2SCONT=this.w_AN2SCONT;
             ,ANCODZON=this.w_ANCODZON;
             ,ANCONCON=this.w_ANCONCON;
             ,ANMAGTER=this.w_ANMAGTER;
             ,ANCODESC=this.w_ANCODESC;
             ,ANCODPOR=this.w_ANCODPOR;
             ,ANMCALSI=this.w_ANMCALSI;
             ,ANMCALST=this.w_ANMCALST;
             ,ANSCORPO=this.w_ANSCORPO;
             ,ANFLCODI=this.w_ANFLCODI;
             ,ANFLIMBA=this.w_ANFLIMBA;
             ,ANFLGCPZ=this.w_ANFLGCPZ;
             ,ANFLGCAU=this.w_ANFLGCAU;
             ,ANFLAPCA=this.w_ANFLAPCA;
             ,ANRITENU=this.w_ANRITENU;
             ,ANCODIRP=this.w_ANCODIRP;
             ,ANCODTR2=this.w_ANCODTR2;
             ,ANPEINPS=this.w_ANPEINPS;
             ,ANRIINPS=this.w_ANRIINPS;
             ,ANCOINPS=this.w_ANCOINPS;
             ,ANCASPRO=this.w_ANCASPRO;
             ,ANCODATT=this.w_ANCODATT;
             ,ANCODASS=this.w_ANCODASS;
             ,ANCOIMPS=this.w_ANCOIMPS;
             ,AN__NOTE=this.w_AN__NOTE;
             ,ANRATING=this.w_ANRATING;
             ,ANGIORIT=this.w_ANGIORIT;
             ,ANVOCFIN=this.w_ANVOCFIN;
             ,ANESCDOF=this.w_ANESCDOF;
             ,ANDESPAR=this.w_ANDESPAR;
             ,ANFLGEST=this.w_ANFLGEST;
             ,ANCOFISC=this.w_ANCOFISC;
             ,ANSCHUMA=this.w_ANSCHUMA;
             ,AN_EREDE=this.w_AN_EREDE;
             ,ANCODSNS=this.w_ANCODSNS;
             ,ANCODREG=this.w_ANCODREG;
             ,ANCODCOM=this.w_ANCODCOM;
             ,ANCAURIT=this.w_ANCAURIT;
             ,ANEVEECC=this.w_ANEVEECC;
             ,ANCATPAR=this.w_ANCATPAR;
             ,ANFLESIG=this.w_ANFLESIG;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAR_MSA : Saving
      this.GSAR_MSA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANCODICE,"SSCODICE";
             ,this.w_ANTIPCON,"SSTIPCON";
             )
      this.GSAR_MSA.mReplace()
      * --- GSAR_MSE : Saving
      this.GSAR_MSE.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"SITIPCON";
             ,this.w_ANCODICE,"SICODCON";
             )
      this.GSAR_MSE.mReplace()
      * --- GSAR_MDD : Saving
      this.GSAR_MDD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"DDTIPCON";
             ,this.w_ANCODICE,"DDCODICE";
             )
      this.GSAR_MDD.mReplace()
      * --- GSAR_MCO : Saving
      this.GSAR_MCO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"COTIPCON";
             ,this.w_ANCODICE,"COCODCON";
             )
      this.GSAR_MCO.mReplace()
      * --- GSAR_MCC : Saving
      this.GSAR_MCC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"CCTIPCON";
             ,this.w_ANCODICE,"CCCODCON";
             )
      this.GSAR_MCC.mReplace()
      * --- GSAR_MIN : Saving
      this.GSAR_MIN.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"MCTIPINT";
             ,this.w_ANCODICE,"MCCODINT";
             )
      this.GSAR_MIN.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsar_afr
    *--- Cliente Variato
    if IsAlt() AND this.cFunction="Edit" and not(bTrsErr) and (g_OFFE='S' OR g_AGEN='S') AND !This.w_DANOM
      * --- Aggiorna Riferimenti Cliente sui Nominativi Offerte
      this.NotifyEvent('FornitoreVariato')
    endif
    * --- Esegue Controlli Finali
       this.NotifyEvent('ControlliFinali')   
       
       
    **Controllo che altri utenti non abbiano lo stesso codice Studio 
    w_MESS=''
    IF NOT CHK_STUDIO(this.w_ANTIPCON,this.w_ANCODICE,this.w_ANCODSTU,.T.) 
    local objcod
    this.getctrl ("w_ANCODICE")
    objcod.value=this.w_ANCODICE
    objcod.valid()
    this.mcalc(.T.)
    bTrsErr=.T.
    i_TrsMsg=w_MESS
    ENDIF
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- gsar_afr
    if not(bTrsErr) and g_CESP='S'
      this.NotifyEvent('CliForEliminato')
    endif
    * --- Fine Area Manuale
    * --- GSAR_MSA : Deleting
    this.GSAR_MSA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANCODICE,"SSCODICE";
           ,this.w_ANTIPCON,"SSTIPCON";
           )
    this.GSAR_MSA.mDelete()
    * --- GSAR_MSE : Deleting
    this.GSAR_MSE.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"SITIPCON";
           ,this.w_ANCODICE,"SICODCON";
           )
    this.GSAR_MSE.mDelete()
    * --- GSAR_MDD : Deleting
    this.GSAR_MDD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"DDTIPCON";
           ,this.w_ANCODICE,"DDCODICE";
           )
    this.GSAR_MDD.mDelete()
    * --- GSAR_MCO : Deleting
    this.GSAR_MCO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"COTIPCON";
           ,this.w_ANCODICE,"COCODCON";
           )
    this.GSAR_MCO.mDelete()
    * --- GSAR_MCC : Deleting
    this.GSAR_MCC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"CCTIPCON";
           ,this.w_ANCODICE,"CCCODCON";
           )
    this.GSAR_MCC.mDelete()
    * --- GSAR_MIN : Deleting
    this.GSAR_MIN.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"MCTIPINT";
           ,this.w_ANCODICE,"MCCODINT";
           )
    this.GSAR_MIN.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTI_IDX,i_nConn)
      *
      * delete CONTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ANTIPCON',this.w_ANTIPCON  ,'ANCODICE',this.w_ANCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
        if .o_AUTO<>.w_AUTO
            .w_ANCODICE = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_ANCODICE)
        endif
        .DoRTCalc(5,45,.t.)
        if .o_ANCHKMAI<>.w_ANCHKMAI.or. .o_ANCHKCPZ<>.w_ANCHKCPZ
            .w_ANCHKFIR = iif(.w_ANCHKMAI='S' or .w_ANCHKCPZ = 'S',.w_ANCHKFIR,'N')
        endif
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        if .o_ANPERFIS<>.w_ANPERFIS
          .Calculate_WWPJDQWIRN()
        endif
        if .o_ANPERFIS<>.w_ANPERFIS
          .Calculate_QWKRQHGBHI()
        endif
        if .o_ANCODFIS<>.w_ANCODFIS
          .Calculate_XNLBETFNTC()
        endif
        .DoRTCalc(47,54,.t.)
            .w_TIPRIF = 'C'
            .w_CODI = .w_ANCODICE
            .w_DES1 = .w_ANDESCRI
          .link_2_4('Full')
        .DoRTCalc(59,81,.t.)
        if .o_ANFLBLLS<>.w_ANFLBLLS
            .w_ANFLSOAL = 'N'
        endif
        if .o_ANFLSOAL<>.w_ANFLSOAL.or. .o_AFFLINTR<>.w_AFFLINTR
            .w_ANFLBODO = iif(.w_AFFLINTR='S' or .w_ANFLSOAL='S' or g_TRAEXP = 'N',' ',.w_ANFLBODO)
        endif
        .DoRTCalc(84,84,.t.)
        if .o_CODSTU<>.w_CODSTU.or. .o_PROSTU<>.w_PROSTU
            .w_ANCODSTU = IIF(g_TRAEXP<>'G',SPACE(9),.w_ANCODSTU)
        endif
        .DoRTCalc(86,89,.t.)
            .w_OBTEST = i_datsys
            .w_ANTIPRIF = IIF(EMPTY(.w_ANCONRIF), ' ', 'C')
        if .o_CODSTU<>.w_CODSTU.or. .o_PROSTU<>.w_PROSTU
          .Calculate_QAXUOQUTMC()
        endif
        if .o_ANCODSTU<>.w_ANCODSTU
          .Calculate_MZFNHIDLHB()
        endif
        if .o_CODSTU<>.w_CODSTU.or. .o_PROSTU<>.w_PROSTU
          .Calculate_XKIAONEMCL()
        endif
        .DoRTCalc(92,109,.t.)
            .w_TIPCC = 'G'
        .DoRTCalc(111,119,.t.)
            .w_RIFDIC = ''
            .w_ANNDIC = ''
            .w_DATDIC = ''
            .w_NUMDIC = ''
        .DoRTCalc(124,128,.t.)
            .w_DES1 = .w_ANDESCRI
            .w_CODI = .w_ANCODICE
        .DoRTCalc(131,132,.t.)
            .w_CODI = .w_ANCODICE
            .w_DES1 = .w_ANDESCRI
            .w_CODI = .w_ANCODICE
            .w_DES1 = .w_ANDESCRI
        if  .o_ANCODICE<>.w_ANCODICE.or. .o_ANTIPCON<>.w_ANTIPCON
          .WriteTo_GSAR_MCO()
        endif
            .w_CODI = .w_ANCODICE
            .w_DES1 = .w_ANDESCRI
        .DoRTCalc(139,142,.t.)
            .w_DESMES1 = LEFT(IIF(.w_AN1MESCL>0 AND .w_AN1MESCL<13, g_MESE[.w_AN1MESCL], "")+SPACE(12),12)
        if .o_AN1MESCL<>.w_AN1MESCL
            .w_ANGIOSC1 = 0
        endif
        if .o_AN1MESCL<>.w_AN1MESCL
            .w_AN2MESCL = 0
        endif
            .w_DESMES2 = LEFT(IIF(.w_AN2MESCL>0 AND .w_AN2MESCL<13, g_MESE[.w_AN2MESCL], "")+SPACE(12),12)
        if .o_AN2MESCL<>.w_AN2MESCL
            .w_ANGIOSC2 = 0
        endif
        .DoRTCalc(148,153,.t.)
        if .o_ANFLESIM<>.w_ANFLESIM
            .w_ANSAGINT = IIF(.w_ANFLESIM='S', 0, .w_ANSAGINT)
        endif
        if .o_ANFLESIM<>.w_ANFLESIM.or. .o_ANSAGINT<>.w_ANSAGINT
            .w_ANSPRINT = IIF(.w_ANFLESIM='S' OR .w_ANSAGINT=0,'N',.w_ANSPRINT)
        endif
        .DoRTCalc(156,160,.t.)
            .w_CODI = .w_ANCODICE
            .w_DES1 = .w_ANDESCRI
        .DoRTCalc(163,174,.t.)
        if .o_AN1SCONT<>.w_AN1SCONT
            .w_AN2SCONT = 0
        endif
        .DoRTCalc(176,200,.t.)
        if .o_ANRITENU<>.w_ANRITENU
            .w_ANCODIRP = IIF(.w_ANRITENU$'CS',.w_ANCODIRP,space(5))
          .link_3_32('Full')
        endif
        if .o_ANRITENU<>.w_ANRITENU
            .w_ANCODTR2 = SPACE(5)
          .link_3_33('Full')
        endif
        if .o_ANRITENU<>.w_ANRITENU
            .w_ANPEINPS = IIF(.w_ANRITENU='C',.w_ANPEINPS,0)
        endif
        if .o_ANRITENU<>.w_ANRITENU
            .w_ANRIINPS = IIF(.w_ANRITENU='C',.w_ANRIINPS,0)
        endif
        if .o_ANRITENU<>.w_ANRITENU
            .w_ANCOINPS = IIF(.w_ANRITENU='C',.w_ANCOINPS,0)
        endif
        .DoRTCalc(206,206,.t.)
        if .o_ANRITENU<>.w_ANRITENU
            .w_ANCODATT = IIF(.w_ANRITENU='C',1,0)
        endif
        if .o_ANRITENU<>.w_ANRITENU
            .w_ANCODASS = IIF(.w_ANRITENU='C',.w_ANCODASS,SPACE(3))
        endif
        if .o_ANRITENU<>.w_ANRITENU
            .w_ANCOIMPS = IIF(.w_ANRITENU='C',.w_ANCOIMPS,SPACE(20))
        endif
            .w_CODI = .w_ANCODICE
            .w_DES1 = .w_ANDESCRI
        .DoRTCalc(212,212,.t.)
            .w_CODI = .w_ANCODICE
            .w_DES1 = .w_ANDESCRI
        .DoRTCalc(215,221,.t.)
            .w_CODI = .w_ANCODICE
        if .o_ANRITENU<>.w_ANRITENU
            .w_ANFLGEST = iif(.w_ANRITENU<>'N',.w_ANFLGEST,'N')
        endif
        .DoRTCalc(224,226,.t.)
        if .o_ANFLGEST<>.w_ANFLGEST.or. .o_ANFLBLLS<>.w_ANFLBLLS
            .w_ANCOFISC = iif(.w_ANFLGEST='S' OR .w_ANFLBLLS='S',.w_ANCOFISC,SPACE(25))
        endif
        if .o_ANFLGEST<>.w_ANFLGEST.or. .o_ANFLSGRE<>.w_ANFLSGRE
            .w_ANSCHUMA = 'N'
        endif
        .DoRTCalc(229,229,.t.)
        if .o_ANRITENU<>.w_ANRITENU
            .w_ANCODSNS = ' '
        endif
        if .o_ANFLGEST<>.w_ANFLGEST
            .w_ANCODREG = iif(.w_ANFLGEST='S','  ',.w_ANCODREG)
          .link_3_53('Full')
        endif
        if .o_ANFLGEST<>.w_ANFLGEST
            .w_ANCODCOM = iif(.w_ANFLGEST='S',Space(4),.w_ANCODCOM)
          .link_3_54('Full')
        endif
        .DoRTCalc(233,233,.t.)
        if .o_ANCODIRP<>.w_ANCODIRP
            .w_ANCAURIT = .w_CAUPRE2
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
          if g_CFNUME='S' AND .p_AUT=0
             cp_AskTableProg(this,i_nConn,"PRNUFO","i_codazi,w_ANCODICE")
          endif
          .op_ANCODICE = .w_ANCODICE
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(235,238,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
    endwith
  return

  proc Calculate_WWPJDQWIRN()
    with this
          * --- Divide ragione sociale
          GSAR_BDR(this;
              ,'F';
             )
    endwith
  endproc
  proc Calculate_QWKRQHGBHI()
    with this
          * --- Sbianco inf. persona fisica
          .w_ANCOGNOM = iif(.w_ANPERFIS='S',.w_ANCOGNOM,'')
          .w_AN__NOME = iif(.w_ANPERFIS='S',.w_AN__NOME,'')
          .w_ANLOCNAS = iif(.w_ANPERFIS='S',.w_ANLOCNAS,'')
          .w_ANPRONAS = iif(.w_ANPERFIS='S',.w_ANPRONAS,'')
          .w_ANDATNAS = iif(.w_ANPERFIS='S',.w_ANDATNAS,Cp_CharToDate('  -  -    '))
          .w_AN_SESSO = iif(.w_ANPERFIS='S',.w_AN_SESSO,'')
          .w_ANNUMCAR = ''
    endwith
  endproc
  proc Calculate_XNLBETFNTC()
    with this
          * --- w_ANCODFIS changed
          .w_ERR = cfanag(.w_ANCODFIS,'V')
          .w_ANPERFIS = iif(.w_ERR, .w_ANPERFIS , 'S' )
          .w_ANDATNAS = iif(.w_ERR, .w_ANDATNAS, IIF (.w_CONTA<>.o_CONTA, .w_ANDATNAS ,cfanag(.w_ANCODFIS,'D') ))
          .w_AN_SESSO = iif(.w_ERR,  .w_AN_SESSO , cfanag(.w_ANCODFIS,'S'))
          .w_ANLOCNAS = iif(.w_ERR, .w_ANLOCNAS , cfanag(.w_ANCODFIS,'L'))
          .w_ANPRONAS = iif(.w_ERR,  .w_ANPRONAS , cfanag(.w_ANCODFIS,'P'))
          .w_CODCOM = iif(.w_ERR, .w_CODCOM , SUBSTR(.w_ANCODFIS,12,4))
    endwith
  endproc
  proc Calculate_OLYGVFQXTR()
    with this
          * --- Calcola auto
          GSAR_BSS(this;
             )
    endwith
  endproc
  proc Calculate_UIROVJBFQO()
    with this
          * --- recupera progressivo
          GSAR_BRK(this;
              ,.w_ANTIPCON;
              ,.w_ANCODICE;
             )
    endwith
  endproc
  proc Calculate_PGQNPFGCXZ()
    with this
          * --- Colora pagina note
          GSUT_BCN(this;
              ,"COLORA";
              ,.w_AN__NOTE;
             )
    endwith
  endproc
  proc Calculate_GRJFLMWOLU()
    with this
          * --- Numero pagina note
          GSUT_BCN(this;
              ,"PAGINA";
              ,"Note";
             )
    endwith
  endproc
  proc Calculate_QAXUOQUTMC()
    with this
          * --- Calcolo sottocontostudio
          .w_ANCODSTU = Icase(g_LEMC='S' AND  !g_TRAEXP$('N-G') AND EMPTY(.w_ANCODSTU),GSLM_BCF(this, .w_ANTIPCON, .w_ANCODICE, .w_CODSTU , .w_ANCONSUP, ,.w_PROSTU ),g_TRAEXP='G',.w_ANCODSTU,' ')
    endwith
  endproc
  proc Calculate_MZFNHIDLHB()
    with this
          * --- Simulo zerofill ancodstu
          .w_ANCODSTU = IIF(g_TRAEXP<>'G',Left(IIF( ( Left( Alltrim( .w_ANCODSTU ) ,1 ) <>"0" And  Len( Alltrim( .w_ANCODSTU ) )=Len( "999999" ) ) Or Left( Alltrim( .w_ANCODSTU ) ,1 ) ="0" , .w_ANCODSTU , IIF(g_LEMC<>'S' OR g_TRAEXP='N','',Right( Repl('0',Len( "999999" ))+Alltrim( .w_ANCODSTU ) ,Len( "999999" ) )  ))+'      ',10),Alltrim( .w_ANCODSTU ))
    endwith
  endproc
  proc Calculate_XKIAONEMCL()
    with this
          * --- Calcolo estremi intervallo sottoconto studio
          iif(g_LEMC='S' AND g_TRAEXP<>'N',GSLM_BCF(this;
              ,.w_ANTIPCON;
              ,.w_ANCODICE;
              ,.w_CODSTU;
              ,.w_ANCONSUP;
              ,.w_ANCODSTU;
              ,.w_PROSTU);
              ,1=1;
             )
    endwith
  endproc
  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_NTGPIXRFKL()
    with this
          * --- Elimina nominativi
          gsar_bkn(this;
             )
    endwith
  endproc
  proc Calculate_VISJNUHVVI()
    with this
          * --- Scorporo a piede fattura
          MessFlgScorporo(this;
             )
    endwith
  endproc
  proc Calculate_JHBPFGHPWV()
    with this
          * --- GSAR_BOL(V) - FornitoreVariato
          gsar_bol(this;
              ,'V';
              ,.w_ANCODICE;
              ,'F';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oANCODICE_1_4.enabled = this.oPgFrm.Page1.oPag.oANCODICE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oAN_SESSO_1_27.enabled = this.oPgFrm.Page1.oPag.oAN_SESSO_1_27.mCond()
    this.oPgFrm.Page1.oPag.oANCOGNOM_1_28.enabled = this.oPgFrm.Page1.oPag.oANCOGNOM_1_28.mCond()
    this.oPgFrm.Page1.oPag.oAN__NOME_1_29.enabled = this.oPgFrm.Page1.oPag.oAN__NOME_1_29.mCond()
    this.oPgFrm.Page1.oPag.oANLOCNAS_1_30.enabled = this.oPgFrm.Page1.oPag.oANLOCNAS_1_30.mCond()
    this.oPgFrm.Page1.oPag.oANPRONAS_1_31.enabled = this.oPgFrm.Page1.oPag.oANPRONAS_1_31.mCond()
    this.oPgFrm.Page1.oPag.oANDATNAS_1_32.enabled = this.oPgFrm.Page1.oPag.oANDATNAS_1_32.mCond()
    this.oPgFrm.Page1.oPag.oANNUMCAR_1_33.enabled = this.oPgFrm.Page1.oPag.oANNUMCAR_1_33.mCond()
    this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.enabled = this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.mCond()
    this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.enabled = this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.mCond()
    this.oPgFrm.Page2.oPag.oANCATCON_2_10.enabled = this.oPgFrm.Page2.oPag.oANCATCON_2_10.mCond()
    this.oPgFrm.Page2.oPag.oANCONCAU_2_16.enabled = this.oPgFrm.Page2.oPag.oANCONCAU_2_16.mCond()
    this.oPgFrm.Page2.oPag.oANPARTSN_2_23.enabled = this.oPgFrm.Page2.oPag.oANPARTSN_2_23.mCond()
    this.oPgFrm.Page2.oPag.oANFLSOAL_2_28.enabled = this.oPgFrm.Page2.oPag.oANFLSOAL_2_28.mCond()
    this.oPgFrm.Page2.oPag.oANFLBODO_2_29.enabled = this.oPgFrm.Page2.oPag.oANFLBODO_2_29.mCond()
    this.oPgFrm.Page2.oPag.oANCODSTU_2_31.enabled = this.oPgFrm.Page2.oPag.oANCODSTU_2_31.mCond()
    this.oPgFrm.Page2.oPag.oANCODSOG_2_33.enabled = this.oPgFrm.Page2.oPag.oANCODSOG_2_33.mCond()
    this.oPgFrm.Page5.oPag.oANGIOSC1_5_9.enabled = this.oPgFrm.Page5.oPag.oANGIOSC1_5_9.mCond()
    this.oPgFrm.Page5.oPag.oAN2MESCL_5_10.enabled = this.oPgFrm.Page5.oPag.oAN2MESCL_5_10.mCond()
    this.oPgFrm.Page5.oPag.oANGIOSC2_5_14.enabled = this.oPgFrm.Page5.oPag.oANGIOSC2_5_14.mCond()
    this.oPgFrm.Page5.oPag.oANSPEINC_5_19.enabled = this.oPgFrm.Page5.oPag.oANSPEINC_5_19.mCond()
    this.oPgFrm.Page5.oPag.oANSAGINT_5_22.enabled = this.oPgFrm.Page5.oPag.oANSAGINT_5_22.mCond()
    this.oPgFrm.Page5.oPag.oANSPRINT_5_23.enabled = this.oPgFrm.Page5.oPag.oANSPRINT_5_23.mCond()
    this.oPgFrm.Page4.oPag.oAN2SCONT_4_24.enabled = this.oPgFrm.Page4.oPag.oAN2SCONT_4_24.mCond()
    this.oPgFrm.Page4.oPag.oANCODESC_4_39.enabled = this.oPgFrm.Page4.oPag.oANCODESC_4_39.mCond()
    this.oPgFrm.Page4.oPag.oANFLIMBA_4_50.enabled = this.oPgFrm.Page4.oPag.oANFLIMBA_4_50.mCond()
    this.oPgFrm.Page4.oPag.oANFLGCPZ_4_51.enabled = this.oPgFrm.Page4.oPag.oANFLGCPZ_4_51.mCond()
    this.oPgFrm.Page4.oPag.oANFLGCAU_4_65.enabled = this.oPgFrm.Page4.oPag.oANFLGCAU_4_65.mCond()
    this.oPgFrm.Page3.oPag.oANCODIRP_3_32.enabled = this.oPgFrm.Page3.oPag.oANCODIRP_3_32.mCond()
    this.oPgFrm.Page3.oPag.oANCODTR2_3_33.enabled = this.oPgFrm.Page3.oPag.oANCODTR2_3_33.mCond()
    this.oPgFrm.Page3.oPag.oANPEINPS_3_34.enabled = this.oPgFrm.Page3.oPag.oANPEINPS_3_34.mCond()
    this.oPgFrm.Page3.oPag.oANRIINPS_3_35.enabled = this.oPgFrm.Page3.oPag.oANRIINPS_3_35.mCond()
    this.oPgFrm.Page3.oPag.oANCOINPS_3_36.enabled = this.oPgFrm.Page3.oPag.oANCOINPS_3_36.mCond()
    this.oPgFrm.Page3.oPag.oANCASPRO_3_37.enabled = this.oPgFrm.Page3.oPag.oANCASPRO_3_37.mCond()
    this.oPgFrm.Page3.oPag.oANCODATT_3_38.enabled = this.oPgFrm.Page3.oPag.oANCODATT_3_38.mCond()
    this.oPgFrm.Page3.oPag.oANCODASS_3_39.enabled = this.oPgFrm.Page3.oPag.oANCODASS_3_39.mCond()
    this.oPgFrm.Page3.oPag.oANCOIMPS_3_40.enabled = this.oPgFrm.Page3.oPag.oANCOIMPS_3_40.mCond()
    this.oPgFrm.Page3.oPag.oANFLGEST_3_42.enabled = this.oPgFrm.Page3.oPag.oANFLGEST_3_42.mCond()
    this.oPgFrm.Page3.oPag.oANCOFISC_3_47.enabled = this.oPgFrm.Page3.oPag.oANCOFISC_3_47.mCond()
    this.oPgFrm.Page3.oPag.oANSCHUMA_3_48.enabled = this.oPgFrm.Page3.oPag.oANSCHUMA_3_48.mCond()
    this.oPgFrm.Page3.oPag.oANCODSNS_3_51.enabled = this.oPgFrm.Page3.oPag.oANCODSNS_3_51.mCond()
    this.oPgFrm.Page3.oPag.oANCODREG_3_53.enabled = this.oPgFrm.Page3.oPag.oANCODREG_3_53.mCond()
    this.oPgFrm.Page3.oPag.oANCODCOM_3_54.enabled = this.oPgFrm.Page3.oPag.oANCODCOM_3_54.mCond()
    this.oPgFrm.Page3.oPag.oANCAURIT_3_55.enabled = this.oPgFrm.Page3.oPag.oANCAURIT_3_55.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_78.enabled = this.oPgFrm.Page1.oPag.oBtn_1_78.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_80.enabled = this.oPgFrm.Page1.oPag.oBtn_1_80.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_82.enabled = this.oPgFrm.Page1.oPag.oBtn_1_82.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_88.enabled = this.oPgFrm.Page1.oPag.oBtn_1_88.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_89.enabled = this.oPgFrm.Page1.oPag.oBtn_1_89.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_94.enabled = this.oPgFrm.Page1.oPag.oBtn_1_94.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_32.enabled = this.oPgFrm.Page2.oPag.oBtn_2_32.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_51.enabled = this.oPgFrm.Page2.oPag.oBtn_2_51.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_52.enabled = this.oPgFrm.Page2.oPag.oBtn_2_52.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_53.enabled = this.oPgFrm.Page2.oPag.oBtn_2_53.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_11.enabled = this.oPgFrm.Page5.oPag.oBtn_5_11.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_12.enabled = this.oPgFrm.Page5.oPag.oBtn_5_12.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_54.enabled = this.oPgFrm.Page4.oPag.oBtn_4_54.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_55.enabled = this.oPgFrm.Page4.oPag.oBtn_4_55.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_56.enabled = this.oPgFrm.Page4.oPag.oBtn_4_56.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_57.enabled = this.oPgFrm.Page4.oPag.oBtn_4_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_112.enabled = this.oPgFrm.Page1.oPag.oBtn_1_112.mCond()
    this.oPgFrm.Page4.oPag.oLinkPC_4_71.enabled = this.oPgFrm.Page4.oPag.oLinkPC_4_71.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(6).enabled=not(IsAlt())
    local i_show5
    i_show5=not(IsAlt())
    this.oPgFrm.Pages(6).enabled=i_show5 and not(IsAlt())
    this.oPgFrm.Pages(6).caption=iif(i_show5,cp_translate("Riferimenti"),"")
    this.oPgFrm.Pages(6).oPag.visible=this.oPgFrm.Pages(6).enabled
    local i_show8
    i_show8=not(g_ISDF<>'S')
    this.oPgFrm.Pages(9).enabled=i_show8
    this.oPgFrm.Pages(9).caption=iif(i_show8,cp_translate("Dati DocFinance"),"")
    this.oPgFrm.Pages(9).oPag.visible=this.oPgFrm.Pages(9).enabled
    this.oPgFrm.Page1.oPag.oANTIPCLF_1_6.visible=!this.oPgFrm.Page1.oPag.oANTIPCLF_1_6.mHide()
    this.oPgFrm.Page1.oPag.oANCHKSTA_1_57.visible=!this.oPgFrm.Page1.oPag.oANCHKSTA_1_57.mHide()
    this.oPgFrm.Page1.oPag.oANCHKMAI_1_58.visible=!this.oPgFrm.Page1.oPag.oANCHKMAI_1_58.mHide()
    this.oPgFrm.Page1.oPag.oANCHKPEC_1_59.visible=!this.oPgFrm.Page1.oPag.oANCHKPEC_1_59.mHide()
    this.oPgFrm.Page1.oPag.oANCHKFAX_1_60.visible=!this.oPgFrm.Page1.oPag.oANCHKFAX_1_60.mHide()
    this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.visible=!this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.mHide()
    this.oPgFrm.Page1.oPag.oANCHKWWP_1_62.visible=!this.oPgFrm.Page1.oPag.oANCHKWWP_1_62.mHide()
    this.oPgFrm.Page1.oPag.oANCHKPTL_1_63.visible=!this.oPgFrm.Page1.oPag.oANCHKPTL_1_63.mHide()
    this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.visible=!this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    this.oPgFrm.Page1.oPag.oANDTINVA_1_73.visible=!this.oPgFrm.Page1.oPag.oANDTINVA_1_73.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_78.visible=!this.oPgFrm.Page1.oPag.oBtn_1_78.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_79.visible=!this.oPgFrm.Page1.oPag.oBtn_1_79.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_80.visible=!this.oPgFrm.Page1.oPag.oBtn_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_88.visible=!this.oPgFrm.Page1.oPag.oBtn_1_88.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_89.visible=!this.oPgFrm.Page1.oPag.oBtn_1_89.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_94.visible=!this.oPgFrm.Page1.oPag.oBtn_1_94.mHide()
    this.oPgFrm.Page2.oPag.oANCATCON_2_10.visible=!this.oPgFrm.Page2.oPag.oANCATCON_2_10.mHide()
    this.oPgFrm.Page2.oPag.oDESCON_2_11.visible=!this.oPgFrm.Page2.oPag.oDESCON_2_11.mHide()
    this.oPgFrm.Page2.oPag.oANCONCAU_2_16.visible=!this.oPgFrm.Page2.oPag.oANCONCAU_2_16.mHide()
    this.oPgFrm.Page2.oPag.oMINCON_2_22.visible=!this.oPgFrm.Page2.oPag.oMINCON_2_22.mHide()
    this.oPgFrm.Page2.oPag.oMAXCON_2_25.visible=!this.oPgFrm.Page2.oPag.oMAXCON_2_25.mHide()
    this.oPgFrm.Page2.oPag.oANCODSTU_2_31.visible=!this.oPgFrm.Page2.oPag.oANCODSTU_2_31.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_32.visible=!this.oPgFrm.Page2.oPag.oBtn_2_32.mHide()
    this.oPgFrm.Page2.oPag.oANCODSOG_2_33.visible=!this.oPgFrm.Page2.oPag.oANCODSOG_2_33.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_46.visible=!this.oPgFrm.Page2.oPag.oStr_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_49.visible=!this.oPgFrm.Page2.oPag.oStr_2_49.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_51.visible=!this.oPgFrm.Page2.oPag.oBtn_2_51.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_52.visible=!this.oPgFrm.Page2.oPag.oBtn_2_52.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_56.visible=!this.oPgFrm.Page2.oPag.oStr_2_56.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_62.visible=!this.oPgFrm.Page2.oPag.oStr_2_62.mHide()
    this.oPgFrm.Page8.oPag.oLinkPC_8_6.visible=!this.oPgFrm.Page8.oPag.oLinkPC_8_6.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_11.visible=!this.oPgFrm.Page5.oPag.oBtn_5_11.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_12.visible=!this.oPgFrm.Page5.oPag.oBtn_5_12.mHide()
    this.oPgFrm.Page5.oPag.oANSAGINT_5_22.visible=!this.oPgFrm.Page5.oPag.oANSAGINT_5_22.mHide()
    this.oPgFrm.Page5.oPag.oANSPRINT_5_23.visible=!this.oPgFrm.Page5.oPag.oANSPRINT_5_23.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_40.visible=!this.oPgFrm.Page5.oPag.oStr_5_40.mHide()
    this.oPgFrm.Page4.oPag.oANCATSCM_4_15.visible=!this.oPgFrm.Page4.oPag.oANCATSCM_4_15.mHide()
    this.oPgFrm.Page4.oPag.oDESSCM_4_16.visible=!this.oPgFrm.Page4.oPag.oDESSCM_4_16.mHide()
    this.oPgFrm.Page4.oPag.oAN1SCONT_4_23.visible=!this.oPgFrm.Page4.oPag.oAN1SCONT_4_23.mHide()
    this.oPgFrm.Page4.oPag.oAN2SCONT_4_24.visible=!this.oPgFrm.Page4.oPag.oAN2SCONT_4_24.mHide()
    this.oPgFrm.Page4.oPag.oANCONCON_4_26.visible=!this.oPgFrm.Page4.oPag.oANCONCON_4_26.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_35.visible=!this.oPgFrm.Page4.oPag.oStr_4_35.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_36.visible=!this.oPgFrm.Page4.oPag.oStr_4_36.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_37.visible=!this.oPgFrm.Page4.oPag.oStr_4_37.mHide()
    this.oPgFrm.Page4.oPag.oANMAGTER_4_38.visible=!this.oPgFrm.Page4.oPag.oANMAGTER_4_38.mHide()
    this.oPgFrm.Page4.oPag.oANCODESC_4_39.visible=!this.oPgFrm.Page4.oPag.oANCODESC_4_39.mHide()
    this.oPgFrm.Page4.oPag.oANCODPOR_4_40.visible=!this.oPgFrm.Page4.oPag.oANCODPOR_4_40.mHide()
    this.oPgFrm.Page4.oPag.oDESMAG_4_41.visible=!this.oPgFrm.Page4.oPag.oDESMAG_4_41.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_42.visible=!this.oPgFrm.Page4.oPag.oStr_4_42.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_43.visible=!this.oPgFrm.Page4.oPag.oStr_4_43.mHide()
    this.oPgFrm.Page4.oPag.oANMCALSI_4_44.visible=!this.oPgFrm.Page4.oPag.oANMCALSI_4_44.mHide()
    this.oPgFrm.Page4.oPag.oANMCALST_4_45.visible=!this.oPgFrm.Page4.oPag.oANMCALST_4_45.mHide()
    this.oPgFrm.Page4.oPag.oMSDESIMB_4_46.visible=!this.oPgFrm.Page4.oPag.oMSDESIMB_4_46.mHide()
    this.oPgFrm.Page4.oPag.oANSCORPO_4_47.visible=!this.oPgFrm.Page4.oPag.oANSCORPO_4_47.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_48.visible=!this.oPgFrm.Page4.oPag.oStr_4_48.mHide()
    this.oPgFrm.Page4.oPag.oANFLIMBA_4_50.visible=!this.oPgFrm.Page4.oPag.oANFLIMBA_4_50.mHide()
    this.oPgFrm.Page4.oPag.oMSDESTRA_4_52.visible=!this.oPgFrm.Page4.oPag.oMSDESTRA_4_52.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_53.visible=!this.oPgFrm.Page4.oPag.oStr_4_53.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_54.visible=!this.oPgFrm.Page4.oPag.oBtn_4_54.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_55.visible=!this.oPgFrm.Page4.oPag.oBtn_4_55.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_56.visible=!this.oPgFrm.Page4.oPag.oBtn_4_56.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_57.visible=!this.oPgFrm.Page4.oPag.oBtn_4_57.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_58.visible=!this.oPgFrm.Page4.oPag.oStr_4_58.mHide()
    this.oPgFrm.Page4.oPag.oANFLGCAU_4_65.visible=!this.oPgFrm.Page4.oPag.oANFLGCAU_4_65.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_66.visible=!this.oPgFrm.Page4.oPag.oStr_4_66.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_67.visible=!this.oPgFrm.Page4.oPag.oStr_4_67.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_68.visible=!this.oPgFrm.Page4.oPag.oStr_4_68.mHide()
    this.oPgFrm.Page4.oPag.oDESCRI_4_69.visible=!this.oPgFrm.Page4.oPag.oDESCRI_4_69.mHide()
    this.oPgFrm.Page4.oPag.oANFLAPCA_4_70.visible=!this.oPgFrm.Page4.oPag.oANFLAPCA_4_70.mHide()
    this.oPgFrm.Page4.oPag.oLinkPC_4_71.visible=!this.oPgFrm.Page4.oPag.oLinkPC_4_71.mHide()
    this.oPgFrm.Page2.oPag.oDESCAU_2_73.visible=!this.oPgFrm.Page2.oPag.oDESCAU_2_73.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_20.visible=!this.oPgFrm.Page3.oPag.oStr_3_20.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_24.visible=!this.oPgFrm.Page3.oPag.oStr_3_24.mHide()
    this.oPgFrm.Page3.oPag.oANCODASS_3_39.visible=!this.oPgFrm.Page3.oPag.oANCODASS_3_39.mHide()
    this.oPgFrm.Page3.oPag.oANCOIMPS_3_40.visible=!this.oPgFrm.Page3.oPag.oANCOIMPS_3_40.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_112.visible=!this.oPgFrm.Page1.oPag.oBtn_1_112.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_79.visible=!this.oPgFrm.Page2.oPag.oBtn_2_79.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_afr
    IF cevent = "ForzaRicalcolaStudio"
    this.w_ANCODSTU=''
    this.notifyevent ("RicalcolaStudio")
    ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_75.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_86.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_87.Event(cEvent)
        if lower(cEvent)==lower("AggiornaCF") or lower(cEvent)==lower("w_ANCODFIS LostFocus")
          .Calculate_XNLBETFNTC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Insert start")
          .Calculate_OLYGVFQXTR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete end")
          .Calculate_UIROVJBFQO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load") or lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_PGQNPFGCXZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_GRJFLMWOLU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CaricaDati") or lower(cEvent)==lower("RicalcolaStudio")
          .Calculate_QAXUOQUTMC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CaricaDati") or lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("Load")
          .Calculate_MZFNHIDLHB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CaricaDati") or lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("Load")
          .Calculate_XKIAONEMCL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Deleted")
          .Calculate_NTGPIXRFKL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ANSCORPO Changed")
          .Calculate_VISJNUHVVI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FornitoreVariato")
          .Calculate_JHBPFGHPWV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AUTOAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_lTable = "NUMAUT_M"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2], .t., this.NUMAUT_M_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AUTOAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AUTOAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NATIPGES,NACODAZI,NAACTIVE";
                   +" from "+i_cTable+" "+i_lTable+" where NACODAZI="+cp_ToStrODBC(this.w_AUTOAZI);
                   +" and NATIPGES="+cp_ToStrODBC('FO');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NATIPGES','FO';
                       ,'NACODAZI',this.w_AUTOAZI)
            select NATIPGES,NACODAZI,NAACTIVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AUTOAZI = NVL(_Link_.NACODAZI,space(5))
      this.w_AUTO = NVL(_Link_.NAACTIVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AUTOAZI = space(5)
      endif
      this.w_AUTO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])+'\'+cp_ToStr(_Link_.NATIPGES,1)+'\'+cp_ToStr(_Link_.NACODAZI,1)
      cp_ShowWarn(i_cKey,this.NUMAUT_M_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AUTOAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNAZION
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_ANNAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_ANNAZION))
          select NACODNAZ,NADESNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANNAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANNAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oANNAZION_1_16'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_ANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_ANNAZION)
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ = NVL(_Link_.NADESNAZ,space(35))
      this.w_CODISO = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ANNAZION = space(3)
      endif
      this.w_DESNAZ = space(35)
      this.w_CODISO = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.NACODNAZ as NACODNAZ116"+ ",link_1_16.NADESNAZ as NADESNAZ116"+ ",link_1_16.NACODISO as NACODISO116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on CONTI.ANNAZION=link_1_16.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and CONTI.ANNAZION=link_1_16.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODAZI1
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_lTable = "PAR_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2], .t., this.PAR_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODAZI,POMASCOF,POCATCOF,POCODPAF";
                   +" from "+i_cTable+" "+i_lTable+" where POCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODAZI',this.w_CODAZI1)
            select POCODAZI,POMASCOF,POCATCOF,POCODPAF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.POCODAZI,space(5))
      this.w_MASTRO = NVL(_Link_.POMASCOF,space(15))
      this.w_CATEGORIA = NVL(_Link_.POCATCOF,space(5))
      this.w_CODPAG = NVL(_Link_.POCODPAF,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_MASTRO = space(15)
      this.w_CATEGORIA = space(5)
      this.w_CODPAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.POCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCONSUP
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_ANCONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_ANCONSUP))
          select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_ANCONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_ANCONSUP)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oANCONSUP_2_5'),i_cWhere,'GSAR_AMC',"Mastri contabili (fornitori)",'GSAR_AFR.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_ANCONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_ANCONSUP)
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_DESSUP = NVL(_Link_.MCDESCRI,space(40))
      this.w_NULIV = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
      this.w_CODSTU = NVL(_Link_.MCCODSTU,space(3))
      this.w_PROSTU = NVL(_Link_.MCPROSTU,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_ANCONSUP = space(15)
      endif
      this.w_DESSUP = space(40)
      this.w_NULIV = 0
      this.w_TIPMAS = space(1)
      this.w_CODSTU = space(3)
      this.w_PROSTU = space(8)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIV=1 AND .w_TIPMAS='F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ANCONSUP = space(15)
        this.w_DESSUP = space(40)
        this.w_NULIV = 0
        this.w_TIPMAS = space(1)
        this.w_CODSTU = space(3)
        this.w_PROSTU = space(8)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.MCCODICE as MCCODICE205"+ ",link_2_5.MCDESCRI as MCDESCRI205"+ ",link_2_5.MCNUMLIV as MCNUMLIV205"+ ",link_2_5.MCTIPMAS as MCTIPMAS205"+ ",link_2_5.MCCODSTU as MCCODSTU205"+ ",link_2_5.MCPROSTU as MCPROSTU205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on CONTI.ANCONSUP=link_2_5.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and CONTI.ANCONSUP=link_2_5.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCONRIF
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCONRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ANCONRIF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPRIF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPRIF;
                     ,'ANCODICE',trim(this.w_ANCONRIF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCONRIF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_ANCONRIF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPRIF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_ANCONRIF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPRIF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCONRIF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oANCONRIF_2_8'),i_cWhere,'GSAR_BZC',"Cliente originario",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPRIF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPRIF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCONRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ANCONRIF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPRIF;
                       ,'ANCODICE',this.w_ANCONRIF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCONRIF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESORI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSORIF = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCONRIF = space(15)
      endif
      this.w_DESORI = space(40)
      this.w_DATOBSORIF = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSORIF) OR .w_DATOBSORIF>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_ANCONRIF = space(15)
        this.w_DESORI = space(40)
        this.w_DATOBSORIF = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCONRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCATCON
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_ANCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_ANCATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oANCATCON_2_10'),i_cWhere,'GSAR_AC2',"Categorie contabili fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_ANCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_ANCATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.C2CODICE as C2CODICE210"+ ",link_2_10.C2DESCRI as C2DESCRI210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on CONTI.ANCATCON=link_2_10.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and CONTI.ANCATCON=link_2_10.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODIVA
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_ANCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_ANCODIVA))
          select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_ANCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_ANCODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oANCODIVA_2_12'),i_cWhere,'GSAR_AIV',"Codici IVA",'GSVE0MDV.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_ANCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_ANCODIVA)
            select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
      this.w_DATOBSOIVA = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_PERIVA = 0
      this.w_DATOBSOIVA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSOIVA) OR .w_DATOBSOIVA>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
        endif
        this.w_ANCODIVA = space(5)
        this.w_DESIVA = space(35)
        this.w_PERIVA = 0
        this.w_DATOBSOIVA = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_12.IVCODIVA as IVCODIVA212"+ ",link_2_12.IVDESIVA as IVDESIVA212"+ ",link_2_12.IVPERIVA as IVPERIVA212"+ ",link_2_12.IVDTOBSO as IVDTOBSO212"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_12 on CONTI.ANCODIVA=link_2_12.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_12"
          i_cKey=i_cKey+'+" and CONTI.ANCODIVA=link_2_12.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANTIPOPE
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATO',True,'TIPCODIV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_ANTIPOPE)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);

          i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TI__TIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TI__TIPO',this.w_ANCATOPE;
                     ,'TICODICE',trim(this.w_ANTIPOPE))
          select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TI__TIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANTIPOPE)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANTIPOPE) and !this.bDontReportError
            deferred_cp_zoom('TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(oSource.parent,'oANTIPOPE_2_14'),i_cWhere,'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANCATOPE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_ANTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_ANCATOPE;
                       ,'TICODICE',this.w_ANTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_TIDESCRI = NVL(_Link_.TIDESCRI,space(30))
      this.w_TIDTOBSO = NVL(cp_ToDate(_Link_.TIDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANTIPOPE = space(10)
      endif
      this.w_TIDESCRI = space(30)
      this.w_TIDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TIDTOBSO) OR .w_TIDTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente oppure obsoleto")
        endif
        this.w_ANTIPOPE = space(10)
        this.w_TIDESCRI = space(30)
        this.w_TIDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCONCAU
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCONCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ANCONCAU)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCC);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCC;
                     ,'ANCODICE',trim(this.w_ANCONCAU))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCONCAU)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCONCAU) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oANCONCAU_2_16'),i_cWhere,'GSAR_BZC',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCONCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ANCONCAU);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCC;
                       ,'ANCODICE',this.w_ANCONCAU)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCONCAU = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCAU = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANCONCAU = space(15)
      endif
      this.w_DESCAU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCONCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODCAT
  func Link_2_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CATA_IDX,3]
    i_lTable = "COD_CATA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2], .t., this.COD_CATA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACC',True,'COD_CATA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_ANCODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_ANCODCAT))
          select CCCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODCAT)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODCAT) and !this.bDontReportError
            deferred_cp_zoom('COD_CATA','*','CCCODICE',cp_AbsName(oSource.parent,'oANCODCAT_2_34'),i_cWhere,'GSAR_ACC',"Elenco codici catastali dei comuni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_ANCODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_ANCODCAT)
            select CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODCAT = NVL(_Link_.CCCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODCAT = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_CATA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODGRU
  func Link_8_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_INTE_IDX,3]
    i_lTable = "GRU_INTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2], .t., this.GRU_INTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGR',True,'GRU_INTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GRCODICE like "+cp_ToStrODBC(trim(this.w_ANCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GRCODICE',trim(this.w_ANCODGRU))
          select GRCODICE,GRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODGRU)==trim(_Link_.GRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_INTE','*','GRCODICE',cp_AbsName(oSource.parent,'oANCODGRU_8_4'),i_cWhere,'GSAR_AGR',"Gruppi intestatari EDI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',oSource.xKey(1))
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(this.w_ANCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',this.w_ANCODGRU)
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODGRU = NVL(_Link_.GRCODICE,space(10))
      this.w_DESGRU = NVL(_Link_.GRDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODGRU = space(10)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])+'\'+cp_ToStr(_Link_.GRCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_INTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_8_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRU_INTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_8_4.GRCODICE as GRCODICE804"+ ",link_8_4.GRDESCRI as GRDESCRI804"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_8_4 on CONTI.ANCODGRU=link_8_4.GRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_8_4"
          i_cKey=i_cKey+'+" and CONTI.ANCODGRU=link_8_4.GRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODPAG
  func Link_5_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_ANCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_ANCODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_ANCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_ANCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_ANCODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oANCODPAG_5_4'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_ANCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_ANCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSOPAG = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSOPAG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSOPAG) OR .w_DATOBSOPAG>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_ANCODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSOPAG = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_4.PACODICE as PACODICE504"+ ","+cp_TransLinkFldName('link_5_4.PADESCRI')+" as PADESCRI504"+ ",link_5_4.PADTOBSO as PADTOBSO504"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_4 on CONTI.ANCODPAG=link_5_4.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_4"
          i_cKey=i_cKey+'+" and CONTI.ANCODPAG=link_5_4.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODBAN
  func Link_5_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_ANCODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_ANCODBAN))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODBAN) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oANCODBAN_5_15'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_ANCODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_ANCODBAN)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODBAN = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(42))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODBAN = space(10)
      endif
      this.w_DESBAN = space(42)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.BAN_CHE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_15.BACODBAN as BACODBAN515"+ ",link_5_15.BADESBAN as BADESBAN515"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_15 on CONTI.ANCODBAN=link_5_15.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_15"
          i_cKey=i_cKey+'+" and CONTI.ANCODBAN=link_5_15.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODBA2
  func Link_5_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODBA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_ANCODBA2)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_ANCODBA2))
          select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODBA2)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_ANCODBA2)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_ANCODBA2)+"%");

            select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODBA2) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oANCODBA2_5_17'),i_cWhere,'GSTE_ACB',"Elenco conti banche",'GSAR_ACL.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODBA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_ANCODBA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_ANCODBA2)
            select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODBA2 = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBA2 = NVL(_Link_.BADESCRI,space(42))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_CONSBF = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODBA2 = space(15)
      endif
      this.w_DESBA2 = space(42)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CONSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto o di tipo salvo buon fine")
        endif
        this.w_ANCODBA2 = space(15)
        this.w_DESBA2 = space(42)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CONSBF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODBA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_17.BACODBAN as BACODBAN517"+ ",link_5_17.BADESCRI as BADESCRI517"+ ",link_5_17.BADTOBSO as BADTOBSO517"+ ",link_5_17.BACONSBF as BACONSBF517"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_17 on CONTI.ANCODBA2=link_5_17.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_17"
          i_cKey=i_cKey+'+" and CONTI.ANCODBA2=link_5_17.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCATCOM
  func Link_4_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ANCATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ANCATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oANCATCOM_4_12'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ANCATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ANCATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCAC = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATCOM = space(3)
      endif
      this.w_DESCAC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATECOMM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_12.CTCODICE as CTCODICE412"+ ",link_4_12.CTDESCRI as CTDESCRI412"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_12 on CONTI.ANCATCOM=link_4_12.CTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_12"
          i_cKey=i_cKey+'+" and CONTI.ANCATCOM=link_4_12.CTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANGRPDEF
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRP_DEFA_IDX,3]
    i_lTable = "GRP_DEFA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2], .t., this.GRP_DEFA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANGRPDEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGD',True,'GRP_DEFA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GDCODICE like "+cp_ToStrODBC(trim(this.w_ANGRPDEF)+"%");

          i_ret=cp_SQL(i_nConn,"select GDCODICE,GDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GDCODICE',trim(this.w_ANGRPDEF))
          select GDCODICE,GDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANGRPDEF)==trim(_Link_.GDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANGRPDEF) and !this.bDontReportError
            deferred_cp_zoom('GRP_DEFA','*','GDCODICE',cp_AbsName(oSource.parent,'oANGRPDEF_4_13'),i_cWhere,'GSAR_AGD',"Gruppi output",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDCODICE,GDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDCODICE',oSource.xKey(1))
            select GDCODICE,GDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANGRPDEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDCODICE,GDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GDCODICE="+cp_ToStrODBC(this.w_ANGRPDEF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDCODICE',this.w_ANGRPDEF)
            select GDCODICE,GDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANGRPDEF = NVL(_Link_.GDCODICE,space(5))
      this.w_GDDESCRI = NVL(_Link_.GDDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANGRPDEF = space(5)
      endif
      this.w_GDDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2])+'\'+cp_ToStr(_Link_.GDCODICE,1)
      cp_ShowWarn(i_cKey,this.GRP_DEFA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANGRPDEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRP_DEFA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_13.GDCODICE as GDCODICE413"+ ",link_4_13.GDDESCRI as GDDESCRI413"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_13 on CONTI.ANGRPDEF=link_4_13.GDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_13"
          i_cKey=i_cKey+'+" and CONTI.ANGRPDEF=link_4_13.GDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCATSCM
  func Link_4_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATSCM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_ANCATSCM)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_ANCATSCM))
          select CSCODICE,CSDESCRI,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATSCM)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATSCM) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oANCATSCM_4_15'),i_cWhere,'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSAR_AFR.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATSCM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_ANCATSCM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_ANCATSCM)
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATSCM = NVL(_Link_.CSCODICE,space(5))
      this.w_DESSCM = NVL(_Link_.CSDESCRI,space(35))
      this.w_FLSCM = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATSCM = space(5)
      endif
      this.w_DESSCM = space(35)
      this.w_FLSCM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSCM $ ' C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria sconti / maggiorazioni inesistente o non di tipo fornitore")
        endif
        this.w_ANCATSCM = space(5)
        this.w_DESSCM = space(35)
        this.w_FLSCM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATSCM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_SCMA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_15.CSCODICE as CSCODICE415"+ ",link_4_15.CSDESCRI as CSDESCRI415"+ ",link_4_15.CSTIPCAT as CSTIPCAT415"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_15 on CONTI.ANCATSCM=link_4_15.CSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_15"
          i_cKey=i_cKey+'+" and CONTI.ANCATSCM=link_4_15.CSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODLIN
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_ANCODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_ANCODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oANCODLIN_4_17'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_ANCODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_ANCODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLIN = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODLIN = space(3)
      endif
      this.w_DESLIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LINGUE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_17.LUCODICE as LUCODICE417"+ ",link_4_17.LUDESCRI as LUDESCRI417"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_17 on CONTI.ANCODLIN=link_4_17.LUCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_17"
          i_cKey=i_cKey+'+" and CONTI.ANCODLIN=link_4_17.LUCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODVAL
  func Link_4_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_ANCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_ANCODVAL))
          select VACODVAL,VADESVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_ANCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_ANCODVAL)+"%");

            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oANCODVAL_4_19'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_ANCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_ANCODVAL)
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKVLCF(.w_ANCODVAL, .w_VALLIS, "FORNITORE",'X',' ') AND CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ANCODVAL = space(3)
        this.w_DESVAL = space(35)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_19.VACODVAL as VACODVAL419"+ ",link_4_19.VADESVAL as VADESVAL419"+ ",link_4_19.VADTOBSO as VADTOBSO419"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_19 on CONTI.ANCODVAL=link_4_19.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_19"
          i_cKey=i_cKey+'+" and CONTI.ANCODVAL=link_4_19.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANNUMLIS
  func Link_4_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNUMLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ANNUMLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ANNUMLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANNUMLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_ANNUMLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_ANNUMLIS)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANNUMLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oANNUMLIS_4_21'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNUMLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ANNUMLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ANNUMLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNUMLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANNUMLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_IVALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKVLCF(.w_ANCODVAL, .w_VALLIS, "FORNITORE", .w_ANSCORPO, .w_IVALIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ANNUMLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_VALLIS = space(3)
        this.w_IVALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNUMLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_21.LSCODLIS as LSCODLIS421"+ ",link_4_21.LSDESLIS as LSDESLIS421"+ ",link_4_21.LSVALLIS as LSVALLIS421"+ ",link_4_21.LSIVALIS as LSIVALIS421"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_21 on CONTI.ANNUMLIS=link_4_21.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_21"
          i_cKey=i_cKey+'+" and CONTI.ANNUMLIS=link_4_21.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODZON
  func Link_4_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_ANCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_ANCODZON))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oANCODZON_4_25'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_ANCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_ANCODZON)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
      this.w_DATOBSOZON = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODZON = space(3)
      endif
      this.w_DESZON = space(35)
      this.w_DATOBSOZON = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSOZON) OR .w_DATOBSOZON>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_ANCODZON = space(3)
        this.w_DESZON = space(35)
        this.w_DATOBSOZON = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ZONE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_25.ZOCODZON as ZOCODZON425"+ ",link_4_25.ZODESZON as ZODESZON425"+ ",link_4_25.ZODTOBSO as ZODTOBSO425"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_25 on CONTI.ANCODZON=link_4_25.ZOCODZON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_25"
          i_cKey=i_cKey+'+" and CONTI.ANCODZON=link_4_25.ZOCODZON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCONCON
  func Link_4_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_CONS_IDX,3]
    i_lTable = "CON_CONS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_CONS_IDX,2], .t., this.CON_CONS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_CONS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCONCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CON_CONS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COCODICE like "+cp_ToStrODBC(trim(this.w_ANCONCON)+"%");
                   +" and CCCODISO="+cp_ToStrODBC(this.w_ANCODISO);

          i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODISO,COCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODISO',this.w_ANCODISO;
                     ,'COCODICE',trim(this.w_ANCONCON))
          select CCCODISO,COCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODISO,COCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCONCON)==trim(_Link_.COCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCONCON) and !this.bDontReportError
            deferred_cp_zoom('CON_CONS','*','CCCODISO,COCODICE',cp_AbsName(oSource.parent,'oANCONCON_4_26'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANCODISO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCCODISO,COCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where COCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CCCODISO="+cp_ToStrODBC(this.w_ANCODISO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODISO',oSource.xKey(1);
                       ,'COCODICE',oSource.xKey(2))
            select CCCODISO,COCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCONCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where COCODICE="+cp_ToStrODBC(this.w_ANCONCON);
                   +" and CCCODISO="+cp_ToStrODBC(this.w_ANCODISO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODISO',this.w_ANCODISO;
                       ,'COCODICE',this.w_ANCONCON)
            select CCCODISO,COCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCONCON = NVL(_Link_.COCODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCONCON = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_CONS_IDX,2])+'\'+cp_ToStr(_Link_.CCCODISO,1)+'\'+cp_ToStr(_Link_.COCODICE,1)
      cp_ShowWarn(i_cKey,this.CON_CONS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCONCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANMAGTER
  func Link_4_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMAGTER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_ANMAGTER)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_ANMAGTER))
          select MGCODMAG,MGDESMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMAGTER)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMAGTER) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oANMAGTER_4_38'),i_cWhere,'GSAR_AMA',"Magazzini",'GSCOWMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMAGTER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_ANMAGTER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_ANMAGTER)
            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMAGTER = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_MAGWIP = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANMAGTER = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_MAGWIP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_ANMAGTER) OR .w_MAGWIP='W'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino C/Lavoro inesistente o non tipo WIP")
        endif
        this.w_ANMAGTER = space(5)
        this.w_DESMAG = space(30)
        this.w_MAGWIP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMAGTER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_38.MGCODMAG as MGCODMAG438"+ ",link_4_38.MGDESMAG as MGDESMAG438"+ ",link_4_38.MGTIPMAG as MGTIPMAG438"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_38 on CONTI.ANMAGTER=link_4_38.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_38"
          i_cKey=i_cKey+'+" and CONTI.ANMAGTER=link_4_38.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODPOR
  func Link_4_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_AREO_IDX,3]
    i_lTable = "COD_AREO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2], .t., this.COD_AREO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GLES_APP',True,'COD_AREO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PPCODICE like "+cp_ToStrODBC(trim(this.w_ANCODPOR)+"%");

          i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PPCODICE',trim(this.w_ANCODPOR))
          select PPCODICE,PPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODPOR)==trim(_Link_.PPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODPOR) and !this.bDontReportError
            deferred_cp_zoom('COD_AREO','*','PPCODICE',cp_AbsName(oSource.parent,'oANCODPOR_4_40'),i_cWhere,'GLES_APP',"Porti/aeroporti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',oSource.xKey(1))
            select PPCODICE,PPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_ANCODPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_ANCODPOR)
            select PPCODICE,PPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODPOR = NVL(_Link_.PPCODICE,space(10))
      this.w_DESCRI = NVL(_Link_.PPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODPOR = space(10)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_AREO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_AREO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_40.PPCODICE as PPCODICE440"+ ",link_4_40.PPDESCRI as PPDESCRI440"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_40 on CONTI.ANCODPOR=link_4_40.PPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_40"
          i_cKey=i_cKey+'+" and CONTI.ANCODPOR=link_4_40.PPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANMCALSI
  func Link_4_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMCALSI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_ANMCALSI)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_ANMCALSI))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMCALSI)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMCALSI) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oANMCALSI_4_44'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMCALSI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_ANMCALSI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_ANMCALSI)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMCALSI = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESIMB = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANMCALSI = space(5)
      endif
      this.w_MSDESIMB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMCALSI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_44(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_44.MSCODICE as MSCODICE444"+ ",link_4_44.MSDESCRI as MSDESCRI444"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_44 on CONTI.ANMCALSI=link_4_44.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_44"
          i_cKey=i_cKey+'+" and CONTI.ANMCALSI=link_4_44.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANMCALST
  func Link_4_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMCALST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_ANMCALST)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_ANMCALST))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMCALST)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMCALST) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oANMCALST_4_45'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMCALST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_ANMCALST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_ANMCALST)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMCALST = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESTRA = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANMCALST = space(5)
      endif
      this.w_MSDESTRA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMCALST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_45(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_45.MSCODICE as MSCODICE445"+ ",link_4_45.MSDESCRI as MSDESCRI445"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_45 on CONTI.ANMCALST=link_4_45.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_45"
          i_cKey=i_cKey+'+" and CONTI.ANMCALST=link_4_45.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODIRP
  func Link_3_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODIRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_ANCODIRP)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_ANCODIRP))
          select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODIRP)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODIRP) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oANCODIRP_3_32'),i_cWhere,'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODIRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_ANCODIRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_ANCODIRP)
            select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODIRP = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESIRPEF = NVL(_Link_.TRDESTRI,space(35))
      this.w_FLIRPE = NVL(_Link_.TRFLACON,space(1))
      this.w_CAUPRE2 = NVL(_Link_.TRCAUPRE2,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODIRP = space(5)
      endif
      this.w_DESIRPEF = space(35)
      this.w_FLIRPE = space(1)
      this.w_CAUPRE2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLIRPE='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
        endif
        this.w_ANCODIRP = space(5)
        this.w_DESIRPEF = space(35)
        this.w_FLIRPE = space(1)
        this.w_CAUPRE2 = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODIRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRI_BUTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_32.TRCODTRI as TRCODTRI332"+ ",link_3_32.TRDESTRI as TRDESTRI332"+ ",link_3_32.TRFLACON as TRFLACON332"+ ",link_3_32.TRCAUPRE2 as TRCAUPRE2332"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_32 on CONTI.ANCODIRP=link_3_32.TRCODTRI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_32"
          i_cKey=i_cKey+'+" and CONTI.ANCODIRP=link_3_32.TRCODTRI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODTR2
  func Link_3_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODTR2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_ANCODTR2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_ANCODTR2))
          select TRCODTRI,TRDESTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODTR2)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODTR2) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oANCODTR2_3_33'),i_cWhere,'GSAR_ATB',"",'GSRI2AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODTR2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_ANCODTR2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_ANCODTR2)
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODTR2 = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESTR2 = NVL(_Link_.TRDESTRI,space(35))
      this.w_FLACO2 = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODTR2 = space(5)
      endif
      this.w_DESTR2 = space(35)
      this.w_FLACO2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLACO2<>'R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ANCODTR2 = space(5)
        this.w_DESTR2 = space(35)
        this.w_FLACO2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODTR2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_33(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRI_BUTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_33.TRCODTRI as TRCODTRI333"+ ",link_3_33.TRDESTRI as TRDESTRI333"+ ",link_3_33.TRFLACON as TRFLACON333"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_33 on CONTI.ANCODTR2=link_3_33.TRCODTRI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_33"
          i_cKey=i_cKey+'+" and CONTI.ANCODTR2=link_3_33.TRCODTRI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANRATING
  func Link_9_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_lTable = "DORATING"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2], .t., this.DORATING_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANRATING) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsdf_Ara',True,'DORATING')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RACODICE like "+cp_ToStrODBC(trim(this.w_ANRATING)+"%");

          i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RACODICE',trim(this.w_ANRATING))
          select RACODICE,RADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANRATING)==trim(_Link_.RACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANRATING) and !this.bDontReportError
            deferred_cp_zoom('DORATING','*','RACODICE',cp_AbsName(oSource.parent,'oANRATING_9_7'),i_cWhere,'Gsdf_Ara',"Rating",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',oSource.xKey(1))
            select RACODICE,RADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANRATING)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(this.w_ANRATING);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',this.w_ANRATING)
            select RACODICE,RADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANRATING = NVL(_Link_.RACODICE,space(2))
      this.w_RADESCRI = NVL(_Link_.RADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ANRATING = space(2)
      endif
      this.w_RADESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])+'\'+cp_ToStr(_Link_.RACODICE,1)
      cp_ShowWarn(i_cKey,this.DORATING_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANRATING Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_9_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DORATING_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
      i_cNewSel = i_cSel+ ",link_9_7.RACODICE as RACODICE907"+ ",link_9_7.RADESCRI as RADESCRI907"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_9_7 on CONTI.ANRATING=link_9_7.RACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_9_7"
          i_cKey=i_cKey+'+" and CONTI.ANRATING=link_9_7.RACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANVOCFIN
  func Link_9_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_FINZ_IDX,3]
    i_lTable = "VOC_FINZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2], .t., this.VOC_FINZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANVOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDF_AVF',True,'VOC_FINZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DFVOCFIN like "+cp_ToStrODBC(trim(this.w_ANVOCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DFVOCFIN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DFVOCFIN',trim(this.w_ANVOCFIN))
          select DFVOCFIN,DFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DFVOCFIN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANVOCFIN)==trim(_Link_.DFVOCFIN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DFDESCRI like "+cp_ToStrODBC(trim(this.w_ANVOCFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DFDESCRI like "+cp_ToStr(trim(this.w_ANVOCFIN)+"%");

            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANVOCFIN) and !this.bDontReportError
            deferred_cp_zoom('VOC_FINZ','*','DFVOCFIN',cp_AbsName(oSource.parent,'oANVOCFIN_9_9'),i_cWhere,'GSDF_AVF',"Voci finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',oSource.xKey(1))
            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANVOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(this.w_ANVOCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',this.w_ANVOCFIN)
            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANVOCFIN = NVL(_Link_.DFVOCFIN,space(6))
      this.w_DFDESCRI = NVL(_Link_.DFDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ANVOCFIN = space(6)
      endif
      this.w_DFDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])+'\'+cp_ToStr(_Link_.DFVOCFIN,1)
      cp_ShowWarn(i_cKey,this.VOC_FINZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANVOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_9_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_FINZ_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_9_9.DFVOCFIN as DFVOCFIN909"+ ",link_9_9.DFDESCRI as DFDESCRI909"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_9_9 on CONTI.ANVOCFIN=link_9_9.DFVOCFIN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_9_9"
          i_cKey=i_cKey+'+" and CONTI.ANVOCFIN=link_9_9.DFVOCFIN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODREG
  func Link_3_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODREG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_ANCODREG)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE,RPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_ANCODREG))
          select RPCODICE,RPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODREG)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODREG) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oANCODREG_3_53'),i_cWhere,'GSCG_ARP',"Codici regioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE,RPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE,RPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODREG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE,RPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_ANCODREG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_ANCODREG)
            select RPCODICE,RPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODREG = NVL(_Link_.RPCODICE,space(2))
      this.w_DESCREG = NVL(_Link_.RPDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODREG = space(2)
      endif
      this.w_DESCREG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODREG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_53(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.REG_PROV_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_53.RPCODICE as RPCODICE353"+ ",link_3_53.RPDESCRI as RPDESCRI353"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_53 on CONTI.ANCODREG=link_3_53.RPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_53"
          i_cKey=i_cKey+'+" and CONTI.ANCODREG=link_3_53.RPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODCOM
  func Link_3_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_COM_IDX,3]
    i_lTable = "ENTI_COM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2], .t., this.ENTI_COM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AEC',True,'ENTI_COM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCODICE like "+cp_ToStrODBC(trim(this.w_ANCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCODICE,ECTERRIT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCODICE',trim(this.w_ANCODCOM))
          select ECCODICE,ECTERRIT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODCOM)==trim(_Link_.ECCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODCOM) and !this.bDontReportError
            deferred_cp_zoom('ENTI_COM','*','ECCODICE',cp_AbsName(oSource.parent,'oANCODCOM_3_54'),i_cWhere,'GSCG_AEC',"CODICE ENTI/COMUNI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE,ECTERRIT";
                     +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',oSource.xKey(1))
            select ECCODICE,ECTERRIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE,ECTERRIT";
                   +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(this.w_ANCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',this.w_ANCODCOM)
            select ECCODICE,ECTERRIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODCOM = NVL(_Link_.ECCODICE,space(4))
      this.w_ECTERRIT = NVL(_Link_.ECTERRIT,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODCOM = space(4)
      endif
      this.w_ECTERRIT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])+'\'+cp_ToStr(_Link_.ECCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_COM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_54(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ENTI_COM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_54.ECCODICE as ECCODICE354"+ ",link_3_54.ECTERRIT as ECTERRIT354"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_54 on CONTI.ANCODCOM=link_3_54.ECCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_54"
          i_cKey=i_cKey+'+" and CONTI.ANCODCOM=link_3_54.ECCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANCODICE_1_4.value==this.w_ANCODICE)
      this.oPgFrm.Page1.oPag.oANCODICE_1_4.value=this.w_ANCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_5.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_5.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANTIPCLF_1_6.RadioValue()==this.w_ANTIPCLF)
      this.oPgFrm.Page1.oPag.oANTIPCLF_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCR2_1_7.value==this.w_ANDESCR2)
      this.oPgFrm.Page1.oPag.oANDESCR2_1_7.value=this.w_ANDESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oANINDIRI_1_11.value==this.w_ANINDIRI)
      this.oPgFrm.Page1.oPag.oANINDIRI_1_11.value=this.w_ANINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANINDIR2_1_12.value==this.w_ANINDIR2)
      this.oPgFrm.Page1.oPag.oANINDIR2_1_12.value=this.w_ANINDIR2
    endif
    if not(this.oPgFrm.Page1.oPag.oAN___CAP_1_13.value==this.w_AN___CAP)
      this.oPgFrm.Page1.oPag.oAN___CAP_1_13.value=this.w_AN___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oANLOCALI_1_14.value==this.w_ANLOCALI)
      this.oPgFrm.Page1.oPag.oANLOCALI_1_14.value=this.w_ANLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oANPROVIN_1_15.value==this.w_ANPROVIN)
      this.oPgFrm.Page1.oPag.oANPROVIN_1_15.value=this.w_ANPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oANNAZION_1_16.value==this.w_ANNAZION)
      this.oPgFrm.Page1.oPag.oANNAZION_1_16.value=this.w_ANNAZION
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNAZ_1_17.value==this.w_DESNAZ)
      this.oPgFrm.Page1.oPag.oDESNAZ_1_17.value=this.w_DESNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODFIS_1_18.value==this.w_ANCODFIS)
      this.oPgFrm.Page1.oPag.oANCODFIS_1_18.value=this.w_ANCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oANPARIVA_1_19.value==this.w_ANPARIVA)
      this.oPgFrm.Page1.oPag.oANPARIVA_1_19.value=this.w_ANPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oANTELEFO_1_20.value==this.w_ANTELEFO)
      this.oPgFrm.Page1.oPag.oANTELEFO_1_20.value=this.w_ANTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oANTELFAX_1_21.value==this.w_ANTELFAX)
      this.oPgFrm.Page1.oPag.oANTELFAX_1_21.value=this.w_ANTELFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oANNUMCEL_1_22.value==this.w_ANNUMCEL)
      this.oPgFrm.Page1.oPag.oANNUMCEL_1_22.value=this.w_ANNUMCEL
    endif
    if not(this.oPgFrm.Page1.oPag.oANINDWEB_1_23.value==this.w_ANINDWEB)
      this.oPgFrm.Page1.oPag.oANINDWEB_1_23.value=this.w_ANINDWEB
    endif
    if not(this.oPgFrm.Page1.oPag.oAN_EMAIL_1_24.value==this.w_AN_EMAIL)
      this.oPgFrm.Page1.oPag.oAN_EMAIL_1_24.value=this.w_AN_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oAN_EMPEC_1_25.value==this.w_AN_EMPEC)
      this.oPgFrm.Page1.oPag.oAN_EMPEC_1_25.value=this.w_AN_EMPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oANPERFIS_1_26.RadioValue()==this.w_ANPERFIS)
      this.oPgFrm.Page1.oPag.oANPERFIS_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAN_SESSO_1_27.RadioValue()==this.w_AN_SESSO)
      this.oPgFrm.Page1.oPag.oAN_SESSO_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCOGNOM_1_28.value==this.w_ANCOGNOM)
      this.oPgFrm.Page1.oPag.oANCOGNOM_1_28.value=this.w_ANCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oAN__NOME_1_29.value==this.w_AN__NOME)
      this.oPgFrm.Page1.oPag.oAN__NOME_1_29.value=this.w_AN__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oANLOCNAS_1_30.value==this.w_ANLOCNAS)
      this.oPgFrm.Page1.oPag.oANLOCNAS_1_30.value=this.w_ANLOCNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oANPRONAS_1_31.value==this.w_ANPRONAS)
      this.oPgFrm.Page1.oPag.oANPRONAS_1_31.value=this.w_ANPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oANDATNAS_1_32.value==this.w_ANDATNAS)
      this.oPgFrm.Page1.oPag.oANDATNAS_1_32.value=this.w_ANDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oANNUMCAR_1_33.value==this.w_ANNUMCAR)
      this.oPgFrm.Page1.oPag.oANNUMCAR_1_33.value=this.w_ANNUMCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODISO_1_50.value==this.w_CODISO)
      this.oPgFrm.Page1.oPag.oCODISO_1_50.value=this.w_CODISO
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKSTA_1_57.RadioValue()==this.w_ANCHKSTA)
      this.oPgFrm.Page1.oPag.oANCHKSTA_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKMAI_1_58.RadioValue()==this.w_ANCHKMAI)
      this.oPgFrm.Page1.oPag.oANCHKMAI_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKPEC_1_59.RadioValue()==this.w_ANCHKPEC)
      this.oPgFrm.Page1.oPag.oANCHKPEC_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKFAX_1_60.RadioValue()==this.w_ANCHKFAX)
      this.oPgFrm.Page1.oPag.oANCHKFAX_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.RadioValue()==this.w_ANCHKCPZ)
      this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKWWP_1_62.RadioValue()==this.w_ANCHKWWP)
      this.oPgFrm.Page1.oPag.oANCHKWWP_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKPTL_1_63.RadioValue()==this.w_ANCHKPTL)
      this.oPgFrm.Page1.oPag.oANCHKPTL_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.RadioValue()==this.w_ANCHKFIR)
      this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODEST_1_65.value==this.w_ANCODEST)
      this.oPgFrm.Page1.oPag.oANCODEST_1_65.value=this.w_ANCODEST
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODCLA_1_66.value==this.w_ANCODCLA)
      this.oPgFrm.Page1.oPag.oANCODCLA_1_66.value=this.w_ANCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODPEC_1_67.value==this.w_ANCODPEC)
      this.oPgFrm.Page1.oPag.oANCODPEC_1_67.value=this.w_ANCODPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oANDTOBSO_1_68.value==this.w_ANDTOBSO)
      this.oPgFrm.Page1.oPag.oANDTOBSO_1_68.value=this.w_ANDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oANDTINVA_1_73.value==this.w_ANDTINVA)
      this.oPgFrm.Page1.oPag.oANDTINVA_1_73.value=this.w_ANDTINVA
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_2.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDES1_2_3.value==this.w_DES1)
      this.oPgFrm.Page2.oPag.oDES1_2_3.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page2.oPag.oANCONSUP_2_5.value==this.w_ANCONSUP)
      this.oPgFrm.Page2.oPag.oANCONSUP_2_5.value=this.w_ANCONSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSUP_2_6.value==this.w_DESSUP)
      this.oPgFrm.Page2.oPag.oDESSUP_2_6.value=this.w_DESSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oANCONRIF_2_8.value==this.w_ANCONRIF)
      this.oPgFrm.Page2.oPag.oANCONRIF_2_8.value=this.w_ANCONRIF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESORI_2_9.value==this.w_DESORI)
      this.oPgFrm.Page2.oPag.oDESORI_2_9.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page2.oPag.oANCATCON_2_10.value==this.w_ANCATCON)
      this.oPgFrm.Page2.oPag.oANCATCON_2_10.value=this.w_ANCATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON_2_11.value==this.w_DESCON)
      this.oPgFrm.Page2.oPag.oDESCON_2_11.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oANCODIVA_2_12.value==this.w_ANCODIVA)
      this.oPgFrm.Page2.oPag.oANCODIVA_2_12.value=this.w_ANCODIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESIVA_2_13.value==this.w_DESIVA)
      this.oPgFrm.Page2.oPag.oDESIVA_2_13.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oANTIPOPE_2_14.value==this.w_ANTIPOPE)
      this.oPgFrm.Page2.oPag.oANTIPOPE_2_14.value=this.w_ANTIPOPE
    endif
    if not(this.oPgFrm.Page2.oPag.oTIDESCRI_2_15.value==this.w_TIDESCRI)
      this.oPgFrm.Page2.oPag.oTIDESCRI_2_15.value=this.w_TIDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oANCONCAU_2_16.value==this.w_ANCONCAU)
      this.oPgFrm.Page2.oPag.oANCONCAU_2_16.value=this.w_ANCONCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oANOPETRE_2_17.RadioValue()==this.w_ANOPETRE)
      this.oPgFrm.Page2.oPag.oANOPETRE_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANTIPPRE_2_18.RadioValue()==this.w_ANTIPPRE)
      this.oPgFrm.Page2.oPag.oANTIPPRE_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMINCON_2_22.value==this.w_MINCON)
      this.oPgFrm.Page2.oPag.oMINCON_2_22.value=this.w_MINCON
    endif
    if not(this.oPgFrm.Page2.oPag.oANPARTSN_2_23.RadioValue()==this.w_ANPARTSN)
      this.oPgFrm.Page2.oPag.oANPARTSN_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLAACC_2_24.RadioValue()==this.w_ANFLAACC)
      this.oPgFrm.Page2.oPag.oANFLAACC_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMAXCON_2_25.value==this.w_MAXCON)
      this.oPgFrm.Page2.oPag.oMAXCON_2_25.value=this.w_MAXCON
    endif
    if not(this.oPgFrm.Page2.oPag.oAFFLINTR_2_26.RadioValue()==this.w_AFFLINTR)
      this.oPgFrm.Page2.oPag.oAFFLINTR_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLBLLS_2_27.RadioValue()==this.w_ANFLBLLS)
      this.oPgFrm.Page2.oPag.oANFLBLLS_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLSOAL_2_28.RadioValue()==this.w_ANFLSOAL)
      this.oPgFrm.Page2.oPag.oANFLSOAL_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLBODO_2_29.RadioValue()==this.w_ANFLBODO)
      this.oPgFrm.Page2.oPag.oANFLBODO_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANIVASOS_2_30.RadioValue()==this.w_ANIVASOS)
      this.oPgFrm.Page2.oPag.oANIVASOS_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANCODSTU_2_31.value==this.w_ANCODSTU)
      this.oPgFrm.Page2.oPag.oANCODSTU_2_31.value=this.w_ANCODSTU
    endif
    if not(this.oPgFrm.Page2.oPag.oANCODSOG_2_33.value==this.w_ANCODSOG)
      this.oPgFrm.Page2.oPag.oANCODSOG_2_33.value=this.w_ANCODSOG
    endif
    if not(this.oPgFrm.Page2.oPag.oANCODCAT_2_34.value==this.w_ANCODCAT)
      this.oPgFrm.Page2.oPag.oANCODCAT_2_34.value=this.w_ANCODCAT
    endif
    if not(this.oPgFrm.Page8.oPag.oDES1_8_1.value==this.w_DES1)
      this.oPgFrm.Page8.oPag.oDES1_8_1.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page8.oPag.oCODI_8_3.value==this.w_CODI)
      this.oPgFrm.Page8.oPag.oCODI_8_3.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page8.oPag.oANCODGRU_8_4.value==this.w_ANCODGRU)
      this.oPgFrm.Page8.oPag.oANCODGRU_8_4.value=this.w_ANCODGRU
    endif
    if not(this.oPgFrm.Page8.oPag.oDESGRU_8_7.value==this.w_DESGRU)
      this.oPgFrm.Page8.oPag.oDESGRU_8_7.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page7.oPag.oCODI_7_1.value==this.w_CODI)
      this.oPgFrm.Page7.oPag.oCODI_7_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page7.oPag.oDES1_7_2.value==this.w_DES1)
      this.oPgFrm.Page7.oPag.oDES1_7_2.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page6.oPag.oCODI_6_1.value==this.w_CODI)
      this.oPgFrm.Page6.oPag.oCODI_6_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page6.oPag.oDES1_6_2.value==this.w_DES1)
      this.oPgFrm.Page6.oPag.oDES1_6_2.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page5.oPag.oCODI_5_1.value==this.w_CODI)
      this.oPgFrm.Page5.oPag.oCODI_5_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page5.oPag.oDES1_5_2.value==this.w_DES1)
      this.oPgFrm.Page5.oPag.oDES1_5_2.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page5.oPag.oANCODPAG_5_4.value==this.w_ANCODPAG)
      this.oPgFrm.Page5.oPag.oANCODPAG_5_4.value=this.w_ANCODPAG
    endif
    if not(this.oPgFrm.Page5.oPag.oDESPAG_5_5.value==this.w_DESPAG)
      this.oPgFrm.Page5.oPag.oDESPAG_5_5.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page5.oPag.oANGIOFIS_5_6.value==this.w_ANGIOFIS)
      this.oPgFrm.Page5.oPag.oANGIOFIS_5_6.value=this.w_ANGIOFIS
    endif
    if not(this.oPgFrm.Page5.oPag.oAN1MESCL_5_7.value==this.w_AN1MESCL)
      this.oPgFrm.Page5.oPag.oAN1MESCL_5_7.value=this.w_AN1MESCL
    endif
    if not(this.oPgFrm.Page5.oPag.oDESMES1_5_8.value==this.w_DESMES1)
      this.oPgFrm.Page5.oPag.oDESMES1_5_8.value=this.w_DESMES1
    endif
    if not(this.oPgFrm.Page5.oPag.oANGIOSC1_5_9.value==this.w_ANGIOSC1)
      this.oPgFrm.Page5.oPag.oANGIOSC1_5_9.value=this.w_ANGIOSC1
    endif
    if not(this.oPgFrm.Page5.oPag.oAN2MESCL_5_10.value==this.w_AN2MESCL)
      this.oPgFrm.Page5.oPag.oAN2MESCL_5_10.value=this.w_AN2MESCL
    endif
    if not(this.oPgFrm.Page5.oPag.oDESMES2_5_13.value==this.w_DESMES2)
      this.oPgFrm.Page5.oPag.oDESMES2_5_13.value=this.w_DESMES2
    endif
    if not(this.oPgFrm.Page5.oPag.oANGIOSC2_5_14.value==this.w_ANGIOSC2)
      this.oPgFrm.Page5.oPag.oANGIOSC2_5_14.value=this.w_ANGIOSC2
    endif
    if not(this.oPgFrm.Page5.oPag.oANCODBAN_5_15.value==this.w_ANCODBAN)
      this.oPgFrm.Page5.oPag.oANCODBAN_5_15.value=this.w_ANCODBAN
    endif
    if not(this.oPgFrm.Page5.oPag.oDESBAN_5_16.value==this.w_DESBAN)
      this.oPgFrm.Page5.oPag.oDESBAN_5_16.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page5.oPag.oANCODBA2_5_17.value==this.w_ANCODBA2)
      this.oPgFrm.Page5.oPag.oANCODBA2_5_17.value=this.w_ANCODBA2
    endif
    if not(this.oPgFrm.Page5.oPag.oANFLINCA_5_18.RadioValue()==this.w_ANFLINCA)
      this.oPgFrm.Page5.oPag.oANFLINCA_5_18.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oANSPEINC_5_19.value==this.w_ANSPEINC)
      this.oPgFrm.Page5.oPag.oANSPEINC_5_19.value=this.w_ANSPEINC
    endif
    if not(this.oPgFrm.Page5.oPag.oANFLESIM_5_21.RadioValue()==this.w_ANFLESIM)
      this.oPgFrm.Page5.oPag.oANFLESIM_5_21.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oANSAGINT_5_22.value==this.w_ANSAGINT)
      this.oPgFrm.Page5.oPag.oANSAGINT_5_22.value=this.w_ANSAGINT
    endif
    if not(this.oPgFrm.Page5.oPag.oANSPRINT_5_23.RadioValue()==this.w_ANSPRINT)
      this.oPgFrm.Page5.oPag.oANSPRINT_5_23.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oANFLACBD_5_24.RadioValue()==this.w_ANFLACBD)
      this.oPgFrm.Page5.oPag.oANFLACBD_5_24.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oANFLRAGG_5_25.RadioValue()==this.w_ANFLRAGG)
      this.oPgFrm.Page5.oPag.oANFLRAGG_5_25.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oANFLGAVV_5_26.RadioValue()==this.w_ANFLGAVV)
      this.oPgFrm.Page5.oPag.oANFLGAVV_5_26.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oANDATAVV_5_27.value==this.w_ANDATAVV)
      this.oPgFrm.Page5.oPag.oANDATAVV_5_27.value=this.w_ANDATAVV
    endif
    if not(this.oPgFrm.Page5.oPag.oDESBA2_5_34.value==this.w_DESBA2)
      this.oPgFrm.Page5.oPag.oDESBA2_5_34.value=this.w_DESBA2
    endif
    if not(this.oPgFrm.Page4.oPag.oCODI_4_10.value==this.w_CODI)
      this.oPgFrm.Page4.oPag.oCODI_4_10.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page4.oPag.oDES1_4_11.value==this.w_DES1)
      this.oPgFrm.Page4.oPag.oDES1_4_11.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page4.oPag.oANCATCOM_4_12.value==this.w_ANCATCOM)
      this.oPgFrm.Page4.oPag.oANCATCOM_4_12.value=this.w_ANCATCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oANGRPDEF_4_13.value==this.w_ANGRPDEF)
      this.oPgFrm.Page4.oPag.oANGRPDEF_4_13.value=this.w_ANGRPDEF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCAC_4_14.value==this.w_DESCAC)
      this.oPgFrm.Page4.oPag.oDESCAC_4_14.value=this.w_DESCAC
    endif
    if not(this.oPgFrm.Page4.oPag.oANCATSCM_4_15.value==this.w_ANCATSCM)
      this.oPgFrm.Page4.oPag.oANCATSCM_4_15.value=this.w_ANCATSCM
    endif
    if not(this.oPgFrm.Page4.oPag.oDESSCM_4_16.value==this.w_DESSCM)
      this.oPgFrm.Page4.oPag.oDESSCM_4_16.value=this.w_DESSCM
    endif
    if not(this.oPgFrm.Page4.oPag.oANCODLIN_4_17.value==this.w_ANCODLIN)
      this.oPgFrm.Page4.oPag.oANCODLIN_4_17.value=this.w_ANCODLIN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESLIN_4_18.value==this.w_DESLIN)
      this.oPgFrm.Page4.oPag.oDESLIN_4_18.value=this.w_DESLIN
    endif
    if not(this.oPgFrm.Page4.oPag.oANCODVAL_4_19.value==this.w_ANCODVAL)
      this.oPgFrm.Page4.oPag.oANCODVAL_4_19.value=this.w_ANCODVAL
    endif
    if not(this.oPgFrm.Page4.oPag.oDESVAL_4_20.value==this.w_DESVAL)
      this.oPgFrm.Page4.oPag.oDESVAL_4_20.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page4.oPag.oANNUMLIS_4_21.value==this.w_ANNUMLIS)
      this.oPgFrm.Page4.oPag.oANNUMLIS_4_21.value=this.w_ANNUMLIS
    endif
    if not(this.oPgFrm.Page4.oPag.oDESLIS_4_22.value==this.w_DESLIS)
      this.oPgFrm.Page4.oPag.oDESLIS_4_22.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page4.oPag.oAN1SCONT_4_23.value==this.w_AN1SCONT)
      this.oPgFrm.Page4.oPag.oAN1SCONT_4_23.value=this.w_AN1SCONT
    endif
    if not(this.oPgFrm.Page4.oPag.oAN2SCONT_4_24.value==this.w_AN2SCONT)
      this.oPgFrm.Page4.oPag.oAN2SCONT_4_24.value=this.w_AN2SCONT
    endif
    if not(this.oPgFrm.Page4.oPag.oANCODZON_4_25.value==this.w_ANCODZON)
      this.oPgFrm.Page4.oPag.oANCODZON_4_25.value=this.w_ANCODZON
    endif
    if not(this.oPgFrm.Page4.oPag.oANCONCON_4_26.RadioValue()==this.w_ANCONCON)
      this.oPgFrm.Page4.oPag.oANCONCON_4_26.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDESZON_4_27.value==this.w_DESZON)
      this.oPgFrm.Page4.oPag.oDESZON_4_27.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page4.oPag.oANMAGTER_4_38.value==this.w_ANMAGTER)
      this.oPgFrm.Page4.oPag.oANMAGTER_4_38.value=this.w_ANMAGTER
    endif
    if not(this.oPgFrm.Page4.oPag.oANCODESC_4_39.value==this.w_ANCODESC)
      this.oPgFrm.Page4.oPag.oANCODESC_4_39.value=this.w_ANCODESC
    endif
    if not(this.oPgFrm.Page4.oPag.oANCODPOR_4_40.value==this.w_ANCODPOR)
      this.oPgFrm.Page4.oPag.oANCODPOR_4_40.value=this.w_ANCODPOR
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAG_4_41.value==this.w_DESMAG)
      this.oPgFrm.Page4.oPag.oDESMAG_4_41.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page4.oPag.oANMCALSI_4_44.value==this.w_ANMCALSI)
      this.oPgFrm.Page4.oPag.oANMCALSI_4_44.value=this.w_ANMCALSI
    endif
    if not(this.oPgFrm.Page4.oPag.oANMCALST_4_45.value==this.w_ANMCALST)
      this.oPgFrm.Page4.oPag.oANMCALST_4_45.value=this.w_ANMCALST
    endif
    if not(this.oPgFrm.Page4.oPag.oMSDESIMB_4_46.value==this.w_MSDESIMB)
      this.oPgFrm.Page4.oPag.oMSDESIMB_4_46.value=this.w_MSDESIMB
    endif
    if not(this.oPgFrm.Page4.oPag.oANSCORPO_4_47.RadioValue()==this.w_ANSCORPO)
      this.oPgFrm.Page4.oPag.oANSCORPO_4_47.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oANFLCODI_4_49.RadioValue()==this.w_ANFLCODI)
      this.oPgFrm.Page4.oPag.oANFLCODI_4_49.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oANFLIMBA_4_50.RadioValue()==this.w_ANFLIMBA)
      this.oPgFrm.Page4.oPag.oANFLIMBA_4_50.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oANFLGCPZ_4_51.RadioValue()==this.w_ANFLGCPZ)
      this.oPgFrm.Page4.oPag.oANFLGCPZ_4_51.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMSDESTRA_4_52.value==this.w_MSDESTRA)
      this.oPgFrm.Page4.oPag.oMSDESTRA_4_52.value=this.w_MSDESTRA
    endif
    if not(this.oPgFrm.Page4.oPag.oGDDESCRI_4_64.value==this.w_GDDESCRI)
      this.oPgFrm.Page4.oPag.oGDDESCRI_4_64.value=this.w_GDDESCRI
    endif
    if not(this.oPgFrm.Page4.oPag.oANFLGCAU_4_65.RadioValue()==this.w_ANFLGCAU)
      this.oPgFrm.Page4.oPag.oANFLGCAU_4_65.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCRI_4_69.value==this.w_DESCRI)
      this.oPgFrm.Page4.oPag.oDESCRI_4_69.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page4.oPag.oANFLAPCA_4_70.RadioValue()==this.w_ANFLAPCA)
      this.oPgFrm.Page4.oPag.oANFLAPCA_4_70.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_73.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_73.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page3.oPag.oECTERRIT_3_9.value==this.w_ECTERRIT)
      this.oPgFrm.Page3.oPag.oECTERRIT_3_9.value=this.w_ECTERRIT
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCREG_3_14.value==this.w_DESCREG)
      this.oPgFrm.Page3.oPag.oDESCREG_3_14.value=this.w_DESCREG
    endif
    if not(this.oPgFrm.Page3.oPag.oDESIRPEF_3_18.value==this.w_DESIRPEF)
      this.oPgFrm.Page3.oPag.oDESIRPEF_3_18.value=this.w_DESIRPEF
    endif
    if not(this.oPgFrm.Page3.oPag.oDESTR2_3_26.value==this.w_DESTR2)
      this.oPgFrm.Page3.oPag.oDESTR2_3_26.value=this.w_DESTR2
    endif
    if not(this.oPgFrm.Page3.oPag.oANRITENU_3_31.RadioValue()==this.w_ANRITENU)
      this.oPgFrm.Page3.oPag.oANRITENU_3_31.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODIRP_3_32.value==this.w_ANCODIRP)
      this.oPgFrm.Page3.oPag.oANCODIRP_3_32.value=this.w_ANCODIRP
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODTR2_3_33.value==this.w_ANCODTR2)
      this.oPgFrm.Page3.oPag.oANCODTR2_3_33.value=this.w_ANCODTR2
    endif
    if not(this.oPgFrm.Page3.oPag.oANPEINPS_3_34.value==this.w_ANPEINPS)
      this.oPgFrm.Page3.oPag.oANPEINPS_3_34.value=this.w_ANPEINPS
    endif
    if not(this.oPgFrm.Page3.oPag.oANRIINPS_3_35.value==this.w_ANRIINPS)
      this.oPgFrm.Page3.oPag.oANRIINPS_3_35.value=this.w_ANRIINPS
    endif
    if not(this.oPgFrm.Page3.oPag.oANCOINPS_3_36.value==this.w_ANCOINPS)
      this.oPgFrm.Page3.oPag.oANCOINPS_3_36.value=this.w_ANCOINPS
    endif
    if not(this.oPgFrm.Page3.oPag.oANCASPRO_3_37.value==this.w_ANCASPRO)
      this.oPgFrm.Page3.oPag.oANCASPRO_3_37.value=this.w_ANCASPRO
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODATT_3_38.value==this.w_ANCODATT)
      this.oPgFrm.Page3.oPag.oANCODATT_3_38.value=this.w_ANCODATT
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODASS_3_39.value==this.w_ANCODASS)
      this.oPgFrm.Page3.oPag.oANCODASS_3_39.value=this.w_ANCODASS
    endif
    if not(this.oPgFrm.Page3.oPag.oANCOIMPS_3_40.value==this.w_ANCOIMPS)
      this.oPgFrm.Page3.oPag.oANCOIMPS_3_40.value=this.w_ANCOIMPS
    endif
    if not(this.oPgFrm.Page10.oPag.oCODI_10_1.value==this.w_CODI)
      this.oPgFrm.Page10.oPag.oCODI_10_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page10.oPag.oDES1_10_2.value==this.w_DES1)
      this.oPgFrm.Page10.oPag.oDES1_10_2.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page10.oPag.oAN__NOTE_10_4.value==this.w_AN__NOTE)
      this.oPgFrm.Page10.oPag.oAN__NOTE_10_4.value=this.w_AN__NOTE
    endif
    if not(this.oPgFrm.Page9.oPag.oCODI_9_1.value==this.w_CODI)
      this.oPgFrm.Page9.oPag.oCODI_9_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page9.oPag.oDES1_9_2.value==this.w_DES1)
      this.oPgFrm.Page9.oPag.oDES1_9_2.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page9.oPag.oANRATING_9_7.value==this.w_ANRATING)
      this.oPgFrm.Page9.oPag.oANRATING_9_7.value=this.w_ANRATING
    endif
    if not(this.oPgFrm.Page9.oPag.oANGIORIT_9_8.value==this.w_ANGIORIT)
      this.oPgFrm.Page9.oPag.oANGIORIT_9_8.value=this.w_ANGIORIT
    endif
    if not(this.oPgFrm.Page9.oPag.oANVOCFIN_9_9.value==this.w_ANVOCFIN)
      this.oPgFrm.Page9.oPag.oANVOCFIN_9_9.value=this.w_ANVOCFIN
    endif
    if not(this.oPgFrm.Page9.oPag.oDFDESCRI_9_10.value==this.w_DFDESCRI)
      this.oPgFrm.Page9.oPag.oDFDESCRI_9_10.value=this.w_DFDESCRI
    endif
    if not(this.oPgFrm.Page9.oPag.oANESCDOF_9_11.RadioValue()==this.w_ANESCDOF)
      this.oPgFrm.Page9.oPag.oANESCDOF_9_11.SetRadio()
    endif
    if not(this.oPgFrm.Page9.oPag.oANDESPAR_9_12.RadioValue()==this.w_ANDESPAR)
      this.oPgFrm.Page9.oPag.oANDESPAR_9_12.SetRadio()
    endif
    if not(this.oPgFrm.Page9.oPag.oRADESCRI_9_13.value==this.w_RADESCRI)
      this.oPgFrm.Page9.oPag.oRADESCRI_9_13.value=this.w_RADESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI_3_41.value==this.w_CODI)
      this.oPgFrm.Page3.oPag.oCODI_3_41.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page3.oPag.oANFLGEST_3_42.RadioValue()==this.w_ANFLGEST)
      this.oPgFrm.Page3.oPag.oANFLGEST_3_42.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANDESCRI_3_44.value==this.w_ANDESCRI)
      this.oPgFrm.Page3.oPag.oANDESCRI_3_44.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oANFLSGRE_3_45.RadioValue()==this.w_ANFLSGRE)
      this.oPgFrm.Page3.oPag.oANFLSGRE_3_45.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANDESCR2_3_46.value==this.w_ANDESCR2)
      this.oPgFrm.Page3.oPag.oANDESCR2_3_46.value=this.w_ANDESCR2
    endif
    if not(this.oPgFrm.Page3.oPag.oANCOFISC_3_47.value==this.w_ANCOFISC)
      this.oPgFrm.Page3.oPag.oANCOFISC_3_47.value=this.w_ANCOFISC
    endif
    if not(this.oPgFrm.Page3.oPag.oANSCHUMA_3_48.RadioValue()==this.w_ANSCHUMA)
      this.oPgFrm.Page3.oPag.oANSCHUMA_3_48.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAN_EREDE_3_50.RadioValue()==this.w_AN_EREDE)
      this.oPgFrm.Page3.oPag.oAN_EREDE_3_50.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODSNS_3_51.RadioValue()==this.w_ANCODSNS)
      this.oPgFrm.Page3.oPag.oANCODSNS_3_51.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODREG_3_53.value==this.w_ANCODREG)
      this.oPgFrm.Page3.oPag.oANCODREG_3_53.value=this.w_ANCODREG
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODCOM_3_54.value==this.w_ANCODCOM)
      this.oPgFrm.Page3.oPag.oANCODCOM_3_54.value=this.w_ANCODCOM
    endif
    if not(this.oPgFrm.Page3.oPag.oANCAURIT_3_55.RadioValue()==this.w_ANCAURIT)
      this.oPgFrm.Page3.oPag.oANCAURIT_3_55.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANEVEECC_3_56.RadioValue()==this.w_ANEVEECC)
      this.oPgFrm.Page3.oPag.oANEVEECC_3_56.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANCATPAR_3_57.RadioValue()==this.w_ANCATPAR)
      this.oPgFrm.Page3.oPag.oANCATPAR_3_57.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CONTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(IIF (.w_CODISO='IT' OR EMPTY (.w_CODISO) , CHKCFP(.w_ANCODFIS, 'CFA', 'F',1,.w_ANCODICE,' ',.w_ANCOGNOM,.w_AN__NOME,.w_ANLOCNAS,.w_ANPRONAS,.w_ANDATNAS,.w_AN_SESSO,.w_ANPERFIS,,.w_CODCOM) , .T.))
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ANCODICE))  and (.w_AUTO<>'S' AND .cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODICE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_ANCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANDESCRI)) or not(CHKCFP(.w_ANDESCRI, "RA", "F",1, .w_ANCODICE)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANDESCRI_1_5.SetFocus()
            i_bnoObbl = !empty(.w_ANDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_ANCODFIS, 'CF', 'F',0,.w_ANCODICE, .w_ANNAZION))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODFIS_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_ANPARIVA, "PI",'F',0,.w_ANCODICE, .w_ANNAZION))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANPARIVA_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANCONSUP)) or not(.w_NULIV=1 AND .w_TIPMAS='F'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCONSUP_2_5.SetFocus()
            i_bnoObbl = !empty(.w_ANCONSUP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSORIF) OR .w_DATOBSORIF>.w_OBTEST)  and not(empty(.w_ANCONRIF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCONRIF_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   (empty(.w_ANCATCON))  and not(g_ACQU<>'S')  and (g_ACQU='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCATCON_2_10.SetFocus()
            i_bnoObbl = !empty(.w_ANCATCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSOIVA) OR .w_DATOBSOIVA>.w_OBTEST))  and not(empty(.w_ANCODIVA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCODIVA_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
          case   not(EMPTY(.w_TIDTOBSO) OR .w_TIDTOBSO>.w_OBTEST)  and not(empty(.w_ANTIPOPE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANTIPOPE_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice incongruente oppure obsoleto")
          case   not(CHKCONSTUCF(.w_ANCODSTU,.w_CODSTU))  and not(g_LEMC<>'S' OR g_TRAEXP='N')  and (g_LEMC='S' AND g_TRAEXP<>'N')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCODSTU_2_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice deve essere compreso tra il limite inferiore e quello superiore oppure lunghezza non consentita")
          case   not(EMPTY(.w_DATOBSOPAG) OR .w_DATOBSOPAG>.w_OBTEST)  and not(empty(.w_ANCODPAG))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oANCODPAG_5_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not(.w_ANGIOFIS <= 31)
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oANGIOFIS_5_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_AN1MESCL<13 AND (.w_AN1MESCL<.w_AN2MESCL OR .w_AN2MESCL=0))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oAN1MESCL_5_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANGIOSC1)) or not(.w_ANGIOSC1 <= 31))  and (.w_AN1MESCL>0)
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oANGIOSC1_5_9.SetFocus()
            i_bnoObbl = !empty(.w_ANGIOSC1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_AN2MESCL<13 AND (.w_AN1MESCL<.w_AN2MESCL OR .w_AN2MESCL=0))  and (.w_AN1MESCL > 0)
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oAN2MESCL_5_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANGIOSC2)) or not(.w_ANGIOSC2 <= 31))  and (.w_AN2MESCL>0)
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oANGIOSC2_5_14.SetFocus()
            i_bnoObbl = !empty(.w_ANGIOSC2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CONSBF='C')  and not(empty(.w_ANCODBA2))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oANCODBA2_5_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto o di tipo salvo buon fine")
          case   not(.w_FLSCM $ ' C')  and not(Isalt())  and not(empty(.w_ANCATSCM))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANCATSCM_4_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria sconti / maggiorazioni inesistente o non di tipo fornitore")
          case   (empty(.w_ANCODLIN))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANCODLIN_4_17.SetFocus()
            i_bnoObbl = !empty(.w_ANCODLIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKVLCF(.w_ANCODVAL, .w_VALLIS, "FORNITORE",'X',' ') AND CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.))  and not(empty(.w_ANCODVAL))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANCODVAL_4_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKVLCF(.w_ANCODVAL, .w_VALLIS, "FORNITORE", .w_ANSCORPO, .w_IVALIS))  and not(empty(.w_ANNUMLIS))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANNUMLIS_4_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSOZON) OR .w_DATOBSOZON>.w_OBTEST)  and not(empty(.w_ANCODZON))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANCODZON_4_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
          case   not(EMPTY(.w_ANMAGTER) OR .w_MAGWIP='W')  and not(Isalt())  and not(empty(.w_ANMAGTER))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANMAGTER_4_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino C/Lavoro inesistente o non tipo WIP")
          case   not(.w_FLIRPE='R')  and (.w_ANRITENU$'CS' and g_RITE='S')  and not(empty(.w_ANCODIRP))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oANCODIRP_3_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
          case   ((empty(.w_ANCODTR2)) or not(.w_FLACO2<>'R'))  and (.w_ANRITENU='C' and g_RITE='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oANCODTR2_3_33.SetFocus()
            i_bnoObbl = !empty(.w_ANCODTR2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ANCODATT>=1 AND .w_ANCODATT<=28)  and (.w_ANRITENU='C')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oANCODATT_3_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice attivit� non valido (inserire 1..28)")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAR_MSA.CheckForm()
      if i_bres
        i_bres=  .GSAR_MSA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MSE.CheckForm()
      if i_bres
        i_bres=  .GSAR_MSE.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=8
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MDD.CheckForm()
      if i_bres
        i_bres=  .GSAR_MDD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=7
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MCO.CheckForm()
      if i_bres
        i_bres=  .GSAR_MCO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=6
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MCC.CheckForm()
      if i_bres
        i_bres=  .GSAR_MCC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=5
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MIN.CheckForm()
      if i_bres
        i_bres=  .GSAR_MIN.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsar_afr
      * Verifico se l'utente ha impostato il campo comunicazione fiscalit�
      * privilegiata oppure soggetto terzo e le combo "Operazione rilevanti IVA" oppure
      * "Tipologia prevalente "a 'beni/servizi' 
      if  i_bRes and (.w_ANFLBLLS='S' or .w_ANFLSOAL="S") and (.w_ANOPETRE $ 'PC' OR .w_ANTIPPRE $ 'BS')
         if ah_yesno ('Attenzione: le operazioni interessate dalla%0Comunicazione fiscalit� privilegiata%0sono normalmente escluse%0dalla Comunicazione operazioni superiori a 3000 euro.%0Confermi ugualmente?')
           * Confermo il salvataggio del record
         else
           i_bnoChk = .f.
           i_bRes = .f.
      		 i_cErrorMsg = Ah_MsgFormat("Riconsiderare i parametri relativi a 'Fiscalit� privilegiata'/'Soggetto terzo' e 'Comunicazione operazioni superiori a 3.000 euro'")
         endif
      endif
      
      
      
      * Se � attivo il modulo revi si esegue il controllo per verificare che i codici di pagamento tra le diverse aziende siano uguali
      IF g_REVI='S'  AND .w_ANFLGCPZ='S'  AND ALLTRIM(i_CODAZI) $ AssocLst()
      local risultato
      risultato=IAHR_CHK1BCP (this)
      IF not risultato
           i_bnoChk = .f.
           i_bRes = .f.
           i_cErrorMsg =Ah_MsgFormat( "Operazione annullata")
      ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AUTO = this.w_AUTO
    this.o_ANTIPCON = this.w_ANTIPCON
    this.o_ANCODICE = this.w_ANCODICE
    this.o_ANCODFIS = this.w_ANCODFIS
    this.o_ANPERFIS = this.w_ANPERFIS
    this.o_ANCHKMAI = this.w_ANCHKMAI
    this.o_ANCHKCPZ = this.w_ANCHKCPZ
    this.o_CODSTU = this.w_CODSTU
    this.o_AFFLINTR = this.w_AFFLINTR
    this.o_ANFLBLLS = this.w_ANFLBLLS
    this.o_ANFLSOAL = this.w_ANFLSOAL
    this.o_ANCODSTU = this.w_ANCODSTU
    this.o_PROSTU = this.w_PROSTU
    this.o_ANFLSGRE = this.w_ANFLSGRE
    this.o_ANNUMCOR = this.w_ANNUMCOR
    this.o_ANCINABI = this.w_ANCINABI
    this.o_AN__BBAN = this.w_AN__BBAN
    this.o_CONTA = this.w_CONTA
    this.o_AN1MESCL = this.w_AN1MESCL
    this.o_AN2MESCL = this.w_AN2MESCL
    this.o_ANFLESIM = this.w_ANFLESIM
    this.o_ANSAGINT = this.w_ANSAGINT
    this.o_AN1SCONT = this.w_AN1SCONT
    this.o_ANRITENU = this.w_ANRITENU
    this.o_ANCODIRP = this.w_ANCODIRP
    this.o_ANFLGEST = this.w_ANFLGEST
    * --- GSAR_MSA : Depends On
    this.GSAR_MSA.SaveDependsOn()
    * --- GSAR_MSE : Depends On
    this.GSAR_MSE.SaveDependsOn()
    * --- GSAR_MDD : Depends On
    this.GSAR_MDD.SaveDependsOn()
    * --- GSAR_MCO : Depends On
    this.GSAR_MCO.SaveDependsOn()
    * --- GSAR_MCC : Depends On
    this.GSAR_MCC.SaveDependsOn()
    * --- GSAR_MIN : Depends On
    this.GSAR_MIN.SaveDependsOn()
    return

  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsar_afrPag1 as StdContainer
  Width  = 737
  height = 560
  stdWidth  = 737
  stdheight = 560
  resizeXpos=342
  resizeYpos=292
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANCODICE_1_4 as StdField with uid="QOWIJQDBXI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANCODICE", cQueryName = "ANTIPCON,ANCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice alfanumerico fornitore",;
    HelpContextID = 168430411,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=113, Top=8, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15)

  func oANCODICE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AUTO<>'S' AND .cFunction='Load')
    endwith
   endif
  endfunc

  proc oANCODICE_1_4.mAfter
    with this.Parent.oContained
      .w_ANCODICE=CALCZER(.w_ANCODICE, 'CONTI')
    endwith
  endproc

  add object oANDESCRI_1_5 as StdField with uid="BSOKDONWXF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Esatta ragione sociale del fornitore",;
    HelpContextID = 185590961,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=255, Top=8, InputMask=replicate('X',60)

  proc oANDESCRI_1_5.mAfter
    with this.Parent.oContained
      .w_ANDESCRI=IIF(.cFunction<>'Query', Normragsoc(.w_ANDESCRI), .w_ANDESCRI)
    endwith
  endproc

  func oANDESCRI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_ANDESCRI, "RA", "F",1, .w_ANCODICE))
    endwith
    return bRes
  endfunc


  add object oANTIPCLF_1_6 as StdCombo with uid="PSIFLHPKBY",rtseq=6,rtrep=.f.,left=595,top=8,width=113,height=21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 188409012;
    , cFormVar="w_ANTIPCLF",RowSource=""+"Generico,"+"Agente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANTIPCLF_1_6.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oANTIPCLF_1_6.GetRadio()
    this.Parent.oContained.w_ANTIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oANTIPCLF_1_6.SetRadio()
    this.Parent.oContained.w_ANTIPCLF=trim(this.Parent.oContained.w_ANTIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_ANTIPCLF=='G',1,;
      iif(this.Parent.oContained.w_ANTIPCLF=='A',2,;
      0))
  endfunc

  func oANTIPCLF_1_6.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oANDESCR2_1_7 as StdField with uid="EXMBGSDQWR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ANDESCR2", cQueryName = "ANDESCR2",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 185590984,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=255, Top=31, InputMask=replicate('X',60)

  proc oANDESCR2_1_7.mAfter
    with this.Parent.oContained
      .w_ANDESCR2=IIF(.cFunction<>'Query', Normragsoc(.w_ANDESCR2), .w_ANDESCR2)
    endwith
  endproc

  add object oANINDIRI_1_11 as StdField with uid="TEIAJIVSZX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ANINDIRI", cQueryName = "ANINDIRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo del fornitore",;
    HelpContextID = 100046001,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=113, Top=57, InputMask=replicate('X',35)

  add object oANINDIR2_1_12 as StdField with uid="QOICRRVOXT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ANINDIR2", cQueryName = "ANINDIR2",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi all'indirizzo del fornitore",;
    HelpContextID = 100046024,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=113, Top=82, InputMask=replicate('X',35)

  add object oAN___CAP_1_13 as StdField with uid="VCYDCDQIBS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_AN___CAP", cQueryName = "AN___CAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale",;
    HelpContextID = 171193514,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=113, Top=107, InputMask=replicate('X',8), bHasZoom = .t. 

  proc oAN___CAP_1_13.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AN___CAP",".w_ANLOCALI",".w_ANPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANLOCALI_1_14 as StdField with uid="AEHPYWAWGE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ANLOCALI", cQueryName = "ANLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 235234481,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=183, Top=107, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oANLOCALI_1_14.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AN___CAP",".w_ANLOCALI",".w_ANPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANPROVIN_1_15 as StdField with uid="GOEZWVIRNN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ANPROVIN", cQueryName = "ANPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza del fornitore",;
    HelpContextID = 129882964,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=113, Top=132, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  proc oANPROVIN_1_15.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_ANPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANNAZION_1_16 as StdField with uid="OSESEHJOWC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ANNAZION", cQueryName = "ANNAZION",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della nazione di appartenenza",;
    HelpContextID = 77808812,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=113, Top=157, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_ANNAZION"

  func oANNAZION_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oANNAZION_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANNAZION_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oANNAZION_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oANNAZION_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_ANNAZION
     i_obj.ecpSave()
  endproc

  add object oDESNAZ_1_17 as StdField with uid="CKLVXMODXU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESNAZ", cQueryName = "DESNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 86375882,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=183, Top=157, InputMask=replicate('X',35)

  add object oANCODFIS_1_18 as StdField with uid="CEOOEYDLVW",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ANCODFIS", cQueryName = "ANCODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale - Doppio click per eseguire il calcolo del codice fiscale di una persona fisica",;
    HelpContextID = 118098777,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=113, Top=181, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16), bHasZoom = .t. 

  func oANCODFIS_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_ANCODFIS, 'CF', 'F',0,.w_ANCODICE, .w_ANNAZION))
    endwith
    return bRes
  endfunc

  proc oANCODFIS_1_18.mZoom
    ZoomCF (  this.parent.oContained )
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANPARIVA_1_19 as StdField with uid="BMHODQEYHW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ANPARIVA", cQueryName = "ANDESCRI,ANPARIVA",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 182246215,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=113, Top=205, InputMask=replicate('X',12)

  proc oANPARIVA_1_19.mBefore
    with this.Parent.oContained
      this.cQueryName = "ANPARIVA"
    endwith
  endproc

  func oANPARIVA_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_ANPARIVA, "PI",'F',0,.w_ANCODICE, .w_ANNAZION))
    endwith
    return bRes
  endfunc

  add object oANTELEFO_1_20 as StdField with uid="MUTJWMJHNM",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ANTELEFO", cQueryName = "ANTELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero telefonico principale del fornitore",;
    HelpContextID = 109124437,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=559, Top=183, InputMask=replicate('X',18)

  add object oANTELFAX_1_21 as StdField with uid="XPTRCTTBND",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ANTELFAX", cQueryName = "ANTELFAX",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di FAX o TELEX",;
    HelpContextID = 142533794,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=559, Top=208, InputMask=replicate('X',18)

  add object oANNUMCEL_1_22 as StdField with uid="QAZQBPDVMR",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ANNUMCEL", cQueryName = "ANNUMCEL",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero del telefono cellulare",;
    HelpContextID = 77642578,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=559, Top=233, InputMask=replicate('X',18)

  add object oANINDWEB_1_23 as StdField with uid="SWBUJDQKZX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ANINDWEB", cQueryName = "ANINDWEB",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo sito Internet",;
    HelpContextID = 134835016,;
   bGlobalFont=.t.,;
    Height=21, Width=563, Left=113, Top=257, InputMask=replicate('X',254)

  add object oAN_EMAIL_1_24 as StdField with uid="CYOARAFJIA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_AN_EMAIL", cQueryName = "AN_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica",;
    HelpContextID = 43109202,;
   bGlobalFont=.t.,;
    Height=21, Width=563, Left=113, Top=284, InputMask=replicate('X',254)

  add object oAN_EMPEC_1_25 as StdField with uid="WJJJONEHWZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_AN_EMPEC", cQueryName = "AN_EMPEC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica certificata (PEC)",;
    HelpContextID = 26331977,;
   bGlobalFont=.t.,;
    Height=21, Width=563, Left=113, Top=311, InputMask=replicate('X',254)

  add object oANPERFIS_1_26 as StdCheck with uid="MAYOVAAUAU",rtseq=26,rtrep=.f.,left=34, top=348, caption="Persona fisica",;
    ToolTipText = "Se attivo: il fornitore � una persona fisica",;
    HelpContextID = 132176729,;
    cFormVar="w_ANPERFIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANPERFIS_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANPERFIS_1_26.GetRadio()
    this.Parent.oContained.w_ANPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oANPERFIS_1_26.SetRadio()
    this.Parent.oContained.w_ANPERFIS=trim(this.Parent.oContained.w_ANPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_ANPERFIS=='S',1,;
      0)
  endfunc


  add object oAN_SESSO_1_27 as StdCombo with uid="PEAHRERJPL",rtseq=27,rtrep=.f.,left=34,top=369,width=98,height=21;
    , ToolTipText = "Sesso";
    , HelpContextID = 69192533;
    , cFormVar="w_AN_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAN_SESSO_1_27.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oAN_SESSO_1_27.GetRadio()
    this.Parent.oContained.w_AN_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oAN_SESSO_1_27.SetRadio()
    this.Parent.oContained.w_AN_SESSO=trim(this.Parent.oContained.w_AN_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_AN_SESSO=='M',1,;
      iif(this.Parent.oContained.w_AN_SESSO=='F',2,;
      0))
  endfunc

  func oAN_SESSO_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc

  add object oANCOGNOM_1_28 as StdField with uid="JAJHUKIBXU",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ANCOGNOM", cQueryName = "ANCOGNOM",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del fornitore se persona fisica",;
    HelpContextID = 12973229,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=251, Top=348, InputMask=replicate('X',30)

  func oANCOGNOM_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc

  add object oAN__NOME_1_29 as StdField with uid="IBNCUCQHOE",rtseq=29,rtrep=.f.,;
    cFormVar = "w_AN__NOME", cQueryName = "AN__NOME",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome del fornitore se persona fisica",;
    HelpContextID = 256128181,;
   bGlobalFont=.t.,;
    Height=21, Width=170, Left=538, Top=348, InputMask=replicate('X',30)

  func oAN__NOME_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc

  add object oANLOCNAS_1_30 as StdField with uid="AQZYOQYGPA",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ANLOCNAS", cQueryName = "ANLOCNAS",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Luogo di nascita",;
    HelpContextID = 17130663,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=251, Top=373, InputMask=replicate('X',30), bHasZoom = .t. 

  func oANLOCNAS_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS="S")
    endwith
   endif
  endfunc

  proc oANLOCNAS_1_30.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_ANLOCNAS",".w_ANPRONAS",,,".w_CODCOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANPRONAS_1_31 as StdField with uid="EBHSQLYWGA",rtseq=31,rtrep=.f.,;
    cFormVar = "w_ANPRONAS", cQueryName = "ANPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia del luogo di nascita",;
    HelpContextID = 4334759,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=538, Top=373, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oANPRONAS_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS="S")
    endwith
   endif
  endfunc

  proc oANPRONAS_1_31.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_ANPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANDATNAS_1_32 as StdField with uid="APVESEIIOW",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ANDATNAS", cQueryName = "ANDATNAS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 255143,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=629, Top=373

  func oANDATNAS_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS ="S")
    endwith
   endif
  endfunc

  add object oANNUMCAR_1_33 as StdField with uid="KUMIMWIJSD",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ANNUMCAR", cQueryName = "ANNUMCAR",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero carta di identit�",;
    HelpContextID = 190792872,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=538, Top=398, InputMask=replicate('X',18)

  func oANNUMCAR_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc

  add object oCODISO_1_50 as StdField with uid="ICJNLLEDQZ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CODISO", cQueryName = "CODISO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice ISO nazione",;
    HelpContextID = 252437466,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=559, Top=157, InputMask=replicate('X',3)

  add object oANCHKSTA_1_57 as StdCheck with uid="HDBTXBPKLN",rtseq=39,rtrep=.f.,left=15, top=424, caption="Stampa",;
    HelpContextID = 74648391,;
    cFormVar="w_ANCHKSTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKSTA_1_57.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKSTA_1_57.GetRadio()
    this.Parent.oContained.w_ANCHKSTA = this.RadioValue()
    return .t.
  endfunc

  func oANCHKSTA_1_57.SetRadio()
    this.Parent.oContained.w_ANCHKSTA=trim(this.Parent.oContained.w_ANCHKSTA)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKSTA=='S',1,;
      0)
  endfunc

  func oANCHKSTA_1_57.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKMAI_1_58 as StdCheck with uid="VNSLKUJWHZ",rtseq=40,rtrep=.f.,left=89, top=424, caption="E-mail",;
    HelpContextID = 26014897,;
    cFormVar="w_ANCHKMAI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKMAI_1_58.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKMAI_1_58.GetRadio()
    this.Parent.oContained.w_ANCHKMAI = this.RadioValue()
    return .t.
  endfunc

  func oANCHKMAI_1_58.SetRadio()
    this.Parent.oContained.w_ANCHKMAI=trim(this.Parent.oContained.w_ANCHKMAI)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKMAI=='S',1,;
      0)
  endfunc

  func oANCHKMAI_1_58.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKPEC_1_59 as StdCheck with uid="VAWOHIBHWM",rtseq=41,rtrep=.f.,left=160, top=424, caption="PEC",;
    HelpContextID = 24316745,;
    cFormVar="w_ANCHKPEC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKPEC_1_59.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKPEC_1_59.GetRadio()
    this.Parent.oContained.w_ANCHKPEC = this.RadioValue()
    return .t.
  endfunc

  func oANCHKPEC_1_59.SetRadio()
    this.Parent.oContained.w_ANCHKPEC=trim(this.Parent.oContained.w_ANCHKPEC)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKPEC=='S',1,;
      0)
  endfunc

  func oANCHKPEC_1_59.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKFAX_1_60 as StdCheck with uid="ZXITRVALRT",rtseq=42,rtrep=.f.,left=224, top=424, caption="FAX",;
    HelpContextID = 143455394,;
    cFormVar="w_ANCHKFAX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKFAX_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKFAX_1_60.GetRadio()
    this.Parent.oContained.w_ANCHKFAX = this.RadioValue()
    return .t.
  endfunc

  func oANCHKFAX_1_60.SetRadio()
    this.Parent.oContained.w_ANCHKFAX=trim(this.Parent.oContained.w_ANCHKFAX)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKFAX=='S',1,;
      0)
  endfunc

  func oANCHKFAX_1_60.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKCPZ_1_61 as StdCheck with uid="SIMTIXJRRY",rtseq=43,rtrep=.f.,left=286, top=424, caption="Web Application",;
    ToolTipText = "Se attivo: consente invio documenti a fornitore su Web Application",;
    HelpContextID = 193787040,;
    cFormVar="w_ANCHKCPZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKCPZ_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKCPZ_1_61.GetRadio()
    this.Parent.oContained.w_ANCHKCPZ = this.RadioValue()
    return .t.
  endfunc

  func oANCHKCPZ_1_61.SetRadio()
    this.Parent.oContained.w_ANCHKCPZ=trim(this.Parent.oContained.w_ANCHKCPZ)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKCPZ=='S',1,;
      0)
  endfunc

  func oANCHKCPZ_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_IZCP$'SA'  or g_CPIN='S') and g_DMIP<>'S' and .w_ANFLGCPZ='S')
    endwith
   endif
  endfunc

  func oANCHKCPZ_1_61.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKWWP_1_62 as StdCheck with uid="MATRZJJCUX",rtseq=44,rtrep=.f.,left=418, top=424, caption="Net-folder",;
    HelpContextID = 141757270,;
    cFormVar="w_ANCHKWWP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKWWP_1_62.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKWWP_1_62.GetRadio()
    this.Parent.oContained.w_ANCHKWWP = this.RadioValue()
    return .t.
  endfunc

  func oANCHKWWP_1_62.SetRadio()
    this.Parent.oContained.w_ANCHKWWP=trim(this.Parent.oContained.w_ANCHKWWP)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKWWP=='S',1,;
      0)
  endfunc

  func oANCHKWWP_1_62.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKPTL_1_63 as StdCheck with uid="RFDRJEBWSG",rtseq=45,rtrep=.f.,left=511, top=424, caption="PostaLite",;
    HelpContextID = 24316754,;
    cFormVar="w_ANCHKPTL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKPTL_1_63.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKPTL_1_63.GetRadio()
    this.Parent.oContained.w_ANCHKPTL = this.RadioValue()
    return .t.
  endfunc

  func oANCHKPTL_1_63.SetRadio()
    this.Parent.oContained.w_ANCHKPTL=trim(this.Parent.oContained.w_ANCHKPTL)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKPTL=='S',1,;
      0)
  endfunc

  func oANCHKPTL_1_63.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKFIR_1_64 as StdCheck with uid="FRVFQCMMTF",rtseq=46,rtrep=.f.,left=618, top=424, caption="Firma digitale",;
    ToolTipText = "Se attivo, consente invio al fornitore del documento firmato",;
    HelpContextID = 124980056,;
    cFormVar="w_ANCHKFIR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKFIR_1_64.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKFIR_1_64.GetRadio()
    this.Parent.oContained.w_ANCHKFIR = this.RadioValue()
    return .t.
  endfunc

  func oANCHKFIR_1_64.SetRadio()
    this.Parent.oContained.w_ANCHKFIR=trim(this.Parent.oContained.w_ANCHKFIR)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKFIR=='S',1,;
      0)
  endfunc

  func oANCHKFIR_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANCHKMAI='S' or .w_ANCHKCPZ = 'S')
    endwith
   endif
  endfunc

  func oANCHKFIR_1_64.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCODEST_1_65 as StdField with uid="BBBNJNPXYQ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_ANCODEST", cQueryName = "ANCODEST",;
    bObbl = .f. , nPag = 1, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Per le fatture PA contiene il codice, di 6 caratteri, dell'ufficio destinatario della fattura, riportato nella rubrica �Indice PA�,  per le fatture privati contiene il codice, di 7 caratteri, assegnato dal Sdi ai sogg. che hanno accreditato un canale",;
    HelpContextID = 101321562,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=113, Top=477, InputMask=replicate('X',7)

  add object oANCODCLA_1_66 as StdField with uid="NDPWHLKDYP",rtseq=48,rtrep=.f.,;
    cFormVar = "w_ANCODCLA", cQueryName = "ANCODCLA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice di classificazione per fatturazione elettronica",;
    HelpContextID = 200668345,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=538, Top=477, InputMask=replicate('X',5)

  add object oANCODPEC_1_67 as StdMemo with uid="BVOZZMZVBA",rtseq=49,rtrep=.f.,;
    cFormVar = "w_ANCODPEC", cQueryName = "ANCODPEC",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo PEC al quale inviare il documento",;
    HelpContextID = 17435465,;
   bGlobalFont=.t.,;
    Height=21, Width=595, Left=113, Top=507

  add object oANDTOBSO_1_68 as StdField with uid="IZPQFZIFWD",rtseq=50,rtrep=.f.,;
    cFormVar = "w_ANDTOBSO", cQueryName = "ANDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 62856021,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=618, Top=534, tabstop=.f.

  add object oANDTINVA_1_73 as StdField with uid="CELKZYBPCL",rtseq=51,rtrep=.f.,;
    cFormVar = "w_ANDTINVA", cQueryName = "ANDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 257891143,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=395, Top=534, tabstop=.f.

  func oANDTINVA_1_73.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oObj_1_75 as cp_runprogram with uid="XRJBLACLJR",left=177, top=625, width=179,height=21,;
    caption='GSAR_BAU(A)',;
   bGlobalFont=.t.,;
    prg="GSAR_BAU('A')",;
    cEvent = "New record,CFGLoaded",;
    nPag=1;
    , HelpContextID = 188758981


  add object oObj_1_76 as cp_runprogram with uid="UTMUKFCDVA",left=-3, top=624, width=179,height=21,;
    caption='GSAR_BAN',;
   bGlobalFont=.t.,;
    prg="GSAR_BAN",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 188944204


  add object oObj_1_77 as cp_runprogram with uid="GTLDPZMPCI",left=-3, top=650, width=179,height=21,;
    caption='GSAR_BAU(D)',;
   bGlobalFont=.t.,;
    prg="GSAR_BAU('D')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 188758213


  add object oBtn_1_78 as StdButton with uid="OJYMBPOMWQ",left=556, top=35, width=48,height=45,;
    CpPicture="bmp\cattura.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da unit� disco";
    , HelpContextID = 105000486;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'CATTURA', disabledpicture = 'bmp\catturad.bmp',Caption='\<Cattura';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_78.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"CONTI",.w_ANTIPCON+ALLTRIM(.w_ANCODICE),"GSAR_AFR","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_78.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ANCODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_78.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_79 as StdButton with uid="YOPFSNISFV",left=608, top=35, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza allegati";
    , HelpContextID = 16543632;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_79.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"CONTI",.w_ANTIPCON+ALLTRIM(.w_ANCODICE),"GSAR_AFR","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_79.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ANCODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_79.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_80 as StdButton with uid="BJRFWTRDLX",left=660, top=35, width=48,height=45,;
    CpPicture="bmp\scanner.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da periferica di acquisizione immagini";
    , HelpContextID = 147521830;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'SCANNER', disabledpicture = 'bmp\scannerd.bmp' , Caption='\<Scanner';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_80.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"CONTI",.w_ANTIPCON+ALLTRIM(.w_ANCODICE),"GSAR_AFR","S",MROW(),MCOL(),.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_80.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ANCODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_80.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM<>'S' OR isAlt() or g_FLARDO<>'S')
     endwith
    endif
  endfunc


  add object oLinkPC_1_81 as StdButton with uid="FIYMUPNCST",left=556, top=80, width=48,height=45,;
    CpPicture="BMP\AnagStorico.bmp", caption="", nPag=1;
    , ToolTipText = "Dati anagrafici storicizzati";
    , HelpContextID = 217181078;
    , Caption='\<An. stor.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_81.Click()
      this.Parent.oContained.GSAR_MSA.LinkPCClick()
    endproc


  add object oBtn_1_82 as StdButton with uid="WQPDFLJUJA",left=608, top=80, width=48,height=45,;
    CpPicture="BMP\Storicizza.bmp", caption="", nPag=1;
    , ToolTipText = "Storicizza i dati anagrafici";
    , HelpContextID = 159360192;
    , Caption='S\<toricizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_82.Click()
      with this.Parent.oContained
        do GSAR_BSZ with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_82.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oObj_1_86 as cp_runprogram with uid="DXWKEYUCCR",left=366, top=624, width=201,height=24,;
    caption='GSCE_BGS(V)',;
   bGlobalFont=.t.,;
    prg="GSCE_BGS('V')",;
    cEvent = "CliForEliminato",;
    nPag=1;
    , HelpContextID = 78838073


  add object oObj_1_87 as cp_runprogram with uid="UVTBWTYAVT",left=178, top=650, width=238,height=24,;
    caption='GSAR_BAU(P)',;
   bGlobalFont=.t.,;
    prg="GSAR_BAU('P')",;
    cEvent = "w_ANPARIVA Changed",;
    nPag=1;
    , HelpContextID = 188755141


  add object oBtn_1_88 as StdButton with uid="PZKLYNNGQZ",left=686, top=257, width=22,height=22,;
    CpPicture="BMP\ROOT.bmp", caption="", nPag=1;
    , ToolTipText = "Apre il browser all'indirizzo web specificato";
    , HelpContextID = 58820762;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_88.Click()
      with this.Parent.oContained
        viewFile(.w_anindweb, .T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_88.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_ANINDWEB))
      endwith
    endif
  endfunc

  func oBtn_1_88.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ANINDWEB))
     endwith
    endif
  endfunc


  add object oBtn_1_89 as StdButton with uid="XEUWBAPBCE",left=686, top=284, width=22,height=22,;
    CpPicture="BMP\btsend.bmp", caption="", nPag=1;
    , ToolTipText = "Manda una e-mail all'indirizzo specificato";
    , HelpContextID = 207222202;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_89.Click()
      with this.Parent.oContained
        MAILTO(.w_AN_EMAIL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_89.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_AN_EMAIL))
      endwith
    endif
  endfunc

  func oBtn_1_89.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_AN_EMAIL))
     endwith
    endif
  endfunc


  add object oBtn_1_94 as StdButton with uid="YXOXMAGUJB",left=660, top=80, width=48,height=45,;
    CpPicture="bmp\ImportCF.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare i dati anagrafici dal codice fiscale";
    , HelpContextID = 135068364;
    , tabstop=.f., caption='Cod.\<Fisc';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_94.Click()
      with this.Parent.oContained
        iif(AH_yesno("Si vuole aggiornare sesso, luogo, provincia e data di nascita in base al codice fiscale?"), .notifyevent("AggiornaCF"),'')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_94.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_ANCODFIS)  AND (UPPER (.w_CODISO)='IT' OR  EMPTY (.w_CODISO)))
      endwith
    endif
  endfunc

  func oBtn_1_94.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_108 as StdButton with uid="EBTWQHKHVR",left=660, top=126, width=48,height=45,;
    CpPicture="BMP\MASTER.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai dati aggiuntivi";
    , HelpContextID = 209671769;
    , Caption='\<Altri dati';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_108.Click()
      do gsar1kad with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_112 as StdButton with uid="IIGKUGLKHF",left=686, top=311, width=22,height=22,;
    CpPicture="BMP\fxmail_pec.bmp", caption="", nPag=1;
    , ToolTipText = "Manda una e-mail PEC all'indirizzo specificato";
    , HelpContextID = 59047178;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_112.Click()
      with this.Parent.oContained
        MAILTO(.w_AN_EMPEC,,,.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_112.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_AN_EMPEC))
      endwith
    endif
  endfunc

  func oBtn_1_112.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_AN_EMPEC))
     endwith
    endif
  endfunc

  add object oStr_1_34 as StdString with uid="OVMROOGVRD",Visible=.t., Left=14, Top=8,;
    Alignment=1, Width=97, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="BFGUFAJUYW",Visible=.t., Left=14, Top=57,;
    Alignment=1, Width=97, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="JJKZZWSALX",Visible=.t., Left=446, Top=183,;
    Alignment=1, Width=110, Height=15,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="TXJNWKEKIB",Visible=.t., Left=446, Top=208,;
    Alignment=1, Width=110, Height=15,;
    Caption="Telefax:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="DPCZFALVCG",Visible=.t., Left=14, Top=181,;
    Alignment=1, Width=97, Height=15,;
    Caption="Cod. fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="GSBQBIXJVS",Visible=.t., Left=14, Top=205,;
    Alignment=1, Width=97, Height=15,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="GIWVNWPNAR",Visible=.t., Left=14, Top=157,;
    Alignment=1, Width=97, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="DJCRDQSFWY",Visible=.t., Left=180, Top=374,;
    Alignment=1, Width=69, Height=15,;
    Caption="Nato a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="BTFGAGSUCK",Visible=.t., Left=598, Top=373,;
    Alignment=1, Width=29, Height=15,;
    Caption="Il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="VHXPQGKOVB",Visible=.t., Left=166, Top=349,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="QLNSHYXFBC",Visible=.t., Left=480, Top=349,;
    Alignment=1, Width=57, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="TAGVHMTGGI",Visible=.t., Left=446, Top=233,;
    Alignment=1, Width=110, Height=15,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="IYXQYFGWZG",Visible=.t., Left=14, Top=259,;
    Alignment=1, Width=97, Height=15,;
    Caption="Internet web:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="CEOUZHDWOU",Visible=.t., Left=14, Top=283,;
    Alignment=1, Width=97, Height=15,;
    Caption="E@mail addr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="JHEZJBIBVC",Visible=.t., Left=14, Top=107,;
    Alignment=1, Width=97, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="XEWXBOYQFE",Visible=.t., Left=68, Top=132,;
    Alignment=1, Width=43, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="GHXLFNONFF",Visible=.t., Left=462, Top=157,;
    Alignment=1, Width=94, Height=15,;
    Caption="Cod.ISO:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="DLJRBNPKSR",Visible=.t., Left=484, Top=374,;
    Alignment=1, Width=53, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="ELYTWMJWVM",Visible=.t., Left=495, Top=535,;
    Alignment=1, Width=121, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="ZSEDUKULLP",Visible=.t., Left=551, Top=8,;
    Alignment=1, Width=40, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="OMNBVZUCBE",Visible=.t., Left=273, Top=535,;
    Alignment=1, Width=118, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="RCGOPLUALE",Visible=.t., Left=4, Top=404,;
    Alignment=0, Width=147, Height=18,;
    Caption="Processi documentali"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oStr_1_85 as StdString with uid="CWGTGSXYAJ",Visible=.t., Left=417, Top=401,;
    Alignment=1, Width=120, Height=15,;
    Caption="Carta di identit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_111 as StdString with uid="UCJXPBXRTJ",Visible=.t., Left=14, Top=307,;
    Alignment=1, Width=97, Height=15,;
    Caption="PEC addr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_113 as StdString with uid="WEDQLDHQOR",Visible=.t., Left=-1, Top=479,;
    Alignment=1, Width=112, Height=18,;
    Caption="Codice destinatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_114 as StdString with uid="BRQAAFGCCM",Visible=.t., Left=367, Top=479,;
    Alignment=1, Width=170, Height=18,;
    Caption="Classificazione FE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_115 as StdString with uid="FPZKWYQMLG",Visible=.t., Left=1, Top=508,;
    Alignment=1, Width=110, Height=18,;
    Caption="PEC destinatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_117 as StdString with uid="XQRKIKRCPA",Visible=.t., Left=2, Top=454,;
    Alignment=0, Width=214, Height=18,;
    Caption="Informazioni per fatturazione elettronica"  ;
  , bGlobalFont=.t.

  add object oBox_1_71 as StdBox with uid="LWHMOJMKZZ",left=2, top=340, width=715,height=1

  add object oBox_1_83 as StdBox with uid="LEBVCACMJU",left=1, top=421, width=733,height=1

  add object oBox_1_116 as StdBox with uid="WGFWMHQFGI",left=1, top=470, width=733,height=1
enddefine
define class tgsar_afrPag2 as StdContainer
  Width  = 737
  height = 560
  stdWidth  = 737
  stdheight = 560
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_2 as StdField with uid="QHWCJLMYJS",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 54256602,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=174, Top=16, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15)

  add object oDES1_2_3 as StdField with uid="MWKKHDGJAB",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55770570,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=316, Top=16, InputMask=replicate('X',40)

  add object oANCONSUP_2_5 as StdField with uid="VCGDUWCZBV",rtseq=59,rtrep=.f.,;
    cFormVar = "w_ANCONSUP", cQueryName = "ANCONSUP",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del fornitore",;
    HelpContextID = 78252886,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=174, Top=46, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_ANCONSUP"

  func oANCONSUP_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCONSUP_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCONSUP_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oANCONSUP_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (fornitori)",'GSAR_AFR.MASTRI_VZM',this.parent.oContained
  endproc
  proc oANCONSUP_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_ANCONSUP
     i_obj.ecpSave()
  endproc

  add object oDESSUP_2_6 as StdField with uid="RYYEBKUGID",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESSUP", cQueryName = "DESSUP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 232848842,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=316, Top=46, InputMask=replicate('X',40)

  add object oANCONRIF_2_8 as StdField with uid="GAFIOZYJEH",rtseq=62,rtrep=.f.,;
    cFormVar = "w_ANCONRIF", cQueryName = "ANCONRIF",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Codice originario cliente corrispondente",;
    HelpContextID = 61475660,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=174, Top=76, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPRIF", oKey_2_1="ANCODICE", oKey_2_2="this.w_ANCONRIF"

  func oANCONRIF_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCONRIF_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCONRIF_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPRIF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPRIF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oANCONRIF_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Cliente originario",'',this.parent.oContained
  endproc
  proc oANCONRIF_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPRIF
     i_obj.w_ANCODICE=this.parent.oContained.w_ANCONRIF
     i_obj.ecpSave()
  endproc

  add object oDESORI_2_9 as StdField with uid="WANFAEILZH",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 85261770,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=316, Top=76, InputMask=replicate('X',40)

  add object oANCATCON_2_10 as StdField with uid="LCBEADBASX",rtseq=64,rtrep=.f.,;
    cFormVar = "w_ANCATCON", cQueryName = "ANCATCON",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile del fornitore",;
    HelpContextID = 184808620,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=174, Top=106, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_ANCATCON"

  func oANCATCON_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ACQU='S')
    endwith
   endif
  endfunc

  func oANCATCON_2_10.mHide()
    with this.Parent.oContained
      return (g_ACQU<>'S')
    endwith
  endfunc

  func oANCATCON_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATCON_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATCON_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oANCATCON_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili fornitori",'',this.parent.oContained
  endproc
  proc oANCATCON_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_ANCATCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_2_11 as StdField with uid="JAMKNLZVQH",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 5307850,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=239, Top=106, InputMask=replicate('X',35)

  func oDESCON_2_11.mHide()
    with this.Parent.oContained
      return (g_ACQU<>'S')
    endwith
  endfunc

  add object oANCODIVA_2_12 as StdField with uid="JWNVMVGKFE",rtseq=66,rtrep=.f.,;
    cFormVar = "w_ANCODIVA", cQueryName = "ANCODIVA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA inesistente oppure obsoleto",;
    ToolTipText = "Eventuale codice IVA",;
    HelpContextID = 168430407,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=174, Top=136, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_ANCODIVA"

  func oANCODIVA_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODIVA_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODIVA_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oANCODIVA_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'GSVE0MDV.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oANCODIVA_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_ANCODIVA
     i_obj.ecpSave()
  endproc

  add object oDESIVA_2_13 as StdField with uid="STWXMYGSDH",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 215678410,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=239, Top=136, InputMask=replicate('X',35)

  add object oANTIPOPE_2_14 as StdField with uid="ATGWNZUHAX",rtseq=68,rtrep=.f.,;
    cFormVar = "w_ANTIPOPE", cQueryName = "ANTIPOPE",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente oppure obsoleto",;
    HelpContextID = 255517877,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=174, Top=166, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPCODIV", cZoomOnZoom="GSAR_ATO", oKey_1_1="TI__TIPO", oKey_1_2="this.w_ANCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_ANTIPOPE"

  func oANTIPOPE_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oANTIPOPE_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANTIPOPE_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIPCODIV_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_ANCATOPE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStr(this.Parent.oContained.w_ANCATOPE)
    endif
    do cp_zoom with 'TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(this.parent,'oANTIPOPE_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this.parent.oContained
  endproc
  proc oANTIPOPE_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TI__TIPO=w_ANCATOPE
     i_obj.w_TICODICE=this.parent.oContained.w_ANTIPOPE
     i_obj.ecpSave()
  endproc

  add object oTIDESCRI_2_15 as StdField with uid="TNOOFYXTGO",rtseq=69,rtrep=.f.,;
    cFormVar = "w_TIDESCRI", cQueryName = "TIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 185591937,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=282, Top=166, InputMask=replicate('X',30)

  add object oANCONCAU_2_16 as StdField with uid="DYUYXOSUGA",rtseq=70,rtrep=.f.,;
    cFormVar = "w_ANCONCAU", cQueryName = "ANCONCAU",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Contropartita per cauzioni imballi",;
    HelpContextID = 190182565,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=174, Top=196, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCC", oKey_2_1="ANCODICE", oKey_2_2="this.w_ANCONCAU"

  func oANCONCAU_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_VEFA='S' And .w_ANFLGCAU='S')
    endwith
   endif
  endfunc

  func oANCONCAU_2_16.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' Or .w_ANFLGCAU<>'S')
    endwith
  endfunc

  func oANCONCAU_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCONCAU_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCONCAU_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCC)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oANCONCAU_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"",'',this.parent.oContained
  endproc
  proc oANCONCAU_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCC
     i_obj.w_ANCODICE=this.parent.oContained.w_ANCONCAU
     i_obj.ecpSave()
  endproc


  add object oANOPETRE_2_17 as StdCombo with uid="RDWLAZXTUW",rtseq=71,rtrep=.f.,left=245,top=267,width=136,height=21;
    , ToolTipText = "Classificazione soggetto";
    , HelpContextID = 85707595;
    , cFormVar="w_ANOPETRE",RowSource=""+"Escludi,"+"Corrispettivi periodici,"+"Contratti collegati,"+"Non definibile", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oANOPETRE_2_17.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oANOPETRE_2_17.GetRadio()
    this.Parent.oContained.w_ANOPETRE = this.RadioValue()
    return .t.
  endfunc

  func oANOPETRE_2_17.SetRadio()
    this.Parent.oContained.w_ANOPETRE=trim(this.Parent.oContained.w_ANOPETRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANOPETRE=='E',1,;
      iif(this.Parent.oContained.w_ANOPETRE=='P',2,;
      iif(this.Parent.oContained.w_ANOPETRE=='C',3,;
      iif(this.Parent.oContained.w_ANOPETRE=='N',4,;
      0))))
  endfunc


  add object oANTIPPRE_2_18 as StdCombo with uid="OKEXLKPYZF",rtseq=72,rtrep=.f.,left=592,top=267,width=136,height=21;
    , ToolTipText = "Classificazione operazioni tra beni e servizi";
    , HelpContextID = 29694795;
    , cFormVar="w_ANTIPPRE",RowSource=""+"Non definibile,"+"Beni,"+"Servizi", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oANTIPPRE_2_18.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'B',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oANTIPPRE_2_18.GetRadio()
    this.Parent.oContained.w_ANTIPPRE = this.RadioValue()
    return .t.
  endfunc

  func oANTIPPRE_2_18.SetRadio()
    this.Parent.oContained.w_ANTIPPRE=trim(this.Parent.oContained.w_ANTIPPRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANTIPPRE=='N',1,;
      iif(this.Parent.oContained.w_ANTIPPRE=='B',2,;
      iif(this.Parent.oContained.w_ANTIPPRE=='S',3,;
      0)))
  endfunc

  add object oMINCON_2_22 as StdField with uid="GZHZALMWXS",rtseq=76,rtrep=.f.,;
    cFormVar = "w_MINCON", cQueryName = "MINCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Limite inferiore del conto fornitori nello studio",;
    HelpContextID = 5327162,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=392, Top=467, InputMask=replicate('X',6)

  func oMINCON_2_22.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP$'N-G')
    endwith
  endfunc

  add object oANPARTSN_2_23 as StdCheck with uid="IHPYTJFNXF",rtseq=77,rtrep=.f.,left=10, top=350, caption="Gestione partite",;
    ToolTipText = "Se attivo: il fornitore � gestito nelle partite",;
    HelpContextID = 98360148,;
    cFormVar="w_ANPARTSN", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Test partite incongruente con test esigibilit�";
   , bGlobalFont=.t.


  func oANPARTSN_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANPARTSN_2_23.GetRadio()
    this.Parent.oContained.w_ANPARTSN = this.RadioValue()
    return .t.
  endfunc

  func oANPARTSN_2_23.SetRadio()
    this.Parent.oContained.w_ANPARTSN=trim(this.Parent.oContained.w_ANPARTSN)
    this.value = ;
      iif(this.Parent.oContained.w_ANPARTSN=='S',1,;
      0)
  endfunc

  func oANPARTSN_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERPAR='S')
    endwith
   endif
  endfunc

  add object oANFLAACC_2_24 as StdCheck with uid="MAKYGGITBG",rtseq=78,rtrep=.f.,left=10, top=387, caption="Accorpa acconti",;
    ToolTipText = "Se attivo in fase di contabilizzazione documenti gli acconti gi� registrati verranno accorpati alla partita della fattura",;
    HelpContextID = 30882633,;
    cFormVar="w_ANFLAACC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLAACC_2_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLAACC_2_24.GetRadio()
    this.Parent.oContained.w_ANFLAACC = this.RadioValue()
    return .t.
  endfunc

  func oANFLAACC_2_24.SetRadio()
    this.Parent.oContained.w_ANFLAACC=trim(this.Parent.oContained.w_ANFLAACC)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLAACC=='S',1,;
      0)
  endfunc

  add object oMAXCON_2_25 as StdField with uid="GEFURGSMOD",rtseq=79,rtrep=.f.,;
    cFormVar = "w_MAXCON", cQueryName = "MAXCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Limite superiore del conto fornitori nello studio",;
    HelpContextID = 5288250,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=564, Top=466, InputMask=replicate('X',6)

  func oMAXCON_2_25.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP$'N-G')
    endwith
  endfunc

  add object oAFFLINTR_2_26 as StdCheck with uid="HIREFEMNLO",rtseq=80,rtrep=.f.,left=10, top=424, caption="Fornitore INTRA",;
    ToolTipText = "Se attivo: il fornitore � un soggetto INTRA",;
    HelpContextID = 257373016,;
    cFormVar="w_AFFLINTR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAFFLINTR_2_26.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAFFLINTR_2_26.GetRadio()
    this.Parent.oContained.w_AFFLINTR = this.RadioValue()
    return .t.
  endfunc

  func oAFFLINTR_2_26.SetRadio()
    this.Parent.oContained.w_AFFLINTR=trim(this.Parent.oContained.w_AFFLINTR)
    this.value = ;
      iif(this.Parent.oContained.w_AFFLINTR=='S',1,;
      0)
  endfunc

  add object oANFLBLLS_2_27 as StdCheck with uid="KZJFWBNBAT",rtseq=81,rtrep=.f.,left=143, top=350, caption="Fiscalit� privilegiata",;
    ToolTipText = "Se attivato, il fornitore � identificato come operatore economico con sede, residenza o domicilio negli Stati o territori a regime privilegiato",;
    HelpContextID = 51954855,;
    cFormVar="w_ANFLBLLS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLBLLS_2_27.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLBLLS_2_27.GetRadio()
    this.Parent.oContained.w_ANFLBLLS = this.RadioValue()
    return .t.
  endfunc

  func oANFLBLLS_2_27.SetRadio()
    this.Parent.oContained.w_ANFLBLLS=trim(this.Parent.oContained.w_ANFLBLLS)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLBLLS=='S',1,;
      0)
  endfunc

  add object oANFLSOAL_2_28 as StdCheck with uid="IRHRPVDJDH",rtseq=82,rtrep=.f.,left=143, top=387, caption="Soggetto terzo",;
    ToolTipText = "Se attivato, sar� possibile indicare l'intestatario effettivo della blacklist in primanota",;
    HelpContextID = 252232878,;
    cFormVar="w_ANFLSOAL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLSOAL_2_28.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLSOAL_2_28.GetRadio()
    this.Parent.oContained.w_ANFLSOAL = this.RadioValue()
    return .t.
  endfunc

  func oANFLSOAL_2_28.SetRadio()
    this.Parent.oContained.w_ANFLSOAL=trim(this.Parent.oContained.w_ANFLSOAL)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLSOAL=='S',1,;
      0)
  endfunc

  func oANFLSOAL_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLBLLS='N')
    endwith
   endif
  endfunc

  add object oANFLBODO_2_29 as StdCheck with uid="QMTBBQJSVA",rtseq=83,rtrep=.f.,left=143, top=424, caption="Bolla doganale",;
    ToolTipText = "Se attivo, il fornitore � soggetto a bolla doganale",;
    HelpContextID = 266812245,;
    cFormVar="w_ANFLBODO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLBODO_2_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANFLBODO_2_29.GetRadio()
    this.Parent.oContained.w_ANFLBODO = this.RadioValue()
    return .t.
  endfunc

  func oANFLBODO_2_29.SetRadio()
    this.Parent.oContained.w_ANFLBODO=trim(this.Parent.oContained.w_ANFLBODO)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLBODO=='S',1,;
      0)
  endfunc

  func oANFLBODO_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLSOAL # 'S' and g_TRAEXP # 'N' and .w_AFFLINTR#'S')
    endwith
   endif
  endfunc

  add object oANIVASOS_2_30 as StdCheck with uid="UXSDBOJDQM",rtseq=84,rtrep=.f.,left=294, top=424, caption="Maturazione temporale IVA in sospensione",;
    ToolTipText = "Maturazione temporale IVA in sospensione",;
    HelpContextID = 203330727,;
    cFormVar="w_ANIVASOS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANIVASOS_2_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANIVASOS_2_30.GetRadio()
    this.Parent.oContained.w_ANIVASOS = this.RadioValue()
    return .t.
  endfunc

  func oANIVASOS_2_30.SetRadio()
    this.Parent.oContained.w_ANIVASOS=trim(this.Parent.oContained.w_ANIVASOS)
    this.value = ;
      iif(this.Parent.oContained.w_ANIVASOS=='S',1,;
      0)
  endfunc

  add object oANCODSTU_2_31 as StdField with uid="SCWEEIMALA",rtseq=85,rtrep=.f.,;
    cFormVar = "w_ANCODSTU", cQueryName = "ANCODSTU",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice deve essere compreso tra il limite inferiore e quello superiore oppure lunghezza non consentita",;
    ToolTipText = "Codice associato al fornitore nello studio",;
    HelpContextID = 67767131,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=135, Top=466, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  func oANCODSTU_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_LEMC='S' AND g_TRAEXP<>'N')
    endwith
   endif
  endfunc

  func oANCODSTU_2_31.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP='N')
    endwith
  endfunc

  func oANCODSTU_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCONSTUCF(.w_ANCODSTU,.w_CODSTU))
    endwith
    return bRes
  endfunc


  add object oBtn_2_32 as StdButton with uid="GAAUWJQKCM",left=224, top=466, width=22,height=22,;
    CpPicture="exe\bmp\auto.ico", caption="", nPag=2;
    , ToolTipText = "Ricalcola il sottoconto studio";
    , HelpContextID = 52384218;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_32.Click()
      this.parent.oContained.NotifyEvent("ForzaRicalcolaStudio")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_LEMC='S' AND g_TRAEXP<>'N')
      endwith
    endif
  endfunc

  func oBtn_2_32.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP$'N-G' OR .cfunction<>'Load')
     endwith
    endif
  endfunc

  add object oANCODSOG_2_33 as StdField with uid="JJMFBIAWML",rtseq=86,rtrep=.f.,;
    cFormVar = "w_ANCODSOG", cQueryName = "ANCODSOG",;
    bObbl = .f. , nPag = 2, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice soggetto (anagrafico unico)",;
    HelpContextID = 200668339,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=489, Top=350, cSayPict="repl('!',8)", cGetPict="repl('!',8)", InputMask=replicate('X',8)

  func oANCODSOG_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_LEMC='S' AND g_TRAEXP<>'N')
    endwith
   endif
  endfunc

  func oANCODSOG_2_33.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP<>'C')
    endwith
  endfunc

  add object oANCODCAT_2_34 as StdField with uid="DRDRMCQAXA",rtseq=87,rtrep=.f.,;
    cFormVar = "w_ANCODCAT", cQueryName = "ANCODCAT",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice catastale del comune di residenza",;
    HelpContextID = 200668326,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=655, Top=350, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="COD_CATA", cZoomOnZoom="GSAR_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_ANCODCAT"

  func oANCODCAT_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODCAT_2_34.ecpDrop(oSource)
    this.Parent.oContained.link_2_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODCAT_2_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_CATA','*','CCCODICE',cp_AbsName(this.parent,'oANCODCAT_2_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACC',"Elenco codici catastali dei comuni",'',this.parent.oContained
  endproc
  proc oANCODCAT_2_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_ANCODCAT
     i_obj.ecpSave()
  endproc


  add object oBtn_2_51 as StdButton with uid="GBMHNPDTNI",left=578, top=490, width=48,height=45,;
    CpPicture="BMP\SCHEDE.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alle schede contabili del fornitore";
    , HelpContextID = 184425758;
    , Caption='\<Contabile';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_51.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_51.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_2_51.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc


  add object oBtn_2_52 as StdButton with uid="NYJPLYVXKM",left=631, top=490, width=48,height=45,;
    CpPicture="BMP\INTENTI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alle dichiarazioni di intento";
    , HelpContextID = 21139835;
    , Caption='\<Dic.Intento';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_52.Click()
      do GSCG_KL2 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_52.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_2_52.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_ANCODICE))
     endwith
    endif
  endfunc


  add object oBtn_2_53 as StdButton with uid="YYURZCSNEG",left=683, top=490, width=48,height=45,;
    CpPicture="BMP\SALDI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai saldi del fornitore";
    , HelpContextID = 210653402;
    , Caption='\<Saldi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_53.Click()
      with this.Parent.oContained
        GSAR_BGS(this.Parent.oContained,"F", .w_ANCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_53.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  add object oDESCAU_2_73 as StdField with uid="TPDVVFKXNE",rtseq=195,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 170982858,;
   bGlobalFont=.t.,;
    Height=21, Width=309, Left=296, Top=196, InputMask=replicate('X',40)

  func oDESCAU_2_73.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' Or .w_ANFLGCAU<>'S')
    endwith
  endfunc


  add object oBtn_2_79 as StdButton with uid="YYONVGWQOM",left=224, top=466, width=22,height=22,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per abbinare sottoconto studio";
    , HelpContextID = 59139626;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_79.Click()
      do GSLMAKRS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_79.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP<>'G' OR .cfunction<>'Load')
     endwith
    endif
  endfunc

  add object oStr_2_35 as StdString with uid="CUPMNAFNRF",Visible=.t., Left=35, Top=16,;
    Alignment=1, Width=137, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_38 as StdString with uid="RPCFXOUKEC",Visible=.t., Left=35, Top=76,;
    Alignment=1, Width=137, Height=15,;
    Caption="Collegamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="WTJDGWCTST",Visible=.t., Left=4, Top=321,;
    Alignment=0, Width=75, Height=15,;
    Caption="Altri dati"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="IRXWJAIGCU",Visible=.t., Left=35, Top=46,;
    Alignment=1, Width=137, Height=15,;
    Caption="Mastro contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="OOHZOFUXBH",Visible=.t., Left=46, Top=106,;
    Alignment=1, Width=126, Height=15,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  func oStr_2_46.mHide()
    with this.Parent.oContained
      return (g_ACQU<>'S')
    endwith
  endfunc

  add object oStr_2_47 as StdString with uid="CAWKTCOOVL",Visible=.t., Left=4, Top=467,;
    Alignment=1, Width=126, Height=18,;
    Caption="Sottoconto studio:"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP='N')
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="EYITWPXPKQ",Visible=.t., Left=295, Top=467,;
    Alignment=1, Width=94, Height=18,;
    Caption="Limite inferiore:"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP$'N-G')
    endwith
  endfunc

  add object oStr_2_49 as StdString with uid="XXHEGRMXKA",Visible=.t., Left=459, Top=466,;
    Alignment=1, Width=102, Height=18,;
    Caption="Limite superiore:"  ;
  , bGlobalFont=.t.

  func oStr_2_49.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP$'N-G')
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="AMXLPVRLOY",Visible=.t., Left=46, Top=136,;
    Alignment=1, Width=126, Height=18,;
    Caption="Cod. IVA esenz./agev.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_56 as StdString with uid="PXOKWNNJPX",Visible=.t., Left=383, Top=350,;
    Alignment=1, Width=103, Height=18,;
    Caption="Cod. soggetto:"  ;
  , bGlobalFont=.t.

  func oStr_2_56.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP<>'C')
    endwith
  endfunc

  add object oStr_2_58 as StdString with uid="GCCUOCCYZL",Visible=.t., Left=59, Top=166,;
    Alignment=1, Width=113, Height=18,;
    Caption="Tipo operazione IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_62 as StdString with uid="TFABSECPSF",Visible=.t., Left=7, Top=196,;
    Alignment=1, Width=166, Height=18,;
    Caption="Contropartita per cauzioni:"  ;
  , bGlobalFont=.t.

  func oStr_2_62.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' Or .w_ANFLGCAU<>'S')
    endwith
  endfunc

  add object oStr_2_65 as StdString with uid="DMHPXWFGXD",Visible=.t., Left=561, Top=350,;
    Alignment=1, Width=88, Height=18,;
    Caption="Cod. comune:"  ;
  , bGlobalFont=.t.

  add object oStr_2_74 as StdString with uid="SUZRUPRUCS",Visible=.t., Left=443, Top=270,;
    Alignment=1, Width=147, Height=18,;
    Caption="Tipologia prevalente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_75 as StdString with uid="FCYVCLTNTI",Visible=.t., Left=76, Top=270,;
    Alignment=1, Width=166, Height=18,;
    Caption="Operazioni rilevanti IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_76 as StdString with uid="ZVACPDOZTI",Visible=.t., Left=1, Top=237,;
    Alignment=0, Width=332, Height=18,;
    Caption="Comunicazione operazioni superiori a 3.000 euro"  ;
  , bGlobalFont=.t.

  add object oBox_2_39 as StdBox with uid="JKIXWTRHBD",left=1, top=335, width=732,height=1

  add object oBox_2_77 as StdBox with uid="USUYVAUTGO",left=3, top=255, width=730,height=2
enddefine
define class tgsar_afrPag3 as StdContainer
  Width  = 737
  height = 560
  stdWidth  = 737
  stdheight = 560
  resizeXpos=367
  resizeYpos=341
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oECTERRIT_3_9 as StdField with uid="OZRPSZMVMI",rtseq=196,rtrep=.f.,;
    cFormVar = "w_ECTERRIT", cQueryName = "ECTERRIT",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 65081498,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=189, Top=511, InputMask=replicate('X',30)

  add object oDESCREG_3_14 as StdField with uid="WZVTSUDYJN",rtseq=197,rtrep=.f.,;
    cFormVar = "w_DESCREG", cQueryName = "DESCREG",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 115278390,;
   bGlobalFont=.t.,;
    Height=21, Width=163, Left=189, Top=482, InputMask=replicate('X',30)

  add object oDESIRPEF_3_18 as StdField with uid="SWCQZMXUAI",rtseq=198,rtrep=.f.,;
    cFormVar = "w_DESIRPEF", cQueryName = "DESIRPEF",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 31785596,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=203, Top=113, InputMask=replicate('X',35)

  add object oDESTR2_3_26 as StdField with uid="ZKRIRCJNNX",rtseq=199,rtrep=.f.,;
    cFormVar = "w_DESTR2", cQueryName = "DESTR2",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 202374602,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=203, Top=143, InputMask=replicate('X',35)


  add object oANRITENU_3_31 as StdCombo with uid="JCZDGVFATC",rtseq=200,rtrep=.f.,left=138,top=84,width=134,height=21;
    , ToolTipText = "Soggetto o meno a ritenuta/e";
    , HelpContextID = 150668453;
    , cFormVar="w_ANRITENU",RowSource=""+"Non soggetto,"+"Solo IRPEF,"+"IRPEF e contr. prev.", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oANRITENU_3_31.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oANRITENU_3_31.GetRadio()
    this.Parent.oContained.w_ANRITENU = this.RadioValue()
    return .t.
  endfunc

  func oANRITENU_3_31.SetRadio()
    this.Parent.oContained.w_ANRITENU=trim(this.Parent.oContained.w_ANRITENU)
    this.value = ;
      iif(this.Parent.oContained.w_ANRITENU=='N',1,;
      iif(this.Parent.oContained.w_ANRITENU=='S',2,;
      iif(this.Parent.oContained.w_ANRITENU=='C',3,;
      0)))
  endfunc

  add object oANCODIRP_3_32 as StdField with uid="LRDFFOSDTV",rtseq=201,rtrep=.f.,;
    cFormVar = "w_ANCODIRP", cQueryName = "ANCODIRP",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un codice tributo I.R.PE.F.",;
    ToolTipText = "Codice tributo I.R.PE.F. di default",;
    HelpContextID = 100005034,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=138, Top=113, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_ANCODIRP"

  func oANCODIRP_3_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU$'CS' and g_RITE='S')
    endwith
   endif
  endfunc

  func oANCODIRP_3_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODIRP_3_32.ecpDrop(oSource)
    this.Parent.oContained.link_3_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODIRP_3_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oANCODIRP_3_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oANCODIRP_3_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_ANCODIRP
     i_obj.ecpSave()
  endproc

  add object oANCODTR2_3_33 as StdField with uid="EYFFUYUWBJ",rtseq=202,rtrep=.f.,;
    cFormVar = "w_ANCODTR2", cQueryName = "ANCODTR2",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo per contributo previdenziale",;
    HelpContextID = 84544312,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=138, Top=143, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_ANCODTR2"

  func oANCODTR2_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU='C' and g_RITE='S')
    endwith
   endif
  endfunc

  func oANCODTR2_3_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODTR2_3_33.ecpDrop(oSource)
    this.Parent.oContained.link_3_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODTR2_3_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oANCODTR2_3_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"",'GSRI2AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oANCODTR2_3_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_ANCODTR2
     i_obj.ecpSave()
  endproc

  add object oANPEINPS_3_34 as StdField with uid="LWMAAZXRAD",rtseq=203,rtrep=.f.,;
    cFormVar = "w_ANPEINPS", cQueryName = "ANPEINPS",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale imponibile ritenute previdenziali",;
    HelpContextID = 11478183,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=138, Top=201, cSayPict='"999.99"', cGetPict='"999.99"'

  func oANPEINPS_3_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU='C')
    endwith
   endif
  endfunc

  add object oANRIINPS_3_35 as StdField with uid="YIJMRTVWBP",rtseq=204,rtrep=.f.,;
    cFormVar = "w_ANRIINPS", cQueryName = "ANRIINPS",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale ritenute previdenziali",;
    HelpContextID = 11207847,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=355, Top=201, cSayPict='"999.99"', cGetPict='"999.99"'

  func oANRIINPS_3_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU='C')
    endwith
   endif
  endfunc

  add object oANCOINPS_3_36 as StdField with uid="BUBUNBPBVX",rtseq=205,rtrep=.f.,;
    cFormVar = "w_ANCOINPS", cQueryName = "ANCOINPS",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale ritenute previdenziali a carico del percipiente",;
    HelpContextID = 10876071,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=138, Top=230, cSayPict='"999.99"', cGetPict='"999.99"'

  func oANCOINPS_3_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU='C')
    endwith
   endif
  endfunc

  add object oANCASPRO_3_37 as StdField with uid="FQKDTOMIXN",rtseq=206,rtrep=.f.,;
    cFormVar = "w_ANCASPRO", cQueryName = "ANCASPRO",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale contributo cassa ordine",;
    HelpContextID = 32246613,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=355, Top=230, cSayPict='"999.99"', cGetPict='"999.99"'

  func oANCASPRO_3_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU$'CS')
    endwith
   endif
  endfunc

  add object oANCODATT_3_38 as StdField with uid="FPOPDWFMMT",rtseq=207,rtrep=.f.,;
    cFormVar = "w_ANCODATT", cQueryName = "ANCODATT",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice attivit� non valido (inserire 1..28)",;
    ToolTipText = "Codice attivit� (1-28)",;
    HelpContextID = 34212698,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=138, Top=259, cSayPict='"99"', cGetPict='"99"'

  func oANCODATT_3_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU='C')
    endwith
   endif
  endfunc

  func oANCODATT_3_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANCODATT>=1 AND .w_ANCODATT<=28)
    endwith
    return bRes
  endfunc

  add object oANCODASS_3_39 as StdField with uid="INQUGXNTZD",rtseq=208,rtrep=.f.,;
    cFormVar = "w_ANCODASS", cQueryName = "ANCODASS",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice altra assicurazione previdenziale",;
    HelpContextID = 34212697,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=355, Top=259, InputMask=replicate('X',3)

  func oANCODASS_3_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU='C' AND .w_FLACO2='I')
    endwith
   endif
  endfunc

  func oANCODASS_3_39.mHide()
    with this.Parent.oContained
      return (.w_FLACO2<>'I')
    endwith
  endfunc

  add object oANCOIMPS_3_40 as StdField with uid="MNYBNNXVEG",rtseq=209,rtrep=.f.,;
    cFormVar = "w_ANCOIMPS", cQueryName = "ANCOIMPS",;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice INPS",;
    HelpContextID = 27653287,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=138, Top=288, InputMask=replicate('X',20)

  func oANCOIMPS_3_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU='C' AND .w_FLACO2='I')
    endwith
   endif
  endfunc

  func oANCOIMPS_3_40.mHide()
    with this.Parent.oContained
      return (.w_FLACO2<>'I')
    endwith
  endfunc

  add object oCODI_3_41 as StdField with uid="MUOYTFKMWU",rtseq=222,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 54256602,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=138, Top=15, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15)

  add object oANFLGEST_3_42 as StdCheck with uid="HGJTZNOYNK",rtseq=223,rtrep=.f.,left=6, top=354, caption="Estero",;
    ToolTipText = "Percipiente estero",;
    HelpContextID = 104282970,;
    cFormVar="w_ANFLGEST", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANFLGEST_3_42.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLGEST_3_42.GetRadio()
    this.Parent.oContained.w_ANFLGEST = this.RadioValue()
    return .t.
  endfunc

  func oANFLGEST_3_42.SetRadio()
    this.Parent.oContained.w_ANFLGEST=trim(this.Parent.oContained.w_ANFLGEST)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLGEST=='S',1,;
      0)
  endfunc

  func oANFLGEST_3_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU<>'N')
    endwith
   endif
  endfunc

  add object oANDESCRI_3_44 as StdField with uid="IUVQERSCIZ",rtseq=224,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI,ANPARIVA,ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 185590961,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=283, Top=15, InputMask=replicate('X',40)

  add object oANFLSGRE_3_45 as StdCheck with uid="XHZQNUTKDR",rtseq=225,rtrep=.f.,left=186, top=354, caption="Soggetto non residente",;
    ToolTipText = "Se attivo: il soggetto � non residente",;
    HelpContextID = 118015157,;
    cFormVar="w_ANFLSGRE", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANFLSGRE_3_45.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLSGRE_3_45.GetRadio()
    this.Parent.oContained.w_ANFLSGRE = this.RadioValue()
    return .t.
  endfunc

  func oANFLSGRE_3_45.SetRadio()
    this.Parent.oContained.w_ANFLSGRE=trim(this.Parent.oContained.w_ANFLSGRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLSGRE=='S',1,;
      0)
  endfunc

  add object oANDESCR2_3_46 as StdField with uid="FOLIOGGOUF",rtseq=226,rtrep=.f.,;
    cFormVar = "w_ANDESCR2", cQueryName = "ANDESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 185590984,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=283, Top=43, InputMask=replicate('X',40)

  add object oANCOFISC_3_47 as StdField with uid="DPYWTHGPAZ",rtseq=227,rtrep=.f.,;
    cFormVar = "w_ANCOFISC", cQueryName = "ANCOFISC",;
    bObbl = .f. , nPag = 3, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice identificativo fiscale",;
    HelpContextID = 170527561,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=521, Top=358, cSayPict="repl('!',25)", cGetPict="repl('!',25)", InputMask=replicate('X',25)

  func oANCOFISC_3_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLGEST='S' OR .w_ANFLBLLS='S' )
    endwith
   endif
  endfunc

  add object oANSCHUMA_3_48 as StdCheck with uid="EURAXEWLHB",rtseq=228,rtrep=.f.,left=6, top=384, caption="Non residenti Schumacker",;
    ToolTipText = "Non residenti Schumacker",;
    HelpContextID = 163640505,;
    cFormVar="w_ANSCHUMA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANSCHUMA_3_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANSCHUMA_3_48.GetRadio()
    this.Parent.oContained.w_ANSCHUMA = this.RadioValue()
    return .t.
  endfunc

  func oANSCHUMA_3_48.SetRadio()
    this.Parent.oContained.w_ANSCHUMA=trim(this.Parent.oContained.w_ANSCHUMA)
    this.value = ;
      iif(this.Parent.oContained.w_ANSCHUMA=='S',1,;
      0)
  endfunc

  func oANSCHUMA_3_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLGEST='S' AND .w_ANFLSGRE<>'S')
    endwith
   endif
  endfunc

  add object oAN_EREDE_3_50 as StdCheck with uid="NHVKLZRYIJ",rtseq=229,rtrep=.f.,left=186, top=384, caption="Erede",;
    ToolTipText = "Erede - Ai fini di quanto disposto art.7 comma 3 Dpr 917/86",;
    HelpContextID = 115460939,;
    cFormVar="w_AN_EREDE", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAN_EREDE_3_50.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAN_EREDE_3_50.GetRadio()
    this.Parent.oContained.w_AN_EREDE = this.RadioValue()
    return .t.
  endfunc

  func oAN_EREDE_3_50.SetRadio()
    this.Parent.oContained.w_AN_EREDE=trim(this.Parent.oContained.w_AN_EREDE)
    this.value = ;
      iif(this.Parent.oContained.w_AN_EREDE=='S',1,;
      0)
  endfunc


  add object oANCODSNS_3_51 as StdCombo with uid="BXVCOUGSPN",value=1,rtseq=230,rtrep=.f.,left=129,top=423,width=390,height=21;
    , ToolTipText = "Codice che identifica la tipologia di somme non soggette";
    , HelpContextID = 200668327;
    , cFormVar="w_ANCODSNS",RowSource=""+"Nessuno,"+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Assegni di servizio civile,"+"7 - Erogazione di altri redditi non soggetti a ritenuta,"+"Erogazione di altri redditi non soggetti a ritenuta (Certificazione unica 2016),"+"Erogazione di altri redditi non soggetti a ritenuta (Certificazione unica 2017)", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oANCODSNS_3_51.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'5',;
    iif(this.value =5,'A',;
    iif(this.value =6,'7',;
    iif(this.value =7,'3',;
    iif(this.value =8,'6',;
    ' ')))))))))
  endfunc
  func oANCODSNS_3_51.GetRadio()
    this.Parent.oContained.w_ANCODSNS = this.RadioValue()
    return .t.
  endfunc

  func oANCODSNS_3_51.SetRadio()
    this.Parent.oContained.w_ANCODSNS=trim(this.Parent.oContained.w_ANCODSNS)
    this.value = ;
      iif(this.Parent.oContained.w_ANCODSNS=='',1,;
      iif(this.Parent.oContained.w_ANCODSNS=='1',2,;
      iif(this.Parent.oContained.w_ANCODSNS=='2',3,;
      iif(this.Parent.oContained.w_ANCODSNS=='5',4,;
      iif(this.Parent.oContained.w_ANCODSNS=='A',5,;
      iif(this.Parent.oContained.w_ANCODSNS=='7',6,;
      iif(this.Parent.oContained.w_ANCODSNS=='3',7,;
      iif(this.Parent.oContained.w_ANCODSNS=='6',8,;
      0))))))))
  endfunc

  func oANCODSNS_3_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU$'CS' AND g_RITE='S')
    endwith
   endif
  endfunc

  add object oANCODREG_3_53 as StdField with uid="IUNRALMTZB",rtseq=231,rtrep=.f.,;
    cFormVar = "w_ANCODREG", cQueryName = "ANCODREG",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice regione",;
    HelpContextID = 50989901,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=129, Top=482, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_ANCODREG"

  func oANCODREG_3_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLGEST<>'S')
    endwith
   endif
  endfunc

  func oANCODREG_3_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_53('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODREG_3_53.ecpDrop(oSource)
    this.Parent.oContained.link_3_53('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODREG_3_53.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oANCODREG_3_53'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regioni",'',this.parent.oContained
  endproc
  proc oANCODREG_3_53.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_ANCODREG
     i_obj.ecpSave()
  endproc

  add object oANCODCOM_3_54 as StdField with uid="HBGMXANCHV",rtseq=232,rtrep=.f.,;
    cFormVar = "w_ANCODCOM", cQueryName = "ANCODCOM",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune",;
    HelpContextID = 200668333,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=129, Top=511, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ENTI_COM", cZoomOnZoom="GSCG_AEC", oKey_1_1="ECCODICE", oKey_1_2="this.w_ANCODCOM"

  func oANCODCOM_3_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLGEST <> 'S')
    endwith
   endif
  endfunc

  func oANCODCOM_3_54.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_54('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODCOM_3_54.ecpDrop(oSource)
    this.Parent.oContained.link_3_54('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODCOM_3_54.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_COM','*','ECCODICE',cp_AbsName(this.parent,'oANCODCOM_3_54'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AEC',"CODICE ENTI/COMUNI",'',this.parent.oContained
  endproc
  proc oANCODCOM_3_54.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AEC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCODICE=this.parent.oContained.w_ANCODCOM
     i_obj.ecpSave()
  endproc


  add object oANCAURIT_3_55 as StdCombo with uid="ZUPWPWCTYH",rtseq=234,rtrep=.f.,left=138,top=173,width=76,height=21;
    , ToolTipText = "Causale prestazione mod. 770 telematico";
    , HelpContextID = 67898202;
    , cFormVar="w_ANCAURIT",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oANCAURIT_3_55.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'Z',;
    iif(this.value =33,'ZO',;
    space(2)))))))))))))))))))))))))))))))))))
  endfunc
  func oANCAURIT_3_55.GetRadio()
    this.Parent.oContained.w_ANCAURIT = this.RadioValue()
    return .t.
  endfunc

  func oANCAURIT_3_55.SetRadio()
    this.Parent.oContained.w_ANCAURIT=trim(this.Parent.oContained.w_ANCAURIT)
    this.value = ;
      iif(this.Parent.oContained.w_ANCAURIT=='A',1,;
      iif(this.Parent.oContained.w_ANCAURIT=='B',2,;
      iif(this.Parent.oContained.w_ANCAURIT=='C',3,;
      iif(this.Parent.oContained.w_ANCAURIT=='D',4,;
      iif(this.Parent.oContained.w_ANCAURIT=='E',5,;
      iif(this.Parent.oContained.w_ANCAURIT=='F',6,;
      iif(this.Parent.oContained.w_ANCAURIT=='G',7,;
      iif(this.Parent.oContained.w_ANCAURIT=='H',8,;
      iif(this.Parent.oContained.w_ANCAURIT=='I',9,;
      iif(this.Parent.oContained.w_ANCAURIT=='J',10,;
      iif(this.Parent.oContained.w_ANCAURIT=='K',11,;
      iif(this.Parent.oContained.w_ANCAURIT=='L',12,;
      iif(this.Parent.oContained.w_ANCAURIT=='L1',13,;
      iif(this.Parent.oContained.w_ANCAURIT=='M',14,;
      iif(this.Parent.oContained.w_ANCAURIT=='M1',15,;
      iif(this.Parent.oContained.w_ANCAURIT=='M2',16,;
      iif(this.Parent.oContained.w_ANCAURIT=='N',17,;
      iif(this.Parent.oContained.w_ANCAURIT=='O',18,;
      iif(this.Parent.oContained.w_ANCAURIT=='O1',19,;
      iif(this.Parent.oContained.w_ANCAURIT=='P',20,;
      iif(this.Parent.oContained.w_ANCAURIT=='Q',21,;
      iif(this.Parent.oContained.w_ANCAURIT=='R',22,;
      iif(this.Parent.oContained.w_ANCAURIT=='S',23,;
      iif(this.Parent.oContained.w_ANCAURIT=='T',24,;
      iif(this.Parent.oContained.w_ANCAURIT=='U',25,;
      iif(this.Parent.oContained.w_ANCAURIT=='V',26,;
      iif(this.Parent.oContained.w_ANCAURIT=='V1',27,;
      iif(this.Parent.oContained.w_ANCAURIT=='V2',28,;
      iif(this.Parent.oContained.w_ANCAURIT=='W',29,;
      iif(this.Parent.oContained.w_ANCAURIT=='X',30,;
      iif(this.Parent.oContained.w_ANCAURIT=='Y',31,;
      iif(this.Parent.oContained.w_ANCAURIT=='Z',32,;
      iif(this.Parent.oContained.w_ANCAURIT=='ZO',33,;
      0)))))))))))))))))))))))))))))))))
  endfunc

  func oANCAURIT_3_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU$'CS' AND g_RITE='S')
    endwith
   endif
  endfunc


  add object oANEVEECC_3_56 as StdCombo with uid="KHTCGZBVHM",rtseq=236,rtrep=.f.,left=129,top=453,width=528,height=21;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 102837065;
    , cFormVar="w_ANEVEECC",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"2 - Soggetti colpiti dagli eventi sismici del 24/08/2016 (Abruzzo Lazio Marche Umbria),"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"4 - Soggetti colpiti dagli eventi sismici dell'ottobre 2016 (Abruzzo Lazio Marche Umbria),"+"5 - Soggetti colpiti dagli eventi sismici di gennaio 2017,"+"8 - Contribuenti colpiti da altri eventi eccezionali (Certificazione unica e 770),"+"Contribuenti interessati da avversit� atmosferiche province di La Spezia e Massa-Carrara,"+"Contribuenti interessati da avversit� atmosferiche provincia di Genova,"+"Contribuenti colpiti da altri eventi eccezionali (770/2013),"+"Contribuenti colpiti da altri eventi eccezionali (770/2015),"+"Contribuenti residenti al 18/11/2013 nei comuni della Sardegna (eventi meteorologici novembre 2013),"+"Contribuenti colpiti da altri eventi eccezionali (Certificazione unica e 770),"+"Contribuenti colpiti da altri eventi eccezionali 2017 (Certificazione unica e 770)", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oANEVEECC_3_56.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'9',;
    iif(this.value =4,'3',;
    iif(this.value =5,'A',;
    iif(this.value =6,'C',;
    iif(this.value =7,'D',;
    iif(this.value =8,'5',;
    iif(this.value =9,'6',;
    iif(this.value =10,'7',;
    iif(this.value =11,'8',;
    iif(this.value =12,'2',;
    iif(this.value =13,'4',;
    iif(this.value =14,'B',;
    space(1))))))))))))))))
  endfunc
  func oANEVEECC_3_56.GetRadio()
    this.Parent.oContained.w_ANEVEECC = this.RadioValue()
    return .t.
  endfunc

  func oANEVEECC_3_56.SetRadio()
    this.Parent.oContained.w_ANEVEECC=trim(this.Parent.oContained.w_ANEVEECC)
    this.value = ;
      iif(this.Parent.oContained.w_ANEVEECC=='0',1,;
      iif(this.Parent.oContained.w_ANEVEECC=='1',2,;
      iif(this.Parent.oContained.w_ANEVEECC=='9',3,;
      iif(this.Parent.oContained.w_ANEVEECC=='3',4,;
      iif(this.Parent.oContained.w_ANEVEECC=='A',5,;
      iif(this.Parent.oContained.w_ANEVEECC=='C',6,;
      iif(this.Parent.oContained.w_ANEVEECC=='D',7,;
      iif(this.Parent.oContained.w_ANEVEECC=='5',8,;
      iif(this.Parent.oContained.w_ANEVEECC=='6',9,;
      iif(this.Parent.oContained.w_ANEVEECC=='7',10,;
      iif(this.Parent.oContained.w_ANEVEECC=='8',11,;
      iif(this.Parent.oContained.w_ANEVEECC=='2',12,;
      iif(this.Parent.oContained.w_ANEVEECC=='4',13,;
      iif(this.Parent.oContained.w_ANEVEECC=='B',14,;
      0))))))))))))))
  endfunc


  add object oANCATPAR_3_57 as StdCombo with uid="VMSQKVGLON",value=30,rtseq=237,rtrep=.f.,left=619,top=423,width=95,height=22;
    , ToolTipText = "Categorie particolari";
    , HelpContextID = 235140264;
    , cFormVar="w_ANCATPAR",RowSource=""+"A,"+"B,"+"C,"+"D,"+"E,"+"F,"+"G,"+"H,"+"K,"+"L,"+"M,"+"N,"+"P,"+"Q,"+"R,"+"S,"+"T,"+"T1,"+"T2,"+"T3,"+"T4,"+"U,"+"V,"+"W,"+"Y,"+"Z,"+"Z1,"+"Z2,"+"Z3,"+"Non gestito", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oANCATPAR_3_57.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'K',;
    iif(this.value =10,'L',;
    iif(this.value =11,'M',;
    iif(this.value =12,'N',;
    iif(this.value =13,'P',;
    iif(this.value =14,'Q',;
    iif(this.value =15,'R',;
    iif(this.value =16,'S',;
    iif(this.value =17,'T',;
    iif(this.value =18,'T1',;
    iif(this.value =19,'T2',;
    iif(this.value =20,'T3',;
    iif(this.value =21,'T4',;
    iif(this.value =22,'U',;
    iif(this.value =23,'V',;
    iif(this.value =24,'W',;
    iif(this.value =25,'Y',;
    iif(this.value =26,'Z',;
    iif(this.value =27,'Z1',;
    iif(this.value =28,'Z2',;
    iif(this.value =29,'Z3',;
    iif(this.value =30,'  ',;
    space(2))))))))))))))))))))))))))))))))
  endfunc
  func oANCATPAR_3_57.GetRadio()
    this.Parent.oContained.w_ANCATPAR = this.RadioValue()
    return .t.
  endfunc

  func oANCATPAR_3_57.SetRadio()
    this.Parent.oContained.w_ANCATPAR=trim(this.Parent.oContained.w_ANCATPAR)
    this.value = ;
      iif(this.Parent.oContained.w_ANCATPAR=='A',1,;
      iif(this.Parent.oContained.w_ANCATPAR=='B',2,;
      iif(this.Parent.oContained.w_ANCATPAR=='C',3,;
      iif(this.Parent.oContained.w_ANCATPAR=='D',4,;
      iif(this.Parent.oContained.w_ANCATPAR=='E',5,;
      iif(this.Parent.oContained.w_ANCATPAR=='F',6,;
      iif(this.Parent.oContained.w_ANCATPAR=='G',7,;
      iif(this.Parent.oContained.w_ANCATPAR=='H',8,;
      iif(this.Parent.oContained.w_ANCATPAR=='K',9,;
      iif(this.Parent.oContained.w_ANCATPAR=='L',10,;
      iif(this.Parent.oContained.w_ANCATPAR=='M',11,;
      iif(this.Parent.oContained.w_ANCATPAR=='N',12,;
      iif(this.Parent.oContained.w_ANCATPAR=='P',13,;
      iif(this.Parent.oContained.w_ANCATPAR=='Q',14,;
      iif(this.Parent.oContained.w_ANCATPAR=='R',15,;
      iif(this.Parent.oContained.w_ANCATPAR=='S',16,;
      iif(this.Parent.oContained.w_ANCATPAR=='T',17,;
      iif(this.Parent.oContained.w_ANCATPAR=='T1',18,;
      iif(this.Parent.oContained.w_ANCATPAR=='T2',19,;
      iif(this.Parent.oContained.w_ANCATPAR=='T3',20,;
      iif(this.Parent.oContained.w_ANCATPAR=='T4',21,;
      iif(this.Parent.oContained.w_ANCATPAR=='U',22,;
      iif(this.Parent.oContained.w_ANCATPAR=='V',23,;
      iif(this.Parent.oContained.w_ANCATPAR=='W',24,;
      iif(this.Parent.oContained.w_ANCATPAR=='Y',25,;
      iif(this.Parent.oContained.w_ANCATPAR=='Z',26,;
      iif(this.Parent.oContained.w_ANCATPAR=='Z1',27,;
      iif(this.Parent.oContained.w_ANCATPAR=='Z2',28,;
      iif(this.Parent.oContained.w_ANCATPAR=='Z3',29,;
      iif(this.Parent.oContained.w_ANCATPAR=='',30,;
      0))))))))))))))))))))))))))))))
  endfunc

  add object oStr_3_10 as StdString with uid="EGIIRDEFHJ",Visible=.t., Left=16, Top=511,;
    Alignment=1, Width=110, Height=15,;
    Caption="Codice comune:"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="MNPCTLNOAX",Visible=.t., Left=-2, Top=423,;
    Alignment=1, Width=128, Height=15,;
    Caption="Somme non soggette:"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="SZFJNZMUPK",Visible=.t., Left=362, Top=358,;
    Alignment=1, Width=156, Height=18,;
    Caption="Codice identificativo fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="ENTKNSREFF",Visible=.t., Left=15, Top=482,;
    Alignment=1, Width=111, Height=15,;
    Caption="Codice regione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="HBKOSEUOHR",Visible=.t., Left=6, Top=454,;
    Alignment=1, Width=120, Height=18,;
    Caption="Eventi eccezionali:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="KNKJVYEGOO",Visible=.t., Left=222, Top=230,;
    Alignment=1, Width=130, Height=18,;
    Caption="% Contr. cassa ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="RINPZJSKLJ",Visible=.t., Left=17, Top=172,;
    Alignment=1, Width=118, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="GVMYSYKYSK",Visible=.t., Left=2, Top=113,;
    Alignment=1, Width=132, Height=18,;
    Caption="Codice tributo I.R.PE.F.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="PGFNSITHXT",Visible=.t., Left=191, Top=259,;
    Alignment=1, Width=161, Height=18,;
    Caption="Altra assicurazione prev.:"  ;
  , bGlobalFont=.t.

  func oStr_3_20.mHide()
    with this.Parent.oContained
      return (.w_FLACO2<>'I')
    endwith
  endfunc

  add object oStr_3_21 as StdString with uid="IZPAPNVVEE",Visible=.t., Left=1, Top=230,;
    Alignment=1, Width=133, Height=18,;
    Caption="% a carico percipiente:"  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="IDIMEAVTXO",Visible=.t., Left=277, Top=201,;
    Alignment=1, Width=75, Height=15,;
    Caption="% Ritenuta:"  ;
  , bGlobalFont=.t.

  add object oStr_3_23 as StdString with uid="NMNAUSNLKX",Visible=.t., Left=30, Top=201,;
    Alignment=1, Width=104, Height=15,;
    Caption="% Imponibile:"  ;
  , bGlobalFont=.t.

  add object oStr_3_24 as StdString with uid="AQVQGUFPDD",Visible=.t., Left=53, Top=288,;
    Alignment=1, Width=81, Height=15,;
    Caption="Codice INPS:"  ;
  , bGlobalFont=.t.

  func oStr_3_24.mHide()
    with this.Parent.oContained
      return (.w_FLACO2<>'I')
    endwith
  endfunc

  add object oStr_3_25 as StdString with uid="GUBTDRNKFX",Visible=.t., Left=6, Top=142,;
    Alignment=1, Width=128, Height=15,;
    Caption="Contrib. previdenziale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_27 as StdString with uid="CMDFSVHHWN",Visible=.t., Left=16, Top=259,;
    Alignment=1, Width=118, Height=15,;
    Caption="Codice attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_3_29 as StdString with uid="ZYXITPELLJ",Visible=.t., Left=8, Top=60,;
    Alignment=0, Width=266, Height=15,;
    Caption="Dati percipiente"  ;
  , bGlobalFont=.t.

  add object oStr_3_30 as StdString with uid="ZDVJJHTPKV",Visible=.t., Left=10, Top=84,;
    Alignment=1, Width=124, Height=15,;
    Caption="Ritenute:"  ;
  , bGlobalFont=.t.

  add object oStr_3_43 as StdString with uid="LRBPJDZWHR",Visible=.t., Left=62, Top=15,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_49 as StdString with uid="ETBQCNSQXN",Visible=.t., Left=3, Top=327,;
    Alignment=0, Width=239, Height=18,;
    Caption="Dati per mod. 770 e Certificazione unica"  ;
  , bGlobalFont=.t.

  add object oStr_3_58 as StdString with uid="JPKGHQDHNU",Visible=.t., Left=497, Top=424,;
    Alignment=1, Width=118, Height=18,;
    Caption="Cat. particolari:"  ;
  , bGlobalFont=.t.

  add object oBox_3_28 as StdBox with uid="FMAMHFUZWU",left=5, top=74, width=719,height=1

  add object oBox_3_52 as StdBox with uid="LIROKUFYHM",left=5, top=347, width=719,height=1
enddefine
define class tgsar_afrPag4 as StdContainer
  Width  = 737
  height = 560
  stdWidth  = 737
  stdheight = 560
  resizeXpos=302
  resizeYpos=308
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_4_10 as StdField with uid="OKDFCXBMCY",rtseq=161,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 54256602,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=144, Top=16, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15)

  add object oDES1_4_11 as StdField with uid="VGQVJLWJTK",rtseq=162,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55770570,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=283, Top=16, InputMask=replicate('X',40)

  add object oANCATCOM_4_12 as StdField with uid="HJEYEEASTW",rtseq=163,rtrep=.f.,;
    cFormVar = "w_ANCATCOM", cQueryName = "ANCATCOM",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale associata al fornitore",;
    HelpContextID = 184808621,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=48, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_ANCATCOM"

  func oANCATCOM_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATCOM_4_12.ecpDrop(oSource)
    this.Parent.oContained.link_4_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATCOM_4_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oANCATCOM_4_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oANCATCOM_4_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_ANCATCOM
     i_obj.ecpSave()
  endproc

  add object oANGRPDEF_4_13 as StdField with uid="OKHGEEOSET",rtseq=164,rtrep=.f.,;
    cFormVar = "w_ANGRPDEF", cQueryName = "ANGRPDEF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo output",;
    HelpContextID = 97340236,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=534, Top=48, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRP_DEFA", cZoomOnZoom="GSAR_AGD", oKey_1_1="GDCODICE", oKey_1_2="this.w_ANGRPDEF"

  func oANGRPDEF_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oANGRPDEF_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANGRPDEF_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRP_DEFA','*','GDCODICE',cp_AbsName(this.parent,'oANGRPDEF_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGD',"Gruppi output",'',this.parent.oContained
  endproc
  proc oANGRPDEF_4_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GDCODICE=this.parent.oContained.w_ANGRPDEF
     i_obj.ecpSave()
  endproc

  add object oDESCAC_4_14 as StdField with uid="MSIBZHMXSL",rtseq=165,rtrep=.f.,;
    cFormVar = "w_DESCAC", cQueryName = "DESCAC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 204537290,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=207, Top=48, InputMask=replicate('X',35)

  add object oANCATSCM_4_15 as StdField with uid="HXNUVOQMDO",rtseq=166,rtrep=.f.,;
    cFormVar = "w_ANCATSCM", cQueryName = "ANCATSCM",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria sconti / maggiorazioni inesistente o non di tipo fornitore",;
    ToolTipText = "Codice della categoria sconti / maggiorazioni associata al fornitore",;
    HelpContextID = 83626835,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=76, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_ANCATSCM"

  func oANCATSCM_4_15.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oANCATSCM_4_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATSCM_4_15.ecpDrop(oSource)
    this.Parent.oContained.link_4_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATSCM_4_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oANCATSCM_4_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSAR_AFR.CAT_SCMA_VZM',this.parent.oContained
  endproc
  proc oANCATSCM_4_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_ANCATSCM
     i_obj.ecpSave()
  endproc

  add object oDESSCM_4_16 as StdField with uid="MKUEXKTCYW",rtseq=167,rtrep=.f.,;
    cFormVar = "w_DESSCM", cQueryName = "DESSCM",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 33619402,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=207, Top=76, InputMask=replicate('X',35)

  func oDESSCM_4_16.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oANCODLIN_4_17 as StdField with uid="NGNRVMJOAW",rtseq=168,rtrep=.f.,;
    cFormVar = "w_ANCODLIN", cQueryName = "ANCODLIN",;
    bObbl = .t. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della lingua di appartenenza del fornitore",;
    HelpContextID = 218762068,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=104, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_ANCODLIN"

  func oANCODLIN_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODLIN_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODLIN_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oANCODLIN_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oANCODLIN_4_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_ANCODLIN
     i_obj.ecpSave()
  endproc

  add object oDESLIN_4_18 as StdField with uid="IVLDVRIOMF",rtseq=169,rtrep=.f.,;
    cFormVar = "w_DESLIN", cQueryName = "DESLIN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 11009482,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=207, Top=104, InputMask=replicate('X',30)

  add object oANCODVAL_4_19 as StdField with uid="RITOEYPPRU",rtseq=170,rtrep=.f.,;
    cFormVar = "w_ANCODVAL", cQueryName = "ANCODVAL",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta normalmente utilizzata per acquisti e ordini",;
    HelpContextID = 150336686,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=132, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_ANCODVAL"

  func oANCODVAL_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODVAL_4_19.ecpDrop(oSource)
    this.Parent.oContained.link_4_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODVAL_4_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oANCODVAL_4_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oANCODVAL_4_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_ANCODVAL
     i_obj.ecpSave()
  endproc

  add object oDESVAL_4_20 as StdField with uid="OATZSEFZAJ",rtseq=171,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52297162,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=207, Top=132, InputMask=replicate('X',35)

  add object oANNUMLIS_4_21 as StdField with uid="RTEWLGEIVH",rtseq=172,rtrep=.f.,;
    cFormVar = "w_ANNUMLIS", cQueryName = "ANNUMLIS",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del listino a cui fa riferimento il fornitore",;
    HelpContextID = 228637529,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=160, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ANNUMLIS"

  func oANNUMLIS_4_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oANNUMLIS_4_21.ecpDrop(oSource)
    this.Parent.oContained.link_4_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANNUMLIS_4_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oANNUMLIS_4_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oANNUMLIS_4_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_ANNUMLIS
     i_obj.ecpSave()
  endproc

  add object oDESLIS_4_22 as StdField with uid="VVNGAPJUAE",rtseq=173,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 195558858,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=207, Top=160, InputMask=replicate('X',40)

  add object oAN1SCONT_4_23 as StdField with uid="SFZRCQPBKZ",rtseq=174,rtrep=.f.,;
    cFormVar = "w_AN1SCONT", cQueryName = "AN1SCONT",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Prima maggiorazione (se positiva) o sconto (se negativa) praticata",;
    HelpContextID = 201894,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=144, Top=188, cSayPict='"999.99"', cGetPict='"999.99"'

  func oAN1SCONT_4_23.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oAN2SCONT_4_24 as StdField with uid="IFHXBCNCEU",rtseq=175,rtrep=.f.,;
    cFormVar = "w_AN2SCONT", cQueryName = "AN2SCONT",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Seconda eventuale maggiorazione (se positiva) o sconto (se negativa) praticata",;
    HelpContextID = 197798,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=207, Top=188, cSayPict='"999.99"', cGetPict='"999.99"'

  func oAN2SCONT_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AN1SCONT # 0)
    endwith
   endif
  endfunc

  func oAN2SCONT_4_24.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oANCODZON_4_25 as StdField with uid="WMWFYRZQQK",rtseq=176,rtrep=.f.,;
    cFormVar = "w_ANCODZON", cQueryName = "ANCODZON",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Codice della zona di appartenenza",;
    HelpContextID = 83227820,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=216, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_ANCODZON"

  func oANCODZON_4_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODZON_4_25.ecpDrop(oSource)
    this.Parent.oContained.link_4_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODZON_4_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oANCODZON_4_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oANCODZON_4_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_ANCODZON
     i_obj.ecpSave()
  endproc


  add object oANCONCON_4_26 as StdZTamTableCombo with uid="OUZRPVRXYH",rtseq=177,rtrep=.f.,left=144,top=242,width=304,height=21;
    , ToolTipText = "Condizione di consegna (utilizzata anche ai fini INTRA)";
    , HelpContextID = 190182572;
    , cFormVar="w_ANCONCON",tablefilter="", bObbl = .f. , nPag = 4;
    , cLinkFile="CON_CONS";
    , cTable='QUERY\GSARCACL.VQR',cKey='COCODICE',cValue='CODESCRI',cOrderBy='CODESCRI',xDefault=space(1);
  , bGlobalFont=.t.


  func oANCONCON_4_26.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oANCONCON_4_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCONCON_4_26.ecpDrop(oSource)
    this.Parent.oContained.link_4_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oDESZON_4_27 as StdField with uid="LPUKJXXDSE",rtseq=178,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 3800522,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=207, Top=216, InputMask=replicate('X',35)

  add object oANMAGTER_4_38 as StdField with uid="NIWSZSBHAF",rtseq=179,rtrep=.f.,;
    cFormVar = "w_ANMAGTER", cQueryName = "ANMAGTER",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino C/Lavoro inesistente o non tipo WIP",;
    ToolTipText = "Eventuale codice mag. utilizzato per il conto lavoro e/o per eventuale assegnazione dei documenti",;
    HelpContextID = 86813528,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=270, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_ANMAGTER"

  func oANMAGTER_4_38.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oANMAGTER_4_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMAGTER_4_38.ecpDrop(oSource)
    this.Parent.oContained.link_4_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANMAGTER_4_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oANMAGTER_4_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSCOWMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oANMAGTER_4_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_ANMAGTER
     i_obj.ecpSave()
  endproc

  add object oANCODESC_4_39 as StdField with uid="IWDVZKXXIR",rtseq=180,rtrep=.f.,;
    cFormVar = "w_ANCODESC", cQueryName = "ANCODESC",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Suffisso codice di ricerca esclusivo per cli/for. viene utilizzato come filtro nel caricamento dei codici articoli sui documenti",;
    HelpContextID = 101321545,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=298, InputMask=replicate('X',5)

  func oANCODESC_4_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_FLCESC='S')
    endwith
   endif
  endfunc

  func oANCODESC_4_39.mHide()
    with this.Parent.oContained
      return (g_FLCESC<>'S')
    endwith
  endfunc

  add object oANCODPOR_4_40 as StdField with uid="JIWLQHQAFS",rtseq=181,rtrep=.f.,;
    cFormVar = "w_ANCODPOR", cQueryName = "ANCODPOR",;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice porto/aeroporto di provenienza della merce",;
    HelpContextID = 250999976,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=144, Top=326, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COD_AREO", cZoomOnZoom="GLES_APP", oKey_1_1="PPCODICE", oKey_1_2="this.w_ANCODPOR"

  func oANCODPOR_4_40.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  func oANCODPOR_4_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODPOR_4_40.ecpDrop(oSource)
    this.Parent.oContained.link_4_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODPOR_4_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_AREO','*','PPCODICE',cp_AbsName(this.parent,'oANCODPOR_4_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GLES_APP',"Porti/aeroporti",'',this.parent.oContained
  endproc
  proc oANCODPOR_4_40.mZoomOnZoom
    local i_obj
    i_obj=GLES_APP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PPCODICE=this.parent.oContained.w_ANCODPOR
     i_obj.ecpSave()
  endproc

  add object oDESMAG_4_41 as StdField with uid="NQXKEZAIBO",rtseq=182,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 136773066,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=207, Top=270, InputMask=replicate('X',30)

  func oDESMAG_4_41.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oANMCALSI_4_44 as StdField with uid="AXAIXBZWZJ",rtseq=183,rtrep=.f.,;
    cFormVar = "w_ANMCALSI", cQueryName = "ANMCALSI",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese di imballo",;
    HelpContextID = 214870863,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=534, Top=270, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_ANMCALSI"

  func oANMCALSI_4_44.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oANMCALSI_4_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMCALSI_4_44.ecpDrop(oSource)
    this.Parent.oContained.link_4_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANMCALSI_4_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oANMCALSI_4_44'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oANMCALSI_4_44.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_ANMCALSI
     i_obj.ecpSave()
  endproc

  add object oANMCALST_4_45 as StdField with uid="AWSYWHRWVU",rtseq=184,rtrep=.f.,;
    cFormVar = "w_ANMCALST", cQueryName = "ANMCALST",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese di trasporto",;
    HelpContextID = 214870874,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=534, Top=298, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_ANMCALST"

  func oANMCALST_4_45.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oANMCALST_4_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMCALST_4_45.ecpDrop(oSource)
    this.Parent.oContained.link_4_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANMCALST_4_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oANMCALST_4_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oANMCALST_4_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_ANMCALST
     i_obj.ecpSave()
  endproc

  add object oMSDESIMB_4_46 as StdField with uid="BTQZBCVNPG",rtseq=185,rtrep=.f.,;
    cFormVar = "w_MSDESIMB", cQueryName = "MSDESIMB",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 84926200,;
   bGlobalFont=.t.,;
    Height=21, Width=144, Left=586, Top=270, InputMask=replicate('X',40)

  func oMSDESIMB_4_46.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oANSCORPO_4_47 as StdCheck with uid="XCHGKTPQUK",rtseq=186,rtrep=.f.,left=27, top=379, caption="Scorporo piede fattura",;
    ToolTipText = "Se attivo: esegue lo scorporo sul totale fattura",;
    HelpContextID = 206632107,;
    cFormVar="w_ANSCORPO", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oANSCORPO_4_47.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANSCORPO_4_47.GetRadio()
    this.Parent.oContained.w_ANSCORPO = this.RadioValue()
    return .t.
  endfunc

  func oANSCORPO_4_47.SetRadio()
    this.Parent.oContained.w_ANSCORPO=trim(this.Parent.oContained.w_ANSCORPO)
    this.value = ;
      iif(this.Parent.oContained.w_ANSCORPO=='S',1,;
      0)
  endfunc

  func oANSCORPO_4_47.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oANFLCODI_4_49 as StdCheck with uid="BQPGUWNBFR",rtseq=187,rtrep=.f.,left=27, top=402, caption="Codifica fornitore",;
    ToolTipText = "Se attivo: stampa la codifica articoli del fornitore nei documenti",;
    HelpContextID = 267860815,;
    cFormVar="w_ANFLCODI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oANFLCODI_4_49.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANFLCODI_4_49.GetRadio()
    this.Parent.oContained.w_ANFLCODI = this.RadioValue()
    return .t.
  endfunc

  func oANFLCODI_4_49.SetRadio()
    this.Parent.oContained.w_ANFLCODI=trim(this.Parent.oContained.w_ANFLCODI)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLCODI=='S',1,;
      0)
  endfunc

  add object oANFLIMBA_4_50 as StdCheck with uid="PPHKDJSCPD",rtseq=188,rtrep=.f.,left=210, top=379, caption="Imballo",;
    ToolTipText = "Se attivo: utilizza codici di ricerca di tipo imballo nelle unit� logistiche",;
    HelpContextID = 27837625,;
    cFormVar="w_ANFLIMBA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oANFLIMBA_4_50.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANFLIMBA_4_50.GetRadio()
    this.Parent.oContained.w_ANFLIMBA = this.RadioValue()
    return .t.
  endfunc

  func oANFLIMBA_4_50.SetRadio()
    this.Parent.oContained.w_ANFLIMBA=trim(this.Parent.oContained.w_ANFLIMBA)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLIMBA=='S',1,;
      0)
  endfunc

  func oANFLIMBA_4_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S')
    endwith
   endif
  endfunc

  func oANFLIMBA_4_50.mHide()
    with this.Parent.oContained
      return (g_MADV<>'S')
    endwith
  endfunc

  add object oANFLGCPZ_4_51 as StdCheck with uid="TSTVLEUDUA",rtseq=189,rtrep=.f.,left=210, top=402, caption="Pubblica su web",;
    ToolTipText = "Se attivo: trasferisce anagrafica fornitore a Web Application",;
    HelpContextID = 197706912,;
    cFormVar="w_ANFLGCPZ", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oANFLGCPZ_4_51.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLGCPZ_4_51.GetRadio()
    this.Parent.oContained.w_ANFLGCPZ = this.RadioValue()
    return .t.
  endfunc

  func oANFLGCPZ_4_51.SetRadio()
    this.Parent.oContained.w_ANFLGCPZ=trim(this.Parent.oContained.w_ANFLGCPZ)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLGCPZ=='S',1,;
      0)
  endfunc

  func oANFLGCPZ_4_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP$'SA' or g_CPIN='S' or g_REVI='S')
    endwith
   endif
  endfunc

  add object oMSDESTRA_4_52 as StdField with uid="SAQKMSVPYV",rtseq=190,rtrep=.f.,;
    cFormVar = "w_MSDESTRA", cQueryName = "MSDESTRA",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 99623175,;
   bGlobalFont=.t.,;
    Height=21, Width=144, Left=586, Top=298, InputMask=replicate('X',40)

  func oMSDESTRA_4_52.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oBtn_4_54 as StdButton with uid="BOJMNTCUPJ",left=505, top=449, width=48,height=45,;
    CpPicture="bmp\doc.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per accedere agli acquisti al fornitore";
    , HelpContextID = 109248623;
    , Caption='\<Acquisti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_54.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_54.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_4_54.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_ACQU<>'S')
     endwith
    endif
  endfunc


  add object oBtn_4_55 as StdButton with uid="VPGCDULAQO",left=558, top=449, width=48,height=45,;
    CpPicture="bmp\FATTURA.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per accedere agli ordini al fornitore";
    , HelpContextID = 54116378;
    , Caption='\<Ordini';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_55.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_55.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_4_55.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_ORDI<>'S')
     endwith
    endif
  endfunc


  add object oBtn_4_56 as StdButton with uid="FUJYHNGTOC",left=611, top=449, width=48,height=45,;
    CpPicture="bmp\prospvend.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per accedere al prospetto degli acquisti al fornitore";
    , HelpContextID = 235854487;
    , caption='\<Pr. Acq.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_56.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_56.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_4_56.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_ACQU<>'S')
     endwith
    endif
  endfunc


  add object oBtn_4_57 as StdButton with uid="WBSJFQXSYB",left=664, top=449, width=48,height=45,;
    CpPicture="BMP\ULTACQ.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per accedere agli ultimi articoli acquistati al fornitore";
    , HelpContextID = 99744011;
    , Caption='\<Art.Acquist.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_57.Click()
      do GSAR_KUA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_57.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_MAGA='S' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_4_57.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_MAGA<>'S' or Isalt())
     endwith
    endif
  endfunc

  add object oGDDESCRI_4_64 as StdField with uid="YMAIRCMCPA",rtseq=191,rtrep=.f.,;
    cFormVar = "w_GDDESCRI", cQueryName = "GDDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 185593425,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=595, Top=48, InputMask=replicate('X',40)


  add object oANFLGCAU_4_65 as StdCombo with uid="SCYCWCDNAE",rtseq=192,rtrep=.f.,left=588,top=377,width=122,height=21;
    , ToolTipText = "Se attivo: sul documento viene gestita la cauzione relativa agli imballi";
    , HelpContextID = 197706917;
    , cFormVar="w_ANFLGCAU",RowSource=""+"Disattiva,"+"Attiva", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oANFLGCAU_4_65.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oANFLGCAU_4_65.GetRadio()
    this.Parent.oContained.w_ANFLGCAU = this.RadioValue()
    return .t.
  endfunc

  func oANFLGCAU_4_65.SetRadio()
    this.Parent.oContained.w_ANFLGCAU=trim(this.Parent.oContained.w_ANFLGCAU)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLGCAU=='N',1,;
      iif(this.Parent.oContained.w_ANFLGCAU=='S',2,;
      0))
  endfunc

  func oANFLGCAU_4_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_VEFA='S')
    endwith
   endif
  endfunc

  func oANFLGCAU_4_65.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oDESCRI_4_69 as StdField with uid="NPAHKNWWCL",rtseq=193,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 86048202,;
   bGlobalFont=.t.,;
    Height=21, Width=270, Left=269, Top=326, InputMask=replicate('X',40)

  func oDESCRI_4_69.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oANFLAPCA_4_70 as StdCheck with uid="EYKGDCBJDX",rtseq=194,rtrep=.f.,left=27, top=425, caption="Applica contributi accessori",;
    ToolTipText = "Se attivo per il fornitore sar� applicata la combinazione dei contributi accessori indicata nella apposita tabella",;
    HelpContextID = 14105415,;
    cFormVar="w_ANFLAPCA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oANFLAPCA_4_70.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLAPCA_4_70.GetRadio()
    this.Parent.oContained.w_ANFLAPCA = this.RadioValue()
    return .t.
  endfunc

  func oANFLAPCA_4_70.SetRadio()
    this.Parent.oContained.w_ANFLAPCA=trim(this.Parent.oContained.w_ANFLAPCA)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLAPCA=='S',1,;
      0)
  endfunc

  func oANFLAPCA_4_70.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc


  add object oLinkPC_4_71 as StdButton with uid="UAJSEWFKVV",left=27, top=449, width=48,height=45,;
    CpPicture="BMP\CONTRACC.BMP", caption="", nPag=4;
    , ToolTipText = "Dettaglio contributo accessori generici";
    , HelpContextID = 219303131;
    , caption='\<Cont.acc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_4_71.Click()
      this.Parent.oContained.GSAR_MIN.LinkPCClick()
    endproc

  func oLinkPC_4_71.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ANFLAPCA = 'S')
      endwith
    endif
  endfunc

  func oLinkPC_4_71.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COAC='N')
     endwith
    endif
  endfunc

  add object oStr_4_28 as StdString with uid="XHEKPLTDDC",Visible=.t., Left=9, Top=162,;
    Alignment=1, Width=134, Height=15,;
    Caption="Cod. listino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_29 as StdString with uid="HVSVZENFHX",Visible=.t., Left=9, Top=106,;
    Alignment=1, Width=134, Height=15,;
    Caption="Cod. lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_4_30 as StdString with uid="SWTBROOQNY",Visible=.t., Left=9, Top=134,;
    Alignment=1, Width=134, Height=15,;
    Caption="Cod. valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_4_31 as StdString with uid="LOQYNVFEZR",Visible=.t., Left=71, Top=16,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_32 as StdString with uid="OBPKESPZIT",Visible=.t., Left=9, Top=50,;
    Alignment=1, Width=134, Height=15,;
    Caption="Cat.commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_4_33 as StdString with uid="RHEZWXLZRM",Visible=.t., Left=9, Top=218,;
    Alignment=1, Width=134, Height=15,;
    Caption="Cod. zona:"  ;
  , bGlobalFont=.t.

  add object oStr_4_34 as StdString with uid="LTFCEHKTTD",Visible=.t., Left=9, Top=190,;
    Alignment=1, Width=134, Height=15,;
    Caption="Sconti/maggiorazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_4_35 as StdString with uid="SZAOQQLCJI",Visible=.t., Left=194, Top=188,;
    Alignment=2, Width=12, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_4_35.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_36 as StdString with uid="BGIBAUYKAF",Visible=.t., Left=9, Top=79,;
    Alignment=1, Width=134, Height=15,;
    Caption="Categoria sconti/mag.:"  ;
  , bGlobalFont=.t.

  func oStr_4_36.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_37 as StdString with uid="FTUIJFKGOB",Visible=.t., Left=9, Top=245,;
    Alignment=1, Width=134, Height=15,;
    Caption="Cond.di consegna:"  ;
  , bGlobalFont=.t.

  func oStr_4_37.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_42 as StdString with uid="QJHESAJQJY",Visible=.t., Left=2, Top=271,;
    Alignment=1, Width=141, Height=18,;
    Caption="Magazzino C/Lavoro:"  ;
  , bGlobalFont=.t.

  func oStr_4_42.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_43 as StdString with uid="ARKKOTMTWT",Visible=.t., Left=6, Top=300,;
    Alignment=1, Width=137, Height=18,;
    Caption="Suff. codici ricerca:"  ;
  , bGlobalFont=.t.

  func oStr_4_43.mHide()
    with this.Parent.oContained
      return (g_FLCESC<>'S')
    endwith
  endfunc

  add object oStr_4_48 as StdString with uid="NVGPYSMOVY",Visible=.t., Left=462, Top=272,;
    Alignment=1, Width=70, Height=18,;
    Caption="Imballo:"  ;
  , bGlobalFont=.t.

  func oStr_4_48.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_53 as StdString with uid="STWKWUGQOL",Visible=.t., Left=470, Top=300,;
    Alignment=1, Width=62, Height=18,;
    Caption="Trasporto:"  ;
  , bGlobalFont=.t.

  func oStr_4_53.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_58 as StdString with uid="MZWGSEIWCL",Visible=.t., Left=485, Top=248,;
    Alignment=0, Width=149, Height=18,;
    Caption="Metodi di calcolo spese"  ;
  , bGlobalFont=.t.

  func oStr_4_58.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_60 as StdString with uid="QEUDSZAUVL",Visible=.t., Left=4, Top=354,;
    Alignment=0, Width=129, Height=18,;
    Caption="Altri dati"  ;
  , bGlobalFont=.t.

  add object oStr_4_63 as StdString with uid="UEVQNRFMDX",Visible=.t., Left=452, Top=48,;
    Alignment=1, Width=80, Height=18,;
    Caption="Gr. output:"  ;
  , bGlobalFont=.t.

  add object oStr_4_66 as StdString with uid="PJIICSMGFY",Visible=.t., Left=458, Top=378,;
    Alignment=1, Width=127, Height=18,;
    Caption="Gestione cauzioni:"  ;
  , bGlobalFont=.t.

  func oStr_4_66.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oStr_4_67 as StdString with uid="LHAVWPRKUJ",Visible=.t., Left=167, Top=584,;
    Alignment=0, Width=374, Height=18,;
    Caption="Anconcon ha una sua classe come control stdztamtablecombo"  ;
  , bGlobalFont=.t.

  func oStr_4_67.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_4_68 as StdString with uid="ZSSFSTMGNP",Visible=.t., Left=5, Top=328,;
    Alignment=1, Width=138, Height=18,;
    Caption="Codice porto/aeroporto:"  ;
  , bGlobalFont=.t.

  func oStr_4_68.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oBox_4_59 as StdBox with uid="DAGKXRCDYS",left=475, top=264, width=262,height=1

  add object oBox_4_61 as StdBox with uid="JVKDYQGSXW",left=1, top=373, width=726,height=1
enddefine
define class tgsar_afrPag5 as StdContainer
  Width  = 737
  height = 560
  stdWidth  = 737
  stdheight = 560
  resizeXpos=310
  resizeYpos=141
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_5_1 as StdField with uid="MYZXVXKXFY",rtseq=137,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 54256602,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=148, Top=14, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15)

  add object oDES1_5_2 as StdField with uid="PRIXTMXAJP",rtseq=138,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55770570,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=290, Top=14, InputMask=replicate('X',40)

  add object oANCODPAG_5_4 as StdField with uid="JHOURHOAXZ",rtseq=139,rtrep=.f.,;
    cFormVar = "w_ANCODPAG", cQueryName = "ANCODPAG",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento che viene praticato dal fornitore",;
    HelpContextID = 250999987,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=148, Top=40, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_ANCODPAG"

  func oANCODPAG_5_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODPAG_5_4.ecpDrop(oSource)
    this.Parent.oContained.link_5_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODPAG_5_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oANCODPAG_5_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oANCODPAG_5_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_ANCODPAG
     i_obj.ecpSave()
  endproc

  add object oDESPAG_5_5 as StdField with uid="TRVMCHBBPI",rtseq=140,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 136576458,;
   bGlobalFont=.t.,;
    Height=21, Width=234, Left=209, Top=40, InputMask=replicate('X',30)

  add object oANGIOFIS_5_6 as StdField with uid="APJXJFWUVQ",rtseq=141,rtrep=.f.,;
    cFormVar = "w_ANGIOFIS", cQueryName = "ANGIOFIS",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale giorno fisso di scadenza",;
    HelpContextID = 129256281,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=549, Top=40, cSayPict='"99"', cGetPict='"99"'

  func oANGIOFIS_5_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANGIOFIS <= 31)
    endwith
    return bRes
  endfunc

  add object oAN1MESCL_5_7 as StdField with uid="KLUFOEGPXG",rtseq=142,rtrep=.f.,;
    cFormVar = "w_AN1MESCL", cQueryName = "AN1MESCL",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Primo mese in cui le scadenze sono rinviate al mese successivo",;
    HelpContextID = 68610898,;
   bGlobalFont=.t.,;
    Height=21, Width=29, Left=148, Top=66, cSayPict='"99"', cGetPict='"99"'

  func oAN1MESCL_5_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AN1MESCL<13 AND (.w_AN1MESCL<.w_AN2MESCL OR .w_AN2MESCL=0))
    endwith
    return bRes
  endfunc

  add object oDESMES1_5_8 as StdField with uid="DRKDNOZKYJ",rtseq=143,rtrep=.f.,;
    cFormVar = "w_DESMES1", cQueryName = "DESMES1",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 199687626,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=209, Top=66, InputMask=replicate('X',12)

  add object oANGIOSC1_5_9 as StdField with uid="NDHFRCHLPY",rtseq=144,rtrep=.f.,;
    cFormVar = "w_ANGIOSC1", cQueryName = "ANGIOSC1",;
    bObbl = .t. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorno del mese successivo al 1^mese escluso a cui viene rinviata la scadenza",;
    HelpContextID = 78924599,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=414, Top=66, cSayPict='"99"', cGetPict='"99"'

  func oANGIOSC1_5_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AN1MESCL>0)
    endwith
   endif
  endfunc

  func oANGIOSC1_5_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANGIOSC1 <= 31)
    endwith
    return bRes
  endfunc

  add object oAN2MESCL_5_10 as StdField with uid="IWSHYESHIL",rtseq=145,rtrep=.f.,;
    cFormVar = "w_AN2MESCL", cQueryName = "AN2MESCL",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Secondo mese in cui le scadenze sono rinviate al mese successivo",;
    HelpContextID = 68614994,;
   bGlobalFont=.t.,;
    Height=21, Width=29, Left=148, Top=92, cSayPict='"99"', cGetPict='"99"'

  func oAN2MESCL_5_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AN1MESCL > 0)
    endwith
   endif
  endfunc

  func oAN2MESCL_5_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AN2MESCL<13 AND (.w_AN1MESCL<.w_AN2MESCL OR .w_AN2MESCL=0))
    endwith
    return bRes
  endfunc


  add object oBtn_5_11 as StdButton with uid="HDTPBMCTVA",left=505, top=66, width=48,height=45,;
    CpPicture="bmp\SOLDIT.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per accedere alle partite relative al fornitore";
    , HelpContextID = 125964022;
    , Caption='\<Partite';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_11.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_5_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc


  add object oBtn_5_12 as StdButton with uid="NKIXVEUYVE",left=559, top=66, width=48,height=45,;
    CpPicture="BMP\estratto.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per accedere all'estratto conto relativo al fornitore";
    , HelpContextID = 221202700;
    , Caption='\<Estr.Conto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_12.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_5_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc

  add object oDESMES2_5_13 as StdField with uid="ETCBSYSDGZ",rtseq=146,rtrep=.f.,;
    cFormVar = "w_DESMES2", cQueryName = "DESMES2",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 68747830,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=209, Top=92, InputMask=replicate('X',12)

  add object oANGIOSC2_5_14 as StdField with uid="IOGCBXDPMJ",rtseq=147,rtrep=.f.,;
    cFormVar = "w_ANGIOSC2", cQueryName = "ANGIOSC2",;
    bObbl = .t. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorno del mese successivo al 2^mese escluso a cui viene rinviata la scadenza",;
    HelpContextID = 78924600,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=414, Top=92, cSayPict='"99"', cGetPict='"99"'

  func oANGIOSC2_5_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AN2MESCL>0)
    endwith
   endif
  endfunc

  func oANGIOSC2_5_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANGIOSC2 <= 31)
    endwith
    return bRes
  endfunc

  add object oANCODBAN_5_15 as StdField with uid="BBUAFOSGKH",rtseq=148,rtrep=.f.,;
    cFormVar = "w_ANCODBAN", cQueryName = "ANCODBAN",;
    bObbl = .f. , nPag = 5, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca su cui vengono effettuati i nostri pagamenti (bonifici) al fornitore",;
    HelpContextID = 217445548,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=130, Top=124, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_ANCODBAN"

  func oANCODBAN_5_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODBAN_5_15.ecpDrop(oSource)
    this.Parent.oContained.link_5_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODBAN_5_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oANCODBAN_5_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oANCODBAN_5_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_ANCODBAN
     i_obj.ecpSave()
  endproc

  add object oDESBAN_5_16 as StdField with uid="HQIYWQVBPY",rtseq=149,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 20053450,;
   bGlobalFont=.t.,;
    Height=21, Width=266, Left=265, Top=124, InputMask=replicate('X',42)

  add object oANCODBA2_5_17 as StdField with uid="VGTBXUNIPC",rtseq=150,rtrep=.f.,;
    cFormVar = "w_ANCODBA2", cQueryName = "ANCODBA2",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto o di tipo salvo buon fine",;
    ToolTipText = "Ns. C/Corrente, comunicato al fornitore per appoggio ricevute bancarie",;
    HelpContextID = 217445576,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=130, Top=152, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_ANCODBA2"

  func oANCODBA2_5_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODBA2_5_17.ecpDrop(oSource)
    this.Parent.oContained.link_5_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODBA2_5_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oANCODBA2_5_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco conti banche",'GSAR_ACL.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oANCODBA2_5_17.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_ANCODBA2
     i_obj.ecpSave()
  endproc

  add object oANFLINCA_5_18 as StdCheck with uid="JRGSODICYN",rtseq=151,rtrep=.f.,left=577, top=124, caption="Escludi spese incasso",;
    ToolTipText = "Se attivo: non applica mai le spese incasso",;
    HelpContextID = 257375047,;
    cFormVar="w_ANFLINCA", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oANFLINCA_5_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANFLINCA_5_18.GetRadio()
    this.Parent.oContained.w_ANFLINCA = this.RadioValue()
    return .t.
  endfunc

  func oANFLINCA_5_18.SetRadio()
    this.Parent.oContained.w_ANFLINCA=trim(this.Parent.oContained.w_ANFLINCA)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLINCA=='S',1,;
      0)
  endfunc

  add object oANSPEINC_5_19 as StdField with uid="LDMYZCPRQK",rtseq=152,rtrep=.f.,;
    cFormVar = "w_ANSPEINC", cQueryName = "ANSPEINC",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo delle spese di incasso riferite alla valuta e al codice pagamento del fornitore, se zero � ignorato",;
    HelpContextID = 98825399,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=650, Top=152, cSayPict='"999999.99"', cGetPict='"999999.99"'

  func oANSPEINC_5_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ANCODVAL) AND NOT EMPTY(.w_ANCODPAG) AND .w_ANFLINCA <> 'S')
    endwith
   endif
  endfunc


  add object oLinkPC_5_20 as stdDynamicChildContainer with uid="JZLXSYFZPA",left=11, top=177, width=716, height=270, bOnScreen=.t.;


  add object oANFLESIM_5_21 as StdCheck with uid="YWJNOPGXNM",rtseq=153,rtrep=.f.,left=22, top=453, caption="Escludi applicazione interessi di mora",;
    ToolTipText = "Se attivo il fornitore viene escluso dall'applicazione degli interessi di mora",;
    HelpContextID = 68631379,;
    cFormVar="w_ANFLESIM", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oANFLESIM_5_21.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANFLESIM_5_21.GetRadio()
    this.Parent.oContained.w_ANFLESIM = this.RadioValue()
    return .t.
  endfunc

  func oANFLESIM_5_21.SetRadio()
    this.Parent.oContained.w_ANFLESIM=trim(this.Parent.oContained.w_ANFLESIM)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLESIM=='S',1,;
      0)
  endfunc

  add object oANSAGINT_5_22 as StdField with uid="DXFDMRMDCA",rtseq=154,rtrep=.f.,;
    cFormVar = "w_ANSAGINT", cQueryName = "ANSAGINT",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saggio di interesse concordato e applicato al posto di quello della tabella saggio interesse di mora. Se attivo il flag spread su saggio di mora, la percentuale rappresenta il tasso da aggiungere o togliere dal saggio di mora definito in tabella",;
    HelpContextID = 97711270,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=446, Top=453, cSayPict='"999.99"', cGetPict='"999.99"'

  func oANSAGINT_5_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLESIM<>'S')
    endwith
   endif
  endfunc

  func oANSAGINT_5_22.mHide()
    with this.Parent.oContained
      return (.w_ANFLESIM='S')
    endwith
  endfunc

  add object oANSPRINT_5_23 as StdCheck with uid="LXHJDEPXRD",rtseq=155,rtrep=.f.,left=550, top=453, caption="Spread su saggio di mora",;
    ToolTipText = "Se attivo la percentuale specificata nel campo interesse di mora rappresenta il tasso da aggiungere o togliere dal tasso di mora definito nella tabella",;
    HelpContextID = 85193894,;
    cFormVar="w_ANSPRINT", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oANSPRINT_5_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANSPRINT_5_23.GetRadio()
    this.Parent.oContained.w_ANSPRINT = this.RadioValue()
    return .t.
  endfunc

  func oANSPRINT_5_23.SetRadio()
    this.Parent.oContained.w_ANSPRINT=trim(this.Parent.oContained.w_ANSPRINT)
    this.value = ;
      iif(this.Parent.oContained.w_ANSPRINT=='S',1,;
      0)
  endfunc

  func oANSPRINT_5_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANSAGINT<>0)
    endwith
   endif
  endfunc

  func oANSPRINT_5_23.mHide()
    with this.Parent.oContained
      return (.w_ANFLESIM='S')
    endwith
  endfunc

  add object oANFLACBD_5_24 as StdCheck with uid="VOYMZEZZOW",rtseq=156,rtrep=.f.,left=550, top=475, caption="Acquisto beni deperibili",;
    ToolTipText = "Default utilizzato nella creazione delle partite legate al fornitore (documenti, primanota e scadenze diverse)",;
    HelpContextID = 203998390,;
    cFormVar="w_ANFLACBD", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oANFLACBD_5_24.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANFLACBD_5_24.GetRadio()
    this.Parent.oContained.w_ANFLACBD = this.RadioValue()
    return .t.
  endfunc

  func oANFLACBD_5_24.SetRadio()
    this.Parent.oContained.w_ANFLACBD=trim(this.Parent.oContained.w_ANFLACBD)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLACBD=='S',1,;
      0)
  endfunc


  add object oANFLRAGG_5_25 as StdCombo with uid="HDCBPMEKJD",value=1,rtseq=157,rtrep=.f.,left=148,top=514,width=153,height=21;
    , ToolTipText = "Condiziona l'accorpamento di effetti nelle distinte";
    , HelpContextID = 48708429;
    , cFormVar="w_ANFLRAGG",RowSource=""+"No,"+"Solo fatture,"+"Fatture e note di credito", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oANFLRAGG_5_25.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    ' '))))
  endfunc
  func oANFLRAGG_5_25.GetRadio()
    this.Parent.oContained.w_ANFLRAGG = this.RadioValue()
    return .t.
  endfunc

  func oANFLRAGG_5_25.SetRadio()
    this.Parent.oContained.w_ANFLRAGG=trim(this.Parent.oContained.w_ANFLRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLRAGG=='',1,;
      iif(this.Parent.oContained.w_ANFLRAGG=='S',2,;
      iif(this.Parent.oContained.w_ANFLRAGG=='T',3,;
      0)))
  endfunc

  add object oANFLGAVV_5_26 as StdCheck with uid="NMQIIPQJJM",rtseq=158,rtrep=.f.,left=325, top=514, caption="Invio avviso",;
    ToolTipText = "Se attivo seleziona in automatico il fornitore nella stampa avvisi",;
    HelpContextID = 37174108,;
    cFormVar="w_ANFLGAVV", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oANFLGAVV_5_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLGAVV_5_26.GetRadio()
    this.Parent.oContained.w_ANFLGAVV = this.RadioValue()
    return .t.
  endfunc

  func oANFLGAVV_5_26.SetRadio()
    this.Parent.oContained.w_ANFLGAVV=trim(this.Parent.oContained.w_ANFLGAVV)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLGAVV=='S',1,;
      0)
  endfunc

  add object oANDATAVV_5_27 as StdField with uid="ZJVZCNUUMW",rtseq=159,rtrep=.f.,;
    cFormVar = "w_ANDATAVV", cQueryName = "ANDATAVV",;
    bObbl = .f. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di avviso",;
    HelpContextID = 50076508,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=645, Top=514

  add object oDESBA2_5_34 as StdField with uid="GPPSLMSXPS",rtseq=160,rtrep=.f.,;
    cFormVar = "w_DESBA2", cQueryName = "DESBA2",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 221380042,;
   bGlobalFont=.t.,;
    Height=21, Width=266, Left=265, Top=152, InputMask=replicate('X',42)

  add object oStr_5_3 as StdString with uid="WHWUWOKOAA",Visible=.t., Left=9, Top=14,;
    Alignment=1, Width=136, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_28 as StdString with uid="CRFSABEBPZ",Visible=.t., Left=9, Top=40,;
    Alignment=1, Width=136, Height=15,;
    Caption="Cod. pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_5_29 as StdString with uid="KXITJIBSWE",Visible=.t., Left=11, Top=121,;
    Alignment=1, Width=118, Height=18,;
    Caption="Banca del fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_5_30 as StdString with uid="BEBKDHCIBE",Visible=.t., Left=37, Top=66,;
    Alignment=1, Width=108, Height=15,;
    Caption="1^ Mese escluso:"  ;
  , bGlobalFont=.t.

  add object oStr_5_31 as StdString with uid="CSTDRTMULR",Visible=.t., Left=37, Top=92,;
    Alignment=1, Width=108, Height=15,;
    Caption="2^ Mese escluso:"  ;
  , bGlobalFont=.t.

  add object oStr_5_32 as StdString with uid="LKXDUTCPAQ",Visible=.t., Left=330, Top=66,;
    Alignment=1, Width=83, Height=15,;
    Caption="Rinvio al:"  ;
  , bGlobalFont=.t.

  add object oStr_5_33 as StdString with uid="LMWCRGNGJG",Visible=.t., Left=330, Top=92,;
    Alignment=1, Width=83, Height=15,;
    Caption="Rinvio al:"  ;
  , bGlobalFont=.t.

  add object oStr_5_35 as StdString with uid="BLWTMMIFTA",Visible=.t., Left=43, Top=152,;
    Alignment=1, Width=86, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_5_36 as StdString with uid="DSJOKZRHBA",Visible=.t., Left=495, Top=514,;
    Alignment=1, Width=146, Height=15,;
    Caption="Ultimo avviso:"  ;
  , bGlobalFont=.t.

  add object oStr_5_38 as StdString with uid="YMGHNMIBCZ",Visible=.t., Left=12, Top=487,;
    Alignment=0, Width=519, Height=15,;
    Caption="Altri dati"  ;
  , bGlobalFont=.t.

  add object oStr_5_39 as StdString with uid="JIOMYQZEVJ",Visible=.t., Left=453, Top=40,;
    Alignment=1, Width=93, Height=15,;
    Caption="Giorno fisso:"  ;
  , bGlobalFont=.t.

  add object oStr_5_40 as StdString with uid="QTFJFLYOHN",Visible=.t., Left=333, Top=456,;
    Alignment=1, Width=113, Height=18,;
    Caption="Interesse di mora:"  ;
  , bGlobalFont=.t.

  func oStr_5_40.mHide()
    with this.Parent.oContained
      return (.w_ANFLESIM='S')
    endwith
  endfunc

  add object oStr_5_41 as StdString with uid="RJKZATLUCC",Visible=.t., Left=21, Top=514,;
    Alignment=1, Width=124, Height=18,;
    Caption="Raggruppa scadenze:"  ;
  , bGlobalFont=.t.

  add object oStr_5_42 as StdString with uid="JKSCOWWUIY",Visible=.t., Left=533, Top=152,;
    Alignment=1, Width=117, Height=18,;
    Caption="Spese incasso:"  ;
  , bGlobalFont=.t.

  add object oBox_5_37 as StdBox with uid="DAWXUGVHGS",left=6, top=504, width=721,height=1
enddefine
define class tgsar_afrPag6 as StdContainer
  Width  = 737
  height = 560
  stdWidth  = 737
  stdheight = 560
  resizeYpos=144
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_6_1 as StdField with uid="APGHWERXVH",rtseq=135,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 54256602,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=132, Top=16, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15)

  add object oDES1_6_2 as StdField with uid="PRIXQQCNUR",rtseq=136,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55770570,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=273, Top=16, InputMask=replicate('X',40)


  add object oLinkPC_6_4 as stdDynamicChildContainer with uid="UMARVJJTPA",left=20, top=56, width=701, height=382, bOnScreen=.t.;


  add object oStr_6_3 as StdString with uid="XRNHBFEWME",Visible=.t., Left=7, Top=16,;
    Alignment=1, Width=122, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsar_afrPag7 as StdContainer
  Width  = 737
  height = 560
  stdWidth  = 737
  stdheight = 560
  resizeXpos=385
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_7_1 as StdField with uid="YPMXOLFETX",rtseq=133,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 54256602,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=132, Top=16, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15)

  add object oDES1_7_2 as StdField with uid="QKCAODGPMY",rtseq=134,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55770570,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=273, Top=16, InputMask=replicate('X',40)


  add object oLinkPC_7_4 as stdDynamicChildContainer with uid="WGXWOMQYVS",left=6, top=61, width=725, height=491, bOnScreen=.t.;


  add object oStr_7_3 as StdString with uid="TDBPBZJTZR",Visible=.t., Left=4, Top=16,;
    Alignment=1, Width=125, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsar_afrPag8 as StdContainer
  Width  = 737
  height = 560
  stdWidth  = 737
  stdheight = 560
  resizeXpos=472
  resizeYpos=390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDES1_8_1 as StdField with uid="SNPVIIXXOR",rtseq=129,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55770570,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=273, Top=16, InputMask=replicate('X',40)

  add object oCODI_8_3 as StdField with uid="GNUJKOMLUE",rtseq=130,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 54256602,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=132, Top=16, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15)

  add object oANCODGRU_8_4 as StdField with uid="HKYGCCIADF",rtseq=131,rtrep=.f.,;
    cFormVar = "w_ANCODGRU", cQueryName = "ANCODGRU",;
    bObbl = .f. , nPag = 8, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 133559461,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=132, Top=83, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="GRU_INTE", cZoomOnZoom="GSAR_AGR", oKey_1_1="GRCODICE", oKey_1_2="this.w_ANCODGRU"

  func oANCODGRU_8_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_8_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODGRU_8_4.ecpDrop(oSource)
    this.Parent.oContained.link_8_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODGRU_8_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_INTE','*','GRCODICE',cp_AbsName(this.parent,'oANCODGRU_8_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGR',"Gruppi intestatari EDI",'',this.parent.oContained
  endproc
  proc oANCODGRU_8_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GRCODICE=this.parent.oContained.w_ANCODGRU
     i_obj.ecpSave()
  endproc


  add object oLinkPC_8_6 as stdDynamicChildContainer with uid="HTFESTCBSO",left=135, top=115, width=467, height=262, bOnScreen=.t.;


  func oLinkPC_8_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_VEFA='N')
     endwith
    endif
  endfunc

  add object oDESGRU_8_7 as StdField with uid="MZLQBCCRMA",rtseq=132,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 152894922,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=219, Top=83, InputMask=replicate('X',35)

  add object oStr_8_2 as StdString with uid="UQNVCGZZYG",Visible=.t., Left=57, Top=16,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_8_5 as StdString with uid="JOYMDRCTFF",Visible=.t., Left=18, Top=85,;
    Alignment=1, Width=111, Height=18,;
    Caption="Gruppo intestari:"  ;
  , bGlobalFont=.t.

  add object oStr_8_8 as StdString with uid="SCBFEMWDNX",Visible=.t., Left=12, Top=57,;
    Alignment=0, Width=170, Height=18,;
    Caption="Dati EDI"  ;
  , bGlobalFont=.t.

  add object oBox_8_9 as StdBox with uid="EDLVUAHTEH",left=10, top=74, width=719,height=1
enddefine
define class tgsar_afrPag9 as StdContainer
  Width  = 737
  height = 560
  stdWidth  = 737
  stdheight = 560
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_9_1 as StdField with uid="VUGVNFGRHM",rtseq=213,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 54256602,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=132, Top=16, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15)

  add object oDES1_9_2 as StdField with uid="QQREXSMUMU",rtseq=214,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55770570,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=273, Top=16, InputMask=replicate('X',40)

  add object oANRATING_9_7 as StdField with uid="OBGJYQXAKO",rtseq=215,rtrep=.f.,;
    cFormVar = "w_ANRATING", cQueryName = "ANRATING",;
    bObbl = .f. , nPag = 9, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Livello di importanza per i pagamenti",;
    HelpContextID = 84083891,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=132, Top=62, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="DORATING", cZoomOnZoom="Gsdf_Ara", oKey_1_1="RACODICE", oKey_1_2="this.w_ANRATING"

  func oANRATING_9_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_9_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oANRATING_9_7.ecpDrop(oSource)
    this.Parent.oContained.link_9_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANRATING_9_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DORATING','*','RACODICE',cp_AbsName(this.parent,'oANRATING_9_7'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsdf_Ara',"Rating",'',this.parent.oContained
  endproc
  proc oANRATING_9_7.mZoomOnZoom
    local i_obj
    i_obj=Gsdf_Ara()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RACODICE=this.parent.oContained.w_ANRATING
     i_obj.ecpSave()
  endproc

  add object oANGIORIT_9_8 as StdField with uid="ACMTTAHCFQ",rtseq=216,rtrep=.f.,;
    cFormVar = "w_ANGIORIT", cQueryName = "ANGIORIT",;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni di ritardo su pagamenti",;
    HelpContextID = 62147418,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=132, Top=105, cSayPict='"999"', cGetPict='"999"'

  add object oANVOCFIN_9_9 as StdField with uid="LSGWGQGNWC",rtseq=217,rtrep=.f.,;
    cFormVar = "w_ANVOCFIN", cQueryName = "ANVOCFIN",;
    bObbl = .f. , nPag = 9, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Voce finanziaria",;
    HelpContextID = 117128020,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=132, Top=148, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="VOC_FINZ", cZoomOnZoom="GSDF_AVF", oKey_1_1="DFVOCFIN", oKey_1_2="this.w_ANVOCFIN"

  func oANVOCFIN_9_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_9_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oANVOCFIN_9_9.ecpDrop(oSource)
    this.Parent.oContained.link_9_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANVOCFIN_9_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_FINZ','*','DFVOCFIN',cp_AbsName(this.parent,'oANVOCFIN_9_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDF_AVF',"Voci finanziarie",'',this.parent.oContained
  endproc
  proc oANVOCFIN_9_9.mZoomOnZoom
    local i_obj
    i_obj=GSDF_AVF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DFVOCFIN=this.parent.oContained.w_ANVOCFIN
     i_obj.ecpSave()
  endproc

  add object oDFDESCRI_9_10 as StdField with uid="LIZWSGVYPA",rtseq=218,rtrep=.f.,;
    cFormVar = "w_DFDESCRI", cQueryName = "DFDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 185592961,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=205, Top=148, InputMask=replicate('X',60)

  add object oANESCDOF_9_11 as StdCheck with uid="JFHGGHJQQS",rtseq=219,rtrep=.f.,left=132, top=191, caption="Escludi nell'esportazione per DocFinance",;
    ToolTipText = "Escludi nell'esportazione per DocFinance",;
    HelpContextID = 184669364,;
    cFormVar="w_ANESCDOF", bObbl = .f. , nPag = 9;
   , bGlobalFont=.t.


  func oANESCDOF_9_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANESCDOF_9_11.GetRadio()
    this.Parent.oContained.w_ANESCDOF = this.RadioValue()
    return .t.
  endfunc

  func oANESCDOF_9_11.SetRadio()
    this.Parent.oContained.w_ANESCDOF=trim(this.Parent.oContained.w_ANESCDOF)
    this.value = ;
      iif(this.Parent.oContained.w_ANESCDOF=='S',1,;
      0)
  endfunc

  add object oANDESPAR_9_12 as StdCheck with uid="ECCTEFVCFP",rtseq=220,rtrep=.f.,left=132, top=231, caption="Riporta descrizione partita",;
    ToolTipText = "Riporta descrizione partita ",;
    HelpContextID = 235922600,;
    cFormVar="w_ANDESPAR", bObbl = .f. , nPag = 9;
   , bGlobalFont=.t.


  func oANDESPAR_9_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANDESPAR_9_12.GetRadio()
    this.Parent.oContained.w_ANDESPAR = this.RadioValue()
    return .t.
  endfunc

  func oANDESPAR_9_12.SetRadio()
    this.Parent.oContained.w_ANDESPAR=trim(this.Parent.oContained.w_ANDESPAR)
    this.value = ;
      iif(this.Parent.oContained.w_ANDESPAR=='S',1,;
      0)
  endfunc

  add object oRADESCRI_9_13 as StdField with uid="LKXITUNVVR",rtseq=221,rtrep=.f.,;
    cFormVar = "w_RADESCRI", cQueryName = "RADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 185594017,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=179, Top=62, InputMask=replicate('X',50)

  add object oStr_9_3 as StdString with uid="CIIUHHRNQL",Visible=.t., Left=57, Top=16,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_9_4 as StdString with uid="ILIQAINTSU",Visible=.t., Left=52, Top=63,;
    Alignment=1, Width=77, Height=18,;
    Caption="Rating:"  ;
  , bGlobalFont=.t.

  add object oStr_9_5 as StdString with uid="FWWBRFHIQB",Visible=.t., Left=-8, Top=107,;
    Alignment=1, Width=137, Height=18,;
    Caption="Giorni di ritardo:"  ;
  , bGlobalFont=.t.

  add object oStr_9_6 as StdString with uid="VPDDBFRBYL",Visible=.t., Left=4, Top=150,;
    Alignment=1, Width=125, Height=18,;
    Caption="Voce finanziaria:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsar_afrPag10 as StdContainer
  Width  = 737
  height = 560
  stdWidth  = 737
  stdheight = 560
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_10_1 as StdField with uid="ZQPBEOWSKP",rtseq=210,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 10, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 54256602,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=132, Top=16, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15)

  add object oDES1_10_2 as StdField with uid="HVIVJELXPE",rtseq=211,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 10, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55770570,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=273, Top=16, InputMask=replicate('X',40)

  add object oAN__NOTE_10_4 as StdMemo with uid="UFSSDKVFTL",rtseq=212,rtrep=.f.,;
    cFormVar = "w_AN__NOTE", cQueryName = "AN__NOTE",;
    bObbl = .f. , nPag = 10, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali annotazioni sul fornitore",;
    HelpContextID = 12307275,;
   bGlobalFont=.t.,;
    Height=488, Width=719, Left=9, Top=44

  add object oStr_10_3 as StdString with uid="EKEMUTQTHI",Visible=.t., Left=57, Top=16,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="ANTIPCON='F'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".ANTIPCON=CONTI.ANTIPCON";
  +" and "+i_cAliasName+".ANCODICE=CONTI.ANCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsar_afr','CONTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ANTIPCON=CONTI.ANTIPCON";
  +" and "+i_cAliasName2+".ANCODICE=CONTI.ANCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_afr
* --- Classe per gestire la combo ANCONCON
* --- derivata dalla classe combo da tabella

define class StdZTamTableCombo as StdTableCombo

proc Init()
  This.backcolor=i_nEBackColor
  This.FontName=i_cCboxFontName
  This.FontSize=i_nCboxFontSize
  This.Fontitalic=i_bCboxFontItalic
endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif

enddefine

*Lancia il messaggio quando cambia il flag "Scorporo a piede fattura"
proc MessFlgScorporo(pParent)
   if pParent.cFunction='Edit'
     ah_ErrorMsg("Attenzione: la modifica potrebbe avere influenza su tutti i documenti gi� caricati per il fornitore",,'')
   endif
endproc

*--- Lancia stampa
*--- Sono costretto ad utilizzare una funzione perch� non funziona la User Program
proc PrintConto(pParent, cType)
   GSAR_SCL(cType)
endproc




func  ZoomCF
param maschera
*la variabile conta serve per disattivaer il ricalcolo della data di nascita 
maschera.w_conta = maschera.w_conta + 1 
return GSAR_BFC( maschera , 'w_ANCODFIS'  , maschera.w_ANCOGNOM,maschera.w_AN__NOME,maschera.w_ANLOCNAS,maschera.w_ANPRONAS,maschera.w_ANDATNAS,maschera.w_AN_SESSO, , maschera.w_CODISO,maschera.w_CODCOM )
endfunc


* --- Fine Area Manuale
