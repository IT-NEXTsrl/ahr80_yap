* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bgb                                                        *
*              Generazione bilancio contabile                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-22                                                      *
* Last revis.: 2009-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bgb",oParentObject)
return(i_retval)

define class tgscg_bgb as StdBatch
  * --- Local variables
  w_INIZIO = .f.
  w_ENTRATO = .f.
  w_DECIMI = 0
  w_RIGHESI = space(1)
  w_GSCG_AGB = .NULL.
  w_PADRE = .NULL.
  w_GBDESCRI = 0
  w_OKMAN = .f.
  Msg = space(90)
  w_SUPERBU = space(15)
  w_BILANCIO = space(15)
  w_ESE = space(4)
  w_PROVVI = space(1)
  w_GBDATBIL = ctod("  /  /  ")
  w_CODBUN = space(3)
  w_PERIODO = 0
  w_DETCON = space(1)
  w_GBNUMBIL = 0
  w_ARROTON = space(1)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_ANNO = 0
  w_TICODICE = space(10)
  w_CODATT = space(5)
  w_VALPER = 0
  w_ERROR = .f.
  w_NUMCOL = space(5)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione automatica Bilancio - da GSBI_MRB
    * --- Riempie le righe del dettaglio
    this.w_GSCG_AGB = this.oParentObject.oParentObject
    this.w_PADRE = this.oParentObject
    this.w_DECIMI = this.w_GSCG_AGB.w_DECIMI
    this.w_RIGHESI = this.w_GSCG_AGB.w_GBRIGHES
    this.w_ARROTON = this.w_GSCG_AGB.w_GBARROTO
    this.w_BILANCIO = this.w_GSCG_AGB.w_GBSTRUCT
    this.w_GBNUMBIL = this.w_GSCG_AGB.w_GBNUMBIL
    this.w_GBDESCRI = this.w_GSCG_AGB.w_GBDESCRI
    * --- Controllo che sia stata inserita una struttura
    if EMPTY(this.w_GSCG_AGB.w_GBSTRUCT)
      ah_errormsg("Selezionare una struttura di bilancio")
      i_retcode = 'stop'
      return
    endif
    this.w_OKMAN = .f.
    this.w_INIZIO = .t.
    if this.w_GSCG_AGB.GSCG_MDM.NumRow()=0 and this.w_PADRE.cFunction="Load" AND Not this.w_GSCG_AGB.w_OKCAR
      * --- Select from gscg1bgb
      do vq_exec with 'gscg1bgb',this,'_Curs_gscg1bgb','',.f.,.t.
      if used('_Curs_gscg1bgb')
        select _Curs_gscg1bgb
        locate for 1=1
        do while not(eof())
        if !this.w_INIZIO
          * --- Append gia' eseguito la prima volta nella BlankRec
          this.w_GSCG_AGB.GSCG_MDM.Initrow()     
        else
          this.w_INIZIO = .F.
        endif
        this.w_GSCG_AGB.GSCG_MDM.w_MRCODTOT = _Curs_gscg1bgb.TICODICE
        this.w_GSCG_AGB.GSCG_MDM.w_MRCODFON = _Curs_gscg1bgb.TIMFONTE
        this.w_GSCG_AGB.GSCG_MDM.w_TIPTOT = _Curs_gscg1bgb.TITIPTOT
        this.w_GSCG_AGB.GSCG_MDM.w_DESCRI = _Curs_gscg1bgb.TIDESCRI
        this.w_GSCG_AGB.GSCG_MDM.SaveRow()     
        this.w_OKMAN = .t.
          select _Curs_gscg1bgb
          continue
        enddo
        use
      endif
      if this.w_OKMAN
        this.w_GSCG_AGB.oPgFrm.page2.setfocus
        this.w_GSCG_AGB.w_OKCAR = .T.
        i_retcode = 'stop'
        return
      endif
    else
      if this.w_GSCG_AGB.GSCG_MDM.NumRow()=0
        * --- Select from gscg1bgb
        do vq_exec with 'gscg1bgb',this,'_Curs_gscg1bgb','',.f.,.t.
        if used('_Curs_gscg1bgb')
          select _Curs_gscg1bgb
          locate for 1=1
          do while not(eof())
          if ah_YESNO("La struttura selezionata presenta dei totalizzatori manuali e la sezione relativa non presenta righe utili%1Si desidera valorizzare i totalizzatori prima di elaborare il bilancio?",,CHR(13))
            this.w_GSCG_AGB.oPgFrm.page2.setfocus
            i_retcode = 'stop'
            return
          endif
          exit
            select _Curs_gscg1bgb
            continue
          enddo
          use
        endif
      endif
    endif
    * --- Svuoto le righe
    * --- Azzera il Transitorio
    SELECT (this.w_PADRE.cTrsName)
    if this.w_PADRE.NumRow()>1
      * --- Almeno una riga c'� sempre
      * --- Avviso - La generazione svuota le righe presenti
      if NOT ah_YESNO("Le righe presenti verranno cancellate. %1Proseguire con l'elaborazione?",,CHR(13))
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Comunque elimino tutto il cursore
    SELECT (this.w_PADRE.cTrsName)
    if this.w_PADRE.cFunction="Edit"
      Delete All
    else
      zap
    endif
    * --- Disabilito i campi del'anagrafica
    this.w_GSCG_AGB.w_ABILITA=.F.
    * --- Calcolo il Bilancio e lo metto in un cursore chiamato -curbilancio-
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_INIZIO = .F.
    this.w_ENTRATO = .f.
    if Used("CurBilancio")
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Richiedo se stampare errori e produco la stampa
      if this.w_ERROR
        * --- Avviso - Si sono verificati degli errori (lacia la stampa di verifica)
        this.Msg = "Si vuole stampare la lista delle segnalazioni?"+chr(13)
        if ah_YESNO(this.Msg)
          L_GBNUMBIL =this.w_GBNUMBIL 
 L_BILANCIO =this.w_BILANCIO 
 L_GBDESCRI=this.w_GBDESCRI
          L_ESE = this.w_ESE
          SELECT * FROM CursError INTO CURSOR __TMP__ ORDER BY CPROWORD1
          CP_CHPRN("QUERY\GSCG_BBG","",this.oParentObject)
        endif
      endif
      if this.w_RIGHESI="S"
        * --- Elimino righe non significative
         
 Delete from Curbilancio where NVL(CURBILANCIO.MRIMPORT,0)=0 AND NVL(CURBILANCIO.TIPTOT," ") <>"D"
      endif
      * --- Carica il Transitorio
       
 Select curbilancio 
 GO TOP
      SCAN
      this.w_PADRE.AddRow()
      * --- Carico le variabili
      select curbilancio
      this.oParentObject.w_MRDESCRI = NVL(CURBILANCIO.MRDESCRI,"")
      this.oParentObject.w_MRCODTOT = NVL(CURBILANCIO.CODTOT,"")
      this.oParentObject.w_CPROWORD = NVL(CURBILANCIO.CPROWORD1,0)
      if this.w_ARROTON = "S"
        this.oParentObject.w_MRIMPORT = cp_ROUND(NVL(CURBILANCIO.MRIMPORT,0),this.w_DECIMI)
        this.oParentObject.w_MRIMPONI = cp_ROUND(NVL(CURBILANCIO.MRIMPONI,0),this.w_DECIMI)
        this.oParentObject.w_MRIMPOST = cp_ROUND(NVL(CURBILANCIO.MRIMPOST,0),this.w_DECIMI)
      else
        this.oParentObject.w_MRIMPORT = cp_ROUND(NVL(CURBILANCIO.MRIMPORT,0),0)
        this.oParentObject.w_MRIMPONI = cp_ROUND(NVL(CURBILANCIO.MRIMPONI,0),0)
        this.oParentObject.w_MRIMPOST = cp_ROUND(NVL(CURBILANCIO.MRIMPOST,0),0)
      endif
      * --- Carattere e Indentatura
      this.oParentObject.w_MR__FONT = NVL(CURBILANCIO.MR__FONT,"")
      this.oParentObject.w_MR__FTST = NVL(CURBILANCIO.MR__FTST,"")
      this.oParentObject.w_MR__INDE = NVL(CURBILANCIO.MR__INDE,0)
      this.oParentObject.w_MR__COLO = NVL(CURBILANCIO.MR__COLO,0)
      this.oParentObject.w_MRPERCEN = NVL(CURBILANCIO.PERCEN,0)
      this.oParentObject.w_MR__NUCO = NVL(CURBILANCIO.MR__NUCO,0)
      this.w_ENTRATO = .T.
      this.w_PADRE.TrsFromWork()
      select curbilancio
      ENDSCAN
    endif
    * --- Se non trovo neanche una riga
    if NOT this.w_ENTRATO
      ah_errormsg("Nessuna riga generata")
      i_retcode = 'stop'
      return
    endif
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.w_PADRE.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=1
    .oPgFrm.Page1.oPag.oBody.nRelRow=1
    EndWith
    * --- Rilascio il cursore
    if USED("curbilancio")
      select curbilancio
      use
    endif
    this.w_GSCG_AGB.oPgFrm.page3.setfocus
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancio la generazione - Richiamo la Procedura di Stampa del Bilancio
    * --- Creo un insieme di variabili da passare al Batch, quest'ultimo mi ritorner� un cursore
    * --- Contente i dati da mettere nelle righe.
    * --- Inizializzo le variabili da passare al Batch
    this.w_ESE = this.w_GSCG_AGB.w_GBBILESE
    * --- // tutti i movimenti
    this.w_ANNO = this.w_GSCG_AGB.w_GB__ANNO
    this.w_DATINI = this.w_GSCG_AGB.w_GBDATINI
    this.w_DATFIN = this.w_GSCG_AGB.w_GBDATFIN
    this.w_CODBUN = this.w_GSCG_AGB.w_GBCODBUN
    this.w_PERIODO = this.w_GSCG_AGB.w_GBPERIOD
    this.w_CODATT = this.w_GSCG_AGB.w_GBCODATT
    * --- Chiamo il batch
    GSCG_BGF(this,"B",this.w_BILANCIO)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Percentuali
     
 CREATE CURSOR CursError (CODICE C(15), CPROWORD1 N(6), DESCRI C(90) ) 
 SELECT * FROM CurBilancio INTO CURSOR CurPercen WHERE MRPERCEN <> 0
    * --- Ciclo per valorizzare le percentuali
    SELECT CurPercen
    GO TOP
    SCAN
    this.w_ERROR = .F.
    if CurPercen.MRIMPORT = 0
      this.Msg = AH_MsgFormat("Divisore su percentuale = 0 sulla riga %1", ALLTRIM(STR(CurPercen.CPROWORD1)))
      INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ; 
 VALUES (CurPercen.CODTOT,CurPercen.CPROWORD1,this.Msg)
      this.w_ERROR = .T.
      this.w_VALPER = 999.99
      this.w_NUMCOL = 0
    else
      Select CurBilancio 
 Locate for CurBilancio.cproword1=CurPercen.MRPERCEN
      if Found()
        this.w_VALPER = CurPercen.MRIMPORT / CurBilancio.MRIMPORT * 100
        this.w_NUMCOL = CurPercen.MRPERCEN
      endif
    endif
    this.w_VALPER = cp_ROUND(this.w_VALPER,2)
    if this.w_VALPER > 999.99
      this.Msg = AH_MsgFormat("Valore percentuale troppo grande rispetto alla riga %1", ALLTRIM(STR(CurBilancio.CPROWORD1)) )
       
 INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ; 
 VALUES (CurPercen.CODTOT,CurPercen.CPROWORD1,this.Msg)
      this.w_ERROR = .T.
      this.w_VALPER = 999.99
      this.w_NUMCOL = 0
    endif
    if this.w_NUMCOL>0
       
 UPDATE CurBilancio SET CurBilancio.PERCEN = this.w_VALPER ; 
 WHERE CurPercen.CODTOT = CurBilancio.CODTOT AND ; 
 CurPercen.CPROWORD1 = CurBilancio.CPROWORD1
       
 UPDATE CurBilancio SET CurBilancio.PERCEN = 100; 
 WHERE CurBilancio.CPROWORD1 =this.w_NUMCOL
    endif
    ENDSCAN
    if used("CurPercen")
      select CurPercen
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_gscg1bgb')
      use in _Curs_gscg1bgb
    endif
    if used('_Curs_gscg1bgb')
      use in _Curs_gscg1bgb
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
