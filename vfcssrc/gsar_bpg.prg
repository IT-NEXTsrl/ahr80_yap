* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpg                                                        *
*              Pre gestione dati (penna)                                       *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_684]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-05-09                                                      *
* Last revis.: 2017-12-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpg",oParentObject,m.pOper)
return(i_retval)

define class tgsar_bpg as StdBatch
  * --- Local variables
  pOper = space(10)
  w_UNIMIS_L = space(3)
  w_GSAR_MPG = .NULL.
  w_NRIGA = 0
  w_GSVE_MMT = .NULL.
  w_LQTAMOV = 0
  w_QTAMAT = 0
  w_LCODLOT = space(20)
  w_LCODUBI = space(40)
  w_LCODICE = space(20)
  w_LCODMAG = space(5)
  w_LUNIMIS = space(3)
  w_LPREZZO = 0
  w_LSERRIF = space(10)
  w_LROWRIF = 0
  w_NOSEL = space(1)
  w_LSERIAL = space(10)
  w_LTIPDOC = space(5)
  w_LALFDOC = space(10)
  w_LDATDOC = ctod("  /  /  ")
  w_LNUMDOC = 0
  w_LCODVAL = space(3)
  w_LDESDOC = space(35)
  w_TEMPORANEO = space(10)
  w_GPFLDOCU = space(1)
  w_GPFLINTE = space(1)
  w_GPFLACCO = space(1)
  w_MOVIMENTOODICARICO = .f.
  w_LCAUMAG = space(5)
  w_OLDPOSTIN = .f.
  w_CLAMAT = space(5)
  w_ARTMAGUBI = .f.
  w_CLAMAT2 = space(5)
  w_TROVATO = .f.
  w_OBJCTRL = .NULL.
  w_CHECK = .f.
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_TmpN = 0
  w_TROVATO = .f.
  w_LOSERIAL = space(10)
  w_LROWNUM = 0
  w_ESCI = .f.
  w_QTAMOVOR = 0
  w_SOLOCIFRE = .f.
  w_POSIZIONE = 0
  w_NUMDOC = 0
  w_DRCODUTE = 0
  w_DRSERIAL = space(15)
  w_MESS = space(10)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_OPERAT = space(1)
  w_OPERAT3 = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_QTAES1 = 0
  w_KEYULO = space(40)
  w_NUMSER = space(15)
  w_MESS = space(10)
  w_FLUSEP = space(1)
  w_NOFRAZ = space(1)
  w_MODUM2 = space(1)
  w_OLD_NOPRSC = space(1)
  w_OLD_QT1 = space(1)
  w_OLD_FM1 = space(1)
  w_OLD_FM2 = space(1)
  w_CODDISP = space(5)
  w_PROGRA = space(18)
  w_PROPAR = space(254)
  w_RIGAERR = 0
  w_LCLADOC = space(2)
  w_LFLVEAC = space(1)
  w_DCFLGROR = space(1)
  w_LCPROWNUM = 0
  w_LFLDOCU = space(1)
  w_LFLINTE = space(1)
  w_LFLACCO = space(1)
  w_DHLOTMAT = space(1)
  w_TIPO = space(5)
  w_NOMEVAR = space(50)
  w_NOMEFIELD = space(10)
  w_TIPOCUR = .f.
  w_COND = .f.
  w_PADRE = .NULL.
  w_INDEX = 0
  w_TmpS = space(200)
  w_OSOURCE = .NULL.
  w_NUMMARK = 0
  w_CACODICE = space(20)
  w_UNMISURA = space(3)
  w_QTADAEVA = 0
  w_QTAEVTMP = 0
  w_SCANPOS = 0
  w_PREZZOORI = 0
  w_SERIALOR = space(10)
  w_ROWORIG = 0
  w_ROWTEMP = 0
  w_LCODART = space(20)
  w_LCODLOT = space(20)
  w_LCODUBI = space(20)
  w_LCODMAT = space(40)
  w_LCODMAG = space(40)
  w_LROWORD = 0
  w_NODOCU = space(1)
  w_LDOQTAEV1 = 0
  w_LDOIMPEVA = 0
  w_LFLRRIF = space(1)
  w_NOMAG = .f.
  w_PARAME = space(3)
  w_OKDOC = .f.
  w_FLGROR = space(1)
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_ARGESMAT = space(1)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_CODCEN = space(15)
  w_MVCODMAT = space(40)
  w_POS = 0
  w_TQTAMOV = 0
  w_NOMEOBJ = space(100)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  DIS_HARD_idx=0
  DOC_DETT_idx=0
  KEY_ARTI_idx=0
  MATRICOL_idx=0
  MOVIMATR_idx=0
  TMP_MAST_idx=0
  RILEVAZI_idx=0
  TIPICONF_idx=0
  UNIT_LOG_idx=0
  LOTTIART_idx=0
  CMT_MAST_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch Associato alla gestione Pre gestione dati
    * --- Riferimento gestione movimentazione
    this.w_GSAR_MPG = this.oParentObject
    if Type ("this.oParentObject.w_OBJ_GEST.class")="C"
      do case
        case this.pOper="C"
          * --- Legge dal dispositivo installato se gestire la creazione automatica dei lotti e delle matricole
          if TYPE( "g_CODPEN" ) <> "U"
            if !EMPTY( g_CODPEN )
              * --- Read from DIS_HARD
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DIS_HARD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIS_HARD_idx,2],.t.,this.DIS_HARD_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DHLOTMAT"+;
                  " from "+i_cTable+" DIS_HARD where ";
                      +"DHCODICE = "+cp_ToStrODBC(g_CODPEN);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DHLOTMAT;
                  from (i_cTable) where;
                      DHCODICE = g_CODPEN;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DHLOTMAT = NVL(cp_ToDate(_read_.DHLOTMAT),cp_NullValue(_read_.DHLOTMAT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
          * --- Leggo alcuni dati dalla gestione da riempire (essenzialmente alcune informazioni 
          *     non disponibili sul tracciato necessarie per valorizzare correttamente Lotti e Ubicazioni)
          *     
          *     A T T E N Z I O N E 
          *     Questa parte di codice va duplicata per ogni gestione che si desidera collegare alla penna
          *     in analogia alla parte relativa al riempimento del temporaneo
          this.oParentObject.w_CAUMAG = SPACE(5)
          this.oParentObject.w_CAUCOL = SPACE(5)
          do case
            case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
              this.oParentObject.w_TIPCON = this.oParentObject.w_OBJ_GEST.w_MVTIPCON
              this.oParentObject.w_CODCON = this.oParentObject.w_OBJ_GEST.w_MVCODCON
              this.oParentObject.w_CODESC = this.oParentObject.w_OBJ_GEST.w_CODESC
              this.oParentObject.w_DATREG = this.oParentObject.w_OBJ_GEST.w_MVDATREG
              * --- Leggo dalla causale di magazzino di testata i valori del flag. La prima riga
              *     non � aggiornata
              * --- Read from CAM_AGAZ
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE"+;
                  " from "+i_cTable+" CAM_AGAZ where ";
                      +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_OBJ_GEST.w_MVTCAMAG);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE;
                  from (i_cTable) where;
                      CMCODICE = this.oParentObject.w_OBJ_GEST.w_MVTCAMAG;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_FLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
                this.oParentObject.w_FLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
                this.oParentObject.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
                this.oParentObject.w_FLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if !EMPTY( NVL( this.oParentObject.w_OBJ_GEST.w_CAUCOL , "" ) )
                * --- Read from CAM_AGAZ
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CMFLORDI,CMFLCASC,CMFLRISE,CMFLIMPE"+;
                    " from "+i_cTable+" CAM_AGAZ where ";
                        +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_OBJ_GEST.w_CAUCOL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CMFLORDI,CMFLCASC,CMFLRISE,CMFLIMPE;
                    from (i_cTable) where;
                        CMCODICE = this.oParentObject.w_OBJ_GEST.w_CAUCOL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_F2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
                  this.oParentObject.w_F2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
                  this.oParentObject.w_F2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
                  this.oParentObject.w_F2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              * --- VARIABILE UTILIZZATA PER LA CREAZIONE AUTOMATICA DEI LOTTI E DELLE 
              *     E DELLE MATRICOLE, QUANDO NEL MAGAZZINO PRINCIPALE O IN QUELLO COLLEGATO 
              *     AUMENTA L'ESISTENZA O IL RISERVATO
              this.w_MOVIMENTOODICARICO = NVL( this.oParentObject.w_FLCASC , " " ) = "+" OR NVL( this.oParentObject.w_FLRISE , " " ) = "+" OR NVL( this.oParentObject.w_F2CASC , " " ) = "+" OR NVL( this.oParentObject.w_F2RISE , " " ) = "+"
              if this.oParentObject.w_OBJ_GEST.w_MVFLPROV="S"
                * --- Se provvisorio sbianco i flag
                this.oParentObject.w_FLCASC = " "
                this.oParentObject.w_FLRISE = " "
                this.oParentObject.w_F2CASC = " "
                this.oParentObject.w_F2RISE = " "
                this.oParentObject.w_F2ORDI = " "
                this.oParentObject.w_F2IMPE = " "
              endif
              this.oParentObject.w_FLMGPR = this.oParentObject.w_OBJ_GEST.w_FLMGPR
              this.oParentObject.w_COMAG = this.oParentObject.w_OBJ_GEST.w_COMAG
              this.oParentObject.w_COMAT = this.oParentObject.w_OBJ_GEST.w_COMAT
              this.oParentObject.w_MAGTER = this.oParentObject.w_OBJ_GEST.w_MAGTER
              this.oParentObject.w_FLANAL = this.oParentObject.w_OBJ_GEST.w_FLANAL
              this.oParentObject.w_FLGCOM = this.oParentObject.w_OBJ_GEST.w_FLGCOM
              this.oParentObject.w_FLVEAC = this.oParentObject.w_OBJ_GEST.w_FLVEAC
              this.oParentObject.w_ESCL1 = this.oParentObject.w_OBJ_GEST.w_ESCL1
              this.oParentObject.w_ESCL2 = this.oParentObject.w_OBJ_GEST.w_ESCL2
              this.oParentObject.w_ESCL3 = this.oParentObject.w_OBJ_GEST.w_ESCL3
              this.oParentObject.w_ESCL4 = this.oParentObject.w_OBJ_GEST.w_ESCL4
              this.oParentObject.w_ESCL5 = this.oParentObject.w_OBJ_GEST.w_ESCL5
              this.oParentObject.w_TIPORN = this.oParentObject.w_OBJ_GEST.w_MVTIPORN
              this.oParentObject.w_CODORN = this.oParentObject.w_OBJ_GEST.w_MVCODORN
              this.oParentObject.w_FLSCOR = this.oParentObject.w_OBJ_GEST.w_MVFLSCOR
              this.oParentObject.w_TIPDOC = this.oParentObject.w_OBJ_GEST.w_MVTIPDOC
              this.oParentObject.w_FLNSRI = this.oParentObject.w_OBJ_GEST.w_FLNSRI
              this.oParentObject.w_FLVSRI = this.oParentObject.w_OBJ_GEST.w_FLVSRI
              this.oParentObject.w_FLRIDE = this.oParentObject.w_OBJ_GEST.w_FLRIDE
              this.oParentObject.w_TPNSRI = this.oParentObject.w_OBJ_GEST.w_TPNSRI
              this.oParentObject.w_TPVSRI = this.oParentObject.w_OBJ_GEST.w_TPVSRI
              this.oParentObject.w_TPRDES = this.oParentObject.w_OBJ_GEST.w_TPRDES
              this.oParentObject.w_CAUMAG = this.oParentObject.w_OBJ_GEST.w_MVTCAMAG
              this.oParentObject.w_CAUCOL = this.oParentObject.w_OBJ_GEST.w_CAUCOL
              this.oParentObject.w_FLPACK = this.oParentObject.w_OBJ_GEST.w_FLPACK
              this.oParentObject.w_FLMTPR = this.oParentObject.w_OBJ_GEST.w_FLMTPR
              this.oParentObject.w_DACAM_01 = this.oParentObject.w_OBJ_GEST.w_DACAM_01
              this.oParentObject.w_DACAM_02 = this.oParentObject.w_OBJ_GEST.w_DACAM_02
              this.oParentObject.w_DACAM_03 = this.oParentObject.w_OBJ_GEST.w_DACAM_03
              this.oParentObject.w_DACAM_04 = this.oParentObject.w_OBJ_GEST.w_DACAM_04
              this.oParentObject.w_DACAM_05 = this.oParentObject.w_OBJ_GEST.w_DACAM_05
              this.oParentObject.w_DACAM_06 = this.oParentObject.w_OBJ_GEST.w_DACAM_06
              this.oParentObject.w_TDFLRA01 = this.oParentObject.w_OBJ_GEST.w_TDFLRA01
              this.oParentObject.w_TDFLRA02 = this.oParentObject.w_OBJ_GEST.w_TDFLRA02
              this.oParentObject.w_TDFLRA03 = this.oParentObject.w_OBJ_GEST.w_TDFLRA03
              this.oParentObject.w_TDFLRA04 = this.oParentObject.w_OBJ_GEST.w_TDFLRA04
              this.oParentObject.w_TDFLRA05 = this.oParentObject.w_OBJ_GEST.w_TDFLRA05
              this.oParentObject.w_TDFLRA06 = this.oParentObject.w_OBJ_GEST.w_TDFLRA06
              this.oParentObject.w_TDFLIA01 = this.oParentObject.w_OBJ_GEST.w_TDFLIA01
              this.oParentObject.w_TDFLIA02 = this.oParentObject.w_OBJ_GEST.w_TDFLIA02
              this.oParentObject.w_TDFLIA03 = this.oParentObject.w_OBJ_GEST.w_TDFLIA03
              this.oParentObject.w_TDFLIA04 = this.oParentObject.w_OBJ_GEST.w_TDFLIA04
              this.oParentObject.w_TDFLIA05 = this.oParentObject.w_OBJ_GEST.w_TDFLIA05
              this.oParentObject.w_TDFLIA06 = this.oParentObject.w_OBJ_GEST.w_TDFLIA06
              this.oParentObject.w_MVAGG_01 = this.oParentObject.w_OBJ_GEST.w_MVAGG_01
              this.oParentObject.w_MVAGG_02 = this.oParentObject.w_OBJ_GEST.w_MVAGG_02
              this.oParentObject.w_MVAGG_03 = this.oParentObject.w_OBJ_GEST.w_MVAGG_03
              this.oParentObject.w_MVAGG_04 = this.oParentObject.w_OBJ_GEST.w_MVAGG_04
              this.oParentObject.w_MVAGG_05 = this.oParentObject.w_OBJ_GEST.w_MVAGG_05
              this.oParentObject.w_MVAGG_06 = this.oParentObject.w_OBJ_GEST.w_MVAGG_06
            case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
              * --- Gestione Lotti Ubicazioni - Mov. POS da fare
              this.oParentObject.w_CODCON = this.oParentObject.w_OBJ_GEST.w_MDCODCLI
              this.oParentObject.w_DATREG = this.oParentObject.w_OBJ_GEST.w_MDDATREG
              this.oParentObject.w_FLCASC = "-"
              this.oParentObject.w_FLRISE = " "
              this.oParentObject.w_F2CASC = " "
              this.oParentObject.w_F2RISE = " "
              this.oParentObject.w_FLVEAC = " "
              this.w_MOVIMENTOODICARICO = NVL( this.oParentObject.w_FLCASC , " " ) = "+" OR NVL( this.oParentObject.w_FLRISE , " " ) = "+" OR NVL( this.oParentObject.w_F2CASC , " " ) = "+" OR NVL( this.oParentObject.w_F2RISE , " " ) = "+"
              this.oParentObject.w_FLPACK = " "
              this.oParentObject.w_CODESC = this.oParentObject.w_OBJ_GEST.w_CODESC
            case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
              this.oParentObject.w_CODCON = this.oParentObject.w_OBJ_GEST.w_CODCON
              this.oParentObject.w_DATREG = this.oParentObject.w_OBJ_GEST.w_MMDATREG
              * --- Per determinare il tipo di flag aggiornamento magazzino leggo la causale di testata
              this.w_LCAUMAG = this.oParentObject.w_OBJ_GEST.w_MMCAUMAG
              * --- Read from CAM_AGAZ
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CMFLCASC,CMFLRISE"+;
                  " from "+i_cTable+" CAM_AGAZ where ";
                      +"CMCODICE = "+cp_ToStrODBC(this.w_LCAUMAG);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CMFLCASC,CMFLRISE;
                  from (i_cTable) where;
                      CMCODICE = this.w_LCAUMAG;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_FLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
                this.oParentObject.w_FLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if !EMPTY( NVL( this.oParentObject.w_OBJ_GEST.w_CAUCOL , "" ) )
                * --- Read from CAM_AGAZ
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CMFLCASC,CMFLRISE"+;
                    " from "+i_cTable+" CAM_AGAZ where ";
                        +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_OBJ_GEST.w_CAUCOL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CMFLCASC,CMFLRISE;
                    from (i_cTable) where;
                        CMCODICE = this.oParentObject.w_OBJ_GEST.w_CAUCOL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_F2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
                  this.oParentObject.w_F2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              this.oParentObject.w_FLMGPR = " "
              this.oParentObject.w_COMAG = Space(5)
              this.oParentObject.w_COMAT = Space(5)
              this.oParentObject.w_MAGTER = Space(5)
              this.oParentObject.w_FLANAL = " "
              this.oParentObject.w_FLGCOM = " "
              this.oParentObject.w_FLVEAC = " "
              this.oParentObject.w_CAUMAG = this.oParentObject.w_OBJ_GEST.w_MMTCAMAG
              this.oParentObject.w_CAUCOL = this.oParentObject.w_OBJ_GEST.w_CAUCOL
              this.w_MOVIMENTOODICARICO = NVL( this.oParentObject.w_FLCASC , " " ) = "+" OR NVL( this.oParentObject.w_FLRISE , " " ) = "+" OR NVL( this.oParentObject.w_F2CASC , " " ) = "+" OR NVL( this.oParentObject.w_F2RISE , " " ) = "+"
              this.oParentObject.w_FLPACK = " "
            case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_kcr"
              this.oParentObject.w_CODCON = ""
              * --- Check obsolescenza / validit� matricole alla data rilevazione...
              this.oParentObject.w_DATREG = this.oParentObject.w_OBJ_GEST.w_DATARIL
              * --- Il dato rilevato � assimilabile ad un carico...
              this.oParentObject.w_FLCASC = "+"
              this.oParentObject.w_FLRISE = " "
              this.oParentObject.w_F2CASC = " "
              this.oParentObject.w_F2RISE = " "
              this.oParentObject.w_FLMGPR = " "
              this.oParentObject.w_COMAG = Space(5)
              this.oParentObject.w_COMAT = Space(5)
              this.oParentObject.w_MAGTER = Space(5)
              this.oParentObject.w_FLANAL = " "
              this.oParentObject.w_FLGCOM = " "
              this.oParentObject.w_FLVEAC = " "
              this.w_MOVIMENTOODICARICO = NVL( this.oParentObject.w_FLCASC , " " ) = "+" OR NVL( this.oParentObject.w_FLRISE , " " ) = "+" OR NVL( this.oParentObject.w_F2CASC , " " ) = "+" OR NVL( this.oParentObject.w_F2RISE , " " ) = "+"
              this.oParentObject.w_FLPACK = " "
          endcase
          * --- Disabilit i msg di errore utente durante il riempimento del transitorio
          this.oParentObject.w_NOMSGCHECK = "S"
          this.w_GSAR_MPG.oPgFrm.ActivePage=3
          AddMsgNL("Elaborazione iniziata alle: %1", this, Time())
          * --- Svuoto il transitorio
           
 Select ( this.w_GSAR_MPG.cTrsName ) 
 Zap
          * --- Mi posiziono sul cursore proveniente dalla penna
           
 Select ( this.oParentObject.w_CURSORE ) 
 Go Top
          * --- Se cursore vuoto creo una riga bianca (almeno una init Row va fatta)
          if Eof( this.oParentObject.w_CURSORE )
            this.w_GSAR_MPG.InitRow()     
            this.w_GSAR_MPG.MarkPos()     
          else
            * --- A pag3 prelevo i dati dal temporaneo derivato dalla penna
            this.w_TIPOCUR = .T.
            * --- Disabilito i post-In (li riabilito al termine del caricamento)
            this.w_OLDPOSTIN = i_ServerConn[1,7]
            i_ServerConn[1,7]=.F.
            do while Not Eof( this.oParentObject.w_CURSORE )
              this.w_GSAR_MPG.InitRow()     
              if Recno()=1
                * --- Al primo giro marco la posizione
                this.w_GSAR_MPG.MarkPos()     
              endif
              * --- Aggiorno le variabili o_xxx per calcoli in mCalc
              this.w_GSAR_MPG.SaveDependsOn()     
               
 Select ( this.oParentObject.w_CURSORE )
              this.oParentObject.w_STATOROW = "S"
              * --- Se gestione matricole attive ed utilizzo l'univocit� per matricole allora
              *     compila la matricola e salto articolo, qta ed unita di misura 
              *     (qta=1. unit� di misura forzatamente principale)
              this.oParentObject.w_CODMAT = Nvl( MATRICOLA , Space(40) )
              * --- PER LA GESTIONE MATRICOLE E LOTTI OCCORRE IL CODICE ARTICOLO
              this.oParentObject.w_CODICE = CODICE
              * --- Read from KEY_ARTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CACODART"+;
                  " from "+i_cTable+" KEY_ARTI where ";
                      +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CACODART;
                  from (i_cTable) where;
                      CACODICE = this.oParentObject.w_CODICE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARGESMAT,ARCLAMAT"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARGESMAT,ARCLAMAT;
                  from (i_cTable) where;
                      ARCODART = this.oParentObject.w_CODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_GESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
                this.w_CLAMAT = NVL(cp_ToDate(_read_.ARCLAMAT),cp_NullValue(_read_.ARCLAMAT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_ARTMAGUBI = .F.
              this.w_CLAMAT = NVL( this.w_CLAMAT , "     " )
              if this.w_DHLOTMAT $ "ME" AND this.w_MOVIMENTOODICARICO AND g_MATR = "S" AND this.oParentObject.w_TIPOOBJ_GEST<>"P"
                * --- w_DHLOTMAT letto dal dispositivo harware, per creare lotto/matricola
                *     (M matricola, E entrambi )
                if NOT EMPTY( this.oParentObject.w_CODMAT ) AND NOT EMPTY( Nvl( CODICE , Space(20) ) ) and Not Empty(this.oParentObject.w_CODART)
                  * --- Per inserire un codice matricola occorre anche il codice articolo
                  * --- Try
                  local bErr_05062140
                  bErr_05062140=bTrsErr
                  this.Try_05062140()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- rollback
                    bTrsErr=.t.
                    cp_EndTrs(.t.)
                  endif
                  bTrsErr=bTrsErr or bErr_05062140
                  * --- End
                endif
              endif
              if g_UNIMAT="M" And Not Empty( this.oParentObject.w_CODMAT ) AND g_MATR="S" AND Lower ( this.oParentObject.w_OBJ_GEST.class ) <> "tgsps_mvd"
                * --- In questo caso occorre anche inserire preventivamente articolo, magazzino e ubicazione
                this.w_ARTMAGUBI = .T.
                this.Page_8()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
               
 Select ( this.oParentObject.w_CURSORE )
              if g_UNIMAT="M" And Not Empty( this.oParentObject.w_CODMAT ) AND g_MATR="S" AND Lower ( this.oParentObject.w_OBJ_GEST.class ) <> "tgsps_mvd"
                this.oParentObject.w_GPCODMAT = this.oParentObject.w_CODMAT
                this.w_GSAR_MPG.w_NOMSG = .F.
                this.w_GSAR_MPG.Notifyevent("w_GPCODMAT Changed")     
                * --- Se articolo vuoto significa che ha fallito il link...
                if Empty( this.oParentObject.w_GPCODICE )
                  AddMsgNL("Riga %1 matricola non valida %2", this, Alltrim(Str( this.oParentObject.w_CPROWORD )), Alltrim(CODICE))
                  this.oParentObject.w_STATOROW = "M"
                else
                  * --- La riga per ora � Ok...
                  this.oParentObject.w_STATOROW = "S"
                endif
              else
                this.oParentObject.w_CODICE = CODICE
                if Empty( this.oParentObject.w_CODICE )
                  AddMsgNL("Riga %1 articolo non specificato %2", this, Alltrim(Str( this.oParentObject.w_CPROWORD )), Alltrim(CODICE))
                  this.oParentObject.w_STATOROW = "A"
                else
                  this.oParentObject.w_GPCODICE = this.oParentObject.w_CODICE
                  this.w_NOMEOBJ = "w_GPCODICE" 
                  this.Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  * --- Se articolo vuoto significa che ha fallito il link...
                  if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                    AddMsgNL("Riga %1 articolo %2 non valido", this, Alltrim(Str( this.oParentObject.w_CPROWORD )), Alltrim(CODICE))
                    this.oParentObject.w_STATOROW = "A"
                  else
                    * --- La riga per ora � Ok...
                    this.oParentObject.w_STATOROW = "S"
                  endif
                endif
                 
 Select ( this.oParentObject.w_CURSORE )
                * --- Se l'unit� di misura non � specificata utilizzo l'unit� di misura
                *     principale dell'articolo o se presente l'unit� associata al codice
                *     di ricerca
                this.w_UNIMIS_L = Nvl( UNIMIS , space(3) )
                if Empty( this.w_UNIMIS_L ) And this.oParentObject.w_STATOROW="S"
                  * --- Leggo l'articolo e unit� di misura alternativa
                  * --- Read from KEY_ARTI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "CACODART,CAUNIMIS,CAMOLTIP"+;
                      " from "+i_cTable+" KEY_ARTI where ";
                          +"CACODICE = "+cp_ToStrODBC(this.w_GSAR_MPG.w_GPCODICE);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      CACODART,CAUNIMIS,CAMOLTIP;
                      from (i_cTable) where;
                          CACODICE = this.w_GSAR_MPG.w_GPCODICE;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.oParentObject.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
                    this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
                    this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if Not Empty ( this.oParentObject.w_CODART)
                    * --- Read from ART_ICOL
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "ARUNMIS1,ARGESMAT"+;
                        " from "+i_cTable+" ART_ICOL where ";
                            +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        ARUNMIS1,ARGESMAT;
                        from (i_cTable) where;
                            ARCODART = this.oParentObject.w_CODART;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_UNIMIS_L = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                      this.oParentObject.w_GESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                  endif
                   
 Select ( this.oParentObject.w_CURSORE )
                else
                  * --- Azzerro unit� di misura legata al codice di ricerca per evitare
                  *     che sia utilizzata nel giro successivo se unit� di miusra non specificata
                  *     nel transitorio
                  this.w_UNMIS3 = space(3)
                endif
                this.oParentObject.w_GPUNIMIS = IIF(NOT EMPTY(this.w_UNMIS3) AND this.w_MOLTI3<>0, this.w_UNMIS3, this.w_UNIMIS_L)
                this.w_NOMEOBJ = "w_GPUNIMIS"
                this.Page_7()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                  AddMsgNL("Riga %1 unit� di misura errata", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                  this.oParentObject.w_STATOROW = "M"
                endif
                 
 Select ( this.oParentObject.w_CURSORE )
                this.oParentObject.w_UNIMIS = Nvl( UNIMIS , "" )
                if NOT EMPTY(NVL(MATRICOLA,SPACE(40))) 
                  * --- Se tracciato con matricole forzo la qt� ad uno, nel caso di dati rilevati svolgo
                  *     il minimo tra la qt� ed uno (se qta>1 prendo uno altrimenti prendo Qta (quindi 0))
                  this.w_TmpN = IIF(Lower ( this.oParentObject.w_OBJ_GEST.class ) <> "tgsma_kcr" ,1,Min(1,NVL( QTAMOV , 0 )) )
                else
                  this.w_TmpN = NVL( QTAMOV , 0 )
                endif
                * --- Codici a peso/prezzo variabile, in questo caso la quantit� � gi� valorizzata
                *     dalla mAfter del campo codice (pag. 7)
                this.oParentObject.w_GPQTAMOV = IIF(this.oParentObject.w_GPQTAMOV<>0, this.oParentObject.w_GPQTAMOV, this.w_TmpN)
                if this.oParentObject.w_GPQTAMOV<>0
                  this.w_OBJCTRL = this.w_GSAR_MPG.GetBodyCtrl( "w_GPQTAMOV" )
                  * --- Valorizza la Quantit� (verifico eventuali qt� frazionarie) se quantit�
                  *     frazionarie e l'unit� di misura non le consente arrotondo qt�
                  if Not this.w_OBJCTRL.Check()
                    if this.w_TmpN<>0
                      * --- Se fallisce il check qt� non frazionabile l'arrotondo..
                      AddMsgNL("Riga %1 quantit� arrotondata (unit� di misura non frazionabile)", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                      this.oParentObject.w_GPQTAMOV = cp_Round( this.w_TmpN , 0 )
                    endif
                  endif
                endif
                if this.oParentObject.w_GPQTAMOV=0 And this.oParentObject.w_STATOROW="S" And Lower ( this.oParentObject.w_OBJ_GEST.class ) <> "tgsma_kcr"
                  * --- Tranne che per le rettifiche inventariali, la quantita a zero non � accettata
                  AddMsgNL("Riga %1 quantit� a zero o non frazionabile", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                  this.oParentObject.w_STATOROW = "Q"
                endif
                if this.oParentObject.w_GPQTAMOV>0
                  this.w_GSAR_MPG.mCalc(.t.)     
                  this.w_GSAR_MPG.SaveDependsOn()     
                endif
                 
 Select ( this.oParentObject.w_CURSORE )
                this.oParentObject.w_QTAMOV = IIF(NOT EMPTY(NVL(MATRICOLA,SPACE(40))),1,NVL( QTAMOV , 0 ))
              endif
               
 Select ( this.oParentObject.w_CURSORE )
              * --- Codici a prezzo variabile, in questo caso il prezzo � gi� valorizzata
              *     dalla mAfter del campo codice (pag. 7)
              this.oParentObject.w_GPPREZZO = IIF(this.oParentObject.w_GPPREZZO<>0, this.oParentObject.w_GPPREZZO, NVL( PREZZO , 0 ))
              this.oParentObject.w_PREZZO = this.oParentObject.w_GPPREZZO
              * --- Gestione Magazzino
              if ! this.w_ARTMAGUBI
                * --- w_ARTMAGUBI=.T.  INDICA CHE l'inserimento � gia stato eseguito a pag 8
                 
 Select ( this.oParentObject.w_CURSORE )
                this.oParentObject.w_CODMAG = Nvl( CODMAG ,Space(5) )
                * --- Se magazzino valorizzato tento di inserirlo..
                if Not Empty( this.oParentObject.w_CODMAG )
                  this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPCODMAG" )
                  if this.w_OBJCTRL.mCond()
                    this.oParentObject.w_GPCODMAG = this.oParentObject.w_CODMAG
                    this.w_NOMEOBJ = "w_GPCODMAG"
                    this.Page_7()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                      AddMsgNL("Riga %1 magazzino non valido", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                      this.oParentObject.w_STATOROW = "D"
                    endif
                  endif
                endif
              endif
               
 Select ( this.oParentObject.w_CURSORE )
              this.oParentObject.w_CODLOT = Nvl( LOTTO ,Space(20) )
              if this.w_DHLOTMAT $ "LE" AND this.w_MOVIMENTOODICARICO AND g_PERLOT="S" AND this.oParentObject.w_TIPOOBJ_GEST<>"P"
                * --- w_DHLOTMAT letto dal dispositivo harware, per creare lotto/matricola
                *     (L lotto, E entrambi )
                if NOT EMPTY( this.oParentObject.w_CODLOT ) AND NOT EMPTY( this.oParentObject.w_CODART )
                  * --- Controlla se il codice lotto esiste, altrimenti lo inserisce
                  * --- Try
                  local bErr_05082E08
                  bErr_05082E08=bTrsErr
                  this.Try_05082E08()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- rollback
                    bTrsErr=.t.
                    cp_EndTrs(.t.)
                  endif
                  bTrsErr=bTrsErr or bErr_05082E08
                  * --- End
                endif
              endif
              this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPCODLOT" )
              if this.w_OBJCTRL.mCond()
                this.oParentObject.w_GPCODLOT = this.oParentObject.w_CODLOT
                this.w_NOMEOBJ = "w_GPCODLOT"
                this.Page_7()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                  AddMsgNL("Riga %1 lotto non valido", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                  this.oParentObject.w_STATOROW = "L"
                endif
              endif
               
 Select ( this.oParentObject.w_CURSORE )
              * --- Gestione Matricole
              if g_UNIMAT<>"M" And this.oParentObject.w_GESMAT="S" AND g_MATR="S" AND Lower ( this.oParentObject.w_OBJ_GEST.class ) <> "tgsps_mvd" AND !EMPTY( Nvl( MATRICOLA ,Space(40) ) )
                 
 Select ( this.oParentObject.w_CURSORE )
                this.oParentObject.w_MTKEYSAL = this.oParentObject.w_CODART
                this.oParentObject.w_GPCODMAT = Nvl( MATRICOLA ,Space(40) ) 
                this.oParentObject.w_CODMAT = Nvl( MATRICOLA ,Space(40) ) 
                this.w_GSAR_MPG.w_NOMSG = .F.
                this.w_GSAR_MPG.Notifyevent("w_GPCODMAT Changed")     
              endif
               
 Select ( this.oParentObject.w_CURSORE )
              * --- Gestione Ubicazione
              * --- w_ARTMAGUBI=.T.  INDICA CHE l'inserimento � gia stato eseguito a pag 8
               
 Select ( this.oParentObject.w_CURSORE )
              this.oParentObject.w_CODUBI = Nvl( UBICAZIONE ,Space(20) )
              this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPCODUBI" )
              if this.w_OBJCTRL.mCond()
                this.oParentObject.w_GPCODUBI = this.oParentObject.w_CODUBI
                this.w_NOMEOBJ = "w_GPCODUBI"
                this.Page_7()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                  AddMsgNL("Riga %1 ubicazione non valida", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                  this.oParentObject.w_STATOROW = "U"
                endif
              endif
              * --- Gestione Documento di Origine
               
 Select ( this.oParentObject.w_CURSORE )
              this.w_LSERRIF = NVL(SERRIF,SPACE(10))
              this.w_QTADAEVA = NVL(QTAMOV, 0)
              this.w_ESCI = .T.
              if NOT EMPTY(this.w_LSERRIF) AND Lower ( this.oParentObject.w_OBJ_GEST.class ) <> "tgsps_mvd"
                * --- Select from gsar_bpg
                do vq_exec with 'gsar_bpg',this,'_Curs_gsar_bpg','',.f.,.t.
                if used('_Curs_gsar_bpg')
                  select _Curs_gsar_bpg
                  locate for 1=1
                  do while not(eof())
                  if this.w_ESCI
                    this.oParentObject.w_EVTIPDOC = NVL(MVTIPDOC,SPACE(5))
                    this.oParentObject.w_EVNUMDOC = NVL(MVNUMDOC,0)
                    this.oParentObject.w_EVALFDOC = NVL(MVALFDOC, Space(10))
                    this.oParentObject.w_EVDATDOC = CP_TODATE(MVDATDOC)
                    this.oParentObject.w_EVROWORD = Nvl(CPROWORD,0)
                    this.oParentObject.w_DOIMPEVA = Nvl(MVIMPEVA,0)
                    this.oParentObject.w_DOQTAEV1 = Nvl(MVQTAEV1,0)
                    this.oParentObject.w_DOQTAEVA = NVL(MVQTAEVA,0)
                    this.w_QTAMOVOR = NVL(MVQTAMOV,0)
                    this.w_LROWNUM = CPROWNUM
                     
 Select ( this.w_GSAR_MPG.cTrsName )
                    this.w_ROWTEMP = RECNO()
                     Locate For Nvl(t_Gpserrif,space(10)) = this.w_Lserrif and Nvl(t_Gprowrif,0) = this.w_Lrownum
                    if Not Found()
                      * --- Se la riga non � gi� stata utilizzata posso evaderla
                      this.w_ESCI = .F.
                       
 Select ( this.w_GSAR_MPG.cTrsName )
                      Go this.w_ROWTEMP
                      this.w_LQTAMOV = nvl(t_GPQTAMOV,0)
                      this.oParentObject.w_GPSERRIF = this.w_LSERRIF
                      this.oParentObject.w_GPROWRIF = this.w_LROWNUM
                      if this.w_QTADAEVA<this.w_QTAMOVOR-this.oParentObject.w_DOQTAEVA
                        this.oParentObject.w_GPEVASIO = " "
                      else
                        this.oParentObject.w_GPEVASIO = "S"
                      endif
                    else
                       
 Select ( this.w_GSAR_MPG.cTrsName )
                      Go this.w_ROWTEMP
                    endif
                  endif
                    select _Curs_gsar_bpg
                    continue
                  enddo
                  use
                endif
              endif
              * --- Gestione magazzino collegato
               
 Select ( this.oParentObject.w_CURSORE )
              this.oParentObject.w_CODMA2 = Nvl( CODMA2 ,Space(5) )
              * --- Se magazzino valorizzato tento di inserirlo..
              if Not Empty( this.oParentObject.w_CODMA2 )
                this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPCODMA2" )
                if this.w_OBJCTRL.mCond()
                  this.oParentObject.w_GPCODMA2 = this.oParentObject.w_CODMA2
                  this.w_NOMEOBJ = "w_GPCODMA2"
                  this.Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                    AddMsgNL("Riga %1 magazzino collegato non valido", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                    this.oParentObject.w_STATOROW = "F"
                  endif
                endif
              endif
              * --- Gestione Ubicazione
               
 Select ( this.oParentObject.w_CURSORE )
              this.oParentObject.w_CODUB2 = Nvl( UBICAZION2 ,Space(20) )
              this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPCODUB2" )
              if this.w_OBJCTRL.mCond()
                this.oParentObject.w_GPCODUB2 = this.oParentObject.w_CODUB2
                this.w_NOMEOBJ = "w_GPCODUB2"
                this.Page_7()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                  AddMsgNL("Riga %1 ubicazione collegata non valida", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                  this.oParentObject.w_STATOROW = "B"
                endif
              endif
              * --- Gestione centro di costo
               
 Select ( this.oParentObject.w_CURSORE )
              this.oParentObject.w_CODCEN_OR = Nvl( CENTRO ,Space(5) )
              * --- Se centro di costo/ricavo valorizzato tento di inserirlo..
              if Not Empty( this.oParentObject.w_CODCEN_OR )
                this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPCODCEN" )
                if this.w_OBJCTRL.mCond()
                  this.oParentObject.w_GPCODCEN = this.oParentObject.w_CODCEN_OR
                  this.w_NOMEOBJ = "w_GPCODCEN"
                  this.Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                    AddMsgNL("Riga %1 centro di costo/ricavo non valido", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                    this.oParentObject.w_STATOROW = "C"
                  endif
                endif
              endif
              * --- Gestione commessa
               
 Select ( this.oParentObject.w_CURSORE )
              this.oParentObject.w_CODCOM_OR = Nvl( COMMESSA ,Space(5) )
              * --- Se commessa valorizzata tento di inserirla
              if Not Empty( this.oParentObject.w_CODCOM_OR )
                this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPCODCOM" )
                if this.w_OBJCTRL.mCond()
                  this.oParentObject.w_GPCODCOM = this.oParentObject.w_CODCOM_OR
                  this.w_NOMEOBJ = "w_GPCODCOM"
                  this.Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                    AddMsgNL("Riga %1 commessa non valida", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                    this.oParentObject.w_STATOROW = "P"
                  endif
                endif
              endif
              * --- Gestione attivit�
               
 Select ( this.oParentObject.w_CURSORE )
              this.oParentObject.w_CODATT_OR = Nvl( ATTIVITA ,0 )
              * --- Se attivit� valorizzata tento di inserirla
              if Not Empty( this.oParentObject.w_CODATT_OR )
                this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPCODATT" )
                if this.w_OBJCTRL.mCond()
                  this.oParentObject.w_GPCODATT = this.oParentObject.w_CODATT_OR
                  this.w_NOMEOBJ = "w_GPCODATT"
                  this.Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                    AddMsgNL("Riga %1 attivit� non valida", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                    this.oParentObject.w_STATOROW = "G"
                  endif
                endif
              endif
              * --- Gestione quantit� nell'unit� di misura principale
               
 Select ( this.oParentObject.w_CURSORE )
              this.oParentObject.w_QTAUM1 = Nvl( QTAUM1 ,Space(5) )
              * --- Se quantit� nell'unit� di misura principale valorizzata tento di inserirla
              if NOT EMPTY(NVL(MATRICOLA,SPACE(40))) 
                this.oParentObject.w_QTAUM1 = 1
                this.oParentObject.w_GPQTAUM1 = 1
              else
                if Not Empty( this.oParentObject.w_QTAUM1 )
                  this.oParentObject.w_GPQTAUM1 = this.oParentObject.w_QTAUM1
                  this.w_NOMEOBJ = "w_GPQTAUM1"
                  this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPQTAUM1" )
                  if this.w_OBJCTRL.mCOND()
                    if NOT this.w_OBJCTRL.CHECK()
                      if this.oParentObject.w_GPQTAUM1<>0
                        * --- Se fallisce il check qt� non frazionabile l'arrotondo..
                        AddMsgNL("Riga %1 quantit� arrotondata (unit� di misura principale non frazionabile)", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                        this.oParentObject.w_GPQTAUM1 = cp_Round( this.oParentObject.w_GPQTAUM1 , 0 )
                      endif
                    endif
                  endif
                endif
              endif
              * --- Gestione unit� logistica
               
 Select ( this.oParentObject.w_CURSORE )
              this.oParentObject.w_UNILOG = Nvl( UNILOG ,0 )
              * --- Il codice unit� logistica � compolsto di sole cifre
              this.w_SOLOCIFRE = .T.
              this.w_POSIZIONE = 1
              do while this.w_POSIZIONE <= 18
                if NOT ISDIGIT( SUBSTR( this.oParentObject.w_UNILOG , this.w_POSIZIONE , 1 ) )
                  this.w_SOLOCIFRE = .F.
                endif
                this.w_POSIZIONE = this.w_POSIZIONE + 1
              enddo
              * --- Se unit� logistica valorizzata con codice di 18 caratteri, tento di inserirla
              if Not Empty( this.oParentObject.w_UNILOG ) AND LEN( ALLTRIM( this.oParentObject.w_UNILOG ) ) = 18 AND this.w_SOLOCIFRE
                * --- LANCIA IL CALCOLO DELLE CONFEZIONI PER COLLO
                DO GSAR_BCO WITH THIS.OPARENTOBJECT , "R"
                this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPUNILOG" )
                if this.w_OBJCTRL.mCond()
                  * --- CERCA DI INSERIRE L'UNIT� LOGISTICA SE NON E' CARICATA
                  * --- Try
                  local bErr_050B0FA0
                  bErr_050B0FA0=bTrsErr
                  this.Try_050B0FA0()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_050B0FA0
                  * --- End
                  this.oParentObject.w_GPUNILOG = this.oParentObject.w_UNILOG
                  this.w_NOMEOBJ = "w_GPUNILOG"
                  this.Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
                    AddMsgNL("Riga %1 unit� logistica non valida", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
                    this.oParentObject.w_STATOROW = "N"
                  endif
                endif
              endif
              this.w_GSAR_MPG.State_Row()     
              this.w_GSAR_MPG.TrsFromWork()     
               
 Select ( this.oParentObject.w_CURSORE )
              if Not Eof( this.oParentObject.w_CURSORE )
                Skip
              endif
            enddo
            * --- Rimetto al valore originario l'abilitazione o meno dei post-in
            i_ServerConn[1,7]= this.w_OLDPOSTIN 
          endif
          this.oParentObject.w_NOMSGCHECK = "N"
          * --- Riposiziono il transitorio alla prima riga (creo tutto da una Zap)
          this.w_GSAR_MPG.RePos()     
          AddMsgNL("Elaborazione terminata alle: %1", this, Time())
          * --- Mi rimetto a pagina 1
          this.oParentobject.oPgFrm.ActivePage=1
          * --- Elimino il cursore creato dal dispositivo
           
 Select ( this.oParentObject.w_CURSORE ) 
 Use
        case this.pOper="S"
          if Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_kcr"
            * --- Ultimo Documento generato (associato all'utente w_CODUTE)
            * --- Select from RILEVAZI
            i_nConn=i_TableProp[this.RILEVAZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MAX(DRNUMDOC) AS NUMDOC  from "+i_cTable+" RILEVAZI ";
                  +" where DRADDRIL="+cp_ToStrODBC(this.oParentObject.w_OBJ_GEST.w_CODUTE)+"";
                   ,"_Curs_RILEVAZI")
            else
              select MAX(DRNUMDOC) AS NUMDOC from (i_cTable);
               where DRADDRIL=this.oParentObject.w_OBJ_GEST.w_CODUTE;
                into cursor _Curs_RILEVAZI
            endif
            if used('_Curs_RILEVAZI')
              select _Curs_RILEVAZI
              locate for 1=1
              do while not(eof())
              this.w_NUMDOC = NVL(_Curs_RILEVAZI.NUMDOC,0)
                select _Curs_RILEVAZI
                continue
              enddo
              use
            endif
            * --- Try
            local bErr_0503E650
            bErr_0503E650=bTrsErr
            this.Try_0503E650()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg("Caricamento fallito %1",,"", i_ErrMsg)
            endif
            bTrsErr=bTrsErr or bErr_0503E650
            * --- End
          else
            if Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
              * --- Esamino le righe con riferimenti per l'evasione, queste saranno trattate immediatamente
              *     tramite l'invocazione di GSVE_BI3. (solo da ciclo documentale).
              *     Per le righe di evasione raggruppo e porto nel cursore solo la somma
              *     delle quantit�, cMatr ed i riferimenti per l'evasione.
              Select ; 
 Max( t_GPEVASIO ) As XCHK, Sum(t_GPQTAMOV) As MVQTAMOV,Min(0) As OLQTAEVA, Sum(t_GPQTAMOV) As MVQTAEVA, ; 
 Sum(t_GPPREZZO) As MVIMPEVA,Min(0) As OLIMPEVA, t_GPSERRIF As MVSERIAL, t_GPROWRIF As CPROWNUM, ; 
 Min(t_CPROWORD) As CPROWORD, MAX( t_GPFLDOCU ) As FLDOCU,; 
 MAX( t_GPFLINTE ) As FLINTE, MAX( t_GPFLACCO ) As FLACCO, Min(t_TIPRIG) As MVTIPRIG, Min(t_DOIMPEVA) As DOIMPEVA,Min(t_DOQTAEV1) As DOQTAEV1,; 
 t_GPPREZZO As VISPRE, t_GPCODATT As MVCODATT, t_GPCODCOM As MVCODCOM, ; 
 t_GPCODCEN As MVCODCEN, Min( t_SCONT1 ) As MVSCONT1, Min( t_SCONT2 ) As MVSCONT2, ; 
 Min( t_SCONT3 ) As MVSCONT3, Min( t_SCONT4 ) As MVSCONT4,; 
 t_GPUNIMIS As MVUNIMIS, t_GPCODICE As MVCODICE, Min(t_FLGROR) As FLGROR, ; 
 Max( t_GPCODMAT ) As cMATR, t_GPCODLOT, t_GPCODUBI , max(t_GPCODMA2) as GPCODMA2, ; 
 SPACE(30) AS MVAGG_01, SPACE(30) AS MVAGG_02, SPACE(30) AS MVAGG_03, SPACE(30) AS MVAGG_04, cp_CharToDate("  -  -    ") AS MVAGG_05, cp_CharToDate("  -  -    ") AS MVAGG_06 ; 
 From ( this.w_GSAR_MPG.cTrsName ) ; 
 Where t_STATOROW="S" And Not Deleted() And Not Empty( t_GPSERRIF ) ; 
 Group By t_GPSERRIF, t_GPROWRIF, t_GPCODICE, t_GPCODLOT , t_GPCODUBI , t_GPCODMAG , t_GPPREZZO, t_GPUNIMIS,; 
 t_GPCODCEN, t_GPCODCOM,t_GPCODATT; 
 Order By t_GPSERRIF, t_GPROWRIF, t_CPROWORD; 
 Into Cursor RigheDoc
              if RecCount("RigheDoc")>0
                * --- Valorizzo le variabili sulla gestione per generare o meno le righe
                *     descrittive...
                this.oParentObject.w_OBJ_GEST.w_FLNSRI = this.oParentObject.w_FLNSRI
                this.oParentObject.w_OBJ_GEST.w_FLVSRI = this.oParentObject.w_FLVSRI
                this.oParentObject.w_OBJ_GEST.w_FLRIDE = this.oParentObject.w_FLRIDE
                if Empty(this.oParentObject.w_OBJ_GEST.w_Cur_Ev_Row)
                  * --- L_Cur_Ev_Row contiene il nome del cursore utilizzato per contenere le righe
                  *     evase ma non importate. Viene valorizzata solo se non lo � gi�. 
                  *     Nel caso si preme pi� volte il bottone importa, il nome del cursore deve rimanere sempre lo stesso
                  this.oParentObject.w_OBJ_GEST.w_Cur_Ev_Row = Sys(2015)
                endif
                if Empty(this.oParentObject.w_OBJ_GEST.w_Cur_Rag_Row)
                  * --- L_Cur_Rag_Row contiene il nome del cursore utilizzato per contenere le righe
                  *     Raggruppate in Importazione. Viene valorizzata solo se non lo � gi�. 
                  *     Nel caso si preme pi� volte il bottone importa, il nome del cursore deve rimanere sempre lo stesso
                  this.oParentObject.w_OBJ_GEST.w_Cur_Rag_Row = Sys(2015)
                endif
                L_GSAR_MPG_TRSNAME = this.w_GSAR_MPG.cTrsName
                * --- ImportaDoc chiude anche il cursore RigheDoc...
                this.oParentObject.w_OBJ_GEST.NotifyEvent("ImportaDoc")     
              else
                * --- Se non lancio l'import chiudo RigheDoc
                Use in RigheDoc
              endif
            endif
            * --- Creo la variabile che conterr� il nome del cursore sul quale verr�
            *     passato e ordinato il cTrsName della maschera GSAR_MPG
            *     e la sposto in una L_... con EXEC per problema macro
            this.w_TEMPORANEO = Sys(2015)
            L_TEMPORANEO = this.w_TEMPORANEO
            * --- Raggruppo le righe valide sulla movimentazione sommandone le quantit�.
            *     Costruisco anche un campo di servizio (cMATR) per sapere se tra le righe
            *     raggruppate ho racchiuso righe con matricole. Questo campo mi
            *     dir�, a seguito dell'inserzione della riga, se devo andare a recuperare l'elenco
            *     delle sue matricole.
            *     Se righe analoghe hanno gli altri campi differenti la procedura svolger� un Max
            *     su questi..
            * --- Raggruppo per tutti i campi tranne che per la matricola...
             
 Select t_GPCODICE, t_GPCODLOT , t_GPCODUBI , t_GPCODMAG , t_GPPREZZO, t_GPUNIMIS,; 
 Sum( t_GPQTAMOV ) As t_GPQTAMOV, t_GPCODCEN, t_GPCODCOM,; 
 t_GPCODATT, Max ( t_TIPATT ) As t_TIPATT, Max ( t_CODART ) As t_CODART, Max( t_GPCODMAT ) As cMATR,; 
 t_GPCODUB2 , t_GPCODMA2, t_GPUNILOG, Sum( t_GPQTAUM1 ) As t_GPQTAUM1; 
 From ( this.w_GSAR_MPG.cTrsName ) ; 
 Where t_STATOROW="S" And Not Deleted() And Empty( t_GPSERRIF ) ; 
 Group By t_GPCODICE, t_GPCODLOT , t_GPCODUBI , t_GPCODMAG , t_GPPREZZO, t_GPUNIMIS,; 
 t_GPCODCEN, t_GPCODCOM,t_GPCODATT , t_GPCODUB2 , t_GPCODMA2, t_GPUNILOG; 
 Order By t_CPROWORD ; 
 into Cursor &L_TEMPORANEO 
            if RecCount( L_Temporaneo )>0
              this.oParentObject.w_OBJ_GEST.MarkPos()     
              * --- A pag3 prelevo i dati dal temporaneo del dettaglio inserimento rapido...
              this.w_TIPOCUR = .F.
              * --- Scorro il transitorio della movimentazione e per ogni riga con w_STATOROW='S' 
              *     la riporto nella gestione
              this.w_NRIGA = 1
               
 Select ( L_TEMPORANEO ) 
 Go Top
              * --- Disabilito durante il riempimento del temporaneo il ricalcolo prezzi...
              *     Solo gestione documenti
              if Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm" 
                this.w_OLD_NOPRSC = this.oParentObject.w_OBJ_GEST.w_NOPRSC
                this.oParentObject.w_OBJ_GEST.w_NOPRSC = "S"
                * --- Indico alla gestione padre che sto eseguendo un caricamento
                this.oParentObject.w_OBJ_GEST.w_NUMGES = 1
              endif
              * --- Disabilito i post-In (li riabilito al termine del caricamento)
              this.w_OLDPOSTIN = i_ServerConn[1,7]
              i_ServerConn[1,7]=.F.
              Scan
              this.oParentObject.w_OBJ_GEST.AddRow()     
              * --- Mi posizono sulla riga da riempire (se non lo facessi la EcpDrop darebbe risultati errati)
              *     Questo perch� la Drop viene svolta sulla riga corrente per il Painter (nabsrow)
              *     Esegue anche la ChildrenChangeRow
              this.oParentObject.w_OBJ_GEST.oPgFrm.Page1.oPag.oBody.SetCurrentRow()     
              * --- Aggiorno le variabili o_xxx per calcoli in mCalc
              this.oParentObject.w_OBJ_GEST.SaveDependsOn()     
              ah_Msg("Inserimento riga %1 ...",.T.,,, Alltrim(Str(this.w_NRIGA)))
              * --- Assegno i valori in ordine di inputazione simulando i passaggi 
              *     svolti dall'utente...
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVCODICE" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  this.oParentObject.w_OBJ_GEST.w_MDQTAMOV = 0
                  this.w_NOMEVAR = "w_MDCODICE" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_NOMEVAR = "w_MMCODICE" 
              endcase
              this.w_NOMEFIELD = "t_GPCODICE"
              this.w_TIPO = "NOFIXED"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVUNIMIS" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  this.w_NOMEVAR = "w_MDUNIMIS" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_NOMEVAR = "w_MMUNIMIS" 
              endcase
              this.w_NOMEFIELD = "t_GPUNIMIS"
              this.w_TIPO = "NOFIXED"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_NOMEFIELD = "t_GPQTAMOV"
              this.w_TIPO = "NOFIXED"
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_OBJCTRL = this.oParentObject.w_OBJ_GEST.GetBodyCtrl( "w_MVQTAMOV" )
                  if this.w_OBJCTRL.mCond()
                    * --- Valorizzo il Control..
                     
 Select ( L_TEMPORANEO )
                    this.w_OBJCTRL.Value = t_GPQTAMOV
                    * --- Se prezzo a zero lancio il ricalcolo
                     
 Select ( L_TEMPORANEO )
                    if Nvl( t_GPPREZZO, 0 )=0
                      this.oParentObject.w_OBJ_GEST.w_NOPRSC = this.w_OLD_NOPRSC
                      * --- Considero il control sempre modificato per gestire i valori da proporre
                      *     (se da penna imposto un valore uguale a quello da proporre la procedura 
                      *     non fa scattare vari eventi..)
                      this.w_OBJCTRL.bUpd = .t.
                    endif
                    * --- Valid sul campo, esegue Check, NotifyEvent Changed, mCalc e SaveDependsOn
                    this.w_OBJCTRL.Valid()     
                    * --- Mi rimetto nella condizione di non dover ricalcolare i prezzi..
                    this.oParentObject.w_OBJ_GEST.w_NOPRSC = "S"
                  endif
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  * --- Se quantit� proposta imposto cmq quella da Car.Rapido
                  this.w_OLD_QT1 = this.oParentObject.w_OBJ_GEST.w_QT1
                  this.oParentObject.w_OBJ_GEST.w_QT1 = "N"
                  this.w_NOMEVAR = "w_MDQTAMOV" 
                  this.Page_3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.oParentObject.w_OBJ_GEST.w_QT1 = this.w_OLD_QT1
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_NOMEVAR = "w_MMQTAMOV" 
                  this.Page_3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
              endcase
              * --- Per valorizzare cmq il magazzino anche se fuori sequenza...
              if Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                this.w_OLD_FM1 = this.oParentObject.w_OBJ_GEST.w_FM1
                this.oParentObject.w_OBJ_GEST.w_FM1 = " "
              endif
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVCODMAG" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  * --- Nel caso di POS non riporto il magazzino
                  this.w_NOMEVAR = "" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_COND = .T.
                  this.w_NOMEVAR = "w_MMCODMAG" 
              endcase
              this.w_NOMEFIELD = "t_GPCODMAG"
              this.w_TIPO = "NOFIXED"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Re imposto il valore originale di w_FM1
              if Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                this.oParentObject.w_OBJ_GEST.w_FM1 = this.w_OLD_FM1
              endif
               
 Select ( L_TEMPORANEO )
              * --- Per valorizzare cmq il magazzino collegato anche se fuori sequenza...
              if Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                this.w_OLD_FM2 = this.oParentObject.w_OBJ_GEST.w_FM2
                this.oParentObject.w_OBJ_GEST.w_FM2 = " "
              endif
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVCODMAT" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  * --- Nel caso di POS non riporto il magazzino
                  this.w_NOMEVAR = "" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_COND = .T.
                  this.w_NOMEVAR = "w_MMCODMAT" 
              endcase
              this.w_NOMEFIELD = "t_GPCODMA2"
              if Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv"
                this.w_TIPO = "FIXED"
              else
                this.w_TIPO = "NOFIXED"
              endif
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Re imposto il valore originale di w_FM1
              if Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                this.oParentObject.w_OBJ_GEST.w_FM2 = this.w_OLD_FM2
              endif
               
 Select ( L_TEMPORANEO )
              if Nvl( t_GPPREZZO, 0 )<>0
                do case
                  case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                    * --- Valorizzo sempre il prezzo (a prescindere dalla condizione)
                    this.w_COND = .T.
                    this.w_NOMEVAR = "w_MVPREZZO" 
                  case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                    * --- Valorizzo sempre il prezzo (a prescindere dalla condizione)
                    this.w_COND = .T.
                    this.w_NOMEVAR = "w_MDPREZZO" 
                  case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                    this.w_COND = .T.
                    this.w_NOMEVAR = "w_MMPREZZO" 
                endcase
                this.w_TIPO = "NOFIXED"
                this.w_NOMEFIELD = "t_GPPREZZO"
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
               
 Select ( L_TEMPORANEO )
              if NVL( t_GPQTAUM1 , 0 ) <> 0
                do case
                  case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                    this.w_NOMEVAR = "w_MVQTAUM1" 
                  case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                    this.w_NOMEVAR = "" 
                  case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                    this.w_NOMEVAR = "w_MMQTAUM1" 
                endcase
                this.w_TIPO = "FIXED"
                this.w_NOMEFIELD = "t_GPQTAUM1"
              endif
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVCODLOT" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  * --- Nel caso di POS non riporto il magazzino
                  this.w_NOMEVAR = "" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_NOMEVAR = "w_MMCODLOT" 
              endcase
              this.w_NOMEFIELD = "t_CODART;t_GPCODLOT"
              this.w_TIPO = "FIXED"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVCODUBI" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  * --- Nel caso di POS non riporto il magazzino
                  this.w_NOMEVAR = "" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_NOMEVAR = "w_MMCODUBI" 
              endcase
              this.w_NOMEFIELD = "t_GPCODMAG;t_GPCODUBI"
              this.w_TIPO = "FIXED"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVCODUB2" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  * --- Nel caso di POS non riporto il magazzino
                  this.w_NOMEVAR = "" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_NOMEVAR = "w_MMCODUB2" 
              endcase
              this.w_NOMEFIELD = "t_GPCODMA2;t_GPCODUB2"
              this.w_TIPO = "FIXED"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVCODCEN" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  * --- Nel caso di POS non riporto il centro di costo
                  this.w_NOMEVAR = "" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_NOMEVAR = "" 
              endcase
              this.w_NOMEFIELD = "t_GPCODCEN"
              this.w_TIPO = "FIXED"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVCODCOM" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  * --- Nel caso di POS non riporto il magazzino
                  this.w_NOMEVAR = "" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_NOMEVAR = "" 
              endcase
              this.w_NOMEFIELD = "t_GPCODCOM"
              this.w_TIPO = "FIXED"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVCODATT" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  * --- Nel caso di POS non riporto il magazzino
                  this.w_NOMEVAR = "" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_NOMEVAR = "" 
              endcase
              this.w_NOMEFIELD = "t_GPCODCOM;t_TIPATT;t_GPCODATT"
              this.w_TIPO = "FIXED"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
               
 Select ( L_TEMPORANEO )
              do case
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                  this.w_NOMEVAR = "w_MVUNILOG" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsps_mvd"
                  * --- Nel caso di POS non riporto il magazzino
                  this.w_NOMEVAR = "" 
                case Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsma_mvm"
                  this.w_NOMEVAR = "" 
              endcase
              this.w_NOMEFIELD = "t_GPUNILOG"
              this.w_TIPO = "FIXED"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
               
 Select ( L_TEMPORANEO )
              if Lower ( this.oParentObject.w_OBJ_GEST.class ) <> "tgsps_mvd" And Not Empty( Nvl ( cMatr ,"" ) )
                this.Page_5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              this.oParentObject.w_OBJ_GEST.SaveRow()     
              this.w_NRIGA = this.w_NRIGA + 1
              EndScan
              * --- Rimetto al valore originario l'abilitazione o meno dei post-in
              i_ServerConn[1,7]= this.w_OLDPOSTIN 
              * --- Rimetto al valore originario w_NOPRSC (solo ciclo documentale)
              if Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsve_mdv" Or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsor_mdv" or Lower ( this.oParentObject.w_OBJ_GEST.class ) = "tgsac_mdv"
                this.oParentObject.w_OBJ_GEST.w_NOPRSC = this.w_OLD_NOPRSC
              endif
              Wait Clear
              * --- Per Evitare che rimanga posizionato sull'unit� di misura
              *     In alcuni casi con righe miste fra importate e caricate manualmente, 
              *     il focus rimane nell'unit� di misura della prima riga descrittiva chiedendo 'Campo Obbligatorio'
              *     Sembra che il problema sia inoltre solo su Oracle. Non � stato possibile replicarlo su Sql
              this.oParentObject.w_OBJ_GEST.oPgFrm.Page1.oPag.oBody.oFirstControl.SetFocus()     
              this.oParentObject.w_OBJ_GEST.RePos()     
            endif
            * --- Evito l'esecuzione della mCalc
            this.bUpdateParentObject=.f.
            * --- Chiusura cursore Temporaneo
            if Used ( L_TEMPORANEO )
              select &L_TEMPORANEO 
 USE
            endif
          endif
        case this.pOper="L"
          * --- Gestione lettura dati da dispositivo (Penna Ottica / File di testo)
          *     Macro Passi
          *     
          *     1) Determinazione Dispositivo da utilizzare (CNF nella temporanea del Client)
          *     2) Lancio Batch di gestione del dispositivo (crea un temporaneo nominato in base al parametro)
          *     3) Apertura movimentazione fittizia riempendola con contenuto cursore
          *     4) Alla conferma della movimentazione scrittura sul transitorio della gestione
          *     
          *     I passi 3 e 4 sono svolti  nel CASE ramo 'C'
          * --- Parametro (Nome della gestione che invoca, Es. GSPS_MDV)
          this.oParentObject.w_CURSORE = Sys(2015)
          do while Used( this.oParentObject.w_CURSORE) 
            this.oParentObject.w_CURSORE = Sys(2015)
          enddo
          * --- Creo cursore (Codice � il codice di ricerca)
           
 Create Cursor ( this.oParentObject.w_CURSORE ) ( CODICE C(20) NULL, UNIMIS C(3) NULL, QTAMOV N(12,3) NULL, PREZZO N(18,5) NULL,; 
 LOTTO CHAR(20) NULL , CODMAG C(5) NULL,UBICAZIONE C(20) NULL , MATRICOLA C(40) NULL,SERRIF C(10) NULL , ; 
 CODMA2 CHAR(5) NULL, UBICAZION2 C(20) NULL, COMMESSA C(15) NULL, ATTIVITA C(15) NULL,CENTRO C(15) NULL, QTAUM1 N(12,3) NULL, UNILOG C(18) NULL)
          this.w_GSAR_MPG.w_MSG = ""
          * --- Leggo il dispositivo installato
          Do GSUT_BLD with This, "L"
          if Type( "g_CODPEN" )="C"
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- L'inserimento rapido nel caso di matricole non da il messaggio di dispostivo
            *     non installato, perch� la maschera deve essere utilizzata per caricare le
            *     matricole.
            if g_MATR<>"S"
              ah_ErrorMsg("Dispositivo non definito sul client, utilizzare l'apposita funzione per definirne uno",,"")
            endif
            i_retcode = 'stop'
            return
          endif
          this.w_GSAR_MPG.oPgFrm.ActivePage=3
          * --- Chiamo il Driver
          L_OldError = on("ERROR")
          Do ( this.w_PROGRA) With this , this.oParentObject.w_CURSORE , this.w_PROPAR , this.w_GSAR_MPG, .f. ,.f.
          on error &L_OldError 
        case this.pOper="G-MAG" Or this.pOper="G-MA2" Or this.pOper="G-ANA" Or this.pOper="V"
          if this.pOper="V"
            * --- Sbianca il contenuto del campo memo
            this.oParentObject.w_Msg = ""
            this.w_RIGAERR = 0
          endif
          this.w_GSAR_MPG.MarkPos()     
          * --- Eseguo la replace direttamente sul transitorio, scrivo i valori direttamente
          *     per evitare problemi nel caso manchi la qt� (ubicazione non editabile)
           
 Select (this.w_GSAR_MPG.ctrsname) 
 Go Top 
 Scan
          if this.pOper="G-ANA" Or this.pOper="G-MAG" Or this.pOper="G-MA2"
            do case
              case this.pOper="G-ANA"
                if NOT EMPTY(this.oParentObject.w_CODCENTES)
                   
 Replace t_GPCODCEN With this.oParentObject.w_CODCENTES
                endif
                if NOT EMPTY(this.oParentObject.w_CODCOMTES)
                   
 Replace t_GPCODCOM with this.oParentObject.w_CODCOMTES
                endif
                if NOT EMPTY(this.oParentObject.w_CODATTTES)
                   
 Replace t_GPCODATT with this.oParentObject.w_CODATTTES
                endif
              case this.pOper="G-MAG"
                 
 Replace t_GPCODMAG with this.oParentObject.w_CODMAGTES, t_GPCODUBI with this.oParentObject.w_UBITES, t_FLUBIC with this.oParentObject.w_FLUBICTES
                REPLACE t_MxFLLOTT WITH IIF((g_PERLOT="S" AND t_FLLOTT$ "SC") OR (g_PERUBI="S" AND t_FLUBIC="S"), LEFT(ALLTRIM(this.oParentObject.w_FLCASC)+IIF(this.oParentObject.w_FLRISE="+", "-", IIF(this.oParentObject.w_FLRISE="-", "+", " ")), 1), " ")
              case this.pOper="G-MA2"
                 
 Replace t_GPCODMA2 with this.oParentObject.w_CODMA2TES, t_GPCODUB2 with this.oParentObject.w_UB2TES, t_F2UBIC with this.oParentObject.w_FLUBI2TES
                REPLACE t_MxF2LOTT WITH IIF((g_PERLOT="S" AND t_FLLOTT$ "SC") OR (g_PERUBI="S" AND t_F2UBIC="S"), LEFT(ALLTRIM(this.oParentObject.w_F2CASC)+IIF(this.oParentObject.w_F2RISE="+", "-", IIF(this.oParentObject.w_F2RISE="-", "+", " ")), 1), " ")
            endcase
            * --- Aggiorno le propriet�
            this.w_GSAR_MPG.WorkFromTrs()     
            * --- Aggiorno lo stato della riga
            this.w_GSAR_MPG.State_Row()     
            * --- Aggiorno di conseguenza il transitorio
            this.w_GSAR_MPG.TrsFromWork()     
          else
            * --- Aggiorno le propriet�
            this.w_GSAR_MPG.WorkFromTrs()     
            * --- Aggiorno lo stato della riga
            this.w_GSAR_MPG.State_Row()     
            if Not Empty( this.oParentObject.w_CAPTION )
              AddMsgNL("%0Riga %1 %2", this, Alltrim(Str( this.oParentObject.w_CPROWORD )), this.oParentObject.w_CAPTION)
              this.w_RIGAERR = this.w_RIGAERR + 1
            endif
          endif
           
 SELECT ( this.w_GSAR_MPG.cTrsName) 
 EndScan
          this.w_GSAR_MPG.RePos()     
          * --- Al termine del caricamento mi riposiziono nel primo TAB
          if this.pOper="G-MAG" Or this.pOper="G-MA2" Or this.pOper="G-ANA" 
            this.w_GSAR_MPG.oPgFrm.ActivePage=1
          else
            if this.w_RIGAERR=0
              AddMsgNL("Tutte le righe presenti saranno inserite")
            endif
            ah_ErrorMsg("Verifica completata",,"")
          endif
        case this.pOper="Z"
          * --- Carico nel temporaneo i documenti selezionati (o dall'utente o gia importati)
           
 Select * from ( this.w_GSAR_MPG.cTrsName ) where Not Empty(Nvl(t_GPCODICE,Space(20))) into cursor Temp NoFilter
          if Reccount("Temp") <> 0
            * --- Se non esistono righe significative nel temporaneo non eseguo il match
            if UPPER(this.oParentObject.w_OBJ_GEST.cPrg) $ "GSVE_MDV-GSAC_MDV-GSOR_MDV"
              * --- --VARIABILI DEFINITE COME IN GSVE_KIM
              * --- Attraverso la TMP_MAST recupero sul database le righe da importare..
              if g_MATR="S"
                * --- Riempio il temporaneo (TMP_MAST) con i documenti selezionati
                *     dall'utente
                * --- Utilizzo la stessa query dello zoom per uniformit� dei dati (w_NOSEL
                *     mi crea un risultato vuoto se valorizzato a 'N')
                this.w_NOSEL = "N"
                * --- Create temporary table TMP_MAST
                i_nIdx=cp_AddTableDef('TMP_MAST') && aggiunge la definizione nella lista delle tabelle
                i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
                vq_exec('QUERY\GSVE_KI2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
                this.TMP_MAST_idx=i_nIdx
                i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
                SELECT Righedoc
                * --- SELEZIONA SOLO LE RIGHE ANCORA DA EVADERE COME NEL RAMO ELSE
                SCAN FOR ; 
 ((XCHK=1 AND FLDOCU <>"S" And ( ( NVL(MVQTAEVA,0)<>0 AND MVTIPRIG<>"F") OR ; 
 (MVTIPRIG="F" AND NVL(MVIMPEVA,0)<>0 ) ) )OR ; 
 ( (NVL(OLQTAEVA,0)< NVL(MVQTAMOV,0)) AND NVL(MVQTAEVA,0)<>0 AND MVTIPRIG<>"F") OR ; 
 (MVTIPRIG="F" AND ( (NVL(OLIMPEVA,0)<NVL(VISPRE,0) AND NVL(VISPRE,0) >0) OR ; 
 (NVL(OLIMPEVA,0)>NVL(VISPRE,0) AND NVL(VISPRE,0) <0) ) AND NVL(MVIMPEVA,0)<>0) OR ; 
 (MVTIPRIG="D" AND NVL(MVQTAEVA,0)<>0)) AND CPROWNUM<>0
                this.w_LSERIAL = RIGHEDOC.MVSERIAL
                this.w_LCPROWNUM = RIGHEDOC.CPROWNUM
                this.w_LALFDOC = NVL(RIGHEDOC.MVALFDOC,Space(10))
                this.w_LDATDOC = CP_TODATE(RIGHEDOC.MVDATDOC)
                this.w_LNUMDOC = NVL(RIGHEDOC.MVNUMDOC,0)
                this.w_LTIPDOC = NVL(RIGHEDOC.MVTIPDOC,SPACE(5))
                this.w_LCLADOC = NVL(RIGHEDOC.MVCLADOC,SPACE(2))
                this.w_LFLVEAC = NVL(RIGHEDOC.MVFLVEAC," ")
                this.w_DCFLGROR = Nvl ( RIGHEDOC.FLGROR , " " )
                this.w_LQTAMOV = RIGHEDOC.MVQTAEVA
                this.w_LPREZZO = RIGHEDOC.MVIMPEVA
                this.w_LFLDOCU = NVL( FLDOCU , " " )
                this.w_LFLINTE = NVL( FLINTE , " " )
                this.w_LFLACCO = NVL( FLACCO , " " )
                * --- Insert into TMP_MAST
                i_nConn=i_TableProp[this.TMP_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"MVSERIAL"+",MVCODVAL"+",MVDATDOC"+",MVNUMDOC"+",MVALFDOC"+",MVTIPDOC"+",TDDESDOC"+",MVFLVEAC"+",MVCLADOC"+",DCFLGROR"+",CPROWNUM"+",MVQTAMOV"+",MVPREZZO"+",FLDOCU"+",FLINTE"+",FLACCO"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_LSERIAL),'TMP_MAST','MVSERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LCODVAL),'TMP_MAST','MVCODVAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LDATDOC),'TMP_MAST','MVDATDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LNUMDOC),'TMP_MAST','MVNUMDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LALFDOC),'TMP_MAST','MVALFDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LTIPDOC),'TMP_MAST','MVTIPDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LDESDOC),'TMP_MAST','TDDESDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LFLVEAC),'TMP_MAST','MVFLVEAC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LCLADOC),'TMP_MAST','MVCLADOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DCFLGROR),'TMP_MAST','DCFLGROR');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LCPROWNUM),'TMP_MAST','CPROWNUM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LQTAMOV),'TMP_MAST','MVQTAMOV');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LPREZZO),'TMP_MAST','MVPREZZO');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LFLDOCU),'TMP_MAST','FLDOCU');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LFLINTE),'TMP_MAST','FLINTE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LFLACCO),'TMP_MAST','FLACCO');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_LSERIAL,'MVCODVAL',this.w_LCODVAL,'MVDATDOC',this.w_LDATDOC,'MVNUMDOC',this.w_LNUMDOC,'MVALFDOC',this.w_LALFDOC,'MVTIPDOC',this.w_LTIPDOC,'TDDESDOC',this.w_LDESDOC,'MVFLVEAC',this.w_LFLVEAC,'MVCLADOC',this.w_LCLADOC,'DCFLGROR',this.w_DCFLGROR,'CPROWNUM',this.w_LCPROWNUM,'MVQTAMOV',this.w_LQTAMOV)
                  insert into (i_cTable) (MVSERIAL,MVCODVAL,MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,TDDESDOC,MVFLVEAC,MVCLADOC,DCFLGROR,CPROWNUM,MVQTAMOV,MVPREZZO,FLDOCU,FLINTE,FLACCO &i_ccchkf. );
                     values (;
                       this.w_LSERIAL;
                       ,this.w_LCODVAL;
                       ,this.w_LDATDOC;
                       ,this.w_LNUMDOC;
                       ,this.w_LALFDOC;
                       ,this.w_LTIPDOC;
                       ,this.w_LDESDOC;
                       ,this.w_LFLVEAC;
                       ,this.w_LCLADOC;
                       ,this.w_DCFLGROR;
                       ,this.w_LCPROWNUM;
                       ,this.w_LQTAMOV;
                       ,this.w_LPREZZO;
                       ,this.w_LFLDOCU;
                       ,this.w_LFLINTE;
                       ,this.w_LFLACCO;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error='Caricamento Testata'
                  return
                endif
                ENDSCAN
                VQ_exec("query\gsveMki2",this.w_GSAR_MPG ,"gsveDkim")
              else
                 
 SELECT ; 
 CPROWORD,MVCODICE,MVUNIMIS,MVQTAEVA As MVQTAMOV,0*MVQTAMOV As MVQTAEVA,MVDATEVA,; 
 MVTIPRIG,VISPRE,0*MVIMPEVA As MVIMPEVA, MVIMPEVA As MVPREZZO,MVSERIAL,CPROWNUM,; 
 FLDOCU, FLINTE, FLACCO,MVCODART,MVDATDOC,MVALFDOC,MVNUMDOC,MVCODUBI,; 
 MVCODLOT,Repl(" ",40) as MVCODMAT,MVCODMAG,"S" as NODOCU,DOQTAEV1,DOIMPEVA,MVFLRISE,; 
 MVCLADOC,MVFLVEAC," " as ARGESMAT,MVCODATT, MVCODCOM, MVCODCEN,MVSCONT1, MVSCONT2, MVSCONT3, MVSCONT4,; 
 FLGROR,MVQTAUM1-DOQTAEV1 as QTAUM1,MVTIPDOC; 
 FROM RigheDoc ; 
 WHERE ((XCHK=1 AND FLDOCU <>"S" And ( ( NVL(MVQTAEVA,0)<>0 AND MVTIPRIG<>"F") OR ; 
 (MVTIPRIG="F" AND NVL(MVIMPEVA,0)<>0 ) ) )OR ; 
 ( (NVL(OLQTAEVA,0)< NVL(MVQTAMOV,0)) AND NVL(MVQTAEVA,0)<>0 AND MVTIPRIG<>"F") OR ; 
 (MVTIPRIG="F" AND ( (NVL(OLIMPEVA,0)<NVL(VISPRE,0) AND NVL(VISPRE,0) >0) OR ; 
 (NVL(OLIMPEVA,0)>NVL(VISPRE,0) AND NVL(VISPRE,0) <0) ) AND NVL(MVIMPEVA,0)<>0) OR ; 
 (MVTIPRIG="D" AND NVL(MVQTAEVA,0)<>0)) AND CPROWNUM<>0 ; 
 INTO CURSOR gsveDkim ORDER BY CPROWORD ASC 
 
 Use in Select("RigheDoc")
              endif
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            ah_ErrorMsg("Attenzione: caricamento rapido senza righe di dettaglio",,"")
          endif
          USE IN SELECT("TEMP")
          * --- Il temporaneo non mi serve pi� lo droppo
          * --- Drop temporary table TMP_MAST
          i_nIdx=cp_GetTableDefIdx('TMP_MAST')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMP_MAST')
          endif
      endcase
    else
      ah_ErrorMsg("Impossibile individuare oggetto gestione",,"")
      i_retcode = 'stop'
      return
    endif
  endproc
  proc Try_05062140()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_TROVATO = .F.
    if g_UNIMAT = "A"
      * --- Se attivata univocit� per articolo, occorre eseguire la ricerca per codice matricola e per articolo
      * --- Select from MATRICOL
      i_nConn=i_TableProp[this.MATRICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2],.t.,this.MATRICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" MATRICOL ";
            +" where AMKEYSAL = "+cp_ToStrODBC(this.oParentObject.w_CODART)+" AND AMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODMAT)+"";
             ,"_Curs_MATRICOL")
      else
        select * from (i_cTable);
         where AMKEYSAL = this.oParentObject.w_CODART AND AMCODICE = this.oParentObject.w_CODMAT;
          into cursor _Curs_MATRICOL
      endif
      if used('_Curs_MATRICOL')
        select _Curs_MATRICOL
        locate for 1=1
        do while not(eof())
        this.w_TROVATO = .T.
          select _Curs_MATRICOL
          continue
        enddo
        use
      endif
    endif
    if g_UNIMAT = "M"
      * --- Se attivata univocit� per matricole, occorre eseguire la ricerca solo per codice matricola
      * --- Select from MATRICOL
      i_nConn=i_TableProp[this.MATRICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2],.t.,this.MATRICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" MATRICOL ";
            +" where AMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODMAT)+"";
             ,"_Curs_MATRICOL")
      else
        select * from (i_cTable);
         where AMCODICE = this.oParentObject.w_CODMAT;
          into cursor _Curs_MATRICOL
      endif
      if used('_Curs_MATRICOL')
        select _Curs_MATRICOL
        locate for 1=1
        do while not(eof())
        this.w_TROVATO = .T.
          select _Curs_MATRICOL
          continue
        enddo
        use
      endif
    endif
    if g_UNIMAT $ "CE"
      * --- Se attivata univocit� per classe matricole, il codice non deve essere inserito nei casi
      *     1 - Il codice matricola esiste ed � abbinato alla stessa classe matricola
      *     2 - Il codice matricola esiste ed � abbinato ad una qualsiasi classe matricola con il flag 'matricole univoche'
      * --- Cerca prima il codice matricola abbinato alla stessa classe matricola
      * --- Select from MATRICOL
      i_nConn=i_TableProp[this.MATRICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2],.t.,this.MATRICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" MATRICOL ";
            +" where AMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODMAT)+" AND AMCLAMAT = "+cp_ToStrODBC(this.w_CLAMAT)+"";
             ,"_Curs_MATRICOL")
      else
        select * from (i_cTable);
         where AMCODICE = this.oParentObject.w_CODMAT AND AMCLAMAT = this.w_CLAMAT;
          into cursor _Curs_MATRICOL
      endif
      if used('_Curs_MATRICOL')
        select _Curs_MATRICOL
        locate for 1=1
        do while not(eof())
        this.w_TROVATO = .T.
          select _Curs_MATRICOL
          continue
        enddo
        use
      endif
      * --- Se non lo trova, cerca se il codice � abbinato ad una qualsiasi classe matricola con il flag 'matricole univoche'
      if NOT this.w_TROVATO
        * --- Select from CMT_MAST
        i_nConn=i_TableProp[this.CMT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CMT_MAST_idx,2],.t.,this.CMT_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CMCODICE  from "+i_cTable+" CMT_MAST ";
              +" where CMUNIMAT = 'S'";
               ,"_Curs_CMT_MAST")
        else
          select CMCODICE from (i_cTable);
           where CMUNIMAT = "S";
            into cursor _Curs_CMT_MAST
        endif
        if used('_Curs_CMT_MAST')
          select _Curs_CMT_MAST
          locate for 1=1
          do while not(eof())
          this.w_CLAMAT2 = CMCODICE
          * --- Select from MATRICOL
          i_nConn=i_TableProp[this.MATRICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2],.t.,this.MATRICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" MATRICOL ";
                +" where AMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODMAT)+" AND AMCLAMAT = "+cp_ToStrODBC(this.w_CLAMAT2)+"";
                 ,"_Curs_MATRICOL")
          else
            select * from (i_cTable);
             where AMCODICE = this.oParentObject.w_CODMAT AND AMCLAMAT = this.w_CLAMAT2;
              into cursor _Curs_MATRICOL
          endif
          if used('_Curs_MATRICOL')
            select _Curs_MATRICOL
            locate for 1=1
            do while not(eof())
            this.w_TROVATO = .T.
              select _Curs_MATRICOL
              continue
            enddo
            use
          endif
            select _Curs_CMT_MAST
            continue
          enddo
          use
        endif
      endif
    endif
    if NOT this.w_TROVATO
      * --- INSERISCE IL CODICE MATRICOLA
      * --- Insert into MATRICOL
      i_nConn=i_TableProp[this.MATRICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MATRICOL_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"AMKEYSAL"+",AMCODICE"+",AMCODART"+",AMCLAMAT"+",AMDATCRE"+",AM_PROGR"+",AM_PRENO"+",AM_PRODU"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'MATRICOL','AMKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAT),'MATRICOL','AMCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'MATRICOL','AMCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLAMAT),'MATRICOL','AMCLAMAT');
        +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MATRICOL','AMDATCRE');
        +","+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PROGR');
        +","+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
        +","+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRODU');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'AMKEYSAL',this.oParentObject.w_CODART,'AMCODICE',this.oParentObject.w_CODMAT,'AMCODART',this.oParentObject.w_CODART,'AMCLAMAT',this.w_CLAMAT,'AMDATCRE',i_DATSYS,'AM_PROGR',0,'AM_PRENO',0,'AM_PRODU',0)
        insert into (i_cTable) (AMKEYSAL,AMCODICE,AMCODART,AMCLAMAT,AMDATCRE,AM_PROGR,AM_PRENO,AM_PRODU &i_ccchkf. );
           values (;
             this.oParentObject.w_CODART;
             ,this.oParentObject.w_CODMAT;
             ,this.oParentObject.w_CODART;
             ,this.w_CLAMAT;
             ,i_DATSYS;
             ,0;
             ,0;
             ,0;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_05082E08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_TROVATO = .F.
    * --- Select from LOTTIART
    i_nConn=i_TableProp[this.LOTTIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select LOCODICE,LOCODART  from "+i_cTable+" LOTTIART ";
          +" where LOCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODLOT)+" AND LOCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART)+"";
           ,"_Curs_LOTTIART")
    else
      select LOCODICE,LOCODART from (i_cTable);
       where LOCODICE = this.oParentObject.w_CODLOT AND LOCODART = this.oParentObject.w_CODART;
        into cursor _Curs_LOTTIART
    endif
    if used('_Curs_LOTTIART')
      select _Curs_LOTTIART
      locate for 1=1
      do while not(eof())
      this.w_TROVATO = .T.
        select _Curs_LOTTIART
        continue
      enddo
      use
    endif
    if NOT this.w_TROVATO
      * --- CALCOLA LOSERIAL
      this.w_LOSERIAL = "0000000000"
      * --- Select from LOTTIART
      i_nConn=i_TableProp[this.LOTTIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select LOCODESE,LOSERIAL  from "+i_cTable+" LOTTIART ";
            +" where LOCODESE = g_CODESE";
            +" order by LOSERIAL";
             ,"_Curs_LOTTIART")
      else
        select LOCODESE,LOSERIAL from (i_cTable);
         where LOCODESE = g_CODESE;
         order by LOSERIAL;
          into cursor _Curs_LOTTIART
      endif
      if used('_Curs_LOTTIART')
        select _Curs_LOTTIART
        locate for 1=1
        do while not(eof())
        this.w_LOSERIAL = LOSERIAL
          select _Curs_LOTTIART
          continue
        enddo
        use
      endif
      this.w_LOSERIAL = RIGHT( "0000000000" + ALLTRIM( STR( VAL( this.w_LOSERIAL ) +1 ) ) , 10 )
      * --- INSERISCE IL LOTTO
      * --- Insert into LOTTIART
      i_nConn=i_TableProp[this.LOTTIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOTTIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LOCODICE"+",LOCODESE"+",LOSERIAL"+",LOCODART"+",LODATCRE"+",LOLOTFOR"+",LOFLSTAT"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'LOTTIART','LOCODICE');
        +","+cp_NullLink(cp_ToStrODBC(g_CODESE),'LOTTIART','LOCODESE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LOSERIAL),'LOTTIART','LOSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'LOTTIART','LOCODART');
        +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'LOTTIART','LODATCRE');
        +","+cp_NullLink(cp_ToStrODBC(SPACE( 20 )),'LOTTIART','LOLOTFOR');
        +","+cp_NullLink(cp_ToStrODBC("D"),'LOTTIART','LOFLSTAT');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOTTIART','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(0),'LOTTIART','UTCV');
        +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'LOTTIART','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'LOTTIART','UTDV');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LOCODICE',this.oParentObject.w_CODLOT,'LOCODESE',g_CODESE,'LOSERIAL',this.w_LOSERIAL,'LOCODART',this.oParentObject.w_CODART,'LODATCRE',i_DATSYS,'LOLOTFOR',SPACE( 20 ),'LOFLSTAT',"D",'UTCC',i_CODUTE,'UTCV',0,'UTDC',SetInfoDate( g_CALUTD ),'UTDV',cp_CharToDate("  -  -    "))
        insert into (i_cTable) (LOCODICE,LOCODESE,LOSERIAL,LOCODART,LODATCRE,LOLOTFOR,LOFLSTAT,UTCC,UTCV,UTDC,UTDV &i_ccchkf. );
           values (;
             this.oParentObject.w_CODLOT;
             ,g_CODESE;
             ,this.w_LOSERIAL;
             ,this.oParentObject.w_CODART;
             ,i_DATSYS;
             ,SPACE( 20 );
             ,"D";
             ,i_CODUTE;
             ,0;
             ,SetInfoDate( g_CALUTD );
             ,cp_CharToDate("  -  -    ");
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_050B0FA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into UNIT_LOG
    i_nConn=i_TableProp[this.UNIT_LOG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIT_LOG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UNIT_LOG_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UL__SSCC"+",ULFLGINT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UNILOG),'UNIT_LOG','UL__SSCC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'UNIT_LOG','ULFLGINT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UL__SSCC',this.oParentObject.w_UNILOG,'ULFLGINT'," ")
      insert into (i_cTable) (UL__SSCC,ULFLGINT &i_ccchkf. );
         values (;
           this.oParentObject.w_UNILOG;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0503E650()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_NRIGA = 1
     
 Select ( this.w_GSAR_MPG.cTrsName ) 
 Go Top
    * --- Scorro il transitorio della movimentazione e per ogni riga con w_STATOROW='S' 
    *     la riporto nella gestione
    Scan For t_STATOROW="S" And Not Deleted()
    ah_Msg("Inserimento dato rilevato %1 ...",.T.,,, Alltrim(Str(this.w_NRIGA)))
    * --- Se vuoto il valore corretto di DRCODMAT � NULL coerentemente all'inserimento rapido dati rilevati
    this.w_LCODMAT = iif(Empty(t_GPCODMAT),NULL,t_GPCODMAT)
    this.w_LUNIMIS = Nvl(t_Gpunimis,space(3))
    this.w_LCODMAG = Nvl(t_Gpcodmag,space(5))
    this.w_LCODUBI = Nvl(t_Gpcodubi,space(20))
    this.w_LCODLOT = Nvl(t_Gpcodlot,space(20))
    this.w_LCODICE = Nvl(t_Gpcodice,space(20))
    this.w_LQTAMOV = nvl(t_GPQTAMOV,0)
    * --- Leggo unit� di misura articolo...
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_LCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP;
        from (i_cTable) where;
            CACODICE = this.w_LCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      this.w_OPERAT3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
      this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUNMIS1,AROPERAT,ARUNMIS2,ARMOLTIP,ARFLUSEP"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_LCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUNMIS1,AROPERAT,ARUNMIS2,ARMOLTIP,ARFLUSEP;
        from (i_cTable) where;
            ARCODART = this.w_LCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
      this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
      this.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Unita' di Misura
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMFLFRAZ,UMMODUM2"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(this.w_UNMIS1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMFLFRAZ,UMMODUM2;
        from (i_cTable) where;
            UMCODICE = this.w_UNMIS1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NOFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_UNMIS1 = LEFT(this.w_UNMIS1+SPACE(3), 3)
    this.w_UNMIS2 = LEFT(this.w_UNMIS2+SPACE(3), 3)
    this.w_UNMIS3 = LEFT(this.w_UNMIS3+SPACE(3), 3)
    this.w_DRCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
    msg=" "
    this.w_QTAES1 = CALQTA(this.w_LQTAMOV,this.w_LUNIMIS,this.w_UNMIS2, this.w_OPERAT,this.w_MOLTIP,this.w_FLUSEP, this.w_NOFRAZ, "N",@msg, this.w_UNMIS3, this.w_OPERAT3, this.w_MOLTI3,,"I")
    if not empty(msg)
      this.w_MESS = ah_MsgFormat("Impossibile confermare%0%1", ah_msgformat(Msg) )
      * --- Raise
      i_Error=this.w_MESS
      return
    endif
    this.w_KEYULO = iif(empty(nvl(this.w_LCODUBI," ")),repl("#",20),this.w_LCODUBI)+iif(empty(nvl(this.w_LCODLOT," ")),repl("#",20),this.w_LCODLOT)
    if g_MATR="S" And this.oParentObject.w_DATREG>=Nvl(g_DATMAT,cp_CharToDate("  /  /    ")) And not empty(this.w_LCODMAT)
      * --- Select from RILEVAZI
      i_nConn=i_TableProp[this.RILEVAZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select DRSERIAL  from "+i_cTable+" RILEVAZI ";
            +" where DRCODRIL="+cp_ToStrODBC(this.oParentObject.w_OBJ_GEST.w_CODRIL)+" And DRCODMAT = "+cp_ToStrODBC(this.w_LCODMAT)+" And DRKEYSAL= "+cp_ToStrODBC(this.w_LCODART)+" And DRCODMAG= "+cp_ToStrODBC(this.oParentObject.w_CODMAG)+" And DRKEYULO="+cp_ToStrODBC(this.w_KEYULO)+"";
             ,"_Curs_RILEVAZI")
      else
        select DRSERIAL from (i_cTable);
         where DRCODRIL=this.oParentObject.w_OBJ_GEST.w_CODRIL And DRCODMAT = this.w_LCODMAT And DRKEYSAL= this.w_LCODART And DRCODMAG= this.oParentObject.w_CODMAG And DRKEYULO=this.w_KEYULO;
          into cursor _Curs_RILEVAZI
      endif
      if used('_Curs_RILEVAZI')
        select _Curs_RILEVAZI
        locate for 1=1
        do while not(eof())
        this.w_NUMSER = NVL(_Curs_RILEVAZI.DRSERIAL,SPACE(15))
        this.w_MESS = ah_MsgFormat("Impossibile confermare%0La matricola %1 � gi� stata rilevata", alltrim(this.w_LCODMAT) )
        * --- Raise
        i_Error=this.w_MESS
        return
          select _Curs_RILEVAZI
          continue
        enddo
        use
      endif
    endif
    this.w_DRSERIAL = Space(15)
    this.w_DRSERIAL = cp_GetProg("RILEVAZI","RILSER",this.w_DRSERIAL,i_CODAZI)
    this.w_NUMDOC = this.w_NUMDOC + 1
    * --- Insert into RILEVAZI
    i_nConn=i_TableProp[this.RILEVAZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RILEVAZI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DRSERIAL"+",DRCODRIL"+",DRCODMAG"+",DRNUMDOC"+",DRADDRIL"+",DRCODRIC"+",DRCODART"+",DRCODVAR"+",DRKEYSAL"+",DRCODUTE"+",DRUBICAZ"+",DR_LOTTO"+",DRKEYULO"+",DRUNIMIS"+",DRQTAESI"+",DRQTAES1"+",DRCONFER"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",DRCODMAT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DRSERIAL),'RILEVAZI','DRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OBJ_GEST.w_CODRIL),'RILEVAZI','DRCODRIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODMAG),'RILEVAZI','DRCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'RILEVAZI','DRNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OBJ_GEST.w_CODUTE),'RILEVAZI','DRADDRIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODICE),'RILEVAZI','DRCODRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODART),'RILEVAZI','DRCODART');
      +","+cp_NullLink(cp_ToStrODBC(Space(20)),'RILEVAZI','DRCODVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODART),'RILEVAZI','DRKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODUTE),'RILEVAZI','DRCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODUBI),'RILEVAZI','DRUBICAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODLOT),'RILEVAZI','DR_LOTTO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KEYULO),'RILEVAZI','DRKEYULO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LUNIMIS),'RILEVAZI','DRUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LQTAMOV),'RILEVAZI','DRQTAESI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAES1),'RILEVAZI','DRQTAES1');
      +","+cp_NullLink(cp_ToStrODBC(" "),'RILEVAZI','DRCONFER');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'RILEVAZI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'RILEVAZI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'RILEVAZI','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODMAT),'RILEVAZI','DRCODMAT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_DRSERIAL,'DRCODRIL',this.oParentObject.w_OBJ_GEST.w_CODRIL,'DRCODMAG',this.w_LCODMAG,'DRNUMDOC',this.w_NUMDOC,'DRADDRIL',this.oParentObject.w_OBJ_GEST.w_CODUTE,'DRCODRIC',this.w_LCODICE,'DRCODART',this.w_LCODART,'DRCODVAR',Space(20),'DRKEYSAL',this.w_LCODART,'DRCODUTE',this.w_DRCODUTE,'DRUBICAZ',this.w_LCODUBI,'DR_LOTTO',this.w_LCODLOT)
      insert into (i_cTable) (DRSERIAL,DRCODRIL,DRCODMAG,DRNUMDOC,DRADDRIL,DRCODRIC,DRCODART,DRCODVAR,DRKEYSAL,DRCODUTE,DRUBICAZ,DR_LOTTO,DRKEYULO,DRUNIMIS,DRQTAESI,DRQTAES1,DRCONFER,UTCC,UTCV,UTDC,UTDV,DRCODMAT &i_ccchkf. );
         values (;
           this.w_DRSERIAL;
           ,this.oParentObject.w_OBJ_GEST.w_CODRIL;
           ,this.w_LCODMAG;
           ,this.w_NUMDOC;
           ,this.oParentObject.w_OBJ_GEST.w_CODUTE;
           ,this.w_LCODICE;
           ,this.w_LCODART;
           ,Space(20);
           ,this.w_LCODART;
           ,this.w_DRCODUTE;
           ,this.w_LCODUBI;
           ,this.w_LCODLOT;
           ,this.w_KEYULO;
           ,this.w_LUNIMIS;
           ,this.w_LQTAMOV;
           ,this.w_QTAES1;
           ," ";
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,this.w_LCODMAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_NRIGA = this.w_NRIGA + 1
     
 Select ( this.w_GSAR_MPG.cTrsName ) 
 EndScan
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Caricamento svolto per %1 dati rilevati",,"", Alltrim(Str(this.w_NRIGA-1)))
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da decidere se corretto utilizzare un file CNF sul client
    this.w_CODDISP = g_CODPEN
    if Empty( this.w_CODDISP ) 
      if g_MATR<>"S"
        ah_ErrorMsg("Dispositivo non definito sul client, utilizzare l'apposita funzione per definirne uno",,"")
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Read from DIS_HARD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_HARD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_HARD_idx,2],.t.,this.DIS_HARD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DHPROGRA,DHPROPAR,DHLOTMAT"+;
        " from "+i_cTable+" DIS_HARD where ";
            +"DHCODICE = "+cp_ToStrODBC(this.w_CODDISP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DHPROGRA,DHPROPAR,DHLOTMAT;
        from (i_cTable) where;
            DHCODICE = this.w_CODDISP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PROGRA = NVL(cp_ToDate(_read_.DHPROGRA),cp_NullValue(_read_.DHPROGRA))
      this.w_PROPAR = NVL(cp_ToDate(_read_.DHPROPAR),cp_NullValue(_read_.DHPROPAR))
      this.w_DHLOTMAT = NVL(cp_ToDate(_read_.DHLOTMAT),cp_NullValue(_read_.DHLOTMAT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_DHLOTMAT = NVL( this.w_DHLOTMAT , "N" )
    this.w_PROGRA = ALLTRIM(this.w_PROGRA)
    this.w_PROPAR = ALLTRIM(this.w_PROPAR)
    if i_ROWS=0 
      ah_ErrorMsg("Il dispositivo definito sul client non � presente tra i dispositivi [%1]",,"",this.w_CODDISP)
      i_retcode = 'stop'
      return
    endif
    if Empty( this.w_PROGRA )
      ah_ErrorMsg("Il dispositivo definito sul client non ha un programma associato [%1]",,"",this.w_CODDISP)
      i_retcode = 'stop'
      return
    endif
    if Not File( Alltrim( this.w_PROGRA )+".fxp" )
      ah_ErrorMsg("Il dispositivo definito sul client [%1] ha un programma associato [%2] inesistente",,"", this.w_CODDISP, this.w_PROGRA)
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza campo ripetuto
    *     =======================================================
    * --- Se .t. prende i dati dal cursore arrivato dalla penna altrimenti
    *     dal temporaneo della movimentazione contenente i dati della penna
    *     Cambia anche il padre dal quale cerco il control
    * --- Valorizzare a .T. se si desidera by passare la condizione di editabilit�
    *     di un campo (Es. Codice magazzino)
    if not Empty( this.w_NOMEVAR )
      if this.w_TIPOCUR
        this.w_PADRE = this.w_GSAR_MPG
      else
        this.w_PADRE = this.oParentObject.w_OBJ_GEST
      endif
      if this.w_TIPO="FIXED"
        this.w_OBJCTRL = this.w_PADRE.GetCtrl( this.w_NOMEVAR )
      else
        this.w_OBJCTRL = this.w_PADRE.GetBodyCtrl( this.w_NOMEVAR )
      endif
      if this.w_COND Or this.w_OBJCTRL.mCond()
        * --- Disabilito gli avvisi delle valid...
        *     Questo asssegnamento � necesario in quanto sotto alcune condizioni
        *     scatta il msg campo obbligatorio sul magazzino x articoli forfettari...
        *     (non serve re impostarlo messo a .f. al termine della Valid..)
        this.oParentObject.w_OBJ_GEST.bDontReportError = .T.
        * --- Valorizzo il Control..
        *     Se il controllo ha un link utilizzo la EcpDrop per evitare di eseguire il link
        *     con query  sull'archivio filtrata tramite operatore Like (Problema dei codice con _ o % all'interno) . 
        *     
        *     Se non ha un Link utilizzo 
        *     La Valid (il check non interroga il database)
        if Not Empty( this.w_OBJCTRL.cLinkFile )
          * --- Valorizzo l'array da passare al metodo (contiene l'elenco dei
          *     valori che compongono la chiave primaria)
          this.w_TmpS = Alltrim(this.w_NOMEFIELD)
          this.w_INDEX = 1
          this.w_OSOURCE.xKey( this.w_INDEX ) = .F.
          do while At(";" , this.w_TmpS )>0 And this.w_INDEX<=15
             
 L_NomeField = SubStr ( this.w_TmpS , 1, At(";" , this.w_Tmps )-1 )
            if this.w_TIPOCUR
               
 Select ( this.oParentObject.w_CURSORE )
            else
               
 Select ( L_TEMPORANEO )
            endif
            this.w_OSOURCE.xKey( this.w_INDEX ) = &L_NOMEFIELD
            this.w_TmpS = SubStr ( this.w_TmpS , At(";" , this.w_Tmps )+1 , Len( this.w_Tmps ) )
            this.w_INDEX = this.w_INDEX + 1
          enddo
           
 L_NomeField = this.w_TmpS
          if this.w_TIPOCUR
             
 Select ( this.oParentObject.w_CURSORE )
          else
             
 Select ( L_TEMPORANEO )
          endif
          this.w_OSOURCE.xKey( this.w_INDEX ) = &L_NOMEFIELD
          this.w_NUMMARK = this.oParentObject.w_OBJ_GEST.nMarkpos
          this.oParentObject.w_OBJ_GEST.nMarkpos = 0
          this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
          this.oParentObject.w_OBJ_GEST.nMarkpos = this.w_NUMMARK
        else
           
 Local L_NOMEFIELD 
 L_NOMEFIELD= this.w_NOMEFIELD
          if this.w_TIPOCUR
             
 Select ( this.oParentObject.w_CURSORE )
          else
             
 Select ( L_TEMPORANEO )
          endif
          this.w_OBJCTRL.Value = &L_NOMEFIELD
          * --- Considero il control sempre modificato per gestire i valori da proporre
          *     (se da penna imposto un valore uguale a quello da proporre la procedura 
          *     non fa scattare vari eventi..)
          this.w_OBJCTRL.bUpd = .t.
          * --- Valid sul campo, esegue Check, NotifyEvent Changed, mCalc e SaveDependsOn
          this.w_OBJCTRL.Valid()     
        endif
      endif
      this.w_COND = .F.
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza il transitorio di GSAR_MPG con informazioni per gestione evasione
    *     (eventualmente duplica righe transitorio)
    select * from GSVEDKIM order by MVDATEVA, MVALFDOC,MVNUMDOC, CPROWORD into cursor GSVEDKIM NoFilter
    =WrCursor("GSVEDKIM")
    SELECT "GSVEDKIM"
    this.w_GSAR_MPG.MarkPos()     
    * --- Elimino eventuali righe aggiunte con precedenti importazioni
     
 select( this.w_GSAR_MPG.cTrsName ) 
 Go Top 
 DELETE FROM ( this.w_GSAR_MPG.cTrsName ) WHERE t_ROWAGG="U" OR EMPTY(Nvl(t_GPCODICE,Space(20)))
    * --- Ripristino situazione a prima di eventuali precedenti importazioni per le righe
    *     gi� importate
     
 select( this.w_GSAR_MPG.cTrsName ) 
 Go Top 
 REPLACE t_GPEVASIO WITH 0,t_GPQTAMOV WITH IIF(t_QTAMOV=0,t_OLDQTA,t_QTAMOV),t_GPPREZZO WITH t_OLDPRE,; 
 t_GPSERRIF WITH SPACE(10),t_GPROWRIF WITH 0,t_EVDATDOC WITH cp_CharToDate("    -  -  "),t_EVNUMDOC WITH 0,t_EVTIPDOC WITH SPACE(5),t_EVALFDOC WITH Space(10),t_EVROWORD WITH 0,t_QTAORI WITH 0,t_DOQTAEV1 WITH 0; 
 ,t_DOIMPEVA WITH 0, t_FLRRIF WITH " " ,t_PARAME WITH "   " FOR NOT EMPTY(NVL(t_GPSERRIF,SPACE(10)))
    this.w_GSAR_MPG.TrsFromWork()     
    * --- "Per ogni riga nel dettaglio del caricamento rapido a cui si voglia associare una riga
    *     di un documento di origine, si cerca una tra le righe dei documenti di origine selezionate,
    *     che abbia i seguenti campi uguali alla riga del caricamento rapido:
    *     articolo, unit� di misura, matricola, lotto, ubicazione, magazzino.
    *     La ricerca viene ripetuta fino a che non si trova una riga nei documenti di origine,
    *     modificando ogni volta il criterio di ricerca, ovvero rinunciando in
    *     ogni ulteriore ricerca alla corrispondenza di un campo.
    *     Se la prima ricerca fallisce, si esegue una seconda ricerca per
    *     articolo, unit� di misura, lotto, ubicazione, magazzino.
    *     Se fallisce la seconda ricerca, si esegue una terza ricerca per
    *     articolo, unit� di misura, ubicazione, magazzino.
    *     In caso di ulteriore fallimento, si prosegue con 
    *     articolo, unit� di misura, magazzino.
    *     Se non � ancora stata trovata una riga da evadere, si esegue la ricerca per
    *     articolo, unit� di misura.
    *     Se anche in questo caso non si trova una riga di origine, la riga del caricamento
    *     rapido non viene evasa."
    * --- Ciclo sul cursore e valorizzo sul transitorio i dati relativi all'evasione
    SCAN FOR Nvl(t_GPEVASIO," ")<>1 AND Nvl(t_GP_ESCLU," ")<>1
    this.w_CACODICE = t_GPCODICE
    this.w_UNMISURA = t_GPUNIMIS
    this.w_LCODART = t_CODART
    this.w_LCODLOT = NVL(t_GPCODLOT,SPACE(20))
    this.w_LCODUBI = NVL(t_GPCODUBI,SPACE(20))
    this.w_LCODMAT = NVL(t_GPCODMAT,SPACE(40))
    this.w_LCODMAG = NVL(t_GPCODMAG,SPACE(5))
    this.w_NOMAG = .F.
    * --- Utilizzata per marcare documenti evasi per non essere riselezionati nella scan
    this.w_NODOCU = NVL(t_NODOCU," ")
    L_OldArea = Select()
    * --- --Cerca nel documento di origine per articolo e unit� di misura
    this.w_OKDOC = .F.
    SELECT GSVEDKIM
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Not this.w_OKDOC
      * --- Cerco riga documento con matricola non valorizzata
      this.w_LCODMAT = SPACE(40)
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not this.w_OKDOC
      * --- Cerco riga documento con matricola e lotto non valorizzati
      this.w_LCODMAT = SPACE(40)
      this.w_LCODLOT = SPACE(20)
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not this.w_OKDOC
      * --- Cerco riga documento con matricola , lotto e ubicazione non valorizzati
      this.w_LCODMAT = SPACE(40)
      this.w_LCODLOT = SPACE(20)
      this.w_LCODUBI = SPACE(20)
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not this.w_OKDOC
      * --- Cerco riga documento con matricola , lotto e ubicazione non valorizzati
      *     senza filtrare sul magazzino (Scartando cmq righe origine che movimentano
      *     il riservato )
      this.w_LCODMAT = SPACE(40)
      this.w_LCODLOT = SPACE(20)
      this.w_LCODUBI = SPACE(20)
      this.w_NOMAG = .T.
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    Select(L_OldArea)
    ENDSCAN
    this.w_GSAR_MPG.RePos(.F.)     
    USE IN SELECT("GSVEDKIM")
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza il dettaglio matricole sulla gestione sottostante (dalla quale � lanciato il caricamento rapido)
    *     E' gia posizionato sulla riga nella quale inserire la matricola
     
 Select ( L_TEMPORANEO )
    if g_MATR="S" And Not Empty( Nvl(cMatr ,"") ) And this.oParentObject.w_DATREG>=nvl(g_DATMAT,cp_CharToDate("  /  /    ")) And NVL(t_GPQTAMOV,0)<>0 And (this.oParentObject.w_FLCASC $ "+-" OR this.oParentObject.w_FLRISE $ "+-" ) 
      ah_Msg("Inserimento matricole riga: %1",.T.,,, ALLTRIM(STR(this.w_NRIGA)))
      GSAR_BDM(this, Nvl(t_Gpcodice,space(20)) , Nvl(t_Gpcodlot,space(20)), Nvl(t_Gpcodubi,space(20)), Nvl(t_Gpcodmag,space(5)) ,Nvl(t_Gpprezzo,0),Nvl(t_Gpunimis,space(3)), Space(10) , 0 , Nvl(t_GpCodCen,space(15)), Nvl(t_GpCodCom,space(15)) , Nvl(t_GpCodAtt,space(15)) ,Nvl(t_GpCodMa2,space(5)) ,Nvl(t_GpCodUb2,space(20)) ,Nvl(t_GpUniLog,space(18)) ,this.oParentObject.w_OBJ_GEST, this.w_GSAR_MPG.cTrsName )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerco una riga sul transitorio con le medesime caratteristiche della riga da evadere
    *     quindi
    *     Articolo
    *     Unit� di misura
    *     Lotto
    *     Ubicazione
    *     Matricola
    *     Magazzino
    * --- NODOCU � sul transitorio ed  � un campo di sistema per capire se la riga � stata gia utilizzata
    *     all'interno della stessa importazione. Quindi evita che la stessa riga evada due righe
    *     d'origine
    SELECT GSVEDKIM
    GO TOP
    LOCATE FOR ; 
 MVCODART = this.w_LCODART AND MVUNIMIS = this.w_UNMISURA AND NVL(MVCODLOT,SPACE(20)) = this.w_LCODLOT AND; 
 NVL(MVCODUBI,SPACE(20)) = this.w_LCODUBI AND ( NVL(MVCODMAT,SPACE(40)) = this.w_LCODMAT OR EMPTY( this.w_LCODMAT ) ) AND; 
 ( NVL(MVCODMAG,SPACE(5)) = this.w_LCODMAG OR ( this.w_NOMAG And Empty( NVL(MVFLRISE," ") ) )) AND NVL(NODOCU," ") = this.w_NODOCU
    if FOUND()
      this.w_OKDOC = .T.
      this.w_QTADAEVA = IIF(Not empty(this.w_LCODMAT) OR G_MATR<>"S",MVQTAMOV,QTAMOV) - MVQTAEVA 
      this.w_SERIALOR = MVSERIAL
      this.w_ROWORIG = CPROWNUM
      this.w_PREZZOORI = cp_Round(VISPRE,g_PERPUL)
      this.w_LTIPDOC = NVL(MVTIPDOC,SPACE(5))
      this.w_LALFDOC = NVL(MVALFDOC, Space(10))
      this.w_LDATDOC = CP_TODATE(MVDATDOC)
      this.w_LNUMDOC = NVL(MVNUMDOC,0)
      this.w_LROWORD = NVL(CPROWORD,0)
      this.w_LDOQTAEV1 = NVL(DOQTAEV1,0)
      this.w_LDOIMPEVA = NVL(DOIMPEVA,0)
      this.w_LFLRRIF = NVL(MVFLRISE," ")
      this.w_PARAME = NVL(MVFLVEAC," ") +NVL(MVCLADOC,"  ") 
      this.w_ARGESMAT = Nvl(ARGESMAT," ")
      this.w_CODCOM = NVL(MVCODCOM,Space(15))
      this.w_CODATT = NVL(MVCODATT,Space(15))
      this.w_CODCEN = NVL(MVCODCEN,Space(15))
      this.w_FLGROR = Nvl( FLGROR , " " )
      this.w_SCONT1 = Nvl ( MVSCONT1 , 0 )
      this.w_SCONT2 = Nvl ( MVSCONT2 , 0 )
      this.w_SCONT3 = Nvl ( MVSCONT3 , 0 )
      this.w_SCONT4 = Nvl ( MVSCONT4 , 0 )
      this.w_GPFLDOCU = NVL( FLDOCU , " " )
      this.w_GPFLINTE = NVL( FLINTE , " " )
      this.w_GPFLACCO = NVL( FLACCO , " " )
      this.w_MVCODMAT = NVL( MVCODMAT, SPACE( 40 ) )
      Select( this.w_GSAR_MPG.cTrsName )
      * --- Valorizzo i dati per evadere quindi riferimenti ed informazioni relative
      *     al documento evaso
      REPLACE T_GPSERRIF WITH this.w_SERIALOR
      REPLACE T_GPROWRIF WITH this.w_ROWORIG
      REPLACE T_GPFLDOCU WITH this.w_GPFLDOCU
      REPLACE T_GPFLINTE WITH this.w_GPFLINTE
      REPLACE T_GPFLACCO WITH this.w_GPFLACCO
      * --- Memorizzo il prezzo originario di riga, mi servir� al momento della duplicazione
      *     della riga..
      REPLACE T_OLDPRE WITH t_GPPREZZO
      * --- Se casuale documento sottostante ha attivo ricalcolo prezzi 
      *     non imposto il prezzo di riga con il prezzo del documento
      *     di origine..
      if this.oParentObject.w_OBJ_GEST.w_PRZDES<>"S"
        if T_OLDPRE = 0
          REPLACE T_GPPREZZO WITH this.w_PREZZOORI
        endif
      else
        * --- Se attivo il check ricalcolo prezzi azzero il prezzo se riga che importa..
        REPLACE T_GPPREZZO WITH 0
      endif
      REPLACE T_EVDATDOC WITH this.w_LDATDOC
      REPLACE T_EVNUMDOC WITH this.w_LNUMDOC
      REPLACE T_EVALFDOC WITH this.w_LALFDOC
      REPLACE T_EVTIPDOC WITH this.w_LTIPDOC
      REPLACE t_QTAORI WITH this.w_QTADAEVA
      REPLACE T_EVROWORD WITH this.w_LROWORD
      REPLACE t_DOQTAEV1 WITH this.w_LDOQTAEV1
      REPLACE t_DOIMPEVA WITH this.w_LDOIMPEVA
      REPLACE t_FLRRIF WITH this.w_LFLRRIF
      REPLACE t_PARAME WITH this.w_PARAME
      REPLACE T_FLGROR WITH this.w_FLGROR
      REPLACE T_SCONT1 WITH this.w_SCONT1
      REPLACE T_SCONT2 WITH this.w_SCONT2
      REPLACE T_SCONT3 WITH this.w_SCONT3
      REPLACE T_SCONT4 WITH this.w_SCONT4
      if g_PERCCR="S" AND this.oParentObject.w_FLANAL="S" And Not this.oParentObject.w_TIPOOBJ_GEST$ "P-M" and Empty(Nvl(t_GPCODCEN," "))
        REPLACE t_GPCODCEN WITH this.w_CODCEN
      endif
      if ((g_PERCAN="S" AND this.oParentObject.w_FLANAL="S") OR (g_COMM="S" AND this.oParentObject.w_FLGCOM="S")) And Not this.oParentObject.w_TIPOOBJ_GEST$ "P-M" and Empty(Nvl(t_GPCODCOM," "))
        REPLACE t_GPCODCOM WITH this.w_CODCOM
      endif
      if NOT EMPTY(Nvl(t_GPCODCOM,Space(15))) AND g_COMM="S" AND this.oParentObject.w_FLGCOM="S" and Empty(Nvl(t_GPCODATT," "))
        REPLACE t_GPCODATT WITH this.w_CODATT
      endif
      * --- A seconda della qt� da evadere e la qta di riga valorizzo T_GPEVASIO (Flag Evasione)
      if this.w_ARGESMAT="S" AND NOT EMPTY( this.w_LCODMAT )
        * --- Scalo da tutte le righe matricole uno, appena arrivo a zero allora la
        *     riga di origine � da condierarsi chiusa...
        *     
        *     La replace for  sposta il puntatore alla fine del cursore, mi riposiziono 
        *     al record attuale..
        this.w_POS = Recno( "GSVEDKIM" )
        SELECT GSVEDKIM
         
 REPLACE QTAUM1 WITH Nvl( QTAUM1 , 0 ) - 1 For MVSERIAL = this.w_SERIALOR And CPROWNUM= this.w_ROWORIG 
 Go this.w_POS
        if Nvl( GSVEDKIM.QTAUM1 , 0 )=0
          * --- Riga chiusa la marco per scartarla dalla prossima valutazione
          REPLACE NODOCU WITH "N"
          Select( this.w_GSAR_MPG.cTrsName )
          REPLACE t_GPEVASIO WITH 1
        endif
      else
        this.w_TQTAMOV = Nvl( t_GPQTAMOV , 0 )
        do case
          case t_GPQTAMOV < this.w_QTADAEVA
            REPLACE t_GPEVASIO WITH 0
          case t_GPQTAMOV = this.w_QTADAEVA
            REPLACE t_GPEVASIO WITH 1
          case t_GPQTAMOV > this.w_QTADAEVA
            REPLACE T_GPEVASIO WITH 1
            this.w_QTADAEVA = t_GPQTAMOV - this.w_QTADAEVA
            REPLACE T_GPQTAMOV WITH T_GPQTAMOV - this.w_QTADAEVA
            * --- Marco la posizione della riga per riposionarmici dopo la duplicazione
            this.w_ROWTEMP = RECNO(this.w_GSAR_MPG.cTrsName )
            * --- Valorizzo le variabili di Work della riga da "duplicare"
            this.w_GSAR_MPG.WorkFromTrs()     
            * --- --Crea nuova riga con la differenza
            Append Blank
            * --- Scrivo nella nuova riga le variabili di Work della riga "duplicata"
            *     Questo per evitare di scrivere tutte le righe del temporaneo
            this.w_GSAR_MPG.TrsFromWork()     
            * --- Scrivo i dati diversi rispetto alla riga originaria (dati dell'evasione bianchi)
            REPLACE T_GPQTAMOV WITH this.w_QTADAEVA
            REPLACE T_GPEVASIO WITH 0
            REPLACE T_CPROWORD WITH RECNO()*10
            REPLACE T_ROWAGG WITH "U"
            REPLACE T_EVDATDOC WITH cp_CharToDate("    -  -  ")
            REPLACE T_EVNUMDOC WITH 0
            REPLACE T_EVALFDOC WITH SPACE(2)
            REPLACE T_EVTIPDOC WITH SPACE(5)
            REPLACE T_EVROWORD WITH 0
            REPLACE T_GPSERRIF WITH SPACE(10)
            REPLACE T_GPROWRIF WITH 0
            REPLACE CPROWNUM WITH RECNO()+1
            * --- Valorizzo GPPREZZO con OLD_PRE della riga origine della duplicazione
            REPLACE t_GPPREZZO WITH T_OLDPRE
            REPLACE I_SRV WITH "A"
            this.w_GSAR_MPG.WorkFromTrs()     
            this.w_GSAR_MPG.State_Row()     
            REPLACE t_STATOROW WITH this.oParentObject.w_STATOROW
            REPLACE t_CAPTION WITH this.oParentObject.w_CAPTION
            GO this.w_ROWTEMP
        endcase
        SELECT GSVEDKIM
        * --- La qt� evasa della riga � cambiata..
        REPLACE MVQTAEVA WITH Nvl( MVQTAEVA , 0 ) + this.w_TQTAMOV
        if MVQTAEVA>=IIF(Not empty(this.w_LCODMAT) OR G_MATR<>"S",MVQTAMOV,QTAMOV)
          * --- Riga chiusa la marco per scartarla dalla prossima valutazione
          REPLACE NODOCU WITH "N" For MVSERIAL = this.w_SERIALOR And CPROWNUM= this.w_ROWORIG 
        endif
      endif
      this.w_GSAR_MPG.WorkFromTrs()     
      this.w_GSAR_MPG.State_Row()     
      REPLACE t_STATOROW WITH this.oParentObject.w_STATOROW
      REPLACE t_CAPTION WITH this.oParentObject.w_CAPTION
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue per la maschera GSAR_MPG la valorizzazione ed il link per i campi
    *     con link.
    *     
    *     Reso indisepensabile per poter utilzizare la Link Full
    *     Restituisce in w_CHECK .t. se tutto ok
    this.w_OBJCTRL = this.w_GSAR_MPG.GetBodyCtrl( this.w_NOMEOBJ )
    if IsNull( this.w_OBJCTRL )
      this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( this.w_NOMEOBJ )
    endif
    * --- Eseguo la AfterInput in modo tale da far scattare il controllo sui codici con peso/prezzo variabile
    if this.w_NOMEOBJ = "w_GPCODICE" 
      this.w_OBJCTRL.Value = this.oParentObject.w_GPCODICE
      this.w_OBJCTRL.mAfter()     
    endif
    L_Method_Name="this.oParentobject.Link"+Right( this.w_OBJCTRL.Name , LEN( this.w_OBJCTRL.Name ) - rat("_" , this.w_OBJCTRL.Name , 2)+1)+"('Full',this)"
    this.w_CHECK = &L_Method_Name
    if this.w_CHECK
      this.w_GSAR_MPG.mCalc(.t.)     
      this.w_GSAR_MPG.SaveDependsOn()     
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- In questo caso occorre inserire magazzino e ubicazione per inserire la matricola
    *     Per il magazino occorre prima l'articolo
    * --- � attivata l'univocit� per matricola
    *     Calcola l'articolo dalla matricola
    * --- Read from MATRICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MATRICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2],.t.,this.MATRICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AMKEYSAL"+;
        " from "+i_cTable+" MATRICOL where ";
            +"AMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODMAT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AMKEYSAL;
        from (i_cTable) where;
            AMCODICE = this.oParentObject.w_CODMAT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_GPCODICE = NVL(cp_ToDate(_read_.AMKEYSAL),cp_NullValue(_read_.AMKEYSAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Viene momentaneamente assegnato il codice di ricerca principale perch� � attivata l'univocit� per matricola
     
 Select ( this.oParentObject.w_CURSORE )
    this.w_NOMEOBJ = "w_GPCODICE" 
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se articolo vuoto significa che ha fallito il link...
    if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
      AddMsgNL("Riga %1 articolo %2 non valido", this, Alltrim(Str( this.oParentObject.w_CPROWORD )), Alltrim(CODICE))
      this.oParentObject.w_STATOROW = "A"
    else
      * --- La riga per ora � Ok...
      this.oParentObject.w_STATOROW = "S"
    endif
     
 Select ( this.oParentObject.w_CURSORE )
    * --- Gestione Magazzino
    this.oParentObject.w_CODMAG = Nvl( CODMAG ,Space(5) )
    * --- Se magazzino valorizzato tento di inserirlo..
    if Not Empty( this.oParentObject.w_CODMAG )
      this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPCODMAG" )
      if this.w_OBJCTRL.mCond()
        this.oParentObject.w_GPCODMAG = this.oParentObject.w_CODMAG
        this.w_NOMEOBJ = "w_GPCODMAG"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
          AddMsgNL("Riga %1 magazzino non valido", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
          this.oParentObject.w_STATOROW = "D"
        endif
      endif
    endif
    * --- Gestione Ubicazione
     
 Select ( this.oParentObject.w_CURSORE )
    this.oParentObject.w_CODUBI = Nvl( UBICAZIONE ,Space(20) )
    this.w_OBJCTRL = this.w_GSAR_MPG.GetCtrl( "w_GPCODUBI" )
    if this.w_OBJCTRL.mCond()
      this.oParentObject.w_GPCODUBI = this.oParentObject.w_CODUBI
      this.w_NOMEOBJ = "w_GPCODUBI"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if Not this.w_CHECK And this.oParentObject.w_STATOROW="S"
        AddMsgNL("Riga %1 ubicazione non valida", this, Alltrim(Str( this.oParentObject.w_CPROWORD )))
        this.oParentObject.w_STATOROW = "U"
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,14)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='DIS_HARD'
    this.cWorkTables[4]='DOC_DETT'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='MATRICOL'
    this.cWorkTables[7]='MOVIMATR'
    this.cWorkTables[8]='*TMP_MAST'
    this.cWorkTables[9]='RILEVAZI'
    this.cWorkTables[10]='TIPICONF'
    this.cWorkTables[11]='UNIT_LOG'
    this.cWorkTables[12]='LOTTIART'
    this.cWorkTables[13]='CMT_MAST'
    this.cWorkTables[14]='UNIMIS'
    return(this.OpenAllTables(14))

  proc CloseCursors()
    if used('_Curs_MATRICOL')
      use in _Curs_MATRICOL
    endif
    if used('_Curs_MATRICOL')
      use in _Curs_MATRICOL
    endif
    if used('_Curs_MATRICOL')
      use in _Curs_MATRICOL
    endif
    if used('_Curs_CMT_MAST')
      use in _Curs_CMT_MAST
    endif
    if used('_Curs_MATRICOL')
      use in _Curs_MATRICOL
    endif
    if used('_Curs_LOTTIART')
      use in _Curs_LOTTIART
    endif
    if used('_Curs_LOTTIART')
      use in _Curs_LOTTIART
    endif
    if used('_Curs_gsar_bpg')
      use in _Curs_gsar_bpg
    endif
    if used('_Curs_RILEVAZI')
      use in _Curs_RILEVAZI
    endif
    if used('_Curs_RILEVAZI')
      use in _Curs_RILEVAZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
