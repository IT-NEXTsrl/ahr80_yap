* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_ksl                                                        *
*              Visualizza saldi per lotto                                      *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-17                                                      *
* Last revis.: 2014-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_ksl",oParentObject))

* --- Class definition
define class tgsma_ksl as StdForm
  Top    = 5
  Left   = 22

  * --- Standard Properties
  Width  = 748
  Height = 531
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-31"
  HelpContextID=169182359
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=38

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  LOTTIART_IDX = 0
  DET_VARI_IDX = 0
  MAGAZZIN_IDX = 0
  UBICAZIO_IDX = 0
  CONTI_IDX = 0
  CAN_TIER_IDX = 0
  cPrg = "gsma_ksl"
  cComment = "Visualizza saldi per lotto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODART = space(20)
  o_CODART = space(20)
  w_CODART = space(20)
  w_DESART = space(40)
  w_VARIAN = space(5)
  o_VARIAN = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_FLOTT = space(1)
  w_UNIMIS = space(3)
  w_CODVAR = space(20)
  o_CODVAR = space(20)
  w_DESVAR = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_LOTTOART = space(20)
  o_LOTTOART = space(20)
  w_LOTTO = space(20)
  w_CODICE = space(20)
  w_VARIANT = space(20)
  w_CODMAG = space(5)
  o_CODMAG = space(5)
  w_CODUBI = space(20)
  w_LOTTOZOOM = space(20)
  o_LOTTOZOOM = space(20)
  w_DATCRE = ctod('  /  /  ')
  w_FLARSTO = space(1)
  w_DATSCA = ctod('  /  /  ')
  w_RIFFOR = space(20)
  w_STATO = space(12)
  w_FLUBIC = space(1)
  w_KEYSAL = space(10)
  w_OKSCA = space(1)
  w_LOTTOART = space(20)
  w_CODUBI = space(20)
  w_NOTE = space(0)
  w_CODCON = space(15)
  w_DESCON = space(40)
  w_NUMCOM = space(20)
  w_DESCOM = space(40)
  w_DATREG = ctod('  /  /  ')
  w_DISLOT = space(1)
  w_TIPCON = space(1)
  w_UBIZOOM = space(20)
  w_MAGZOOM = space(5)
  w_CODART2 = space(10)
  w_ZoomSlo = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kslPag1","gsma_ksl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODART_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSlo = this.oPgFrm.Pages(1).oPag.ZoomSlo
    DoDefault()
    proc Destroy()
      this.w_ZoomSlo = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='DET_VARI'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='UBICAZIO'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='CAN_TIER'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODART=space(20)
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_VARIAN=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLOTT=space(1)
      .w_UNIMIS=space(3)
      .w_CODVAR=space(20)
      .w_DESVAR=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_LOTTOART=space(20)
      .w_LOTTO=space(20)
      .w_CODICE=space(20)
      .w_VARIANT=space(20)
      .w_CODMAG=space(5)
      .w_CODUBI=space(20)
      .w_LOTTOZOOM=space(20)
      .w_DATCRE=ctod("  /  /  ")
      .w_FLARSTO=space(1)
      .w_DATSCA=ctod("  /  /  ")
      .w_RIFFOR=space(20)
      .w_STATO=space(12)
      .w_FLUBIC=space(1)
      .w_KEYSAL=space(10)
      .w_OKSCA=space(1)
      .w_LOTTOART=space(20)
      .w_CODUBI=space(20)
      .w_NOTE=space(0)
      .w_CODCON=space(15)
      .w_DESCON=space(40)
      .w_NUMCOM=space(20)
      .w_DESCOM=space(40)
      .w_DATREG=ctod("  /  /  ")
      .w_DISLOT=space(1)
      .w_TIPCON=space(1)
      .w_UBIZOOM=space(20)
      .w_MAGZOOM=space(5)
      .w_CODART2=space(10)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODART))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODART))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,7,.f.)
        .w_CODVAR = SPACE(20)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODVAR))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_OBTEST = i_INIDAT
        .w_LOTTOART = SPACE(20)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_LOTTOART))
          .link_1_14('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_LOTTO))
          .link_1_15('Full')
        endif
        .DoRTCalc(13,15,.f.)
        if not(empty(.w_CODMAG))
          .link_1_18('Full')
        endif
        .w_CODUBI = Space(5)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODUBI))
          .link_1_19('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomSlo.Calculate()
        .w_LOTTOZOOM = .w_ZoomSlo.GetVar("MLCODLOT")
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_LOTTOZOOM))
          .link_1_21('Full')
        endif
          .DoRTCalc(18,18,.f.)
        .w_FLARSTO = 'N'
          .DoRTCalc(20,21,.f.)
        .w_STATO = IIF(EMPTY(.w_LOTTOART),'D',.w_STATO)
          .DoRTCalc(23,23,.f.)
        .w_KEYSAL = .w_CODART+IIF(EMPTY(.w_CODVAR), REPL('#',20), .w_CODVAR)
        .w_OKSCA = 'T'
        .w_LOTTOART = Space(20)
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_LOTTOART))
          .link_1_38('Full')
        endif
        .w_CODUBI = Space(5)
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CODUBI))
          .link_1_39('Full')
        endif
        .DoRTCalc(28,29,.f.)
        if not(empty(.w_CODCON))
          .link_1_42('Full')
        endif
        .DoRTCalc(30,31,.f.)
        if not(empty(.w_NUMCOM))
          .link_1_45('Full')
        endif
          .DoRTCalc(32,32,.f.)
        .w_DATREG = CTOD("")
          .DoRTCalc(34,34,.f.)
        .w_TIPCON = 'F'
        .w_UBIZOOM = .w_ZoomSlo.GetVar("MLCODUBI")
        .w_MAGZOOM = .w_ZoomSlo.GetVar("MLCODMAG")
    endwith
    this.DoRTCalc(38,38,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_CODART<>.w_CODART
            .w_CODVAR = SPACE(20)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,10,.t.)
        if .o_CODART<>.w_CODART.or. .o_VARIAN<>.w_VARIAN.or. .o_CODVAR<>.w_CODVAR
            .w_LOTTOART = SPACE(20)
          .link_1_14('Full')
        endif
          .link_1_15('Full')
        .DoRTCalc(13,15,.t.)
        if .o_CODMAG<>.w_CODMAG
            .w_CODUBI = Space(5)
          .link_1_19('Full')
        endif
        .oPgFrm.Page1.oPag.ZoomSlo.Calculate()
            .w_LOTTOZOOM = .w_ZoomSlo.GetVar("MLCODLOT")
          .link_1_21('Full')
        .DoRTCalc(18,21,.t.)
        if .o_LOTTOART<>.w_LOTTOART
            .w_STATO = IIF(EMPTY(.w_LOTTOART),'D',.w_STATO)
        endif
        .DoRTCalc(23,23,.t.)
        if .o_CODART<>.w_CODART.or. .o_CODVAR<>.w_CODVAR
            .w_KEYSAL = .w_CODART+IIF(EMPTY(.w_CODVAR), REPL('#',20), .w_CODVAR)
        endif
        .DoRTCalc(25,25,.t.)
        if .o_CODART<>.w_CODART
            .w_LOTTOART = Space(20)
          .link_1_38('Full')
        endif
        if .o_CODMAG<>.w_CODMAG
            .w_CODUBI = Space(5)
          .link_1_39('Full')
        endif
        .DoRTCalc(28,28,.t.)
        if .o_LOTTOZOOM<>.w_LOTTOZOOM.or. .o_LOTTOART<>.w_LOTTOART
          .link_1_42('Full')
        endif
        .DoRTCalc(30,30,.t.)
        if .o_LOTTOZOOM<>.w_LOTTOZOOM.or. .o_LOTTOART<>.w_LOTTOART
          .link_1_45('Full')
        endif
        .DoRTCalc(32,35,.t.)
            .w_UBIZOOM = .w_ZoomSlo.GetVar("MLCODUBI")
            .w_MAGZOOM = .w_ZoomSlo.GetVar("MLCODMAG")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(38,38,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomSlo.Calculate()
    endwith
  return

  proc Calculate_FOOMEPMWPB()
    with this
          * --- Scheda lotti
          LancioSchedeLotti(this;
             )
    endwith
  endproc
  proc Calculate_PLFHLTLJWF()
    with this
          * --- Aggiorna i campi di piede qunado viene lanciata una nuova ricerca
          .w_CODART2 = .w_CODART
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODVAR_1_8.enabled = this.oPgFrm.Page1.oPag.oCODVAR_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCODUBI_1_19.enabled = this.oPgFrm.Page1.oPag.oCODUBI_1_19.mCond()
    this.oPgFrm.Page1.oPag.oSTATO_1_31.enabled = this.oPgFrm.Page1.oPag.oSTATO_1_31.mCond()
    this.oPgFrm.Page1.oPag.oCODUBI_1_39.enabled = this.oPgFrm.Page1.oPag.oCODUBI_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODART_1_1.visible=!this.oPgFrm.Page1.oPag.oCODART_1_1.mHide()
    this.oPgFrm.Page1.oPag.oCODART_1_2.visible=!this.oPgFrm.Page1.oPag.oCODART_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCODVAR_1_8.visible=!this.oPgFrm.Page1.oPag.oCODVAR_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDESVAR_1_9.visible=!this.oPgFrm.Page1.oPag.oDESVAR_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oLOTTOART_1_14.visible=!this.oPgFrm.Page1.oPag.oLOTTOART_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCODUBI_1_19.visible=!this.oPgFrm.Page1.oPag.oCODUBI_1_19.mHide()
    this.oPgFrm.Page1.oPag.oLOTTOART_1_38.visible=!this.oPgFrm.Page1.oPag.oLOTTOART_1_38.mHide()
    this.oPgFrm.Page1.oPag.oCODUBI_1_39.visible=!this.oPgFrm.Page1.oPag.oCODUBI_1_39.mHide()
    this.oPgFrm.Page1.oPag.oNUMCOM_1_45.visible=!this.oPgFrm.Page1.oPag.oNUMCOM_1_45.mHide()
    this.oPgFrm.Page1.oPag.oDESCOM_1_46.visible=!this.oPgFrm.Page1.oPag.oDESCOM_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_53.visible=!this.oPgFrm.Page1.oPag.oBtn_1_53.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomSlo.Event(cEvent)
        if lower(cEvent)==lower("w_zoomslo selected")
          .Calculate_FOOMEPMWPB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Esegui")
          .Calculate_PLFHLTLJWF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODART
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODVAR,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARCODVAR,ARUNMIS1,ARDTOBSO,ARFLLOTT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODVAR,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARCODVAR,ARUNMIS1,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_1'),i_cWhere,'GSMA_AAR',"Codici articoli/servizi",'LOTTI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODVAR,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODVAR,ARUNMIS1,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODVAR,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARCODVAR,ARUNMIS1,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_VARIAN = NVL(_Link_.ARCODVAR,space(5))
      this.w_UNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_FLOTT = NVL(_Link_.ARFLLOTT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_VARIAN = space(5)
      this.w_UNIMIS = space(3)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLOTT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)) AND .w_FLOTT='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_VARIAN = space(5)
        this.w_UNIMIS = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLOTT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_2'),i_cWhere,'GSMA_AAR',"Codici articoli/servizi",'GSMD_ALO.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_UNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_FLOTT = NVL(_Link_.ARFLLOTT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_UNIMIS = space(3)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLOTT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)) AND .w_FLOTT<>'N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_UNIMIS = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLOTT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAR
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DET_VARI_IDX,3]
    i_lTable = "DET_VARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DET_VARI_IDX,2], .t., this.DET_VARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DET_VARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DET_VARI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DVCODICE like "+cp_ToStrODBC(trim(this.w_CODVAR)+"%");
                   +" and DVCODVAR="+cp_ToStrODBC(this.w_VARIAN);

          i_ret=cp_SQL(i_nConn,"select DVCODVAR,DVCODICE,DVDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DVCODVAR,DVCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DVCODVAR',this.w_VARIAN;
                     ,'DVCODICE',trim(this.w_CODVAR))
          select DVCODVAR,DVCODICE,DVDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DVCODVAR,DVCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAR)==trim(_Link_.DVCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAR) and !this.bDontReportError
            deferred_cp_zoom('DET_VARI','*','DVCODVAR,DVCODICE',cp_AbsName(oSource.parent,'oCODVAR_1_8'),i_cWhere,'',"Varianti",'VARLOTTI.DET_VARI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_VARIAN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DVCODVAR,DVCODICE,DVDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DVCODVAR,DVCODICE,DVDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DVCODVAR,DVCODICE,DVDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DVCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DVCODVAR="+cp_ToStrODBC(this.w_VARIAN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DVCODVAR',oSource.xKey(1);
                       ,'DVCODICE',oSource.xKey(2))
            select DVCODVAR,DVCODICE,DVDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DVCODVAR,DVCODICE,DVDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DVCODICE="+cp_ToStrODBC(this.w_CODVAR);
                   +" and DVCODVAR="+cp_ToStrODBC(this.w_VARIAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DVCODVAR',this.w_VARIAN;
                       ,'DVCODICE',this.w_CODVAR)
            select DVCODVAR,DVCODICE,DVDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAR = NVL(_Link_.DVCODICE,space(20))
      this.w_DESVAR = NVL(_Link_.DVDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAR = space(20)
      endif
      this.w_DESVAR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DET_VARI_IDX,2])+'\'+cp_ToStr(_Link_.DVCODVAR,1)+'\'+cp_ToStr(_Link_.DVCODICE,1)
      cp_ShowWarn(i_cKey,this.DET_VARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTTOART
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTTOART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOTTOART like "+cp_ToStrODBC(trim(this.w_LOTTOART)+"%");
                   +" and LOKEYSAL="+cp_ToStrODBC(this.w_KEYSAL);

          i_ret=cp_SQL(i_nConn,"select LOKEYSAL,LOTTOART,LOCODICE,LODATCRE,LODATSCA,LOLOTFOR,LOFLSTAT,LOCODART,LOCODVAR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOKEYSAL,LOTTOART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOKEYSAL',this.w_KEYSAL;
                     ,'LOTTOART',trim(this.w_LOTTOART))
          select LOKEYSAL,LOTTOART,LOCODICE,LODATCRE,LODATSCA,LOLOTFOR,LOFLSTAT,LOCODART,LOCODVAR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOKEYSAL,LOTTOART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTTOART)==trim(_Link_.LOTTOART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTTOART) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOKEYSAL,LOTTOART',cp_AbsName(oSource.parent,'oLOTTOART_1_14'),i_cWhere,'GSAR_ALO',"Anagrafica lotti",'LOTTI.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_KEYSAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOKEYSAL,LOTTOART,LOCODICE,LODATCRE,LODATSCA,LOLOTFOR,LOFLSTAT,LOCODART,LOCODVAR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOKEYSAL,LOTTOART,LOCODICE,LODATCRE,LODATSCA,LOLOTFOR,LOFLSTAT,LOCODART,LOCODVAR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOKEYSAL,LOTTOART,LOCODICE,LODATCRE,LODATSCA,LOLOTFOR,LOFLSTAT,LOCODART,LOCODVAR";
                     +" from "+i_cTable+" "+i_lTable+" where LOTTOART="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOKEYSAL="+cp_ToStrODBC(this.w_KEYSAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOKEYSAL',oSource.xKey(1);
                       ,'LOTTOART',oSource.xKey(2))
            select LOKEYSAL,LOTTOART,LOCODICE,LODATCRE,LODATSCA,LOLOTFOR,LOFLSTAT,LOCODART,LOCODVAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTTOART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOKEYSAL,LOTTOART,LOCODICE,LODATCRE,LODATSCA,LOLOTFOR,LOFLSTAT,LOCODART,LOCODVAR";
                   +" from "+i_cTable+" "+i_lTable+" where LOTTOART="+cp_ToStrODBC(this.w_LOTTOART);
                   +" and LOKEYSAL="+cp_ToStrODBC(this.w_KEYSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOKEYSAL',this.w_KEYSAL;
                       ,'LOTTOART',this.w_LOTTOART)
            select LOKEYSAL,LOTTOART,LOCODICE,LODATCRE,LODATSCA,LOLOTFOR,LOFLSTAT,LOCODART,LOCODVAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTTOART = NVL(_Link_.LOTTOART,space(20))
      this.w_LOTTO = NVL(_Link_.LOCODICE,space(20))
      this.w_DATCRE = NVL(cp_ToDate(_Link_.LODATCRE),ctod("  /  /  "))
      this.w_DATSCA = NVL(cp_ToDate(_Link_.LODATSCA),ctod("  /  /  "))
      this.w_RIFFOR = NVL(_Link_.LOLOTFOR,space(20))
      this.w_STATO = NVL(_Link_.LOFLSTAT,space(12))
      this.w_CODICE = NVL(_Link_.LOCODART,space(20))
      this.w_VARIANT = NVL(_Link_.LOCODVAR,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTTOART = space(20)
      endif
      this.w_LOTTO = space(20)
      this.w_DATCRE = ctod("  /  /  ")
      this.w_DATSCA = ctod("  /  /  ")
      this.w_RIFFOR = space(20)
      this.w_STATO = space(12)
      this.w_CODICE = space(20)
      this.w_VARIANT = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CODART=.w_CODICE) OR EMPTY(.w_CODART)) AND ((.w_CODVAR=.w_VARIANT) OR EMPTY(.w_VARIAN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        endif
        this.w_LOTTOART = space(20)
        this.w_LOTTO = space(20)
        this.w_DATCRE = ctod("  /  /  ")
        this.w_DATSCA = ctod("  /  /  ")
        this.w_RIFFOR = space(20)
        this.w_STATO = space(12)
        this.w_CODICE = space(20)
        this.w_VARIANT = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOKEYSAL,1)+'\'+cp_ToStr(_Link_.LOTTOART,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTTOART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTTO
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',this.w_LOTTO)
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTTO = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTTO = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_18'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBI
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_CODMAG;
                     ,'UBCODICE',trim(this.w_CODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oCODUBI_1_19'),i_cWhere,'GSAR_AUB',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_CODMAG;
                       ,'UBCODICE',this.w_CODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTTOZOOM
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTTOZOOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTTOZOOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LONUMCOM,LOLOTFOR,LOCODCON,LO__NOTE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTTOZOOM);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART2;
                       ,'LOCODICE',this.w_LOTTOZOOM)
            select LOCODART,LOCODICE,LODATCRE,LODATSCA,LONUMCOM,LOLOTFOR,LOCODCON,LO__NOTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTTOZOOM = NVL(_Link_.LOCODICE,space(20))
      this.w_DATCRE = NVL(cp_ToDate(_Link_.LODATCRE),ctod("  /  /  "))
      this.w_DATSCA = NVL(cp_ToDate(_Link_.LODATSCA),ctod("  /  /  "))
      this.w_NUMCOM = NVL(_Link_.LONUMCOM,space(20))
      this.w_RIFFOR = NVL(_Link_.LOLOTFOR,space(20))
      this.w_CODCON = NVL(_Link_.LOCODCON,space(15))
      this.w_NOTE = NVL(_Link_.LO__NOTE,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_LOTTOZOOM = space(20)
      endif
      this.w_DATCRE = ctod("  /  /  ")
      this.w_DATSCA = ctod("  /  /  ")
      this.w_NUMCOM = space(20)
      this.w_RIFFOR = space(20)
      this.w_CODCON = space(15)
      this.w_NOTE = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTTOZOOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTTOART
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTTOART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTTOART)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODART;
                     ,'LOCODICE',trim(this.w_LOTTOART))
          select LOCODART,LOCODICE,LOFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTTOART)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTTOART) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTTOART_1_38'),i_cWhere,'GSMD_ALO',"Anagrafica lotti",'GSMD_SPL.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTTOART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTTOART);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOCODICE',this.w_LOTTOART)
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTTOART = NVL(_Link_.LOCODICE,space(20))
      this.w_LOTTO = NVL(_Link_.LOCODICE,space(20))
      this.w_STATO = NVL(_Link_.LOFLSTAT,space(12))
      this.w_CODICE = NVL(_Link_.LOCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTTOART = space(20)
      endif
      this.w_LOTTO = space(20)
      this.w_STATO = space(12)
      this.w_CODICE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CODART=.w_CODICE) OR EMPTY(.w_CODART))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        endif
        this.w_LOTTOART = space(20)
        this.w_LOTTO = space(20)
        this.w_STATO = space(12)
        this.w_CODICE = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTTOART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBI
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_CODMAG;
                     ,'UBCODICE',trim(this.w_CODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oCODUBI_1_39'),i_cWhere,'GSAR_AUB',"Ubicazioni",'GSMD_SMU.UBICAZIO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_CODMAG;
                       ,'UBCODICE',this.w_CODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMCOM
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_NUMCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_NUMCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMCOM = NVL(_Link_.CNCODCAN,space(20))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_NUMCOM = space(20)
      endif
      this.w_DESCOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODART_1_1.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_1.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_2.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_2.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_3.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_3.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_7.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_7.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAR_1_8.value==this.w_CODVAR)
      this.oPgFrm.Page1.oPag.oCODVAR_1_8.value=this.w_CODVAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAR_1_9.value==this.w_DESVAR)
      this.oPgFrm.Page1.oPag.oDESVAR_1_9.value=this.w_DESVAR
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTTOART_1_14.value==this.w_LOTTOART)
      this.oPgFrm.Page1.oPag.oLOTTOART_1_14.value=this.w_LOTTOART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_18.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_18.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_19.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_19.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCRE_1_23.value==this.w_DATCRE)
      this.oPgFrm.Page1.oPag.oDATCRE_1_23.value=this.w_DATCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLARSTO_1_24.RadioValue()==this.w_FLARSTO)
      this.oPgFrm.Page1.oPag.oFLARSTO_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSCA_1_27.value==this.w_DATSCA)
      this.oPgFrm.Page1.oPag.oDATSCA_1_27.value=this.w_DATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oRIFFOR_1_30.value==this.w_RIFFOR)
      this.oPgFrm.Page1.oPag.oRIFFOR_1_30.value=this.w_RIFFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_31.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTTOART_1_38.value==this.w_LOTTOART)
      this.oPgFrm.Page1.oPag.oLOTTOART_1_38.value=this.w_LOTTOART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_39.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_39.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_40.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_40.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_42.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_42.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_43.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_43.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMCOM_1_45.value==this.w_NUMCOM)
      this.oPgFrm.Page1.oPag.oNUMCOM_1_45.value=this.w_NUMCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_46.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_46.value=this.w_DESCOM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODART)) or not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)) AND .w_FLOTT='S'))  and not(upper(g_APPLICATION)="ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CODART)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
          case   ((empty(.w_CODART)) or not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)) AND .w_FLOTT<>'N'))  and not(upper(g_APPLICATION)<>"ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODART)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
          case   (empty(.w_CODVAR))  and not(upper(g_APPLICATION)="ADHOC REVOLUTION")  and (NOT EMPTY(.w_VARIAN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODVAR_1_8.SetFocus()
            i_bnoObbl = !empty(.w_CODVAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_CODART=.w_CODICE) OR EMPTY(.w_CODART)) AND ((.w_CODVAR=.w_VARIANT) OR EMPTY(.w_VARIAN)))  and not(upper(g_APPLICATION)="ADHOC REVOLUTION")  and not(empty(.w_LOTTOART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTTOART_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
          case   not(((.w_CODART=.w_CODICE) OR EMPTY(.w_CODART)))  and not(upper(g_APPLICATION)<>"ADHOC REVOLUTION")  and not(empty(.w_LOTTOART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTTOART_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODART = this.w_CODART
    this.o_VARIAN = this.w_VARIAN
    this.o_CODVAR = this.w_CODVAR
    this.o_LOTTOART = this.w_LOTTOART
    this.o_CODMAG = this.w_CODMAG
    this.o_LOTTOZOOM = this.w_LOTTOZOOM
    return

enddefine

* --- Define pages as container
define class tgsma_kslPag1 as StdContainer
  Width  = 744
  height = 531
  stdWidth  = 744
  stdheight = 531
  resizeXpos=657
  resizeYpos=278
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODART_1_1 as StdField with uid="DUXMXUDZQC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Articolo selezionato",;
    HelpContextID = 210036698,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=84, Top=8, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_1.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oCODART_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_LOTTOART)
        bRes2=.link_1_38('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODART_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Codici articoli/servizi",'LOTTI.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oCODART_1_2 as StdField with uid="GIUBQTNMBG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Articolo selezionato",;
    HelpContextID = 210036698,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=84, Top=8, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_2.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oCODART_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
      if .not. empty(.w_LOTTOART)
        bRes2=.link_1_38('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODART_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Codici articoli/servizi",'GSMD_ALO.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_3 as StdField with uid="RYCFJETKJM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209977802,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=254, Top=8, InputMask=replicate('X',40)

  add object oUNIMIS_1_7 as StdField with uid="YPFYZSANPU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale di riferimento",;
    HelpContextID = 235444154,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=620, Top=8, InputMask=replicate('X',3)

  add object oCODVAR_1_8 as StdField with uid="CXNJVRGBAV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODVAR", cQueryName = "CODVAR",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Variante selezionata",;
    HelpContextID = 260040666,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=84, Top=33, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DET_VARI", oKey_1_1="DVCODVAR", oKey_1_2="this.w_VARIAN", oKey_2_1="DVCODICE", oKey_2_2="this.w_CODVAR"

  func oCODVAR_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_VARIAN))
    endwith
   endif
  endfunc

  func oCODVAR_1_8.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oCODVAR_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAR_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAR_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DET_VARI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DVCODVAR="+cp_ToStrODBC(this.Parent.oContained.w_VARIAN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DVCODVAR="+cp_ToStr(this.Parent.oContained.w_VARIAN)
    endif
    do cp_zoom with 'DET_VARI','*','DVCODVAR,DVCODICE',cp_AbsName(this.parent,'oCODVAR_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Varianti",'VARLOTTI.DET_VARI_VZM',this.parent.oContained
  endproc

  add object oDESVAR_1_9 as StdField with uid="NLHCWHHBMP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESVAR", cQueryName = "DESVAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 259981770,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=254, Top=33, InputMask=replicate('X',40)

  func oDESVAR_1_9.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oLOTTOART_1_14 as StdField with uid="WCOPXLQFJF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_LOTTOART", cQueryName = "LOTTOART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto selezionato non congruente con il codice articolo",;
    ToolTipText = "Codice lotto selezionato",;
    HelpContextID = 262203126,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=84, Top=58, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSAR_ALO", oKey_1_1="LOKEYSAL", oKey_1_2="this.w_KEYSAL", oKey_2_1="LOTTOART", oKey_2_2="this.w_LOTTOART"

  func oLOTTOART_1_14.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oLOTTOART_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTTOART_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTTOART_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOKEYSAL="+cp_ToStrODBC(this.Parent.oContained.w_KEYSAL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOKEYSAL="+cp_ToStr(this.Parent.oContained.w_KEYSAL)
    endif
    do cp_zoom with 'LOTTIART','*','LOKEYSAL,LOTTOART',cp_AbsName(this.parent,'oLOTTOART_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALO',"Anagrafica lotti",'LOTTI.LOTTIART_VZM',this.parent.oContained
  endproc
  proc oLOTTOART_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOKEYSAL=w_KEYSAL
     i_obj.w_LOTTOART=this.parent.oContained.w_LOTTOART
     i_obj.ecpSave()
  endproc

  add object oCODMAG_1_18 as StdField with uid="EEZZGFBCSV",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 176744410,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=330, Top=58, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
      if .not. empty(.w_CODUBI)
        bRes2=.link_1_19('Full')
      endif
      if .not. empty(.w_CODUBI)
        bRes2=.link_1_39('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oCODUBI_1_19 as StdField with uid="GTXINOEYZQ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Ubicazione (vuota tutte)",;
    HelpContextID = 141617114,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=485, Top=58, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSAR_AUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_CODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUBI"

  func oCODUBI_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_CODMAG) and .w_Flubic='S')
    endwith
   endif
  endfunc

  func oCODUBI_1_19.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oCODUBI_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUBI_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUBI_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_CODMAG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_CODMAG)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oCODUBI_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUB',"Ubicazioni",'',this.parent.oContained
  endproc
  proc oCODUBI_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_CODMAG
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUBI
     i_obj.ecpSave()
  endproc


  add object ZoomSlo as cp_zoombox with uid="PHHVQSONNF",left=3, top=112, width=736,height=292,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="LOTTIART",cZoomFile="GSMA_KSL",bOptions=.f.,bQueryOnLoad=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 189690906

  add object oDATCRE_1_23 as StdField with uid="KPZIPMBWJZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATCRE", cQueryName = "DATCRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data creazione",;
    HelpContextID = 193066442,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=117, Top=480

  add object oFLARSTO_1_24 as StdCheck with uid="GVKODKDZJD",rtseq=19,rtrep=.f.,left=485, top=86, caption="Movimenti storicizzati",;
    ToolTipText = "Se attivo saranno visualizzati anche i movim. di magazzino e i documenti storicizzati",;
    HelpContextID = 207887018,;
    cFormVar="w_FLARSTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLARSTO_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLARSTO_1_24.GetRadio()
    this.Parent.oContained.w_FLARSTO = this.RadioValue()
    return .t.
  endfunc

  func oFLARSTO_1_24.SetRadio()
    this.Parent.oContained.w_FLARSTO=trim(this.Parent.oContained.w_FLARSTO)
    this.value = ;
      iif(this.Parent.oContained.w_FLARSTO=='S',1,;
      0)
  endfunc


  add object oBtn_1_25 as StdButton with uid="ZKIRZHGWRH",left=684, top=63, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 86815674;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        .NotifyEvent('Esegui')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATSCA_1_27 as StdField with uid="WUOHLKJOBB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DATSCA", cQueryName = "DATSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di scadenza",;
    HelpContextID = 262015542,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=117, Top=504

  add object oRIFFOR_1_30 as StdField with uid="EBBTEGFUCU",rtseq=21,rtrep=.f.,;
    cFormVar = "w_RIFFOR", cQueryName = "RIFFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 246402282,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=116, Top=456, InputMask=replicate('X',20)


  add object oSTATO_1_31 as StdCombo with uid="AYVGMNOBWG",value=4,rtseq=22,rtrep=.f.,left=84,top=85,width=99,height=21;
    , ToolTipText = "Status del lotto";
    , HelpContextID = 257814054;
    , cFormVar="w_STATO",RowSource=""+"Disponibile,"+"Sospeso,"+"Esaurito,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_31.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    iif(this.value =4,'',;
    space(12))))))
  endfunc
  func oSTATO_1_31.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_31.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='D',1,;
      iif(this.Parent.oContained.w_STATO=='S',2,;
      iif(this.Parent.oContained.w_STATO=='E',3,;
      iif(this.Parent.oContained.w_STATO=='',4,;
      0))))
  endfunc

  func oSTATO_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_LOTTOART))
    endwith
   endif
  endfunc

  add object oLOTTOART_1_38 as StdField with uid="ELYHCTBTZX",rtseq=26,rtrep=.f.,;
    cFormVar = "w_LOTTOART", cQueryName = "LOTTOART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto selezionato non congruente con il codice articolo",;
    ToolTipText = "Codice lotto selezionato",;
    HelpContextID = 262203126,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=84, Top=58, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTTOART"

  func oLOTTOART_1_38.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oLOTTOART_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTTOART_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTTOART_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTTOART_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Anagrafica lotti",'GSMD_SPL.LOTTIART_VZM',this.parent.oContained
  endproc
  proc oLOTTOART_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_CODART
     i_obj.w_LOCODICE=this.parent.oContained.w_LOTTOART
     i_obj.ecpSave()
  endproc

  add object oCODUBI_1_39 as StdField with uid="KAQCAJYCPL",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Ubicazione (vuota tutte)",;
    HelpContextID = 141617114,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=485, Top=58, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSAR_AUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_CODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUBI"

  func oCODUBI_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_CODMAG) and .w_Flubic='S')
    endwith
   endif
  endfunc

  func oCODUBI_1_39.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oCODUBI_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUBI_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUBI_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_CODMAG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_CODMAG)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oCODUBI_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUB',"Ubicazioni",'GSMD_SMU.UBICAZIO_VZM',this.parent.oContained
  endproc
  proc oCODUBI_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_CODMAG
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUBI
     i_obj.ecpSave()
  endproc

  add object oNOTE_1_40 as StdMemo with uid="PSQYYMFTCV",rtseq=28,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 174069974,;
   bGlobalFont=.t.,;
    Height=68, Width=417, Left=320, Top=456

  add object oCODCON_1_42 as StdField with uid="GKBPQOXFFG",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 45279194,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=116, Top=432, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCON_1_43 as StdField with uid="MBVFNTYYCD",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 45220298,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=271, Top=432, InputMask=replicate('X',40)

  add object oNUMCOM_1_45 as StdField with uid="ANDMRXTYRT",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NUMCOM", cQueryName = "NUMCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 62017834,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=116, Top=408, InputMask=replicate('X',20), cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_NUMCOM"

  func oNUMCOM_1_45.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  func oNUMCOM_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCOM_1_46 as StdField with uid="FWAKMAJCLB",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61997514,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=271, Top=408, InputMask=replicate('X',40)

  func oDESCOM_1_46.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc


  add object oBtn_1_53 as StdButton with uid="XFKMYMRDMR",left=684, top=409, width=48,height=45,;
    CpPicture="bmp\lotti.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza scheda lotto";
    , HelpContextID = 231593854;
    , Caption='\<Sch. lotti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      with this.Parent.oContained
        do GSMD1BSL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_53.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_LOTTOZOOM))
      endwith
    endif
  endfunc

  func oBtn_1_53.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (upper(g_APPLICATION)<>"ADHOC REVOLUTION")
     endwith
    endif
  endfunc

  add object oStr_1_10 as StdString with uid="ZHSTUFRJVR",Visible=.t., Left=36, Top=8,;
    Alignment=1, Width=45, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="HOAQOAANVP",Visible=.t., Left=28, Top=33,;
    Alignment=1, Width=53, Height=15,;
    Caption="Variante:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="TDRQYRHONQ",Visible=.t., Left=9, Top=58,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="CNYMGZKOGU",Visible=.t., Left=589, Top=8,;
    Alignment=1, Width=27, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="RZLTHHHAQC",Visible=.t., Left=45, Top=480,;
    Alignment=1, Width=66, Height=15,;
    Caption="Creato il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="RQPEALHWNM",Visible=.t., Left=39, Top=504,;
    Alignment=1, Width=73, Height=15,;
    Caption="Scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="UREOEKCIRR",Visible=.t., Left=8, Top=456,;
    Alignment=1, Width=105, Height=15,;
    Caption="Rif.lotto fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="MXENHOLMIW",Visible=.t., Left=257, Top=58,;
    Alignment=1, Width=69, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="ZPFLDKTPNN",Visible=.t., Left=400, Top=58,;
    Alignment=1, Width=82, Height=15,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="GKVHYIDMYI",Visible=.t., Left=37, Top=85,;
    Alignment=1, Width=44, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="SBZEFUDKCZ",Visible=.t., Left=280, Top=456,;
    Alignment=1, Width=38, Height=15,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="HIMQGIZHRD",Visible=.t., Left=52, Top=432,;
    Alignment=1, Width=61, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="ZQWKTEMWOE",Visible=.t., Left=47, Top=408,;
    Alignment=1, Width=69, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_ksl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsma_ksl
Proc LancioSchedeLotti(pParent)
   If upper(g_APPLICATION)="ADHOC REVOLUTION"
     GSMD1BSL(pParent)
   Endif
Endproc


* --- Fine Area Manuale
