* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bad                                                        *
*              Elimina scrit.assestamento                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_60]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-14                                                      *
* Last revis.: 2001-03-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,p_PARA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bad",oParentObject,m.p_PARA)
return(i_retval)

define class tgscg_bad as StdBatch
  * --- Local variables
  p_PARA = space(1)
  w_DATREG = ctod("  /  /  ")
  w_COUNT = 0
  w_PNSERIAL = space(10)
  w_MESS = space(100)
  w_FLPROV = space(1)
  w_CODESER = space(5)
  w_oMess = .NULL.
  * --- WorkFile variables
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  MOVICOST_idx=0
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_oMess=createobject("Ah_Message")
    do case
      case this.p_PARA="A"
        * --- Controlla che la Data di Registrazione della Scrittura di Storno sia definita in qualche Esercizio
        if this.oParentObject.w_ASFLSTOR="S" 
          this.w_DATREG = iif(nvl(this.oParentObject.w_ASDTRGST,cp_CharToDate("  -  -  "))=cp_CharToDate("  -  -  "),IIF(this.oParentObject.w_ASDATFIN<>this.oParentObject.w_FINESE OR this.oParentObject.w_ASDATREG>this.oParentObject.w_FINESE,this.oParentObject.w_ASDATREG+1,this.oParentObject.w_FINESE+1),this.oParentObject.w_ASDTRGST)
          this.w_CODESER = CALCESER( this.w_DATREG ,"")
          if Empty( this.w_CODESER )
            if Not Empty(this.w_DATREG)
              this.oParentObject.w_FLESER = .F.
              ah_ErrorMsg("La data di registrazione della scrittura di storno: %1, non � compresa in nessun esercizio","!","",dtoc(nvl(this.w_DATREG,cp_CharToDate("- -"))))
              this.oParentObject.w_ASDTRGST = cp_CharToDate("  -  -  ")
            endif
          endif
        endif
      case this.p_PARA="D"
        * --- Elimina nei dettagli i riferimenti alle righe di primanota della generazione scritture di assestamento
        if (this.oParentObject.w_CONCON >= this.oParentObject.w_ASDATREG ) OR (this.oParentObject.w_STALIG>=this.oParentObject.w_ASDATREG)
          this.w_MESS = ah_MsgFormat("Data registrazione inferiore o uguale alla data di consolidamento o alla data di stampa del libro giornale.%0Cancellazione annullata")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        endif
        this.w_MESS = "La cancellazione della generazione eliminer� anche%0tutte le registrazioni di primanota associate se solo provvisorie.%0confermi la cancellazione?"
        if ah_YesNo(this.w_MESS)
          this.w_oMess.AddMsgPartNL("Transazione abbandonata!")     
          VQ_EXEC("QUERY\GSCG_BAD",this,"CursDelete")
          SELECT CursDelete
          GO TOP
          COUNT FOR PNFLPROV<>"S" TO this.w_COUNT
          if this.w_COUNT = 0
            * --- Try
            local bErr_03EFB430
            bErr_03EFB430=bTrsErr
            this.Try_03EFB430()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              this.w_oMess.AddMsgPartNL("Impossibile cancellare alcune registrazioni di primanota!")     
              this.w_MESS = this.w_oMess.ComposeMessage()
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS
            endif
            bTrsErr=bTrsErr or bErr_03EFB430
            * --- End
          else
            this.w_oMess.AddMsgPartNL("Impossibile cancellare generazione")     
            this.w_oMess.AddMsgPartNL("perch� esistono riferimenti a registrazioni confermate.")     
            this.w_MESS = this.w_oMess.ComposeMessage()
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            i_retcode = 'stop'
            return
          endif
        else
          this.w_oMess.AddMsgPartNL("Transazione abbandonata!")     
          this.w_oMess.AddMsgPartNL("Cancellazione annullata!")     
          this.w_MESS = this.w_oMess.ComposeMessage()
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        endif
    endcase
  endproc
  proc Try_03EFB430()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SELECT CursDelete
    GO TOP
    SCAN
    this.w_PNSERIAL = ASRIFEPN
    * --- Delete from MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and MRROWORD > "+cp_ToStrODBC(0);
             )
    else
      delete from (i_cTable) where;
            MRSERIAL = this.w_PNSERIAL;
            and MRROWORD > 0;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
             )
    else
      delete from (i_cTable) where;
            PNSERIAL = this.w_PNSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
             )
    else
      delete from (i_cTable) where;
            PNSERIAL = this.w_PNSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    ENDSCAN
    return


  proc Init(oParentObject,p_PARA)
    this.p_PARA=p_PARA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='PNT_DETT'
    this.cWorkTables[3]='MOVICOST'
    this.cWorkTables[4]='ESERCIZI'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="p_PARA"
endproc
