* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bsr                                                        *
*              Stampa saldi IVA                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_64]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-15                                                      *
* Last revis.: 2000-09-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bsr",oParentObject)
return(i_retval)

define class tgscg_bsr as StdBatch
  * --- Local variables
  w_NOMREP = space(10)
  * --- WorkFile variables
  PRI_DETT_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Saldi Registri IVA (da GSCG_SSR)
    if EMPTY(this.oParentObject.w_ANNO)
      ah_ErrorMsg("Selezionare un anno",,"")
      i_retcode = 'stop'
      return
    endif
    this.w_NOMREP = "GSCG_SST.FRX"
    L_ANNO = this.oParentObject.w_ANNO
    VQ_EXEC("QUERY\GSCG_SSR.VQR",this,"SALDI")
    * --- Esegue la Stampa
    if USED("SALDI") AND NOT EMPTY(this.w_NOMREP)
      SELECT * FROM SALDI INTO CURSOR __TMP__ ORDER BY 1,2,3,4
      select SALDI
      USE
      select __TMP__
      GO TOP
      CP_CHPRN("QUERY\" + this.w_NOMREP, " ", this)
    endif
    if USED("__TMP__") 
      select __TMP__
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRI_DETT'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
