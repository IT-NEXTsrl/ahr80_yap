* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mua                                                        *
*              Utenti / azienda                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_38]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-10                                                      *
* Last revis.: 2016-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mua")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mua")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mua")
  return

* --- Class definition
define class tgsar_mua as StdPCForm
  Width  = 439
  Height = 256
  Top    = 8
  Left   = 16
  cComment = "Utenti / azienda"
  cPrg = "gsar_mua"
  HelpContextID=63534953
  add object cnt as tcgsar_mua
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mua as PCContext
  w_UACODAZI = space(5)
  w_RAGAZI = space(40)
  w_OBTEST = space(8)
  w_UACODUTE = 0
  w_READUTE = 0
  w_DESUTE = space(30)
  w_DESUTE1 = space(30)
  w_OCODUTE = 0
  w_WEBTYPE = space(1)
  proc Save(i_oFrom)
    this.w_UACODAZI = i_oFrom.w_UACODAZI
    this.w_RAGAZI = i_oFrom.w_RAGAZI
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_UACODUTE = i_oFrom.w_UACODUTE
    this.w_READUTE = i_oFrom.w_READUTE
    this.w_DESUTE = i_oFrom.w_DESUTE
    this.w_DESUTE1 = i_oFrom.w_DESUTE1
    this.w_OCODUTE = i_oFrom.w_OCODUTE
    this.w_WEBTYPE = i_oFrom.w_WEBTYPE
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_UACODAZI = this.w_UACODAZI
    i_oTo.w_RAGAZI = this.w_RAGAZI
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_UACODUTE = this.w_UACODUTE
    i_oTo.w_READUTE = this.w_READUTE
    i_oTo.w_DESUTE = this.w_DESUTE
    i_oTo.w_DESUTE1 = this.w_DESUTE1
    i_oTo.w_OCODUTE = this.w_OCODUTE
    i_oTo.w_WEBTYPE = this.w_WEBTYPE
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mua as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 439
  Height = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-10-27"
  HelpContextID=63534953
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  UTE_AZI_IDX = 0
  AZIENDA_IDX = 0
  CPUSERS_IDX = 0
  cpusers_IDX = 0
  cFile = "UTE_AZI"
  cKeySelect = "UACODAZI"
  cKeyWhere  = "UACODAZI=this.w_UACODAZI"
  cKeyDetail  = "UACODAZI=this.w_UACODAZI and UACODUTE=this.w_UACODUTE"
  cKeyWhereODBC = '"UACODAZI="+cp_ToStrODBC(this.w_UACODAZI)';

  cKeyDetailWhereODBC = '"UACODAZI="+cp_ToStrODBC(this.w_UACODAZI)';
      +'+" and UACODUTE="+cp_ToStrODBC(this.w_UACODUTE)';

  cKeyWhereODBCqualified = '"UTE_AZI.UACODAZI="+cp_ToStrODBC(this.w_UACODAZI)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mua"
  cComment = "Utenti / azienda"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  cAutoZoom = 'GSAR0MUA'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_UACODAZI = space(5)
  o_UACODAZI = space(5)
  w_RAGAZI = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_UACODUTE = 0
  w_READUTE = 0
  w_DESUTE = space(30)
  w_DESUTE1 = space(30)
  w_OCODUTE = 0
  w_WEBTYPE = space(1)
  w_LblAzi = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_muaPag1","gsar_mua",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_LblAzi = this.oPgFrm.Pages(1).oPag.LblAzi
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_LblAzi = .NULL.
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CPUSERS'
    this.cWorkTables[3]='cpusers'
    this.cWorkTables[4]='UTE_AZI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.UTE_AZI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.UTE_AZI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mua'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from UTE_AZI where UACODAZI=KeySet.UACODAZI
    *                            and UACODUTE=KeySet.UACODUTE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsar_mua
      this.w_UACODUTE=iif(len(alltrim(str(this.w_UACODUTE)))>4,val(left(alltrim(STR(this.w_UACODUTE)),4)),this.w_UACODUTE)
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.UTE_AZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_AZI_IDX,2],this.bLoadRecFilter,this.UTE_AZI_IDX,"gsar_mua")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('UTE_AZI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "UTE_AZI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' UTE_AZI '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'UACODAZI',this.w_UACODAZI  )
      select * from (i_cTable) UTE_AZI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_OBTEST = i_INIDAT
        .w_UACODAZI = NVL(UACODAZI,space(5))
          if link_1_1_joined
            this.w_UACODAZI = NVL(AZCODAZI101,NVL(this.w_UACODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .oPgFrm.Page1.oPag.LblAzi.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'UTE_AZI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESUTE = space(30)
          .w_WEBTYPE = space(1)
          .w_UACODUTE = NVL(UACODUTE,0)
          .link_2_1('Load')
        .w_READUTE = .w_UACODUTE
          .link_2_2('Load')
        .w_DESUTE1 = IIF(EMPTY(.w_UACODUTE), SPACE(30), IIF(EMPTY(.w_DESUTE), '<non esistente!>', .w_DESUTE))
        .w_OCODUTE = .w_UACODUTE
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace UACODUTE with .w_UACODUTE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.LblAzi.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_UACODAZI=space(5)
      .w_RAGAZI=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_UACODUTE=0
      .w_READUTE=0
      .w_DESUTE=space(30)
      .w_DESUTE1=space(30)
      .w_OCODUTE=0
      .w_WEBTYPE=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_UACODAZI))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        .w_OBTEST = i_INIDAT
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_UACODUTE))
         .link_2_1('Full')
        endif
        .w_READUTE = .w_UACODUTE
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_READUTE))
         .link_2_2('Full')
        endif
        .DoRTCalc(6,6,.f.)
        .w_DESUTE1 = IIF(EMPTY(.w_UACODUTE), SPACE(30), IIF(EMPTY(.w_DESUTE), '<non esistente!>', .w_DESUTE))
        .w_OCODUTE = .w_UACODUTE
        .oPgFrm.Page1.oPag.LblAzi.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'UTE_AZI')
    this.DoRTCalc(9,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'UTE_AZI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.UTE_AZI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UACODAZI,"UACODAZI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_UACODUTE N(6);
      ,t_DESUTE1 C(30);
      ,UACODUTE N(6);
      ,t_READUTE N(6);
      ,t_DESUTE C(30);
      ,t_OCODUTE N(6);
      ,t_WEBTYPE C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_muabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUACODUTE_2_1.controlsource=this.cTrsName+'.t_UACODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTE1_2_4.controlsource=this.cTrsName+'.t_DESUTE1'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(71)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUACODUTE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.UTE_AZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_AZI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.UTE_AZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_AZI_IDX,2])
      *
      * insert into UTE_AZI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'UTE_AZI')
        i_extval=cp_InsertValODBCExtFlds(this,'UTE_AZI')
        i_cFldBody=" "+;
                  "(UACODAZI,UACODUTE,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_UACODAZI)+","+cp_ToStrODBCNull(this.w_UACODUTE)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'UTE_AZI')
        i_extval=cp_InsertValVFPExtFlds(this,'UTE_AZI')
        cp_CheckDeletedKey(i_cTable,0,'UACODAZI',this.w_UACODAZI,'UACODUTE',this.w_UACODUTE)
        INSERT INTO (i_cTable) (;
                   UACODAZI;
                  ,UACODUTE;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_UACODAZI;
                  ,this.w_UACODUTE;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.UTE_AZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_AZI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_UACODUTE<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'UTE_AZI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and UACODUTE="+cp_ToStrODBC(&i_TN.->UACODUTE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'UTE_AZI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and UACODUTE=&i_TN.->UACODUTE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_UACODUTE<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and UACODUTE="+cp_ToStrODBC(&i_TN.->UACODUTE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and UACODUTE=&i_TN.->UACODUTE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace UACODUTE with this.w_UACODUTE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update UTE_AZI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'UTE_AZI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     "UACODUTE="+cp_ToStrODBC(this.w_UACODUTE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and UACODUTE="+cp_ToStrODBC(UACODUTE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'UTE_AZI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                     UACODUTE=this.w_UACODUTE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and UACODUTE=&i_TN.->UACODUTE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.UTE_AZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_AZI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_UACODUTE<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete UTE_AZI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and UACODUTE="+cp_ToStrODBC(&i_TN.->UACODUTE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and UACODUTE=&i_TN.->UACODUTE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_UACODUTE<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.UTE_AZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_AZI_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,4,.t.)
          .w_READUTE = .w_UACODUTE
          .link_2_2('Full')
        .DoRTCalc(6,6,.t.)
          .w_DESUTE1 = IIF(EMPTY(.w_UACODUTE), SPACE(30), IIF(EMPTY(.w_DESUTE), '<non esistente!>', .w_DESUTE))
        if .o_UACODAZI<>.w_UACODAZI
          .w_OCODUTE = .w_UACODUTE
        endif
        .oPgFrm.Page1.oPag.LblAzi.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_READUTE with this.w_READUTE
      replace t_DESUTE with this.w_DESUTE
      replace t_OCODUTE with this.w_OCODUTE
      replace t_WEBTYPE with this.w_WEBTYPE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.LblAzi.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.LblAzi.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UACODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UACODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UACODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_UACODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_UACODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UACODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_UACODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UACODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on UTE_AZI.UACODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and UTE_AZI.UACODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=UACODUTE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UACODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_UACODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME,WEBTYPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_UACODUTE)
          select CODE,NAME,WEBTYPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UACODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oUACODUTE_2_1'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME,WEBTYPE";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME,WEBTYPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UACODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME,WEBTYPE";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UACODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UACODUTE)
            select CODE,NAME,WEBTYPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UACODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE1 = NVL(_Link_.NAME,space(30))
      this.w_WEBTYPE = NVL(_Link_.WEBTYPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UACODUTE = 0
      endif
      this.w_DESUTE1 = space(30)
      this.w_WEBTYPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_UACODUTE<=9999
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice utente risulta pi� grande di 4 caratteri")
        endif
        this.w_UACODUTE = 0
        this.w_DESUTE1 = space(30)
        this.w_WEBTYPE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UACODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READUTE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_READUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_READUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READUTE = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_READUTE = 0
      endif
      this.w_DESUTE = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_2.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_2.value=this.w_RAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUACODUTE_2_1.value==this.w_UACODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUACODUTE_2_1.value=this.w_UACODUTE
      replace t_UACODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUACODUTE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTE1_2_4.value==this.w_DESUTE1)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTE1_2_4.value=this.w_DESUTE1
      replace t_DESUTE1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTE1_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'UTE_AZI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_mua
      * --- Deve accettare solo Codici Diversi da 0 e non Variati
      private p_oldrec
      select (this.cTrsName)
      p_oldrec=recno()
      GO TOP
      DELETE ALL FOR t_UACODUTE=0 OR t_WEBTYPE<>' '
      GO TOP
      REPLACE ALL I_SRV WITH ' ' FOR I_SRV='U'
      GOTO p_oldrec
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_UACODUTE<=9999) and not(empty(.w_UACODUTE)) and (.w_UACODUTE<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUACODUTE_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il codice utente risulta pi� grande di 4 caratteri")
      endcase
      if .w_UACODUTE<>0
        * --- Area Manuale = Check Row
        * --- gsar_mua
        if .w_WEBTYPE<>' '
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = Ah_MsgFormat("Codice Utente non consentito(WEB)")
        endif
        
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_UACODAZI = this.w_UACODAZI
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_UACODUTE<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_UACODUTE=0
      .w_READUTE=0
      .w_DESUTE=space(30)
      .w_DESUTE1=space(30)
      .w_OCODUTE=0
      .w_WEBTYPE=space(1)
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_UACODUTE))
        .link_2_1('Full')
      endif
        .w_READUTE = .w_UACODUTE
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_READUTE))
        .link_2_2('Full')
      endif
      .DoRTCalc(6,6,.f.)
        .w_DESUTE1 = IIF(EMPTY(.w_UACODUTE), SPACE(30), IIF(EMPTY(.w_DESUTE), '<non esistente!>', .w_DESUTE))
        .w_OCODUTE = .w_UACODUTE
    endwith
    this.DoRTCalc(9,9,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_UACODUTE = t_UACODUTE
    this.w_READUTE = t_READUTE
    this.w_DESUTE = t_DESUTE
    this.w_DESUTE1 = t_DESUTE1
    this.w_OCODUTE = t_OCODUTE
    this.w_WEBTYPE = t_WEBTYPE
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_UACODUTE with this.w_UACODUTE
    replace t_READUTE with this.w_READUTE
    replace t_DESUTE with this.w_DESUTE
    replace t_DESUTE1 with this.w_DESUTE1
    replace t_OCODUTE with this.w_OCODUTE
    replace t_WEBTYPE with this.w_WEBTYPE
    if i_srv='A'
      replace UACODUTE with this.w_UACODUTE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_muaPag1 as StdContainer
  Width  = 435
  height = 256
  stdWidth  = 435
  stdheight = 256
  resizeXpos=223
  resizeYpos=165
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRAGAZI_1_2 as StdField with uid="EZMLAGUCSG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 82034922,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=63, Top=9, InputMask=replicate('X',40)


  add object LblAzi as cp_calclbl with uid="DILGRCBQKL",left=7, top=9, width=56,height=25,;
    caption='LblAzi',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.t.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=1;
    , HelpContextID = 48320586


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=39, width=417,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="UACODUTE",Label1="Utenti",Field2="DESUTE1",Label2="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 252555898

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=58,;
    width=411+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=59,width=410+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CPUSERS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CPUSERS'
        oDropInto=this.oBodyCol.oRow.oUACODUTE_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_muaBodyRow as CPBodyRowCnt
  Width=401
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oUACODUTE_2_1 as StdTrsField with uid="VKWQGLXATA",rtseq=4,rtrep=.t.,;
    cFormVar="w_UACODUTE",value=0,isprimarykey=.t.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 97124235,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il codice utente risulta pi� grande di 4 caratteri",;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"], bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_UACODUTE"

  func oUACODUTE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oUACODUTE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oUACODUTE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oUACODUTE_2_1.readonly and this.parent.oUACODUTE_2_1.isprimarykey)
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oUACODUTE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti",'',this.parent.oContained
   endif
  endproc

  add object oDESUTE1_2_4 as StdTrsField with uid="QCSPTGFZGB",rtseq=7,rtrep=.t.,;
    cFormVar="w_DESUTE1",value=space(30),enabled=.f.,;
    HelpContextID = 154074570,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=325, Left=71, Top=0, InputMask=replicate('X',30)
  add object oLast as LastKeyMover
  * ---
  func oUACODUTE_2_1.When()
    return(.t.)
  proc oUACODUTE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oUACODUTE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mua','UTE_AZI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".UACODAZI=UTE_AZI.UACODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
