* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bfi                                                        *
*              Elabora documento                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-12-01                                                      *
* Last revis.: 2014-07-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEstensione,w_GFPATHFILE,w_GFNOMEFILE,w_GFCLASDOCU,w_GFORIGFILE,w_CDMODALL,w_LICLADOC,w_GFDESCFILE,w_GFKEYINDIC,w_Archivio,w_Chiave,w_NoVisMenu,w_ParTipoAlle,w_ParClasseAlle
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bfi",oParentObject,m.pEstensione,m.w_GFPATHFILE,m.w_GFNOMEFILE,m.w_GFCLASDOCU,m.w_GFORIGFILE,m.w_CDMODALL,m.w_LICLADOC,m.w_GFDESCFILE,m.w_GFKEYINDIC,m.w_Archivio,m.w_Chiave,m.w_NoVisMenu,m.w_ParTipoAlle,m.w_ParClasseAlle)
return(i_retval)

define class tgsut_bfi as StdBatch
  * --- Local variables
  w_FILE_ORIG = space(254)
  pEstensione = space(3)
  w_GFPATHFILE = space(60)
  w_GFNOMEFILE = space(60)
  w_GFCLASDOCU = space(10)
  w_GFORIGFILE = space(254)
  w_CDMODALL = space(1)
  w_LICLADOC = space(100)
  w_GFDESCFILE = space(200)
  w_GFKEYINDIC = space(10)
  w_Archivio = space(2)
  w_Chiave = space(20)
  w_NoVisMenu = .f.
  w_ParTipoAlle = space(5)
  w_ParClasseAlle = space(5)
  w_RETVAL = 0
  w_OGGETT = space(220)
  w_TESTO = space(0)
  w_TIPOALLE = space(5)
  w_CLASSEALL = space(5)
  GIA_FIRMATO = .f.
  w_FILE = space(254)
  w_FILEFIRMD = space(254)
  w_PathPdfCreato = space(100)
  w_CODAZI = space(5)
  w_FLGDIK = space(1)
  w_OKPDF = .f.
  w_INARCHIVIO = space(1)
  w_MODALL = space(1)
  w_PATALL = space(254)
  w_NOMFIL = space(220)
  * --- WorkFile variables
  PROMINDI_idx=0
  PAR_ALTE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ELABORA DOCUMENTO
    * --- Parametri per EDS
    * --- Archivio su cui eseguire l'archiviazione
    * --- Valore campo chiave dell'archivio su cui eseguire l'archiviazione
    * --- Se proveniamo da GSAL_KDA non deve visualizzare il men� ma firmare direttamente il file
    this.w_FILEFIRMD = "@@@@@"
    this.w_PathPdfCreato = ""
    this.GIA_FIRMATO = .F.
    if Isalt()
      this.w_CODAZI = i_CODAZI
      * --- Read from PAR_ALTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PAFLGDIK"+;
          " from "+i_cTable+" PAR_ALTE where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PAFLGDIK;
          from (i_cTable) where;
              PACODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLGDIK = NVL(cp_ToDate(_read_.PAFLGDIK),cp_NullValue(_read_.PAFLGDIK))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if ALLTRIM(UPPER(this.pEstensione)) <> "PDF" AND !this.w_NoVisMenu
      if i_VisualTheme = -1
        L_RETVAL=0
        DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT
        DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Firma il documento")
        DEFINE BAR 2 OF popCmd PROMPT ah_MsgFormat("Converti in PDF e firma il documento")
        ON SELECTION BAR 1 OF popCmd L_RETVAL = 1
        ON SELECTION BAR 2 OF popCmd L_RETVAL = 2
        ACTIVATE POPUP popCmd
        DEACTIVATE POPUP popCmd
        RELEASE POPUPS popCmd
        this.w_RETVAL = L_RETVAL
        Release L_RETVAL
      else
        local oBtnMenu, oMenuItem
        oBtnMenu = CreateObject("cbPopupMenu")
        oBtnMenu.oParentObject = This
        oBtnMenu.oPopupMenu.cOnClickProc="ExecScript(NAMEPROC)"
        oMenuItem = oBtnMenu.AddMenuItem()
        oMenuItem.Caption = Ah_Msgformat("Firma il documento")
        oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=1"
        oMenuItem.Visible=.T.
        oMenuItem = oBtnMenu.AddMenuItem()
        oMenuItem.Caption = Ah_Msgformat("Converti in PDF e firma il documento")
        oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=2"
        oMenuItem.Visible=.T.
        oBtnMenu.InitMenu()
        oBtnMenu.ShowMenu()
        oBtnMenu = .NULL.
      endif
      this.w_OKPDF = .t.
      do case
        case this.w_RETVAL = 1
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.w_RETVAL = 2
          * --- Conversione in PDF e firma
          if ! CHECKPRINTTO("." + this.pEstensione) 
            this.w_OKPDF = .f.
            if this.w_NoVisMenu
              if Ah_Yesno( "Il documento selezionato non pu� essere convertito in PDF%0Si desidera proseguire comunque con la firma?" )
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              Ah_ErrorMsg( "Il documento selezionato non pu� essere convertito in PDF" )
            endif
          else
             
 Local L_PathPdf 
 L_PathPdf=""
            * --- Se il pdf viene generato correttamente (senza invio e_mail) oppure se proveniamo da GSAL_KDA
            if Crea_Invio_Pdf(this.w_GFPATHFILE, this.w_GFNOMEFILE, this.w_GFCLASDOCU, this.w_GFORIGFILE, this.w_CDMODALL, @L_PathPdf, "") OR this.w_NoVisMenu
              this.w_PathPdfCreato = L_PathPdf
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Eliminazione pdf
              if cp_fileexist(this.w_PathPdfCreato)
                ERASE(this.w_PathPdfCreato)
              endif
            endif
          endif
      endcase
    else
      * --- Se documento PDF non appare men� a tendina ma viene eseguita solo la firma
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Se esiste il documento firmato (.P7M) pone la domanda se creare o meno l'indice
    if this.GIA_FIRMATO
      Ah_ErrorMsg( "Il documento firmato era gi� esistente%0%1%0Nessun indice sar� creato","i",,this.w_FILEFIRMD )
    endif
    if NOT this.GIA_FIRMATO and cp_fileexist(this.w_FILEFIRMD) AND ( this.w_NoVisMenu OR IsAlt() OR AH_YESNO("Vuoi creare l'indice del documento firmato?")) 
      * --- Duplicazione documento
      if this.w_RETVAL = 2 and this.w_OKPDF
        this.w_OGGETT = ALLTRIM(this.w_GFDESCFILE) + "  (PDF firmato)"
      else
        this.w_OGGETT = ALLTRIM(this.w_GFDESCFILE) + "  (firmato)"
      endif
      * --- Lettura Note aggiuntive Tipologia e Classe allegato
      if this.w_NoVisMenu
        * --- Proveniamo da GSAL_KDA
        this.w_TIPOALLE = this.w_ParTipoAlle
        this.w_CLASSEALL = this.w_ParClasseAlle
      else
        * --- Read from PROMINDI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ID_TESTO,IDTIPALL,IDCLAALL"+;
            " from "+i_cTable+" PROMINDI where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_GFKEYINDIC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ID_TESTO,IDTIPALL,IDCLAALL;
            from (i_cTable) where;
                IDSERIAL = this.w_GFKEYINDIC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TESTO = NVL(cp_ToDate(_read_.ID_TESTO),cp_NullValue(_read_.ID_TESTO))
          this.w_TIPOALLE = NVL(cp_ToDate(_read_.IDTIPALL),cp_NullValue(_read_.IDTIPALL))
          this.w_CLASSEALL = NVL(cp_ToDate(_read_.IDCLAALL),cp_NullValue(_read_.IDCLAALL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_INARCHIVIO = "X"
      L_SerIndex=""
      Private L_ArrParam
      dimension L_ArrParam(30)
      L_ArrParam(1)="AR"
      L_ArrParam(2)=this.w_FILEFIRMD
      L_ArrParam(3)=this.w_LICLADOC
      L_ArrParam(4)=i_CODUTE
      L_ArrParam(5)=this.oParentObject
      L_ArrParam(6)=this.w_ARCHIVIO
      L_ArrParam(7)=this.w_CHIAVE
      L_ArrParam(8)=this.w_OGGETT
      L_ArrParam(9)=this.w_TESTO
      L_ArrParam(10)=.F.
      L_ArrParam(11)=.F.
      L_ArrParam(12)=" "
      L_ArrParam(13)=" "
      L_ArrParam(14)=.null.
      L_ArrParam(15)=this.w_TIPOALLE
      L_ArrParam(16)=this.w_CLASSEALL
      L_ArrParam(17)=" "
      L_ArrParam(18)=" "
      L_ArrParam(19)="U"
      L_ArrParam(21)=.T.
      L_ArrParam(22)=this.w_FILE_ORIG
      GSUT_BBA(this,@L_ArrParam, @L_SerIndex)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Dopo la copia cancello il file
      L_SerIndex=LEFT(L_SerIndex, AT("*", L_SerIndex)-1)
      if Not empty(L_SerIndex)
        * --- Read from PROMINDI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IDMODALL,IDPATALL,IDNOMFIL"+;
            " from "+i_cTable+" PROMINDI where ";
                +"IDSERIAL = "+cp_ToStrODBC(L_SerIndex);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IDMODALL,IDPATALL,IDNOMFIL;
            from (i_cTable) where;
                IDSERIAL = L_SerIndex;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MODALL = NVL(cp_ToDate(_read_.IDMODALL),cp_NullValue(_read_.IDMODALL))
          this.w_PATALL = NVL(cp_ToDate(_read_.IDPATALL),cp_NullValue(_read_.IDPATALL))
          this.w_NOMFIL = NVL(cp_ToDate(_read_.IDNOMFIL),cp_NullValue(_read_.IDNOMFIL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_MODALL="F" AND ADDBS(JUSTPATH(this.w_FILEFIRMD)) <> ADDBS(Alltrim(this.w_PATALL)) and Not Empty(JUSTdrive(this.w_PATALL)) AND Cp_fileexist(this.w_FILEFIRMD) and Cp_fileexist(ADDBS(Alltrim(this.w_PATALL))+JUSTFNAME(this.w_FILEFIRMD))
          * --- Se il file firmato creato preventivamente � stato creato in un altra directory  rispetto alla generazione dell'indice 
          *     elimino il primo
          DELETE FILE (this.w_FILEFIRMD)
        endif
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- w_FILE_ORIG serve per GSUT_BBA
    if !EMPTY(this.w_PathPdfCreato)
      * --- Il file � un PDF ottenuto da Crea_Invio_Pdf
      this.w_FILE = Alltrim(this.w_PathPdfCreato)
    else
      * --- Documento da firmare
      this.w_FILE = addbs(ALLTRIM(this.w_GFPATHFILE))+ALLTRIM(this.w_GFNOMEFILE)
    endif
    this.w_FILE_ORIG = this.w_FILE
    this.w_FILEFIRMD = Alltrim(this.w_FILE) + ".P7M"
    * --- Se documento gi� firmato
    if empty(justdrive(this.w_FILE))
      this.w_FILE = Fullpath(this.w_FILE)
    endif
    if cp_fileexist(this.w_FILEFIRMD)
      this.GIA_FIRMATO = .T.
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Firma
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Procedura di firma del documento
    if !this.GIA_FIRMATO OR Ah_Yesno( "Attenzione! Il documento firmato � gi� esistente%0Si desidera proseguire comunque con la firma?")
      if this.w_FLGDIK="S"
        GSUT_BFF(this,this.w_FILE)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_FILEFIRMD = FirDigDoc(JustPath(this.w_FILE), JustPath(this.w_FILE), JustFName(this.w_FILE),,.T.,.T.)
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pEstensione,w_GFPATHFILE,w_GFNOMEFILE,w_GFCLASDOCU,w_GFORIGFILE,w_CDMODALL,w_LICLADOC,w_GFDESCFILE,w_GFKEYINDIC,w_Archivio,w_Chiave,w_NoVisMenu,w_ParTipoAlle,w_ParClasseAlle)
    this.pEstensione=pEstensione
    this.w_GFPATHFILE=w_GFPATHFILE
    this.w_GFNOMEFILE=w_GFNOMEFILE
    this.w_GFCLASDOCU=w_GFCLASDOCU
    this.w_GFORIGFILE=w_GFORIGFILE
    this.w_CDMODALL=w_CDMODALL
    this.w_LICLADOC=w_LICLADOC
    this.w_GFDESCFILE=w_GFDESCFILE
    this.w_GFKEYINDIC=w_GFKEYINDIC
    this.w_Archivio=w_Archivio
    this.w_Chiave=w_Chiave
    this.w_NoVisMenu=w_NoVisMenu
    this.w_ParTipoAlle=w_ParTipoAlle
    this.w_ParClasseAlle=w_ParClasseAlle
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PROMINDI'
    this.cWorkTables[2]='PAR_ALTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEstensione,w_GFPATHFILE,w_GFNOMEFILE,w_GFCLASDOCU,w_GFORIGFILE,w_CDMODALL,w_LICLADOC,w_GFDESCFILE,w_GFKEYINDIC,w_Archivio,w_Chiave,w_NoVisMenu,w_ParTipoAlle,w_ParClasseAlle"
endproc
