* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: castivarate                                                     *
*              Calcola castelletto IVA e rate                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_68]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-28                                                      *
* Last revis.: 2015-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pDOCINFO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcastivarate",oParentObject,m.pDOCINFO)
return(i_retval)

define class tcastivarate as StdBatch
  * --- Local variables
  pDOCINFO = .NULL.
  w_RETURN = .NULL.
  w_MVSERIAL = space(10)
  w_MVFLRINC = space(1)
  w_MVFLRIMB = space(1)
  w_MVCAUIMB = 0
  w_MVRIFACC = space(10)
  w_RITATT = 0
  w_AFFLINTR = space(1)
  w_BOLCAU = space(1)
  w_MVIVACAU = space(5)
  w_MVCAUIMB = 0
  w_REVCAU = space(1)
  w_TROV = .f.
  w_IMPSPE = 0
  w_ACCMAN = .f.
  w_PAINCASS = space(1)
  w_PASALDO = space(1)
  w_TROVPAG = 0
  w_TROVSAL = 0
  w_TOTFATTU = 0
  w_SALDFAT = 0
  MAXALIVA = 0
  w_MAXBOL = 0
  w_APPO = 0
  w_ESCL1 = space(4)
  w_ESCL2 = space(4)
  w_TOTIMPON = 0
  w_TOTIMPOS = 0
  w_APPIMB = 0
  w_APPTRA = 0
  w_FLOMAG = space(1)
  w_FLESEN = .f.
  w_ARRSUP = 0
  w_ALIVA = 0
  w_CODICE = space(20)
  w_TIPRIG = space(1)
  w_ROWORD = 0
  w_CODART = space(20)
  w_TCODIVA = space(3)
  w_TUNIMIS = space(3)
  w_TQTAMOV = 0
  w_TQTAUM1 = 0
  w_TPERIVA = 0
  w_TVALMAG = 0
  w_TVALRIG = 0
  w_TBOLIVA = 0
  w_FLSERA = space(1)
  w_PERIVE = 0
  w_CODIVA = space(5)
  w_MVCODIVE = space(5)
  w_BOLIVE = space(1)
  w_ACQAUT = space(1)
  w_REVCHA = space(1)
  w_MVFLSCOR = space(1)
  w_TIPREG = space(1)
  w_DECTOT = 0
  w_FLFOSC = space(1)
  w_MVCODPAG = space(5)
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVSCONTI = 0
  w_MVDATDOC = ctod("  /  /  ")
  w_IMPARR = 0
  w_MVFLVEAC = space(1)
  w_MVIVAARR = space(5)
  w_MVIMPARR = 0
  w_PEIARR = 0
  w_MVSPETRA = 0
  w_MVSPEINC = 0
  w_MVSPEIMB = 0
  w_MVIVATRA = space(5)
  w_MVIVAIMB = space(5)
  w_MVIVAINC = space(5)
  w_APPINC = 0
  w_PEITRA = 0
  w_BOLTRA = space(1)
  w_PEIIMB = 0
  w_BOLIMB = space(1)
  w_PEIINC = 0
  w_BOLINC = space(1)
  w_MVFLRTRA = space(1)
  w_ACQINT = space(1)
  w_MVTIPCON = space(1)
  w_MVCODORN = space(15)
  w_TIPDOC = space(2)
  w_MVCLADOC = space(2)
  w_MVCODVAL = space(3)
  w_MVSPEBOL = 0
  w_BOLSUP = 0
  w_BOLESE = 0
  w_RITPRE = 0
  w_MVACCPRE = 0
  w_MVTOTENA = 0
  w_MVTOTRIT = 0
  w_MVACCONT = 0
  w_MVACCSUC = 0
  w_FLINCA = space(1)
  w_MVPERFIN = 0
  w_MVIMPFIN = 0
  w_MVFLSFIN = space(1)
  w_GIOFIS = 0
  w_GIORN1 = 0
  w_GIORN2 = 0
  w_MESE1 = 0
  w_MESE2 = 0
  w_TOTRIP = 0
  w_LOOP = 0
  w_MVSCOPAG = 0
  w_NURATE = 0
  w_MVDATDIV = ctod("  /  /  ")
  w_MVFLSALD = space(1)
  w_CLBOLFAT = space(1)
  w_BOLMIN = 0
  w_BOLARR = 0
  w_BOLCAM = 0
  w_MVIVABOL = space(5)
  w_MVFLSCOM = space(1)
  w_TOTSCO = 0
  w_DTVALMAG = 0
  w_TROV = .f.
  w_RESTO_SCO = 0
  w_ROW_RESTO_SCO = 0
  w_TOT_IMP_SCO = 0
  w_TmpN_SCO = 0
  w_TmpIVA_SCO = 0
  w_pCODIVAS = space(3)
  w_pPERIVA = 0
  w_pIMPSPE = 0
  w_pFLESEN = space(1)
  w_TmpTOTIMP = 0
  mcTmpIva = .null.
  w_FIRSTLAP = .f.
  w_REVCHA = space(1)
  w_TOTIMPN = 0
  w_TOTIMPND = 0
  w_NDIFF = 0
  w_IMPORTO = 0
  w_INDEX = 0
  w_TmpN = 0
  w_TmpIVA = 0
  w_TmpLen = 0
  w_CALRATE = .f.
  w_VALRIG = 0
  w_RATETOT = 0
  w_RATEVA = space(1)
  w_IMPNET = 0
  w_IMPIVA = 0
  w_APPO2 = space(10)
  w_APPO3 = space(5)
  * --- WorkFile variables
  CONTI_idx=0
  MOD_PAGA_idx=0
  VOCIIVA_idx=0
  AZIENDA_idx=0
  CAN_TIER_idx=0
  PAG_AMEN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruzione Castelletto IVA
    * --- Parametri:
    *                     pDOCINFO as DOCOBJ E' un oggetto che virtualizza un documento
    *                                                            ( testata e dettaglio tramite due memorycursor )
    this.w_RETURN=createobject("CASTIVA",this)
    * --- Costante, numero massimo di aliquote iva gestite
    this.MAXALIVA = 6
    this.w_RETURN.nErrorLog = ""
    * --- Valorizzo la variabile per gli eventuali filtri
    this.w_MVSERIAL = this.pDOCINFO.cMVSERIAL
    if this.pDOCINFO.cRead $ "HA"
      * --- Lettura dati Testata
      *     Valorizzo oParentObject con this affinch� possa utilizzare nella query, che 
      *     popola il MemCursor, la variabile w_MVSERIAL
      *     Cambio l'oggetto di riferimento per i filtri con this
      this.pDOCINFO.mcHeader.oParentObject = This
      this.pDOCINFO.mcHeader.FillFromQuery()     
    endif
    if this.pDOCINFO.cRead $ "DA"
      * --- Lettura dati Dettaglio
      *     Valorizzo oParentObject con this affinch� possa utilizzare nella query, che 
      *     popola il MemCursor, la variabile w_MVSERIAL
      this.pDOCINFO.mcDetail.oParentObject = this
      this.pDOCINFO.mcDetail.FillFromQuery()     
    endif
    * --- Leggo le informazioni di testata
    this.w_RITATT = 0
    this.pDOCINFO.mcHeader.gotop()     
    this.w_FLESEN = NOT EMPTY(Nvl(this.pDOCINFO.mcHeader.MVCODIVE,Space(5)))
    this.w_MVCODIVE = Nvl(this.pDOCINFO.mcHeader.MVCODIVE,Space(5))
    this.w_PERIVE = Nvl(this.pDOCINFO.mcHeader.PERIVE,0)
    this.w_BOLIVE = Nvl(this.pDOCINFO.mcHeader.BOLIVE," ")
    this.w_MVFLSCOR = Nvl(this.pDOCINFO.mcHeader.MVFLSCOR," ")
    this.w_MVFLSCOM = Nvl(this.pDOCINFO.mcHeader.MVFLSCOM," ")
    this.w_TIPREG = Nvl(this.pDOCINFO.mcHeader.CCTIPREG," ")
    this.w_TIPDOC = Nvl(this.pDOCINFO.mcHeader.CCTIPDOC,"  ")
    this.w_ACQAUT = IIF(this.w_TIPDOC $ "AU-NU","S","N")
    this.w_DECTOT = Nvl(this.pDOCINFO.mcHeader.DECTOT,0)
    this.w_FLFOSC = Nvl(this.pDOCINFO.mcHeader.MVFLFOSC," ")
    this.w_MVCODPAG = Nvl(this.pDOCINFO.mcHeader.MVCODPAG,Space(5))
    this.w_MVSCOCL1 = Nvl(this.pDOCINFO.mcHeader.MVSCOCL1,0)
    this.w_MVSCOCL2 = Nvl(this.pDOCINFO.mcHeader.MVSCOCL2,0)
    this.w_MVSCOPAG = Nvl(this.pDOCINFO.mcHeader.MVSCOPAG,0)
    this.w_MVSCONTI = Nvl(this.pDOCINFO.mcHeader.MVSCONTI,0)
    this.w_MVDATDOC = cp_todate(this.pDOCINFO.mcHeader.MVDATDOC)
    this.w_MVFLVEAC = Nvl(this.pDOCINFO.mcHeader.MVFLVEAC," ")
    this.w_MVIVAARR = Nvl(this.pDOCINFO.mcHeader.MVIVAARR,Space(5))
    this.w_MVIMPARR = Nvl(this.pDOCINFO.mcHeader.MVIMPARR,0)
    this.w_PEIARR = Nvl(this.pDOCINFO.mcHeader.PEIARR,0)
    this.w_MVSPETRA = Nvl(this.pDOCINFO.mcHeader.MVSPETRA,0)
    this.w_MVSPEINC = Nvl(this.pDOCINFO.mcHeader.MVSPEINC,0)
    this.w_MVSPEIMB = Nvl(this.pDOCINFO.mcHeader.MVSPEIMB,0)
    this.w_MVIVATRA = Nvl(this.pDOCINFO.mcHeader.MVIVATRA,Space(5))
    this.w_MVIVAIMB = Nvl(this.pDOCINFO.mcHeader.MVIVAIMB,SPace(5))
    this.w_MVIVAINC = Nvl(this.pDOCINFO.mcHeader.MVIVAINC,Space(5))
    this.w_PEITRA = Nvl(this.pDOCINFO.mcHeader.PEITRA,0)
    this.w_BOLTRA = Nvl(this.pDOCINFO.mcHeader.BOLTRA," ")
    this.w_PEIIMB = Nvl(this.pDOCINFO.mcHeader.PEIIMB,0)
    this.w_BOLIMB = Nvl(this.pDOCINFO.mcHeader.BOLIMB," ")
    this.w_PEIINC = Nvl(this.pDOCINFO.mcHeader.PEIINC,0)
    this.w_BOLINC = Nvl(this.pDOCINFO.mcHeader.BOLINC," ")
    this.w_MVFLRTRA = Nvl(this.pDOCINFO.mcHeader.MVFLRTRA," ")
    this.w_MVCODORN = Nvl(this.pDOCINFO.mcHeader.MVCODORN,Space(15))
    this.w_MVCLADOC = Nvl(this.pDOCINFO.mcHeader.MVCLADOC,"  ")
    this.w_MVCODVAL = Nvl(this.pDOCINFO.mcHeader.MVCODVAL,Space(5))
    this.w_MVTIPCON = Nvl(this.pDOCINFO.mcHeader.MVTIPCON," ")
    this.w_MVFLRINC = Nvl(this.pDOCINFO.mcHeader.MVFLRINC," ")
    this.w_MVFLRIMB = Nvl(this.pDOCINFO.mcHeader.MVFLRIMB," ")
    if NOT EMPTY(this.w_MVCODORN)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AFFLINTR,ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANGIOFIS"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.w_MVCODORN);
              +" and ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AFFLINTR,ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANGIOFIS;
          from (i_cTable) where;
              ANCODICE = this.w_MVCODORN;
              and ANTIPCON = this.w_MVTIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AFFLINTR = NVL(cp_ToDate(_read_.AFFLINTR),cp_NullValue(_read_.AFFLINTR))
        this.w_GIORN1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
        this.w_GIORN2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
        this.w_MESE1 = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
        this.w_MESE2 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
        this.w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ACQINT = IIF(this.w_AFFLINTR="S" AND this.w_MVFLVEAC="A" AND (this.w_TIPDOC $ "FE-NE" OR this.w_MVCLADOC $ "DI-DT"), "S", "N")
    else
      this.w_GIOFIS = Nvl(this.pDOCINFO.mcHeader.GIOFIS,0)
      this.w_GIORN1 = Nvl(this.pDOCINFO.mcHeader.GIORN1,0)
      this.w_GIORN2 = Nvl(this.pDOCINFO.mcHeader.GIORN2,0)
      this.w_MESE1 = Nvl(this.pDOCINFO.mcHeader.MESE1,0)
      this.w_MESE2 = Nvl(this.pDOCINFO.mcHeader.MESE2,0)
      this.w_ACQINT = Nvl(this.pDOCINFO.mcHeader.ACQINT," ")
    endif
    this.w_BOLSUP = Nvl(this.pDOCINFO.mcHeader.BOLSUP,0)
    this.w_BOLESE = Nvl(this.pDOCINFO.mcHeader.BOLESE,0)
    this.w_RITPRE = Nvl(this.pDOCINFO.mcHeader.MVRITPRE,0)
    this.w_MVACCPRE = Nvl(this.pDOCINFO.mcHeader.MVACCPRE,0)
    this.w_MVTOTENA = Nvl(this.pDOCINFO.mcHeader.MVTOTENA,0)
    this.w_MVTOTRIT = Nvl(this.pDOCINFO.mcHeader.MVTOTRIT,0)
    this.w_MVACCONT = Nvl(this.pDOCINFO.mcHeader.MVACCONT,0)
    this.w_MVACCSUC = Nvl(this.pDOCINFO.mcHeader.MVACCSUC,0)
    this.w_FLINCA = Nvl(this.pDOCINFO.cFLINCA," ")
    this.w_TROVPAG = Nvl(this.pDOCINFO.nTROVPAG,0)
    this.w_TROVSAL = Nvl(this.pDOCINFO.nTROVSAL,0)
    this.w_MVPERFIN = Nvl(this.pDOCINFO.mcHeader.MVPERFIN,0)
    this.w_MVIMPFIN = Nvl(this.pDOCINFO.mcHeader.MVIMPFIN,0)
    this.w_MVFLSFIN = Nvl(this.pDOCINFO.mcHeader.MVFLSFIN," ")
    this.w_MVDATDIV = Cp_todate(this.pDOCINFO.mcHeader.MVDATDIV)
    this.w_MVFLSALD = Nvl(this.pDOCINFO.mcHeader.MVFLSALD," ")
    this.w_CLBOLFAT = Nvl(this.pDOCINFO.mcHeader.CLBOLFAT," ")
    this.w_BOLMIN = Nvl(this.pDOCINFO.mcHeader.BOLMIN,0)
    this.w_BOLARR = Nvl(this.pDOCINFO.mcHeader.BOLARR,0)
    this.w_BOLCAM = Nvl(this.pDOCINFO.mcHeader.BOLCAM,0)
    this.w_MVSPEBOL = Nvl(this.pDOCINFO.mcHeader.MVSPEBOL,0)
    this.w_MVIVABOL = Nvl(this.pDOCINFO.mcHeader.MVIVABOL,Space(5))
    this.w_MVCAUIMB = Nvl(this.pDOCINFO.mcHeader.MVCAUIMB,0)
    this.w_BOLCAU = Nvl(this.pDOCINFO.mcHeader.BOLCAU," ")
    this.w_MVIVACAU = Nvl(this.pDOCINFO.mcHeader.MVIVACAU,Space(5))
    this.w_REVCAU = Nvl(this.pDOCINFO.mcHeader.REVCAU," ")
    this.w_MVRIFACC = Nvl(this.pDOCINFO.mcHeader.MVRIFACC,Space(10))
    this.w_RITATT = Nvl(this.pDOCINFO.mcHeader.MVRITATT,0)
    * --- Variabili x calcolo Arrotondamenti
    this.w_ARRSUP = IIF(this.w_DECTOT=0, .499, .00499)
    this.w_ARRSUP = IIF(this.w_DECTOT=1, .0499, this.w_ARRSUP)
    this.w_ARRSUP = IIF(this.w_DECTOT=2, .00499, this.w_ARRSUP)
    this.w_ARRSUP = IIF(this.w_DECTOT=3, .000499, this.w_ARRSUP)
    * --- Aliquote IVA, conterr� il numero totale di aliquote IVA
    this.w_ALIVA = 0
    if Not this.pDOCINFO.cSoloRate
      * --- Leggo righe dettaglio e popolo il castelletto
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Calcoli Castelletto IVA
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Aggiungo riga castelletto relativa alle cauzioni imballaggi se previste
      if this.w_MVCAUIMB<>0
        if Empty( this.w_MVIVACAU )
          this.w_RETURN.nErrorLog = "Codice IVA cauzione non definito: inserirlo nei parametri vendite"
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_TROV = .f.
          do while not this.w_RETURN.mcCastiva.eof()
            * --- Aliquota gia' esistente ?
            if this.w_RETURN.mcCastiva.CODIVA= this.w_MVIVACAU AND this.w_RETURN.mcCastiva.FLOMAG = "X"
              * --- Sommo all'imponibile il valore di riga
              *     Aggiungo il valore di riga anche al Lordo (se scorporo piede fattura)
              *     questa colonna ha senso..
               
 this.w_RETURN.mcCastiva.IMPON = this.w_RETURN.mcCastiva.IMPON + this.w_MVCAUIMB 
 this.w_RETURN.mcCastiva.LORDO = this.w_RETURN.mcCastiva.LORDO + this.w_MVCAUIMB 
 this.w_RETURN.mcCastiva.IMPDET=this.w_RETURN.mcCastiva.IMPDET + this.w_MVCAUIMB
              this.w_TROV = .T.
              * --- Esco..
              Exit
            endif
            this.w_RETURN.mcCastiva.Next()     
          enddo
          if Not this.w_Trov
            * --- Se aliquota inesistente
            this.w_ALIVA = this.w_ALIVA + 1
            if this.w_ALIVA > 6
              this.w_RETURN.nErrorLog = "Consentite massimo 6 aliquote"
              this.Page_9()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Aggiunge nuova aliquota
              this.w_RETURN.mcCastiva.AppendBlank()     
              this.w_RETURN.mcCastiva.CODIVA = this.w_MVIVACAU
              * --- Se Esente o Doc. di Acquisto INTRA (FLESEN)
               
 this.w_RETURN.mcCastiva.PERIVA = 0 
 this.w_RETURN.mcCastiva.FLESEN = IIF(this.w_TIPREG="E", " ", IIF(this.w_FLESEN , this.w_BOLIVE, this.w_BOLCAU)) 
 this.w_RETURN.mcCastiva.FLOMAG="X" 
 this.w_RETURN.mcCastiva.REVCHA=IIF(this.w_ACQAUT="S", this.w_REVCAU," ") 
 this.w_RETURN.mcCastiva.IMPDET = this.w_MVCAUIMB 
 this.w_RETURN.mcCastiva.IMPON = this.w_MVCAUIMB 
 this.w_RETURN.mcCastiva.LORDO = this.w_MVCAUIMB
              this.w_RETURN.mcCastiva.SaveCurrentRecord()     
            endif
          endif
        endif
      endif
      * --- Per calcolo Bolli Esenti
      this.w_MAXBOL = 0
      this.w_IMPSPE = 0
      this.w_RETURN.mcCastiva.gotop()     
      do while not this.w_RETURN.mcCastiva.eof()
        * --- Se no Omaggio
        if this.w_RETURN.mcCastiva.FLOMAG = "X"
          this.w_TOTIMPON = this.w_TOTIMPON + this.w_RETURN.mcCastiva.IMPON
          * --- Se Scorporo Fine Fattura Le Spese vengono calcolate sulle Aliquote
          this.w_IMPSPE = this.w_IMPSPE + (this.w_RETURN.mcCastiva.SPEINC + this.w_RETURN.mcCastiva.SPEIMB + this.w_RETURN.mcCastiva.SPETRA + this.w_RETURN.mcCastiva.IMPARR)
        endif
        * --- Se No Omaggio di IVA calcola Totale Imposta
        if this.w_RETURN.mcCastiva.FLOMAG $ "X-I"
          this.w_TOTIMPOS = this.w_TOTIMPOS + this.w_RETURN.mcCastiva.IMPOS
        endif
        * --- Se % IVA = 0 e Gestisce Bolli Esenti - Calcola Totale Importi Esenti su cui viene calcolato il Bollo
        this.w_MAXBOL = this.w_MAXBOL + IIF(this.w_RETURN.mcCastiva.FLESEN="S" AND this.w_RETURN.mcCastiva.PERIVA=0, this.w_RETURN.mcCastiva.IMPON, 0)
        this.w_RETURN.mcCastiva.Next()     
      enddo
      * --- Calcola i Bolli Esenti, di default le vendite hanno i bolli esenti.
      *     Utilizzo una variabile di appoggio FLFOBO perch� in futuro potr� essere
      *     resa editabile sui documenti
      if this.w_MVFLVEAC="V"
        this.w_MVSPEBOL = IIF(this.w_MAXBOL>this.w_BOLSUP, this.w_BOLESE, 0)
      endif
      if this.w_MVCLADOC="FA" AND this.w_MVFLVEAC="V"
        this.w_ACCMAN = .F.
        if NVL(this.w_TROVPAG,0)=2 and NVL(this.w_MVACCONT,0) <>0
          this.w_ACCMAN = .T.
        endif
        * --- Se l'acconto � stato modificato manualmente ed � diverso da 0 non lo ricalcolo in automatico
        if this.w_TROVPAG<>3
          * --- Read from PAG_AMEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PAINCASS,PASALDO"+;
              " from "+i_cTable+" PAG_AMEN where ";
                  +"PACODICE = "+cp_ToStrODBC(this.w_MVCODPAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PAINCASS,PASALDO;
              from (i_cTable) where;
                  PACODICE = this.w_MVCODPAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PAINCASS = NVL(cp_ToDate(_read_.PAINCASS),cp_NullValue(_read_.PAINCASS))
            this.w_PASALDO = NVL(cp_ToDate(_read_.PASALDO),cp_NullValue(_read_.PASALDO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_TROVPAG = IIF(this.w_PAINCASS="S", 1,0)
          this.w_TROVSAL = IIF(this.w_PASALDO="S", 1,0)
        endif
        if this.w_ACCMAN
          if this.w_TROVPAG=1
            if this.w_TROVPAG=1 and this.w_TROVSAL=1 and this.w_MVIMPFIN=0
              Ah_ErrorMsg("Il pagamento ha attivo il check a saldo, la differenza rispetto all'acconto verr� considerata come sconto finanziario")
            endif
            this.w_TROVPAG = 2
          endif
        endif
        if (this.w_TROVPAG=1 OR this.w_TROVPAG=3 ) AND !(this.w_MVCLADOC="RF" AND this.w_FLINCA="S")
          this.w_MVACCONT = (this.w_TOTIMPON + this.w_TOTIMPOS + this.w_MVSPEBOL + this.w_MVIMPARR) - this.w_MVACCPRE
        endif
      endif
      * --- Se Chiamato dai Documenti e Ricevuta Fiscale e Incassata...
      if this.w_MVCLADOC="RF" AND this.w_FLINCA="S"
        this.w_SALDFAT = this.w_TOTFATTU-(this.w_MVACCONT+this.w_MVACCPRE)
        if (this.pDOCINFO.cAzione="Load" ) OR (this.pDOCINFO.cAzione="Edit" and (this.w_SALDFAT<= 0 ) and EMPTY(this.w_MVRIFACC)) 
          this.w_MVACCONT = (this.w_TOTIMPON + this.w_TOTIMPOS + this.w_MVSPEBOL + this.w_MVIMPARR) - this.w_MVACCSUC
          this.w_MVFLSALD = " "
        endif
      endif
      * --- Calcola Eventuali Rate +
      *     Calcolo Spese Bolli se tipo pagamento Tratta
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Calcola Bolli x Castelletto IVA
      if this.w_MVSPEBOL <> 0
        if EMPTY(this.w_MVIVABOL)
          if this.w_MVFLVEAC="V"
            if EMPTY(g_COIBOL)
              this.w_RETURN.nErrorLog = "Codice IVA spese bolli non definito: inserirlo nei parametri vendite"
              this.w_MVSPEBOL = 0
            else
              this.w_RETURN.nErrorLog = "Inserire codice IVA spese bolli"
            endif
          else
            this.w_RETURN.nErrorLog = "Inserire codice IVA spese bolli"
          endif
          * --- Azzero le spese perch� il totale documento non vada in negativo
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_TROV = .F.
          this.w_RETURN.mcCastiva.gotop()     
          do while not this.w_RETURN.mcCastiva.eof()
            if this.w_MVIVABOL = this.w_RETURN.mcCastiva.CODIVA AND this.w_RETURN.mcCastiva.FLOMAG = "X"
              * --- Aliquota gia' esistente
              this.w_RETURN.mcCastiva.IMPON = this.w_RETURN.mcCastiva.IMPON + this.w_MVSPEBOL
              this.w_RETURN.mcCastiva.LORDO = this.w_RETURN.mcCastiva.LORDO + this.w_MVSPEBOL
              this.w_TROV = .T.
              EXIT
            endif
            this.w_RETURN.mcCastiva.Next()     
          enddo
        endif
        if not this.w_TROV
          * --- Inserisco una nuova riga nel castelleto IVA
          * --- Calcola Riga Castelletto IVA Esente
          this.w_ALIVA = this.w_ALIVA + 1
          * --- Testa se raggiunte max aliquote
          if this.w_ALIVA > this.MAXALIVA
            this.w_RETURN.nErrorLog = ah_MsgFormat("Consentite massimo %1 aliquote", ALLTRIM(STR(this.MAXALIVA)))
            this.Page_9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Aggiunge nuova aliquota
            this.w_RETURN.mcCastiva.AppendBlank()     
            this.w_RETURN.mcCastiva.CODIVA = this.w_MVIVABOL
            this.w_RETURN.mcCastiva.IMPON = this.w_MVSPEBOL
            this.w_RETURN.mcCastiva.LORDO = this.w_MVSPEBOL
            this.w_RETURN.mcCastiva.SPEINC = 0
            this.w_RETURN.mcCastiva.SPEIMB = 0
            this.w_RETURN.mcCastiva.SPETRA = 0
            this.w_RETURN.mcCastiva.PERIVA = 0
            this.w_RETURN.mcCastiva.IMPOS = 0
            this.w_RETURN.mcCastiva.FLOMAG = "X"
            this.w_RETURN.mcCastiva.REVCHA = " "
            this.w_RETURN.mcCastiva.SaveCurrentRecord()     
          endif
        endif
      endif
      this.w_TOTFATTU = this.w_TOTIMPON + this.w_TOTIMPOS + this.w_MVSPEBOL
      * --- Mi posiziono sulla prima riga affinch� venga salvato nel cursore tutte le righe
      this.w_RETURN.mcCastiva.gotop()     
    else
      this.w_TOTIMPOS = this.pDOCINFO.nTOTIMPOS
      this.w_TOTIMPON = this.pDOCINFO.nTOTIMPON
      this.w_IMPSPE = this.pDOCINFO.nIMPSPE
      this.w_MVSPEBOL = this.pDOCINFO.nMVSPEBOL
      this.w_MVACCONT = iif(this.pDOCINFO.nMVACCONT<>0 OR this.w_TROVPAG=2, this.pDOCINFO.nMVACCONT, this.w_MVACCONT)
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_RETURN.mcRate.gotop()     
    * --- Aggiorno totali
    this.Page_9()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dichiarazione Variabili
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura righe documento e prima valorizzazione del castelletto IVA
    this.w_TOTSCO = 0
    this.pDOCINFO.mcDetail.gotop()     
    do while not this.pDOCINFO.mcDetail.eof()
      this.w_CODICE = ALLTRIM(this.pDOCINFO.mcDetail.MVCODICE)
      this.w_TIPRIG = ALLTRIM(this.pDOCINFO.mcDetail.MVTIPRIG)
      if (NOT EMPTY(this.w_CODICE)) AND this.w_TIPRIG $ "RFMA"
        this.w_FLOMAG = ALLTRIM(this.pDOCINFO.mcDetail.MVFLOMAG)
        this.w_TCODIVA = ALLTRIM(this.pDOCINFO.mcDetail.MVCODIVA)
        this.w_TPERIVA = this.pDOCINFO.mcDetail.PERIVA 
        * --- Se generazione da corrispettivi lo sconto � gia corretto sulla riga
        *               (non lo ripartisco alla fine...)
        if this.w_MVFLSCOM<>"S" 
          this.w_TVALRIG = this.pDOCINFO.mcDetail.MVVALRIG+ this.pDOCINFO.mcDetail.MVIMPSCO
        else
          this.w_TVALRIG = this.pDOCINFO.mcDetail.MVVALRIG 
        endif
        this.w_TOTSCO = this.w_TOTSCO + this.pDOCINFO.mcDetail.MVIMPSCO
        this.w_FLSERA = ALLTRIM(this.pDOCINFO.mcDetail.FLSERA)
        * --- Calcolo Totale Righe Documento
        this.w_TOTRIP = this.w_TOTRIP + IIF(this.w_FLOMAG="X", this.w_TVALRIG, 0)
        if this.w_TVALRIG<>0 AND this.w_FLOMAG $ "XIE"
          * --- Netto Merce (o Lordo se Gestito Scorporo Piede Fattura)
          if this.w_FLESEN And this.w_PERIVE=0
            * --- Se codice IVA di testata esente, se la riga ha codice IVA esente utilizzo quella di riga per la costruzione del castelletto IVA
            this.w_CODIVA = IIF(this.w_TIPREG="E", SPACE(5), IIF(Nvl( this.w_TPERIVA,0)<>0 , this.w_MVCODIVE, this.w_TCODIVA))
          else
            * --- Se di riga ho un aliquota esente ha priorit� rispetto a quella di testata
            this.w_CODIVA = IIF(this.w_TIPREG="E", SPACE(5), IIF(this.w_FLESEN and Nvl( this.w_TPERIVA,0)<>0 , this.w_MVCODIVE, this.w_TCODIVA))
          endif
          if this.w_MVFLSCOR="S" And this.w_FLOMAG="X"
            this.w_DTVALMAG = this.pDOCINFO.mcDetail.MVVALMAG
          else
            this.w_DTVALMAG = 0
          endif
          * --- Calcolo aliquote castelletto IVA
          this.w_TROV = .F.
          this.w_RETURN.mcCastiva.gotop()     
          do while not this.w_RETURN.mcCastiva.eof()
            * --- Aliquota gia' esistente ?
            if alltrim(this.w_RETURN.mcCastiva.CODIVA) == alltrim(this.w_CODIVA) AND this.w_RETURN.mcCastiva.FLOMAG = this.w_FLOMAG
              * --- Sommo all'imponibile il valore di riga
              *     Aggiungo il valore di riga anche al Lordo (se scorporo piede fattura)
              *     questa colonna ha senso..
              if this.w_FLSERA="S"
                this.w_RETURN.mcCastiva.IMPSPE = this.w_RETURN.mcCastiva.IMPSPE + this.w_TVALRIG
                this.w_RETURN.mcCastiva.IMPDET = this.w_RETURN.mcCastiva.IMPDET + this.w_DTVALMAG
              else
                this.w_RETURN.mcCastiva.IMPON = this.w_RETURN.mcCastiva.IMPON + this.w_TVALRIG
                this.w_RETURN.mcCastiva.LORDO = this.w_RETURN.mcCastiva.LORDO + this.w_TVALRIG
                this.w_RETURN.mcCastiva.IMPDET = this.w_RETURN.mcCastiva.IMPDET + this.w_DTVALMAG
              endif
              this.w_TROV = .T.
              * --- Esco dal ciclo
              EXIT
            endif
            this.w_RETURN.mcCastiva.Next()     
          enddo
          if Not this.w_TROV
            * --- Se aliquota inesistente
            this.w_ALIVA = this.w_ALIVA + 1
            * --- Controllo se ho superato il numero massimo di aliquote
            if this.w_ALIVA > this.MAXALIVA
              this.w_RETURN.nErrorLog = ah_MsgFormat("Consentite massimo %1 aliquote", ALLTRIM(STR(this.MAXALIVA)))
              this.Page_9()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Aggiunge nuova aliquota
              this.w_RETURN.mcCastiva.AppendBlank()     
              this.w_RETURN.mcCastiva.CODIVA = this.w_CODIVA
              if this.w_ACQAUT="S" and Not Empty(this.w_CODIVA)
                * --- Verifico che il codice Iva sia di tipo Reverse Charge
                * --- Read from VOCIIVA
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.VOCIIVA_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "IVREVCHA"+;
                    " from "+i_cTable+" VOCIIVA where ";
                        +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    IVREVCHA;
                    from (i_cTable) where;
                        IVCODIVA = this.w_CODIVA;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_REVCHA = NVL(cp_ToDate(_read_.IVREVCHA),cp_NullValue(_read_.IVREVCHA))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              this.w_TBOLIVA = this.pDOCINFO.mcDetail.BOLIVA 
              * --- Se Esente o Doc. di Acquisto INTRA (FLESEN)
              this.w_RETURN.mcCastiva.PERIVA = IIF(this.w_FLESEN and Nvl( this.w_TPERIVA,0)<>0 , this.w_PERIVE, this.w_TPERIVA)
              this.w_RETURN.mcCastiva.FLESEN = IIF(this.w_TIPREG="E", " ", IIF(this.w_FLESEN and Nvl( this.w_TPERIVA,0)<>0 , this.w_BOLIVE, this.w_TBOLIVA))
              this.w_RETURN.mcCastiva.FLOMAG = this.w_FLOMAG
              this.w_RETURN.mcCastiva.REVCHA = this.w_REVCHA
              this.w_RETURN.mcCastiva.IMPDET = this.w_RETURN.mcCastiva.IMPDET + this.w_DTVALMAG
              if this.w_FLSERA="S"
                this.w_RETURN.mcCastiva.IMPSPE = this.w_TVALRIG
              else
                this.w_RETURN.mcCastiva.IMPON = this.w_TVALRIG
                this.w_RETURN.mcCastiva.LORDO = this.w_TVALRIG
              endif
              this.w_RETURN.mcCastiva.SaveCurrentRecord()     
            endif
          endif
        endif
      endif
      this.pDOCINFO.mcDetail.Next()     
    enddo
    * --- Se scorporo a piede fattura, al termine della lettura del dettaglio applico lo scorporo
    *     sulla colonna imponibile (calcolata come VALRIG)
    if this.w_MVFLSCOR="S" And this.w_TIPREG<>"E"
      this.w_RETURN.mcCastiva.gotop()     
      do while not this.w_RETURN.mcCastiva.eof()
        this.w_RETURN.mcCastiva.IMPON = CALNET( this.w_RETURN.mcCastiva.IMPON, this.w_RETURN.mcCastiva.PERIVA , this.w_DECTOT , this.w_MVCODIVE , this.w_PERIVE)
        this.w_RETURN.mcCastiva.Next()     
      enddo
    endif
    * --- Se ho sconti di piede e non � stata fatta la ripartizione  li vado ad applicare all'imponibile nel castelletto IVA
    if (this.w_MVFLSCOM="S" OR this.w_TOTSCO =0) And (this.w_MVSCOCL1<>0 Or this.w_MVSCOCL2<>0 Or this.w_MVSCOPAG<>0 Or this.w_FLFOSC="S" )
      * --- Gestione resto nel caso di ripartizione sconti forzati..
      this.w_RESTO_SCO = this.w_MVSCONTI
      if this.w_FLFOSC="S"
        * --- Calcolo totale lordo o netto da utilizzare come monte per effettuare
        *     la ripartizione dello sconto che pu� essere lordo (scorporo) o netto
        this.w_TOT_IMP_SCO = 0
        this.w_RETURN.mcCastiva.gotop()     
        do while not this.w_RETURN.mcCastiva.eof()
          if this.w_RETURN.mcCastiva.FLOMAG="X" 
            this.w_TOT_IMP_SCO = this.w_TOT_IMP_SCO + iif( this.w_MVFLSCOR="S" , this.w_RETURN.mcCastiva.LORDO , this.w_RETURN.mcCastiva.IMPON )
          endif
          this.w_RETURN.mcCastiva.Next()     
        enddo
      endif
      * --- Se calcolo sconti su omaggi scorro il castelletto IVA e riapplico gli sconti
      *     di piede agli elementi di tipo omaggio imponibile e imponibile+iva
      *     Questo trattamento � applicato cmq sulle righe normali
      this.w_RETURN.mcCastiva.gotop()     
      do while not this.w_RETURN.mcCastiva.eof()
        if this.w_RETURN.mcCastiva.FLOMAG="X" Or (this.w_MVFLSCOM="S")
          * --- Applico lo sconto di piede all'imponibile, due casi, 
          *     sconto forzato o sconto a percentuale.
          *     Nel primo caso scorporo lo sconto di piede sui vari imponibili
          *     Nel secondo caso applico aliquota per aliquota le % di sconto
          if this.w_FLFOSC="S"
            if this.w_RETURN.mcCastiva.FLOMAG="X" 
              if this.w_TOT_IMP_SCO=0 And this.w_MVSCONTI<>0
                * --- Warning nel caso non riesca a ripartire lo sconto...
                this.w_RETURN.nErrorLog = "Impossibile ripartire lo sconto di piede sul castelletto IVA"
              else
                * --- Se ho sconti allora li ripartisco...
                if this.w_TOT_IMP_SCO<>0 And this.w_MVSCONTI<>0
                  * --- Calcolo la parte di imponibile delle spese da inserire sull'aliquota valutata
                  this.w_TmpN_SCO = cp_ROUND(this.w_MVSCONTI * this.w_RETURN.mcCastiva.IMPON / this.w_TOT_IMP_SCO, this.w_DECTOT)
                  this.w_RETURN.mcCastiva.IMPON = this.w_RETURN.mcCastiva.IMPON + this.w_TmpN_SCO
                  * --- Aggiungo al monte impnibile+imposta la parte di
                  *     imponibile della spesa pi� l'imposta..
                  this.w_TmpIVA_SCO = IIF(this.w_TmpN_SCO=0, 0, cp_ROUND(this.w_TmpN_SCO * (1 + this.w_RETURN.mcCastiva.PERIVA / 100), this.w_DECTOT))
                  this.w_RESTO_SCO = this.w_RESTO_SCO - IIF(this.w_MVFLSCOR="S" , this.w_TmpIVA_SCO , this.w_TmpN_SCO)
                  this.w_ROW_RESTO_SCO = this.w_RETURN.mcCastiva.recno()
                  this.w_RETURN.mcCastiva.LORDO = this.w_RETURN.mcCastiva.LORDO + this.w_TmpIVA_SCO
                endif
              endif
            endif
          else
            * --- Applico gli sconti agli imponibili, se scorporo applico lo scorporo all'imponibile non
            *     scorporato + sconti.
            *     Per aggiustare la differenza conteggio solo gli sconti applicati sulle righe non
            *     omaggio (per confrontare il risultato con MVSCONTI)
            if this.w_MVFLSCOR="S"
              if this.w_RETURN.mcCastiva.FLOMAG="X" 
                this.w_RESTO_SCO = this.w_RESTO_SCO - Calsco( this.w_RETURN.mcCastiva.LORDO , this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT)
              endif
              this.w_RETURN.mcCastiva.IMPON = CALNET(this.w_RETURN.mcCastiva.LORDO + Calsco(this.w_RETURN.mcCastiva.LORDO, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT), this.w_RETURN.mcCastiva.PERIVA, this.w_DECTOT , this.w_MVCODIVE , this.w_PERIVE)
            else
              if this.w_RETURN.mcCastiva.FLOMAG="X" 
                this.w_RESTO_SCO = this.w_RESTO_SCO - Calsco(this.w_RETURN.mcCastiva.IMPON , this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT)
              endif
              this.w_RETURN.mcCastiva.IMPON = this.w_RETURN.mcCastiva.IMPON + Calsco(this.w_RETURN.mcCastiva.IMPON, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT)
            endif
            if this.w_RETURN.mcCastiva.FLOMAG="X" 
              this.w_ROW_RESTO_SCO = this.w_RETURN.mcCastiva.recno()
            endif
            this.w_RETURN.mcCastiva.LORDO = this.w_RETURN.mcCastiva.LORDO + Calsco( this.w_RETURN.mcCastiva.LORDO, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT)
          endif
        endif
        this.w_RETURN.mcCastiva.Next()     
      enddo
      * --- Aggiungo eventuale resto dello sconto, in caso di scorporo w_RESTO_SCO
      *     � comprensivo dell'IVA altrimenti � solo imponibile
      if this.w_ROW_RESTO_SCO <> 0 And this.w_RESTO_SCO<>0
        this.w_RETURN.mcCastiva.goto(this.w_ROW_RESTO_SCO)     
        if this.w_MVFLSCOR="S"
          this.w_RETURN.mcCastiva.LORDO = this.w_RETURN.mcCastiva.LORDO + this.w_RESTO_SCO
        else
          this.w_RETURN.mcCastiva.IMPON = this.w_RETURN.mcCastiva.IMPON + this.w_RESTO_SCO
        endif
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcoli Finali e Castelletto IVA
    *     Aggiunge le spese al castelleto IVA nel caso di Corrispettivi in ventilazione
    *     la procedura aggiunge le spese incasso (le uniche presenti) all'imponibile
    *     senza curarsi di un'eventuale codice IVA associato
    * --- Importo arrotondamento gestito solo negli acquisti e direttamente
    *     dalla gestione...
    if this.w_MVFLVEAC="A" 
      * --- Se Ciclo Acquisti tratta le Spese trasporto, Bolli e Arrotondamenti dettagliandole sul Castelletto IVA 
      this.w_IMPARR = IIF(EMPTY(this.w_MVIVAARR), 0, this.w_MVIMPARR)
    else
      this.w_IMPARR = 0
    endif
    * --- Parametri da valorizzare per Pag5, Codice IVA spesa ed importo 
    *     spesa.
    *     Inoltre occorre passare percentuale IVA e flag esente
    this.w_APPINC = this.w_MVSPEINC
    * --- Nel caso di corrispettivi in ventilazione le spese le inserisco nel
    *     castelletto IVA senza scorporarle (lo scorporo vero potr� essere fatto solo al 
    *     momento della stampa registri tramite la ventilazione degli acquisti).
    *     Il castelletto IVA in questo caso � costituito solo dall'imponibile per
    *     tutte le parti del documento,...
    if this.w_MVSPEINC<>0 AND (NOT EMPTY(this.w_MVIVAINC) Or this.w_TIPREG="E")
      this.w_pCODIVAS = IIF( this.w_TIPREG="E" , SPACE(5) , this.w_MVIVAINC )
      this.w_INDEX = "SPEINC"
      this.w_pIMPSPE = this.w_MVSPEINC
      this.w_pPERIVA = IIF( this.w_TIPREG="E" , 0, this.w_PEIINC )
      this.w_pFLESEN = IIF( this.w_TIPREG="E" , 0, this.w_BOLINC )
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_APPINC = 0
    endif
    if this.w_TIPREG<>"E"
      this.w_APPIMB = this.w_MVSPEIMB
      if this.w_MVSPEIMB<>0 AND NOT EMPTY(this.w_MVIVAIMB)
        this.w_INDEX = "SPEIMB"
        this.w_pCODIVAS = this.w_MVIVAIMB
        this.w_pIMPSPE = this.w_MVSPEIMB
        this.w_pPERIVA = this.w_PEIIMB
        this.w_pFLESEN = this.w_BOLIMB
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_APPIMB = 0
      endif
      this.w_APPTRA = IIF(this.w_MVFLRTRA="S", 0, this.w_MVSPETRA)
      if this.w_MVSPETRA<>0 AND NOT EMPTY(this.w_MVIVATRA)
        this.w_pCODIVAS = this.w_MVIVATRA
        this.w_pIMPSPE = this.w_MVSPETRA
        this.w_INDEX = "SPETRA"
        this.w_pPERIVA = this.w_PEITRA
        this.w_pFLESEN = this.w_BOLTRA
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_APPTRA = 0
      endif
      if this.w_IMPARR<>0 
        this.w_INDEX = "IMPARR"
        this.w_pCODIVAS = this.w_MVIVAARR
        this.w_pIMPSPE = this.w_IMPARR
        this.w_pPERIVA = this.w_PEIARR
        this.w_pFLESEN = " "
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Calcola Spese Accessorie Ripartite (In Proporzione tra gli Imponibili e il Totale Documento Scontati)
      if EMPTY(this.w_MVIVAINC) Or EMPTY(this.w_MVIVAIMB) Or EMPTY(this.w_MVIVATRA)
        * --- w_TmpTOTIMP contiene il totale imponibile del castelletto IVA comprensivo
        *     di righe normali e righe omaggio
        this.w_TmpTOTIMP = 0
        this.w_RETURN.mcCastiva.gotop()     
        do while not this.w_RETURN.mcCastiva.eof()
          * --- Totale castelletto IVA escluso Rev. Charged
          this.w_TmpTOTIMP = this.w_TmpTOTIMP + ABS( this.w_RETURN.mcCastiva.IMPON )
          this.w_RETURN.mcCastiva.Next()     
        enddo
        this.w_FIRSTLAP = .T.
        this.w_RETURN.mcCastiva.gotop()     
        do while not this.w_RETURN.mcCastiva.eof()
          * --- Scorporo le spese sulle righe a codici IVA con Reverse Charge o non sconto merce
          if this.w_RETURN.mcCastiva.FLOMAG $ "X-I-E" AND NOT EMPTY(this.w_RETURN.mcCastiva.CODIVA) 
            * --- Se Aliquota Imponibile
            if EMPTY(this.w_MVIVAINC) And this.w_MVSPEINC<>0
              this.w_IMPORTO = this.w_MVSPEINC
              this.w_INDEX = "SPEINC"
              this.Page_6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if EMPTY(this.w_MVIVAIMB) And this.w_MVSPEIMB<>0
              this.w_IMPORTO = this.w_MVSPEIMB
              this.w_INDEX = "SPEIMB"
              this.Page_6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if EMPTY(this.w_MVIVATRA) And this.w_MVSPETRA<>0
              this.w_IMPORTO = this.w_MVSPETRA
              if this.w_FIRSTLAP
                this.w_APPTRA = this.w_MVSPETRA
                this.w_FIRSTLAP = .F.
              endif
              this.w_INDEX = "SPETRA"
              this.Page_6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          this.w_RETURN.mcCastiva.Next()     
        enddo
        * --- Inserisco nel castelleto IVA le righe relative alle spese (dopo per non influenzare il calcolo)
        if this.w_TMPLEN>0
          * --- Li ho inseriti tutti verifico se devo inserire nuove righe nel castelletto IVA
          this.mcTmpIva.GoTop()     
          do while not this.mcTmpIva.eof()
            this.w_TROV = .F.
            this.w_RETURN.mcCastiva.gotop()     
            do while not this.w_RETURN.mcCastiva.eof()
              if this.mcTmpIva.CODIVA=this.w_RETURN.mcCastiva.CODIVA AND this.w_RETURN.mcCastiva.FLOMAG=this.mcTmpIva.FLOMAG
                * --- Aliquota gia' esistente - Non occorre introdurre una nuova riga
                this.w_RETURN.mcCastiva.SPEINC = this.w_RETURN.mcCastiva.SPEINC + this.mcTmpIva.SPEINC
                this.w_RETURN.mcCastiva.SPEIMB = this.w_RETURN.mcCastiva.SPEIMB + this.mcTmpIva.SPEIMB
                this.w_RETURN.mcCastiva.SPETRA = this.w_RETURN.mcCastiva.SPETRA + this.mcTmpIva.SPETRA
                this.w_TROV = .T.
                EXIT
              endif
              this.w_RETURN.mcCastiva.Next()     
            enddo
            if not this.w_TROV
              * --- Inserisco una nuova riga nel castelleto IVA
              * --- Calcola Riga Castelletto IVA Esente
              this.w_ALIVA = this.w_ALIVA + 1
              * --- Testa se raggiunte max aliquote
              if this.w_ALIVA > this.MAXALIVA
                this.w_RETURN.nErrorLog = ah_MsgFormat("Consentite massimo %1 aliquote", ALLTRIM(STR(this.MAXALIVA)))
                this.Page_9()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                * --- Aggiunge nuova aliquota
                this.w_RETURN.mcCastiva.AppendBlank()     
                this.w_RETURN.mcCastiva.CODIVA = this.mcTmpIva.CODIVA
                this.w_RETURN.mcCastiva.SPEINC = this.mcTmpIva.SPEINC
                this.w_RETURN.mcCastiva.SPEIMB = this.mcTmpIva.SPEIMB
                this.w_RETURN.mcCastiva.SPETRA = this.mcTmpIva.SPETRA
                this.w_RETURN.mcCastiva.IMPON = 0
                this.w_RETURN.mcCastiva.PERIVA = this.mcTmpIva.PERIVA
                this.w_RETURN.mcCastiva.IMPOS = 0
                this.w_RETURN.mcCastiva.LORDO = 0
                this.w_RETURN.mcCastiva.FLOMAG = "X"
                this.w_RETURN.mcCastiva.REVCHA = " "
                this.w_RETURN.mcCastiva.SaveCurrentRecord()     
              endif
            endif
            this.mcTmpIva.Next()     
          enddo
        endif
      endif
      if this.w_APPINC<>0 OR this.w_APPIMB<>0 OR this.w_APPTRA<>0
        * --- Aggiunge alla Prima Aliquota non sconto merce l'eventuale Resto
        this.w_RETURN.mcCastiva.gotop()     
        do while not this.w_RETURN.mcCastiva.eof()
          if this.w_RETURN.mcCastiva.FLOMAG $ "X-I-E" AND NOT EMPTY(this.w_RETURN.mcCastiva.CODIVA) 
            * --- Se Aliquota Imponibile
            this.w_RETURN.mcCastiva.SPEINC = this.w_RETURN.mcCastiva.SPEINC + this.w_APPINC
            this.w_RETURN.mcCastiva.SPEIMB = this.w_RETURN.mcCastiva.SPEIMB + this.w_APPIMB
            this.w_RETURN.mcCastiva.SPETRA = this.w_RETURN.mcCastiva.SPETRA + this.w_APPTRA
            * --- Provoco l'uscita dal ciclo
            EXIT
          endif
          this.w_RETURN.mcCastiva.Next()     
        enddo
      endif
      * --- Ripartisce Spese Accessorie su Imponibile
      *     Le spese possono essere presenti solo su righe del castelletto IVA
      *     di tipo normale (Nessun Omaggio),
      this.w_RETURN.mcCastiva.gotop()     
      do while not this.w_RETURN.mcCastiva.eof()
        if this.w_RETURN.mcCastiva.FLOMAG = "X" AND NOT EMPTY(this.w_RETURN.mcCastiva.CODIVA)
          * --- Se Aliquota Imponibile
          if this.w_MVFLSCOR="S"
            * --- Calcola per Eventuale Scorporo Piede Fattura
            this.w_RETURN.mcCastiva.LORDO = this.w_RETURN.mcCastiva.LORDO + (this.w_RETURN.mcCastiva.SPEINC + this.w_RETURN.mcCastiva.SPEIMB + this.w_RETURN.mcCastiva.SPETRA)
            * --- Gli Importi Aliquote Spese sono Sempre Riferiti al Netto!
            if Not(this.w_ACQINT="S" OR (this.w_ACQAUT="S" and this.w_RETURN.mcCastiva.REVCHA="S")) And this.w_RETURN.mcCastiva.PERIVA > 0
              this.w_RETURN.mcCastiva.SPEINC = CALNET(this.w_RETURN.mcCastiva.SPEINC, this.w_RETURN.mcCastiva.PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE)
              this.w_RETURN.mcCastiva.SPEIMB = CALNET(this.w_RETURN.mcCastiva.SPEIMB, this.w_RETURN.mcCastiva.PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE)
              this.w_RETURN.mcCastiva.SPETRA = CALNET(this.w_RETURN.mcCastiva.SPETRA, this.w_RETURN.mcCastiva.PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE)
            endif
          endif
          this.w_RETURN.mcCastiva.IMPON = this.w_RETURN.mcCastiva.IMPON + (this.w_RETURN.mcCastiva.SPEINC + this.w_RETURN.mcCastiva.SPEIMB + this.w_RETURN.mcCastiva.SPETRA + this.w_RETURN.mcCastiva.IMPARR)
        endif
        * --- Calcola Imposta su Castelletto IVA
        if Not(this.w_ACQINT="S" OR (this.w_ACQAUT="S" and this.w_RETURN.mcCastiva.REVCHA ="S")) And this.w_RETURN.mcCastiva.PERIVA > 0
          * --- Calcola Valore Imposta
          * --- Non Applico Iva solo nei casi:
          *     1) Causali Contabili di Tipo Fatture\Note di Credito Intra
          *     2) Causali Contabili di Tipo Fatture\Note di Credito R.C. e Flag
          *         Reverse Charge attivato nella relativa Aliquota
          if this.w_MVFLSCOR = "S"
            * --- Se Scorporo calcola per Differenza tra Lordo Aliquota e Imponibile Scorporato
            this.w_RETURN.mcCastiva.IMPON = CALNET((this.w_RETURN.mcCastiva.LORDO + this.w_RETURN.mcCastiva.IMPSPE), this.w_RETURN.mcCastiva.PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE)
            if ((this.w_RETURN.mcCastiva.REVCHA="S" AND Not (this.w_TIPDOC $ "AU-NU")) OR this.w_RETURN.mcCastiva.REVCHA<>"S" ) 
              this.w_RETURN.mcCastiva.IMPOS = cp_Round((this.w_RETURN.mcCastiva.LORDO + this.w_RETURN.mcCastiva.IMPSPE) - this.w_RETURN.mcCastiva.IMPON, this.w_DECTOT)
            endif
            * --- Gli errori di calcolo numerico tra dettaglio documento e castelletto IVA
            *     li gestisco solo per le righe non omaggio
            if this.w_RETURN.mcCastiva.FLOMAG = "X"
              * --- Se ho delle spese non ripartite non posso confrontare l'imponibile totale
              *     con l'imponibile presente sul dettaglio. In quest'ultimo infatti non ho le spese.
              if Not((Empty( this.w_MVFLRINC ) And this.w_MVSPEINC<>0) Or ( Empty( this.w_MVFLRIMB ) And this.w_MVSPEIMB<>0 ) Or ( Empty( this.w_MVFLRTRA ) And this.w_MVSPETRA<>0 ) )
                * --- Nel caso in cui eseguendo lo scorporo sulla somma delle righe
                *     ottengo un valore differente rispetto alla somma dei singoli scorpori 
                *     vado ad aggiustare MVVALMAG asul dettaglio per evitare
                *     squadrature in sede di contabilizzazione.
                *     Vado a ricercare la prima riga del dettaglio per l'aliquota, ed aggiorno
                *     su di essa i vari importi di riga a partire da MVVALMAG.
                *     MVVALRIG non � modificato perch� questo importo � l'unico
                *     non toccato dalla ripartizione (che ad ogni ricalcolo rivalorizza gli importi
                *     senza questo aggiustamento)
                this.w_NDIFF = this.w_RETURN.mcCastiva.IMPON - this.w_RETURN.mcCastiva.IMPDET
                if this.w_NDIFF<>0
                  * --- Memorizzo differenza di arrotondamento
                  this.w_RETURN.mcCastiva.ARRSCORP = this.w_NDIFF
                endif
              else
                * --- Calcolo il totale dell'imponibile sul dettaglio ed il totale dell'imponibile
                *     sul castelletto IVA
                this.w_TOTIMPN = this.w_TOTIMPN + this.w_RETURN.mcCastiva.IMPON
                this.w_TOTIMPND = this.w_TOTIMPND +this.w_RETURN.mcCastiva.IMPDET
                * --- Aggiungo al totale l'imponibile delle spese non scorporate
                if Empty( this.w_MVFLRINC ) And this.w_MVSPEINC<>0
                  this.w_TOTIMPND = this.w_TOTIMPND +this.w_RETURN.mcCastiva.SPEINC
                endif
                if Empty( this.w_MVFLRIMB ) And this.w_MVSPEIMB<>0
                  this.w_TOTIMPND = this.w_TOTIMPND + this.w_RETURN.mcCastiva.SPEIMB
                endif
                if Empty( this.w_MVFLRTRA ) And this.w_MVSPETRA<>0 
                  this.w_TOTIMPND = this.w_TOTIMPND +this.w_RETURN.mcCastiva.SPETRA
                endif
              endif
            endif
          else
            this.w_RETURN.mcCastiva.IMPON = this.w_RETURN.mcCastiva.IMPON + this.w_RETURN.mcCastiva.IMPSPE
            if ((this.w_RETURN.mcCastiva.REVCHA="S" and Not(this.w_TIPDOC $ "AU-NU") ) OR this.w_RETURN.mcCastiva.REVCHA <> "S" ) 
              * --- L' arrotondamento al ... superiore e' previsto solo per le lire..
              this.w_RETURN.mcCastiva.IMPOS = cp_ROUND(( this.w_RETURN.mcCastiva.IMPON * this.w_RETURN.mcCastiva.PERIVA / 100)+IIF(this.w_MVCODVAL=g_CODLIR,this.w_ARRSUP,0), this.w_DECTOT)
            endif
          endif
        else
          * --- Aggiungo comunque all'imponibile l'imponibile delle spese sul dettaglio
          this.w_RETURN.mcCastiva.IMPON = this.w_RETURN.mcCastiva.IMPON + this.w_RETURN.mcCastiva.IMPSPE
        endif
        this.w_RETURN.mcCastiva.Next()     
      enddo
      * --- Verifico la congruenza tra l'imponibile sul dettaglio e l'imponibile sul castelletto
      *     IVA in modo globale (ho spese non ripartite)
      if this.w_MVFLSCOR="S" And ((Empty( this.w_MVFLRINC ) And this.w_MVSPEINC<>0) Or ( Empty( this.w_MVFLRIMB ) And this.w_MVSPEIMB<>0 ) Or ( Empty( this.w_MVFLRTRA ) And this.w_MVSPETRA<>0 ) )
        this.w_NDIFF = this.w_TOTIMPN - this.w_TOTIMPND
        if this.w_NDIFF<>0
          * --- Memorizzo differenza di arrotondamento sulla prima riga
          this.w_RETURN.mcCastiva.gotop()     
          this.w_RETURN.mcCastiva.ARRSCORP = this.w_NDIFF
        endif
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce Aliquote IVA per Spese
    *     Aggiunge alla colonna specificata in INDEX l'importo della spesa.
    *     Se aliquota gia nel castelletto  (come aliquota no omaggio)
    *     aggiunge l'importo altrimenti crea nuova riga
    this.w_TROV = .F.
    this.w_RETURN.mcCastiva.gotop()     
    INDEX = this.w_INDEX
    do while not this.w_RETURN.mcCastiva.eof()
      if this.w_pCODIVAS = this.w_RETURN.mcCastiva.CODIVA AND this.w_RETURN.mcCastiva.FLOMAG = "X"
        * --- Aliquota gia' esistente
        *     Se Corr. in ventilazione aggiungo le spese direttamente all'imponibile senza
        *     scorporare..
        if this.w_TIPREG="E"
          this.w_RETURN.mcCastiva.IMPON = this.w_RETURN.mcCastiva.IMPON + this.w_pIMPSPE
          this.w_RETURN.mcCastiva.LORDO = this.w_RETURN.mcCastiva.LORDO + this.w_pIMPSPE
        else
          this.w_RETURN.mcCastiva.&INDEX = this.w_pIMPSPE
        endif
        this.w_TROV = .T.
        EXIT
      endif
      this.w_RETURN.mcCastiva.Next()     
    enddo
    * --- Se aliquota inesistente
    if Not this.w_TROV
      * --- Aggiunge nuova aliquota
      this.w_ALIVA = this.w_ALIVA + 1
      * --- Testa se raggiunte max aliquote
      if this.w_ALIVA > this.MAXALIVA
        this.w_RETURN.nErrorLog = ah_MsgFormat("Consentite massimo %1 aliquote", ALLTRIM(STR(this.MAXALIVA)))
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_ACQAUT="S" and Not Empty(this.w_pCODIVAS)
        * --- Verifico che il codice Iva sia di tipo Reverse Charge
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVREVCHA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_pCODIVAS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVREVCHA;
            from (i_cTable) where;
                IVCODIVA = this.w_pCODIVAS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_REVCHA = NVL(cp_ToDate(_read_.IVREVCHA),cp_NullValue(_read_.IVREVCHA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        this.w_REVCHA = " "
      endif
      * --- Aggiunge nuova aliquota
      this.w_RETURN.mcCastiva.AppendBlank()     
      this.w_RETURN.mcCastiva.CODIVA = this.w_pCODIVAS
      this.w_RETURN.mcCastiva.FLOMAG = "X"
      this.w_RETURN.mcCastiva.FLESEN = this.w_pFLESEN
      this.w_RETURN.mcCastiva.PERIVA = this.w_pPERIVA
      this.w_RETURN.mcCastiva.REVCHA = this.w_REVCHA
      * --- Corr. in ventilazione, le spese vanno direttamente nell'imponibile..
      if this.w_TIPREG="E"
        this.w_RETURN.mcCastiva.IMPON = this.w_pIMPSPE
        this.w_RETURN.mcCastiva.LORDO = this.w_pIMPSPE
      else
        this.w_RETURN.mcCastiva.&INDEX = this.w_pIMPSPE
      endif
      this.w_RETURN.mcCastiva.SaveCurrentRecord()     
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- w_IMPORTO contiene l'importo mentre w_INDEX contiene l'indice della spesa
    *     2 - Spese incasso
    *     3 - Spese imballo
    *     4 - Spese di Trasporto
    INDEX = this.w_INDEX
    if this.w_MVFLSCOR="S"
      * --- Ripartisco le spese accessorie scorporando la spesa
      this.w_TmpN = IIF(this.w_TmpTOTIMP=0,0,cp_ROUND(this.w_IMPORTO * ( ABS( this.w_RETURN.mcCastiva.IMPON ) / this.w_TmpTOTIMP) / (1+ this.w_RETURN.mcCastiva.PERIVA/100), this.w_DECTOT))
    else
      * --- Ripartisco le spese accessorie
      this.w_TmpN = IIF(this.w_TmpTOTIMP=0,0,cp_ROUND((this.w_IMPORTO * ABS( this.w_RETURN.mcCastiva.IMPON )) / this.w_TmpTOTIMP, this.w_DECTOT))
    endif
    * --- Calcolo Imponibile + IVA della spesa ripartita
    this.w_TmpIVA = IIF(this.w_TmpN=0,0,cp_ROUND(this.w_TmpN * (1 + this.w_RETURN.mcCastiva.PERIVA / 100), this.w_DECTOT))
    * --- Decremento le rispettive variabili che conterranno l'eventuale resto
    *     In caso di scorporo sottraggo imponibile + IVA
    do case
      case this.w_INDEX="SPEINC"
        if this.w_MVFLSCOR="S"
          this.w_APPINC = this.w_APPINC - this.w_TmpIVA
        else
          this.w_APPINC = this.w_APPINC - this.w_TmpN
        endif
      case this.w_INDEX="SPEIMB"
        if this.w_MVFLSCOR="S"
          this.w_APPIMB = this.w_APPIMB - this.w_TmpIVA
        else
          this.w_APPIMB = this.w_APPIMB - this.w_TmpN
        endif
      case this.w_INDEX="SPETRA"
        if this.w_MVFLSCOR="S"
          this.w_APPTRA = this.w_APPTRA - this.w_TmpIVA
        else
          this.w_APPTRA = this.w_APPTRA - this.w_TmpN
        endif
    endcase
    * --- Se la riga non � omaggio aggiorno direttamente AI
    if this.w_RETURN.mcCastiva.FLOMAG = "X"
      * --- Se la riga � normale valorizzo le spese, se scorporo comprensive di imposta
      if this.w_MVFLSCOR="S"
        this.w_RETURN.mcCastiva.&INDEX = this.w_RETURN.mcCastiva.&INDEX + this.w_TmpIVA
      else
        this.w_RETURN.mcCastiva.&INDEX = this.w_RETURN.mcCastiva.&INDEX + this.w_TmpN
      endif
    else
      * --- La riga � omaggio quindi costruisco l'array temporaneo che aggiunger� una
      *     riga al castelletto IVA con la sola spesa accessoria ripartita sulla corrispondente
      *     riga omaggio
      this.w_TROV = .F.
      this.mcTmpIva.GoTop()     
      do while not this.mcTmpIva.eof()
        * --- Aliquota gia' esistente
        if this.mcTmpIva.CODIVA = this.w_RETURN.mcCastiva.CODIVA
          if this.w_MVFLSCOR="S"
            this.mcTmpIva.&INDEX = this.mcTmpIva.&INDEX + this.w_TmpIVA
          else
            this.mcTmpIva.&INDEX = this.mcTmpIva.&INDEX + this.w_TmpN
          endif
          this.w_TROV = .T.
          EXIT
        endif
        this.mcTmpIva.Next()     
      enddo
      * --- Non l'ho trovato lo aggiungo
      if not this.w_TROV
        this.w_TmpLen = this.w_TmpLen + 1
        this.mcTmpIva.AppendBlank()     
        this.mcTmpIva.CODIVA = this.w_RETURN.mcCastiva.CODIVA
        this.mcTmpIva.SPEINC = 0
        this.mcTmpIva.SPEIMB = 0
        this.mcTmpIva.SPETRA = 0
        this.mcTmpIva.IMPON = 0
        this.mcTmpIva.PERIVA = this.w_RETURN.mcCastiva.PERIVA
        this.mcTmpIva.IMPOS = 0
        this.mcTmpIva.LORDO = 0
        this.mcTmpIva.FLESEN = " "
        this.mcTmpIva.FLOMAG = "X"
        this.mcTmpIva.IMPARR = 0
        this.mcTmpIva.REVCHA = this.w_RETURN.mcCastiva.REVCHA
        if this.w_MVFLSCOR="S"
          this.mcTmpIva.&INDEX = this.w_TmpIVA
        else
          this.mcTmpIva.&INDEX = this.w_TmpN
        endif
        this.mcTmpIva.SaveCurrentRecord()     
      endif
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola le Eventuali Rate
    DIMENSION RA[1000, 11]
    this.w_IMPSPE = cp_Round( this.w_IMPSPE , this.w_DECTOT ) 
    * --- Netto Rata: Tot.Imponibile+Spese Bolli-Acconti-Spese non Rip.
    this.w_IMPNET = (this.w_TOTIMPON+this.w_MVSPEBOL)-(this.w_IMPSPE+this.w_MVACCONT+ this.w_MVACCPRE + this.w_MVTOTRIT+ this.w_RITPRE + this.w_MVTOTENA)
    * --- Calcolo w_FLINCA
    if this.w_FLINCA="C"
      this.w_FLINCA = IIF(this.w_MVCLADOC="RF" AND ((this.w_TOTIMPON+this.w_TOTIMPOS+this.w_MVSPEBOL+this.w_MVIMPARR)=(this.w_MVACCONT+this.w_MVACCSUC)), "S", " ")
    endif
    if this.w_MVCLADOC="RF" And this.w_FLINCA<>"S"
      this.w_IMPNET = this.w_IMPNET - this.w_MVACCSUC
    endif
    this.w_IMPIVA = this.w_TOTIMPOS
    * --- Ricalcolo Sconto Finanziario
    *     Lo sconto finanziario � applicato al totale documento.
    *     Va calcolato prima delle determinazione delle Spese su Cambiale...
    * --- Se no Forza Sconti Finanziario Ricalcolo lo sconto
    if this.w_MVFLSFIN<>"S" AND ( this.w_IMPNET+ this.w_IMPIVA + this.w_IMPSPE )>0
      this.w_MVIMPFIN = cp_Round( IIF(this.w_MVPERFIN>0 ,( this.w_IMPNET+ this.w_IMPIVA + this.w_IMPSPE )*this.w_MVPERFIN/100,this.w_MVIMPFIN) , this.w_DECTOT )
    endif
    * --- Lo sconto finanziario e la ritenuta attiva (vendite) li decurto sempre dall'imponibile
    this.w_IMPNET = this.w_IMPNET - this.w_MVIMPFIN - this.w_RITATT
    this.w_ESCL1 = STR(this.w_MESE1, 2, 0) + STR(this.w_GIORN1, 2, 0) + STR(this.w_GIOFIS, 2, 0)
    this.w_ESCL2 = STR(this.w_MESE2, 2, 0) + STR(this.w_GIORN2, 2, 0)
    this.w_NURATE = 0
    this.w_CALRATE = .T.
    * --- Calcolo le scadenze per data evasione (solo su ordini)
    if this.w_MVCLADOC="OR"
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZRATEVA"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZRATEVA;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RATEVA = NVL(cp_ToDate(_read_.AZRATEVA),cp_NullValue(_read_.AZRATEVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Calcola le rate in base alla data prevista evasione, la procedura
      *     raggruppa le righe in base alla data prevista evasione e proporziona
      *     utilizzando il valore di riga. Questo introduce delle imprecisioni di calcolo
      *     dovute alle possibili diverse percentuali iva di riga e/o alle spese di testata.
      *     Queste imprecisioni sono accettabili per lo scopo della funzionalit� che deve
      *     indicare sommariamente come potranno essere i pagamenti se rispettate le 
      *     date di prevista evasione.
      if this.w_RATEVA="S" 
        * --- Creo un temporaneo di appoggio raggruppando per la data prevista evasione..
        if this.w_MVFLSCOM<>"S" 
          this.pDOCINFO.mcDetail.Exec_MCSelect("Tmp_Righe","Sum(IIF(MVFLOMAG = 'X' , MVVALRIG , 0)+MVIMPSCO) As ValRig, MVDATEVA As DatEva",; 
 "Not Empty(MVCODICE) And MVTIPRIG$'RFMA' And ( MVFLOMAG='X' Or MVFLOMAG='I')","","MVDATEVA","")     
        else
          this.pDOCINFO.mcDetail.Exec_MCSelect("Tmp_Righe","Sum(IIF(MVFLOMAG = 'X' , MVVALRIG , 0)) As ValRig, MVDATEVA As DatEva",; 
 "Not Empty(MVCODICE) And MVTIPRIG$'RFMA' And ( MVFLOMAG='X' Or MVFLOMAG='I')","","MVDATEVA","")     
        endif
        * --- Ciclo sulle righe del temporaneo Righe Ordine
        *     Debbo eseguire cmq i calcoli tranne nel caso ho una sola
        *     data prevista evasione che coincide con la data documento
        if Not ( this.w_MVDATDOC = Tmp_Righe.DatEva And RecCount( "Tmp_Righe" )=1 ) and this.w_TOTRIP<>0
          * --- Creo il cursore di appoggio
          Create Cursor RATETMP (R1 D, R2 N(18,4), R3 N(18,4), R4 N(18,4), R5 C(10), R6 C(1))
           
 Select "Tmp_Righe" 
 Go Top
          do while Not Eof( "Tmp_Righe" )
            * --- Lancio 'Scadenze' per ogni riga
            *     Come primo passaggio svuoto l'array...
            this.w_LOOP = 1
            do while this.w_LOOP<=1000
               
 RA[ this.w_LOOP , 1] = cp_CharToDate("  -  -  ") 
 RA[ this.w_LOOP , 2] = 0 
 RA[ this.w_LOOP , 3] = 0 
 RA[ this.w_LOOP , 4] = 0 
 RA[ this.w_LOOP , 5] = "  " 
 RA[ this.w_LOOP , 6] = "  " 
 RA[ this.w_LOOP , 7] = "  " 
 RA[ this.w_LOOP , 8] = "  " 
 RA[ this.w_LOOP , 9] = " " 
 RA[ this.w_LOOP , 10] = " " 
 RA[ this.w_LOOP , 11] = " "
              this.w_LOOP = this.w_LOOP + 1
            enddo
            this.w_VALRIG = Tmp_Righe.ValRig
            * --- Calcolo le rate proporzionado il valore di riga con il totale dei valori di riga
            *     (w_TOTRIP)
            this.w_NURATE = SCADENZE("RA",this.w_MVCODPAG,Tmp_Righe.DatEva,cp_Round(this.w_IMPNET*this.w_VALRIG/this.w_TOTRIP,this.w_DECTOT),cp_Round(this.w_IMPIVA*this.w_VALRIG/this.w_TOTRIP,this.w_DECTOT),cp_Round(this.w_IMPSPE*this.w_VALRIG/this.w_TOTRIP,this.w_DECTOT),this.w_ESCL1,this.w_ESCL2,this.w_DECTOT,this.w_MVDATDIV)
            this.w_NURATE = IIF(this.w_NURATE>999, 999, this.w_NURATE)
            * --- 'Appendo' al cursore il contenuto dell'array RA[]
            this.w_LOOP = 1
            do while this.w_LOOP<=this.w_NURATE
              if NOT EMPTY(RA[ this.w_LOOP , 1]) AND ( RA[ this.w_LOOP , 2]<>0 Or RA[ this.w_LOOP , 3]<>0 or RA[ this.w_LOOP , 4]<>0 )
                * --- Almeno una rata determinata non occorre calcolare le rate
                *     (se documento con tutte righe omaggio e spese Tmp_Righe non
                *     avrebbe record)
                this.w_CALRATE = .F.
                INSERT INTO RATETMP (R1, R2, R3, R4, R5, R6) VALUES (RA[this.w_LOOP,1],RA[this.w_LOOP,2],RA[this.w_LOOP,3],; 
 RA[this.w_LOOP,4],RA[this.w_LOOP,5], RA[this.w_LOOP,6])
              endif
              this.w_LOOP = this.w_LOOP + 1
            enddo
             
 Select "Tmp_Righe" 
 Skip
          enddo
          * --- Ho almeno una rata 'Valida' ?
          if Not this.w_CALRATE
            * --- Raggruppo il cursore per Data Scadenza e Pagamento
             
 Select R1, SUM(R2) as R2, SUM(R3) as R3, SUM(R4) as R4, MAX(R5) as R5, MAX(R6) as R6 from ; 
 RATETMP group by R1,R5 into cursor RATETMP NoFilter Readwrite
            * --- Verifico il totale rate...
             
 Select RATETMP 
 Sum(RATETMP.R2+RATETMP.R3+RATETMP.R4) to this.w_RATETOT
            * --- Una volta terminati i calcoli verifico che il totale finale coincida con
            *     il totale da ottenere..
            this.w_RATETOT = (this.w_IMPNET + this.w_IMPIVA + this.w_IMPSPE ) - this.w_RATETOT
            * --- Se la differenza � <>0 sommo la differenza alla prima rata sull'imponibile..
            if this.w_RATETOT<>0
               
 Select("RATETMP") 
 Go Top 
 Replace R2 With R2+this.w_RATETOT
            endif
            * --- Copio il contenuto di RATETMP nel MemoryCursor
            this.w_NURATE = 0
            this.w_RETURN.mcRate.gotop()     
             
 Select RATETMP 
 Locate for 1=1
            do while not eof( "RATETMP" )
              * --- Sposto il contenuto del cursore all'interno del MemoryCursor
              this.w_RETURN.mcRate.AppendBlank()     
              this.w_RETURN.mcRate.DATRAT = RATETMP.R1
              this.w_RETURN.mcRate.IMPNET = RATETMP.R2
              this.w_RETURN.mcRate.IMPIVA = RATETMP.R3
              this.w_RETURN.mcRate.IMPSPE = RATETMP.R4
              this.w_RETURN.mcRate.MODPAG = RATETMP.R5
              this.w_RETURN.mcRate.FLPROV = RATETMP.R6
              this.w_NURATE = this.w_NURATE + 1
              this.w_RETURN.mcRate.SaveCurrentRecord()     
               
 Select RATETMP 
 Continue
            enddo
          endif
          * --- Chiudo il cursore
          Use in Select("RATETMP")
        endif
        * --- Rimuovo il cursore...
        Use In Tmp_Righe
      endif
    endif
    * --- Se non calcolo le rate per gli ordini allora ricalcolo normalmente le rate
    if this.w_CALRATE And NOT EMPTY(this.w_MVCODPAG)
      this.w_NURATE = SCADENZE("RA",this.w_MVCODPAG,this.w_MVDATDOC,this.w_IMPNET,this.w_IMPIVA,this.w_IMPSPE,this.w_ESCL1,this.w_ESCL2,this.w_DECTOT,this.w_MVDATDIV)
      this.w_NURATE = IIF(this.w_NURATE>999, 999, this.w_NURATE)
      * --- Copio il contenuto di RA nel MemoryCursor
      this.w_LOOP = 1
      do while this.w_LOOP <= this.w_NURATE
        * --- Sposto il contenuto del cursore all'interno del MemoryCursor
        this.w_RETURN.mcRate.AppendBlank()     
        this.w_RETURN.mcRate.DATRAT = RA[this.w_LOOP, 1]
        this.w_RETURN.mcRate.IMPNET = RA[this.w_LOOP, 2]
        this.w_RETURN.mcRate.IMPIVA = RA[this.w_LOOP, 3]
        this.w_RETURN.mcRate.IMPSPE = RA[this.w_LOOP, 4]
        this.w_RETURN.mcRate.MODPAG = RA[this.w_LOOP, 5]
        this.w_RETURN.mcRate.FLPROV = RA[this.w_LOOP, 6]
        this.w_RETURN.mcRate.SaveCurrentRecord()     
        this.w_LOOP = this.w_LOOP + 1
      enddo
    endif
    * --- Passa al Calcolo delle Rate il Parametro
    this.w_RETURN.nTOTRATE = 0
    * --- Mette tutto sul Netto e Azzera le Date Rata con Importo = 0 (Attenzione se Corrispettivo con Abbuono non genera Scadenze TOTRATE=0)
    this.w_RETURN.mcRate.gotop()     
    do while not this.w_RETURN.mcRate.eof()
      this.w_RETURN.mcRate.IMPNET = IIF(this.w_MVFLSALD="S", 0, this.w_RETURN.mcRate.IMPNET + this.w_RETURN.mcRate.IMPIVA + this.w_RETURN.mcRate.IMPSPE)
      this.w_RETURN.mcRate.DATRAT = IIF(this.w_RETURN.mcRate.IMPNET<>0, this.w_RETURN.mcRate.DATRAT, cp_CharToDate("  -  -  "))
      * --- Totale delle Rate...
      this.w_RETURN.nTOTRATE = this.w_RETURN.nTOTRATE + this.w_RETURN.mcRate.IMPNET
      this.w_RETURN.mcRate.Next()     
    enddo
    * --- Se Previsti Bolli in Fattura
    if this.w_MVFLVEAC="V" AND this.w_CLBOLFAT="S" AND this.w_MVCLADOC<>"RF"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Bolli su Cambiale se Cliente prevede Bolli in Fattura
    this.w_RETURN.mcRate.gotop()     
    do while not this.w_RETURN.mcRate.eof()
      * --- Scorre le Rate Valorizzate
      if this.w_RETURN.mcRate.IMPNET<>0
        this.w_APPO2 = this.w_RETURN.mcRate.MODPAG
        this.w_APPO3 = SPACE(2)
        * --- Legge la Categoria di Pagamento di Appartenenza
        * --- Read from MOD_PAGA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MPTIPPAG"+;
            " from "+i_cTable+" MOD_PAGA where ";
                +"MPCODICE = "+cp_ToStrODBC(this.w_APPO2);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MPTIPPAG;
            from (i_cTable) where;
                MPCODICE = this.w_APPO2;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APPO3 = NVL(cp_ToDate(_read_.MPTIPPAG),cp_NullValue(_read_.MPTIPPAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_APPO3 = "CA"
          * --- Se Cambiale Aggiunge Bolli
          this.w_BOLARR = IIF(this.w_BOLARR > 0, this.w_BOLARR, 1)
          * --- Calcola Bolli come percentuale dell'importo della rata.
          *     Anche a questa parte devo applicare la % spese...
          *     SPEBOL= (%BOL * IMP NETTO) / (1 - %BOL)
          this.w_APPO = (this.w_RETURN.mcRate.IMPNET * this.w_BOLCAM ) / ( 1000 - this.w_BOLCAM)
          * --- Arrotondamento (superiore)
          this.w_APPO = cp_ROUND((this.w_APPO / this.w_BOLARR)+this.w_ARRSUP, this.w_DECTOT) * this.w_BOLARR
          * --- Minimo Bolli
          this.w_APPO = IIF(this.w_APPO > this.w_BOLMIN, this.w_APPO, this.w_BOLMIN)
          * --- Ricalcola Importo Rata
          this.w_RETURN.mcRate.IMPNET = this.w_RETURN.mcRate.IMPNET + this.w_APPO
          this.w_RETURN.nTOTRATE = this.w_RETURN.nTOTRATE + this.w_APPO
          this.w_MVSPEBOL = this.w_MVSPEBOL + this.w_APPO
        endif
      endif
      this.w_RETURN.mcRate.Next()     
    enddo
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Uscita, metto a null w_RESULT affinch� Castivarate non rimanga in memoria
    if Not this.pDOCINFO.cNoRelease
      * --- Lascio i cursori in memoria, che saranno chiusi dai programmi chiamanti (Es. gsar_bfa)
      this.pDOCINFO.Done()     
      this.pDOCINFO = .Null.
    endif
    this.w_RETURN.nTOTIMPOS = this.w_TOTIMPOS
    this.w_RETURN.nTOTIMPON = this.w_TOTIMPON
    this.w_RETURN.nMVSPEBOL = this.w_MVSPEBOL
    this.w_RETURN.nMVIMPFIN = this.w_MVIMPFIN
    this.w_RETURN.nBOLARR = this.w_BOLARR
    this.w_RETURN.nTROVPAG = this.w_TROVPAG
    this.w_RETURN.nTROVSAL = this.w_TROVSAL
    this.w_RETURN.nMVACCONT = this.w_MVACCONT
    this.w_RETURN.nMVFLSALD = this.w_MVFLSALD
    this.w_RETURN.nIMPSPE = this.w_IMPSPE
    Private L_Return 
 L_Return= this.w_RETURN
    this.w_RETURN = .Null.
    i_retcode = 'stop'
    i_retval = L_Return
    return
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pDOCINFO)
    this.pDOCINFO=pDOCINFO
    this.mcTmpIva=createobject("cp_MemoryCursor",".\query\CastIvaInfo.vqr",this)
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MOD_PAGA'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='PAG_AMEN'
    return(this.OpenAllTables(6))

  proc RemoveMemoryCursors()
    this.mcTmpIva.Done()
  endproc
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pDOCINFO"
endproc
