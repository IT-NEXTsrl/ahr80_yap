* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_gld                                                        *
*              Gadget Live Tile Detailed                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-16                                                      *
* Last revis.: 2017-11-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_gld",oParentObject))

* --- Class definition
define class tgsut_gld as StdGadgetForm
  Top    = 7
  Left   = 11

  * --- Standard Properties
  Width  = 133
  Height = 90
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-29"
  HelpContextID=220808041
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_gld"
  cComment = "Gadget Live Tile Detailed"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_QUERYDTL = space(254)
  o_QUERYDTL = space(254)
  w_PRGGRID = space(250)
  w_VALUE = space(10)
  w_TITLE = space(254)
  o_TITLE = space(254)
  w_QUERY = space(254)
  w_RET = .F.
  w_IMAGE = space(254)
  w_DATETIME = space(20)
  w_PROGRAM = space(250)
  w_BTRSGRID = .F.
  w_VALUEFORMAT = space(50)
  w_PRGDESCRI = space(50)
  w_FONTCLR = 0
  w_IMAGETMP = space(254)
  w_APPLYTHEMEIMG = .F.
  w_IMAGETMP_1 = space(254)
  w_VALUELBL = .NULL.
  w_IMGOBJ = .NULL.
  w_oFooter = .NULL.
  w_oGrid = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_gld
  bMouseHover = .F.
  
  *--- ApplyTheme
  Proc ApplyTheme()
     DoDefault()
     Local l_oldErr, l_OnErrorMsg
     With This
       m.l_OnErrorMsg = ""
       m.l_oldErr = On("Error")
       On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
       
       .w_FONTCLR =  Iif(.nFontColor<>-1, .nFontColor, Rgb(243,243,243))
       .w_IMAGETMP = Iif(.w_APPLYTHEMEIMG, cp_TmpColorizeImg(.w_IMAGETMP_1, .w_FONTCLR), .w_IMAGETMP_1)
       .w_oGrid.ForeColor = .w_FONTCLR
       
       On Error &l_oldErr
       If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
          .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
       Endif
       
       .mCalc(.t.)
       .SaveDependsOn()
     Endwith
  EndProc
  
  *--- AddMenu
  *-- Voci a men� per la grid
  Proc AddMenu(loPopupMenu)
     DoDefault()
     *-- Oggetto i_PopupCurForm contiene Thisform
     loMenuItem = m.loPopupMenu.AddMenuItem()
     loMenuItem.Caption = Iif(g_OFFICE=='M', cp_Translate(MSG_EXCEL_EXPORT), cp_Translate(MSG_CALC_EXPORT))
     loMenuItem.BeginGroup = .T.
     loMenuItem.Visible = .T.
     loMenuItem.OnClick = "i_PopupCurForm.w_oGrid.grd.CursorExport('E')"
     loMenuItem = m.loPopupMenu.AddMenuItem()
     loMenuItem.Caption = cp_Translate(MSG_DBF_EXPORT)
     loMenuItem.BeginGroup = .F.
     loMenuItem.Visible = .T.
     loMenuItem.OnClick = "i_PopupCurForm.w_oGrid.grd.CursorExport('D')"
     loMenuItem = m.loPopupMenu.AddMenuItem()
     loMenuItem.Caption = cp_Translate(MSG_CSV_EXPORT)
     loMenuItem.BeginGroup = .F.
     loMenuItem.Visible = .T.
     loMenuItem.OnClick = "i_PopupCurForm.w_oGrid.grd.CursorExport('C')"
  EndProc
  * --- Fine Area Manuale
  *--- Define Header
  add object oHeader as cp_headergadget with Width=this.Width, Left=0, Top=0, nNumPages=2 
  Height = This.Height + this.oHeader.Height

  * --- Define Page Frame
  add object oPgFrm as cp_oPgFrm with PageCount=2, Top=this.oHeader.Height, Width=this.Width, Height=this.Height-this.oHeader.Height
  proc oPgFrm.Init
    cp_oPgFrm::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_gldPag1","gsut_gld",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(2).addobject("oPag","tgsut_gldPag2","gsut_gld",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Pag.2")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_VALUELBL = this.oPgFrm.Pages(1).oPag.VALUELBL
    this.w_IMGOBJ = this.oPgFrm.Pages(1).oPag.IMGOBJ
    this.w_oFooter = this.oPgFrm.Pages(1).oPag.oFooter
    this.w_oGrid = this.oPgFrm.Pages(2).oPag.oGrid
    DoDefault()
    proc Destroy()
      this.w_VALUELBL = .NULL.
      this.w_IMGOBJ = .NULL.
      this.w_oFooter = .NULL.
      this.w_oGrid = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_gld
    This.w_VALUELBL.FontName ='Open Sans'
    This.w_VALUELBL.FontSize = 35
    This.w_VALUELBL.bGlobalFont = .f.
    This.w_VALUELBL.Alignment = 1
    
    This.w_IMGOBJ.Visible = .f.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_QUERYDTL=space(254)
      .w_PRGGRID=space(250)
      .w_VALUE=space(10)
      .w_TITLE=space(254)
      .w_QUERY=space(254)
      .w_RET=.f.
      .w_IMAGE=space(254)
      .w_DATETIME=space(20)
      .w_PROGRAM=space(250)
      .w_BTRSGRID=.f.
      .w_VALUEFORMAT=space(50)
      .w_PRGDESCRI=space(50)
      .w_FONTCLR=0
      .w_IMAGETMP=space(254)
      .w_APPLYTHEMEIMG=.f.
      .w_IMAGETMP_1=space(254)
      .oPgFrm.Page1.oPag.VALUELBL.Calculate(.w_VALUE,.w_FONTCLR)
      .oPgFrm.Page1.oPag.IMGOBJ.Calculate(.w_IMAGETMP)
          .DoRTCalc(1,1,.f.)
        .w_PRGGRID = .w_PRGGRID
      .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
      .oPgFrm.Page2.oPag.oGrid.Calculate(.w_QUERYDTL)
          .DoRTCalc(3,6,.f.)
        .w_IMAGE = ''
          .DoRTCalc(8,10,.f.)
        .w_VALUEFORMAT = ''
          .DoRTCalc(12,12,.f.)
        .w_FONTCLR = Rgb(243,243,243)
          .DoRTCalc(14,14,.f.)
        .w_APPLYTHEMEIMG = .t.
    endwith
    this.DoRTCalc(16,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.VALUELBL.Calculate(.w_VALUE,.w_FONTCLR)
        .oPgFrm.Page1.oPag.IMGOBJ.Calculate(.w_IMAGETMP)
        .DoRTCalc(1,1,.t.)
            .w_PRGGRID = .w_PRGGRID
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
        if .o_QUERYDTL<>.w_QUERYDTL
        .oPgFrm.Page2.oPag.oGrid.Calculate(.w_QUERYDTL)
        endif
      	*--- Assegnamento caption ad oggetto header
          If .o_TITLE <> .w_TITLE
			   .oHeader.Calculate(.oPgFrm.Page1.oPag.oContained.w_TITLE)
          EndIf
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.VALUELBL.Calculate(.w_VALUE,.w_FONTCLR)
        .oPgFrm.Page1.oPag.IMGOBJ.Calculate(.w_IMAGETMP)
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
        .oPgFrm.Page2.oPag.oGrid.Calculate(.w_QUERYDTL)
    endwith
  return

  proc Calculate_OGRWDURRLK()
    with this
          * --- Refresh Label
          .w_RET = RefreshAll(This)
    endwith
  endproc
  proc Calculate_YCUUTVASEB()
    with this
          * --- Gadget Option
          .w_RET = This.GadgetOption()
          .w_RET = RefreshAll(This)
    endwith
  endproc
  proc Calculate_LSNUCPBSNU()
    with this
          * --- Dimensioni gadget massimizzato
          .w_RET = CalcSize(This)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.VALUELBL.Event(cEvent)
      .oPgFrm.Page1.oPag.IMGOBJ.Event(cEvent)
      .oPgFrm.Page1.oPag.oFooter.Event(cEvent)
      .oPgFrm.Page2.oPag.oGrid.Event(cEvent)
        if lower(cEvent)==lower("GadgetOnDemand") or lower(cEvent)==lower("GadgetArranged") or lower(cEvent)==lower("GadgetOnTimer") or lower(cEvent)==lower("oheader RefreshOnDemand")
          .Calculate_OGRWDURRLK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GadgetOption")
          .Calculate_YCUUTVASEB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GadgetMaximize Init") or lower(cEvent)==lower("GadgetMinimize Init")
          .Calculate_LSNUCPBSNU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_gld
    If Lower(cEvent)='w_ogrid dblclick'
       With This
         If !Empty(.w_PRGGRID)
           Local cCursor,l_oldArea,l_oldError,ggname
           m.ggname = 'frm'+Alltrim(.Name)
           .cAlertMsg = ""
           m.l_oldError = On("Error")
           On Error &ggname..cAlertMsg=Message()
           
           m.l_oldArea = Select()
           Select(.w_oGrid.cCursor)
           Go Top
           Skip (.w_oGrid.CurrentRow-1)
           ExecScript(.w_PRGGRID)
           Select(m.l_oldArea)
           
           On error &l_oldError
         Endif
       Endwith
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_QUERYDTL = this.w_QUERYDTL
    this.o_TITLE = this.w_TITLE
    return

enddefine

* --- Define pages as container
define class tgsut_gldPag1 as StdContainer
  Width  = 133
  height = 90
  stdWidth  = 133
  stdheight = 90
  resizeXpos=93
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object VALUELBL as cp_calclbl with uid="DJZRBPOYUI",left=68, top=5, width=56,height=62,;
    caption='xValue',;
   bGlobalFont=.t.,;
    caption="00",Alignment=1,fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=1,fontname="Arial",;
    nPag=1;
    , HelpContextID = 261696118


  add object IMGOBJ as cp_showimage with uid="DVGZJQZVIK",left=3, top=5, width=62,height=62,;
    caption='cImage',;
   bGlobalFont=.t.,;
    stretch=1,default="..\vfcsim\themes\fepa\bmp\gd_Document.png",;
    nPag=1;
    , HelpContextID = 246340646


  add object oFooter as cp_FooterGadget with uid="UMXGAILDJX",left=-1, top=70, width=133,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 225625062
enddefine
define class tgsut_gldPag2 as StdContainer
  Width  = 133
  height = 90
  stdWidth  = 133
  stdheight = 90
  resizeXpos=75
  resizeYpos=52
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oGrid as cp_VirtualZoom with uid="DKKFDHJZBW",left=3, top=3, width=127,height=84,;
    caption='Griglia',;
   bGlobalFont=.t.,;
    ScrollBarsWidth=8,bTrsHeaderTitle=.f.,HeaderFontBold=.t.,HeaderFontItalic=.f.,HeaderFontSize=0,;
    cEvent = "RefreshAll",;
    nPag=2;
    , HelpContextID = 50643814
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_gld','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_gld
*--- Refresh
Proc RefreshAll(pParent)
  Local cCursor,l_Value,l_Title,l_Image,bFoundAlias,l_Anchor,l_oldError,l_OnErrorMsg
  
  With m.pParent
    .cAlertMsg = ""
    
    *--- Trascodifica intestazione colonne
    If Vartype(.w_BTRSGRID)=='L' && Gestione trascodifiche
      .w_oGrid.bTrsHeaderTitle = .w_BTRSGRID
    Endif
    
    *--- Aggiorno la griglia
    .NotifyEvent('RefreshAll')
    
    m.l_oldError = On("Error")
    *--- Eseguo query se passata, altrimento conto i record
    If Not Empty(.w_QUERY) And cp_FileExist(ForceExt(.w_QUERY,'VQR')) 
      cCursor = "Gadget_GLD"+Sys(2015)
      Vq_Exec(.w_QUERY, m.pParent, m.cCursor)
      If Used(m.cCursor)
        On Error m.bFoundAlias = .f.
        
        *--- xValue
        m.bFoundAlias = .t.
        m.l_Value = Tran(&cCursor..xValue, .w_VALUEFORMAT)
        If m.bFoundAlias
          .w_VALUE = Alltrim(m.l_Value)
        Else
          .cAlertMsg = ah_MsgFormat('La query %1 deve avere una colonna con alias "xValue"',Alltrim(.w_QUERY))
          .w_IMGOBJ.Visible = .f.
          On Error &l_oldError
          Return
        Endif
        
        *--- cTitle
        m.bFoundAlias = .t.
        m.l_Title = Alltrim(&cCursor..cTitle)
        If m.bFoundAlias
          .w_TITLE = m.l_Title
        Else
          .w_TITLE = Alltrim(.w_TITLE)
        Endif
        
        *--- cImage
        m.bFoundAlias = .t.
        m.l_Image = Alltrim(&cCursor..cImage)
        If m.bFoundAlias
          .w_IMAGETMP_1 = m.l_Image
        Else
          .w_IMAGETMP_1 = Alltrim(.w_IMAGE)
        Endif
        
        m.l_OnErrorMsg = ""
        On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
        
        *--- Chiusura cursore, altrimenti ho un errore nella mcalc
        Use In Select(m.cCursor)   
        
      EndIf
    Else
      *--- Conto il numero dei record
      m.l_OnErrorMsg = ""
      On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
      
      .w_VALUE = Tran(Reccount(.w_oGrid.cCursor), .w_VALUEFORMAT)
    EndIf
     
    *--- Se l'immagine non � stata passata o non trovo il file nascondo il controllo
    .w_IMAGETMP = Iif(.w_APPLYTHEMEIMG, cp_TmpColorizeImg(.w_IMAGETMP_1, .w_FONTCLR), .w_IMAGETMP_1)
    If !Empty(.w_IMAGETMP) And cp_FileExist(.w_IMAGETMP)
      .w_IMGOBJ.Visible = .t.
      m.l_Anchor = .w_VALUELBL.Anchor
      .w_VALUELBL.Anchor = 0
      .w_VALUELBL.Move(.w_IMGOBJ.Left+.w_IMGOBJ.Width+5, .w_VALUELBL.Top, .Width-.w_IMGOBJ.Width-15)
      .w_VALUELBL.Anchor = m.l_Anchor
    Else
      .w_IMGOBJ.Visible = .f.
      m.l_Anchor = .w_VALUELBL.Anchor
      .w_VALUELBL.Anchor = 0
      .w_VALUELBL.Move(5, .w_VALUELBL.Top, .Width-10)
      .w_VALUELBL.Anchor = m.l_Anchor
    Endif
    
    *--- Aggiorno la data di ultimo aggiornamento
    .tLastUpdate = DateTime()
    .w_DATETIME = Alltrim(TTOC(.tLastUpdate))+' '
    
    On Error &l_oldError
    
    If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
      .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
    Endif
    .mCalc(.T.)
    .SaveDependsOn()
  EndWith
EndProc

Proc CalcSize(pParent)
  With m.pParent
     If .MaxWidth=-1 And .MaxHeight=-1
       local nWidth,nHeight
       *--- Header e testata zoom
       m.nHeight = (.Height-.w_oGrid.Height)+.w_oGrid.nHeaderHeight+(.w_oGrid.nPadding*3)
       *--- Righe
       m.nHeight = m.nHeight + (.w_oGrid.Grd.RowHeight+.w_oGrid.Grd.nVOffset)*.w_oGrid.Grd.RowCount
       m.nHeight = m.nHeight - .w_oGrid.Grd.nVOffset + .w_oGrid.ScrollBarsWidth
       
       m.nWidth = (.Width-.w_oGrid.Width)+(.w_oGrid.nPadding*2)
       *--Colonne
       For i=1 To .w_oGrid.ColumnCount
         m.nWidth = m.nWidth + .w_oGrid.Grd.Columns(i).Width + .w_oGrid.Grd.nHOffset
       Endfor
       m.nWidth = m.nWidth - .w_oGrid.Grd.nHOffset + .w_oGrid.ScrollBarsWidth
       
       .MaxWidth = Max(.Width, m.nWidth)
       .MaxHeight = Max(.Height, m.nHeight)
     Else
       .MaxWidth = -1
       .MaxHeight = -1
     Endif
  EndWith
EndProc
* --- Fine Area Manuale
