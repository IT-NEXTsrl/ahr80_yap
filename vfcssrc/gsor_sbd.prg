* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_sbd                                                        *
*              Stampa brogliaccio ordini                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_65]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2007-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsor_sbd",oParentObject))

* --- Class definition
define class tgsor_sbd as StdForm
  Top    = 13
  Left   = 30

  * --- Standard Properties
  Width  = 562
  Height = 351
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-16"
  HelpContextID=107571561
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  cPrg = "gsor_sbd"
  cComment = "Stampa brogliaccio ordini"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLVEAC1 = space(1)
  o_FLVEAC1 = space(1)
  w_CICLO = space(1)
  o_CICLO = space(1)
  w_CATEGO = space(2)
  o_CATEGO = space(2)
  w_TIPODOC = space(5)
  w_FLVEAC = space(1)
  w_CLIFOR = space(1)
  w_BDATE = ctod('  /  /  ')
  o_BDATE = ctod('  /  /  ')
  w_EDATE = ctod('  /  /  ')
  w_TIPO = space(1)
  w_FLPROV = space(1)
  w_EVAINI = ctod('  /  /  ')
  w_EVAFIN = ctod('  /  /  ')
  w_CLIENTE = space(15)
  w_CODAGE = space(5)
  w_DESCRI1 = space(40)
  w_DESCRI = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CATDOC = space(2)
  w_DESAGE = space(35)
  w_ONUME = 0
  o_ONUME = 0
  w_numero = 0
  w_serie1 = space(10)
  w_numero1 = 0
  w_serie2 = space(10)
  * --- Area Manuale = Declare Variables
  * --- gsor_sbd
  * Eseguo le query in modo sincrono
  sync=.t.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsor_sbdPag1","gsor_sbd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLVEAC1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AGENTI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsor_sbd
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLVEAC1=space(1)
      .w_CICLO=space(1)
      .w_CATEGO=space(2)
      .w_TIPODOC=space(5)
      .w_FLVEAC=space(1)
      .w_CLIFOR=space(1)
      .w_BDATE=ctod("  /  /  ")
      .w_EDATE=ctod("  /  /  ")
      .w_TIPO=space(1)
      .w_FLPROV=space(1)
      .w_EVAINI=ctod("  /  /  ")
      .w_EVAFIN=ctod("  /  /  ")
      .w_CLIENTE=space(15)
      .w_CODAGE=space(5)
      .w_DESCRI1=space(40)
      .w_DESCRI=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CATDOC=space(2)
      .w_DESAGE=space(35)
      .w_ONUME=0
      .w_numero=0
      .w_serie1=space(10)
      .w_numero1=0
      .w_serie2=space(10)
        .w_FLVEAC1 = 'V'
        .w_CICLO = IIF(.w_FLVEAC1='E', ' ', .w_FLVEAC1)
        .w_CATEGO = 'OR'
        .w_TIPODOC = SPACE(5)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_TIPODOC))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,6,.f.)
        .w_BDATE = G_INIESE
        .w_EDATE = G_FINESE
        .w_TIPO = IIF(.w_FLVEAC1='V', 'C', IIF(.w_FLVEAC1='A', 'F', ' '))
        .w_FLPROV = 'N'
        .w_EVAINI = IIF(.w_ONUME=3 OR .w_ONUME=4,.w_EVAINI,cp_CharToDate('  -  -    ') )
        .w_EVAFIN = IIF(.w_ONUME=3 OR .w_ONUME=4,.w_EVAFIN,cp_CharToDate('  -  -    ') )
        .w_CLIENTE = SPACE(15)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CLIENTE))
          .link_1_13('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CODAGE))
          .link_1_14('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
          .DoRTCalc(15,16,.f.)
        .w_OBTEST = IIF(EMPTY(.w_BDATE),i_INIDAT,.w_BDATE)
          .DoRTCalc(18,21,.f.)
        .w_numero = 1
        .w_serie1 = ''
        .w_numero1 = 999999999999999
        .w_serie2 = ''
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_CICLO = IIF(.w_FLVEAC1='E', ' ', .w_FLVEAC1)
            .w_CATEGO = 'OR'
        if .o_CATEGO<>.w_CATEGO.or. .o_CICLO<>.w_CICLO
            .w_TIPODOC = SPACE(5)
          .link_1_4('Full')
        endif
        .DoRTCalc(5,8,.t.)
            .w_TIPO = IIF(.w_FLVEAC1='V', 'C', IIF(.w_FLVEAC1='A', 'F', ' '))
        .DoRTCalc(10,10,.t.)
        if .o_ONUME<>.w_ONUME
            .w_EVAINI = IIF(.w_ONUME=3 OR .w_ONUME=4,.w_EVAINI,cp_CharToDate('  -  -    ') )
        endif
        if .o_ONUME<>.w_ONUME
            .w_EVAFIN = IIF(.w_ONUME=3 OR .w_ONUME=4,.w_EVAFIN,cp_CharToDate('  -  -    ') )
        endif
        if .o_FLVEAC1<>.w_FLVEAC1
            .w_CLIENTE = SPACE(15)
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(14,16,.t.)
        if .o_BDATE<>.w_BDATE
            .w_OBTEST = IIF(EMPTY(.w_BDATE),i_INIDAT,.w_BDATE)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(18,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oEVAINI_1_11.enabled = this.oPgFrm.Page1.oPag.oEVAINI_1_11.mCond()
    this.oPgFrm.Page1.oPag.oEVAFIN_1_12.enabled = this.oPgFrm.Page1.oPag.oEVAFIN_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCLIENTE_1_13.enabled = this.oPgFrm.Page1.oPag.oCLIENTE_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPODOC
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPODOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOR_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPODOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPODOC))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPODOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPODOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPODOC_1_4'),i_cWhere,'GSOR_ATD',"Causali ordini",'GSOR1SBD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPODOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPODOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPODOC)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPODOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCRI = NVL(_Link_.TDDESDOC,space(35))
      this.w_CLIFOR = NVL(_Link_.TDFLINTE,space(1))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPODOC = space(5)
      endif
      this.w_DESCRI = space(35)
      this.w_CLIFOR = space(1)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TIPODOC) OR (.w_CATDOC='OR' AND (.w_FLVEAC1='E' OR .w_FLVEAC=.w_FLVEAC1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TIPODOC = space(5)
        this.w_DESCRI = space(35)
        this.w_CLIFOR = space(1)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPODOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLIENTE
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_CLIENTE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIENTE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLIENTE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLIENTE_1_13'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLIENTE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_CLIENTE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIENTE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLIENTE = space(15)
      endif
      this.w_DESCRI1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endif
        this.w_CLIENTE = space(15)
        this.w_DESCRI1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE_1_14'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLVEAC1_1_1.RadioValue()==this.w_FLVEAC1)
      this.oPgFrm.Page1.oPag.oFLVEAC1_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODOC_1_4.value==this.w_TIPODOC)
      this.oPgFrm.Page1.oPag.oTIPODOC_1_4.value=this.w_TIPODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oBDATE_1_7.value==this.w_BDATE)
      this.oPgFrm.Page1.oPag.oBDATE_1_7.value=this.w_BDATE
    endif
    if not(this.oPgFrm.Page1.oPag.oEDATE_1_8.value==this.w_EDATE)
      this.oPgFrm.Page1.oPag.oEDATE_1_8.value=this.w_EDATE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPROV_1_10.RadioValue()==this.w_FLPROV)
      this.oPgFrm.Page1.oPag.oFLPROV_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEVAINI_1_11.value==this.w_EVAINI)
      this.oPgFrm.Page1.oPag.oEVAINI_1_11.value=this.w_EVAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oEVAFIN_1_12.value==this.w_EVAFIN)
      this.oPgFrm.Page1.oPag.oEVAFIN_1_12.value=this.w_EVAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIENTE_1_13.value==this.w_CLIENTE)
      this.oPgFrm.Page1.oPag.oCLIENTE_1_13.value=this.w_CLIENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODAGE_1_14.value==this.w_CODAGE)
      this.oPgFrm.Page1.oPag.oCODAGE_1_14.value=this.w_CODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_21.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_21.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_26.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_26.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_33.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_33.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.onumero_1_39.value==this.w_numero)
      this.oPgFrm.Page1.oPag.onumero_1_39.value=this.w_numero
    endif
    if not(this.oPgFrm.Page1.oPag.oserie1_1_40.value==this.w_serie1)
      this.oPgFrm.Page1.oPag.oserie1_1_40.value=this.w_serie1
    endif
    if not(this.oPgFrm.Page1.oPag.onumero1_1_41.value==this.w_numero1)
      this.oPgFrm.Page1.oPag.onumero1_1_41.value=this.w_numero1
    endif
    if not(this.oPgFrm.Page1.oPag.oserie2_1_42.value==this.w_serie2)
      this.oPgFrm.Page1.oPag.oserie2_1_42.value=this.w_serie2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_TIPODOC) OR (.w_CATDOC='OR' AND (.w_FLVEAC1='E' OR .w_FLVEAC=.w_FLVEAC1)))  and not(empty(.w_TIPODOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPODOC_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_BDATE)) or not((empty(.w_edate)) OR (.w_edate>=.w_bdate)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBDATE_1_7.SetFocus()
            i_bnoObbl = !empty(.w_BDATE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   ((empty(.w_EDATE)) or not((.w_EDATE>=.w_BDATE) OR  ( empty(.w_bdate))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEDATE_1_8.SetFocus()
            i_bnoObbl = !empty(.w_EDATE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not((.w_EVAFIN>=.w_EVAINI) OR  EMPTY(.w_EVAFIN))  and (.w_ONUME=3 OR .w_ONUME=4)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVAINI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_EVAFIN>=.w_EVAINI) OR  EMPTY(.w_EVAFIN))  and (.w_ONUME=3 OR .w_ONUME=4)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVAFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_TIPO $ 'CF')  and not(empty(.w_CLIENTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLIENTE_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
          case   not((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onumero_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del codice finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oserie1_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onumero1_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del codice finale")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oserie2_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC1 = this.w_FLVEAC1
    this.o_CICLO = this.w_CICLO
    this.o_CATEGO = this.w_CATEGO
    this.o_BDATE = this.w_BDATE
    this.o_ONUME = this.w_ONUME
    return

enddefine

* --- Define pages as container
define class tgsor_sbdPag1 as StdContainer
  Width  = 558
  height = 351
  stdWidth  = 558
  stdheight = 351
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLVEAC1_1_1 as StdCombo with uid="HBDTMJXMXR",rtseq=1,rtrep=.f.,left=128,top=10,width=123,height=21;
    , ToolTipText = "Tipo gestione interessata";
    , HelpContextID = 15812438;
    , cFormVar="w_FLVEAC1",RowSource=""+"Impegni da cliente,"+"Ordini a fornitore,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC1_1_1.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oFLVEAC1_1_1.GetRadio()
    this.Parent.oContained.w_FLVEAC1 = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC1_1_1.SetRadio()
    this.Parent.oContained.w_FLVEAC1=trim(this.Parent.oContained.w_FLVEAC1)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC1=='V',1,;
      iif(this.Parent.oContained.w_FLVEAC1=='A',2,;
      iif(this.Parent.oContained.w_FLVEAC1=='E',3,;
      0)))
  endfunc

  add object oTIPODOC_1_4 as StdField with uid="GEDQIYCERT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TIPODOC", cQueryName = "TIPODOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo ordine selezionato",;
    HelpContextID = 220914998,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=128, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOR_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPODOC"

  func oTIPODOC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPODOC_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPODOC_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPODOC_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOR_ATD',"Causali ordini",'GSOR1SBD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPODOC_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSOR_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPODOC
     i_obj.ecpSave()
  endproc

  add object oBDATE_1_7 as StdField with uid="GEGFYSWTGU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_BDATE", cQueryName = "BDATE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento di inizio stampa",;
    HelpContextID = 29429994,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=128, Top=109

  func oBDATE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_edate)) OR (.w_edate>=.w_bdate))
    endwith
    return bRes
  endfunc

  add object oEDATE_1_8 as StdField with uid="DPWRCZZCGD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_EDATE", cQueryName = "EDATE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento di fine stampa",;
    HelpContextID = 29429946,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=128, Top=137

  func oEDATE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EDATE>=.w_BDATE) OR  ( empty(.w_bdate)))
    endwith
    return bRes
  endfunc


  add object oFLPROV_1_10 as StdCombo with uid="QMVLQXRXPN",value=3,rtseq=10,rtrep=.f.,left=325,top=164,width=103,height=21;
    , ToolTipText = "Permette di selezionare tutti i documenti, solo quelli provvisori o confermati";
    , HelpContextID = 81651542;
    , cFormVar="w_FLPROV",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPROV_1_10.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oFLPROV_1_10.GetRadio()
    this.Parent.oContained.w_FLPROV = this.RadioValue()
    return .t.
  endfunc

  func oFLPROV_1_10.SetRadio()
    this.Parent.oContained.w_FLPROV=trim(this.Parent.oContained.w_FLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_FLPROV=='N',1,;
      iif(this.Parent.oContained.w_FLPROV=='S',2,;
      iif(this.Parent.oContained.w_FLPROV=='',3,;
      0)))
  endfunc

  add object oEVAINI_1_11 as StdField with uid="WFGARTRYAT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_EVAINI", cQueryName = "EVAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona ordini evasi a partire dalla data (vuoto = no selezione)",;
    HelpContextID = 138149562,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=128, Top=164

  func oEVAINI_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME=3 OR .w_ONUME=4)
    endwith
   endif
  endfunc

  func oEVAINI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EVAFIN>=.w_EVAINI) OR  EMPTY(.w_EVAFIN))
    endwith
    return bRes
  endfunc

  add object oEVAFIN_1_12 as StdField with uid="OLAELJLGVP",rtseq=12,rtrep=.f.,;
    cFormVar = "w_EVAFIN", cQueryName = "EVAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona ordini evasi fino alla data (vuoto = no selezione)",;
    HelpContextID = 59702970,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=128, Top=191

  func oEVAFIN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME=3 OR .w_ONUME=4)
    endwith
   endif
  endfunc

  func oEVAFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EVAFIN>=.w_EVAINI) OR  EMPTY(.w_EVAFIN))
    endwith
    return bRes
  endfunc

  add object oCLIENTE_1_13 as StdField with uid="EDDONEAZYD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CLIENTE", cQueryName = "CLIENTE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente oppure obsoleto",;
    ToolTipText = "Eventuale selezione su intestatario (vuoto = no selezione)",;
    HelpContextID = 46167846,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=128, Top=219, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLIENTE"

  func oCLIENTE_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPO $ 'CF')
    endwith
   endif
  endfunc

  func oCLIENTE_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIENTE_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIENTE_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLIENTE_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCLIENTE_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_CLIENTE
     i_obj.ecpSave()
  endproc

  add object oCODAGE_1_14 as StdField with uid="QRSSCPNUDR",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODAGE", cQueryName = "CODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale selezione su agente (vuoto = no selezione)",;
    HelpContextID = 213112282,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=128, Top=247, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE"

  func oCODAGE_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oCODAGE_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CODAGE
     i_obj.ecpSave()
  endproc

  add object oDESCRI1_1_21 as StdField with uid="OFSZWBAZUK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 134156342,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=259, Top=219, InputMask=replicate('X',40)


  add object oObj_1_22 as cp_outputCombo with uid="ZLERXIVPWT",left=128, top=274, width=409,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 70426086


  add object oBtn_1_23 as StdButton with uid="QUDTNUTATM",left=439, top=300, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 34218022;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="FKSLKGVGAU",left=489, top=300, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 100254138;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI_1_26 as StdField with uid="AUHMCIPLLJ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134279114,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=193, Top=42, InputMask=replicate('X',35)

  add object oDESAGE_1_33 as StdField with uid="XWASPHVOQY",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 213053386,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=193, Top=247, InputMask=replicate('X',35)

  add object onumero_1_39 as StdField with uid="BXBQYPGVTU",rtseq=22,rtrep=.f.,;
    cFormVar = "w_numero", cQueryName = "numero",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del codice finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 2286294,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=325, Top=109, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func onumero_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))
    endwith
    return bRes
  endfunc

  add object oserie1_1_40 as StdField with uid="HVPVSEHHGU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_serie1", cQueryName = "serie1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Alfa del documento iniziale selezionato",;
    HelpContextID = 245947610,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=454, Top=109, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oserie1_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object onumero1_1_41 as StdField with uid="PROBAWFVAK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_numero1", cQueryName = "numero1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del codice finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 2286294,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=325, Top=137, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func onumero1_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO1)))
    endwith
    return bRes
  endfunc

  add object oserie2_1_42 as StdField with uid="LHPNDMBVHJ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_serie2", cQueryName = "serie2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Alfa del documento finale selezionato",;
    HelpContextID = 229170394,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=454, Top=137, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oserie2_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc

  add object oStr_1_15 as StdString with uid="PTBMLNPXMM",Visible=.t., Left=65, Top=109,;
    Alignment=1, Width=61, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="WUTHOLPFWZ",Visible=.t., Left=82, Top=137,;
    Alignment=1, Width=44, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="HXRVZPWGTU",Visible=.t., Left=240, Top=109,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="EXTLSRLMHU",Visible=.t., Left=255, Top=137,;
    Alignment=1, Width=68, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="PICXELHVWO",Visible=.t., Left=27, Top=79,;
    Alignment=0, Width=117, Height=15,;
    Caption="Selezione ordini"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="RAZKTALMLF",Visible=.t., Left=32, Top=274,;
    Alignment=1, Width=94, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="SWNLMJDLFX",Visible=.t., Left=32, Top=10,;
    Alignment=1, Width=94, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="YPJFGXUJDZ",Visible=.t., Left=32, Top=219,;
    Alignment=1, Width=94, Height=15,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="FDXPGPXJMQ",Visible=.t., Left=32, Top=42,;
    Alignment=1, Width=94, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="HUQWDPSGKF",Visible=.t., Left=64, Top=247,;
    Alignment=1, Width=62, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="BFSQTYSSIT",Visible=.t., Left=17, Top=164,;
    Alignment=1, Width=109, Height=18,;
    Caption="Da data evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="FOHDYFIDYS",Visible=.t., Left=24, Top=191,;
    Alignment=1, Width=102, Height=18,;
    Caption="A data evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="DMPRCDDUMT",Visible=.t., Left=256, Top=164,;
    Alignment=1, Width=67, Height=18,;
    Caption="Stato doc:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="LVVBINNLET",Visible=.t., Left=442, Top=109,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="PKVCFMTCZS",Visible=.t., Left=442, Top=137,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oBox_1_19 as StdBox with uid="JFHNVAVHPZ",left=18, top=94, width=538,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsor_sbd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
