* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_snp                                                        *
*              Stampa numerazione pagine                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_15]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-27                                                      *
* Last revis.: 2008-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_snp",oParentObject))

* --- Class definition
define class tgscg_snp as StdForm
  Top    = 35
  Left   = 95

  * --- Standard Properties
  Width  = 457
  Height = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-07"
  HelpContextID=93706903
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_snp"
  cComment = "Stampa numerazione pagine"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FILTRO = space(1)
  w_INIPAG = 0
  w_FINPAG = 0
  w_ANNO = space(4)
  w_giornale = space(1)
  w_LEGENDA = space(1)
  w_TESTA = space(70)
  w_TESTO = space(1)
  w_ORINUM = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_snpPag1","gscg_snp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oINIPAG_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_snp
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FILTRO=space(1)
      .w_INIPAG=0
      .w_FINPAG=0
      .w_ANNO=space(4)
      .w_giornale=space(1)
      .w_LEGENDA=space(1)
      .w_TESTA=space(70)
      .w_TESTO=space(1)
      .w_ORINUM=space(1)
        .w_FILTRO = this.oParentObject
        .w_INIPAG = 1
        .w_FINPAG = .w_inipag + 100
        .w_ANNO = left(dtos(i_datsys),4)
        .w_giornale = this.oParentObject
          .DoRTCalc(6,6,.f.)
        .w_TESTA = iif(this.oParentObject='G','REGISTRO CESPITI','')
          .DoRTCalc(8,8,.f.)
        .w_ORINUM = iif(.w_FILTRO='G','O','V')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLEGENDA_1_6.enabled = this.oPgFrm.Page1.oPag.oLEGENDA_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oINIPAG_1_2.value==this.w_INIPAG)
      this.oPgFrm.Page1.oPag.oINIPAG_1_2.value=this.w_INIPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oFINPAG_1_3.value==this.w_FINPAG)
      this.oPgFrm.Page1.oPag.oFINPAG_1_3.value=this.w_FINPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_4.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_4.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.ogiornale_1_5.RadioValue()==this.w_giornale)
      this.oPgFrm.Page1.oPag.ogiornale_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLEGENDA_1_6.RadioValue()==this.w_LEGENDA)
      this.oPgFrm.Page1.oPag.oLEGENDA_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTESTA_1_7.value==this.w_TESTA)
      this.oPgFrm.Page1.oPag.oTESTA_1_7.value=this.w_TESTA
    endif
    if not(this.oPgFrm.Page1.oPag.oTESTO_1_8.RadioValue()==this.w_TESTO)
      this.oPgFrm.Page1.oPag.oTESTO_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oORINUM_1_17.RadioValue()==this.w_ORINUM)
      this.oPgFrm.Page1.oPag.oORINUM_1_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_INIPAG<=.w_FINPAG)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFINPAG_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Pagina finale pi� grande di quella iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_snpPag1 as StdContainer
  Width  = 453
  height = 202
  stdWidth  = 453
  stdheight = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINIPAG_1_2 as StdField with uid="FYLELHYVJD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_INIPAG", cQueryName = "INIPAG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Pagina iniziale",;
    HelpContextID = 252002938,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=120, Top=12, cSayPict='"9999999"', cGetPict='"9999999"'

  add object oFINPAG_1_3 as StdField with uid="SNHZBLJPGO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FINPAG", cQueryName = "FINPAG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Pagina finale pi� grande di quella iniziale",;
    ToolTipText = "Pagina finale",;
    HelpContextID = 251983786,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=120, Top=38, cSayPict='"9999999"', cGetPict='"9999999"'

  func oFINPAG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_INIPAG<=.w_FINPAG)
    endwith
    return bRes
  endfunc

  add object oANNO_1_4 as StdField with uid="CFGKMNGNKW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'anno",;
    HelpContextID = 99224838,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=324, Top=12, InputMask=replicate('X',4)


  add object ogiornale_1_5 as StdCombo with uid="VRFOZCPDTN",rtseq=5,rtrep=.f.,left=119,top=66,width=140,height=21;
    , ToolTipText = "Tipo bollato selezionato";
    , HelpContextID = 34653493;
    , cFormVar="w_giornale",RowSource=""+"Registri IVA,"+"Altri libri bollati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func ogiornale_1_5.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func ogiornale_1_5.GetRadio()
    this.Parent.oContained.w_giornale = this.RadioValue()
    return .t.
  endfunc

  func ogiornale_1_5.SetRadio()
    this.Parent.oContained.w_giornale=trim(this.Parent.oContained.w_giornale)
    this.value = ;
      iif(this.Parent.oContained.w_giornale=='I',1,;
      iif(this.Parent.oContained.w_giornale=='G',2,;
      0))
  endfunc

  add object oLEGENDA_1_6 as StdCheck with uid="IBJXJRGLEO",rtseq=6,rtrep=.f.,left=275, top=40, caption="Legenda sigle IVA",;
    ToolTipText = "Stampa legenda sigle IVA sulla prima pagina",;
    HelpContextID = 20998986,;
    cFormVar="w_LEGENDA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLEGENDA_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oLEGENDA_1_6.GetRadio()
    this.Parent.oContained.w_LEGENDA = this.RadioValue()
    return .t.
  endfunc

  func oLEGENDA_1_6.SetRadio()
    this.Parent.oContained.w_LEGENDA=trim(this.Parent.oContained.w_LEGENDA)
    this.value = ;
      iif(this.Parent.oContained.w_LEGENDA=='S',1,;
      0)
  endfunc

  func oLEGENDA_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GIORNALE='I')
    endwith
   endif
  endfunc

  add object oTESTA_1_7 as StdField with uid="FERHPVDQEQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TESTA", cQueryName = "TESTA",;
    bObbl = .f. , nPag = 1, value=space(70), bMultilanguage =  .f.,;
    ToolTipText = "Riga di intestazione",;
    HelpContextID = 167728438,;
   bGlobalFont=.t.,;
    Height=21, Width=394, Left=4, Top=127, InputMask=replicate('X',70)

  add object oTESTO_1_8 as StdCheck with uid="SDHKYDMNEQ",rtseq=8,rtrep=.f.,left=14, top=154, caption="Stampa solo testo",;
    ToolTipText = "Stampa solo testo",;
    HelpContextID = 182408502,;
    cFormVar="w_TESTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTESTO_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTESTO_1_8.GetRadio()
    this.Parent.oContained.w_TESTO = this.RadioValue()
    return .t.
  endfunc

  func oTESTO_1_8.SetRadio()
    this.Parent.oContained.w_TESTO=trim(this.Parent.oContained.w_TESTO)
    this.value = ;
      iif(this.Parent.oContained.w_TESTO=='S',1,;
      0)
  endfunc


  add object oBtn_1_9 as StdButton with uid="FHYLREEFKC",left=349, top=152, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 93735654;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        do GSCG_BNP with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="NQSNFZOQHN",left=398, top=152, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 101024326;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oORINUM_1_17 as StdCombo with uid="PODAYUYDVJ",rtseq=9,rtrep=.f.,left=336,top=66,width=111,height=21;
    , ToolTipText = "Orientamento numerazione pagine";
    , HelpContextID = 130498074;
    , cFormVar="w_ORINUM",RowSource=""+"Verticale,"+"Orizzontale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORINUM_1_17.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'O',;
    space(1))))
  endfunc
  func oORINUM_1_17.GetRadio()
    this.Parent.oContained.w_ORINUM = this.RadioValue()
    return .t.
  endfunc

  func oORINUM_1_17.SetRadio()
    this.Parent.oContained.w_ORINUM=trim(this.Parent.oContained.w_ORINUM)
    this.value = ;
      iif(this.Parent.oContained.w_ORINUM=='V',1,;
      iif(this.Parent.oContained.w_ORINUM=='O',2,;
      0))
  endfunc

  add object oStr_1_11 as StdString with uid="KQJFOUFBGV",Visible=.t., Left=213, Top=12,;
    Alignment=1, Width=108, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="MNZAHRECHX",Visible=.t., Left=8, Top=108,;
    Alignment=0, Width=386, Height=15,;
    Caption="Descrizione libro bollato (libro giornale - registro IVA....)"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="KJLGWXGZFY",Visible=.t., Left=11, Top=66,;
    Alignment=1, Width=107, Height=15,;
    Caption="Tipo bollato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="OIJHRWJLKL",Visible=.t., Left=32, Top=12,;
    Alignment=1, Width=86, Height=15,;
    Caption="Da pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="WJSIIOZTFI",Visible=.t., Left=32, Top=38,;
    Alignment=1, Width=86, Height=15,;
    Caption="A pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="ORJTBXHBWF",Visible=.t., Left=263, Top=66,;
    Alignment=1, Width=68, Height=18,;
    Caption="Tipo num.:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_FILTRO<>'G' or .w_GIORNALE='I')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_snp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
