* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_brp                                                        *
*              Aggiorna righe partite in P.N.                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_60]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-25                                                      *
* Last revis.: 2011-11-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_brp",oParentObject)
return(i_retval)

define class tgscg_brp as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_TmpN1 = 0
  w_IMPDCA = 0
  w_TIPCON = space(1)
  w_PAGCON = space(5)
  w_IMPDCP = 0
  w_CODCON = space(15)
  w_IMPABB = 0
  w_CONDCA = space(15)
  w_ROWORD = 0
  w_ROWORD1 = 0
  w_IMPABP = 0
  w_CONDCP = space(15)
  w_APPO = 0
  w_IMPABA = 0
  w_CONABA = space(15)
  w_TmpN2 = 0
  w_OREC = 0
  w_CONABP = space(15)
  w_IMPPDP = 0
  w_IMPPDA = 0
  w_TIPCLF = space(1)
  w_CODCLF = space(15)
  w_FLAGAC = space(1)
  w_CAUNOP = space(5)
  w_MESS = space(10)
  w_TOTABB = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  CONTI_idx=0
  CAU_CONT_idx=0
  PAG_AMEN_idx=0
  CONTROPA_idx=0
  MOD_PAGA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Righe Differenza Cambi, Abbuoni e Acconto da Chiusura Partite in P.N. (Chiamato da GSCG_MPN)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_PADRE = this.oParentObject
    this.w_PADRE.MarkPos()     
     
 Select ( this.w_PADRE.cTrsName ) 
 Go Top
    SCAN FOR (NOT EMPTY(t_PNCODCON)) AND NOT DELETED()
    do case
      case t_PNTIPCON="G" AND t_PNCODCON=this.w_CONDCA
        * --- Trovata Riga Differenza Cambi Attiva
        this.w_PADRE.WorkFromTrs()     
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
        this.oParentObject.w_FLAGTOT = NVL(t_FLAGTOT, " ")
        this.w_APPO = this.w_IMPDCA + ABS(this.oParentObject.w_PNIMPDAR)- ABS(this.oParentObject.w_PNIMPAVE)
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPDCA = 0
      case t_PNTIPCON="G" AND t_PNCODCON=this.w_CONDCP
        * --- Trovata Riga Differenza Cambi Passiva
        this.w_PADRE.WorkFromTrs()     
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
        this.oParentObject.w_FLAGTOT = NVL(t_FLAGTOT, " ")
        this.w_APPO = this.w_IMPDCP + ABS(this.oParentObject.w_PNIMPDAR)- ABS(this.oParentObject.w_PNIMPAVE)
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPDCP = 0
      case t_PNTIPCON=this.w_TIPCON AND t_PNCODCON=this.w_CODCON AND t_PNFLABAN="AB"+this.w_ROWORD
        * --- Trovata Riga Cli/For Abbuono
        this.w_PADRE.WorkFromTrs()     
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
        this.oParentObject.w_FLAGTOT = NVL(t_FLAGTOT, " ")
        * --- Inverte il Segno perche' va nella sezione Opposta
        this.w_APPO = -this.w_IMPABB 
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPABB = 0
      case t_PNTIPCON="G" AND t_PNCODCON=this.w_CONABP AND t_PNFLABAN="PA"+this.w_ROWORD
        * --- Trovata Riga  Abbuono Passivo
        this.w_PADRE.WorkFromTrs()     
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
        this.oParentObject.w_FLAGTOT = NVL(t_FLAGTOT, " ")
        this.w_APPO = this.w_IMPABP 
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPABP = 0
      case t_PNTIPCON="G" AND t_PNCODCON=this.w_CONABA AND t_PNFLABAN="AT"+this.w_ROWORD
        * --- Trovata Riga  Abbuono Attivo
        this.w_PADRE.WorkFromTrs()     
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
        this.oParentObject.w_FLAGTOT = NVL(t_FLAGTOT, " ")
        this.w_APPO = this.w_IMPABA
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPABA = 0
      case t_PNTIPCON=this.w_TIPCLF AND t_PNCODCON=this.w_CODCLF AND t_PNFLSALF="N" AND (this.w_IMPPDP<>0 OR this.w_IMPPDA<>0) AND t_PARTSN=" "
        this.w_PADRE.WorkFromTrs()     
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
        this.oParentObject.w_FLAGTOT = NVL(t_FLAGTOT, " ")
        this.w_APPO = 0
        if this.w_IMPPDA<>0
          this.w_APPO = this.w_IMPPDA + ABS(this.oParentObject.w_PNIMPDAR)- ABS(this.oParentObject.w_PNIMPAVE)
          this.w_IMPPDA = 0
        endif
        if this.w_IMPPDP<>0
          this.w_APPO = this.w_APPO + this.w_IMPPDP + ABS(this.oParentObject.w_PNIMPDAR)- ABS(this.oParentObject.w_PNIMPAVE)
          this.w_IMPPDP = 0
        endif
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    ENDSCAN
    * --- Scrive gli altri Importi non trovati sulle Righe...
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Scrive importo sulla eventuale riga Cassa/Banca...
    if this.oParentObject.w_TOTDAR<>this.oParentObject.w_TOTAVE 
      this.w_TmpN1 = this.oParentObject.w_TOTDAR-this.oParentObject.w_TOTAVE
      SCAN FOR (NOT EMPTY(t_PNCODCON)) AND t_FLAGTOT $ "TDPIA" AND (NOT DELETED()) ; 
 AND ((t_PNIMPDAR=0 AND t_FLDAVE="D") OR (t_PNIMPAVE=0 AND t_FLDAVE="A"))
      this.w_PADRE.WorkFromTrs()     
      * --- La differenza va sulla sezione Opposta per chiudere...
      this.oParentObject.w_PNIMPDAR = IIF(this.w_TmpN1<0, ABS(this.w_TmpN1), 0)
      this.oParentObject.w_PNIMPAVE = IIF(this.w_TmpN1>0, this.w_TmpN1, 0)
      this.oParentObject.w_SLIMPDAR = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE>=0,this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE,0)
      this.oParentObject.w_SLIMPAVE = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE<0,ABS(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE),0)
      this.oParentObject.w_IMPDAR = this.oParentObject.w_PNIMPDAR
      this.oParentObject.w_IMPAVE = this.oParentObject.w_PNIMPAVE
      this.oParentObject.w_FLDAVE = IIF(this.oParentObject.w_PNIMPDAR<>0, "D", IIF(this.oParentObject.w_PNIMPAVE<>0, "A", " "))
      this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR + this.oParentObject.w_SLIMPDAR
      this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE + this.oParentObject.w_SLIMPAVE
      this.oParentObject.w_FLAGTOT = NVL(t_FLAGTOT, " ")
      * --- Aggiorna Totalizzatore Automatismi
      this.oParentObject.w_TOTFLAG = AGGFLAG(this.oParentObject.w_FLAGTOT, 0, this.oParentObject.w_PNIMPDAR+this.oParentObject.w_PNIMPAVE, this.oParentObject.w_TOTFLAG)
      * --- Aggiorna gli <Old> per prevenire successivi Calcoli
      this.w_PADRE.SetControlsValue()     
      * --- Carica il Temporaneo Primanota
      this.w_PADRE.TrsFromWork()     
      * --- Flag Notifica Riga Variata
      if NOT i_SRV $ "AU"
        replace i_SRV with "U"
      endif
      * --- Aggiorna solo la prima riga valida
      EXIT
      ENDSCAN
    endif
    this.w_PADRE.Repos(.T.)     
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Variabili Locali e Globali
    * --- Carica i Valori nelle Variabili Locali e Azzera quelli su Riga
    *     utilizzo with perch� sia le locali che le globali hanno lo
    *     stesso nome, dichiarandole quindi sia locali e globali potrei
    *     nella generazione creare un Prg sbagliato
    WITH this.oParentObject
    this.w_TIPCLF = .w_PNTIPCLF
    this.w_CODCLF = .w_PNCODCLF
    this.w_IMPDCA = cp_ROUND(.w_IMPDCA, 6)
    this.w_IMPDCP = cp_ROUND(.w_IMPDCP, 6)
    this.w_IMPABB = cp_ROUND(.w_IMPABB, 6)
    this.w_TOTABB = cp_Round(.w_IMPABB+.w_IMPABA+.w_IMPABP,6)
    this.w_IMPABP = cp_ROUND(IIF(this.w_TOTABB>0, this.w_TOTABB, 0), 6)
    this.w_IMPABA = cp_ROUND(IIF(this.w_TOTABB>0, 0, this.w_TOTABB), 6)
    this.w_IMPPDP = cp_ROUND(.w_IMPPDP, 6)
    this.w_IMPPDA = cp_ROUND(.w_IMPPDA, 6)
    this.w_FLAGAC = .w_FLAGAC
     
 .w_IMPDCA = 0 
 .w_IMPDCP = 0 
 .w_IMPABB = 0 
 .w_IMPPDP = 0 
 .w_IMPPDA = 0
    * --- Contropartite per Abbuoni ecc..
    this.w_TIPCON = .w_PNTIPCON
    this.w_CODCON = .w_PNCODCON
    this.w_ROWORD = ALLTRIM(STR(.w_CPROWNUM, 4, 0))
    this.w_ROWORD1 = .w_CPROWORD
    ENDWITH
    this.oParentObject.w_FLAGTOT = " "
    * --- Legge Contropartite
    this.w_PAGCON = SPACE(5)
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COSADIFC,COSADIFN,COSAABBA,COSAABBP,COSAPAGA,COCAUNOP"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COSADIFC,COSADIFN,COSAABBA,COSAABBP,COSAPAGA,COCAUNOP;
        from (i_cTable) where;
            COCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CONDCA = NVL(cp_ToDate(_read_.COSADIFC),cp_NullValue(_read_.COSADIFC))
      this.w_CONDCP = NVL(cp_ToDate(_read_.COSADIFN),cp_NullValue(_read_.COSADIFN))
      this.w_CONABA = NVL(cp_ToDate(_read_.COSAABBA),cp_NullValue(_read_.COSAABBA))
      this.w_CONABP = NVL(cp_ToDate(_read_.COSAABBP),cp_NullValue(_read_.COSAABBP))
      this.w_PAGCON = NVL(cp_ToDate(_read_.COSAPAGA),cp_NullValue(_read_.COSAPAGA))
      this.w_CAUNOP = NVL(cp_ToDate(_read_.COCAUNOP),cp_NullValue(_read_.COCAUNOP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Righe Finali in Fondo alla Movimentazione
     
 Select ( this.w_PADRE.cTrsName ) 
 Go Bottom
    if BOF() AND RECCOUNT()<>0
      SKIP -1
    endif
    this.w_MESS = " "
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    do while this.w_IMPDCA<>0 OR this.w_IMPDCP<>0 OR this.w_IMPABB<>0 OR this.w_IMPABA<>0 OR this.w_IMPABP<>0 OR this.w_IMPPDP<>0 OR this.w_IMPPDA<>0 
      SELECT ( this.w_PADRE.cTrsName )
      if NOT EMPTY(t_PNCODCON)
        * --- Se non sono su una riga di Append, scrivo nuova riga
        this.w_PADRE.InitRow()     
      else
        this.w_PADRE.WorkFromTrs()     
      endif
      this.w_PADRE.o_PNIMPDAR = 0
      this.w_PADRE.o_PNIMPAVE = 0
      * --- Differenza Cambi Attiva
      if this.w_IMPDCA<>0
        this.oParentObject.w_PNTIPCON = "G"
        this.oParentObject.w_PNCODCON = this.w_CONDCA
        this.oParentObject.w_PNFLPART = "N"
        this.oParentObject.w_PNCODPAG = SPACE(5)
        this.oParentObject.w_FLAGTOT = "+"
        this.w_APPO = this.w_IMPDCA
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPDCA = 0
        LOOP
      endif
      * --- Riga Cliente\Fornitore per equiparare differenza cambi
      if this.w_IMPPDA<>0
        this.oParentObject.w_PNTIPCON = this.w_TIPCLF
        this.oParentObject.w_PNCODCON = this.w_CODCLF
        this.oParentObject.w_PNFLPART = "N"
        this.oParentObject.w_PNCODPAG = SPACE(5)
        this.oParentObject.w_PNCAURIG = this.w_CAUNOP
        this.oParentObject.w_CAUDES = space(30)
        this.oParentObject.w_FLAGTOT = "+"
        this.w_APPO = this.w_IMPPDA
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Empty(this.w_CAUNOP)
          if this.w_TIPCLF="C"
            this.w_oMess.AddMsgPartNL("Attenzione, riga cliente di chiusura differenza cambi non corretta%0Causale contabile non gestita a partite, non specificata nei parametri differenze abbuoni")     
            this.w_oPart = this.w_oMess.AddMsgPartNL("� quindi necessario per salvare la registrazione, completare riga %1%0con causale di riga che non gestisce le partite")
          else
            this.w_oMess.AddMsgPartNL("Attenzione, riga fornitore di chiusura differenza cambi non corretta%0Causale contabile non gestita a partite, non specificata nei parametri differenze abbuoni")     
            this.w_oPart = this.w_oMess.AddMsgPartNL("� quindi necessario per salvare la registrazione, completare riga %1%0con causale di riga che non gestisce le partite")
          endif
          this.w_oPart.AddParam(ALLTRIM(STR(this.oParentObject.w_CPROWORD)))     
          * --- Questo assegnamento serve perch� successivamente w_MESS viene testato
          this.w_MESS = this.w_oMess.ComposeMessage(.F.)
          this.w_oMess.Ah_ErrorMsg()     
        endif
        this.w_IMPPDA = 0
        LOOP
      endif
      * --- Differenza Cambi Passiva
      if this.w_IMPDCP<>0
        this.oParentObject.w_PNTIPCON = "G"
        this.oParentObject.w_PNCODCON = this.w_CONDCP
        this.oParentObject.w_PNFLPART = "N"
        this.oParentObject.w_PNCODPAG = SPACE(5)
        this.oParentObject.w_FLAGTOT = "+"
        this.w_APPO = this.w_IMPDCP
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPDCP = 0
        LOOP
      endif
      * --- Riga Cliente\Fornitore per equiparare differenza cambi
      if this.w_IMPPDP<>0 And Empty(this.w_MESS)
        this.oParentObject.w_PNTIPCON = this.w_TIPCLF
        this.oParentObject.w_PNCODCON = this.w_CODCLF
        this.oParentObject.w_PNFLPART = "N"
        this.oParentObject.w_PNCODPAG = SPACE(5)
        this.oParentObject.w_PNCAURIG = this.w_CAUNOP
        this.oParentObject.w_CAUDES = space(30)
        this.oParentObject.w_FLAGTOT = "+"
        this.w_APPO = this.w_IMPPDP
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Empty(this.w_CAUNOP)
          if this.w_TIPCLF="C"
            this.w_oMess.AddMsgPartNL("Attenzione, riga cliente di chiusura differenza cambi non corretta%0Causale contabile non gestita a partite, non specificata nei parametri differenze abbuoni")     
            this.w_oPart = this.w_oMess.AddMsgPartNL("� quindi necessario per salvare la registrazione, completare riga %1%0con causale di riga che non gestisce le partite")
          else
            this.w_oMess.AddMsgPartNL("Attenzione, riga fornitore di chiusura differenza cambi non corretta%0Causale contabile non gestita a partite, non specificata nei parametri differenze abbuoni")     
            this.w_oPart = this.w_oMess.AddMsgPartNL("� quindi necessario per salvare la registrazione, completare riga %1%0con causale di riga che non gestisce le partite")
          endif
          this.w_oPart.AddParam(ALLTRIM(STR(this.oParentObject.w_CPROWORD)))     
          * --- Questo assegnamento serve perch� successivamente w_MESS viene testato
          this.w_MESS = this.w_oMess.ComposeMessage(.F.)
          this.w_oMess.Ah_ErrorMsg()     
        endif
        this.w_IMPPDP = 0
        LOOP
      endif
      * --- Scrive Riga Abbuoni Attivi o Passivi
      if this.w_IMPABB<>0
        * --- Riga Cliente/Fornitore
        this.oParentObject.w_PNTIPCON = this.w_TIPCON
        this.oParentObject.w_PNCODCON = this.w_CODCON
        this.oParentObject.w_PNFLPART = "S"
        this.oParentObject.w_PNCODPAG = this.w_PAGCON
        this.oParentObject.w_PNFLABAN = "AB"+this.w_ROWORD
        this.oParentObject.w_FLAGTOT = "+"
        * --- Inverte il Segno perche' va nella sezione Opposta
        this.w_APPO = -this.w_IMPABB
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPABB = 0
        LOOP
      endif
      if this.w_IMPABA<>0
        * --- Riga Abbuoni Attivi
        this.oParentObject.w_PNTIPCON = "G"
        this.oParentObject.w_PNCODCON = this.w_CONABA
        this.oParentObject.w_PNFLPART = "N"
        this.oParentObject.w_PNCODPAG = SPACE(5)
        this.oParentObject.w_PNFLABAN = "AT"+this.w_ROWORD
        this.oParentObject.w_FLAGTOT = "+"
        this.w_APPO = this.w_IMPABA
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPABA = 0
        LOOP
      endif
      if this.w_IMPABP<>0
        * --- Riga Abbuoni Passivi
        this.oParentObject.w_PNTIPCON = "G"
        this.oParentObject.w_PNCODCON = this.w_CONABP
        this.oParentObject.w_PNFLPART = "N"
        this.oParentObject.w_PNCODPAG = SPACE(5)
        this.oParentObject.w_PNFLABAN = "PA"+this.w_ROWORD
        this.oParentObject.w_FLAGTOT = "+"
        this.w_APPO = this.w_IMPABP
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPABP = 0
        LOOP
      endif
    enddo
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_APPO=0
      * --- Elimina la Riga...
      if NOT DELETED()
        DELETE
      endif
    else
      * --- Scrive Riga Temporaneo Primanota
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDESCRI,ANDESCR2,ANCCTAGG,ANPARTSN,ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDESCRI,ANDESCR2,ANCCTAGG,ANPARTSN,ANCONSUP;
          from (i_cTable) where;
              ANTIPCON = this.oParentObject.w_PNTIPCON;
              and ANCODICE = this.oParentObject.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DESCON = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
        this.oParentObject.w_DESCO2 = NVL(cp_ToDate(_read_.ANDESCR2),cp_NullValue(_read_.ANDESCR2))
        this.oParentObject.w_CCTAGG = NVL(cp_ToDate(_read_.ANCCTAGG),cp_NullValue(_read_.ANCCTAGG))
        this.oParentObject.w_PARTSN = NVL(cp_ToDate(_read_.ANPARTSN),cp_NullValue(_read_.ANPARTSN))
        this.oParentObject.w_MASTRO = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.oParentObject.w_PNCAURIG) AND this.oParentObject.w_PNCAURIG<>this.oParentObject.w_PNCODCAU
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCDESCRI"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCAURIG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCDESCRI;
            from (i_cTable) where;
                CCCODICE = this.oParentObject.w_PNCAURIG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_CAUDES = NVL(cp_ToDate(_read_.CCDESCRI),cp_NullValue(_read_.CCDESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Se riga Cliente\Fornitore relativa a Differenza Cambi a seguito di Acconti
      this.oParentObject.w_SEZB = IIF(this.oParentObject.w_PNTIPCON="G", CALCSEZ(this.oParentObject.w_MASTRO), " ")
      this.oParentObject.w_PNIMPDAR = IIF(this.w_APPO>0, this.w_APPO, 0)
      this.oParentObject.w_PNIMPAVE = IIF(this.w_APPO<0, ABS(this.w_APPO), 0)
      this.oParentObject.w_SLIMPDAR = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE>=0,this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE,0)
      this.oParentObject.w_SLIMPAVE = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE<0,ABS(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE),0)
      this.oParentObject.w_IMPDAR = this.oParentObject.w_PNIMPDAR
      this.oParentObject.w_IMPAVE = this.oParentObject.w_PNIMPAVE
      this.oParentObject.w_FLDAVE = IIF(this.oParentObject.w_PNIMPDAR<>0, "D", IIF(this.oParentObject.w_PNIMPAVE<>0, "A", " "))
      this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR + this.oParentObject.w_SLIMPDAR
      this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE +this.oParentObject.w_SLIMPAVE
      * --- Aggiorna Totalizzatore Automatismi
      this.w_TmpN2 = this.w_PADRE.o_PNIMPDAR - this.w_PADRE.o_PNIMPAVE
      this.oParentObject.w_TOTFLAG = AGGFLAG(this.oParentObject.w_FLAGTOT, this.w_TmpN2 , this.oParentObject.w_PNIMPDAR+this.oParentObject.w_PNIMPAVE, this.oParentObject.w_TOTFLAG)
      * --- Aggiorna gli <Old> per prevenire successivi Calcoli
      this.w_PADRE.o_PNIMPDAR = this.oParentObject.w_PNIMPDAR
      this.w_PADRE.o_PNIMPAVE = this.oParentObject.w_PNIMPAVE
      * --- Carica il Temporaneo Primanota
      this.w_PADRE.TrsFromWork()     
      * --- Flag Notifica Riga Variata
      if i_SRV<>"A" AND NOT DELETED()
        replace i_SRV with "U"
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='PAG_AMEN'
    this.cWorkTables[4]='CONTROPA'
    this.cWorkTables[5]='MOD_PAGA'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
