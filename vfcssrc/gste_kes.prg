* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kes                                                        *
*              Import/export archivi SEPA                                      *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_88]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-08-06                                                      *
* Last revis.: 2016-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kes",oParentObject))

* --- Class definition
define class tgste_kes as StdForm
  Top    = 11
  Left   = 14

  * --- Standard Properties
  Width  = 624
  Height = 291+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-11-18"
  HelpContextID=65680489
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  cPrg = "gste_kes"
  cComment = "Import/export archivi SEPA"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_RADSELIE1 = space(10)
  w_SELENT = space(10)
  w_DBF1 = space(200)
  w_DBF11 = space(200)
  w_SELCAR = space(10)
  w_DBF5 = space(200)
  w_SELSTR = space(10)
  w_DBF2 = space(200)
  w_SELVAP = space(10)
  w_SELVAR = space(10)
  w_DBF10 = space(200)
  w_DBF3 = space(200)
  w_SELFAS = space(10)
  w_DBF9 = space(200)
  w_SELFOR = space(10)
  w_DBF7 = space(200)
  w_DBF77 = space(200)
  w_SELELE = space(10)
  w_DBF8 = space(200)
  w_SELTRS = space(10)
  w_DBF4 = space(200)
  w_DBF44 = space(200)
  w_SELTRR = space(10)
  w_DBF6 = space(200)
  w_SOLSTR = space(1)
  w_RADSELEZ2 = space(10)
  w_MSG = space(0)
  w_FLVERBOS = .F.
  w_STRUTTURE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kesPag1","gste_kes",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Archivi")
      .Pages(2).addobject("oPag","tgste_kesPag2","gste_kes",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Strutture")
      .Pages(3).addobject("oPag","tgste_kesPag3","gste_kes",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Messaggi elaborazione")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRADSELIE1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_STRUTTURE = this.oPgFrm.Pages(2).oPag.STRUTTURE
    DoDefault()
    proc Destroy()
      this.w_STRUTTURE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RADSELIE1=space(10)
      .w_SELENT=space(10)
      .w_DBF1=space(200)
      .w_DBF11=space(200)
      .w_SELCAR=space(10)
      .w_DBF5=space(200)
      .w_SELSTR=space(10)
      .w_DBF2=space(200)
      .w_SELVAP=space(10)
      .w_SELVAR=space(10)
      .w_DBF10=space(200)
      .w_DBF3=space(200)
      .w_SELFAS=space(10)
      .w_DBF9=space(200)
      .w_SELFOR=space(10)
      .w_DBF7=space(200)
      .w_DBF77=space(200)
      .w_SELELE=space(10)
      .w_DBF8=space(200)
      .w_SELTRS=space(10)
      .w_DBF4=space(200)
      .w_DBF44=space(200)
      .w_SELTRR=space(10)
      .w_DBF6=space(200)
      .w_SOLSTR=space(1)
      .w_RADSELEZ2=space(10)
      .w_MSG=space(0)
      .w_FLVERBOS=.f.
        .w_RADSELIE1 = ' '
        .w_SELENT = 'EN'
        .w_DBF1 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"ENT_MAST.DBF"
        .w_DBF11 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"ENT_DETT.DBF"
        .w_SELCAR = iif(.w_RADSELIE1='I','CS','  ')
        .w_DBF5 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"CAR_SPEC.DBF"
        .w_SELSTR = 'ST'
        .w_DBF2 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"VASTRUTT.DBF"
        .w_SELVAP = '  '
        .w_SELVAR = '  '
        .w_DBF10 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"VAR_FILE.DBF"
        .w_DBF3 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"VAPREDEF.DBF"
        .w_SELFAS = '  '
        .w_DBF9 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"PAT_FILE.DBF"
        .w_SELFOR = 'FO'
        .w_DBF7 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"VAFORMAT.DBF"
        .w_DBF77 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"VADETTFO.DBF"
        .w_SELELE = 'EL'
        .w_DBF8 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"VAELEMEN.DBF"
        .w_SELTRS = '  '
        .w_DBF4 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"TRS_MAST.DBF"
        .w_DBF44 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"TRS_DETT.DBF"
        .w_SELTRR = '  '
        .w_DBF6 = "Files_x_Import\SEPA\"+IIF( ISLESP(),"LESP\",'')+"VATRASCO.DBF"
        .w_SOLSTR = ' '
      .oPgFrm.Page2.oPag.STRUTTURE.Calculate()
        .w_RADSELEZ2 = 'S'
          .DoRTCalc(27,27,.f.)
        .w_FLVERBOS = .F.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
            .w_SELCAR = iif(.w_RADSELIE1='I','CS','  ')
        .oPgFrm.Page2.oPag.STRUTTURE.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.STRUTTURE.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_1.enabled = this.oPgFrm.Page2.oPag.oBtn_2_1.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.STRUTTURE.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gste_kes
    If cevent='Blank' or cevent='w_RADSELEZ2 Changed'
       UPDATE (This.w_STRUTTURE.cCursor) SET XCHK=IIF(This.w_RADSELEZ2='S', 1 , 0)
    Endif
    
    If cevent='Blank' or cevent='w_RADSELIE1 Changed'
       UPDATE (This.w_STRUTTURE.cCursor) SET XCHK=IIF(This.w_RADSELIE1='E', 1 , 0)
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRADSELIE1_1_1.RadioValue()==this.w_RADSELIE1)
      this.oPgFrm.Page1.oPag.oRADSELIE1_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELENT_1_2.RadioValue()==this.w_SELENT)
      this.oPgFrm.Page1.oPag.oSELENT_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF1_1_3.value==this.w_DBF1)
      this.oPgFrm.Page1.oPag.oDBF1_1_3.value=this.w_DBF1
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF11_1_5.value==this.w_DBF11)
      this.oPgFrm.Page1.oPag.oDBF11_1_5.value=this.w_DBF11
    endif
    if not(this.oPgFrm.Page1.oPag.oSELSTR_1_9.RadioValue()==this.w_SELSTR)
      this.oPgFrm.Page1.oPag.oSELSTR_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF2_1_10.value==this.w_DBF2)
      this.oPgFrm.Page1.oPag.oDBF2_1_10.value=this.w_DBF2
    endif
    if not(this.oPgFrm.Page1.oPag.oSELFOR_1_17.RadioValue()==this.w_SELFOR)
      this.oPgFrm.Page1.oPag.oSELFOR_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF7_1_18.value==this.w_DBF7)
      this.oPgFrm.Page1.oPag.oDBF7_1_18.value=this.w_DBF7
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF77_1_20.value==this.w_DBF77)
      this.oPgFrm.Page1.oPag.oDBF77_1_20.value=this.w_DBF77
    endif
    if not(this.oPgFrm.Page1.oPag.oSELELE_1_22.RadioValue()==this.w_SELELE)
      this.oPgFrm.Page1.oPag.oSELELE_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF8_1_23.value==this.w_DBF8)
      this.oPgFrm.Page1.oPag.oDBF8_1_23.value=this.w_DBF8
    endif
    if not(this.oPgFrm.Page2.oPag.oRADSELEZ2_2_4.RadioValue()==this.w_RADSELEZ2)
      this.oPgFrm.Page2.oPag.oRADSELEZ2_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMSG_3_1.value==this.w_MSG)
      this.oPgFrm.Page3.oPag.oMSG_3_1.value=this.w_MSG
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVERBOS_3_2.RadioValue()==this.w_FLVERBOS)
      this.oPgFrm.Page3.oPag.oFLVERBOS_3_2.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DBF1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF1_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DBF1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF11))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF11_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DBF11)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF5_1_8.SetFocus()
            i_bnoObbl = !empty(.w_DBF5)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF2_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DBF2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF10))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF10_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DBF10)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF3_1_14.SetFocus()
            i_bnoObbl = !empty(.w_DBF3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF9))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF9_1_16.SetFocus()
            i_bnoObbl = !empty(.w_DBF9)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF7))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF7_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DBF7)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF77))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF77_1_20.SetFocus()
            i_bnoObbl = !empty(.w_DBF77)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF8))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF8_1_23.SetFocus()
            i_bnoObbl = !empty(.w_DBF8)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF4_1_26.SetFocus()
            i_bnoObbl = !empty(.w_DBF4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF44))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF44_1_27.SetFocus()
            i_bnoObbl = !empty(.w_DBF44)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF6_1_29.SetFocus()
            i_bnoObbl = !empty(.w_DBF6)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgste_kesPag1 as StdContainer
  Width  = 620
  height = 471
  stdWidth  = 620
  stdheight = 471
  resizeXpos=371
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRADSELIE1_1_1 as StdRadio with uid="OKXXQIMNCR",rtseq=1,rtrep=.f.,left=147, top=19, width=415,height=17;
    , ToolTipText = "Seleziona import/export";
    , cFormVar="w_RADSELIE1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADSELIE1_1_1.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Importazione da file DBF"
      this.Buttons(1).HelpContextID = 213734763
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Importazione da file DBF","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Esportazione su file DBF"
      this.Buttons(2).HelpContextID = 213734763
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Esportazione su file DBF","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",17)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona import/export")
      StdRadio::init()
    endproc

  func oRADSELIE1_1_1.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    space(10))))
  endfunc
  func oRADSELIE1_1_1.GetRadio()
    this.Parent.oContained.w_RADSELIE1 = this.RadioValue()
    return .t.
  endfunc

  func oRADSELIE1_1_1.SetRadio()
    this.Parent.oContained.w_RADSELIE1=trim(this.Parent.oContained.w_RADSELIE1)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELIE1=='I',1,;
      iif(this.Parent.oContained.w_RADSELIE1=='E',2,;
      0))
  endfunc

  add object oSELENT_1_2 as StdCheck with uid="AOLULFBESB",rtseq=2,rtrep=.f.,left=18, top=78, caption="Entit�",;
    ToolTipText = "Se attivato importa gli archivi delle entit�",;
    HelpContextID = 180365786,;
    cFormVar="w_SELENT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELENT_1_2.RadioValue()
    return(iif(this.value =1,'EN',;
    ' '))
  endfunc
  func oSELENT_1_2.GetRadio()
    this.Parent.oContained.w_SELENT = this.RadioValue()
    return .t.
  endfunc

  func oSELENT_1_2.SetRadio()
    this.Parent.oContained.w_SELENT=trim(this.Parent.oContained.w_SELENT)
    this.value = ;
      iif(this.Parent.oContained.w_SELENT=='EN',1,;
      0)
  endfunc

  add object oDBF1_1_3 as StdField with uid="ZPJIIFTPAA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DBF1", cQueryName = "DBF1",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 62164426,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=79, InputMask=replicate('X',200)


  add object oBtn_1_4 as StdButton with uid="MKUMAQUBOB",left=584, top=82, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65479466;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .w_DBF1=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDBF11_1_5 as StdField with uid="CZIBBZGIKR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DBF11", cQueryName = "DBF11",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 10784202,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=101, InputMask=replicate('X',200)


  add object oBtn_1_6 as StdButton with uid="GJSGDJJLXS",left=584, top=104, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65479466;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      with this.Parent.oContained
        .w_DBF11=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELSTR_1_9 as StdCheck with uid="XRFZASRLRC",rtseq=7,rtrep=.f.,left=18, top=129, caption="Strutture",;
    ToolTipText = "Se attivato importa le strutture",;
    HelpContextID = 206711258,;
    cFormVar="w_SELSTR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELSTR_1_9.RadioValue()
    return(iif(this.value =1,'ST',;
    ' '))
  endfunc
  func oSELSTR_1_9.GetRadio()
    this.Parent.oContained.w_SELSTR = this.RadioValue()
    return .t.
  endfunc

  func oSELSTR_1_9.SetRadio()
    this.Parent.oContained.w_SELSTR=trim(this.Parent.oContained.w_SELSTR)
    this.value = ;
      iif(this.Parent.oContained.w_SELSTR=='ST',1,;
      0)
  endfunc

  add object oDBF2_1_10 as StdField with uid="RLYZERYPVU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DBF2", cQueryName = "DBF2",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 62098890,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=130, InputMask=replicate('X',200)

  add object oSELFOR_1_17 as StdCheck with uid="ZJASOWTRBW",rtseq=15,rtrep=.f.,left=18, top=158, caption="Formati",;
    ToolTipText = "Se attivato importa gli archivi dei formati",;
    HelpContextID = 212806106,;
    cFormVar="w_SELFOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELFOR_1_17.RadioValue()
    return(iif(this.value =1,'FO',;
    ' '))
  endfunc
  func oSELFOR_1_17.GetRadio()
    this.Parent.oContained.w_SELFOR = this.RadioValue()
    return .t.
  endfunc

  func oSELFOR_1_17.SetRadio()
    this.Parent.oContained.w_SELFOR=trim(this.Parent.oContained.w_SELFOR)
    this.value = ;
      iif(this.Parent.oContained.w_SELFOR=='FO',1,;
      0)
  endfunc

  add object oDBF7_1_18 as StdField with uid="SLHGDWQMAO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DBF7", cQueryName = "DBF7",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 61771210,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=158, InputMask=replicate('X',200)


  add object oBtn_1_19 as StdButton with uid="IIWNPBRVGH",left=584, top=161, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65479466;
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        .w_DBF7=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDBF77_1_20 as StdField with uid="LGTPUQKUMO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DBF77", cQueryName = "DBF77",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 4099530,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=181, InputMask=replicate('X',200)


  add object oBtn_1_21 as StdButton with uid="SJCYGOUIXJ",left=584, top=184, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65479466;
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      with this.Parent.oContained
        .w_DBF77=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELELE_1_22 as StdCheck with uid="NAQAYGDNDI",rtseq=18,rtrep=.f.,left=18, top=204, caption="Elementi",;
    ToolTipText = "Se attivato importa gli archivio degli elementi",;
    HelpContextID = 165685722,;
    cFormVar="w_SELELE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELELE_1_22.RadioValue()
    return(iif(this.value =1,'EL',;
    ' '))
  endfunc
  func oSELELE_1_22.GetRadio()
    this.Parent.oContained.w_SELELE = this.RadioValue()
    return .t.
  endfunc

  func oSELELE_1_22.SetRadio()
    this.Parent.oContained.w_SELELE=trim(this.Parent.oContained.w_SELELE)
    this.value = ;
      iif(this.Parent.oContained.w_SELELE=='EL',1,;
      0)
  endfunc

  add object oDBF8_1_23 as StdField with uid="COMWHFPKRC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DBF8", cQueryName = "DBF8",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 61705674,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=205, InputMask=replicate('X',200)


  add object oBtn_1_24 as StdButton with uid="EYMXADSUQA",left=584, top=208, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65479466;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        .w_DBF8=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_31 as StdButton with uid="CZRHGAVNYF",left=513, top=249, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 65352362;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        GSAR1BED(this.Parent.oContained,"ELABO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_RADSELIE1))
      endwith
    endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="NYQDDFCUPJ",left=565, top=249, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 2429001;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_36 as StdButton with uid="OAPUGTGTUT",left=584, top=133, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65479466;
  , bGlobalFont=.t.

    proc oBtn_1_36.Click()
      with this.Parent.oContained
        .w_DBF2=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_33 as StdString with uid="HKXMTIVULE",Visible=.t., Left=19, Top=38,;
    Alignment=0, Width=541, Height=15,;
    Caption="Selezione archivi"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="OJVLVUPTEC",Visible=.t., Left=216, Top=62,;
    Alignment=0, Width=34, Height=15,;
    Caption="PATH"  ;
  , bGlobalFont=.t.

  add object oBox_1_34 as StdBox with uid="RAIJTTRZDL",left=11, top=57, width=599,height=180
enddefine
define class tgste_kesPag2 as StdContainer
  Width  = 620
  height = 471
  stdWidth  = 620
  stdheight = 471
  resizeXpos=296
  resizeYpos=191
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_2_1 as StdButton with uid="FGSNTZPQMK",left=481, top=230, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 65352362;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_1.Click()
      with this.Parent.oContained
        GSAR1BED(this.Parent.oContained,"ELABO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_RADSELIE1))
      endwith
    endif
  endfunc


  add object oBtn_2_2 as StdButton with uid="UPNOJBIOOS",left=533, top=230, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Esci";
    , HelpContextID = 2429001;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STRUTTURE as cp_szoombox with uid="OCISBOFOQX",left=16, top=20, width=579,height=191,;
    caption='STRUTTURE',;
   bGlobalFont=.t.,;
    cZoomFile="GSTE_KES",cTable="VASTRUTT",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Blank",;
    nPag=2;
    , HelpContextID = 95439304

  add object oRADSELEZ2_2_4 as StdRadio with uid="NMFJOCMXDJ",rtseq=26,rtrep=.f.,left=17, top=230, width=169,height=32;
    , ToolTipText = "Seleziona/deseleziona tutte le strutture";
    , cFormVar="w_RADSELEZ2", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oRADSELEZ2_2_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 213734800
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 213734800
      this.Buttons(2).Top=15
      this.SetAll("Width",167)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutte le strutture")
      StdRadio::init()
    endproc

  func oRADSELEZ2_2_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(10))))
  endfunc
  func oRADSELEZ2_2_4.GetRadio()
    this.Parent.oContained.w_RADSELEZ2 = this.RadioValue()
    return .t.
  endfunc

  func oRADSELEZ2_2_4.SetRadio()
    this.Parent.oContained.w_RADSELEZ2=trim(this.Parent.oContained.w_RADSELEZ2)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELEZ2=='S',1,;
      iif(this.Parent.oContained.w_RADSELEZ2=='D',2,;
      0))
  endfunc
enddefine
define class tgste_kesPag3 as StdContainer
  Width  = 620
  height = 471
  stdWidth  = 620
  stdheight = 471
  resizeXpos=414
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSG_3_1 as StdMemo with uid="QVQLZYFOGE",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 3, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 65367098,;
   bGlobalFont=.t.,;
    Height=441, Width=612, Left=4, Top=6, Readonly=.T.

  add object oFLVERBOS_3_2 as StdCheck with uid="GJREOHVBHZ",rtseq=28,rtrep=.f.,left=6, top=448, caption="Produci log dettagliato",;
    HelpContextID = 209683287,;
    cFormVar="w_FLVERBOS", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVERBOS_3_2.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVERBOS_3_2.GetRadio()
    this.Parent.oContained.w_FLVERBOS = this.RadioValue()
    return .t.
  endfunc

  func oFLVERBOS_3_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVERBOS==.T.,1,;
      0)
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kes','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
