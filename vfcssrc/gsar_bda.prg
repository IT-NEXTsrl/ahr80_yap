* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bda                                                        *
*              Check disponibilita magazzino                                   *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_61]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-02                                                      *
* Last revis.: 2014-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNoTransaction,w_PADRE,pMandatesi
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bda",oParentObject,m.pNoTransaction,m.w_PADRE,m.pMandatesi)
return(i_retval)

define class tgsar_bda as StdBatch
  * --- Local variables
  pNoTransaction = space(1)
  w_PADRE = .NULL.
  pMandatesi = space(1)
  w_cFIELDS = space(254)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_NUMRIF = 0
  w_FLRISE = space(1)
  w_FLIMPE = space(1)
  w_FLORDI = space(1)
  w_F2RISE = space(1)
  w_F2IMPE = space(1)
  w_F2ORDI = space(1)
  w_CODMAT = space(5)
  w_DELTA = 0
  w_DELTAC = 0
  w_ROW = 0
  w_CODART = space(20)
  w_CODMAG = space(5)
  w_DISP = 0
  w_DISPC = 0
  w_DISPR = 0
  w_QTAPER = 0
  w_QTRPER = 0
  w_QTOPER = 0
  w_QTIPER = 0
  w_NROW = space(5)
  w_FLDISP = space(1)
  w_FLDISC = space(1)
  w_MOV_DISP = space(1)
  w_MOV_CONT = space(1)
  w_MOV_RISE = space(1)
  w_ANNULLA = .f.
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_DAIM = .f.
  w_OK = .f.
  w_OLD_STOPFK = .f.
  w_TERR = 0
  w_QTAMSG = 0
  w_QTAMSG2 = 0
  w_QTAMSG3 = 0
  w_LMESS = space(200)
  * --- WorkFile variables
  ART_ICOL_idx=0
  SALDIART_idx=0
  DOC_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Check Disponibilit� articoli
    *     =================================
    *     
    *     Recupero dal transitorio i dati relativi alla gestione saldi di magazzino da stornare...
    *     Per queste operazioni considero tutte le righe modificate, quindi anche quelle
    *     cancellate...
    * --- Se passato a 'S' controlli fuori transazione (Msg con conferma per tutti gli
    *     articoli sotto diposnibilit� pressenti) altrimenti
    *     controlli solo per articoli con blocco disponibilita' senza conferma, messaggio 
    *     proposto solo per il primo (Transazione abbandonata)
    * --- Gestione che invoca il metodo (gestione che ha un transitorio...)
    *     Nel caso di esplosione distinta, viene creato nel batch GSAR_BEA direttamente il cursore Trs_Add
    * --- Se uguale a 'S' mantiene nelle var. w_RESOCON1 e w_MESBLOK i messaggi di log del batch chiamante
    * --- Determino quanto devo stornare dai saldi di magazzino e quanto vado a scrivere sui saldi di magazzino...
    do case
      case Upper ( this.w_PADRE.Class )= "TGSOR_MDV" Or Upper ( this.w_PADRE.Class )= "TGSVE_MDV" or Upper ( this.w_PADRE.Class )= "TGSAC_MDV" 
        * --- Recupero totale diviso per articolo magazzino dei movimenti attuali sul magazzino principale
        this.w_cFIELDS = "Min(t_CpRoword) As Riga ,Sum(MVQTAUM1*IIF( MVFLCASC='-',1, IIF( MVFLCASC='+',-1, 0)) - MVQTASAL*IIF( MVFLRISE='-',1, IIF( MVFLRISE='+',-1, 0))) As Disp ,"
        this.w_cFIELDS = this.w_cFIELDS + "Sum(MVQTAUM1*IIF( MVFLCASC='-',1, IIF( MVFLCASC='+',-1, 0)) - MVQTASAL*IIF( MVFLRISE='-',1, IIF( MVFLRISE='+',-1, 0)) + MVQTASAL*IIF( MVFLORDI='-',1, IIF( MVFLORDI='+',-1, 0)) - MVQTASAL*IIF( MVFLIMPE='-',1, IIF( MVFLIMPE='+',-1, 0))) As DispC , "
        this.w_cFIELDS = this.w_cFIELDS + "Sum(MVQTASAL*IIF( MVFLRISE='-',1, IIF( MVFLRISE='+',-1, 0))) As DispR , "
        this.w_cFIELDS = this.w_cFIELDS + "MVCODMAG As CodMag, MVKEYSAL As KEYSAL,Max(Left(Alltrim(MVFLCASC+MVFLRISE),1)) As Mov_Disp, Max(Left(Alltrim(MVFLCASC+MVFLRISE+MVFLORDI+MVFLIMPE),1)) As Mov_Cont,Max(MVFLRISE) As Mov_Rise "
        this.w_PADRE.Exec_Select("Tmp_1",this.w_cFields,"iif( "+cp_ToStrODBC(this.w_PADRE.cFunction)+"<>'Query', Not(Deleted() And i_SRV='A'), Not Deleted()) And not Empty(Nvl(MVKEYSAL ,'')) And ( MVFLCASC In ('+','-') Or MVFLRISE In ('+','-') Or MVFLORDI In ('+','-') Or MVFLIMPE In ('+','-')  ) And t_MVTIPRIG='R'","","KEYSAL,CODMAG","")     
        * --- Recupero totale diviso per articolo magazzino dei movimenti attuali sul magazzino collegato
        this.w_cFIELDS = "Min(t_CpRoword) As Riga ,Sum(MVQTAUM1*IIF( MVF2CASC='-',1, IIF( MVF2CASC='+',-1, 0)) - MVQTASAL*IIF( MVF2RISE='-',1, IIF( MVF2RISE='+',-1, 0))) As Disp ,"
        this.w_cFIELDS = this.w_cFIELDS + "Sum(MVQTAUM1*IIF( MVF2CASC='-',1, IIF( MVF2CASC='+',-1, 0)) - MVQTASAL*IIF( MVF2RISE='-',1, IIF( MVF2RISE='+',-1, 0)) + MVQTASAL*IIF( MVF2ORDI='-',1, IIF( MVF2ORDI='+',-1, 0)) - MVQTASAL*IIF( MVF2IMPE='-',1, IIF( MVF2IMPE='+',-1, 0))) As DispC , "
        this.w_cFIELDS = this.w_cFIELDS + "Sum(MVQTASAL*IIF( MVF2RISE='-',1, IIF( MVF2RISE='+',-1, 0))) As DispR , "
        this.w_cFIELDS = this.w_cFIELDS + "MVCODMAT As CodMag, MVKEYSAL As KEYSAL,Max(Left(Alltrim(MVF2CASC+MVF2RISE),1)) As Mov_Disp, Max(Left(Alltrim(MVF2CASC+MVF2RISE+MVF2ORDI+MVF2IMPE),1)) As Mov_Cont,Max(MVF2RISE) As Mov_Rise "
        this.w_PADRE.Exec_Select("Tmp_2",this.w_cFields,"iif( "+cp_ToStrODBC(this.w_PADRE.cFunction)+"<>'Query', Not(Deleted() And i_SRV='A'), Not Deleted()) And not Empty(Nvl(MVKEYSAL ,'')) And ( MVF2CASC In ('+','-') Or MVF2RISE In ('+','-') Or MVF2ORDI In ('+','-') Or MVF2IMPE In ('+','-')  ) And t_MVTIPRIG='R'","","KEYSAL,CODMAG","")     
        * --- Recupero totale diviso per articolo magazzino dei movimenti originali sul magazzino principale
        this.w_cFIELDS = "Min(t_CpRoword) As Riga ,Sum(t_MVQTAUM1*IIF( t_MVFLCASC='-',-1, IIF( t_MVFLCASC='+',1, 0)) - t_MVQTASAL*IIF( t_MVFLRISE='-',-1, IIF( t_MVFLRISE='+',1, 0))) As Disp ,"
        this.w_cFIELDS = this.w_cFIELDS + "Sum(t_MVQTAUM1*IIF( t_MVFLCASC='-',-1, IIF( t_MVFLCASC='+',1, 0)) - t_MVQTASAL*IIF( t_MVFLRISE='-',-1, IIF( t_MVFLRISE='+',1, 0)) +"
        this.w_cFIELDS = this.w_cFIELDS + "t_MVQTASAL*IIF( t_MVFLORDI='-',-1, IIF( t_MVFLORDI='+',1, 0)) - t_MVQTASAL*IIF( t_MVFLIMPE='-',-1, IIF( t_MVFLIMPE='+',1, 0))) As DispC ,"
        this.w_cFIELDS = this.w_cFIELDS + "Sum(t_MVQTASAL*IIF( t_MVFLRISE='-',-1, IIF( t_MVFLRISE='+',1, 0))) As DispR , "
        this.w_cFIELDS = this.w_cFIELDS + "t_MVCODMAG As CodMag, t_MVKEYSAL As KEYSAL,Max(Left(Alltrim(t_MVFLCASC+t_MVFLRISE),1)) As Mov_Disp, Max(Left(Alltrim(t_MVFLCASC+t_MVFLRISE +t_MVFLORDI+t_MVFLIMPE),1)) As Mov_Cont,Max(t_MVFLRISE) As Mov_Rise"
        this.w_PADRE.Exec_Select("Tmp_3", this.w_cFields , Cp_ToStrODBC(this.w_PADRE.cFunction)+"<>'Query' And Not Deleted() And not Empty(Nvl(t_MVKEYSAL ,'')) And ( t_MVFLCASC In ('+','-') Or t_MVFLRISE In ('+','-') Or t_MVFLORDI In ('+','-') Or t_MVFLIMPE In ('+','-')  ) And t_MVTIPRIG='R' " ,"","KEYSAL,CODMAG","")     
        * --- Recupero totale diviso per articolo magazzino dei movimenti originali sul magazzino collegato
        this.w_cFIELDS = "Min(t_CpRoword) As Riga ,Sum(t_MVQTAUM1*IIF( t_MVF2CASC='-',-1, IIF( t_MVF2CASC='+',1, 0)) - t_MVQTASAL*IIF( t_MVF2RISE='-',-1, IIF( t_MVF2RISE='+',1, 0))) As Disp , "
        this.w_cFIELDS = this.w_cFIELDS + "Sum(t_MVQTAUM1*IIF( t_MVF2CASC='-',-1, IIF( t_MVF2CASC='+',1, 0)) - t_MVQTASAL*IIF( t_MVF2RISE='-',-1, IIF( t_MVF2RISE='+',1, 0)) +"
        this.w_cFIELDS = this.w_cFIELDS + "t_MVQTASAL*IIF( t_MVF2ORDI='-',-1, IIF( t_MVF2ORDI='+',1, 0)) - t_MVQTASAL*IIF( t_MVF2IMPE='-',-1, IIF( t_MVF2IMPE='+',1, 0))) As DispC , "
        this.w_cFIELDS = this.w_cFIELDS + "Sum(t_MVQTASAL*IIF( t_MVF2RISE='-',-1, IIF( t_MVF2RISE='+',1, 0))) As DispR , "
        this.w_cFIELDS = this.w_cFIELDS + "t_MVCODMAT As CodMag, t_MVKEYSAL As KEYSAL,Max(Left(Alltrim(t_MVF2CASC+t_MVF2RISE),1)) As Mov_Disp, Max(Left(Alltrim(MVF2CASC+MVF2RISE+t_MVF2ORDI+t_MVF2IMPE),1)) As Mov_Cont,Max(t_MVF2RISE) As Mov_Rise "
        this.w_PADRE.Exec_Select("Tmp_4", this.w_cFields , Cp_ToStrODBC(this.w_PADRE.cFunction)+"<>'Query' And Not Deleted() And not Empty(Nvl(t_MVKEYSAL ,'')) And ( t_MVF2CASC In ('+','-') Or t_MVF2RISE In ('+','-') Or t_MVF2ORDI In ('+','-') Or t_MVF2IMPE In ('+','-')  ) And t_MVTIPRIG='R'  " ,"","KEYSAL,CODMAG","")     
        * --- Metto assieme i quattro cursori..
         
 Select * From Tmp_1 Union All; 
 Select * From Tmp_2 Union All; 
 Select * From Tmp_3 Union All; 
 Select * From Tmp_4 Into Cursor Trs_Add NoFilter
        * --- Rimuovo i cursori..
         
 Use In Tmp_1 
 Use In Tmp_2 
 Use In Tmp_3 
 Use In Tmp_4
        * --- Gestisco eventuali evasioni di riservato / ordinato / impegnato.
        *     Raggruppo le righe per i loro riferimenti. (Se reale evasione)
        this.w_PADRE.Exec_Select("Trs_Eva", "t_MVSERRIF As SerRif, t_MVROWRIF As RowRif , t_MVNUMRIF As NumRif, Sum(IIf( "+Cp_ToStrODBC(this.w_PADRE.cFunction)+"='Query', 0 , t_MVQTAIM1 ) - MVQTAIM1) As Delta, Min(t_CpRowOrd) As Riga " , "Nvl( t_MVROWRIF ,0  )<>0 And t_MVFLARIF In ('-','+') And t_MVTIPRIG='R' And Not(Deleted() And i_SRV='A'  )" ,"","t_MVSERRIF, t_MVROWRIF, t_MVNUMRIF ","Delta<>0 ")     
      case Upper ( this.w_PADRE.Class )= "TGSMA_MVM" 
        * --- Recupero totale diviso per articolo magazzino dei movimenti attuali sul magazzino principale
        this.w_cFIELDS = "Min(t_CpRoword) As Riga ,Sum(MMQTAUM1*IIF( MMFLCASC='-',1, IIF( MMFLCASC='+',-1, 0)) - MMQTAUM1*IIF( MMFLRISE='-',1, IIF( MMFLRISE='+',-1, 0))) As Disp ,"
        this.w_cFIELDS = this.w_cFIELDS + "Sum(MMQTAUM1*IIF( MMFLCASC='-',1, IIF( MMFLCASC='+',-1, 0)) - MMQTAUM1*IIF( MMFLRISE='-',1, IIF( MMFLRISE='+',-1, 0)) + MMQTAUM1*IIF( MMFLORDI='-',1, IIF( MMFLORDI='+',-1, 0)) - MMQTAUM1*IIF( MMFLIMPE='-',1, IIF( MMFLIMPE='+',-1, 0))) As DispC , "
        this.w_cFIELDS = this.w_cFIELDS + "Sum(MMQTAUM1*IIF( MMFLRISE='-',1, IIF( MMFLRISE='+',-1, 0))) As DispR , "
        this.w_cFIELDS = this.w_cFIELDS + "MMCODMAG As CodMag, MMKEYSAL As KEYSAL,Max(Left(Alltrim(MMFLCASC+MMFLRISE),1)) As Mov_Disp, Max(Left(Alltrim(MMFLORDI+MMFLIMPE),1)) As Mov_Cont,Max(MMFLRISE) As Mov_Rise "
        this.w_PADRE.Exec_Select("Tmp_1",this.w_cFields,"iif( "+cp_ToStrODBC(this.w_PADRE.cFunction)+"<>'Query', Not(Deleted() And i_SRV='A'), Not Deleted()) And not Empty(Nvl(MMKEYSAL ,'')) And ( MMFLCASC In ('+','-') Or MMFLRISE In ('+','-') Or MMFLORDI In ('+','-') Or MMFLIMPE In ('+','-')  )","","KEYSAL,CODMAG","")     
        * --- Recupero totale diviso per articolo magazzino dei movimenti attuali sul magazzino collegato
        this.w_cFIELDS = "Min(t_CpRoword) As Riga ,Sum(MMQTAUM1*IIF( MMF2CASC='-',1, IIF( MMF2CASC='+',-1, 0)) - MMQTAUM1*IIF( MMF2RISE='-',1, IIF( MMF2RISE='+',-1, 0))) As Disp ,"
        this.w_cFIELDS = this.w_cFIELDS + "Sum(MMQTAUM1*IIF( MMF2CASC='-',1, IIF( MMF2CASC='+',-1, 0)) - MMQTAUM1*IIF( MMF2RISE='-',1, IIF( MMF2RISE='+',-1, 0)) + MMQTAUM1*IIF( MMF2ORDI='-',1, IIF( MMF2ORDI='+',-1, 0)) - MMQTAUM1*IIF( MMF2IMPE='-',1, IIF( MMF2IMPE='+',-1, 0))) As DispC , "
        this.w_cFIELDS = this.w_cFIELDS + "Sum(MMQTAUM1*IIF( MMF2RISE='-',1, IIF( MMF2RISE='+',-1, 0))) As DispR , "
        this.w_cFIELDS = this.w_cFIELDS + "MMCODMAT As CodMag, MMKEYSAL As KEYSAL,Max(Left(Alltrim(MMF2CASC+MMF2RISE),1)) As Mov_Disp, Max(Left(Alltrim(MMF2ORDI+MMF2IMPE),1)) As Mov_Cont,Max(MMF2RISE) As Mov_Rise "
        this.w_PADRE.Exec_Select("Tmp_2",this.w_cFields,"iif( "+cp_ToStrODBC(this.w_PADRE.cFunction)+"<>'Query', Not(Deleted() And i_SRV='A'), Not Deleted()) And not Empty(Nvl(MMKEYSAL ,'')) And ( MMF2CASC In ('+','-') Or MMF2RISE In ('+','-') Or MMF2ORDI In ('+','-') Or MMF2IMPE In ('+','-')  )","","KEYSAL,CODMAG","")     
        * --- Recupero totale diviso per articolo magazzino dei movimenti originali sul magazzino principale
        this.w_cFIELDS = "Min(t_CpRoword) As Riga ,Sum(t_MMQTAUM1*IIF( t_MMFLCASC='-',-1, IIF( t_MMFLCASC='+',1, 0)) - t_MMQTAUM1*IIF( t_MMFLRISE='-',-1, IIF( t_MMFLRISE='+',1, 0))) As Disp ,"
        this.w_cFIELDS = this.w_cFIELDS + "Sum(t_MMQTAUM1*IIF( t_MMFLCASC='-',-1, IIF( t_MMFLCASC='+',1, 0)) - t_MMQTAUM1*IIF( t_MMFLRISE='-',-1, IIF( t_MMFLRISE='+',1, 0)) +"
        this.w_cFIELDS = this.w_cFIELDS + "t_MMQTAUM1*IIF( t_MMFLORDI='-',-1, IIF( t_MMFLORDI='+',1, 0)) - t_MMQTAUM1*IIF( t_MMFLIMPE='-',-1, IIF( t_MMFLIMPE='+',1, 0))) As DispC ,"
        this.w_cFIELDS = this.w_cFIELDS + "Sum(t_MMQTAUM1*IIF( t_MMFLRISE='-',-1, IIF( t_MMFLRISE='+',1, 0))) As DispR , "
        this.w_cFIELDS = this.w_cFIELDS + "t_MMCODMAG As CodMag, t_MMKEYSAL As KEYSAL,Max(Left(Alltrim(t_MMFLCASC+t_MMFLRISE),1)) As Mov_Disp, Max(Left(Alltrim(t_MMFLORDI+t_MMFLIMPE),1)) As Mov_Cont,Max(t_MMFLRISE) As Mov_Rise"
        this.w_PADRE.Exec_Select("Tmp_3", this.w_cFields , Cp_ToStrODBC(this.w_PADRE.cFunction)+"<>'Query' And Not Deleted() And not Empty(Nvl(t_MMKEYSAL ,'')) And ( t_MMFLCASC In ('+','-') Or t_MMFLRISE In ('+','-') Or t_MMFLORDI In ('+','-') Or t_MMFLIMPE In ('+','-')  )" ,"","KEYSAL,CODMAG","")     
        * --- Recupero totale diviso per articolo magazzino dei movimenti originali sul magazzino collegato
        this.w_cFIELDS = "Min(t_CpRoword) As Riga ,Sum(t_MMQTAUM1*IIF( t_MMF2CASC='-',-1, IIF( t_MMF2CASC='+',1, 0)) - t_MMQTAUM1*IIF( t_MMF2RISE='-',-1, IIF( t_MMF2RISE='+',1, 0))) As Disp , "
        this.w_cFIELDS = this.w_cFIELDS + "Sum(t_MMQTAUM1*IIF( t_MMF2CASC='-',-1, IIF( t_MMF2CASC='+',1, 0)) - t_MMQTAUM1*IIF( t_MMF2RISE='-',-1, IIF( t_MMF2RISE='+',1, 0)) +"
        this.w_cFIELDS = this.w_cFIELDS + "t_MMQTAUM1*IIF( t_MMF2ORDI='-',-1, IIF( t_MMF2ORDI='+',1, 0)) - t_MMQTAUM1*IIF( t_MMF2IMPE='-',-1, IIF( t_MMF2IMPE='+',1, 0))) As DispC , "
        this.w_cFIELDS = this.w_cFIELDS + "Sum(t_MMQTAUM1*IIF( t_MMF2RISE='-',-1, IIF( t_MMF2RISE='+',1, 0))) As DispR , "
        this.w_cFIELDS = this.w_cFIELDS + "t_MMCODMAT As CodMag, t_MMKEYSAL As KEYSAL,Max(Left(Alltrim(t_MMF2CASC+t_MMF2RISE),1)) As Mov_Disp, Max(Left(Alltrim(t_MMF2ORDI+t_MMF2IMPE),1)) As Mov_Cont,Max(t_MMF2RISE) As Mov_Rise "
        this.w_PADRE.Exec_Select("Tmp_4", this.w_cFields , Cp_ToStrODBC(this.w_PADRE.cFunction)+"<>'Query' And Not Deleted() And not Empty(Nvl(t_MMKEYSAL ,'')) And ( t_MMF2CASC In ('+','-') Or t_MMF2RISE In ('+','-') Or t_MMF2ORDI In ('+','-') Or t_MMF2IMPE In ('+','-')  )" ,"","KEYSAL,CODMAG","")     
        * --- Metto assieme i quattro cursori..
         
 Select * From Tmp_1 Union All; 
 Select * From Tmp_2 Union All; 
 Select * From Tmp_3 Union All; 
 Select * From Tmp_4 Into Cursor Trs_Add NoFilter
        * --- Rimuovo i cursori..
         
 Use In Tmp_1 
 Use In Tmp_2 
 Use In Tmp_3 
 Use In Tmp_4
      case Upper ( this.w_PADRE.Class )= "TGSAR_BEA" 
        * --- Nel caso di esplosione distinta, nel batch GSAR_BEA viene gi� creato il cursore 
        *     Trs_Add. Quindi non devo fare niente
      case Upper ( this.w_PADRE.Class )= "TGSOF_BGD" 
        * --- Nel caso di generazione ordine da offerta, nel batch GSOF_BGD viene gi� creato il cursore 
        *     Trs_Add.
    endcase
    * --- Sommo il risultato in un unico trs...
     
 Select Min(Riga) As Riga, Sum( Disp ) As Disp, Sum( Dispc ) As Dispc, Sum( Dispr ) As Dispr, Keysal, Codmag, Max( Mov_Disp ) As Mov_Disp, ; 
 Max( Mov_Cont ) As Mov_Cont , Max( Mov_Rise ) As Mov_Rise From Trs_Add Group By KEYSAL,CODMAG Into Cursor _Temp_ NoFilter
     
 Select Trs_Add 
 Use
    if Used("Trs_Eva")
      do while Not Eof("Trs_Eva")
        this.w_ROW = Nvl( Trs_Eva.Riga , 0 )
        this.w_SERRIF = Trs_Eva.SerRif
        this.w_ROWRIF = Trs_Eva.RowRif
        this.w_NUMRIF = Trs_Eva.NumRif
        this.w_DELTA = Nvl( Trs_Eva.Delta , 0 )
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVKEYSAL,MVCODMAG,MVFLRISE,MVFLIMPE,MVFLORDI,MVCODMAT,MVF2RISE,MVF2IMPE,MVF2ORDI"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVKEYSAL,MVCODMAG,MVFLRISE,MVFLIMPE,MVFLORDI,MVCODMAT,MVF2RISE,MVF2IMPE,MVF2ORDI;
            from (i_cTable) where;
                MVSERIAL = this.w_SERRIF;
                and CPROWNUM = this.w_ROWRIF;
                and MVNUMRIF = this.w_NUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODART = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
          this.w_CODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
          this.w_FLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
          this.w_FLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
          this.w_FLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
          this.w_CODMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
          this.w_F2RISE = NVL(cp_ToDate(_read_.MVF2RISE),cp_NullValue(_read_.MVF2RISE))
          this.w_F2IMPE = NVL(cp_ToDate(_read_.MVF2IMPE),cp_NullValue(_read_.MVF2IMPE))
          this.w_F2ORDI = NVL(cp_ToDate(_read_.MVF2ORDI),cp_NullValue(_read_.MVF2ORDI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Rendo scrivibile il transitorio per stornare...
        if Recno( "Trs_Eva" )=1
          Cur=WrCursor( "_Temp_" )
        endif
        * --- La riga evasa movimenta ordinato/impegnato / riservato, se si allora
        *     il magazzino sar� stornato
        if Not Empty( this.w_FLRISE + this.w_FLIMPE + this.w_FLORDI )
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Not Empty( this.w_F2RISE + this.w_F2IMPE + this.w_F2ORDI ) And Not Empty( this.w_CODMAT )
          this.w_FLRISE = this.w_F2RISE
          this.w_FLIMPE = this.w_F2IMPE
          this.w_FLORDI = this.w_F2ORDI
          this.w_CODMAG = this.w_CODMAT
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
         
 Select Trs_Eva 
 Skip
      enddo
      * --- Rimuovo il temporaneo
       
 Select Trs_Eva 
 Use
    endif
    this.w_OK = .T.
    * --- Scorro il transitorio e riga per riga verifico la disponibilit� sul database...
     
 Select _Temp_ 
 Go Top
    do while Not Eof( "_Temp_" )
      this.w_CODART = _Temp_.KEYSAL
      this.w_CODMAG = _Temp_.CODMAG
      this.w_DISP = Nvl ( _Temp_.Disp , 0 )
      this.w_DISPC = Nvl ( _Temp_.DispC , 0 )
      this.w_DISPR = Nvl ( _Temp_.DispR , 0 )
      this.w_NROW = Alltrim(Str( _Temp_.Riga ))
      this.w_MOV_DISP = Nvl ( _Temp_.Mov_Disp , "" )
      this.w_MOV_CONT = Nvl ( _Temp_.Mov_Cont , "" )
      this.w_MOV_RISE = Nvl ( _Temp_.Mov_Rise , "" )
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARFLDISP,ARFLDISC"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARFLDISP,ARFLDISC;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLDISP = NVL(cp_ToDate(_read_.ARFLDISP),cp_NullValue(_read_.ARFLDISP))
        this.w_FLDISC = NVL(cp_ToDate(_read_.ARFLDISC),cp_NullValue(_read_.ARFLDISC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.pNoTransaction="S"
        if ( this.w_FLDISP$"SC" And ( Not Empty( this.w_MOV_DISP ) And this.w_DISP< 0 or Not Empty( this.w_MOV_RISE ) and this.w_DISPR<0 ) ) Or ( this.w_FLDISC$"SC" And Not Empty( this.w_MOV_DISP + this.w_MOV_CONT ) And this.w_DISPC< 0 )
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        if ( this.w_FLDISP="S" And ( Not Empty( this.w_MOV_DISP) And this.w_DISP<0 or Not Empty( this.w_MOV_RISE) And this.w_DISPR<0 ) ) Or ( this.w_FLDISC="S" And Not Empty( this.w_MOV_DISP + this.w_MOV_CONT ) And this.w_DISPC<0 )
          * --- Leggo il saldo articolo / magazzino sul database..
          *     La disponibilit� sul documento non conta pi� leggo il dato direttamente
          *     sul saldo ed aggiornato...
          this.w_DISP = 0
          this.w_DISPC = 0
          this.w_DISPR = 0
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if ! Empty (this.w_MESBLOK)
          Exit
        endif
      endif
       
 Select _Temp_ 
 Skip
    enddo
     
 Select _Temp_ 
 Use
    if TYPE("pMandatesi")="C" AND this.pMandatesi="S"
      this.w_RESOCON1 = this.oparentobject.w_RESOCON1+this.w_RESOCON1
      this.w_MESBLOK = this.oparentobject.w_MESBLOK+this.w_MESBLOK
    endif
    if UPPER(this.oParentObject.CLASS)="TGSVE_BCK"
      this.oparentobject.w_RESOCON1=this.oparentobject.w_RESOCON1+this.w_RESOCON1 
 this.oparentobject.w_MESBLOK=this.oparentobject.w_MESBLOK+this.w_MESBLOK
      if ! Empty (this.w_MESBLOK)
        this.w_OK = .F.
      endif
    else
      if this.pNoTransaction="S" 
        if ! Empty(this.w_MESBLOK) Or !Empty(this.w_RESOCON1)
          if VARTYPE(this.w_PADRE)=="O" AND lower(this.w_PADRE.class)=="tgsva_bps"
            if !Empty(this.w_MESBLOK)
              this.w_OK = .F.
              this.oParentObject.w_MESS = this.w_MESBLOK
            endif
          else
            * --- Visualizzo la maschera con all'interno tutti i messagi di errore
            this.w_ANNULLA = False
            this.w_DAIM = False
            * --- L'HasEvent  � lanciata all'interno della Cp_DOaction che mette a .t.
            *     i_stopFk per impedire successive pressioni di tasto. Per poter
            *     gestire Esc e F10 sulla maschera la valorizzo a .F.
            if type("i_stopFK")="U"
              public i_stopFK
            endif
            this.w_OLD_STOPFK = i_stopFK
            i_stopFK=.f.
            do GSVE_KLG with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_OK = this.w_ANNULLA
            i_stopFK=this.w_OLD_STOPFK
            if TYPE("pMandatesi")="C" AND this.pMandatesi="S"
              * --- Se gi� messe a video le sbianco nel caso nel batch chiamante sia utilizzata altre volte la maschera GSVE_KLG
              this.oparentobject.w_RESOCON1="" 
 this.oparentobject.w_MESBLOK=""
            endif
          endif
        endif
      else
        if ! Empty (this.w_MESBLOK)
          * --- Notifico al batch chiamante l'annullo della transazione...
          this.oParentObject.w_MESS = this.w_MESBLOK
          this.w_OK = .F.
        endif
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_OK
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tipo errore
    *     0 = ok
    *     000001 (1)  = Warning (Contabile)
    *     000010 (2) = Bloccante (Contabile)
    *     000100 (4) = Warning (Esistenza)
    *     001000 (8) = Bloccante (Esistenza)
    *     010000 (16) = Warning (Riservato)
    *     100000 (32) = Bloccante (Riservato)
    * --- Read from SALDIART
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SLQTAPER,SLQTRPER,SLQTOPER,SLQTIPER"+;
        " from "+i_cTable+" SALDIART where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_CODART);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SLQTAPER,SLQTRPER,SLQTOPER,SLQTIPER;
        from (i_cTable) where;
            SLCODICE = this.w_CODART;
            and SLCODMAG = this.w_CODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_QTAPER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
      this.w_QTRPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
      this.w_QTOPER = NVL(cp_ToDate(_read_.SLQTOPER),cp_NullValue(_read_.SLQTOPER))
      this.w_QTIPER = NVL(cp_ToDate(_read_.SLQTIPER),cp_NullValue(_read_.SLQTIPER))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TERR = 0
    this.w_QTAMSG = 0
    this.w_QTAMSG2 = 0
    this.w_QTAMSG3 = 0
    * --- Verifico se articolo Ok...
    * --- Disponibilit� contabile
    if this.w_FLDISC$"SC" And ( this.w_QTAPER - this.w_QTRPER + this.w_QTOPER - this.w_QTIPER ) + this.w_DISPC< 0 AND !Empty( this.w_MOV_CONT )
      this.w_QTAMSG = this.w_QTAPER - this.w_QTRPER + this.w_QTOPER - this.w_QTIPER + this.w_DISPC
      if this.w_FLDISC = "C"
        this.w_TERR = BITOR( this.w_TERR ,1 )
      else
        this.w_TERR = BITOR( this.w_TERR ,2 )
      endif
    endif
    * --- Disponibilit� reale
    if this.w_FLDISP$"SC" And ( this.w_QTAPER - this.w_QTRPER ) + this.w_DISP < 0 AND !Empty( this.w_MOV_DISP )
      this.w_QTAMSG2 = this.w_QTAPER - this.w_QTRPER + this.w_DISP
      if this.w_FLDISP = "C"
        this.w_TERR = BITOR( this.w_TERR ,4 )
      else
        this.w_TERR = BITOR( this.w_TERR ,8 )
      endif
    endif
    * --- Riservato
    if this.w_FLDISP$"SC" And this.w_QTRPER + this.w_DISPR< 0 AND !Empty( this.w_MOV_RISE )
      this.w_QTAMSG3 = this.w_QTRPER + this.w_DISPR
      if this.w_FLDISP = "C"
        this.w_TERR = BITOR( this.w_TERR ,16 )
      else
        this.w_TERR = BITOR( this.w_TERR ,32 )
      endif
    endif
    if this.w_TERR<>0
      if Upper ( this.w_PADRE.Class )= "TGSAR_BEA" 
        * --- Numero riga � la riga del documento padre dalla quale viene esploso il documento 
        this.w_LMESS = ah_MsgFormat("Articolo: %1%0Riga documento di origine: %2%0Magazzino: %3%0", ALLTR( this.w_CODART ), this.w_NROW, this.w_CODMAG)
      else
        this.w_LMESS = ah_MsgFormat("Articolo: %1%0Prima occorrenza articolo Riga: %2%0Magazzino: %3%0", ALLTR( this.w_CODART ), this.w_NROW, this.w_CODMAG)
      endif
      if BITAND( this.w_TERR , 1 ) > 0 OR BITAND( this.w_TERR , 4 ) > 0 OR BITAND( this.w_TERR , 16 ) > 0
        * --- Warning
        this.w_RESOCON1 = this.w_RESOCON1 + IIF(EMPTY(this.w_RESOCON1),"", Chr(13) + Chr(13)) + this.w_LMESS
        if BITAND( this.w_TERR , 1 ) > 0
          this.w_RESOCON1 = this.w_RESOCON1 + ah_MsgFormat("Disponibilit� contabile articolo negativa (%1)%0", TRAN( this.w_QTAMSG ,"@Z "+v_PQ(12)))
        endif
        if BITAND( this.w_TERR , 4 ) > 0
          this.w_RESOCON1 = this.w_RESOCON1 + ah_MsgFormat("Disponibilit� articolo negativa (%1)%0", TRAN( this.w_QTAMSG2 ,"@Z "+v_PQ(12)))
        endif
        if BITAND( this.w_TERR , 16 ) > 0
          this.w_RESOCON1 = this.w_RESOCON1 + ah_MsgFormat("Quantit� riservata articolo negativa (%1)%0", TRAN( this.w_QTAMSG3 ,"@Z "+v_PQ(12)))
        endif
      endif
      if BITAND( this.w_TERR , 2 ) > 0 OR BITAND( this.w_TERR , 8 ) > 0 OR BITAND( this.w_TERR , 32 ) > 0
        * --- Bloccante
        this.w_MESBLOK = this.w_MESBLOK + IIF(EMPTY(this.w_MESBLOK),"", Chr(13) + Chr(13)) + this.w_LMESS
        if BITAND( this.w_TERR , 2 ) > 0
          this.w_MESBLOK = this.w_MESBLOK + ah_MsgFormat("Disponibilit� contabile articolo negativa (%1)%0", TRAN( this.w_QTAMSG ,"@Z "+v_PQ(12)))
        endif
        if BITAND( this.w_TERR , 8 ) > 0
          this.w_MESBLOK = this.w_MESBLOK + ah_MsgFormat("Disponibilit� articolo negativa (%1)%0", TRAN( this.w_QTAMSG2 ,"@Z "+v_PQ(12)))
        endif
        if BITAND( this.w_TERR , 32 ) > 0
          this.w_MESBLOK = this.w_MESBLOK + ah_MsgFormat("Quantit� riservata articolo negativa (%1)%0", TRAN( this.w_QTAMSG3 ,"@Z "+v_PQ(12)))
        endif
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ho due disponibilit� contabile e normale, nel primo caso storno per
    *     qualsiasi flag mentre per quella normale solo se il documento di 
    *     origine movimenta il riservato
    this.w_DELTAC = this.w_DELTA* IIF( this.w_FLIMPE="+",-1,1)*IIF( this.w_FLORDI="-",-1,1)* IIF( this.w_FLRISE="+",-1, 1 )
    this.w_DELTA = this.w_DELTA* IIF( this.w_FLRISE="+",-1, IIF( this.w_FLRISE="-",1,0 ) )
    * --- Ricerco l'articolo ed il magazzino...
    Select _Temp_ 
 Go Top 
 Locate For KeySal= this.w_CODART And CodMag = this.w_CODMAG
    if Found()
      * --- Se trovo la coppia dettraggo dalla disponibilita le quantita stornate per evasione..
      Replace Disp With Disp - this.w_DELTA, DispC With DispC - this.w_DELTAC, DispR With DispR + this.w_DELTA, Mov_Rise WITH IIF( this.w_FLRISE="+","-", IIF( this.w_FLRISE="-","+"," " ) )
    else
      * --- La riga evade ma non movimenta il magazzino, l'aggiungo cmq al controllo
      *     per verificare se la sua evasione (e quindi lo storno) manda in negativo le giacenze
       
 Insert into _Temp_ (Riga,Disp,Dispc,Dispr,Keysal,Codmag,Mov_Rise ) Values; 
 ( this.w_ROW , -this.w_DELTA ,-this.w_DELTAC , this.w_DELTA , this.w_CODART , this.w_CODMAG, IIF( this.w_FLRISE="+","-", IIF( this.w_FLRISE="-","+"," " ) ) )
    endif
  endproc


  proc Init(oParentObject,pNoTransaction,w_PADRE,pMandatesi)
    this.pNoTransaction=pNoTransaction
    this.w_PADRE=w_PADRE
    this.pMandatesi=pMandatesi
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='SALDIART'
    this.cWorkTables[3]='DOC_DETT'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNoTransaction,w_PADRE,pMandatesi"
endproc
