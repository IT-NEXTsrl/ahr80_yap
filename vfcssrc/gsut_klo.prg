* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_klo                                                        *
*              Opzioni shortcut                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-12-19                                                      *
* Last revis.: 2015-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_klo",oParentObject))

* --- Class definition
define class tgsut_klo as StdForm
  Top    = 12
  Left   = 10

  * --- Standard Properties
  Width  = 500
  Height = 226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-30"
  HelpContextID=51821719
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_klo"
  cComment = "Opzioni shortcut"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_LABEL = space(252)
  w_TOOLTIP = space(252)
  w_IMAGE = space(252)
  o_IMAGE = space(252)
  w_APPLYTHEMEIMG = .F.
  o_APPLYTHEMEIMG = .F.
  w_IMAGETMP = space(252)
  w_PROGRAM = space(252)
  w_FONTCLR = 0
  w_MODIFYMODE = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kloPag1","gsut_klo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLABEL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LABEL=space(252)
      .w_TOOLTIP=space(252)
      .w_IMAGE=space(252)
      .w_APPLYTHEMEIMG=.f.
      .w_IMAGETMP=space(252)
      .w_PROGRAM=space(252)
      .w_FONTCLR=0
      .w_MODIFYMODE=.f.
      .w_LABEL=oParentObject.w_LABEL
      .w_TOOLTIP=oParentObject.w_TOOLTIP
      .w_IMAGE=oParentObject.w_IMAGE
      .w_APPLYTHEMEIMG=oParentObject.w_APPLYTHEMEIMG
      .w_PROGRAM=oParentObject.w_PROGRAM
      .w_FONTCLR=oParentObject.w_FONTCLR
          .DoRTCalc(1,4,.f.)
        .w_IMAGETMP = Iif(.w_APPLYTHEMEIMG, cp_TmpColorizeImg(.w_IMAGE, .w_FONTCLR), .w_IMAGE)
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate(.w_IMAGETMP)
          .DoRTCalc(6,7,.f.)
        .w_MODIFYMODE = Vartype(oGadgetManager.oCharmBar)='O' And oGadgetManager.oCharmBar.bModifyMode
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_LABEL=.w_LABEL
      .oParentObject.w_TOOLTIP=.w_TOOLTIP
      .oParentObject.w_IMAGE=.w_IMAGE
      .oParentObject.w_APPLYTHEMEIMG=.w_APPLYTHEMEIMG
      .oParentObject.w_PROGRAM=.w_PROGRAM
      .oParentObject.w_FONTCLR=.w_FONTCLR
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_APPLYTHEMEIMG<>.w_APPLYTHEMEIMG.or. .o_IMAGE<>.w_IMAGE
            .w_IMAGETMP = Iif(.w_APPLYTHEMEIMG, cp_TmpColorizeImg(.w_IMAGE, .w_FONTCLR), .w_IMAGE)
        endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(.w_IMAGETMP)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(.w_IMAGETMP)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLABEL_1_1.enabled = this.oPgFrm.Page1.oPag.oLABEL_1_1.mCond()
    this.oPgFrm.Page1.oPag.oTOOLTIP_1_3.enabled = this.oPgFrm.Page1.oPag.oTOOLTIP_1_3.mCond()
    this.oPgFrm.Page1.oPag.oIMAGE_1_4.enabled = this.oPgFrm.Page1.oPag.oIMAGE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oAPPLYTHEMEIMG_1_5.enabled = this.oPgFrm.Page1.oPag.oAPPLYTHEMEIMG_1_5.mCond()
    this.oPgFrm.Page1.oPag.oPROGRAM_1_8.enabled = this.oPgFrm.Page1.oPag.oPROGRAM_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLABEL_1_1.value==this.w_LABEL)
      this.oPgFrm.Page1.oPag.oLABEL_1_1.value=this.w_LABEL
    endif
    if not(this.oPgFrm.Page1.oPag.oTOOLTIP_1_3.value==this.w_TOOLTIP)
      this.oPgFrm.Page1.oPag.oTOOLTIP_1_3.value=this.w_TOOLTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAGE_1_4.value==this.w_IMAGE)
      this.oPgFrm.Page1.oPag.oIMAGE_1_4.value=this.w_IMAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oAPPLYTHEMEIMG_1_5.RadioValue()==this.w_APPLYTHEMEIMG)
      this.oPgFrm.Page1.oPag.oAPPLYTHEMEIMG_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGRAM_1_8.value==this.w_PROGRAM)
      this.oPgFrm.Page1.oPag.oPROGRAM_1_8.value=this.w_PROGRAM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IMAGE = this.w_IMAGE
    this.o_APPLYTHEMEIMG = this.w_APPLYTHEMEIMG
    return

enddefine

* --- Define pages as container
define class tgsut_kloPag1 as StdContainer
  Width  = 496
  height = 226
  stdWidth  = 496
  stdheight = 226
  resizeXpos=344
  resizeYpos=165
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLABEL_1_1 as StdField with uid="OBXQEZQUHD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LABEL", cQueryName = "LABEL",;
    bObbl = .f. , nPag = 1, value=space(252), bMultilanguage =  .f.,;
    ToolTipText = "Etichetta del link",;
    HelpContextID = 136323766,;
   bGlobalFont=.t.,;
    Height=21, Width=405, Left=81, Top=10, InputMask=replicate('X',252)

  func oLABEL_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  add object oTOOLTIP_1_3 as StdField with uid="NALXIPMEYM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TOOLTIP", cQueryName = "TOOLTIP",;
    bObbl = .f. , nPag = 1, value=space(252), bMultilanguage =  .f.,;
    ToolTipText = "Etichetta del link",;
    HelpContextID = 240647882,;
   bGlobalFont=.t.,;
    Height=21, Width=405, Left=81, Top=33, InputMask=replicate('X',252)

  func oTOOLTIP_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  add object oIMAGE_1_4 as StdField with uid="ZQPDPPPTRS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IMAGE", cQueryName = "IMAGE",;
    bObbl = .f. , nPag = 1, value=space(252), bMultilanguage =  .f.,;
    ToolTipText = "Immagine associata al link",;
    HelpContextID = 129113734,;
   bGlobalFont=.t.,;
    Height=21, Width=309, Left=81, Top=56, InputMask=replicate('X',252), bHasZoom = .t. 

  func oIMAGE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  proc oIMAGE_1_4.mZoom
    This.Parent.oContained.w_IMAGE = GetFile()
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAPPLYTHEMEIMG_1_5 as StdCheck with uid="GUKQVBMTSJ",rtseq=4,rtrep=.f.,left=393, top=59, caption="Applica tema",;
    ToolTipText = "Modifica i colori dell'immagine in base al tema",;
    HelpContextID = 28961563,;
    cFormVar="w_APPLYTHEMEIMG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAPPLYTHEMEIMG_1_5.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oAPPLYTHEMEIMG_1_5.GetRadio()
    this.Parent.oContained.w_APPLYTHEMEIMG = this.RadioValue()
    return .t.
  endfunc

  func oAPPLYTHEMEIMG_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_APPLYTHEMEIMG==.T.,1,;
      0)
  endfunc

  func oAPPLYTHEMEIMG_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc

  add object oPROGRAM_1_8 as StdField with uid="ZKDLNKBORJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PROGRAM", cQueryName = "PROGRAM",;
    bObbl = .f. , nPag = 1, value=space(252), bMultilanguage =  .f.,;
    ToolTipText = "Programma da eseguire al click",;
    HelpContextID = 159581174,;
   bGlobalFont=.t.,;
    Height=90, Width=309, Left=81, Top=79, InputMask=replicate('X',252)

  func oPROGRAM_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODIFYMODE)
    endwith
   endif
  endfunc


  add object oObj_1_10 as cp_showimage with uid="RSVKVUNMJY",left=394, top=82, width=83,height=82,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="default bitmap",stretch=2,;
    nPag=1;
    , HelpContextID = 38616090


  add object oBtn_1_11 as StdButton with uid="MRBRWTXHBW",left=81, top=173, width=48,height=45,;
    CpPicture="BMP\RUN.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per testare l'espressione programma";
    , HelpContextID = 59922230;
    , Caption='\<Test';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        TestPrg(ThisForm)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="ZMIZZNTUDJ",left=388, top=173, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 51850470;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="JWIBHYOYEX",left=438, top=173, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 59139142;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="PUYPJVNLWT",Visible=.t., Left=26, Top=12,;
    Alignment=1, Width=55, Height=18,;
    Caption="Etichetta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="DPMDUQFFLW",Visible=.t., Left=17, Top=58,;
    Alignment=1, Width=64, Height=18,;
    Caption="Immagine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XKEOSFCVXJ",Visible=.t., Left=6, Top=79,;
    Alignment=1, Width=75, Height=18,;
    Caption="Programma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="ERLOZCJUDQ",Visible=.t., Left=26, Top=35,;
    Alignment=1, Width=55, Height=18,;
    Caption="Tooltip:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_klo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_klo
Proc TestPrg(pParent)
  Local OldErr, bErr
  bErr = .F.
  OldErr = On("Error")
  On Error bErr=.T.
  Eval(m.pParent.w_PROGRAM)
  If m.bErr
    Ah_ErrorMsg([Si � verificato un errore durante il test dell'espressione "Programma"])
  EndIf
  On Error &OldErr
EndProc
* --- Fine Area Manuale
